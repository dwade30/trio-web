﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptExtractReport.
	/// </summary>
	public partial class rptExtractReport : BaseSectionReport
	{
		public static rptExtractReport InstancePtr
		{
			get
			{
				return (rptExtractReport)Sys.GetInstance(typeof(rptExtractReport));
			}
		}

		protected rptExtractReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				RSExtract?.Dispose();
				rsTemp?.Dispose();
				RSMast?.Dispose();
				rsReport?.Dispose();
				RSPrint?.Dispose();
				rsMisc?.Dispose();
                RSExtract = null;
                rsTemp = null;
                RSMast = null;
                rsReport = null;
                RSPrint = null;
                rsMisc = null;
                clCodeList = null;
            }
			base.Dispose(disposing);
		}

		public rptExtractReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Extract Report";
		}
		// nObj = 1
		//   0	rptExtractReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// Dim dbTemp As DAO.Database
		clsDRWrapper RSExtract = new clsDRWrapper();
		
		clsDRWrapper rsTemp = new clsDRWrapper();
		clsDRWrapper RSMast = new clsDRWrapper();
		clsDRWrapper rsReport = new clsDRWrapper();
		clsDRWrapper RSPrint = new clsDRWrapper();
		clsDRWrapper rsMisc = new clsDRWrapper();
		bool boolCol1;
		bool booldone;
		int intLineNumber;
		int intLinesToPrint;
		int intCurrReport;
		int intCurrGroup;
		bool boolFirstTime;
		// vbPorter upgrade warning: strDataAmount As string	OnWrite(string, int, double)
		string strDataAmount = "";
		string strDataDesc = "";
		bool boolGroupFired;
		// vbPorter upgrade warning: qdf As Variant --> As object
		object qdf;
		string strSQL2;
		string strSQL1;
		int intNumReports;
		bool boolOKtoSave;
		int lngPage;
		int intlargestnumlines;
		bool boolTotals;
		bool boolSaleType;
		int lngUID;
		string strOpen3 = "";
		string strOpen4 = "";
		string strOpen5 = "";
		string strXCoord = "";
		string strYCoord = "";
		clsReportCodes clCodeList = new clsReportCodes();
		int intLastGroup;

		private void GetOpenStrings()
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1091",
                    modGlobalVariables.Statics.strRECostFileDatabase);
                if (!clsLoad.EndOfFile())
                {
                    strXCoord = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                    if (strXCoord == string.Empty)
                    {
                        strXCoord = "User Defined 1";
                    }
                }
                else
                {
                    strXCoord = "User Defined 1";
                }

                clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1092",
                    modGlobalVariables.Statics.strRECostFileDatabase);
                if (!clsLoad.EndOfFile())
                {
                    strYCoord = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                    if (strYCoord == string.Empty)
                    {
                        strYCoord = "User Defined 2";
                    }
                }
                else
                {
                    strYCoord = "User Defined 2";
                }

                clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1520",
                    modGlobalVariables.Statics.strRECostFileDatabase);
                if (!clsLoad.EndOfFile())
                {
                    strOpen3 = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                    if (strOpen3 == string.Empty)
                    {
                        strOpen3 = "Open 3";
                    }
                }
                else
                {
                    strOpen3 = "Open 3";
                }

                clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1530",
                    modGlobalVariables.Statics.strRECostFileDatabase);
                if (!clsLoad.EndOfFile())
                {
                    strOpen4 = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                    if (strOpen4 == string.Empty)
                    {
                        strOpen4 = "Open 4";
                    }
                }
                else
                {
                    strOpen4 = "Open 4";
                }

                clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1570",
                    modGlobalVariables.Statics.strRECostFileDatabase);
                if (!clsLoad.EndOfFile())
                {
                    strOpen5 = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                    if (strOpen5 == string.Empty)
                    {
                        strOpen5 = "Open 5";
                    }
                }
                else
                {
                    strOpen5 = "Open 5";
                }
            }
        }

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			GetOpenStrings();
			Fields.Add("thebinder");
			Fields["thebinder"].Value = 0;
			// Call SetupNextReport(intCurrReport)
			SetupNextReport(ref intCurrGroup);
			boolFirstTime = true;
			SubReport1.Report = new srptExtractTotals();
			lngPage = 1;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			RunButDontPrint:
			;
			if (!RSMast.EndOfFile())
			{
				intCurrGroup = FCConvert.ToInt32(RSMast.Get_Fields_Int32("groupnumber"));
			}
			else
			{
				return;
			}
			if (boolFirstTime)
			{
				if (!RSMast.EndOfFile())
				{
					if (!boolSaleType)
					{
						TWRE0000.rptExtractReport.InstancePtr.Fields["thebinder"].Value = RSMast.Get_Fields_Int32("rsaccount") + RSMast.Get_Fields_Int32("rscard");
					}
					else
					{
						TWRE0000.rptExtractReport.InstancePtr.Fields["thebinder"].Value = RSMast.Get_Fields_Int32("rsaccount") + RSMast.Get_Fields_Int32("rscard") + RSMast.Get_Fields_Int32("saleid");
					}
					// boolFirstTime = False
					intCurrGroup = FCConvert.ToInt32(RSMast.Get_Fields_Int32("groupnumber"));
				}
				eArgs.EOF = false;
				return;
			}
			if (intLineNumber > intLinesToPrint)
			{
				RSMast.MoveNext();
				if (!RSMast.EndOfFile())
				{
					if (!boolSaleType)
					{
						TWRE0000.rptExtractReport.InstancePtr.Fields["thebinder"].Value = RSMast.Get_Fields_Int32("rsaccount") + RSMast.Get_Fields_Int32("rscard");
					}
					else
					{
						TWRE0000.rptExtractReport.InstancePtr.Fields["thebinder"].Value = RSMast.Get_Fields_Int32("rsaccount") + RSMast.Get_Fields_Int32("rscard") + RSMast.Get_Fields_Int32("saleid");
					}
					intCurrGroup = FCConvert.ToInt32(RSMast.Get_Fields_Int32("groupnumber"));
				}
				// End If
			}
			else if (intLinesToPrint > 5)
			{
				TWRE0000.rptExtractReport.InstancePtr.Detail.CanGrow = true;
				// rptExtractReport.Detail.Height = 240
				TWRE0000.rptExtractReport.InstancePtr.Detail.Visible = true;
				// rptExtractReport.Detail.CanGrow = False
				FillNextLine();
				// If intLineNumber > intLinesToPrint Then
				// RSMast.MoveNext
				// If Not RSMast.eof Then
				// rptExtractReport.Fields("thebinder").Value = RSMast.fields("rsaccount") & RSMast.fields("rscard")
				// End If
				// End If
			}
			else
			{
				// rptExtractReport.Detail.CanGrow = True
				// rptExtractReport.Detail.Height = 0
				// rptExtractReport.Detail.CanGrow = False
				TWRE0000.rptExtractReport.InstancePtr.Detail.Visible = false;
				RSMast.MoveNext();
				if (!RSMast.EndOfFile())
				{
					intCurrGroup = FCConvert.ToInt32(RSMast.Get_Fields_Int32("groupnumber"));
					if (!boolSaleType)
					{
						TWRE0000.rptExtractReport.InstancePtr.Fields["thebinder"].Value = RSMast.Get_Fields_Int32("rsaccount") + RSMast.Get_Fields_Int32("rscard");
					}
					else
					{
						TWRE0000.rptExtractReport.InstancePtr.Fields["thebinder"].Value = RSMast.Get_Fields_Int32("rsaccount") + RSMast.Get_Fields_Int32("rscard") + RSMast.Get_Fields_Int32("saleid");
					}
				}
			}
			eArgs.EOF = false;
		}

		private void ActiveReport_Initialize()
		{
			boolCol1 = false;
			booldone = true;
			// Set DBTemp = OpenDatabase(strredatabase, False, False, ";PWD=" & DATABASEPASSWORD)
			// Set RSExtract = DBTemp.OpenRecordset("Select * from extract where reportnumber = 0")
			// 
			// txtDate = rsextract.fields("extractdate") & ""
			// txtTitle = rsextract.fields("title") & ""
			// 
			// Set RSExtract = DBTemp.OpenRecordset("select * from extracttable where accountnumber > 0 order by groupnumber,accountnumber,cardnumber")
			// 
		}

		public void Start()
		{
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = RSExtract.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			boolSaleType = false;
			// strSQL1 = "select extracttable.* from (extracttable inner join (labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber))"
			// strSQL1 = "select distinct extracttable.groupnumber from (extracttable inner join (labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) where extracttable.userid = " & lngUID & " "
			strSQL1 = "select distinct extracttable.groupnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " + FCConvert.ToString(lngUID) + " ";
			// strSQL2 = "select master.*,EXTRACTtable.GROUPNUMBER from (master inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and master.rsdeleted = 0 "
			// strSQL2 = "select mj.*,EXTRACTtable.GROUPNUMBER from (" & strMasterJoinQuery & " inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and mj.rsdeleted = 0 "
			strSQL2 = "select mj.*,EXTRACTtable.GROUPNUMBER from (" + strMasterJoinQuery + " inner join extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and mj.rsdeleted = 0 ";
			// strSQL = "select  mj.* from (" & strMasterJoinQuery & " inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and mj.rsdeleted = 0"
			RSExtract.OpenRecordset("select * from extract inner join labelaccounts on (extract.groupnumber = labelaccounts.accountnumber) and (labelaccounts.userid = extract.userid) where reportnumber > 0 and extract.userid = " + FCConvert.ToString(lngUID), modGlobalVariables.strREDatabase);
			intNumReports = 0;
			while (!RSExtract.EndOfFile())
			{
				txtGroupTitles.Text = txtGroupTitles.Text + RSExtract.Get_Fields_Int32("groupnumber") + "  " + RSExtract.Get_Fields_String("title") + "\r\n";
				if (Conversion.Val(RSExtract.Get_Fields_Int32("groupnumber") + "") > intNumReports)
				{
					intNumReports = FCConvert.ToInt32(Math.Round(Conversion.Val(RSExtract.Get_Fields_Int32("groupnumber"))));
				}
				RSExtract.MoveNext();
			}
			// intNumReports = RSExtract.RecordCount
			intlargestnumlines = 1;
			modGlobalVariables.Statics.reporttotals = new double[intNumReports + 1, 1 + 1];
			RSExtract.OpenRecordset("Select * from extract where reportnumber = 0 and userid = " + FCConvert.ToString(lngUID), modGlobalVariables.strREDatabase);
			txtDate.Text = "Date Extracted " + RSExtract.Get_Fields_DateTime("extractdate") + "";
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDatePrinted.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtPage.Text = "Page 1";
			if (Strings.Trim(RSExtract.Get_Fields_String("title") + "") != string.Empty)
			{
				txtTitle.Text = RSExtract.Get_Fields_String("Title") + "";
			}
			if (Strings.UCase(FCConvert.ToString(RSExtract.Get_Fields_String("EXTtype"))) == "SALE")
			{
				// must change strsql2
				boolSaleType = true;
				// strSQL2 = "select srmaster.*,EXTRACTtable.GROUPNUMBER from (srmaster inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (srmaster.rsaccount = extracttable.accountnumber) and (srmaster.rscard = extracttable.cardnumber) and (srmaster.saleid = extracttable.saleid)) where extracttable.userid = " & lngUID & " and srmaster.rsdeleted = 0 "
				strSQL2 = "select srmaster.*,EXTRACTtable.GROUPNUMBER from (srmaster inner join extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)  on (srmaster.rsaccount = extracttable.accountnumber) and (srmaster.rscard = extracttable.cardnumber) and (srmaster.saleid = extracttable.saleid)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and srmaster.rsdeleted = 0 ";
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) != "RSLOCSTREET")
				{
					RSMast.OpenRecordset(strSQL2 + " order by extracttable.groupnumber,srmaster." + modPrintRoutines.Statics.gstrFieldName + ",srmaster.rscard,srmaster.saleid", modGlobalVariables.strREDatabase);
				}
				else
				{
					RSMast.OpenRecordset(strSQL2 + " order by extracttable.groupnumber,srmaster.rslocstreet,val(srmaster.rslocnumalph & ''),srmaster.rscard,srmaster.saleid", modGlobalVariables.strREDatabase);
				}
			}
			else
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) != "RSLOCSTREET")
				{
					RSMast.OpenRecordset(strSQL2 + " order by extracttable.groupnumber,mj." + modPrintRoutines.Statics.gstrFieldName + ",mj.rscard", modGlobalVariables.strREDatabase);
				}
				else
				{
					RSMast.OpenRecordset(strSQL2 + " order by extracttable.groupnumber,mj.rslocstreet,mj.rslocnumalph ,mj.rscard", modGlobalVariables.strREDatabase);
				}
			}
			// Set RSExtract = dbTemp.OpenRecordset(strSQL1 & " order by extracttable.accountnumber,extracttable.cardnumber,extracttable.groupnumber",strredatabase)
			RSExtract.OpenRecordset(strSQL1 + " order by extracttable.groupnumber", modGlobalVariables.strREDatabase);
			if (!RSMast.EndOfFile())
			{
				intCurrGroup = FCConvert.ToInt32(RSExtract.Get_Fields_Int32("groupnumber"));
			}
			else
			{
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rsTemp.OpenRecordset("select * from extract where groupnumber = " + FCConvert.ToString(intCurrGroup), modGlobalVariables.strREDatabase);
			intCurrReport = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("reportnumber"));
			// Call MakeQueryDefs
			DeleteQueryDefs();
			// Me.Show
			clCodeList.LoadCodes();
			frmReportViewer.InstancePtr.Init(this);
		}

		private void SetupCol1()
		{
			txtCard.Text = FCConvert.ToString(RSMast.Get_Fields_Int32("rscard"));
			txtName.Text = RSMast.Get_Fields_String("rsname") + "";
			txtmaplot.Text = RSMast.Get_Fields_String("rsmaplot") + "";
			txtaddr1.Text = RSMast.Get_Fields_String("rsaddr1") + "";
			txtaddr2.Text = RSMast.Get_Fields_String("rsaddr2") + "";
			txtaddr3.Text = RSMast.Get_Fields_String("rsaddr3") + "";
			txtstate.Text = RSMast.Get_Fields_String("rsstate") + "";
			txtzip.Text = RSMast.Get_Fields_String("rszip") + " " + RSMast.Get_Fields_String("rszip4");
			txtlocnum.Text = RSMast.Get_Fields_String("rslocnumalph") + "";
			txtlocation.Text = RSMast.Get_Fields_String("rslocstreet");
		}

		private void SetupCol2()
		{
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			intLinesToPrint = intLinesToPrint;
			boolOKtoSave = true;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Call SetFixedSizeReport(Me, MDIParent.Grid)
			int intIndex;
			boolOKtoSave = false;
			// intIndex = Me.Toolbar.Tools.Count
			// Me.Toolbar.Tools.Add ""
			// Me.Toolbar.Tools(intIndex).Caption = ""
			// Me.Toolbar.Tools(intIndex).Type = 2
			// Me.Toolbar.Tools(intIndex).ID = "9990"
			// Me.Toolbar.Tools.Add "PRINT TO FILE"
			// Me.Toolbar.Tools(intIndex + 1).ID = "9950"
			// Me.Toolbar.Tools.Add ""
			// Me.Toolbar.Tools(intIndex + 2).Type = 2
			// Me.Toolbar.Tools(intIndex + 2).ID = "9990"
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			EraseQuery();
		}

		

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intLinesToPrint <= 5 || boolFirstTime || boolGroupFired)
			{
				Detail.CanShrink = true;
				// rptExtractReport.Detail.Height = 0
				Detail.Visible = false;
				// rptExtractReport.Detail.CanGrow = False
				boolFirstTime = false;
				boolGroupFired = false;
			}
			else
			{
				TWRE0000.rptExtractReport.InstancePtr.Detail.CanGrow = true;
				// rptExtractReport.Detail.Height = 240
				// rptExtractReport.Detail.CanGrow = False
				Detail.Visible = true;
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			intLineNumber = 1;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// intCurrReport = RSExtract.Fields("")
			// Call SetupNextReport(intCurrReport)
			if (intCurrGroup != intLastGroup)
			{
				SetupNextReport(ref intCurrGroup);
			}
			else
			{
				if (intLinesToPrint > 0)
				{
					rsReport.MoveFirst();
				}
			}
			intLastGroup = intCurrGroup;
			intLineNumber = 1;
			txtAccount.Text = FCConvert.ToString(RSMast.Get_Fields_Int32("rsaccount"));
			txtCard.Text = FCConvert.ToString(RSMast.Get_Fields_Int32("rscard"));
			txtName.Text = RSMast.Get_Fields_String("rsname") + "";
			txtmaplot.Text = RSMast.Get_Fields_String("rsmaplot") + "";
			txtaddr1.Text = RSMast.Get_Fields_String("rsaddr1") + "";
			txtaddr2.Text = RSMast.Get_Fields_String("rsaddr2") + "";
			txtaddr3.Text = RSMast.Get_Fields_String("rsaddr3") + "";
			txtstate.Text = RSMast.Get_Fields_String("rsstate") + "";
			txtzip.Text = RSMast.Get_Fields_String("rszip") + " " + RSMast.Get_Fields_String("rszip4");
			txtlocnum.Text = RSMast.Get_Fields_String("rslocnumalph") + "";
			txtlocation.Text = RSMast.Get_Fields_String("rslocstreet") + "";
			txtGroup.Text = intCurrGroup.ToString();
			modGlobalVariables.Statics.reporttotals[intCurrGroup, 0] += 1;
			while (intLineNumber < 6)
			{
				FillNextLine();
			}
			boolGroupFired = true;
		}
		// vbPorter upgrade warning: intReportNumber As short	OnWriteFCConvert.ToInt32(
		private void SetupNextReport(ref int intReportNumber)
		{
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                clsTemp.OpenRecordset(
                    "select * from extract where userid = " + FCConvert.ToString(lngUID) + " and groupnumber = " +
                    FCConvert.ToString(intReportNumber), modGlobalVariables.strREDatabase);
                intCurrReport = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("reportnumber"));
                clsTemp.OpenRecordset(
                    "select * from reporttable where reportnumber = " + FCConvert.ToString(intCurrReport),
                    modGlobalVariables.strREDatabase);
                clsTemp.MoveLast();
                clsTemp.MoveFirst();
                if (clsTemp.RecordCount() > Information.UBound(modGlobalVariables.Statics.reporttotals, 2))
                {
                    fecherFoundation.Extensions.ArrayExtensions.ResizeArray(ref modGlobalVariables.Statics.reporttotals,
                        intNumReports + 1, clsTemp.RecordCount() + 1);
                }

                rsReport.OpenRecordset(
                    "select * from reporttable where reportnumber = " + FCConvert.ToString(intCurrReport) +
                    " and torp <> 'N' order by linenumber", modGlobalVariables.strREDatabase);
                // Call clsTemp.OpenRecordset("select * from extract where reportnumber = " & intReportNumber, strredatabase)
                // intCurrGroup = clsTemp.Fields("groupnumber")
                if (!rsReport.EndOfFile())
                {
                    rsReport.MoveLast();
                    rsReport.MoveFirst();
                    intLinesToPrint = rsReport.RecordCount();
                    // If intLinesToPrint > intlargestnumlines Then
                    // ReDim Preserve reporttotals(intNumReports, intLinesToPrint)
                    // 
                    // End If
                }
                else
                {
                    intLinesToPrint = 0;
                }
            }
        }

		private void FillNextLine()
		{
			int intGroup = 0;
			clsReportParameter tCode;
			try
			{
				// On Error GoTo ErrorHandler
				if (intLineNumber > intLinesToPrint)
				{
					intLineNumber += 1;
					return;
				}
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				tCode = clCodeList.GetCodeByCode(FCConvert.ToInt32(Conversion.Val(rsReport.Get_Fields("code"))), FCConvert.ToInt32(Conversion.Val(rsReport.Get_Fields_Int32("codeid"))));
				if (!(tCode == null))
				{
					if (!tCode.SpecialCase)
					{
						HandleRegCodes(ref tCode);
					}
					else
					{
						HandleSpecialCodes(ref tCode);
					}
				}
				else
				{
					strDataAmount = "";
					strDataDesc = "";
				}
				intGroup = FCConvert.ToInt32(RSMast.Get_Fields_Int32("groupnumber"));
				switch (intLineNumber)
				{
					case 1:
						{
							txtAmount1.Text = strDataAmount;
							txtDesc1.Text = strDataDesc;
							// reporttotals(intCurrGroup, 0) = reporttotals(intCurrGroup, 0) + 1
							// reporttotals(intCurrGroup, 1) = reporttotals(intCurrGroup, 1) + Val(strDataAmount & "")
							break;
						}
					case 2:
						{
							txtAmount2.Text = strDataAmount;
							txtDesc2.Text = strDataDesc;
							// reporttotals(intCurrGroup, 2) = reporttotals(intCurrGroup, 2) + Val(strDataAmount & "")
							break;
						}
					case 3:
						{
							txtAmount3.Text = strDataAmount;
							txtDesc3.Text = strDataDesc;
							// reporttotals(intCurrGroup, 3) = reporttotals(intCurrGroup, 3) + Val(strDataAmount & "")
							break;
						}
					case 4:
						{
							txtAmount4.Text = strDataAmount;
							txtDesc4.Text = strDataDesc;
							// reporttotals(intCurrGroup, 4) = reporttotals(intCurrGroup, 4) + Val(strDataAmount & "")
							break;
						}
					case 5:
						{
							txtAmount5.Text = strDataAmount;
							txtDesc5.Text = strDataDesc;
							// reporttotals(intCurrGroup, 5) = reporttotals(intCurrGroup, 5) + Val(strDataAmount & "")
							break;
						}
					default:
						{
							txtAmount.Text = strDataAmount;
							txtDesc.Text = strDataDesc;
							// reporttotals(intCurrGroup, intLineNumber) = reporttotals(intCurrGroup, intLineNumber) + Val(strDataAmount & "")
							break;
						}
				}
				//end switch
				// If intCurrGroup <> intGroup Then
				// intGroup = intGroup
				// End If
				if (modGlobalVariables.Statics.reporttotals[intGroup, intLineNumber] != modGlobalConstants.EleventyBillion)
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsReport.Get_Fields("code")) == 1)
					{
						// count number of neighborhood codes
						// reporttotals(intCurrGroup, intLineNumber) = reporttotals(intCurrGroup, intLineNumber) + 1
						modGlobalVariables.Statics.reporttotals[intGroup, rsReport.Get_Fields_Int32("linenumber")] += 1;
					}
					else
					{
						// reporttotals(intCurrGroup, intLineNumber) = reporttotals(intCurrGroup, intLineNumber) + Val(strDataAmount & "")
						if (Information.IsNumeric(strDataAmount))
						{
							modGlobalVariables.Statics.reporttotals[intGroup, rsReport.Get_Fields_Int32("linenumber")] += Conversion.Val(strDataAmount + "");
						}
					}
				}
				intLineNumber += 1;
				rsReport.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 6)
				{
					modGlobalVariables.Statics.reporttotals[intGroup, rsReport.Get_Fields_Int32("linenumber")] = modGlobalConstants.EleventyBillion;
					intLineNumber += 1;
					rsReport.MoveNext();
					return;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Fill Next Line", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void HandleRegCodes(ref clsReportParameter tCode)
		{
            using (clsDRWrapper clsDesc = new clsDRWrapper())
            {
                if (!boolSaleType)
                {
                    RSPrint.OpenRecordset(
                        "Select * from " + tCode.TableName + " where rsaccount = " +
                        RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"),
                        modGlobalVariables.strREDatabase);
                }
                else
                {
                    RSPrint.OpenRecordset(
                        "Select * from sr" + tCode.TableName + " where rsaccount = " +
                        RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") +
                        " and salEdate = '" + RSMast.Get_Fields_DateTime("saledate") + "'",
                        modGlobalVariables.strREDatabase);
                }

                if (!RSPrint.EndOfFile())
                {
                    strDataAmount = RSPrint.Get_Fields(tCode.FieldName) + "";
                    // strdataamount = rsprint.fields(rstemp.fields("FieldName")).value
                    strDataDesc = tCode.Description;
                    switch (tCode.Code)
                    {
                        case 1:
                        {
                            if (Conversion.Val(strDataAmount) > 0 && Conversion.Val(strDataAmount) < 100)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(1200 + Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                                {
                                    clsDesc.OpenRecordset(
                                        "select * from neighborhood where code = " +
                                        FCConvert.ToString(Conversion.Val(strDataAmount)),
                                        modGlobalVariables.Statics.strRECostFileDatabase);
                                }
                                else
                                {
                                    clsDesc.OpenRecordset(
                                        "select * from neighborhood where code = " +
                                        FCConvert.ToString(Conversion.Val(strDataAmount)) + " and townnumber = " +
                                        FCConvert.ToString(Conversion.Val(RSMast.Get_Fields_Int32("ritrancode"))),
                                        modGlobalVariables.Statics.strRECostFileDatabase);
                                }

                                if (!clsDesc.EndOfFile())
                                {
                                    // strDataDesc = clsDesc.Fields("cldesc")
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("description"));
                                }
                                else
                                {
                                    // strDataDesc = "Neighborhood"
                                }
                            }

                            break;
                        }
                        case 3:
                        {
                            strDataDesc = strXCoord;
                            break;
                        }
                        case 4:
                        {
                            strDataDesc = strYCoord;
                            break;
                        }
                        case 5:
                        {
                            if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from zones where code = " +
                                    FCConvert.ToString(Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                            }
                            else
                            {
                                clsDesc.OpenRecordset(
                                    "select * from zones where code = " +
                                    FCConvert.ToString(Conversion.Val(strDataAmount)) + " and townnumber = " +
                                    FCConvert.ToString(Conversion.Val(RSMast.Get_Fields_Int32("ritrancode"))),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                            }

                            if (!clsDesc.EndOfFile())
                            {
                                strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("description"));
                            }

                            break;
                        }
                        case 9:
                        {
                            // street surface
                            // strDataDesc = rsTemp.Fields("Description") & ""
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(1321 + Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("cldesc"));
                                }
                            }

                            break;
                        }
                        case 12:
                        {
                            // sale type
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(1350 + Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("cldesc"));
                                }
                            }

                            break;
                        }
                        case 14:
                        {
                            // financing
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(1360 + Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("cldesc"));
                                }
                            }

                            break;
                        }
                        case 16:
                        {
                            // verified
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(1370 + Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("cldesc"));
                                }
                            }

                            break;
                        }
                        case 17:
                        {
                            // validity
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(1380 + Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("cldesc"));
                                }
                            }

                            break;
                        }
                        case 200:
                        {
                            // bldg style
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from dwellingstyle where code = " +
                                    FCConvert.ToString(Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("description"));
                                }
                            }

                            break;
                        }
                        case 205:
                        {
                            // stories
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(1480 + Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("cldesc"));
                                }
                            }

                            break;
                        }
                        case 206:
                        {
                            // exterior
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from dwellingexterior where code = " +
                                    FCConvert.ToString(Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("description"));
                                }
                            }

                            break;
                        }
                        case 210:
                        {
                            strDataDesc = strOpen3;
                            break;
                        }
                        case 211:
                        {
                            strDataDesc = strOpen4;
                            break;
                        }
                        case 221:
                        {
                            strDataDesc = strOpen5;
                            break;
                        }
                        case 222:
                        {
                            // heat
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from dwellingheat where code = " +
                                    FCConvert.ToString(Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("description"));
                                }
                            }

                            break;
                        }
                        case 226:
                        {
                            // kitchen
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(1610 + Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("cldesc"));
                                }
                            }

                            break;
                        }
                        case 227:
                        {
                            // baths
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(1620 + Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("cldesc"));
                                }
                            }

                            break;
                        }
                        case 241:
                        {
                            // condition
                            if (Conversion.Val(strDataAmount) > 0)
                            {
                                clsDesc.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(1460 + Conversion.Val(strDataAmount)),
                                    modGlobalVariables.Statics.strRECostFileDatabase);
                                if (!clsDesc.EndOfFile())
                                {
                                    strDataAmount = FCConvert.ToString(clsDesc.Get_Fields_String("cldesc"));
                                }
                            }

                            break;
                        }
                        default:
                        {
                            strDataDesc = tCode.Description;
                            break;
                        }
                    }

                    //end switch
                    strDataAmount = Strings.Trim(strDataAmount);
                }
                else
                {
                    strDataAmount = "";
                    strDataDesc = "";
                }
            }
        }

		private void HandleSpecialCodes(ref clsReportParameter tCode)
		{
			string strSQL = "";
			string strParm = "";
			int x;
			string strPrefix = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			int lngUID;
			// vbPorter upgrade warning: lngTemp As int	OnWrite(int, double)	OnRead(string)
			int lngTemp = 0;
			clsDRWrapper dbTemp = new clsDRWrapper();
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolSaleType)
			{
				strPrefix = "SR";
			}
			else
			{
				strPrefix = "";
			}
			switch (tCode.Code)
			{
				case 7:
					{
						strDataDesc = "Topography";
						strDataAmount = RSMast.Get_Fields_Int32("pitopography1") + RSMast.Get_Fields_Int32("pitopography2") + "";
						break;
					}
				case 8:
					{
						strDataDesc = "Utilities";
						strDataAmount = RSMast.Get_Fields_Int32("piutilities1") + RSMast.Get_Fields_Int32("piutilities2") + "";
						break;
					}
				case 15:
					{
						strDataDesc = "Sale Date";
						if (!fecherFoundation.FCUtils.IsEmptyDateTime(RSMast.Get_Fields_DateTime("saledate")))
						{
							if (RSMast.Get_Fields_DateTime("saledate").ToOADate() != 0)
							{
								strDataAmount = RSMast.Get_Fields_DateTime("SaleDate") + "";
							}
							else
							{
								strDataAmount = "";
							}
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 18:
					{
						strDataDesc = "Exempt Codes";
						strDataAmount = RSMast.Get_Fields_Int32("riexemptcd1") + " " + RSMast.Get_Fields_Int32("riexemptcd2") + " " + RSMast.Get_Fields_Int32("riexemptcd3");
						break;
					}
				case 24:
					{
						strDataDesc = "Land Schedule";
						int intZone = 0;
						int intSecZone = 0;
						int intNeighborhood = 0;
						// vbPorter upgrade warning: intSchedule As short --> As int	OnWriteFCConvert.ToDouble(	OnRead(string)
						int intSchedule = 0;
						intZone = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMast.Get_Fields_Int32("pizone"))));
						intSecZone = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMast.Get_Fields_Int32("piseczone"))));
						intNeighborhood = FCConvert.ToInt32(Math.Round(Conversion.Val(RSMast.Get_Fields_Int32("pineighborhood"))));
						if (FCConvert.ToBoolean(RSMast.Get_Fields_Boolean("zoneoverride")))
						{
							intZone = intSecZone;
						}
						// If intSecZone > 50 Then
						// intZone = intSecZone - 40
						// Else
						// If intZone > 0 Then
						// intZone = intZone - 10
						// Else
						// intZone = 1
						// End If
						// End If
						intSchedule = FCConvert.ToInt32(modGlobalVariables.Statics.LandTRec.Schedule(ref intNeighborhood, ref intZone));
						// If intZone > 0 Then
						// If (intZone * 100 + intNeighborhood > 1100) And (intZone * 100 + intNeighborhood < 5100) Then
						// Call clsTemp.OpenRecordset("select * from landtrecord where lrecordnumber = " & intZone * 100 + intNeighborhood, strREDatabase)
						// intSchedule = Val(clsTemp.Fields("ltcode"))
						// Call clsTemp.OpenRecordset("select * from landSCHEDULE where schedule = " & intSchedule, strREDatabase)
						// If clsTemp.EndOfFile Then intSchedule = 0
						// End If
						// End If
						strDataAmount = FCConvert.ToString(intSchedule);
						break;
					}
				case 27:
					{
						strDataDesc = "Total - Calculated";
						strDataAmount = FCConvert.ToString(Conversion.Val(RSMast.Get_Fields_Int32("hlcompvalland") + "") + Conversion.Val(RSMast.Get_Fields_Int32("hlcompvalbldg") + "")) + "";
						break;
					}
				case 30:
					{
						strDataDesc = "Total - Billing";
						strDataAmount = FCConvert.ToString(Conversion.Val(RSMast.Get_Fields_Int32("lastlandval") + "") + Conversion.Val(RSMast.Get_Fields_Int32("lastbldgval") + ""));
						break;
					}
				case 34:
					{
						strDataDesc = "Map/Lot";
						strDataAmount = Strings.Trim(RSMast.Get_Fields_String("rsmaplot") + "");
						break;
					}
				case 40:
					{
						strDataDesc = "Location";
						strDataAmount = Strings.Trim(RSMast.Get_Fields_String("rslocnumalph") + " " + RSMast.Get_Fields_String("rslocstreet"));
						break;
					}
				case 44:
					{
						strDataDesc = "Net - Calculated";
						strDataAmount = FCConvert.ToString(Conversion.Val(RSMast.Get_Fields_Int32("hlcompvalland" + "")) + Conversion.Val(RSMast.Get_Fields_Int32("hlcompvalbldg")) - Conversion.Val(RSMast.Get_Fields_Int32("rlexemption")));
						break;
					}
				case 45:
					{
						strDataDesc = "Net - Billing";
						strDataAmount = FCConvert.ToString(Conversion.Val(RSMast.Get_Fields_Int32("lastlandval") + "") + Conversion.Val(RSMast.Get_Fields_Int32("lastbldgval") + "") - Conversion.Val(RSMast.Get_Fields_Int32("rlexemption")));
						break;
					}
				case 46:
					{
						strDataDesc = "Net - Current";
						strDataAmount = FCConvert.ToString(Conversion.Val(RSMast.Get_Fields_Int32("rllandval") + "") + Conversion.Val(RSMast.Get_Fields_Int32("rlbldgval") + "") - Conversion.Val(RSMast.Get_Fields_Int32("rlexemption")));
						break;
					}
				case 50:
					{
						strDataDesc = "Date Inspected";
						if (FCConvert.ToDateTime(RSMast.Get_Fields_DateTime("dateinspected") as object).ToOADate() == 0)
						{
							strDataAmount = "";
						}
						else
						{
							strDataAmount = Strings.Format(RSMast.Get_Fields_DateTime("Dateinspected") + "", "MM/dd/yyyy");
						}
						break;
					}
				case 52:
					{
						strDataDesc = "";
						strDataAmount = FCConvert.ToString(Conversion.Val(RSMast.Get_Fields_String("rslocnumalph") + ""));
						break;
					}
				case 61:
					{
						strDataDesc = "Book/Page";
						strDataAmount = modGlobalFunctions.GetCurrentBookPageString(RSMast.Get_Fields_Int32("rsaccount"));
						break;
					}
				case 62:
					{
						strDataDesc = "Ref 1";
						strDataAmount = Strings.Trim(Strings.Mid(RSMast.Get_Fields_String("rsref1") + Strings.StrDup(22, " "), 1, 22));
						break;
					}
				case 63:
					{
						strDataDesc = "Ref 1 (24-35)";
						strDataAmount = Strings.Trim(Strings.Mid(RSMast.Get_Fields_String("rsref1") + Strings.StrDup(36, " "), 24, 12));
						break;
					}
				case 64:
					{
						strDataDesc = "Assessment Change";
						// If Not boolShortRealEstate Then
						strDataAmount = FCConvert.ToString((Conversion.Val(RSMast.Get_Fields_Int32("rllandval")) + Conversion.Val(RSMast.Get_Fields_Int32("rlbldgval"))) - (Conversion.Val(RSMast.Get_Fields_Int32("lastlandval")) + Conversion.Val(RSMast.Get_Fields_Int32("lastbldgval"))));
						// Else
						// lngTemp = CustomizedInfo.BillYear
						// If lngTemp = 0 Then
						// lngTemp = Year(Date)
						// End If
						// Call clsTemp.OpenRecordset("select assessment from previousassessment where taxyear = " & lngTemp - 1 & " and account = " & RSMast.Fields("rsaccount"), strREDatabase)
						// If Not clsTemp.EndOfFile Then
						// lngTemp = Val(clsTemp.Fields("assessment"))
						// Else
						// lngTemp = 0
						// End If
						// strDataAmount = lngTemp
						// End If
						break;
					}
				case 65:
					{
						strDataDesc = "Tax Amount";
						strDataAmount = Strings.Format((Conversion.Val(RSMast.Get_Fields_Int32("lastlandval")) + Conversion.Val(RSMast.Get_Fields_Int32("lastbldgval")) - Conversion.Val(RSMast.Get_Fields_Int32("rlexemption"))) * (Conversion.Val(modGlobalVariables.Statics.CustomizedInfo.BillRate) / 1000), "0.00");
						break;
					}
				case 66:
					{
						strDataDesc = "Billing Change";
						lngTemp = modGlobalVariables.Statics.CustomizedInfo.BillYear;
						if (lngTemp == 0)
						{
							lngTemp = DateTime.Today.Year;
						}
						clsTemp.OpenRecordset("select assessment from previousassessment where taxyear = " + FCConvert.ToString(lngTemp - 1) + " and account = " + RSMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						if (!clsTemp.EndOfFile())
						{
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("assessment"))));
							lngTemp = FCConvert.ToInt32(Conversion.Val(RSMast.Get_Fields_Int32("lastlandval")) + Conversion.Val(RSMast.Get_Fields_Int32("lastbldgval")) - Conversion.Val(RSMast.Get_Fields_Int32("rlexemption")) - lngTemp);
						}
						else
						{
							lngTemp = 0;
						}
						strDataAmount = FCConvert.ToString(lngTemp);
						break;
					}
				case 74:
					{
						// date created
						strDataDesc = "Date Created";
						if (!fecherFoundation.FCUtils.IsNull(RSMast.Get_Fields_DateTime("datecreated") + ""))
						{
							if (Information.IsDate(RSMast.Get_Fields("datecreated")))
							{
								if (RSMast.Get_Fields_DateTime("datecreated").ToOADate() != 0)
								{
									strDataAmount = FCConvert.ToString(RSMast.Get_Fields_DateTime("datecreated"));
								}
								else
								{
									strDataAmount = "";
								}
							}
							else
							{
								strDataAmount = "";
							}
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 98:
					{
						strDataDesc = "Influence Factor";
						strDataAmount = "";
						for (x = 1; x <= 6; x++)
						{
							// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
							if (RSMast.Get_Fields("piland" + FCConvert.ToString(x) + "type") > 0)
							{
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								strDataAmount += RSMast.Get_Fields("piland" + FCConvert.ToString(x) + "inf") + " ";
							}
						}
						// x
						if (RSMast.Get_Fields_Int32("piland7type") > 0)
						{
							strDataAmount += RSMast.Get_Fields_Double("piland7inf");
						}
						break;
					}
				case 99:
					{
						strDataDesc = "Land Influence Code";
						strDataAmount = "";
						for (x = 1; x <= 6; x++)
						{
							// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
							if (RSMast.Get_Fields("piland" + FCConvert.ToString(x) + "type") > 0)
							{
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								strDataAmount += RSMast.Get_Fields("piland" + FCConvert.ToString(x) + "infcode") + " ";
							}
						}
						// x
						if (RSMast.Get_Fields_Int32("piland7type") > 0)
						{
							strDataAmount += RSMast.Get_Fields_Int32("piland7infcode");
						}
						break;
					}
				case 100:
					{
						strDataDesc = "Land Type";
						strDataAmount = "";
						for (x = 1; x <= 6; x++)
						{
							// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
							if (RSMast.Get_Fields("piland" + FCConvert.ToString(x) + "type") > 0)
							{
								// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
								strDataAmount += RSMast.Get_Fields("piland" + FCConvert.ToString(x) + "type") + " ";
							}
						}
						// x
						if (RSMast.Get_Fields_Int32("piland7type") > 0)
						{
							strDataAmount += RSMast.Get_Fields_Int32("piland7type") + "";
						}
						break;
					}
				case 101:
					{
						strDataDesc = "Frontage";
						if (!boolSaleType)
						{
							strSQL = "(select rsaccount,rscard,piland1type as Ltype,val(piland1unitsa ) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland2type as Ltype,val(piland2unitsa ) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland3type as Ltype,val(piland3unitsa ) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland4type as Ltype,val(piland4unitsa) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
							strSQL += ") union all (select rsaccount,rscard,piland5type as Ltype,val(piland5unitsa) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland6type as Ltype,val(piland6unitsa ) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland7type as Ltype,val(piland7unitsa) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ");";
							// Set qdf = dbTemp.CreateQueryDef("GetInfo", strSQL)
							strSQL = "SELECT getinfo.rsaccount,getinfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (GetInfo.rsaccount = extracttable.accountnumber) and (GetInfo.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and GetInfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and GetInfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and GetInfo.ltype between 11 and 15 group by GetInfo.rsaccount,GetInfo.rscard";
						}
						else
						{
							strSQL = "(select rsaccount,rscard,saleid,piland1type as Ltype,val(piland1unitsa ) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland2type as Ltype,val(piland2unitsa ) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland3type as Ltype,val(piland3unitsa ) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland4type as Ltype,val(piland4unitsa) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
							strSQL += " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland5type as Ltype,val(piland5unitsa) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland6type as Ltype,val(piland6unitsa ) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland7type as Ltype,val(piland7unitsa) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ");";
							qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
							strSQL = "SELECT getinfo.rsaccount,getinfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (GetInfo.rsaccount = extracttable.accountnumber) and (GetInfo.rscard = extracttable.cardnumber) and (getinfo.saleid = extracttable.saleid) ) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and GetInfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and GetInfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getinfo.saleid = " + RSMast.Get_Fields_Int32("saleid") + " and GetInfo.ltype between 11 and 15 group by GetInfo.rsaccount,GetInfo.rscard,getinfo.saleid";
						}
						rsMisc.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						// dbTemp.QueryDefs.Delete ("GetInfo")
						break;
					}
				case 102:
					{
						strDataDesc = "Frontage";
						if (!boolSaleType)
						{
							strSQL = "(select rsaccount,rscard,piland1type as Ltype,val(piland1unitsb ) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland2type as Ltype,val(piland2unitsb ) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland3type as Ltype,val(piland3unitsb ) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland4type as Ltype,val(piland4unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
							strSQL += ") union all (select rsaccount,rscard,piland5type as Ltype,val(piland5unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland6type as Ltype,val(piland6unitsb ) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland7type as Ltype,val(piland7unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ");";
							qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
							strSQL = "SELECT GetInfo.rsaccount,GetInfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (getinfo.rsaccount = extracttable.accountnumber) and (getinfo.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getinfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getinfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getinfo.ltype between 11 and 15 group by getinfo.rsaccount,getinfo.rscard";
						}
						else
						{
							strSQL = "(select rsaccount,rscard,saleid,piland1type as Ltype,val(piland1unitsb ) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland2type as Ltype,val(piland2unitsb ) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland3type as Ltype,val(piland3unitsb ) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland4type as Ltype,val(piland4unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
							strSQL += " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland5type as Ltype,val(piland5unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland6type as Ltype,val(piland6unitsb ) as Sum1 from SRmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland7type as Ltype,val(piland7unitsb) as Sum1 from SRmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ");";
							qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
							strSQL = "SELECT GetInfo.rsaccount,GetInfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (getinfo.rsaccount = extracttable.accountnumber) and (getinfo.rscard = extracttable.cardnumber) and (getinfo.saleid = extracttable.saleid) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getinfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getinfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + " and getinfo.ltype between 11 and 15 group by getinfo.rsaccount,getinfo.rscard,getinfo.saleid";
						}
						rsMisc.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						// dbTemp.QueryDefs.Delete ("GetInfo")
						break;
					}
				case 103:
					{
						if (!boolSaleType)
						{
							strSQL = "(select rsaccount,rscard,piland1type as Ltype,val(piland1unitsa & piland1unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland2type as Ltype,val(piland2unitsa & piland2unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland3type as Ltype,val(piland3unitsa & piland3unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland4type as Ltype,val(piland4unitsa & piland4unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
							strSQL += ") union all (select rsaccount,rscard,piland5type as Ltype,val(piland5unitsa & piland5unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland6type as Ltype,val(piland6unitsa & piland6unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland7type as Ltype,val(piland7unitsa & piland7unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ");";
							qdf = dbTemp.CreateQueryDef("GetSQFT", strSQL);
							strDataDesc = "Square Feet (Codes 16-20)";
							strSQL = "SELECT getsqft.rsaccount,getsqft.rscard, sum(Sum1) as TheSum from (GetSQFT right join extracttable on (getsqft.rsaccount = extracttable.accountnumber) and (getsqft.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getsqft.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getsqft.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getsqft.ltype between 16 and 20 group by getsqft.rsaccount,getsqft.rscard";
						}
						else
						{
							strSQL = "(select rsaccount,rscard,saleid,salepiland1type as Ltype,val(piland1unitsa & piland1unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland2type as Ltype,val(piland2unitsa & piland2unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland3type as Ltype,val(piland3unitsa & piland3unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland4type as Ltype,val(piland4unitsa & piland4unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = ";
							strSQL += RSMast.Get_Fields_Int32("rscard");
							strSQL += " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland5type as Ltype,val(piland5unitsa & piland5unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland6type as Ltype,val(piland6unitsa & piland6unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland7type as Ltype,val(piland7unitsa & piland7unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ");";
							qdf = dbTemp.CreateQueryDef("GetSQFT", strSQL);
							strDataDesc = "Square Feet (Codes 16-20)";
							strSQL = "SELECT getsqft.rsaccount,getsqft.rscard, sum(Sum1) as TheSum from (GetSQFT right join extracttable on (getsqft.rsaccount = extracttable.accountnumber) and (getsqft.rscard = extracttable.cardnumber) and (getsqft.saleid = extracttable.saleid) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getsqft.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getsqft.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getsqft.saleid = " + RSMast.Get_Fields_Int32("saleid") + " and getsqft.ltype between 16 and 20 group by getsqft.rsaccount,getsqft.rscard,getsqft.saleid";
						}
						rsMisc.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						// dbTemp.QueryDefs.Delete ("GetSQFT")
						break;
					}
				case 104:
					{
						strDataDesc = "Acreage";
						if (boolSaleType)
						{
							strSQL = "(select rsaccount,rscard,saleid,piland1type as Ltype,val(piland1unitsa & piland1unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland2type as Ltype,val(piland2unitsa & piland2unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland3type as Ltype,val(piland3unitsa & piland3unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland4type as Ltype,val(piland4unitsa & piland4unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
							strSQL += " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland5type as Ltype,val(piland5unitsa & piland5unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland6type as Ltype,val(piland6unitsa & piland6unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland7type as Ltype,val(piland7unitsa & piland7unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ");";
							qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
							strSQL = "SELECT getinfo.rsaccount,getinfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (getinfo.rsaccount = extracttable.accountnumber) and (getinfo.rscard = extracttable.cardnumber) and (getinfo.saleid = extracttable.saleid)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getinfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getinfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getinfo.saleid = " + RSMast.Get_Fields_Int32("saleid") + " and getinfo.ltype between 21 and 41 group by getinfo.rsaccount,getinfo.rscard";
						}
						else
						{
							strSQL = "(select rsaccount,rscard,piland1type as Ltype,val(piland1unitsa & piland1unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland2type as Ltype,val(piland2unitsa & piland2unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland3type as Ltype,val(piland3unitsa & piland3unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland4type as Ltype,val(piland4unitsa & piland4unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
							strSQL += ") union all (select rsaccount,rscard,piland5type as Ltype,val(piland5unitsa & piland5unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland6type as Ltype,val(piland6unitsa & piland6unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland7type as Ltype,val(piland7unitsa & piland7unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ");";
							qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
							strSQL = "SELECT getinfo.rsaccount,getinfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (getinfo.rsaccount = extracttable.accountnumber) and (getinfo.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getinfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getinfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getinfo.ltype between 21 and 41 group by getinfo.rsaccount,getinfo.rscard";
						}
						rsMisc.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + "") / 100);
						}
						else
						{
							strDataAmount = "";
						}
						// dbTemp.QueryDefs.Delete ("GetInfo")
						break;
					}
				case 105:
					{
						strDataDesc = "Sites ";
						if (!boolSaleType)
						{
							strSQL = "(select rsaccount,rscard,piland1type as Ltype,val(piland1unitsa & piland1unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland2type as Ltype,val(piland2unitsa & piland2unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland3type as Ltype,val(piland3unitsa & piland3unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland4type as Ltype,val(piland4unitsa & piland4unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
							strSQL += ") union all (select rsaccount,rscard,piland5type as Ltype,val(piland5unitsa & piland5unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland6type as Ltype,val(piland6unitsa & piland6unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union all (select rsaccount,rscard,piland7type as Ltype,val(piland7unitsa & piland7unitsb) as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ");";
							qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
							strSQL = "SELECT getinfo.rsaccount,getinfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (getinfo.rsaccount = extracttable.accountnumber) and (getinfo.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getinfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getinfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getinfo.ltype between 42 and 46 group by getinfo.rsaccount,getinfo.rscard";
						}
						else
						{
							strSQL = "(select rsaccount,rscard,saleid,piland1type as Ltype,val(piland1unitsa & piland1unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland2type as Ltype,val(piland2unitsa & piland2unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland3type as Ltype,val(piland3unitsa & piland3unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland4type as Ltype,val(piland4unitsa & piland4unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
							strSQL += " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland5type as Ltype,val(piland5unitsa & piland5unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland6type as Ltype,val(piland6unitsa & piland6unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union all (select rsaccount,rscard,saleid,piland7type as Ltype,val(piland7unitsa & piland7unitsb) as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ");";
							qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
							strSQL = "SELECT getinfo.rsaccount,getinfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (getinfo.rsaccount = extracttable.accountnumber) and (getinfo.rscard = extracttable.cardnumber) and (getinfo.saleid = extracttable.saleid)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getinfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getinfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getinfo.saleid = " + RSMast.Get_Fields_Int32("saleid") + " and getinfo.ltype between 42 and 46 group by getinfo.rsaccount,getinfo.rscard,getinfo.saleid";
						}
						rsMisc.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + "") / 100);
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 500:
					{
						strDataDesc = tCode.Description;
						if (modGlobalVariables.Statics.LandTypes.FindCode(tCode.ID))
						{
							switch (modGlobalVariables.Statics.LandTypes.UnitsType)
							{
								case modREConstants.CNSTLANDTYPEFRONTFOOT:
								case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
									{
										if (!boolSaleType)
										{
											strSQL = "(select rsaccount,rscard,piland1type as Ltype,val(piland1unitsa & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland2type as Ltype,val(piland2unitsa & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland3type as Ltype,val(piland3unitsa & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland4type as Ltype,val(piland4unitsa & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
											strSQL += ") union (select rsaccount,rscard,piland5type as Ltype,val(piland5unitsa & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland6type as Ltype,val(piland6unitsa & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") UNION (select rsaccount,rscard,piland7type as Ltype,val(piland7unitsa & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ");";
											qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
											strSQL = "SELECT getinfo.rsaccount,getinfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (getinfo.rsaccount = extracttable.accountnumber) and (getinfo.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getinfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getinfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getinfo.ltype = " + tCode.ID + " group by getinfo.rsaccount,getinfo.rscard";
										}
										else
										{
											strSQL = "(select rsaccount,rscard,saleid,piland1type as Ltype,val(piland1unitsa & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland2type as Ltype,val(piland2unitsa & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland3type as Ltype,val(piland3unitsa & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland4type as Ltype,val(piland4unitsa & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
											strSQL += " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland5type as Ltype,val(piland5unitsa & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland6type as Ltype,val(piland6unitsa & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") UNION (select rsaccount,rscard,saleid,piland7type as Ltype,val(piland7unitsa & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ");";
											qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
											strSQL = "SELECT getinfo.rsaccount,getinfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (getinfo.rsaccount = extracttable.accountnumber) and (getinfo.rscard = extracttable.cardnumber) and (getinfo.saleid = extracttable.saleid)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getinfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getinfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getinfo.saleid = " + RSMast.Get_Fields_Int32("saleid") + " and getinfo.ltype = " + tCode.ID + " group by getinfo.rsaccount,getinfo.rscard,getinfo.saleid";
										}
										rsMisc.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
										if (!rsMisc.EndOfFile())
										{
											// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
											strDataAmount = rsMisc.Get_Fields("thesum") + "";
										}
										else
										{
											strDataAmount = "";
										}
										// dbTemp.QueryDefs.Delete ("GetInfo")
										break;
									}
								case modREConstants.CNSTLANDTYPESQUAREFEET:
									{
										if (!boolSaleType)
										{
											strSQL = "(select rsaccount,rscard,piland1type as Ltype,val(piland1unitsa & piland1unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland2type as Ltype,val(piland2unitsa & piland2unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland3type as Ltype,val(piland3unitsa & piland3unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland4type as Ltype,val(piland4unitsa & piland4unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
											strSQL += ") union (select rsaccount,rscard,piland5type as Ltype,val(piland5unitsa & piland5unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland6type as Ltype,val(piland6unitsa & piland6unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") UNION (select rsaccount,rscard,piland7type as Ltype,val(piland7unitsa & piland7unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ");";
											qdf = dbTemp.CreateQueryDef("GetSQFT", strSQL);
											strSQL = "SELECT getsqft.rsaccount,getsqft.rscard, sum(Sum1) as TheSum from (GetSQFT right join extracttable on (getsqft.rsaccount = extracttable.accountnumber) and (getsqft.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getsqft.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getsqft.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getsqft.ltype = " + tCode.ID + " group by getsqft.rsaccount,getsqft.rscard";
										}
										else
										{
											strSQL = "(select rsaccount,rscard,saleid,piland1type as Ltype,val(piland1unitsa & piland1unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland2type as Ltype,val(piland2unitsa & piland2unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = ";
											strSQL += RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland3type as Ltype,val(piland3unitsa & piland3unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland4type as Ltype,val(piland4unitsa & piland4unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
											strSQL += " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland5type as Ltype,val(piland5unitsa & piland5unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland6type as Ltype,val(piland6unitsa & piland6unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") UNION (select rsaccount,rscard,saleid,piland7type as Ltype,val(piland7unitsa & piland7unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ");";
											qdf = dbTemp.CreateQueryDef("GetSQFT", strSQL);
											strSQL = "SELECT getsqft.rsaccount,getsqft.rscard, sum(Sum1) as TheSum from (GetSQFT right join extracttable on (getsqft.rsaccount = extracttable.accountnumber) and (getsqft.rscard = extracttable.cardnumber) and (getsqft.saleid = extracttable.saleid)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getsqft.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getsqft.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getsqft.saleid = " + RSMast.Get_Fields_Int32("saleid") + " and getsqft.ltype = " + tCode.ID + " group by getsqft.rsaccount,getsqft.rscard,getsqft.saleid";
										}
										rsMisc.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
										if (!rsMisc.EndOfFile())
										{
											// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
											strDataAmount = FCConvert.ToString(rsMisc.Get_Fields("thesum"));
										}
										else
										{
											strDataAmount = "";
										}
										// dbTemp.QueryDefs.Delete ("GetSQFT")
										break;
									}
								default:
									{
										if (!boolSaleType)
										{
											strSQL = "(select rsaccount,rscard,piland1type as Ltype,val(piland1unitsa & piland1unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland2type as Ltype,val(piland2unitsa & piland2unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland3type as Ltype,val(piland3unitsa & piland3unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland4type as Ltype,val(piland4unitsa & piland4unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
											strSQL += ") union (select rsaccount,rscard,piland5type as Ltype,val(piland5unitsa & piland5unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") union (select rsaccount,rscard,piland6type as Ltype,val(piland6unitsa & piland6unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ") UNION (select rsaccount,rscard,piland7type as Ltype,val(piland7unitsa & piland7unitsb & '') as Sum1 from master where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + ");";
											qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
											strSQL = "SELECT getinfo.rsaccount,getinfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (getinfo.rsaccount = extracttable.accountnumber) and (getinfo.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getinfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getinfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getinfo.ltype =" + tCode.ID + " group by getinfo.rsaccount,getinfo.rscard";
										}
										else
										{
											strSQL = "(select rsaccount,rscard,saleid,piland1type as Ltype,val(piland1unitsa & piland1unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland2type as Ltype,val(piland2unitsa & piland2unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = ";
											strSQL += RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland3type as Ltype,val(piland3unitsa & piland3unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland4type as Ltype,val(piland4unitsa & piland4unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard");
											strSQL += " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland5type as Ltype,val(piland5unitsa & piland5unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") union (select rsaccount,rscard,saleid,piland6type as Ltype,val(piland6unitsa & piland6unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ") UNION (select rsaccount,rscard,saleid,piland7type as Ltype,val(piland7unitsa & piland7unitsb & '') as Sum1 from srmaster where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saleid = " + RSMast.Get_Fields_Int32("saleid") + ");";
											qdf = dbTemp.CreateQueryDef("GetInfo", strSQL);
											strSQL = "SELECT getinfo.rsaccount,getinfo.rscard, sum(Sum1) as TheSum from (GetInfo right join extracttable on (getinfo.rsaccount = extracttable.accountnumber) and (getinfo.rscard = extracttable.cardnumber) and (getinfo.saleid = extracttable.saleid)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and getinfo.rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and getinfo.rscard = " + RSMast.Get_Fields_Int32("rscard") + " and getinfo.saleid = " + RSMast.Get_Fields_Int32("saleid") + " and getinfo.ltype =" + tCode.ID + " group by getinfo.rsaccount,getinfo.rscard,getinfo.saleid";
										}
										rsMisc.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
										if (!rsMisc.EndOfFile())
										{
											// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
											strDataAmount = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields("thesum") + "") / 100);
										}
										else
										{
											strDataAmount = "";
										}
										// dbTemp.QueryDefs.Delete ("GetInfo")
										break;
									}
							}
							//end switch
						}
						// 
						break;
					}
				case 201:
					{
						strDataDesc = "Dwelling Units (Account)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select sum(diunitsdwelling) as TheSum from dwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select sum(diunitsdwelling) as TheSum from srdwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 203:
					{
						strDataDesc = "Other Units (Account)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select sum(diunitsother) as TheSum from dwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select sum(diunitsother) as TheSum from srdwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 208:
					{
						strDataDesc = "S/F Masonry Trim (Account)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select sum(disfmasonry) as TheSum from dwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select sum(disfmasonry) as TheSum from srdwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 218:
					{
						strDataDesc = "S/F Bsmt Living (Account)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select sum(disfbsmtliving) as TheSum from dwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select sum(disfbsmtliving) as TheSum from srdwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 220:
					{
						strDataDesc = "Fin Bsmt Grade & Factor";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from dwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srdwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("dibsmtfingrade1") + rsMisc.Get_Fields_Int32("dibsmtfingrade2") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 238:
					{
						strDataDesc = "Grade & Factor";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from dwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srdwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("digrade1") + rsMisc.Get_Fields_Int32("digrade2") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 239:
					{
						strDataDesc = "S/F By Account";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select sum(disqft) as TheSum from dwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select sum(disqft) as TheSum from srdwelling where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 301:
					{
						strDataDesc = "Occupancy";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("OCC1") + "-" + rsMisc.Get_Fields_Int32("OCC2");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 302:
					{
						strDataDesc = "Dwelling Units (Account)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select sum(dwel1 + dwel2) as TheSum from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select sum(dwel1 + dwel2) as TheSum from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 303:
					{
						strDataDesc = "Dwelling Units (Card)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select sum(dwel1 + dwel2) as TheSum from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select sum(dwel1 + dwel2) as TheSum from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 304:
					{
						strDataDesc = "Building Class";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1class") + "-" + rsMisc.Get_Fields_Int32("c2class");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 305:
					{
						strDataDesc = "Building Quality";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1quality") + "-" + rsMisc.Get_Fields_Int32("c1quality");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 306:
					{
						strDataDesc = "Grade Factor";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", "", FCConvert.ToInt32(modGlobalVariables.strREDatabase));
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1grade") + "-" + rsMisc.Get_Fields_Int32("c2grade");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 307:
					{
						strDataDesc = "Exterior Walls";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1extwalls") + "-" + rsMisc.Get_Fields_Int32("c2extwalls");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 308:
					{
						strDataDesc = "Stories";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1stories") + "-" + rsMisc.Get_Fields_Int32("c2stories");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 309:
					{
						strDataDesc = "Height";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1height") + "-" + rsMisc.Get_Fields_Int32("c1height");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 310:
					{
						strDataDesc = "Ground Area (Account)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select sum(c1floor + c2floor) as TheSum from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select sum(c1floor + c2floor) as TheSum from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 311:
					{
						strDataDesc = "Ground Area (Card)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1floor") + "-" + rsMisc.Get_Fields_Int32("c2floor");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 312:
					{
						strDataDesc = "Perimeter/Units";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1perimeter") + "-" + rsMisc.Get_Fields_Int32("c2perimeter");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 313:
					{
						strDataDesc = "Heating/Cooling";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1heat") + "-" + rsMisc.Get_Fields_Int32("c2heat");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 314:
					{
						strDataDesc = "Year Built";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1built") + "-" + rsMisc.Get_Fields_Int32("c2built");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 315:
					{
						strDataDesc = "Year Remodeled";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1remodel") + "-" + rsMisc.Get_Fields_Int32("c2remodel");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 316:
					{
						strDataDesc = "Condition";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1condition") + "-" + rsMisc.Get_Fields_Int32("c2condition");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 317:
					{
						strDataDesc = "Physical % Entered";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1phys") + "-" + rsMisc.Get_Fields_Int32("c2phys");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 318:
					{
						strDataDesc = "Functional % Entered";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("c1funct") + "-" + rsMisc.Get_Fields_Int32("c2funct");
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 319:
					{
						strDataDesc = "Economic % Entered";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = rsMisc.Get_Fields_Int32("cmecon") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 320:
					{
						// sqft by account
						strDataDesc = "Square Footage (Account)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select sum(val(c1floor & '') * val(c1stories & '') + (val(c2floor & '') * val(c2stories & ''))) as TheSum from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select sum(val(c1floor & '') * val(c1stories & '') + (val(c2floor & '') * val(c2stories & ''))) as TheSum from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 321:
					{
						// sqft by card
						strDataDesc = "Square Footage (Card)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							strDataAmount = FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields_Int32("c1floor") + "") * Conversion.Val(rsMisc.Get_Fields_Int32("c1stories") + "")) + "-" + FCConvert.ToString(Conversion.Val(rsMisc.Get_Fields_Int32("c2floor") + "") * Conversion.Val(rsMisc.Get_Fields_Int32("c2stories") + ""));
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 322:
					{
						// total sqft by card
						strDataDesc = "Total Sqft (Account)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select sum(val(SQFT & '')) as TheSum from commercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select sum(val(SQFT & '')) as TheSum from srcommercial where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						if (!rsMisc.EndOfFile())
						{
							// TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
							strDataAmount = rsMisc.Get_Fields("thesum") + "";
						}
						else
						{
							strDataAmount = "";
						}
						break;
					}
				case 401:
					{
						strDataDesc = "Outbuildings";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from outbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from sroutbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						strDataAmount = "";
						if (!rsMisc.EndOfFile())
						{
							for (x = 1; x <= 10; x++)
							{
								// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
								if (rsMisc.Get_Fields("oitype" + FCConvert.ToString(x)) > 0)
								{
									if (x > 1)
									{
										strDataAmount += " ";
									}
									// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
									strDataAmount += rsMisc.Get_Fields("oitype" + FCConvert.ToString(x));
								}
							}
							// x
						}
						break;
					}
				case 402:
					{
						strDataDesc = "Year Built (OutBuilding)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from outbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from sroutbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						strDataAmount = "";
						if (!rsMisc.EndOfFile())
						{
							for (x = 1; x <= 10; x++)
							{
								// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
								if (rsMisc.Get_Fields("oiyear" + FCConvert.ToString(x)) > 0)
								{
									if (x > 1)
									{
										strDataAmount += " ";
									}
									// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
									strDataAmount += rsMisc.Get_Fields("oiyear" + FCConvert.ToString(x));
								}
							}
							// x
						}
						break;
					}
				case 403:
					{
						strDataDesc = "Square Footage (Outbuilding)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from outbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from sroutbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						strDataAmount = "";
						if (!rsMisc.EndOfFile())
						{
							for (x = 1; x <= 10; x++)
							{
								// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
								if (rsMisc.Get_Fields("oiunits" + FCConvert.ToString(x)) > 0)
								{
									if (x > 1)
									{
										strDataAmount += " ";
									}
									// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
									strDataAmount += rsMisc.Get_Fields("oiunits" + FCConvert.ToString(x));
								}
							}
							// x
						}
						break;
					}
				case 404:
					{
						strDataDesc = "Grade + Factor (Outbuilding)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from outbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from sroutbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						strDataAmount = "";
						if (!rsMisc.EndOfFile())
						{
							for (x = 1; x <= 10; x++)
							{
								// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
								if (rsMisc.Get_Fields("oitype" + FCConvert.ToString(x)) > 0)
								{
									if (x > 1)
									{
										strDataAmount += " ";
									}
									// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
									strDataAmount += rsMisc.Get_Fields("oigradecd" + FCConvert.ToString(x)) + rsMisc.Get_Fields("oigradepct" + FCConvert.ToString(x));
								}
							}
							// x
						}
						break;
					}
				case 405:
					{
						strDataDesc = "Condition (Outbuilding)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from outbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from sroutbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						strDataAmount = "";
						if (!rsMisc.EndOfFile())
						{
							for (x = 1; x <= 10; x++)
							{
								// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
								if (rsMisc.Get_Fields("oitype" + FCConvert.ToString(x)) > 0)
								{
									if (x > 1)
									{
										strDataAmount += " ";
									}
									// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
									strDataAmount += rsMisc.Get_Fields("oicond" + FCConvert.ToString(x));
								}
							}
							// x
						}
						break;
					}
				case 406:
					{
						strDataDesc = "Physical Percent (Outbuilding)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from outbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from sroutbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						strDataAmount = "";
						if (!rsMisc.EndOfFile())
						{
							for (x = 1; x <= 10; x++)
							{
								// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
								if (rsMisc.Get_Fields("oitype" + FCConvert.ToString(x)) > 0)
								{
									if (x > 1)
									{
										strDataAmount += " ";
									}
									// TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
									strDataAmount += rsMisc.Get_Fields("oipctphys" + FCConvert.ToString(x));
								}
							}
							// x
						}
						break;
					}
				case 407:
					{
						strDataDesc = "Functional Percent (Outbuilding)";
						if (!boolSaleType)
						{
							rsMisc.OpenRecordset("select * from outbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard"), modGlobalVariables.strREDatabase);
						}
						else
						{
							rsMisc.OpenRecordset("select * from sroutbuilding where rsaccount = " + RSMast.Get_Fields_Int32("rsaccount") + " and rscard = " + RSMast.Get_Fields_Int32("rscard") + " and saledate = '" + RSMast.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
						}
						strDataAmount = "";
						if (!rsMisc.EndOfFile())
						{
							for (x = 1; x <= 10; x++)
							{
								// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
								if (rsMisc.Get_Fields("oitype" + FCConvert.ToString(x)) > 0)
								{
									if (x > 1)
									{
										strDataAmount += " ";
									}
									// TODO Get_Fields: Field [oipctfunct] not found!! (maybe it is an alias?)
									strDataAmount += rsMisc.Get_Fields("oipctfunct" + FCConvert.ToString(x));
								}
							}
							// x
						}
						break;
					}
			}
			clsTemp.Dispose();
			dbTemp.Dispose();
		}

		

		private void DeleteQueryDefs()
		{
			// Dim dbTemp As DAO.Database
			// Dim qryTemp As DAO.QueryDef
			// Dim boolDeletedOne As Boolean
			// 
			// On Error GoTo ErrorHandler
			// 
			// Set dbTemp = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
			// 
			// here:
			// boolDeletedOne = False
			// For Each qryTemp In dbTemp.QueryDefs
			// If UCase(qryTemp.Name) = "GETINFO" Then
			// boolDeletedOne = True
			// dbTemp.QueryDefs.Delete (qryTemp.Name)
			// Exit For
			// End If
			// If UCase(qryTemp.Name) = "GETSQFT" Then
			// boolDeletedOne = True
			// dbTemp.QueryDefs.Delete (qryTemp.Name)
			// Exit For
			// End If
			// Next qryTemp
			// 
			// If boolDeletedOne Then GoTo here
			return;
			ErrorHandler:
			;
			//MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Delete Query Definitions", null, MessageBoxButtons.OK, MessageBoxIcon.Hand, modal:false);
		}

		private void ChangeQuery(ref string strSQL)
		{
			//qdf.SQL = strSQL;
		}

		private void EraseQuery()
		{
			// On Error GoTo here
			// dbTemp.QueryDefs.Delete ("Get Info")
			// here:
		}
		//
		// Private Sub GetDescription(intCode As Integer)
		//
		// Set rsmisc = dbTemp.OpenRecordset("Select * from costrecord where crecordnumber = " & (1280 + intCode),strredatabase)
		// strDataDesc = Trim(rsmisc.fields("cldesc") & "")
		// Set rsMisc = dbTemp.OpenRecordset("select * from landtype where code = " & intCode - 100,strredatabase)
		// strDataDesc = Trim(rsMisc.Fields("description") & "")
		// End Sub
		private void AddBoxes()
		{
			object ctl;
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                float lngTop;
                float lngLeft;
                float lngWidth;
                // vbPorter upgrade warning: strTemp As string	OnWrite(string, double)
                string strTemp = "";
                string strName = "";
                int intGroup = 0;
                int intLine = 0;
                int intReportNumber = 0;
                clsReportParameter tCode;
                RSExtract.OpenRecordset(
                    "select * from extract where userid = " + FCConvert.ToString(lngUID) +
                    " reportnumber > 0 order by groupnumber", modGlobalVariables.strREDatabase);
                lngTop = 100 / 1440F;
                lngWidth = 1 / 1440F;
                lngLeft = 2880 / 1440F;
                while (!RSExtract.EndOfFile())
                {
                    intReportNumber = FCConvert.ToInt32(RSExtract.Get_Fields_Int32("reportnumber"));
                    intGroup = FCConvert.ToInt32(RSExtract.Get_Fields_Int32("groupnumber"));
                    clsTemp.OpenRecordset(
                        "select * from reporttable where reportnumber = " + FCConvert.ToString(intReportNumber) +
                        " and torp = 'T' order by linenumber", modGlobalVariables.strREDatabase);
                    strName = "report" + FCConvert.ToString(intReportNumber);
                    strTemp = "Group " + FCConvert.ToString(intGroup) + " " + RSExtract.Get_Fields_String("title");
                    AddBox(lngTop, 100 / 1440F, 2880 / 1440F, strName, strTemp, false);
                    lngTop += 255 / 1440F;
                    intLine = 1;
                    while (!clsTemp.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        tCode = clCodeList.GetCodeByCode(FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("code"))),
                            FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields_Int32("CodeID"))));
                        // Call GetDescription(Val(clsTemp.Fields("code")))
                        if (!(tCode == null))
                        {
                            strDataDesc = tCode.Description;
                        }
                        else
                        {
                            strDataDesc = "";
                        }

                        strTemp = strDataDesc;
                        strName = "Group" + FCConvert.ToString(intGroup) + "Line" +
                                  clsTemp.Get_Fields_Int32("linenumber");
                        AddBox(lngTop, lngLeft, 2880 / 1440F, strName, strTemp, false);
                        // title
                        strName = "Group" + FCConvert.ToString(intGroup) + "Value" +
                                  clsTemp.Get_Fields_Int32("linenumber");
                        strTemp = FCConvert.ToString(modGlobalVariables.Statics.reporttotals[intGroup, intLine]);
                        strTemp = Strings.Format(strTemp, "#,###,###,##0");
                        AddBox(lngTop, lngLeft + (2880 + 50) / 1440F, lngWidth, strName, strTemp, true);
                        lngTop += 255 / 1440;
                        intLine += 1;
                        clsTemp.MoveNext();
                    }

                    RSExtract.MoveNext();
                }
            }
        }

		private void AddBox(float lngTop, float lngLeft, float lngWidth, string strName, string strValue, bool boolRight)
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox ctl;
			ctl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			ctl.Top = lngTop;
			ctl.Width = lngWidth;
			ctl.Left = lngLeft;
			if (boolRight)
			{
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
			}
			else
			{
				ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
			}
			ctl.Height = 225 / 1440f;
			ctl.Name = strName;
			Fields.Add(ctl.Name);
			ctl.DataField = ctl.Name;
			Fields[strName].Value = strValue;
			this.ReportFooter.Controls.Add(ctl);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

		
	}
}
