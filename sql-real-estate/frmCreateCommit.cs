﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCommit.
	/// </summary>
	public partial class frmCommit : BaseForm
	{
		public frmCommit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCommit InstancePtr
		{
			get
			{
				return (frmCommit)Sys.GetInstance(typeof(frmCommit));
			}
		}

		protected frmCommit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public void Init()
		{
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Choose the real estate database to copy from";
			MDIParent.InstancePtr.CommonDialog1.FileName = "twre0000.vb1";
			MDIParent.InstancePtr.CommonDialog1.Filter = "*.vb1|*.vb1";
			MDIParent.InstancePtr.CommonDialog1.ShowOpen();
			txtDB.Text = MDIParent.InstancePtr.CommonDialog1.FileName;
		}

		private void cmdBuildCommitment_Click(object sender, System.EventArgs e)
		{
			//FileSystemObject fso = new FileSystemObject();
			if (Conversion.Val(txtCYear.Text) < 1990 || Conversion.Val(txtCYear.Text) > DateTime.Today.Year + 2)
			{
				MessageBox.Show("You must enter a valid commitment year first", "Invalid Commitment Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.Trim(txtDB.Text) == string.Empty)
			{
				MessageBox.Show("You must choose a real estate database file to read the information from", "No Database File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (!File.Exists(txtDB.Text))
			{
				MessageBox.Show("The file " + txtDB.Text + " does not exist", "Invalid File Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			//Application.DoEvents();
			if (modCommit.CreateCommitDatabase())
			{
				// CreateCommitTables
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			if (MessageBox.Show("This will create commitment entries for the year " + txtCYear.Text + "." + "\r\n" + "Is this correct?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//Application.DoEvents();
				modCommit.FillCommitmentRecords_8(FCConvert.ToInt32(Conversion.Val(txtCYear.Text)), Strings.Trim(txtDB.Text));
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				txtCYear.Text = "";
				txtDB.Text = "";
			}
		}

		public void cmdBuildCommitment_Click()
		{
			cmdBuildCommitment_Click(cmdBuildCommitment, new System.EventArgs());
		}

		private void frmCommit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCommit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCommit properties;
			//frmCommit.ScaleWidth	= 5880;
			//frmCommit.ScaleHeight	= 4335;
			//frmCommit.LinkTopic	= "Form1";
			//frmCommit.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuBuild_Click(object sender, System.EventArgs e)
		{
			cmdBuildCommitment_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
