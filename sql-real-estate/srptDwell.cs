﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptDwell.
	/// </summary>
	public partial class srptDwell : FCSectionReport
	{
		public static srptDwell InstancePtr
		{
			get
			{
				return (srptDwell)Sys.GetInstance(typeof(srptDwell));
			}
		}

		protected srptDwell _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptDwell()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptDwell	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCurC;
		

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intCurC = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			if (intCurC == -10)
			{
				// Unload Me
				this.Close();
				return;
			}

		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intCurC < 0)
			{
				// not from valuation screen
				// If lngOutbuildingTotal(MR.Fields("rscard")) > 0 Then
				if (modOutbuilding.Statics.lngOutbuildingTotal[modGlobalVariables.Statics.intCurrentCard] > 0)
				{
					lblSfla.Visible = false;
					txtSfla.Visible = false;
				}
				else
				{
					lblSfla.Visible = true;
					txtSfla.Visible = true;
					txtSfla.Text = Strings.Format(modDwelling.Statics.intSFLA, "#,###,###,##0");
				}
				txtBase.Text = Strings.Format(modDwelling.Statics.dblBase, "#,##0");
				txtTrim.Text = Strings.Format(modDwelling.Statics.intTrim, "#,##0");
				txtRoof.Text = Strings.Format(modDwelling.Statics.intRoof, "#,##0");
				txtO3.Text = Strings.Format(modDwelling.Statics.intOpen3, "#,##0");
				txtO4.Text = Strings.Format(modDwelling.Statics.intOpen4, "#,##0");
				txtBsmt.Text = Strings.Format(modDwelling.Statics.intBasement, "#,##0");
				txtFBsmt.Text = Strings.Format(modDwelling.Statics.intFinBsmt, "#,##0");
				txtHeatCost.Text = Strings.Format(modDwelling.Statics.intHeat, "#,##0");
				txtPlumbing.Text = Strings.Format(modDwelling.Statics.intPlumbing, "#,##0");
				txtAtticCost.Text = Strings.Format(modDwelling.Statics.intAttic, "#,##0");
				txtFireplaceCost.Text = Strings.Format(modDwelling.Statics.intFirePlace, "#,##0");
				txtInsulationCost.Text = Strings.Format(modDwelling.Statics.intInsulation, "#,##0");
				txtUnfinCost.Text = Strings.Format(modDwelling.Statics.intUnfinished, "#,##0");
				// dwell condition
				modProperty.Statics.WKey = FCConvert.ToInt32(1610 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diKITCHENS") + ""));
				if (modProperty.Statics.WKey != 1610)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtKitchensDesc.Text = Strings.Trim(modGlobalVariables.Statics.CostRec.CSDesc + "");
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diyearbuilt")) != 1)
				{
					txtBuilt.Text = modDataTypes.Statics.DWL.Get_Fields_Int32("diYEARBUILT") + "";
				}
				else
				{
					txtBuilt.Text = "Old";
				}
				txtRenovated.Text = modDataTypes.Statics.DWL.Get_Fields_Int32("diYEARREMODEL") + "";
				modProperty.Statics.WKey = FCConvert.ToInt32(1620 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diBATHS") + ""));
				if (modProperty.Statics.WKey != 1620)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtBathsDesc.Text = Strings.Trim(modGlobalVariables.Statics.CostRec.CSDesc + "");
				}
				txtTotal.Text = Strings.Format(modDwelling.Statics.lngRepCostNew[modGlobalVariables.Statics.intCurrentCard], "#,##0");
				modProperty.Statics.WKey = FCConvert.ToInt32(1460 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diCONDITION") + ""));
				if (modProperty.Statics.WKey != 1460)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtCondition.Text = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc + "");
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(1630 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diLAYOUT") + ""));
				if (modProperty.Statics.WKey != 1630)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtLayout.Text = Strings.Trim(modGlobalVariables.Statics.CostRec.CSDesc + "");
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("distyle") + "")));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "DWELLING", "STYLE");
				lblStyle.Text = modGlobalVariables.Statics.CostRec.ClDesc;
				modProperty.Statics.WORK1 = Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade2") + "") / 100;
				modProperty.Statics.WKey = FCConvert.ToInt32(1480 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSTORIES") + ""));
				if (modProperty.Statics.WKey != 1480)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtStyle.Text = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diextwalls") + "")));
				// If WKey <> 1500 Then
				if (modProperty.Statics.WKey != 0)
				{
					// Call Get_Cost_Record(WKey)
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "DWELLING", "EXTERIOR");
					txtExt.Text = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				txtUnits.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsdwelling") + "")) + "  " + "OTHER Units-" + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diunitsother") + ""));
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN3") + "") > 0)
				{
					modProperty.Statics.WKey = 1520;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
					modProperty.Statics.WORKA3 = modGlobalVariables.Statics.CostRec.CSDesc;
					if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 && Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN3")) < 10)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1520 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN3")));
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.CostRec.ClDesc;
					}
					else
					{
						modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN3"));
					}
					lblO3.Text = Strings.Trim(modProperty.Statics.WORKA1);
					lblopen3.Text = Strings.Trim(modProperty.Statics.WORKA1);
					txtOpen3.Text = Strings.Trim(modREASValuations.Statics.WORKA2) + " " + Strings.Mid(FCConvert.ToString(modProperty.Statics.WORKA3), 1, 6);
				}
				else
				{
					txtOpen3.Text = "";
					lblO3.Text = "";
					lblopen3.Text = "";
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN4") + "") > 0)
				{
					modProperty.Statics.WKey = 1530;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
					modProperty.Statics.WORKA3 = modGlobalVariables.Statics.CostRec.CSDesc;
					if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1530 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN4")));
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.CostRec.ClDesc;
					}
					else
					{
						modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN4"));
					}
					lblO4.Text = Strings.Trim(modProperty.Statics.WORKA1);
					lblopen4.Text = Strings.Trim(modProperty.Statics.WORKA1);
					txtOpen4.Text = Strings.Trim(modREASValuations.Statics.WORKA2) + " " + Strings.Mid(FCConvert.ToString(modProperty.Statics.WORKA3), 1, 6);
				}
				else
				{
					lblO4.Text = "";
					txtOpen4.Text = "";
					lblopen4.Text = "";
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(1540 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diFOUNDATION") + ""));
				if (modProperty.Statics.WKey != 1540)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtFoundation.Text = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc + "");
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSFBSMTLIVING") + "") == 0)
				{
					txtFinBsmt.Text = "None";
				}
				else
				{
					if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade1")) == 9)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1670 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade1")));
						modProperty.Statics.WORK1 = Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade2")) * Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade2")) / 10000;
					}
					else
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1670 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade1")));
						modProperty.Statics.WORK1 = Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dibsmtfingrade2")) / 100;
					}
					if (modProperty.Statics.WKey != 1570)
					{
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						txtFinBsmt.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("disfbsmtliving"))) + " Sqft, Grade " + Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2) + Strings.Format(modProperty.Statics.WORK1, "0.00");
					}
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diheat") + "")));
				// If WKey <> 1590 Then
				if (modProperty.Statics.WKey > 0)
				{
					// Call Get_Cost_Record(WKey)
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "DWELLING", "HEAT");
					modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				txtHeating.Text = Strings.Trim(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diPCTHEAT") + ""))) + "% ";
				if (modProperty.Statics.WKey > 0)
					txtHeating.Text = txtHeating.Text + " " + modProperty.Statics.WORKA1;
				txtRooms.Text = FCConvert.ToString(Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diROOMS") + ""));
				txtBedrooms.Text = FCConvert.ToString(Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diBEDROOMS") + ""));
				txtBaths.Text = FCConvert.ToString(Strings.Trim(modDataTypes.Statics.DWL.Get_Fields_Int32("diFULLBATHS") + ""));
				modProperty.Statics.WKey = FCConvert.ToInt32(1640 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diATTIC") + ""));
				if (modProperty.Statics.WKey != 1640)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtAttic.Text = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				txtFireplaces.Text = modDataTypes.Statics.DWL.Get_Fields_Int32("diFIREPLACES") + "";
				modProperty.Statics.WKey = FCConvert.ToInt32(1650 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diINSULATION") + ""));
				if (modProperty.Statics.WKey != 1650)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtInsulation.Text = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diPCTUNFINISHED") + "") == 0)
				{
					txtUnfinLiving.Text = "NONE";
				}
				else
				{
					txtUnfinLiving.Text = modDataTypes.Statics.DWL.Get_Fields_Int32("diPCTUNFINISHED") + "%";
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(1680 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diFUNCTCODE") + ""));
				if (modProperty.Statics.WKey != 1680)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtFobs.Text = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc + "");
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("dieconcode") + "")));
				if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "DWELLING", "ECONOMIC"))
				{
					txtEcObs.Text = modGlobalVariables.Statics.CostRec.ClDesc;
				}
				txtPhys.Text = Strings.Format(modDwelling.Statics.dblPhysicalPercent * 100, "###") + "%";
				txtFunc.Text = Strings.Format(modDwelling.Statics.dblFunctionalPercent * 100, "###") + "%";
				txtEco.Text = Strings.Format(modDwelling.Statics.dblEconomicPercent * 100, "###") + "%";
				txtValue.Text = Strings.Format(modDwelling.Statics.lngdwellcostnew[modGlobalVariables.Statics.intCurrentCard], "#,##0");
				lblsqft.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSQFT") + ""), "#,##0") + " Sqft";
				modProperty.Statics.WKey = FCConvert.ToInt32(1670 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade1") + ""));
				if (modProperty.Statics.WKey != 1670)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtGrade.Text = "Grade " + Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2) + (FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("digrade2") + "")));
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSFMASONRY") + "") == 0)
				{
					txtMasonry.Text = "None";
				}
				else
				{
					txtMasonry.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diSFMASONRY")), "#,###") + "Sqft";
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(1510 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diROOF") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
				if (modProperty.Statics.WKey != 1510)
					txtRoofCover.Text = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc + "");
				modREASValuations.Statics.WORKA2 = "";
				modProperty.Statics.WKey = FCConvert.ToInt32(1560 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diWETBSMT") + ""));
				if (modProperty.Statics.WKey != 1560)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modREASValuations.Statics.WORKA2 = Strings.Trim(modGlobalVariables.Statics.CostRec.CSDesc);
				}
				modProperty.Statics.WKey = FCConvert.ToInt32(1550 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diBSMT") + ""));
				if (modProperty.Statics.WKey != 1550)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtBasement.Text = Strings.Trim(modREASValuations.Statics.WORKA2 + " " + modGlobalVariables.Statics.CostRec.CSDesc + "");
				}
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diBSMTGAR") + "") == 0)
				{
					txtGar.Text = "None";
				}
				else
				{
					txtGar.Text = modDataTypes.Statics.DWL.Get_Fields_Int32("diBSMTGAR") + " CAR";
				}
				txtCooling.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diPCTCOOL") + "")) + "% ";
				modProperty.Statics.WKey = FCConvert.ToInt32(1600 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diCOOL") + ""));
				if (modProperty.Statics.WKey != 1600)
				{
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtCooling.Text = txtCooling.Text + modGlobalVariables.Statics.CostRec.ClDesc;
				}
				txtHalfBaths.Text = FCConvert.ToString(modDataTypes.Statics.DWL.Get_Fields_Int32("diHALFBATHS") + "");
				txtFixtures.Text = FCConvert.ToString(modDataTypes.Statics.DWL.Get_Fields_Int32("diADDNFIXTURES") + "");
				if (Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN5") + "") > 0)
				{
					modProperty.Statics.WKey = 1570;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc + "");
					modProperty.Statics.WORKA3 = Strings.Trim(modGlobalVariables.Statics.CostRec.CSDesc + "");
					if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999)
					{
						modProperty.Statics.WKey = FCConvert.ToInt32(1570 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN5") + ""));
						if (modProperty.Statics.WKey != 1570)
						{
							modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
							modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.CostRec.ClDesc;
						}
					}
					else
					{
						modREASValuations.Statics.WORKA2 = FCConvert.ToString(modDataTypes.Statics.DWL.Get_Fields_Int32("diOPEN5"));
					}
					lblOpen5.Text = Strings.Trim(modProperty.Statics.WORKA1);
					if (modProperty.Statics.WKey != 1570)
						txtOpen5.Text = Strings.Trim(modREASValuations.Statics.WORKA2);
					lblO5.Text = Strings.Trim(FCConvert.ToString(modProperty.Statics.WORKA3));
					txtO5.Text = Strings.Format(modDwelling.Statics.intOpen5, "#,##0");
				}
				else
				{
					lblO5.Text = "";
					txtO5.Text = "";
				}
			}
			else
			{
				// With frmNewValuationReport.DwellDescGrid(intCurC)
				if (modSpeedCalc.Statics.CalcOutList[intCurC + 1].OutbuildingType[1] > 0)
				{
					lblSfla.Visible = false;
					txtSfla.Visible = false;
				}
				else
				{
					lblSfla.Visible = true;
					txtSfla.Visible = true;
					lblSfla.Text = "SFLA";
					txtSfla.Text = Strings.Format(modDwelling.Statics.intSFLA, "#,###,##0");
				}
				// lblStyle.Caption = .TextMatrix(0, 0)
				// txtStyle.Text = .TextMatrix(0, 1)
				// txtExt.Text = .TextMatrix(1, 1)
				// txtUnits.Text = .TextMatrix(2, 1)
				// lblopen3.Caption = .TextMatrix(3, 0)
				// txtOpen3.Text = .TextMatrix(3, 1)
				// lblopen4.Caption = .TextMatrix(4, 0)
				// txtOpen4.Text = .TextMatrix(4, 1)
				// txtFoundation.Text = .TextMatrix(5, 1)
				// txtFinBsmt.Text = .TextMatrix(6, 1)
				// txtHeating.Text = .TextMatrix(7, 1)
				// txtRooms.Text = .TextMatrix(8, 1)
				// txtBedrooms.Text = .TextMatrix(9, 1)
				// txtBaths.Text = .TextMatrix(10, 1)
				// txtAttic.Text = .TextMatrix(11, 1)
				// txtFireplaces.Text = .TextMatrix(12, 1)
				// txtInsulation.Text = .TextMatrix(13, 1)
				// txtUnfinLiving.Text = .TextMatrix(14, 1)
				lblStyle.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].BuildingType;
				txtStyle.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Stories;
				txtExt.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Exterior;
				txtUnits.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].DwellUnits;
				lblopen3.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open3Title;
				txtOpen3.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open3;
				lblopen4.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open4Title;
				txtOpen4.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open4;
				txtFoundation.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Foundation;
				txtFinBsmt.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].FinBasementArea;
				txtHeating.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Heating;
				txtRooms.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Rooms;
				txtBedrooms.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Bedrooms;
				txtBaths.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Bath;
				txtAttic.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Attic;
				txtFireplaces.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Fireplaces;
				txtInsulation.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Insulation;
				txtUnfinLiving.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].UnfinLivingArea;
				// With frmNewValuationReport.DwellMiscGrid(intCurC)
				// lblsqft.Caption = .TextMatrix(0, 0)
				// txtGrade.Text = .TextMatrix(0, 1)
				// txtMasonry.Text = .TextMatrix(1, 1)
				// txtRoofCover.Text = .TextMatrix(2, 1)
				// txtBasement.Text = .TextMatrix(5, 1)
				// txtGar.Text = .TextMatrix(6, 1)
				// txtCooling.Text = .TextMatrix(7, 1)
				// lblOpen5.Caption = .TextMatrix(8, 0)
				// txtOpen5.Text = .TextMatrix(8, 1)
				// txtFixtures.Text = .TextMatrix(9, 1)
				// txtHalfBaths.Text = .TextMatrix(10, 1)
				lblsqft.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Sqft;
				txtGrade.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Grade;
				txtMasonry.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].MasonryTrim;
				txtRoofCover.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Roof;
				txtBasement.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Basement;
				txtGar.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].BsmtGarage;
				txtCooling.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Cooling;
				lblOpen5.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open5Title;
				txtOpen5.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open5;
				txtFixtures.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].AddnFixtures;
				txtHalfBaths.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].HalfBaths;
				// With frmNewValuationReport.DwellingGrid(intCurC)
				// txtBase.Text = .TextMatrix(0, 1)
				// txtTrim.Text = .TextMatrix(1, 1)
				// txtRoof.Text = .TextMatrix(2, 1)
				// lblO3.Caption = .TextMatrix(3, 0)
				// txtO3.Text = .TextMatrix(3, 1)
				// lblO4.Caption = .TextMatrix(4, 0)
				// txtO4.Text = .TextMatrix(4, 1)
				// txtBsmt.Text = .TextMatrix(5, 1)
				// txtFBsmt.Text = .TextMatrix(6, 1)
				// txtHeatCost.Text = .TextMatrix(7, 1)
				// lblO5.Caption = .TextMatrix(8, 0)
				// txtO5.Text = .TextMatrix(8, 1)
				// txtPlumbing.Text = .TextMatrix(10, 1)
				// txtAtticCost.Text = .TextMatrix(11, 1)
				// txtFireplaceCost.Text = .TextMatrix(12, 1)
				// txtInsulationCost.Text = .TextMatrix(13, 1)
				// txtUnfinCost.Text = .TextMatrix(14, 1)
				txtBase.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Base;
				txtTrim.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Trim;
				txtRoof.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].RoofVal;
				lblO3.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open3Title;
				txtO3.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open3Val;
				lblO4.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open4Title;
				txtO4.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open4Val;
				txtBsmt.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].BasementVal;
				txtFBsmt.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].FinBasementVal;
				txtHeatCost.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].HeatVal;
				lblO5.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open5Title;
				txtO5.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Open5Val;
				txtPlumbing.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].PlumbingVal;
				txtAtticCost.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].AtticVal;
				txtFireplaceCost.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].FireplaceVal;
				txtInsulationCost.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].InsulationVal;
				txtUnfinCost.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].UnfinishedVal;
				// With frmNewValuationReport.DwellCondGrid(intCurC)
				// txtBuilt.Text = .TextMatrix(1, 0)
				// txtRenovated.Text = .TextMatrix(1, 1)
				// txtKitchensDesc.Text = .TextMatrix(1, 2)
				// txtBathsDesc.Text = .TextMatrix(1, 3)
				// txtCondition.Text = .TextMatrix(1, 4)
				// txtLayout.Text = .TextMatrix(1, 5)
				// txtTotal.Text = .TextMatrix(1, 6)
				if (Conversion.Val(modSpeedCalc.Statics.CalcDwellList[intCurC + 1].YearBuilt) != 1)
				{
					txtBuilt.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].YearBuilt;
				}
				else
				{
					txtBuilt.Text = "Old";
				}
				txtRenovated.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Renovated;
				txtKitchensDesc.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Kitchens;
				txtBathsDesc.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Baths;
				txtCondition.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Condition;
				txtLayout.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Layout;
				txtTotal.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].Total;
				// With frmNewValuationReport.DwellPercGrid(intCurC)
				// txtFobs.Text = .TextMatrix(1, 0)
				// txtEcObs.Text = .TextMatrix(1, 1)
				// txtPhys.Text = .TextMatrix(1, 2)
				// txtFunc.Text = .TextMatrix(1, 3)
				// txtEco.Text = .TextMatrix(1, 4)
				// txtValue.Text = .TextMatrix(1, 5)
				txtFobs.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].FuncObs;
				txtEcObs.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].EconObs;
				txtPhys.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].PhysPerc;
				txtFunc.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].FuncPerc;
				txtEco.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].EconPerc;
				txtValue.Text = modSpeedCalc.Statics.CalcDwellList[intCurC + 1].DwellValue;
			}
		}

	
	}
}
