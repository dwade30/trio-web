﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPictures.
	/// </summary>
	public partial class rptPictures : BaseSectionReport
	{
		public static rptPictures InstancePtr
		{
			get
			{
				return (rptPictures)Sys.GetInstance(typeof(rptPictures));
			}
		}

		protected rptPictures _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptPictures()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Picture Report";
		}
		// nObj = 1
		//   0	rptPictures	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();
		bool boolNoPictures;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			// Call clsReport.OpenRecordset("select * from picturerecord inner join (SElect rsaccount,rscard,rsname,rsmaplot from master where not rsdeleted = 1) as tbl1 on (picturerecord.mraccountnumber = tbl1.rsaccount) and (picturerecord.rscard = tbl1.rscard) order by mraccountnumber,picturerecord.rscard,picnum", strREDatabase)
			// If clsReport.EndOfFile Then
			// MsgBox "There are no pictures associated with any files", vbOKOnly + vbInformation, "No Pictures"
			// Unload Me
			// Exit Sub
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			if (!clsReport.EndOfFile())
			{
				txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("rsmaplot")));
				if (!boolNoPictures)
				{
					txtCard.Text = FCConvert.ToString(clsReport.Get_Fields_Int32("rscard"));
					txtAccount.Text = FCConvert.ToString(clsReport.Get_Fields_Int32("mraccountnumber"));
                    strTemp = clsReport.Get_Fields_String("PICTURELOCATION");
				}
				else
				{
					strTemp = Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("rsname")));
					txtCard.Text = FCConvert.ToString(clsReport.Get_Fields_Int32("rscard"));
					txtAccount.Text = FCConvert.ToString(clsReport.Get_Fields_Int32("rsaccount"));
					txtLocation.Text = Strings.Trim(clsReport.Get_Fields_String("rslocnumalph") + " " + clsReport.Get_Fields_String("rslocstreet"));
				}
				txtFile.Text = strTemp;
				clsReport.MoveNext();
			}
		}
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public void Init(ref bool boolRange, ref short intType, string strmin = "", string strmax = "", bool boolFromExtract = false, bool boolNoPics = false, bool boolBldgValOnly = false)
		{
			string strSQL = "";
			string strWhere = "";
			string strOrderBy = "";
			string strFName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			// strREFullDBName = rsTemp.GetFullDBName("RealEstate")
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			boolNoPictures = boolNoPics;
			strFName = "Pictures";
			if (boolNoPictures)
			{
				lblTitle.Text = "Accounts Without Photos";
				lblFileorName.Text = "Name";
				strFName = "NoPictures";
			}
			switch (intType)
			{
				case 1:
					{
						// account
						if (!boolNoPictures)
						{
							strOrderBy = " order by rsaccount,picturerecord.rscard,picnum ";
						}
						else
						{
							strOrderBy = " order by rsaccount,tbl1.rscard ";
						}
						strWhere = " and rsaccount between " + FCConvert.ToString(Conversion.Val(strmin)) + " and " + FCConvert.ToString(Conversion.Val(strmax));
						break;
					}
				case 2:
					{
						// map lot
						if (!boolNoPictures)
						{
							strOrderBy = " order by rsmaplot,rsaccount,picturerecord.rscard,picnum ";
						}
						else
						{
							strOrderBy = " order by rsmaplot,rsaccount,tbl1.rscard ";
						}
						strWhere = " and rsmaplot between '" + strmin + "' and '" + strmax + "'";
						break;
					}
			}
			//end switch
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (!boolNoPictures)
			{
				txtLocation.Visible = false;
				lblLocation.Visible = false;
			}
			else
			{
				txtLocation.Visible = true;
				lblLocation.Visible = true;
				txtFile.Width = txtLocation.Left - txtFile.Left - 20 / 1440f;
			}
			if (boolFromExtract)
			{
				if (!boolNoPictures)
				{
					// strSQL = "select * from picturerecord inner join (select master.rsaccount,master.rscard,master.rsmaplot,master.rsname,rslocnumalph,rslocstreet from (master inner join  (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and not rsdeleted = 1) as tbl1  on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard)  " & strOrderBy
					strSQL = "select * from picturerecord inner join (select mj.rsaccount,mj.rscard,mj.rsmaplot,mj.rsname,rslocnumalph,rslocstreet from (" + strMasterJoinQuery + " inner join  (extracttable inner join labelaccounts  on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and not rsdeleted = 1) as tbl1  on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard)  " + strOrderBy;
				}
				else
				{
					if (!boolBldgValOnly)
					{
						// strSQL = "select * from picturerecord right join (select master.rsaccount,master.rscard,master.rsmaplot,master.rsname,rslocnumalph,rslocstreet,lastbldgval from (master inner join  (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and not rsdeleted = 1) as tbl1  on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard) where isnull(picturerecord.mraccountnumber,0)  = 0  " & strOrderBy
						strSQL = "select tbl1.rscard as rscard,* from picturerecord right join (select mj.rsaccount,mj.rscard,mj.rsmaplot,mj.rsname,rslocnumalph,rslocstreet,lastbldgval from (" + strMasterJoinQuery + " inner join  (extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and not rsdeleted = 1) as tbl1  on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard) where isnull(picturerecord.mraccountnumber,0)  = 0  " + strOrderBy;
					}
					else
					{
						// strSQL = "select * from picturerecord right join (select master.rsaccount,master.rscard,master.rsmaplot,master.rsname,rslocnumalph,rslocstreet,lastbldgval from (master inner join  (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and not rsdeleted = 1 and lastbldgval > 0) as tbl1  on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard) where isnull(picturerecord.mraccountnumber,0)  = 0  " & strOrderBy
						strSQL = "select tbl1.rscard as rscard,* from picturerecord right join (select mj.rsaccount,mj.rscard,mj.rsmaplot,mj.rsname,rslocnumalph,rslocstreet,lastbldgval from (" + strMasterJoinQuery + " inner join  (extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and not rsdeleted = 1 and lastbldgval > 0) as tbl1  on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard) where isnull(picturerecord.mraccountnumber,0)  = 0  " + strOrderBy;
					}
				}
			}
			else
			{
				if (!boolRange)
				{
					strWhere = "";
				}
				if (!boolNoPictures)
				{
					// strSQL = "select * from picturerecord inner join (select * from master  where not rsdeleted = 1 " & strWhere & ") as tbl1 on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard) " & strOrderBy
					strSQL = "select tbl1.rscard as rscard,* from picturerecord inner join (" + strMasterJoin + "  where not rsdeleted = 1 " + strWhere + ") as tbl1 on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard) " + strOrderBy;
				}
				else
				{
					if (!boolBldgValOnly)
					{
						// strSQL = "select * from picturerecord right join (select * from master  where not rsdeleted = 1 " & strWhere & ") as tbl1 on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard) where isnull(picturerecord.mraccountnumber,0)  = 0 " & strOrderBy
						strSQL = "select tbl1.rscard as rscard,* from picturerecord right join (" + strMasterJoin + "  where not rsdeleted = 1 " + strWhere + ") as tbl1 on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard) where isnull(picturerecord.mraccountnumber,0)  = 0 " + strOrderBy;
					}
					else
					{
						// strSQL = "select tbl1.rscard as rscard,* from picturerecord right join (select * from master  where not rsdeleted = 1 and lastbldgval > 0 " & strWhere & ") as tbl1 on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard) where isnull(picturerecord.mraccountnumber,0)  = 0 " & strOrderBy
						strSQL = "select tbl1.rscard as rscard,* from picturerecord right join (" + strMasterJoin + "  where not rsdeleted = 1 and lastbldgval > 0 " + strWhere + ") as tbl1 on (tbl1.rsaccount = picturerecord.mraccountnumber) and (tbl1.rscard = picturerecord.rscard) where isnull(picturerecord.mraccountnumber,0)  = 0 " + strOrderBy;
					}
				}
			}
			clsReport.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (clsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, strFName);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
