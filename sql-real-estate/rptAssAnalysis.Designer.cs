﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptAssAnalysis.
	/// </summary>
	partial class rptAssAnalysis
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAssAnalysis));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssess1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreage1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubReport3 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtoutcount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtoutassess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAverage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.SubReport4 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBldgCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgAve = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubReport5 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubReport6 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubReport7 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCommCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCommAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCommAve = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport8 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubReport9 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLandOvCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandOvAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandOvAve = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBldgOvCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgOvAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgOvAve = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport10 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEACount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEAAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEAAve = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtoutcount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtoutassess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAverage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgAve)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCommCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCommAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCommAve)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandOvCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandOvAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandOvAve)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgOvCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgOvAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgOvAve)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEACount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEAAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEAAve)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1,
				this.SubReport2,
				this.Field1,
				this.Label1,
				this.Label3,
				this.Field2,
				this.Field3,
				this.Label4,
				this.txtCount1,
				this.txtAssess1,
				this.txtAcreage1,
				this.Line1,
				this.Field4,
				this.Label10,
				this.Label11,
				this.Label12,
				this.SubReport3,
				this.Label13,
				this.Line2,
				this.Label14,
				this.txtoutcount,
				this.txtoutassess,
				this.txtAverage,
				this.Label15,
				this.Line3,
				this.SubReport4,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.txtBldgCount,
				this.txtBldgAssess,
				this.txtBldgAve,
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.SubReport5,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.SubReport6,
				this.Label29,
				this.Line4,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label33,
				this.SubReport7,
				this.Label34,
				this.txtCommCount,
				this.txtCommAssess,
				this.txtCommAve,
				this.SubReport8,
				this.Label35,
				this.Line5,
				this.Label36,
				this.Label37,
				this.Label38,
				this.Label39,
				this.SubReport9,
				this.Label40,
				this.Line6,
				this.Label41,
				this.Label42,
				this.Label43,
				this.Label44,
				this.txtLandOvCount,
				this.txtLandOvAssess,
				this.txtLandOvAve,
				this.Label45,
				this.Line7,
				this.Label46,
				this.Label47,
				this.txtBldgOvCount,
				this.txtBldgOvAssess,
				this.txtBldgOvAve,
				this.SubReport10,
				this.Label48,
				this.txtEACount,
				this.txtEAAssess,
				this.txtEAAve
			});
			this.Detail.Height = 9.760417F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.21875F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.40625F;
			this.SubReport1.Width = 7.4375F;
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.25F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.96875F;
			this.SubReport2.Width = 7.4375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 1.8125F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Field1.Text = "LAND TOTAL";
			this.Field1.Top = 0.625F;
			this.Field1.Width = 1F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.Label1.Text = "L  A  N  D    A  N  A  L  Y  S  I  S";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label3.Text = "COUNT";
			this.Label3.Top = 0.25F;
			this.Label3.Width = 0.5F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.19F;
			this.Field2.Left = 5.375F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Field2.Text = "ASSESSMENT";
			this.Field2.Top = 0.25F;
			this.Field2.Width = 1F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.19F;
			this.Field3.Left = 6.6875F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Field3.Text = "ACREAGE";
			this.Field3.Top = 0.25F;
			this.Field3.Width = 0.6875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.0625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label4.Text = "LAND CODE";
			this.Label4.Top = 0.25F;
			this.Label4.Width = 1.125F;
			// 
			// txtCount1
			// 
			this.txtCount1.Height = 0.19F;
			this.txtCount1.Left = 3.5F;
			this.txtCount1.Name = "txtCount1";
			this.txtCount1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtCount1.Text = null;
			this.txtCount1.Top = 0.625F;
			this.txtCount1.Width = 0.8125F;
			// 
			// txtAssess1
			// 
			this.txtAssess1.Height = 0.19F;
			this.txtAssess1.Left = 5F;
			this.txtAssess1.Name = "txtAssess1";
			this.txtAssess1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAssess1.Text = null;
			this.txtAssess1.Top = 0.625F;
			this.txtAssess1.Width = 1.25F;
			// 
			// txtAcreage1
			// 
			this.txtAcreage1.Height = 0.19F;
			this.txtAcreage1.Left = 6.3125F;
			this.txtAcreage1.Name = "txtAcreage1";
			this.txtAcreage1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAcreage1.Text = null;
			this.txtAcreage1.Top = 0.625F;
			this.txtAcreage1.Width = 1.0625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.1875F;
			this.Line1.Width = 7.4375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 0.1875F;
			this.Line1.Y2 = 0.1875F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.19F;
			this.Field4.Left = 0F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Field4.Text = "OUTBUILDING CODE";
			this.Field4.Top = 1.6875F;
			this.Field4.Width = 1.4375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label10.Text = "COUNT";
			this.Label10.Top = 1.71875F;
			this.Label10.Width = 0.625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.4375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label11.Text = "ASSESSMENT";
			this.Label11.Top = 1.71875F;
			this.Label11.Width = 0.9375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 6.6875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label12.Text = "AVERAGE";
			this.Label12.Top = 1.71875F;
			this.Label12.Width = 0.6875F;
			// 
			// SubReport3
			// 
			this.SubReport3.CloseBorder = false;
			this.SubReport3.Height = 0.1875F;
			this.SubReport3.Left = 0F;
			this.SubReport3.Name = "SubReport3";
			this.SubReport3.Report = null;
			this.SubReport3.Top = 1.90625F;
			this.SubReport3.Width = 7.4375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.0625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.Label13.Text = "O  U  T  B  U  I  L  D  I  N  G    A  N  A  L  Y  S  I  S";
			this.Label13.Top = 1.40625F;
			this.Label13.Width = 7.3125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.625F;
			this.Line2.Width = 7.4375F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 1.625F;
			this.Line2.Y2 = 1.625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.1875F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label14.Text = "TOTAL";
			this.Label14.Top = 2.09375F;
			this.Label14.Width = 0.5F;
			// 
			// txtoutcount
			// 
			this.txtoutcount.CanGrow = false;
			this.txtoutcount.Height = 0.19F;
			this.txtoutcount.Left = 3.375F;
			this.txtoutcount.MultiLine = false;
			this.txtoutcount.Name = "txtoutcount";
			this.txtoutcount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtoutcount.Text = "Field5";
			this.txtoutcount.Top = 2.09375F;
			this.txtoutcount.Width = 0.9375F;
			// 
			// txtoutassess
			// 
			this.txtoutassess.CanGrow = false;
			this.txtoutassess.Height = 0.19F;
			this.txtoutassess.Left = 4.875F;
			this.txtoutassess.MultiLine = false;
			this.txtoutassess.Name = "txtoutassess";
			this.txtoutassess.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtoutassess.Text = "Field5";
			this.txtoutassess.Top = 2.09375F;
			this.txtoutassess.Width = 1.375F;
			// 
			// txtAverage
			// 
			this.txtAverage.CanGrow = false;
			this.txtAverage.Height = 0.19F;
			this.txtAverage.Left = 6.3125F;
			this.txtAverage.MultiLine = false;
			this.txtAverage.Name = "txtAverage";
			this.txtAverage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAverage.Text = "Field5";
			this.txtAverage.Top = 2.09375F;
			this.txtAverage.Width = 1F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.Label15.Text = "D  W  E  L  L  I  N  G    A  N  A  L  Y  S  I  S";
			this.Label15.Top = 2.4375F;
			this.Label15.Width = 7.375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2.625F;
			this.Line3.Width = 7.375F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.375F;
			this.Line3.Y1 = 2.625F;
			this.Line3.Y2 = 2.625F;
			// 
			// SubReport4
			// 
			this.SubReport4.CloseBorder = false;
			this.SubReport4.Height = 0.1875F;
			this.SubReport4.Left = 0F;
			this.SubReport4.Name = "SubReport4";
			this.SubReport4.Report = null;
			this.SubReport4.Top = 3F;
			this.SubReport4.Width = 7.4375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label16.Text = "BUILDING CODE";
			this.Label16.Top = 2.84375F;
			this.Label16.Width = 1.25F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label17.Text = "COUNT";
			this.Label17.Top = 2.84375F;
			this.Label17.Width = 0.5F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 5.4375F;
			this.Label18.MultiLine = false;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label18.Text = "ASSESSMENT";
			this.Label18.Top = 2.84375F;
			this.Label18.Width = 0.9375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 6.6875F;
			this.Label19.MultiLine = false;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label19.Text = "AVERAGE";
			this.Label19.Top = 2.84375F;
			this.Label19.Width = 0.6875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 1.5F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label20.Text = "DWELLING TOTAL";
			this.Label20.Top = 3.1875F;
			this.Label20.Width = 1.25F;
			// 
			// txtBldgCount
			// 
			this.txtBldgCount.CanGrow = false;
			this.txtBldgCount.Height = 0.19F;
			this.txtBldgCount.Left = 3.375F;
			this.txtBldgCount.MultiLine = false;
			this.txtBldgCount.Name = "txtBldgCount";
			this.txtBldgCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtBldgCount.Text = "Field5";
			this.txtBldgCount.Top = 3.1875F;
			this.txtBldgCount.Width = 0.9375F;
			// 
			// txtBldgAssess
			// 
			this.txtBldgAssess.CanGrow = false;
			this.txtBldgAssess.Height = 0.19F;
			this.txtBldgAssess.Left = 4.75F;
			this.txtBldgAssess.MultiLine = false;
			this.txtBldgAssess.Name = "txtBldgAssess";
			this.txtBldgAssess.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtBldgAssess.Text = "Field6";
			this.txtBldgAssess.Top = 3.1875F;
			this.txtBldgAssess.Width = 1.5F;
			// 
			// txtBldgAve
			// 
			this.txtBldgAve.CanGrow = false;
			this.txtBldgAve.Height = 0.19F;
			this.txtBldgAve.Left = 6.3125F;
			this.txtBldgAve.MultiLine = false;
			this.txtBldgAve.Name = "txtBldgAve";
			this.txtBldgAve.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtBldgAve.Text = "Field7";
			this.txtBldgAve.Top = 3.1875F;
			this.txtBldgAve.Width = 1.0625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0F;
			this.Label21.MultiLine = false;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label21.Text = "BUILDING STYLE";
			this.Label21.Top = 3.6875F;
			this.Label21.Width = 1.3125F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3.875F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'";
			this.Label22.Text = "COUNT";
			this.Label22.Top = 3.6875F;
			this.Label22.Width = 0.5625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 5.4375F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'";
			this.Label23.Text = "ASSESSMENT";
			this.Label23.Top = 3.6875F;
			this.Label23.Width = 0.9375F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 6.6875F;
			this.Label24.MultiLine = false;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'";
			this.Label24.Text = "AVERAGE";
			this.Label24.Top = 3.6875F;
			this.Label24.Width = 0.6875F;
			// 
			// SubReport5
			// 
			this.SubReport5.CloseBorder = false;
			this.SubReport5.Height = 0.1875F;
			this.SubReport5.Left = 0F;
			this.SubReport5.Name = "SubReport5";
			this.SubReport5.Report = null;
			this.SubReport5.Top = 3.875F;
			this.SubReport5.Width = 7.4375F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label25.Text = "AGE OF DWELLING";
			this.Label25.Top = 4.40625F;
			this.Label25.Width = 1.3125F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.19F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 3.875F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'";
			this.Label26.Text = "COUNT";
			this.Label26.Top = 4.40625F;
			this.Label26.Width = 0.5625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.19F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 5.4375F;
			this.Label27.MultiLine = false;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'";
			this.Label27.Text = "ASSESSMENT";
			this.Label27.Top = 4.40625F;
			this.Label27.Width = 0.9375F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.19F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 6.6875F;
			this.Label28.MultiLine = false;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'";
			this.Label28.Text = "AVERAGE";
			this.Label28.Top = 4.40625F;
			this.Label28.Width = 0.6875F;
			// 
			// SubReport6
			// 
			this.SubReport6.CloseBorder = false;
			this.SubReport6.Height = 0.21875F;
			this.SubReport6.Left = 0F;
			this.SubReport6.Name = "SubReport6";
			this.SubReport6.Report = null;
			this.SubReport6.Top = 4.59375F;
			this.SubReport6.Width = 7.4375F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0F;
			this.Label29.MultiLine = false;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.Label29.Text = "C  O  M  M  E  R  C  I  A  L    A  N  A  L  Y  S  I  S";
			this.Label29.Top = 5.03125F;
			this.Label29.Width = 7.4375F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 5.21875F;
			this.Line4.Width = 7.3125F;
			this.Line4.X1 = 0.125F;
			this.Line4.X2 = 7.4375F;
			this.Line4.Y1 = 5.21875F;
			this.Line4.Y2 = 5.21875F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.19F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0.0625F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'";
			this.Label30.Text = "OCCUPANCY CODE";
			this.Label30.Top = 5.375F;
			this.Label30.Width = 1.375F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.19F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 3.875F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'";
			this.Label31.Text = "COUNT";
			this.Label31.Top = 5.375F;
			this.Label31.Width = 0.5625F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.19F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 5.4375F;
			this.Label32.MultiLine = false;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'";
			this.Label32.Text = "ASSESSMENT";
			this.Label32.Top = 5.375F;
			this.Label32.Width = 0.9375F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 6.6875F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'";
			this.Label33.Text = "AVERAGE";
			this.Label33.Top = 5.375F;
			this.Label33.Width = 0.6875F;
			// 
			// SubReport7
			// 
			this.SubReport7.CloseBorder = false;
			this.SubReport7.Height = 0.1875F;
			this.SubReport7.Left = 0F;
			this.SubReport7.Name = "SubReport7";
			this.SubReport7.Report = null;
			this.SubReport7.Top = 5.5625F;
			this.SubReport7.Width = 7.4375F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.19F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 1.5F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'";
			this.Label34.Text = "COMMERCIAL TOTAL";
			this.Label34.Top = 5.75F;
			this.Label34.Width = 1.4375F;
			// 
			// txtCommCount
			// 
			this.txtCommCount.CanGrow = false;
			this.txtCommCount.Height = 0.19F;
			this.txtCommCount.Left = 3.375F;
			this.txtCommCount.MultiLine = false;
			this.txtCommCount.Name = "txtCommCount";
			this.txtCommCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCommCount.Text = null;
			this.txtCommCount.Top = 5.75F;
			this.txtCommCount.Width = 0.9375F;
			// 
			// txtCommAssess
			// 
			this.txtCommAssess.CanGrow = false;
			this.txtCommAssess.Height = 0.19F;
			this.txtCommAssess.Left = 4.75F;
			this.txtCommAssess.MultiLine = false;
			this.txtCommAssess.Name = "txtCommAssess";
			this.txtCommAssess.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCommAssess.Text = "Field5";
			this.txtCommAssess.Top = 5.75F;
			this.txtCommAssess.Width = 1.5F;
			// 
			// txtCommAve
			// 
			this.txtCommAve.CanGrow = false;
			this.txtCommAve.Height = 0.1875F;
			this.txtCommAve.Left = 6.3125F;
			this.txtCommAve.MultiLine = false;
			this.txtCommAve.Name = "txtCommAve";
			this.txtCommAve.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCommAve.Text = "Field5";
			this.txtCommAve.Top = 5.75F;
			this.txtCommAve.Width = 1.0625F;
			// 
			// SubReport8
			// 
			this.SubReport8.CloseBorder = false;
			this.SubReport8.Height = 0.25F;
			this.SubReport8.Left = 0F;
			this.SubReport8.Name = "SubReport8";
			this.SubReport8.Report = null;
			this.SubReport8.Top = 6.65625F;
			this.SubReport8.Width = 7.4375F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label35.Text = "E  X  E  M  P  T  I  O  N    A  N  A  L  Y  S  I  S";
			this.Label35.Top = 6.09375F;
			this.Label35.Width = 7.4375F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 6.28125F;
			this.Line5.Width = 7.4375F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 7.4375F;
			this.Line5.Y1 = 6.28125F;
			this.Line5.Y2 = 6.28125F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 0F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Tahoma\'";
			this.Label36.Text = "EXEMPTION CODE";
			this.Label36.Top = 6.46875F;
			this.Label36.Width = 1.5F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.19F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 3.875F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Tahoma\'";
			this.Label37.Text = "COUNT";
			this.Label37.Top = 6.46875F;
			this.Label37.Width = 0.5F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.19F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 5.4375F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Tahoma\'";
			this.Label38.Text = "EXEMPTION";
			this.Label38.Top = 6.46875F;
			this.Label38.Width = 0.9375F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.19F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 6.6875F;
			this.Label39.MultiLine = false;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-family: \'Tahoma\'";
			this.Label39.Text = "AVERAGE";
			this.Label39.Top = 6.46875F;
			this.Label39.Width = 0.6875F;
			// 
			// SubReport9
			// 
			this.SubReport9.CloseBorder = false;
			this.SubReport9.Height = 0.21875F;
			this.SubReport9.Left = 0F;
			this.SubReport9.Name = "SubReport9";
			this.SubReport9.Report = null;
			this.SubReport9.Top = 7.875F;
			this.SubReport9.Width = 7.5F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.19F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 0.0625F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.Label40.Text = "L  A  N  D    O  V  E  R  R  I  D  E  S";
			this.Label40.Top = 7.375F;
			this.Label40.Width = 7.4375F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 0.0625F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 7.53125F;
			this.Line6.Width = 7.4375F;
			this.Line6.X1 = 7.5F;
			this.Line6.X2 = 0.0625F;
			this.Line6.Y1 = 7.53125F;
			this.Line6.Y2 = 7.53125F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.19F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 0.0625F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-family: \'Tahoma\'";
			this.Label41.Text = "LAND CODE";
			this.Label41.Top = 7.6875F;
			this.Label41.Width = 0.9375F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.19F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 3.875F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-family: \'Tahoma\'";
			this.Label42.Text = "COUNT";
			this.Label42.Top = 7.6875F;
			this.Label42.Width = 0.5F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.19F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 5.5F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Tahoma\'";
			this.Label43.Text = "ASSESSMENT";
			this.Label43.Top = 7.6875F;
			this.Label43.Width = 1F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.19F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 6.75F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-family: \'Tahoma\'";
			this.Label44.Text = "AVERAGE";
			this.Label44.Top = 7.6875F;
			this.Label44.Width = 0.6875F;
			// 
			// txtLandOvCount
			// 
			this.txtLandOvCount.CanGrow = false;
			this.txtLandOvCount.Height = 0.1875F;
			this.txtLandOvCount.Left = 3.4375F;
			this.txtLandOvCount.MultiLine = false;
			this.txtLandOvCount.Name = "txtLandOvCount";
			this.txtLandOvCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLandOvCount.Text = "Field5";
			this.txtLandOvCount.Top = 8.09375F;
			this.txtLandOvCount.Width = 0.875F;
			// 
			// txtLandOvAssess
			// 
			this.txtLandOvAssess.CanGrow = false;
			this.txtLandOvAssess.Height = 0.1875F;
			this.txtLandOvAssess.Left = 4.6875F;
			this.txtLandOvAssess.MultiLine = false;
			this.txtLandOvAssess.Name = "txtLandOvAssess";
			this.txtLandOvAssess.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLandOvAssess.Text = "Field5";
			this.txtLandOvAssess.Top = 8.09375F;
			this.txtLandOvAssess.Width = 1.5F;
			// 
			// txtLandOvAve
			// 
			this.txtLandOvAve.CanGrow = false;
			this.txtLandOvAve.Height = 0.1875F;
			this.txtLandOvAve.Left = 6.25F;
			this.txtLandOvAve.MultiLine = false;
			this.txtLandOvAve.Name = "txtLandOvAve";
			this.txtLandOvAve.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLandOvAve.Text = "Field5";
			this.txtLandOvAve.Top = 8.09375F;
			this.txtLandOvAve.Width = 1.125F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.19F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 0.125F;
			this.Label45.MultiLine = false;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label45.Text = "B  L  D  G    O  V  E  R  R  I  D  E  S";
			this.Label45.Top = 8.65625F;
			this.Label45.Width = 7.375F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 0F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 8.8125F;
			this.Line7.Width = 7.5F;
			this.Line7.X1 = 0F;
			this.Line7.X2 = 7.5F;
			this.Line7.Y1 = 8.8125F;
			this.Line7.Y2 = 8.8125F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1875F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 1.4375F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Tahoma\'";
			this.Label46.Text = "Land Override Total";
			this.Label46.Top = 8.09375F;
			this.Label46.Width = 1.8125F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.21875F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 1.4375F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-family: \'Tahoma\'";
			this.Label47.Text = "Bldg Override Total";
			this.Label47.Top = 9.0625F;
			this.Label47.Width = 1.8125F;
			// 
			// txtBldgOvCount
			// 
			this.txtBldgOvCount.CanGrow = false;
			this.txtBldgOvCount.Height = 0.19F;
			this.txtBldgOvCount.Left = 3.4375F;
			this.txtBldgOvCount.MultiLine = false;
			this.txtBldgOvCount.Name = "txtBldgOvCount";
			this.txtBldgOvCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBldgOvCount.Text = "Field5";
			this.txtBldgOvCount.Top = 9.0625F;
			this.txtBldgOvCount.Width = 0.875F;
			// 
			// txtBldgOvAssess
			// 
			this.txtBldgOvAssess.CanGrow = false;
			this.txtBldgOvAssess.Height = 0.19F;
			this.txtBldgOvAssess.Left = 4.6875F;
			this.txtBldgOvAssess.MultiLine = false;
			this.txtBldgOvAssess.Name = "txtBldgOvAssess";
			this.txtBldgOvAssess.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBldgOvAssess.Text = "Field6";
			this.txtBldgOvAssess.Top = 9.03125F;
			this.txtBldgOvAssess.Width = 1.5F;
			// 
			// txtBldgOvAve
			// 
			this.txtBldgOvAve.CanGrow = false;
			this.txtBldgOvAve.Height = 0.19F;
			this.txtBldgOvAve.Left = 6.25F;
			this.txtBldgOvAve.MultiLine = false;
			this.txtBldgOvAve.Name = "txtBldgOvAve";
			this.txtBldgOvAve.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBldgOvAve.Text = "Field7";
			this.txtBldgOvAve.Top = 9.03125F;
			this.txtBldgOvAve.Width = 1.125F;
			// 
			// SubReport10
			// 
			this.SubReport10.CloseBorder = false;
			this.SubReport10.Height = 0.125F;
			this.SubReport10.Left = 0F;
			this.SubReport10.Name = "SubReport10";
			this.SubReport10.Report = null;
			this.SubReport10.Top = 8.84375F;
			this.SubReport10.Width = 7.5625F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1875F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 1.625F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-family: \'Tahoma\'";
			this.Label48.Text = "EXEMPTION TOTAL";
			this.Label48.Top = 6.90625F;
			this.Label48.Width = 1.4375F;
			// 
			// txtEACount
			// 
			this.txtEACount.CanGrow = false;
			this.txtEACount.Height = 0.1875F;
			this.txtEACount.Left = 3.375F;
			this.txtEACount.MultiLine = false;
			this.txtEACount.Name = "txtEACount";
			this.txtEACount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtEACount.Text = "Field5";
			this.txtEACount.Top = 6.90625F;
			this.txtEACount.Width = 0.9375F;
			// 
			// txtEAAssess
			// 
			this.txtEAAssess.CanGrow = false;
			this.txtEAAssess.Height = 0.1875F;
			this.txtEAAssess.Left = 4.6875F;
			this.txtEAAssess.MultiLine = false;
			this.txtEAAssess.Name = "txtEAAssess";
			this.txtEAAssess.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtEAAssess.Text = "Field6";
			this.txtEAAssess.Top = 6.90625F;
			this.txtEAAssess.Width = 1.5F;
			// 
			// txtEAAve
			// 
			this.txtEAAve.CanGrow = false;
			this.txtEAAve.Height = 0.1875F;
			this.txtEAAve.Left = 6.25F;
			this.txtEAAve.MultiLine = false;
			this.txtEAAve.Name = "txtEAAve";
			this.txtEAAve.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtEAAve.Text = "Field7";
			this.txtEAAve.Top = 6.90625F;
			this.txtEAAve.Width = 1.125F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.Field5,
				this.Field6
			});
			this.PageHeader.Height = 0.3645833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// Label2
			// 
			this.Label2.Height = 0.21875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.6875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.Label2.Text = "Real Estate Assessment Analysis";
			this.Label2.Top = 0F;
			this.Label2.Width = 3.875F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.375F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.19F;
			this.Field5.Left = 7.125F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.Field5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.Field5.Text = null;
			this.Field5.Top = 0.1875F;
			this.Field5.Width = 0.3125F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.19F;
			this.Field6.Left = 6.5F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field6.Text = "Page";
			this.Field6.Top = 0.1875F;
			this.Field6.Width = 0.625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptAssAnalysis
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtoutcount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtoutassess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAverage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgAve)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCommCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCommAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCommAve)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandOvCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandOvAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandOvAve)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgOvCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgOvAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgOvAve)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEACount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEAAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEAAve)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssess1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreage1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtoutcount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtoutassess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAverage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgAve;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCommCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCommAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCommAve;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandOvCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandOvAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandOvAve;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgOvCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgOvAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgOvAve;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEACount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEAAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEAAve;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
