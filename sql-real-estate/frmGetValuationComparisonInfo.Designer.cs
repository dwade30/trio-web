﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetValuationComparisonInfo.
	/// </summary>
	partial class frmGetValuationComparisonInfo : BaseForm
	{
		public fecherFoundation.FCComboBox cmbCriteria;
		public fecherFoundation.FCLabel lblCriteria;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbtFrom;
		public fecherFoundation.FCLabel lbltFrom;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtRange;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkRecalc;
		public System.Collections.Generic.List<fecherFoundation.FCComboBox> cmbYear;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCCheckBox chkShowUnexplained;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtDepreciation;
		public fecherFoundation.FCTextBox txtDepreciationMH;
		public fecherFoundation.FCTextBox txtDepreciationComm;
		public fecherFoundation.FCLabel Label2_2;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCFrame framRange;
		public fecherFoundation.FCTextBox txtRange_0;
		public fecherFoundation.FCTextBox txtRange_1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkRecalc_1;
		public fecherFoundation.FCCheckBox chkRecalc_0;
		public fecherFoundation.FCComboBox cmbYear_1;
		public fecherFoundation.FCComboBox cmbYear_0;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetValuationComparisonInfo));
			this.cmbCriteria = new fecherFoundation.FCComboBox();
			this.lblCriteria = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbtFrom = new fecherFoundation.FCComboBox();
			this.lbltFrom = new fecherFoundation.FCLabel();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.chkShowUnexplained = new fecherFoundation.FCCheckBox();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtDepreciation = new fecherFoundation.FCTextBox();
			this.txtDepreciationMH = new fecherFoundation.FCTextBox();
			this.txtDepreciationComm = new fecherFoundation.FCTextBox();
			this.Label2_2 = new fecherFoundation.FCLabel();
			this.Label2_1 = new fecherFoundation.FCLabel();
			this.Label2_0 = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.framRange = new fecherFoundation.FCFrame();
			this.txtRange_0 = new fecherFoundation.FCTextBox();
			this.txtRange_1 = new fecherFoundation.FCTextBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkRecalc_1 = new fecherFoundation.FCCheckBox();
			this.chkRecalc_0 = new fecherFoundation.FCCheckBox();
			this.cmbYear_1 = new fecherFoundation.FCComboBox();
			this.cmbYear_0 = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdImport = new Wisej.Web.Button();
			this.cmdSave = new Wisej.Web.Button();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowUnexplained)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framRange)).BeginInit();
			this.framRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkRecalc_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRecalc_0)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 446);
			this.BottomPanel.Size = new System.Drawing.Size(668, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(668, 386);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdImport);
			this.TopPanel.Size = new System.Drawing.Size(668, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdImport, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(270, 30);
			this.HeaderText.Text = "New Valuation Analysis";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbCriteria
			// 
			this.cmbCriteria.AutoSize = false;
			this.cmbCriteria.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCriteria.FormattingEnabled = true;
			this.cmbCriteria.Items.AddRange(new object[] {
				"Auto determine eligibility",
				"Any Increased Valuation"
			});
			this.cmbCriteria.Location = new System.Drawing.Point(120, 30);
			this.cmbCriteria.Name = "cmbCriteria";
			this.cmbCriteria.Size = new System.Drawing.Size(241, 40);
			this.cmbCriteria.TabIndex = 15;
			this.cmbCriteria.Text = "Auto determine eligibility";
			this.ToolTip1.SetToolTip(this.cmbCriteria, null);
			this.cmbCriteria.SelectedIndexChanged += new System.EventHandler(this.optCriteria_CheckedChanged);
			// 
			// lblCriteria
			// 
			this.lblCriteria.AutoSize = true;
			this.lblCriteria.Location = new System.Drawing.Point(20, 44);
			this.lblCriteria.Name = "lblCriteria";
			this.lblCriteria.Size = new System.Drawing.Size(65, 15);
			this.lblCriteria.TabIndex = 16;
			this.lblCriteria.Text = "CRITERIA";
			this.ToolTip1.SetToolTip(this.lblCriteria, null);
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"Account",
				"Map Lot",
				"Location"
			});
			this.cmbRange.Location = new System.Drawing.Point(94, 160);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(110, 40);
			this.cmbRange.TabIndex = 8;
			this.cmbRange.Text = "Account";
			this.ToolTip1.SetToolTip(this.cmbRange, null);
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(10, 174);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(51, 15);
			this.lblRange.TabIndex = 9;
			this.lblRange.Text = "RANGE";
			this.ToolTip1.SetToolTip(this.lblRange, null);
			// 
			// cmbtFrom
			// 
			this.cmbtFrom.AutoSize = false;
			this.cmbtFrom.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbtFrom.FormattingEnabled = true;
			this.cmbtFrom.Items.AddRange(new object[] {
				"All",
				"Range"
			});
			this.cmbtFrom.Location = new System.Drawing.Point(94, 30);
			this.cmbtFrom.Name = "cmbtFrom";
			this.cmbtFrom.Size = new System.Drawing.Size(120, 40);
			this.cmbtFrom.TabIndex = 27;
			this.cmbtFrom.Text = "All";
			this.ToolTip1.SetToolTip(this.cmbtFrom, null);
			this.cmbtFrom.SelectedIndexChanged += new System.EventHandler(this.OptFrom_CheckedChanged);
			// 
			// lbltFrom
			// 
			this.lbltFrom.AutoSize = true;
			this.lbltFrom.Location = new System.Drawing.Point(20, 44);
			this.lbltFrom.Name = "lbltFrom";
			this.lbltFrom.Size = new System.Drawing.Size(44, 15);
			this.lbltFrom.TabIndex = 28;
			this.lbltFrom.Text = "FROM";
			this.ToolTip1.SetToolTip(this.lbltFrom, null);
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.chkShowUnexplained);
			this.Frame4.Controls.Add(this.cmbCriteria);
			this.Frame4.Controls.Add(this.lblCriteria);
			this.Frame4.Location = new System.Drawing.Point(30, 507);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(381, 137);
			this.Frame4.TabIndex = 25;
			this.Frame4.Text = "Accounts To Include";
			this.ToolTip1.SetToolTip(this.Frame4, null);
			// 
			// chkShowUnexplained
			// 
			this.chkShowUnexplained.Location = new System.Drawing.Point(20, 90);
			this.chkShowUnexplained.Name = "chkShowUnexplained";
			this.chkShowUnexplained.Size = new System.Drawing.Size(341, 27);
			this.chkShowUnexplained.TabIndex = 14;
			this.chkShowUnexplained.Text = "Show accounts with unexplained increases";
			this.ToolTip1.SetToolTip(this.chkShowUnexplained, "If this option is checked, then accounts with increases to do any changes will be" + " included.  If unchecked only accounts with certain types of changes will be inc" + "luded ");
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.txtDepreciation);
			this.Frame3.Controls.Add(this.txtDepreciationMH);
			this.Frame3.Controls.Add(this.txtDepreciationComm);
			this.Frame3.Controls.Add(this.Label2_2);
			this.Frame3.Controls.Add(this.Label2_1);
			this.Frame3.Controls.Add(this.Label2_0);
			this.Frame3.Location = new System.Drawing.Point(30, 664);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(467, 90);
			this.Frame3.TabIndex = 21;
			this.Frame3.Text = "Depreciation Years";
			this.ToolTip1.SetToolTip(this.Frame3, null);
			// 
			// txtDepreciation
			// 
			this.txtDepreciation.AutoSize = false;
			this.txtDepreciation.BackColor = System.Drawing.SystemColors.Window;
			this.txtDepreciation.LinkItem = null;
			this.txtDepreciation.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDepreciation.LinkTopic = null;
			this.txtDepreciation.Location = new System.Drawing.Point(90, 30);
			this.txtDepreciation.Name = "txtDepreciation";
			this.txtDepreciation.Size = new System.Drawing.Size(58, 40);
			this.txtDepreciation.TabIndex = 15;
			this.txtDepreciation.Text = "0";
			this.ToolTip1.SetToolTip(this.txtDepreciation, null);
			// 
			// txtDepreciationMH
			// 
			this.txtDepreciationMH.AutoSize = false;
			this.txtDepreciationMH.BackColor = System.Drawing.SystemColors.Window;
			this.txtDepreciationMH.LinkItem = null;
			this.txtDepreciationMH.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDepreciationMH.LinkTopic = null;
			this.txtDepreciationMH.Location = new System.Drawing.Point(229, 30);
			this.txtDepreciationMH.Name = "txtDepreciationMH";
			this.txtDepreciationMH.Size = new System.Drawing.Size(58, 40);
			this.txtDepreciationMH.TabIndex = 16;
			this.txtDepreciationMH.Text = "0";
			this.ToolTip1.SetToolTip(this.txtDepreciationMH, null);
			// 
			// txtDepreciationComm
			// 
			this.txtDepreciationComm.AutoSize = false;
			this.txtDepreciationComm.BackColor = System.Drawing.SystemColors.Window;
			this.txtDepreciationComm.LinkItem = null;
			this.txtDepreciationComm.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDepreciationComm.LinkTopic = null;
			this.txtDepreciationComm.Location = new System.Drawing.Point(389, 30);
			this.txtDepreciationComm.Name = "txtDepreciationComm";
			this.txtDepreciationComm.Size = new System.Drawing.Size(58, 40);
			this.txtDepreciationComm.TabIndex = 17;
			this.txtDepreciationComm.Text = "0";
			this.ToolTip1.SetToolTip(this.txtDepreciationComm, null);
			// 
			// Label2_2
			// 
			this.Label2_2.Location = new System.Drawing.Point(310, 44);
			this.Label2_2.Name = "Label2_2";
			this.Label2_2.Size = new System.Drawing.Size(50, 17);
			this.Label2_2.TabIndex = 24;
			this.Label2_2.Text = "COMM";
			this.ToolTip1.SetToolTip(this.Label2_2, null);
			// 
			// Label2_1
			// 
			this.Label2_1.Location = new System.Drawing.Point(170, 44);
			this.Label2_1.Name = "Label2_1";
			this.Label2_1.Size = new System.Drawing.Size(25, 17);
			this.Label2_1.TabIndex = 23;
			this.Label2_1.Text = "MH";
			this.ToolTip1.SetToolTip(this.Label2_1, null);
			// 
			// Label2_0
			// 
			this.Label2_0.Location = new System.Drawing.Point(20, 44);
			this.Label2_0.Name = "Label2_0";
			this.Label2_0.Size = new System.Drawing.Size(25, 17);
			this.Label2_0.TabIndex = 22;
			this.Label2_0.Text = "R/E";
			this.ToolTip1.SetToolTip(this.Label2_0, null);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.framRange);
			this.Frame2.Controls.Add(this.cmbtFrom);
			this.Frame2.Controls.Add(this.lbltFrom);
			this.Frame2.Location = new System.Drawing.Point(30, 187);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(246, 300);
			this.Frame2.TabIndex = 20;
			this.Frame2.Text = "Include";
			this.ToolTip1.SetToolTip(this.Frame2, null);
			// 
			// framRange
			// 
			this.framRange.AppearanceKey = "groupBoxNoBorders";
			this.framRange.Controls.Add(this.txtRange_0);
			this.framRange.Controls.Add(this.cmbRange);
			this.framRange.Controls.Add(this.lblRange);
			this.framRange.Controls.Add(this.txtRange_1);
			this.framRange.Controls.Add(this.Label3);
			this.framRange.Location = new System.Drawing.Point(10, 80);
			this.framRange.Name = "framRange";
			this.framRange.Size = new System.Drawing.Size(226, 210);
			this.framRange.TabIndex = 26;
			this.ToolTip1.SetToolTip(this.framRange, null);
			this.framRange.Visible = false;
			// 
			// txtRange_0
			// 
			this.txtRange_0.AutoSize = false;
			this.txtRange_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtRange_0.LinkItem = null;
			this.txtRange_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRange_0.LinkTopic = null;
			this.txtRange_0.Location = new System.Drawing.Point(10, 10);
			this.txtRange_0.Name = "txtRange_0";
			this.txtRange_0.Size = new System.Drawing.Size(194, 40);
			this.txtRange_0.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtRange_0, null);
			// 
			// txtRange_1
			// 
			this.txtRange_1.AutoSize = false;
			this.txtRange_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtRange_1.LinkItem = null;
			this.txtRange_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRange_1.LinkTopic = null;
			this.txtRange_1.Location = new System.Drawing.Point(10, 100);
			this.txtRange_1.Name = "txtRange_1";
			this.txtRange_1.Size = new System.Drawing.Size(196, 40);
			this.txtRange_1.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.txtRange_1, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(99, 70);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(25, 17);
			this.Label3.TabIndex = 27;
			this.Label3.Text = "TO";
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkRecalc_1);
			this.Frame1.Controls.Add(this.chkRecalc_0);
			this.Frame1.Controls.Add(this.cmbYear_1);
			this.Frame1.Controls.Add(this.cmbYear_0);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(316, 137);
			this.Frame1.TabIndex = 18;
			this.Frame1.Text = "Years To Compare";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// chkRecalc_1
			// 
			this.chkRecalc_1.Location = new System.Drawing.Point(183, 90);
			this.chkRecalc_1.Name = "chkRecalc_1";
			this.chkRecalc_1.Size = new System.Drawing.Size(113, 27);
			this.chkRecalc_1.TabIndex = 3;
			this.chkRecalc_1.Text = "Recalculate";
			this.ToolTip1.SetToolTip(this.chkRecalc_1, null);
			// 
			// chkRecalc_0
			// 
			this.chkRecalc_0.Location = new System.Drawing.Point(20, 90);
			this.chkRecalc_0.Name = "chkRecalc_0";
			this.chkRecalc_0.Size = new System.Drawing.Size(113, 27);
			this.chkRecalc_0.TabIndex = 2;
			this.chkRecalc_0.Text = "Recalculate";
			this.ToolTip1.SetToolTip(this.chkRecalc_0, "Recalculate with the current cost files and the specifed depreciation years");
			// 
			// cmbYear_1
			// 
			this.cmbYear_1.AutoSize = false;
			this.cmbYear_1.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear_1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear_1.FormattingEnabled = true;
			this.cmbYear_1.Location = new System.Drawing.Point(183, 30);
			this.cmbYear_1.Name = "cmbYear_1";
			this.cmbYear_1.Size = new System.Drawing.Size(100, 40);
			this.cmbYear_1.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.cmbYear_1, null);
			// 
			// cmbYear_0
			// 
			this.cmbYear_0.AutoSize = false;
			this.cmbYear_0.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear_0.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear_0.FormattingEnabled = true;
			this.cmbYear_0.Location = new System.Drawing.Point(20, 30);
			this.cmbYear_0.Name = "cmbYear_0";
			this.cmbYear_0.Size = new System.Drawing.Size(100, 40);
			this.cmbYear_0.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.cmbYear_0, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(141, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(21, 17);
			this.Label1.TabIndex = 19;
			this.Label1.Text = "TO";
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// cmdImport
			// 
			this.cmdImport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdImport.AppearanceKey = "toolbarButton";
			this.cmdImport.Location = new System.Drawing.Point(384, 29);
			this.cmdImport.Name = "cmdImport";
			this.cmdImport.Size = new System.Drawing.Size(256, 24);
			this.cmdImport.TabIndex = 1;
			this.cmdImport.Text = "Import Commitment Data from Archive";
			this.ToolTip1.SetToolTip(this.cmdImport, null);
			this.cmdImport.Click += new System.EventHandler(this.mnuImport_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(250, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(180, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save & Continue";
			this.ToolTip1.SetToolTip(this.cmdSave, null);
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmGetValuationComparisonInfo
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(668, 554);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmGetValuationComparisonInfo";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "New Valuation Analysis";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmGetValuationComparisonInfo_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetValuationComparisonInfo_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.Frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowUnexplained)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framRange)).EndInit();
			this.framRange.ResumeLayout(false);
			this.framRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkRecalc_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRecalc_0)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdSave;
		private Button cmdImport;
	}
}
