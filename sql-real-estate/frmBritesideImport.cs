﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmBritesideImport.
	/// </summary>
	public partial class frmBritesideImport : BaseForm
	{
		public frmBritesideImport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBritesideImport InstancePtr
		{
			get
			{
				return (frmBritesideImport)Sys.GetInstance(typeof(frmBritesideImport));
			}
		}

		protected frmBritesideImport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private string strSourceDir = string.Empty;
		const int CNSTGRIDCOLACCOUNT = 0;
		const int CNSTGRIDCOLOWNER = 1;
		const int CNSTGRIDCOLMAPLOT = 2;
		const int CNSTGRIDCOLLAND = 3;
		const int CNSTGRIDCOLBLDG = 4;
		const int CNSTGRIDCOLEXEMPTION = 5;
		const int CNSTGRIDCOLNEW = 6;
		private bool boolLoaded;

		private void frmBritesideImport_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
				LoadData();
				//Application.DoEvents();
				this.Unload();
			}
		}

		private void frmBritesideImport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBritesideImport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBritesideImport properties;
			//frmBritesideImport.FillStyle	= 0;
			//frmBritesideImport.ScaleWidth	= 3885;
			//frmBritesideImport.ScaleHeight	= 2325;
			//frmBritesideImport.LinkTopic	= "Form2";
			//frmBritesideImport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		public void Init()
		{
			string strPath;
			strPath = frmGetDirectory.InstancePtr.Init("Choose directory the source database files are in", "", false, "Source Directory", true);
			if (strPath == "CANCEL")
				return;
			strSourceDir = strPath;
			boolLoaded = false;
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void LoadData()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngNewAcct = 0;
				int x;
				double dblSoft = 0;
				double dblMixed = 0;
				double dblHard = 0;
				// vbPorter upgrade warning: lngSoft As int	OnWrite(short, double)
				int lngSoft = 0;
				// vbPorter upgrade warning: lngMixed As int	OnWrite(short, double)
				int lngMixed = 0;
				// vbPorter upgrade warning: lngHard As int	OnWrite(short, double)
				int lngHard = 0;
				double dblOther = 0;
				int lngOther = 0;
				double dblAcres = 0;
				int lngLand = 0;
				string[] strAry = null;
				int lngAcct = 0;
				bool boolHasHomestead = false;
				int lngHomesteadAmount = 0;
				clsDRWrapper rsExemptions = new clsDRWrapper();
				string strTemp = "";
				int lngRow;
				string strConStr;
				cParty tParty = new cParty();
				cPartyAddress tPartyAddr;
				strConStr = rsLoad.MakeODBCConnectionStringForDBase(strSourceDir);
				rsLoad.AddConnection(strTemp, "Briteside", FCConvert.ToInt32(dbConnectionTypes.ODBC));
				rsExemptions.OpenRecordset("select * from exemptcode order by description", modGlobalVariables.strREDatabase);
				// rsLoad.ConnectionStringOverride = "dBase 5.0;" & strSourceDir
				// rsLoad.Path = strSourceDir
				rsLoad.OpenRecordset("select * from trio order by owner", "Briteside");
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [trioacct] not found!! (maybe it is an alias?)
					rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("trioacct"))) + " and rsaccount > 0", modGlobalVariables.strREDatabase);
					// If Val(rsLoad.Fields("buildval")) <> 0 Or Val(rsLoad.Fields("landval")) <> 0 Or Val(rsLoad.Fields("totexempt")) <> 0 Then
					// skip bogus accounts
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNEW, FCConvert.ToString(false));
					}
					else
					{
						rsSave.AddNew();
						rsSave.Set_Fields("datecreated", DateTime.Today);
						// TODO Get_Fields: Field [trioacct] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsLoad.Get_Fields("trioacct")) > 0)
						{
							// TODO Get_Fields: Field [trioacct] not found!! (maybe it is an alias?)
							lngNewAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("trioacct"))));
						}
						else
						{
							lngNewAcct = modGlobalRoutines.GetNextAccountToCreate();
						}
						rsSave.Set_Fields("rsaccount", lngNewAcct);
						rsSave.Set_Fields("rscard", 1);
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNEW, FCConvert.ToString(true));
					}
					lngAcct = FCConvert.ToInt32(rsSave.Get_Fields_Int32("rsaccount"));
					Label1.Text = "Importing Account " + FCConvert.ToString(lngAcct);
					Label1.Refresh();
					//Application.DoEvents();
					rsSave.Set_Fields("rsname", rsLoad.Get_Fields_String("owner"));
					// TODO Get_Fields: Field [owner2] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("rssecowner", rsLoad.Get_Fields("owner2"));
					// TODO Get_Fields: Field [prevowner] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("rspreviousmaster", rsLoad.Get_Fields("prevowner"));
					// TODO Get_Fields: Field [own1add1] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("rsaddr1", rsLoad.Get_Fields("own1add1"));
					// TODO Get_Fields: Field [own1add2] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("rsaddr2", rsLoad.Get_Fields("own1add2"));
					// TODO Get_Fields: Field [own1city] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("rsaddr3", rsLoad.Get_Fields("own1city"));
					// TODO Get_Fields: Field [own1state] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("rsstate", rsLoad.Get_Fields("own1state"));
					// TODO Get_Fields: Field [own1zip5] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("rszip", rsLoad.Get_Fields("own1zip5"));
					// TODO Get_Fields: Field [own1zip4] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("rszip4", Strings.Replace(FCConvert.ToString(rsLoad.Get_Fields("own1zip4")), "-", "", 1, -1, CompareConstants.vbTextCompare));
					rsSave.Set_Fields("rsref2", rsLoad.Get_Fields_String("ref2"));
					// TODO Get_Fields: Field [own1email] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("email", rsLoad.Get_Fields("own1email"));
					// TODO Get_Fields: Field [totacres] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("piacres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totacres"))));
					// TODO Get_Fields: Field [totacres] not found!! (maybe it is an alias?)
					dblAcres = Conversion.Val(rsLoad.Get_Fields("totacres"));
					// TODO Get_Fields: Field [entrydate] not found!! (maybe it is an alias?)
					if (Information.IsDate(rsLoad.Get_Fields("entrydate")))
					{
						// TODO Get_Fields: Field [entrydate] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("dateinspected", rsLoad.Get_Fields("entrydate"));
					}
					// TODO Get_Fields: Field [landval] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("lastlandval", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("landval"))));
					// TODO Get_Fields: Field [landval] not found!! (maybe it is an alias?)
					lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("landval"))));
					// TODO Get_Fields: Field [buildval] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("lastbldgval", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("buildval"))));
					// TODO Get_Fields: Field [totexempt] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("rlexemption", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("totexempt"))));
					rsSave.Set_Fields("exemptval1", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("exempt1"))));
					rsSave.Set_Fields("exemptval2", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("exempt2"))));
					rsSave.Set_Fields("exemptval3", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("exempt3"))));
					rsSave.Set_Fields("exemptpct1", 100);
					rsSave.Set_Fields("exemptpct2", 100);
					rsSave.Set_Fields("exemptpct3", 100);
					if (FCConvert.ToString(rsLoad.Get_Fields_String("maplot")).Length > 3)
					{
						rsSave.Set_Fields("rsmaplot", Strings.Left(FCConvert.ToString(rsLoad.Get_Fields_String("maplot")), 3) + "-" + Strings.Mid(FCConvert.ToString(rsLoad.Get_Fields_String("maplot")), 4));
					}
					else
					{
						rsSave.Set_Fields("rsmaplot", rsLoad.Get_Fields_String("maplot"));
					}
					rsSave.Set_Fields("pisaleprice", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("saleprice"))));
					if (Information.IsDate(rsLoad.Get_Fields("saledate")))
					{
						rsSave.Set_Fields("saledate", rsLoad.Get_Fields_DateTime("saledate"));
					}
					lngSoft = 0;
					lngMixed = 0;
					lngHard = 0;
					dblSoft = 0;
					dblHard = 0;
					dblMixed = 0;
					for (x = 1; x <= 3; x++)
					{
						// TODO Get_Fields: Field [tg] not found!! (maybe it is an alias?)
						if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("tg" + FCConvert.ToString(x) + "type")))) == "SOFTWOOD")
						{
							// TODO Get_Fields: Field [tg] not found!! (maybe it is an alias?)
							lngSoft += FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("tg" + FCConvert.ToString(x) + "value")));
							// TODO Get_Fields: Field [tg] not found!! (maybe it is an alias?)
							dblSoft += Conversion.Val(rsLoad.Get_Fields("tg" + FCConvert.ToString(x) + "acres"));
						}
						// TODO Get_Fields: Field [tg] not found!! (maybe it is an alias?)
						else if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("tg" + FCConvert.ToString(x) + "type")))) == "HARDWOOD")
						{
							// TODO Get_Fields: Field [tg] not found!! (maybe it is an alias?)
							lngHard += FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("tg" + FCConvert.ToString(x) + "value")));
							// TODO Get_Fields: Field [tg] not found!! (maybe it is an alias?)
							dblHard += Conversion.Val(rsLoad.Get_Fields("tg" + FCConvert.ToString(x) + "acres"));
						}
							// TODO Get_Fields: Field [tg] not found!! (maybe it is an alias?)
							else if (Strings.UCase(Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("tg" + FCConvert.ToString(x) + "type")))) == "MIXED WOOD")
						{
							// TODO Get_Fields: Field [tg] not found!! (maybe it is an alias?)
							lngMixed += FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("tg" + FCConvert.ToString(x) + "value")));
							// TODO Get_Fields: Field [tg] not found!! (maybe it is an alias?)
							dblMixed += Conversion.Val(rsLoad.Get_Fields("tg" + FCConvert.ToString(x) + "acres"));
						}
					}
					// x
					dblOther = dblAcres - dblSoft - dblHard - dblMixed;
					if (dblOther < 0)
						dblOther = 0;
					lngOther = lngLand - lngSoft - lngHard - lngMixed;
					if (lngOther < 0)
						lngOther = 0;
					rsSave.Set_Fields("rssoft", dblSoft);
					rsSave.Set_Fields("rssoftvalue", lngSoft);
					rsSave.Set_Fields("rshard", dblHard);
					rsSave.Set_Fields("rshardvalue", lngHard);
					rsSave.Set_Fields("rsmixed", dblMixed);
					rsSave.Set_Fields("rsmixedvalue", lngMixed);
					rsSave.Set_Fields("rsother", dblOther);
					rsSave.Set_Fields("rsothervalue", lngOther);
					// TODO Get_Fields: Field [buildcode] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("ribldgcode", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("buildcode"))));
					// TODO Get_Fields: Field [propcode] not found!! (maybe it is an alias?)
					rsSave.Set_Fields("rilandcode", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("propcode"))));
					rsSave.Set_Fields("hashomestead", false);
					boolHasHomestead = false;
					lngHomesteadAmount = 0;
					for (x = 1; x <= 3; x++)
					{
						strTemp = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_Int32("exempt" + FCConvert.ToString(x) + "typ")));
						if (strTemp != string.Empty && Conversion.Val(rsLoad.Get_Fields_Int32("exempt" + FCConvert.ToString(x))) > 0)
						{
							// If rsExemptions.FindFirstRecord("description", strTemp) Then
							if (rsExemptions.FindFirst("description = " + strTemp))
							{
								// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
								rsSave.Set_Fields("riexemptcd" + x, rsExemptions.Get_Fields("code"));
								// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
								if (FCConvert.ToInt32(rsExemptions.Get_Fields("category")) == modcalcexemptions.CNSTEXEMPTCATHOMESTEAD)
								{
									rsSave.Set_Fields("hashomestead", true);
									boolHasHomestead = true;
									lngHomesteadAmount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("exempt" + FCConvert.ToString(x)))));
								}
							}
						}
						else
						{
							rsSave.Set_Fields("riexemptcd" + x, "0");
						}
					}
					// x
					rsSave.Set_Fields("hashomestead", boolHasHomestead);
					rsSave.Set_Fields("homesteadvalue", lngHomesteadAmount);
					// TODO Get_Fields: Field [own1phone] not found!! (maybe it is an alias?)
					strTemp = FCConvert.ToString(rsLoad.Get_Fields("own1phone"));
					strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Trim(strTemp);
					if (strTemp != string.Empty)
					{
						strTemp = Strings.Right("0000000000" + strTemp, 10);
						rsSave.Set_Fields("rstelephone", strTemp);
					}
					Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(lngAcct));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLBLDG, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Int32("lastbldgval"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLEXEMPTION, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Int32("rlexemption"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLLAND, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Int32("lastlandval"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(rsSave.Get_Fields_String("rsmaplot")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNER, FCConvert.ToString(rsSave.Get_Fields_String("rsname")));
					rsSave.Update();
					if (strTemp != string.Empty)
					{
						rsSave.Execute("delete from phonenumbers where parentid = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
						rsSave.OpenRecordset("select * from phonenumbers where parentid = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
						rsSave.AddNew();
						rsSave.Set_Fields("phonecode", 0);
						rsSave.Set_Fields("parentid", lngAcct);
						rsSave.Set_Fields("phonenumber", strTemp);
						rsSave.Update();
					}
					if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("bookpage"))) != string.Empty)
					{
						strAry = Strings.Split(FCConvert.ToString(rsLoad.Get_Fields_String("bookpage")), "-", -1, CompareConstants.vbTextCompare);
						if (Information.UBound(strAry, 1) > 0)
						{
							rsSave.Execute("delete from bookpage where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
							rsSave.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
							rsSave.AddNew();
							rsSave.Set_Fields("account", lngAcct);
							rsSave.Set_Fields("card", 1);
							rsSave.Set_Fields("current", true);
							rsSave.Set_Fields("line", 1);
							rsSave.Set_Fields("saledate", 0);
							rsSave.Set_Fields("book", strAry[0]);
							rsSave.Set_Fields("page", strAry[1]);
							rsSave.Update();
						}
					}
					// End If
					rsLoad.MoveNext();
				}
				rptBritesideImport.InstancePtr.Init(this.Modal);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void cmdProcess_Click(object sender, EventArgs e)
		{
		}
	}
}
