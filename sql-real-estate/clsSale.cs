﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class clsSale
	{
		//=========================================================
		// vbPorter upgrade warning: ClientID As int	OnRead(string)
		public int ClientID;
		public DateTime SaleDate;
		public double Price;
		public string OriginalOwner = "";
		public int LandValue;
		public int BuildingValue;
	}
}
