﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmNoPictures.
	/// </summary>
	partial class frmNoPictures : BaseForm
	{
		public fecherFoundation.FCTextBox txtPrefix;
		public fecherFoundation.FCButton cmdNo;
		public fecherFoundation.FCButton cmdYes;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNoPictures));
			this.txtPrefix = new fecherFoundation.FCTextBox();
			this.cmdNo = new fecherFoundation.FCButton();
			this.cmdYes = new fecherFoundation.FCButton();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdYes)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 232);
			this.BottomPanel.Size = new System.Drawing.Size(594, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtPrefix);
			this.ClientArea.Controls.Add(this.cmdNo);
			this.ClientArea.Controls.Add(this.cmdYes);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(594, 172);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(594, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(139, 30);
			this.HeaderText.Text = "No Pictures";
			// 
			// txtPrefix
			// 
			this.txtPrefix.AutoSize = false;
			this.txtPrefix.BackColor = System.Drawing.SystemColors.Window;
			this.txtPrefix.LinkItem = null;
			this.txtPrefix.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPrefix.LinkTopic = null;
			this.txtPrefix.Location = new System.Drawing.Point(317, 60);
			this.txtPrefix.Name = "txtPrefix";
			this.txtPrefix.Size = new System.Drawing.Size(252, 40);
			this.txtPrefix.TabIndex = 2;
			// 
			// cmdNo
			// 
			this.cmdNo.AppearanceKey = "actionButton";
			this.cmdNo.ForeColor = System.Drawing.Color.White;
			this.cmdNo.Location = new System.Drawing.Point(111, 120);
			this.cmdNo.Name = "cmdNo";
			this.cmdNo.Size = new System.Drawing.Size(66, 40);
			this.cmdNo.TabIndex = 4;
			this.cmdNo.Text = "No";
			this.cmdNo.Click += new System.EventHandler(this.cmdNo_Click);
			// 
			// cmdYes
			// 
			this.cmdYes.AppearanceKey = "actionButton";
			this.cmdYes.ForeColor = System.Drawing.Color.White;
			this.cmdYes.Location = new System.Drawing.Point(30, 120);
			this.cmdYes.Name = "cmdYes";
			this.cmdYes.Size = new System.Drawing.Size(72, 40);
			this.cmdYes.TabIndex = 3;
			this.cmdYes.Text = "Yes";
			this.cmdYes.Click += new System.EventHandler(this.cmdYes_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 74);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(240, 17);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "FILENAME OR FILE PRE-FIX  (OPTIONAL)";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(437, 20);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "THERE IS NO PICTURE FOR THIS CARD.  WOULD YOU LIKE TO ADD ONE NOW?";
			// 
			// frmNoPictures
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(594, 340);
			this.FillColor = 0;
			this.Name = "frmNoPictures";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "No pictures";
			this.Load += new System.EventHandler(this.frmNoPictures_Load);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdYes)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
