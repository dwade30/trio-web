﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cLocationMultiplierController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cCommercialLocation GetCommercialLocation(int lngID)
		{
			cCommercialLocation GetCommercialLocation = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCommercialLocation commLoc = null;
				rsLoad.OpenRecordset("select * from CommercialLocations where id = " + FCConvert.ToString(lngID), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					commLoc = new cCommercialLocation();
					// TODO Get_Fields: Field [ClassCategory] not found!! (maybe it is an alias?)
					commLoc = rsLoad.Get_Fields("ClassCategory");
					commLoc.Location = FCConvert.ToString(modGlobalVariables.Statics.rs.Get_Fields_String("Location"));
					commLoc.ID = FCConvert.ToInt32(modGlobalVariables.Statics.rs.Get_Fields_Int32("ID"));
					commLoc.State = FCConvert.ToString(modGlobalVariables.Statics.rs.Get_Fields_String("state"));
					commLoc.IsUpdated = false;
				}
				GetCommercialLocation = commLoc;
				return GetCommercialLocation;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialLocation;
		}

		public cGenericCollection GetCommercialLocations()
		{
			cGenericCollection GetCommercialLocations = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialLocation commLoc;
				rs.OpenRecordset("select * from CommercialLocations order by Location,state", "RealEstate");
				while (!rs.EndOfFile())
				{
					commLoc = new cCommercialLocation();
					commLoc.Location = FCConvert.ToString(rs.Get_Fields_String("Location"));
					commLoc.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					commLoc.State = FCConvert.ToString(rs.Get_Fields_String("state"));
					commLoc.IsUpdated = false;
					gColl.AddItem(commLoc);
					rs.MoveNext();
				}
				GetCommercialLocations = gColl;
				return GetCommercialLocations;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialLocations;
		}

		public cGenericCollection GetLocationMultipliersForLocation(int lngLocID)
		{
			cGenericCollection GetLocationMultipliersForLocation = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialLocationMultiplier locMult;
				rs.OpenRecordset("select * from CommercialLocationMultipliers where CommercialLocationID = " + FCConvert.ToString(lngLocID) + " order by classcategory", "RealEstate");
				while (!rs.EndOfFile())
				{
					locMult = new cCommercialLocationMultiplier();
					// TODO Get_Fields: Field [ClassCategory] not found!! (maybe it is an alias?)
					locMult.ClassCategory = FCConvert.ToInt32(rs.Get_Fields("ClassCategory"));
					locMult.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					locMult.Multiplier = rs.Get_Fields_Int32("Multiplier");
					// TODO Get_Fields: Field [CommercialLocationID] not found!! (maybe it is an alias?)
					locMult.CommercialLocationID = FCConvert.ToInt32(rs.Get_Fields("CommercialLocationID"));
					locMult.IsUpdated = false;
					gColl.AddItem(locMult);
					rs.MoveNext();
				}
				GetLocationMultipliersForLocation = gColl;
				return GetLocationMultipliersForLocation;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetLocationMultipliersForLocation;
		}

		public cCommercialLocationMultiplier GetLocationMultiplier(int lngID)
		{
			cCommercialLocationMultiplier GetLocationMultiplier = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCommercialLocationMultiplier locMult = null;
				rsLoad.OpenRecordset("select * from CommercialLocationMultipliers where id = " + FCConvert.ToString(lngID), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					locMult = new cCommercialLocationMultiplier();
					// TODO Get_Fields: Field [ClassCategory] not found!! (maybe it is an alias?)
					locMult.ClassCategory = FCConvert.ToInt32(rsLoad.Get_Fields("ClassCategory"));
					// TODO Get_Fields: Field [CommercialLocationID] not found!! (maybe it is an alias?)
					locMult.CommercialLocationID = FCConvert.ToInt32(rsLoad.Get_Fields("CommercialLocationID"));
					locMult.Multiplier = rsLoad.Get_Fields_Int32("Multiplier");
					locMult.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					locMult.IsUpdated = false;
				}
				GetLocationMultiplier = locMult;
				return GetLocationMultiplier;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetLocationMultiplier;
		}

		public void DeleteLocationMultiplier(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialLocationMultipliers where id = " + FCConvert.ToString(lngID), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveLocationMultiplier(ref cCommercialLocationMultiplier locMult)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (locMult.IsDeleted)
				{
					DeleteLocationMultiplier(locMult.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commerciallocationmultipliers where id = " + locMult.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (locMult.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Commercial location multiplier record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("CommercialLocationID", locMult.CommercialLocationID);
				rsSave.Set_Fields("Multiplier", locMult.Multiplier);
				rsSave.Set_Fields("ClassCategory", locMult.ClassCategory);
				rsSave.Update();
				locMult.ID = rsSave.Get_Fields_Int32("ID");
				locMult.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void DeleteCommercialLocation(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialLocations where id = " + FCConvert.ToString(lngID), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveCommercialLocation(ref cCommercialLocation commLoc)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (commLoc.IsDeleted)
				{
					DeleteCommercialLocation(commLoc.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commerciallocations where id = " + commLoc.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (commLoc.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Commercial location record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("Location", commLoc.Location);
				rsSave.Set_Fields("State", commLoc.State);
				rsSave.Update();
				commLoc.ID = rsSave.Get_Fields_Int32("ID");
				commLoc.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
