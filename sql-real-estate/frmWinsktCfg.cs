﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmWinsktCfg.
	/// </summary>
	public partial class frmWinsktCfg : BaseForm
	{
		public frmWinsktCfg()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmWinsktCfg InstancePtr
		{
			get
			{
				return (frmWinsktCfg)Sys.GetInstance(typeof(frmWinsktCfg));
			}
		}

		protected frmWinsktCfg _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLCODE = 1;
		const int CNSTGRIDCOLNAME = 2;
		const int CNSTGRIDCOLAREA = 4;
		const int CNSTGRIDCOLADDSUBSECAREA = 5;
		const int CNSTGRIDCOLLINEWIDTH = 6;
		const int CNSTGRIDCOLLINESTYLE = 7;
		const int CNSTGRIDCOLFILLSTYLE = 8;
		const int CNSTGRIDCOLAUTODIMENSION = 9;
		const int CNSTGRIDCOLAREAMODIFIED = 10;
		const int CNSTGRIDCOLADDSUBFIRSTAREA = 3;
		const int CNSTGRIDCOLMULTIPLIER = 11;
		const int CNSTMAXDESCLENGTH = 26;
		string strFileHeader;
		bool boolMovingRows;
		int lngFirstRowToMove;
		int lngLastRowToMove;

		private void frmWinsktCfg_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmWinsktCfg_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmWinsktCfg properties;
			//frmWinsktCfg.FillStyle	= 0;
			//frmWinsktCfg.ScaleWidth	= 9045;
			//frmWinsktCfg.ScaleHeight	= 7110;
			//frmWinsktCfg.LinkTopic	= "Form2";
			//frmWinsktCfg.LockControls	= true;
			//frmWinsktCfg.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ErrorHandler
				boolMovingRows = false;
				strFileHeader = "[Area Type]" + "\r\n" + "0=Misc. Area" + "\r\n" + "1=Living Area" + "\r\n" + "2=Garage Area" + "\r\n";
				strFileHeader += "3=Porch Area" + "\r\n" + "4=Patio/Deck Area" + "\r\n" + "5=Pool Area" + "\r\n" + "6=Storage Area" + "\r\n";
				strFileHeader += "7=Lot Area" + "\r\n" + "8=Building Area" + "\r\n" + "9=Rentable Area" + "\r\n" + "10=Parking Area" + "\r\n";
				strFileHeader += "11=Dwelling Area" + "\r\n" + "12=Basement Living" + "\r\n" + "13=Commercial Area" + "\r\n" + "14=Outbuilding Area" + "\r\n" + "15=Land Area" + "\r\n";
				strFileHeader += "\r\n" + "[Help]" + "\r\n";
				strFileHeader += "0=QuickStart Guide, Quick.hlp" + "\r\n";
				strFileHeader += "1=Winsketch User Manual, Manual.pdf" + "\r\n";
				strFileHeader += "2=Technical Support, Support.hlp" + "\r\n";
				strFileHeader += "3=Release Notes, readme.txt" + "\r\n";
				strFileHeader += "4=User License, license.txt" + "\r\n";
				strFileHeader += "\r\n" + "[OEMInfo]" + "\r\n";
				strFileHeader += "ProductName=WinSketch Pro v7.4 32bit" + "\r\n";
				strFileHeader += "\r\n" + "[Enablers]" + "\r\n";
				strFileHeader += "AreaCalc = 18435" + "\r\n";
				strFileHeader += "PocketTotal = 34545" + "\r\n";
				strFileHeader += "Eval = 15" + "\r\n";
				strFileHeader += "Multiplier=" + "\r\n";
				strFileHeader += "LinearFootage=" + "\r\n";
				strFileHeader += "\r\n";
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				SetupGrid();
				OpenCfgFiles();
				SetupGridPhrases();
				LoadPhrasesFile();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Form_Load line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmWinsktCfg_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridPhrases();
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						mnuAddRow_Click();
						break;
					}
			}
			//end switch
		}

		private void Grid_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int lngRow;
			int lngMouseRow;
			string strTemp = "";
			int lngNumRows;
			lngMouseRow = Grid.MouseRow;
			if (lngMouseRow < 1)
			{
				boolMovingRows = false;
				lngFirstRowToMove = 0;
				lngLastRowToMove = 0;
			}
			if (e.Button == MouseButtons.Right && boolMovingRows)
			{
				boolMovingRows = false;
				lngNumRows = lngLastRowToMove - lngFirstRowToMove + 1;
				if (lngFirstRowToMove > 0)
				{
					strTemp = FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpText, lngFirstRowToMove, 0, lngLastRowToMove, Grid.Cols - 1));
					for (lngRow = lngLastRowToMove; lngRow >= lngFirstRowToMove; lngRow--)
					{
						Grid.RemoveItem(lngRow);
					}
					// lngRow
					for (lngRow = lngFirstRowToMove; lngRow <= lngLastRowToMove; lngRow++)
					{
						Grid.AddItem("", lngMouseRow);
					}
					// lngRow
					Grid.Cell(FCGrid.CellPropertySettings.flexcpText, lngMouseRow, 0, lngMouseRow + lngNumRows - 1, Grid.Cols - 1, strTemp);
				}
				lngFirstRowToMove = 0;
				lngLastRowToMove = 0;
			}
			else
			{
				boolMovingRows = false;
				lngFirstRowToMove = 0;
				lngLastRowToMove = 0;
			}
		}

		private void GridPhrases_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						mnuAddRow_Click();
						break;
					}
			}
			//end switch
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			if (framPhrases.Visible)
			{
				GridPhrases.Rows += 1;
				GridPhrases.TopRow = GridPhrases.Rows - 1;
			}
			else
			{
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLADDSUBSECAREA, FCConvert.ToString(1));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA, "Misc. Area");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLAREAMODIFIED, " ");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTODIMENSION, "True");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLFILLSTYLE, "-4");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLLINESTYLE, "722");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLLINEWIDTH, "1");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLMULTIPLIER, "1.00");
				Grid.TopRow = lngRow;
			}
		}

		public void mnuAddRow_Click()
		{
			mnuAddRow_Click(cmdAddRow, new System.EventArgs());
		}

		private void mnuCopyLoad_Click(object sender, System.EventArgs e)
		{
			// copies the files and then loads them
			//FileSystemObject fso = new FileSystemObject();
			string strSrcDir = "";
			string strDestdir = "";
			if (framPhrases.Visible)
			{
				if (MessageBox.Show("Warning: This will overwrite your WinSketch phrase file with the one on the server." + "\r\n" + "Are you sure you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				{
					return;
				}
				strSrcDir = Directory.GetDirectoryRoot(Application.MapPath("\\"));
				strSrcDir += "\\triodata\\triomast\\";
				strDestdir = FCFileSystem.Statics.UserDataFolder;
				if (Strings.Right(strDestdir, 1) != "\\")
				{
					strDestdir += "\\";
				}
				if (File.Exists(strSrcDir + "Phrases"))
				{
					File.Copy(strSrcDir + "Phrases", strDestdir + "Phrases", true);
				}
				else
				{
					MessageBox.Show("The phrase file was not on the server", "File not found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				SetupGridPhrases();
				LoadPhrasesFile();
			}
			else
			{
				if (MessageBox.Show("Warning: This will overwrite your WinSketch config files with those on the server." + "\r\n" + "Are you sure you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				{
					return;
				}
				strSrcDir = Directory.GetDirectoryRoot(Application.MapPath("\\"));
				strSrcDir += "\\triodata\\triomast\\";
				strDestdir = FCFileSystem.Statics.UserDataFolder;
				if (Strings.Right(strDestdir, 1) != "\\")
				{
					strDestdir += "\\";
				}
				if (File.Exists(strSrcDir + "Winskt.cfg"))
				{
					File.Copy(strSrcDir + "Winskt.cfg", strDestdir + "Winskt.cfg", true);
					if (File.Exists(strSrcDir + "Usrskt.cfg"))
					{
						File.Copy(strSrcDir + "Usrskt.cfg", strDestdir + "Usrskt.cfg", true);
					}
				}
				else
				{
					MessageBox.Show("The config files were not on the server", "Files not found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				SetupGrid();
				OpenCfgFiles();
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			int lngStart = 0;
			int lngEnd = 0;
			int x;
			if (framPhrases.Visible)
			{
				if (GridPhrases.Row < 1)
				{
					MessageBox.Show("You must select at least one row do delete", "No Rows Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				lngStart = GridPhrases.Row;
				lngEnd = GridPhrases.RowSel;
				if (lngEnd < lngStart)
				{
					lngStart = GridPhrases.RowSel;
					lngEnd = GridPhrases.Row;
				}
				for (x = lngEnd; x >= lngStart; x--)
				{
					GridPhrases.RemoveItem(x);
				}
				// x
			}
			else
			{
				if (Grid.Row > 0)
				{
					lngStart = Grid.Row;
					lngEnd = Grid.RowSel;
					if (lngEnd < lngStart)
					{
						lngStart = Grid.RowSel;
						lngEnd = Grid.Row;
					}
					for (x = lngEnd; x >= lngStart; x--)
					{
						Grid.RemoveItem(x);
					}
					// x
				}
				else
				{
					MessageBox.Show("You must select a row to delete", "No Row Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void mnuEditPhrases_Click(object sender, System.EventArgs e)
		{
			if (framPhrases.Visible == false)
			{
				framPhrases.Visible = true;
				// FC:FINAL:VGE - #i1081 Changing main Grid visibility
				Grid.Visible = false;
				framPhrases.BringToFront();
				mnuMove.Visible = false;
				mnuEditPhrases.Text = "Edit Config Files";
				mnuReload.Text = "Reload Phrases";
			}
			else
			{
				mnuMove.Visible = true;
				framPhrases.Visible = false;
				// FC:FINAL:VGE - #i1081 Changing main Grid visibility
				Grid.Visible = true;
				mnuEditPhrases.Text = "Edit Phrases";
				mnuReload.Text = "Reload Config File";
			}
		}

		public void mnuExit_Click()
		{
			this.Unload();
		}

		private void SetupGrid()
		{
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				Grid.Cols = 12;
				Grid.Rows = 1;
				Grid.ColHidden(CNSTGRIDCOLADDSUBSECAREA, true);
				Grid.ColHidden(CNSTGRIDCOLLINEWIDTH, true);
				Grid.ColHidden(CNSTGRIDCOLLINESTYLE, true);
				Grid.ColHidden(CNSTGRIDCOLFILLSTYLE, true);
				// .ColHidden(CNSTGRIDCOLAUTODIMENSION) = True
				Grid.ColHidden(CNSTGRIDCOLAREAMODIFIED, true);
				Grid.ColHidden(CNSTGRIDCOLMULTIPLIER, true);
				Grid.TextMatrix(0, CNSTGRIDCOLCODE, "Code");
				Grid.TextMatrix(0, CNSTGRIDCOLNAME, "Description");
				Grid.TextMatrix(0, CNSTGRIDCOLAREA, "Area Type");
				Grid.TextMatrix(0, CNSTGRIDCOLADDSUBFIRSTAREA, "+/-");
				Grid.TextMatrix(0, CNSTGRIDCOLAUTODIMENSION, "Auto Dim.");
				strTemp = " |Misc. Area|Living Area|Garage Area|Porch Area|Patio/Deck Area|Pool Area|Storage Area|Lot Area|Building Area|Rentable Area|Parking Area|Dwelling Area|Basement Living|Commercial Area|Outbuilding Area|Land Area";
				Grid.ColComboList(CNSTGRIDCOLAREA, strTemp);
				Grid.ColComboList(CNSTGRIDCOLAREAMODIFIED, strTemp);
				Grid.ColComboList(CNSTGRIDCOLADDSUBFIRSTAREA, " |Adds To|Subs From");
				Grid.ColComboList(CNSTGRIDCOLAUTODIMENSION, "True|False");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGrid line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGrid()
		{
			// vbPorter upgrade warning: GridWidth As Variant --> As int
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.05 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCODE, FCConvert.ToInt32(0.08 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLNAME, FCConvert.ToInt32(0.25 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLADDSUBFIRSTAREA, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLAREA, FCConvert.ToInt32(0.25 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLAUTODIMENSION, FCConvert.ToInt32(0.1 * GridWidth));
		}

		private void LoadPhrasesFile()
		{
			//FileSystemObject fso = new FileSystemObject();
			//StreamReader ts;
            FCFileHandle ts = new FCFileHandle();
            string strPath;
			string strLine = "";
			int x;
			string[] strAry = null;
			try
			{
				// On Error GoTo ErrorHandler
				FCFileSystem.FileClose();
				GridPhrases.Rows = 1;
				strPath = FCFileSystem.Statics.UserDataFolder;
				if (Strings.Right(strPath, 1) != "\\")
				{
					strPath += "\\";
				}
				if (File.Exists(strPath + "Phrases"))
				{
                    ts.Open(strPath + "Phrases", OpenMode.Input, OpenAccess.Read, OpenShare.Default);
                    //ts = File.OpenText(strPath + "Phrases");
					strLine = ts.Read(3);
					strLine = ts.ReadAll();
					for (x = 1; x <= strLine.Length; x++)
					{
						if (Convert.ToByte(Strings.Mid(strLine, x, 1)[0]) < 32)
						{
							fecherFoundation.Strings.MidSet(ref strLine, x, 1, "|");
						}
					}
					// x
					strAry = Strings.Split(strLine, "|", -1, CompareConstants.vbTextCompare);
					for (x = 0; x <= Information.UBound(strAry, 1); x++)
					{
						GridPhrases.AddItem("\t" + strAry[x]);
					}
					// x
					// FC:FINAL:VGE - #i1081 Disposing opened file to avoid colisions later.
					ts.Close();
					//ts.Dispose();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadPhrasesFile line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void OpenCfgFiles()
		{
			//FileSystemObject fso = new FileSystemObject();
			StreamReader ts;
			string strPath;
			string strLine = "";
			bool boolFound = false;
			try
			{
				// On Error GoTo ErrorHandler
				FCFileSystem.FileClose();
				strPath = FCFileSystem.Statics.UserDataFolder;
				if (Strings.Right(strPath, 1) != "\\")
				{
					strPath += "\\";
				}
				if (File.Exists(strPath + "Winskt.cfg"))
				{
					ts = File.OpenText(strPath + "winskt.cfg");
					boolFound = false;
					while (!ts.EndOfStream)
					{
						strLine = ts.ReadLine();
						if (Strings.UCase(Strings.Trim(strLine)) == "[AREA NAME]")
						{
							boolFound = true;
							break;
						}
					}
					if (boolFound)
					{
						FillGrid(ts);
					}
					ts.Close();
				}
				FCFileSystem.FileClose();
				if (File.Exists(strPath + "Usrskt.cfg"))
				{
					ts = File.OpenText(strPath + "usrskt.cfg");
					boolFound = false;
					while (!ts.EndOfStream)
					{
						strLine = ts.ReadLine();
						if (Strings.UCase(Strings.Trim(strLine)) == "[AREA NAME]")
						{
							boolFound = true;
							break;
						}
					}
					if (boolFound)
					{
						FillGrid(ts);
					}
					ts.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In OpenCfgFiles in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillGrid(StreamReader ts)
		{
			// takes a text stream and fills the grid
			try
			{
				// On Error GoTo ErrorHandler
				string strLine = "";
				int intpos = 0;
				string[] strAry = null;
				string strTemp = "";
				int lngRow;
				while (!ts.EndOfStream)
				{
					strLine = ts.ReadLine();
					intpos = Strings.InStr(1, strLine, "=", CompareConstants.vbTextCompare);
					if (intpos > 0)
					{
						strLine = Strings.Mid(strLine, intpos + 1);
						strAry = Strings.Split(strLine, ",", -1, CompareConstants.vbTextCompare);
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						intpos = Strings.InStr(1, strAry[0], " ", CompareConstants.vbTextCompare);
						if (intpos > 1)
						{
							strTemp = strAry[0];
							strTemp = Strings.Mid(strTemp, 1, intpos - 1);
							if (Conversion.Val(strTemp) > 0)
							{
								Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE, FCConvert.ToString(Conversion.Val(strTemp)));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, Strings.Mid(strAry[0], intpos + 1));
							}
							else
							{
								Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, strAry[0]);
							}
						}
						else
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, strAry[0]);
						}
						if (Conversion.Val(strAry[1]) == 0)
						{
							strTemp = "Misc. Area";
						}
						else if (Conversion.Val(strAry[1]) == 1)
						{
							strTemp = "Living Area";
						}
						else if (Conversion.Val(strAry[1]) == 2)
						{
							strTemp = "Garage Area";
						}
						else if (Conversion.Val(strAry[1]) == 3)
						{
							strTemp = "Porch Area";
						}
						else if (Conversion.Val(strAry[1]) == 4)
						{
							strTemp = "Patio/Deck Area";
						}
						else if (Conversion.Val(strAry[1]) == 5)
						{
							strTemp = "Pool Area";
						}
						else if (Conversion.Val(strAry[1]) == 6)
						{
							strTemp = "Storage Area";
						}
						else if (Conversion.Val(strAry[1]) == 7)
						{
							strTemp = "Lot Area";
						}
						else if (Conversion.Val(strAry[1]) == 8)
						{
							strTemp = "Building Area";
						}
						else if (Conversion.Val(strAry[1]) == 9)
						{
							strTemp = "Rentable Area";
						}
						else if (Conversion.Val(strAry[1]) == 10)
						{
							strTemp = "Parking Area";
						}
						else if (Conversion.Val(strAry[1]) == 11)
						{
							strTemp = "Dwelling Area";
						}
						else if (Conversion.Val(strAry[1]) == 12)
						{
							strTemp = "Basement Living";
						}
						else if (Conversion.Val(strAry[1]) == 13)
						{
							strTemp = "Commercial Area";
						}
						else if (Conversion.Val(strAry[1]) == 14)
						{
							strTemp = "Outbuilding Area";
						}
						else if (Conversion.Val(strAry[1]) == 15)
						{
							strTemp = "Land Area";
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA, strTemp);
						Grid.TextMatrix(lngRow, CNSTGRIDCOLADDSUBSECAREA, strAry[2]);
						Grid.TextMatrix(lngRow, CNSTGRIDCOLLINEWIDTH, strAry[3]);
						Grid.TextMatrix(lngRow, CNSTGRIDCOLLINESTYLE, strAry[4]);
						Grid.TextMatrix(lngRow, CNSTGRIDCOLFILLSTYLE, strAry[5]);
						if (Conversion.Val(strAry[6]) == 0)
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTODIMENSION, "False");
						}
						else
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTODIMENSION, "True");
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLAREAMODIFIED, strAry[7]);
						if (Conversion.Val(strAry[8]) > 0)
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLADDSUBFIRSTAREA, "Adds To");
						}
						else if (Conversion.Val(strAry[8]) < 0)
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLADDSUBFIRSTAREA, "Subs From");
						}
						else
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLADDSUBFIRSTAREA, " ");
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLMULTIPLIER, FCConvert.ToString(Conversion.Val(strAry[9])));
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngCode As string	OnWrite(short, double)
		private void FillInRow_728(int lngRow, string lngCode, string strName, short intArea, short intAutoDimension, short intAddSubFirstArea)
		{
			FillInRow(ref lngRow, ref lngCode, ref strName, ref intArea, ref intAutoDimension, ref intAddSubFirstArea);
		}

		private void FillInRow(ref int lngRow, ref string lngCode, ref string strName, ref short intArea, ref short intAutoDimension, ref short intAddSubFirstArea)
		{
			int lngR;
			string strTemp = "";
			lngR = lngRow;
			if (lngRow == 0)
			{
				Grid.Rows += 1;
				lngR = Grid.Rows - 1;
			}
			if (Conversion.Val(lngCode) > 0)
			{
				Grid.TextMatrix(lngR, CNSTGRIDCOLCODE, lngCode);
			}
			else
			{
				Grid.TextMatrix(lngR, CNSTGRIDCOLCODE, "");
			}
			Grid.TextMatrix(lngR, CNSTGRIDCOLNAME, strName);
			switch (intArea)
			{
				case 0:
					{
						strTemp = "Misc. Area";
						break;
					}
				case 1:
					{
						strTemp = "Living Area";
						break;
					}
				case 2:
					{
						strTemp = "Garage Area";
						break;
					}
				case 3:
					{
						strTemp = "Porch Area";
						break;
					}
				case 4:
					{
						strTemp = "Patio/Deck Area";
						break;
					}
				case 5:
					{
						strTemp = "Pool Area";
						break;
					}
				case 6:
					{
						strTemp = "Storage Area";
						break;
					}
				case 7:
					{
						strTemp = "Lot Area";
						break;
					}
				case 8:
					{
						strTemp = "Building Area";
						break;
					}
				case 9:
					{
						strTemp = "Rentable Area";
						break;
					}
				case 10:
					{
						strTemp = "Parking Area";
						break;
					}
				case 11:
					{
						strTemp = "Dwelling Area";
						break;
					}
				case 12:
					{
						strTemp = "Basement Living";
						break;
					}
				case 13:
					{
						strTemp = "Commercial Area";
						break;
					}
				case 14:
					{
						strTemp = "Outbuilding Area";
						break;
					}
				case 15:
					{
						strTemp = "Land Area";
						break;
					}
			}
			//end switch
			Grid.TextMatrix(lngR, CNSTGRIDCOLAREA, strTemp);
			Grid.TextMatrix(lngR, CNSTGRIDCOLADDSUBSECAREA, FCConvert.ToString(1));
			Grid.TextMatrix(lngR, CNSTGRIDCOLLINEWIDTH, FCConvert.ToString(2));
			Grid.TextMatrix(lngR, CNSTGRIDCOLLINESTYLE, FCConvert.ToString(722));
			Grid.TextMatrix(lngR, CNSTGRIDCOLFILLSTYLE, FCConvert.ToString(-4));
			switch (intAutoDimension)
			{
				case 1:
					{
						Grid.TextMatrix(lngR, CNSTGRIDCOLAUTODIMENSION, "True");
						break;
					}
				default:
					{
						Grid.TextMatrix(lngR, CNSTGRIDCOLAUTODIMENSION, "False");
						break;
					}
			}
			//end switch
			Grid.TextMatrix(lngR, CNSTGRIDCOLAREAMODIFIED, " ");
			switch (intAddSubFirstArea)
			{
				case 1:
					{
						Grid.TextMatrix(lngR, CNSTGRIDCOLADDSUBFIRSTAREA, "Adds To");
						break;
					}
				default:
					{
						Grid.TextMatrix(lngR, CNSTGRIDCOLADDSUBFIRSTAREA, "Subs From");
						break;
					}
			}
			//end switch
			Grid.TextMatrix(lngR, CNSTGRIDCOLMULTIPLIER, FCConvert.ToString(1));
		}

		private void mnuLoadNew_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			int intCurrentBldgNumber;
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				// Use the cost file information to set up the Win Sketch Config file
				// reset the grid
				SetupGrid();
				lngRow = 1;
				FillInRow_728(0, FCConvert.ToString(0), "First Floor", 11, 1, 1);
				FillInRow_728(0, FCConvert.ToString(0), "Second Floor", 11, 1, 1);
				FillInRow_728(0, FCConvert.ToString(0), "Third Floor", 11, 1, 1);
				FillInRow_728(0, FCConvert.ToString(0), "Basement Finished", 12, 1, 1);
				FillInRow_728(0, FCConvert.ToString(0), "Basement Unfinished", 0, 1, 1);
				// the rest is based on the cost files
				// start with the dwellings
				clsLoad.OpenRecordset("select * from DWELLINGstyle order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					FillInRow_728(0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))), clsLoad.Get_Fields_String("description"), 11, 1, 1);
					clsLoad.MoveNext();
				}
				if (modGNBas.Statics.gboolCommercial)
				{
					// next do commercial buildings
					clsLoad.OpenRecordset("select * from commercialcost where crecordnumber between 1 and 159 order by crecordnumber", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						FillInRow_728(0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber"))), clsLoad.Get_Fields_String("cldesc"), 13, 1, 1);
						clsLoad.MoveNext();
					}
				}
				// then do outbuildings
				clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1 and 999 order by crecordnumber", "twre0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					strTemp = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
					if (strTemp != string.Empty)
					{
						strTemp = Strings.Replace(strTemp, ".", "", 1, -1, CompareConstants.vbTextCompare);
						strTemp = Strings.Trim(strTemp);
						if (strTemp != string.Empty)
						{
							FillInRow_728(0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber"))), clsLoad.Get_Fields_String("cldesc"), 14, 1, 1);
						}
					}
					clsLoad.MoveNext();
				}
				// then do land
				clsLoad.OpenRecordset("select * from landtype order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					FillInRow_728(0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))), clsLoad.Get_Fields_String("description"), 15, 1, 1);
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MnuLoadNew", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuMove_Click(object sender, System.EventArgs e)
		{
			int lngStart = 0;
			int lngEnd = 0;
			int x;
			if (Grid.Row > 0)
			{
				lngStart = Grid.Row;
				lngEnd = Grid.RowSel;
				if (lngEnd < lngStart)
				{
					lngStart = Grid.RowSel;
					lngEnd = Grid.Row;
				}
				lngFirstRowToMove = lngStart;
				lngLastRowToMove = lngEnd;
				boolMovingRows = true;
				MessageBox.Show("Right-Click on the row to insert the selected row(s) above");
			}
			else
			{
				MessageBox.Show("You must select at least one row to move", "No Row Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuReload_Click(object sender, System.EventArgs e)
		{
			if (framPhrases.Visible)
			{
				LoadPhrasesFile();
			}
			else
			{
				SetupGrid();
				OpenCfgFiles();
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			if (framPhrases.Visible)
			{
				SavePhraseFile();
			}
			else
			{
				SaveCFGFiles();
			}
		}

		private bool SavePhraseFile()
		{
			bool SavePhraseFile = false;
			//FileSystemObject fso = new FileSystemObject();
			StreamWriter ts;
			int lngRow;
			string strLine;
			string strPath;
			string strSepar;
			try
			{
				// On Error GoTo ErrorHandler
				FCFileSystem.FileClose();
				GridPhrases.Row = 0;
				//Application.DoEvents();
				SavePhraseFile = false;
				strPath = FCFileSystem.Statics.UserDataFolder;
				if (Strings.Right(strPath, 1) != "\\")
				{
					strPath += "\\";
				}
				ts = File.CreateText(strPath + "Phrases");
				strSepar = FCConvert.ToString(Convert.ToChar(12));
				// used as separator
				strLine = FCConvert.ToString(Convert.ToChar(11)) + FCConvert.ToString(Convert.ToChar(0));
				for (lngRow = 1; lngRow <= GridPhrases.Rows - 1; lngRow++)
				{
					if (Strings.Trim(GridPhrases.TextMatrix(lngRow, 1)) != string.Empty)
					{
						strLine += strSepar + GridPhrases.TextMatrix(lngRow, 1);
					}
				}
				// lngRow
				ts.Write(strLine);
				ts.Close();
				SavePhraseFile = true;
				MessageBox.Show("File Saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SavePhraseFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SavePhraseFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SavePhraseFile;
		}

		private bool SaveCFGFiles()
		{
			bool SaveCFGFiles = false;
			//FileSystemObject fso = new FileSystemObject();
			StreamWriter ts;
			int lngRow;
			string strLine = "";
			// vbPorter upgrade warning: intArea As Variant, short --> As int	OnWriteFCConvert.ToInt16(
			int intArea = 0;
			string strPath;
			try
			{
				// On Error GoTo ErrorHandler
				SaveCFGFiles = false;
				Grid.Row = 0;
				//Application.DoEvents();
				strPath = FCFileSystem.Statics.UserDataFolder;
				if (Strings.Right(strPath, 1) != "\\")
				{
					strPath += "\\";
				}
				ts = File.CreateText(strPath + "WinSkt.cfg");
				ts.Write(strFileHeader);
				ts.WriteLine("[Area Name]");
				// now write what's in the grid
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE)) > 0)
					{
						strLine = FCConvert.ToString(lngRow - 1) + "=" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE))) + " " + Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME));
					}
					else
					{
						strLine = FCConvert.ToString(lngRow - 1) + "=" + Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME));
					}
					if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "MISC. AREA")
					{
						intArea = 0;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "LIVING AREA")
					{
						intArea = 1;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "GARAGE AREA")
					{
						intArea = 2;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "PORCH AREA")
					{
						intArea = 3;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "PATIO/DECK AREA")
					{
						intArea = 4;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "POOL AREA")
					{
						intArea = 5;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "STORAGE AREA")
					{
						intArea = 6;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "LOT AREA")
					{
						intArea = 7;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "BUILDING AREA")
					{
						intArea = 8;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "RENTABLE AREA")
					{
						intArea = 9;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "PARKING AREA")
					{
						intArea = 10;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "DWELLING AREA")
					{
						intArea = 11;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "BASEMENT LIVING")
					{
						intArea = 12;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "COMMERCIAL AREA")
					{
						intArea = 13;
					}
					else if ((Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "OUTBUILDING AREA") || (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "OUTBUILDING ARE"))
					{
						intArea = 14;
					}
					else if (Strings.UCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLAREA)) == "LAND AREA")
					{
						intArea = 15;
					}
					strLine += "," + FCConvert.ToString(intArea);
					strLine += "," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLADDSUBSECAREA)));
					strLine += "," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLLINEWIDTH)));
					strLine += "," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLLINESTYLE)));
					strLine += "," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFILLSTYLE)));
					if (Strings.UCase(Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTODIMENSION))) == "TRUE")
					{
						strLine += ",1";
					}
					else if (Strings.UCase(Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTODIMENSION))) == "FALSE")
					{
						strLine += ",0";
					}
					else
					{
						strLine += ",1";
					}
					strLine += "," + Grid.TextMatrix(lngRow, CNSTGRIDCOLAREAMODIFIED);
					if (Strings.UCase(Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLADDSUBFIRSTAREA))) == "ADDS TO")
					{
						strLine += ",1";
					}
					else if (Strings.UCase(Strings.Trim(Grid.TextMatrix(lngRow, CNSTGRIDCOLADDSUBFIRSTAREA))) == "SUBS FROM")
					{
						strLine += ",-1";
					}
					else
					{
						strLine += ",0";
					}
					strLine += "," + Strings.Format(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLMULTIPLIER)), "0.00");
					ts.WriteLine(strLine);
				}
				// lngRow
				ts.Close();
				ts = File.CreateText(strPath + "UsrSkt.cfg");
				ts.WriteLine("[Area Name]");
				ts.Close();
				SaveCFGFiles = true;
				MessageBox.Show("Files saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveCFGFiles;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCFileSystem.FileClose();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveCFGFiles", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCFGFiles;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (framPhrases.Visible)
			{
				if (SavePhraseFile())
				{
					mnuExit_Click();
				}
			}
			else
			{
				if (SaveCFGFiles())
				{
					mnuExit_Click();
				}
			}
		}

		private void mnuSaveToServer_Click(object sender, System.EventArgs e)
		{
			//FileSystemObject fso = new FileSystemObject();
			string strSrcPath;
			string strDestPath;
			try
			{
				// On Error GoTo ErrorHandler
				strSrcPath = FCFileSystem.Statics.UserDataFolder;
				if (Strings.Right(strSrcPath, 1) != "\\")
				{
					strSrcPath += "\\";
				}
				strDestPath = Directory.GetDirectoryRoot(Application.MapPath("\\"));
				strDestPath += "\\triodata\\triomast\\";
				if (framPhrases.Visible)
				{
					if (SavePhraseFile())
					{
						File.Copy(strSrcPath + "Phrases", strDestPath + "Phrases", true);
					}
					else
					{
						MessageBox.Show("Could not copy file to server due to an error saving", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				else
				{
					if (SaveCFGFiles())
					{
						File.Copy(strSrcPath + "Winskt.cfg", strDestPath + "Winskt.cfg", true);
						File.Copy(strSrcPath + "Usrskt.cfg", strDestPath + "Usrskt.cfg", true);
					}
					else
					{
						MessageBox.Show("Could not copy files to server due to an error saving", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveToServer", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridPhrases()
		{
			try
			{
				// On Error GoTo ErrorHandler
				GridPhrases.Cols = 2;
				GridPhrases.TextMatrix(0, 1, "Description");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridPhrases line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridPhrases()
		{
			int GridWidth = 0;
			GridWidth = GridPhrases.WidthOriginal;
			GridPhrases.ColWidth(0, FCConvert.ToInt32(0.1 * GridWidth));
		}
	}
}
