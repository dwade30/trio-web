﻿using System.IO;
using System.IO.Compression;
using System.Web;



namespace TWRE0000
{
    public class FileUpload : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            var fileName = context.Request.Headers["fileName"];
            var destinationFolder = context.Request.Headers["serverDestinationFolder"];
            if (!Directory.Exists(destinationFolder))
            {
                Directory.CreateDirectory(destinationFolder);
            }
            foreach (string fileKey in HttpContext.Current.Request.Files)
            {
                HttpPostedFile file = HttpContext.Current.Request.Files[fileKey];
                using (var archive = new ZipArchive(file.InputStream))
                {
                    foreach (var zipEntry in archive.Entries)
                    {
                        var zipEntryPath = System.IO.Path.Combine(destinationFolder, zipEntry.FullName);
                        zipEntry.ExtractToFile(zipEntryPath, true);
                    }
                }   
            }
        }

        #endregion
    }
}
