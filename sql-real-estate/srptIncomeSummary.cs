﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptIncomeSummary.
	/// </summary>
	public partial class srptIncomeSummary : FCSectionReport
	{
		public static srptIncomeSummary InstancePtr
		{
			get
			{
				return (srptIncomeSummary)Sys.GetInstance(typeof(srptIncomeSummary));
			}
		}

		protected srptIncomeSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptIncomeSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptIncomeSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL = "";
                string[] strAry = null;
                int lngPGI = 0;
                // vbPorter upgrade warning: lngVacancy As int	OnWriteFCConvert.ToDouble(
                int lngVacancy = 0;
                double dblPCTVac = 0;
                int lngEGI = 0;
                // vbPorter upgrade warning: lngExpenses As int	OnWrite(double, short)
                int lngExpenses = 0;
                double dblOVRate = 0;
                int lngTotal = 0;
                int lngENI = 0;
                double dblERate = 0;
                double dblCapRate = 0;
                double dblTownPerc = 0;
                double dblSTDOVRate;
                bool boolLoadFromScreen;
                strAry = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
                boolLoadFromScreen = FCConvert.CBool(strAry[2]);
                if (!boolLoadFromScreen)
                {
                    strSQL = "Select sum(squarefootage * marketsfyear) as pgi from INCOMEAPPROACH where account = " +
                             strAry[0] + " and card = " + strAry[1];
                    clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                    if (clsLoad.EndOfFile())
                    {
                        this.Close();
                        return;
                    }

                    // TODO Get_Fields: Field [pgi] not found!! (maybe it is an alias?)
                    lngPGI = FCConvert.ToInt32(clsLoad.Get_Fields("pgi"));
                    txtPGI.Text = Strings.Format(lngPGI, "#,###,##0");
                    strSQL = "Select * from INCOMEVALUE where account = " + strAry[0] + " and card = " + strAry[1];
                    clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                    if (!clsLoad.EndOfFile())
                    {
                        dblPCTVac = Conversion.Val(clsLoad.Get_Fields_Double("vacancy")) / 100;
                        lblVacancy.Text = "Less Vacancy of " +
                                          Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("vacancy")), "0.00") +
                                          "%";
                        lngVacancy = FCConvert.ToInt32((dblPCTVac * lngPGI) * -1);
                        txtVacancy.Text = Strings.Format(lngVacancy, "#,###,##0");
                        lngEGI = lngVacancy + lngPGI;
                        txtEffectiveGrossIncome.Text = Strings.Format(lngEGI, "#,###,##0");
                        dblOVRate = Conversion.Val(clsLoad.Get_Fields_Double("overallrate")) / 100;
                        lngTotal = FCConvert.ToInt32(
                            Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("indicatedvalue"))));
                        txtIndicatedMarketValue.Text = Strings.Format(lngTotal, "#,###,##0");
                        clsLoad.OpenRecordset("select overallrate from defaults", modGlobalVariables.strREDatabase);
                        if (!clsLoad.EndOfFile())
                        {
                            dblSTDOVRate = Conversion.Val(clsLoad.Get_Fields_Double("overallrate")) / 100;
                        }
                        else
                        {
                            dblSTDOVRate = 0;
                        }

                        strSQL = "select sum(amount) as tot from incomeexpense where account = " + strAry[0] +
                                 " and card = " + strAry[1];
                        clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                        if (!clsLoad.EndOfFile())
                        {
                            // TODO Get_Fields: Field [tot] not found!! (maybe it is an alias?)
                            lngExpenses = FCConvert.ToInt32((Conversion.Val(clsLoad.Get_Fields("tot"))) * -1);
                        }
                        else
                        {
                            lngExpenses = 0;
                        }

                        txtLessExpenses.Text = Strings.Format(lngExpenses, "#,###,###,##0");
                        lngENI = lngEGI + lngExpenses;
                        txtEffectiveNetIncome.Text = Strings.Format(lngENI, "#,###,##0");
                        txtOverallRate.Text = Strings.Format((dblOVRate * 100), "0.00") + "%";
                        // Call clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1452", strREDatabase)
                        // dblTownPerc = Val(clsLoad.Fields("CBASE"))
                        // dblTownPerc = dblTownPerc / 100 'dos never used decimal points
                        dblTownPerc = modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio;
                        // Call clsLoad.OpenRecordset("SELECT * FROM [Real Estate WWK]", strgndatabase)
                        // dblBRate = Round((Val(clsLoad.Fields("wbillrate")) / 100000) * dblStdOVRate * dblTownPerc / 10, 2) 'want in percentage not per 1000
                        // dblERate = Round((CustomizedInfo.BillRate / 1000) * dblSTDOVRate * dblTownPerc, 4) 'want in percentage not per 1000
                        dblERate = modGlobalRoutines.Round(
                            (modGlobalVariables.Statics.CustomizedInfo.BillRate / 1000) * dblTownPerc, 4);
                        // want in percentage not per 1000
                        dblCapRate = dblERate + dblOVRate;
                        txtEffectiveTaxRate.Text = Strings.Format(dblERate * 100, "0.00") + "%";
                        txtCapitalizationRate.Text = Strings.Format(dblCapRate * 100, "0.00") + "%";
                    }
                }
                else
                {
                    txtPGI.Text = frmNewREProperty.InstancePtr.GridValues.TextMatrix(0, 2);
                    lblVacancy.Text = "Less Vacancy of " + frmNewREProperty.InstancePtr.GridValues.TextMatrix(1, 1) +
                                      "%";
                    txtVacancy.Text = frmNewREProperty.InstancePtr.GridValues.TextMatrix(1, 2);
                    txtEffectiveGrossIncome.Text = frmNewREProperty.InstancePtr.GridValues.TextMatrix(2, 2);
                    txtLessExpenses.Text = frmNewREProperty.InstancePtr.GridValues.TextMatrix(3, 2);
                    txtEffectiveNetIncome.Text = frmNewREProperty.InstancePtr.GridValues.TextMatrix(4, 2);
                    txtOverallRate.Text = frmNewREProperty.InstancePtr.GridValues.TextMatrix(5, 2) + "%";
                    txtEffectiveTaxRate.Text = frmNewREProperty.InstancePtr.GridValues.TextMatrix(6, 2) + "%";
                    txtCapitalizationRate.Text = frmNewREProperty.InstancePtr.GridValues.TextMatrix(7, 2) + "%";
                    txtIndicatedMarketValue.Text = frmNewREProperty.InstancePtr.GridValues.TextMatrix(8, 2);
                }
            }
        }

		
	}
}
