﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptTreeGrowthBreakdown.
	/// </summary>
	public partial class rptTreeGrowthBreakdown : BaseSectionReport
	{
		public static rptTreeGrowthBreakdown InstancePtr
		{
			get
			{
				return (rptTreeGrowthBreakdown)Sys.GetInstance(typeof(rptTreeGrowthBreakdown));
			}
		}

		protected rptTreeGrowthBreakdown _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}

		public rptTreeGrowthBreakdown()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tree Growth Breakdown";
		}
		// nObj = 1
		//   0	rptTreeGrowthBreakdown	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		double dblTotASoft;
		double dblTotAHard;
		double dblTotAMixed;
		double dblTotAOther;
		int lngTotSoft;
		int lngTotHard;
		int lngTotMixed;
		int lngTotOther;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			dblTotASoft = 0;
			dblTotAHard = 0;
			dblTotAMixed = 0;
			dblTotAOther = 0;
			lngTotSoft = 0;
			lngTotHard = 0;
			lngTotMixed = 0;
			lngTotOther = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			double dblSoftAcres = 0;
			double dblHardAcres = 0;
			double dblMixedAcres = 0;
			double dblOtherAcres = 0;
			int lngSoftVal = 0;
			int lngHardVal = 0;
			int lngMixedVal = 0;
			int lngOtherVal = 0;
			if (!clsLoad.EndOfFile())
			{
				txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("rsaccount"));
				txtName.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("DeedName1")));
				txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("rsmaplot")));
				dblSoftAcres = Conversion.Val(clsLoad.Get_Fields_Double("rssoft"));
				dblHardAcres = Conversion.Val(clsLoad.Get_Fields_Double("rshard"));
				dblMixedAcres = Conversion.Val(clsLoad.Get_Fields_Double("rsmixed"));
				dblOtherAcres = Conversion.Val(clsLoad.Get_Fields_Double("rsother"));
				lngSoftVal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("rssoftvalue"))));
				lngHardVal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("rshardvalue"))));
				lngMixedVal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("rsmixedvalue"))));
				lngOtherVal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("rsothervalue"))));
				txtAHard.Text = Strings.Format(dblHardAcres, "#,##0.00");
				txtASoft.Text = Strings.Format(dblSoftAcres, "#,##0.00");
				txtAMixed.Text = Strings.Format(dblMixedAcres, "#,##0.00");
				txtAOther.Text = Strings.Format(dblOtherAcres, "#,##0.00");
				txtHard.Text = Strings.Format(lngHardVal, "#,###,##0");
				txtSoft.Text = Strings.Format(lngSoftVal, "#,###,##0");
				txtMixed.Text = Strings.Format(lngMixedVal, "#,###,##0");
				txtOther.Text = Strings.Format(lngOtherVal, "#,###,##0");
				dblTotASoft += dblSoftAcres;
				dblTotAHard += dblHardAcres;
				dblTotAMixed += dblMixedAcres;
				dblTotAOther += dblOtherAcres;
				lngTotHard += lngHardVal;
				lngTotSoft += lngSoftVal;
				lngTotMixed += lngMixedVal;
				lngTotOther += lngOtherVal;
				clsLoad.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = FCConvert.ToString(this.PageNumber);
		}
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public void Init(ref bool boolRange, ref short intType, string strmin = "", string strmax = "", bool boolFromExtract = false)
		{
			
			string strSQL = "";
			string strWhere = "";
			string strOrderBy = "";
			bool boolIncludeNoGrowth;
			boolIncludeNoGrowth = false;
			if (MessageBox.Show("Do you want to show accounts with no tree growth?", "Tree Growth", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				boolIncludeNoGrowth = true;
			}
			switch (intType)
			{
				case 1:
					{
						// account
						strOrderBy = " order by rsaccount,rscard ";
						strWhere = " and rsaccount between " + FCConvert.ToString(Conversion.Val(strmin)) + " and " + FCConvert.ToString(Conversion.Val(strmax));
						break;
					}
				case 2:
					{
						// name
						strOrderBy = " order by DeedName1,rsaccount,rscard ";
						strWhere = " and DeedName1 between '" + strmin + "' and '" + strmax + "'";
						break;
					}
				case 3:
					{
						// map lot
						strOrderBy = " order by rsmaplot,rsaccount,rscard ";
						strWhere = " and rsmaplot between '" + strmin + "' and '" + strmax + "'";
						break;
					}
				case 4:
					{
						// location
						strOrderBy = " order by rslocstreet,CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float),rsaccount,rscard ";
						strWhere = " and rslocstreet between '" + strmin + "' and '" + strmax + "'";
						break;
					}
			}
			//end switch
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolFromExtract)
			{
				if (boolIncludeNoGrowth)
				{
					strSQL = "Select * from Master where rsaccount in (select extracttable.accountnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " + FCConvert.ToString(lngUID) + " ) and not master.rsdeleted = 1 " + strOrderBy;
				}
				else
				{					
					strSQL = "Select * from Master where rsaccount in (select extracttable.accountnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " + FCConvert.ToString(lngUID) + " ) and not master.rsdeleted = 1 and (rsmixedvalue > 0 or rshardvalue > 0 or rssoftvalue > 0 or rssoft > 0 or rshard > 0 or rsmixed > 0)" + strOrderBy;
				}
				clsLoad.OpenRecordset("select TITLE FROM EXTRACT WHERe reportnumber = 0 and userid = " + FCConvert.ToString(lngUID), modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					if (FCConvert.ToString(clsLoad.Get_Fields_String("title")) != string.Empty)
					{
						lblTitle2.Text = clsLoad.Get_Fields_String("title");
					}
				}
			}
			else
			{
				if (!boolRange)
				{
					strWhere = "";
				}
				if (!boolIncludeNoGrowth)
				{
					strWhere = " and (rsmixed > 0 or rssoft > 0 or rshard > 0 or rssoftvalue > 0 or rsmixedvalue > 0 or rshardvalue > 0)";
				}
				strSQL = "Select * from master where not rsdeleted = 1 " + strWhere + strOrderBy;
			}
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No records found", "No records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "TreeGrowth");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotAHard.Text = Strings.Format(dblTotAHard, "#,###,##0.00");
			txtTotASoft.Text = Strings.Format(dblTotASoft, "#,###,##0.00");
			txtTotAMixed.Text = Strings.Format(dblTotAMixed, "#,###,##0.00");
			txtTotAOther.Text = Strings.Format(dblTotAOther, "#,###,##0.00");
			txtTotHard.Text = Strings.Format(lngTotHard, "#,###,##0");
			txtTotSoft.Text = Strings.Format(lngTotSoft, "#,###,##0");
			txtTotMixed.Text = Strings.Format(lngTotMixed, "#,###,##0");
			txtTotOther.Text = Strings.Format(lngTotOther, "#,###,##0");
		}

		
	}
}
