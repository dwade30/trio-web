﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptPropertyCard2.
	/// </summary>
	partial class srptPropertyCard2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptPropertyCard2));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.imgSketch = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.imgPicture = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCards = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCost29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtUnits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSound1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSound2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSound3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSound4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSound5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSound6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSound7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSound8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSound9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSound10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtPhys1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line74 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line77 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line78 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line79 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line80 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line81 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line82 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line83 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line84 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line85 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line86 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line87 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line88 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line89 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line90 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line95 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line96 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line97 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line98 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line99 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line100 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line101 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line102 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line103 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line104 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line105 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line106 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line107 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line108 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.imgSketch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPicture)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCards)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.imgSketch,
				this.imgPicture,
				this.Label1,
				this.txtMapLot,
				this.Label2,
				this.txtAccount,
				this.Label3,
				this.Label4,
				this.txtCard,
				this.Label5,
				this.txtCards,
				this.txtDate,
				this.txtLocation,
				this.txtMuni,
				this.txtCost1,
				this.txtCost2,
				this.txtCost3,
				this.txtCost4,
				this.txtCost5,
				this.txtCost6,
				this.txtCost21,
				this.txtCost22,
				this.txtCost23,
				this.txtCost24,
				this.txtCost25,
				this.txtCost26,
				this.txtCost27,
				this.txtCost28,
				this.txtCost29,
				this.txtType1,
				this.Label6,
				this.Line2,
				this.Line3,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Line7,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Line4,
				this.txtType2,
				this.txtType3,
				this.txtType4,
				this.txtType5,
				this.txtType6,
				this.txtType7,
				this.txtType8,
				this.txtType9,
				this.txtType10,
				this.txtYear1,
				this.txtYear2,
				this.txtYear3,
				this.txtYear4,
				this.txtYear5,
				this.txtYear6,
				this.txtYear7,
				this.txtYear8,
				this.txtYear9,
				this.txtYear10,
				this.Line8,
				this.txtUnits1,
				this.txtUnits2,
				this.txtUnits3,
				this.txtUnits4,
				this.txtUnits5,
				this.txtUnits6,
				this.txtUnits7,
				this.txtUnits8,
				this.txtUnits9,
				this.txtUnits10,
				this.txtGrade1,
				this.txtGrade2,
				this.txtGrade3,
				this.txtGrade4,
				this.txtGrade5,
				this.txtGrade6,
				this.txtGrade7,
				this.txtGrade8,
				this.txtGrade9,
				this.txtGrade10,
				this.txtCond1,
				this.txtCond2,
				this.txtCond3,
				this.txtCond4,
				this.txtCond5,
				this.txtCond6,
				this.txtCond7,
				this.txtCond8,
				this.txtCond9,
				this.txtCond10,
				this.txtSound1,
				this.txtSound2,
				this.txtSound3,
				this.txtSound4,
				this.txtSound5,
				this.txtSound6,
				this.txtSound7,
				this.txtSound8,
				this.txtSound9,
				this.txtSound10,
				this.Line9,
				this.Line6,
				this.Line5,
				this.txtPhys1,
				this.txtPhys2,
				this.txtPhys3,
				this.txtPhys4,
				this.txtPhys5,
				this.txtPhys6,
				this.txtPhys7,
				this.txtPhys8,
				this.txtPhys9,
				this.txtPhys10,
				this.txtFunc1,
				this.txtFunc2,
				this.txtFunc3,
				this.txtFunc4,
				this.txtFunc5,
				this.txtFunc6,
				this.txtFunc7,
				this.txtFunc8,
				this.txtFunc9,
				this.txtFunc10,
				this.Line10,
				this.Line11,
				this.Line12,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label33,
				this.Label34,
				this.SubReport1,
				this.Line1,
				this.Line14,
				this.Line15,
				this.Line16,
				this.Line17,
				this.Line18,
				this.Line19,
				this.Line20,
				this.Line21,
				this.Line22,
				this.Line23,
				this.Line24,
				this.Line25,
				this.Line26,
				this.Line27,
				this.Line28,
				this.Line29,
				this.Line30,
				this.Line31,
				this.Line32,
				this.Line33,
				this.Line34,
				this.Line35,
				this.Line36,
				this.Line37,
				this.Line38,
				this.Line39,
				this.Line40,
				this.Line41,
				this.Line42,
				this.Line43,
				this.Line44,
				this.Line45,
				this.Line46,
				this.Line47,
				this.Line48,
				this.Line49,
				this.Line50,
				this.Line51,
				this.Line52,
				this.Line53,
				this.Line54,
				this.Line55,
				this.Line56,
				this.Line57,
				this.Line58,
				this.Line59,
				this.Line60,
				this.Line61,
				this.Line62,
				this.Line63,
				this.Line64,
				this.Line65,
				this.Line66,
				this.Line67,
				this.Line13,
				this.Line68,
				this.Line69,
				this.Line70,
				this.Line71,
				this.Line72,
				this.Line73,
				this.Line74,
				this.Line75,
				this.Line76,
				this.Line77,
				this.Line78,
				this.Line79,
				this.Line80,
				this.Line81,
				this.Line82,
				this.Line83,
				this.Line84,
				this.Line85,
				this.Line86,
				this.Line87,
				this.Line88,
				this.Line89,
				this.Line90,
				this.Line91,
				this.Line92,
				this.Line93,
				this.Line94,
				this.Line95,
				this.Line96,
				this.Line97,
				this.Line98,
				this.Line99,
				this.Line100,
				this.Line101,
				this.Line102,
				this.Line103,
				this.Line104,
				this.Line105,
				this.Line106,
				this.Line107,
				this.Line108
			});
			this.Detail.Height = 7.46875F;
			this.Detail.Name = "Detail";
			// 
			// imgSketch
			// 
			this.imgSketch.Height = 3.59375F;
			this.imgSketch.HyperLink = null;
			this.imgSketch.ImageData = null;
			this.imgSketch.Left = 5.25F;
			this.imgSketch.LineWeight = 1F;
			this.imgSketch.Name = "imgSketch";
			this.imgSketch.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgSketch.Top = 0.40625F;
			this.imgSketch.Width = 4.749306F;
			// 
			// imgPicture
			// 
			this.imgPicture.Height = 3.40625F;
			this.imgPicture.HyperLink = null;
			this.imgPicture.ImageData = null;
			this.imgPicture.Left = 5.25F;
			this.imgPicture.LineWeight = 1F;
			this.imgPicture.Name = "imgPicture";
			this.imgPicture.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgPicture.Top = 4F;
			this.imgPicture.Width = 4.75F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 10pt";
			this.Label1.Text = "Map Lot";
			this.Label1.Top = 0.1875F;
			this.Label1.Width = 0.5625F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.1875F;
			this.txtMapLot.Left = 0.75F;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-size: 10pt";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0.1875F;
			this.txtMapLot.Width = 1.625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.4375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 10pt";
			this.Label2.Text = "Account";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 0.625F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 3.0625F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 10pt";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.1875F;
			this.txtAccount.Width = 0.625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.75F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 10pt";
			this.Label3.Text = "Location";
			this.Label3.Top = 0.1875F;
			this.Label3.Width = 0.6875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 7.375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 10pt";
			this.Label4.Text = "Card";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 0.375F;
			// 
			// txtCard
			// 
			this.txtCard.Height = 0.1875F;
			this.txtCard.Left = 7.8125F;
			this.txtCard.Name = "txtCard";
			this.txtCard.Style = "font-size: 10pt";
			this.txtCard.Text = null;
			this.txtCard.Top = 0.1875F;
			this.txtCard.Width = 0.3125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 8.1875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 10pt";
			this.Label5.Text = "Of";
			this.Label5.Top = 0.1875F;
			this.Label5.Width = 0.25F;
			// 
			// txtCards
			// 
			this.txtCards.Height = 0.1875F;
			this.txtCards.Left = 8.5F;
			this.txtCards.Name = "txtCards";
			this.txtCards.Style = "font-size: 10pt";
			this.txtCards.Text = null;
			this.txtCards.Top = 0.1875F;
			this.txtCards.Width = 0.3125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 8.9375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-size: 10pt";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.1875F;
			this.txtDate.Width = 1F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 4.4375F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-size: 10pt";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.1875F;
			this.txtLocation.Width = 2.875F;
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 3F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0F;
			this.txtMuni.Width = 4F;
			// 
			// txtCost1
			// 
			this.txtCost1.Height = 0.125F;
			this.txtCost1.Left = 4.375F;
			this.txtCost1.Name = "txtCost1";
			this.txtCost1.Style = "font-size: 6pt";
			this.txtCost1.Text = "1";
			this.txtCost1.Top = 5.53125F;
			this.txtCost1.Width = 0.875F;
			// 
			// txtCost2
			// 
			this.txtCost2.Height = 0.125F;
			this.txtCost2.Left = 4.375F;
			this.txtCost2.Name = "txtCost2";
			this.txtCost2.Style = "font-size: 6pt";
			this.txtCost2.Text = "2";
			this.txtCost2.Top = 5.65625F;
			this.txtCost2.Width = 0.875F;
			// 
			// txtCost3
			// 
			this.txtCost3.Height = 0.125F;
			this.txtCost3.Left = 4.375F;
			this.txtCost3.Name = "txtCost3";
			this.txtCost3.Style = "font-size: 6pt";
			this.txtCost3.Text = "3";
			this.txtCost3.Top = 5.78125F;
			this.txtCost3.Width = 0.875F;
			// 
			// txtCost4
			// 
			this.txtCost4.Height = 0.125F;
			this.txtCost4.Left = 4.375F;
			this.txtCost4.Name = "txtCost4";
			this.txtCost4.Style = "font-size: 6pt";
			this.txtCost4.Text = "4";
			this.txtCost4.Top = 5.90625F;
			this.txtCost4.Width = 0.875F;
			// 
			// txtCost5
			// 
			this.txtCost5.Height = 0.125F;
			this.txtCost5.Left = 4.375F;
			this.txtCost5.Name = "txtCost5";
			this.txtCost5.Style = "font-size: 6pt";
			this.txtCost5.Text = "5";
			this.txtCost5.Top = 6.03125F;
			this.txtCost5.Width = 0.875F;
			// 
			// txtCost6
			// 
			this.txtCost6.Height = 0.125F;
			this.txtCost6.Left = 4.375F;
			this.txtCost6.Name = "txtCost6";
			this.txtCost6.Style = "font-size: 6pt";
			this.txtCost6.Text = "6";
			this.txtCost6.Top = 6.15625F;
			this.txtCost6.Width = 0.875F;
			// 
			// txtCost21
			// 
			this.txtCost21.Height = 0.125F;
			this.txtCost21.Left = 4.375F;
			this.txtCost21.Name = "txtCost21";
			this.txtCost21.Style = "font-size: 6pt";
			this.txtCost21.Text = "21";
			this.txtCost21.Top = 6.28125F;
			this.txtCost21.Width = 0.875F;
			// 
			// txtCost22
			// 
			this.txtCost22.Height = 0.125F;
			this.txtCost22.Left = 4.375F;
			this.txtCost22.Name = "txtCost22";
			this.txtCost22.Style = "font-size: 6pt";
			this.txtCost22.Text = "22";
			this.txtCost22.Top = 6.40625F;
			this.txtCost22.Width = 0.875F;
			// 
			// txtCost23
			// 
			this.txtCost23.Height = 0.125F;
			this.txtCost23.Left = 4.375F;
			this.txtCost23.Name = "txtCost23";
			this.txtCost23.Style = "font-size: 6pt";
			this.txtCost23.Text = "23";
			this.txtCost23.Top = 6.53125F;
			this.txtCost23.Width = 0.875F;
			// 
			// txtCost24
			// 
			this.txtCost24.Height = 0.125F;
			this.txtCost24.Left = 4.375F;
			this.txtCost24.Name = "txtCost24";
			this.txtCost24.Style = "font-size: 6pt";
			this.txtCost24.Text = "24";
			this.txtCost24.Top = 6.65625F;
			this.txtCost24.Width = 0.875F;
			// 
			// txtCost25
			// 
			this.txtCost25.Height = 0.125F;
			this.txtCost25.Left = 4.375F;
			this.txtCost25.Name = "txtCost25";
			this.txtCost25.Style = "font-size: 6pt";
			this.txtCost25.Text = "25";
			this.txtCost25.Top = 6.78125F;
			this.txtCost25.Width = 0.875F;
			// 
			// txtCost26
			// 
			this.txtCost26.Height = 0.125F;
			this.txtCost26.Left = 4.375F;
			this.txtCost26.Name = "txtCost26";
			this.txtCost26.Style = "font-size: 6pt";
			this.txtCost26.Text = "26";
			this.txtCost26.Top = 6.90625F;
			this.txtCost26.Width = 0.875F;
			// 
			// txtCost27
			// 
			this.txtCost27.Height = 0.125F;
			this.txtCost27.Left = 4.375F;
			this.txtCost27.Name = "txtCost27";
			this.txtCost27.Style = "font-size: 6pt";
			this.txtCost27.Text = "27";
			this.txtCost27.Top = 7.03125F;
			this.txtCost27.Width = 0.875F;
			// 
			// txtCost28
			// 
			this.txtCost28.Height = 0.125F;
			this.txtCost28.Left = 4.375F;
			this.txtCost28.Name = "txtCost28";
			this.txtCost28.Style = "font-size: 6pt";
			this.txtCost28.Text = "28";
			this.txtCost28.Top = 7.15625F;
			this.txtCost28.Width = 0.875F;
			// 
			// txtCost29
			// 
			this.txtCost29.Height = 0.125F;
			this.txtCost29.Left = 4.375F;
			this.txtCost29.Name = "txtCost29";
			this.txtCost29.Style = "font-size: 6pt";
			this.txtCost29.Text = "29";
			this.txtCost29.Top = 7.28125F;
			this.txtCost29.Width = 0.875F;
			// 
			// txtType1
			// 
			this.txtType1.Height = 0.19F;
			this.txtType1.Left = 0F;
			this.txtType1.MultiLine = false;
			this.txtType1.Name = "txtType1";
			this.txtType1.Style = "font-size: 8.5pt";
			this.txtType1.Text = null;
			this.txtType1.Top = 5.84375F;
			this.txtType1.Width = 1.125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.6875F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label6.Text = "Additions, Outbuildings & Improvements";
			this.Label6.Top = 5.53125F;
			this.Label6.Width = 2.625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 5.53125F;
			this.Line2.Width = 5.25F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 5.25F;
			this.Line2.Y1 = 5.53125F;
			this.Line2.Y2 = 5.53125F;
			// 
			// Line3
			// 
			this.Line3.Height = 1.875F;
			this.Line3.Left = 4.375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 5.53125F;
			this.Line3.Width = 0F;
			this.Line3.X1 = 4.375F;
			this.Line3.X2 = 4.375F;
			this.Line3.Y1 = 5.53125F;
			this.Line3.Y2 = 7.40625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "";
			this.Label7.Text = "Type";
			this.Label7.Top = 5.6875F;
			this.Label7.Width = 0.5625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 1.125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "text-align: center";
			this.Label8.Text = "Year";
			this.Label8.Top = 5.6875F;
			this.Label8.Width = 0.375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 1.5625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "text-align: center";
			this.Label9.Text = "Units";
			this.Label9.Top = 5.6875F;
			this.Label9.Width = 0.3125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.9375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "text-align: center";
			this.Label10.Text = "Grade";
			this.Label10.Top = 5.6875F;
			this.Label10.Width = 0.4375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "text-align: center";
			this.Label11.Text = "Cond";
			this.Label11.Top = 5.6875F;
			this.Label11.Width = 0.375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.75F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "text-align: center";
			this.Label12.Text = "Phys.";
			this.Label12.Top = 5.6875F;
			this.Label12.Width = 0.4375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.1875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "text-align: center";
			this.Label13.Text = "Funct.";
			this.Label13.Top = 5.6875F;
			this.Label13.Width = 0.375F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 0F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 5.84375F;
			this.Line7.Width = 4.375F;
			this.Line7.X1 = 0F;
			this.Line7.X2 = 4.375F;
			this.Line7.Y1 = 5.84375F;
			this.Line7.Y2 = 5.84375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "text-align: center";
			this.Label14.Text = "Sound Value";
			this.Label14.Top = 5.6875F;
			this.Label14.Width = 0.75F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.4375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "text-align: right";
			this.Label15.Text = "%";
			this.Label15.Top = 5.84375F;
			this.Label15.Width = 0.1875F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "text-align: right";
			this.Label16.Text = "%";
			this.Label16.Top = 5.84375F;
			this.Label16.Width = 0.1875F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 5.6875F;
			this.Line4.Width = 4.375F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 4.375F;
			this.Line4.Y1 = 5.6875F;
			this.Line4.Y2 = 5.6875F;
			// 
			// txtType2
			// 
			this.txtType2.Height = 0.19F;
			this.txtType2.Left = 0F;
			this.txtType2.MultiLine = false;
			this.txtType2.Name = "txtType2";
			this.txtType2.Style = "font-size: 8.5pt";
			this.txtType2.Text = null;
			this.txtType2.Top = 6F;
			this.txtType2.Width = 1.125F;
			// 
			// txtType3
			// 
			this.txtType3.Height = 0.19F;
			this.txtType3.Left = 0F;
			this.txtType3.MultiLine = false;
			this.txtType3.Name = "txtType3";
			this.txtType3.Style = "font-size: 8.5pt";
			this.txtType3.Text = null;
			this.txtType3.Top = 6.15625F;
			this.txtType3.Width = 1.125F;
			// 
			// txtType4
			// 
			this.txtType4.Height = 0.19F;
			this.txtType4.Left = 0F;
			this.txtType4.MultiLine = false;
			this.txtType4.Name = "txtType4";
			this.txtType4.Style = "font-size: 8.5pt";
			this.txtType4.Text = null;
			this.txtType4.Top = 6.3125F;
			this.txtType4.Width = 1.125F;
			// 
			// txtType5
			// 
			this.txtType5.Height = 0.19F;
			this.txtType5.Left = 0F;
			this.txtType5.MultiLine = false;
			this.txtType5.Name = "txtType5";
			this.txtType5.Style = "font-size: 8.5pt";
			this.txtType5.Text = null;
			this.txtType5.Top = 6.46875F;
			this.txtType5.Width = 1.125F;
			// 
			// txtType6
			// 
			this.txtType6.Height = 0.19F;
			this.txtType6.Left = 0F;
			this.txtType6.MultiLine = false;
			this.txtType6.Name = "txtType6";
			this.txtType6.Style = "font-size: 8.5pt";
			this.txtType6.Text = null;
			this.txtType6.Top = 6.625F;
			this.txtType6.Width = 1.125F;
			// 
			// txtType7
			// 
			this.txtType7.Height = 0.19F;
			this.txtType7.Left = 0F;
			this.txtType7.MultiLine = false;
			this.txtType7.Name = "txtType7";
			this.txtType7.Style = "font-size: 8.5pt";
			this.txtType7.Text = null;
			this.txtType7.Top = 6.78125F;
			this.txtType7.Width = 1.125F;
			// 
			// txtType8
			// 
			this.txtType8.Height = 0.19F;
			this.txtType8.Left = 0F;
			this.txtType8.MultiLine = false;
			this.txtType8.Name = "txtType8";
			this.txtType8.Style = "font-size: 8.5pt";
			this.txtType8.Text = null;
			this.txtType8.Top = 6.9375F;
			this.txtType8.Width = 1.125F;
			// 
			// txtType9
			// 
			this.txtType9.Height = 0.19F;
			this.txtType9.Left = 0F;
			this.txtType9.MultiLine = false;
			this.txtType9.Name = "txtType9";
			this.txtType9.Style = "font-size: 8.5pt";
			this.txtType9.Text = null;
			this.txtType9.Top = 7.09375F;
			this.txtType9.Width = 1.125F;
			// 
			// txtType10
			// 
			this.txtType10.Height = 0.19F;
			this.txtType10.Left = 0F;
			this.txtType10.MultiLine = false;
			this.txtType10.Name = "txtType10";
			this.txtType10.Style = "font-size: 8.5pt";
			this.txtType10.Text = null;
			this.txtType10.Top = 7.25F;
			this.txtType10.Width = 1.125F;
			// 
			// txtYear1
			// 
			this.txtYear1.Height = 0.19F;
			this.txtYear1.Left = 1.125F;
			this.txtYear1.MultiLine = false;
			this.txtYear1.Name = "txtYear1";
			this.txtYear1.Style = "font-size: 8.5pt";
			this.txtYear1.Text = null;
			this.txtYear1.Top = 5.84375F;
			this.txtYear1.Width = 0.375F;
			// 
			// txtYear2
			// 
			this.txtYear2.Height = 0.19F;
			this.txtYear2.Left = 1.125F;
			this.txtYear2.MultiLine = false;
			this.txtYear2.Name = "txtYear2";
			this.txtYear2.Style = "font-size: 8.5pt";
			this.txtYear2.Text = null;
			this.txtYear2.Top = 6F;
			this.txtYear2.Width = 0.375F;
			// 
			// txtYear3
			// 
			this.txtYear3.Height = 0.19F;
			this.txtYear3.Left = 1.125F;
			this.txtYear3.MultiLine = false;
			this.txtYear3.Name = "txtYear3";
			this.txtYear3.Style = "font-size: 8.5pt";
			this.txtYear3.Text = null;
			this.txtYear3.Top = 6.15625F;
			this.txtYear3.Width = 0.375F;
			// 
			// txtYear4
			// 
			this.txtYear4.Height = 0.19F;
			this.txtYear4.Left = 1.125F;
			this.txtYear4.MultiLine = false;
			this.txtYear4.Name = "txtYear4";
			this.txtYear4.Style = "font-size: 8.5pt";
			this.txtYear4.Text = null;
			this.txtYear4.Top = 6.3125F;
			this.txtYear4.Width = 0.375F;
			// 
			// txtYear5
			// 
			this.txtYear5.Height = 0.19F;
			this.txtYear5.Left = 1.125F;
			this.txtYear5.MultiLine = false;
			this.txtYear5.Name = "txtYear5";
			this.txtYear5.Style = "font-size: 8.5pt";
			this.txtYear5.Text = null;
			this.txtYear5.Top = 6.46875F;
			this.txtYear5.Width = 0.375F;
			// 
			// txtYear6
			// 
			this.txtYear6.Height = 0.19F;
			this.txtYear6.Left = 1.125F;
			this.txtYear6.MultiLine = false;
			this.txtYear6.Name = "txtYear6";
			this.txtYear6.Style = "font-size: 8.5pt";
			this.txtYear6.Text = null;
			this.txtYear6.Top = 6.625F;
			this.txtYear6.Width = 0.375F;
			// 
			// txtYear7
			// 
			this.txtYear7.Height = 0.19F;
			this.txtYear7.Left = 1.125F;
			this.txtYear7.MultiLine = false;
			this.txtYear7.Name = "txtYear7";
			this.txtYear7.Style = "font-size: 8.5pt";
			this.txtYear7.Text = null;
			this.txtYear7.Top = 6.78125F;
			this.txtYear7.Width = 0.375F;
			// 
			// txtYear8
			// 
			this.txtYear8.Height = 0.19F;
			this.txtYear8.Left = 1.125F;
			this.txtYear8.MultiLine = false;
			this.txtYear8.Name = "txtYear8";
			this.txtYear8.Style = "font-size: 8.5pt";
			this.txtYear8.Text = null;
			this.txtYear8.Top = 6.9375F;
			this.txtYear8.Width = 0.375F;
			// 
			// txtYear9
			// 
			this.txtYear9.Height = 0.19F;
			this.txtYear9.Left = 1.125F;
			this.txtYear9.MultiLine = false;
			this.txtYear9.Name = "txtYear9";
			this.txtYear9.Style = "font-size: 8.5pt";
			this.txtYear9.Text = null;
			this.txtYear9.Top = 7.09375F;
			this.txtYear9.Width = 0.375F;
			// 
			// txtYear10
			// 
			this.txtYear10.Height = 0.19F;
			this.txtYear10.Left = 1.125F;
			this.txtYear10.MultiLine = false;
			this.txtYear10.Name = "txtYear10";
			this.txtYear10.Style = "font-size: 8.5pt";
			this.txtYear10.Text = null;
			this.txtYear10.Top = 7.25F;
			this.txtYear10.Width = 0.375F;
			// 
			// Line8
			// 
			this.Line8.Height = 1.71875F;
			this.Line8.Left = 1.125F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 5.6875F;
			this.Line8.Width = 0F;
			this.Line8.X1 = 1.125F;
			this.Line8.X2 = 1.125F;
			this.Line8.Y1 = 5.6875F;
			this.Line8.Y2 = 7.40625F;
			// 
			// txtUnits1
			// 
			this.txtUnits1.Height = 0.19F;
			this.txtUnits1.Left = 1.5F;
			this.txtUnits1.MultiLine = false;
			this.txtUnits1.Name = "txtUnits1";
			this.txtUnits1.Style = "font-size: 8.5pt";
			this.txtUnits1.Text = null;
			this.txtUnits1.Top = 5.84375F;
			this.txtUnits1.Width = 0.4375F;
			// 
			// txtUnits2
			// 
			this.txtUnits2.Height = 0.19F;
			this.txtUnits2.Left = 1.5F;
			this.txtUnits2.MultiLine = false;
			this.txtUnits2.Name = "txtUnits2";
			this.txtUnits2.Style = "font-size: 8.5pt";
			this.txtUnits2.Text = null;
			this.txtUnits2.Top = 6F;
			this.txtUnits2.Width = 0.4375F;
			// 
			// txtUnits3
			// 
			this.txtUnits3.Height = 0.19F;
			this.txtUnits3.Left = 1.5F;
			this.txtUnits3.MultiLine = false;
			this.txtUnits3.Name = "txtUnits3";
			this.txtUnits3.Style = "font-size: 8.5pt";
			this.txtUnits3.Text = null;
			this.txtUnits3.Top = 6.15625F;
			this.txtUnits3.Width = 0.4375F;
			// 
			// txtUnits4
			// 
			this.txtUnits4.Height = 0.19F;
			this.txtUnits4.Left = 1.5F;
			this.txtUnits4.MultiLine = false;
			this.txtUnits4.Name = "txtUnits4";
			this.txtUnits4.Style = "font-size: 8.5pt";
			this.txtUnits4.Text = null;
			this.txtUnits4.Top = 6.3125F;
			this.txtUnits4.Width = 0.4375F;
			// 
			// txtUnits5
			// 
			this.txtUnits5.Height = 0.19F;
			this.txtUnits5.Left = 1.5F;
			this.txtUnits5.MultiLine = false;
			this.txtUnits5.Name = "txtUnits5";
			this.txtUnits5.Style = "font-size: 8.5pt";
			this.txtUnits5.Text = null;
			this.txtUnits5.Top = 6.46875F;
			this.txtUnits5.Width = 0.4375F;
			// 
			// txtUnits6
			// 
			this.txtUnits6.Height = 0.19F;
			this.txtUnits6.Left = 1.5F;
			this.txtUnits6.MultiLine = false;
			this.txtUnits6.Name = "txtUnits6";
			this.txtUnits6.Style = "font-size: 8.5pt";
			this.txtUnits6.Text = null;
			this.txtUnits6.Top = 6.625F;
			this.txtUnits6.Width = 0.4375F;
			// 
			// txtUnits7
			// 
			this.txtUnits7.Height = 0.19F;
			this.txtUnits7.Left = 1.5F;
			this.txtUnits7.MultiLine = false;
			this.txtUnits7.Name = "txtUnits7";
			this.txtUnits7.Style = "font-size: 8.5pt";
			this.txtUnits7.Text = null;
			this.txtUnits7.Top = 6.78125F;
			this.txtUnits7.Width = 0.4375F;
			// 
			// txtUnits8
			// 
			this.txtUnits8.Height = 0.19F;
			this.txtUnits8.Left = 1.5F;
			this.txtUnits8.MultiLine = false;
			this.txtUnits8.Name = "txtUnits8";
			this.txtUnits8.Style = "font-size: 8.5pt";
			this.txtUnits8.Text = null;
			this.txtUnits8.Top = 6.9375F;
			this.txtUnits8.Width = 0.4375F;
			// 
			// txtUnits9
			// 
			this.txtUnits9.Height = 0.19F;
			this.txtUnits9.Left = 1.5F;
			this.txtUnits9.MultiLine = false;
			this.txtUnits9.Name = "txtUnits9";
			this.txtUnits9.Style = "font-size: 8.5pt";
			this.txtUnits9.Text = null;
			this.txtUnits9.Top = 7.09375F;
			this.txtUnits9.Width = 0.4375F;
			// 
			// txtUnits10
			// 
			this.txtUnits10.Height = 0.19F;
			this.txtUnits10.Left = 1.5F;
			this.txtUnits10.MultiLine = false;
			this.txtUnits10.Name = "txtUnits10";
			this.txtUnits10.Style = "font-size: 8.5pt";
			this.txtUnits10.Text = null;
			this.txtUnits10.Top = 7.25F;
			this.txtUnits10.Width = 0.4375F;
			// 
			// txtGrade1
			// 
			this.txtGrade1.Height = 0.19F;
			this.txtGrade1.Left = 1.9375F;
			this.txtGrade1.MultiLine = false;
			this.txtGrade1.Name = "txtGrade1";
			this.txtGrade1.Style = "font-size: 8.5pt";
			this.txtGrade1.Text = null;
			this.txtGrade1.Top = 5.84375F;
			this.txtGrade1.Width = 0.4375F;
			// 
			// txtGrade2
			// 
			this.txtGrade2.Height = 0.19F;
			this.txtGrade2.Left = 1.9375F;
			this.txtGrade2.MultiLine = false;
			this.txtGrade2.Name = "txtGrade2";
			this.txtGrade2.Style = "font-size: 8.5pt";
			this.txtGrade2.Text = null;
			this.txtGrade2.Top = 6F;
			this.txtGrade2.Width = 0.4375F;
			// 
			// txtGrade3
			// 
			this.txtGrade3.Height = 0.19F;
			this.txtGrade3.Left = 1.9375F;
			this.txtGrade3.MultiLine = false;
			this.txtGrade3.Name = "txtGrade3";
			this.txtGrade3.Style = "font-size: 8.5pt";
			this.txtGrade3.Text = null;
			this.txtGrade3.Top = 6.15625F;
			this.txtGrade3.Width = 0.4375F;
			// 
			// txtGrade4
			// 
			this.txtGrade4.Height = 0.19F;
			this.txtGrade4.Left = 1.9375F;
			this.txtGrade4.MultiLine = false;
			this.txtGrade4.Name = "txtGrade4";
			this.txtGrade4.Style = "font-size: 8.5pt";
			this.txtGrade4.Text = null;
			this.txtGrade4.Top = 6.3125F;
			this.txtGrade4.Width = 0.4375F;
			// 
			// txtGrade5
			// 
			this.txtGrade5.Height = 0.19F;
			this.txtGrade5.Left = 1.9375F;
			this.txtGrade5.MultiLine = false;
			this.txtGrade5.Name = "txtGrade5";
			this.txtGrade5.Style = "font-size: 8.5pt";
			this.txtGrade5.Text = null;
			this.txtGrade5.Top = 6.46875F;
			this.txtGrade5.Width = 0.4375F;
			// 
			// txtGrade6
			// 
			this.txtGrade6.Height = 0.19F;
			this.txtGrade6.Left = 1.9375F;
			this.txtGrade6.MultiLine = false;
			this.txtGrade6.Name = "txtGrade6";
			this.txtGrade6.Style = "font-size: 8.5pt";
			this.txtGrade6.Text = null;
			this.txtGrade6.Top = 6.625F;
			this.txtGrade6.Width = 0.4375F;
			// 
			// txtGrade7
			// 
			this.txtGrade7.Height = 0.19F;
			this.txtGrade7.Left = 1.9375F;
			this.txtGrade7.MultiLine = false;
			this.txtGrade7.Name = "txtGrade7";
			this.txtGrade7.Style = "font-size: 8.5pt";
			this.txtGrade7.Text = null;
			this.txtGrade7.Top = 6.78125F;
			this.txtGrade7.Width = 0.4375F;
			// 
			// txtGrade8
			// 
			this.txtGrade8.Height = 0.19F;
			this.txtGrade8.Left = 1.9375F;
			this.txtGrade8.MultiLine = false;
			this.txtGrade8.Name = "txtGrade8";
			this.txtGrade8.Style = "font-size: 8.5pt";
			this.txtGrade8.Text = null;
			this.txtGrade8.Top = 6.9375F;
			this.txtGrade8.Width = 0.4375F;
			// 
			// txtGrade9
			// 
			this.txtGrade9.Height = 0.19F;
			this.txtGrade9.Left = 1.9375F;
			this.txtGrade9.MultiLine = false;
			this.txtGrade9.Name = "txtGrade9";
			this.txtGrade9.Style = "font-size: 8.5pt";
			this.txtGrade9.Text = null;
			this.txtGrade9.Top = 7.09375F;
			this.txtGrade9.Width = 0.4375F;
			// 
			// txtGrade10
			// 
			this.txtGrade10.Height = 0.19F;
			this.txtGrade10.Left = 1.9375F;
			this.txtGrade10.MultiLine = false;
			this.txtGrade10.Name = "txtGrade10";
			this.txtGrade10.Style = "font-size: 8.5pt";
			this.txtGrade10.Text = null;
			this.txtGrade10.Top = 7.25F;
			this.txtGrade10.Width = 0.4375F;
			// 
			// txtCond1
			// 
			this.txtCond1.Height = 0.19F;
			this.txtCond1.Left = 2.375F;
			this.txtCond1.MultiLine = false;
			this.txtCond1.Name = "txtCond1";
			this.txtCond1.Style = "font-size: 8.5pt";
			this.txtCond1.Text = null;
			this.txtCond1.Top = 5.84375F;
			this.txtCond1.Width = 0.375F;
			// 
			// txtCond2
			// 
			this.txtCond2.Height = 0.19F;
			this.txtCond2.Left = 2.375F;
			this.txtCond2.MultiLine = false;
			this.txtCond2.Name = "txtCond2";
			this.txtCond2.Style = "font-size: 8.5pt";
			this.txtCond2.Text = null;
			this.txtCond2.Top = 6F;
			this.txtCond2.Width = 0.375F;
			// 
			// txtCond3
			// 
			this.txtCond3.Height = 0.19F;
			this.txtCond3.Left = 2.375F;
			this.txtCond3.MultiLine = false;
			this.txtCond3.Name = "txtCond3";
			this.txtCond3.Style = "font-size: 8.5pt";
			this.txtCond3.Text = null;
			this.txtCond3.Top = 6.15625F;
			this.txtCond3.Width = 0.375F;
			// 
			// txtCond4
			// 
			this.txtCond4.Height = 0.19F;
			this.txtCond4.Left = 2.375F;
			this.txtCond4.MultiLine = false;
			this.txtCond4.Name = "txtCond4";
			this.txtCond4.Style = "font-size: 8.5pt";
			this.txtCond4.Text = null;
			this.txtCond4.Top = 6.3125F;
			this.txtCond4.Width = 0.375F;
			// 
			// txtCond5
			// 
			this.txtCond5.Height = 0.19F;
			this.txtCond5.Left = 2.375F;
			this.txtCond5.MultiLine = false;
			this.txtCond5.Name = "txtCond5";
			this.txtCond5.Style = "font-size: 8.5pt";
			this.txtCond5.Text = null;
			this.txtCond5.Top = 6.46875F;
			this.txtCond5.Width = 0.375F;
			// 
			// txtCond6
			// 
			this.txtCond6.Height = 0.19F;
			this.txtCond6.Left = 2.375F;
			this.txtCond6.MultiLine = false;
			this.txtCond6.Name = "txtCond6";
			this.txtCond6.Style = "font-size: 8.5pt";
			this.txtCond6.Text = null;
			this.txtCond6.Top = 6.625F;
			this.txtCond6.Width = 0.375F;
			// 
			// txtCond7
			// 
			this.txtCond7.Height = 0.19F;
			this.txtCond7.Left = 2.375F;
			this.txtCond7.MultiLine = false;
			this.txtCond7.Name = "txtCond7";
			this.txtCond7.Style = "font-size: 8.5pt";
			this.txtCond7.Text = null;
			this.txtCond7.Top = 6.78125F;
			this.txtCond7.Width = 0.375F;
			// 
			// txtCond8
			// 
			this.txtCond8.Height = 0.19F;
			this.txtCond8.Left = 2.375F;
			this.txtCond8.MultiLine = false;
			this.txtCond8.Name = "txtCond8";
			this.txtCond8.Style = "font-size: 8.5pt";
			this.txtCond8.Text = null;
			this.txtCond8.Top = 6.9375F;
			this.txtCond8.Width = 0.375F;
			// 
			// txtCond9
			// 
			this.txtCond9.Height = 0.19F;
			this.txtCond9.Left = 2.375F;
			this.txtCond9.MultiLine = false;
			this.txtCond9.Name = "txtCond9";
			this.txtCond9.Style = "font-size: 8.5pt";
			this.txtCond9.Text = null;
			this.txtCond9.Top = 7.09375F;
			this.txtCond9.Width = 0.375F;
			// 
			// txtCond10
			// 
			this.txtCond10.Height = 0.19F;
			this.txtCond10.Left = 2.375F;
			this.txtCond10.MultiLine = false;
			this.txtCond10.Name = "txtCond10";
			this.txtCond10.Style = "font-size: 8.5pt";
			this.txtCond10.Text = null;
			this.txtCond10.Top = 7.25F;
			this.txtCond10.Width = 0.375F;
			// 
			// txtSound1
			// 
			this.txtSound1.Height = 0.19F;
			this.txtSound1.Left = 3.625F;
			this.txtSound1.MultiLine = false;
			this.txtSound1.Name = "txtSound1";
			this.txtSound1.Style = "font-size: 8.5pt";
			this.txtSound1.Text = null;
			this.txtSound1.Top = 5.84375F;
			this.txtSound1.Width = 0.75F;
			// 
			// txtSound2
			// 
			this.txtSound2.Height = 0.19F;
			this.txtSound2.Left = 3.625F;
			this.txtSound2.MultiLine = false;
			this.txtSound2.Name = "txtSound2";
			this.txtSound2.Style = "font-size: 8.5pt";
			this.txtSound2.Text = null;
			this.txtSound2.Top = 6F;
			this.txtSound2.Width = 0.75F;
			// 
			// txtSound3
			// 
			this.txtSound3.Height = 0.19F;
			this.txtSound3.Left = 3.625F;
			this.txtSound3.MultiLine = false;
			this.txtSound3.Name = "txtSound3";
			this.txtSound3.Style = "font-size: 8.5pt";
			this.txtSound3.Text = null;
			this.txtSound3.Top = 6.15625F;
			this.txtSound3.Width = 0.75F;
			// 
			// txtSound4
			// 
			this.txtSound4.Height = 0.19F;
			this.txtSound4.Left = 3.625F;
			this.txtSound4.MultiLine = false;
			this.txtSound4.Name = "txtSound4";
			this.txtSound4.Style = "font-size: 8.5pt";
			this.txtSound4.Text = null;
			this.txtSound4.Top = 6.3125F;
			this.txtSound4.Width = 0.75F;
			// 
			// txtSound5
			// 
			this.txtSound5.Height = 0.19F;
			this.txtSound5.Left = 3.625F;
			this.txtSound5.MultiLine = false;
			this.txtSound5.Name = "txtSound5";
			this.txtSound5.Style = "font-size: 8.5pt";
			this.txtSound5.Text = null;
			this.txtSound5.Top = 6.46875F;
			this.txtSound5.Width = 0.75F;
			// 
			// txtSound6
			// 
			this.txtSound6.Height = 0.19F;
			this.txtSound6.Left = 3.625F;
			this.txtSound6.MultiLine = false;
			this.txtSound6.Name = "txtSound6";
			this.txtSound6.Style = "font-size: 8.5pt";
			this.txtSound6.Text = null;
			this.txtSound6.Top = 6.625F;
			this.txtSound6.Width = 0.75F;
			// 
			// txtSound7
			// 
			this.txtSound7.Height = 0.19F;
			this.txtSound7.Left = 3.625F;
			this.txtSound7.MultiLine = false;
			this.txtSound7.Name = "txtSound7";
			this.txtSound7.Style = "font-size: 8.5pt";
			this.txtSound7.Text = null;
			this.txtSound7.Top = 6.78125F;
			this.txtSound7.Width = 0.75F;
			// 
			// txtSound8
			// 
			this.txtSound8.Height = 0.19F;
			this.txtSound8.Left = 3.625F;
			this.txtSound8.MultiLine = false;
			this.txtSound8.Name = "txtSound8";
			this.txtSound8.Style = "font-size: 8.5pt";
			this.txtSound8.Text = null;
			this.txtSound8.Top = 6.9375F;
			this.txtSound8.Width = 0.75F;
			// 
			// txtSound9
			// 
			this.txtSound9.Height = 0.19F;
			this.txtSound9.Left = 3.625F;
			this.txtSound9.MultiLine = false;
			this.txtSound9.Name = "txtSound9";
			this.txtSound9.Style = "font-size: 8.5pt";
			this.txtSound9.Text = null;
			this.txtSound9.Top = 7.09375F;
			this.txtSound9.Width = 0.75F;
			// 
			// txtSound10
			// 
			this.txtSound10.Height = 0.19F;
			this.txtSound10.Left = 3.625F;
			this.txtSound10.MultiLine = false;
			this.txtSound10.Name = "txtSound10";
			this.txtSound10.Style = "font-size: 8.5pt";
			this.txtSound10.Text = null;
			this.txtSound10.Top = 7.25F;
			this.txtSound10.Width = 0.75F;
			// 
			// Line9
			// 
			this.Line9.Height = 1.71875F;
			this.Line9.Left = 2.375F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 5.6875F;
			this.Line9.Width = 0F;
			this.Line9.X1 = 2.375F;
			this.Line9.X2 = 2.375F;
			this.Line9.Y1 = 5.6875F;
			this.Line9.Y2 = 7.40625F;
			// 
			// Line6
			// 
			this.Line6.Height = 1.71875F;
			this.Line6.Left = 1.9375F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 5.6875F;
			this.Line6.Width = 0F;
			this.Line6.X1 = 1.9375F;
			this.Line6.X2 = 1.9375F;
			this.Line6.Y1 = 5.6875F;
			this.Line6.Y2 = 7.40625F;
			// 
			// Line5
			// 
			this.Line5.Height = 1.71875F;
			this.Line5.Left = 1.5F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 5.6875F;
			this.Line5.Width = 0F;
			this.Line5.X1 = 1.5F;
			this.Line5.X2 = 1.5F;
			this.Line5.Y1 = 5.6875F;
			this.Line5.Y2 = 7.40625F;
			// 
			// txtPhys1
			// 
			this.txtPhys1.Height = 0.19F;
			this.txtPhys1.Left = 2.75F;
			this.txtPhys1.MultiLine = false;
			this.txtPhys1.Name = "txtPhys1";
			this.txtPhys1.Style = "font-size: 8.5pt";
			this.txtPhys1.Text = null;
			this.txtPhys1.Top = 5.84375F;
			this.txtPhys1.Width = 0.25F;
			// 
			// txtPhys2
			// 
			this.txtPhys2.Height = 0.19F;
			this.txtPhys2.Left = 2.75F;
			this.txtPhys2.MultiLine = false;
			this.txtPhys2.Name = "txtPhys2";
			this.txtPhys2.Style = "font-size: 8.5pt";
			this.txtPhys2.Text = null;
			this.txtPhys2.Top = 6F;
			this.txtPhys2.Width = 0.25F;
			// 
			// txtPhys3
			// 
			this.txtPhys3.Height = 0.19F;
			this.txtPhys3.Left = 2.75F;
			this.txtPhys3.MultiLine = false;
			this.txtPhys3.Name = "txtPhys3";
			this.txtPhys3.Style = "font-size: 8.5pt";
			this.txtPhys3.Text = null;
			this.txtPhys3.Top = 6.15625F;
			this.txtPhys3.Width = 0.25F;
			// 
			// txtPhys4
			// 
			this.txtPhys4.Height = 0.19F;
			this.txtPhys4.Left = 2.75F;
			this.txtPhys4.MultiLine = false;
			this.txtPhys4.Name = "txtPhys4";
			this.txtPhys4.Style = "font-size: 8.5pt";
			this.txtPhys4.Text = null;
			this.txtPhys4.Top = 6.3125F;
			this.txtPhys4.Width = 0.25F;
			// 
			// txtPhys5
			// 
			this.txtPhys5.Height = 0.19F;
			this.txtPhys5.Left = 2.75F;
			this.txtPhys5.MultiLine = false;
			this.txtPhys5.Name = "txtPhys5";
			this.txtPhys5.Style = "font-size: 8.5pt";
			this.txtPhys5.Text = null;
			this.txtPhys5.Top = 6.46875F;
			this.txtPhys5.Width = 0.25F;
			// 
			// txtPhys6
			// 
			this.txtPhys6.Height = 0.19F;
			this.txtPhys6.Left = 2.75F;
			this.txtPhys6.MultiLine = false;
			this.txtPhys6.Name = "txtPhys6";
			this.txtPhys6.Style = "font-size: 8.5pt";
			this.txtPhys6.Text = null;
			this.txtPhys6.Top = 6.625F;
			this.txtPhys6.Width = 0.25F;
			// 
			// txtPhys7
			// 
			this.txtPhys7.Height = 0.19F;
			this.txtPhys7.Left = 2.75F;
			this.txtPhys7.MultiLine = false;
			this.txtPhys7.Name = "txtPhys7";
			this.txtPhys7.Style = "font-size: 8.5pt";
			this.txtPhys7.Text = null;
			this.txtPhys7.Top = 6.78125F;
			this.txtPhys7.Width = 0.25F;
			// 
			// txtPhys8
			// 
			this.txtPhys8.Height = 0.19F;
			this.txtPhys8.Left = 2.75F;
			this.txtPhys8.MultiLine = false;
			this.txtPhys8.Name = "txtPhys8";
			this.txtPhys8.Style = "font-size: 8.5pt";
			this.txtPhys8.Text = null;
			this.txtPhys8.Top = 6.9375F;
			this.txtPhys8.Width = 0.25F;
			// 
			// txtPhys9
			// 
			this.txtPhys9.Height = 0.19F;
			this.txtPhys9.Left = 2.75F;
			this.txtPhys9.MultiLine = false;
			this.txtPhys9.Name = "txtPhys9";
			this.txtPhys9.Style = "font-size: 8.5pt";
			this.txtPhys9.Text = null;
			this.txtPhys9.Top = 7.09375F;
			this.txtPhys9.Width = 0.25F;
			// 
			// txtPhys10
			// 
			this.txtPhys10.Height = 0.19F;
			this.txtPhys10.Left = 2.75F;
			this.txtPhys10.MultiLine = false;
			this.txtPhys10.Name = "txtPhys10";
			this.txtPhys10.Style = "font-size: 8.5pt";
			this.txtPhys10.Text = null;
			this.txtPhys10.Top = 7.25F;
			this.txtPhys10.Width = 0.25F;
			// 
			// txtFunc1
			// 
			this.txtFunc1.Height = 0.19F;
			this.txtFunc1.Left = 3.1875F;
			this.txtFunc1.MultiLine = false;
			this.txtFunc1.Name = "txtFunc1";
			this.txtFunc1.Style = "font-size: 8.5pt";
			this.txtFunc1.Text = null;
			this.txtFunc1.Top = 5.84375F;
			this.txtFunc1.Width = 0.25F;
			// 
			// txtFunc2
			// 
			this.txtFunc2.Height = 0.19F;
			this.txtFunc2.Left = 3.1875F;
			this.txtFunc2.MultiLine = false;
			this.txtFunc2.Name = "txtFunc2";
			this.txtFunc2.Style = "font-size: 8.5pt";
			this.txtFunc2.Text = null;
			this.txtFunc2.Top = 6F;
			this.txtFunc2.Width = 0.25F;
			// 
			// txtFunc3
			// 
			this.txtFunc3.Height = 0.19F;
			this.txtFunc3.Left = 3.1875F;
			this.txtFunc3.MultiLine = false;
			this.txtFunc3.Name = "txtFunc3";
			this.txtFunc3.Style = "font-size: 8.5pt";
			this.txtFunc3.Text = null;
			this.txtFunc3.Top = 6.15625F;
			this.txtFunc3.Width = 0.25F;
			// 
			// txtFunc4
			// 
			this.txtFunc4.Height = 0.19F;
			this.txtFunc4.Left = 3.1875F;
			this.txtFunc4.MultiLine = false;
			this.txtFunc4.Name = "txtFunc4";
			this.txtFunc4.Style = "font-size: 8.5pt";
			this.txtFunc4.Text = null;
			this.txtFunc4.Top = 6.3125F;
			this.txtFunc4.Width = 0.25F;
			// 
			// txtFunc5
			// 
			this.txtFunc5.Height = 0.19F;
			this.txtFunc5.Left = 3.1875F;
			this.txtFunc5.MultiLine = false;
			this.txtFunc5.Name = "txtFunc5";
			this.txtFunc5.Style = "font-size: 8.5pt";
			this.txtFunc5.Text = null;
			this.txtFunc5.Top = 6.46875F;
			this.txtFunc5.Width = 0.25F;
			// 
			// txtFunc6
			// 
			this.txtFunc6.Height = 0.19F;
			this.txtFunc6.Left = 3.1875F;
			this.txtFunc6.MultiLine = false;
			this.txtFunc6.Name = "txtFunc6";
			this.txtFunc6.Style = "font-size: 8.5pt";
			this.txtFunc6.Text = null;
			this.txtFunc6.Top = 6.625F;
			this.txtFunc6.Width = 0.25F;
			// 
			// txtFunc7
			// 
			this.txtFunc7.Height = 0.19F;
			this.txtFunc7.Left = 3.1875F;
			this.txtFunc7.MultiLine = false;
			this.txtFunc7.Name = "txtFunc7";
			this.txtFunc7.Style = "font-size: 8.5pt";
			this.txtFunc7.Text = null;
			this.txtFunc7.Top = 6.78125F;
			this.txtFunc7.Width = 0.25F;
			// 
			// txtFunc8
			// 
			this.txtFunc8.Height = 0.19F;
			this.txtFunc8.Left = 3.1875F;
			this.txtFunc8.MultiLine = false;
			this.txtFunc8.Name = "txtFunc8";
			this.txtFunc8.Style = "font-size: 8.5pt";
			this.txtFunc8.Text = null;
			this.txtFunc8.Top = 6.9375F;
			this.txtFunc8.Width = 0.25F;
			// 
			// txtFunc9
			// 
			this.txtFunc9.Height = 0.19F;
			this.txtFunc9.Left = 3.1875F;
			this.txtFunc9.MultiLine = false;
			this.txtFunc9.Name = "txtFunc9";
			this.txtFunc9.Style = "font-size: 8.5pt";
			this.txtFunc9.Text = null;
			this.txtFunc9.Top = 7.09375F;
			this.txtFunc9.Width = 0.25F;
			// 
			// txtFunc10
			// 
			this.txtFunc10.Height = 0.19F;
			this.txtFunc10.Left = 3.1875F;
			this.txtFunc10.MultiLine = false;
			this.txtFunc10.Name = "txtFunc10";
			this.txtFunc10.Style = "font-size: 8.5pt";
			this.txtFunc10.Text = null;
			this.txtFunc10.Top = 7.25F;
			this.txtFunc10.Width = 0.25F;
			// 
			// Line10
			// 
			this.Line10.Height = 1.71875F;
			this.Line10.Left = 2.75F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 5.6875F;
			this.Line10.Width = 0F;
			this.Line10.X1 = 2.75F;
			this.Line10.X2 = 2.75F;
			this.Line10.Y1 = 5.6875F;
			this.Line10.Y2 = 7.40625F;
			// 
			// Line11
			// 
			this.Line11.Height = 1.71875F;
			this.Line11.Left = 3.1875F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 5.6875F;
			this.Line11.Width = 0F;
			this.Line11.X1 = 3.1875F;
			this.Line11.X2 = 3.1875F;
			this.Line11.Y1 = 5.6875F;
			this.Line11.Y2 = 7.40625F;
			// 
			// Line12
			// 
			this.Line12.Height = 1.71875F;
			this.Line12.Left = 3.625F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 5.6875F;
			this.Line12.Width = 0F;
			this.Line12.X1 = 3.625F;
			this.Line12.X2 = 3.625F;
			this.Line12.Y1 = 5.6875F;
			this.Line12.Y2 = 7.40625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "text-align: right";
			this.Label17.Text = "%";
			this.Label17.Top = 6F;
			this.Label17.Width = 0.1875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "text-align: right";
			this.Label18.Text = "%";
			this.Label18.Top = 6.15625F;
			this.Label18.Width = 0.1875F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "text-align: right";
			this.Label19.Text = "%";
			this.Label19.Top = 6.3125F;
			this.Label19.Width = 0.1875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 3F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "text-align: right";
			this.Label20.Text = "%";
			this.Label20.Top = 6.46875F;
			this.Label20.Width = 0.1875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 3F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "text-align: right";
			this.Label21.Text = "%";
			this.Label21.Top = 6.625F;
			this.Label21.Width = 0.1875F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.19F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "text-align: right";
			this.Label22.Text = "%";
			this.Label22.Top = 6.78125F;
			this.Label22.Width = 0.1875F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 3F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "text-align: right";
			this.Label23.Text = "%";
			this.Label23.Top = 6.9375F;
			this.Label23.Width = 0.1875F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 3F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "text-align: right";
			this.Label24.Text = "%";
			this.Label24.Top = 7.09375F;
			this.Label24.Width = 0.1875F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.19F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 3F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "text-align: right";
			this.Label25.Text = "%";
			this.Label25.Top = 7.25F;
			this.Label25.Width = 0.1875F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.19F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 3.4375F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "text-align: right";
			this.Label26.Text = "%";
			this.Label26.Top = 6F;
			this.Label26.Width = 0.1875F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.19F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 3.4375F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "text-align: right";
			this.Label27.Text = "%";
			this.Label27.Top = 6.15625F;
			this.Label27.Width = 0.1875F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.19F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 3.4375F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "text-align: right";
			this.Label28.Text = "%";
			this.Label28.Top = 6.3125F;
			this.Label28.Width = 0.1875F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.19F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 3.4375F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "text-align: right";
			this.Label29.Text = "%";
			this.Label29.Top = 6.46875F;
			this.Label29.Width = 0.1875F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.19F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 3.4375F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "text-align: right";
			this.Label30.Text = "%";
			this.Label30.Top = 6.625F;
			this.Label30.Width = 0.1875F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.19F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 3.4375F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "text-align: right";
			this.Label31.Text = "%";
			this.Label31.Top = 6.78125F;
			this.Label31.Width = 0.1875F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.19F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 3.4375F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "text-align: right";
			this.Label32.Text = "%";
			this.Label32.Top = 6.9375F;
			this.Label32.Width = 0.1875F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.19F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 3.4375F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "text-align: right";
			this.Label33.Text = "%";
			this.Label33.Top = 7.09375F;
			this.Label33.Width = 0.1875F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.19F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 3.4375F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "text-align: right";
			this.Label34.Text = "%";
			this.Label34.Top = 7.25F;
			this.Label34.Width = 0.1875F;
			// 
			// SubReport1
			// 
			this.SubReport1.CanGrow = false;
			this.SubReport1.CanShrink = false;
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 5.125F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.375F;
			this.SubReport1.Width = 5.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.375F;
			this.Line1.Width = 9.979167F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 9.979167F;
			this.Line1.Y1 = 0.375F;
			this.Line1.Y2 = 0.375F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 5.25F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 4F;
			this.Line14.Width = 4.75F;
			this.Line14.X1 = 5.25F;
			this.Line14.X2 = 10F;
			this.Line14.Y1 = 4F;
			this.Line14.Y2 = 4F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 0F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 6F;
			this.Line15.Width = 4.375F;
			this.Line15.X1 = 4.375F;
			this.Line15.X2 = 0F;
			this.Line15.Y1 = 6F;
			this.Line15.Y2 = 6F;
			// 
			// Line16
			// 
			this.Line16.Height = 0F;
			this.Line16.Left = 0F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 6.15625F;
			this.Line16.Width = 4.375F;
			this.Line16.X1 = 4.375F;
			this.Line16.X2 = 0F;
			this.Line16.Y1 = 6.15625F;
			this.Line16.Y2 = 6.15625F;
			// 
			// Line17
			// 
			this.Line17.Height = 0F;
			this.Line17.Left = 0F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 6.3125F;
			this.Line17.Width = 4.375F;
			this.Line17.X1 = 4.375F;
			this.Line17.X2 = 0F;
			this.Line17.Y1 = 6.3125F;
			this.Line17.Y2 = 6.3125F;
			// 
			// Line18
			// 
			this.Line18.Height = 0F;
			this.Line18.Left = 0F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 6.46875F;
			this.Line18.Width = 4.375F;
			this.Line18.X1 = 4.375F;
			this.Line18.X2 = 0F;
			this.Line18.Y1 = 6.46875F;
			this.Line18.Y2 = 6.46875F;
			// 
			// Line19
			// 
			this.Line19.Height = 0F;
			this.Line19.Left = 0F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 6.625F;
			this.Line19.Width = 4.375F;
			this.Line19.X1 = 4.375F;
			this.Line19.X2 = 0F;
			this.Line19.Y1 = 6.625F;
			this.Line19.Y2 = 6.625F;
			// 
			// Line20
			// 
			this.Line20.Height = 0F;
			this.Line20.Left = 0F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 6.78125F;
			this.Line20.Width = 4.375F;
			this.Line20.X1 = 4.375F;
			this.Line20.X2 = 0F;
			this.Line20.Y1 = 6.78125F;
			this.Line20.Y2 = 6.78125F;
			// 
			// Line21
			// 
			this.Line21.Height = 0F;
			this.Line21.Left = 0F;
			this.Line21.LineWeight = 1F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 6.9375F;
			this.Line21.Width = 4.375F;
			this.Line21.X1 = 4.375F;
			this.Line21.X2 = 0F;
			this.Line21.Y1 = 6.9375F;
			this.Line21.Y2 = 6.9375F;
			// 
			// Line22
			// 
			this.Line22.Height = 0F;
			this.Line22.Left = 0F;
			this.Line22.LineWeight = 1F;
			this.Line22.Name = "Line22";
			this.Line22.Top = 7.09375F;
			this.Line22.Width = 4.375F;
			this.Line22.X1 = 4.375F;
			this.Line22.X2 = 0F;
			this.Line22.Y1 = 7.09375F;
			this.Line22.Y2 = 7.09375F;
			// 
			// Line23
			// 
			this.Line23.Height = 0F;
			this.Line23.Left = 0F;
			this.Line23.LineWeight = 1F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 7.25F;
			this.Line23.Width = 4.375F;
			this.Line23.X1 = 4.375F;
			this.Line23.X2 = 0F;
			this.Line23.Y1 = 7.25F;
			this.Line23.Y2 = 7.25F;
			// 
			// Line24
			// 
			this.Line24.Height = 0F;
			this.Line24.Left = 0F;
			this.Line24.LineWeight = 1F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 7.40625F;
			this.Line24.Width = 4.375F;
			this.Line24.X1 = 4.375F;
			this.Line24.X2 = 0F;
			this.Line24.Y1 = 7.40625F;
			this.Line24.Y2 = 7.40625F;
			// 
			// Line25
			// 
			this.Line25.Height = 3.604167F;
			this.Line25.Left = 5.354167F;
			this.Line25.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line25.LineWeight = 1F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 0.375F;
			this.Line25.Width = 0F;
			this.Line25.X1 = 5.354167F;
			this.Line25.X2 = 5.354167F;
			this.Line25.Y1 = 0.375F;
			this.Line25.Y2 = 3.979167F;
			// 
			// Line26
			// 
			this.Line26.Height = 3.604167F;
			this.Line26.Left = 5.447917F;
			this.Line26.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line26.LineWeight = 1F;
			this.Line26.Name = "Line26";
			this.Line26.Top = 0.375F;
			this.Line26.Width = 0F;
			this.Line26.X1 = 5.447917F;
			this.Line26.X2 = 5.447917F;
			this.Line26.Y1 = 0.375F;
			this.Line26.Y2 = 3.979167F;
			// 
			// Line27
			// 
			this.Line27.Height = 3.604167F;
			this.Line27.Left = 5.552083F;
			this.Line27.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line27.LineWeight = 1F;
			this.Line27.Name = "Line27";
			this.Line27.Top = 0.375F;
			this.Line27.Width = 0F;
			this.Line27.X1 = 5.552083F;
			this.Line27.X2 = 5.552083F;
			this.Line27.Y1 = 0.375F;
			this.Line27.Y2 = 3.979167F;
			// 
			// Line28
			// 
			this.Line28.Height = 0F;
			this.Line28.Left = 5.25F;
			this.Line28.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line28.LineWeight = 1F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 0.4791667F;
			this.Line28.Width = 4.729167F;
			this.Line28.X1 = 5.25F;
			this.Line28.X2 = 9.979167F;
			this.Line28.Y1 = 0.4791667F;
			this.Line28.Y2 = 0.4791667F;
			// 
			// Line29
			// 
			this.Line29.Height = 0F;
			this.Line29.Left = 5.25F;
			this.Line29.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line29.LineWeight = 1F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 0.5729167F;
			this.Line29.Width = 4.729167F;
			this.Line29.X1 = 5.25F;
			this.Line29.X2 = 9.979167F;
			this.Line29.Y1 = 0.5729167F;
			this.Line29.Y2 = 0.5729167F;
			// 
			// Line30
			// 
			this.Line30.Height = 3.604167F;
			this.Line30.Left = 5.645833F;
			this.Line30.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line30.LineWeight = 1F;
			this.Line30.Name = "Line30";
			this.Line30.Top = 0.375F;
			this.Line30.Width = 0F;
			this.Line30.X1 = 5.645833F;
			this.Line30.X2 = 5.645833F;
			this.Line30.Y1 = 0.375F;
			this.Line30.Y2 = 3.979167F;
			// 
			// Line31
			// 
			this.Line31.Height = 3.604167F;
			this.Line31.Left = 5.75F;
			this.Line31.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line31.LineWeight = 1F;
			this.Line31.Name = "Line31";
			this.Line31.Top = 0.375F;
			this.Line31.Width = 0F;
			this.Line31.X1 = 5.75F;
			this.Line31.X2 = 5.75F;
			this.Line31.Y1 = 0.375F;
			this.Line31.Y2 = 3.979167F;
			// 
			// Line32
			// 
			this.Line32.Height = 3.604167F;
			this.Line32.Left = 5.854167F;
			this.Line32.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line32.LineWeight = 1F;
			this.Line32.Name = "Line32";
			this.Line32.Top = 0.375F;
			this.Line32.Width = 0F;
			this.Line32.X1 = 5.854167F;
			this.Line32.X2 = 5.854167F;
			this.Line32.Y1 = 0.375F;
			this.Line32.Y2 = 3.979167F;
			// 
			// Line33
			// 
			this.Line33.Height = 3.604167F;
			this.Line33.Left = 5.947917F;
			this.Line33.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line33.LineWeight = 1F;
			this.Line33.Name = "Line33";
			this.Line33.Top = 0.375F;
			this.Line33.Width = 0F;
			this.Line33.X1 = 5.947917F;
			this.Line33.X2 = 5.947917F;
			this.Line33.Y1 = 0.375F;
			this.Line33.Y2 = 3.979167F;
			// 
			// Line34
			// 
			this.Line34.Height = 3.604167F;
			this.Line34.Left = 6.052083F;
			this.Line34.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line34.LineWeight = 1F;
			this.Line34.Name = "Line34";
			this.Line34.Top = 0.375F;
			this.Line34.Width = 0F;
			this.Line34.X1 = 6.052083F;
			this.Line34.X2 = 6.052083F;
			this.Line34.Y1 = 0.375F;
			this.Line34.Y2 = 3.979167F;
			// 
			// Line35
			// 
			this.Line35.Height = 3.604167F;
			this.Line35.Left = 6.145833F;
			this.Line35.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line35.LineWeight = 1F;
			this.Line35.Name = "Line35";
			this.Line35.Top = 0.375F;
			this.Line35.Width = 0F;
			this.Line35.X1 = 6.145833F;
			this.Line35.X2 = 6.145833F;
			this.Line35.Y1 = 0.375F;
			this.Line35.Y2 = 3.979167F;
			// 
			// Line36
			// 
			this.Line36.Height = 3.604167F;
			this.Line36.Left = 6.25F;
			this.Line36.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line36.LineWeight = 1F;
			this.Line36.Name = "Line36";
			this.Line36.Top = 0.375F;
			this.Line36.Width = 0F;
			this.Line36.X1 = 6.25F;
			this.Line36.X2 = 6.25F;
			this.Line36.Y1 = 0.375F;
			this.Line36.Y2 = 3.979167F;
			// 
			// Line37
			// 
			this.Line37.Height = 3.604167F;
			this.Line37.Left = 6.354167F;
			this.Line37.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line37.LineWeight = 1F;
			this.Line37.Name = "Line37";
			this.Line37.Top = 0.375F;
			this.Line37.Width = 0F;
			this.Line37.X1 = 6.354167F;
			this.Line37.X2 = 6.354167F;
			this.Line37.Y1 = 0.375F;
			this.Line37.Y2 = 3.979167F;
			// 
			// Line38
			// 
			this.Line38.Height = 3.604167F;
			this.Line38.Left = 6.447917F;
			this.Line38.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line38.LineWeight = 1F;
			this.Line38.Name = "Line38";
			this.Line38.Top = 0.375F;
			this.Line38.Width = 0F;
			this.Line38.X1 = 6.447917F;
			this.Line38.X2 = 6.447917F;
			this.Line38.Y1 = 0.375F;
			this.Line38.Y2 = 3.979167F;
			// 
			// Line39
			// 
			this.Line39.Height = 3.604167F;
			this.Line39.Left = 6.552083F;
			this.Line39.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line39.LineWeight = 1F;
			this.Line39.Name = "Line39";
			this.Line39.Top = 0.375F;
			this.Line39.Width = 0F;
			this.Line39.X1 = 6.552083F;
			this.Line39.X2 = 6.552083F;
			this.Line39.Y1 = 0.375F;
			this.Line39.Y2 = 3.979167F;
			// 
			// Line40
			// 
			this.Line40.Height = 3.604167F;
			this.Line40.Left = 6.645833F;
			this.Line40.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line40.LineWeight = 1F;
			this.Line40.Name = "Line40";
			this.Line40.Top = 0.375F;
			this.Line40.Width = 0F;
			this.Line40.X1 = 6.645833F;
			this.Line40.X2 = 6.645833F;
			this.Line40.Y1 = 0.375F;
			this.Line40.Y2 = 3.979167F;
			// 
			// Line41
			// 
			this.Line41.Height = 3.604167F;
			this.Line41.Left = 6.75F;
			this.Line41.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line41.LineWeight = 1F;
			this.Line41.Name = "Line41";
			this.Line41.Top = 0.375F;
			this.Line41.Width = 0F;
			this.Line41.X1 = 6.75F;
			this.Line41.X2 = 6.75F;
			this.Line41.Y1 = 0.375F;
			this.Line41.Y2 = 3.979167F;
			// 
			// Line42
			// 
			this.Line42.Height = 3.604167F;
			this.Line42.Left = 6.854167F;
			this.Line42.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line42.LineWeight = 1F;
			this.Line42.Name = "Line42";
			this.Line42.Top = 0.375F;
			this.Line42.Width = 0F;
			this.Line42.X1 = 6.854167F;
			this.Line42.X2 = 6.854167F;
			this.Line42.Y1 = 0.375F;
			this.Line42.Y2 = 3.979167F;
			// 
			// Line43
			// 
			this.Line43.Height = 3.604167F;
			this.Line43.Left = 6.947917F;
			this.Line43.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line43.LineWeight = 1F;
			this.Line43.Name = "Line43";
			this.Line43.Top = 0.375F;
			this.Line43.Width = 0F;
			this.Line43.X1 = 6.947917F;
			this.Line43.X2 = 6.947917F;
			this.Line43.Y1 = 0.375F;
			this.Line43.Y2 = 3.979167F;
			// 
			// Line44
			// 
			this.Line44.Height = 3.604167F;
			this.Line44.Left = 7.052083F;
			this.Line44.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line44.LineWeight = 1F;
			this.Line44.Name = "Line44";
			this.Line44.Top = 0.375F;
			this.Line44.Width = 0F;
			this.Line44.X1 = 7.052083F;
			this.Line44.X2 = 7.052083F;
			this.Line44.Y1 = 0.375F;
			this.Line44.Y2 = 3.979167F;
			// 
			// Line45
			// 
			this.Line45.Height = 3.604167F;
			this.Line45.Left = 7.145833F;
			this.Line45.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line45.LineWeight = 1F;
			this.Line45.Name = "Line45";
			this.Line45.Top = 0.375F;
			this.Line45.Width = 0F;
			this.Line45.X1 = 7.145833F;
			this.Line45.X2 = 7.145833F;
			this.Line45.Y1 = 0.375F;
			this.Line45.Y2 = 3.979167F;
			// 
			// Line46
			// 
			this.Line46.Height = 3.604167F;
			this.Line46.Left = 7.25F;
			this.Line46.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line46.LineWeight = 1F;
			this.Line46.Name = "Line46";
			this.Line46.Top = 0.375F;
			this.Line46.Width = 0F;
			this.Line46.X1 = 7.25F;
			this.Line46.X2 = 7.25F;
			this.Line46.Y1 = 0.375F;
			this.Line46.Y2 = 3.979167F;
			// 
			// Line47
			// 
			this.Line47.Height = 3.604167F;
			this.Line47.Left = 7.354167F;
			this.Line47.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line47.LineWeight = 1F;
			this.Line47.Name = "Line47";
			this.Line47.Top = 0.375F;
			this.Line47.Width = 0F;
			this.Line47.X1 = 7.354167F;
			this.Line47.X2 = 7.354167F;
			this.Line47.Y1 = 0.375F;
			this.Line47.Y2 = 3.979167F;
			// 
			// Line48
			// 
			this.Line48.Height = 3.604167F;
			this.Line48.Left = 7.447917F;
			this.Line48.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line48.LineWeight = 1F;
			this.Line48.Name = "Line48";
			this.Line48.Top = 0.375F;
			this.Line48.Width = 0F;
			this.Line48.X1 = 7.447917F;
			this.Line48.X2 = 7.447917F;
			this.Line48.Y1 = 0.375F;
			this.Line48.Y2 = 3.979167F;
			// 
			// Line49
			// 
			this.Line49.Height = 3.614583F;
			this.Line49.Left = 7.552083F;
			this.Line49.LineWeight = 1F;
			this.Line49.Name = "Line49";
			this.Line49.Top = 0.375F;
			this.Line49.Width = 0F;
			this.Line49.X1 = 7.552083F;
			this.Line49.X2 = 7.552083F;
			this.Line49.Y1 = 0.375F;
			this.Line49.Y2 = 3.989583F;
			// 
			// Line50
			// 
			this.Line50.Height = 3.604167F;
			this.Line50.Left = 7.552083F;
			this.Line50.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line50.LineWeight = 1F;
			this.Line50.Name = "Line50";
			this.Line50.Top = 0.375F;
			this.Line50.Width = 0F;
			this.Line50.X1 = 7.552083F;
			this.Line50.X2 = 7.552083F;
			this.Line50.Y1 = 0.375F;
			this.Line50.Y2 = 3.979167F;
			// 
			// Line51
			// 
			this.Line51.Height = 3.604167F;
			this.Line51.Left = 7.645833F;
			this.Line51.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line51.LineWeight = 1F;
			this.Line51.Name = "Line51";
			this.Line51.Top = 0.375F;
			this.Line51.Width = 0F;
			this.Line51.X1 = 7.645833F;
			this.Line51.X2 = 7.645833F;
			this.Line51.Y1 = 0.375F;
			this.Line51.Y2 = 3.979167F;
			// 
			// Line52
			// 
			this.Line52.Height = 3.604167F;
			this.Line52.Left = 7.75F;
			this.Line52.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line52.LineWeight = 1F;
			this.Line52.Name = "Line52";
			this.Line52.Top = 0.375F;
			this.Line52.Width = 0F;
			this.Line52.X1 = 7.75F;
			this.Line52.X2 = 7.75F;
			this.Line52.Y1 = 0.375F;
			this.Line52.Y2 = 3.979167F;
			// 
			// Line53
			// 
			this.Line53.Height = 3.604167F;
			this.Line53.Left = 7.854167F;
			this.Line53.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line53.LineWeight = 1F;
			this.Line53.Name = "Line53";
			this.Line53.Top = 0.375F;
			this.Line53.Width = 0F;
			this.Line53.X1 = 7.854167F;
			this.Line53.X2 = 7.854167F;
			this.Line53.Y1 = 0.375F;
			this.Line53.Y2 = 3.979167F;
			// 
			// Line54
			// 
			this.Line54.Height = 3.604167F;
			this.Line54.Left = 7.947917F;
			this.Line54.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line54.LineWeight = 1F;
			this.Line54.Name = "Line54";
			this.Line54.Top = 0.375F;
			this.Line54.Width = 0F;
			this.Line54.X1 = 7.947917F;
			this.Line54.X2 = 7.947917F;
			this.Line54.Y1 = 0.375F;
			this.Line54.Y2 = 3.979167F;
			// 
			// Line55
			// 
			this.Line55.Height = 3.604167F;
			this.Line55.Left = 8.052083F;
			this.Line55.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line55.LineWeight = 1F;
			this.Line55.Name = "Line55";
			this.Line55.Top = 0.375F;
			this.Line55.Width = 0F;
			this.Line55.X1 = 8.052083F;
			this.Line55.X2 = 8.052083F;
			this.Line55.Y1 = 0.375F;
			this.Line55.Y2 = 3.979167F;
			// 
			// Line56
			// 
			this.Line56.Height = 3.604167F;
			this.Line56.Left = 8.145833F;
			this.Line56.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line56.LineWeight = 1F;
			this.Line56.Name = "Line56";
			this.Line56.Top = 0.375F;
			this.Line56.Width = 0F;
			this.Line56.X1 = 8.145833F;
			this.Line56.X2 = 8.145833F;
			this.Line56.Y1 = 0.375F;
			this.Line56.Y2 = 3.979167F;
			// 
			// Line57
			// 
			this.Line57.Height = 3.604167F;
			this.Line57.Left = 8.25F;
			this.Line57.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line57.LineWeight = 1F;
			this.Line57.Name = "Line57";
			this.Line57.Top = 0.375F;
			this.Line57.Width = 0F;
			this.Line57.X1 = 8.25F;
			this.Line57.X2 = 8.25F;
			this.Line57.Y1 = 0.375F;
			this.Line57.Y2 = 3.979167F;
			// 
			// Line58
			// 
			this.Line58.Height = 3.604167F;
			this.Line58.Left = 8.354167F;
			this.Line58.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line58.LineWeight = 1F;
			this.Line58.Name = "Line58";
			this.Line58.Top = 0.375F;
			this.Line58.Width = 0F;
			this.Line58.X1 = 8.354167F;
			this.Line58.X2 = 8.354167F;
			this.Line58.Y1 = 0.375F;
			this.Line58.Y2 = 3.979167F;
			// 
			// Line59
			// 
			this.Line59.Height = 3.604167F;
			this.Line59.Left = 8.447917F;
			this.Line59.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line59.LineWeight = 1F;
			this.Line59.Name = "Line59";
			this.Line59.Top = 0.375F;
			this.Line59.Width = 0F;
			this.Line59.X1 = 8.447917F;
			this.Line59.X2 = 8.447917F;
			this.Line59.Y1 = 0.375F;
			this.Line59.Y2 = 3.979167F;
			// 
			// Line60
			// 
			this.Line60.Height = 3.604167F;
			this.Line60.Left = 8.552083F;
			this.Line60.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line60.LineWeight = 1F;
			this.Line60.Name = "Line60";
			this.Line60.Top = 0.375F;
			this.Line60.Width = 0F;
			this.Line60.X1 = 8.552083F;
			this.Line60.X2 = 8.552083F;
			this.Line60.Y1 = 0.375F;
			this.Line60.Y2 = 3.979167F;
			// 
			// Line61
			// 
			this.Line61.Height = 3.604167F;
			this.Line61.Left = 8.645833F;
			this.Line61.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line61.LineWeight = 1F;
			this.Line61.Name = "Line61";
			this.Line61.Top = 0.375F;
			this.Line61.Width = 0F;
			this.Line61.X1 = 8.645833F;
			this.Line61.X2 = 8.645833F;
			this.Line61.Y1 = 0.375F;
			this.Line61.Y2 = 3.979167F;
			// 
			// Line62
			// 
			this.Line62.Height = 3.604167F;
			this.Line62.Left = 8.75F;
			this.Line62.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line62.LineWeight = 1F;
			this.Line62.Name = "Line62";
			this.Line62.Top = 0.375F;
			this.Line62.Width = 0F;
			this.Line62.X1 = 8.75F;
			this.Line62.X2 = 8.75F;
			this.Line62.Y1 = 0.375F;
			this.Line62.Y2 = 3.979167F;
			// 
			// Line63
			// 
			this.Line63.Height = 3.604167F;
			this.Line63.Left = 8.854167F;
			this.Line63.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line63.LineWeight = 1F;
			this.Line63.Name = "Line63";
			this.Line63.Top = 0.375F;
			this.Line63.Width = 0F;
			this.Line63.X1 = 8.854167F;
			this.Line63.X2 = 8.854167F;
			this.Line63.Y1 = 0.375F;
			this.Line63.Y2 = 3.979167F;
			// 
			// Line64
			// 
			this.Line64.Height = 3.604167F;
			this.Line64.Left = 8.947917F;
			this.Line64.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line64.LineWeight = 1F;
			this.Line64.Name = "Line64";
			this.Line64.Top = 0.375F;
			this.Line64.Width = 0F;
			this.Line64.X1 = 8.947917F;
			this.Line64.X2 = 8.947917F;
			this.Line64.Y1 = 0.375F;
			this.Line64.Y2 = 3.979167F;
			// 
			// Line65
			// 
			this.Line65.Height = 3.604167F;
			this.Line65.Left = 9.052083F;
			this.Line65.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line65.LineWeight = 1F;
			this.Line65.Name = "Line65";
			this.Line65.Top = 0.375F;
			this.Line65.Width = 0F;
			this.Line65.X1 = 9.052083F;
			this.Line65.X2 = 9.052083F;
			this.Line65.Y1 = 0.375F;
			this.Line65.Y2 = 3.979167F;
			// 
			// Line66
			// 
			this.Line66.Height = 3.604167F;
			this.Line66.Left = 9.145833F;
			this.Line66.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line66.LineWeight = 1F;
			this.Line66.Name = "Line66";
			this.Line66.Top = 0.375F;
			this.Line66.Width = 0F;
			this.Line66.X1 = 9.145833F;
			this.Line66.X2 = 9.145833F;
			this.Line66.Y1 = 0.375F;
			this.Line66.Y2 = 3.979167F;
			// 
			// Line67
			// 
			this.Line67.Height = 3.604167F;
			this.Line67.Left = 9.25F;
			this.Line67.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line67.LineWeight = 1F;
			this.Line67.Name = "Line67";
			this.Line67.Top = 0.375F;
			this.Line67.Width = 0F;
			this.Line67.X1 = 9.25F;
			this.Line67.X2 = 9.25F;
			this.Line67.Y1 = 0.375F;
			this.Line67.Y2 = 3.979167F;
			// 
			// Line13
			// 
			this.Line13.Height = 7.03125F;
			this.Line13.Left = 5.25F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 0.375F;
			this.Line13.Width = 0F;
			this.Line13.X1 = 5.25F;
			this.Line13.X2 = 5.25F;
			this.Line13.Y1 = 0.375F;
			this.Line13.Y2 = 7.40625F;
			// 
			// Line68
			// 
			this.Line68.Height = 3.604167F;
			this.Line68.Left = 9.354167F;
			this.Line68.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line68.LineWeight = 1F;
			this.Line68.Name = "Line68";
			this.Line68.Top = 0.375F;
			this.Line68.Width = 0F;
			this.Line68.X1 = 9.354167F;
			this.Line68.X2 = 9.354167F;
			this.Line68.Y1 = 0.375F;
			this.Line68.Y2 = 3.979167F;
			// 
			// Line69
			// 
			this.Line69.Height = 3.604167F;
			this.Line69.Left = 9.458333F;
			this.Line69.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line69.LineWeight = 1F;
			this.Line69.Name = "Line69";
			this.Line69.Top = 0.375F;
			this.Line69.Width = 0F;
			this.Line69.X1 = 9.458333F;
			this.Line69.X2 = 9.458333F;
			this.Line69.Y1 = 0.375F;
			this.Line69.Y2 = 3.979167F;
			// 
			// Line70
			// 
			this.Line70.Height = 3.604167F;
			this.Line70.Left = 9.5625F;
			this.Line70.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line70.LineWeight = 1F;
			this.Line70.Name = "Line70";
			this.Line70.Top = 0.375F;
			this.Line70.Width = 0F;
			this.Line70.X1 = 9.5625F;
			this.Line70.X2 = 9.5625F;
			this.Line70.Y1 = 0.375F;
			this.Line70.Y2 = 3.979167F;
			// 
			// Line71
			// 
			this.Line71.Height = 3.604167F;
			this.Line71.Left = 9.65625F;
			this.Line71.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line71.LineWeight = 1F;
			this.Line71.Name = "Line71";
			this.Line71.Top = 0.375F;
			this.Line71.Width = 0F;
			this.Line71.X1 = 9.65625F;
			this.Line71.X2 = 9.65625F;
			this.Line71.Y1 = 0.375F;
			this.Line71.Y2 = 3.979167F;
			// 
			// Line72
			// 
			this.Line72.Height = 3.604167F;
			this.Line72.Left = 9.760417F;
			this.Line72.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line72.LineWeight = 1F;
			this.Line72.Name = "Line72";
			this.Line72.Top = 0.375F;
			this.Line72.Width = 0F;
			this.Line72.X1 = 9.760417F;
			this.Line72.X2 = 9.760417F;
			this.Line72.Y1 = 0.375F;
			this.Line72.Y2 = 3.979167F;
			// 
			// Line73
			// 
			this.Line73.Height = 3.604167F;
			this.Line73.Left = 9.864583F;
			this.Line73.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line73.LineWeight = 1F;
			this.Line73.Name = "Line73";
			this.Line73.Top = 0.375F;
			this.Line73.Width = 0F;
			this.Line73.X1 = 9.864583F;
			this.Line73.X2 = 9.864583F;
			this.Line73.Y1 = 0.375F;
			this.Line73.Y2 = 3.979167F;
			// 
			// Line74
			// 
			this.Line74.Height = 0F;
			this.Line74.Left = 5.25F;
			this.Line74.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line74.LineWeight = 1F;
			this.Line74.Name = "Line74";
			this.Line74.Top = 0.6770833F;
			this.Line74.Width = 4.729167F;
			this.Line74.X1 = 5.25F;
			this.Line74.X2 = 9.979167F;
			this.Line74.Y1 = 0.6770833F;
			this.Line74.Y2 = 0.6770833F;
			// 
			// Line75
			// 
			this.Line75.Height = 0F;
			this.Line75.Left = 5.25F;
			this.Line75.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line75.LineWeight = 1F;
			this.Line75.Name = "Line75";
			this.Line75.Top = 0.7708333F;
			this.Line75.Width = 4.729167F;
			this.Line75.X1 = 5.25F;
			this.Line75.X2 = 9.979167F;
			this.Line75.Y1 = 0.7708333F;
			this.Line75.Y2 = 0.7708333F;
			// 
			// Line76
			// 
			this.Line76.Height = 0F;
			this.Line76.Left = 5.25F;
			this.Line76.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line76.LineWeight = 1F;
			this.Line76.Name = "Line76";
			this.Line76.Top = 0.875F;
			this.Line76.Width = 4.729167F;
			this.Line76.X1 = 5.25F;
			this.Line76.X2 = 9.979167F;
			this.Line76.Y1 = 0.875F;
			this.Line76.Y2 = 0.875F;
			// 
			// Line77
			// 
			this.Line77.Height = 0F;
			this.Line77.Left = 5.25F;
			this.Line77.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line77.LineWeight = 1F;
			this.Line77.Name = "Line77";
			this.Line77.Top = 0.9791667F;
			this.Line77.Width = 4.729167F;
			this.Line77.X1 = 5.25F;
			this.Line77.X2 = 9.979167F;
			this.Line77.Y1 = 0.9791667F;
			this.Line77.Y2 = 0.9791667F;
			// 
			// Line78
			// 
			this.Line78.Height = 0F;
			this.Line78.Left = 5.25F;
			this.Line78.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line78.LineWeight = 1F;
			this.Line78.Name = "Line78";
			this.Line78.Top = 1.072917F;
			this.Line78.Width = 4.729167F;
			this.Line78.X1 = 5.25F;
			this.Line78.X2 = 9.979167F;
			this.Line78.Y1 = 1.072917F;
			this.Line78.Y2 = 1.072917F;
			// 
			// Line79
			// 
			this.Line79.Height = 0F;
			this.Line79.Left = 5.25F;
			this.Line79.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line79.LineWeight = 1F;
			this.Line79.Name = "Line79";
			this.Line79.Top = 1.177083F;
			this.Line79.Width = 4.729167F;
			this.Line79.X1 = 5.25F;
			this.Line79.X2 = 9.979167F;
			this.Line79.Y1 = 1.177083F;
			this.Line79.Y2 = 1.177083F;
			// 
			// Line80
			// 
			this.Line80.Height = 0F;
			this.Line80.Left = 5.25F;
			this.Line80.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line80.LineWeight = 1F;
			this.Line80.Name = "Line80";
			this.Line80.Top = 1.270833F;
			this.Line80.Width = 4.729167F;
			this.Line80.X1 = 5.25F;
			this.Line80.X2 = 9.979167F;
			this.Line80.Y1 = 1.270833F;
			this.Line80.Y2 = 1.270833F;
			// 
			// Line81
			// 
			this.Line81.Height = 0F;
			this.Line81.Left = 5.25F;
			this.Line81.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line81.LineWeight = 1F;
			this.Line81.Name = "Line81";
			this.Line81.Top = 1.375F;
			this.Line81.Width = 4.729167F;
			this.Line81.X1 = 5.25F;
			this.Line81.X2 = 9.979167F;
			this.Line81.Y1 = 1.375F;
			this.Line81.Y2 = 1.375F;
			// 
			// Line82
			// 
			this.Line82.Height = 0F;
			this.Line82.Left = 5.25F;
			this.Line82.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line82.LineWeight = 1F;
			this.Line82.Name = "Line82";
			this.Line82.Top = 1.479167F;
			this.Line82.Width = 4.729167F;
			this.Line82.X1 = 5.25F;
			this.Line82.X2 = 9.979167F;
			this.Line82.Y1 = 1.479167F;
			this.Line82.Y2 = 1.479167F;
			// 
			// Line83
			// 
			this.Line83.Height = 0F;
			this.Line83.Left = 5.25F;
			this.Line83.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line83.LineWeight = 1F;
			this.Line83.Name = "Line83";
			this.Line83.Top = 1.572917F;
			this.Line83.Width = 4.729167F;
			this.Line83.X1 = 5.25F;
			this.Line83.X2 = 9.979167F;
			this.Line83.Y1 = 1.572917F;
			this.Line83.Y2 = 1.572917F;
			// 
			// Line84
			// 
			this.Line84.Height = 0F;
			this.Line84.Left = 5.25F;
			this.Line84.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line84.LineWeight = 1F;
			this.Line84.Name = "Line84";
			this.Line84.Top = 1.677083F;
			this.Line84.Width = 4.729167F;
			this.Line84.X1 = 5.25F;
			this.Line84.X2 = 9.979167F;
			this.Line84.Y1 = 1.677083F;
			this.Line84.Y2 = 1.677083F;
			// 
			// Line85
			// 
			this.Line85.Height = 0F;
			this.Line85.Left = 5.25F;
			this.Line85.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line85.LineWeight = 1F;
			this.Line85.Name = "Line85";
			this.Line85.Top = 1.770833F;
			this.Line85.Width = 4.729167F;
			this.Line85.X1 = 5.25F;
			this.Line85.X2 = 9.979167F;
			this.Line85.Y1 = 1.770833F;
			this.Line85.Y2 = 1.770833F;
			// 
			// Line86
			// 
			this.Line86.Height = 0F;
			this.Line86.Left = 5.25F;
			this.Line86.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line86.LineWeight = 1F;
			this.Line86.Name = "Line86";
			this.Line86.Top = 1.875F;
			this.Line86.Width = 4.729167F;
			this.Line86.X1 = 5.25F;
			this.Line86.X2 = 9.979167F;
			this.Line86.Y1 = 1.875F;
			this.Line86.Y2 = 1.875F;
			// 
			// Line87
			// 
			this.Line87.Height = 0F;
			this.Line87.Left = 5.25F;
			this.Line87.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line87.LineWeight = 1F;
			this.Line87.Name = "Line87";
			this.Line87.Top = 1.979167F;
			this.Line87.Width = 4.729167F;
			this.Line87.X1 = 5.25F;
			this.Line87.X2 = 9.979167F;
			this.Line87.Y1 = 1.979167F;
			this.Line87.Y2 = 1.979167F;
			// 
			// Line88
			// 
			this.Line88.Height = 0F;
			this.Line88.Left = 5.25F;
			this.Line88.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line88.LineWeight = 1F;
			this.Line88.Name = "Line88";
			this.Line88.Top = 2.072917F;
			this.Line88.Width = 4.729167F;
			this.Line88.X1 = 5.25F;
			this.Line88.X2 = 9.979167F;
			this.Line88.Y1 = 2.072917F;
			this.Line88.Y2 = 2.072917F;
			// 
			// Line89
			// 
			this.Line89.Height = 0F;
			this.Line89.Left = 5.25F;
			this.Line89.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line89.LineWeight = 1F;
			this.Line89.Name = "Line89";
			this.Line89.Top = 2.177083F;
			this.Line89.Width = 4.729167F;
			this.Line89.X1 = 5.25F;
			this.Line89.X2 = 9.979167F;
			this.Line89.Y1 = 2.177083F;
			this.Line89.Y2 = 2.177083F;
			// 
			// Line90
			// 
			this.Line90.Height = 0F;
			this.Line90.Left = 5.25F;
			this.Line90.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line90.LineWeight = 1F;
			this.Line90.Name = "Line90";
			this.Line90.Top = 2.270833F;
			this.Line90.Width = 4.729167F;
			this.Line90.X1 = 5.25F;
			this.Line90.X2 = 9.979167F;
			this.Line90.Y1 = 2.270833F;
			this.Line90.Y2 = 2.270833F;
			// 
			// Line91
			// 
			this.Line91.Height = 0F;
			this.Line91.Left = 5.25F;
			this.Line91.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line91.LineWeight = 1F;
			this.Line91.Name = "Line91";
			this.Line91.Top = 2.375F;
			this.Line91.Width = 4.729167F;
			this.Line91.X1 = 5.25F;
			this.Line91.X2 = 9.979167F;
			this.Line91.Y1 = 2.375F;
			this.Line91.Y2 = 2.375F;
			// 
			// Line92
			// 
			this.Line92.Height = 0F;
			this.Line92.Left = 5.25F;
			this.Line92.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line92.LineWeight = 1F;
			this.Line92.Name = "Line92";
			this.Line92.Top = 2.479167F;
			this.Line92.Width = 4.729167F;
			this.Line92.X1 = 5.25F;
			this.Line92.X2 = 9.979167F;
			this.Line92.Y1 = 2.479167F;
			this.Line92.Y2 = 2.479167F;
			// 
			// Line93
			// 
			this.Line93.Height = 0F;
			this.Line93.Left = 5.25F;
			this.Line93.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line93.LineWeight = 1F;
			this.Line93.Name = "Line93";
			this.Line93.Top = 2.572917F;
			this.Line93.Width = 4.729167F;
			this.Line93.X1 = 5.25F;
			this.Line93.X2 = 9.979167F;
			this.Line93.Y1 = 2.572917F;
			this.Line93.Y2 = 2.572917F;
			// 
			// Line94
			// 
			this.Line94.Height = 0F;
			this.Line94.Left = 5.25F;
			this.Line94.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line94.LineWeight = 1F;
			this.Line94.Name = "Line94";
			this.Line94.Top = 2.677083F;
			this.Line94.Width = 4.729167F;
			this.Line94.X1 = 5.25F;
			this.Line94.X2 = 9.979167F;
			this.Line94.Y1 = 2.677083F;
			this.Line94.Y2 = 2.677083F;
			// 
			// Line95
			// 
			this.Line95.Height = 0F;
			this.Line95.Left = 5.25F;
			this.Line95.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line95.LineWeight = 1F;
			this.Line95.Name = "Line95";
			this.Line95.Top = 2.770833F;
			this.Line95.Width = 4.729167F;
			this.Line95.X1 = 5.25F;
			this.Line95.X2 = 9.979167F;
			this.Line95.Y1 = 2.770833F;
			this.Line95.Y2 = 2.770833F;
			// 
			// Line96
			// 
			this.Line96.Height = 0F;
			this.Line96.Left = 5.25F;
			this.Line96.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line96.LineWeight = 1F;
			this.Line96.Name = "Line96";
			this.Line96.Top = 2.875F;
			this.Line96.Width = 4.729167F;
			this.Line96.X1 = 5.25F;
			this.Line96.X2 = 9.979167F;
			this.Line96.Y1 = 2.875F;
			this.Line96.Y2 = 2.875F;
			// 
			// Line97
			// 
			this.Line97.Height = 0F;
			this.Line97.Left = 5.25F;
			this.Line97.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line97.LineWeight = 1F;
			this.Line97.Name = "Line97";
			this.Line97.Top = 2.979167F;
			this.Line97.Width = 4.729167F;
			this.Line97.X1 = 5.25F;
			this.Line97.X2 = 9.979167F;
			this.Line97.Y1 = 2.979167F;
			this.Line97.Y2 = 2.979167F;
			// 
			// Line98
			// 
			this.Line98.Height = 0F;
			this.Line98.Left = 5.25F;
			this.Line98.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line98.LineWeight = 1F;
			this.Line98.Name = "Line98";
			this.Line98.Top = 3.072917F;
			this.Line98.Width = 4.729167F;
			this.Line98.X1 = 5.25F;
			this.Line98.X2 = 9.979167F;
			this.Line98.Y1 = 3.072917F;
			this.Line98.Y2 = 3.072917F;
			// 
			// Line99
			// 
			this.Line99.Height = 0F;
			this.Line99.Left = 5.25F;
			this.Line99.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line99.LineWeight = 1F;
			this.Line99.Name = "Line99";
			this.Line99.Top = 3.177083F;
			this.Line99.Width = 4.729167F;
			this.Line99.X1 = 5.25F;
			this.Line99.X2 = 9.979167F;
			this.Line99.Y1 = 3.177083F;
			this.Line99.Y2 = 3.177083F;
			// 
			// Line100
			// 
			this.Line100.Height = 0F;
			this.Line100.Left = 5.25F;
			this.Line100.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line100.LineWeight = 1F;
			this.Line100.Name = "Line100";
			this.Line100.Top = 3.270833F;
			this.Line100.Width = 4.729167F;
			this.Line100.X1 = 5.25F;
			this.Line100.X2 = 9.979167F;
			this.Line100.Y1 = 3.270833F;
			this.Line100.Y2 = 3.270833F;
			// 
			// Line101
			// 
			this.Line101.Height = 0F;
			this.Line101.Left = 5.25F;
			this.Line101.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line101.LineWeight = 1F;
			this.Line101.Name = "Line101";
			this.Line101.Top = 3.375F;
			this.Line101.Width = 4.729167F;
			this.Line101.X1 = 5.25F;
			this.Line101.X2 = 9.979167F;
			this.Line101.Y1 = 3.375F;
			this.Line101.Y2 = 3.375F;
			// 
			// Line102
			// 
			this.Line102.Height = 0F;
			this.Line102.Left = 5.25F;
			this.Line102.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line102.LineWeight = 1F;
			this.Line102.Name = "Line102";
			this.Line102.Top = 3.479167F;
			this.Line102.Width = 4.729167F;
			this.Line102.X1 = 5.25F;
			this.Line102.X2 = 9.979167F;
			this.Line102.Y1 = 3.479167F;
			this.Line102.Y2 = 3.479167F;
			// 
			// Line103
			// 
			this.Line103.Height = 0F;
			this.Line103.Left = 5.25F;
			this.Line103.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line103.LineWeight = 1F;
			this.Line103.Name = "Line103";
			this.Line103.Top = 3.572917F;
			this.Line103.Width = 4.729167F;
			this.Line103.X1 = 5.25F;
			this.Line103.X2 = 9.979167F;
			this.Line103.Y1 = 3.572917F;
			this.Line103.Y2 = 3.572917F;
			// 
			// Line104
			// 
			this.Line104.Height = 0F;
			this.Line104.Left = 5.25F;
			this.Line104.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line104.LineWeight = 1F;
			this.Line104.Name = "Line104";
			this.Line104.Top = 3.677083F;
			this.Line104.Width = 4.729167F;
			this.Line104.X1 = 5.25F;
			this.Line104.X2 = 9.979167F;
			this.Line104.Y1 = 3.677083F;
			this.Line104.Y2 = 3.677083F;
			// 
			// Line105
			// 
			this.Line105.Height = 0F;
			this.Line105.Left = 5.25F;
			this.Line105.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line105.LineWeight = 1F;
			this.Line105.Name = "Line105";
			this.Line105.Top = 3.770833F;
			this.Line105.Width = 4.729167F;
			this.Line105.X1 = 5.25F;
			this.Line105.X2 = 9.979167F;
			this.Line105.Y1 = 3.770833F;
			this.Line105.Y2 = 3.770833F;
			// 
			// Line106
			// 
			this.Line106.Height = 0F;
			this.Line106.Left = 5.25F;
			this.Line106.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Line106.LineWeight = 1F;
			this.Line106.Name = "Line106";
			this.Line106.Top = 3.875F;
			this.Line106.Width = 4.729167F;
			this.Line106.X1 = 5.25F;
			this.Line106.X2 = 9.979167F;
			this.Line106.Y1 = 3.875F;
			this.Line106.Y2 = 3.875F;
			// 
			// Line107
			// 
			this.Line107.Height = 0F;
			this.Line107.Left = 5.25F;
			this.Line107.LineWeight = 1F;
			this.Line107.Name = "Line107";
			this.Line107.Top = 3.979167F;
			this.Line107.Width = 4.729167F;
			this.Line107.X1 = 5.25F;
			this.Line107.X2 = 9.979167F;
			this.Line107.Y1 = 3.979167F;
			this.Line107.Y2 = 3.979167F;
			// 
			// Line108
			// 
			this.Line108.Height = 3.604167F;
			this.Line108.Left = 9.974305F;
			this.Line108.LineWeight = 1F;
			this.Line108.Name = "Line108";
			this.Line108.Top = 0.375F;
			this.Line108.Width = 0F;
			this.Line108.X1 = 9.974305F;
			this.Line108.X2 = 9.974305F;
			this.Line108.Y1 = 0.375F;
			this.Line108.Y2 = 3.979167F;
			// 
			// srptPropertyCard2
			//
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.imgSketch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgPicture)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCards)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCost29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSound10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgSketch;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgPicture;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCard;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCards;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCost29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSound1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSound2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSound3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSound4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSound5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSound6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSound7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSound8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSound9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSound10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line32;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line34;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line36;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line46;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line47;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line48;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line49;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line50;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line51;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line52;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line53;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line54;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line55;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line57;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line59;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line60;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line61;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line62;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line63;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line64;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line65;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line66;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line67;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line68;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line69;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line70;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line71;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line72;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line73;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line74;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line75;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line76;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line77;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line78;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line79;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line80;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line81;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line82;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line83;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line84;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line85;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line86;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line87;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line88;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line89;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line90;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line91;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line92;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line93;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line94;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line95;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line96;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line97;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line98;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line99;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line100;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line101;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line102;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line103;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line104;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line105;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line106;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line107;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line108;
	}
}
