﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage8.
	/// </summary>
	public partial class srptMVRPage8 : FCSectionReport
	{
		public srptMVRPage8()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private MunicipalValuationReturn valuationReturn;
        public srptMVRPage8(MunicipalValuationReturn valuationReturn) : this()
        {
            this.valuationReturn = valuationReturn;
        }
		private void InitializeComponentEx()
		{
		}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            txtMunicipality.Text = valuationReturn.Municipality;
			txtMuniname.Text = txtMunicipality.Text;
			lblTitle.Text = "MAINE REVENUE SERVICES - " + valuationReturn.ReportYear + " MUNICIPAL VALUATION RETURN";
            lblYear.Text = valuationReturn.ReportYear.ToString();
			lblYear2.Text = lblYear.Text;
			Label117.Text = "47. Interest rate charged on overdue " + lblYear.Text + " property taxes (36 M.R.S.  505)";
			Label119.Text = "48. Date(s) that " + lblYear.Text + " property taxes are due.";
			txtDate.Text = valuationReturn.MunicipalRecords.SignatureDate?.ToString("MM/dd/yyyy");
            txt39a1.Text = valuationReturn.MunicipalRecords.AssessmentFunction.ToString();
            txt39a2.Text = valuationReturn.MunicipalRecords.AssessorOrAgentName;
            txtEmail.Text = valuationReturn.MunicipalRecords.AssessorOrAgentEmail;
            txtLine43a.Text = valuationReturn.MunicipalRecords.AssessmentRecordsAreComputerized ? "YES" : "NO";
            if (valuationReturn.MunicipalRecords.AssessmentRecordsAreComputerized)
            {
                txt43b.Text = valuationReturn.MunicipalRecords.AssessmentSoftware;
            }
            else
            {
                txt43b.Text = "";
            }

            if (valuationReturn.MunicipalRecords.ImplementedATaxReliefProgram)
            {
                txtLine44a.Text = "YES";
                txtLine44b.Text =
                    valuationReturn.MunicipalRecords.NumberOfPeopleQualifiedForTaxProgram.FormatAsNumber();
                txtLine44c.Text = valuationReturn.MunicipalRecords.AmountOfReliefGrantedForTaxProgram
                    .FormatAsCurrencyNoSymbol();
            }
            else
            {
                txtLine44a.Text = "NO";
                txtLine44b.Text = "";
                txtLine44c.Text = "";
            }

            if (valuationReturn.MunicipalRecords.ImplementedElderlyTaxCredit)
            {
                txtLine51a.Text = "YES";
                txtLine51b.Text = valuationReturn.MunicipalRecords.NumberOfPeopleQualifiedForElderlyCredit
                    .FormatAsNumber();
                txtLine51c.Text = valuationReturn.MunicipalRecords.AmountOfReliefGrantedForElderlyCredit
                    .FormatAsCurrencyNoSymbol();
            }
            else
            {
                txtLine51a.Text = "NO";
                txtLine51b.Text = "";
                txtLine51c.Text = "";
            }

            if (valuationReturn.MunicipalRecords.ImplementedSeniorTaxDeferral)
            {
                txtSeniorDeferralYesNo.Text = "YES";
                txtSeniorTaxDeferralGranted.Text = valuationReturn.MunicipalRecords
                    .AmountOfReliefGrantedForSeniorTaxDeferral.FormatAsCurrencyNoSymbol();
                txtNumberQualifiedForSeniorTaxDeferral.Text = valuationReturn.MunicipalRecords
                    .NumberOfPeopleQualifiedForSeniorTaxDeferral.FormatAsNumber();
            }
            else
            {
                txtSeniorDeferralYesNo.Text = "NO";
                txtSeniorTaxDeferralGranted.Text = "";
                txtNumberQualifiedForSeniorTaxDeferral.Text = "";
            }

            txtLine40From.Text = valuationReturn.MunicipalRecords.FiscalYearStart?.FormatAndPadShortDate() ?? "";
            txtLine40To.Text = valuationReturn.MunicipalRecords.FiscalYearEnd?.FormatAndPadShortDate() ?? "";
            txtLine41.Text = valuationReturn.MunicipalRecords.OverDueInterestRate.ToString() + "%";
            txtLine42Due1.Text = valuationReturn.MunicipalRecords.Period1DueDate?.FormatAndPadShortDate();
            txtLine42Due2.Text = valuationReturn.MunicipalRecords.Period2DueDate?.FormatAndPadShortDate();
            txtLine42Due3.Text = valuationReturn.MunicipalRecords.Period3DueDate?.FormatAndPadShortDate();
            txtLine42Due4.Text = valuationReturn.MunicipalRecords.Period4DueDate?.FormatAndPadShortDate();
			
		}

		
	}
}
