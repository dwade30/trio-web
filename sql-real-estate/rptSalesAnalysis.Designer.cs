﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptSalesAnalysis.
	/// </summary>
	partial class rptSalesAnalysis
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSalesAnalysis));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtSaleFactor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLandFactor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBldgFactor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtNumSales = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtSalePrice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValuation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDeviation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblSalePrice = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblValuation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDeviation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMedian = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMean = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMidQuart = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblWeighted = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblStandDev = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMedian = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMean = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMidQuart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtWeighted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAveDev = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoOfDisp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStandDev = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCoOfVar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPriceDiff = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTrended = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAdjusted = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtExtractTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaleFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLandFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBldgFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumSales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValuation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeviation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSalePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValuation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeviation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMedian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMean)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMidQuart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWeighted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStandDev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMedian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMean)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMidQuart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeighted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAveDev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoOfDisp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStandDev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoOfVar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPriceDiff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTrended)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAdjusted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtractTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.SubReport1,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Label8});
            this.Detail.Height = 0.5833333F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.21875F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0.28125F;
            this.SubReport1.Width = 7.5F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.19F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.25F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label1.Text = "Account";
            this.Label1.Top = 0.09375F;
            this.Label1.Width = 0.625F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0.875F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label2.Text = "Map/Lot";
            this.Label2.Top = 0.09375F;
            this.Label2.Width = 1.125F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.19F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 2.5625F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label3.Text = "Date";
            this.Label3.Top = 0.09375F;
            this.Label3.Width = 0.4375F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.19F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 3.6F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label4.Text = "Price";
            this.Label4.Top = 0.09375F;
            this.Label4.Width = 0.4375F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.19F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 4.2F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label5.Text = "Valuation";
            this.Label5.Top = 0.09375F;
            this.Label5.Width = 0.7793331F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.19F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 5.408F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label6.Text = "Ratio";
            this.Label6.Top = 0.09375F;
            this.Label6.Width = 0.4375F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.19F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 6.25F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label7.Text = "Mean";
            this.Label7.Top = 0.09375F;
            this.Label7.Width = 0.4375F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1875F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 7.0625F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label8.Text = "Dev";
            this.Label8.Top = 0.09375F;
            this.Label8.Width = 0.375F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label9,
            this.txtSaleFactor,
            this.txtLandFactor,
            this.txtBldgFactor,
            this.Label10,
            this.txtNumSales,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Line1,
            this.Line2,
            this.txtSalePrice,
            this.txtValuation,
            this.txtDeviation,
            this.lblSalePrice,
            this.lblValuation,
            this.lblDeviation,
            this.Label15,
            this.lblMedian,
            this.lblMean,
            this.lblMidQuart,
            this.lblWeighted,
            this.Label20,
            this.Label21,
            this.lblStandDev,
            this.Label23,
            this.Label24,
            this.txtMedian,
            this.txtMean,
            this.txtMidQuart,
            this.txtWeighted,
            this.txtAveDev,
            this.txtCoOfDisp,
            this.txtStandDev,
            this.txtCoOfVar,
            this.txtPriceDiff,
            this.lblTrended,
            this.lblAdjusted});
            this.ReportFooter.Height = 4.760417F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Label9
            // 
            this.Label9.Height = 0.19F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 2F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'";
            this.Label9.Text = "Trends:";
            this.Label9.Top = 0.34375F;
            this.Label9.Width = 0.875F;
            // 
            // txtSaleFactor
            // 
            this.txtSaleFactor.CanGrow = false;
            this.txtSaleFactor.Height = 0.19F;
            this.txtSaleFactor.Left = 4.5625F;
            this.txtSaleFactor.MultiLine = false;
            this.txtSaleFactor.Name = "txtSaleFactor";
            this.txtSaleFactor.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtSaleFactor.Text = "Field2";
            this.txtSaleFactor.Top = 0.5F;
            this.txtSaleFactor.Width = 0.9375F;
            // 
            // txtLandFactor
            // 
            this.txtLandFactor.CanGrow = false;
            this.txtLandFactor.Height = 0.19F;
            this.txtLandFactor.Left = 4.5625F;
            this.txtLandFactor.MultiLine = false;
            this.txtLandFactor.Name = "txtLandFactor";
            this.txtLandFactor.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtLandFactor.Text = "Field3";
            this.txtLandFactor.Top = 0.6875F;
            this.txtLandFactor.Width = 0.9375F;
            // 
            // txtBldgFactor
            // 
            this.txtBldgFactor.CanGrow = false;
            this.txtBldgFactor.Height = 0.19F;
            this.txtBldgFactor.Left = 4.875F;
            this.txtBldgFactor.MultiLine = false;
            this.txtBldgFactor.Name = "txtBldgFactor";
            this.txtBldgFactor.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtBldgFactor.Text = "Field4";
            this.txtBldgFactor.Top = 0.875F;
            this.txtBldgFactor.Width = 0.625F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.19F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 2F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Tahoma\'";
            this.Label10.Text = "Number of Sales:";
            this.Label10.Top = 1.1875F;
            this.Label10.Width = 1.375F;
            // 
            // txtNumSales
            // 
            this.txtNumSales.CanGrow = false;
            this.txtNumSales.Height = 0.19F;
            this.txtNumSales.Left = 4.875F;
            this.txtNumSales.MultiLine = false;
            this.txtNumSales.Name = "txtNumSales";
            this.txtNumSales.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtNumSales.Text = "Field2";
            this.txtNumSales.Top = 1.15625F;
            this.txtNumSales.Width = 0.625F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.19F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 2.5625F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \'Tahoma\'";
            this.Label11.Text = "Sale Factor";
            this.Label11.Top = 0.5F;
            this.Label11.Width = 1F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.19F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 2.5625F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Tahoma\'";
            this.Label12.Text = "Land Factor";
            this.Label12.Top = 0.6875F;
            this.Label12.Width = 1F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.19F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 2.5625F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Tahoma\'";
            this.Label13.Text = "Bldg Factor";
            this.Label13.Top = 0.875F;
            this.Label13.Width = 1F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.19F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 2F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Tahoma\'";
            this.Label14.Text = "Totals:";
            this.Label14.Top = 1.5F;
            this.Label14.Width = 0.875F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.09375F;
            this.Line1.Width = 7.5F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.5F;
            this.Line1.Y1 = 0.09375F;
            this.Line1.Y2 = 0.09375F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.125F;
            this.Line2.Width = 7.5F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.5F;
            this.Line2.Y1 = 0.125F;
            this.Line2.Y2 = 0.125F;
            // 
            // txtSalePrice
            // 
            this.txtSalePrice.CanGrow = false;
            this.txtSalePrice.Height = 0.19F;
            this.txtSalePrice.Left = 4.375F;
            this.txtSalePrice.MultiLine = false;
            this.txtSalePrice.Name = "txtSalePrice";
            this.txtSalePrice.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtSalePrice.Text = "Field3";
            this.txtSalePrice.Top = 1.6875F;
            this.txtSalePrice.Width = 1.125F;
            // 
            // txtValuation
            // 
            this.txtValuation.CanGrow = false;
            this.txtValuation.Height = 0.19F;
            this.txtValuation.Left = 4.375F;
            this.txtValuation.MultiLine = false;
            this.txtValuation.Name = "txtValuation";
            this.txtValuation.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtValuation.Text = "Field3";
            this.txtValuation.Top = 1.875F;
            this.txtValuation.Width = 1.125F;
            // 
            // txtDeviation
            // 
            this.txtDeviation.CanGrow = false;
            this.txtDeviation.Height = 0.19F;
            this.txtDeviation.Left = 4.375F;
            this.txtDeviation.MultiLine = false;
            this.txtDeviation.Name = "txtDeviation";
            this.txtDeviation.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtDeviation.Text = "Field3";
            this.txtDeviation.Top = 2.0625F;
            this.txtDeviation.Width = 1.125F;
            // 
            // lblSalePrice
            // 
            this.lblSalePrice.Height = 0.19F;
            this.lblSalePrice.HyperLink = null;
            this.lblSalePrice.Left = 2.5625F;
            this.lblSalePrice.Name = "lblSalePrice";
            this.lblSalePrice.Style = "font-family: \'Tahoma\'";
            this.lblSalePrice.Text = "Sale Price:";
            this.lblSalePrice.Top = 1.6875F;
            this.lblSalePrice.Width = 1.6875F;
            // 
            // lblValuation
            // 
            this.lblValuation.Height = 0.19F;
            this.lblValuation.HyperLink = null;
            this.lblValuation.Left = 2.5625F;
            this.lblValuation.Name = "lblValuation";
            this.lblValuation.Style = "font-family: \'Tahoma\'";
            this.lblValuation.Text = "Valuation:";
            this.lblValuation.Top = 1.875F;
            this.lblValuation.Width = 1.6875F;
            // 
            // lblDeviation
            // 
            this.lblDeviation.Height = 0.19F;
            this.lblDeviation.HyperLink = null;
            this.lblDeviation.Left = 2.5625F;
            this.lblDeviation.Name = "lblDeviation";
            this.lblDeviation.Style = "font-family: \'Tahoma\'";
            this.lblDeviation.Text = "Deviation:";
            this.lblDeviation.Top = 2.0625F;
            this.lblDeviation.Width = 1.6875F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1875F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 2.5F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label15.Text = "Sales Ratio Statistics";
            this.Label15.Top = 2.5F;
            this.Label15.Width = 2.5F;
            // 
            // lblMedian
            // 
            this.lblMedian.Height = 0.19F;
            this.lblMedian.HyperLink = null;
            this.lblMedian.Left = 2F;
            this.lblMedian.Name = "lblMedian";
            this.lblMedian.Style = "font-family: \'Tahoma\'";
            this.lblMedian.Text = "Median";
            this.lblMedian.Top = 2.78125F;
            this.lblMedian.Width = 1F;
            // 
            // lblMean
            // 
            this.lblMean.Height = 0.19F;
            this.lblMean.HyperLink = null;
            this.lblMean.Left = 2F;
            this.lblMean.Name = "lblMean";
            this.lblMean.Style = "font-family: \'Tahoma\'";
            this.lblMean.Text = "Mean";
            this.lblMean.Top = 3F;
            this.lblMean.Width = 1F;
            // 
            // lblMidQuart
            // 
            this.lblMidQuart.Height = 0.19F;
            this.lblMidQuart.HyperLink = null;
            this.lblMidQuart.Left = 2F;
            this.lblMidQuart.Name = "lblMidQuart";
            this.lblMidQuart.Style = "font-family: \'Tahoma\'";
            this.lblMidQuart.Text = "Mid-Quartile Mean";
            this.lblMidQuart.Top = 3.21875F;
            this.lblMidQuart.Width = 1.375F;
            // 
            // lblWeighted
            // 
            this.lblWeighted.Height = 0.19F;
            this.lblWeighted.HyperLink = null;
            this.lblWeighted.Left = 2F;
            this.lblWeighted.Name = "lblWeighted";
            this.lblWeighted.Style = "font-family: \'Tahoma\'";
            this.lblWeighted.Text = "Weighted Mean";
            this.lblWeighted.Top = 3.4375F;
            this.lblWeighted.Width = 1.375F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.19F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 2F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-family: \'Tahoma\'";
            this.Label20.Text = "Average Deviation";
            this.Label20.Top = 3.65625F;
            this.Label20.Width = 1.375F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.19F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 2F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-family: \'Tahoma\'";
            this.Label21.Text = "Coefficient of Dispersion";
            this.Label21.Top = 3.875F;
            this.Label21.Width = 1.5625F;
            // 
            // lblStandDev
            // 
            this.lblStandDev.Height = 0.19F;
            this.lblStandDev.HyperLink = null;
            this.lblStandDev.Left = 2F;
            this.lblStandDev.Name = "lblStandDev";
            this.lblStandDev.Style = "font-family: \'Tahoma\'";
            this.lblStandDev.Text = "Standard Deviation";
            this.lblStandDev.Top = 4.09375F;
            this.lblStandDev.Width = 1.5625F;
            // 
            // Label23
            // 
            this.Label23.Height = 0.19F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 2F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-family: \'Tahoma\'";
            this.Label23.Text = "Coefficient of Variation";
            this.Label23.Top = 4.3125F;
            this.Label23.Width = 1.5625F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.19F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 2F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-family: \'Tahoma\'";
            this.Label24.Text = "Price-Related Differential";
            this.Label24.Top = 4.53125F;
            this.Label24.Width = 1.6875F;
            // 
            // txtMedian
            // 
            this.txtMedian.Height = 0.19F;
            this.txtMedian.Left = 4.5F;
            this.txtMedian.Name = "txtMedian";
            this.txtMedian.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtMedian.Text = "Field3";
            this.txtMedian.Top = 2.78125F;
            this.txtMedian.Width = 1F;
            // 
            // txtMean
            // 
            this.txtMean.Height = 0.19F;
            this.txtMean.Left = 4.5F;
            this.txtMean.Name = "txtMean";
            this.txtMean.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtMean.Text = "Field4";
            this.txtMean.Top = 3F;
            this.txtMean.Width = 1F;
            // 
            // txtMidQuart
            // 
            this.txtMidQuart.Height = 0.19F;
            this.txtMidQuart.Left = 4.5F;
            this.txtMidQuart.Name = "txtMidQuart";
            this.txtMidQuart.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtMidQuart.Text = "Field5";
            this.txtMidQuart.Top = 3.21875F;
            this.txtMidQuart.Width = 1F;
            // 
            // txtWeighted
            // 
            this.txtWeighted.Height = 0.19F;
            this.txtWeighted.Left = 4.5F;
            this.txtWeighted.Name = "txtWeighted";
            this.txtWeighted.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtWeighted.Text = "Field6";
            this.txtWeighted.Top = 3.4375F;
            this.txtWeighted.Width = 1F;
            // 
            // txtAveDev
            // 
            this.txtAveDev.Height = 0.19F;
            this.txtAveDev.Left = 4.5F;
            this.txtAveDev.Name = "txtAveDev";
            this.txtAveDev.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtAveDev.Text = "Field7";
            this.txtAveDev.Top = 3.65625F;
            this.txtAveDev.Width = 1F;
            // 
            // txtCoOfDisp
            // 
            this.txtCoOfDisp.Height = 0.19F;
            this.txtCoOfDisp.Left = 4.5F;
            this.txtCoOfDisp.Name = "txtCoOfDisp";
            this.txtCoOfDisp.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtCoOfDisp.Text = "Field8";
            this.txtCoOfDisp.Top = 3.875F;
            this.txtCoOfDisp.Width = 1F;
            // 
            // txtStandDev
            // 
            this.txtStandDev.Height = 0.19F;
            this.txtStandDev.Left = 4.5F;
            this.txtStandDev.Name = "txtStandDev";
            this.txtStandDev.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtStandDev.Text = "Field9";
            this.txtStandDev.Top = 4.09375F;
            this.txtStandDev.Width = 1F;
            // 
            // txtCoOfVar
            // 
            this.txtCoOfVar.Height = 0.19F;
            this.txtCoOfVar.Left = 4.5F;
            this.txtCoOfVar.Name = "txtCoOfVar";
            this.txtCoOfVar.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtCoOfVar.Text = "Field10";
            this.txtCoOfVar.Top = 4.3125F;
            this.txtCoOfVar.Width = 1F;
            // 
            // txtPriceDiff
            // 
            this.txtPriceDiff.Height = 0.19F;
            this.txtPriceDiff.Left = 4.5F;
            this.txtPriceDiff.Name = "txtPriceDiff";
            this.txtPriceDiff.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtPriceDiff.Text = "Field11";
            this.txtPriceDiff.Top = 4.53125F;
            this.txtPriceDiff.Width = 1F;
            // 
            // lblTrended
            // 
            this.lblTrended.Height = 0.19F;
            this.lblTrended.HyperLink = null;
            this.lblTrended.Left = 5.625F;
            this.lblTrended.Name = "lblTrended";
            this.lblTrended.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTrended.Text = "(TRENDED)";
            this.lblTrended.Top = 1.6875F;
            this.lblTrended.Visible = false;
            this.lblTrended.Width = 1.0625F;
            // 
            // lblAdjusted
            // 
            this.lblAdjusted.Height = 0.19F;
            this.lblAdjusted.HyperLink = null;
            this.lblAdjusted.Left = 5.625F;
            this.lblAdjusted.Name = "lblAdjusted";
            this.lblAdjusted.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblAdjusted.Text = "(ADJUSTED)";
            this.lblAdjusted.Top = 1.875F;
            this.lblAdjusted.Visible = false;
            this.lblAdjusted.Width = 1.0625F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMuniName,
            this.Field1,
            this.txtExtractTitle,
            this.txtTime,
            this.txtDate,
            this.txtPage});
            this.PageHeader.Height = 0.4375F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // txtMuniName
            // 
            this.txtMuniName.Height = 0.19F;
            this.txtMuniName.Left = 0.0625F;
            this.txtMuniName.Name = "txtMuniName";
            this.txtMuniName.Style = "font-family: \'Tahoma\'";
            this.txtMuniName.Text = "Field1";
            this.txtMuniName.Top = 0.03125F;
            this.txtMuniName.Width = 1.1875F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.21875F;
            this.Field1.Left = 2F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.Field1.Text = "SALES ANALYSIS REPORT";
            this.Field1.Top = 0.03125F;
            this.Field1.Width = 3.5F;
            // 
            // txtExtractTitle
            // 
            this.txtExtractTitle.Height = 0.19F;
            this.txtExtractTitle.Left = 1.3125F;
            this.txtExtractTitle.Name = "txtExtractTitle";
            this.txtExtractTitle.Style = "font-family: \'Tahoma\'; text-align: center";
            this.txtExtractTitle.Text = "Field2";
            this.txtExtractTitle.Top = 0.25F;
            this.txtExtractTitle.Width = 4.8125F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.19F;
            this.txtTime.Left = 0.0625F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Tahoma\'";
            this.txtTime.Text = "Field1";
            this.txtTime.Top = 0.1875F;
            this.txtTime.Width = 1.1875F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.19F;
            this.txtDate.Left = 6.25F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtDate.Text = "Field1";
            this.txtDate.Top = 0.03125F;
            this.txtDate.Width = 1.1875F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.19F;
            this.txtPage.Left = 6.25F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtPage.Text = "Field1";
            this.txtPage.Top = 0.1875F;
            this.txtPage.Width = 1.1875F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptSalesAnalysis
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.499306F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaleFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLandFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBldgFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumSales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValuation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeviation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSalePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValuation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeviation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMedian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMean)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMidQuart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWeighted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStandDev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMedian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMean)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMidQuart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeighted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAveDev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoOfDisp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStandDev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCoOfVar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPriceDiff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTrended)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAdjusted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtractTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleFactor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandFactor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgFactor;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumSales;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSalePrice;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValuation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeviation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSalePrice;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblValuation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeviation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMedian;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMean;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMidQuart;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWeighted;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStandDev;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMedian;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMean;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMidQuart;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWeighted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAveDev;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoOfDisp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStandDev;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoOfVar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPriceDiff;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTrended;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjusted;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExtractTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
