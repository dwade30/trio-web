﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptLinstingNameLocMap.
	/// </summary>
	public partial class rptListingNameLocMap : BaseSectionReport
	{
		public rptListingNameLocMap()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Name Location Map/Lot";
		}

		public static rptListingNameLocMap InstancePtr
		{
			get
			{
				return (rptListingNameLocMap)Sys.GetInstance(typeof(rptListingNameLocMap));
			}
		}

		protected rptListingNameLocMap _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTemp.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptListingNameLocMap	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsTemp = new clsDRWrapper();
		// Dim dbTemp As DAO.Database
		object varAccount;
		int intPage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			nextaccount:
			;
			if (rsTemp.EndOfFile())
				return;
			string strTemp = "";
			txtAccount.Text = Strings.Format(rsTemp.Get_Fields_Int32("RSAccount"), "00000");
			txtCard.Text = Strings.Format(rsTemp.Get_Fields_Int32("RSCard"), "000");
			txtName.Text = Strings.Trim(rsTemp.Get_Fields_String("RSName") + " ");
			if (Information.IsNumeric(rsTemp.Get_Fields_String("rslocnumalph") + ""))
			{
				strTemp = Conversion.Str(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_String("rslocnumalph") + "")));
				if (Conversion.Val(rsTemp.Get_Fields_String("rslocnumalph") + "") == 0)
					strTemp = " ";
			}
			else
			{
				strTemp = rsTemp.Get_Fields_String("rslocnumalph") + "";
			}
			strTemp += " " + rsTemp.Get_Fields_String("rslocapt") + "";
			txtLocation.Text = Strings.Trim(strTemp + " " + rsTemp.Get_Fields_String("RSLOCStreet") + " ");
			txtMap.Text = Strings.Trim(rsTemp.Get_Fields_String("RSMAPLOT") + " ");
			varAccount = rsTemp.Get_Fields(modPrintRoutines.Statics.gstrFieldName);
			if (!rsTemp.EndOfFile())
				rsTemp.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			if (rptListingNameLocMap.InstancePtr.Document.Printer != null)
			{
				//rptListingNameLocMap.InstancePtr.Document.Printer.PrintQuality = ddPQMedium;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtCaption.Text = "Real Estate";
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			if (Strings.Trim(this.Document.Printer.PrinterName) != string.Empty)
			{
				//this.Document.Printer.RenderMode = 1;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			intPage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
		}

		public void Start(bool boolByExtract, bool boolByAccount)
		{
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = rsTemp.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			// corey
			// If rptListingNameLocMap.Printer <> "" Then
			// rptListingNameLocMap.Printer.PrintQuality = ddPQMedium
			// End If
			// rptListingNameLocMap.Zoom = 100
			int lngID;
			string strSQL = "";
			if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
			{
				txtCaption2.Text = "Account List by Account";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
			{
				txtCaption2.Text = "Account List by Location";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSMAPLOT")
			{
				txtCaption2.Text = "Account List by Map/Lot";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSNAME")
			{
				txtCaption2.Text = "Account List by Name";
			}
			if (modGlobalVariables.Statics.boolRange)
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
				{
					txtCaption2.Text = txtCaption2.Text + " (" + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " - " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + ")";
				}
				else
				{
					txtCaption2.Text = txtCaption2.Text + " (" + modPrintRoutines.Statics.gstrMinAccountRange + " - " + modPrintRoutines.Statics.gstrMaxAccountRange + ")";
				}
			}
			lngID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolByExtract)
			{
				if (boolByAccount)
				{
					strSQL = strMasterJoin + " where rsaccount in (select extracttable.accountnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " + FCConvert.ToString(lngID) + ") AND rsdeleted = 0 ";
				}
				else
				{
					// strSQL = "select  master.* from (master inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and master.rsdeleted = 0"
					strSQL = "select  mj.* from (" + strMasterJoinQuery + " inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngID) + " and mj.rsdeleted = 0";
				}
				rsTemp.OpenRecordset("select TITLE FROM EXTRACT WHERe reportnumber = 0 and userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
				if (!rsTemp.EndOfFile())
				{
					if (FCConvert.ToString(rsTemp.Get_Fields_String("title")) != string.Empty)
					{
						txtCaption2.Text = FCConvert.ToString(rsTemp.Get_Fields_String("title"));
					}
				}
			}
			else
			{
				strSQL = strMasterJoin + " where rsdeleted = 0";
			}
			if (boolByAccount)
			{
				strSQL += " and rscard = 1";
			}
			if (modGlobalVariables.Statics.boolRange)
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " + modPrintRoutines.Statics.gstrFieldName + " <= " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + " order by " + modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
				}
				else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' order by " + modPrintRoutines.Statics.gstrFieldName + ",CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float)  ", modGlobalVariables.strREDatabase);
				}
				else
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,RSCard", modGlobalVariables.strREDatabase);
				}
			}
			else
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
				{
					rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float)  ", modGlobalVariables.strREDatabase);
				}
				else
				{
					if (Strings.LCase(modPrintRoutines.Statics.gstrFieldName) != "rsaccount")
					{
						rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,rscard", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",rscard", modGlobalVariables.strREDatabase);
					}
				}
			}
			// Me.Show
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "NameLocMap");
		}

		private void rptListingNameLocMap_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptListingNameLocMap properties;
			//rptListingNameLocMap.Caption	= "Name Location Map/Lot";
			//rptListingNameLocMap.Icon	= "rptListingNameLocMap.dsx":0000";
			//rptListingNameLocMap.Left	= 0;
			//rptListingNameLocMap.Top	= 0;
			//rptListingNameLocMap.Width	= 19080;
			//rptListingNameLocMap.Height	= 12990;
			//rptListingNameLocMap.StartUpPosition	= 3;
			//rptListingNameLocMap.SectionData	= "rptListingNameLocMap.dsx":058A;
			//End Unmaped Properties
		}
	}
}
