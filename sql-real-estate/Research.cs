﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmRESearch.
	/// </summary>
	public partial class frmRESearch : BaseForm
	{
		public frmRESearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRESearch InstancePtr
		{
			get
			{
				return (frmRESearch)Sys.GetInstance(typeof(frmRESearch));
			}
		}

		protected frmRESearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string can = "";
		// Dim FormLoaded As String * 1
		int ONE;
		char[] REC = new char[24];
		// Dim SORTREC$(1000)
		char[] STRACCTNUM = new char[6];
		// Dim SELECTNUM(1000)
		// Dim STARTM(1000)
		// Dim STARTN(1000)
		// Dim STARTW(1000)
		string ACN = "";
		string COMP1 = "";
		string COMP2 = "";
		string COMP3 = "";
		string COMP4 = "";
		string DISP1 = "";
		string DISP2 = "";
		string DISP3 = "";
		string dispdate = "";
		string DISPLOC = "";
		string DISPMAP = "";
		string DISPNAME = "";
		string FILE = "";
		string fl = "";
		int i;
		string Inrec = "";
		int IR;
		int j;
		int JR;
		int k;
		int KOUNT;
		int L;
		int LASTRECNUM;
		int ItemCnt;
		string m = "";
		int MAXFILES;
		int n;
		string NVFLAG = "";
		int PORTS;
		int R;
		string rc = "";
		int recnum;
		string SH = "";
		int SPAN;
		int WINDW;
		int x;
		int XX;
		// Dim ZERO%
		string strSellerName;
		private cSettingsController setCont = new cSettingsController();

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			frmRESearch.InstancePtr.Unload();
		}

		private void frmRESearch_Activated(object sender, System.EventArgs e)
		{
			if (modMDIParent.FormExist(this))
				return;
			if (can == "N")
			{
				FCGlobal.Screen.MousePointer = 0;
				MessageBox.Show("There were no matches found for your search.", "No Matches Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				frmRESearch.InstancePtr.Unload();
			}
			FCGlobal.Screen.MousePointer = 0;
			ResizeSearchGrid();
		}
		// BW MOVED :from Form_KeyUp
		// KeyUp registering on all open forms... escape key
		// being carried through multiple forms... BAD
		private void frmRESearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				can = "0";
				KeyCode = (Keys)0;
				frmRESearch.InstancePtr.Unload();
			}
		}

		private void frmRESearch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRESearch properties;
			//frmRESearch.ScaleWidth	= 9600;
			//frmRESearch.ScaleHeight	= 8040;
			//frmRESearch.LinkTopic	= "Form1";
			//frmRESearch.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this);
			if (modGlobalVariables.Statics.boolPreviousOwnerRecords)
			{
				REBL02_3200_DISPLAY_RECORDS(modGlobalVariables.Statics.searchRS);
				return;
			}
			can = "0";
			if (modGNBas.Statics.LongScreenSearch == true)
				goto LongScreenSearchTag;
			LongScreenSearchTag:
			;
			modGNBas.Statics.SEQ = FCConvert.ToString(modGNBas.Statics.Resp1);
			modGNBas.Statics.SEARCH = FCConvert.ToString(modGNBas.Statics.Resp2);
			if (modGNBas.Statics.SEQ != "R")
			{
				lblSearch.Text = modGNBas.Statics.SEARCH;
			}
			// 
			if (modGNBas.Statics.LongScreenSearch != true)
			{
			}
			else
			{
				REBL02_3200_DISPLAY_RECORDS(modGlobalVariables.Statics.searchRS);
				// Call FromLongScreenSearch
			}
			// Call SetFixedSize(Me, True)
		}
		// Private Sub REBL02_1000_INITIALIZE()
		//
		//
		// ZERO% = 0
		// End Sub
		private void REBL02_1500_GET_SEQUENCE()
		{
		}

		private void REBL02_3000_LOCATE_RECORDS()
		{
			if (!(modGlobalVariables.Statics.searchRS.BeginningOfFile() && modGlobalVariables.Statics.searchRS.EndOfFile()))
			{
				modGlobalVariables.Statics.searchRS.MoveFirst();
				while (!modGlobalVariables.Statics.searchRS.EndOfFile())
				{
					if (FCConvert.ToInt32(modGlobalVariables.Statics.searchRS.Get_Fields_Int32("rsaccount")) == modGlobalVariables.Statics.gintLastAccountNumber)
					{
						break;
					}
					else
					{
						modGlobalVariables.Statics.searchRS.MoveNext();
					}
				}
				if (!modGlobalVariables.Statics.searchRS.EndOfFile())
				{
					modGlobalVariables.Statics.searchRS.MoveNext();
					if (!modGlobalVariables.Statics.searchRS.EndOfFile())
					{
						while (!modGlobalVariables.Statics.searchRS.EndOfFile())
						{
							if (!FCConvert.ToBoolean(modGlobalVariables.Statics.searchRS.Get_Fields_Boolean("rsdeleted")))
							{
								// found next valid account
								modGlobalVariables.Statics.CurrentAccount = FCConvert.ToInt32(modGlobalVariables.Statics.searchRS.Get_Fields_Int32("rsaccount"));
								modGlobalVariables.Statics.gintLastAccountNumber = modGlobalVariables.Statics.CurrentAccount;
								// CurrentCard = 1
								modGNBas.Statics.gintCardNumber = 1;
								modGlobalVariables.Statics.ActiveCard = 1;
								clsDRWrapper temp = modDataTypes.Statics.MR;
								modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.CurrentAccount, 1);
								modDataTypes.Statics.MR = temp;
								temp = modDataTypes.Statics.CMR;
								modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.CurrentAccount, 1);
								modDataTypes.Statics.CMR = temp;
								temp = modDataTypes.Statics.DWL;
								modREMain.OpenDwellingTable(ref temp, modGlobalVariables.Statics.CurrentAccount, 1);
								modDataTypes.Statics.DWL = temp;
								temp = modDataTypes.Statics.OUT;
								modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.CurrentAccount, 1);
								modDataTypes.Statics.OUT = temp;
								// Call OpenIncomeTable(INCVB, CurrentAccount)
								// Call OpenWWKTable(WWK)
								if (modGlobalVariables.Statics.wmainresponse == 1)
								{
									frmREProperty.InstancePtr.Show(App.MainForm);
									return;
								}
								else
								{
									frmSHREBLUpdate.InstancePtr.Show(App.MainForm);
									return;
								}
							}
							else
							{
								modGlobalVariables.Statics.searchRS.MoveNext();
							}
						}
					}
					else
					{
						modGlobalVariables.Statics.searchRS.MoveLast();
						return;
					}
				}
			}
		}
		//
		private void REBL02_3200_DISPLAY_RECORDS(clsDRWrapper rs = null)
		{
			// vbPorter upgrade warning: NEWREC As FixedString	OnWrite(string)
			char[] NEWREC = new char[150];
			string ItemList = "";
			string strHoldResp2;
			// vbPorter upgrade warning: intResponse As Variant --> As int
			int intResponse = 0;
			string strDel = "";
			string strDate = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			DateTime dttemp;
			string[] strAry = null;
			string strWhere = "";
			string strSQL = "";
			string strOrderBy = "";
			string strTemp;
			string strSSDBName;
			string strMasterJoin;
			string strREFullDBName;
			bool boolShowAll = false;
			// Dim strTemp As String
			try
			{
				// On Error GoTo ErrorRoutine
				strTemp = setCont.GetSettingValue("AccountSearchShowAll", "RealEstate", "User", FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserID()), "");
				if (strTemp != string.Empty)
				{
					if (FCConvert.CBool(strTemp))
					{
						boolShowAll = true;
					}
					else
					{
						boolShowAll = false;
					}
				}
				else
				{
					boolShowAll = false;
				}
				strTemp = "";
				SetupSearchGrid();
				strSSDBName = clsLoad.Get_GetFullDBName("SystemSettings");
				strREFullDBName = clsLoad.Get_GetFullDBName("RealEstate");
				strMasterJoin = modREMain.GetMasterJoin();
				if (modGlobalVariables.Statics.boolPreviousOwnerRecords)
				{
					lblSearchCount.Visible = false;
					switch (modGlobalVariables.Statics.intSearchPosition)
					{
						case 0:
							{
								lblSearch.Visible = false;
								Label55.Visible = false;
								clsLoad.OpenRecordset("select * from previousowner where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " order by saledate,name", modGlobalVariables.strREDatabase);
								break;
							}
						case 1:
							{
								lblSearch.Text = FCConvert.ToString(modGNBas.Statics.Resp2);
								if (modGNBas.Statics.Resp1 == "N")
								{
									clsLoad.OpenRecordset("select * from previousowner where name like '%" + modGNBas.Statics.Resp2 + "%' or secowner like '%" + modGNBas.Statics.Resp2 + "%' order by name", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.Resp1 == "M")
								{
									clsLoad.OpenRecordset("select previousowner.*,master.rsmaplot from previousowner inner join master on (previousowner.account = master.rsaccount) where rscard = 1 and rsmaplot like '%" + modGNBas.Statics.Resp2 + "%' order by name", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.Resp1 == "R")
								{
									strSQL = frmGetSaleRange.InstancePtr.Init();
									if (Strings.Trim(strSQL) != "")
									{
										clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
									}
									else
									{
										return;
									}
								}
								else if (modGNBas.Statics.Resp1 == "S")
								{
									if (Information.IsDate(modGNBas.Statics.Resp2))
									{
										clsLoad.OpenRecordset("select * from previousowner where saledate = '" + modGNBas.Statics.Resp2 + "' order by saledate,name", modGlobalVariables.strREDatabase);
									}
									else
									{
										MessageBox.Show("Invalid Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
								}
								// contains
								break;
							}
						case 2:
							{
								// starts with
								lblSearch.Text = FCConvert.ToString(modGNBas.Statics.Resp2);
								if (modGNBas.Statics.Resp1 == "N")
								{
									clsLoad.OpenRecordset("select * from previousowner where name between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' or secowner between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' order by name", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.Resp1 == "M")
								{
									clsLoad.OpenRecordset("select previousowner.*,master.rsmaplot from previousowner inner join master on (previousowner.account = master.rsaccount) where rscard = 1 and rsmaplot between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzz' order by name", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.Resp1 == "R")
								{
									strSQL = frmGetSaleRange.InstancePtr.Init();
									if (Strings.Trim(strSQL) != "")
									{
										clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
									}
									else
									{
										return;
									}
								}
								else if (modGNBas.Statics.Resp1 == "S")
								{
									if (Information.IsDate(modGNBas.Statics.Resp2))
									{
										clsLoad.OpenRecordset("select * from previousowner where saledate = '" + modGNBas.Statics.Resp2 + "' order by saledate,name", modGlobalVariables.strREDatabase);
									}
									else
									{
										MessageBox.Show("Invalid Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
								}
								break;
							}
						case 3:
							{
								// ends with
								lblSearch.Text = FCConvert.ToString(modGNBas.Statics.Resp2);
								if (modGNBas.Statics.Resp1 == "N")
								{
									clsLoad.OpenRecordset("select * from previousowner where name like '%" + modGNBas.Statics.Resp2 + "' or secowner like '%" + modGNBas.Statics.Resp2 + "' order by name", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.Resp1 == "M")
								{
									clsLoad.OpenRecordset("select previousowner.*,master.rsmaplot from previousowner inner join master on (previousowner.account = master.rsaccount) where rscard = 1 and rsmaplot like '%" + modGNBas.Statics.Resp2 + "' order by name", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.Resp1 == "R")
								{
									strSQL = frmGetSaleRange.InstancePtr.Init();
									if (Strings.Trim(strSQL) != "")
									{
										clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
									}
									else
									{
										return;
									}
								}
								else if (modGNBas.Statics.Resp1 == "S")
								{
									if (Information.IsDate(modGNBas.Statics.Resp2))
									{
										clsLoad.OpenRecordset("select * from previousowner where saledate = '" + modGNBas.Statics.Resp2 + "' order by saledate,name", modGlobalVariables.strREDatabase);
									}
									else
									{
										MessageBox.Show("Invalid Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
								}
								break;
							}
					}
					//end switch
					while (!clsLoad.EndOfFile())
					{
                        if (Information.IsDate(clsLoad.Get_Fields("saledate")))
                        { 
						    dttemp = clsLoad.Get_Fields_DateTime("saledate");
						    if (dttemp.ToOADate() != 0)
						    {
							    strDate = Strings.Format(dttemp, "MM/dd/yyyy");
						    }
						    else
						    {
							    strDate = "";
						    }
                        }
                        else
                        {
                            strDate = "";
                        }
                        // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                        Grid.AddItem(clsLoad.Get_Fields_String("name") + "\t" + clsLoad.Get_Fields_String("secowner") + "\t" + clsLoad.Get_Fields("account") + "\t" + strDate + "\t" + clsLoad.Get_Fields_Int32("id"));
						clsLoad.MoveNext();
					}
					return;
				}
				ItemCnt = 0;
				strHoldResp2 = FCConvert.ToString(modGNBas.Statics.Resp2);
				if (!modGlobalVariables.Statics.boolShowDel)
				{
					Shape1.Visible = false;
					Label2.Visible = false;
					strDel = " and not rsdeleted = 1 ";
				}
				else
				{
					strDel = "";
				}
				if (FCConvert.ToString(modGNBas.Statics.Resp1) == "B")
				{
					strAry = Strings.Split(FCConvert.ToString(modGNBas.Statics.Resp2), ",", -1, CompareConstants.vbTextCompare);
					if (Conversion.Val(strAry[1]) > 0)
					{
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							// Call searchRS.OpenRecordset("select * from master inner join bookpage on (master.rsaccount = bookpage.account) and (master.rscard = bookpage.card) where book = " & Val(strAry(0)) & " and page = " & Val(strAry(1)) & strDel & " order by book,page", strREDatabase)
							strSQL = strMasterJoin + " inner join " + strREFullDBName + ".dbo.bookpage on (mparty.rsaccount = " + strREFullDBName + ".dbo.bookpage.account) and (mparty.rscard = " + strREFullDBName + ".dbo.bookpage.card) where book = '" + strAry[0] + "' and page = '" + strAry[1] + "' " + strDel + " order by book,page";
							modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
						}
						else
						{
							modGlobalVariables.Statics.searchRS.OpenRecordset("select * from srmaster inner join srbookpage on (srmaster.rsaccount = srbookpage.account) and (srmaster.rscard = srbookpage.card) where book = '" + strAry[0] + "' and page = '" + strAry[1] + "'" + strDel + " order by book,page", modGlobalVariables.strREDatabase);
						}
					}
					else
					{
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							// Call searchRS.OpenRecordset("select * from master inner join bookpage on (master.rsaccount = bookpage.account) and (master.rscard = bookpage.card) where book = " & Val(strAry(0)) & strDel & " order by book,page", strREDatabase)
							strSQL = strMasterJoin + " inner join " + strREFullDBName + ".dbo.bookpage on (mparty.rsaccount = " + strREFullDBName + ".dbo.bookpage.account) and (mparty.rscard = " + strREFullDBName + ".dbo.bookpage.card) where book = '" + strAry[0] + "' order by book,page";
							modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
						}
						else
						{
							modGlobalVariables.Statics.searchRS.OpenRecordset("select * from srmaster inner join srbookpage on (srmaster.rsaccount = srbookpage.account) and (srmaster.rscard = srbookpage.card) where book = '" + strAry[0] + "' " + strDel + " order by book,page", "", FCConvert.ToInt32(modGlobalVariables.strREDatabase));
						}
					}
				}
				else if (FCConvert.ToString(modGNBas.Statics.Resp1) == "Open1" || FCConvert.ToString(modGNBas.Statics.Resp1) == "Open2")
				{
					strTemp = "pi" + modGNBas.Statics.Resp1;
					if (FCConvert.ToString(modGNBas.Statics.Resp2) != " ")
					{
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							// strSQL = "select * from master where " & strTemp & " = " & Val(Resp2)
							strSQL = strMasterJoin + " where " + strTemp + " = " + FCConvert.ToString(Conversion.Val(modGNBas.Statics.Resp2));
						}
						else
						{
							strSQL = "select * from srmaster where " + strTemp + " = " + FCConvert.ToString(Conversion.Val(modGNBas.Statics.Resp2));
						}
					}
					else
					{
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							strSQL = "select * from master where " + strTemp + " IS NULL";
						}
						else
						{
							strSQL = "Select * from srmaster where " + strTemp + " IS NULL ";
						}
					}
					strSQL += strDel;
					strOrderBy = " order by rsaccount,rscard";
					strSQL += strOrderBy;
					modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				}
				else if (modGNBas.Statics.Resp1 == "L")
				{
					// location
					strAry = Strings.Split(FCConvert.ToString(modGNBas.Statics.Resp2), ",", -1, CompareConstants.vbTextCompare);
					modGNBas.Statics.Resp2 = strAry[1];
					if (FCConvert.ToString(modGNBas.Statics.Resp2) != " ")
					{
						switch (modGlobalVariables.Statics.intSearchPosition)
						{
							case 1:
								{
									modGNBas.Statics.Resp2 = "'%" + modGNBas.Statics.Resp2 + "%'";
									break;
								}
							case 2:
								{
									// Resp2 = "'" & Resp2 & "*'"
									break;
								}
							case 3:
								{
									modGNBas.Statics.Resp2 = "'%" + modGNBas.Statics.Resp2 + "'";
									break;
								}
						}
						//end switch
					}
					// strOrderBy = "order by tbl1.rslocstreet,cast(rtrim(tbl1.rslocnumalph) as int)"
					strOrderBy = "order by tbl1.rslocstreet,tbl1.rslocnumalph";
					if (Conversion.Val(strAry[0]) > 0)
					{
						// number and street
						// strWhere = " where isnumeric(rslocnumalph) = 1 and cast(rslocnumalph as int) = " & Val(strAry(0)) & " and "
						strWhere = " where rslocnumalph  = '" + FCConvert.ToString(Conversion.Val(strAry[0])) + "' and ";
					}
					else
					{
						// just street
						strWhere = " where  ";
					}
					if (modGlobalVariables.Statics.intSearchPosition == 2)
					{
						strWhere += " rslocstreet between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' ";
					}
					else
					{
						strWhere += " rslocstreet like " + modGNBas.Statics.Resp2 + " ";
					}
					strWhere += strDel;
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						if (FCConvert.ToString(modGNBas.Statics.Resp2) == " ")
						{
							strOrderBy = " order by rsaccount ";
							strSQL = strMasterJoin + " where RSLOCSTREET = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and RSCard = 1 ";
						}
						else
						{
							// strSQL = "select id,tbl1.rslocstreet as rslocstreet,tbl1.rslocnumalph as rslocnumalph,tbl1.rslocapt as rslocapt,rsaccount,rscard,rsdeleted,rsname,rssecowner,rspreviousmaster,rsmaplot,saledate  from master inner join (select distinct rsaccount as account,rslocstreet,rslocnumalph,rslocapt from master " & strWhere & " ) as tbl1 on (tbl1.account = master.rsaccount) where master.rscard = 1 " & strDel
							strSQL = strMasterJoin + " inner join (select distinct rsaccount as account,rslocstreet,rslocnumalph,rslocapt from " + strREFullDBName + ".dbo.master " + strWhere + " ) as tbl1 on (tbl1.account = mparty.rsaccount) where mparty.rscard = 1 " + strDel;
						}
					}
					else
					{
						if (FCConvert.ToString(modGNBas.Statics.Resp2) == " ")
						{
							strOrderBy = " order by rsaccount ";
							strSQL = "SELECT * FROM srMaster where RSLOCSTREET = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and RSCard = 1 order by RSaccount";
						}
						else
						{
							strSQL = "select id,tbl1.rslocstreet as rslocstreet,tbl1.rslocnumalph as rslocnumalph,tbl1.rslocapt as rslocapt,rsaccount,rscard,rsdeleted,rsname,rssecowner,rspreviousmaster,rsmaplot,saledate  from srmaster inner join (select distinct rsaccount as account,rslocstreet,rslocnumalph,rslocapt from srmaster " + strWhere + " ) as tbl1 on (tbl1.account = srmaster.rsaccount) where srmaster.rscard = 1 " + strDel;
						}
					}
					strSQL += strOrderBy;
					modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				}
				else
				{
					if (FCConvert.ToString(modGNBas.Statics.Resp2) != " ")
					{
						switch (modGlobalVariables.Statics.intSearchPosition)
						{
							case 1:
								{
									modGNBas.Statics.Resp2 = "'%" + modGNBas.Statics.Resp2 + "%'";
									break;
								}
							case 2:
								{
									// Resp2 = "'" & Resp2 & "*'"
									break;
								}
							case 3:
								{
									modGNBas.Statics.Resp2 = "'%" + modGNBas.Statics.Resp2 + "'";
									break;
								}
						}
						//end switch
					}
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						if (FCConvert.ToString(modGNBas.Statics.Resp2) == " ")
						{
							if (modGNBas.Statics.SEQ == "L")
							{
								// Call searchRS.OpenRecordset("SELECT * FROM Master where RSLOCSTREET = '" & Resp2 & "' " & strDel & " and RSCard = 1 order by RSaccount", strREDatabase)
								strSQL = strMasterJoin + " where RSLOCSTREET = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and RSCard = 1 order by RSaccount";
								modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
							}
							else if (modGNBas.Statics.SEQ == "N")
							{
								// Call searchRS.OpenRecordset("SELECT * FROM Master where RSName = '" & Resp2 & "' " & strDel & " and RSCard = 1 order by RSaccount", strREDatabase)
								strSQL = strMasterJoin + " where RSName = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and RSCard = 1 order by RSaccount";
								modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
							}
							else if (modGNBas.Statics.SEQ == "M")
							{
								// Call searchRS.OpenRecordset("SELECT * FROM Master where RSMAPLOT = '" & Resp2 & "' " & strDel & " and RSCard = 1 order by RSaccount", strREDatabase)
								strSQL = strMasterJoin + " where RSMAPLOT = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and RSCard = 1 order by RSaccount";
								modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
							}
							else if (modGNBas.Statics.SEQ == "Ref1")
							{
								// Call searchRS.OpenRecordset("select * from master where rsref1 = '" & Resp2 & "' " & strDel & " and rscard = 1 order by rsaccount", strREDatabase)
								strSQL = strMasterJoin + " where rsref1 = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and rscard = 1 order by rsaccount";
								modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
							}
							else if (modGNBas.Statics.SEQ == "Ref2")
							{
								// Call searchRS.OpenRecordset("select * from master where rsref2 = '" & Resp2 & "' " & strDel & " and rscard = 1 order by rsaccount", strREDatabase)
								strSQL = strMasterJoin + " where rsref2 = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and rscard = 1 order by rsaccount";
								modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
							}
						}
						else
						{
							if (modGlobalVariables.Statics.intSearchPosition == 2)
							{
								if (modGNBas.Statics.SEQ == "L")
								{
									// Set searchRS = secDB.OpenRecordset("SELECT * FROM Master where RSLOCSTREET between '" & Resp2 & "' and '" & Resp2 & "zzzz' " & strDel & " and RSCard = 1 order by RSLOCSTREET , val(trim(rslocnumalph) & """")",strredatabase)
									// Call searchRS.OpenRecordset("select id,tbl1.rslocstreet as rslocstreet,tbl1.rslocnumalph as rslocnumalph,tbl1.rslocapt as rslocapt,rsaccount,rscard,rsdeleted,rsname,rssecowner,rspreviousmaster,rsmaplot,saledate  from master inner join (select distinct rsaccount as account,rslocstreet,rslocnumalph,rslocapt from master where rslocstreet between '" & Resp2 & "' and '" & Resp2 & "zzzz' " & strDel & " ) as tbl1 on (tbl1.account = master.rsaccount) where master.rscard = 1 " & strDel & " order by tbl1.rslocstreet,val(trim(tbl1.rslocnumalph & '') & '')", strREDatabase)
									strSQL = strMasterJoin + " inner join (select distinct rsaccount as account,rslocstreet,rslocnumalph,rslocapt from " + strREFullDBName + ".dbo.master where rslocstreet between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' " + strDel + " ) as tbl1 on (tbl1.account = mparty.rsaccount) where mparty.rscard = 1 " + strDel + " order by tbl1.rslocstreet,tbl1.rslocnumalph";
									modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "N")
								{
									if (modGlobalVariables.Statics.boolIncPrevOwner)
									{
										// Call searchRS.OpenRecordset("SELECT * FROM Master where (RSName between '" & Resp2 & "' and '" & Resp2 & "zzzz' or rssecowner between '" & Resp2 & "' and '" & Resp2 & "zzzz'    or rspreviousmaster between '" & Resp2 & "' and '" & Resp2 & "zzzz') " & strDel & "  and RSCard = 1 order by RSName", strREDatabase)
										strSQL = strMasterJoin + " where (RSName between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' or rssecowner between  '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' or rspreviousmaster between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz') " + strDel + "  and RSCard = 1 order by RSName";
										modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
									}
									else
									{
										// Call searchRS.OpenRecordset("SELECT * FROM Master where (RSName between '" & Resp2 & "' and '" & Resp2 & "zzzz' or rssecowner between  '" & Resp2 & "' and '" & Resp2 & "zzzz') " & strDel & "  and RSCard = 1 order by RSName", strREDatabase)
										// strSQL = "select master.*,  from master left join " & strSSDBName & ".dbo.PartyAndAddressView on (master.ownerpartyid = " & strSSDBName & ".dbo.PartyAndAddressView.PartyID) where "
										strSQL = strMasterJoin + " where (RSName between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' or rssecowner between  '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz') " + strDel + "  and RSCard = 1 order by RSName";
										modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
									}
								}
								else if (modGNBas.Statics.SEQ == "M")
								{
									// Set searchRS = secDB.OpenRecordset("SELECT * FROM Master where RSMAPLOT between '" & Resp2 & "' and '" & Resp2 & "zzzz' " & strDel & " and RSCard = 1 order by RSMAPLOT",strredatabase)
									// Call searchRS.OpenRecordset("SELECT * FROM Master where RSMAPLOT like '" & Resp2 & "*' " & strDel & " and RSCard = 1 order by RSMAPLOT", strREDatabase)
									strSQL = strMasterJoin + " where RSMAPLOT like '" + modGNBas.Statics.Resp2 + "%' " + strDel + " and RSCard = 1 order by RSMAPLOT";
									modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "Ref1")
								{
									// Call searchRS.OpenRecordset("SELECT * FROM Master where RSref1 like '" & Resp2 & "*' " & strDel & " and RSCard = 1 order by RSref1", strREDatabase)
									strSQL = strMasterJoin + "  where RSref1 like '" + modGNBas.Statics.Resp2 + "%' " + strDel + " and RSCard = 1 order by RSref1";
									modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "Ref2")
								{
									// Call searchRS.OpenRecordset("SELECT * FROM Master where RSref2 like '" & Resp2 & "*' " & strDel & " and RSCard = 1 order by RSref2", strREDatabase)
									strSQL = strMasterJoin + "  where RSref2 like '" + modGNBas.Statics.Resp2 + "%' " + strDel + " and RSCard = 1 order by RSref2";
									modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
								}
							}
							else
							{
								if (modGNBas.Statics.SEQ == "L")
								{
									// Set searchRS = secDB.OpenRecordset("SELECT * FROM Master where RSLOCSTREET like " & Resp2 & " " & strDel & " and RSCard = 1 order by RSLOCSTREET , val(trim(rslocnumalph) & """")",strredatabase)
									// Call searchRS.OpenRecordset("select id,tbl1.rslocstreet as rslocstreet,tbl1.rslocnumalph as rslocnumalph,tbl1.rslocapt as rslocapt,rsaccount,rscard,rsdeleted,rsname,rssecowner,rspreviousmaster,rsmaplot,saledate  from master inner join (select distinct rsaccount as account,rslocstreet,rslocnumalph,rslocapt from master where rslocstreet like " & Resp2 & " " & strDel & " ) as tbl1 on (tbl1.account = master.rsaccount) where master.rscard = 1 " & strDel & " order by tbl1.rslocstreet,val(trim(tbl1.rslocnumalph & '') & '')", strREDatabase)
									strSQL = strMasterJoin + " inner join (select distinct rsaccount as account,rslocstreet,rslocnumalph,rslocapt from " + strREFullDBName + ".dbo.master where rslocstreet like " + modGNBas.Statics.Resp2 + " " + strDel + " ) as tbl1 on (tbl1.account = mparty.rsaccount) where mparty.rscard = 1 " + strDel + " order by tbl1.rslocstreet,tbl1.rslocnumalph";
									modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "N")
								{
									if (modGlobalVariables.Statics.boolIncPrevOwner)
									{
										// Call searchRS.OpenRecordset("SELECT * FROM Master where (RSName like " & Resp2 & " or rssecowner like " & Resp2 & " or rspreviousmaster like " & Resp2 & ") " & strDel & "  and RSCard = 1 order by RSName", strREDatabase)
										strSQL = strMasterJoin + " where (RSName like " + modGNBas.Statics.Resp2 + " or rssecowner like " + modGNBas.Statics.Resp2 + " or rspreviousmaster like " + modGNBas.Statics.Resp2 + ") " + strDel + "  and RSCard = 1 order by RSName";
										modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
									}
									else
									{
										// Call searchRS.OpenRecordset("SELECT * FROM Master where (RSName like " & Resp2 & " or rssecowner like " & Resp2 & ") " & strDel & "  and RSCard = 1 order by RSName", strREDatabase)
										strSQL = strMasterJoin + " where (RSName like " + modGNBas.Statics.Resp2 + " or rssecowner like " + modGNBas.Statics.Resp2 + ") " + strDel + "  and RSCard = 1 order by RSName";
										modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
									}
								}
								else if (modGNBas.Statics.SEQ == "M")
								{
									// Call searchRS.OpenRecordset("SELECT * FROM Master where RSMAPLOT like " & Resp2 & " " & strDel & " and RSCard = 1 order by RSMAPLOT", strREDatabase)
									strSQL = strMasterJoin + " where RSMAPLOT like " + modGNBas.Statics.Resp2 + " " + strDel + " and RSCard = 1 order by RSMAPLOT";
									modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "Ref1")
								{
									// Call searchRS.OpenRecordset("SELECT * FROM Master where rsref1 like " & Resp2 & " " & strDel & " and RSCard = 1 order by RSref1", strREDatabase)
									strSQL = strMasterJoin + " where rsref1 like " + modGNBas.Statics.Resp2 + " " + strDel + " and RSCard = 1 order by RSref1";
									modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "Ref2")
								{
									// Call searchRS.OpenRecordset("SELECT * FROM Master where rsref2 like " & Resp2 & " " & strDel & " and RSCard = 1 order by RSref2", strREDatabase)
									strSQL = strMasterJoin + "  where rsref2 like " + modGNBas.Statics.Resp2 + " " + strDel + " and RSCard = 1 order by RSref2";
									modGlobalVariables.Statics.searchRS.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
								}
							}
						}
						// if resp2 = " "
					}
					else
					{
						if (FCConvert.ToString(modGNBas.Statics.Resp2) == " ")
						{
							if (modGNBas.Statics.SEQ == "L")
							{
								modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSLOCSTREET = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and RSCard = 1 order by RSaccount", modGlobalVariables.strREDatabase);
							}
							else if (modGNBas.Statics.SEQ == "N")
							{
								modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSName = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and RSCard = 1 order by RSaccount", modGlobalVariables.strREDatabase);
							}
							else if (modGNBas.Statics.SEQ == "M")
							{
								modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSMAPLOT = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and RSCard = 1 order by RSaccount", modGlobalVariables.strREDatabase);
							}
							else if (modGNBas.Statics.SEQ == "Ref1")
							{
								modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSref1 = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and RSCard = 1 order by RSaccount", modGlobalVariables.strREDatabase);
							}
							else if (modGNBas.Statics.SEQ == "Ref2")
							{
								modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSref2 = '" + modGNBas.Statics.Resp2 + "' " + strDel + " and RSCard = 1 order by RSaccount", modGlobalVariables.strREDatabase);
								// Case "A"
								// Set searchRS = secDB.OpenRecordset("select * from srmaster where rsaccount = '" & Resp2 & "' order by saledate")
							}
							else if ((modGNBas.Statics.SEQ == "S") || (modGNBas.Statics.SEQ == "R"))
							{
								modGlobalVariables.Statics.searchRS.OpenRecordset("select * from srmaster where saledate = '" + modGNBas.Statics.Resp2 + "' " + strDel + " order by rsaccount", modGlobalVariables.strREDatabase);
							}
						}
						else
						{
							if (modGlobalVariables.Statics.intSearchPosition == 2)
							{
								if (modGNBas.Statics.SEQ == "L")
								{
									// Set searchRS = secDB.OpenRecordset("SELECT * FROM srMaster where RSLOCSTREET between '" & Resp2 & "' and '" & Resp2 & "zzzz' " & strDel & " and RSCard = 1 order by RSLOCSTREET , val(rslocnumalph & """" )",strredatabase)
									modGlobalVariables.Statics.searchRS.OpenRecordset("select id,tbl1.rslocstreet as rslocstreet,tbl1.rslocnumalph as rslocnumalph,tbl1.rslocapt as rslocapt,rsaccount,rscard,rsdeleted,rsname,rssecowner,rspreviousmaster,rsmaplot,saledate  from srmaster inner join (select distinct rsaccount as account,rslocstreet,rslocnumalph,rslocapt from srmaster where rslocstreet between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' " + strDel + " ) as tbl1 on (tbl1.account = srmaster.rsaccount) where srmaster.rscard = 1 " + strDel + " order by tbl1.rslocstreet,tbl1.rslocnumalph ", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "N")
								{
									if (modGlobalVariables.Statics.boolIncPrevOwner)
									{
										modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where (RSName between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' or rssecowner between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' or rspreviousmaster between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' ) " + strDel + " and RSCard = 1 order by RSName", modGlobalVariables.strREDatabase);
									}
									else
									{
										modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where (RSName between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' or rssecowner between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' ) " + strDel + " and RSCard = 1 order by RSName", modGlobalVariables.strREDatabase);
									}
								}
								else if (modGNBas.Statics.SEQ == "M")
								{
									modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSMAPLOT between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' " + strDel + " and RSCard = 1 order by RSMAPLOT", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "Ref1")
								{
									modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSref1 between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' " + strDel + " and RSCard = 1 order by RSref1", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "Ref2")
								{
									modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSref2 between '" + modGNBas.Statics.Resp2 + "' and '" + modGNBas.Statics.Resp2 + "zzzz' " + strDel + " and RSCard = 1 order by RSref2", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "R")
								{
									frmGetSaleRange.InstancePtr.Show(FCForm.FormShowEnum.Modal);
								}
								else if (modGNBas.Statics.SEQ == "S")
								{
									modGlobalVariables.Statics.searchRS.OpenRecordset("select * from srmaster where saledate like '" + modGNBas.Statics.Resp2 + "' " + strDel + " order by rsaccount", modGlobalVariables.strREDatabase);
								}
							}
							else
							{
								if (modGNBas.Statics.SEQ == "L")
								{
									// Set searchRS = secDB.OpenRecordset("SELECT * FROM srMaster where RSLOCSTREET like " & Resp2 & " " & strDel & " and RSCard = 1 order by RSLOCSTREET , val(rslocnumalph & """" )",strredatabase)
									modGlobalVariables.Statics.searchRS.OpenRecordset("select id,tbl1.rslocstreet as rslocstreet,tbl1.rslocnumalph as rslocnumalph,tbl1.rslocapt as rslocapt,rsaccount,rscard,rsdeleted,rsname,rssecowner,rspreviousmaster,rsmaplot,saledate  from srmaster inner join (select distinct rsaccount as account,rslocstreet,rslocnumalph,rslocapt from srmaster where rslocstreet like " + modGNBas.Statics.Resp2 + " " + strDel + " ) as tbl1 on (tbl1.account = srmaster.rsaccount) where srmaster.rscard = 1 " + strDel + " order by tbl1.rslocstreet,tbl1.rslocnumalph ", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "N")
								{
									if (modGlobalVariables.Statics.boolIncPrevOwner)
									{
										modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where (RSName like " + modGNBas.Statics.Resp2 + " or rssecowner like " + modGNBas.Statics.Resp2 + " or rspreviousmaster like " + modGNBas.Statics.Resp2 + " ) " + strDel + " and RSCard = 1 order by RSName", modGlobalVariables.strREDatabase);
									}
									else
									{
										modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where (RSName like " + modGNBas.Statics.Resp2 + " or rssecowner like " + modGNBas.Statics.Resp2 + " ) " + strDel + " and RSCard = 1 order by RSName", modGlobalVariables.strREDatabase);
									}
								}
								else if (modGNBas.Statics.SEQ == "M")
								{
									modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSMAPLOT like " + modGNBas.Statics.Resp2 + " " + strDel + " and RSCard = 1 order by RSMAPLOT", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "Ref1")
								{
									modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSref1 like " + modGNBas.Statics.Resp2 + " " + strDel + " and RSCard = 1 order by RSref1", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "Ref2")
								{
									modGlobalVariables.Statics.searchRS.OpenRecordset("SELECT * FROM srMaster where RSref2 like " + modGNBas.Statics.Resp2 + " " + strDel + " and RSCard = 1 order by RSref2", modGlobalVariables.strREDatabase);
								}
								else if (modGNBas.Statics.SEQ == "R")
								{
									frmGetSaleRange.InstancePtr.Show(FCForm.FormShowEnum.Modal);
									// Set searchRS = secDB.OpenRecordset("select * from srmaster where saledate = " & Val(Resp2) & " order by saledate")
								}
								else if (modGNBas.Statics.SEQ == "S")
								{
									modGlobalVariables.Statics.searchRS.OpenRecordset("select * from srmaster where saledate like " + modGNBas.Statics.Resp2 + " " + strDel + " order by rsaccount", modGlobalVariables.strREDatabase);
								}
							}
						}
						// if resp2 = " "
					}
					// if boolregrecords
				}
				while (!modGlobalVariables.Statics.searchRS.EndOfFile())
				{
					if (!fecherFoundation.FCUtils.IsNull(modGlobalVariables.Statics.searchRS.Get_Fields_Int32("rsaccount")))
					{
						if (Conversion.Val(modGlobalVariables.Statics.searchRS.Get_Fields_Int32("rsaccount")) == 0)
							goto REBL02_J_EXIT;
						if (Conversion.Val(modGlobalVariables.Statics.searchRS.Get_Fields_Int32("rscard")) == 0)
							goto REBL02_J_EXIT;
					}
					DISPNAME = Strings.Trim(FCConvert.ToString(modGlobalVariables.Statics.searchRS.Get_Fields_String("RSNAME")));
					if (modGNBas.Statics.SEQ == "N" && boolShowAll)
					{
						switch (modGlobalVariables.Statics.intSearchPosition)
						{
							case 1:
								{
									intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("RSNAME") + "", strHoldResp2, CompareConstants.vbTextCompare);
									if (intResponse > 0)
									{
										ItemCnt += 1;
										// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
										Grid.AddItem(modGlobalVariables.Statics.searchRS.Get_Fields_String("RSNAME") + "\t" + rs.Get_Fields("book") + "\t" + rs.Get_Fields("page") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
									}
									intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "", strHoldResp2, CompareConstants.vbTextCompare);
									if (intResponse > 0)
									{
										ItemCnt += 1;
										// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
										Grid.AddItem(modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "\t" + rs.Get_Fields("book") + "\t" + rs.Get_Fields("page") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
									}
									if (modGlobalVariables.Statics.boolIncPrevOwner)
									{
										intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "", strHoldResp2, CompareConstants.vbTextCompare);
										if (intResponse > 0)
										{
											ItemCnt += 1;
											// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
											Grid.AddItem(modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "\t" + rs.Get_Fields("book") + "\t" + rs.Get_Fields("page") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
										}
									}
									break;
								}
							case 2:
								{
									// begins with
									intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rsname") + "", strHoldResp2, CompareConstants.vbTextCompare);
									if (intResponse == 1)
									{
										ItemCnt += 1;
										// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
										Grid.AddItem(modGlobalVariables.Statics.searchRS.Get_Fields_String("RSNAME") + "\t" + rs.Get_Fields("book") + "\t" + rs.Get_Fields("page") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
									}
									intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "", strHoldResp2, CompareConstants.vbTextCompare);
									if (intResponse == 1)
									{
										ItemCnt += 1;
										// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
										Grid.AddItem(modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "\t" + rs.Get_Fields("book") + "\t" + rs.Get_Fields("page") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
									}
									if (modGlobalVariables.Statics.boolIncPrevOwner)
									{
										intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "", strHoldResp2, CompareConstants.vbTextCompare);
										if (intResponse == 1)
										{
											ItemCnt += 1;
											// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
											Grid.AddItem(modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "\t" + rs.Get_Fields("book") + "\t" + rs.Get_Fields("page") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
										}
									}
									break;
								}
							case 3:
								{
									// ends with
									intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rsname") + "", strHoldResp2, CompareConstants.vbTextCompare);
									if ((intResponse == (FCConvert.ToString(modGlobalVariables.Statics.searchRS.Get_Fields_String("rsname")).Length - strHoldResp2.Length + 1)) && intResponse > 0)
									{
										ItemCnt += 1;
										// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
										Grid.AddItem(modGlobalVariables.Statics.searchRS.Get_Fields_String("RSNAME") + "\t" + rs.Get_Fields("book") + "\t" + rs.Get_Fields("page") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
									}
									intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "", strHoldResp2, CompareConstants.vbTextCompare);
									if ((intResponse == (FCConvert.ToString(modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner")).Length - strHoldResp2.Length + 1)) && intResponse > 0)
									{
										ItemCnt += 1;
										// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
										Grid.AddItem(modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "\t" + rs.Get_Fields("book") + "\t" + rs.Get_Fields("page") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
									}
									if (modGlobalVariables.Statics.boolIncPrevOwner)
									{
										intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "", strHoldResp2, CompareConstants.vbTextCompare);
										if ((intResponse == (FCConvert.ToString(modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster")).Length - strHoldResp2.Length + 1)) && intResponse != 0)
										{
											ItemCnt += 1;
											// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
											// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
											Grid.AddItem(modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "\t" + rs.Get_Fields("book") + "\t" + rs.Get_Fields("page") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
										}
									}
									break;
								}
						}
						//end switch
					}
					else
					{
						if (modGNBas.Statics.SEQ == "N")
						{
							switch (modGlobalVariables.Statics.intSearchPosition)
							{
								case 1:
									{
										intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("RSNAME") + "", strHoldResp2, CompareConstants.vbTextCompare);
										if (!(intResponse > 0))
										{
											intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "", strHoldResp2, CompareConstants.vbTextCompare);
											if (!(intResponse > 0))
											{
												intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "", strHoldResp2, CompareConstants.vbTextCompare);
												if (intResponse > 0)
												{
													DISPNAME = Strings.Trim(modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "");
												}
											}
											else
											{
												DISPNAME = Strings.Trim(FCConvert.ToString(modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner")));
											}
										}
										break;
									}
								case 2:
									{
										// begins with
										intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rsname") + "", strHoldResp2, CompareConstants.vbTextCompare);
										if (intResponse != 1)
										{
											intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "", strHoldResp2, CompareConstants.vbTextCompare);
											if (intResponse != 1)
											{
												intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "", strHoldResp2, CompareConstants.vbTextCompare);
												if (intResponse == 1)
												{
													DISPNAME = Strings.Trim(modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "");
												}
											}
											else
											{
												DISPNAME = Strings.Trim(modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "");
											}
										}
										break;
									}
								case 3:
									{
										// ends with
										intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rsname") + "", strHoldResp2, CompareConstants.vbTextCompare);
										if ((intResponse != (FCConvert.ToString(modGlobalVariables.Statics.searchRS.Get_Fields_String("rsname")).Length - strHoldResp2.Length + 1)) || intResponse == 0)
										{
											intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "", strHoldResp2, CompareConstants.vbTextCompare);
											if ((intResponse != (FCConvert.ToString(modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner")).Length - strHoldResp2.Length + 1)) || intResponse == 0)
											{
												intResponse = Strings.InStr(1, modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "", strHoldResp2, CompareConstants.vbTextCompare);
												if ((intResponse == (FCConvert.ToString(modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster")).Length - strHoldResp2.Length + 1)) && intResponse != 0)
												{
													DISPNAME = Strings.Trim(modGlobalVariables.Statics.searchRS.Get_Fields_String("rspreviousmaster") + "");
												}
											}
											else
											{
												DISPNAME = Strings.Trim(modGlobalVariables.Statics.searchRS.Get_Fields_String("rssecowner") + "");
											}
										}
										break;
									}
							}
							//end switch
						}
						ItemCnt += 1;
						if (modGNBas.Statics.SEQ == "Ref1")
						{
							//FC:FINAL:AM:#i1397 - check for date
							//if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("saledate")))
							if (fecherFoundation.Information.IsDate(rs.Get_Fields("saledate")))
							{
								if (rs.Get_Fields_DateTime("saledate").ToOADate() != 0)
								{
									strDate = rs.Get_Fields_DateTime("saledate").ToString("d");
								}
								else
								{
									strDate = "";
								}
							}
							else
							{
								strDate = "";
							}
							Grid.AddItem(DISPNAME + "\t" + "\t" + rs.Get_Fields_String("rsref1") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
						}
						else if (modGNBas.Statics.SEQ == "Ref2")
						{
							//FC:FINAL:AM:#i1397 - check for date
							//if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("saledate")))
							if (fecherFoundation.Information.IsDate(rs.Get_Fields("saledate")))
							{
								if (rs.Get_Fields_DateTime("saledate").ToOADate() != 0)
								{
									strDate = rs.Get_Fields_DateTime("saledate").ToString("d");
								}
								else
								{
									strDate = "";
								}
							}
							else
							{
								strDate = "";
							}
							Grid.AddItem(DISPNAME + "\t" + "\t" + rs.Get_Fields_String("rsref2") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
						}
						else if (modGNBas.Statics.SEQ == "Open1")
						{
							//FC:FINAL:AM:#i1397 - check for date
							//if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("saledate")))
							if (fecherFoundation.Information.IsDate(rs.Get_Fields("saledate")))
							{
								if (rs.Get_Fields_DateTime("saledate").ToOADate() != 0)
								{
									strDate = rs.Get_Fields_DateTime("saledate").ToString("d");
								}
								else
								{
									strDate = "";
								}
							}
							else
							{
								strDate = "";
							}
							Grid.AddItem(DISPNAME + "\t" + "\t" + rs.Get_Fields_Int32("piopen1") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
						}
						else if (modGNBas.Statics.SEQ == "Open2")
						{
							//FC:FINAL:AM:#i1397 - check for date
							//if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("saledate")))
							if (fecherFoundation.Information.IsDate(rs.Get_Fields("saledate")))
							{
								if (rs.Get_Fields_DateTime("saledate").ToOADate() != 0)
								{
									strDate = rs.Get_Fields_DateTime("saledate").ToString("d");
								}
								else
								{
									strDate = "";
								}
							}
							else
							{
								strDate = "";
							}
							Grid.AddItem(DISPNAME + "\t" + "\t" + rs.Get_Fields_Int32("piopen2") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
						}
						else if (modGNBas.Statics.SEQ != "B")
						{
							//FC:FINAL:AM:#i1397 - check for date
							//if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("saledate")))
							if (fecherFoundation.Information.IsDate(rs.Get_Fields("saledate")))
							{
								if (rs.Get_Fields_DateTime("saledate").ToOADate() != 0)
								{
									strDate = rs.Get_Fields_DateTime("saledate").ToString("d");
								}
								else
								{
									strDate = "";
								}
							}
							else
							{
								strDate = "";
							}
							Grid.AddItem(DISPNAME + "\t" + rs.Get_Fields_String("rslocnumalph") + "\t" + rs.Get_Fields_String("rslocstreet") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
						}
						else
						{
							if (modGlobalVariables.Statics.boolRegRecords)
							{
								//FC:FINAL:AM:#i1397 - check for date
								//if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("master.saledate")))
								// TODO Get_Fields: Field [master.saledate] not found!! (maybe it is an alias?)
								if (fecherFoundation.Information.IsDate(rs.Get_Fields("master.saledate")))
								{
									// TODO Get_Fields: Field [master.saledate] not found!! (maybe it is an alias?)
									if (rs.Get_Fields("master.saledate").ToOADate() != 0)
									{
										// TODO Get_Fields: Field [master.saledate] not found!! (maybe it is an alias?)
										strDate = rs.Get_Fields("master.saledate").ToString("d");
									}
									else
									{
										strDate = "";
									}
								}
								else
								{
									strDate = "";
								}
							}
							else
							{
								//FC:FINAL:AM:#i1397 - check for date
								//if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("srmaster.saledate")))
								// TODO Get_Fields: Field [srmaster.saledate] not found!! (maybe it is an alias?)
								if (fecherFoundation.Information.IsDate(rs.Get_Fields("srmaster.saledate")))
								{
									// TODO Get_Fields: Field [Srmaster.saledate] not found!! (maybe it is an alias?)
									if (rs.Get_Fields("Srmaster.saledate").ToOADate() != 0)
									{
										// TODO Get_Fields: Field [srmaster.saledate] not found!! (maybe it is an alias?)
										strDate = rs.Get_Fields("srmaster.saledate").ToString("d");
									}
									else
									{
										strDate = "";
									}
								}
								else
								{
									strDate = "";
								}
							}
							// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
							Grid.AddItem(DISPNAME + "\t" + rs.Get_Fields("book") + "\t" + rs.Get_Fields("page") + "\t" + strDate + "\t" + rs.Get_Fields_String("rsmaplot") + "\t" + rs.Get_Fields_Int32("rsaccount") + "\t" + rs.Get_Fields_Int32("id"));
						}
					}
					if (rs.Get_Fields_Boolean("rsdeleted"))
					{
						Grid.TextMatrix(Grid.Rows - 1, 7, "DEL");
						Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Rows - 1, 0, Grid.Rows - 1, 6, Information.RGB(230, 20, 20));
					}
					NEWREC = string.Empty.ToCharArray();
					REBL02_J_EXIT:
					;
					modGlobalVariables.Statics.searchRS.MoveNext();
				}
				lblSearchCount.Text = "Matches: " + Conversion.Str(ItemCnt);
				if (modGNBas.Statics.SEQ == "N")
				{
					if (Grid.Rows > 1)
					{
						// sorted by rsname.  If we matched on other names, the sorting is off
						Grid.Col = 0;
						Grid.Sort = FCGrid.SortSettings.flexSortStringNoCaseAscending;
						Grid.Row = 0;
					}
					// Grid.Row = -1
				}
				else if (modGNBas.Statics.SEQ == "L")
				{
					Grid.Col = 1;
					//Grid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
					//Grid.Col = 2;
                    //Grid.Sort = FCGrid.SortSettings.flexSortStringNoCaseAscending;
                    //FC:FINAL:AM:#3261 - sort multiple columns
                    Grid.SortColumns(FCGrid.SortSettings.flexSortStringNoCaseAscending, 2, 1);
                    Grid.Row = 0;
				}
				if (Grid.Rows == 2)
				{
					// only one match so just open it
					Grid.Row = 1;
					Grid_DblClick(new object(), new System.EventArgs());
				}
				/*? On Error GoTo 0 */
				return;
			}
			catch (Exception ex)
			{
				// ErrorRoutine:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "in Display Records", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				modGlobalRoutines.ErrorRoutine();
			}
		}
		// Private Sub EstablishAccount()
		// ACN$ = can$
		// If Val(ACN$) <> 0 Then
		// can$ = SELECTNUM(Val(ACN$))
		// Else
		// MsgBox "No records selected"
		// End If
		// End Sub
		private void frmRESearch_Resize(object sender, System.EventArgs e)
		{
			ResizeSearchGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobalVariables.Statics.boolPreviousOwnerRecords = false;
		}

		private void Grid_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (Grid.HitTest(e.X, e.Y).Type != DataGridView.HitTestType.Cell)
            {
                    return;
            }
            Grid_DblClick(null, null);
        }


        private void Grid_DblClick(object sender, System.EventArgs e)
		{
            if (Grid.Row <= 0)
				return;
			if (modGlobalVariables.Statics.boolPreviousOwnerRecords)
			{
				frmPreviousOwner.InstancePtr.Init(FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, 4)));
				this.Unload();
				return;
			}
			modGNBas.Statics.response = Strings.Trim(Grid.TextMatrix(Grid.Row, 5));
			modGlobalVariables.Statics.gcurrsaledate = Strings.Trim(Grid.TextMatrix(Grid.Row, 3));
			strSellerName = Strings.Trim(Grid.TextMatrix(Grid.Row, 0));
			modGlobalVariables.Statics.gintAutoID = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, 6));
			//Application.DoEvents();
			modGlobalRoutines.escapequote(ref strSellerName);
			GetSearchAccount();
		}

		public void GetSearchAccount()
		{
			try
			{
				// On Error GoTo ErrorTag
				// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
				DialogResult intResponse;
				string tstring = "";
				clsDRWrapper clsTemp = new clsDRWrapper();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// unload now so you don't have data synchronization problems
				if (modGlobalVariables.Statics.wmainresponse == 1)
				{
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						if (modGlobalRoutines.Formisloaded_2("frmNewREProperty"))
						{
							frmNewREProperty.InstancePtr.Unload();
						}
					}
					else
					{
						if (modGlobalRoutines.Formisloaded_2("frmREProperty"))
						{
							frmREProperty.InstancePtr.Unload();
						}
					}
				}
				else
				{
					if (modGlobalRoutines.Formisloaded_2("frmSHREBLUpdate"))
					{
						frmSHREBLUpdate.InstancePtr.Unload();
					}
				}
				//Application.DoEvents();
				modGlobalVariables.Statics.AddNewAccountFlag = false;
				modGlobalVariables.Statics.CurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(modGNBas.Statics.response)));
				modGlobalVariables.Statics.gintLastAccountNumber = modGlobalVariables.Statics.CurrentAccount;
				modGNBas.Statics.response = "X";
				modGNBas.Statics.gintCardNumber = 1;
				modGlobalVariables.Statics.ActiveCard = 1;
				// CurrentCard = 1
				modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", FCConvert.ToString(Conversion.Val(Conversion.Str(modGlobalVariables.Statics.CurrentAccount))));
				modGlobalVariables.Statics.gintLastAccountNumber = (modRegistry.GetRegistryKey("RELastAccountNumber") != string.Empty ? FCConvert.ToInt32(Math.Round(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn))) : 0);
				modGlobalVariables.Statics.CurrentAccount = modGlobalVariables.Statics.gintLastAccountNumber;
				// Call OpenWRETable(WRE, 1)
				// Call OpenWWKTable(WWK)
				clsTemp.OpenRecordset("select * from modules", modGlobalVariables.Statics.strGNDatabase);
				modGlobalVariables.Statics.MaxAccounts = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("re_maxaccounts"))));
				if (modGlobalVariables.Statics.CurrentAccount > 0)
				{
					// this on error checks to see if record is locked
					object ans;
					TryOver:
					;
					/*? On Error Resume Next  */
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						clsDRWrapper temp = modDataTypes.Statics.MR3;
						modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.CurrentAccount);
						modDataTypes.Statics.MR3 = temp;
						temp = modDataTypes.Statics.MR;
						modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.CurrentAccount);
						modDataTypes.Statics.MR = temp;
					}
					else
					{
						// Set MR = DBTemp.OpenRecordset("Select * from srmaster where rsaccount = " & CurrentAccount & " and saledate = #" & gcurrsaledate & "# and rsname like '*" & strSellerName & "*'")
						modDataTypes.Statics.MR.OpenRecordset("select * from srmaster where id = " + FCConvert.ToString(modGlobalVariables.Statics.gintAutoID), modGlobalVariables.strREDatabase);
						modDataTypes.Statics.MR.Edit();
						// Set MR3 = DBTemp.OpenRecordset("Select * from srmaster where rsaccount = " & CurrentAccount & " and saledate = #" & gcurrsaledate & "# and rsname like '*" & strSellerName & "*'")
						modDataTypes.Statics.MR3.OpenRecordset("select * from srmaster where id = " + FCConvert.ToString(modGlobalVariables.Statics.gintAutoID), modGlobalVariables.strREDatabase);
						modGlobalVariables.Statics.glngSaleID = FCConvert.ToInt32(modDataTypes.Statics.MR3.Get_Fields_Int32("saleid"));
						modDataTypes.Statics.MR3.Edit();
					}
					if (FCConvert.ToBoolean(modDataTypes.Statics.MR.Get_Fields_Boolean("rsdeleted")))
					{
						intResponse = MessageBox.Show(" The Account you have selected is a Deleted Account." + "\r\n" + "Would you like to Un-Delete it?", "Deleted Account", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (intResponse == DialogResult.Yes)
						{
							if (modGlobalVariables.Statics.boolRegRecords)
							{
								modGlobalRoutines.UndeleteMasterRecord(modGlobalVariables.Statics.CurrentAccount);
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, 0, Grid.Row, 6, Information.RGB(255, 255, 255));
							}
							else
							{
								modGlobalRoutines.UndeleteMasterRecord(modGlobalVariables.Statics.CurrentAccount, modGlobalVariables.Statics.gintAutoID);
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Row, 0, Grid.Row, 6, Information.RGB(255, 255, 255));
							}
						}
						else
						{
						}
					}
					else
					{
						if (modGlobalVariables.Statics.wmainresponse == 1)
						{
							if (modGlobalVariables.Statics.boolRegRecords)
							{
								if (modGlobalRoutines.Formisloaded_2("frmNewREProperty"))
								{
									frmNewREProperty.InstancePtr.Unload();
								}
								frmNewREProperty.InstancePtr.Show(App.MainForm);
								frmNewREProperty.InstancePtr.cmdPreviousAccountFromSearch.Enabled = true;
								frmNewREProperty.InstancePtr.cmdNextAccountFromSearch.Enabled = true;
							}
							else
							{
								if (modGlobalRoutines.Formisloaded_2("frmREProperty"))
								{
									frmREProperty.InstancePtr.Unload();
								}
								frmREProperty.InstancePtr.Show(App.MainForm);
								frmREProperty.InstancePtr.mnuPreviousAccountFromSearch.Enabled = true;
								frmREProperty.InstancePtr.mnuNextAccountFromSearch.Enabled = true;
							}
						}
						else
						{
							if (modGlobalRoutines.Formisloaded_2("frmSHREBLUpdate"))
							{
								frmSHREBLUpdate.InstancePtr.Unload();
							}
							frmSHREBLUpdate.InstancePtr.Show(App.MainForm);
							frmSHREBLUpdate.InstancePtr.mnuPreviousSave.Enabled = true;
							frmSHREBLUpdate.InstancePtr.mnuNextSave.Enabled = true;
						}
						modGNBas.Statics.gboolLoadingDone = true;
						if (modGlobalVariables.Statics.InvalidAccountNumber == false)
						{
							FCGlobal.Screen.MousePointer = 0;
						}
						else
						{
							// txtGetAccountNumber.Text = ""
							// txtGetAccountNumber.SetFocus
							modGlobalVariables.Statics.InvalidAccountNumber = false;
						}
					}
				}
				else if (modGlobalVariables.Statics.CurrentAccount == 0)
				{
					modGlobalVariables.Statics.ActiveCard = 1;
					// CurrentCard = 1
					modGNBas.Statics.gintCardNumber = 1;
					if (modGlobalVariables.Statics.wmainresponse == 1)
					{
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							if (modGlobalRoutines.Formisloaded_2("frmNewREProperty"))
							{
								frmNewREProperty.InstancePtr.Unload();
							}
							frmNewREProperty.InstancePtr.Show(App.MainForm);
							frmNewREProperty.InstancePtr.mnuAddNewAccount_Click();
							frmNewREProperty.InstancePtr.BringToFront();
						}
						else
						{
							if (modGlobalRoutines.Formisloaded_2("frmREProperty"))
							{
								frmREProperty.InstancePtr.Unload();
							}
							frmREProperty.InstancePtr.Show(App.MainForm);
							frmREProperty.InstancePtr.mnuAddNewAccount_Click();
							frmREProperty.InstancePtr.BringToFront();
						}
					}
					else
					{
						if (modGlobalRoutines.Formisloaded_2("frmSHREBLUpdate"))
						{
							frmSHREBLUpdate.InstancePtr.Unload();
							//Application.DoEvents();
						}
						frmSHREBLUpdate.InstancePtr.Show(App.MainForm);
						frmSHREBLUpdate.InstancePtr.BringToFront();
					}
					FCGlobal.Screen.MousePointer = 0;
				}
				FCGlobal.Screen.MousePointer = 0;
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show(Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				FCGlobal.Screen.MousePointer = 0;
			}
		}

		public void SetupSearchGrid()
		{
			Grid.Rows = 1;
			if (!modGlobalVariables.Statics.boolPreviousOwnerRecords)
			{
				Grid.ColHidden(6, true);
				Grid.ColHidden(7, true);
				Grid.TextMatrix(0, 0, "Name");
				if (!(modGNBas.Statics.SEQ == "B"))
				{
					if (modGNBas.Statics.SEQ != "Ref1" && modGNBas.Statics.SEQ != "Ref2" && modGNBas.Statics.SEQ != "Open1" && modGNBas.Statics.SEQ != "Open2")
					{
						Grid.TextMatrix(0, 1, "ST NO");
                        //FC:FINAL:CHN: Set column data type to fix ordering.
                        Grid.ColDataType(1, FCGrid.DataTypeSettings.flexDTLong);
                        Grid.TextMatrix(0, 2, "Location");
					}
					else
					{
						Grid.ColHidden(1, true);
						if (modGNBas.Statics.SEQ == "Ref1")
						{
							Grid.TextMatrix(0, 2, "Reference 1");
						}
						else if (modGNBas.Statics.SEQ == "Ref2")
						{
							Grid.TextMatrix(0, 2, "Reference 2");
						}
						else if (modGNBas.Statics.SEQ == "Open1")
						{
							Grid.TextMatrix(0, 2, "Open 1");
							if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1330))
							{
								if (Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) != "")
								{
									Grid.TextMatrix(0, 2, Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc));
								}
							}
						}
						else if (modGNBas.Statics.SEQ == "Open2")
						{
							Grid.TextMatrix(0, 0, "Open 2");
							if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1340))
							{
								if (Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) != "")
								{
									Grid.TextMatrix(0, 2, Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc));
								}
							}
						}
					}
				}
				else
				{
					Grid.TextMatrix(0, 1, "Book");
					Grid.TextMatrix(0, 2, "Page");
				}
				Grid.TextMatrix(0, 3, "SaleDate");
				Grid.TextMatrix(0, 4, "Map/Lot");
				Grid.TextMatrix(0, 5, "Account");
				//FC:FINAL:CHN - issue #1608: Set column data type to fix ordering.
				Grid.ColDataType(5, FCGrid.DataTypeSettings.flexDTLong);
				Grid.ColDataType(3, FCGrid.DataTypeSettings.flexDTDate);
			}
			else
			{
				Grid.ColHidden(4, true);
				Grid.ColHidden(5, true);
				Grid.ColHidden(6, true);
				Grid.ColHidden(7, true);
				Grid.TextMatrix(0, 0, "Name");
				Grid.TextMatrix(0, 1, "Second Owner");
				Grid.TextMatrix(0, 2, "Account");
				//FC:FINAL:CHN - issue #1608: Set column data type to fix ordering.
				Grid.ColDataType(2, FCGrid.DataTypeSettings.flexDTLong);
				Grid.TextMatrix(0, 3, "Sale Date");
				Grid.ColDataType(3, FCGrid.DataTypeSettings.flexDTDate);
				Grid.ExtendLastCol = true;
			}
			ResizeSearchGrid();

            Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);           
            Grid.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

		public void ResizeSearchGrid()
		{
			int GridWidth;
			GridWidth = Grid.WidthOriginal;
			if (!modGlobalVariables.Statics.boolPreviousOwnerRecords)
			{
				Grid.ColWidth(0, FCConvert.ToInt32(0.3 * GridWidth));
				// 2880
				if (modGNBas.Statics.SEQ == "Ref1" || modGNBas.Statics.SEQ == "Ref2" || modGNBas.Statics.SEQ == "Open1" || modGNBas.Statics.SEQ == "Open2")
				{
					Grid.ColWidth(2, FCConvert.ToInt32(0.31 * GridWidth));
				}
				else if (modGNBas.Statics.SEQ != "B")
				{
					Grid.ColWidth(1, FCConvert.ToInt32(0.08 * GridWidth));
					Grid.ColWidth(2, FCConvert.ToInt32(0.23 * GridWidth));
					// 2160
				}
				else
				{
					Grid.ColWidth(1, FCConvert.ToInt32(0.155 * GridWidth));
					Grid.ColWidth(2, FCConvert.ToInt32(0.155 * GridWidth));
				}
				Grid.ColWidth(3, FCConvert.ToInt32(0.1 * GridWidth));
				// 950
				Grid.ColWidth(4, FCConvert.ToInt32(0.17 * GridWidth));
				// 1600
				Grid.ColWidth(5, FCConvert.ToInt32(0.08 * GridWidth));
				// 800
			}
			else
			{
				Grid.ColWidth(0, FCConvert.ToInt32(0.35 * GridWidth));
				Grid.ColWidth(1, FCConvert.ToInt32(0.35 * GridWidth));
				Grid.ColWidth(2, FCConvert.ToInt32(0.1 * GridWidth));
				Grid.ColWidth(3, FCConvert.ToInt32(0.15 * GridWidth));
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptSearch.InstancePtr);
		}
    }
}
