﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmMovePictures.
	/// </summary>
	partial class frmMovePictures : BaseForm
	{
		public fecherFoundation.FCComboBox cmbCopy;
		public fecherFoundation.FCLabel lblCopy;
		public FCGrid GridDirectories;
		public fecherFoundation.FCComboBox cmbDirectory;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMovePictures));
            this.cmbCopy = new fecherFoundation.FCComboBox();
            this.lblCopy = new fecherFoundation.FCLabel();
            this.GridDirectories = new fecherFoundation.FCGrid();
            this.cmbDirectory = new fecherFoundation.FCComboBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDirectories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 181);
            this.BottomPanel.Size = new System.Drawing.Size(731, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridDirectories);
            this.ClientArea.Controls.Add(this.cmbCopy);
            this.ClientArea.Controls.Add(this.lblCopy);
            this.ClientArea.Controls.Add(this.cmbDirectory);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(751, 302);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbDirectory, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCopy, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbCopy, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridDirectories, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(751, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(292, 30);
            this.HeaderText.Text = "Copy / Move Picture Files";
            // 
            // cmbCopy
            // 
            this.cmbCopy.Items.AddRange(new object[] {
            "Copy and re-name only",
            "Copy and re-name, then delete original file"});
            this.cmbCopy.Location = new System.Drawing.Point(133, 30);
            this.cmbCopy.Name = "cmbCopy";
            this.cmbCopy.Size = new System.Drawing.Size(382, 40);
            this.cmbCopy.TabIndex = 1;
            this.cmbCopy.Text = "Copy and re-name only";
            // 
            // lblCopy
            // 
            this.lblCopy.AutoSize = true;
            this.lblCopy.Location = new System.Drawing.Point(30, 44);
            this.lblCopy.Name = "lblCopy";
            this.lblCopy.Size = new System.Drawing.Size(46, 17);
            this.lblCopy.Text = "COPY";
            // 
            // GridDirectories
            // 
            this.GridDirectories.Cols = 1;
            this.GridDirectories.ColumnHeadersVisible = false;
            this.GridDirectories.FixedCols = 0;
            this.GridDirectories.FixedRows = 0;
            this.GridDirectories.Location = new System.Drawing.Point(29, 164);
            this.GridDirectories.Name = "GridDirectories";
            this.GridDirectories.RowHeadersVisible = false;
            this.GridDirectories.Rows = 0;
            this.GridDirectories.Size = new System.Drawing.Size(211, 17);
            this.GridDirectories.TabIndex = 4;
            this.GridDirectories.Visible = false;
            // 
            // cmbDirectory
            // 
            this.cmbDirectory.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDirectory.Location = new System.Drawing.Point(342, 90);
            this.cmbDirectory.Name = "cmbDirectory";
            this.cmbDirectory.Size = new System.Drawing.Size(333, 40);
            this.cmbDirectory.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cmbDirectory, "The default picture directory to copy files to.  Pictures in one of these default" +
        " directories will not be copied or moved.");
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 104);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(292, 17);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "DIRECTORY TO COPY / MOVE PICTURES TO";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(308, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(150, 48);
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmMovePictures
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(751, 362);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmMovePictures";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Copy / Move Picture Files";
            this.Load += new System.EventHandler(this.frmMovePictures_Load);
            this.Activated += new System.EventHandler(this.frmMovePictures_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMovePictures_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDirectories)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
