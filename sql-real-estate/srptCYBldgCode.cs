﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptCYBldgCode.
	/// </summary>
	public partial class srptCYBldgCode : FCSectionReport
	{
		public static srptCYBldgCode InstancePtr
		{
			get
			{
				return (srptCYBldgCode)Sys.GetInstance(typeof(srptCYBldgCode));
			}
		}

		protected srptCYBldgCode _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCodes?.Dispose();
				clsAssess?.Dispose();
                clsAssess = null;
                clsCodes = null;
            }
			base.Dispose(disposing);
		}

		public srptCYBldgCode()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptCYBldgCode	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsCodes = new clsDRWrapper();
		clsDRWrapper clsAssess = new clsDRWrapper();
		double lngLand;
		double lngbldg;
		double lngExempt;
		double lngTotal;
		int lngCode;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsAssess.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Call clsCodes.OpenRecordset("select * from costrecord where crecordnumber between 1801 and 1899", strredatabase)
			clsCodes.OpenRecordset("select * from tblbldgcode order by code", modGlobalVariables.strREDatabase);
			clsAssess.OpenRecordset("select ribldgcode,count(rsaccount) as thecount, sum(rllandval) as landtot, sum(rlbldgval) as bldgtot, sum(correxemption) as exempttot from master where not rsdeleted = 1 group by ribldgcode order by ribldgcode", modGlobalVariables.strREDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAssess.GetData("ribldgcode"))));
			txtCode.Text = lngCode.ToString();
			if (lngCode == 0)
			{
				txtCode.Text = txtCode.Text + " Uncoded";
			}
			else
			{
				// Call clsCodes.FindFirstRecord("crecordnumber", 1800 + lngCode)
				// Call clsCodes.FindFirstRecord("code", lngCode)
				clsCodes.FindFirst("code = " + FCConvert.ToString(lngCode));
				if (!clsCodes.NoMatch)
				{
					// txtCode.Text = txtCode.Text & " " & clsCodes.GetData("csdesc")
					txtCode.Text = txtCode.Text + " " + clsCodes.Get_Fields_String("ShortDescription");
				}
				else
				{
					txtCode.Text = txtCode.Text + " Unknown";
				}
			}
			txtCount.Text = FCConvert.ToString(Conversion.Val(clsAssess.GetData("thecount")));
			lngLand = Conversion.Val(clsAssess.GetData("landtot"));
			lngbldg = Conversion.Val(clsAssess.GetData("bldgtot"));
			lngExempt = Conversion.Val(clsAssess.GetData("exempttot"));
			lngTotal = lngLand - lngExempt + lngbldg;
			txtLand.Text = Strings.Format(lngLand, "###,###,##0");
			txtBldg.Text = Strings.Format(lngbldg, "###,###,###,##0");
			txtExempt.Text = Strings.Format(lngExempt, "##,###,###,##0");
			txtTotal.Text = Strings.Format(lngTotal, "###,###,###,##0");
			clsAssess.MoveNext();
		}

		
	}
}
