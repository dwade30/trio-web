﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmRESearch.
	/// </summary>
	partial class frmRESearch : BaseForm
	{
		public fecherFoundation.FCGrid Grid;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCLabel lblSearchCount;
		public fecherFoundation.FCLabel lblSearch;
		public fecherFoundation.FCLabel Label55;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRESearch));
            this.Grid = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Shape1 = new fecherFoundation.FCLabel();
            this.lblSearchCount = new fecherFoundation.FCLabel();
            this.lblSearch = new fecherFoundation.FCLabel();
            this.Label55 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 434);
            this.BottomPanel.Size = new System.Drawing.Size(738, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Shape1);
            this.ClientArea.Controls.Add(this.lblSearchCount);
            this.ClientArea.Controls.Add(this.lblSearch);
            this.ClientArea.Controls.Add(this.Label55);
            this.ClientArea.Size = new System.Drawing.Size(738, 374);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(738, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(90, 30);
            this.HeaderText.Text = "Search";
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 8;
            this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(30, 174);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 1;
            this.Grid.Size = new System.Drawing.Size(687, 182);
            this.Grid.TabIndex = 5;
            this.Grid.MouseDoubleClick += new Wisej.Web.MouseEventHandler(Grid_MouseDoubleClick);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 139);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(630, 15);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "CLICK ON A HEADING TO SORT BY THAT COLUMN.  CLICK A SECOND TIME TO SORT IN REVERS" +
    "E ORDER";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(62, 91);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(141, 18);
            this.Label2.TabIndex = 6;
            this.Label2.Text = " = DELETED ACCOUNT";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Shape1
            // 
            this.Shape1.BackColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.Shape1.Location = new System.Drawing.Point(30, 91);
            this.Shape1.Name = "Shape1";
            this.Shape1.Size = new System.Drawing.Size(15, 20);
            this.Shape1.TabIndex = 8;
            // 
            // lblSearchCount
            // 
            this.lblSearchCount.Location = new System.Drawing.Point(548, 40);
            this.lblSearchCount.Name = "lblSearchCount";
            this.lblSearchCount.Size = new System.Drawing.Size(116, 20);
            this.lblSearchCount.TabIndex = 4;
            this.lblSearchCount.Text = "LABEL99";
            // 
            // lblSearch
            // 
            this.lblSearch.BorderStyle = 1;
            this.lblSearch.Location = new System.Drawing.Point(200, 40);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(274, 21);
            this.lblSearch.TabIndex = 3;
            this.lblSearch.FontUnderline = true;
            // 
            // Label55
            // 
            this.Label55.BackColor = System.Drawing.SystemColors.Window;
            this.Label55.BorderStyle = 1;
            this.Label55.Location = new System.Drawing.Point(30, 40);
            this.Label55.Name = "Label55";
            this.Label55.Size = new System.Drawing.Size(100, 16);
            this.Label55.TabIndex = 2;
            this.Label55.Text = "SEARCHING FOR : ";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 0;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print List";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(260, 35);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(87, 48);
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // frmRESearch
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(738, 542);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmRESearch";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Search";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmRESearch_Load);
            this.Activated += new System.EventHandler(this.frmRESearch_Activated);
            this.Resize += new System.EventHandler(this.frmRESearch_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRESearch_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}

        #endregion

        private FCButton cmdPrint;
		private System.ComponentModel.IContainer components;
	}
}
