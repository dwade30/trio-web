﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptIncomeApproach.
	/// </summary>
	public partial class rptIncomeApproach : BaseSectionReport
	{
		public static rptIncomeApproach InstancePtr
		{
			get
			{
				return (rptIncomeApproach)Sys.GetInstance(typeof(rptIncomeApproach));
			}
		}

		protected rptIncomeApproach _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptIncomeApproach()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}
		// nObj = 1
		//   0	rptIncomeApproach	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngAccount;
		int intCard;
		bool boolLoadFromScreen;
		// vbPorter upgrade warning: CARD As short	OnWriteFCConvert.ToInt32(
		public void Init(ref int ACCOUNT, ref int CARD, string strName, bool boolFromScreen = false)
		{
			lngAccount = ACCOUNT;
			intCard = CARD;
			txtName.Text = strName;
			boolLoadFromScreen = boolFromScreen;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtAccount.Text = lngAccount.ToString();
			txtCard.Text = intCard.ToString();
			//FC:FINAL:RPU:#i1029 - Set SectionReport.Report = null not the SectionReport object
			//subData = null;
			subData.Report = null;
			subData.Report = new srptIncomeApproach();
			subData.Report.UserData = FCConvert.ToString(lngAccount) + "," + FCConvert.ToString(intCard) + "," + FCConvert.ToString(boolLoadFromScreen);
			//SubExpenses = null;
			SubExpenses.Report = null;
			SubExpenses.Report = new srptIncomeExpenses();
			SubExpenses.Report.UserData = FCConvert.ToString(lngAccount) + "," + FCConvert.ToString(intCard) + "," + FCConvert.ToString(boolLoadFromScreen);
			//subSummary = null;
			subSummary.Report = null;
			subSummary.Report = new srptIncomeSummary();
			subSummary.Report.UserData = FCConvert.ToString(lngAccount) + "," + FCConvert.ToString(intCard) + "," + FCConvert.ToString(boolLoadFromScreen);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
