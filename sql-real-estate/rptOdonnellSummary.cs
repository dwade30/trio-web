﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptOdonellSummary.
	/// </summary>
	public partial class rptOdonellSummary : BaseSectionReport
	{
		public rptOdonellSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Summary";
		}

		public static rptOdonellSummary InstancePtr
		{
			get
			{
				return (rptOdonellSummary)Sys.GetInstance(typeof(rptOdonellSummary));
			}
		}

		protected rptOdonellSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOdonnellSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTGRIDCOLACCOUNT = 0;
		const int CNSTGRIDCOLNAME = 1;
		const int CNSTGRIDCOLMAPLOT = 2;
		const int CNSTGRIDCOLLAND = 3;
		const int CNSTGRIDCOLBLDG = 4;
		const int CNSTGRIDCOLEXEMPTION = 5;
		const int CNSTGRIDCOLNEW = 6;
		const int CNSTGRIDCOLTOTACRES = 7;
		const int CNSTGRIDCOLSOFTACRES = 8;
		const int CNSTGRIDCOLMIXEDACRES = 9;
		const int CNSTGRIDCOLHARDACRES = 10;
		const int CNSTGRIDCOLSOFTVALUE = 11;
		const int CNSTGRIDCOLMIXEDVALUE = 12;
		const int CNSTGRIDCOLHARDVALUE = 13;
		const int CNSTGRIDNOTUPDATEDCOLACCOUNT = 0;
		const int CNSTGRIDNOTUPDATEDCOLNAME = 1;
		const int CNSTGRIDNOTUPDATEDCOLMAPLOT = 2;
		const int CNSTGRIDNOTUPDATEDCOLLAND = 3;
		const int CNSTGRIDNOTUPDATEDCOLBLDG = 4;
		const int CNSTGRIDNOTUPDATEDCOLEXEMPTION = 5;
		const int CNSTGRIDNOTUPDATEDCOLTOTACRES = 6;
		const int CNSTGRIDNOTUPDATEDCOLSOFTACRES = 7;
		const int CNSTGRIDNOTUPDATEDCOLMIXEDACRES = 8;
		const int CNSTGRIDNOTUPDATEDCOLHARDACRES = 9;
		const int CNSTGRIDNOTUPDATEDCOLSOFTVALUE = 10;
		const int CNSTGRIDNOTUPDATEDCOLMIXEDVALUE = 11;
		const int CNSTGRIDNOTUPDATEDCOLHARDVALUE = 12;
		double dblLand;
		double dblBldg;
		double dblExempt;
		double dblSoftAcres;
		private double dblMixedAcres;
		private double dblHardAcres;
		private double dblTotAcres;
		private double dblSoftValue;
		private double dblMixedValue;
		private double dblHardValue;
		private double dblUpdatedLand;
		private double dblUpdatedBldg;
		private double dblNewLand;
		private double dblNewBldg;
		private double dblNotUpdatedLand;
		private double dblNotUpdatedBldg;
		private double dblUpdatedExempt;
		private double dblNewExempt;
		private double dblNotUpdatedExempt;

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "Summary", showModal: modalDialog);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int lngRow;
			lngRow = 0;
			txtNumNotUpdated.Text = FCConvert.ToString(frmImportODonnell.InstancePtr.gridNotUpdated.Rows);
			dblLand = 0;
			dblBldg = 0;
			dblExempt = 0;
			dblSoftAcres = 0;
			dblMixedAcres = 0;
			dblHardAcres = 0;
			dblSoftValue = 0;
			dblMixedValue = 0;
			dblHardValue = 0;
			dblTotAcres = 0;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtPage.Text = "Page 1";
			dblUpdatedLand = 0;
			dblUpdatedBldg = 0;
			dblNewLand = 0;
			dblNewBldg = 0;
			dblNotUpdatedLand = 0;
			dblNotUpdatedBldg = 0;
			dblUpdatedExempt = 0;
			dblNewExempt = 0;
			dblNotUpdatedExempt = 0;
			int lngUpdated;
			int lngNew;
			lngUpdated = 0;
			lngNew = 0;
			if (frmImportODonnell.InstancePtr.Grid.Rows > 0)
			{
				for (lngRow = 0; lngRow <= frmImportODonnell.InstancePtr.Grid.Rows - 1; lngRow++)
				{
					dblLand += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLLAND));
					dblBldg += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLBLDG));
					dblExempt += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLEXEMPTION));
					dblSoftAcres += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLSOFTACRES));
					dblMixedAcres += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLMIXEDACRES));
					dblHardAcres += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLHARDACRES));
					dblTotAcres += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLTOTACRES));
					if (FCConvert.CBool(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLNEW)))
					{
						dblNewLand += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLLAND));
						dblNewBldg += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLBLDG));
						dblNewExempt += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLEXEMPTION));
						lngNew += 1;
					}
					else
					{
						dblUpdatedLand += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLLAND));
						dblUpdatedBldg += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLBLDG));
						dblUpdatedExempt += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLEXEMPTION));
						lngUpdated += 1;
					}
				}
				// lngRow
			}
			txtNumNew.Text = lngNew.ToString();
			txtNumUpdated.Text = lngUpdated.ToString();
			if (frmImportODonnell.InstancePtr.gridNotUpdated.Rows > 0)
			{
				for (lngRow = 0; lngRow <= frmImportODonnell.InstancePtr.gridNotUpdated.Rows - 1; lngRow++)
				{
					dblLand += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLLAND));
					dblBldg += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLBLDG));
					dblExempt += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLEXEMPTION));
					dblSoftAcres += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLSOFTACRES));
					dblMixedAcres += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLMIXEDACRES));
					dblHardAcres += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLHARDACRES));
					dblTotAcres += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLTOTACRES));
					dblNotUpdatedLand += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLLAND));
					dblNotUpdatedBldg += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLBLDG));
					dblNotUpdatedExempt += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLEXEMPTION));
				}
				// lngRow
			}
			txtNumNotUpdated.Text = FCConvert.ToString(frmImportODonnell.InstancePtr.gridNotUpdated.Rows);
			txtTotBldg.Text = Strings.Format(dblBldg, "#,###,###,##0");
			txtTotLand.Text = Strings.Format(dblLand, "#,###,###,##0");
			txtTotExemption.Text = Strings.Format(dblExempt, "#,###,###,##0");
			txtSoftAcres.Text = Strings.Format(dblSoftAcres, "#,###,###,##0.00");
			txtMixedAcres.Text = Strings.Format(dblMixedAcres, "#,###,###,##0.00");
			txtHardAcres.Text = Strings.Format(dblHardAcres, "#,###,###,##0.00");
			txtTotalAcres.Text = Strings.Format(dblTotAcres, "#,###,###,##0.00");
			txtSoftValue.Text = Strings.Format(dblSoftValue, "#,###,###,##0");
			txtMixedValue.Text = Strings.Format(dblMixedValue, "#,###,###,##0");
			txtHardValue.Text = Strings.Format(dblHardValue, "#,###,###,##0");
			txtTotalLand.Text = txtTotLand.Text;
			txtOtherAcres.Text = Strings.Format((dblTotAcres - (dblMixedAcres + dblSoftAcres + dblHardAcres)), "#,###,###,##0.00");
			txtOtherValue.Text = Strings.Format(dblLand - (dblSoftValue + dblMixedValue + dblHardValue), "#,###,###,###,##0");
			txtTotUpdatedBuilding.Text = Strings.Format(dblUpdatedBldg, "#,###,###,##0");
			txtTotUpdatedLand.Text = Strings.Format(dblUpdatedLand, "#,###,###,##0");
			txtTotUpdatedExemption.Text = Strings.Format(dblUpdatedExempt, "#,###,###,##0");
			txtNewland.Text = Strings.Format(dblNewLand, "#,###,###,##0");
			txtNewBldg.Text = Strings.Format(dblNewBldg, "#,###,###,##0");
			txtNewExempt.Text = Strings.Format(dblNewExempt, "#,###,###,##0");
			txtNotUpdatedBldg.Text = Strings.Format(dblNotUpdatedBldg, "#,###,###,##0");
			txtNotUpdatedLand.Text = Strings.Format(dblNotUpdatedLand, "#,###,###,##0");
			txtNotUpdatedExempt.Text = Strings.Format(dblNotUpdatedExempt, "#,###,###,##0");
		}

		
	}
}
