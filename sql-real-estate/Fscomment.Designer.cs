//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmFSComment.
	/// </summary>
	partial class frmFSComment : BaseForm
	{
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdPrevious;
		public fecherFoundation.FCButton cmdNext;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCRichTextBox rtbC1;
		public fecherFoundation.FCLabel lblKey;
		public fecherFoundation.FCLabel lblScreen2;
		public fecherFoundation.FCLabel lblCommentAccount;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFSComment));
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdPrevious = new fecherFoundation.FCButton();
			this.cmdNext = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.rtbC1 = new fecherFoundation.FCRichTextBox();
			this.lblKey = new fecherFoundation.FCLabel();
			this.lblScreen2 = new fecherFoundation.FCLabel();
			this.lblCommentAccount = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrevious)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNext)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.rtbC1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 565);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdPrevious);
			this.ClientArea.Controls.Add(this.cmdNext);
			this.ClientArea.Controls.Add(this.rtbC1);
			this.ClientArea.Controls.Add(this.lblKey);
			this.ClientArea.Controls.Add(this.lblScreen2);
			this.ClientArea.Controls.Add(this.lblCommentAccount);
			this.ClientArea.Size = new System.Drawing.Size(610, 505);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(119, 30);
			this.HeaderText.Text = "Comment";
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.ForeColor = System.Drawing.Color.White;
			this.cmdSave.Location = new System.Drawing.Point(263, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(72, 48);
			this.cmdSave.TabIndex = 7;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// cmdPrevious
			// 
			this.cmdPrevious.AppearanceKey = "actionButton";
			this.cmdPrevious.ForeColor = System.Drawing.Color.White;
			this.cmdPrevious.Location = new System.Drawing.Point(30, 462);
			this.cmdPrevious.Name = "cmdPrevious";
			this.cmdPrevious.Size = new System.Drawing.Size(84, 40);
			this.cmdPrevious.TabIndex = 5;
			this.cmdPrevious.Text = "Previous";
			this.cmdPrevious.Visible = false;
			// 
			// cmdNext
			// 
			this.cmdNext.AppearanceKey = "actionButton";
			this.cmdNext.ForeColor = System.Drawing.Color.White;
			this.cmdNext.Location = new System.Drawing.Point(134, 462);
			this.cmdNext.Name = "cmdNext";
			this.cmdNext.Size = new System.Drawing.Size(67, 40);
			this.cmdNext.TabIndex = 6;
			this.cmdNext.Text = "Next";
			this.cmdNext.Visible = false;
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(473, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(57, 24);
			this.cmdDelete.TabIndex = 8;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(534, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(47, 24);
			this.cmdPrint.TabIndex = 9;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// rtbC1
			// 
			this.rtbC1.Location = new System.Drawing.Point(30, 70);
			this.rtbC1.Multiline = true;
			this.rtbC1.Name = "rtbC1";
			this.rtbC1.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.rtbC1.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.rtbC1.SelTabCount = null;
			this.rtbC1.Size = new System.Drawing.Size(1086, 450);
			this.rtbC1.TabIndex = 2;
			// 
			// lblKey
			// 
			this.lblKey.Location = new System.Drawing.Point(30, 445);
			this.lblKey.Name = "lblKey";
			this.lblKey.Size = new System.Drawing.Size(28, 16);
			this.lblKey.TabIndex = 3;
			// 
			// lblScreen2
			// 
			this.lblScreen2.Location = new System.Drawing.Point(542, 30);
			this.lblScreen2.Name = "lblScreen2";
			this.lblScreen2.Size = new System.Drawing.Size(28, 16);
			this.lblScreen2.TabIndex = 1;
			// 
			// lblCommentAccount
			// 
			this.lblCommentAccount.Location = new System.Drawing.Point(30, 30);
			this.lblCommentAccount.Name = "lblCommentAccount";
			this.lblCommentAccount.Size = new System.Drawing.Size(500, 20);
			this.lblCommentAccount.TabIndex = 0;
			this.lblCommentAccount.Text = "ACCOUNT# ";
			// 
			// frmFSComment
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 673);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmFSComment";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Comment";
			this.Load += new System.EventHandler(this.frmFSComment_Load);
			this.Activated += new System.EventHandler(this.frmFSComment_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmFSComment_KeyUp);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrevious)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNext)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.rtbC1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}