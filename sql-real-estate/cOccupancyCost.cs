﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace TWRE0000
{
	public class cOccupancyCost
	{
		//=========================================================
		private int lngRecordID;
		private int lngOccupancyCodeID;
		private int lngConstructionClassID;
		private int lngQualityCode;
		private double dblSqftCost;
		private int intLife;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public short Life
		{
			set
			{
				intLife = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Life = 0;
				Life = FCConvert.ToInt16(intLife);
				return Life;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int OccupancyCodeID
		{
			set
			{
				lngOccupancyCodeID = value;
				IsUpdated = true;
			}
			get
			{
				int OccupancyCodeID = 0;
				OccupancyCodeID = lngOccupancyCodeID;
				return OccupancyCodeID;
			}
		}

		public int QualityCode
		{
			set
			{
				lngQualityCode = value;
				IsUpdated = true;
			}
			get
			{
				int QualityCode = 0;
				QualityCode = lngQualityCode;
				return QualityCode;
			}
		}

		public double SqftCost
		{
			set
			{
				dblSqftCost = value;
				IsUpdated = true;
			}
			get
			{
				double SqftCost = 0;
				SqftCost = dblSqftCost;
				return SqftCost;
			}
		}

		public int ConstructionClassID
		{
			set
			{
				lngConstructionClassID = value;
				IsUpdated = true;
			}
			get
			{
				int ConstructionClassID = 0;
				ConstructionClassID = lngConstructionClassID;
				return ConstructionClassID;
			}
		}
	}
}
