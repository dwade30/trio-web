﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWRE0000
{
	public class cREXferSetting
	{
		//=========================================================
		private bool bUsesBlock;
		private bool bUsesUnits;
		private bool bUsesLot;
		private bool bMatchByAccount;
		private bool bMatchByTrioAccount;
		private bool bPPMatchByOpen1;
		private bool bPPMatchByTrioAccount;
		private bool bOmitTrailingZeroes;
		private int inExtraChars;
		private int inBlankRecords;
		private bool bUsesCRLF;
		private int lAssessType;
		private string strMuniName = string.Empty;
		private int inDivisionSize;
		private int inPPExtraCharacters;
		private bool bUseMultiOwner;
		private int inOwnerOption;
		private bool bSplitValuation;
		private bool bImportPreviousOwner;
		private bool bUsePartyAddressFromSystem;

		public bool boolUsesBlock
		{
			set
			{
				bUsesBlock = value;
			}
			get
			{
				bool boolUsesBlock = false;
				boolUsesBlock = bUsesBlock;
				return boolUsesBlock;
			}
		}

		public bool boolUsesUnits
		{
			set
			{
				bUsesUnits = value;
			}
			get
			{
				bool boolUsesUnits = false;
				boolUsesUnits = bUsesUnits;
				return boolUsesUnits;
			}
		}

		public bool boolUsesLot
		{
			set
			{
				bUsesLot = value;
			}
			get
			{
				bool boolUsesLot = false;
				boolUsesLot = bUsesLot;
				return boolUsesLot;
			}
		}

		public bool boolMatchByAccount
		{
			set
			{
				bMatchByAccount = value;
			}
			get
			{
				bool boolMatchByAccount = false;
				boolMatchByAccount = bMatchByAccount;
				return boolMatchByAccount;
			}
		}

		public bool boolMatchByTRIOAccount
		{
			set
			{
				bMatchByTrioAccount = value;
			}
			get
			{
				bool boolMatchByTRIOAccount = false;
				boolMatchByTRIOAccount = bMatchByTrioAccount;
				return boolMatchByTRIOAccount;
			}
		}

		public bool boolPPMatchByOpen1
		{
			set
			{
				bPPMatchByOpen1 = value;
			}
			get
			{
				bool boolPPMatchByOpen1 = false;
				boolPPMatchByOpen1 = bPPMatchByOpen1;
				return boolPPMatchByOpen1;
			}
		}

		public bool boolPPMatchByTRIOaccount
		{
			set
			{
				bPPMatchByTrioAccount = value;
			}
			get
			{
				bool boolPPMatchByTRIOaccount = false;
				boolPPMatchByTRIOaccount = bPPMatchByTrioAccount;
				return boolPPMatchByTRIOaccount;
			}
		}

		public bool boolOmitTrailingZeroes
		{
			set
			{
				bOmitTrailingZeroes = value;
			}
			get
			{
				bool boolOmitTrailingZeroes = false;
				boolOmitTrailingZeroes = bOmitTrailingZeroes;
				return boolOmitTrailingZeroes;
			}
		}

		public int intExtraChars
		{
			set
			{
				inExtraChars = value;
			}
			get
			{
				int intExtraChars = 0;
				intExtraChars = inExtraChars;
				return intExtraChars;
			}
		}

		public int intBlankRecords
		{
			set
			{
				inBlankRecords = value;
			}
			get
			{
				int intBlankRecords = 0;
				intBlankRecords = inBlankRecords;
				return intBlankRecords;
			}
		}

		public bool boolUsesCRLF
		{
			set
			{
				bUsesCRLF = value;
			}
			get
			{
				bool boolUsesCRLF = false;
				boolUsesCRLF = bUsesCRLF;
				return boolUsesCRLF;
			}
		}

		public int AssessType
		{
			set
			{
				lAssessType = value;
			}
			get
			{
				int AssessType = 0;
				AssessType = lAssessType;
				return AssessType;
			}
		}

		public string MuniName
		{
			set
			{
				strMuniName = value;
			}
			get
			{
				string MuniName = "";
				MuniName = strMuniName;
				return MuniName;
			}
		}

		public int intDivisionSize
		{
			set
			{
				inDivisionSize = value;
			}
			get
			{
				int intDivisionSize = 0;
				intDivisionSize = inDivisionSize;
				return intDivisionSize;
			}
		}

		public int intPPExtraCharacters
		{
			set
			{
				inPPExtraCharacters = value;
			}
			get
			{
				int intPPExtraCharacters = 0;
				intPPExtraCharacters = inPPExtraCharacters;
				return intPPExtraCharacters;
			}
		}

		public bool boolUseMultiOwner
		{
			set
			{
				bUseMultiOwner = value;
			}
			get
			{
				bool boolUseMultiOwner = false;
				boolUseMultiOwner = bUseMultiOwner;
				return boolUseMultiOwner;
			}
		}

		public int intOwnerOption
		{
			set
			{
				inOwnerOption = value;
			}
			get
			{
				int intOwnerOption = 0;
				intOwnerOption = inOwnerOption;
				return intOwnerOption;
			}
		}

		public bool boolSplitValuation
		{
			set
			{
				bSplitValuation = value;
			}
			get
			{
				bool boolSplitValuation = false;
				boolSplitValuation = bSplitValuation;
				return boolSplitValuation;
			}
		}

		public bool boolImportPreviousOwner
		{
			set
			{
				bImportPreviousOwner = value;
			}
			get
			{
				bool boolImportPreviousOwner = false;
				boolImportPreviousOwner = bImportPreviousOwner;
				return boolImportPreviousOwner;
			}
		}

		public bool UsePartyAddressFromSystem
		{
			set
			{
				bUsePartyAddressFromSystem = value;
			}
			get
			{
				bool UsePartyAddressFromSystem = false;
				UsePartyAddressFromSystem = bUsePartyAddressFromSystem;
				return UsePartyAddressFromSystem;
			}
		}
	}
}
