﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Commands;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmArchive.
	/// </summary>
	public partial class frmArchive : BaseForm
	{
		public frmArchive()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmArchive InstancePtr
		{
			get
			{
				return (frmArchive)Sys.GetInstance(typeof(frmArchive));
			}
		}

		protected frmArchive _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strThisModule;
		string strLastDrive = "";
		bool boolOkToCreate;
		int lngRowToEdit;
		const int CNSTGRIDCOLYEAR = 0;
		const int CNSTGRIDCOLDESC = 1;
		const int CNSTGRIDCOLDIRECTORY = 2;
		const int CNSTGRIDCOLAUTOID = 3;
		const string CNSTREPPARCHIVETYPE = "CommitmentArchive";
		private frmNewArchive newArchiveForm = null;

		public void Init(string strDB, bool boolCreate)
		{
			boolOkToCreate = boolCreate;
			if (!boolOkToCreate)
			{
				cmdCreateArchive.Enabled = false;
				cmdNew.Visible = false;
			}
			strThisModule = strDB;
			this.Show(App.MainForm);
		}

		private void cmdNew_Click(object sender, System.EventArgs e)
		{
			AddArchive();
		}

		private void AddArchive()
		{
            if (modGlobalVariables.Statics.boolInArchiveMode)
            {
                MessageBox.Show("Please switch to CURRENT YEAR to add new archive", "Add archive", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int intMin;
			int intMax;
			int intYear;
			int x;
			intMin = 2000;
			for (x = 1; x <= Grid.Rows - 1; x++)
			{
				if (Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLYEAR)) > intMin)
				{
					intMin = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLYEAR))));
				}
			}
			// x
			intMin += 1;
			intYear = DateTime.Now.Year;
			if (intYear < intMin)
			{
				intYear = intMin;
			}
			intMax = DateTime.Now.Year + 1;
			if (intMax < intMin)
			{
				MessageBox.Show("No valid archive year can be created." + "\r\n" + "If you wish to overwrite an archive you must remove it first.", "No Valid Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			newArchiveForm = new frmNewArchive();
			newArchiveForm.Init(intMin, intMax, intYear);
			//FC:FINAL:MSH - i.issue #1182: assign handler for reloading archives table after creating new archive
			newArchiveForm.ArchiveCreated -= new frmNewArchive.ArchiveCreatedEventHandler(newArchiveForm_ArchiveCreated);
			newArchiveForm.ArchiveCreated += new frmNewArchive.ArchiveCreatedEventHandler(newArchiveForm_ArchiveCreated);
			// Dim s As New SettingsInfo
			// s.ReloadArchives
			// FillGrid
		}

		private void frmArchive_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmArchive properties;
			//frmArchive.ScaleWidth	= 5880;
			//frmArchive.ScaleHeight	= 4245;
			//frmArchive.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			SetupGrid();
			FillGrid();
		}

		private void frmArchive_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.Row > 0)
			{
				RunArchive(Grid.Row);
			}
		}

		private void RunArchive(int intRow)
		{
            //FC:FINAL:MSH - issue #1727: add message that Archive mode can't be executed if any other forms are opened
            int twreOpenForms = Application.OpenForms.OfType<Form>().Where(form => form.GetType().Assembly.GetName().Name.ToUpper() == "TWRE0000").Count();
            if (twreOpenForms > 1)
            {
                MessageBox.Show("All Real Estate tabs must be closed before changing the working year", "Run Archive", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

			if (intRow > 0 && intRow <= Grid.Rows - 1)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				int intYear = 0;
				intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(intRow, CNSTGRIDCOLYEAR))));
				if (intYear > 2000)
				{
					rsLoad.DefaultGroup = CNSTREPPARCHIVETYPE + "_" + FCConvert.ToString(intYear);
					// load menu color and archive year screen
					//modGlobalFunctions.SetMenuBackColor();
					modGlobalConstants.Statics.gstrArchiveYear = Grid.TextMatrix(intRow, 0);
					//FC:FINAL:MSH - show "Archive" image for informing user that module in archive mode
					//modArchiveImage.ShowArchiveImage(ref modGlobalConstants.Statics.gstrArchiveYear);
					modGlobalVariables.Statics.boolInArchiveMode = true;
					//MDIParent.InstancePtr.StatusBar1.Panels[0].Text = modGlobalConstants.Statics.MuniName + " " + modGlobalConstants.Statics.gstrArchiveYear;
					App.MainForm.StatusBarText1 = TWSharedLibrary.Variables.Statics.DataGroup + " " + modGlobalConstants.Statics.gstrArchiveYear;
					// do a forceupdate
					modGlobalConstants.Statics.MuniName = TWSharedLibrary.Variables.Statics.DataGroup + " " + modGlobalConstants.Statics.gstrArchiveYear;
					modGlobalRoutines.ModSpecificReset();
					StaticSettings.GlobalCommandDispatcher.Send(new SetArchiveMode
					{
						ArchiveMode = modGlobalConstants.Statics.gstrArchiveYear != "",
						ArchiveTag = modGlobalConstants.Statics.gstrArchiveYear == "" ? "Live" : rsLoad.DefaultGroup
					});
					this.Unload();
				}
                else if (intYear == 0)
                {
                    rsLoad.DefaultGroup = "Live";
                    modGlobalConstants.Statics.gstrArchiveYear = "";
                    modGlobalVariables.Statics.boolInArchiveMode = false;
                    App.MainForm.StatusBarText1 = TWSharedLibrary.Variables.Statics.DataGroup;
                    modGlobalConstants.Statics.MuniName = TWSharedLibrary.Variables.Statics.DataGroup;
					//modPPGN.ModSpecificReset();
					StaticSettings.GlobalCommandDispatcher.Send(new SetArchiveMode
					{
						ArchiveMode = modGlobalConstants.Statics.gstrArchiveYear != "",
						ArchiveTag = modGlobalConstants.Statics.gstrArchiveYear == "" ? "Live" : rsLoad.DefaultGroup
					});
					Close();
                }
                else
				{
					MessageBox.Show("The archive year is not valid.", "Invalid Archive Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuCreateArchive_Click(object sender, System.EventArgs e)
		{
			AddArchive();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{   
            if (Grid.Row > 0)
			{
                if (modGlobalVariables.Statics.boolInArchiveMode)
                {
                    MessageBox.Show("Please switch to CURRENT YEAR to delete an archive", "Delete archive", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (MessageBox.Show("This will permanently delete the archive databases in this archive." + "\r\n" + "Are you sure you want to do delete it?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
				{
					DeleteArchive(Grid.Row);
				}
			}
			else
			{
				MessageBox.Show("You haven't selected an archive.", "No Archive Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void DeleteArchive(int intRow)
		{
			cSystemSettings tSet = new cSystemSettings();
			cArchiveUtility tArch = new cArchiveUtility();
			cArchiveItem tArchive = new cArchiveItem();
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strDBName = "";
			string strGroupName;
			clsDRWrapper rsSave = new clsDRWrapper();
			tArchive.ArchiveID = FCConvert.ToInt16(Math.Round(Conversion.Val(Grid.TextMatrix(intRow, CNSTGRIDCOLYEAR))));
			tArchive.ArchiveType = CNSTREPPARCHIVETYPE;
			strGroupName = tArch.DeriveGroupName(tArchive.ArchiveType, tArchive.ArchiveID);
			modGlobalFunctions.AddCYAEntry_8("RE", "Deleted Commitment Archive", strGroupName);
			strSQL = "select * from sys.databases where name like N'TRIO_" + rsLoad.MegaGroup + "_" + strGroupName + "%'";
			rsLoad.OpenRecordset(strSQL, "SystemSettings");
			while (!rsLoad.EndOfFile())
			{
				strDBName = FCConvert.ToString(rsLoad.Get_Fields_String("name"));
				strSQL = "alter database [" + strDBName + "] set single_user with rollback immediate; Drop Database [" + strDBName + "]";
				rsSave.Execute(strSQL, "SystemSettings");
				rsLoad.MoveNext();
			}
			tSet.RemoveArchiveItem(ref tArchive);
			SettingsInfo s = new SettingsInfo();
			s.ReloadArchives();
			FillGrid();
		}

		private void FillGrid()
		{
			cArchiveUtility tArch = new cArchiveUtility();
			FCCollection tColl = new FCCollection();
			//ConnectionGroup tcGroup = new ConnectionGroup();
			SettingsInfo s = new SettingsInfo();
			s.ReloadArchives();
			tColl = tArch.GetArchiveConnectionGroups(CNSTREPPARCHIVETYPE, 0);
			Grid.Rows = 1;
			int intRow;
            if (modGlobalVariables.Statics.boolInArchiveMode == true)
            {
                Grid.Rows += 1;
                intRow = Grid.Rows - 1;
                Grid.TextMatrix(intRow, CNSTGRIDCOLDESC, "CURRENT YEAR DATA");
                Grid.TextMatrix(intRow, CNSTGRIDCOLYEAR, "CURRENT");
            }
            if (!(tColl == null))
			{
				foreach (ConnectionGroup tcGroup in tColl)
				{
					Grid.Rows += 1;
					intRow = Grid.Rows - 1;
					Grid.TextMatrix(intRow, CNSTGRIDCOLDESC, tcGroup.Description);
					Grid.TextMatrix(intRow, CNSTGRIDCOLYEAR, FCConvert.ToString(tcGroup.ID));
				}
				// tcGroup
			}
		}

		private void SetupGrid()
		{
			Grid.TextMatrix(0, CNSTGRIDCOLYEAR, "Year");
			Grid.TextMatrix(0, CNSTGRIDCOLDESC, "Description");
			Grid.ColHidden(CNSTGRIDCOLDIRECTORY, true);
			Grid.ColHidden(CNSTGRIDCOLAUTOID, true);

            Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

		private void ResizeGrid()
		{
			int lngGridWidth = 0;
			lngGridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.2 * lngGridWidth));
		}

		private void mnuRunArchive_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row > 0)
			{
				RunArchive(Grid.Row);
			}
			else
			{
				MessageBox.Show("You haven't selected an archive.", "No Archive Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void newArchiveForm_ArchiveCreated()
		{
			SettingsInfo s = new SettingsInfo();
			s.ReloadArchives();
			FillGrid();
			/*- newArchiveForm = null; */
		}
	}
}
