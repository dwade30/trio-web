//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Text;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmSHREBLUpdate.
	/// </summary>
	partial class frmSHREBLUpdate : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label8;
		public fecherFoundation.FCButton cmdRemoveSecOwner;
		public fecherFoundation.FCButton cmdRemoveOwner;
		public fecherFoundation.FCTextBox txt2ndOwnerID;
		public fecherFoundation.FCButton cmdEditSecOwner;
		public fecherFoundation.FCButton cmdSearchSecOwner;
		public fecherFoundation.FCTextBox txtOwnerID;
		public fecherFoundation.FCButton cmdEditOwner;
		public fecherFoundation.FCButton cmdSearchOwner;
		public FCGrid gridLandCode;
		public FCGrid SaleGrid;
		public fecherFoundation.FCTextBox txtMRPIACRES;
		public fecherFoundation.FCTextBox txtMRPIOPEN1;
		public fecherFoundation.FCTextBox txtMRPIOPEN2;
		public fecherFoundation.FCTextBox txtMRDIDATEINSPECTED;
		public fecherFoundation.FCTextBox txtMRDIENTRANCECODE;
		public fecherFoundation.FCTextBox txtMRDIINFORMATION;
		public fecherFoundation.FCCheckBox chkRLivingTrust;
		public FCGrid GridExempts;
		public fecherFoundation.FCTextBox txtPreviousOwner;
		public fecherFoundation.FCCheckBox chkBankruptcy;
		public fecherFoundation.FCCheckBox chkTaxAcquired;
		public fecherFoundation.FCTextBox txtMRRLLANDVAL;
		public fecherFoundation.FCTextBox txtMRRLBLDGVAL;
		public fecherFoundation.FCTextBox txtMRRLEXEMPTION;
		public fecherFoundation.FCTextBox txtMRRSMAPLOT;
		public fecherFoundation.FCTextBox txtMRRSREF2;
		public fecherFoundation.FCTextBox txtMRRSREF1;
		public fecherFoundation.FCTextBox txtMRRSLOCNUMALPH;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel lblTotalAcreage;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel lblMRRLLANDVAL;
		public fecherFoundation.FCLabel lblMRRLBLDGVAL;
		public fecherFoundation.FCLabel lblMRRLEXEMPTION;
		public fecherFoundation.FCLabel lblNetAssessment;
		public fecherFoundation.FCLabel lblTaxAmount;
		public fecherFoundation.FCLabel lblTaxRate;
		public fecherFoundation.FCLabel TaxAmountLabel;
		public fecherFoundation.FCLabel TaxRateLabel;
		public fecherFoundation.FCTextBox txtMRRSLOCAPT;
		public fecherFoundation.FCTextBox txtMRRSLOCSTREET;
		public fecherFoundation.FCTextBox txtDocumentLocationName;
		public fecherFoundation.FCCommonDialog CommonDialog1;
		public fecherFoundation.FCGrid BPGrid;
		public fecherFoundation.FCGrid TreeGrid;
		public fecherFoundation.FCGrid MapLotGrid;
		public FCGrid gridBldgCode;
		public FCGrid GridDeletedPhones;
		public FCGrid gridPropertyCode;
		public FCGrid gridTranCode;
		public fecherFoundation.FCPanel framSaleRecord;
		public fecherFoundation.FCTextBox txtMRRSZIP4;
		public fecherFoundation.FCTextBox txtMRRSADDR1;
		public fecherFoundation.FCTextBox txtMRRSADDR2;
		public fecherFoundation.FCTextBox txtMRRSADDR3;
		public fecherFoundation.FCTextBox txtMRRSZIP;
		public fecherFoundation.FCTextBox txtMRRSSTATE;
		public fecherFoundation.FCLabel Label32;
		public fecherFoundation.FCLabel Label30;
		public fecherFoundation.FCLabel Label29;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label26;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCLabel lblAddress;
		public fecherFoundation.FCLabel lbl2ndOwner;
		public fecherFoundation.FCLabel lblOwner1;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Open2Label;
		public fecherFoundation.FCLabel Open1Label;
		public fecherFoundation.FCLabel Label39;
		public fecherFoundation.FCLabel Label41;
		public fecherFoundation.FCLabel Label42;
		public fecherFoundation.FCLabel lblMRDIINFORMATION;
		public fecherFoundation.FCLabel lblMRDIENTRANCE;
		public fecherFoundation.FCLabel Label31;
		public fecherFoundation.FCLabel lblMRHLUPDATE;
		public fecherFoundation.FCLabel Label27;
		public fecherFoundation.FCLabel lblComment;
		public fecherFoundation.FCLabel lblAlertLabel;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label35;
		public fecherFoundation.FCLabel lblMRHLCARDACCT1;
		public fecherFoundation.FCLabel lblHighCard;
		public fecherFoundation.FCLabel Label28;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label43;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8_0;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel lblMRHIUPDCODE;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddAccount;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteCard;
		public fecherFoundation.FCToolStripMenuItem Separator;
		public fecherFoundation.FCToolStripMenuItem mnuGotoFirstCard;
		public fecherFoundation.FCToolStripMenuItem mnuGotoNextCard;
		public fecherFoundation.FCToolStripMenuItem mnuGotoPreviousCard;
		public fecherFoundation.FCToolStripMenuItem mnuGotoLastCard;
		public fecherFoundation.FCToolStripMenuItem mnuOptions;
		public fecherFoundation.FCToolStripMenuItem mnuComments;
		public fecherFoundation.FCToolStripMenuItem mnuRECLComment;
		public fecherFoundation.FCToolStripMenuItem mnuCollectionsNote;
		public fecherFoundation.FCToolStripMenuItem mnuInterestedParties;
		public fecherFoundation.FCToolStripMenuItem mnuPendingTransfer;
		public fecherFoundation.FCToolStripMenuItem mnuSearch;
		public fecherFoundation.FCToolStripMenuItem mnuPreviousSave;
		public fecherFoundation.FCToolStripMenuItem mnuNextSave;
		public fecherFoundation.FCToolStripMenuItem mnuEdit;
		public fecherFoundation.FCToolStripMenuItem mnuEditPicture;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintLabel1;
		public fecherFoundation.FCToolStripMenuItem mnuPrintMailing;
		public fecherFoundation.FCToolStripMenuItem mnuPrintCard;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAllCards;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAccountInformationSheet;
		public fecherFoundation.FCToolStripMenuItem mnuPrintInterestedParties;
		public fecherFoundation.FCToolStripMenuItem mnuValuationReport;
		public fecherFoundation.FCToolStripMenuItem Separator2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveQuit;
		public fecherFoundation.FCToolStripMenuItem mnuSavePending;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		private Wisej.Web.ToolTip ToolTip1;
        private Wisej.Web.ToolTip ToolTip2;

        protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSHREBLUpdate));
			this.txtMRRLLANDVAL = new fecherFoundation.FCTextBox();
			this.txtMRRLBLDGVAL = new fecherFoundation.FCTextBox();
			this.txtMRRLEXEMPTION = new fecherFoundation.FCTextBox();
			this.cmdRemoveSecOwner = new fecherFoundation.FCButton();
			this.cmdRemoveOwner = new fecherFoundation.FCButton();
			this.txt2ndOwnerID = new fecherFoundation.FCTextBox();
			this.cmdEditSecOwner = new fecherFoundation.FCButton();
			this.cmdSearchSecOwner = new fecherFoundation.FCButton();
			this.txtOwnerID = new fecherFoundation.FCTextBox();
			this.cmdEditOwner = new fecherFoundation.FCButton();
			this.cmdSearchOwner = new fecherFoundation.FCButton();
			this.gridLandCode = new fecherFoundation.FCGrid();
			this.SaleGrid = new fecherFoundation.FCGrid();
			this.txtMRDIDATEINSPECTED = new fecherFoundation.FCTextBox();
			this.txtMRDIINFORMATION = new fecherFoundation.FCTextBox();
			this.Label42 = new fecherFoundation.FCLabel();
			this.Label41 = new fecherFoundation.FCLabel();
			this.txtMRDIENTRANCECODE = new fecherFoundation.FCTextBox();
			this.Label39 = new fecherFoundation.FCLabel();
			this.txtMRRSLOCAPT = new fecherFoundation.FCTextBox();
			this.txtMRPIACRES = new fecherFoundation.FCTextBox();
			this.Open1Label = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.txtMRPIOPEN1 = new fecherFoundation.FCTextBox();
			this.txtMRPIOPEN2 = new fecherFoundation.FCTextBox();
			this.Open2Label = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label8_0 = new fecherFoundation.FCLabel();
			this.txtPreviousOwner = new fecherFoundation.FCTextBox();
			this.Label7 = new fecherFoundation.FCLabel();
			this.txtMRRSREF2 = new fecherFoundation.FCTextBox();
			this.txtMRRSREF1 = new fecherFoundation.FCTextBox();
			this.txtMRRSLOCNUMALPH = new fecherFoundation.FCTextBox();
			this.txtMRRSLOCSTREET = new fecherFoundation.FCTextBox();
			this.chkRLivingTrust = new fecherFoundation.FCCheckBox();
			this.GridExempts = new fecherFoundation.FCGrid();
			this.chkBankruptcy = new fecherFoundation.FCCheckBox();
			this.chkTaxAcquired = new fecherFoundation.FCCheckBox();
			this.txtMRRSMAPLOT = new fecherFoundation.FCTextBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.lblTotalAcreage = new fecherFoundation.FCLabel();
			this.Label19 = new fecherFoundation.FCLabel();
			this.Label23 = new fecherFoundation.FCLabel();
			this.Label21 = new fecherFoundation.FCLabel();
			this.Label20 = new fecherFoundation.FCLabel();
			this.Label24 = new fecherFoundation.FCLabel();
			this.lblMRRLLANDVAL = new fecherFoundation.FCLabel();
			this.lblMRRLBLDGVAL = new fecherFoundation.FCLabel();
			this.lblMRRLEXEMPTION = new fecherFoundation.FCLabel();
			this.lblNetAssessment = new fecherFoundation.FCLabel();
			this.lblTaxAmount = new fecherFoundation.FCLabel();
			this.lblTaxRate = new fecherFoundation.FCLabel();
			this.TaxAmountLabel = new fecherFoundation.FCLabel();
			this.TaxRateLabel = new fecherFoundation.FCLabel();
			this.txtDocumentLocationName = new fecherFoundation.FCTextBox();
			this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
			this.BPGrid = new fecherFoundation.FCGrid();
			this.TreeGrid = new fecherFoundation.FCGrid();
			this.MapLotGrid = new fecherFoundation.FCGrid();
			this.gridBldgCode = new fecherFoundation.FCGrid();
			this.GridDeletedPhones = new fecherFoundation.FCGrid();
			this.gridPropertyCode = new fecherFoundation.FCGrid();
			this.gridTranCode = new fecherFoundation.FCGrid();
			this.framSaleRecord = new fecherFoundation.FCPanel();
			this.txtMRRSNAME = new fecherFoundation.FCTextBox();
			this.txtMRRSSECOWNER = new fecherFoundation.FCTextBox();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.txtMRRSZIP4 = new fecherFoundation.FCTextBox();
			this.txtMRRSADDR1 = new fecherFoundation.FCTextBox();
			this.txtMRRSADDR2 = new fecherFoundation.FCTextBox();
			this.txtMRRSADDR3 = new fecherFoundation.FCTextBox();
			this.txtMRRSZIP = new fecherFoundation.FCTextBox();
			this.txtMRRSSTATE = new fecherFoundation.FCTextBox();
			this.Label32 = new fecherFoundation.FCLabel();
			this.Label30 = new fecherFoundation.FCLabel();
			this.Label29 = new fecherFoundation.FCLabel();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Label26 = new fecherFoundation.FCLabel();
			this.Label25 = new fecherFoundation.FCLabel();
			this.lblOwner1 = new fecherFoundation.FCLabel();
			this.lbl2ndOwner = new fecherFoundation.FCLabel();
			this.fcPanel4 = new fecherFoundation.FCPanel();
			this.Label43 = new fecherFoundation.FCLabel();
			this.fcPanel3 = new fecherFoundation.FCPanel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblAddress = new fecherFoundation.FCLabel();
			this.Label22 = new fecherFoundation.FCLabel();
			this.lblMRDIINFORMATION = new fecherFoundation.FCLabel();
			this.lblMRDIENTRANCE = new fecherFoundation.FCLabel();
			this.Label31 = new fecherFoundation.FCLabel();
			this.lblMRHLUPDATE = new fecherFoundation.FCLabel();
			this.Label27 = new fecherFoundation.FCLabel();
			this.lblComment = new fecherFoundation.FCLabel();
			this.lblAlertLabel = new fecherFoundation.FCLabel();
			this.Label18 = new fecherFoundation.FCLabel();
			this.Label35 = new fecherFoundation.FCLabel();
			this.lblMRHLCARDACCT1 = new fecherFoundation.FCLabel();
			this.lblHighCard = new fecherFoundation.FCLabel();
			this.Label28 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label15 = new fecherFoundation.FCLabel();
			this.Label16 = new fecherFoundation.FCLabel();
			this.Label17 = new fecherFoundation.FCLabel();
			this.lblMRHIUPDCODE = new fecherFoundation.FCLabel();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuGotoFirstCard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGotoNextCard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGotoPreviousCard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGotoLastCard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuViewGroupInformation = new fecherFoundation.FCToolStripMenuItem();
			this.mnuViewMortgageInformation = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptions = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRECLComment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCollectionsNote = new fecherFoundation.FCToolStripMenuItem();
			this.mnuInterestedParties = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPendingTransfer = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPreviousSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNextSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEdit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditPicture = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintMailing = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintAllCards = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintAccountInformationSheet = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintInterestedParties = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSavePending = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteCard = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuValuationReport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintCard = new fecherFoundation.FCToolStripMenuItem();
			this.Separator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuComments = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintLabel1 = new fecherFoundation.FCToolStripMenuItem();
			this.Separator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveQuit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdComments = new fecherFoundation.FCButton();
			this.cmdAddAccount = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdSaveQuit = new fecherFoundation.FCButton();
			this.cmdSavePending = new fecherFoundation.FCButton();
			this.cmdPrintLabel1 = new fecherFoundation.FCButton();
			this.cmdPrintCard = new fecherFoundation.FCButton();
			this.cmdDeleteCard = new fecherFoundation.FCButton();
			this.fcPanel1 = new fecherFoundation.FCPanel();
			this.fcPanel2 = new fecherFoundation.FCPanel();
			this.cmdValuationReport = new fecherFoundation.FCButton();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.txtDeedName1 = new fecherFoundation.FCTextBox();
			this.fcLabel2 = new fecherFoundation.FCLabel();
			this.txtDeedName2 = new fecherFoundation.FCTextBox();
			this.cardNumber = new fecherFoundation.FCComboBox();
			this.ToolTip2 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRemoveSecOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditSecOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearchSecOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridLandCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SaleGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRLivingTrust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridExempts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBankruptcy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTaxAcquired)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.BPGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TreeGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.MapLotGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridBldgCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridPropertyCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTranCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framSaleRecord)).BeginInit();
			this.framSaleRecord.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcPanel4)).BeginInit();
			this.fcPanel4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcPanel3)).BeginInit();
			this.fcPanel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdComments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSavePending)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fcPanel1)).BeginInit();
			this.fcPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcPanel2)).BeginInit();
			this.fcPanel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdValuationReport)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveQuit);
			this.BottomPanel.Controls.Add(this.cmdSavePending);
			this.BottomPanel.Location = new System.Drawing.Point(0, 1382);
			this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.framSaleRecord);
			this.ClientArea.Controls.Add(this.cardNumber);
			this.ClientArea.Controls.Add(this.fcLabel1);
			this.ClientArea.Controls.Add(this.txtDeedName1);
			this.ClientArea.Controls.Add(this.fcLabel2);
			this.ClientArea.Controls.Add(this.txtDeedName2);
			this.ClientArea.Controls.Add(this.lblOwner1);
			this.ClientArea.Controls.Add(this.lbl2ndOwner);
			this.ClientArea.Controls.Add(this.lblAddress);
			this.ClientArea.Controls.Add(this.Label18);
			this.ClientArea.Controls.Add(this.gridLandCode);
			this.ClientArea.Controls.Add(this.SaleGrid);
			this.ClientArea.Controls.Add(this.txtDocumentLocationName);
			this.ClientArea.Controls.Add(this.txtMRRLEXEMPTION);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.txtMRRLLANDVAL);
			this.ClientArea.Controls.Add(this.txtMRRLBLDGVAL);
			this.ClientArea.Controls.Add(this.GridExempts);
			this.ClientArea.Controls.Add(this.txtMRRSMAPLOT);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.BPGrid);
			this.ClientArea.Controls.Add(this.TreeGrid);
			this.ClientArea.Controls.Add(this.MapLotGrid);
			this.ClientArea.Controls.Add(this.gridBldgCode);
			this.ClientArea.Controls.Add(this.GridDeletedPhones);
			this.ClientArea.Controls.Add(this.gridPropertyCode);
			this.ClientArea.Controls.Add(this.Label22);
			this.ClientArea.Controls.Add(this.Label31);
			this.ClientArea.Controls.Add(this.lblMRHLUPDATE);
			this.ClientArea.Controls.Add(this.lblComment);
			this.ClientArea.Controls.Add(this.lblAlertLabel);
			this.ClientArea.Controls.Add(this.Label35);
			this.ClientArea.Controls.Add(this.lblMRHLCARDACCT1);
			this.ClientArea.Controls.Add(this.lblHighCard);
			this.ClientArea.Controls.Add(this.Label28);
			this.ClientArea.Controls.Add(this.Label11);
			this.ClientArea.Controls.Add(this.Label9);
			this.ClientArea.Controls.Add(this.Label12);
			this.ClientArea.Controls.Add(this.Label13);
			this.ClientArea.Controls.Add(this.Label15);
			this.ClientArea.Controls.Add(this.Label16);
			this.ClientArea.Controls.Add(this.Label17);
			this.ClientArea.Controls.Add(this.lblMRHIUPDCODE);
			this.ClientArea.Controls.Add(this.fcPanel1);
			this.ClientArea.Controls.Add(this.fcPanel2);
			this.ClientArea.Controls.Add(this.gridTranCode);
			this.ClientArea.Controls.Add(this.fcPanel4);
			this.ClientArea.Controls.Add(this.fcPanel3);
			this.ClientArea.Size = new System.Drawing.Size(1078, 606);
			this.ClientArea.Controls.SetChildIndex(this.fcPanel3, 0);
			this.ClientArea.Controls.SetChildIndex(this.fcPanel4, 0);
			this.ClientArea.Controls.SetChildIndex(this.gridTranCode, 0);
			this.ClientArea.Controls.SetChildIndex(this.fcPanel2, 0);
			this.ClientArea.Controls.SetChildIndex(this.fcPanel1, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblMRHIUPDCODE, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label17, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label16, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label15, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label13, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label12, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label9, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label11, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label28, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblHighCard, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblMRHLCARDACCT1, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label35, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblAlertLabel, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblComment, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblMRHLUPDATE, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label31, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label22, 0);
			this.ClientArea.Controls.SetChildIndex(this.gridPropertyCode, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridDeletedPhones, 0);
			this.ClientArea.Controls.SetChildIndex(this.gridBldgCode, 0);
			this.ClientArea.Controls.SetChildIndex(this.MapLotGrid, 0);
			this.ClientArea.Controls.SetChildIndex(this.TreeGrid, 0);
			this.ClientArea.Controls.SetChildIndex(this.BPGrid, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtMRRSMAPLOT, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridExempts, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtMRRLBLDGVAL, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtMRRLLANDVAL, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtMRRLEXEMPTION, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtDocumentLocationName, 0);
			this.ClientArea.Controls.SetChildIndex(this.SaleGrid, 0);
			this.ClientArea.Controls.SetChildIndex(this.gridLandCode, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label18, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblAddress, 0);
			this.ClientArea.Controls.SetChildIndex(this.lbl2ndOwner, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblOwner1, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtDeedName2, 0);
			this.ClientArea.Controls.SetChildIndex(this.fcLabel2, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtDeedName1, 0);
			this.ClientArea.Controls.SetChildIndex(this.fcLabel1, 0);
			this.ClientArea.Controls.SetChildIndex(this.cardNumber, 0);
			this.ClientArea.Controls.SetChildIndex(this.framSaleRecord, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdComments);
			this.TopPanel.Controls.Add(this.cmdPrintLabel1);
			this.TopPanel.Controls.Add(this.cmdPrintCard);
			this.TopPanel.Controls.Add(this.cmdValuationReport);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdValuationReport, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintCard, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintLabel1, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdComments, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(205, 28);
			this.HeaderText.Text = "Short Maintenance";
			// 
			// txtMRRLLANDVAL
			// 
			this.txtMRRLLANDVAL.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRLLANDVAL.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRLLANDVAL.Location = new System.Drawing.Point(785, 117);
			this.txtMRRLLANDVAL.MaxLength = 11;
			this.txtMRRLLANDVAL.Name = "txtMRRLLANDVAL";
			this.txtMRRLLANDVAL.Size = new System.Drawing.Size(180, 40);
			this.txtMRRLLANDVAL.TabIndex = 13;
			this.txtMRRLLANDVAL.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMRRLLANDVAL.Enter += new System.EventHandler(this.txtMRRLLANDVAL_Enter);
			this.txtMRRLLANDVAL.Leave += new System.EventHandler(this.txtMRRLLANDVAL_Leave);
			this.txtMRRLLANDVAL.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRLLANDVAL_Validating);
			this.txtMRRLLANDVAL.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRLLANDVAL_KeyDown);
			this.txtMRRLLANDVAL.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMRRLLANDVAL_KeyPress);
			this.txtMRRLLANDVAL.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRLLANDVAL_KeyUp);
			// 
			// txtMRRLBLDGVAL
			// 
			this.txtMRRLBLDGVAL.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRLBLDGVAL.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRLBLDGVAL.Location = new System.Drawing.Point(785, 167);
			this.txtMRRLBLDGVAL.MaxLength = 11;
			this.txtMRRLBLDGVAL.Name = "txtMRRLBLDGVAL";
			this.txtMRRLBLDGVAL.Size = new System.Drawing.Size(180, 40);
			this.txtMRRLBLDGVAL.TabIndex = 14;
			this.txtMRRLBLDGVAL.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMRRLBLDGVAL.Enter += new System.EventHandler(this.txtMRRLBLDGVAL_Enter);
			this.txtMRRLBLDGVAL.Leave += new System.EventHandler(this.txtMRRLBLDGVAL_Leave);
			this.txtMRRLBLDGVAL.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRLBLDGVAL_Validating);
			this.txtMRRLBLDGVAL.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMRRLBLDGVAL_KeyPress);
			// 
			// txtMRRLEXEMPTION
			// 
			this.txtMRRLEXEMPTION.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRLEXEMPTION.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRLEXEMPTION.Location = new System.Drawing.Point(785, 217);
			this.txtMRRLEXEMPTION.MaxLength = 11;
			this.txtMRRLEXEMPTION.Name = "txtMRRLEXEMPTION";
			this.txtMRRLEXEMPTION.Size = new System.Drawing.Size(180, 40);
			this.txtMRRLEXEMPTION.TabIndex = 15;
			this.txtMRRLEXEMPTION.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtMRRLEXEMPTION.Enter += new System.EventHandler(this.txtMRRLEXEMPTION_Enter);
			this.txtMRRLEXEMPTION.Leave += new System.EventHandler(this.txtMRRLEXEMPTION_Leave);
			this.txtMRRLEXEMPTION.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRRLEXEMPTION_Validating);
			this.txtMRRLEXEMPTION.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMRRLEXEMPTION_KeyPress);
			// 
			// cmdRemoveSecOwner
			// 
			this.cmdRemoveSecOwner.AppearanceKey = "actionButton";
			this.cmdRemoveSecOwner.Cursor = Wisej.Web.Cursors.Default;
			this.cmdRemoveSecOwner.ImageSource = "icon-close-menu";
			this.cmdRemoveSecOwner.Location = new System.Drawing.Point(420, 0);
			this.cmdRemoveSecOwner.Name = "cmdRemoveSecOwner";
			this.cmdRemoveSecOwner.Size = new System.Drawing.Size(40, 40);
			this.cmdRemoveSecOwner.TabIndex = 24;
			this.cmdRemoveSecOwner.Click += new System.EventHandler(this.cmdRemoveSecOwner_Click);
			// 
			// cmdRemoveOwner
			// 
			this.cmdRemoveOwner.AppearanceKey = "actionButton";
			this.cmdRemoveOwner.Cursor = Wisej.Web.Cursors.Default;
			this.cmdRemoveOwner.ImageSource = "icon-close-menu";
			this.cmdRemoveOwner.Location = new System.Drawing.Point(420, 0);
			this.cmdRemoveOwner.Name = "cmdRemoveOwner";
			this.cmdRemoveOwner.Size = new System.Drawing.Size(40, 40);
			this.cmdRemoveOwner.TabIndex = 23;
			this.cmdRemoveOwner.Click += new System.EventHandler(this.cmdRemoveOwner_Click);
			// 
			// txt2ndOwnerID
			// 
			this.txt2ndOwnerID.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this.txt2ndOwnerID.Cursor = Wisej.Web.Cursors.Default;
			this.txt2ndOwnerID.Location = new System.Drawing.Point(198, 0);
			this.txt2ndOwnerID.Name = "txt2ndOwnerID";
			this.txt2ndOwnerID.Size = new System.Drawing.Size(116, 40);
			this.txt2ndOwnerID.TabIndex = 3;
			this.txt2ndOwnerID.Tag = "address";
			this.txt2ndOwnerID.Text = "0";
			this.txt2ndOwnerID.Validating += new System.ComponentModel.CancelEventHandler(this.txt2ndOwnerID_Validating);
			// 
			// cmdEditSecOwner
			// 
			this.cmdEditSecOwner.AppearanceKey = "actionButton";
			this.cmdEditSecOwner.Cursor = Wisej.Web.Cursors.Default;
			this.cmdEditSecOwner.ImageSource = "icon - edit";
			this.cmdEditSecOwner.Location = new System.Drawing.Point(375, 0);
			this.cmdEditSecOwner.Name = "cmdEditSecOwner";
			this.cmdEditSecOwner.Size = new System.Drawing.Size(40, 40);
			this.cmdEditSecOwner.TabIndex = 2;
			this.cmdEditSecOwner.Click += new System.EventHandler(this.cmdEditSecOwner_Click);
			// 
			// cmdSearchSecOwner
			// 
			this.cmdSearchSecOwner.AppearanceKey = "actionButton";
			this.cmdSearchSecOwner.Cursor = Wisej.Web.Cursors.Default;
			this.cmdSearchSecOwner.ImageSource = "icon - search";
			this.cmdSearchSecOwner.Location = new System.Drawing.Point(331, 0);
			this.cmdSearchSecOwner.Name = "cmdSearchSecOwner";
			this.cmdSearchSecOwner.Size = new System.Drawing.Size(40, 40);
			this.cmdSearchSecOwner.TabIndex = 1;
			this.cmdSearchSecOwner.Click += new System.EventHandler(this.cmdSearchSecOwner_Click);
			// 
			// txtOwnerID
			// 
			this.txtOwnerID.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this.txtOwnerID.Cursor = Wisej.Web.Cursors.Default;
			this.txtOwnerID.Location = new System.Drawing.Point(198, 0);
			this.txtOwnerID.Name = "txtOwnerID";
			this.txtOwnerID.Size = new System.Drawing.Size(117, 40);
			this.txtOwnerID.TabIndex = 3;
			this.txtOwnerID.Tag = "address";
			this.txtOwnerID.Text = "0";
			this.txtOwnerID.Validating += new System.ComponentModel.CancelEventHandler(this.txtOwnerID_Validating);
			// 
			// cmdEditOwner
			// 
			this.cmdEditOwner.AppearanceKey = "actionButton";
			this.cmdEditOwner.Cursor = Wisej.Web.Cursors.Default;
			this.cmdEditOwner.ImageSource = "icon - edit";
			this.cmdEditOwner.Location = new System.Drawing.Point(375, 0);
			this.cmdEditOwner.Name = "cmdEditOwner";
			this.cmdEditOwner.Size = new System.Drawing.Size(40, 40);
			this.cmdEditOwner.TabIndex = 2;
			this.cmdEditOwner.Click += new System.EventHandler(this.cmdEditOwner_Click);
			// 
			// cmdSearchOwner
			// 
			this.cmdSearchOwner.Anchor = Wisej.Web.AnchorStyles.Top;
			this.cmdSearchOwner.AppearanceKey = "actionButton";
			this.cmdSearchOwner.Cursor = Wisej.Web.Cursors.Default;
			this.cmdSearchOwner.ImageSource = "icon - search";
			this.cmdSearchOwner.Location = new System.Drawing.Point(331, 0);
			this.cmdSearchOwner.Name = "cmdSearchOwner";
			this.cmdSearchOwner.Size = new System.Drawing.Size(40, 40);
			this.cmdSearchOwner.TabIndex = 1;
			this.cmdSearchOwner.Click += new System.EventHandler(this.cmdSearchOwner_Click);
			// 
			// gridLandCode
			// 
			this.gridLandCode.Cols = 1;
			this.gridLandCode.ColumnHeadersVisible = false;
			this.gridLandCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridLandCode.ExtendLastCol = true;
			this.gridLandCode.FixedCols = 0;
			this.gridLandCode.FixedRows = 0;
			this.gridLandCode.Location = new System.Drawing.Point(813, 481);
			this.gridLandCode.Name = "gridLandCode";
			this.gridLandCode.ReadOnly = false;
			this.gridLandCode.RowHeadersVisible = false;
			this.gridLandCode.Rows = 1;
			this.gridLandCode.Size = new System.Drawing.Size(327, 42);
			this.gridLandCode.TabIndex = 18;
			this.gridLandCode.Tag = "land";
			// 
			// SaleGrid
			// 
			this.SaleGrid.Cols = 6;
			this.SaleGrid.ColumnHeadersHeight = 60;
			this.SaleGrid.ExtendLastCol = true;
			this.SaleGrid.FixedCols = 0;
			this.SaleGrid.FixedRows = 2;
			this.SaleGrid.Location = new System.Drawing.Point(30, 1054);
			this.SaleGrid.Name = "SaleGrid";
			this.SaleGrid.RowHeadersVisible = false;
			this.SaleGrid.Rows = 3;
			this.SaleGrid.Size = new System.Drawing.Size(1110, 102);
			this.SaleGrid.TabIndex = 32;
			this.SaleGrid.Tag = "sale";
			this.SaleGrid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.SaleGrid_BeforeEdit);
			this.SaleGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.SaleGrid_ValidateEdit);
			this.SaleGrid.Leave += new System.EventHandler(this.SaleGrid_Leave);
			// 
			// txtMRDIDATEINSPECTED
			// 
			this.txtMRDIDATEINSPECTED.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRDIDATEINSPECTED.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRDIDATEINSPECTED.Location = new System.Drawing.Point(197, 450);
			this.txtMRDIDATEINSPECTED.MaxLength = 8;
			this.txtMRDIDATEINSPECTED.Name = "txtMRDIDATEINSPECTED";
			this.txtMRDIDATEINSPECTED.Size = new System.Drawing.Size(174, 40);
			this.txtMRDIDATEINSPECTED.TabIndex = 11;
			this.txtMRDIDATEINSPECTED.Tag = "land";
			this.txtMRDIDATEINSPECTED.Enter += new System.EventHandler(this.txtMRDIDATEINSPECTED_Enter);
			this.txtMRDIDATEINSPECTED.Leave += new System.EventHandler(this.txtMRDIDATEINSPECTED_Leave);
			this.txtMRDIDATEINSPECTED.MouseUp += new Wisej.Web.MouseEventHandler(this.txtMRDIDATEINSPECTED_MouseUp);
			this.txtMRDIDATEINSPECTED.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRDIDATEINSPECTED_KeyUp);
			// 
			// txtMRDIINFORMATION
			// 
			this.txtMRDIINFORMATION.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRDIINFORMATION.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRDIINFORMATION.Location = new System.Drawing.Point(197, 400);
			this.txtMRDIINFORMATION.MaxLength = 1;
			this.txtMRDIINFORMATION.Name = "txtMRDIINFORMATION";
			this.txtMRDIINFORMATION.Size = new System.Drawing.Size(40, 40);
			this.txtMRDIINFORMATION.TabIndex = 10;
			this.txtMRDIINFORMATION.Tag = "land";
			this.txtMRDIINFORMATION.Enter += new System.EventHandler(this.txtMRDIINFORMATION_Enter);
			this.txtMRDIINFORMATION.Leave += new System.EventHandler(this.txtMRDIINFORMATION_Leave);
			this.txtMRDIINFORMATION.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRDIINFORMATION_KeyUp);
			// 
			// Label42
			// 
			this.Label42.AutoSize = true;
			this.Label42.Location = new System.Drawing.Point(0, 414);
			this.Label42.Name = "Label42";
			this.Label42.Size = new System.Drawing.Size(94, 15);
			this.Label42.TabIndex = 12;
			this.Label42.Text = "INFORMATION";
			// 
			// Label41
			// 
			this.Label41.AutoSize = true;
			this.Label41.Location = new System.Drawing.Point(0, 364);
			this.Label41.Name = "Label41";
			this.Label41.Size = new System.Drawing.Size(75, 15);
			this.Label41.TabIndex = 10;
			this.Label41.Text = "ENTRANCE";
			// 
			// txtMRDIENTRANCECODE
			// 
			this.txtMRDIENTRANCECODE.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRDIENTRANCECODE.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRDIENTRANCECODE.Location = new System.Drawing.Point(197, 350);
			this.txtMRDIENTRANCECODE.MaxLength = 1;
			this.txtMRDIENTRANCECODE.Name = "txtMRDIENTRANCECODE";
			this.txtMRDIENTRANCECODE.Size = new System.Drawing.Size(40, 40);
			this.txtMRDIENTRANCECODE.TabIndex = 9;
			this.txtMRDIENTRANCECODE.Tag = "land";
			this.txtMRDIENTRANCECODE.Enter += new System.EventHandler(this.txtMRDIENTRANCECODE_Enter);
			this.txtMRDIENTRANCECODE.Leave += new System.EventHandler(this.txtMRDIENTRANCECODE_Leave);
			this.txtMRDIENTRANCECODE.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRDIENTRANCECODE_KeyUp);
			// 
			// Label39
			// 
			this.Label39.AutoSize = true;
			this.Label39.Location = new System.Drawing.Point(0, 464);
			this.Label39.Name = "Label39";
			this.Label39.Size = new System.Drawing.Size(119, 15);
			this.Label39.TabIndex = 14;
			this.Label39.Text = "LAST INSPECTION";
			// 
			// txtMRRSLOCAPT
			// 
			this.txtMRRSLOCAPT.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSLOCAPT.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSLOCAPT.Location = new System.Drawing.Point(278, 50);
			this.txtMRRSLOCAPT.MaxLength = 1;
			this.txtMRRSLOCAPT.Name = "txtMRRSLOCAPT";
			this.txtMRRSLOCAPT.Size = new System.Drawing.Size(40, 40);
			this.txtMRRSLOCAPT.TabIndex = 2;
			this.txtMRRSLOCAPT.TabStop = false;
			this.txtMRRSLOCAPT.Tag = "address";
			this.txtMRRSLOCAPT.Enter += new System.EventHandler(this.txtMRRSLOCAPT_Enter);
			this.txtMRRSLOCAPT.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSLOCAPT_KeyUp);
			// 
			// txtMRPIACRES
			// 
			this.txtMRPIACRES.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRPIACRES.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRPIACRES.Location = new System.Drawing.Point(197, 300);
			this.txtMRPIACRES.Name = "txtMRPIACRES";
			this.txtMRPIACRES.Size = new System.Drawing.Size(174, 40);
			this.txtMRPIACRES.TabIndex = 8;
			this.txtMRPIACRES.Tag = "land";
			this.txtMRPIACRES.Enter += new System.EventHandler(this.txtMRPIACRES_Enter);
			this.txtMRPIACRES.Leave += new System.EventHandler(this.txtMRPIACRES_Leave);
			this.txtMRPIACRES.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRPIACRES_Validating);
			this.txtMRPIACRES.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRPIACRES_KeyUp);
			// 
			// Open1Label
			// 
			this.Open1Label.Location = new System.Drawing.Point(0, 214);
			this.Open1Label.Name = "Open1Label";
			this.Open1Label.Size = new System.Drawing.Size(173, 16);
			this.Open1Label.TabIndex = 6;
			// 
			// Label5
			// 
			this.Label5.AutoSize = true;
			this.Label5.Location = new System.Drawing.Point(0, 314);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(94, 15);
			this.Label5.TabIndex = 9;
			this.Label5.Text = "TOTAL ACRES";
			// 
			// txtMRPIOPEN1
			// 
			this.txtMRPIOPEN1.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRPIOPEN1.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRPIOPEN1.Location = new System.Drawing.Point(197, 200);
			this.txtMRPIOPEN1.MaxLength = 4;
			this.txtMRPIOPEN1.Name = "txtMRPIOPEN1";
			this.txtMRPIOPEN1.Size = new System.Drawing.Size(76, 40);
			this.txtMRPIOPEN1.TabIndex = 6;
			this.txtMRPIOPEN1.Tag = "land";
			this.txtMRPIOPEN1.Enter += new System.EventHandler(this.txtMRPIOPEN1_Enter);
			this.txtMRPIOPEN1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRPIOPEN1_KeyUp);
			// 
			// txtMRPIOPEN2
			// 
			this.txtMRPIOPEN2.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRPIOPEN2.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRPIOPEN2.Location = new System.Drawing.Point(197, 250);
			this.txtMRPIOPEN2.MaxLength = 4;
			this.txtMRPIOPEN2.Name = "txtMRPIOPEN2";
			this.txtMRPIOPEN2.Size = new System.Drawing.Size(76, 40);
			this.txtMRPIOPEN2.TabIndex = 7;
			this.txtMRPIOPEN2.Tag = "land";
			this.txtMRPIOPEN2.Enter += new System.EventHandler(this.txtMRPIOPEN2_Enter);
			this.txtMRPIOPEN2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRPIOPEN2_KeyUp);
			// 
			// Open2Label
			// 
			this.Open2Label.Location = new System.Drawing.Point(0, 264);
			this.Open2Label.Name = "Open2Label";
			this.Open2Label.Size = new System.Drawing.Size(173, 16);
			this.Open2Label.TabIndex = 7;
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(0, 164);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(119, 16);
			this.Label10.TabIndex = 5;
			this.Label10.Text = "REFERENCE 2";
			// 
			// Label8_0
			// 
			this.Label8_0.Location = new System.Drawing.Point(0, 114);
			this.Label8_0.Name = "Label8_0";
			this.Label8_0.Size = new System.Drawing.Size(102, 15);
			this.Label8_0.TabIndex = 4;
			this.Label8_0.Text = "REFERENCE 1";
			// 
			// txtPreviousOwner
			// 
			this.txtPreviousOwner.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtPreviousOwner.Cursor = Wisej.Web.Cursors.Default;
			this.txtPreviousOwner.Location = new System.Drawing.Point(197, 0);
			this.txtPreviousOwner.Name = "txtPreviousOwner";
			this.txtPreviousOwner.Size = new System.Drawing.Size(423, 40);
			this.txtPreviousOwner.TabIndex = 1;
			this.txtPreviousOwner.Tag = "address";
			this.txtPreviousOwner.Enter += new System.EventHandler(this.txtPreviousOwner_Enter);
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(0, 64);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(80, 15);
			this.Label7.TabIndex = 2;
			this.Label7.Text = "LOCATION";
			// 
			// txtMRRSREF2
			// 
			this.txtMRRSREF2.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSREF2.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSREF2.Location = new System.Drawing.Point(197, 150);
			this.txtMRRSREF2.MaxLength = 44;
			this.txtMRRSREF2.Name = "txtMRRSREF2";
			this.txtMRRSREF2.Size = new System.Drawing.Size(423, 40);
			this.txtMRRSREF2.TabIndex = 5;
			this.txtMRRSREF2.Tag = "address";
			this.txtMRRSREF2.Enter += new System.EventHandler(this.txtMRRSREF2_Enter);
			this.txtMRRSREF2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSREF2_KeyUp);
			// 
			// txtMRRSREF1
			// 
			this.txtMRRSREF1.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSREF1.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSREF1.Location = new System.Drawing.Point(197, 100);
			this.txtMRRSREF1.MaxLength = 44;
			this.txtMRRSREF1.Name = "txtMRRSREF1";
			this.txtMRRSREF1.Size = new System.Drawing.Size(423, 40);
			this.txtMRRSREF1.TabIndex = 4;
			this.txtMRRSREF1.Tag = "address";
			this.txtMRRSREF1.Enter += new System.EventHandler(this.txtMRRSREF1_Enter);
			this.txtMRRSREF1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSREF1_KeyUp);
			// 
			// txtMRRSLOCNUMALPH
			// 
			this.txtMRRSLOCNUMALPH.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSLOCNUMALPH.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSLOCNUMALPH.Location = new System.Drawing.Point(197, 50);
			this.txtMRRSLOCNUMALPH.MaxLength = 5;
			this.txtMRRSLOCNUMALPH.Name = "txtMRRSLOCNUMALPH";
			this.txtMRRSLOCNUMALPH.Size = new System.Drawing.Size(76, 40);
			this.txtMRRSLOCNUMALPH.TabIndex = 2;
			this.txtMRRSLOCNUMALPH.Tag = "address";
			this.txtMRRSLOCNUMALPH.Enter += new System.EventHandler(this.txtMRRSLOCNUMALPH_Enter);
			this.txtMRRSLOCNUMALPH.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSLOCNUMALPH_KeyUp);
			// 
			// txtMRRSLOCSTREET
			// 
			this.txtMRRSLOCSTREET.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSLOCSTREET.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSLOCSTREET.Location = new System.Drawing.Point(323, 50);
			this.txtMRRSLOCSTREET.MaxLength = 26;
			this.txtMRRSLOCSTREET.Name = "txtMRRSLOCSTREET";
			this.txtMRRSLOCSTREET.Size = new System.Drawing.Size(297, 40);
			this.txtMRRSLOCSTREET.TabIndex = 3;
			this.txtMRRSLOCSTREET.Tag = "address";
			this.txtMRRSLOCSTREET.Enter += new System.EventHandler(this.txtMRRSLOCSTREET_Enter);
			this.txtMRRSLOCSTREET.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSLOCSTREET_KeyUp);
			// 
			// chkRLivingTrust
			// 
			this.chkRLivingTrust.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkRLivingTrust.Location = new System.Drawing.Point(283, 0);
			this.chkRLivingTrust.Name = "chkRLivingTrust";
			this.chkRLivingTrust.Size = new System.Drawing.Size(164, 22);
			this.chkRLivingTrust.TabIndex = 2;
			this.chkRLivingTrust.Text = "Revocable Living Trust";
			this.ToolTip1.SetToolTip(this.chkRLivingTrust, "This information is used for the Veteran Exemptions worksheet for the MVR");
			// 
			// GridExempts
			// 
			this.GridExempts.Cols = 3;
			this.GridExempts.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridExempts.ExtendLastCol = true;
			this.GridExempts.FixedCols = 0;
			this.GridExempts.Location = new System.Drawing.Point(684, 267);
			this.GridExempts.Name = "GridExempts";
			this.GridExempts.ReadOnly = false;
			this.GridExempts.RowHeadersVisible = false;
			this.GridExempts.Rows = 4;
			this.GridExempts.Size = new System.Drawing.Size(456, 152);
			this.GridExempts.StandardTab = false;
			this.GridExempts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridExempts.TabIndex = 16;
			this.GridExempts.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridExempts_CellFormatting);
			this.GridExempts.CurrentCellChanged += new System.EventHandler(this.GridExempts_RowColChange);
			// 
			// chkBankruptcy
			// 
			this.chkBankruptcy.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkBankruptcy.Location = new System.Drawing.Point(150, 0);
			this.chkBankruptcy.Name = "chkBankruptcy";
			this.chkBankruptcy.Size = new System.Drawing.Size(97, 22);
			this.chkBankruptcy.TabIndex = 1;
			this.chkBankruptcy.Text = "Bankruptcy";
			this.chkBankruptcy.CheckedChanged += new System.EventHandler(this.chkBankruptcy_CheckedChanged);
			// 
			// chkTaxAcquired
			// 
			this.chkTaxAcquired.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkTaxAcquired.Location = new System.Drawing.Point(0, 1);
			this.chkTaxAcquired.Name = "chkTaxAcquired";
			this.chkTaxAcquired.Size = new System.Drawing.Size(108, 22);
			this.chkTaxAcquired.TabIndex = 3;
			this.chkTaxAcquired.Text = "Tax Acquired";
			this.chkTaxAcquired.CheckedChanged += new System.EventHandler(this.chkTaxAcquired_CheckedChanged);
			// 
			// txtMRRSMAPLOT
			// 
			this.txtMRRSMAPLOT.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSMAPLOT.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSMAPLOT.Location = new System.Drawing.Point(670, 30);
			this.txtMRRSMAPLOT.MaxLength = 17;
			this.txtMRRSMAPLOT.Name = "txtMRRSMAPLOT";
			this.txtMRRSMAPLOT.Size = new System.Drawing.Size(295, 38);
			this.txtMRRSMAPLOT.TabIndex = 1;
			this.txtMRRSMAPLOT.Tag = "address";
			this.txtMRRSMAPLOT.Enter += new System.EventHandler(this.txtMRRSMAPLOT_Enter);
			this.txtMRRSMAPLOT.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSMAPLOT_KeyUp);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.lblTotalAcreage);
			this.Frame1.Controls.Add(this.Label19);
			this.Frame1.Controls.Add(this.Label23);
			this.Frame1.Controls.Add(this.Label21);
			this.Frame1.Controls.Add(this.Label20);
			this.Frame1.Controls.Add(this.Label24);
			this.Frame1.Controls.Add(this.lblMRRLLANDVAL);
			this.Frame1.Controls.Add(this.lblMRRLBLDGVAL);
			this.Frame1.Controls.Add(this.lblMRRLEXEMPTION);
			this.Frame1.Controls.Add(this.lblNetAssessment);
			this.Frame1.Controls.Add(this.lblTaxAmount);
			this.Frame1.Controls.Add(this.lblTaxRate);
			this.Frame1.Controls.Add(this.TaxAmountLabel);
			this.Frame1.Controls.Add(this.TaxRateLabel);
			this.Frame1.Location = new System.Drawing.Point(738, 1166);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(402, 216);
			this.Frame1.TabIndex = 34;
			this.Frame1.Text = "Total Account Values";
			// 
			// lblTotalAcreage
			// 
			this.lblTotalAcreage.Location = new System.Drawing.Point(134, 180);
			this.lblTotalAcreage.Name = "lblTotalAcreage";
			this.lblTotalAcreage.Size = new System.Drawing.Size(90, 16);
			this.lblTotalAcreage.TabIndex = 0;
			this.lblTotalAcreage.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Label19
			// 
			this.Label19.Location = new System.Drawing.Point(20, 180);
			this.Label19.Name = "Label19";
			this.Label19.Size = new System.Drawing.Size(73, 16);
			this.Label19.TabIndex = 1;
			this.Label19.Text = "ACREAGE";
			// 
			// Label23
			// 
			this.Label23.Location = new System.Drawing.Point(20, 106);
			this.Label23.Name = "Label23";
			this.Label23.Size = new System.Drawing.Size(86, 16);
			this.Label23.TabIndex = 2;
			this.Label23.Text = "EXEMPTION";
			// 
			// Label21
			// 
			this.Label21.Location = new System.Drawing.Point(20, 70);
			this.Label21.Name = "Label21";
			this.Label21.Size = new System.Drawing.Size(70, 16);
			this.Label21.TabIndex = 3;
			this.Label21.Text = "BUILDING";
			// 
			// Label20
			// 
			this.Label20.Location = new System.Drawing.Point(20, 34);
			this.Label20.Name = "Label20";
			this.Label20.Size = new System.Drawing.Size(44, 16);
			this.Label20.TabIndex = 4;
			this.Label20.Text = "LAND";
			// 
			// Label24
			// 
			this.Label24.Location = new System.Drawing.Point(20, 142);
			this.Label24.Name = "Label24";
			this.Label24.Size = new System.Drawing.Size(68, 16);
			this.Label24.TabIndex = 5;
			this.Label24.Text = "TAXABLE";
			// 
			// lblMRRLLANDVAL
			// 
			this.lblMRRLLANDVAL.Location = new System.Drawing.Point(134, 34);
			this.lblMRRLLANDVAL.Name = "lblMRRLLANDVAL";
			this.lblMRRLLANDVAL.Size = new System.Drawing.Size(90, 16);
			this.lblMRRLLANDVAL.TabIndex = 6;
			this.lblMRRLLANDVAL.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblMRRLBLDGVAL
			// 
			this.lblMRRLBLDGVAL.Location = new System.Drawing.Point(134, 70);
			this.lblMRRLBLDGVAL.Name = "lblMRRLBLDGVAL";
			this.lblMRRLBLDGVAL.Size = new System.Drawing.Size(90, 16);
			this.lblMRRLBLDGVAL.TabIndex = 7;
			this.lblMRRLBLDGVAL.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblMRRLEXEMPTION
			// 
			this.lblMRRLEXEMPTION.Location = new System.Drawing.Point(134, 106);
			this.lblMRRLEXEMPTION.Name = "lblMRRLEXEMPTION";
			this.lblMRRLEXEMPTION.Size = new System.Drawing.Size(90, 16);
			this.lblMRRLEXEMPTION.TabIndex = 8;
			this.lblMRRLEXEMPTION.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblNetAssessment
			// 
			this.lblNetAssessment.Location = new System.Drawing.Point(134, 144);
			this.lblNetAssessment.Name = "lblNetAssessment";
			this.lblNetAssessment.Size = new System.Drawing.Size(90, 16);
			this.lblNetAssessment.TabIndex = 9;
			this.lblNetAssessment.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblTaxAmount
			// 
			this.lblTaxAmount.Location = new System.Drawing.Point(284, 144);
			this.lblTaxAmount.Name = "lblTaxAmount";
			this.lblTaxAmount.Size = new System.Drawing.Size(74, 16);
			this.lblTaxAmount.TabIndex = 10;
			this.lblTaxAmount.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblTaxRate
			// 
			this.lblTaxRate.Location = new System.Drawing.Point(284, 70);
			this.lblTaxRate.Name = "lblTaxRate";
			this.lblTaxRate.Size = new System.Drawing.Size(74, 16);
			this.lblTaxRate.TabIndex = 11;
			this.lblTaxRate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// TaxAmountLabel
			// 
			this.TaxAmountLabel.Location = new System.Drawing.Point(284, 106);
			this.TaxAmountLabel.Name = "TaxAmountLabel";
			this.TaxAmountLabel.Size = new System.Drawing.Size(67, 16);
			this.TaxAmountLabel.TabIndex = 12;
			this.TaxAmountLabel.Text = "EST. TAX";
			this.TaxAmountLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// TaxRateLabel
			// 
			this.TaxRateLabel.AutoSize = true;
			this.TaxRateLabel.Location = new System.Drawing.Point(284, 34);
			this.TaxRateLabel.Name = "TaxRateLabel";
			this.TaxRateLabel.Size = new System.Drawing.Size(67, 15);
			this.TaxRateLabel.TabIndex = 13;
			this.TaxRateLabel.Text = "TAX RATE";
			this.TaxRateLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// txtDocumentLocationName
			// 
			this.txtDocumentLocationName.BackColor = System.Drawing.SystemColors.Window;
			this.txtDocumentLocationName.Cursor = Wisej.Web.Cursors.Default;
			this.txtDocumentLocationName.Location = new System.Drawing.Point(740, 1295);
			this.txtDocumentLocationName.Name = "txtDocumentLocationName";
			this.txtDocumentLocationName.Size = new System.Drawing.Size(278, 40);
			this.txtDocumentLocationName.TabIndex = 26;
			this.txtDocumentLocationName.Visible = false;
			// 
			// CommonDialog1
			// 
			this.CommonDialog1.Name = "CommonDialog1";
			this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
			// 
			// BPGrid
			// 
			this.BPGrid.Cols = 4;
			this.BPGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.BPGrid.ExtendLastCol = true;
			this.BPGrid.FixedCols = 0;
			this.BPGrid.Location = new System.Drawing.Point(684, 637);
			this.BPGrid.Name = "BPGrid";
			this.BPGrid.ReadOnly = false;
			this.BPGrid.RowHeadersVisible = false;
			this.BPGrid.Rows = 3;
			this.BPGrid.Size = new System.Drawing.Size(456, 150);
			this.BPGrid.TabIndex = 28;
			this.ToolTip1.SetToolTip(this.BPGrid, "Mark book page entries that are current by placing a check mark in the field to t" +
        "he left");
			this.BPGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.BPGrid_AfterEdit);
			this.BPGrid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.BPGrid_BeforeEdit);
			this.BPGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.BPGrid_ValidateEdit);
			this.BPGrid.Leave += new System.EventHandler(this.BPGrid_Leave);
			this.BPGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.BPGrid_KeyDownEvent);
			// 
			// TreeGrid
			// 
			this.TreeGrid.Cols = 5;
			this.TreeGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.TreeGrid.FixedCols = 0;
			this.TreeGrid.Location = new System.Drawing.Point(30, 1166);
			this.TreeGrid.Name = "TreeGrid";
			this.TreeGrid.ReadOnly = false;
			this.TreeGrid.RowHeadersVisible = false;
			this.TreeGrid.Rows = 3;
			this.TreeGrid.Size = new System.Drawing.Size(700, 112);
			this.TreeGrid.TabIndex = 29;
			this.TreeGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.TreeGrid_AfterEdit);
			this.TreeGrid.CurrentCellChanged += new System.EventHandler(this.TreeGrid_RowColChange);
			// 
			// MapLotGrid
			// 
			this.MapLotGrid.Cols = 3;
			this.MapLotGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.MapLotGrid.ExtendLastCol = true;
			this.MapLotGrid.FixedCols = 0;
			this.MapLotGrid.Location = new System.Drawing.Point(684, 797);
			this.MapLotGrid.Name = "MapLotGrid";
			this.MapLotGrid.ReadOnly = false;
			this.MapLotGrid.RowHeadersVisible = false;
			this.MapLotGrid.Rows = 3;
			this.MapLotGrid.Size = new System.Drawing.Size(456, 109);
			this.MapLotGrid.TabIndex = 30;
			this.ToolTip1.SetToolTip(this.MapLotGrid, "Used to keep a history of map-lot numbers");
			this.MapLotGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.MapLotGrid_AfterEdit);
			this.MapLotGrid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.MapLotGrid_BeforeEdit);
			this.MapLotGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.MapLotGrid_ValidateEdit);
			this.MapLotGrid.Leave += new System.EventHandler(this.MapLotGrid_Leave);
			this.MapLotGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.MapLotGrid_KeyDownEvent);
			// 
			// gridBldgCode
			// 
			this.gridBldgCode.Cols = 1;
			this.gridBldgCode.ColumnHeadersVisible = false;
			this.gridBldgCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridBldgCode.ExtendLastCol = true;
			this.gridBldgCode.FixedCols = 0;
			this.gridBldgCode.FixedRows = 0;
			this.gridBldgCode.Location = new System.Drawing.Point(813, 533);
			this.gridBldgCode.Name = "gridBldgCode";
			this.gridBldgCode.ReadOnly = false;
			this.gridBldgCode.RowHeadersVisible = false;
			this.gridBldgCode.Rows = 1;
			this.gridBldgCode.Size = new System.Drawing.Size(327, 42);
			this.gridBldgCode.TabIndex = 19;
			this.gridBldgCode.Tag = "land";
			// 
			// GridDeletedPhones
			// 
			this.GridDeletedPhones.ColumnHeadersVisible = false;
			this.GridDeletedPhones.FixedRows = 0;
			this.GridDeletedPhones.Location = new System.Drawing.Point(30, 30);
			this.GridDeletedPhones.Name = "GridDeletedPhones";
			this.GridDeletedPhones.Rows = 0;
			this.GridDeletedPhones.Size = new System.Drawing.Size(43, 45);
			this.GridDeletedPhones.TabIndex = 31;
			this.GridDeletedPhones.Visible = false;
			// 
			// gridPropertyCode
			// 
			this.gridPropertyCode.Cols = 1;
			this.gridPropertyCode.ColumnHeadersVisible = false;
			this.gridPropertyCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridPropertyCode.ExtendLastCol = true;
			this.gridPropertyCode.FixedCols = 0;
			this.gridPropertyCode.FixedRows = 0;
			this.gridPropertyCode.Location = new System.Drawing.Point(813, 585);
			this.gridPropertyCode.Name = "gridPropertyCode";
			this.gridPropertyCode.ReadOnly = false;
			this.gridPropertyCode.RowHeadersVisible = false;
			this.gridPropertyCode.Rows = 1;
			this.gridPropertyCode.Size = new System.Drawing.Size(327, 42);
			this.gridPropertyCode.TabIndex = 20;
			this.gridPropertyCode.Tag = "land";
			this.gridPropertyCode.ComboDropDown += new System.EventHandler(this.gridPropertyCode_ComboDropDown);
			this.gridPropertyCode.ComboCloseUp += new System.EventHandler(this.gridPropertyCode_ComboCloseUp);
			// 
			// gridTranCode
			// 
			this.gridTranCode.Cols = 1;
			this.gridTranCode.ColumnHeadersVisible = false;
			this.gridTranCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridTranCode.ExtendLastCol = true;
			this.gridTranCode.FixedCols = 0;
			this.gridTranCode.FixedRows = 0;
			this.gridTranCode.Location = new System.Drawing.Point(813, 429);
			this.gridTranCode.Name = "gridTranCode";
			this.gridTranCode.ReadOnly = false;
			this.gridTranCode.RowHeadersVisible = false;
			this.gridTranCode.Rows = 1;
			this.gridTranCode.Size = new System.Drawing.Size(327, 42);
			this.gridTranCode.TabIndex = 17;
			this.gridTranCode.Tag = "land";
			this.gridTranCode.ComboCloseUp += new System.EventHandler(this.gridTranCode_ComboCloseUp);
			// 
			// framSaleRecord
			// 
			this.framSaleRecord.Controls.Add(this.txtMRRSNAME);
			this.framSaleRecord.Controls.Add(this.txtMRRSSECOWNER);
			this.framSaleRecord.Controls.Add(this.Label6);
			this.framSaleRecord.Controls.Add(this.Label4);
			this.framSaleRecord.Controls.Add(this.txtMRRSZIP4);
			this.framSaleRecord.Controls.Add(this.txtMRRSADDR1);
			this.framSaleRecord.Controls.Add(this.txtMRRSADDR2);
			this.framSaleRecord.Controls.Add(this.txtMRRSADDR3);
			this.framSaleRecord.Controls.Add(this.txtMRRSZIP);
			this.framSaleRecord.Controls.Add(this.txtMRRSSTATE);
			this.framSaleRecord.Controls.Add(this.Label32);
			this.framSaleRecord.Controls.Add(this.Label30);
			this.framSaleRecord.Controls.Add(this.Label29);
			this.framSaleRecord.Controls.Add(this.Label14);
			this.framSaleRecord.Controls.Add(this.Label26);
			this.framSaleRecord.Controls.Add(this.Label25);
			this.framSaleRecord.Location = new System.Drawing.Point(26, 116);
			this.framSaleRecord.Name = "framSaleRecord";
			this.framSaleRecord.Size = new System.Drawing.Size(633, 424);
			this.framSaleRecord.TabIndex = 1001;
			this.framSaleRecord.Visible = false;
			// 
			// txtMRRSNAME
			// 
			this.txtMRRSNAME.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSNAME.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSNAME.Location = new System.Drawing.Point(199, 3);
			this.txtMRRSNAME.MaxLength = 50;
			this.txtMRRSNAME.Name = "txtMRRSNAME";
			this.txtMRRSNAME.Size = new System.Drawing.Size(258, 40);
			this.txtMRRSNAME.TabIndex = 0;
			this.txtMRRSNAME.Tag = "address";
			// 
			// txtMRRSSECOWNER
			// 
			this.txtMRRSSECOWNER.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSSECOWNER.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSSECOWNER.Location = new System.Drawing.Point(199, 51);
			this.txtMRRSSECOWNER.MaxLength = 50;
			this.txtMRRSSECOWNER.Name = "txtMRRSSECOWNER";
			this.txtMRRSSECOWNER.Size = new System.Drawing.Size(258, 40);
			this.txtMRRSSECOWNER.TabIndex = 1;
			this.txtMRRSSECOWNER.Tag = "address";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(4, 68);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(90, 15);
			this.Label6.TabIndex = 2;
			this.Label6.Text = "2ND OWNER";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(3, 19);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(47, 15);
			this.Label4.TabIndex = 3;
			this.Label4.Text = "NAME";
			// 
			// txtMRRSZIP4
			// 
			this.txtMRRSZIP4.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSZIP4.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSZIP4.Location = new System.Drawing.Point(325, 253);
			this.txtMRRSZIP4.MaxLength = 4;
			this.txtMRRSZIP4.Name = "txtMRRSZIP4";
			this.txtMRRSZIP4.Size = new System.Drawing.Size(90, 51);
			this.txtMRRSZIP4.TabIndex = 4;
			this.txtMRRSZIP4.Tag = "address";
			// 
			// txtMRRSADDR1
			// 
			this.txtMRRSADDR1.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSADDR1.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSADDR1.Location = new System.Drawing.Point(199, 103);
			this.txtMRRSADDR1.Name = "txtMRRSADDR1";
			this.txtMRRSADDR1.Size = new System.Drawing.Size(258, 40);
			this.txtMRRSADDR1.TabIndex = 5;
			this.txtMRRSADDR1.Tag = "address";
			// 
			// txtMRRSADDR2
			// 
			this.txtMRRSADDR2.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSADDR2.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSADDR2.Location = new System.Drawing.Point(199, 153);
			this.txtMRRSADDR2.Name = "txtMRRSADDR2";
			this.txtMRRSADDR2.Size = new System.Drawing.Size(258, 40);
			this.txtMRRSADDR2.TabIndex = 6;
			this.txtMRRSADDR2.Tag = "address";
			// 
			// txtMRRSADDR3
			// 
			this.txtMRRSADDR3.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSADDR3.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSADDR3.Location = new System.Drawing.Point(199, 203);
			this.txtMRRSADDR3.MaxLength = 24;
			this.txtMRRSADDR3.Name = "txtMRRSADDR3";
			this.txtMRRSADDR3.Size = new System.Drawing.Size(258, 40);
			this.txtMRRSADDR3.TabIndex = 7;
			this.txtMRRSADDR3.Tag = "address";
			// 
			// txtMRRSZIP
			// 
			this.txtMRRSZIP.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSZIP.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSZIP.Location = new System.Drawing.Point(199, 253);
			this.txtMRRSZIP.MaxLength = 5;
			this.txtMRRSZIP.Name = "txtMRRSZIP";
			this.txtMRRSZIP.Size = new System.Drawing.Size(106, 51);
			this.txtMRRSZIP.TabIndex = 8;
			this.txtMRRSZIP.Tag = "address";
			// 
			// txtMRRSSTATE
			// 
			this.txtMRRSSTATE.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtMRRSSTATE.Cursor = Wisej.Web.Cursors.Default;
			this.txtMRRSSTATE.Location = new System.Drawing.Point(575, 203);
			this.txtMRRSSTATE.MaxLength = 2;
			this.txtMRRSSTATE.Name = "txtMRRSSTATE";
			this.txtMRRSSTATE.Size = new System.Drawing.Size(45, 40);
			this.txtMRRSSTATE.TabIndex = 9;
			this.txtMRRSSTATE.Tag = "address";
			// 
			// Label32
			// 
			this.Label32.Location = new System.Drawing.Point(2, 267);
			this.Label32.Name = "Label32";
			this.Label32.Size = new System.Drawing.Size(28, 15);
			this.Label32.TabIndex = 10;
			this.Label32.Text = "ZIP";
			// 
			// Label30
			// 
			this.Label30.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Label30.Location = new System.Drawing.Point(2, 217);
			this.Label30.Name = "Label30";
			this.Label30.Size = new System.Drawing.Size(38, 15);
			this.Label30.TabIndex = 11;
			this.Label30.Text = "CITY";
			// 
			// Label29
			// 
			this.Label29.Location = new System.Drawing.Point(2, 167);
			this.Label29.Name = "Label29";
			this.Label29.Size = new System.Drawing.Size(84, 15);
			this.Label29.TabIndex = 12;
			this.Label29.Text = "ADDRESS 2";
			// 
			// Label14
			// 
			this.Label14.Location = new System.Drawing.Point(2, 117);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(84, 15);
			this.Label14.TabIndex = 13;
			this.Label14.Text = "ADDRESS 1";
			// 
			// Label26
			// 
			this.Label26.Location = new System.Drawing.Point(487, 217);
			this.Label26.Name = "Label26";
			this.Label26.Size = new System.Drawing.Size(53, 15);
			this.Label26.TabIndex = 14;
			this.Label26.Text = "STATE";
			// 
			// Label25
			// 
			this.Label25.AutoSize = true;
			this.Label25.Location = new System.Drawing.Point(312, 262);
			this.Label25.Name = "Label25";
			this.Label25.Size = new System.Drawing.Size(8, 15);
			this.Label25.TabIndex = 15;
			this.Label25.Text = "-";
			// 
			// lblOwner1
			// 
			this.lblOwner1.Location = new System.Drawing.Point(199, 164);
			this.lblOwner1.Name = "lblOwner1";
			this.lblOwner1.Size = new System.Drawing.Size(258, 16);
			this.lblOwner1.TabIndex = 28;
			// 
			// lbl2ndOwner
			// 
			this.lbl2ndOwner.Location = new System.Drawing.Point(199, 245);
			this.lbl2ndOwner.Name = "lbl2ndOwner";
			this.lbl2ndOwner.Size = new System.Drawing.Size(256, 16);
			this.lbl2ndOwner.TabIndex = 29;
			// 
			// fcPanel4
			// 
			this.fcPanel4.Controls.Add(this.Label43);
			this.fcPanel4.Controls.Add(this.cmdEditSecOwner);
			this.fcPanel4.Controls.Add(this.cmdRemoveSecOwner);
			this.fcPanel4.Controls.Add(this.cmdSearchSecOwner);
			this.fcPanel4.Controls.Add(this.txt2ndOwnerID);
			this.fcPanel4.Location = new System.Drawing.Point(30, 195);
			this.fcPanel4.Name = "fcPanel4";
			this.fcPanel4.Size = new System.Drawing.Size(460, 40);
			this.fcPanel4.TabIndex = 24;
			// 
			// Label43
			// 
			this.Label43.Location = new System.Drawing.Point(0, 14);
			this.Label43.Name = "Label43";
			this.Label43.Size = new System.Drawing.Size(90, 16);
			this.Label43.TabIndex = 0;
			this.Label43.Text = "2ND OWNER";
			// 
			// fcPanel3
			// 
			this.fcPanel3.Controls.Add(this.Label1);
			this.fcPanel3.Controls.Add(this.txtOwnerID);
			this.fcPanel3.Controls.Add(this.cmdSearchOwner);
			this.fcPanel3.Controls.Add(this.cmdEditOwner);
			this.fcPanel3.Controls.Add(this.cmdRemoveOwner);
			this.fcPanel3.Location = new System.Drawing.Point(30, 117);
			this.fcPanel3.Name = "fcPanel3";
			this.fcPanel3.Size = new System.Drawing.Size(460, 40);
			this.fcPanel3.TabIndex = 23;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(0, 14);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(60, 16);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "OWNER";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(26, 404);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(74, 15);
			this.Label2.TabIndex = 33;
			this.Label2.Text = "ADDRESS";
			// 
			// lblAddress
			// 
			this.lblAddress.Location = new System.Drawing.Point(220, 404);
			this.lblAddress.Name = "lblAddress";
			this.lblAddress.Size = new System.Drawing.Size(413, 137);
			this.lblAddress.TabIndex = 30;
			// 
			// Label22
			// 
			this.Label22.AutoSize = true;
			this.Label22.Location = new System.Drawing.Point(684, 599);
			this.Label22.Name = "Label22";
			this.Label22.Size = new System.Drawing.Size(116, 15);
			this.Label22.TabIndex = 4;
			this.Label22.Text = "PROPERTY CODE";
			// 
			// lblMRDIINFORMATION
			// 
			this.lblMRDIINFORMATION.Location = new System.Drawing.Point(273, 414);
			this.lblMRDIINFORMATION.Name = "lblMRDIINFORMATION";
			this.lblMRDIINFORMATION.Size = new System.Drawing.Size(193, 15);
			this.lblMRDIINFORMATION.TabIndex = 13;
			// 
			// lblMRDIENTRANCE
			// 
			this.lblMRDIENTRANCE.Location = new System.Drawing.Point(273, 364);
			this.lblMRDIENTRANCE.Name = "lblMRDIENTRANCE";
			this.lblMRDIENTRANCE.Size = new System.Drawing.Size(185, 15);
			this.lblMRDIENTRANCE.TabIndex = 11;
			// 
			// Label31
			// 
			this.Label31.Location = new System.Drawing.Point(30, 1298);
			this.Label31.Name = "Label31";
			this.Label31.Size = new System.Drawing.Size(104, 16);
			this.Label31.TabIndex = 35;
			this.Label31.Text = "LAST UPDATED";
			// 
			// lblMRHLUPDATE
			// 
			this.lblMRHLUPDATE.Location = new System.Drawing.Point(140, 1298);
			this.lblMRHLUPDATE.Name = "lblMRHLUPDATE";
			this.lblMRHLUPDATE.Size = new System.Drawing.Size(77, 16);
			this.lblMRHLUPDATE.TabIndex = 36;
			// 
			// Label27
			// 
			this.Label27.Location = new System.Drawing.Point(0, 14);
			this.Label27.Name = "Label27";
			this.Label27.Size = new System.Drawing.Size(119, 15);
			this.Label27.TabIndex = 0;
			this.Label27.Text = "PREV OWNER";
			// 
			// lblComment
			// 
			this.lblComment.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
			this.lblComment.Location = new System.Drawing.Point(533, 43);
			this.lblComment.Name = "lblComment";
			this.lblComment.Size = new System.Drawing.Size(27, 16);
			this.lblComment.TabIndex = 37;
			this.lblComment.Text = "C";
			this.lblComment.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblComment.Visible = false;
			// 
			// lblAlertLabel
			// 
			this.lblAlertLabel.Font = new System.Drawing.Font("semibold", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblAlertLabel.Location = new System.Drawing.Point(30, 85);
			this.lblAlertLabel.Name = "lblAlertLabel";
			this.lblAlertLabel.Size = new System.Drawing.Size(150, 23);
			this.lblAlertLabel.TabIndex = 38;
			this.lblAlertLabel.Text = "SALE RECORD";
			this.lblAlertLabel.Visible = false;
			// 
			// Label18
			// 
			this.Label18.Location = new System.Drawing.Point(30, 44);
			this.Label18.Name = "Label18";
			this.Label18.Size = new System.Drawing.Size(90, 15);
			this.Label18.TabIndex = 31;
			this.Label18.Text = "ACCOUNT";
			this.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label35
			// 
			this.Label35.Location = new System.Drawing.Point(280, 44);
			this.Label35.Name = "Label35";
			this.Label35.Size = new System.Drawing.Size(39, 18);
			this.Label35.TabIndex = 39;
			this.Label35.Text = "CARD";
			this.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMRHLCARDACCT1
			// 
			this.lblMRHLCARDACCT1.Location = new System.Drawing.Point(156, 44);
			this.lblMRHLCARDACCT1.Name = "lblMRHLCARDACCT1";
			this.lblMRHLCARDACCT1.Size = new System.Drawing.Size(48, 15);
			this.lblMRHLCARDACCT1.TabIndex = 40;
			// 
			// lblHighCard
			// 
			this.lblHighCard.ForeColor = System.Drawing.SystemColors.ActiveCaption;
			this.lblHighCard.Location = new System.Drawing.Point(514, 44);
			this.lblHighCard.Name = "lblHighCard";
			this.lblHighCard.Size = new System.Drawing.Size(24, 15);
			this.lblHighCard.TabIndex = 41;
			this.lblHighCard.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label28
			// 
			this.Label28.AutoSize = true;
			this.Label28.Location = new System.Drawing.Point(479, 44);
			this.Label28.Name = "Label28";
			this.Label28.Size = new System.Drawing.Size(22, 15);
			this.Label28.TabIndex = 42;
			this.Label28.Text = "OF";
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(684, 131);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(75, 15);
			this.Label11.TabIndex = 43;
			this.Label11.Text = "LAND";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(585, 44);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(65, 18);
			this.Label9.TabIndex = 44;
			this.Label9.Text = "MAP/LOT";
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(684, 181);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(109, 16);
			this.Label12.TabIndex = 45;
			this.Label12.Text = "BUILDING";
			// 
			// Label13
			// 
			this.Label13.Location = new System.Drawing.Point(684, 231);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(109, 16);
			this.Label13.TabIndex = 46;
			this.Label13.Text = "EXEMPTION";
			// 
			// Label15
			// 
			this.Label15.AutoSize = true;
			this.Label15.Location = new System.Drawing.Point(684, 443);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(80, 15);
			this.Label15.TabIndex = 1;
			this.Label15.Text = "TRAN CODE";
			// 
			// Label16
			// 
			this.Label16.AutoSize = true;
			this.Label16.Location = new System.Drawing.Point(684, 495);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(79, 15);
			this.Label16.TabIndex = 2;
			this.Label16.Text = "LAND CODE";
			// 
			// Label17
			// 
			this.Label17.AutoSize = true;
			this.Label17.Location = new System.Drawing.Point(684, 547);
			this.Label17.Name = "Label17";
			this.Label17.Size = new System.Drawing.Size(106, 15);
			this.Label17.TabIndex = 3;
			this.Label17.Text = "BUILDING CODE";
			// 
			// lblMRHIUPDCODE
			// 
			this.lblMRHIUPDCODE.Location = new System.Drawing.Point(564, 1205);
			this.lblMRHIUPDCODE.Name = "lblMRHIUPDCODE";
			this.lblMRHIUPDCODE.Size = new System.Drawing.Size(31, 18);
			this.lblMRHIUPDCODE.TabIndex = 47;
			this.lblMRHIUPDCODE.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.lblMRHIUPDCODE.Visible = false;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuGotoFirstCard,
            this.mnuGotoNextCard,
            this.mnuGotoPreviousCard,
            this.mnuGotoLastCard,
            this.mnuViewGroupInformation,
            this.mnuViewMortgageInformation,
            this.mnuOptions,
            this.mnuSearch,
            this.mnuEdit,
            this.mnuPrint,
            this.mnuSavePending,
            this.mnuDeleteCard,
            this.mnuDelete,
            this.mnuAddAccount});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuGotoFirstCard
			// 
			this.mnuGotoFirstCard.Index = 0;
			this.mnuGotoFirstCard.Name = "mnuGotoFirstCard";
			this.mnuGotoFirstCard.Text = "Goto First Card";
			this.mnuGotoFirstCard.Visible = false;
			this.mnuGotoFirstCard.Click += new System.EventHandler(this.mnuGotoFirstCard_Click);
			// 
			// mnuGotoNextCard
			// 
			this.mnuGotoNextCard.Index = 1;
			this.mnuGotoNextCard.Name = "mnuGotoNextCard";
			this.mnuGotoNextCard.Text = "Goto Next Card ";
			this.mnuGotoNextCard.Visible = false;
			this.mnuGotoNextCard.Click += new System.EventHandler(this.mnuGotoNextCard_Click);
			// 
			// mnuGotoPreviousCard
			// 
			this.mnuGotoPreviousCard.Index = 2;
			this.mnuGotoPreviousCard.Name = "mnuGotoPreviousCard";
			this.mnuGotoPreviousCard.Text = "Goto Previous Card ";
			this.mnuGotoPreviousCard.Visible = false;
			this.mnuGotoPreviousCard.Click += new System.EventHandler(this.mnuGotoPreviousCard_Click);
			// 
			// mnuGotoLastCard
			// 
			this.mnuGotoLastCard.Index = 3;
			this.mnuGotoLastCard.Name = "mnuGotoLastCard";
			this.mnuGotoLastCard.Text = "Goto Last Card";
			this.mnuGotoLastCard.Visible = false;
			this.mnuGotoLastCard.Click += new System.EventHandler(this.mnuGotoLastCard_Click);
			// 
			// mnuViewGroupInformation
			// 
			this.mnuViewGroupInformation.Index = 4;
			this.mnuViewGroupInformation.Name = "mnuViewGroupInformation";
			this.mnuViewGroupInformation.Text = "View Group Information";
			this.mnuViewGroupInformation.Click += new System.EventHandler(this.mnuViewGroupInformation_Click);
			// 
			// mnuViewMortgageInformation
			// 
			this.mnuViewMortgageInformation.Index = 5;
			this.mnuViewMortgageInformation.Name = "mnuViewMortgageInformation";
			this.mnuViewMortgageInformation.Text = "View Mortgage Information";
			this.mnuViewMortgageInformation.Click += new System.EventHandler(this.mnuViewMortgageInformation_Click);
			// 
			// mnuOptions
			// 
			this.mnuOptions.Index = 6;
			this.mnuOptions.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRECLComment,
            this.mnuCollectionsNote,
            this.mnuInterestedParties,
            this.mnuPendingTransfer});
			this.mnuOptions.Name = "mnuOptions";
			this.mnuOptions.Text = "Options";
			// 
			// mnuRECLComment
			// 
			this.mnuRECLComment.Index = 0;
			this.mnuRECLComment.Name = "mnuRECLComment";
			this.mnuRECLComment.Text = "Add Real Estate / Collections Comment";
			this.mnuRECLComment.Click += new System.EventHandler(this.mnuRECLComment_Click);
			// 
			// mnuCollectionsNote
			// 
			this.mnuCollectionsNote.Index = 1;
			this.mnuCollectionsNote.Name = "mnuCollectionsNote";
			this.mnuCollectionsNote.Text = "Collections Note";
			this.mnuCollectionsNote.Click += new System.EventHandler(this.mnuCollectionsNote_Click);
			// 
			// mnuInterestedParties
			// 
			this.mnuInterestedParties.Index = 2;
			this.mnuInterestedParties.Name = "mnuInterestedParties";
			this.mnuInterestedParties.Text = "Interested Parties";
			this.mnuInterestedParties.Click += new System.EventHandler(this.mnuInterestedParties_Click);
			// 
			// mnuPendingTransfer
			// 
			this.mnuPendingTransfer.Index = 3;
			this.mnuPendingTransfer.Name = "mnuPendingTransfer";
			this.mnuPendingTransfer.Text = "Create pending transfer";
			this.mnuPendingTransfer.Click += new System.EventHandler(this.mnuPendingTransfer_Click);
			// 
			// mnuSearch
			// 
			this.mnuSearch.Index = 7;
			this.mnuSearch.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPreviousSave,
            this.mnuNextSave});
			this.mnuSearch.Name = "mnuSearch";
			this.mnuSearch.Text = "Search";
			// 
			// mnuPreviousSave
			// 
			this.mnuPreviousSave.Enabled = false;
			this.mnuPreviousSave.Index = 0;
			this.mnuPreviousSave.Name = "mnuPreviousSave";
			this.mnuPreviousSave.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuPreviousSave.Text = "Previous Account From Search (Save)";
			this.mnuPreviousSave.Click += new System.EventHandler(this.mnuPreviousSave_Click);
			// 
			// mnuNextSave
			// 
			this.mnuNextSave.Enabled = false;
			this.mnuNextSave.Index = 1;
			this.mnuNextSave.Name = "mnuNextSave";
			this.mnuNextSave.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuNextSave.Text = "Next Account From Search (Save)";
			this.mnuNextSave.Click += new System.EventHandler(this.mnuNextSave_Click);
			// 
			// mnuEdit
			// 
			this.mnuEdit.Index = 8;
			this.mnuEdit.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEditPicture});
			this.mnuEdit.Name = "mnuEdit";
			this.mnuEdit.Text = "Edit";
			this.mnuEdit.Visible = false;
			// 
			// mnuEditPicture
			// 
			this.mnuEditPicture.Index = 0;
			this.mnuEditPicture.Name = "mnuEditPicture";
			this.mnuEditPicture.Text = "Edit Picture";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 9;
			this.mnuPrint.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintMailing,
            this.mnuPrintAllCards,
            this.mnuPrintAccountInformationSheet,
            this.mnuPrintInterestedParties});
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			// 
			// mnuPrintMailing
			// 
			this.mnuPrintMailing.Index = 0;
			this.mnuPrintMailing.Name = "mnuPrintMailing";
			this.mnuPrintMailing.Shortcut = Wisej.Web.Shortcut.ShiftF2;
			this.mnuPrintMailing.Text = "Print Mailing Label";
			this.mnuPrintMailing.Click += new System.EventHandler(this.mnuPrintMailing_Click);
			// 
			// mnuPrintAllCards
			// 
			this.mnuPrintAllCards.Index = 1;
			this.mnuPrintAllCards.Name = "mnuPrintAllCards";
			this.mnuPrintAllCards.Text = "Print All Cards";
			this.mnuPrintAllCards.Visible = false;
			// 
			// mnuPrintAccountInformationSheet
			// 
			this.mnuPrintAccountInformationSheet.Index = 2;
			this.mnuPrintAccountInformationSheet.Name = "mnuPrintAccountInformationSheet";
			this.mnuPrintAccountInformationSheet.Text = "Print Account Information Sheet";
			this.mnuPrintAccountInformationSheet.Click += new System.EventHandler(this.mnuPrintAccountInformationSheet_Click);
			// 
			// mnuPrintInterestedParties
			// 
			this.mnuPrintInterestedParties.Index = 3;
			this.mnuPrintInterestedParties.Name = "mnuPrintInterestedParties";
			this.mnuPrintInterestedParties.Text = "Interested Parties";
			this.mnuPrintInterestedParties.Click += new System.EventHandler(this.mnuPrintInterestedParties_Click);
			// 
			// mnuSavePending
			// 
			this.mnuSavePending.Enabled = false;
			this.mnuSavePending.Index = 10;
			this.mnuSavePending.Name = "mnuSavePending";
			this.mnuSavePending.Text = "Save Pending Changes";
			this.mnuSavePending.Click += new System.EventHandler(this.mnuSavePending_Click);
			// 
			// mnuDeleteCard
			// 
			this.mnuDeleteCard.Index = 11;
			this.mnuDeleteCard.Name = "mnuDeleteCard";
			this.mnuDeleteCard.Text = "Delete Card";
			this.mnuDeleteCard.Visible = false;
			this.mnuDeleteCard.Click += new System.EventHandler(this.mnuDeleteCard_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 12;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete Account";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuAddAccount
			// 
			this.mnuAddAccount.Index = 13;
			this.mnuAddAccount.Name = "mnuAddAccount";
			this.mnuAddAccount.Text = "Add Account";
			this.mnuAddAccount.Click += new System.EventHandler(this.mnuAddAccount_Click);
			// 
			// mnuValuationReport
			// 
			this.mnuValuationReport.Index = -1;
			this.mnuValuationReport.Name = "mnuValuationReport";
			this.mnuValuationReport.Text = "Valuation Report (Calculates)";
			this.mnuValuationReport.Visible = false;
			this.mnuValuationReport.Click += new System.EventHandler(this.mnuValuationReport_Click);
			// 
			// mnuPrintCard
			// 
			this.mnuPrintCard.Index = -1;
			this.mnuPrintCard.Name = "mnuPrintCard";
			this.mnuPrintCard.Text = "Print Card";
			this.mnuPrintCard.Click += new System.EventHandler(this.mnuPrintCard_Click);
			// 
			// Separator
			// 
			this.Separator.Index = -1;
			this.Separator.Name = "Separator";
			this.Separator.Text = "-";
			// 
			// mnuComments
			// 
			this.mnuComments.Index = -1;
			this.mnuComments.Name = "mnuComments";
			this.mnuComments.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuComments.Text = "Comments";
			this.mnuComments.Click += new System.EventHandler(this.mnuComments_Click);
			// 
			// mnuPrintLabel1
			// 
			this.mnuPrintLabel1.Index = -1;
			this.mnuPrintLabel1.Name = "mnuPrintLabel1";
			this.mnuPrintLabel1.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuPrintLabel1.Text = "Print Label";
			this.mnuPrintLabel1.Click += new System.EventHandler(this.mnuPrintLabel1_Click);
			// 
			// Separator2
			// 
			this.Separator2.Index = -1;
			this.Separator2.Name = "Separator2";
			this.Separator2.Text = "-";
			// 
			// mnuSaveQuit
			// 
			this.mnuSaveQuit.Index = -1;
			this.mnuSaveQuit.Name = "mnuSaveQuit";
			this.mnuSaveQuit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveQuit.Text = "Save";
			this.mnuSaveQuit.Click += new System.EventHandler(this.mnuSaveQuit_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 0;
			this.mnuQuit.Name = "mnuQuit";
			this.mnuQuit.Text = "Exit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = -1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "";
			// 
			// cmdComments
			// 
			this.cmdComments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdComments.Cursor = Wisej.Web.Cursors.Default;
			this.cmdComments.Location = new System.Drawing.Point(655, 29);
			this.cmdComments.Name = "cmdComments";
			this.cmdComments.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdComments.Size = new System.Drawing.Size(84, 24);
			this.cmdComments.TabIndex = 1;
			this.cmdComments.Text = "Comments";
			this.cmdComments.Click += new System.EventHandler(this.mnuComments_Click);
			// 
			// cmdAddAccount
			// 
			this.cmdAddAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddAccount.Cursor = Wisej.Web.Cursors.Default;
			this.cmdAddAccount.Location = new System.Drawing.Point(204, 29);
			this.cmdAddAccount.Name = "cmdAddAccount";
			this.cmdAddAccount.Size = new System.Drawing.Size(98, 24);
			this.cmdAddAccount.Text = "Add Account";
			this.cmdAddAccount.Click += new System.EventHandler(this.mnuAddAccount_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.Cursor = Wisej.Web.Cursors.Default;
			this.cmdDelete.Location = new System.Drawing.Point(308, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(112, 24);
			this.cmdDelete.Text = "Delete Account";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdSaveQuit
			// 
			this.cmdSaveQuit.AppearanceKey = "acceptButton";
			this.cmdSaveQuit.Cursor = Wisej.Web.Cursors.Default;
			this.cmdSaveQuit.Location = new System.Drawing.Point(482, 34);
			this.cmdSaveQuit.Name = "cmdSaveQuit";
			this.cmdSaveQuit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveQuit.Size = new System.Drawing.Size(134, 48);
			this.cmdSaveQuit.TabIndex = 0;
			this.cmdSaveQuit.Text = "Save";
			this.cmdSaveQuit.Click += new System.EventHandler(this.mnuSaveQuit_Click);
			// 
			// cmdSavePending
			// 
			this.cmdSavePending.AppearanceKey = "acceptButton";
			this.cmdSavePending.Cursor = Wisej.Web.Cursors.Default;
			this.cmdSavePending.Location = new System.Drawing.Point(482, 34);
			this.cmdSavePending.Name = "cmdSavePending";
			this.cmdSavePending.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSavePending.Size = new System.Drawing.Size(214, 48);
			this.cmdSavePending.TabIndex = 1;
			this.cmdSavePending.Text = "Save Pending Changes";
			this.cmdSavePending.Visible = false;
			this.cmdSavePending.Click += new System.EventHandler(this.mnuSavePending_Click);
			// 
			// cmdPrintLabel1
			// 
			this.cmdPrintLabel1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintLabel1.Cursor = Wisej.Web.Cursors.Default;
			this.cmdPrintLabel1.Location = new System.Drawing.Point(745, 29);
			this.cmdPrintLabel1.Name = "cmdPrintLabel1";
			this.cmdPrintLabel1.Shortcut = Wisej.Web.Shortcut.F2;
			this.cmdPrintLabel1.Size = new System.Drawing.Size(82, 24);
			this.cmdPrintLabel1.TabIndex = 4;
			this.cmdPrintLabel1.Text = "Print Label";
			this.cmdPrintLabel1.Click += new System.EventHandler(this.mnuPrintLabel1_Click);
			// 
			// cmdPrintCard
			// 
			this.cmdPrintCard.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintCard.Cursor = Wisej.Web.Cursors.Default;
			this.cmdPrintCard.Location = new System.Drawing.Point(833, 29);
			this.cmdPrintCard.Name = "cmdPrintCard";
			this.cmdPrintCard.Size = new System.Drawing.Size(81, 24);
			this.cmdPrintCard.TabIndex = 5;
			this.cmdPrintCard.Text = "Print Card";
			this.cmdPrintCard.Click += new System.EventHandler(this.mnuPrintCard_Click);
			// 
			// cmdDeleteCard
			// 
			this.cmdDeleteCard.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteCard.Cursor = Wisej.Web.Cursors.Default;
			this.cmdDeleteCard.Location = new System.Drawing.Point(426, 29);
			this.cmdDeleteCard.Name = "cmdDeleteCard";
			this.cmdDeleteCard.Size = new System.Drawing.Size(90, 24);
			this.cmdDeleteCard.Text = "Delete Card";
			this.cmdDeleteCard.Visible = false;
			this.cmdDeleteCard.Click += new System.EventHandler(this.mnuDeleteCard_Click);
			// 
			// fcPanel1
			// 
			this.fcPanel1.Controls.Add(this.chkRLivingTrust);
			this.fcPanel1.Controls.Add(this.chkBankruptcy);
			this.fcPanel1.Controls.Add(this.chkTaxAcquired);
			this.fcPanel1.Location = new System.Drawing.Point(199, 80);
			this.fcPanel1.Name = "fcPanel1";
			this.fcPanel1.Size = new System.Drawing.Size(487, 30);
			this.fcPanel1.TabIndex = 22;
			// 
			// fcPanel2
			// 
			this.fcPanel2.Controls.Add(this.Label27);
			this.fcPanel2.Controls.Add(this.txtPreviousOwner);
			this.fcPanel2.Controls.Add(this.Label7);
			this.fcPanel2.Controls.Add(this.txtMRRSLOCNUMALPH);
			this.fcPanel2.Controls.Add(this.txtMRRSLOCAPT);
			this.fcPanel2.Controls.Add(this.txtMRRSLOCSTREET);
			this.fcPanel2.Controls.Add(this.Label8_0);
			this.fcPanel2.Controls.Add(this.txtMRRSREF1);
			this.fcPanel2.Controls.Add(this.Label10);
			this.fcPanel2.Controls.Add(this.txtMRRSREF2);
			this.fcPanel2.Controls.Add(this.Open1Label);
			this.fcPanel2.Controls.Add(this.Open2Label);
			this.fcPanel2.Controls.Add(this.txtMRPIOPEN1);
			this.fcPanel2.Controls.Add(this.txtMRPIOPEN2);
			this.fcPanel2.Controls.Add(this.txtMRPIACRES);
			this.fcPanel2.Controls.Add(this.Label5);
			this.fcPanel2.Controls.Add(this.Label41);
			this.fcPanel2.Controls.Add(this.lblMRDIENTRANCE);
			this.fcPanel2.Controls.Add(this.Label42);
			this.fcPanel2.Controls.Add(this.txtMRDIENTRANCECODE);
			this.fcPanel2.Controls.Add(this.txtMRDIINFORMATION);
			this.fcPanel2.Controls.Add(this.lblMRDIINFORMATION);
			this.fcPanel2.Controls.Add(this.Label39);
			this.fcPanel2.Controls.Add(this.txtMRDIDATEINSPECTED);
			this.fcPanel2.Location = new System.Drawing.Point(30, 550);
			this.fcPanel2.Name = "fcPanel2";
			this.fcPanel2.Size = new System.Drawing.Size(620, 495);
			this.fcPanel2.TabIndex = 2;
			// 
			// cmdValuationReport
			// 
			this.cmdValuationReport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdValuationReport.Cursor = Wisej.Web.Cursors.Default;
			this.cmdValuationReport.Location = new System.Drawing.Point(920, 29);
			this.cmdValuationReport.Name = "cmdValuationReport";
			this.cmdValuationReport.Size = new System.Drawing.Size(194, 25);
			this.cmdValuationReport.TabIndex = 6;
			this.cmdValuationReport.Text = "Valuation Report (Calculates)";
			this.cmdValuationReport.Visible = false;
			this.cmdValuationReport.Click += new System.EventHandler(this.mnuValuationReport_Click);
			// 
			// fcLabel1
			// 
			this.fcLabel1.Location = new System.Drawing.Point(26, 302);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(102, 15);
			this.fcLabel1.TabIndex = 22;
			this.fcLabel1.Text = "DEED NAME 1";
			// 
			// txtDeedName1
			// 
			this.txtDeedName1.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtDeedName1.Cursor = Wisej.Web.Cursors.Default;
			this.txtDeedName1.Location = new System.Drawing.Point(223, 288);
			this.txtDeedName1.MaxLength = 255;
			this.txtDeedName1.Name = "txtDeedName1";
			this.txtDeedName1.Size = new System.Drawing.Size(423, 40);
			this.txtDeedName1.TabIndex = 25;
			this.txtDeedName1.Tag = "address";
			// 
			// fcLabel2
			// 
			this.fcLabel2.Location = new System.Drawing.Point(26, 352);
			this.fcLabel2.Name = "fcLabel2";
			this.fcLabel2.Size = new System.Drawing.Size(119, 16);
			this.fcLabel2.TabIndex = 26;
			this.fcLabel2.Text = "DEED NAME 2";
			// 
			// txtDeedName2
			// 
			this.txtDeedName2.BackColor = System.Drawing.SystemColors.HighlightText;
			this.txtDeedName2.Cursor = Wisej.Web.Cursors.Default;
			this.txtDeedName2.Location = new System.Drawing.Point(223, 338);
			this.txtDeedName2.MaxLength = 255;
			this.txtDeedName2.Name = "txtDeedName2";
			this.txtDeedName2.Size = new System.Drawing.Size(423, 40);
			this.txtDeedName2.TabIndex = 27;
			this.txtDeedName2.Tag = "address";
			// 
			// cardNumber
			// 
			this.cardNumber.Location = new System.Drawing.Point(365, 27);
			this.cardNumber.Name = "cardNumber";
			this.cardNumber.Size = new System.Drawing.Size(60, 40);
			this.cardNumber.TabIndex = 21;
			this.cardNumber.SelectedIndexChanged += new System.EventHandler(this.cardNumber_SelectedIndexChanged);
			this.cardNumber.Validating += new System.ComponentModel.CancelEventHandler(this.cardNumber_Validating);
			// 
			// frmSHREBLUpdate
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.Cursor = Wisej.Web.Cursors.Default;
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmSHREBLUpdate";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Short Maintenance";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmSHREBLUpdate_Load);
			this.Activated += new System.EventHandler(this.frmSHREBLUpdate_Activated);
			this.Resize += new System.EventHandler(this.frmSHREBLUpdate_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSHREBLUpdate_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSHREBLUpdate_KeyPress);
			this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmSHREBLUpdate_KeyUp);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRemoveSecOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditSecOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearchSecOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridLandCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SaleGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRLivingTrust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridExempts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBankruptcy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTaxAcquired)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.BPGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TreeGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.MapLotGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridBldgCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridPropertyCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTranCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framSaleRecord)).EndInit();
			this.framSaleRecord.ResumeLayout(false);
			this.framSaleRecord.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcPanel4)).EndInit();
			this.fcPanel4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fcPanel3)).EndInit();
			this.fcPanel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdComments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSavePending)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintLabel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fcPanel1)).EndInit();
			this.fcPanel1.ResumeLayout(false);
			this.fcPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fcPanel2)).EndInit();
			this.fcPanel2.ResumeLayout(false);
			this.fcPanel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdValuationReport)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdDelete;
		private FCButton cmdAddAccount;
		private FCButton cmdComments;
		private FCButton cmdSaveQuit;
		private FCButton cmdSavePending;
		private FCButton cmdPrintLabel1;
		private FCButton cmdValuationReport;
		private FCButton cmdPrintCard;
		private FCButton cmdDeleteCard;
		private FCPanel fcPanel1;
		private FCPanel fcPanel2;
		private FCPanel fcPanel3;
		private FCPanel fcPanel4;
        public FCLabel fcLabel1;
        public FCTextBox txtDeedName1;
        public FCLabel fcLabel2;
        public FCTextBox txtDeedName2;
        public FCTextBox txtMRRSNAME;
        public FCTextBox txtMRRSSECOWNER;
        public FCLabel Label6;
        public FCLabel Label4;
        private FCComboBox cardNumber;
		private FCToolStripMenuItem fcToolStripMenuItem1;
		private FCToolStripMenuItem mnuViewGroupInformation;
		private FCToolStripMenuItem mnuViewMortgageInformation;
	}
}