﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportExportCostFiles.
	/// </summary>
	public partial class frmImportExportCostFiles : BaseForm
	{
		public frmImportExportCostFiles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkCostFiles = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_11, 11);
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_9, 9);
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_3, 3);
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_7, 7);
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_6, 6);
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_10, 10);
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_2, 2);
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_1, 1);
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_0, 0);
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_4, 4);
			this.chkCostFiles.AddControlArrayElement(chkCostFiles_5, 5);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmImportExportCostFiles InstancePtr
		{
			get
			{
				return (frmImportExportCostFiles)Sys.GetInstance(typeof(frmImportExportCostFiles));
			}
		}

		protected frmImportExportCostFiles _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 7/19/2004
		// ********************************************************
		bool boolImport;
		string strImExDatabase;
		string strDBPath = "";
		string strDBFile = "";
		const int CNSTEXPCOMMERCIAL = 0;
		const int CNSTEXPDWELLING = 1;
		const int CNSTEXPOUTBUILDING = 2;
		const int CNSTEXPOUTBUILDINGSQFT = 3;
		const int CNSTEXPEXEMPTION = 4;
		const int CNSTEXPPROPERTY = 5;
		const int CNSTEXPLANDSCHEDULE = 6;
		const int CNSTEXPZONENEIGHBORHOOD = 7;
		const int CNSTEXPFRACTIONALACREAGE = 8;
		const int CNSTEXPRSF = 9;
		const int CNSTEXPTRANLANDBLDGCODES = 10;
		const int CNSTEXPLANDACREAGEFACTOR = 11;

		public void Init(bool boolExport)
		{
			string strFile = "";
			//FileSystemObject fso = new FileSystemObject();
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				boolImport = !boolExport;
				if (boolImport == true)
				{
					this.Text = "Import Cost Files";
					this.HeaderText.Text = "Import Cost Files";
					Frame1.Text = "Import";
					chkAll.Text = "Import All";
					// open database and setup screen
					MDIParent.InstancePtr.CommonDialog1.DialogTitle = "File to import from";
                    // MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNFileMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                    MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.DefaultExt = "*.JSON";
					MDIParent.InstancePtr.CommonDialog1.Filter = "TRIO Export files  (*.JSON)|*.JSON";
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					strFile = MDIParent.InstancePtr.CommonDialog1.FileName;
					if (File.Exists(strFile))
					{
						strDBPath = Directory.GetParent(strFile).FullName;
						// If strDBPath = CurDir Then strDBPath = ""
						if (strDBPath != string.Empty)
						{
							if (Strings.Right(strDBPath, 1) != "\\")
								strDBPath += "\\";
						}
						// 
						strDBFile = new FileInfo(strFile).Name;
						string strTemp = "";
						// strTemp = clsLoad.MakeODBCConnectionStringForAccess(strDBPath & strDBFile)
						// Call clsLoad.AddConnection(strTemp, "CostFilesImport", dbConnectionTypes_ODBC)
						GenericJSONTableSerializer tSer = new GenericJSONTableSerializer();
						TableCollection tTabColl = new TableCollection();
						string strJSON = "";
						//StreamWriter ts;
						StreamReader ts;
						//ts = File.OpenText(strFile);
						ts = File.OpenText(strFile);
						//strJSON = ts.ReadAll();
						strJSON = ts.ReadToEnd();
						ts.Close();
						tTabColl = tSer.GetCollectionFromJSON(strJSON);
						if (!(tTabColl == null))
						{
						}
						else
						{
							MessageBox.Show("The data is not in the correct format" + "\r\n" + "Cannot continue", "Wrong File Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						// now see which tables are filled and if this is a legit db
						// If strDBPath <> vbNullString Then
						// 
						// clsLoad.Path = strDBPath
						// End If
						// If clsLoad.UpdateDatabaseTable("master", "CostFilesImport") Then
						// MsgBox "This is a Real Estate database not an exported cost file database" & vbNewLine & "Cannot continue", vbExclamation, "Wrong File Format"
						// Exit Sub
						// End If
						// 
						// If Not clsLoad.UpdateDatabaseTable("propertycost", strDBFile) Then
						// MsgBox "This file does not have the correct tables. It is not an exported cost file database" & vbNewLine & "Cannot continue", vbExclamation, "Wrong File Format"
						// Exit Sub
						// End If
						chkCostFiles[FCConvert.ToInt16(CNSTEXPPROPERTY)].Enabled = false;
						chkCostFiles[FCConvert.ToInt16(CNSTEXPCOMMERCIAL)].Enabled = false;
						chkCostFiles[FCConvert.ToInt16(CNSTEXPDWELLING)].Enabled = false;
						chkCostFiles[FCConvert.ToInt16(CNSTEXPEXEMPTION)].Enabled = false;
						chkCostFiles[FCConvert.ToInt16(CNSTEXPLANDACREAGEFACTOR)].Enabled = false;
						chkCostFiles[FCConvert.ToInt16(CNSTEXPLANDSCHEDULE)].Enabled = false;
						chkCostFiles[FCConvert.ToInt16(CNSTEXPZONENEIGHBORHOOD)].Enabled = false;
						chkCostFiles[FCConvert.ToInt16(CNSTEXPOUTBUILDING)].Enabled = false;
						chkCostFiles[FCConvert.ToInt16(CNSTEXPOUTBUILDINGSQFT)].Enabled = false;
						chkCostFiles[FCConvert.ToInt16(CNSTEXPRSF)].Enabled = false;
						chkCostFiles[FCConvert.ToInt16(CNSTEXPTRANLANDBLDGCODES)].Enabled = false;
						int x;
						for (x = 0; x <= tTabColl.TableCount() - 1; x++)
						{
							if (Strings.LCase(tTabColl.TableName(x)) == "propertycost")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPPROPERTY)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "commercialcost")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPCOMMERCIAL)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "dwellingcost")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPDWELLING)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "exemptcode")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPEXEMPTION)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "landtype")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPLANDACREAGEFACTOR)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "landschedule")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPLANDSCHEDULE)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "neighborhood")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPZONENEIGHBORHOOD)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "outbuildingcost")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPOUTBUILDING)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "outbuildingsqftfactors")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPOUTBUILDINGSQFT)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "rszrecord")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPRSF)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "tblbldgcode")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPTRANLANDBLDGCODES)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "tbllandcode")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPTRANLANDBLDGCODES)].Enabled = true;
							}
							else if (Strings.LCase(tTabColl.TableName(x)) == "tbltrancode")
							{
								chkCostFiles[FCConvert.ToInt16(CNSTEXPTRANLANDBLDGCODES)].Enabled = true;
							}
						}
						// x
					}
					else
					{
						MessageBox.Show("The file " + strFile + " does not exist", "File Does Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					this.Text = "Export Cost Files";
					this.HeaderText.Text = "Export Cost Files";
					Frame1.Text = "Export";
					chkAll.Text = "Export All";
				}
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 32755)
				{
					// cancelled
					return;
				}
			}
		}

		private void chkAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int x;
			if (chkAll.CheckState == Wisej.Web.CheckState.Checked)
			{
				for (x = 0; x <= 11; x++)
				{
					if (x != 8)
					{
						if (boolImport)
						{
							if (chkCostFiles[FCConvert.ToInt16(x)].Enabled == true)
							{
								chkCostFiles[FCConvert.ToInt16(x)].CheckState = Wisej.Web.CheckState.Checked;
							}
						}
						else
						{
							chkCostFiles[FCConvert.ToInt16(x)].CheckState = Wisej.Web.CheckState.Checked;
							chkCostFiles[FCConvert.ToInt16(x)].Enabled = false;
						}
					}
				}
				// x
				Frame1.Enabled = false;
			}
			else
			{
				Frame1.Enabled = true;
				if (!boolImport)
				{
					for (x = 0; x <= 11; x++)
					{
						if (x != 8)
						{
							chkCostFiles[FCConvert.ToInt16(x)].Enabled = true;
						}
					}
					// x
				}
			}
		}

		private void frmImportExportCostFiles_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmImportExportCostFiles_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmImportExportCostFiles properties;
			//frmImportExportCostFiles.FillStyle	= 0;
			//frmImportExportCostFiles.ScaleWidth	= 9045;
			//frmImportExportCostFiles.ScaleHeight	= 7260;
			//frmImportExportCostFiles.LinkTopic	= "Form2";
			//frmImportExportCostFiles.LockControls	= true;
			//frmImportExportCostFiles.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}
		// Private Function CreateCostDatabase(strFile As String) As Boolean
		// Dim dbCreate As DAO.Database
		// Dim clsCreate As New clsDRWrapper
		// Dim x As Integer
		//
		// On Error GoTo ErrorHandler
		//
		// CreateCostDatabase = False
		//
		// Call DBEngine.CreateDatabase(strDBPath & strDBFile, dbLangGeneral, dbVersion40)
		//
		// clsCreate.Path = strDBPath
		// If clsCreate.CreateNewDatabaseTable("CommercialCost", strFile) Then
		// Call clsCreate.AddTableField("Cunit", dbLong)
		// Call clsCreate.AddTableField("CBase", dbLong)
		// Call clsCreate.AddTableField("CLDesc", dbText)
		// Call clsCreate.AddTableField("CSDesc", dbText)
		// Call clsCreate.SetFieldAllowZeroLength("CLDesc", True)
		// Call clsCreate.SetFieldAllowZeroLength("CSDesc", True)
		// Call clsCreate.AddTableField("Crecordnumber", dbLong)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		//
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("DwellingCost", strFile) Then
		// Call clsCreate.AddTableField("Cunit", dbLong)
		// Call clsCreate.AddTableField("CBase", dbLong)
		// Call clsCreate.AddTableField("CLDesc", dbText)
		// Call clsCreate.AddTableField("CSDesc", dbText)
		// Call clsCreate.AddTableField("Crecordnumber", dbLong)
		// Call clsCreate.SetFieldAllowZeroLength("CLDesc", True)
		// Call clsCreate.SetFieldAllowZeroLength("CSDesc", True)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("DwellingHeat", strFile) Then
		// Call clsCreate.AddTableField("Unit", dbDouble)
		// Call clsCreate.AddTableField("Base", dbDouble)
		// Call clsCreate.AddTableField("Description", dbText, 255)
		// Call clsCreate.AddTableField("ShortDescription", dbText, 255)
		// Call clsCreate.SetFieldAllowZeroLength("Description", True)
		// Call clsCreate.SetFieldAllowZeroLength("ShortDescription", True)
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("DwellingStyle", strFile) Then
		// Call clsCreate.AddTableField("Factor", dbDouble)
		// Call clsCreate.AddTableField("Description", dbText, 255)
		// Call clsCreate.AddTableField("ShortDescription", dbText, 255)
		// Call clsCreate.SetFieldAllowZeroLength("Description", True)
		// Call clsCreate.SetFieldAllowZeroLength("ShortDescription", True)
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("DwellingExterior", strFile) Then
		// Call clsCreate.AddTableField("Factor", dbDouble)
		// Call clsCreate.AddTableField("Description", dbText, 255)
		// Call clsCreate.AddTableField("ShortDescription", dbText, 255)
		// Call clsCreate.SetFieldAllowZeroLength("Description", True)
		// Call clsCreate.SetFieldAllowZeroLength("ShortDescription", True)
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("EconomicCode", strFile) Then
		// Call clsCreate.AddTableField("Factor", dbDouble)
		// Call clsCreate.AddTableField("Description", dbText, 255)
		// Call clsCreate.AddTableField("ShortDescription", dbText, 255)
		// Call clsCreate.SetFieldAllowZeroLength("Description", True)
		// Call clsCreate.SetFieldAllowZeroLength("ShortDescription", True)
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("OutbuildingCost", strFile) Then
		// Call clsCreate.AddTableField("Cunit", dbLong)
		// Call clsCreate.AddTableField("CBase", dbLong)
		// Call clsCreate.AddTableField("CLDesc", dbText)
		// Call clsCreate.AddTableField("CSDesc", dbText)
		// Call clsCreate.SetFieldAllowZeroLength("CLDesc", True)
		// Call clsCreate.SetFieldAllowZeroLength("CSDesc", True)
		// Call clsCreate.AddTableField("Crecordnumber", dbLong)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		//
		// If clsCreate.CreateNewDatabaseTable("ExemptCode", strFile) Then
		// Call clsCreate.AddTableField("id", dbLong)
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("Amount", dbLong)
		// Call clsCreate.AddTableField("Description", dbText, 50)
		// Call clsCreate.AddTableField("ShortDescription", dbText, 50)
		// Call clsCreate.AddTableField("Category", dbInteger)
		// Call clsCreate.AddTableField("Enlisted", dbInteger)
		// Call clsCreate.AddTableField("ApplyTo", dbInteger)
		// Call clsCreate.SetFieldAllowZeroLength("Description", True)
		// Call clsCreate.SetFieldAllowZeroLength("ShortDescription", True)
		//
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("tblTranCode", strFile) Then
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("Amount", dbDouble)
		// Call clsCreate.AddTableField("Description", dbText)
		// Call clsCreate.AddTableField("ShortDescription", dbText)
		// Call clsCreate.SetFieldAllowZeroLength("Description", True)
		// Call clsCreate.SetFieldAllowZeroLength("ShortDescription", True)
		//
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("tblBldgCode", strFile) Then
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("Amount", dbDouble)
		// Call clsCreate.AddTableField("Description", dbText)
		// Call clsCreate.AddTableField("ShortDescription", dbText)
		// Call clsCreate.SetFieldAllowZeroLength("Description", True)
		// Call clsCreate.SetFieldAllowZeroLength("ShortDescription", True)
		//
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("tblLandCode", strFile) Then
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("Amount", dbDouble)
		// Call clsCreate.AddTableField("Description", dbText)
		// Call clsCreate.AddTableField("ShortDescription", dbText)
		// Call clsCreate.SetFieldAllowZeroLength("Description", True)
		// Call clsCreate.SetFieldAllowZeroLength("ShortDescription", True)
		//
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("PropertyCost", strFile) Then
		// Call clsCreate.AddTableField("Cunit", dbLong)
		// Call clsCreate.AddTableField("CBase", dbLong)
		// Call clsCreate.AddTableField("CLDesc", dbText)
		// Call clsCreate.SetFieldAllowZeroLength("CLDesc", True)
		// Call clsCreate.AddTableField("CSDesc", dbText)
		// Call clsCreate.SetFieldAllowZeroLength("CSDesc", True)
		// Call clsCreate.AddTableField("Crecordnumber", dbLong)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("ZoneSchedule", strFile) Then
		// Call clsCreate.AddTableField("Neighborhood", dbLong)
		// Call clsCreate.AddTableField("Zone", dbLong)
		// Call clsCreate.AddTableField("Schedule", dbLong)
		// Call clsCreate.AddTableField("LandFactor", dbDouble)
		// Call clsCreate.AddTableField("BldgFactor", dbDouble)
		// Call clsCreate.AddTableField("Townnumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("Zones", strFile) Then
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("Description", dbText, 255)
		// Call clsCreate.SetFieldAllowZeroLength("Description", True)
		// Call clsCreate.AddTableField("ShortDescription", dbText, 255)
		// Call clsCreate.AddTableField("SecondaryDescription", dbText, 255)
		// Call clsCreate.AddTableField("SecondaryShort", dbText, 255)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Call clsCreate.SetAllTextFieldsToAllowNull("Zones", strFile)
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("Neighborhood", strFile) Then
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("Description", dbText, 255)
		// Call clsCreate.SetFieldAllowZeroLength("Description", True)
		// Call clsCreate.AddTableField("ShortDescription", dbText, 255)
		// Call clsCreate.SetFieldAllowZeroLength("ShortDescription", True)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("OutbuildingSQFT", strFile) Then
		// Call clsCreate.AddTableField("Factor", dbDouble)
		// Call clsCreate.AddTableField("Code", dbLong)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("RSZRecord", strFile) Then
		// Call clsCreate.AddTableField("RSZ", dbText)
		// Call clsCreate.SetFieldAllowZeroLength("RSZ", True)
		// For x = 1 To 25
		// Call clsCreate.AddTableField("RSZFct" & Format(x, "00"), dbText)
		// Call clsCreate.AddTableField("RSZRGE" & Format(x, "00"), dbText)
		// Next x
		// Call clsCreate.AddTableField("RRecordNumber", dbLong)
		//
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		//
		// If clsCreate.CreateNewDatabaseTable("LandType", strFile) Then
		// With clsCreate
		// Call .AddTableField("id", dbLong)
		// Call .SetFieldAttribute("id", dbAutoIncrField)
		// Call .AddTableField("Code", dbLong)
		// Call .AddTableField("Description", dbText, 50)
		// Call .AddTableField("ShortDescription", dbText, 50)
		// Call .SetFieldAllowZeroLength("Description", True)
		// Call .SetFieldAllowZeroLength("ShortDescription", True)
		// Call .AddTableField("Type", dbLong)
		// Call .AddTableField("Category", dbLong)
		// Call .AddTableField("Factor", dbDouble)
		// End With
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		// If clsCreate.CreateNewDatabaseTable("LandSchedule", strFile) Then
		// With clsCreate
		// Call .AddTableField("id", dbLong)
		// Call .SetFieldAttribute("id", dbAutoIncrField)
		// Call .AddTableField("Schedule", dbLong)
		// Call .AddTableField("Code", dbLong)
		// Call .AddTableField("Amount", dbDouble)
		// Call .AddTableField("Exponent1", dbDouble)
		// Call .AddTableField("Exponent2", dbDouble)
		// Call clsCreate.AddTableField("TownNumber", dbLong)
		// End With
		// Call clsCreate.UpdateTableCreation
		// Else
		// Exit Function
		// End If
		//
		//
		// CreateCostDatabase = True
		// Exit Function
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & "  " & Err.Description & vbNewLine & "In CreateCostDatabase", vbCritical, "Error"
		// End Function
		private bool FillCostDatabase(ref string strPath, ref string strFile)
		{
			bool FillCostDatabase = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsLoad2 = new clsDRWrapper();
			// Dim clsSave As New clsDRWrapper
			int x;
			//FileSystemObject fso = new FileSystemObject();
			StreamWriter ts;
			TableCollection tTabColl = new TableCollection();
			tTabColl.DBName = "CostFiles";
			TableItem tTabItem = new TableItem();
			RecordItem tRec = new RecordItem();
			try
			{
				// On Error GoTo ErrorHandler
				FillCostDatabase = false;
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPCOMMERCIAL)].CheckState == Wisej.Web.CheckState.Checked)
				{
					clsLoad.OpenRecordset("select * from commercialcost", modGlobalVariables.strREDatabase);
					// Call clsSave.OpenRecordset("select * from commercialcost", strFile)
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "CommercialCost";
					// Do While Not clsLoad.EndOfFile
					// tRec = New RecordItem
					// Call tRec.SetItem("TownNumber", Val(clsLoad.Fields("townnumber")))
					// Call tRec.SetItem("CUnit", Val(clsLoad.Fields("Cunit")))
					// Call tRec.SetItem("CBase", Val(clsLoad.Fields("cbase")))
					// Call tRec.SetItem("CLDesc", clsLoad.Fields("cldesc"))
					// Call tRec.SetItem("CSDesc", clsLoad.Fields("csdesc"))
					// Call tRec.settiem("CRecordNumber", clsLoad.Fields("crecordnumber"))
					// tTabItem.AddItem (tRec)
					// clsLoad.MoveNext
					// Loop
					tTabColl.AddItem(tTabItem);
				}
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPOUTBUILDING)].CheckState == Wisej.Web.CheckState.Checked)
				{
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber < 1000", modGlobalVariables.strREDatabase);
					// Call clsSave.OpenRecordset("select * from outbuildingcost", strFile)
					tTabItem = new TableItem();
					tTabItem.TableName = "OutbuildingCost";
					while (!clsLoad.EndOfFile())
					{
						tRec = new RecordItem();
						if (!(Conversion.Val(clsLoad.Get_Fields_String("cunit")) > modGlobalConstants.EleventyBillion))
						{
							tRec.SetItem("CUnit", FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("cunit"))));
						}
						else
						{
							tRec.SetItem("CUnit", FCConvert.ToString(99999999));
						}
						if (!(Conversion.Val(clsLoad.Get_Fields_String("cbase")) > modGlobalConstants.EleventyBillion))
						{
							tRec.SetItem("CBase", FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("cbase"))));
						}
						else
						{
							tRec.SetItem("CBase", FCConvert.ToString(99999999));
						}
						tRec.SetItem("CLDesc", clsLoad.Get_Fields_String("cldesc"));
						tRec.SetItem("CSDesc", clsLoad.Get_Fields_String("csdesc"));
						//FC:FINAL:MSH - i.issue #1140: SetItem work only with string values
						tRec.SetItem("CRecordNumber", FCConvert.ToString(clsLoad.Get_Fields_Int32("crecordnumber")));
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						tRec.SetItem("TownNumber", FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("townnumber"))));
						tTabItem.AddItem(ref tRec);
						clsLoad.MoveNext();
					}
					tTabColl.AddItem(tTabItem);
				}
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPTRANLANDBLDGCODES)].CheckState == Wisej.Web.CheckState.Checked)
				{
					clsLoad.OpenRecordset("SELECT * from tblTranCode", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "tblTranCode";
					// Do While Not clsLoad.EndOfFile
					// tRec = New RecordItem
					// Call tRec.SetItem("Code", Val(clsLoad.Fields("Code")))
					// Call tRec.SetItem("Description", clsLoad.Fields("description"))
					// Call tRec.SetItem("ShortDescription", clsLoad.Fields("ShortDescription"))
					// Call tRec.SetItem("Amount", clsLoad.Fields("Amount"))
					// tTabItem.AddItem (tRec)
					// clsLoad.MoveNext
					// Loop
					tTabColl.AddItem(tTabItem);
					clsLoad.OpenRecordset("select * from tblLandCode", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "tblLandCode";
					// Do While Not clsLoad.EndOfFile
					// tRec = New RecordItem
					// Call tRec.SetItem("Code", Val(clsLoad.Fields("Code")))
					// Call tRec.SetItem("Description", clsLoad.Fields("description"))
					// Call tRec.SetItem("ShortDescription", clsLoad.Fields("ShortDescription"))
					// Call tRec.SetItem("Amount", clsLoad.Fields("Amount"))
					// tTabItem.AddItem (tRec)
					// clsLoad.MoveNext
					// Loop
					tTabColl.AddItem(tTabItem);
					clsLoad.OpenRecordset("Select * from tblBldgCode", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "tblBldgCode";
					tTabColl.AddItem(tTabItem);
				}
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPDWELLING)].CheckState == Wisej.Web.CheckState.Checked)
				{
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1450 and 1899", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "DwellingCost";
					tTabColl.AddItem(tTabItem);
					clsLoad.OpenRecordset("select * from DwellingHeat", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "DwellingHeat";
					tTabColl.AddItem(tTabItem);
					clsLoad.OpenRecordset("select * from dwellingexterior", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "DwellingExterior";
					tTabColl.AddItem(tTabItem);
					clsLoad.OpenRecordset("Select * from dwellingstyle", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "DwellingStyle";
					tTabColl.AddItem(tTabItem);
					clsLoad.OpenRecordset("select * from economiccode", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "EconomicCode";
					tTabColl.AddItem(tTabItem);
				}
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPPROPERTY)].CheckState == Wisej.Web.CheckState.Checked)
				{
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1000 and 1449", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "PropertyCost";
					tTabColl.AddItem(tTabItem);
				}
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPEXEMPTION)].CheckState == Wisej.Web.CheckState.Checked)
				{
					// this will have to change when exemptions are put in their own table
					clsLoad.OpenRecordset("select * from exemptcode order by code", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "ExemptCode";
					tTabColl.AddItem(tTabItem);
				}
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPRSF)].CheckState == Wisej.Web.CheckState.Checked)
				{
					clsLoad.OpenRecordset("select * from rszrecord", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "rszRecord";
					tTabColl.AddItem(tTabItem);
				}
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPLANDSCHEDULE)].CheckState == Wisej.Web.CheckState.Checked)
				{
					clsLoad.OpenRecordset("select * from landschedule order by schedule,code", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "LandSchedule";
					tTabColl.AddItem(tTabItem);
				}
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPOUTBUILDINGSQFT)].CheckState == Wisej.Web.CheckState.Checked)
				{
					clsLoad.OpenRecordset("select * from outbuildingsqftfactors order by code", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "OutbuildingSqftFactors";
					tTabColl.AddItem(tTabItem);
				}
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPLANDACREAGEFACTOR)].CheckState == Wisej.Web.CheckState.Checked)
				{
					clsLoad.OpenRecordset("SELECT * from landtype order by code", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "LandType";
					tTabColl.AddItem(tTabItem);
				}
				if (chkCostFiles[FCConvert.ToInt16(CNSTEXPZONENEIGHBORHOOD)].CheckState == Wisej.Web.CheckState.Checked)
				{
					clsLoad.OpenRecordset("select * from ZONESCHEDULE ", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "ZoneSchedule";
					tTabColl.AddItem(tTabItem);
					clsLoad.OpenRecordset("select * from zones", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "Zones";
					tTabColl.AddItem(tTabItem);
					clsLoad.OpenRecordset("select * from neighborhood", modGlobalVariables.strREDatabase);
					tTabItem = clsLoad.TableToTableItem();
					tTabItem.TableName = "Neighborhood";
					tTabColl.AddItem(tTabItem);
				}
				ts = File.CreateText(strPath + strFile);
				GenericJSONTableSerializer tSer = new GenericJSONTableSerializer();
				ts.Write(tSer.GetJSONFromCollection(ref tTabColl));
				ts.Close();
				FillCostDatabase = true;
				MessageBox.Show("Export Complete", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return FillCostDatabase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In  FillCostDatabase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillCostDatabase;
		}

		private bool ExportCostFiles()
		{
			bool ExportCostFiles = false;
			//FileSystemObject fso = new FileSystemObject();
			clsDRWrapper clsCreate = new clsDRWrapper();
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				ExportCostFiles = false;
				//FC:FINAL:MSH - i.issue #1140: Save dialog not implemented
				//// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNCreatePrompt	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Export File To Create";
				//// .FileTitle = "Choose a directory and a name for the file to export"
				//MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
				//MDIParent.InstancePtr.CommonDialog1.FileName = "";
				//MDIParent.InstancePtr.CommonDialog1.Filter = "TRIO Export files  (*.JSON)|*.JSON";
				//MDIParent.InstancePtr.CommonDialog1.DefaultExt = "JSON";
				////- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				//MDIParent.InstancePtr.CommonDialog1.Show();
				//strTemp = MDIParent.InstancePtr.CommonDialog1.FileName;
				//strDBPath = Directory.GetParent(strTemp).FullName;
				//strDBFile = new FileInfo(strTemp).Name;
				//FC:FINAL:MSH - i.issue #1140: create temp file on server and add data to it
				string tempFileName = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp", "ExportCostFiles_" + Path.GetRandomFileName());
				tempFileName = Path.ChangeExtension(tempFileName, "JSON");
				if (!Directory.Exists(Path.GetDirectoryName(tempFileName)))
				{
					Directory.CreateDirectory(Path.GetDirectoryName(tempFileName));
				}
				strTemp = tempFileName;
				strDBPath = Directory.GetParent(strTemp).FullName;
				strDBFile = new FileInfo(strTemp).Name;
				if (strDBPath == FCFileSystem.Statics.UserDataFolder)
					strDBPath = "";
				// no need to change paths when it is the curdir
				if (strDBPath != string.Empty)
				{
					if (Strings.Right(strDBPath, 1) != "\\")
						strDBPath += "\\";
				}
				strImExDatabase = strDBPath + strDBFile;
				if (File.Exists(strImExDatabase))
				{
					File.Delete(strImExDatabase);
				}
				// If Not CreateCostDatabase(strDBFile) Then
				// Exit Function
				// End If
				if (!FillCostDatabase(ref strDBPath, ref strDBFile))
				{
					return ExportCostFiles;
				}
				//FC:FINAL:MSH - i.issue #1140: save file on client side
				FCUtils.Download(strTemp);
				ExportCostFiles = true;
				return ExportCostFiles;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (Information.Err(ex).Number == 32755)
				{
					// they hit cancel
					ExportCostFiles = false;
					return ExportCostFiles;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ExportCostFiles", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ExportCostFiles;
		}

		private bool ImportCostFiles()
		{
			bool ImportCostFiles = false;
			// file has already been chosen
			// Dim clsLoad As New clsDRWrapper
			clsDRWrapper clsSave = new clsDRWrapper();
			clsDRWrapper clsSave2 = new clsDRWrapper();
			////FileSystemObject fso = new FileSystemObject();
			//StreamWriter ts;
			StreamReader ts;
			int x;
			string strFile = "";
			try
			{
				// On Error GoTo ErrorHandler
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				frmWait.InstancePtr.Init("Importing Cost Files", false, 100, true);
				//if (!File.Exists(strDBPath + strDBFile))
				if (!File.Exists(strDBPath + strDBFile))
				{
					MessageBox.Show("File not found " + strDBPath + strDBFile, "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return ImportCostFiles;
				}
				//ts = File.OpenText(strDBPath + strDBFile);
				ts = File.OpenText(strDBPath + strDBFile);
				TableCollection tTabColl = new TableCollection();
				TableItem tTabItem = new TableItem();
				if (!(ts == null))
				{
					GenericJSONTableSerializer tSer = new GenericJSONTableSerializer();
					//tTabColl = tSer.GetCollectionFromJSON(ts.ReadAll());
					tTabColl = tSer.GetCollectionFromJSON(ts.ReadToEnd());
					//FC:FINAL:DDU:#1856 - read file without IOException
					ts.Close();
					if (!(tTabColl == null))
					{
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPCOMMERCIAL)].CheckState == Wisej.Web.CheckState.Checked)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Commercial Cost";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            //Application.DoEvents();
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "commercialcost")
								{
									clsSave.Execute("delete from commercialcost", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select * from commercialcost", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPDWELLING)].CheckState == Wisej.Web.CheckState.Checked)
						{
							// this has several tables
							frmWait.InstancePtr.lblMessage.Text = "Importing Dwelling Cost";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "dwellingcost")
								{
									clsSave.Execute("delete from costrecord where crecordnumber between 1450 and 1899", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select top 1 * from costrecord where id = -1", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
							frmWait.InstancePtr.lblMessage.Text = "Importing Dwelling Heat";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "dwellingheat")
								{
									clsSave.Execute("delete from dwellingheat", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select * from dwellingheat", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
							frmWait.InstancePtr.lblMessage.Text = "Importing Dwelling Exterior";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "dwellingexterior")
								{
									clsSave.Execute("delete from dwellingexterior", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select * from dwellingexterior", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
							frmWait.InstancePtr.lblMessage.Text = "Importing Economic Code";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "economiccode")
								{
									clsSave.Execute("delete from economiccode", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select * from economiccode", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
							frmWait.InstancePtr.lblMessage.Text = "Importing Dwelling Style";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "dwellingstyle")
								{
									clsSave.Execute("delete from dwellingstyle", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select * from dwellingstyle", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPEXEMPTION)].CheckState == Wisej.Web.CheckState.Checked)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Exemption Codes";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "exemptcode")
								{
									clsSave.Execute("delete from EXEMPTCODE", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select top 1 * from exemptcode", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPLANDACREAGEFACTOR)].CheckState == Wisej.Web.CheckState.Checked)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Land Types and Acreage Factors";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "landtype")
								{
									clsSave.Execute("delete from landtype", modGlobalVariables.strREDatabase, false);
									clsSave.OpenRecordset("select * from landtype", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPZONENEIGHBORHOOD)].CheckState == Wisej.Web.CheckState.Checked)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Zone / Neighborhood";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "neighborhood")
								{
									clsSave.Execute("delete from neighborhood", modGlobalVariables.strREDatabase, false);
									clsSave.OpenRecordset("select top 1 * from neighborhood", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
							for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "zones")
								{
									clsSave.Execute("delete from zones", modGlobalVariables.strREDatabase, false);
									clsSave.OpenRecordset("select top 1 * from zones", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
							for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "zoneschedule")
								{
									clsSave.Execute("delete from zoneschedule", modGlobalVariables.strREDatabase, false);
									clsSave.OpenRecordset("select top 1 * from zoneschedule", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPLANDSCHEDULE)].CheckState == Wisej.Web.CheckState.Checked)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Land Schedules";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "landschedule")
								{
									clsSave.Execute("delete from landschedule", modGlobalVariables.strREDatabase, false);
									// DoEvents
									clsSave.OpenRecordset("select * from landschedule", modGlobalVariables.strREDatabase);
									// DoEvents
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPOUTBUILDING)].CheckState == Wisej.Web.CheckState.Checked)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Outbuilding Cost";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "outbuildingcost")
								{
									clsSave.Execute("delete from costrecord where crecordnumber < 1000", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select top 1 * from costrecord where id = -1", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPOUTBUILDINGSQFT)].CheckState == Wisej.Web.CheckState.Checked)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Outbuilding SQFT Factors";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "outbuildingsqftfactors")
								{
									clsSave.Execute("delete from outbuildingsqftfactors", modGlobalVariables.strREDatabase, false);
									clsSave.OpenRecordset("select * from outbuildingsqftfactors", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPPROPERTY)].CheckState == Wisej.Web.CheckState.Checked)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Property Cost";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "propertycost")
								{
									clsSave.Execute("delete from costrecord where crecordnumber between 1000 and 1449", modGlobalVariables.strREDatabase, false);
									clsSave.OpenRecordset("select top 1 * from costrecord", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPRSF)].CheckState == Wisej.Web.CheckState.Checked)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Residential Size Factors";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "rszrecord")
								{
									clsSave.Execute("delete * from rszrecord", modGlobalVariables.strREDatabase, false);
									clsSave.OpenRecordset("select * from rszrecord", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						if (chkCostFiles[FCConvert.ToInt16(CNSTEXPTRANLANDBLDGCODES)].CheckState == Wisej.Web.CheckState.Checked)
						{
							frmWait.InstancePtr.lblMessage.Text = "Importing Land Codes";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "tbllandcode")
								{
									clsSave.Execute("delete from tbllandcode", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select * from tbllandcode", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
							frmWait.InstancePtr.lblMessage.Text = "Importing Tran Codes";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "tbltrancode")
								{
									clsSave.Execute("delete from tbltrancode", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select * from tbltrancode", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
							frmWait.InstancePtr.lblMessage.Text = "Importing Building Codes";
                            frmWait.InstancePtr.Refresh();
                            // frmWait.lblMessage.Refresh
                            for (x = 0; x <= tTabColl.TableCount() - 1; x++)
							{
								//Application.DoEvents();
								if (Strings.LCase(tTabColl.TableName(x)) == "tblbldgcode")
								{
									clsSave.Execute("delete from tblbldgcode", modGlobalVariables.strREDatabase);
									clsSave.OpenRecordset("select * from tblbldgcode", modGlobalVariables.strREDatabase);
									TableItem tTableItem = tTabColl.Item(tTabColl.TableName(x));
									clsSave.TableItemToTable(true, ref tTableItem, "ID");
									clsSave.MoveFirst();
									clsSave.Update(true);
									break;
								}
							}
							// x
						}
						ImportCostFiles = true;
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Cost files imported", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return ImportCostFiles;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ImportCostFiles", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportCostFiles;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (boolImport)
			{
				if (ImportCostFiles())
				{
					mnuExit_Click();
				}
			}
			else
			{
				if (ExportCostFiles())
				{
					mnuExit_Click();
				}
			}
		}
	}
}
