﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using System.Linq;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;
using CryptoUtility = TWSharedLibrary.CryptoUtility;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmDeleteDuplicatePictures.
	/// </summary>
	public partial class frmDeleteDuplicatePictures : BaseForm
	{
		public frmDeleteDuplicatePictures()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDeleteDuplicatePictures InstancePtr
		{
			get
			{
				return (frmDeleteDuplicatePictures)Sys.GetInstance(typeof(frmDeleteDuplicatePictures));
			}
		}

		protected frmDeleteDuplicatePictures _InstancePtr = null;

		private void frmDeleteDuplicatePictures_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmDeleteDuplicatePictures_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

        
        private void RemoveDuplicates()
        {
            var hashLists = new Dictionary<int, List<(int Id,string CardIdentifier,byte[] HashBytes,Guid DocumentIdentifier)>>();
            var rsLoad = new clsDRWrapper();
            var rsSave = new clsDRWrapper();
            var affected = new List<int>();
            rsLoad.OpenRecordset("Select Id, ReferenceId,DocumentIdentifier,AltReference, HashBytes('Sha2_256',ItemData) as theHash from Documents where ReferenceGroup = 'RealEstate' and ReferenceType = 'PropertyPicture' order by referenceid", "CentralDocuments");
            while (!rsLoad.EndOfFile())
            {
                
                var account = rsLoad.Get_Fields_Int32("ReferenceId");
                lblProgress.Text = "Checking Account " + account;
                lblProgress.Refresh();
                var hashBytes = rsLoad.Get_Fields_Binary("theHash");
                var documentIdentifier = rsLoad.Get_Fields_Guid("DocumentIdentifier");
                var cardIdentifier = rsLoad.Get_Fields_String("AltReference");
                var id = rsLoad.Get_Fields_Int32("Id");
                List<(int Id,string CardIdentifier, byte[] HashBytes, Guid DocumentIdentifier)> hashList = null;
                if (!hashLists.ContainsKey(account))
                {
                    hashList = new List<(int Id,string CardIdentifier, byte[] HashBytes, Guid DocumentIdentifier)>();
                    hashLists.Add(account,hashList);
                }
                else
                {
                    hashList = hashLists[account];
                }

                var boolDeleted = false;
                foreach (var hashItem in hashList)
                {
                    if (hashItem.HashBytes.SequenceEqual(hashBytes))
                    {
                        rsSave.Execute("Delete from Documents where id = " + id, "CentralDocuments");
                        rsSave.Execute("Delete from picturerecord where DocumentIdentifier = '" + documentIdentifier.ToString() + "'","RealEstate");
                        if (!affected.Contains(account))
                        {
                            affected.Add(account);
                        }
                        boolDeleted = true;
                        break;
                    }
                }

                if (!boolDeleted)
                {
                    hashList.Add((Id: id, CardIdentifier: cardIdentifier, HashBytes: hashBytes, DocumentIdentifier: documentIdentifier));
                }
                rsLoad.MoveNext();
            }

            foreach (var affectedAccount in affected)
            {
                rsLoad.OpenRecordset("Select distinct rscard from picturerecord order by rscard", "RealEstate");
                while (!rsLoad.EndOfFile())
                {
                    var curPic = 1;
                    var card = rsLoad.Get_Fields_Int32("rscard");
                    rsSave.OpenRecordset("select * from picturerecord where mraccountnumber = " + affectedAccount + " and rscard = " + card + " order by picnum", modGlobalVariables.strREDatabase);
                    while (!rsSave.EndOfFile())
                    {
                        lblProgress.Text = "Processing Account " + affectedAccount + " Card " + card;
                        lblProgress.Refresh();
                        if (rsSave.Get_Fields_Int32("picnum") != curPic)
                        {
                            rsSave.Edit();
                            rsSave.Set_Fields("picnum", curPic);
                            rsSave.Update();
                        }
                        rsSave.MoveNext();
                        curPic += 1;
                    }

                    rsLoad.MoveNext();
                }

                
            }

            FCMessageBox.Show("Finished checking for duplicate images", MsgBoxStyle.OkOnly, "Done");
            lblProgress.Text = "";
        }
		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
        {
            RemoveDuplicates();			
		}
	}
}
