﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage1.
	/// </summary>
	partial class srptMVRPage1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMVRPage1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbllLine13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCommitmentDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine14b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine14c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine14d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtline14e = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtline14f = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtline14g = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine14a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape13 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape14 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape15 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbllLine13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCounty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCommitmentDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine14b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine14c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine14d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtline14e)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtline14f)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtline14g)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine14a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label85,
            this.txtLine13,
            this.Label84,
            this.txtLine12,
            this.txtLine11,
            this.Label80,
            this.Label81,
            this.Label82,
            this.txtLine9,
            this.txtLine8,
            this.txtLine7,
            this.Shape10,
            this.Shape8,
            this.Shape6,
            this.Shape5,
            this.Shape4,
            this.lblTitle,
            this.Label2,
            this.Label3,
            this.lblTitle2,
            this.Label7,
            this.lblYear,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Label23,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.lblYear2,
            this.lblYear3,
            this.Label33,
            this.lbllLine13,
            this.Label34,
            this.Label35,
            this.Label36,
            this.Label37,
            this.Label38,
            this.Label39,
            this.Label40,
            this.Label41,
            this.Label42,
            this.Label44,
            this.Label45,
            this.Label46,
            this.Label47,
            this.Label50,
            this.Label51,
            this.Label52,
            this.Label53,
            this.Label54,
            this.Label55,
            this.Label56,
            this.Label57,
            this.Label58,
            this.Label59,
            this.Label60,
            this.Label61,
            this.Label62,
            this.Label63,
            this.Label64,
            this.Label65,
            this.Label66,
            this.Label68,
            this.Label69,
            this.Label70,
            this.Label71,
            this.Label72,
            this.Label73,
            this.Label74,
            this.Label75,
            this.Label76,
            this.Label77,
            this.Label78,
            this.Label79,
            this.Label83,
            this.Label86,
            this.Label87,
            this.Label88,
            this.Label89,
            this.Label90,
            this.Label91,
            this.Label92,
            this.Shape9,
            this.Label94,
            this.Label95,
            this.Label96,
            this.txtCounty,
            this.txtMunicipality,
            this.txtCommitmentDate,
            this.Label97,
            this.txtLine3,
            this.txtLine4,
            this.txtLine5,
            this.txtLine6,
            this.txtLine10,
            this.txtLine14b,
            this.txtLine14c,
            this.txtLine14d,
            this.txtline14e,
            this.txtline14f,
            this.txtline14g,
            this.txtLine14a,
            this.Label98,
            this.lblYear4,
            this.Line5,
            this.Line4,
            this.Line3,
            this.Line6,
            this.Shape20,
            this.Label99,
            this.Label100,
            this.Shape1,
            this.Shape2,
            this.Shape3,
            this.Shape7,
            this.Shape12,
            this.Shape11,
            this.Shape13,
            this.Shape14,
            this.Shape15,
            this.Shape16,
            this.Shape17,
            this.Shape19});
			this.Detail.Height = 9.635417F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Label85
			// 
			this.Label85.Height = 0.1875F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 5.75F;
			this.Label85.Name = "Label85";
			this.Label85.Style = "";
			this.Label85.Text = "$";
			this.Label85.Top = 6F;
			this.Label85.Visible = false;
			this.Label85.Width = 0.125F;
			// 
			// txtLine13
			// 
			this.txtLine13.Height = 0.1875F;
			this.txtLine13.Left = 5.875F;
			this.txtLine13.Name = "txtLine13";
			this.txtLine13.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine13.Text = " ";
			this.txtLine13.Top = 6F;
			this.txtLine13.Width = 1.375F;
			// 
			// Label84
			// 
			this.Label84.Height = 0.1875F;
			this.Label84.HyperLink = null;
			this.Label84.Left = 5.75F;
			this.Label84.Name = "Label84";
			this.Label84.Style = "";
			this.Label84.Text = "$";
			this.Label84.Top = 5.375F;
			this.Label84.Visible = false;
			this.Label84.Width = 0.125F;
			// 
			// txtLine12
			// 
			this.txtLine12.Height = 0.1875F;
			this.txtLine12.Left = 5.875F;
			this.txtLine12.Name = "txtLine12";
			this.txtLine12.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine12.Text = " ";
			this.txtLine12.Top = 5.6875F;
			this.txtLine12.Width = 1.375F;
			// 
			// txtLine11
			// 
			this.txtLine11.Height = 0.1875F;
			this.txtLine11.Left = 5.875F;
			this.txtLine11.Name = "txtLine11";
			this.txtLine11.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine11.Text = " ";
			this.txtLine11.Top = 5.375F;
			this.txtLine11.Width = 1.375F;
			// 
			// Label80
			// 
			this.Label80.Height = 0.1875F;
			this.Label80.HyperLink = null;
			this.Label80.Left = 5.75F;
			this.Label80.Name = "Label80";
			this.Label80.Style = "";
			this.Label80.Text = "$";
			this.Label80.Top = 3.9375F;
			this.Label80.Visible = false;
			this.Label80.Width = 0.125F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.1875F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 5.75F;
			this.Label81.Name = "Label81";
			this.Label81.Style = "";
			this.Label81.Text = "$";
			this.Label81.Top = 4.1875F;
			this.Label81.Visible = false;
			this.Label81.Width = 0.125F;
			// 
			// Label82
			// 
			this.Label82.Height = 0.1875F;
			this.Label82.HyperLink = null;
			this.Label82.Left = 5.75F;
			this.Label82.Name = "Label82";
			this.Label82.Style = "";
			this.Label82.Text = "$";
			this.Label82.Top = 4.4375F;
			this.Label82.Visible = false;
			this.Label82.Width = 0.125F;
			// 
			// txtLine9
			// 
			this.txtLine9.Height = 0.1875F;
			this.txtLine9.Left = 5.875F;
			this.txtLine9.Name = "txtLine9";
			this.txtLine9.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine9.Text = " ";
			this.txtLine9.Top = 4.4375F;
			this.txtLine9.Width = 1.375F;
			// 
			// txtLine8
			// 
			this.txtLine8.Height = 0.1875F;
			this.txtLine8.Left = 5.875F;
			this.txtLine8.Name = "txtLine8";
			this.txtLine8.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine8.Text = " ";
			this.txtLine8.Top = 4.1875F;
			this.txtLine8.Width = 1.375F;
			// 
			// txtLine7
			// 
			this.txtLine7.Height = 0.1875F;
			this.txtLine7.Left = 5.875F;
			this.txtLine7.Name = "txtLine7";
			this.txtLine7.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine7.Text = " ";
			this.txtLine7.Top = 3.9375F;
			this.txtLine7.Width = 1.375F;
			// 
			// Shape10
			// 
			this.Shape10.Height = 0.1875F;
			this.Shape10.Left = 5.75F;
			this.Shape10.Name = "Shape10";
			this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape10.Top = 6F;
			this.Shape10.Width = 1.5625F;
			// 
			// Shape8
			// 
			this.Shape8.Height = 0.1875F;
			this.Shape8.Left = 5.75F;
			this.Shape8.Name = "Shape8";
			this.Shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape8.Top = 5.375F;
			this.Shape8.Width = 1.5625F;
			// 
			// Shape6
			// 
			this.Shape6.Height = 0.1875F;
			this.Shape6.Left = 5.75F;
			this.Shape6.Name = "Shape6";
			this.Shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape6.Top = 4.4375F;
			this.Shape6.Width = 1.5625F;
			// 
			// Shape5
			// 
			this.Shape5.Height = 0.1875F;
			this.Shape5.Left = 5.75F;
			this.Shape5.Name = "Shape5";
			this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape5.Top = 4.1875F;
			this.Shape5.Width = 1.5625F;
			// 
			// Shape4
			// 
			this.Shape4.Height = 0.1875F;
			this.Shape4.Left = 5.75F;
			this.Shape4.Name = "Shape4";
			this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape4.Top = 3.9375F;
			this.Shape4.Width = 1.5625F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1.25F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-weight: bold; text-align: center";
			this.lblTitle.Text = "MUNICIPAL VALUATION RETURN";
			this.lblTitle.Top = 0.1875F;
			this.lblTitle.Width = 5.5625F;
			// 
			// Label2
			// 
			this.Label2.Angle = 1800;
			this.Label2.Height = 1.0625F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.5F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "ddo-font-vertical: true";
			this.Label2.Text = "Municipality";
			this.Label2.Top = 0.125F;
			this.Label2.Width = 0.25F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.25F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-style: italic; text-align: center";
			this.Label3.Text = "(36 M.R.S. §383)";
			this.Label3.Top = 0.375F;
			this.Label3.Width = 5.5625F;
			// 
			// lblTitle2
			// 
			this.lblTitle2.Height = 0.1875F;
			this.lblTitle2.HyperLink = null;
			this.lblTitle2.Left = 1.25F;
			this.lblTitle2.Name = "lblTitle2";
			this.lblTitle2.Style = "font-weight: bold; text-align: center";
			this.lblTitle2.Text = "DUE DATE - NOVEMBER 1, XXXX (or within 30 days of commitment, whichever is later)" +
    "";
			this.lblTitle2.Top = 0.5625F;
			this.lblTitle2.Width = 5.5625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 9pt";
			this.Label7.Text = "3.";
			this.Label7.Top = 1.625F;
			this.Label7.Width = 0.25F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 0.375F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "";
			this.lblYear.Text = "2012";
			this.lblYear.Top = 1.625F;
			this.lblYear.Width = 0.375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.75F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 9pt";
			this.Label8.Text = "Certified Ratio. (Percentage of current just value upon which assessments are bas" +
    "ed.)";
			this.Label8.Top = 1.625F;
			this.Label8.Width = 4.75F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 8.5pt; font-style: italic; font-weight: bold; text-decoration: underli" +
    "ne";
			this.Label9.Text = "Homestead, Veterans, Blind and BETE Exemptions, Tree Growth and Farmland Values m" +
    "ust be adjusted by this percentage";
			this.Label9.Top = 1.875F;
			this.Label9.Width = 6.1875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-size: 9pt";
			this.Label10.Text = "4.";
			this.Label10.Top = 2.5625F;
			this.Label10.Width = 0.1875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 9pt";
			this.Label11.Text = "Land.    (include value of transmission, distribution lines & substations, dams a" +
    "nd power houses)";
			this.Label11.Top = 2.5625F;
			this.Label11.Width = 4.875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 9pt";
			this.Label12.Text = "5.";
			this.Label12.Top = 2.875F;
			this.Label12.Width = 0.1875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 9pt";
			this.Label13.Text = "6.";
			this.Label13.Top = 3.1875F;
			this.Label13.Width = 0.1875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.375F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 9pt";
			this.Label14.Text = "Buildings.";
			this.Label14.Top = 2.875F;
			this.Label14.Width = 4.875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 9pt";
			this.Label15.Text = "Total taxable valuation of real estate (sum of lines 4 & 5 above).";
			this.Label15.Top = 3.1875F;
			this.Label15.Width = 4.875F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.6875F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label16.Text = "(must match Municipal Tax Rate Calculation Standard Form page 10, line 1)";
			this.Label16.Top = 3.375F;
			this.Label16.Width = 4.5625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 9pt";
			this.Label17.Text = "7.";
			this.Label17.Top = 3.9375F;
			this.Label17.Width = 0.1875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 9pt";
			this.Label18.Text = "8.";
			this.Label18.Top = 4.1875F;
			this.Label18.Width = 0.1875F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 9pt";
			this.Label19.Text = "9.";
			this.Label19.Top = 4.4375F;
			this.Label19.Width = 0.1875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 9pt";
			this.Label20.Text = "10.";
			this.Label20.Top = 4.6875F;
			this.Label20.Width = 0.25F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 9pt";
			this.Label21.Text = "11.";
			this.Label21.Top = 5.375F;
			this.Label21.Width = 0.25F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 9pt";
			this.Label22.Text = "12.";
			this.Label22.Top = 5.6875F;
			this.Label22.Width = 0.25F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-size: 9pt";
			this.Label23.Text = "13.";
			this.Label23.Top = 6F;
			this.Label23.Width = 0.25F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.375F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 9pt";
			this.Label24.Text = "Production machinery and equipment.";
			this.Label24.Top = 3.9375F;
			this.Label24.Width = 4.875F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.375F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-size: 9pt";
			this.Label25.Text = "Business equipment (furniture, furnishings, and fixtures).";
			this.Label25.Top = 4.1875F;
			this.Label25.Width = 4.875F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.375F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-size: 9pt";
			this.Label26.Text = "All other personal property";
			this.Label26.Top = 4.4375F;
			this.Label26.Width = 4.875F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.375F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-size: 9pt";
			this.Label27.Text = "Total taxable valuation of personal property (sum of lines 7 through 9 above).";
			this.Label27.Top = 4.6875F;
			this.Label27.Width = 4.875F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.6875F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label28.Text = "(must match Municipal Tax Rate Calculation Standard Form  page 10, line 2)";
			this.Label28.Top = 4.875F;
			this.Label28.Width = 4.5625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0.375F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 9pt";
			this.Label29.Text = "Total taxable valuation of real estate and personal property (sum of lines 6 & 10" +
    " above).";
			this.Label29.Top = 5.375F;
			this.Label29.Width = 4.875F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0.6875F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label30.Text = "(must match Municipal Tax Rate Calculation Standard Form page 10, line 3)";
			this.Label30.Top = 5.53125F;
			this.Label30.Width = 4.5625F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0.75F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-size: 9pt";
			this.Label31.Text = "Property Tax Rate (example .01520)";
			this.Label31.Top = 5.6875F;
			this.Label31.Width = 2.25F;
			// 
			// lblYear2
			// 
			this.lblYear2.Height = 0.1875F;
			this.lblYear2.HyperLink = null;
			this.lblYear2.Left = 0.375F;
			this.lblYear2.Name = "lblYear2";
			this.lblYear2.Style = "";
			this.lblYear2.Text = "2013";
			this.lblYear2.Top = 5.6875F;
			this.lblYear2.Width = 0.375F;
			// 
			// lblYear3
			// 
			this.lblYear3.Height = 0.1875F;
			this.lblYear3.HyperLink = null;
			this.lblYear3.Left = 0.375F;
			this.lblYear3.Name = "lblYear3";
			this.lblYear3.Style = "";
			this.lblYear3.Text = "2008";
			this.lblYear3.Top = 6F;
			this.lblYear3.Width = 0.375F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0.75F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 9pt";
			this.Label33.Text = "Property Tax Levy (includes overlay and any fractional gains from rounding).";
			this.Label33.Top = 6F;
			this.Label33.Width = 4.5F;
			// 
			// lbllLine13
			// 
			this.lbllLine13.Height = 0.1875F;
			this.lbllLine13.HyperLink = null;
			this.lbllLine13.Left = 0.375F;
			this.lbllLine13.Name = "lbllLine13";
			this.lbllLine13.Style = "font-size: 8.5pt; font-style: italic; font-weight: bold";
			this.lbllLine13.Text = "Note: This is the exact amount of";
			this.lbllLine13.Top = 6.1875F;
			this.lbllLine13.Width = 1.653F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0.75F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label34.Text = "(must match Municipal Tax Rate Calculation Standard Form page 10, line 19)";
			this.Label34.Top = 6.375F;
			this.Label34.Width = 4.5F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0.938F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-size: 9pt";
			this.Label35.Text = "1.";
			this.Label35.Top = 0.875F;
			this.Label35.Width = 0.25F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 0.9375F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-size: 9pt";
			this.Label36.Text = "2.";
			this.Label36.Top = 1.1875F;
			this.Label36.Width = 0.25F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1875F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 0F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-size: 9pt";
			this.Label37.Text = "14.";
			this.Label37.Top = 7F;
			this.Label37.Width = 0.25F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1875F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 0.375F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-size: 9pt";
			this.Label38.Text = "a. Total number of $25,000 Homestead exemptions granted.";
			this.Label38.Top = 7F;
			this.Label38.Width = 4.5625F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.1875F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 0.375F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-size: 9pt";
			this.Label39.Text = "b. Total exempt value for all $25,000 Homestead exemptions granted. (Line 14a x $" +
    "25,000)";
			this.Label39.Top = 7.25F;
			this.Label39.Width = 4.8125F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1875F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 0.375F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-size: 9pt";
			this.Label40.Text = "c. Total number of properties fully exempted  (valued less than $25,000) by Homes" +
    "tead";
			this.Label40.Top = 7.5F;
			this.Label40.Width = 4.875F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1875F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 0.375F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-size: 9pt";
			this.Label41.Text = "d. Total exempt value for all properties fully exempted (valued less than $25,000" +
    ") by";
			this.Label41.Top = 7.9375F;
			this.Label41.Width = 4.5625F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1875F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 0.375F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-size: 9pt";
			this.Label42.Text = "e. Total number of Homestead exemptions granted. (sum of 14a & 14c)";
			this.Label42.Top = 8.3125F;
			this.Label42.Width = 4.5625F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1875F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 0.375F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-size: 9pt";
			this.Label44.Text = "f. Total exempt value for all Homestead exemptions granted. (sum of 14b & 14d)";
			this.Label44.Top = 8.5625F;
			this.Label44.Width = 4.5625F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1875F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 0.375F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-size: 9pt";
			this.Label45.Text = "g. Total assessed value of all homestead qualified property (land and buildings)." +
    "";
			this.Label45.Top = 9F;
			this.Label45.Width = 4.5625F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1875F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 0.78125F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-size: 8.5pt";
			this.Label46.Text = "(must match Municipal Tax Rate Calculation Standard Form page 10, line 4a)";
			this.Label46.Top = 8.71875F;
			this.Label46.Width = 4.3125F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1875F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 3.5F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-size: 9pt";
			this.Label47.Text = "- 1 -";
			this.Label47.Top = 9.375F;
			this.Label47.Width = 0.3125F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.1875F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 5.5F;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-size: 9pt";
			this.Label50.Text = "4";
			this.Label50.Top = 2.5625F;
			this.Label50.Width = 0.1875F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1875F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 5.5F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-size: 9pt";
			this.Label51.Text = "5";
			this.Label51.Top = 2.875F;
			this.Label51.Width = 0.1875F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1875F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 5.5F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-size: 9pt";
			this.Label52.Text = "6";
			this.Label52.Top = 3.1875F;
			this.Label52.Width = 0.1875F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1875F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 5.5F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-size: 9pt";
			this.Label53.Text = "7";
			this.Label53.Top = 3.9375F;
			this.Label53.Width = 0.1875F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.1875F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 5.5F;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-size: 9pt";
			this.Label54.Text = "8";
			this.Label54.Top = 4.1875F;
			this.Label54.Width = 0.1875F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.1875F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 5.5F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-size: 9pt";
			this.Label55.Text = "9";
			this.Label55.Top = 4.4375F;
			this.Label55.Width = 0.1875F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.1875F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 5.5F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-size: 9pt";
			this.Label56.Text = "10";
			this.Label56.Top = 4.6875F;
			this.Label56.Width = 0.25F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1875F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 5.5F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-size: 9pt";
			this.Label57.Text = "11";
			this.Label57.Top = 5.375F;
			this.Label57.Width = 0.25F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 5.5F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-size: 9pt";
			this.Label58.Text = "12";
			this.Label58.Top = 5.6875F;
			this.Label58.Width = 0.25F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.1875F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 5.5F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "font-size: 9pt";
			this.Label59.Text = "13";
			this.Label59.Top = 6F;
			this.Label59.Width = 0.25F;
			// 
			// Label60
			// 
			this.Label60.Height = 0.1875F;
			this.Label60.HyperLink = null;
			this.Label60.Left = 5.5F;
			this.Label60.Name = "Label60";
			this.Label60.Style = "font-size: 9pt";
			this.Label60.Text = "14a";
			this.Label60.Top = 7F;
			this.Label60.Width = 0.25F;
			// 
			// Label61
			// 
			this.Label61.Height = 0.1875F;
			this.Label61.HyperLink = null;
			this.Label61.Left = 5.5F;
			this.Label61.Name = "Label61";
			this.Label61.Style = "font-size: 9pt";
			this.Label61.Text = "14b";
			this.Label61.Top = 7.25F;
			this.Label61.Width = 0.25F;
			// 
			// Label62
			// 
			this.Label62.Height = 0.1875F;
			this.Label62.HyperLink = null;
			this.Label62.Left = 5.5F;
			this.Label62.Name = "Label62";
			this.Label62.Style = "font-size: 9pt";
			this.Label62.Text = "14c";
			this.Label62.Top = 7.5F;
			this.Label62.Width = 0.25F;
			// 
			// Label63
			// 
			this.Label63.Height = 0.1875F;
			this.Label63.HyperLink = null;
			this.Label63.Left = 5.5F;
			this.Label63.Name = "Label63";
			this.Label63.Style = "font-size: 9pt";
			this.Label63.Text = "14d";
			this.Label63.Top = 7.9375F;
			this.Label63.Width = 0.25F;
			// 
			// Label64
			// 
			this.Label64.Height = 0.1875F;
			this.Label64.HyperLink = null;
			this.Label64.Left = 5.5F;
			this.Label64.Name = "Label64";
			this.Label64.Style = "font-size: 9pt";
			this.Label64.Text = "14e";
			this.Label64.Top = 8.3125F;
			this.Label64.Width = 0.25F;
			// 
			// Label65
			// 
			this.Label65.Height = 0.1875F;
			this.Label65.HyperLink = null;
			this.Label65.Left = 5.5F;
			this.Label65.Name = "Label65";
			this.Label65.Style = "font-size: 9pt";
			this.Label65.Text = "14f";
			this.Label65.Top = 8.5625F;
			this.Label65.Width = 0.25F;
			// 
			// Label66
			// 
			this.Label66.Height = 0.1875F;
			this.Label66.HyperLink = null;
			this.Label66.Left = 5.5F;
			this.Label66.Name = "Label66";
			this.Label66.Style = "font-size: 9pt";
			this.Label66.Text = "14g";
			this.Label66.Top = 9F;
			this.Label66.Width = 0.25F;
			// 
			// Label68
			// 
			this.Label68.Height = 0.1875F;
			this.Label68.HyperLink = null;
			this.Label68.Left = 1.25F;
			this.Label68.Name = "Label68";
			this.Label68.Style = "font-size: 9pt";
			this.Label68.Text = "County:";
			this.Label68.Top = 0.875F;
			this.Label68.Width = 0.5625F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 1.25F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-size: 9pt";
			this.Label69.Text = "Municipality:";
			this.Label69.Top = 1.1875F;
			this.Label69.Width = 0.9375F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.1875F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 2.25F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "font-weight: bold";
			this.Label70.Text = "TAXABLE";
			this.Label70.Top = 2.125F;
			this.Label70.Width = 0.875F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.1875F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 3F;
			this.Label71.Name = "Label71";
			this.Label71.Style = "font-weight: bold";
			this.Label71.Text = "VALUATION OF REAL ESTATE";
			this.Label71.Top = 2.125F;
			this.Label71.Width = 2.4375F;
			// 
			// Label72
			// 
			this.Label72.Height = 0.1875F;
			this.Label72.HyperLink = null;
			this.Label72.Left = 2F;
			this.Label72.Name = "Label72";
			this.Label72.Style = "font-weight: bold";
			this.Label72.Text = "TAXABLE";
			this.Label72.Top = 3.5625F;
			this.Label72.Width = 0.875F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.1875F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 2.75F;
			this.Label73.Name = "Label73";
			this.Label73.Style = "font-weight: bold";
			this.Label73.Text = "VALUATION OF PERSONAL PROPERTY";
			this.Label73.Top = 3.5625F;
			this.Label73.Width = 2.9375F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1875F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 2.125F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-weight: bold";
			this.Label74.Text = "OTHER TAX INFORMATION";
			this.Label74.Top = 5.0625F;
			this.Label74.Width = 2.9375F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.1875F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 2F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "font-weight: bold";
			this.Label75.Text = "HOMESTEAD EXEMPTION REIMBURSEMENT CLAIM";
			this.Label75.Top = 6.5625F;
			this.Label75.Width = 3.6875F;
			// 
			// Label76
			// 
			this.Label76.Height = 0.1875F;
			this.Label76.HyperLink = null;
			this.Label76.Left = 1.5F;
			this.Label76.Name = "Label76";
			this.Label76.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label76.Text = "Homestead Exemptions must be adjusted by the municipality\'s certified ratio";
			this.Label76.Top = 6.75F;
			this.Label76.Width = 5.3125F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.1875F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 5.75F;
			this.Label77.Name = "Label77";
			this.Label77.Style = "";
			this.Label77.Text = "$";
			this.Label77.Top = 2.5625F;
			this.Label77.Visible = false;
			this.Label77.Width = 0.125F;
			// 
			// Label78
			// 
			this.Label78.Height = 0.1875F;
			this.Label78.HyperLink = null;
			this.Label78.Left = 5.75F;
			this.Label78.Name = "Label78";
			this.Label78.Style = "";
			this.Label78.Text = "$";
			this.Label78.Top = 2.875F;
			this.Label78.Visible = false;
			this.Label78.Width = 0.125F;
			// 
			// Label79
			// 
			this.Label79.Height = 0.1875F;
			this.Label79.HyperLink = null;
			this.Label79.Left = 5.75F;
			this.Label79.Name = "Label79";
			this.Label79.Style = "";
			this.Label79.Text = "$";
			this.Label79.Top = 3.1875F;
			this.Label79.Visible = false;
			this.Label79.Width = 0.125F;
			// 
			// Label83
			// 
			this.Label83.Height = 0.1875F;
			this.Label83.HyperLink = null;
			this.Label83.Left = 5.75F;
			this.Label83.Name = "Label83";
			this.Label83.Style = "";
			this.Label83.Text = "$";
			this.Label83.Top = 4.6875F;
			this.Label83.Visible = false;
			this.Label83.Width = 0.125F;
			// 
			// Label86
			// 
			this.Label86.Height = 0.1875F;
			this.Label86.HyperLink = null;
			this.Label86.Left = 5.75F;
			this.Label86.Name = "Label86";
			this.Label86.Style = "";
			this.Label86.Text = "#";
			this.Label86.Top = 7F;
			this.Label86.Visible = false;
			this.Label86.Width = 0.125F;
			// 
			// Label87
			// 
			this.Label87.Height = 0.1875F;
			this.Label87.HyperLink = null;
			this.Label87.Left = 5.75F;
			this.Label87.Name = "Label87";
			this.Label87.Style = "";
			this.Label87.Text = "$";
			this.Label87.Top = 7.25F;
			this.Label87.Visible = false;
			this.Label87.Width = 0.125F;
			// 
			// Label88
			// 
			this.Label88.Height = 0.1875F;
			this.Label88.HyperLink = null;
			this.Label88.Left = 5.75F;
			this.Label88.Name = "Label88";
			this.Label88.Style = "";
			this.Label88.Text = "#";
			this.Label88.Top = 7.5F;
			this.Label88.Visible = false;
			this.Label88.Width = 0.125F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1875F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 5.75F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "";
			this.Label89.Text = "$";
			this.Label89.Top = 7.9375F;
			this.Label89.Visible = false;
			this.Label89.Width = 0.125F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.1875F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 5.75F;
			this.Label90.Name = "Label90";
			this.Label90.Style = "";
			this.Label90.Text = "#";
			this.Label90.Top = 8.3125F;
			this.Label90.Visible = false;
			this.Label90.Width = 0.125F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.1875F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 5.75F;
			this.Label91.Name = "Label91";
			this.Label91.Style = "";
			this.Label91.Text = "$";
			this.Label91.Top = 8.5625F;
			this.Label91.Visible = false;
			this.Label91.Width = 0.125F;
			// 
			// Label92
			// 
			this.Label92.Height = 0.1875F;
			this.Label92.HyperLink = null;
			this.Label92.Left = 5.75F;
			this.Label92.Name = "Label92";
			this.Label92.Style = "";
			this.Label92.Text = "$";
			this.Label92.Top = 9F;
			this.Label92.Visible = false;
			this.Label92.Width = 0.125F;
			// 
			// Shape9
			// 
			this.Shape9.Height = 0.1875F;
			this.Shape9.Left = 5.75F;
			this.Shape9.Name = "Shape9";
			this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape9.Top = 5.6875F;
			this.Shape9.Width = 1.5625F;
			// 
			// Label94
			// 
			this.Label94.Height = 0.1875F;
			this.Label94.HyperLink = null;
			this.Label94.Left = 0.8125F;
			this.Label94.Name = "Label94";
			this.Label94.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label94.Text = "(Exclude exempt valuations of all categories)";
			this.Label94.Top = 2.3125F;
			this.Label94.Width = 5.5625F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.1875F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 1.9375F;
			this.Label95.Name = "Label95";
			this.Label95.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label95.Text = "(Exclude Exempt Valuations of All Categories)";
			this.Label95.Top = 3.75F;
			this.Label95.Width = 3.625F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.1875F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 4.25F;
			this.Label96.Name = "Label96";
			this.Label96.Style = "font-size: 10pt; font-weight: bold; text-align: left";
			this.Label96.Text = "Commitment Date:";
			this.Label96.Top = 0.875F;
			this.Label96.Width = 1.25F;
			// 
			// txtCounty
			// 
			this.txtCounty.Height = 0.1875F;
			this.txtCounty.Left = 1.8125F;
			this.txtCounty.Name = "txtCounty";
			this.txtCounty.Tag = "2||";
			this.txtCounty.Text = null;
			this.txtCounty.Top = 0.875F;
			this.txtCounty.Width = 1.6875F;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.Height = 0.1875F;
			this.txtMunicipality.Left = 2.125F;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Text = null;
			this.txtMunicipality.Top = 1.1875F;
			this.txtMunicipality.Width = 3.375F;
			// 
			// txtCommitmentDate
			// 
			this.txtCommitmentDate.Height = 0.1875F;
			this.txtCommitmentDate.Left = 5.625F;
			this.txtCommitmentDate.Name = "txtCommitmentDate";
			this.txtCommitmentDate.Style = "text-align: right";
			this.txtCommitmentDate.Text = null;
			this.txtCommitmentDate.Top = 0.875F;
			this.txtCommitmentDate.Width = 1.4375F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.1875F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 5.5F;
			this.Label97.Name = "Label97";
			this.Label97.Style = "font-size: 9pt";
			this.Label97.Text = "3";
			this.Label97.Top = 1.625F;
			this.Label97.Width = 0.1875F;
			// 
			// txtLine3
			// 
			this.txtLine3.Height = 0.1875F;
			this.txtLine3.Left = 5.875F;
			this.txtLine3.Name = "txtLine3";
			this.txtLine3.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine3.Text = " ";
			this.txtLine3.Top = 1.625F;
			this.txtLine3.Width = 1.375F;
			// 
			// txtLine4
			// 
			this.txtLine4.Height = 0.1875F;
			this.txtLine4.Left = 5.875F;
			this.txtLine4.Name = "txtLine4";
			this.txtLine4.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine4.Text = " ";
			this.txtLine4.Top = 2.5625F;
			this.txtLine4.Width = 1.375F;
			// 
			// txtLine5
			// 
			this.txtLine5.Height = 0.1875F;
			this.txtLine5.Left = 5.875F;
			this.txtLine5.Name = "txtLine5";
			this.txtLine5.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine5.Text = " ";
			this.txtLine5.Top = 2.875F;
			this.txtLine5.Width = 1.375F;
			// 
			// txtLine6
			// 
			this.txtLine6.Height = 0.1875F;
			this.txtLine6.Left = 5.875F;
			this.txtLine6.Name = "txtLine6";
			this.txtLine6.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine6.Text = " ";
			this.txtLine6.Top = 3.1875F;
			this.txtLine6.Width = 1.375F;
			// 
			// txtLine10
			// 
			this.txtLine10.Height = 0.1875F;
			this.txtLine10.Left = 5.875F;
			this.txtLine10.Name = "txtLine10";
			this.txtLine10.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine10.Text = " ";
			this.txtLine10.Top = 4.6875F;
			this.txtLine10.Width = 1.375F;
			// 
			// txtLine14b
			// 
			this.txtLine14b.Height = 0.1875F;
			this.txtLine14b.Left = 5.875F;
			this.txtLine14b.Name = "txtLine14b";
			this.txtLine14b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine14b.Text = " ";
			this.txtLine14b.Top = 7.25F;
			this.txtLine14b.Width = 1.375F;
			// 
			// txtLine14c
			// 
			this.txtLine14c.Height = 0.1875F;
			this.txtLine14c.Left = 5.875F;
			this.txtLine14c.Name = "txtLine14c";
			this.txtLine14c.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine14c.Text = " ";
			this.txtLine14c.Top = 7.5F;
			this.txtLine14c.Width = 1.375F;
			// 
			// txtLine14d
			// 
			this.txtLine14d.Height = 0.1875F;
			this.txtLine14d.Left = 5.875F;
			this.txtLine14d.Name = "txtLine14d";
			this.txtLine14d.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine14d.Text = " ";
			this.txtLine14d.Top = 7.9375F;
			this.txtLine14d.Width = 1.375F;
			// 
			// txtline14e
			// 
			this.txtline14e.Height = 0.1875F;
			this.txtline14e.Left = 5.875F;
			this.txtline14e.Name = "txtline14e";
			this.txtline14e.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtline14e.Text = " ";
			this.txtline14e.Top = 8.3125F;
			this.txtline14e.Width = 1.375F;
			// 
			// txtline14f
			// 
			this.txtline14f.Height = 0.1875F;
			this.txtline14f.Left = 5.875F;
			this.txtline14f.Name = "txtline14f";
			this.txtline14f.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtline14f.Text = " ";
			this.txtline14f.Top = 8.5625F;
			this.txtline14f.Width = 1.375F;
			// 
			// txtline14g
			// 
			this.txtline14g.Height = 0.1875F;
			this.txtline14g.Left = 5.875F;
			this.txtline14g.Name = "txtline14g";
			this.txtline14g.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtline14g.Text = " ";
			this.txtline14g.Top = 9F;
			this.txtline14g.Width = 1.375F;
			// 
			// txtLine14a
			// 
			this.txtLine14a.Height = 0.1875F;
			this.txtLine14a.Left = 5.875F;
			this.txtLine14a.Name = "txtLine14a";
			this.txtLine14a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine14a.Text = " ";
			this.txtLine14a.Top = 7F;
			this.txtLine14a.Width = 1.375F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.1875F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 2.319666F;
			this.Label98.Name = "Label98";
			this.Label98.Style = "font-size: 8.5pt; font-style: italic; font-weight: bold";
			this.Label98.Text = "tax actually committed to the Collector";
			this.Label98.Top = 6.187F;
			this.Label98.Width = 2.930335F;
			// 
			// lblYear4
			// 
			this.lblYear4.Height = 0.1875F;
			this.lblYear4.HyperLink = null;
			this.lblYear4.Left = 2.028F;
			this.lblYear4.Name = "lblYear4";
			this.lblYear4.Style = "font-size: 8.5pt; font-style: italic; font-weight: bold";
			this.lblYear4.Text = " 2008";
			this.lblYear4.Top = 6.187F;
			this.lblYear4.Width = 0.375F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 3F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 5.0625F;
			this.Line5.Width = 7.25F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 7.25F;
			this.Line5.Y1 = 5.0625F;
			this.Line5.Y2 = 5.0625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 3.5625F;
			this.Line4.Width = 7.25F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.25F;
			this.Line4.Y1 = 3.5625F;
			this.Line4.Y2 = 3.5625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2.125F;
			this.Line3.Width = 7.25F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.25F;
			this.Line3.Y1 = 2.125F;
			this.Line3.Y2 = 2.125F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 0F;
			this.Line6.LineWeight = 3F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 6.5625F;
			this.Line6.Width = 7.25F;
			this.Line6.X1 = 0F;
			this.Line6.X2 = 7.25F;
			this.Line6.Y1 = 6.5625F;
			this.Line6.Y2 = 6.5625F;
			// 
			// Shape20
			// 
			this.Shape20.Height = 1.354167F;
			this.Shape20.Left = 0.0625F;
			this.Shape20.Name = "Shape20";
			this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape20.Top = 0.07291666F;
			this.Shape20.Width = 0.3125F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.1875F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 0.5138889F;
			this.Label99.Name = "Label99";
			this.Label99.Style = "font-size: 9pt";
			this.Label99.Text = "Homestead exemptions granted";
			this.Label99.Top = 8.125F;
			this.Label99.Width = 4.5625F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.1875F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 0.5138889F;
			this.Label100.Name = "Label100";
			this.Label100.Style = "font-size: 9pt";
			this.Label100.Text = "exemptions granted";
			this.Label100.Top = 7.6875F;
			this.Label100.Width = 4.5625F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 0.1875F;
			this.Shape1.Left = 5.75F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 2.5625F;
			this.Shape1.Width = 1.5625F;
			// 
			// Shape2
			// 
			this.Shape2.Height = 0.1875F;
			this.Shape2.Left = 5.75F;
			this.Shape2.Name = "Shape2";
			this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape2.Top = 2.875F;
			this.Shape2.Width = 1.5625F;
			// 
			// Shape3
			// 
			this.Shape3.Height = 0.1875F;
			this.Shape3.Left = 5.75F;
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 3.1875F;
			this.Shape3.Width = 1.5625F;
			// 
			// Shape7
			// 
			this.Shape7.Height = 0.1875F;
			this.Shape7.Left = 5.75F;
			this.Shape7.Name = "Shape7";
			this.Shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape7.Top = 4.6875F;
			this.Shape7.Width = 1.5625F;
			// 
			// Shape12
			// 
			this.Shape12.Height = 0.1875F;
			this.Shape12.Left = 5.75F;
			this.Shape12.Name = "Shape12";
			this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape12.Top = 7.25F;
			this.Shape12.Width = 1.5625F;
			// 
			// Shape11
			// 
			this.Shape11.Height = 0.1875F;
			this.Shape11.Left = 5.75F;
			this.Shape11.Name = "Shape11";
			this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape11.Top = 7F;
			this.Shape11.Width = 1.5625F;
			// 
			// Shape13
			// 
			this.Shape13.Height = 0.1875F;
			this.Shape13.Left = 5.75F;
			this.Shape13.Name = "Shape13";
			this.Shape13.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape13.Top = 7.5F;
			this.Shape13.Width = 1.5625F;
			// 
			// Shape14
			// 
			this.Shape14.Height = 0.1875F;
			this.Shape14.Left = 5.75F;
			this.Shape14.Name = "Shape14";
			this.Shape14.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape14.Top = 7.9375F;
			this.Shape14.Width = 1.5625F;
			// 
			// Shape15
			// 
			this.Shape15.Height = 0.1875F;
			this.Shape15.Left = 5.75F;
			this.Shape15.Name = "Shape15";
			this.Shape15.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape15.Top = 8.3125F;
			this.Shape15.Width = 1.5625F;
			// 
			// Shape16
			// 
			this.Shape16.Height = 0.1875F;
			this.Shape16.Left = 5.75F;
			this.Shape16.Name = "Shape16";
			this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape16.Top = 8.5625F;
			this.Shape16.Width = 1.5625F;
			// 
			// Shape17
			// 
			this.Shape17.Height = 0.1875F;
			this.Shape17.Left = 5.75F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 9F;
			this.Shape17.Width = 1.5625F;
			// 
			// Shape19
			// 
			this.Shape19.Height = 0.1875F;
			this.Shape19.Left = 5.75F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 1.625F;
			this.Shape19.Width = 1.5625F;
			// 
			// srptMVRPage1
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.40625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbllLine13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCounty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCommitmentDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine14b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine14c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine14d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtline14e)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtline14f)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtline14g)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine14a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape8;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape6;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbllLine13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label65;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label79;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCounty;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCommitmentDate;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine14b;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine14c;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine14d;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtline14e;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtline14f;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtline14g;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine14a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape7;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape13;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape14;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape15;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
	}
}
