﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptExtractTotals.
	/// </summary>
	public partial class srptExtractTotals : FCSectionReport
	{
		public static srptExtractTotals InstancePtr
		{
			get
			{
				return (srptExtractTotals)Sys.GetInstance(typeof(srptExtractTotals));
			}
		}

		protected srptExtractTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsExtract?.Dispose();
				clsGroup?.Dispose();
                clsExtract = null;
                clsGroup = null;
            }
			base.Dispose(disposing);
		}

		public srptExtractTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptExtractTotals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsExtract = new clsDRWrapper();
		clsDRWrapper clsGroup = new clsDRWrapper();
		bool boolTotals;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			boolTotals = false;
			while (!boolTotals)
			{
				if (clsExtract.EndOfFile())
					break;
				// Call clsGroup.OpenRecordset("select * from reporttable where reportnumber = " & clsExtract.Fields("reportnumber") & " and torp = 'T'", strREDatabase)
				clsGroup.OpenRecordset("select * from reporttable where reportnumber = " + clsExtract.Get_Fields_Int32("reportnumber"), modGlobalVariables.strREDatabase);
				if (clsGroup.EndOfFile())
				{
					clsExtract.MoveNext();
				}
				else
				{
					boolTotals = true;
				}
			}
			eArgs.EOF = clsExtract.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			SubReport1.Report = new srptGroupTotal();
			clsExtract.OpenRecordset("select * from extract INNER JOIN LABELACCOUNTS ON (extract.GROUPnumber = labelaccounts.accountnumber)and (extract.userid = labelaccounts.userid) where extract.userid = " + FCConvert.ToString(lngUID) + " and reportnumber > 0 order by groupnumber", modGlobalVariables.strREDatabase);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			//MDIParent.InstancePtr.Show();
			//Application.DoEvents();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsExtract.EndOfFile())
			{
				SubReport1.Report.UserData = clsExtract.Get_Fields_Int32("reportnumber");
				clsExtract.MoveNext();
			}
		}

		
	}
}
