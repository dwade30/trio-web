﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cCommercialTrendController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cCommercialTrend GetCommercialTrend(int lngId)
		{
			cCommercialTrend GetCommercialTrend = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCommercialTrend commTrend = null;
				rsLoad.OpenRecordset("select * from CommercialTrends where id = " + FCConvert.ToString(lngId), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					commTrend = new cCommercialTrend();
					// TODO Get_Fields: Field [ConstructionClassID] not found!! (maybe it is an alias?)
					commTrend.ConstructionClassID = FCConvert.ToInt32(rsLoad.Get_Fields("ConstructionClassID"));
					commTrend.Multiplier = rsLoad.Get_Fields_Int32("Multiplier");
					// TODO Get_Fields: Field [SectionID] not found!! (maybe it is an alias?)
					commTrend.SectionID = FCConvert.ToInt32(rsLoad.Get_Fields("SectionID"));
					commTrend.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					commTrend.IsUpdated = false;
				}
				GetCommercialTrend = commTrend;
				return GetCommercialTrend;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialTrend;
		}

		public cGenericCollection GetCommercialTrends()
		{
			cGenericCollection GetCommercialTrends = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialTrend commTrend;
				rs.OpenRecordset("select * from CommercialTrends order by SectionID,ConstructionClassID", "RealEstate");
				while (!rs.EndOfFile())
				{
					commTrend = new cCommercialTrend();
					// TODO Get_Fields: Field [ConstructionClassID] not found!! (maybe it is an alias?)
					commTrend.ConstructionClassID = FCConvert.ToInt32(rs.Get_Fields("ConstructionClassID"));
					commTrend.Multiplier = rs.Get_Fields_Int32("Multiplier");
					// TODO Get_Fields: Field [SectionID] not found!! (maybe it is an alias?)
					commTrend.SectionID = FCConvert.ToInt32(rs.Get_Fields("SectionID"));
					commTrend.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					commTrend.IsUpdated = false;
					gColl.AddItem(commTrend);
					rs.MoveNext();
				}
				GetCommercialTrends = gColl;
				return GetCommercialTrends;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialTrends;
		}

		public void DeleteCommercialTrend(int lngId)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialTrends where id = " + FCConvert.ToString(lngId), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveCommercialTrend(ref cCommercialTrend commTrend)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (commTrend.IsDeleted)
				{
					DeleteCommercialTrend(commTrend.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commercialtrends where id = " + commTrend.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (commTrend.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Commercial trend record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("ConstructionClassID", commTrend.ConstructionClassID);
				rsSave.Set_Fields("Multiplier", commTrend.Multiplier);
				rsSave.Set_Fields("SectionID", commTrend.SectionID);
				rsSave.Update();
				commTrend.ID = rsSave.Get_Fields_Int32("ID");
				commTrend.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void DeleteCommercialTrends()
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialTrends", "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
