﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;

namespace TWRE0000
{
	public class modProperty
	{
		// vbPorter upgrade warning: intSchedule As short	OnWriteFCConvert.ToInt32(
		public static void FillExponentArrays(ref int intSchedule)
		{
			// gets the exponents for the given schedule and fills the array
			int x;
			clsDRWrapper clsLoad = new clsDRWrapper();
			// check if we need to fill this or not
			if (intSchedule == Statics.gintCurrentLandSchedule)
				return;
			clsLoad.OpenRecordset("select max(code) as maxcode from landtype", modGlobalVariables.strREDatabase);
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
				if (Conversion.Val(clsLoad.Get_Fields("maxcode")) > 0)
				{
					// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
					Statics.LEXP = new double[FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("maxcode"))) + 1];
					// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
					Statics.LEX2 = new double[FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("maxcode"))) + 1];
				}
				else
				{
					Statics.LEXP = new double[99 + 1];
					Statics.LEX2 = new double[99 + 1];
				}
			}
			else
			{
				Statics.LEXP = new double[99 + 1];
				Statics.LEX2 = new double[99 + 1];
			}
			Statics.WidthExponentsOne.Clear();
			Statics.WidthExponentsTwo.Clear();
			clsLoad.OpenRecordset("select * from landschedule where schedule = " + FCConvert.ToString(intSchedule) + " and code > 0 order by code", modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				if (Conversion.Val(clsLoad.Get_Fields("code")) <= Information.UBound(Statics.LEXP, 1))
				{
					//FC:FINAL:MSH - i.issue #1059: incorrect converting from double to int
					//LEXP[Conversion.Val(clsLoad.Get_Fields("code"))] = Conversion.Val(clsLoad.Get_Fields("exponent1")));
					//LEX2[Conversion.Val(clsLoad.Get_Fields("code"))] = Conversion.Val(clsLoad.Get_Fields("exponent2")));
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					Statics.LEXP[FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("code")))] = Conversion.Val(clsLoad.Get_Fields_Double("exponent1"));
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					Statics.LEX2[FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("code")))] = Conversion.Val(clsLoad.Get_Fields_Double("exponent2"));
				}
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				Statics.WidthExponentsOne.Add(Conversion.Val(clsLoad.Get_Fields("code")), Conversion.Val(clsLoad.Get_Fields_Double("WidthExponent1")));
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				Statics.WidthExponentsTwo.Add(Conversion.Val(clsLoad.Get_Fields("Code")), Conversion.Val(clsLoad.Get_Fields_Double("WidthExponent2")));
				clsLoad.MoveNext();
			}
			Statics.gintCurrentLandSchedule = intSchedule;
		}

		public static void FillArrays()
		{
			// need to open a table before making assignments to LFRA....
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from landtype ORDER BY code", modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				Statics.LFRA[FCConvert.ToInt32(clsLoad.Get_Fields("code")) - 10] = Conversion.Val(clsLoad.Get_Fields_Double("factor")) * 100;
				clsLoad.MoveNext();
			}
		}

		public static void Speed_PROPERTY_DATA()
		{
			Statics.dblAcreage = 0;
			for (int i = 0; i < modSpeedCalc.Statics.CalcPropertyIndex + 1; i++)
			{
                //FC:FINAL:SBE - #3369 - do not reinitialize the same record because current values will be lost
                if (modSpeedCalc.Statics.CalcPropertyList[i].LandType == null)
                {
                    modSpeedCalc.Statics.CalcPropertyList[i] = new modGlobalVariables.CalcPropertyType(0);
                }
			}
			if (modGlobalVariables.Statics.intCurrentCard > 1)
			{
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland1type") + "") == 0 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland2type") + "") == 0 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland3type") + "") == 0 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland4type") + "") == 0 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("PILAND5TYPE")) == 0 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland6type") + "") == 0 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland7type") + "") == 0)
				{
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood") + "") > 0 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pizone") + "") > 0)
					{
						modSpeedCalc.SpeedPrintProperty();
					}
					goto REAS03_5001_TAG;
				}
			}
			modSpeedCalc.SpeedPrintProperty();
			// Type
			Statics.intLandType[1] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland1type") + "")));
			Statics.intLandType[2] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland2type") + "")));
			Statics.intLandType[3] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland3type") + "")));
			Statics.intLandType[4] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland4type") + "")));
			Statics.intLandType[5] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("PILAND5TYPE") + "")));
			Statics.intLandType[6] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland6type") + "")));
			Statics.intLandType[7] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland7type") + "")));
			// Units
			Statics.strLandUnit[1] = modDataTypes.Statics.MR.Get_Fields_String("piland1unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland1unitsb") + "";
			Statics.strLandUnit[2] = modDataTypes.Statics.MR.Get_Fields_String("piland2unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland2unitsb") + "";
			Statics.strLandUnit[3] = modDataTypes.Statics.MR.Get_Fields_String("piland3unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland3unitsb") + "";
			Statics.strLandUnit[4] = modDataTypes.Statics.MR.Get_Fields_String("piland4unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland4unitsb") + "";
			Statics.strLandUnit[5] = modDataTypes.Statics.MR.Get_Fields_String("piland5unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland5unitsb") + "";
			Statics.strLandUnit[6] = modDataTypes.Statics.MR.Get_Fields_String("piland6unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland6unitsb") + "";
			Statics.strLandUnit[7] = modDataTypes.Statics.MR.Get_Fields_String("piland7unitsa") + modDataTypes.Statics.MR.Get_Fields_String("piland7unitsb") + "";
			// Influence Factor
			Statics.intLandInfl[1] = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piland1inf") + "");
			Statics.intLandInfl[2] = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piland2inf") + "");
			Statics.intLandInfl[3] = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piland3inf") + "");
			Statics.intLandInfl[4] = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piland4inf") + "");
			Statics.intLandInfl[5] = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piland5inf") + "");
			Statics.intLandInfl[6] = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piland6inf") + "");
			Statics.intLandInfl[7] = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piland7inf") + "");
			// Influence Code
			Statics.intLandCode[1] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland1infcode") + "")));
			Statics.intLandCode[2] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland2infcode") + "")));
			Statics.intLandCode[3] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland3infcode") + "")));
			Statics.intLandCode[4] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland4infcode") + "")));
			Statics.intLandCode[5] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland5infcode") + "")));
			Statics.intLandCode[6] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland6infcode") + "")));
			Statics.intLandCode[7] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland7infcode") + "")));
			// 
			int II;
			for (II = 1; II <= 7; II++)
			{
				if (Statics.intLandType[II] > 0)
				{
					Statics.intLandType[II] -= 10;
					Statics.WORK1 = Statics.intLandType[II];
					// - 10
					Statics.WORKA1 = Statics.strLandUnit[II];
					Statics.dblAcreage += FCConvert.ToDouble(Statics.WORKA1);
					modREASValuations.Statics.WORKA2 = FCConvert.ToString(Statics.intLandInfl[II]);
					Statics.WORKA3 = Statics.intLandCode[II];
					Statics.Row = II;
					speed_display_land(ref II);
				}
				if (II == 1)
				{
					if (Statics.FirstLandFactor == "Y")
					{
						Statics.WORK1 = 0;
						speed_display_land(ref II);
					}
				}
			}
			// II
			// With frmNewValuationReport.PropertyGrid
			// .Rows = .Rows + 1
			// .MergeCells = flexMergeSpill
			string tstring;
			// a temporary string
			tstring = Conversion.Str(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piacres"))));
			// .TextMatrix(.Rows - 1, 0) = "Total Acres  " & tstring
			modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].Acres = "Total Acres  " + tstring;
			Statics.dblAcreage = Conversion.Val(tstring);
			Statics.AccountAcreage += Statics.dblAcreage;
			if (modGlobalVariables.Statics.AcreOption == 1 && Statics.dblAcreage != 0)
			{
				Statics.Perval = modREASValuations.Statics.LandAcreValue / Statics.dblAcreage;
				// .TextMatrix(.Rows - 1, 2) = Format(Perval#, "#,##0.00") & " Per Acre"
				modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].PerAcreVal = Strings.Format(Statics.Perval, "#,##0.00") + " Per Acre";
			}
			// .TextMatrix(.Rows - 1, 5) = "Land Total"
			// .TextMatrix(.Rows - 1, 6) = Format(LandTotal(gintCardNumber), "#,##0")
			modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].TotalLandValue = Strings.Format(Statics.LandTotal[modGNBas.Statics.gintCardNumber], "#,##0");
			// End With
			REAS03_5001_TAG:
			;
		}
		// vbPorter upgrade warning: intII As short	OnWriteFCConvert.ToInt32(
		private static void speed_display_land(ref int intII)
		{
			int intHoldType;
			// vbPorter upgrade warning: strUnits1 As string	OnWrite(string, double)
			string strUnits1;
			string strUnits2;
			// Dim clsTemp As New clsDRWrapper
			int intLType;
			string strAcreType = "";
			// With frmNewValuationReport.PropertyGrid(intCurrentCard - 1)
			// .Rows = .Rows + 1
			// strUnits1 = Mid(strLandUnit(intII), 1, 3)
			strUnits1 = Strings.Mid(Statics.strLandUnit[intII], 1, Statics.strLandUnit[intII].Length - 3);
			// strUnits2 = Mid(strLandUnit(intII), 4, 3)
			strUnits2 = Strings.Right(Statics.strLandUnit[intII], 3);
			intHoldType = Statics.intLandType[intII];
			if (Statics.intLandType[intII] > 0)
				modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandType[intII] = Statics.intLandType[intII] + 10;
			if (Statics.intLandType[intII] == 89)
				goto REAS03_5101_TAG;
			if (modGlobalVariables.Statics.LandTypes.FindCode(Statics.intLandType[intII] + 10))
			{
				switch (modGlobalVariables.Statics.LandTypes.UnitsType)
				{
					case modREConstants.CNSTLANDTYPEFRONTFOOT:
						{
							modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandUnits[intII] = strUnits1 + " X " + strUnits2;
							modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandMethod[intII] = "Front Foot";
							break;
						}
					case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
						{
							modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandUnits[intII] = strUnits1 + " X " + strUnits2;
							modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandMethod[intII] = "FF Width";
							break;
						}
					case modREConstants.CNSTLANDTYPESQUAREFEET:
						{
							strUnits1 = FCConvert.ToString(Conversion.Val(strUnits1) * 1000 + Conversion.Val(strUnits2));
							modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandUnits[intII] = Strings.Format(strUnits1, "###,###");
							modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandMethod[intII] = "SQFT";
							break;
						}
					case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
					case modREConstants.CNSTLANDTYPEACRES:
					case modREConstants.CNSTLANDTYPESITE:
					case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
					case modREConstants.CNSTLANDTYPELINEARFEET:
					case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
						{
							strUnits1 = FCConvert.ToString(Conversion.Val(strUnits1 + strUnits2) / 100);
							modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandUnits[intII] = Strings.Format(strUnits1, "###0.00");
							switch (modGlobalVariables.Statics.LandTypes.UnitsType)
							{
								case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
									{
										modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandMethod[intII] = "Fr. Acre";
										break;
									}
								case modREConstants.CNSTLANDTYPEACRES:
									{
										modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandMethod[intII] = "Acre";
										break;
									}
								case modREConstants.CNSTLANDTYPESITE:
									{
										modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandMethod[intII] = "Site";
										break;
									}
								case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
									{
										modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandMethod[intII] = "Improvement";
										break;
									}
								case modREConstants.CNSTLANDTYPELINEARFEET:
									{
										modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandMethod[intII] = "Lin. Ft.";
										break;
									}
								case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
									{
										modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandMethod[intII] = "Lin. Ft. Wid.";
										break;
									}
							}
							//end switch
							break;
						}
					default:
						{
							strUnits1 = FCConvert.ToString(Conversion.Val(strUnits1 + strUnits2) / 100);
							modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandUnits[intII] = Strings.Format(strUnits1, "###0.00");
							modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandMethod[intII] = "";
							break;
						}
				}
				//end switch
				if (!modGlobalVariables.Statics.boolSeparateLandMethod)
				{
					strAcreType = Strings.Mid(modGlobalVariables.Statics.LandTypes.ShortDescription + "     ", 1, 5) + "-" + modGlobalVariables.Statics.LandTypes.Description;
				}
				else
				{
					strAcreType = modGlobalVariables.Statics.LandTypes.Description;
				}
			}
			else
			{
			}
			modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandDesc[intII] = strAcreType;
			if (modREASValuations.Statics.UnitPrice[intII] < 100000.0)
			{
				// .TextMatrix(intII, 2) = Format(UnitPrice(intII), "##,###.00")
				modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandPricePerUnit[intII] = Strings.Format(modREASValuations.Statics.UnitPrice[intII], "##,###.00");
			}
			else if (modREASValuations.Statics.UnitPrice[intII] < 10000000.0)
			{
				// .TextMatrix(intII, 2) = Format(UnitPrice(intII), "#,###,###")
				modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandPricePerUnit[intII] = Strings.Format(modREASValuations.Statics.UnitPrice[intII], "#,###,###");
			}
			else
			{
				// .TextMatrix(intII, 2) = Format(UnitPrice(intII), "#########")
				modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandPricePerUnit[intII] = Strings.Format(modREASValuations.Statics.UnitPrice[intII], "#########");
			}
			// .TextMatrix(intII, 3) = Format(Price(intII), "###,###,###")
			modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandTotal[intII] = Strings.Format(modREASValuations.Statics.Price[intII], "###,###,###");
			REAS03_5101_TAG:
			;
			// Get #2, (1440 + Val(WORKA4)), CR
			// Call OpenCRTable(CR, (1440 + Val(WORKA4)))
			clsDRWrapper temp = modDataTypes.Statics.CR;
			modREMain.OpenCRTable(ref temp, (1440 + Conversion.Val(Statics.WORKA3)));
			modDataTypes.Statics.CR = temp;
			modREASValuations.Statics.WORKA6 = Strings.Mid(FCConvert.ToString(modDataTypes.Statics.CR.Get_Fields_String("ClDesc")), 1, 10) + " ";
			// intHoldType = Val(WORKA2) / 100
			// .TextMatrix(intII, 4) = Val(WORKA2) & "%"
			modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandFctr[intII] = FCConvert.ToString(Conversion.Val(modREASValuations.Statics.WORKA2)) + "%";
			// .TextMatrix(intII, 5) = WORKA6
			modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandInfluence[intII] = modREASValuations.Statics.WORKA6;
			// .TextMatrix(intII, 6) = Format(Value(intII), "#,##0")
			modSpeedCalc.Statics.CalcPropertyList[modSpeedCalc.Statics.CalcPropertyIndex].LandValue[intII] = Strings.Format(modREASValuations.Statics.Value[intII], "#,##0");
			// End With
		}

		public class StaticVariables
		{
			// ***  Variables for Correlation Tab  ***
			//=========================================================
			public int gintCurrentLandSchedule;
			public bool boolFromProperty;
			// vbPorter upgrade warning: lngCurrentLand As int	OnWrite(double, int)
			public int[] lngCurrentLand = new int[999 + 1];
			// vbPorter upgrade warning: lngCurrentBldg As int	OnWrite(double, int)
			public int[] lngCurrentBldg = new int[999 + 1];
			public int[] lngPreviousLand = new int[999 + 1];
			public int[] lngPreviousBldg = new int[999 + 1];
			public int[] lngMarketLand = new int[999 + 1];
			public int[] lngMarketBldg = new int[999 + 1];
			public int[] lngIncomeLand = new int[999 + 1];
			// vbPorter upgrade warning: lngIncomeBldg As int	OnWrite(double, int)
			public int[] lngIncomeBldg = new int[999 + 1];
			public int[] lngAcceptedLand = new int[999 + 1];
			public int[] lngAcceptedBldg = new int[999 + 1];
			public int[] lngCorrelatedLand = new int[999 + 1];
			public int[] lngCorrelatedBldg = new int[999 + 1];
			public int[] intOverRideCodeLand = new int[999 + 1];
			public int[] intOverRideCodeBldg = new int[999 + 1];
			public int[] lngOverRideLand = new int[999 + 1];
			public int[] lngOverRideBldg = new int[999 + 1];
			public int lngacceptedmultiplier;
			// ***  Variables for Correlation Tab  ***
			public string OverRideFlag = "";
			public string ORFlags = "";
			// String
			public double[] LFRA = new double[2500 + 1];
			public double[] LEXP = null;
			public double[] LEX2 = null;
			public Dictionary<object, object> WidthExponentsOne = new Dictionary<object, object>();
			public Dictionary<object, object> WidthExponentsTwo = new Dictionary<object, object>();
			// vbPorter upgrade warning: LandTotal As double	OnRead(double, int)
			// Public LSFL(99)             As Double
			public double[] LandTotal = new double[999 + 1];
			// vbPorter upgrade warning: BuildingTotal As double	OnRead(string, int)
			public double[] BuildingTotal = new double[999 + 1];
			public double[] LandSummaryTotal = new double[999 + 1];
			public double LandTrend;
			public double LandFactor;
			public string RUNTYPE = string.Empty;
			// String
			public int intWorkZone;
			public int intWorkNeighborhood;
			// vbPorter upgrade warning: WKey As short --> As int	OnWrite(double, int)
			public int WKey;
			// vbPorter upgrade warning: WORK1 As Variant --> As double	OnWrite(int, double)
			public double WORK1;
			public object Work2;
			public object Work3;
			// vbPorter upgrade warning: WORKA1 As Variant --> As string
			public string WORKA1 = "";
			// vbPorter upgrade warning: WORKA3 As object	OnWrite(int, string, double, object)
			public object WORKA3;
			public int intTemp;
			// a temporary variable for a select statement
			public object USED;
			// Dim ValuationReportOption   As Integer
			public double Perval;
			// Double
			public double AccountAcreage;
			public int Row;
			public int[] intLandType = new int[7 + 1];
			public string[] strLandUnit = new string[7 + 1];
			// vbPorter upgrade warning: intLandInfl As double	OnRead(string)
			public double[] intLandInfl = new double[7 + 1];
			public int[] intLandCode = new int[7 + 1];
			// Dim UnitPrice(7)            As Double
			// Dim Price(7)                As Double
			// Dim Value(7)                As Double
			public double dblAcreage;
			public int lngUnits;
			public int intAcreOption;
			public int LINECOUNT;
			// Dim Schedule                As Integer
			public double StandardDepth;
			public double StandardLot;
			public double StandardWidth;
			public string AcreType = "";
			// String
			public string FirstLandFactor = "";
			public double StreetPrice;
			public double InfluenceFactor;
			public string LandUnitType = "";
			// String
			public string HHAcreType = "";
			// Dim LandAcreValue           As Double
			// String
			public double RateFactor;
			public double Lexponent;
			public double Lexponent2;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
