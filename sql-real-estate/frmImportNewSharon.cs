﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportNewSharon.
	/// </summary>
	public partial class frmImportNewSharon : BaseForm
	{
		public frmImportNewSharon()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmImportNewSharon InstancePtr
		{
			get
			{
				return (frmImportNewSharon)Sys.GetInstance(typeof(frmImportNewSharon));
			}
		}

		protected frmImportNewSharon _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private string strSourceDir = string.Empty;
		private string strSourceFile = string.Empty;
		string strFileName;
		private bool boolLoaded;
		const int CNSTGRIDCOLACCOUNT = 0;
		const int CNSTGRIDCOLOWNER = 1;
		const int CNSTGRIDCOLMAPLOT = 2;
		const int CNSTGRIDCOLLAND = 3;
		const int CNSTGRIDCOLBLDG = 4;
		const int CNSTGRIDCOLEXEMPTION = 5;
		const int CNSTGRIDCOLNEW = 6;
		PartyUtil tPu = new PartyUtil();
		private object[] arParties = null;

		private void frmImportNewSharon_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
				LoadData();
				//Application.DoEvents();
				this.Unload();
			}
		}

		private void frmImportNewSharon_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmImportNewSharon_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmImportNewSharon properties;
			//frmImportNewSharon.FillStyle	= 0;
			//frmImportNewSharon.ScaleWidth	= 3885;
			//frmImportNewSharon.ScaleHeight	= 2235;
			//frmImportNewSharon.LinkTopic	= "Form2";
			//frmImportNewSharon.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		public void Init()
		{
			string strPath = "";
			//FileSystemObject fso = new FileSystemObject();
			try
			{
				// On Error GoTo ErrorHandler
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNExplorer	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Open File";
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strPath = MDIParent.InstancePtr.CommonDialog1.FileName;
				if (strPath == string.Empty)
					return;
				strSourceFile = strPath;
				strSourceDir = Directory.GetParent(strSourceFile).FullName;
				strFileName = new FileInfo(strSourceFile).Name;
				if (Strings.Right(strSourceDir, 1) != "\\")
					strSourceDir += "\\";
				boolLoaded = false;
				tPu = new PartyUtil();
				this.Show(FCForm.FormShowEnum.Modal);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (!(Information.Err(ex).Number == 32755))
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void LoadData()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strTable;
				clsDRWrapper rsSave = new clsDRWrapper();
				string strMapLot = "";
				int lngRow = 0;
				int lngNewAcct = 0;
				int lngAcct = 0;
				string strZip = "";
				string strTemp = "";
				string strZipExt = "";
				string[] strAry = null;
				string strLastMapLot;
				bool boolSameAccount = false;
				string strOrMapLot = "";
				string strConStr;
				cPartyController tPC = new cPartyController();
				cCreateGUID tGuid = new cCreateGUID();
				tPu.ConnectionString = rsSave.Get_ConnectionInformation("CentralParties");
				Array.Resize(ref arParties, 0 + 1);
				Grid.Rows = 0;
				strLastMapLot = "None";
				strConStr = rsLoad.MakeODBCConnectionStringForExcel(strSourceFile);
				rsLoad.AddConnection(strTemp, strFileName, FCConvert.ToInt32(dbConnectionTypes.ODBC));
				// rsLoad.ConnectionStringOverride = "Excel 8.0;" & strSourceDir
				// rsLoad.Path = strSourceDir
				cParty oParty = new cParty();
				cParty soParty = new cParty();
				UtilParty uParty = new UtilParty();
				// vbPorter upgrade warning: tVar As object	OnRead(UtilParty)
				UtilParty[] tVar = null;
				string strOwner1 = "";
				string strOwner2 = "";
				cPartyAddress tAddr;
				strTable = Strings.Mid(strFileName, 1, strFileName.Length - 4);
				// table is filename - .xls
				rsSave.Execute("update master set rsdeleted = 1,hlupdate = '" + Strings.Format(DateTime.Today, "MM/dd/yyyy") + "' WHERE rsdeleted = 0", "twre0000.vb1");
				rsSave.Execute("update master set rsmaplot = mid(rsmaplot,2) where rsmaplot & '' <> '' and left(rsmaplot & ' ',1) = 'M'", modGlobalVariables.strREDatabase);
				rsLoad.OpenRecordset("select * from [" + strTable + "$] order by [Map],[Lot(s)],[Suffix]", strFileName);
				while (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [map] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Lot(s)] not found!! (maybe it is an alias?)
					strMapLot = rsLoad.Get_Fields("map") + "-" + rsLoad.Get_Fields("Lot(s)");
					strOrMapLot = "";
					if (FCConvert.ToString(rsLoad.Get_Fields_String("suffix")) != "00" && FCConvert.ToString(rsLoad.Get_Fields_String("suffix")) != "")
					{
						if (FCConvert.ToString(rsLoad.Get_Fields_String("suffix")).Length == 2)
						{
							if (Strings.Left(FCConvert.ToString(rsLoad.Get_Fields_String("suffix")), 1) == "0")
							{
								strOrMapLot = strMapLot + "-" + Strings.Mid(FCConvert.ToString(rsLoad.Get_Fields_String("suffix")), 2);
							}
						}
						// If IsNumeric(rsLoad.Fields("suffix")) And Len(rsLoad.Fields("suffix")) = 1 Then
						// strMapLot = strMapLot & "-0" & rsLoad.Fields("suffix")
						// Else
						strMapLot += "-" + rsLoad.Get_Fields_String("suffix");
						// End If
					}
					if (strOrMapLot == string.Empty)
					{
						rsSave.OpenRecordset("select * from master where rscard = 1 and rsmaplot = '" + strMapLot + "'", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsSave.OpenRecordset("select * from master where rscard = 1 and (rsmaplot = '" + strMapLot + "' or rsmaplot = '" + strOrMapLot + "')", modGlobalVariables.strREDatabase);
					}
					if (strLastMapLot != strMapLot)
					{
						boolSameAccount = false;
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						strLastMapLot = strMapLot;
					}
					else
					{
						boolSameAccount = true;
					}
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNEW, FCConvert.ToString(false));
						rsSave.Set_Fields("rsdeleted", false);
					}
					else
					{
						rsSave.AddNew();
						rsSave.Set_Fields("datecreated", DateTime.Today);
						lngNewAcct = modGlobalRoutines.GetNextAccountToCreate();
						rsSave.Set_Fields("rsaccount", lngNewAcct);
						rsSave.Set_Fields("rscard", 1);
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNEW, FCConvert.ToString(true));
					}
					lngAcct = FCConvert.ToInt32(rsSave.Get_Fields_Int32("rsaccount"));
					Label1.Text = "Importing Account " + strMapLot;
					Label1.Refresh();
					//Application.DoEvents();
					if (!boolSameAccount)
					{
						rsSave.Set_Fields("Ownerpartyid", 0);
						rsSave.Set_Fields("secownerpartyid", 0);
						oParty.Clear();
						oParty.DateCreated = DateTime.Now;
						oParty.PartyGUID = tGuid.CreateGUID();
						oParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
						soParty.Clear();
						soParty.DateCreated = oParty.DateCreated;
						soParty.PartyGUID = tGuid.CreateGUID();
						soParty.CreatedBy = oParty.CreatedBy;
						// TODO Get_Fields: Field [Lastname(1)] not found!! (maybe it is an alias?)
						strOwner1 = FCConvert.ToString(rsLoad.Get_Fields("Lastname(1)"));
						// TODO Get_Fields: Field [2nd Owner] not found!! (maybe it is an alias?)
						strOwner2 = FCConvert.ToString(rsLoad.Get_Fields("2nd Owner"));
						if (Strings.Trim(strOwner2) == "")
						{
							tVar = tPu.SplitName(strOwner1, true);
							if (Information.UBound(tVar, 1) >= 0)
							{
								if (Information.UBound(tVar, 1) > 0)
								{
									uParty = tVar[1];
									soParty.Designation = uParty.Designation;
									soParty.FirstName = uParty.FirstName;
									soParty.MiddleName = uParty.MiddleName;
									soParty.LastName = uParty.LastName;
									soParty.PartyType = uParty.PartyType;
								}
								uParty = tVar[0];
								oParty.Designation = uParty.Designation;
								oParty.FirstName = uParty.FirstName;
								oParty.MiddleName = uParty.MiddleName;
								oParty.LastName = uParty.LastName;
								oParty.PartyType = uParty.PartyType;
							}
						}
						else
						{
							tVar = tPu.SplitName(strOwner1, false);
							if (Information.UBound(tVar, 1) >= 0)
							{
								uParty = tVar[0];
								oParty.Designation = uParty.Designation;
								oParty.FirstName = uParty.FirstName;
								oParty.MiddleName = uParty.MiddleName;
								oParty.LastName = uParty.LastName;
								oParty.PartyType = uParty.PartyType;
							}
							tVar = tPu.SplitName(strOwner2, false);
							if (Information.UBound(tVar, 1) >= 0)
							{
								uParty = tVar[0];
								soParty.Designation = uParty.Designation;
								soParty.FirstName = uParty.FirstName;
								soParty.MiddleName = uParty.MiddleName;
								soParty.LastName = uParty.LastName;
								soParty.PartyType = uParty.PartyType;
							}
						}
						if (oParty.FirstName != "" || oParty.LastName != "")
						{
							tAddr = new cPartyAddress();
							// TODO Get_Fields: Field [Address (1)] not found!! (maybe it is an alias?)
							tAddr.Address1 = FCConvert.ToString(rsLoad.Get_Fields("Address (1)"));
							tAddr.Address2 = "";
							tAddr.Address3 = "";
							tAddr.City = FCConvert.ToString(rsLoad.Get_Fields_String("city"));
							tAddr.State = FCConvert.ToString(rsLoad.Get_Fields_String("state"));
							tAddr.Zip = FCConvert.ToString(rsLoad.Get_Fields_String("zip"));
							tAddr.AddressType = "Primary";
							oParty.Addresses.Add(tAddr);
							tPC.SaveParty(ref oParty, false);
							rsSave.Set_Fields("ownerpartyid", oParty.ID);
							Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
							arParties[Information.UBound(arParties)] = oParty.ID;
						}
						if (soParty.FirstName != "" || soParty.LastName != "")
						{
							tPC.SaveParty(ref soParty, true);
							rsSave.Set_Fields("secownerpartyid", soParty.ID);
							Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
							arParties[Information.UBound(arParties)] = soParty.ID;
						}
						// rsSave.Fields("rsname") = rsLoad.Fields("Lastname(1)")
						// rsSave.Fields("rssecowner") = rsLoad.Fields("2nd Owner")
						// rsSave.Fields("rsaddr1") = rsLoad.Fields("Address (1)")
						// rsSave.Fields("rsaddr2") = ""
						// rsSave.Fields("rsaddr3") = rsLoad.Fields("city")
						// rsSave.Fields("rsstate") = rsLoad.Fields("state")
						// strTemp = rsLoad.Fields("zip")
						// If strTemp <> vbNullString Then
						// strAry = Split(strTemp, "-", , vbTextCompare)
						// strZip = strAry(0)
						// If UBound(strAry) > 0 Then
						// strZipExt = strAry(1)
						// Else
						// strZipExt = ""
						// End If
						// Else
						// strZip = ""
						// strZipExt = ""
						// End If
						// rsSave.Fields("rszip") = strZip
						// rsSave.Fields("rszip4") = strZipExt
						rsSave.Set_Fields("rsmaplot", strMapLot);
						rsSave.Set_Fields("rssoft", 0);
						rsSave.Set_Fields("rsmixed", 0);
						rsSave.Set_Fields("rshard", 0);
						// TODO Get_Fields: Field [SW] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("rssoftvalue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("SW"))));
						// TODO Get_Fields: Field [MW] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("rsmixedvalue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("MW"))));
						// TODO Get_Fields: Field [HW] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("rshardvalue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("HW"))));
						// TODO Get_Fields: Field [Land Value] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [SW] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [MW] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [HW] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("rsothervalue", Conversion.Val(rsLoad.Get_Fields("Land Value")) - Conversion.Val(rsLoad.Get_Fields("SW")) - Conversion.Val(rsLoad.Get_Fields("MW")) - Conversion.Val(rsLoad.Get_Fields("HW")));
						rsSave.Set_Fields("rsother", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("Acres"))));
						rsSave.Set_Fields("piacres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("Acres"))));
						rsSave.Set_Fields("exemptpct1", 100);
						rsSave.Set_Fields("exemptpct2", 100);
						rsSave.Set_Fields("exemptpct3", 100);
						// TODO Get_Fields: Field [Land Value] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("lastlandval", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("Land Value"))));
						// TODO Get_Fields: Field [Bld Value] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("lastbldgval", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("Bld Value"))));
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsSave.Set_Fields("rsref2", "Account: " + rsLoad.Get_Fields("Account"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(lngAcct));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLBLDG, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Int32("lastbldgval"))));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLEXEMPTION, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Int32("rlexemption"))));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLLAND, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Int32("lastlandval"))));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(rsSave.Get_Fields_String("rsmaplot")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNER, FCConvert.ToString(rsSave.Get_Fields_String("rsname")));
						rsSave.Update();
						// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsLoad.Get_Fields("Book")) != "" && FCConvert.ToString(rsLoad.Get_Fields("Page")) != "")
						{
							rsSave.Execute("delete from bookpage where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
							rsSave.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
							rsSave.AddNew();
							rsSave.Set_Fields("account", lngAcct);
							rsSave.Set_Fields("card", 1);
							rsSave.Set_Fields("current", true);
							rsSave.Set_Fields("line", 1);
							rsSave.Set_Fields("saledate", 0);
							// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
							rsSave.Set_Fields("book", rsLoad.Get_Fields("Book"));
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							rsSave.Set_Fields("page", rsLoad.Get_Fields("Page"));
							rsSave.Update();
						}
					}
					else
					{
						// TODO Get_Fields: Field [SW] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("rssoftvalue", Conversion.Val(rsLoad.Get_Fields("SW")) + Conversion.Val(rsSave.Get_Fields_Int32("rssoftvalue")));
						// TODO Get_Fields: Field [MW] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("rsmixedvalue", Conversion.Val(rsLoad.Get_Fields("MW")) + Conversion.Val(rsSave.Get_Fields_Int32("rsmixedvalue")));
						// TODO Get_Fields: Field [HW] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("rshardvalue", Conversion.Val(rsLoad.Get_Fields("HW")) + Conversion.Val(rsSave.Get_Fields_Int32("rshardvalue")));
						// TODO Get_Fields: Field [Land Value] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [SW] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [MW] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [HW] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("rsothervalue", (Conversion.Val(rsLoad.Get_Fields("Land Value")) - Conversion.Val(rsLoad.Get_Fields("SW")) - Conversion.Val(rsLoad.Get_Fields("MW")) - Conversion.Val(rsLoad.Get_Fields("HW"))) + Conversion.Val(rsSave.Get_Fields_Int32("rsothervalue")));
						rsSave.Set_Fields("rsother", Conversion.Val(rsLoad.Get_Fields_Double("Acres")) + Conversion.Val(rsSave.Get_Fields_Double("rsother")));
						rsSave.Set_Fields("piacres", Conversion.Val(rsLoad.Get_Fields_Double("Acres")) + Conversion.Val(rsSave.Get_Fields_Double("piacres")));
						// TODO Get_Fields: Field [Land Value] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("lastlandval", Conversion.Val(rsLoad.Get_Fields("Land Value")) + Conversion.Val(rsSave.Get_Fields_Int32("lastlandval")));
						// TODO Get_Fields: Field [Bld Value] not found!! (maybe it is an alias?)
						rsSave.Set_Fields("lastbldgval", Conversion.Val(rsLoad.Get_Fields("Bld Value")) + Conversion.Val(rsSave.Get_Fields_Int32("lastbldgval")));
						rsSave.Update();
					}
					rsLoad.MoveNext();
				}
				rptNewSharonImport.InstancePtr.Init(this.Modal);
				// If UBound(arParties) > 0 Then
				// If MsgBox(UBound(arParties) & " New parties added" & vbNewLine & "Would you like to check for duplicates now?" & vbNewLine & "This can also be done at any time later.", vbYesNo + vbQuestion, "Check for Duplicates") = vbYes Then
				// Label1.Caption = "Loading Party information"
				// Label1.Refresh
				// DoEvents
				// Call tPu.GetCondensedPartiesHintsFromListAsynchronously(arParties())
				// End If
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void tPu_UpdateMessage(double dblProgress, string strMessage)
		{
			Label1.Text = strMessage;
		}
	}
}
