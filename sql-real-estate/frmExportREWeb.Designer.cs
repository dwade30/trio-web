﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExportREWeb.
	/// </summary>
	partial class frmExportREWeb : BaseForm
	{
		public fecherFoundation.FCComboBox cmbCurrent;
		public fecherFoundation.FCLabel lblCurrent;
		public fecherFoundation.FCComboBox cmbFrom;
		public fecherFoundation.FCLabel lblFrom;
		public fecherFoundation.FCComboBox cmbtRange;
		public fecherFoundation.FCTextBox txtRef2Caption;
		public fecherFoundation.FCTextBox txtRef1Caption;
		public fecherFoundation.FCCheckBox chkIncludeRef2;
		public fecherFoundation.FCCheckBox chkIncludeRef1;
		public fecherFoundation.FCCheckBox chkIncludeSketch;
		public fecherFoundation.FCComboBox cmbIncludePictures;
		public fecherFoundation.FCCheckBox chkOutstanding;
		public fecherFoundation.FCFrame Frame2;
		public FCGrid Grid;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtTaxYear;
		public FCGrid GridType;
		public fecherFoundation.FCLabel lblTaxYear;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame framRange;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public FCGrid GridRecords;
		public FCGrid gridTownCode;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblpictures;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExportREWeb));
            this.cmbCurrent = new fecherFoundation.FCComboBox();
            this.lblCurrent = new fecherFoundation.FCLabel();
            this.cmbFrom = new fecherFoundation.FCComboBox();
            this.lblFrom = new fecherFoundation.FCLabel();
            this.cmbtRange = new fecherFoundation.FCComboBox();
            this.txtRef2Caption = new fecherFoundation.FCTextBox();
            this.txtRef1Caption = new fecherFoundation.FCTextBox();
            this.chkIncludeRef2 = new fecherFoundation.FCCheckBox();
            this.chkIncludeRef1 = new fecherFoundation.FCCheckBox();
            this.chkIncludeSketch = new fecherFoundation.FCCheckBox();
            this.cmbIncludePictures = new fecherFoundation.FCComboBox();
            this.chkOutstanding = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.Grid = new fecherFoundation.FCGrid();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtTaxYear = new fecherFoundation.FCTextBox();
            this.GridType = new fecherFoundation.FCGrid();
            this.lblTaxYear = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.framRange = new fecherFoundation.FCFrame();
            this.txtStart = new fecherFoundation.FCTextBox();
            this.txtEnd = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.GridRecords = new fecherFoundation.FCGrid();
            this.gridTownCode = new fecherFoundation.FCGrid();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblpictures = new fecherFoundation.FCLabel();
            this.cmdSettings = new Wisej.Web.Button();
            this.cmdSaveExit = new Wisej.Web.Button();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeRef2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeRef1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSketch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOutstanding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framRange)).BeginInit();
            this.framRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridRecords)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 892);
            this.BottomPanel.Size = new System.Drawing.Size(868, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtRef2Caption);
            this.ClientArea.Controls.Add(this.txtRef1Caption);
            this.ClientArea.Controls.Add(this.chkIncludeRef2);
            this.ClientArea.Controls.Add(this.chkIncludeRef1);
            this.ClientArea.Controls.Add(this.chkIncludeSketch);
            this.ClientArea.Controls.Add(this.cmbIncludePictures);
            this.ClientArea.Controls.Add(this.chkOutstanding);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.gridTownCode);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblpictures);
            this.ClientArea.Size = new System.Drawing.Size(888, 628);
            this.ClientArea.Controls.SetChildIndex(this.lblpictures, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.gridTownCode, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkOutstanding, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbIncludePictures, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkIncludeSketch, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkIncludeRef1, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkIncludeRef2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtRef1Caption, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtRef2Caption, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSave);
            this.TopPanel.Controls.Add(this.cmdSettings);
            this.TopPanel.Size = new System.Drawing.Size(888, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSettings, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSave, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(129, 28);
            this.HeaderText.Text = "Web Export";
            // 
            // cmbCurrent
            // 
            this.cmbCurrent.Items.AddRange(new object[] {
            "Calculated",
            "Billing"});
            this.cmbCurrent.Location = new System.Drawing.Point(168, 90);
            this.cmbCurrent.Name = "cmbCurrent";
            this.cmbCurrent.Size = new System.Drawing.Size(120, 40);
            this.cmbCurrent.TabIndex = 2;
            this.cmbCurrent.Text = "Billing";
            // 
            // lblCurrent
            // 
            this.lblCurrent.AutoSize = true;
            this.lblCurrent.Location = new System.Drawing.Point(20, 104);
            this.lblCurrent.Name = "lblCurrent";
            this.lblCurrent.Size = new System.Drawing.Size(123, 16);
            this.lblCurrent.TabIndex = 3;
            this.lblCurrent.Text = "CURRENT VALUES";
            // 
            // cmbFrom
            // 
            this.cmbFrom.Items.AddRange(new object[] {
            "From RE",
            "From Bills"});
            this.cmbFrom.Location = new System.Drawing.Point(168, 210);
            this.cmbFrom.Name = "cmbFrom";
            this.cmbFrom.Size = new System.Drawing.Size(120, 40);
            this.cmbFrom.TabIndex = 0;
            this.cmbFrom.Text = "From RE";
            this.cmbFrom.Visible = false;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(20, 224);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(86, 16);
            this.lblFrom.TabIndex = 1;
            this.lblFrom.Text = "TAX VALUES";
            this.lblFrom.Visible = false;
            // 
            // cmbtRange
            // 
            this.cmbtRange.Items.AddRange(new object[] {
            "Account",
            "Name",
            "Map / Lot",
            "Location"});
            this.cmbtRange.Location = new System.Drawing.Point(20, 186);
            this.cmbtRange.Name = "cmbtRange";
            this.cmbtRange.Size = new System.Drawing.Size(145, 40);
            this.cmbtRange.TabIndex = 3;
            this.cmbtRange.Text = "Account";
            // 
            // txtRef2Caption
            // 
            this.txtRef2Caption.BackColor = System.Drawing.SystemColors.Window;
            this.txtRef2Caption.Location = new System.Drawing.Point(256, 532);
            this.txtRef2Caption.Name = "txtRef2Caption";
            this.txtRef2Caption.Size = new System.Drawing.Size(128, 40);
            this.txtRef2Caption.TabIndex = 11;
            // 
            // txtRef1Caption
            // 
            this.txtRef1Caption.BackColor = System.Drawing.SystemColors.Window;
            this.txtRef1Caption.Location = new System.Drawing.Point(256, 472);
            this.txtRef1Caption.Name = "txtRef1Caption";
            this.txtRef1Caption.Size = new System.Drawing.Size(128, 40);
            this.txtRef1Caption.TabIndex = 9;
            // 
            // chkIncludeRef2
            // 
            this.chkIncludeRef2.Location = new System.Drawing.Point(30, 532);
            this.chkIncludeRef2.Name = "chkIncludeRef2";
            this.chkIncludeRef2.Size = new System.Drawing.Size(154, 23);
            this.chkIncludeRef2.TabIndex = 10;
            this.chkIncludeRef2.Text = "Include Reference 2";
            // 
            // chkIncludeRef1
            // 
            this.chkIncludeRef1.Location = new System.Drawing.Point(30, 472);
            this.chkIncludeRef1.Name = "chkIncludeRef1";
            this.chkIncludeRef1.Size = new System.Drawing.Size(154, 23);
            this.chkIncludeRef1.TabIndex = 8;
            this.chkIncludeRef1.Text = "Include Reference 1";
            // 
            // chkIncludeSketch
            // 
            this.chkIncludeSketch.Location = new System.Drawing.Point(404, 532);
            this.chkIncludeSketch.Name = "chkIncludeSketch";
            this.chkIncludeSketch.Size = new System.Drawing.Size(210, 23);
            this.chkIncludeSketch.TabIndex = 31;
            this.chkIncludeSketch.Text = "Include Sketch With Pictures";
            // 
            // cmbIncludePictures
            // 
            this.cmbIncludePictures.BackColor = System.Drawing.SystemColors.Window;
            this.cmbIncludePictures.Location = new System.Drawing.Point(404, 472);
            this.cmbIncludePictures.Name = "cmbIncludePictures";
            this.cmbIncludePictures.Size = new System.Drawing.Size(90, 40);
            this.cmbIncludePictures.TabIndex = 29;
            // 
            // chkOutstanding
            // 
            this.chkOutstanding.Location = new System.Drawing.Point(404, 425);
            this.chkOutstanding.Name = "chkOutstanding";
            this.chkOutstanding.Size = new System.Drawing.Size(216, 23);
            this.chkOutstanding.TabIndex = 18;
            this.chkOutstanding.Text = "Update Outstanding Amounts";
            this.chkOutstanding.Visible = false;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.Grid);
            this.Frame2.Location = new System.Drawing.Point(30, 592);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(802, 300);
            this.Frame2.TabIndex = 28;
            this.Frame2.Text = "Exclude";
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 9;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 4;
            this.Grid.FrozenCols = 3;
            this.Grid.Location = new System.Drawing.Point(20, 30);
            this.Grid.Name = "Grid";
            this.Grid.ReadOnly = false;
            this.Grid.Rows = 1;
            this.Grid.Size = new System.Drawing.Size(762, 250);
            this.Grid.TabIndex = 19;
            this.Grid.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_CellChanged);
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.cmbFrom);
            this.Frame3.Controls.Add(this.lblFrom);
            this.Frame3.Controls.Add(this.cmbCurrent);
            this.Frame3.Controls.Add(this.lblCurrent);
            this.Frame3.Controls.Add(this.txtTaxYear);
            this.Frame3.Controls.Add(this.GridType);
            this.Frame3.Controls.Add(this.lblTaxYear);
            this.Frame3.Location = new System.Drawing.Point(275, 90);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(308, 270);
            this.Frame3.TabIndex = 21;
            this.Frame3.Text = "Type";
            // 
            // txtTaxYear
            // 
            this.txtTaxYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxYear.Location = new System.Drawing.Point(168, 150);
            this.txtTaxYear.Name = "txtTaxYear";
            this.txtTaxYear.Size = new System.Drawing.Size(80, 40);
            this.txtTaxYear.TabIndex = 15;
            this.txtTaxYear.Visible = false;
            // 
            // GridType
            // 
            this.GridType.Cols = 1;
            this.GridType.ColumnHeadersVisible = false;
            this.GridType.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridType.ExtendLastCol = true;
            this.GridType.FixedCols = 0;
            this.GridType.FixedRows = 0;
            this.GridType.Location = new System.Drawing.Point(20, 30);
            this.GridType.Name = "GridType";
            this.GridType.ReadOnly = false;
            this.GridType.RowHeadersVisible = false;
            this.GridType.Rows = 1;
            this.GridType.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.GridType.Size = new System.Drawing.Size(268, 41);
            this.GridType.TabIndex = 12;
            // 
            // lblTaxYear
            // 
            this.lblTaxYear.Location = new System.Drawing.Point(20, 164);
            this.lblTaxYear.Name = "lblTaxYear";
            this.lblTaxYear.Size = new System.Drawing.Size(63, 15);
            this.lblTaxYear.TabIndex = 25;
            this.lblTaxYear.Text = "TAX YEAR";
            this.lblTaxYear.Visible = false;
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxLeftBorder";
            this.Frame1.Controls.Add(this.framRange);
            this.Frame1.Controls.Add(this.GridRecords);
            this.Frame1.Location = new System.Drawing.Point(30, 90);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(225, 362);
            this.Frame1.TabIndex = 20;
            this.Frame1.Text = "Records";
            // 
            // framRange
            // 
            this.framRange.Controls.Add(this.txtStart);
            this.framRange.Controls.Add(this.cmbtRange);
            this.framRange.Controls.Add(this.txtEnd);
            this.framRange.Controls.Add(this.Label2);
            this.framRange.Controls.Add(this.Label1);
            this.framRange.Location = new System.Drawing.Point(20, 90);
            this.framRange.Name = "framRange";
            this.framRange.Size = new System.Drawing.Size(185, 252);
            this.framRange.TabIndex = 22;
            this.framRange.Text = "Range";
            this.framRange.Visible = false;
            // 
            // txtStart
            // 
            this.txtStart.BackColor = System.Drawing.SystemColors.Window;
            this.txtStart.Location = new System.Drawing.Point(20, 30);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(145, 40);
            this.txtStart.TabIndex = 2;
            // 
            // txtEnd
            // 
            this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
            this.txtEnd.Location = new System.Drawing.Point(20, 108);
            this.txtEnd.Name = "txtEnd";
            this.txtEnd.Size = new System.Drawing.Size(145, 40);
            this.txtEnd.TabIndex = 3;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(70, 158);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(25, 18);
            this.Label2.TabIndex = 24;
            this.Label2.Text = "OF";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(70, 80);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(25, 18);
            this.Label1.TabIndex = 23;
            this.Label1.Text = "TO";
            // 
            // GridRecords
            // 
            this.GridRecords.Cols = 1;
            this.GridRecords.ColumnHeadersVisible = false;
            this.GridRecords.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridRecords.ExtendLastCol = true;
            this.GridRecords.FixedCols = 0;
            this.GridRecords.FixedRows = 0;
            this.GridRecords.Location = new System.Drawing.Point(20, 30);
            this.GridRecords.Name = "GridRecords";
            this.GridRecords.ReadOnly = false;
            this.GridRecords.RowHeadersVisible = false;
            this.GridRecords.Rows = 1;
            this.GridRecords.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.GridRecords.Size = new System.Drawing.Size(185, 41);
            this.GridRecords.TabIndex = 1;
            // 
            // gridTownCode
            // 
            this.gridTownCode.Cols = 1;
            this.gridTownCode.ColumnHeadersVisible = false;
            this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTownCode.ExtendLastCol = true;
            this.gridTownCode.FixedCols = 0;
            this.gridTownCode.FixedRows = 0;
            this.gridTownCode.Location = new System.Drawing.Point(30, 30);
            this.gridTownCode.Name = "gridTownCode";
            this.gridTownCode.ReadOnly = false;
            this.gridTownCode.RowHeadersVisible = false;
            this.gridTownCode.Rows = 1;
            this.gridTownCode.Size = new System.Drawing.Size(144, 40);
            this.gridTownCode.TabIndex = 1001;
            this.gridTownCode.Visible = false;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(216, 546);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(26, 18);
            this.Label4.TabIndex = 33;
            this.Label4.Text = "AS";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(216, 486);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(26, 18);
            this.Label3.TabIndex = 32;
            this.Label3.Text = "AS";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblpictures
            // 
            this.lblpictures.Location = new System.Drawing.Point(531, 486);
            this.lblpictures.Name = "lblpictures";
            this.lblpictures.Size = new System.Drawing.Size(156, 17);
            this.lblpictures.TabIndex = 30;
            this.lblpictures.Text = "PICTURES PER ACCOUNT";
            // 
            // cmdSettings
            // 
            this.cmdSettings.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSettings.AppearanceKey = "toolbarButton";
            this.cmdSettings.Location = new System.Drawing.Point(792, 29);
            this.cmdSettings.Name = "cmdSettings";
            this.cmdSettings.Size = new System.Drawing.Size(68, 24);
            this.cmdSettings.TabIndex = 1;
            this.cmdSettings.Text = "Settings";
            this.cmdSettings.Click += new System.EventHandler(this.mnuSettings_Click);
            // 
            // cmdSaveExit
            // 
            this.cmdSaveExit.AppearanceKey = "acceptButton";
            this.cmdSaveExit.Location = new System.Drawing.Point(293, 30);
            this.cmdSaveExit.Name = "cmdSaveExit";
            this.cmdSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveExit.Size = new System.Drawing.Size(174, 48);
            this.cmdSaveExit.TabIndex = 2;
            this.cmdSaveExit.Text = "Save & Continue";
            this.cmdSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSave.Location = new System.Drawing.Point(738, 29);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdSave.Size = new System.Drawing.Size(48, 24);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmExportREWeb
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(888, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmExportREWeb";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Web Export";
            this.Load += new System.EventHandler(this.frmExportREWeb_Load);
            this.Resize += new System.EventHandler(this.frmExportREWeb_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExportREWeb_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeRef2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeRef1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSketch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOutstanding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.framRange)).EndInit();
            this.framRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridRecords)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdSaveExit;
		private Button cmdSettings;
        private FCButton cmdSave;
    }
}
