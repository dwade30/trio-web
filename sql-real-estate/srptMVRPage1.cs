﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage1.
	/// </summary>
	public partial class srptMVRPage1 : FCSectionReport
	{
		public srptMVRPage1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private MunicipalValuationReturn valuationReturn;
        public srptMVRPage1(MunicipalValuationReturn valuationReturn) : this()
        {
            this.valuationReturn = valuationReturn;
        }

		private void InitializeComponentEx()
		{

		}
		// nObj = 1
		//   0	srptMVRPage1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private void Detail_Format(object sender, EventArgs e)
		{
			lblTitle.Text = "MAINE REVENUE SERVICES - " + valuationReturn.ReportYear  + " MUNICIPAL VALUATION RETURN";
			lblTitle2.Text = "DUE DATE - NOVEMBER 1, " + valuationReturn.ReportYear + " (or within 30 days of commitment, whichever is later)";
			lblYear.Text = valuationReturn.ReportYear.ToString();
			lblYear2.Text = lblYear.Text;
			lblYear3.Text = lblYear.Text;
			lblYear4.Text = lblYear.Text;
			if (valuationReturn.CommitmentDate == DateTime.MinValue)
			{
				txtCommitmentDate.Text = " ";
			}
			else
            {
                txtCommitmentDate.Text = valuationReturn.CommitmentDate.FormatAndPadShortDate();
			}

            txtCounty.Text = valuationReturn.County;
            txtMunicipality.Text = valuationReturn.Municipality;
            txtLine3.Text = valuationReturn.CertifiedRatio.ToString();
			txtLine4.Text = valuationReturn.RealEstate.TaxableLand.FormatAsNumber();
			txtLine5.Text = valuationReturn.RealEstate.TaxableBuilding.FormatAsNumber();
			txtLine6.Text = valuationReturn.RealEstate.TaxableRealEstate.FormatAsNumber();
			txtLine7.Text = valuationReturn.PersonalProperty.ProductionMachineryAndEquipment.FormatAsNumber();			
			txtLine8.Text = valuationReturn.PersonalProperty.BusinessEquipment.FormatAsNumber();			
			txtLine9.Text = valuationReturn.PersonalProperty.OtherPersonalProperty.FormatAsNumber();
			txtLine10.Text = valuationReturn.PersonalProperty.TaxablePersonalProperty.FormatAsNumber();			
			txtLine11.Text = (valuationReturn.RealEstate.TaxableRealEstate + valuationReturn.PersonalProperty.TaxablePersonalProperty).FormatAsNumber();
			txtLine12.Text = valuationReturn.OtherTaxInformation.TaxRate.ToString();			
			txtLine13.Text = valuationReturn.OtherTaxInformation.TaxLevy.ToString("#,###.00");
			txtLine14a.Text = valuationReturn.Homestead.NumberOfFullHomesteadsGranted.ToString();
			txtLine14b.Text = Convert.ToInt32(valuationReturn.Homestead.TotalValueOfFullHomesteadExemptions).FormatAsNumber();
			txtLine14c.Text = valuationReturn.Homestead.NumberOfFullyExemptedHomesteads.FormatAsNumber();
			txtLine14d.Text = Convert.ToInt32(valuationReturn.Homestead.TotalValueOfFullyExemptedHomesteads).FormatAsNumber();
			txtline14e.Text = valuationReturn.Homestead.TotalNumberOfHomesteadExemptions.FormatAsNumber();
            txtline14f.Text = valuationReturn.Homestead.TotalExemptValueOfAllHomesteads.ToInteger().FormatAsNumber();			
			txtline14g.Text = valuationReturn.Homestead.TotalAssessedValueOfAllHomesteadProperty.FormatAsNumber();
		}

		
	}
}
