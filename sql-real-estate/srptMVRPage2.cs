﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace TWRE0000
{
	public partial class srptMVRPage2 : FCSectionReport
	{

		public srptMVRPage2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private MunicipalValuationReturn valuationReturn;
        public srptMVRPage2(MunicipalValuationReturn valuationReturn) : this()
        {
            this.valuationReturn = valuationReturn;
        }

		private void InitializeComponentEx()
		{
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblTitle.Text = "MAINE REVENUE SERVICES - " + valuationReturn.ReportYear + " MUNICIPAL VALUATION RETURN";
			txtMunicipality.Text = valuationReturn.Municipality;
			txtLine15b.Text = valuationReturn.Bete.ApplicationsApproved.FormatAsNumber();			
			txtLine15c.Text = valuationReturn.Bete.TotalBeteQualifiedExempt.FormatAsNumber();			
			txtLine15d.Text = valuationReturn.Bete.TotalBeteInTIF.FormatAsNumber();			
			txtLine15a.Text = valuationReturn.Bete.ApplicationsProcessed.FormatAsNumber();			
			txtLine16a.Text = valuationReturn.TIF.TifIncrease.FormatAsNumber();
			lblYear.Text = (valuationReturn.ReportYear - 1).ToString();
			Label166.Text = "a. Number of BETE applications processed for tax year " + valuationReturn.ReportYear.ToString();
			
			lblYear3.Text = valuationReturn.ReportYear.ToString();
			lblYear4.Text = valuationReturn.ReportYear.ToString();
			Label120.Text = "a. Number of parcels classified as of April 1," + valuationReturn.ReportYear.ToString();
            txtLine17a.Text = valuationReturn.Excise.Fiscal ? "Fiscal" : "Calendar";
            txtLine16b.Text = valuationReturn.TIF.CapturedAssessedValue.FormatAsNumber();
            txtLine16c.Text = valuationReturn.TIF.RevenueDeposited.FormatAsCurrencyNoSymbol();
            txtLine16d.Text = valuationReturn.TIF.BeteRevenueDeposited.FormatAsCurrencyNoSymbol();
            txtLine17b.Text = valuationReturn.Excise.MVExcise.FormatAsCurrencyNoSymbol();
            txtLine17c.Text = valuationReturn.Excise.WatercraftExcise.FormatAsCurrencyNoSymbol();

            txtLine19a.Text = valuationReturn.ElectricalGeneration.DistributionAndTransmissionLines.FormatAsNumber();			
			txtLine19b.Text = valuationReturn.ElectricalGeneration.GenerationFacilities.FormatAsNumber();
			
			txtLine20.Text = valuationReturn.ForestTreeGrowth.AveragePerAcreUnitValue.FormatAsCurrencyNoSymbol();
            txtLine21a.Text = valuationReturn.ForestTreeGrowth.NumberOfParcelsClassified.FormatAsNumber();

            txtLine21b.Text = valuationReturn.ForestTreeGrowth.SoftwoodAcreage.FormatAsCurrencyNoSymbol();
			txtLine21c.Text = valuationReturn.ForestTreeGrowth.MixedwoodAcreage.FormatAsCurrencyNoSymbol();
			txtLine21d.Text = valuationReturn.ForestTreeGrowth.HardwoodAcreage.FormatAsCurrencyNoSymbol();
			txtLine21e.Text = valuationReturn.ForestTreeGrowth.TotalAcres.FormatAsCurrencyNoSymbol();
			txtLine22.Text = valuationReturn.ForestTreeGrowth.TotalAssessedValuation.FormatAsNumber(0);
			txtHardwood.Text = valuationReturn.ForestTreeGrowth.HardwoodRate.FormatAsCurrencyNoSymbol();
			txtSoftwood.Text = valuationReturn.ForestTreeGrowth.SoftwoodRate.FormatAsCurrencyNoSymbol();
			txtMixedWood.Text = valuationReturn.ForestTreeGrowth.MixedWoodRate.FormatAsCurrencyNoSymbol();
		}

		
	}
}
