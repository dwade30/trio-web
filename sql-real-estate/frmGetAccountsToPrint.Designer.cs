﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetAccountsToPrint.
	/// </summary>
	partial class frmGetAccountsToPrint : BaseForm
	{
		public fecherFoundation.FCListBox List1;
		public fecherFoundation.FCListBox lstAccounts;
		public fecherFoundation.FCTextBox txtAcctNum;
		public fecherFoundation.FCLabel Label29;
		public fecherFoundation.FCLabel Label28;
		public fecherFoundation.FCLabel Label27;
		public fecherFoundation.FCLabel Label26;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetAccountsToPrint));
			this.List1 = new fecherFoundation.FCListBox();
			this.lstAccounts = new fecherFoundation.FCListBox();
			this.txtAcctNum = new fecherFoundation.FCTextBox();
			this.Label29 = new fecherFoundation.FCLabel();
			this.Label28 = new fecherFoundation.FCLabel();
			this.Label27 = new fecherFoundation.FCLabel();
			this.Label26 = new fecherFoundation.FCLabel();
			this.Label25 = new fecherFoundation.FCLabel();
			this.Label24 = new fecherFoundation.FCLabel();
			this.Label23 = new fecherFoundation.FCLabel();
			this.Label22 = new fecherFoundation.FCLabel();
			this.Label21 = new fecherFoundation.FCLabel();
			this.Label20 = new fecherFoundation.FCLabel();
			this.Label19 = new fecherFoundation.FCLabel();
			this.Label18 = new fecherFoundation.FCLabel();
			this.Label17 = new fecherFoundation.FCLabel();
			this.Label16 = new fecherFoundation.FCLabel();
			this.Label15 = new fecherFoundation.FCLabel();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdDone = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdDone);
			this.BottomPanel.Location = new System.Drawing.Point(0, 442);
			this.BottomPanel.Size = new System.Drawing.Size(654, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.List1);
			this.ClientArea.Controls.Add(this.lstAccounts);
			this.ClientArea.Controls.Add(this.txtAcctNum);
			this.ClientArea.Controls.Add(this.Label29);
			this.ClientArea.Controls.Add(this.Label28);
			this.ClientArea.Controls.Add(this.Label27);
			this.ClientArea.Controls.Add(this.Label26);
			this.ClientArea.Controls.Add(this.Label25);
			this.ClientArea.Controls.Add(this.Label24);
			this.ClientArea.Controls.Add(this.Label23);
			this.ClientArea.Controls.Add(this.Label22);
			this.ClientArea.Controls.Add(this.Label21);
			this.ClientArea.Controls.Add(this.Label20);
			this.ClientArea.Controls.Add(this.Label19);
			this.ClientArea.Controls.Add(this.Label18);
			this.ClientArea.Controls.Add(this.Label17);
			this.ClientArea.Controls.Add(this.Label16);
			this.ClientArea.Controls.Add(this.Label15);
			this.ClientArea.Controls.Add(this.Label14);
			this.ClientArea.Controls.Add(this.Label13);
			this.ClientArea.Controls.Add(this.Label12);
			this.ClientArea.Controls.Add(this.Label11);
			this.ClientArea.Controls.Add(this.Label10);
			this.ClientArea.Controls.Add(this.Label9);
			this.ClientArea.Controls.Add(this.Label8);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(654, 382);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(654, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(160, 30);
			this.HeaderText.Text = "Get Accounts";
			// 
			// List1
			// 
			this.List1.Appearance = 0;
			this.List1.BackColor = System.Drawing.SystemColors.Window;
			this.List1.Location = new System.Drawing.Point(30, 201);
			this.List1.MultiSelect = 0;
			this.List1.Name = "List1";
			this.List1.Size = new System.Drawing.Size(88, 103);
			this.List1.Sorted = false;
			this.List1.TabIndex = 33;
			this.List1.KeyDown += new Wisej.Web.KeyEventHandler(this.List1_KeyDown);
			// 
			// lstAccounts
			// 
			this.lstAccounts.Appearance = 0;
			this.lstAccounts.BackColor = System.Drawing.SystemColors.Window;
			this.lstAccounts.Location = new System.Drawing.Point(172, 201);
			this.lstAccounts.MultiSelect = 0;
			this.lstAccounts.Name = "lstAccounts";
			this.lstAccounts.Size = new System.Drawing.Size(460, 141);
			this.lstAccounts.Sorted = false;
			this.lstAccounts.TabIndex = 3;
			this.lstAccounts.Click += new System.EventHandler(this.lstAccounts_Click);
			// 
			// txtAcctNum
			// 
			this.txtAcctNum.AutoSize = false;
			this.txtAcctNum.BackColor = System.Drawing.SystemColors.Window;
			this.txtAcctNum.LinkItem = null;
			this.txtAcctNum.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAcctNum.LinkTopic = null;
			this.txtAcctNum.Location = new System.Drawing.Point(30, 141);
			this.txtAcctNum.Name = "txtAcctNum";
			this.txtAcctNum.Size = new System.Drawing.Size(92, 40);
			this.txtAcctNum.TabIndex = 0;
			this.txtAcctNum.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAcctNum_KeyDown);
			// 
			// Label29
			// 
			this.Label29.Location = new System.Drawing.Point(359, 169);
			this.Label29.Name = "Label29";
			this.Label29.Size = new System.Drawing.Size(108, 15);
			this.Label29.TabIndex = 32;
			this.Label29.Text = "NUMERIC/OTHER";
			this.Label29.Click += new System.EventHandler(this.Label29_Click);
			// 
			// Label28
			// 
			this.Label28.Location = new System.Drawing.Point(515, 135);
			this.Label28.Name = "Label28";
			this.Label28.Size = new System.Drawing.Size(13, 16);
			this.Label28.TabIndex = 31;
			this.Label28.Text = "Z";
			this.Label28.Click += new System.EventHandler(this.Label28_Click);
			// 
			// Label27
			// 
			this.Label27.Location = new System.Drawing.Point(497, 135);
			this.Label27.Name = "Label27";
			this.Label27.Size = new System.Drawing.Size(13, 16);
			this.Label27.TabIndex = 30;
			this.Label27.Text = "Y";
			this.Label27.Click += new System.EventHandler(this.Label27_Click);
			// 
			// Label26
			// 
			this.Label26.Location = new System.Drawing.Point(478, 135);
			this.Label26.Name = "Label26";
			this.Label26.Size = new System.Drawing.Size(13, 16);
			this.Label26.TabIndex = 29;
			this.Label26.Text = "X";
			this.Label26.Click += new System.EventHandler(this.Label26_Click);
			// 
			// Label25
			// 
			this.Label25.Location = new System.Drawing.Point(458, 135);
			this.Label25.Name = "Label25";
			this.Label25.Size = new System.Drawing.Size(17, 16);
			this.Label25.TabIndex = 28;
			this.Label25.Text = "W";
			this.Label25.Click += new System.EventHandler(this.Label25_Click);
			// 
			// Label24
			// 
			this.Label24.Location = new System.Drawing.Point(442, 135);
			this.Label24.Name = "Label24";
			this.Label24.Size = new System.Drawing.Size(13, 16);
			this.Label24.TabIndex = 27;
			this.Label24.Text = "V";
			this.Label24.Click += new System.EventHandler(this.Label24_Click);
			// 
			// Label23
			// 
			this.Label23.Location = new System.Drawing.Point(424, 135);
			this.Label23.Name = "Label23";
			this.Label23.Size = new System.Drawing.Size(13, 16);
			this.Label23.TabIndex = 26;
			this.Label23.Text = "U";
			this.Label23.Click += new System.EventHandler(this.Label23_Click);
			// 
			// Label22
			// 
			this.Label22.Location = new System.Drawing.Point(406, 135);
			this.Label22.Name = "Label22";
			this.Label22.Size = new System.Drawing.Size(13, 16);
			this.Label22.TabIndex = 25;
			this.Label22.Text = "T";
			this.Label22.Click += new System.EventHandler(this.Label22_Click);
			// 
			// Label21
			// 
			this.Label21.Location = new System.Drawing.Point(387, 135);
			this.Label21.Name = "Label21";
			this.Label21.Size = new System.Drawing.Size(13, 16);
			this.Label21.TabIndex = 24;
			this.Label21.Text = "S";
			this.Label21.Click += new System.EventHandler(this.Label21_Click);
			// 
			// Label20
			// 
			this.Label20.Location = new System.Drawing.Point(369, 135);
			this.Label20.Name = "Label20";
			this.Label20.Size = new System.Drawing.Size(13, 16);
			this.Label20.TabIndex = 23;
			this.Label20.Text = "R";
			this.Label20.Click += new System.EventHandler(this.Label20_Click);
			// 
			// Label19
			// 
			this.Label19.Location = new System.Drawing.Point(351, 135);
			this.Label19.Name = "Label19";
			this.Label19.Size = new System.Drawing.Size(13, 16);
			this.Label19.TabIndex = 22;
			this.Label19.Text = "Q";
			this.Label19.Click += new System.EventHandler(this.Label19_Click);
			// 
			// Label18
			// 
			this.Label18.Location = new System.Drawing.Point(333, 135);
			this.Label18.Name = "Label18";
			this.Label18.Size = new System.Drawing.Size(13, 16);
			this.Label18.TabIndex = 21;
			this.Label18.Text = "P";
			this.Label18.Click += new System.EventHandler(this.Label18_Click);
			// 
			// Label17
			// 
			this.Label17.Location = new System.Drawing.Point(315, 135);
			this.Label17.Name = "Label17";
			this.Label17.Size = new System.Drawing.Size(13, 16);
			this.Label17.TabIndex = 20;
			this.Label17.Text = "O";
			this.Label17.Click += new System.EventHandler(this.Label17_Click);
			// 
			// Label16
			// 
			this.Label16.Location = new System.Drawing.Point(296, 135);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(13, 16);
			this.Label16.TabIndex = 19;
			this.Label16.Text = "N";
			this.Label16.Click += new System.EventHandler(this.Label16_Click);
			// 
			// Label15
			// 
			this.Label15.Location = new System.Drawing.Point(515, 103);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(13, 16);
			this.Label15.TabIndex = 18;
			this.Label15.Text = "M";
			this.Label15.Click += new System.EventHandler(this.Label15_Click);
			// 
			// Label14
			// 
			this.Label14.Location = new System.Drawing.Point(497, 103);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(13, 16);
			this.Label14.TabIndex = 17;
			this.Label14.Text = "L";
			this.Label14.Click += new System.EventHandler(this.Label14_Click);
			// 
			// Label13
			// 
			this.Label13.Location = new System.Drawing.Point(478, 103);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(13, 16);
			this.Label13.TabIndex = 16;
			this.Label13.Text = "K";
			this.Label13.Click += new System.EventHandler(this.Label13_Click);
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(460, 103);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(13, 16);
			this.Label12.TabIndex = 15;
			this.Label12.Text = "J";
			this.Label12.Click += new System.EventHandler(this.Label12_Click);
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(442, 103);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(13, 16);
			this.Label11.TabIndex = 14;
			this.Label11.Text = "I";
			this.Label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.Label11.Click += new System.EventHandler(this.Label11_Click);
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(424, 103);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(13, 16);
			this.Label10.TabIndex = 13;
			this.Label10.Text = "H";
			this.Label10.Click += new System.EventHandler(this.Label10_Click);
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(406, 103);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(13, 16);
			this.Label9.TabIndex = 12;
			this.Label9.Text = "G";
			this.Label9.Click += new System.EventHandler(this.Label9_Click);
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(387, 103);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(13, 16);
			this.Label8.TabIndex = 11;
			this.Label8.Text = "F";
			this.Label8.Click += new System.EventHandler(this.Label8_Click);
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(369, 103);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(13, 16);
			this.Label7.TabIndex = 10;
			this.Label7.Text = "E";
			this.Label7.Click += new System.EventHandler(this.Label7_Click);
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(351, 103);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(13, 16);
			this.Label6.TabIndex = 9;
			this.Label6.Text = "D";
			this.Label6.Click += new System.EventHandler(this.Label6_Click);
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(333, 103);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(13, 16);
			this.Label5.TabIndex = 8;
			this.Label5.Text = "C";
			this.Label5.Click += new System.EventHandler(this.Label5_Click);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(315, 103);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(13, 16);
			this.Label4.TabIndex = 7;
			this.Label4.Text = "B";
			this.Label4.Click += new System.EventHandler(this.Label4_Click);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(296, 103);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(13, 16);
			this.Label3.TabIndex = 6;
			this.Label3.Text = "A";
			this.Label3.Click += new System.EventHandler(this.Label3_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(610, 64);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "PLEASE CHOOSE THE ACCOUNTS YOU WISH TO PRINT BY ENTERING THE NUMBER OR BY DOUBLE-" + "CLICKING AN ACCOUNT IN THE ACCOUNT LIST AFTER CHOOSING THE SEARCH GROUP BY CLICK" + "ING ON A CATEGORY BELOW";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(24, 103);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(120, 18);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "ACCOUNT NUMBER";
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "acceptButton";
			this.cmdDone.Location = new System.Drawing.Point(288, 30);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Size = new System.Drawing.Size(100, 48);
			this.cmdDone.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdDone.TabIndex = 5;
			this.cmdDone.Text = "Continue";
			this.cmdDone.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// frmGetAccountsToPrint
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(654, 550);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.Name = "frmGetAccountsToPrint";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Get Accounts";
			this.Load += new System.EventHandler(this.frmGetAccountsToPrint_Load);
			this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmGetAccountsToPrint_FormClosing);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetAccountsToPrint_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdDone;
	}
}
