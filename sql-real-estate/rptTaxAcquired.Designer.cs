﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptTaxAcquired.
	/// </summary>
	partial class rptTaxAcquired
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxAcquired));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtMapLot,
				this.txtLocation,
				this.txtAssessment
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotalAssessment,
				this.Line1,
				this.Field1
			});
			this.ReportFooter.Height = 0.3541667F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.lblTitle,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5
			});
			this.PageHeader.Height = 0.7291667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6.395833F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.041667F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Tax Acquired";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 3.041667F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.02083333F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label1.Text = "Account";
			this.Label1.Top = 0.5208333F;
			this.Label1.Width = 0.65625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.2083333F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.65625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Text = "Name";
			this.Label2.Top = 0.5208333F;
			this.Label2.Width = 2.239583F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.2083333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.052083F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label3.Text = "Map/Lot";
			this.Label3.Top = 0.5208333F;
			this.Label3.Width = 1.239583F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.2083333F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.447917F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label4.Text = "Location";
			this.Label4.Top = 0.5208333F;
			this.Label4.Width = 1.15625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.2083333F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 6.520833F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label5.Text = "Assessment";
			this.Label5.Top = 0.5208333F;
			this.Label5.Width = 0.90625F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0.03125F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.01041667F;
			this.txtAccount.Width = 0.5833333F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.65625F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = null;
			this.txtName.Top = 0.01041667F;
			this.txtName.Width = 2.3125F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.1875F;
			this.txtMapLot.Left = 3.052083F;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-family: \'Tahoma\'";
			this.txtMapLot.Text = "000-999-000-999-000";
			this.txtMapLot.Top = 0.01041667F;
			this.txtMapLot.Width = 1.3125F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 4.447917F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.01041667F;
			this.txtLocation.Width = 2.041667F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.Height = 0.1875F;
			this.txtAssessment.Left = 6.53125F;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 0.01041667F;
			this.txtAssessment.Width = 0.8958333F;
			// 
			// txtTotalAssessment
			// 
			this.txtTotalAssessment.Height = 0.1875F;
			this.txtTotalAssessment.Left = 6.197917F;
			this.txtTotalAssessment.Name = "txtTotalAssessment";
			this.txtTotalAssessment.Style = "text-align: right";
			this.txtTotalAssessment.Text = null;
			this.txtTotalAssessment.Top = 0.1666667F;
			this.txtTotalAssessment.Width = 1.229167F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.08333334F;
			this.Line1.LineWeight = 2F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.08333334F;
			this.Line1.Width = 7.3125F;
			this.Line1.X1 = 0.08333334F;
			this.Line1.X2 = 7.395833F;
			this.Line1.Y1 = 0.08333334F;
			this.Line1.Y2 = 0.08333334F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 4.864583F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-weight: bold; text-align: right";
			this.Field1.Text = "Total";
			this.Field1.Top = 0.1666667F;
			this.Field1.Width = 1.229167F;
			// 
			// rptTaxAcquired
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
