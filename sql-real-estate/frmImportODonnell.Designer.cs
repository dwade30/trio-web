﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Xml;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportODonnell.
	/// </summary>
	partial class frmImportODonnell : BaseForm
	{
		public fecherFoundation.FCComboBox cmbUpdate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public FCGrid gridNotUpdated;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkSale;
		public fecherFoundation.FCCheckBox chkTGPlanDue;
		public fecherFoundation.FCCheckBox chkTGYear;
		public fecherFoundation.FCCheckBox chkPhone;
		public fecherFoundation.FCCheckBox chkRef1;
		public fecherFoundation.FCCheckBox chkBookPage;
		public fecherFoundation.FCCheckBox chkAcreage;
		public fecherFoundation.FCCheckBox chkLocation;
		public fecherFoundation.FCCheckBox chkNameAddress;
		public fecherFoundation.FCCheckBox chkExemption;
		public fecherFoundation.FCCheckBox chkBldg;
		public fecherFoundation.FCCheckBox chkLand;
		public fecherFoundation.FCCheckBox chkMapLot;
		public FCGrid Grid;
		public fecherFoundation.FCLabel lblTotal;
		public fecherFoundation.FCLabel lblNew;
		public fecherFoundation.FCLabel lblUpdated;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel lblProgress;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuMarkDeleted;
		public fecherFoundation.FCToolStripMenuItem mnuClearLand;
		public fecherFoundation.FCToolStripMenuItem mnuClearAcreage;
		public fecherFoundation.FCToolStripMenuItem mnuClearTreeGrowth;
		public fecherFoundation.FCToolStripMenuItem mnuClearTGAcreage;
		public fecherFoundation.FCToolStripMenuItem mnuClearBldg;
		public fecherFoundation.FCToolStripMenuItem mnuClearExempts;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuPrintNotUpdated;
		public fecherFoundation.FCToolStripMenuItem mnuImportedReport;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuImport;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImportODonnell));
			this.cmbUpdate = new fecherFoundation.FCComboBox();
			this.gridNotUpdated = new fecherFoundation.FCGrid();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkSale = new fecherFoundation.FCCheckBox();
			this.chkTGPlanDue = new fecherFoundation.FCCheckBox();
			this.chkTGYear = new fecherFoundation.FCCheckBox();
			this.chkPhone = new fecherFoundation.FCCheckBox();
			this.chkRef1 = new fecherFoundation.FCCheckBox();
			this.chkBookPage = new fecherFoundation.FCCheckBox();
			this.chkAcreage = new fecherFoundation.FCCheckBox();
			this.chkLocation = new fecherFoundation.FCCheckBox();
			this.chkNameAddress = new fecherFoundation.FCCheckBox();
			this.chkExemption = new fecherFoundation.FCCheckBox();
			this.chkBldg = new fecherFoundation.FCCheckBox();
			this.chkLand = new fecherFoundation.FCCheckBox();
			this.chkMapLot = new fecherFoundation.FCCheckBox();
			this.Grid = new fecherFoundation.FCGrid();
			this.lblTotal = new fecherFoundation.FCLabel();
			this.lblNew = new fecherFoundation.FCLabel();
			this.lblUpdated = new fecherFoundation.FCLabel();
			this.Label1_2 = new fecherFoundation.FCLabel();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.lblProgress = new fecherFoundation.FCLabel();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuMarkDeleted = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClearLand = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClearAcreage = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClearTreeGrowth = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClearTGAcreage = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClearBldg = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClearExempts = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintNotUpdated = new fecherFoundation.FCToolStripMenuItem();
			this.mnuImportedReport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuImport = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdImport = new fecherFoundation.FCButton();
			this.cmdImported = new fecherFoundation.FCButton();
			this.cmdSummary = new fecherFoundation.FCButton();
			this.cmdNotUpdated = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSale)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTGPlanDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTGYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRef1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAcreage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNameAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdImport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdImported)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNotUpdated)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdImport);
			this.BottomPanel.Location = new System.Drawing.Point(0, 409);
			this.BottomPanel.Size = new System.Drawing.Size(673, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbUpdate);
			this.ClientArea.Controls.Add(this.gridNotUpdated);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Controls.Add(this.lblTotal);
			this.ClientArea.Controls.Add(this.lblNew);
			this.ClientArea.Controls.Add(this.lblUpdated);
			this.ClientArea.Controls.Add(this.Label1_2);
			this.ClientArea.Controls.Add(this.Label1_1);
			this.ClientArea.Controls.Add(this.Label1_0);
			this.ClientArea.Controls.Add(this.lblProgress);
			this.ClientArea.Size = new System.Drawing.Size(673, 349);
			this.ClientArea.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNotUpdated);
			this.TopPanel.Controls.Add(this.cmdSummary);
			this.TopPanel.Controls.Add(this.cmdImported);
			this.TopPanel.Size = new System.Drawing.Size(673, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdImported, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSummary, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNotUpdated, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(190, 30);
			this.HeaderText.Text = "Import Accounts";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbUpdate
			// 
			this.cmbUpdate.AutoSize = false;
			this.cmbUpdate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbUpdate.FormattingEnabled = true;
			this.cmbUpdate.Items.AddRange(new object[] {
				"Update existing",
				"Import new"
			});
			this.cmbUpdate.Location = new System.Drawing.Point(30, 349);
			this.cmbUpdate.Name = "cmbUpdate";
			this.cmbUpdate.Size = new System.Drawing.Size(253, 40);
			this.cmbUpdate.TabIndex = 0;
			this.cmbUpdate.Text = "Update existing";
			this.ToolTip1.SetToolTip(this.cmbUpdate, null);
			// 
			// gridNotUpdated
			// 
			this.gridNotUpdated.AllowSelection = false;
			this.gridNotUpdated.AllowUserToResizeColumns = false;
			this.gridNotUpdated.AllowUserToResizeRows = false;
			this.gridNotUpdated.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridNotUpdated.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridNotUpdated.BackColorBkg = System.Drawing.Color.Empty;
			this.gridNotUpdated.BackColorFixed = System.Drawing.Color.Empty;
			this.gridNotUpdated.BackColorSel = System.Drawing.Color.Empty;
			this.gridNotUpdated.Cols = 13;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridNotUpdated.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.gridNotUpdated.ColumnHeadersHeight = 30;
			this.gridNotUpdated.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridNotUpdated.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridNotUpdated.DefaultCellStyle = dataGridViewCellStyle2;
			this.gridNotUpdated.DragIcon = null;
			this.gridNotUpdated.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridNotUpdated.FixedRows = 0;
			this.gridNotUpdated.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridNotUpdated.FrozenCols = 0;
			this.gridNotUpdated.GridColor = System.Drawing.Color.Empty;
			this.gridNotUpdated.GridColorFixed = System.Drawing.Color.Empty;
			this.gridNotUpdated.Location = new System.Drawing.Point(348, 385);
			this.gridNotUpdated.Name = "gridNotUpdated";
			this.gridNotUpdated.ReadOnly = true;
			this.gridNotUpdated.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridNotUpdated.RowHeightMin = 0;
			this.gridNotUpdated.Rows = 0;
			this.gridNotUpdated.ScrollTipText = null;
			this.gridNotUpdated.ShowColumnVisibilityMenu = false;
			this.gridNotUpdated.Size = new System.Drawing.Size(16, 4);
			this.gridNotUpdated.StandardTab = true;
			this.gridNotUpdated.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridNotUpdated.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridNotUpdated.TabIndex = 22;
			this.ToolTip1.SetToolTip(this.gridNotUpdated, null);
			this.gridNotUpdated.Visible = false;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkSale);
			this.Frame1.Controls.Add(this.chkTGPlanDue);
			this.Frame1.Controls.Add(this.chkTGYear);
			this.Frame1.Controls.Add(this.chkPhone);
			this.Frame1.Controls.Add(this.chkRef1);
			this.Frame1.Controls.Add(this.chkBookPage);
			this.Frame1.Controls.Add(this.chkAcreage);
			this.Frame1.Controls.Add(this.chkLocation);
			this.Frame1.Controls.Add(this.chkNameAddress);
			this.Frame1.Controls.Add(this.chkExemption);
			this.Frame1.Controls.Add(this.chkBldg);
			this.Frame1.Controls.Add(this.chkLand);
			this.Frame1.Controls.Add(this.chkMapLot);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(452, 299);
			this.Frame1.TabIndex = 21;
			this.Frame1.Text = "Update";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// chkSale
			// 
			this.chkSale.Location = new System.Drawing.Point(187, 215);
			this.chkSale.Name = "chkSale";
			this.chkSale.Size = new System.Drawing.Size(147, 27);
			this.chkSale.TabIndex = 12;
			this.chkSale.Text = "Sale Information";
			this.ToolTip1.SetToolTip(this.chkSale, "Year tree growth management plan is due");
			// 
			// chkTGPlanDue
			// 
			this.chkTGPlanDue.Location = new System.Drawing.Point(20, 252);
			this.chkTGPlanDue.Name = "chkTGPlanDue";
			this.chkTGPlanDue.Size = new System.Drawing.Size(122, 27);
			this.chkTGPlanDue.TabIndex = 6;
			this.chkTGPlanDue.Text = "TG Plan Due";
			this.ToolTip1.SetToolTip(this.chkTGPlanDue, "Year tree growth management plan is due");
			// 
			// chkTGYear
			// 
			this.chkTGYear.Location = new System.Drawing.Point(20, 215);
			this.chkTGYear.Name = "chkTGYear";
			this.chkTGYear.Size = new System.Drawing.Size(125, 27);
			this.chkTGYear.TabIndex = 5;
			this.chkTGYear.Text = "TG First Year";
			this.ToolTip1.SetToolTip(this.chkTGYear, "Year first classified as tree growth");
			// 
			// chkPhone
			// 
			this.chkPhone.Location = new System.Drawing.Point(187, 67);
			this.chkPhone.Name = "chkPhone";
			this.chkPhone.Size = new System.Drawing.Size(137, 27);
			this.chkPhone.TabIndex = 8;
			this.chkPhone.Text = "Phone Number";
			this.ToolTip1.SetToolTip(this.chkPhone, null);
			// 
			// chkRef1
			// 
			this.chkRef1.Location = new System.Drawing.Point(187, 178);
			this.chkRef1.Name = "chkRef1";
			this.chkRef1.Size = new System.Drawing.Size(66, 27);
			this.chkRef1.TabIndex = 11;
			this.chkRef1.Text = "Ref 1";
			this.ToolTip1.SetToolTip(this.chkRef1, null);
			// 
			// chkBookPage
			// 
			this.chkBookPage.Location = new System.Drawing.Point(187, 141);
			this.chkBookPage.Name = "chkBookPage";
			this.chkBookPage.Size = new System.Drawing.Size(123, 27);
			this.chkBookPage.TabIndex = 10;
			this.chkBookPage.Text = "Book & Page";
			this.ToolTip1.SetToolTip(this.chkBookPage, null);
			// 
			// chkAcreage
			// 
			this.chkAcreage.Location = new System.Drawing.Point(20, 178);
			this.chkAcreage.Name = "chkAcreage";
			this.chkAcreage.Size = new System.Drawing.Size(88, 27);
			this.chkAcreage.TabIndex = 4;
			this.chkAcreage.Text = "Acreage";
			this.ToolTip1.SetToolTip(this.chkAcreage, null);
			// 
			// chkLocation
			// 
			this.chkLocation.Location = new System.Drawing.Point(187, 104);
			this.chkLocation.Name = "chkLocation";
			this.chkLocation.Size = new System.Drawing.Size(89, 27);
			this.chkLocation.TabIndex = 9;
			this.chkLocation.Text = "Location";
			this.ToolTip1.SetToolTip(this.chkLocation, null);
			// 
			// chkNameAddress
			// 
			this.chkNameAddress.Location = new System.Drawing.Point(187, 30);
			this.chkNameAddress.Name = "chkNameAddress";
			this.chkNameAddress.Size = new System.Drawing.Size(151, 27);
			this.chkNameAddress.TabIndex = 7;
			this.chkNameAddress.Text = "Name & Address";
			this.ToolTip1.SetToolTip(this.chkNameAddress, null);
			// 
			// chkExemption
			// 
			this.chkExemption.Location = new System.Drawing.Point(20, 141);
			this.chkExemption.Name = "chkExemption";
			this.chkExemption.Size = new System.Drawing.Size(105, 27);
			this.chkExemption.TabIndex = 3;
			this.chkExemption.Text = "Exemption";
			this.ToolTip1.SetToolTip(this.chkExemption, null);
			// 
			// chkBldg
			// 
			this.chkBldg.Location = new System.Drawing.Point(20, 104);
			this.chkBldg.Name = "chkBldg";
			this.chkBldg.Size = new System.Drawing.Size(86, 27);
			this.chkBldg.TabIndex = 2;
			this.chkBldg.Text = "Building";
			this.ToolTip1.SetToolTip(this.chkBldg, null);
			// 
			// chkLand
			// 
			this.chkLand.Location = new System.Drawing.Point(20, 67);
			this.chkLand.Name = "chkLand";
			this.chkLand.Size = new System.Drawing.Size(63, 27);
			this.chkLand.TabIndex = 1;
			this.chkLand.Text = "Land";
			this.ToolTip1.SetToolTip(this.chkLand, null);
			// 
			// chkMapLot
			// 
			this.chkMapLot.Location = new System.Drawing.Point(20, 30);
			this.chkMapLot.Name = "chkMapLot";
			this.chkMapLot.Size = new System.Drawing.Size(86, 27);
			this.chkMapLot.TabIndex = 0;
			this.chkMapLot.Text = "Map/Lot";
			this.ToolTip1.SetToolTip(this.chkMapLot, null);
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.Cols = 14;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.Grid.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle4;
			this.Grid.DragIcon = null;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.FixedCols = 0;
			this.Grid.FixedRows = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(360, 72);
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 0;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(20, 27);
			this.Grid.StandardTab = true;
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.Grid, null);
			this.Grid.Visible = false;
			// 
			// lblTotal
			// 
			this.lblTotal.Location = new System.Drawing.Point(340, 456);
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Size = new System.Drawing.Size(47, 19);
			this.lblTotal.TabIndex = 20;
			this.lblTotal.Text = "0";
			this.ToolTip1.SetToolTip(this.lblTotal, null);
			// 
			// lblNew
			// 
			this.lblNew.Location = new System.Drawing.Point(216, 456);
			this.lblNew.Name = "lblNew";
			this.lblNew.Size = new System.Drawing.Size(47, 19);
			this.lblNew.TabIndex = 19;
			this.lblNew.Text = "0";
			this.ToolTip1.SetToolTip(this.lblNew, null);
			// 
			// lblUpdated
			// 
			this.lblUpdated.Location = new System.Drawing.Point(99, 456);
			this.lblUpdated.Name = "lblUpdated";
			this.lblUpdated.Size = new System.Drawing.Size(47, 19);
			this.lblUpdated.TabIndex = 18;
			this.lblUpdated.Text = "0";
			this.ToolTip1.SetToolTip(this.lblUpdated, null);
			// 
			// Label1_2
			// 
			this.Label1_2.Location = new System.Drawing.Point(292, 456);
			this.Label1_2.Name = "Label1_2";
			this.Label1_2.Size = new System.Drawing.Size(38, 19);
			this.Label1_2.TabIndex = 17;
			this.Label1_2.Text = "TOTAL";
			this.ToolTip1.SetToolTip(this.Label1_2, null);
			// 
			// Label1_1
			// 
			this.Label1_1.Location = new System.Drawing.Point(173, 456);
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.Size = new System.Drawing.Size(35, 19);
			this.Label1_1.TabIndex = 16;
			this.Label1_1.Text = "NEW";
			this.ToolTip1.SetToolTip(this.Label1_1, null);
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(30, 456);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(60, 19);
			this.Label1_0.TabIndex = 15;
			this.Label1_0.Text = "UPDATED";
			this.ToolTip1.SetToolTip(this.Label1_0, null);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(30, 409);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(355, 27);
			this.lblProgress.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.lblProgress, null);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuMarkDeleted,
				this.mnuClearLand,
				this.mnuClearAcreage,
				this.mnuClearTreeGrowth,
				this.mnuClearTGAcreage,
				this.mnuClearBldg,
				this.mnuClearExempts
			});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuMarkDeleted
			// 
			this.mnuMarkDeleted.Index = 0;
			this.mnuMarkDeleted.Name = "mnuMarkDeleted";
			this.mnuMarkDeleted.Text = "Mark All Deleted";
			this.mnuMarkDeleted.Click += new System.EventHandler(this.mnuMarkDeleted_Click);
			// 
			// mnuClearLand
			// 
			this.mnuClearLand.Index = 1;
			this.mnuClearLand.Name = "mnuClearLand";
			this.mnuClearLand.Text = "Clear Land Values";
			this.mnuClearLand.Click += new System.EventHandler(this.mnuClearLand_Click);
			// 
			// mnuClearAcreage
			// 
			this.mnuClearAcreage.Index = 2;
			this.mnuClearAcreage.Name = "mnuClearAcreage";
			this.mnuClearAcreage.Text = "Clear Acreage";
			this.mnuClearAcreage.Click += new System.EventHandler(this.mnuClearAcreage_Click);
			// 
			// mnuClearTreeGrowth
			// 
			this.mnuClearTreeGrowth.Index = 3;
			this.mnuClearTreeGrowth.Name = "mnuClearTreeGrowth";
			this.mnuClearTreeGrowth.Text = "Clear Tree Growth Values";
			this.mnuClearTreeGrowth.Click += new System.EventHandler(this.mnuClearTreeGrowth_Click);
			// 
			// mnuClearTGAcreage
			// 
			this.mnuClearTGAcreage.Index = 4;
			this.mnuClearTGAcreage.Name = "mnuClearTGAcreage";
			this.mnuClearTGAcreage.Text = "Clear Tree Growth Acreage";
			this.mnuClearTGAcreage.Click += new System.EventHandler(this.mnuClearTGAcreage_Click);
			// 
			// mnuClearBldg
			// 
			this.mnuClearBldg.Index = 5;
			this.mnuClearBldg.Name = "mnuClearBldg";
			this.mnuClearBldg.Text = "Clear Building Values";
			this.mnuClearBldg.Click += new System.EventHandler(this.mnuClearBldg_Click);
			// 
			// mnuClearExempts
			// 
			this.mnuClearExempts.Index = 6;
			this.mnuClearExempts.Name = "mnuClearExempts";
			this.mnuClearExempts.Text = "Clear Exemptions";
			this.mnuClearExempts.Click += new System.EventHandler(this.mnuClearExempts_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSepar3,
				this.mnuPrintNotUpdated,
				this.mnuImportedReport,
				this.mnuPrintPreview,
				this.mnuSepar2,
				this.mnuImport,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = 0;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuPrintNotUpdated
			// 
			this.mnuPrintNotUpdated.Index = 1;
			this.mnuPrintNotUpdated.Name = "mnuPrintNotUpdated";
			this.mnuPrintNotUpdated.Text = "Not Updated Report";
			this.mnuPrintNotUpdated.Click += new System.EventHandler(this.mnuPrintNotUpdated_Click);
			// 
			// mnuImportedReport
			// 
			this.mnuImportedReport.Index = 2;
			this.mnuImportedReport.Name = "mnuImportedReport";
			this.mnuImportedReport.Text = "Imported Report";
			this.mnuImportedReport.Click += new System.EventHandler(this.mnuImportedReport_Click);
			// 
			// mnuPrintPreview
			// 
			this.mnuPrintPreview.Index = 3;
			this.mnuPrintPreview.Name = "mnuPrintPreview";
			this.mnuPrintPreview.Text = "Summary Report";
			this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 4;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuImport
			// 
			this.mnuImport.Index = 5;
			this.mnuImport.Name = "mnuImport";
			this.mnuImport.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuImport.Text = "Import File";
			this.mnuImport.Click += new System.EventHandler(this.mnuImport_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 6;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 7;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdImport
			// 
			this.cmdImport.AppearanceKey = "acceptButton";
			this.cmdImport.Location = new System.Drawing.Point(277, 30);
			this.cmdImport.Name = "cmdImport";
			this.cmdImport.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdImport.Size = new System.Drawing.Size(110, 48);
			this.cmdImport.TabIndex = 0;
			this.cmdImport.Text = "Import File";
			this.ToolTip1.SetToolTip(this.cmdImport, null);
			this.cmdImport.Click += new System.EventHandler(this.mnuImport_Click);
			// 
			// cmdImported
			// 
			this.cmdImported.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdImported.AppearanceKey = "toolbarButton";
			this.cmdImported.Location = new System.Drawing.Point(527, 29);
			this.cmdImported.Name = "cmdImported";
			this.cmdImported.Size = new System.Drawing.Size(118, 24);
			this.cmdImported.TabIndex = 1;
			this.cmdImported.Text = "Imported Report";
			this.ToolTip1.SetToolTip(this.cmdImported, null);
			this.cmdImported.Click += new System.EventHandler(this.mnuImportedReport_Click);
			// 
			// cmdSummary
			// 
			this.cmdSummary.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSummary.AppearanceKey = "toolbarButton";
			this.cmdSummary.Location = new System.Drawing.Point(401, 29);
			this.cmdSummary.Name = "cmdSummary";
			this.cmdSummary.Size = new System.Drawing.Size(120, 24);
			this.cmdSummary.TabIndex = 2;
			this.cmdSummary.Text = "Summary Report";
			this.ToolTip1.SetToolTip(this.cmdSummary, null);
			this.cmdSummary.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// cmdNotUpdated
			// 
			this.cmdNotUpdated.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNotUpdated.AppearanceKey = "toolbarButton";
			this.cmdNotUpdated.Location = new System.Drawing.Point(253, 29);
			this.cmdNotUpdated.Name = "cmdNotUpdated";
			this.cmdNotUpdated.Size = new System.Drawing.Size(142, 24);
			this.cmdNotUpdated.TabIndex = 3;
			this.cmdNotUpdated.Text = "Not Updated Report";
			this.ToolTip1.SetToolTip(this.cmdNotUpdated, null);
			this.cmdNotUpdated.Click += new System.EventHandler(this.mnuPrintNotUpdated_Click);
			// 
			// frmImportODonnell
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(673, 517);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmImportODonnell";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Import Accounts";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmImportODonnell_Load);
			this.Activated += new System.EventHandler(this.frmImportODonnell_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmImportODonnell_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSale)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTGPlanDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTGYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRef1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAcreage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNameAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdImport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdImported)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNotUpdated)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdImport;
		private FCButton cmdNotUpdated;
		private FCButton cmdSummary;
		private FCButton cmdImported;
	}
}
