﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Wisej.Web;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptRange1PageProperty.
	/// </summary>
	public partial class rptRange1PageProperty : BaseSectionReport
	{
		public static rptRange1PageProperty InstancePtr
		{
			get
			{
				return (rptRange1PageProperty)Sys.GetInstance(typeof(rptRange1PageProperty));
			}
		}

		protected rptRange1PageProperty _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptRange1PageProperty()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Property Card";
		}
		// nObj = 1
		//   0	rptRange1PageProperty	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// Dim dbTemp As DAO.Database
		int NumRecords;
		bool boolSpecific;
		bool boolFromExtract;
		bool boolDidntUpdateHeader;
		int intCards;
		int intTownNumber;
		bool boolFirstRec;
		private bool boolOnlyChanges;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			boolFirstRec = true;
		}
		// vbPorter upgrade warning: intRangeType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intOrderType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intTownCode As short	OnWriteFCConvert.ToInt32(
		public void Init(ref int intRangeType, ref int intOrderType, bool modalDialog, int intTownCode = -1, bool boolModal = false, bool boolChangesOnly = false)
		{
			intTownNumber = intTownCode;
			boolSpecific = false;
			boolOnlyChanges = boolChangesOnly;
			switch (intRangeType)
			{
				case 0:
					{
						// no range
						modGlobalVariables.Statics.boolByRange = false;
						boolSpecific = false;
						boolFromExtract = false;
						break;
					}
				case 1:
					{
						// range
						modGlobalVariables.Statics.boolByRange = true;
						boolSpecific = false;
						boolFromExtract = false;
						break;
					}
				case 2:
					{
						// specific accounts
						modGlobalVariables.Statics.boolByRange = false;
						boolSpecific = true;
						boolFromExtract = false;
						break;
					}
				case 3:
					{
						// from extract
						modGlobalVariables.Statics.boolByRange = false;
						boolSpecific = false;
						boolFromExtract = true;
						break;
					}
				case 4:
					{
						// town code
						modGlobalVariables.Statics.boolByRange = false;
						boolSpecific = false;
						boolFromExtract = false;
						break;
					}
			}
			//end switch
			modPrintRoutines.Statics.intwhichorder = intOrderType;
			if (!boolModal)
			{
				frmReportViewer.InstancePtr.Init(this, showModal: modalDialog,allowCancel:true);
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog,allowCancel:true);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                int lngUID;
                //string strMasterJoin;
                //strMasterJoin = modREMain.GetMasterJoin();
                lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
                SubReport1.Report = new rpt1PagePropertyCard();
                if (modPrintRoutines.Statics.gstrFieldName.ToLower() == "rsname")
                {
                    modPrintRoutines.Statics.gstrFieldName = "DeedName1";
                }

                if (boolFromExtract)
                {
                    clsTemp.OpenRecordset(
                        "select * from extract where userid = " + FCConvert.ToString(lngUID) + " and reportnumber = 0",
                        modGlobalVariables.strREDatabase);
                    if (!clsTemp.EndOfFile())
                    {
                    }
                    else
                    {
                        MessageBox.Show("No extract has been done.  Cannot continue", "No Extract",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.Close();
                        return;
                    }
                }

                lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
                if (modPrintRoutines.Statics.gstrFieldName == "RSAccount")
                {
                    if (intTownNumber > 0)
                    {
                        modDataTypes.Statics.MR.OpenRecordset(
                            "SELECT  * FROM Master where ritrancode = " + intTownNumber +
                            " and not rsdeleted = 1 order by rsaccount,RSCard", modGlobalVariables.strREDatabase);
                    }
                    else if (boolSpecific)
                    {
                        modDataTypes.Statics.MR.OpenRecordset(
                            "select  * from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " +
                            lngUID + ") order by rsaccount,rscard", modGlobalVariables.strREDatabase);
                    }
                    else if (boolFromExtract)
                    {
                        modDataTypes.Statics.MR.OpenRecordset(
                            "select * from master where (not rsdeleted = 1) and rsaccount in (select ACCOUNTNUMBER from extracttable where userid = " +
                            lngUID + ") order by rsaccount,rscard", modGlobalVariables.strREDatabase);
                    }
                    else if (modGlobalVariables.Statics.boolByRange)
                    {
                        if (modGlobalVariables.Statics.boolRegRecords)
                        {
                            modDataTypes.Statics.MR.OpenRecordset(
                                "SELECT  * FROM Master where " + modPrintRoutines.Statics.gstrFieldName + " >= " +
                                modGlobalVariables.Statics.gintMinAccountRange.ToString() + " and " +
                                modPrintRoutines.Statics.gstrFieldName + " <= " +
                                modGlobalVariables.Statics.gintMaxAccountRange + " and not rsdeleted = 1 order by " +
                                modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            modDataTypes.Statics.MR.OpenRecordset(
                                "SELECT  * FROM srMaster where " + modPrintRoutines.Statics.gstrFieldName + " >= " +
                                FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " +
                                modPrintRoutines.Statics.gstrFieldName + " <= " +
                                FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) +
                                " and not rsdeleted = 1 and saleid = " +
                                FCConvert.ToString(modGlobalVariables.Statics.glngSaleID) + " order by " +
                                modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
                        }
                    }
                    else
                    {
                        modDataTypes.Statics.MR.OpenRecordset(
                            "SELECT  * FROM Master where not rsdeleted = 1 order by " +
                            modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
                    }
                }
                else
                {
                    if (intTownNumber > 0)
                    {
                        modDataTypes.Statics.MR.OpenRecordset(
                            "SELECT  * FROM Master where ritrancode = " + intTownNumber +
                            " and not rsdeleted = 1 order by " + modPrintRoutines.Statics.gstrFieldName +
                            ",rsaccount,RSCard", modGlobalVariables.strREDatabase);
                    }
                    else if (boolSpecific)
                    {
                        modDataTypes.Statics.MR.OpenRecordset(
                            "select  * from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " +
                            lngUID + ") order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,rscard",
                            modGlobalVariables.strREDatabase);
                    }
                    else if (boolFromExtract)
                    {
                        modDataTypes.Statics.MR.OpenRecordset(
                            "select  * from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from extracttable where userid = " +
                            lngUID + ") order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,rscard",
                            modGlobalVariables.strREDatabase);
                    }
                    else if (modGlobalVariables.Statics.boolByRange)
                    {
                        // Set MR = dbTemp.OpenRecordset("SELECT  * FROM Master where " & gstrFieldName & " >= '" & gstrMinAccountRange & "' and " & gstrFieldName & " <= '" & gstrMaxAccountRange & "' and not rsdeleted = 1 order by " & gstrFieldName & ",RSCard",strredatabase)
                        modDataTypes.Statics.MR.OpenRecordset(
                            "SELECT  * FROM Master where " + modPrintRoutines.Statics.gstrFieldName + " >= '" +
                            modPrintRoutines.Statics.gstrMinAccountRange + "' and " +
                            modPrintRoutines.Statics.gstrFieldName + " <= '" +
                            modPrintRoutines.Statics.gstrMaxAccountRange + "' and not rsdeleted = 1 order by " +
                            modPrintRoutines.Statics.gstrFieldName + ",rsaccount,RSCard",
                            modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        modDataTypes.Statics.MR.OpenRecordset(
                            "SELECT  * FROM Master where not rsdeleted = 1 order by " +
                            modPrintRoutines.Statics.gstrFieldName + " ,rsaccount,RSCard",
                            modGlobalVariables.strREDatabase);
                    }
                }

                if (modDataTypes.Statics.MR.EndOfFile())
                {
                    MessageBox.Show("No Matches found", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                    return;
                }

                modDataTypes.Statics.MR.MoveLast();
                modDataTypes.Statics.MR.MoveFirst();
                modDataTypes.Statics.MR.Edit();
                NumRecords = modDataTypes.Statics.MR.RecordCount();
                //Frmassessmentprogress.InstancePtr.ProgressBar1.Maximum = NumRecords + 1;
                modGlobalVariables.Statics.gintLastAccountNumber =
                    FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
                if (modGlobalVariables.Statics.boolRegRecords)
                {
                    modDataTypes.Statics.OUT.OpenRecordset("select * from outbuilding order by rsaccount,rscard",
                        modGlobalVariables.strREDatabase);
                    modDataTypes.Statics.CMR.OpenRecordset("select * from commercial order by rsaccount,rscard",
                        modGlobalVariables.strREDatabase);
                    modDataTypes.Statics.DWL.OpenRecordset("select * from dwelling order by rsaccount,rscard",
                        modGlobalVariables.strREDatabase);
                }
                else
                {
                    modDataTypes.Statics.OUT.OpenRecordset(
                        "select * from sroutbuilding where saledate = '" + modGlobalVariables.Statics.gcurrsaledate +
                        "' order by rsaccount,rscard", modGlobalVariables.strREDatabase);
                    modDataTypes.Statics.CMR.OpenRecordset(
                        "select * from srcommercial where saledate = '" + modGlobalVariables.Statics.gcurrsaledate +
                        "' order by rsaccount,rscard", modGlobalVariables.strREDatabase);
                    modDataTypes.Statics.DWL.OpenRecordset(
                        "select * from srdwelling where saledate = '" + modGlobalVariables.Statics.gcurrsaledate +
                        "' order by rsaccount,rscard", modGlobalVariables.strREDatabase);
                }

                modSpeedCalc.REAS03_1075_GET_PARAM_RECORD();
                Array.Resize(ref modSpeedCalc.Statics.SummaryList, NumRecords + 1 + 1);
                modSpeedCalc.Statics.SummaryListIndex = 0;
                modProperty.Statics.RUNTYPE = "D";
                modProperty.Statics.boolFromProperty = true;
                modGlobalVariables.Statics.boolUseArrays = true;
                modGlobalRoutines.Check_Arrays();
            }
        }

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			modGlobalVariables.Statics.boolUseArrays = false;

			if (modSpeedCalc.Statics.boolCalcErrors && !reportCanceled)
			{
				MessageBox.Show("There were errors calculating.  Following is a list.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				//! Load frmShowResults;
				frmShowResults.InstancePtr.Init(ref modSpeedCalc.Statics.CalcLog);
				frmShowResults.InstancePtr.lblScreenTitle.Text = "Calculation Errors Log";
				frmShowResults.InstancePtr.Show();
			}
			modSpeedCalc.Statics.boolCalcErrors = false;
			modSpeedCalc.Statics.CalcLog = "";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int lngTotLand;
			int lngTotBldg;
			int lngTotTot;
			double dblOldLand;
			double dblOldBldg;
			OnceAgain:
			;

			eArgs.EOF = modDataTypes.Statics.MR.EndOfFile();
			if (eArgs.EOF)
				return;

			modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
			
		}

		
	}
}
