﻿using Global;
using System;

namespace TWRE0000
{
	public class cREAccount
	{
		//=========================================================
		private int lngID;
		private int lngAccount;
		private int lngOwnerID;
		private int lngSecOwnerID;
		private bool boolTaxAcquired;
		private bool boolInBankruptcy;
		private bool boolRevocableTrust;
		private cParty pOwner = new cParty();
		private cParty pSecOwner = new cParty();
		private int[] intExemptCode = new int[3 + 1];
		private double[] dblExemptPct = new double[3 + 1];
		private double[] dblExemptVal = new double[3 + 1];
		private string strMapLot = string.Empty;
		private int lngTranCode;
		// VBto upgrade warning: dtSaleDate As DateTime	OnWrite(DateTime, int)
		private DateTime dtSaleDate;
		private int intSaleFinancing;
		private int intSaleValidity;
		private int intSaleVerified;
		private int intSaleType;
		private string strAccountID = string.Empty;
		private double dblSalePrice;
		private string strPrevOwner = string.Empty;
        public string DeedName1 { get; set; } = "";
        public string DeedName2 { get; set; } = "";
		public string PreviousOwner
		{
			get
			{
				string PreviousOwner = "";
				PreviousOwner = strPrevOwner;
				return PreviousOwner;
			}
			set
			{
				strPrevOwner = value;
			}
		}

		public double SalePrice
		{
			get
			{
				double SalePrice = 0;
				SalePrice = dblSalePrice;
				return SalePrice;
			}
			set
			{
				dblSalePrice = value;
			}
		}

		public string AccountID
		{
			get
			{
				string AccountID = "";
				AccountID = strAccountID;
				return AccountID;
			}
			set
			{
				strAccountID = value;
			}
		}

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public int Account
		{
			get
			{
				int Account = 0;
				Account = lngAccount;
				return Account;
			}
			set
			{
				lngAccount = value;
			}
		}

		public int OwnerID
		{
			get
			{
				int OwnerID = 0;
				OwnerID = lngOwnerID;
				return OwnerID;
			}
			set
			{
				lngOwnerID = value;
			}
		}

		public int SecOwnerID
		{
			get
			{
				int SecOwnerID = 0;
				SecOwnerID = lngSecOwnerID;
				return SecOwnerID;
			}
			set
			{
				lngSecOwnerID = value;
			}
		}

		public bool TaxAcquired
		{
			get
			{
				bool TaxAcquired = false;
				TaxAcquired = boolTaxAcquired;
				return TaxAcquired;
			}
			set
			{
				boolTaxAcquired = value;
			}
		}

		public bool InBankruptcy
		{
			get
			{
				bool InBankruptcy = false;
				InBankruptcy = boolInBankruptcy;
				return InBankruptcy;
			}
			set
			{
				boolInBankruptcy = value;
			}
		}

		public bool RevocableTrust
		{
			get
			{
				bool RevocableTrust = false;
				RevocableTrust = boolRevocableTrust;
				return RevocableTrust;
			}
			set
			{
				boolRevocableTrust = value;
			}
		}

		public cParty OwnerParty
		{
			get
			{
				return pOwner;
			}
			set
			{
				pOwner = value;
			}
		}

		public cParty SecOwnerParty
		{
			get
			{
				return pSecOwner;
			}
			set
			{
				pSecOwner = value;
			}
		}

		public double Get_ExemptPct(int intIndex)
		{
			double ExemptPct = 0;
			ExemptPct = dblExemptPct[intIndex];
			return ExemptPct;
		}

		public void Set_ExemptPct(int intIndex, double dblPct)
		{
			dblExemptPct[intIndex] = dblPct;
		}

		public void Set_ExemptCode(int intIndex, int intCode)
		{
			intExemptCode[intIndex] = intCode;
		}

		public int Get_ExemptCode(int intIndex)
		{
			int ExemptCode = 0;
			ExemptCode = intExemptCode[intIndex];
			return ExemptCode;
		}

		public double Get_ExemptVal(int intIndex)
		{
			double ExemptVal = 0;
			ExemptVal = dblExemptVal[intIndex];
			return ExemptVal;
		}

		public void Set_ExemptVal(int intIndex, double dblValue)
		{
			dblExemptVal[intIndex] = dblValue;
		}

		public string MapLot
		{
			get
			{
				string MapLot = "";
				MapLot = strMapLot;
				return MapLot;
			}
			set
			{
				strMapLot = value;
			}
		}

		public int TranCode
		{
			get
			{
				int TranCode = 0;
				TranCode = lngTranCode;
				return TranCode;
			}
			set
			{
				lngTranCode = value;
			}
		}

		public DateTime SaleDate
		{
			get
			{
				DateTime SaleDate = System.DateTime.Now;
				SaleDate = dtSaleDate;
				return SaleDate;
			}
			set
			{
				dtSaleDate = value;
			}
		}

		public int SaleFinancing
		{
			get
			{
				int SaleFinancing = 0;
				SaleFinancing = intSaleFinancing;
				return SaleFinancing;
			}
			set
			{
				intSaleFinancing = value;
			}
		}

		public int SaleValidity
		{
			get
			{
				int SaleValidity = 0;
				SaleValidity = intSaleValidity;
				return SaleValidity;
			}
			set
			{
				intSaleValidity = value;
			}
		}

		public int SaleVerified
		{
			get
			{
				int SaleVerified = 0;
				SaleVerified = intSaleVerified;
				return SaleVerified;
			}
			set
			{
				intSaleVerified = value;
			}
		}

		public int SaleType
		{
			get
			{
				int SaleType = 0;
				SaleType = intSaleType;
				return SaleType;
			}
			set
			{
				intSaleType = value;
			}
		}

		public void Clear()
		{
			lngID = 0;
			intSaleType = 0;
			intSaleFinancing = 0;
			intSaleValidity = 0;
			intSaleVerified = 0;
			int x;
			for (x = 1; x <= 3; x++)
			{
				intExemptCode[x] = 0;
				dblExemptPct[x] = 100;
				dblExemptVal[x] = 0;
			}
			// x
			strMapLot = "";
			lngAccount = 0;
			lngOwnerID = 0;
			lngSecOwnerID = 0;
			lngTranCode = 0;
			boolInBankruptcy = false;
			boolTaxAcquired = false;
			boolRevocableTrust = false;
			dtSaleDate = DateTime.FromOADate(0);
			pOwner = new cParty();
			pSecOwner = new cParty();
			strAccountID = "";
			dblSalePrice = 0;
            DeedName2 = "";
            DeedName1 = "";
        }

		public cREAccount() : base()
		{
			Clear();
		}
	}
}
