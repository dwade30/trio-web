﻿//Fecher vbPorter - Version 1.0.0.40

using System.Collections.Generic;
using Global;

namespace TWRE0000
{
	public class cAccountInfo
	{
		//=========================================================
		private int lngAccount;
		private double dblExemptTotal;
		private string strmaplot = string.Empty;
		private List<string> collOwners = new List<string>();
		private cGenericCollection collExemptions = new cGenericCollection();

		public cGenericCollection Exemptions
		{
			get
			{
				cGenericCollection Exemptions = null;
				Exemptions = collExemptions;
				return Exemptions;
			}
		}

		public int Account
		{
			set
			{
				lngAccount = value;
			}
			get
			{
				int Account = 0;
				Account = lngAccount;
				return Account;
			}
		}

		public double ExemptTotal
		{
			set
			{
				dblExemptTotal = value;
			}
			get
			{
				double ExemptTotal = 0;
				ExemptTotal = dblExemptTotal;
				return ExemptTotal;
			}
		}

		public string Maplot
		{
			set
			{
				strmaplot = value;
			}
			get
			{
				string Maplot = "";
				Maplot = strmaplot;
				return Maplot;
			}
		}

		public List<string> Owners
		{
			get
			{
				return collOwners;
			}
		}
	}
}
