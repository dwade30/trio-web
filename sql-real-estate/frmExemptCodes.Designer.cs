﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExemptCodes.
	/// </summary>
	partial class frmExemptCodes : BaseForm
	{
		public FCGrid GridDelete;
		public FCGrid Grid;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExemptCodes));
            this.GridDelete = new fecherFoundation.FCGrid();
            this.Grid = new fecherFoundation.FCGrid();
            this.cmdAddCode = new Wisej.Web.Button();
            this.cmdDeleteCode = new Wisej.Web.Button();
            this.cmdSave = new Wisej.Web.Button();
            this.cmdPrintPreview = new Wisej.Web.Button();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 388);
            this.BottomPanel.Size = new System.Drawing.Size(757, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridDelete);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Size = new System.Drawing.Size(757, 328);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddCode);
            this.TopPanel.Controls.Add(this.cmdDeleteCode);
            this.TopPanel.Controls.Add(this.cmdPrintPreview);
            this.TopPanel.Size = new System.Drawing.Size(757, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreview, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteCode, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddCode, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(173, 30);
            this.HeaderText.Text = "Exempt Codes";
            // 
            // GridDelete
            // 
            this.GridDelete.Cols = 1;
            this.GridDelete.ColumnHeadersVisible = false;
            this.GridDelete.FixedCols = 0;
            this.GridDelete.FixedRows = 0;
            this.GridDelete.Location = new System.Drawing.Point(493, 24);
            this.GridDelete.Name = "GridDelete";
            this.GridDelete.RowHeadersVisible = false;
            this.GridDelete.Rows = 0;
            this.GridDelete.Size = new System.Drawing.Size(17, 2);
            this.GridDelete.TabIndex = 1;
            this.GridDelete.Visible = false;
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 8;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(30, 30);
            this.Grid.Name = "Grid";
            this.Grid.ReadOnly = false;
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 1;
            this.Grid.Size = new System.Drawing.Size(707, 278);
            this.Grid.StandardTab = false;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
            this.Grid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.Grid_BeforeEdit);
            this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
            this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // cmdAddCode
            // 
            this.cmdAddCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddCode.AppearanceKey = "toolbarButton";
            this.cmdAddCode.Location = new System.Drawing.Point(558, 29);
            this.cmdAddCode.Name = "cmdAddCode";
            this.cmdAddCode.Size = new System.Drawing.Size(80, 24);
            this.cmdAddCode.TabIndex = 1;
            this.cmdAddCode.Text = "Add Code";
            this.cmdAddCode.Click += new System.EventHandler(this.mnuAddCode_Click);
            // 
            // cmdDeleteCode
            // 
            this.cmdDeleteCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteCode.AppearanceKey = "toolbarButton";
            this.cmdDeleteCode.Location = new System.Drawing.Point(644, 29);
            this.cmdDeleteCode.Name = "cmdDeleteCode";
            this.cmdDeleteCode.Size = new System.Drawing.Size(90, 24);
            this.cmdDeleteCode.TabIndex = 2;
            this.cmdDeleteCode.Text = "Delete Code";
            this.cmdDeleteCode.Click += new System.EventHandler(this.mnuDeleteCode_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(357, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(90, 48);
            this.cmdSave.TabIndex = 3;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreview.AppearanceKey = "toolbarButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(442, 29);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Size = new System.Drawing.Size(110, 24);
            this.cmdPrintPreview.TabIndex = 2;
            this.cmdPrintPreview.Text = "Print Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // frmExemptCodes
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(757, 496);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmExemptCodes";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Exemption Codes";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmExemptCodes_Load);
            this.Activated += new System.EventHandler(this.frmExemptCodes_Activated);
            this.Resize += new System.EventHandler(this.frmExemptCodes_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExemptCodes_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdAddCode;
		private Button cmdDeleteCode;
		private Button cmdSave;
		private Button cmdPrintPreview;
	}
}
