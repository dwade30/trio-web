﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmexemptions.
	/// </summary>
	partial class frmexemptions : BaseForm
	{
		public fecherFoundation.FCComboBox cmbtsort;
		public fecherFoundation.FCLabel lbltsort;
		public fecherFoundation.FCComboBox cmbtdisplay;
		public fecherFoundation.FCLabel lbltdisplay;
		public fecherFoundation.FCFrame framRange;
		public FCGrid GridEnd;
		public FCGrid GridStart;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCButton cmdContinue;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmexemptions));
            this.cmbtsort = new fecherFoundation.FCComboBox();
            this.lbltsort = new fecherFoundation.FCLabel();
            this.cmbtdisplay = new fecherFoundation.FCComboBox();
            this.lbltdisplay = new fecherFoundation.FCLabel();
            this.framRange = new fecherFoundation.FCFrame();
            this.GridEnd = new fecherFoundation.FCGrid();
            this.GridStart = new fecherFoundation.FCGrid();
            this.lblTo = new fecherFoundation.FCLabel();
            this.cmdContinue = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framRange)).BeginInit();
            this.framRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 378);
            this.BottomPanel.Size = new System.Drawing.Size(580, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framRange);
            this.ClientArea.Controls.Add(this.cmdContinue);
            this.ClientArea.Controls.Add(this.cmbtsort);
            this.ClientArea.Controls.Add(this.lbltsort);
            this.ClientArea.Controls.Add(this.cmbtdisplay);
            this.ClientArea.Controls.Add(this.lbltdisplay);
            this.ClientArea.Size = new System.Drawing.Size(580, 318);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(580, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(272, 30);
            this.HeaderText.Text = "Exemption Audit Report";
            // 
            // cmbtsort
            // 
            this.cmbtsort.AutoSize = false;
            this.cmbtsort.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbtsort.FormattingEnabled = true;
            this.cmbtsort.Items.AddRange(new object[] {
            "Name",
            "Name Within Exempts",
            "Map/Lot",
            "Map/Lot Within Exempts",
            "Account",
            "Account Within Exempts"});
            this.cmbtsort.Location = new System.Drawing.Point(127, 90);
            this.cmbtsort.Name = "cmbtsort";
            this.cmbtsort.Size = new System.Drawing.Size(222, 40);
            this.cmbtsort.TabIndex = 15;
            this.cmbtsort.Text = "Name Within Exempts";
            // 
            // lbltsort
            // 
            this.lbltsort.AutoSize = true;
            this.lbltsort.Location = new System.Drawing.Point(30, 104);
            this.lbltsort.Name = "lbltsort";
            this.lbltsort.Size = new System.Drawing.Size(62, 15);
            this.lbltsort.TabIndex = 16;
            this.lbltsort.Text = "SORT BY";
            // 
            // cmbtdisplay
            // 
            this.cmbtdisplay.AutoSize = false;
            this.cmbtdisplay.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbtdisplay.FormattingEnabled = true;
            this.cmbtdisplay.Items.AddRange(new object[] {
            "Partial Exempts Only",
            "Totally Exempts Only",
            "Partially and Totally",
            "Specific Exempt Code(s)"});
            this.cmbtdisplay.Location = new System.Drawing.Point(127, 30);
            this.cmbtdisplay.Name = "cmbtdisplay";
            this.cmbtdisplay.Size = new System.Drawing.Size(222, 40);
            this.cmbtdisplay.TabIndex = 17;
            this.cmbtdisplay.Text = "Partially and Totally";
            this.cmbtdisplay.SelectedIndexChanged += new System.EventHandler(this.Optdisplay_CheckedChanged);
            // 
            // lbltdisplay
            // 
            this.lbltdisplay.AutoSize = true;
            this.lbltdisplay.Location = new System.Drawing.Point(30, 44);
            this.lbltdisplay.Name = "lbltdisplay";
            this.lbltdisplay.Size = new System.Drawing.Size(60, 15);
            this.lbltdisplay.TabIndex = 18;
            this.lbltdisplay.Text = "DISPLAY";
            // 
            // framRange
            // 
            this.framRange.Controls.Add(this.GridEnd);
            this.framRange.Controls.Add(this.GridStart);
            this.framRange.Controls.Add(this.lblTo);
            this.framRange.Location = new System.Drawing.Point(30, 150);
            this.framRange.Name = "framRange";
            this.framRange.Size = new System.Drawing.Size(443, 90);
            this.framRange.TabIndex = 14;
            this.framRange.Visible = false;
            // 
            // GridEnd
            // 
            this.GridEnd.AllowSelection = false;
            this.GridEnd.AllowUserToResizeColumns = false;
            this.GridEnd.AllowUserToResizeRows = false;
            this.GridEnd.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridEnd.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridEnd.BackColorBkg = System.Drawing.Color.Empty;
            this.GridEnd.BackColorFixed = System.Drawing.Color.Empty;
            this.GridEnd.BackColorSel = System.Drawing.Color.Empty;
            this.GridEnd.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridEnd.Cols = 1;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridEnd.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridEnd.ColumnHeadersHeight = 30;
            this.GridEnd.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridEnd.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridEnd.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridEnd.DragIcon = null;
            this.GridEnd.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridEnd.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridEnd.ExtendLastCol = true;
            this.GridEnd.FixedCols = 0;
            this.GridEnd.FixedRows = 0;
            this.GridEnd.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridEnd.FrozenCols = 0;
            this.GridEnd.GridColor = System.Drawing.Color.Empty;
            this.GridEnd.GridColorFixed = System.Drawing.Color.Empty;
            this.GridEnd.Location = new System.Drawing.Point(253, 30);
            this.GridEnd.Name = "GridEnd";
            this.GridEnd.OutlineCol = 0;
            this.GridEnd.RowHeadersVisible = false;
            this.GridEnd.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridEnd.RowHeightMin = 0;
            this.GridEnd.Rows = 1;
            this.GridEnd.ScrollTipText = null;
            this.GridEnd.ShowColumnVisibilityMenu = false;
            this.GridEnd.Size = new System.Drawing.Size(170, 40);
            this.GridEnd.StandardTab = true;
            this.GridEnd.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridEnd.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridEnd.TabIndex = 16;
            // 
            // GridStart
            // 
            this.GridStart.AllowSelection = false;
            this.GridStart.AllowUserToResizeColumns = false;
            this.GridStart.AllowUserToResizeRows = false;
            this.GridStart.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridStart.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridStart.BackColorBkg = System.Drawing.Color.Empty;
            this.GridStart.BackColorFixed = System.Drawing.Color.Empty;
            this.GridStart.BackColorSel = System.Drawing.Color.Empty;
            this.GridStart.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridStart.Cols = 1;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridStart.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GridStart.ColumnHeadersHeight = 30;
            this.GridStart.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridStart.ColumnHeadersVisible = false;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridStart.DefaultCellStyle = dataGridViewCellStyle4;
            this.GridStart.DragIcon = null;
            this.GridStart.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridStart.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridStart.ExtendLastCol = true;
            this.GridStart.FixedCols = 0;
            this.GridStart.FixedRows = 0;
            this.GridStart.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridStart.FrozenCols = 0;
            this.GridStart.GridColor = System.Drawing.Color.Empty;
            this.GridStart.GridColorFixed = System.Drawing.Color.Empty;
            this.GridStart.Location = new System.Drawing.Point(20, 30);
            this.GridStart.Name = "GridStart";
            this.GridStart.OutlineCol = 0;
            this.GridStart.RowHeadersVisible = false;
            this.GridStart.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridStart.RowHeightMin = 0;
            this.GridStart.Rows = 1;
            this.GridStart.ScrollTipText = null;
            this.GridStart.ShowColumnVisibilityMenu = false;
            this.GridStart.Size = new System.Drawing.Size(170, 40);
            this.GridStart.StandardTab = true;
            this.GridStart.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridStart.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridStart.TabIndex = 17;
            // 
            // lblTo
            // 
            this.lblTo.Location = new System.Drawing.Point(213, 44);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 18);
            this.lblTo.TabIndex = 15;
            this.lblTo.Text = "TO";
            // 
            // cmdContinue
            // 
            this.cmdContinue.AppearanceKey = "acceptButton";
            this.cmdContinue.Location = new System.Drawing.Point(30, 260);
            this.cmdContinue.Name = "cmdContinue";
            this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdContinue.Size = new System.Drawing.Size(112, 48);
            this.cmdContinue.TabIndex = 10;
            this.cmdContinue.Text = "Continue";
            this.cmdContinue.Click += new System.EventHandler(this.cmdok_Click);
            // 
            // frmexemptions
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(580, 486);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmexemptions";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Exemption Audit Report";
            this.Load += new System.EventHandler(this.frmexemptions_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmexemptions_KeyDown);
            this.Resize += new System.EventHandler(this.frmexemptions_Resize);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framRange)).EndInit();
            this.framRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
