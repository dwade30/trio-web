﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Global;
using GrapeCity.ActiveReports.Extensibility.Data;
using SharedApplication;

namespace TWRE0000
{
    public class REPropertyBriefsQueryHandler : IQueryHandler<REListingQuery, IEnumerable<REAccountPropertyBrief>>
    {
        public IEnumerable<REAccountPropertyBrief> ExecuteQuery(REListingQuery query)
        {
            var strSQL = "Select rsaccount,rscard,rsmaplot,deedname1,deedname2,rslocstreet,rslocapt,rslocnumalph from master where rscard = 1 and ";
            if (query.FromExtract)
            {
                var lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
                strSQL +=
                    " rsaccount in (select extracttable.accountnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " +
                    lngUID + ") AND rsdeleted = 0 ";
            }
            else
            {
                strSQL += " isnull(rsdeleted,0) = 0 ";
            }

            if (query.ByRange)
            {
                switch (query.FieldForOrdering)
                {
                    case REListingField.Account:
                        strSQL += " and rsaccount between " + query.MinimumAccount.ToString() + " and " +
                                  query.MaximumAccount.ToString() + " order by rsaccount";
                        break;
                    case REListingField.Map:
                        strSQL += " and rsmpalot between '" + query.RangeStart + "' and '" + query.RangeEnd +
                                  "' order by rsmaplot,rsaccount";
                        break;
                    case REListingField.Name:
                        strSQL += " and deedname1 between '" + query.RangeStart + "' and '" + query.RangeEnd +
                                  "' order by deedname1,rsaccount";
                        break;
                }
            }
            else
            {
                switch (query.FieldForOrdering)
                {
                    case REListingField.Account:
                        strSQL += " order by rsaccount";
                        break;
                    case REListingField.Map:
                        strSQL += " order by rsmaplot, rsaccount";
                        break;
                    case REListingField.Name:
                        strSQL += " order by deedname1,rsaccount";
                        break;
                }
            }

            var rsLoad = new clsDRWrapper();
            rsLoad.OpenRecordset(strSQL, "RealEstate");
            return ToPropertyBriefs(ref rsLoad);
        }


            private  IEnumerable<REAccountPropertyBrief> ToPropertyBriefs(ref clsDRWrapper propertyRows)
            {
                var propertyBriefs = new List<REAccountPropertyBrief>();
                if (propertyRows == null)
                {
                    return propertyBriefs;
                }

                while (!propertyRows.EndOfFile())
                {
                    var propertyBrief = new REAccountPropertyBrief()
                    {
                        Account = propertyRows.Get_Fields_Int32("rsaccount"),
                        DeedName1 = propertyRows.Get_Fields_String("DeedName1"),
                        DeedName2 = propertyRows.Get_Fields_String("DeedName2"),
                        MapLot = propertyRows.Get_Fields_String("RSMapLot"),
                        Location =
                        {
                            Apartment = propertyRows.Get_Fields_String("rslocapt"),
                            StreetName = propertyRows.Get_Fields_String("Rslocstreet"),
                            StreetNumber = propertyRows.Get_Fields_String("rslocnumalph")
                        }
                    };
                    propertyBriefs.Add(propertyBrief);
                    propertyRows.MoveNext();
                }

                return propertyBriefs;
            }
    }

    
}
