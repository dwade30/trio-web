﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmWhichExemptions.
	/// </summary>
	public partial class frmWhichExemptions : BaseForm
	{
		public frmWhichExemptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmWhichExemptions InstancePtr
		{
			get
			{
				return (frmWhichExemptions)Sys.GetInstance(typeof(frmWhichExemptions));
			}
		}

		protected frmWhichExemptions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void Command1_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = false;
			if (cmbtBilOrCor.SelectedIndex == 0)
			{
				modGlobalVariables.Statics.boolBillingorCorrelated = true;
			}
			else
			{
				modGlobalVariables.Statics.boolBillingorCorrelated = false;
			}
			this.Unload();
		}

		private void frmWhichExemptions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmWhichExemptions properties;
			//frmWhichExemptions.ScaleWidth	= 5880;
			//frmWhichExemptions.ScaleHeight	= 4185;
			//frmWhichExemptions.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalVariables.Statics.CancelledIt = true;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// If Cancel = vbCancel Then CancelledIt = True
			// CancelledIt = True
		}
	}
}
