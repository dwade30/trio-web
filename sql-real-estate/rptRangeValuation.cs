﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication.CentralDocuments.Commands;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using TWSharedLibrary;
using Wisej.Web;


namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptRangeValuation.
	/// </summary>
	public partial class rptRangeValuation : BaseSectionReport
	{
		public static rptRangeValuation InstancePtr
		{
			get
			{
				return (rptRangeValuation)Sys.GetInstance(typeof(rptRangeValuation));
			}
		}

		protected rptRangeValuation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptRangeValuation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Valuation Report";
		}

		int NumRecords;
		bool boolSpecific;
		bool boolFromExtract;
		bool boolDidntUpdateHeader;
		int intCards;
		int intTownNumber;
		bool boolFirstRec;
		private bool boolOnlyChanges;
		public FCGrid GridSummary;
		//FC:FINAL:AM:#i1237 - execute the code after the report is closed (in original it was displayed modal)
		public Action reportClosed = null;
        private bool exportPDFs = false;
        private bool runSilent = false;
        private Dictionary<Guid,int> accountsByPage = new Dictionary<Guid, int>();
        private string exportFolder = "";
		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuni.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			boolDidntUpdateHeader = false;
			boolFirstRec = true;

		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{ 
			int lngTotLand;
			int lngTotBldg;
			int lngTotTot;
			double dblOldLand;
			double dblOldBldg;
			OnceAgain:
			;
			if (!boolFirstRec)
			{
				if (!modDataTypes.Statics.MR.EndOfFile())
				{
					modDataTypes.Statics.MR.MoveNext();
				}
			}
			else
			{
				boolFirstRec = false;
			}
			eArgs.EOF = modDataTypes.Statics.MR.EndOfFile();
			if (eArgs.EOF)
				return;
			modDataTypes.Statics.MR.Edit();
			this.Fields["grpHeader"].Value = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
			if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
			{
				modREASValuations.Statics.BBPhysicalPercent = 0;
				modREASValuations.Statics.HoldDwellingEconomic = 0;
			}
			modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
			dblOldLand = FCConvert.ToDouble(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval"));
			dblOldBldg = FCConvert.ToDouble(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval"));
			modGlobalVariables.Statics.boolfromcalcandassessment = true;
			modSpeedCalc.CalcCard();
			modGlobalVariables.Statics.boolfromcalcandassessment = false;
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + "") == 1)
			{
				clsDRWrapper clsTemp = new clsDRWrapper();
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					clsTemp.OpenRecordset("select count(rsaccount) as thecount from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
				}
				else
				{
					clsTemp.OpenRecordset("select count(rsaccount) as thecount from srmaster where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and saleid = " + FCConvert.ToString(modGlobalVariables.Statics.glngSaleID), modGlobalVariables.strREDatabase);
				}
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				intCards = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thecount"))));
			}
			if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("saledate") + ""))
			{
				if (modDataTypes.Statics.MR.Get_Fields_DateTime("SALEDATE").ToOADate() != 0)
				{
					subSaleData.Report.UserData = -1;
					subSaleData.Visible = true;
				}
				else
				{
					subSaleData.Report.UserData = -10;
					subSaleData.Visible = false;
				}
			}
			else
			{
				subSaleData.Report.UserData = -10;
				subSaleData.Visible = false;
			}
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood") + "") > 0)
			{
				SubMisc.Report.UserData = -1;
				SubMisc.Visible = true;
			}
			else
			{
				SubMisc.Report.UserData = -10;
				SubMisc.Visible = false;
			}
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piland1type") + "") > 0)
			{
				subLandDesc.Report.UserData = -1;
				subLandDesc.Visible = true;
			}
			else
			{
				subLandDesc.Report.UserData = -10;
				subLandDesc.Visible = false;
			}
			if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "C")
			{
				subDwellComm.Visible = false;
				subComm.Visible = true;
				subComm.Report.UserData = -1;
				subDwellComm.Report.UserData = -10;
			}
			else
			{
				subComm.Visible = false;
				subComm.Report.UserData = -10;
				if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "Y")
				{
					subDwellComm.Report.UserData = -1;
					subDwellComm.Visible = true;
				}
				else
				{
					subDwellComm.Visible = false;
					subDwellComm.Report.UserData = -10;
				}
			}
			if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields_Int32("oitype1") + "") > 0)
			{
				subOutBuilding.Report.UserData = -1;
				subOutBuilding.Visible = true;
			}
			else
			{
				subOutBuilding.Report.UserData = -10;
				subOutBuilding.Visible = false;
			}
			// HandlePicsSketches
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("HIVALLANDCODE") + "") > 2)
			{
				lblLandTot.Text = "Land Override";
			}
			else
			{
				lblLandTot.Text = "Accpt Land";
			}
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode") + "") > 2)
			{
				lblBldgTot.Text = "Bldg Override";
			}
			else
			{
				lblBldgTot.Text = "Accepted Bldg";
			}
			lngTotLand = modProperty.Statics.lngCorrelatedLand[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")];
			lngTotBldg = modProperty.Statics.lngCorrelatedBldg[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")];
			lngTotLand = modGlobalRoutines.RERound(ref lngTotLand);
			lngTotBldg = modGlobalRoutines.RERound(ref lngTotBldg);
			txtLand.Text = Strings.Format(lngTotLand, "#,###,##0");
			txtBldg.Text = Strings.Format(lngTotBldg, "#,###,##0");
			txtTotal.Text = Strings.Format(lngTotLand + lngTotBldg, "#,###,###,##0");
			if (boolOnlyChanges)
			{
				if (dblOldLand == lngTotLand)
				{
					if (dblOldBldg == lngTotBldg)
					{
						if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
						{
							GridSummary.Rows = 0;
							GridSummary.Rows = 1;
						}
						else
						{
							GridSummary.Rows += 1;
						}
						GridSummary.TextMatrix(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") - 1, 0, modDataTypes.Statics.MR.Get_Fields_Int32("rscard"));
						GridSummary.TextMatrix(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") - 1, 1, modProperty.Statics.lngCurrentLand[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")]);
						GridSummary.TextMatrix(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") - 1, 2, modProperty.Statics.lngCurrentBldg[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")]);
						GridSummary.TextMatrix(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") - 1, 3, modProperty.Statics.lngCurrentLand[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")] + modProperty.Statics.lngCurrentBldg[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")]);
						GridSummary.TextMatrix(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") - 1, 4, modProperty.Statics.lngCorrelatedLand[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")]);
						GridSummary.TextMatrix(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") - 1, 5, modProperty.Statics.lngCorrelatedBldg[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")]);
						GridSummary.TextMatrix(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") - 1, 6, modProperty.Statics.lngCorrelatedLand[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")] + modProperty.Statics.lngCorrelatedBldg[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")]);
						// GridSummary.TextMatrix(0, 7) = MR.Fields("rsname") & ""
						GridSummary.TextMatrix(0, 8, modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + "");
						GridSummary.TextMatrix(0, 9, modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + "");
						GridSummary.TextMatrix(0, 10, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + " " + modDataTypes.Statics.MR.Get_Fields_String("rslocstreet")));
						// GridSummary.TextMatrix(0, 11) = Trim(MR.Fields("rssecowner") & "")
						if (modGlobalVariables.Statics.boolRegRecords)
                        {
                            GridSummary.TextMatrix(0, 7, modDataTypes.Statics.MR.Get_Fields_String("DeedName1"));
                            GridSummary.TextMatrix(0, 11, modDataTypes.Statics.MR.Get_Fields_String("DeedName2"));							
						}
						else
						{
							GridSummary.TextMatrix(0, 7, modDataTypes.Statics.MR.Get_Fields_String("rsname") + "");
							GridSummary.TextMatrix(0, 11, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rssecowner") + ""));
						}
						goto OnceAgain;
					}
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{

			modGlobalVariables.Statics.boolUseArrays = false;
            if (!reportCanceled)
            {

                if (modGlobalVariables.Statics.boolUpdateWhenCalculate)
                {
                    modSpeedCalc.SaveSummary();
                }

                if (exportPDFs)
                {
                    SavePDFFiles();
                }
                else
                {
                    if (modSpeedCalc.Statics.boolCalcErrors)
                    {
                        MessageBox.Show("There were errors calculating.  Following is a list.", null,
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        //! Load frmShowResults;
                        frmShowResults.InstancePtr.Init(ref modSpeedCalc.Statics.CalcLog);
                        frmShowResults.InstancePtr.lblScreenTitle.Text = "Calculation Errors Log";
                        frmShowResults.InstancePtr.Show();
                    }
                }
            }

            modSpeedCalc.Statics.boolCalcErrors = false;
			modSpeedCalc.Statics.CalcLog = "";
			if (reportClosed != null)
			{
				reportClosed.Invoke();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			int lngUID;
			string strMasterJoin;
			strMasterJoin = modREMain.GetMasterJoin();
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			//this.Document.Printer.RenderMode = 1;
			SubMisc.Report = new srptMisc();
			subSaleData.Report = new srptSaleData();
			subLandDesc.Report = new srptLandDescription();
			subOutBuilding.Report = new srptOut();
			subDwellComm.Report = new srptDwell();
			subComm.Report = new srptComm();
			switch (modPrintRoutines.Statics.intwhichorder)
			{
				case 1:
					{
						txtorder.Text = "By Account";
						break;
					}
				case 2:
					{
						txtorder.Text = "By Name";
						break;
					}
				case 3:
					{
						txtorder.Text = "By Map/Lot";
						break;
					}
				case 4:
					{
						txtorder.Text = "By Location";
						break;
					}
			}
			//end switch
			if (intTownNumber > 0)
			{
				txtorder.Text = "";
				txtorder.Text = txtorder.Text + " (" + modRegionalTown.GetTownKeyName_2(intTownNumber) + ")";
				txtorder.Text = Strings.Trim(txtorder.Text);
			}
			else if (modGlobalVariables.Statics.boolByRange)
			{
				txtorder.Text = "";
				txtorder.Text = txtorder.Text + "  (";
				if (modPrintRoutines.Statics.gstrFieldName == "RSAccount")
				{
					txtorder.Text = txtorder.Text + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " To " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange);
				}
				else
				{
					txtorder.Text = txtorder.Text + modPrintRoutines.Statics.gstrMinAccountRange + " To " + modPrintRoutines.Statics.gstrMaxAccountRange;
				}
				txtorder.Text = txtorder.Text + ")";
				txtorder.Text = Strings.Trim(txtorder.Text);
			}
			else if (boolSpecific)
			{
				txtorder.Text = "";
				txtorder.Text = txtorder.Text + "  (Specific)";
			}
			else if (boolFromExtract)
			{
				txtorder.Text = txtorder.Text + " - ";
				txtorder.Text = "";
				clsTemp.OpenRecordset("select * from extract where userid = " + FCConvert.ToString(lngUID) + " and reportnumber = 0", modGlobalVariables.strREDatabase);
				if (!clsTemp.EndOfFile())
				{
					if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("title"))) != string.Empty)
					{
						txtorder.Text = txtorder.Text + Strings.Trim(clsTemp.Get_Fields_String("title"));
					}
					else
					{
						txtorder.Text = txtorder.Text + "(From Extract)";
					}
				}
				else
				{
					MessageBox.Show("No extract has been done.  Cannot continue", "No Extract", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
					if (reportClosed != null)
					{
						reportClosed.Invoke();
					}
					return;
				}
			}
			else
			{
				txtorder.Text = txtorder.Text + "  (ALL)";
			}
			txtorder.Text = Strings.Trim(txtorder.Text);
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (modPrintRoutines.Statics.gstrFieldName == "RSAccount")
			{
				if (intTownNumber > 0)
				{
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where ritrancode = " + FCConvert.ToString(intTownNumber) + " and not rsdeleted = 1 order by rsaccount,RSCard", modGlobalVariables.strREDatabase);
				}
				else if (boolSpecific)
				{
					modDataTypes.Statics.MR.OpenRecordset("select  * from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ") order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				}
				else if (boolFromExtract)
				{
					modDataTypes.Statics.MR.OpenRecordset("select * from master where (not rsdeleted = 1) and rsaccount in (select ACCOUNTNUMBER from extracttable where userid = " + FCConvert.ToString(lngUID) + ") order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				}
				else if (modGlobalVariables.Statics.boolByRange)
				{
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						// If LCase(gstrFieldName) <> "rsname" Then
						modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where " + modPrintRoutines.Statics.gstrFieldName + " >= " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " + modPrintRoutines.Statics.gstrFieldName + " <= " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + " and not rsdeleted = 1 order by " + modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
						// Else
						// Call MR.OpenRecordset("SELECT  * FROM Master where " & gstrFieldName & " >= " & gintMinAccountRange & " and " & gstrFieldName & " <= " & gintMaxAccountRange & " and not rsdeleted = 1 order by " & gstrFieldName & ",RSCard", strREDatabase)
						// End If
					}
					else
					{
						// Call MR.OpenRecordset("SELECT  *,rsname as OwnerFullName,rssecowner as SecOwnerFullName FROM srMaster where " & gstrFieldName & " >= " & gintMinAccountRange & " and " & gstrFieldName & " <= " & gintMaxAccountRange & " and not rsdeleted = 1 and saleid = " & glngSaleID & " order by " & gstrFieldName & ",RSCard", strREDatabase)
						modDataTypes.Statics.MR.OpenRecordset("SELECT  *  FROM srMaster where " + modPrintRoutines.Statics.gstrFieldName + " >= " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " + modPrintRoutines.Statics.gstrFieldName + " <= " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + " and not rsdeleted = 1 and saleid = " + FCConvert.ToString(modGlobalVariables.Statics.glngSaleID) + " order by " + modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where not rsdeleted = 1 order by " + modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
				}

			}
			else
			{
				if (intTownNumber > 0)
				{
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where ritrancode = " + FCConvert.ToString(intTownNumber) + " and not rsdeleted = 1 order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,RSCard", modGlobalVariables.strREDatabase);
				}
				else if (boolSpecific)
				{
					modDataTypes.Statics.MR.OpenRecordset("select  * from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ") order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,rscard", modGlobalVariables.strREDatabase);
				}
				else if (boolFromExtract)
				{
					modDataTypes.Statics.MR.OpenRecordset("select  * from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from extracttable where userid = " + FCConvert.ToString(lngUID) + ") order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,rscard", modGlobalVariables.strREDatabase);
				}
				else if (modGlobalVariables.Statics.boolByRange)
				{
					// If LCase(gstrFieldName) <> "rsname" Then
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' and not rsdeleted = 1 order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,RSCard", modGlobalVariables.strREDatabase);
				}
				else
				{
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where not rsdeleted = 1 order by " + modPrintRoutines.Statics.gstrFieldName + " ,rsaccount,RSCard", modGlobalVariables.strREDatabase);
				}
				// Call MR.InsertName("OwnerPartyID,SecOwnerPartyID", "Owner,SecOwner", False, True, False, "", False, gstrFieldName, True)
			}
			if (modDataTypes.Statics.MR.EndOfFile())
			{
				MessageBox.Show("No Matches found", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				if (reportClosed != null)
				{
					reportClosed.Invoke();
				}
				return;
			}
			modDataTypes.Statics.MR.MoveLast();
			modDataTypes.Statics.MR.MoveFirst();
			modDataTypes.Statics.MR.Edit();
			NumRecords = modDataTypes.Statics.MR.RecordCount();
			Frmassessmentprogress.InstancePtr.ProgressBar1.Maximum = NumRecords + 1;
			// boolfromcalcandassessment = True
			modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
			if (modGlobalVariables.Statics.boolRegRecords)
			{
				modDataTypes.Statics.OUT.OpenRecordset("select * from outbuilding order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				modDataTypes.Statics.CMR.OpenRecordset("select * from commercial order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				modDataTypes.Statics.DWL.OpenRecordset("select * from dwelling order by rsaccount,rscard", modGlobalVariables.strREDatabase);
			}
			else
			{
				modDataTypes.Statics.OUT.OpenRecordset("select * from sroutbuilding where saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "' order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				modDataTypes.Statics.CMR.OpenRecordset("select * from srcommercial where saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "' order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				modDataTypes.Statics.DWL.OpenRecordset("select * from srdwelling where saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "' order by rsaccount,rscard", modGlobalVariables.strREDatabase);
			}
			modSpeedCalc.REAS03_1075_GET_PARAM_RECORD();
			Array.Resize(ref modSpeedCalc.Statics.SummaryList, NumRecords + 1 + 1);
			modSpeedCalc.Statics.SummaryListIndex = 0;
			modProperty.Statics.RUNTYPE = "P";
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1452, "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
			if (Conversion.Val(modGlobalVariables.Statics.CostRec.CBase) == 0)
			{
				txtRatio.Text = "0%";
			}
			else
			{
				txtRatio.Text = FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.CostRec.CBase)) + "%";
			}
			if (Conversion.Val(modGlobalVariables.Statics.CostRec.CBase) == 100 || modGlobalVariables.Statics.CustomizedInfo.HideValuationRatio)
			{
				txtRatio.Visible = false;
				lblRatio.Visible = false;
			}
			else
			{
				txtRatio.Visible = true;
				lblRatio.Visible = true;
			}
		}

		private void Detail_BeforePrint(object sender, EventArgs e)
		{

		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (GridSummary.Rows > 1)
            {
                if (exportPDFs)
                {
                    if (!accountsByPage.ContainsKey(this.CurrentPage.Identifier))
                    {
                        accountsByPage.Add(this.CurrentPage.Identifier,
                            modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
                    }
                }

                GroupFooter1.Visible = true;
				this.SubReport1.Report = new srptRangeCalcTotal();
				txtCard.Visible = false;
				Label6.Visible = false;
				txtNumCards.Visible = false;
				Label4.Visible = false;
				txtName.Text = GridSummary.TextMatrix(0, 7);
				txtAccount.Text = GridSummary.TextMatrix(0, 8);
				txtMapLot.Text = GridSummary.TextMatrix(0, 9);
				txtLocation.Text = GridSummary.TextMatrix(0, 10);
				txtSecName.Text = GridSummary.TextMatrix(0, 11);
				boolDidntUpdateHeader = true;
			}
			else
			{
				this.SubReport1.Report = null;
				// GroupFooter1.Height = 0
				GroupFooter1.Visible = false;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            
			if (!modDataTypes.Statics.MR.EndOfFile() && !reportCanceled)
			{
                if (exportPDFs)
                {                    
                    if (!accountsByPage.ContainsKey(this.CurrentPage.Identifier))
                    {
                        accountsByPage.Add(this.CurrentPage.Identifier, modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
                    }
                }
                
                if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
				{
					GridSummary.Rows = 0;
					GridSummary.Rows = 1;
				}
				else
				{
					GridSummary.Rows += 1;
				}
				GridSummary.TextMatrix(FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) - 1, 0, FCConvert.ToInt32(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("rscard"))));
				GridSummary.TextMatrix(FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) - 1, 1, modProperty.Statics.lngCurrentLand[FCConvert.ToInt32(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")))]);
				GridSummary.TextMatrix(FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) - 1, 2, modProperty.Statics.lngCurrentBldg[FCConvert.ToInt32(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")))]);
				GridSummary.TextMatrix(FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) - 1, 3, modProperty.Statics.lngCurrentLand[FCConvert.ToInt32(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")))] + modProperty.Statics.lngCurrentBldg[FCConvert.ToInt32(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")))]);
				GridSummary.TextMatrix(FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) - 1, 4, modProperty.Statics.lngCorrelatedLand[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")]);
				GridSummary.TextMatrix(FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) - 1, 5, modProperty.Statics.lngCorrelatedBldg[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")]);
				GridSummary.TextMatrix(FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) - 1, 6, modProperty.Statics.lngCorrelatedLand[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")] + modProperty.Statics.lngCorrelatedBldg[modDataTypes.Statics.MR.Get_Fields_Int32("rscard")]);
				GridSummary.TextMatrix(0, 7, "");
				GridSummary.TextMatrix(0, 11, "");
				if (modGlobalVariables.Statics.boolRegRecords)
                {
                    GridSummary.TextMatrix(0, 7, modDataTypes.Statics.MR.Get_Fields_String("DeedName1"));
                    GridSummary.TextMatrix(0, 11, modDataTypes.Statics.MR.Get_Fields_String("DeedName2"));					
				}
				else
				{
					GridSummary.TextMatrix(0, 7, modDataTypes.Statics.MR.Get_Fields_String("rsname") + "");
					GridSummary.TextMatrix(0, 11, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rssecowner") + ""));
				}
				GridSummary.TextMatrix(0, 8, modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + "");
				GridSummary.TextMatrix(0, 9, modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + "");
				GridSummary.TextMatrix(0, 10, Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + " " + modDataTypes.Statics.MR.Get_Fields_String("rslocstreet")));
			}
			HandlePicsSketches();
		}

		private void HandlePicsSketches()
		{
			PageBreak1.Enabled = false;
			Image1.Visible = false;
			Image2.Visible = false;
			Image3.Visible = false;
			Image4.Visible = false;
			Image5.Visible = false;
			Image6.Visible = false;
			if (!modDataTypes.Statics.MR.EndOfFile())
			{
				if (modGlobalVariables.Statics.CustomizedInfo.boolShowPicOnVal || modGlobalVariables.Statics.CustomizedInfo.boolShowSketchOnVal)
				{
					if (ShowPicsSketches(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modDataTypes.Statics.MR.Get_Fields_Int32("id"), FCConvert.ToInt16(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")), Guid.Parse( modDataTypes.Statics.MR.Get_Fields_String("CardId"))))
					{
						PageBreak1.Enabled = true;
                        if (!accountsByPage.ContainsKey(this.CurrentPage.Identifier))
                        {
                            accountsByPage.Add(this.CurrentPage.Identifier,modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
                        }
					}
					else
					{
						PageBreak1.Enabled = false;
					}
				}
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
			if (!modDataTypes.Statics.MR.EndOfFile())
			{
				if (!PageBreak1.Enabled || boolDidntUpdateHeader)
				{
					boolDidntUpdateHeader = false;
					txtCard.Visible = true;
					Label4.Visible = true;
					Label6.Visible = true;
					txtNumCards.Visible = true;
					txtCard.Text = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("rscard"));
					txtNumCards.Text = intCards.ToString();
					txtAccount.Text = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
					txtName.Text = "";
                    txtSecName.Text = "";
					if (modGlobalVariables.Statics.boolRegRecords)
                    {
                        txtName.Text = modDataTypes.Statics.MR.Get_Fields_String("DeedName1");
                        txtSecName.Text = modDataTypes.Statics.MR.Get_Fields_String("DeedName2");
                    }
					else
					{
						txtName.Text = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsName")));
						if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rssecowner") + "") != string.Empty)
						{
							txtSecName.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rssecowner") + "");
						}
					}
					txtMapLot.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + "");
					txtLocation.Text = Strings.Trim(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + " " + modDataTypes.Statics.MR.Get_Fields_String("rslocapt")) + " " + modDataTypes.Statics.MR.Get_Fields_String("rslocstreet"));
				}
				else
				{
					boolDidntUpdateHeader = true;
				}
			}
			else
			{
				txtCard.Visible = false;
				Label4.Visible = false;
				Label6.Visible = false;
				txtNumCards.Visible = false;
			}
		}

		public void Init(int intRangeType, int intOrderType, bool modalDialog, int intTownCode = -1, bool boolModal = false, bool boolChangesOnly = false, Action reportClosed = null, bool exportAsPDFs = false, bool boolSilent = false,string tempExportFolder = "")
        {
            exportPDFs = exportAsPDFs;
            runSilent = boolSilent;
            exportFolder = tempExportFolder;
			intTownNumber = intTownCode;
			boolSpecific = false;
			boolOnlyChanges = boolChangesOnly;
			this.reportClosed = reportClosed;
            var preview = true;
			GridSummary = new FCGrid();
			GridSummary.FixedCols = 0;
			GridSummary.FixedRows = 0;
			GridSummary.Cols = 12;
			GridSummary.Rows = 1;
			switch (intRangeType)
			{
				case 0:
					{
						// no range
						modGlobalVariables.Statics.boolByRange = false;
						boolSpecific = false;
						boolFromExtract = false;
						break;
					}
				case 1:
					{
						// range
						modGlobalVariables.Statics.boolByRange = true;
						boolSpecific = false;
						boolFromExtract = false;
						break;
					}
				case 2:
					{
						// specific accounts
						modGlobalVariables.Statics.boolByRange = false;
						boolSpecific = true;
						boolFromExtract = false;
						break;
					}
				case 3:
					{
						// from extract
						modGlobalVariables.Statics.boolByRange = false;
						boolSpecific = false;
						boolFromExtract = true;
						break;
					}
				case 4:
					{
						// town code
						modGlobalVariables.Statics.boolByRange = false;
						boolSpecific = false;
						boolFromExtract = false;
						break;
					}
			}
			//end switch
			modPrintRoutines.Statics.intwhichorder = intOrderType;
            //if (exportPDFs && !runSilent)
            //{
            //    var previewChoice = MessageBox.Show("Would you like to preview the pages?", "View Pages?",
            //        MessageBoxButtons.YesNo);
            //    preview = previewChoice == DialogResult.Yes;
            //}
            if (preview && !runSilent)
            {
                if (!boolModal)
                {
                    frmReportViewer.InstancePtr.Init(this, showModal: modalDialog, allowCancel:true);
                }
                else
                {
                    frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal),
                        showModal: modalDialog, allowCancel:true);
                }
            }
            else
            {
                //if (!runSilent)
                //{
                  //  this.Run(true);
                //}
                //else
                //{
                   this.Run(false);
                //}
            }

        }
		private bool ShowPicsSketches(int lngAcct, int lngID,  short intCard, Guid cardIdentifier)
		{
			bool ShowPicsSketches = false;
			bool boolPics;
			bool boolSketch;
			string strSketches;
			string[] strAry = null;
			string strPics;
			int intMaxPics;
			int x;
            bool showedAnImage = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			ShowPicsSketches = false;
			Image1.Image = null;
			Image2.Image = null;
			Image3.Image = null;
			Image4.Image = null;
			Image5.Image = null;
			Image6.Image = null;
			Image1.Height = 2880 / 1440f;
			Image2.Height = 2880 / 1440f;
			Image3.Height = 2880 / 1440f;
			Image4.Height = 2880 / 1440f;
			Image5.Height = 2880 / 1440f;
			Image6.Height = 2880 / 1440f;
			Image1.Width = 3;
			Image2.Width = 3;
			Image3.Width = 3;
			Image4.Width = 3;
			Image5.Width = 3;
			Image6.Width = 3;
			Image5.Top = 7380 / 1440f;
			boolPics = modGlobalVariables.Statics.CustomizedInfo.boolShowPicOnVal;
			boolSketch = modGlobalVariables.Statics.CustomizedInfo.boolShowSketchOnVal;

            int sketchId = 0;
			if (boolSketch)
			{

                var sketchInfos = StaticSettings.GlobalCommandDispatcher.Send(new GetSketchInfos(cardIdentifier)).Result.ToList();
                if (!sketchInfos.Any())
                {
                    boolSketch = false;
                }
                else
                {
                    sketchId = sketchInfos.FirstOrDefault().Id;
                }
            }
			strPics = "";
            List<PictureRecord> picList = new List<PictureRecord>();
            IPropertyPicturesViewModel picturesVM = null;
            
            
            if (boolPics)
			{
                picturesVM = StaticSettings.GlobalCommandDispatcher.Send(new GetPropertyPictureViewModel()).Result;
                picturesVM.LoadPictures(lngAcct, intCard, cardIdentifier);
                if (picturesVM.Pictures.Count() < 1)
                {
                    boolPics = false;
                }
            }
			intMaxPics = 0;
			if (boolPics && boolSketch)
			{
				intMaxPics = 5;
			}
			else if (boolPics)
			{
				intMaxPics = 6;
			}
			else if (boolSketch)
			{
				intMaxPics = 0;
			}
			if (boolPics && picturesVM != null)
            {
                picturesVM.Pictures.SetToFirstPicture();
                //int limit = intMaxPics;
                if (modGlobalVariables.Statics.CustomizedInfo.boolShowFirstPiconVal)
                    intMaxPics = 1;
                x = 0;
                if (picturesVM.Pictures.Count() < intMaxPics)
                {
                    intMaxPics = picturesVM.Pictures.Count();
                }
                if (intMaxPics == 1 )
                {
                    Detail.Controls["Image1"].Width = 6;
                    Detail.Controls["Image1"].Height = 4;
                }

                while (picturesVM.Pictures.HasCurrentPicture() && x < intMaxPics)
                {
                    x++;
                    var picData = picturesVM.GetCurrentPicture();
                    var picControl =
                        (Detail.Controls["Image" + x] as GrapeCity.ActiveReports.SectionReportModel.Picture);
                    if (picData != null)
                    {
                        showedAnImage = true;
                        picControl.Image = FCUtils.PictureFromBytes(picData, picControl.Width, picControl.Height);
                    }

                    picturesVM.Pictures.NextPicture();
                }

            }

			if (boolSketch)
			{
				Detail.Controls["Image" + (intMaxPics + 1)].Height = 4;
                int imageNo = intMaxPics + 1;
				if (intMaxPics == 0)
				{
					Detail.Controls["Image" + (intMaxPics + 1)].Width = 6;
					Detail.Controls["Image" + (intMaxPics + 1)].Height = 8;					
				}
				else if (intMaxPics == 1 || intMaxPics == 2)
				{
					Detail.Controls["Image5"].Width = 6;
					Detail.Controls["Image5"].Height = 4;
                    imageNo = 5;					
					Detail.Controls["Image5"].Top = Detail.Controls["Image1"].Top + Detail.Controls["Image1"].Height + .5f;
				}
                var width = Detail.Controls["Image" + imageNo].Width ;
                var height = Detail.Controls["Image" + imageNo].Height;
                
                (Detail.Controls["Image" + imageNo] as GrapeCity.ActiveReports.SectionReportModel.Picture).Image = FCUtils.PictureFromBytes(StaticSettings.GlobalCommandDispatcher.Send(new GetSketchImage(sketchId)).Result, width,height);
                if ((Detail.Controls["Image" + imageNo] as GrapeCity.ActiveReports.SectionReportModel.Picture).Image !=
                    null)
                {
                    showedAnImage = true;
                }

			}
			if ((boolSketch || boolPics) && showedAnImage)
			{
				ShowPicsSketches = true;
			}
			if (ShowPicsSketches)
			{
				Image1.Visible = true;
				Image2.Visible = true;
				Image3.Visible = true;
				Image4.Visible = true;
				Image5.Visible = true;
				Image6.Visible = true;
			}
			return ShowPicsSketches;
		}

		private void rptRangeValuation_Load(object sender, System.EventArgs e)
		{

		}

        private void SavePDFFiles()
        {
            int lastAccount = 0;
            int startPage = 0;
            int endPage = 0;
            int currentPage = 0;
            bool processedAPage = false;
            var pdfFolder = exportFolder;
            foreach (GrapeCity.ActiveReports.Document.Section.Page page in this.Document.Pages)
            {
                processedAPage = true;
                currentPage += 1;
                var identifier =  page.Identifier;
                int account = 0;
                if (accountsByPage.ContainsKey(identifier))
                {
                    if (accountsByPage.TryGetValue(identifier, out account))
                    {
                        if (account != lastAccount)
                        {
                            if (lastAccount > 0)
                            {
                                var fileName = lastAccount.ToString() + ".pdf";
                                SaveRange(Path.Combine(pdfFolder, fileName), startPage, endPage);
                            }
                            startPage = currentPage;
                        }
                    }
                }

                lastAccount = account;
                endPage = currentPage;
            }

            if (lastAccount > 0)
            {
                var fileName = lastAccount.ToString() + ".pdf";
                SaveRange(Path.Combine(pdfFolder, fileName), startPage, endPage);
            }

        }

        private void SaveRange(string fileName, int startPage,int endPage)
        {
            if (!String.IsNullOrWhiteSpace(fileName))
            {
                if (endPage >= startPage)
                {
                    GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport pdfExporter = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                    string pageRange;
                    if (endPage > startPage)
                    {
                        pageRange = startPage.ToString() + " - " + endPage.ToString();
                    }
                    else
                    {
                        pageRange = startPage.ToString();
                    }
                    pdfExporter.Export(this.Document,fileName,pageRange);                    
                }
            }
        }      

        private string GetBaseTempFolder()
        {
            string tempFolder = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }

            return tempFolder;
        }

       
    }
}
