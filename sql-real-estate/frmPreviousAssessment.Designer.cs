﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPreviousAssessment.
	/// </summary>
	partial class frmPreviousAssessment : BaseForm
	{
		public FCGrid GridDelete;
		public FCGrid Grid;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCLabel lblCurrentAcct;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuImport;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPreviousAssessment));
            this.GridDelete = new fecherFoundation.FCGrid();
            this.Grid = new fecherFoundation.FCGrid();
            this.txtAccount = new fecherFoundation.FCTextBox();
            this.lblCurrentAcct = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuImport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdImport = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdImport)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 392);
            this.BottomPanel.Size = new System.Drawing.Size(691, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridDelete);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.txtAccount);
            this.ClientArea.Controls.Add(this.lblCurrentAcct);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(691, 332);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAdd);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdImport);
            this.TopPanel.Size = new System.Drawing.Size(691, 80);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdImport, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAdd, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(249, 30);
            this.HeaderText.Text = "Previous Assessment";
            // 
            // GridDelete
            // 
            this.GridDelete.AllowSelection = false;
            this.GridDelete.AllowUserToResizeColumns = false;
            this.GridDelete.AllowUserToResizeRows = false;
            this.GridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridDelete.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridDelete.BackColorBkg = System.Drawing.Color.Empty;
            this.GridDelete.BackColorFixed = System.Drawing.Color.Empty;
            this.GridDelete.BackColorSel = System.Drawing.Color.Empty;
            this.GridDelete.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridDelete.Cols = 1;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridDelete.ColumnHeadersHeight = 30;
            this.GridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridDelete.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridDelete.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridDelete.DragIcon = null;
            this.GridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridDelete.FixedCols = 0;
            this.GridDelete.FixedRows = 0;
            this.GridDelete.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridDelete.FrozenCols = 0;
            this.GridDelete.GridColor = System.Drawing.Color.Empty;
            this.GridDelete.GridColorFixed = System.Drawing.Color.Empty;
            this.GridDelete.Location = new System.Drawing.Point(452, 5);
            this.GridDelete.Name = "GridDelete";
            this.GridDelete.OutlineCol = 0;
            this.GridDelete.ReadOnly = true;
            this.GridDelete.RowHeadersVisible = false;
            this.GridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridDelete.RowHeightMin = 0;
            this.GridDelete.Rows = 0;
            this.GridDelete.ScrollTipText = null;
            this.GridDelete.ShowColumnVisibilityMenu = false;
            this.GridDelete.ShowFocusCell = false;
            this.GridDelete.Size = new System.Drawing.Size(66, 25);
            this.GridDelete.StandardTab = true;
            this.GridDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridDelete.TabIndex = 6;
            this.GridDelete.Visible = false;
            // 
            // Grid
            // 
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 8;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.Grid.ColumnHeadersHeight = 30;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle4;
            this.Grid.DragIcon = null;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.FixedCols = 0;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(30, 111);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.RowHeadersVisible = false;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 1;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(631, 213);
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid.TabIndex = 5;
            this.Grid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.Grid_BeforeEdit);
            this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // txtAccount
            // 
            this.txtAccount.AutoSize = false;
            this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccount.LinkItem = null;
            this.txtAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAccount.LinkTopic = null;
            this.txtAccount.Location = new System.Drawing.Point(154, 30);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(167, 40);
            this.txtAccount.TabIndex = 1;
            this.txtAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAccount_KeyDown);
            this.txtAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccount_Validating);
            // 
            // lblCurrentAcct
            // 
            this.lblCurrentAcct.Location = new System.Drawing.Point(553, 42);
            this.lblCurrentAcct.Name = "lblCurrentAcct";
            this.lblCurrentAcct.Size = new System.Drawing.Size(118, 17);
            this.lblCurrentAcct.TabIndex = 3;
            this.lblCurrentAcct.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(355, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(147, 17);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "CURRENT ACCOUNT";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(30, 90);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(453, 17);
            this.lblName.TabIndex = 4;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(58, 17);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "ACCOUNT";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAdd,
            this.mnuDelete,
            this.mnuSepar3,
            this.mnuImport,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuAdd
            // 
            this.mnuAdd.Index = 0;
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Text = "Add Row";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Row";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 2;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuImport
            // 
            this.mnuImport.Index = 3;
            this.mnuImport.Name = "mnuImport";
            this.mnuImport.Text = "Import (All) from billing";
            this.mnuImport.Click += new System.EventHandler(this.mnuImport_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 4;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 5;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 6;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 7;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 8;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(304, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAdd
            // 
            this.cmdAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAdd.AppearanceKey = "toolbarButton";
            this.cmdAdd.Location = new System.Drawing.Point(339, 29);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(74, 24);
            this.cmdAdd.TabIndex = 1;
            this.cmdAdd.Text = "Add Row";
            this.cmdAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.AppearanceKey = "toolbarButton";
            this.cmdDelete.Location = new System.Drawing.Point(419, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(90, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete Row";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // cmdImport
            // 
            this.cmdImport.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdImport.AppearanceKey = "toolbarButton";
            this.cmdImport.Location = new System.Drawing.Point(605, 29);
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Size = new System.Drawing.Size(148, 24);
            this.cmdImport.TabIndex = 3;
            this.cmdImport.Text = "Import (All) from billing";
            this.cmdImport.Click += new System.EventHandler(this.mnuImport_Click);
            // 
            // frmPreviousAssessment
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(691, 500);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPreviousAssessment";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Previous Assessment";
            this.Load += new System.EventHandler(this.frmPreviousAssessment_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPreviousAssessment_KeyDown);
            this.Resize += new System.EventHandler(this.frmPreviousAssessment_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdImport)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdDelete;
		private FCButton cmdAdd;
		private FCButton cmdImport;
	}
}
