﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptCostFiles.
	/// </summary>
	partial class rptCostFiles
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCostFiles));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtKey = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnitData = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBaseData = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUnit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBase = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtKey)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnitData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaseData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtKey,
            this.txtUnitData,
            this.txtBaseData,
            this.txtLDesc,
            this.txtSDesc});
			this.Detail.Height = 0.2F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtKey
			// 
			this.txtKey.CanGrow = false;
			this.txtKey.Height = 0.19F;
			this.txtKey.Left = 0F;
			this.txtKey.MultiLine = false;
			this.txtKey.Name = "txtKey";
			this.txtKey.Text = null;
			this.txtKey.Top = 0F;
			this.txtKey.Width = 0.438F;
			// 
			// txtUnitData
			// 
			this.txtUnitData.CanGrow = false;
			this.txtUnitData.Height = 0.19F;
			this.txtUnitData.Left = 0.625F;
			this.txtUnitData.MultiLine = false;
			this.txtUnitData.Name = "txtUnitData";
			this.txtUnitData.Style = "text-align: right";
			this.txtUnitData.Text = null;
			this.txtUnitData.Top = 0F;
			this.txtUnitData.Width = 1.813F;
			// 
			// txtBaseData
			// 
			this.txtBaseData.CanGrow = false;
			this.txtBaseData.Height = 0.19F;
			this.txtBaseData.Left = 2.5F;
			this.txtBaseData.MultiLine = false;
			this.txtBaseData.Name = "txtBaseData";
			this.txtBaseData.Style = "text-align: right";
			this.txtBaseData.Text = null;
			this.txtBaseData.Top = 0F;
			this.txtBaseData.Width = 1.438F;
			// 
			// txtLDesc
			// 
			this.txtLDesc.CanGrow = false;
			this.txtLDesc.Height = 0.19F;
			this.txtLDesc.Left = 4.0625F;
			this.txtLDesc.MultiLine = false;
			this.txtLDesc.Name = "txtLDesc";
			this.txtLDesc.Text = null;
			this.txtLDesc.Top = 0F;
			this.txtLDesc.Width = 2.188F;
			// 
			// txtSDesc
			// 
			this.txtSDesc.CanGrow = false;
			this.txtSDesc.Height = 0.19F;
			this.txtSDesc.Left = 6.3125F;
			this.txtSDesc.MultiLine = false;
			this.txtSDesc.Name = "txtSDesc";
			this.txtSDesc.Text = null;
			this.txtSDesc.Top = 0F;
			this.txtSDesc.Width = 1.125F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtPage,
            this.lblTitle,
            this.txtUnit,
            this.txtBase,
            this.Field3,
            this.Field4,
            this.Field5,
            this.txtMuni,
            this.txtTime,
            this.txtDate});
			this.PageHeader.Height = 0.68F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.18F;
			this.txtPage.Left = 6.562F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.219F;
			this.txtPage.Width = 0.875F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = null;
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5625F;
			// 
			// txtUnit
			// 
			this.txtUnit.CanGrow = false;
			this.txtUnit.Height = 0.18F;
			this.txtUnit.Left = 0.5625F;
			this.txtUnit.MultiLine = false;
			this.txtUnit.Name = "txtUnit";
			this.txtUnit.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.txtUnit.Text = "Unit";
			this.txtUnit.Top = 0.4375F;
			this.txtUnit.Width = 1.875F;
			// 
			// txtBase
			// 
			this.txtBase.CanGrow = false;
			this.txtBase.Height = 0.18F;
			this.txtBase.Left = 2.562F;
			this.txtBase.MultiLine = false;
			this.txtBase.Name = "txtBase";
			this.txtBase.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.txtBase.Text = "Base";
			this.txtBase.Top = 0.437F;
			this.txtBase.Width = 1.375F;
			// 
			// Field3
			// 
			this.Field3.CanGrow = false;
			this.Field3.Height = 0.19F;
			this.Field3.Left = 4.062F;
			this.Field3.MultiLine = false;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field3.Text = "Long Description";
			this.Field3.Top = 0.437F;
			this.Field3.Width = 1.563F;
			// 
			// Field4
			// 
			this.Field4.CanGrow = false;
			this.Field4.Height = 0.18F;
			this.Field4.Left = 6.3125F;
			this.Field4.MultiLine = false;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field4.Text = "Short Desc.";
			this.Field4.Top = 0.4375F;
			this.Field4.Width = 1.063F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.21875F;
			this.Field5.Left = 0F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field5.Text = "Key";
			this.Field5.Top = 0.4375F;
			this.Field5.Width = 0.375F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.18F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.18F;
			this.txtTime.Left = 4.768372E-07F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.219F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.18F;
			this.txtDate.Left = 6.062F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.031F;
			this.txtDate.Width = 1.375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptCostFiles
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtKey)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnitData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaseData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKey;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnitData;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBaseData;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSDesc;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBase;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
