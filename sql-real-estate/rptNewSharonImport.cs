﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewSharonImport.
	/// </summary>
	public partial class rptNewSharonImport : BaseSectionReport
	{
		public static rptNewSharonImport InstancePtr
		{
			get
			{
				return (rptNewSharonImport)Sys.GetInstance(typeof(rptNewSharonImport));
			}
		}

		protected rptNewSharonImport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptNewSharonImport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptNewSharonImport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngRow;
		const int CNSTGRIDCOLACCOUNT = 0;
		const int CNSTGRIDCOLOWNER = 1;
		const int CNSTGRIDCOLMAPLOT = 2;
		const int CNSTGRIDCOLLAND = 3;
		const int CNSTGRIDCOLBLDG = 4;
		const int CNSTGRIDCOLEXEMPTION = 5;
		const int CNSTGRIDCOLNEW = 6;
		private int lngNew;
		private int lngCount;
		private double dblLandTot;
		private double dblBldgTot;

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "Import", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngRow > frmImportNewSharon.InstancePtr.Grid.Rows - 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
			lngRow = 1;
			lngNew = 0;
			lngCount = 0;
			dblLandTot = 0;
			dblBldgTot = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngRow < frmImportNewSharon.InstancePtr.Grid.Rows)
			{
				txtAcct.Text = frmImportNewSharon.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT);
				txtBldg.Text = Strings.Format(Conversion.Val(frmImportNewSharon.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLBLDG)), "#,###,###,##0");
				dblBldgTot += Conversion.Val(frmImportNewSharon.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLBLDG));
				txtLand.Text = Strings.Format(Conversion.Val(frmImportNewSharon.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLLAND)), "#,###,###,##0");
				dblLandTot += Conversion.Val(frmImportNewSharon.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLLAND));
				txtMapLot.Text = frmImportNewSharon.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT);
				txtName.Text = frmImportNewSharon.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNER);
				if (FCConvert.CBool(frmImportNewSharon.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLNEW)))
				{
					txtNew.Text = "New";
					lngNew += 1;
				}
				else
				{
					txtNew.Text = "";
					lngCount += 1;
				}
				lngRow += 1;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtNumNew.Text = Strings.Format(lngNew, "#,###,##0");
			txtNumUpdated.Text = Strings.Format(lngCount, "#,###,##0");
			txtTotLand.Text = Strings.Format(dblLandTot, "#,###,###,##0");
			txtTotBuilding.Text = Strings.Format(dblBldgTot, "#,###,###,##0");
		}

		
	}
}
