﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class modPrintRoutines
	{
		public static object GetMaxString()
		{
			object GetMaxString = null;
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT MAX(" + Statics.gstrFieldName + ") as MaxAccount FROM Master", modGlobalVariables.strREDatabase);
			// TODO Get_Fields: Field [MaxAccount] not found!! (maybe it is an alias?)
			GetMaxString = rsTemp.Get_Fields("MaxAccount");
			return GetMaxString;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string fixthedate(ref string inputstring)
		{
			string fixthedate = "";
			string datstring;
			int slen;
			string tstring = "";
			// a temporary string
			int yr = 0;
			// will hold the year
			int curyear = 0;
			datstring = Strings.Mid(inputstring, 1, 2);
			datstring += "/";
			datstring += Strings.Mid(inputstring, 3, 2);
			datstring += "/";
			// if the string is 8 chars then the year is already done
			// if not, then decide whether to put a 19 or 20 first
			// the only dates not in 4 digits should be the inspection
			// date so it will be a fairly recent year
			slen = inputstring.Length;
			switch (slen)
			{
				case 6:
					{
						tstring = Strings.Mid(inputstring, 5, 2);
						curyear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(DateTime.Today), 2))));
						yr = FCConvert.ToInt32(Math.Round(Conversion.Val(tstring)));
						if (yr > curyear)
						{
							datstring += "19";
						}
						else
						{
							datstring += "20";
						}
						datstring += tstring;
						break;
					}
				case 8:
					{
						datstring += Strings.Mid(inputstring, 5, 4);
						break;
					}
				default:
					{
						datstring = inputstring;
						break;
					}
			}
			//end switch
			fixthedate = datstring;
			return fixthedate;
		}

		public class StaticVariables
		{
            public string gstrFieldName = string.Empty;



			// vbPorter upgrade warning: gstrMinAccountRange As string	OnWrite(string, int)
			public string gstrMinAccountRange = "";
			// vbPorter upgrade warning: gstrMaxAccountRange As string	OnWrite(string, int)
			public string gstrMaxAccountRange = "";
			public int intwhichorder;
			// 1,2,3 or 4. which order to print in
			public string regorchanged = string.Empty;
			public string allorexempt = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
