﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptpdfPropertyCard1.
	/// </summary>
	partial class srptpdfPropertyCard1
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptpdfPropertyCard1));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCards = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine1L1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine2L1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine3L1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine4L1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine5L1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine6L1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine1L2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine2L2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine3L2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine4L2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine5L2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine6L2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine1L3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine2L3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine3L3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine4L3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine5L3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine6L3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine1L4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine2L4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine3L4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine4L4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine5L4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine6L4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNotes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtNeighborhood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTreeGrowth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblXCoord = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtXCoord = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblYCoord = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYCoord = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtZone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSecZone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTopography1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTopCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopography2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUtility1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtUtilCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtilCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtilCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtilCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtilCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtilCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtilCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtilCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtilCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtility2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtStreetCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreetCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreetCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreetCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreetCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreetCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreetCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreetCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreetCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblOpen1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOpen1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblOpen2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOpen2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSalePrice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtSaleTypeCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleTypeCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleTypeCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleTypeCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleTypeCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleTypeCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleTypeCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleTypeCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleTypeCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleFinancing = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtFinancingCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinancingCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinancingCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinancingCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinancingCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinancingCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinancingCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinancingCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinancingCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtValidity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtValidityCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValidityCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValidityCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValidityCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValidityCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValidityCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValidityCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValidityCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValidityCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtVerified = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtVerifiedCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVerifiedCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVerifiedCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVerifiedCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVerifiedCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVerifiedCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVerifiedCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVerifiedCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVerifiedCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLandDesc11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFrontage1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepth1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtFrontFootFactor1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFrontFootCode1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontage2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepth2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootFactor2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFrontFootCode2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontage3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepth3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootType3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootFactor3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFrontFootCode3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontage4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepth4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootType4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootFactor4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFrontFootCode4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontage5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepth5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootType5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootFactor5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFrontFootCode5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontage6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepth6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootType6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootFactor6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFrontFootCode6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontage7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepth7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootType7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFrontFootFactor7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFrontFootCode7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSqftUnits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftFactor1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSqftCode1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftFactor2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSqftCode2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftUnits3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftType3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftFactor3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSqftCode3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftUnits4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftType4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftFactor4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSqftCode4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftUnits5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftType5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftFactor5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSqftCode5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftUnits6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftType6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftFactor6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSqftCode6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftUnits7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftType7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSqftFactor7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSqftCode7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcreageUnits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageFactor1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcreageCode1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageFactor2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcreageCode2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageUnits3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageType3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageFactor3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcreageCode3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageUnits4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageType4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageFactor4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcreageCode4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageUnits5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageType5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageFactor5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcreageCode5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageUnits6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageType6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageFactor6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcreageCode6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageUnits7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageType7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcreageFactor7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcreageCode7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line74 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line77 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line78 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line79 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line81 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line82 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line83 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line84 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line85 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line86 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line87 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line88 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLandDesc16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtInfluenceCost1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfluenceCost2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfluenceCost3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfluenceCost4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfluenceCost5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfluenceCost6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfluenceCost7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfluenceCost8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfluenceCost9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line80 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLandDesc24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLandDesc30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line89 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line90 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line93 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line94 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtAssYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssYear6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssLand14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssBldg14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssExempt14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssTotal6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCards)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1L1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2L1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3L1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4L1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5L1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6L1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1L2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2L2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3L2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4L2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5L2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6L2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1L3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2L3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3L3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4L3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5L3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6L3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1L4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2L4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3L4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4L4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5L4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6L4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeighborhood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTreeGrowth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblXCoord)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtXCoord)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYCoord)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYCoord)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopography1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopography2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtility1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtility2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalePrice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleFinancing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerified)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label17,
				this.Label1,
				this.txtMapLot,
				this.Label2,
				this.txtAccount,
				this.Label3,
				this.Label4,
				this.txtCard,
				this.Label5,
				this.txtCards,
				this.txtDate,
				this.txtLine1L1,
				this.txtLine2L1,
				this.txtLine3L1,
				this.txtLine4L1,
				this.txtLine5L1,
				this.txtLine6L1,
				this.Line1,
				this.txtLine1L2,
				this.txtLine2L2,
				this.txtLine3L2,
				this.txtLine4L2,
				this.txtLine5L2,
				this.txtLine6L2,
				this.txtLine1L3,
				this.txtLine2L3,
				this.txtLine3L3,
				this.txtLine4L3,
				this.txtLine5L3,
				this.txtLine6L3,
				this.Line2,
				this.Line3,
				this.txtLine1L4,
				this.txtLine2L4,
				this.txtLine3L4,
				this.txtLine4L4,
				this.txtLine5L4,
				this.txtLine6L4,
				this.Line4,
				this.Field11,
				this.Label6,
				this.Line5,
				this.Label7,
				this.Field12,
				this.txtNotes,
				this.Line7,
				this.txtLocation,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Line10,
				this.txtNeighborhood,
				this.Line12,
				this.Label11,
				this.txtTreeGrowth,
				this.Line13,
				this.lblXCoord,
				this.txtXCoord,
				this.Line14,
				this.lblYCoord,
				this.txtYCoord,
				this.Label12,
				this.txtZone,
				this.Line16,
				this.Label13,
				this.txtSecZone,
				this.Line18,
				this.Label14,
				this.txtTopography1,
				this.Line20,
				this.txtTopCost1,
				this.txtTopCost2,
				this.txtTopCost3,
				this.txtTopCost8,
				this.txtTopCost9,
				this.txtTopCost7,
				this.txtTopCost4,
				this.txtTopCost5,
				this.txtTopCost6,
				this.txtTopography2,
				this.Label15,
				this.txtUtility1,
				this.Line22,
				this.txtUtilCost1,
				this.txtUtilCost2,
				this.txtUtilCost3,
				this.txtUtilCost8,
				this.txtUtilCost9,
				this.txtUtilCost7,
				this.txtUtilCost4,
				this.txtUtilCost5,
				this.txtUtilCost6,
				this.txtUtility2,
				this.Label16,
				this.txtStreet,
				this.Line24,
				this.txtStreetCost1,
				this.txtStreetCost2,
				this.txtStreetCost3,
				this.txtStreetCost8,
				this.txtStreetCost9,
				this.txtStreetCost7,
				this.txtStreetCost4,
				this.txtStreetCost5,
				this.txtStreetCost6,
				this.Line25,
				this.lblOpen1,
				this.txtOpen1,
				this.Line26,
				this.lblOpen2,
				this.txtOpen2,
				this.Line27,
				this.Line28,
				this.Label18,
				this.txtSaleDate,
				this.Line29,
				this.Label19,
				this.txtSalePrice,
				this.Label20,
				this.txtSaleType,
				this.Line31,
				this.txtSaleTypeCost1,
				this.txtSaleTypeCost2,
				this.txtSaleTypeCost3,
				this.txtSaleTypeCost8,
				this.txtSaleTypeCost9,
				this.txtSaleTypeCost7,
				this.txtSaleTypeCost4,
				this.txtSaleTypeCost5,
				this.txtSaleTypeCost6,
				this.Label21,
				this.txtSaleFinancing,
				this.Line33,
				this.txtFinancingCost1,
				this.txtFinancingCost2,
				this.txtFinancingCost3,
				this.txtFinancingCost8,
				this.txtFinancingCost9,
				this.txtFinancingCost7,
				this.txtFinancingCost4,
				this.txtFinancingCost5,
				this.txtFinancingCost6,
				this.Label22,
				this.txtValidity,
				this.Line35,
				this.txtValidityCost1,
				this.txtValidityCost2,
				this.txtValidityCost3,
				this.txtValidityCost8,
				this.txtValidityCost9,
				this.txtValidityCost7,
				this.txtValidityCost4,
				this.txtValidityCost5,
				this.txtValidityCost6,
				this.Label23,
				this.txtVerified,
				this.Line37,
				this.txtVerifiedCost1,
				this.txtVerifiedCost2,
				this.txtVerifiedCost3,
				this.txtVerifiedCost8,
				this.txtVerifiedCost9,
				this.txtVerifiedCost7,
				this.txtVerifiedCost4,
				this.txtVerifiedCost5,
				this.txtVerifiedCost6,
				this.Line6,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Line38,
				this.Line39,
				this.Line40,
				this.Line41,
				this.Line9,
				this.Line42,
				this.Line43,
				this.Line44,
				this.Line45,
				this.Line46,
				this.Line47,
				this.Line48,
				this.Line49,
				this.Line50,
				this.Line51,
				this.Line52,
				this.Line53,
				this.Line54,
				this.Line55,
				this.Label29,
				this.Line56,
				this.Line58,
				this.Label30,
				this.Line8,
				this.Label31,
				this.txtLandDesc11,
				this.txtLandDesc12,
				this.txtLandDesc13,
				this.txtLandDesc14,
				this.txtLandDesc15,
				this.Label32,
				this.Line62,
				this.Label33,
				this.Label34,
				this.Line57,
				this.Label35,
				this.Label36,
				this.txtFrontage1,
				this.txtDepth1,
				this.txtFrontFootType1,
				this.Line65,
				this.Label37,
				this.Label38,
				this.Line63,
				this.txtFrontFootFactor1,
				this.Label39,
				this.txtFrontFootCode1,
				this.txtFrontage2,
				this.txtDepth2,
				this.txtFrontFootType2,
				this.txtFrontFootFactor2,
				this.Label40,
				this.txtFrontFootCode2,
				this.txtFrontage3,
				this.txtDepth3,
				this.txtFrontFootType3,
				this.txtFrontFootFactor3,
				this.Label41,
				this.txtFrontFootCode3,
				this.txtFrontage4,
				this.txtDepth4,
				this.txtFrontFootType4,
				this.txtFrontFootFactor4,
				this.Label42,
				this.txtFrontFootCode4,
				this.txtFrontage5,
				this.txtDepth5,
				this.txtFrontFootType5,
				this.txtFrontFootFactor5,
				this.Label43,
				this.txtFrontFootCode5,
				this.txtFrontage6,
				this.txtDepth6,
				this.txtFrontFootType6,
				this.txtFrontFootFactor6,
				this.Label44,
				this.txtFrontFootCode6,
				this.txtFrontage7,
				this.txtDepth7,
				this.txtFrontFootType7,
				this.txtFrontFootFactor7,
				this.Label45,
				this.txtFrontFootCode7,
				this.Line60,
				this.Line66,
				this.Line67,
				this.Line68,
				this.Line69,
				this.Line70,
				this.Line71,
				this.Line64,
				this.Line61,
				this.Label46,
				this.txtSqftUnits1,
				this.txtSqftType1,
				this.txtSqftFactor1,
				this.Label47,
				this.txtSqftCode1,
				this.txtSqftUnits2,
				this.txtSqftType2,
				this.txtSqftFactor2,
				this.Label48,
				this.txtSqftCode2,
				this.txtSqftUnits3,
				this.txtSqftType3,
				this.txtSqftFactor3,
				this.Label49,
				this.txtSqftCode3,
				this.txtSqftUnits4,
				this.txtSqftType4,
				this.txtSqftFactor4,
				this.Label50,
				this.txtSqftCode4,
				this.txtSqftUnits5,
				this.txtSqftType5,
				this.txtSqftFactor5,
				this.Label51,
				this.txtSqftCode5,
				this.txtSqftUnits6,
				this.txtSqftType6,
				this.txtSqftFactor6,
				this.Label52,
				this.txtSqftCode6,
				this.txtSqftUnits7,
				this.txtSqftType7,
				this.txtSqftFactor7,
				this.Label53,
				this.txtSqftCode7,
				this.Label54,
				this.txtAcreageUnits1,
				this.txtAcreageType1,
				this.txtAcreageFactor1,
				this.Label55,
				this.txtAcreageCode1,
				this.txtAcreageUnits2,
				this.txtAcreageType2,
				this.txtAcreageFactor2,
				this.Label56,
				this.txtAcreageCode2,
				this.txtAcreageUnits3,
				this.txtAcreageType3,
				this.txtAcreageFactor3,
				this.Label57,
				this.txtAcreageCode3,
				this.txtAcreageUnits4,
				this.txtAcreageType4,
				this.txtAcreageFactor4,
				this.Label58,
				this.txtAcreageCode4,
				this.txtAcreageUnits5,
				this.txtAcreageType5,
				this.txtAcreageFactor5,
				this.Label59,
				this.txtAcreageCode5,
				this.txtAcreageUnits6,
				this.txtAcreageType6,
				this.txtAcreageFactor6,
				this.Label60,
				this.txtAcreageCode6,
				this.txtAcreageUnits7,
				this.txtAcreageType7,
				this.txtAcreageFactor7,
				this.Label61,
				this.txtAcreageCode7,
				this.Line73,
				this.Line74,
				this.Line75,
				this.Line76,
				this.Line77,
				this.Line78,
				this.Line79,
				this.Line81,
				this.Line82,
				this.Line83,
				this.Line84,
				this.Line85,
				this.Line86,
				this.Line87,
				this.Line88,
				this.txtLandDesc16,
				this.txtLandDesc17,
				this.txtLandDesc18,
				this.txtLandDesc19,
				this.txtLandDesc20,
				this.Label62,
				this.Line72,
				this.txtInfluenceCost1,
				this.txtInfluenceCost2,
				this.txtInfluenceCost3,
				this.txtInfluenceCost4,
				this.txtInfluenceCost5,
				this.txtInfluenceCost6,
				this.txtInfluenceCost7,
				this.txtInfluenceCost8,
				this.txtInfluenceCost9,
				this.txtLandDesc21,
				this.txtLandDesc22,
				this.txtLandDesc23,
				this.Label63,
				this.Line80,
				this.Label64,
				this.txtLandDesc24,
				this.txtLandDesc25,
				this.txtLandDesc26,
				this.txtLandDesc27,
				this.txtLandDesc28,
				this.txtLandDesc29,
				this.Label65,
				this.txtLandDesc30,
				this.txtLandDesc31,
				this.txtLandDesc32,
				this.txtLandDesc33,
				this.txtLandDesc34,
				this.txtLandDesc35,
				this.txtLandDesc36,
				this.txtLandDesc37,
				this.txtLandDesc38,
				this.txtLandDesc39,
				this.txtLandDesc40,
				this.txtLandDesc41,
				this.txtLandDesc42,
				this.txtLandDesc43,
				this.txtLandDesc44,
				this.Line59,
				this.lblMuni,
				this.Label67,
				this.Label68,
				this.Label69,
				this.Line89,
				this.Line90,
				this.Line91,
				this.Line92,
				this.Line93,
				this.Line94,
				this.txtAssYear1,
				this.txtAssYear2,
				this.txtAssYear3,
				this.txtAssYear4,
				this.txtAssYear5,
				this.txtAssYear7,
				this.txtAssYear8,
				this.txtAssYear9,
				this.txtAssYear10,
				this.txtAssYear11,
				this.txtAssYear12,
				this.txtAssYear13,
				this.txtAssYear14,
				this.txtAssYear6,
				this.txtAssLand1,
				this.txtAssLand2,
				this.txtAssLand3,
				this.txtAssLand4,
				this.txtAssLand5,
				this.txtAssLand6,
				this.txtAssLand7,
				this.txtAssLand8,
				this.txtAssLand9,
				this.txtAssLand10,
				this.txtAssLand11,
				this.txtAssLand12,
				this.txtAssLand13,
				this.txtAssLand14,
				this.txtAssBldg1,
				this.txtAssBldg2,
				this.txtAssBldg3,
				this.txtAssBldg4,
				this.txtAssBldg5,
				this.txtAssBldg6,
				this.txtAssBldg7,
				this.txtAssBldg8,
				this.txtAssBldg9,
				this.txtAssBldg10,
				this.txtAssBldg11,
				this.txtAssBldg12,
				this.txtAssBldg13,
				this.txtAssBldg14,
				this.txtAssExempt1,
				this.txtAssExempt2,
				this.txtAssExempt3,
				this.txtAssExempt4,
				this.txtAssExempt5,
				this.txtAssExempt6,
				this.txtAssExempt7,
				this.txtAssExempt8,
				this.txtAssExempt9,
				this.txtAssExempt10,
				this.txtAssExempt11,
				this.txtAssExempt12,
				this.txtAssExempt13,
				this.txtAssExempt14,
				this.txtAssTotal1,
				this.txtAssTotal2,
				this.txtAssTotal3,
				this.txtAssTotal4,
				this.txtAssTotal5,
				this.txtAssTotal7,
				this.txtAssTotal8,
				this.txtAssTotal9,
				this.txtAssTotal10,
				this.txtAssTotal11,
				this.txtAssTotal12,
				this.txtAssTotal13,
				this.txtAssTotal14,
				this.txtAssTotal6,
				this.txtLandDesc45,
				this.txtLandDesc46,
				this.Label70,
				this.txtTotalAcres
			});
			this.Detail.Height = 7.499306F;
			this.Detail.Name = "Detail";
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.5625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label17.Text = "Sale Data";
			this.Label17.Top = 4.375F;
			this.Label17.Width = 1.875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 10pt";
			this.Label1.Text = "Map Lot";
			this.Label1.Top = 0F;
			this.Label1.Width = 0.625F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.1875F;
			this.txtMapLot.Left = 0.75F;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-size: 10pt";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0F;
			this.txtMapLot.Width = 1.625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.4375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 10pt";
			this.Label2.Text = "Account";
			this.Label2.Top = 0F;
			this.Label2.Width = 0.625F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 3.0625F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 10pt";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.75F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 10pt";
			this.Label3.Text = "Location";
			this.Label3.Top = 0F;
			this.Label3.Width = 0.6875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 7.375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 10pt";
			this.Label4.Text = "Card";
			this.Label4.Top = 0F;
			this.Label4.Width = 0.375F;
			// 
			// txtCard
			// 
			this.txtCard.Height = 0.1875F;
			this.txtCard.Left = 7.8125F;
			this.txtCard.Name = "txtCard";
			this.txtCard.Style = "font-size: 10pt";
			this.txtCard.Text = null;
			this.txtCard.Top = 0F;
			this.txtCard.Width = 0.3125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 8.1875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 10pt";
			this.Label5.Text = "Of";
			this.Label5.Top = 0F;
			this.Label5.Width = 0.25F;
			// 
			// txtCards
			// 
			this.txtCards.Height = 0.1875F;
			this.txtCards.Left = 8.5F;
			this.txtCards.Name = "txtCards";
			this.txtCards.Style = "font-size: 10pt";
			this.txtCards.Text = null;
			this.txtCards.Top = 0F;
			this.txtCards.Width = 0.3125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 8.9375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-size: 10pt";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1F;
			// 
			// txtLine1L1
			// 
			this.txtLine1L1.Height = 0.19F;
			this.txtLine1L1.Left = 0F;
			this.txtLine1L1.MultiLine = false;
			this.txtLine1L1.Name = "txtLine1L1";
			this.txtLine1L1.Style = "font-size: 9pt";
			this.txtLine1L1.Text = "Field5";
			this.txtLine1L1.Top = 0.25F;
			this.txtLine1L1.Width = 3.5F;
			// 
			// txtLine2L1
			// 
			this.txtLine2L1.Height = 0.19F;
			this.txtLine2L1.Left = 0F;
			this.txtLine2L1.MultiLine = false;
			this.txtLine2L1.Name = "txtLine2L1";
			this.txtLine2L1.Style = "font-size: 9pt";
			this.txtLine2L1.Text = "Field5";
			this.txtLine2L1.Top = 0.4166667F;
			this.txtLine2L1.Width = 3.5F;
			// 
			// txtLine3L1
			// 
			this.txtLine3L1.Height = 0.19F;
			this.txtLine3L1.Left = 0F;
			this.txtLine3L1.MultiLine = false;
			this.txtLine3L1.Name = "txtLine3L1";
			this.txtLine3L1.Style = "font-size: 9pt";
			this.txtLine3L1.Text = "Field5";
			this.txtLine3L1.Top = 0.5833333F;
			this.txtLine3L1.Width = 3.5F;
			// 
			// txtLine4L1
			// 
			this.txtLine4L1.Height = 0.19F;
			this.txtLine4L1.Left = 0F;
			this.txtLine4L1.MultiLine = false;
			this.txtLine4L1.Name = "txtLine4L1";
			this.txtLine4L1.Style = "font-size: 9pt";
			this.txtLine4L1.Text = "Field5";
			this.txtLine4L1.Top = 0.75F;
			this.txtLine4L1.Width = 3.5F;
			// 
			// txtLine5L1
			// 
			this.txtLine5L1.Height = 0.19F;
			this.txtLine5L1.Left = 0F;
			this.txtLine5L1.MultiLine = false;
			this.txtLine5L1.Name = "txtLine5L1";
			this.txtLine5L1.Style = "font-size: 9pt";
			this.txtLine5L1.Text = "Field5";
			this.txtLine5L1.Top = 0.9166667F;
			this.txtLine5L1.Width = 3.5F;
			// 
			// txtLine6L1
			// 
			this.txtLine6L1.Height = 0.19F;
			this.txtLine6L1.Left = 0F;
			this.txtLine6L1.MultiLine = false;
			this.txtLine6L1.Name = "txtLine6L1";
			this.txtLine6L1.Style = "font-size: 9pt";
			this.txtLine6L1.Text = "Field5";
			this.txtLine6L1.Top = 1.083333F;
			this.txtLine6L1.Width = 3.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.25F;
			this.Line1.Width = 3.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 3.5F;
			this.Line1.Y1 = 1.25F;
			this.Line1.Y2 = 1.25F;
			// 
			// txtLine1L2
			// 
			this.txtLine1L2.Height = 0.19F;
			this.txtLine1L2.Left = 0F;
			this.txtLine1L2.MultiLine = false;
			this.txtLine1L2.Name = "txtLine1L2";
			this.txtLine1L2.Style = "font-size: 9pt";
			this.txtLine1L2.Text = "Field5";
			this.txtLine1L2.Top = 1.25F;
			this.txtLine1L2.Width = 3.5F;
			// 
			// txtLine2L2
			// 
			this.txtLine2L2.Height = 0.19F;
			this.txtLine2L2.Left = 0F;
			this.txtLine2L2.MultiLine = false;
			this.txtLine2L2.Name = "txtLine2L2";
			this.txtLine2L2.Style = "font-size: 9pt";
			this.txtLine2L2.Text = "pg";
			this.txtLine2L2.Top = 1.40625F;
			this.txtLine2L2.Width = 3.5F;
			// 
			// txtLine3L2
			// 
			this.txtLine3L2.Height = 0.19F;
			this.txtLine3L2.Left = 0F;
			this.txtLine3L2.MultiLine = false;
			this.txtLine3L2.Name = "txtLine3L2";
			this.txtLine3L2.Style = "font-size: 9pt";
			this.txtLine3L2.Text = "Field5";
			this.txtLine3L2.Top = 1.5625F;
			this.txtLine3L2.Width = 3.5F;
			// 
			// txtLine4L2
			// 
			this.txtLine4L2.Height = 0.19F;
			this.txtLine4L2.Left = 0F;
			this.txtLine4L2.MultiLine = false;
			this.txtLine4L2.Name = "txtLine4L2";
			this.txtLine4L2.Style = "font-size: 9pt";
			this.txtLine4L2.Text = "Field5";
			this.txtLine4L2.Top = 1.71875F;
			this.txtLine4L2.Width = 3.5F;
			// 
			// txtLine5L2
			// 
			this.txtLine5L2.Height = 0.19F;
			this.txtLine5L2.Left = 0F;
			this.txtLine5L2.MultiLine = false;
			this.txtLine5L2.Name = "txtLine5L2";
			this.txtLine5L2.Style = "font-size: 9pt";
			this.txtLine5L2.Text = "Field5";
			this.txtLine5L2.Top = 1.875F;
			this.txtLine5L2.Width = 3.5F;
			// 
			// txtLine6L2
			// 
			this.txtLine6L2.Height = 0.19F;
			this.txtLine6L2.Left = 0F;
			this.txtLine6L2.MultiLine = false;
			this.txtLine6L2.Name = "txtLine6L2";
			this.txtLine6L2.Style = "font-size: 9pt";
			this.txtLine6L2.Text = "Field5";
			this.txtLine6L2.Top = 2.03125F;
			this.txtLine6L2.Width = 3.5F;
			// 
			// txtLine1L3
			// 
			this.txtLine1L3.Height = 0.19F;
			this.txtLine1L3.Left = 0F;
			this.txtLine1L3.MultiLine = false;
			this.txtLine1L3.Name = "txtLine1L3";
			this.txtLine1L3.Style = "font-size: 9pt";
			this.txtLine1L3.Text = "Field11";
			this.txtLine1L3.Top = 2.1875F;
			this.txtLine1L3.Width = 3.5F;
			// 
			// txtLine2L3
			// 
			this.txtLine2L3.Height = 0.19F;
			this.txtLine2L3.Left = 0F;
			this.txtLine2L3.MultiLine = false;
			this.txtLine2L3.Name = "txtLine2L3";
			this.txtLine2L3.Style = "font-size: 9pt";
			this.txtLine2L3.Text = "pg";
			this.txtLine2L3.Top = 2.34375F;
			this.txtLine2L3.Width = 3.5F;
			// 
			// txtLine3L3
			// 
			this.txtLine3L3.Height = 0.19F;
			this.txtLine3L3.Left = 0F;
			this.txtLine3L3.MultiLine = false;
			this.txtLine3L3.Name = "txtLine3L3";
			this.txtLine3L3.Style = "font-size: 9pt";
			this.txtLine3L3.Text = "Field5";
			this.txtLine3L3.Top = 2.5F;
			this.txtLine3L3.Width = 3.5F;
			// 
			// txtLine4L3
			// 
			this.txtLine4L3.Height = 0.19F;
			this.txtLine4L3.Left = 0F;
			this.txtLine4L3.MultiLine = false;
			this.txtLine4L3.Name = "txtLine4L3";
			this.txtLine4L3.Style = "font-size: 9pt";
			this.txtLine4L3.Text = "Field5";
			this.txtLine4L3.Top = 2.65625F;
			this.txtLine4L3.Width = 3.5F;
			// 
			// txtLine5L3
			// 
			this.txtLine5L3.Height = 0.19F;
			this.txtLine5L3.Left = 0F;
			this.txtLine5L3.MultiLine = false;
			this.txtLine5L3.Name = "txtLine5L3";
			this.txtLine5L3.Style = "font-size: 9pt";
			this.txtLine5L3.Text = "Field5";
			this.txtLine5L3.Top = 2.8125F;
			this.txtLine5L3.Width = 3.5F;
			// 
			// txtLine6L3
			// 
			this.txtLine6L3.Height = 0.19F;
			this.txtLine6L3.Left = 0F;
			this.txtLine6L3.MultiLine = false;
			this.txtLine6L3.Name = "txtLine6L3";
			this.txtLine6L3.Style = "font-size: 9pt";
			this.txtLine6L3.Text = "Field5";
			this.txtLine6L3.Top = 2.96875F;
			this.txtLine6L3.Width = 3.5F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 3.15625F;
			this.Line2.Width = 3.5F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 3.5F;
			this.Line2.Y1 = 3.15625F;
			this.Line2.Y2 = 3.15625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2.1875F;
			this.Line3.Width = 3.5F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 3.5F;
			this.Line3.Y1 = 2.1875F;
			this.Line3.Y2 = 2.1875F;
			// 
			// txtLine1L4
			// 
			this.txtLine1L4.Height = 0.19F;
			this.txtLine1L4.Left = 0F;
			this.txtLine1L4.MultiLine = false;
			this.txtLine1L4.Name = "txtLine1L4";
			this.txtLine1L4.Style = "font-size: 9pt";
			this.txtLine1L4.Text = "Field11";
			this.txtLine1L4.Top = 3.1875F;
			this.txtLine1L4.Width = 3.5F;
			// 
			// txtLine2L4
			// 
			this.txtLine2L4.Height = 0.19F;
			this.txtLine2L4.Left = 0F;
			this.txtLine2L4.MultiLine = false;
			this.txtLine2L4.Name = "txtLine2L4";
			this.txtLine2L4.Style = "font-size: 9pt";
			this.txtLine2L4.Text = "pg";
			this.txtLine2L4.Top = 3.34375F;
			this.txtLine2L4.Width = 3.5F;
			// 
			// txtLine3L4
			// 
			this.txtLine3L4.Height = 0.19F;
			this.txtLine3L4.Left = 0F;
			this.txtLine3L4.MultiLine = false;
			this.txtLine3L4.Name = "txtLine3L4";
			this.txtLine3L4.Style = "font-size: 9pt";
			this.txtLine3L4.Text = "Field5";
			this.txtLine3L4.Top = 3.5F;
			this.txtLine3L4.Width = 3.5F;
			// 
			// txtLine4L4
			// 
			this.txtLine4L4.Height = 0.19F;
			this.txtLine4L4.Left = 0F;
			this.txtLine4L4.MultiLine = false;
			this.txtLine4L4.Name = "txtLine4L4";
			this.txtLine4L4.Style = "font-size: 9pt";
			this.txtLine4L4.Text = "Field5";
			this.txtLine4L4.Top = 3.65625F;
			this.txtLine4L4.Width = 3.5F;
			// 
			// txtLine5L4
			// 
			this.txtLine5L4.Height = 0.19F;
			this.txtLine5L4.Left = 0F;
			this.txtLine5L4.MultiLine = false;
			this.txtLine5L4.Name = "txtLine5L4";
			this.txtLine5L4.Style = "font-size: 9pt";
			this.txtLine5L4.Text = "Field5";
			this.txtLine5L4.Top = 3.8125F;
			this.txtLine5L4.Width = 3.5F;
			// 
			// txtLine6L4
			// 
			this.txtLine6L4.Height = 0.19F;
			this.txtLine6L4.Left = 0F;
			this.txtLine6L4.MultiLine = false;
			this.txtLine6L4.Name = "txtLine6L4";
			this.txtLine6L4.Style = "font-size: 9pt";
			this.txtLine6L4.Text = "Field5";
			this.txtLine6L4.Top = 3.96875F;
			this.txtLine6L4.Width = 3.5F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 4.15625F;
			this.Line4.Width = 3.5F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 3.5F;
			this.Line4.Y1 = 4.15625F;
			this.Line4.Y2 = 4.15625F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.19F;
			this.Field11.Left = 0F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-size: 9pt";
			this.Field11.Text = "Inspection Witnessed By:";
			this.Field11.Top = 4.1875F;
			this.Field11.Width = 1.5625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 10pt";
			this.Label6.Text = "X";
			this.Label6.Top = 4.59375F;
			this.Label6.Width = 0.25F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0F;
			this.Line5.LineWeight = 3F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 4.78125F;
			this.Line5.Width = 3.5F;
			this.Line5.X1 = 0F;
			this.Line5.X2 = 3.5F;
			this.Line5.Y1 = 4.78125F;
			this.Line5.Y2 = 4.78125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 2.6875F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 9pt";
			this.Label7.Text = "Date";
			this.Label7.Top = 4.59375F;
			this.Label7.Width = 0.375F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.19F;
			this.Field12.Left = 0F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-size: 9pt";
			this.Field12.Text = "Notes:";
			this.Field12.Top = 5.71875F;
			this.Field12.Width = 0.5F;
			// 
			// txtNotes
			// 
			this.txtNotes.Height = 1.25F;
			this.txtNotes.Left = 0F;
			this.txtNotes.Name = "txtNotes";
			this.txtNotes.Style = "font-size: 9pt";
			this.txtNotes.Text = null;
			this.txtNotes.Top = 5.90625F;
			this.txtNotes.Width = 3.5F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 0.0625F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 0.2083333F;
			this.Line7.Width = 9.936111F;
			this.Line7.X1 = 0.0625F;
			this.Line7.X2 = 9.998611F;
			this.Line7.Y1 = 0.2083333F;
			this.Line7.Y2 = 0.2083333F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 4.4375F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-size: 10pt";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0F;
			this.txtLocation.Width = 2.875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 3.5625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label8.Text = "Property Data";
			this.Label8.Top = 0.21875F;
			this.Label8.Width = 1.875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 6.6875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label9.Text = "Assessment Record";
			this.Label9.Top = 0.21875F;
			this.Label9.Width = 1.875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.125F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.5625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-size: 6pt";
			this.Label10.Text = "Neighborhood";
			this.Label10.Top = 0.4375F;
			this.Label10.Width = 0.625F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 3.5F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 0.75F;
			this.Line10.Width = 2F;
			this.Line10.X1 = 3.5F;
			this.Line10.X2 = 5.5F;
			this.Line10.Y1 = 0.75F;
			this.Line10.Y2 = 0.75F;
			// 
			// txtNeighborhood
			// 
			this.txtNeighborhood.Height = 0.125F;
			this.txtNeighborhood.Left = 4.1875F;
			this.txtNeighborhood.Name = "txtNeighborhood";
			this.txtNeighborhood.Style = "font-size: 6pt; font-weight: bold";
			this.txtNeighborhood.Text = null;
			this.txtNeighborhood.Top = 0.4375F;
			this.txtNeighborhood.Width = 1.3125F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 3.5F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 0.90625F;
			this.Line12.Width = 2F;
			this.Line12.X1 = 3.5F;
			this.Line12.X2 = 5.5F;
			this.Line12.Y1 = 0.90625F;
			this.Line12.Y2 = 0.90625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.125F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.5625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 6pt";
			this.Label11.Text = "Tree Growth Year";
			this.Label11.Top = 0.78125F;
			this.Label11.Width = 0.8125F;
			// 
			// txtTreeGrowth
			// 
			this.txtTreeGrowth.Height = 0.125F;
			this.txtTreeGrowth.Left = 4.4375F;
			this.txtTreeGrowth.Name = "txtTreeGrowth";
			this.txtTreeGrowth.Style = "font-size: 6pt; font-weight: bold";
			this.txtTreeGrowth.Text = null;
			this.txtTreeGrowth.Top = 0.78125F;
			this.txtTreeGrowth.Width = 1.0625F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 3.5F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 1.0625F;
			this.Line13.Width = 2F;
			this.Line13.X1 = 3.5F;
			this.Line13.X2 = 5.5F;
			this.Line13.Y1 = 1.0625F;
			this.Line13.Y2 = 1.0625F;
			// 
			// lblXCoord
			// 
			this.lblXCoord.Height = 0.125F;
			this.lblXCoord.HyperLink = null;
			this.lblXCoord.Left = 3.5625F;
			this.lblXCoord.Name = "lblXCoord";
			this.lblXCoord.Style = "font-size: 6pt";
			this.lblXCoord.Text = null;
			this.lblXCoord.Top = 0.9375F;
			this.lblXCoord.Width = 1.3125F;
			// 
			// txtXCoord
			// 
			this.txtXCoord.Height = 0.125F;
			this.txtXCoord.Left = 4.9375F;
			this.txtXCoord.Name = "txtXCoord";
			this.txtXCoord.Style = "font-size: 6pt; font-weight: bold";
			this.txtXCoord.Text = null;
			this.txtXCoord.Top = 0.9375F;
			this.txtXCoord.Width = 0.5F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 3.5F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 1.21875F;
			this.Line14.Width = 2F;
			this.Line14.X1 = 3.5F;
			this.Line14.X2 = 5.5F;
			this.Line14.Y1 = 1.21875F;
			this.Line14.Y2 = 1.21875F;
			// 
			// lblYCoord
			// 
			this.lblYCoord.Height = 0.125F;
			this.lblYCoord.HyperLink = null;
			this.lblYCoord.Left = 3.5625F;
			this.lblYCoord.Name = "lblYCoord";
			this.lblYCoord.Style = "font-size: 6pt";
			this.lblYCoord.Text = null;
			this.lblYCoord.Top = 1.09375F;
			this.lblYCoord.Width = 1.3125F;
			// 
			// txtYCoord
			// 
			this.txtYCoord.Height = 0.125F;
			this.txtYCoord.Left = 4.9375F;
			this.txtYCoord.Name = "txtYCoord";
			this.txtYCoord.Style = "font-size: 6pt; font-weight: bold";
			this.txtYCoord.Text = null;
			this.txtYCoord.Top = 1.09375F;
			this.txtYCoord.Width = 0.5F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.125F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 3.5625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 6pt";
			this.Label12.Text = "Zone/Land Use";
			this.Label12.Top = 1.25F;
			this.Label12.Width = 0.75F;
			// 
			// txtZone
			// 
			this.txtZone.Height = 0.125F;
			this.txtZone.Left = 4.25F;
			this.txtZone.Name = "txtZone";
			this.txtZone.Style = "font-size: 6pt; font-weight: bold";
			this.txtZone.Text = null;
			this.txtZone.Top = 1.25F;
			this.txtZone.Width = 1.25F;
			// 
			// Line16
			// 
			this.Line16.Height = 0F;
			this.Line16.Left = 3.5F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 1.5625F;
			this.Line16.Width = 2F;
			this.Line16.X1 = 3.5F;
			this.Line16.X2 = 5.5F;
			this.Line16.Y1 = 1.5625F;
			this.Line16.Y2 = 1.5625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.125F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.5625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 6pt";
			this.Label13.Text = "Secondary Zone";
			this.Label13.Top = 1.59375F;
			this.Label13.Width = 0.75F;
			// 
			// txtSecZone
			// 
			this.txtSecZone.Height = 0.125F;
			this.txtSecZone.Left = 4.25F;
			this.txtSecZone.Name = "txtSecZone";
			this.txtSecZone.Style = "font-size: 6pt; font-weight: bold";
			this.txtSecZone.Text = null;
			this.txtSecZone.Top = 1.59375F;
			this.txtSecZone.Width = 1.25F;
			// 
			// Line18
			// 
			this.Line18.Height = 0F;
			this.Line18.Left = 3.5F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 1.90625F;
			this.Line18.Width = 2F;
			this.Line18.X1 = 3.5F;
			this.Line18.X2 = 5.5F;
			this.Line18.Y1 = 1.90625F;
			this.Line18.Y2 = 1.90625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.125F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.5625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 6pt";
			this.Label14.Text = "Topography";
			this.Label14.Top = 1.9375F;
			this.Label14.Width = 0.5625F;
			// 
			// txtTopography1
			// 
			this.txtTopography1.Height = 0.125F;
			this.txtTopography1.Left = 4.0625F;
			this.txtTopography1.Name = "txtTopography1";
			this.txtTopography1.Style = "font-size: 6pt; font-weight: bold";
			this.txtTopography1.Text = null;
			this.txtTopography1.Top = 1.9375F;
			this.txtTopography1.Width = 0.75F;
			// 
			// Line20
			// 
			this.Line20.Height = 0F;
			this.Line20.Left = 3.5F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 2.625F;
			this.Line20.Width = 2F;
			this.Line20.X1 = 3.5F;
			this.Line20.X2 = 5.5F;
			this.Line20.Y1 = 2.625F;
			this.Line20.Y2 = 2.625F;
			// 
			// txtTopCost1
			// 
			this.txtTopCost1.Height = 0.125F;
			this.txtTopCost1.Left = 3.5F;
			this.txtTopCost1.Name = "txtTopCost1";
			this.txtTopCost1.Style = "font-size: 6pt";
			this.txtTopCost1.Text = "1.";
			this.txtTopCost1.Top = 2.21875F;
			this.txtTopCost1.Width = 0.625F;
			// 
			// txtTopCost2
			// 
			this.txtTopCost2.Height = 0.125F;
			this.txtTopCost2.Left = 3.5F;
			this.txtTopCost2.Name = "txtTopCost2";
			this.txtTopCost2.Style = "font-size: 6pt";
			this.txtTopCost2.Text = "2.";
			this.txtTopCost2.Top = 2.34375F;
			this.txtTopCost2.Width = 0.625F;
			// 
			// txtTopCost3
			// 
			this.txtTopCost3.Height = 0.125F;
			this.txtTopCost3.Left = 3.5F;
			this.txtTopCost3.Name = "txtTopCost3";
			this.txtTopCost3.Style = "font-size: 6pt";
			this.txtTopCost3.Text = "3.";
			this.txtTopCost3.Top = 2.46875F;
			this.txtTopCost3.Width = 0.625F;
			// 
			// txtTopCost8
			// 
			this.txtTopCost8.Height = 0.125F;
			this.txtTopCost8.Left = 4.875F;
			this.txtTopCost8.Name = "txtTopCost8";
			this.txtTopCost8.Style = "font-size: 6pt";
			this.txtTopCost8.Text = "8.";
			this.txtTopCost8.Top = 2.34375F;
			this.txtTopCost8.Width = 0.625F;
			// 
			// txtTopCost9
			// 
			this.txtTopCost9.Height = 0.125F;
			this.txtTopCost9.Left = 4.875F;
			this.txtTopCost9.Name = "txtTopCost9";
			this.txtTopCost9.Style = "font-size: 6pt";
			this.txtTopCost9.Text = "9.";
			this.txtTopCost9.Top = 2.46875F;
			this.txtTopCost9.Width = 0.625F;
			// 
			// txtTopCost7
			// 
			this.txtTopCost7.Height = 0.125F;
			this.txtTopCost7.Left = 4.875F;
			this.txtTopCost7.Name = "txtTopCost7";
			this.txtTopCost7.Style = "font-size: 6pt";
			this.txtTopCost7.Text = "7.";
			this.txtTopCost7.Top = 2.21875F;
			this.txtTopCost7.Width = 0.625F;
			// 
			// txtTopCost4
			// 
			this.txtTopCost4.Height = 0.125F;
			this.txtTopCost4.Left = 4.1875F;
			this.txtTopCost4.Name = "txtTopCost4";
			this.txtTopCost4.Style = "font-size: 6pt";
			this.txtTopCost4.Text = "4.";
			this.txtTopCost4.Top = 2.21875F;
			this.txtTopCost4.Width = 0.625F;
			// 
			// txtTopCost5
			// 
			this.txtTopCost5.Height = 0.125F;
			this.txtTopCost5.Left = 4.1875F;
			this.txtTopCost5.Name = "txtTopCost5";
			this.txtTopCost5.Style = "font-size: 6pt";
			this.txtTopCost5.Text = "5.";
			this.txtTopCost5.Top = 2.34375F;
			this.txtTopCost5.Width = 0.625F;
			// 
			// txtTopCost6
			// 
			this.txtTopCost6.Height = 0.125F;
			this.txtTopCost6.Left = 4.1875F;
			this.txtTopCost6.Name = "txtTopCost6";
			this.txtTopCost6.Style = "font-size: 6pt";
			this.txtTopCost6.Text = "6.";
			this.txtTopCost6.Top = 2.46875F;
			this.txtTopCost6.Width = 0.625F;
			// 
			// txtTopography2
			// 
			this.txtTopography2.Height = 0.125F;
			this.txtTopography2.Left = 4.8125F;
			this.txtTopography2.Name = "txtTopography2";
			this.txtTopography2.Style = "font-size: 6pt; font-weight: bold";
			this.txtTopography2.Text = null;
			this.txtTopography2.Top = 1.9375F;
			this.txtTopography2.Width = 0.6875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.125F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.5625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 6pt";
			this.Label15.Text = "Utilities";
			this.Label15.Top = 2.65625F;
			this.Label15.Width = 0.375F;
			// 
			// txtUtility1
			// 
			this.txtUtility1.Height = 0.125F;
			this.txtUtility1.Left = 3.9375F;
			this.txtUtility1.Name = "txtUtility1";
			this.txtUtility1.Style = "font-size: 6pt; font-weight: bold";
			this.txtUtility1.Text = null;
			this.txtUtility1.Top = 2.65625F;
			this.txtUtility1.Width = 0.75F;
			// 
			// Line22
			// 
			this.Line22.Height = 0F;
			this.Line22.Left = 3.5F;
			this.Line22.LineWeight = 1F;
			this.Line22.Name = "Line22";
			this.Line22.Top = 3.34375F;
			this.Line22.Width = 2F;
			this.Line22.X1 = 3.5F;
			this.Line22.X2 = 5.5F;
			this.Line22.Y1 = 3.34375F;
			this.Line22.Y2 = 3.34375F;
			// 
			// txtUtilCost1
			// 
			this.txtUtilCost1.Height = 0.125F;
			this.txtUtilCost1.Left = 3.5F;
			this.txtUtilCost1.Name = "txtUtilCost1";
			this.txtUtilCost1.Style = "font-size: 6pt";
			this.txtUtilCost1.Text = "1.";
			this.txtUtilCost1.Top = 2.9375F;
			this.txtUtilCost1.Width = 0.625F;
			// 
			// txtUtilCost2
			// 
			this.txtUtilCost2.Height = 0.125F;
			this.txtUtilCost2.Left = 3.5F;
			this.txtUtilCost2.Name = "txtUtilCost2";
			this.txtUtilCost2.Style = "font-size: 6pt";
			this.txtUtilCost2.Text = "2.";
			this.txtUtilCost2.Top = 3.0625F;
			this.txtUtilCost2.Width = 0.625F;
			// 
			// txtUtilCost3
			// 
			this.txtUtilCost3.Height = 0.125F;
			this.txtUtilCost3.Left = 3.5F;
			this.txtUtilCost3.Name = "txtUtilCost3";
			this.txtUtilCost3.Style = "font-size: 6pt";
			this.txtUtilCost3.Text = "3.";
			this.txtUtilCost3.Top = 3.1875F;
			this.txtUtilCost3.Width = 0.625F;
			// 
			// txtUtilCost8
			// 
			this.txtUtilCost8.Height = 0.125F;
			this.txtUtilCost8.Left = 4.875F;
			this.txtUtilCost8.Name = "txtUtilCost8";
			this.txtUtilCost8.Style = "font-size: 6pt";
			this.txtUtilCost8.Text = "8.";
			this.txtUtilCost8.Top = 3.0625F;
			this.txtUtilCost8.Width = 0.625F;
			// 
			// txtUtilCost9
			// 
			this.txtUtilCost9.Height = 0.125F;
			this.txtUtilCost9.Left = 4.875F;
			this.txtUtilCost9.Name = "txtUtilCost9";
			this.txtUtilCost9.Style = "font-size: 6pt";
			this.txtUtilCost9.Text = "9.";
			this.txtUtilCost9.Top = 3.1875F;
			this.txtUtilCost9.Width = 0.625F;
			// 
			// txtUtilCost7
			// 
			this.txtUtilCost7.Height = 0.125F;
			this.txtUtilCost7.Left = 4.875F;
			this.txtUtilCost7.Name = "txtUtilCost7";
			this.txtUtilCost7.Style = "font-size: 6pt";
			this.txtUtilCost7.Text = "7.";
			this.txtUtilCost7.Top = 2.9375F;
			this.txtUtilCost7.Width = 0.625F;
			// 
			// txtUtilCost4
			// 
			this.txtUtilCost4.Height = 0.125F;
			this.txtUtilCost4.Left = 4.1875F;
			this.txtUtilCost4.Name = "txtUtilCost4";
			this.txtUtilCost4.Style = "font-size: 6pt";
			this.txtUtilCost4.Text = "4.";
			this.txtUtilCost4.Top = 2.9375F;
			this.txtUtilCost4.Width = 0.625F;
			// 
			// txtUtilCost5
			// 
			this.txtUtilCost5.Height = 0.125F;
			this.txtUtilCost5.Left = 4.1875F;
			this.txtUtilCost5.Name = "txtUtilCost5";
			this.txtUtilCost5.Style = "font-size: 6pt";
			this.txtUtilCost5.Text = "5.";
			this.txtUtilCost5.Top = 3.0625F;
			this.txtUtilCost5.Width = 0.625F;
			// 
			// txtUtilCost6
			// 
			this.txtUtilCost6.Height = 0.125F;
			this.txtUtilCost6.Left = 4.1875F;
			this.txtUtilCost6.Name = "txtUtilCost6";
			this.txtUtilCost6.Style = "font-size: 6pt";
			this.txtUtilCost6.Text = "6.";
			this.txtUtilCost6.Top = 3.1875F;
			this.txtUtilCost6.Width = 0.625F;
			// 
			// txtUtility2
			// 
			this.txtUtility2.Height = 0.125F;
			this.txtUtility2.Left = 4.6875F;
			this.txtUtility2.Name = "txtUtility2";
			this.txtUtility2.Style = "font-size: 6pt; font-weight: bold";
			this.txtUtility2.Text = null;
			this.txtUtility2.Top = 2.65625F;
			this.txtUtility2.Width = 0.8125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.125F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.5625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 6pt";
			this.Label16.Text = "Street";
			this.Label16.Top = 3.375F;
			this.Label16.Width = 0.375F;
			// 
			// txtStreet
			// 
			this.txtStreet.Height = 0.125F;
			this.txtStreet.Left = 4F;
			this.txtStreet.Name = "txtStreet";
			this.txtStreet.Style = "font-size: 6pt; font-weight: bold";
			this.txtStreet.Text = null;
			this.txtStreet.Top = 3.375F;
			this.txtStreet.Width = 1.4375F;
			// 
			// Line24
			// 
			this.Line24.Height = 0F;
			this.Line24.Left = 3.5F;
			this.Line24.LineWeight = 1F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 4.0625F;
			this.Line24.Width = 2F;
			this.Line24.X1 = 3.5F;
			this.Line24.X2 = 5.5F;
			this.Line24.Y1 = 4.0625F;
			this.Line24.Y2 = 4.0625F;
			// 
			// txtStreetCost1
			// 
			this.txtStreetCost1.Height = 0.125F;
			this.txtStreetCost1.Left = 3.5F;
			this.txtStreetCost1.Name = "txtStreetCost1";
			this.txtStreetCost1.Style = "font-size: 6pt";
			this.txtStreetCost1.Text = "1.";
			this.txtStreetCost1.Top = 3.65625F;
			this.txtStreetCost1.Width = 0.625F;
			// 
			// txtStreetCost2
			// 
			this.txtStreetCost2.Height = 0.125F;
			this.txtStreetCost2.Left = 3.5F;
			this.txtStreetCost2.Name = "txtStreetCost2";
			this.txtStreetCost2.Style = "font-size: 6pt";
			this.txtStreetCost2.Text = "2.";
			this.txtStreetCost2.Top = 3.78125F;
			this.txtStreetCost2.Width = 0.625F;
			// 
			// txtStreetCost3
			// 
			this.txtStreetCost3.Height = 0.125F;
			this.txtStreetCost3.Left = 3.5F;
			this.txtStreetCost3.Name = "txtStreetCost3";
			this.txtStreetCost3.Style = "font-size: 6pt";
			this.txtStreetCost3.Text = "3.";
			this.txtStreetCost3.Top = 3.90625F;
			this.txtStreetCost3.Width = 0.625F;
			// 
			// txtStreetCost8
			// 
			this.txtStreetCost8.Height = 0.125F;
			this.txtStreetCost8.Left = 4.875F;
			this.txtStreetCost8.Name = "txtStreetCost8";
			this.txtStreetCost8.Style = "font-size: 6pt";
			this.txtStreetCost8.Text = "8.";
			this.txtStreetCost8.Top = 3.78125F;
			this.txtStreetCost8.Width = 0.625F;
			// 
			// txtStreetCost9
			// 
			this.txtStreetCost9.Height = 0.125F;
			this.txtStreetCost9.Left = 4.875F;
			this.txtStreetCost9.Name = "txtStreetCost9";
			this.txtStreetCost9.Style = "font-size: 6pt";
			this.txtStreetCost9.Text = "9.";
			this.txtStreetCost9.Top = 3.90625F;
			this.txtStreetCost9.Width = 0.625F;
			// 
			// txtStreetCost7
			// 
			this.txtStreetCost7.Height = 0.125F;
			this.txtStreetCost7.Left = 4.875F;
			this.txtStreetCost7.Name = "txtStreetCost7";
			this.txtStreetCost7.Style = "font-size: 6pt";
			this.txtStreetCost7.Text = "7.";
			this.txtStreetCost7.Top = 3.65625F;
			this.txtStreetCost7.Width = 0.625F;
			// 
			// txtStreetCost4
			// 
			this.txtStreetCost4.Height = 0.125F;
			this.txtStreetCost4.Left = 4.1875F;
			this.txtStreetCost4.Name = "txtStreetCost4";
			this.txtStreetCost4.Style = "font-size: 6pt";
			this.txtStreetCost4.Text = "4.";
			this.txtStreetCost4.Top = 3.65625F;
			this.txtStreetCost4.Width = 0.625F;
			// 
			// txtStreetCost5
			// 
			this.txtStreetCost5.Height = 0.125F;
			this.txtStreetCost5.Left = 4.1875F;
			this.txtStreetCost5.Name = "txtStreetCost5";
			this.txtStreetCost5.Style = "font-size: 6pt";
			this.txtStreetCost5.Text = "5.";
			this.txtStreetCost5.Top = 3.78125F;
			this.txtStreetCost5.Width = 0.625F;
			// 
			// txtStreetCost6
			// 
			this.txtStreetCost6.Height = 0.125F;
			this.txtStreetCost6.Left = 4.1875F;
			this.txtStreetCost6.Name = "txtStreetCost6";
			this.txtStreetCost6.Style = "font-size: 6pt";
			this.txtStreetCost6.Text = "6.";
			this.txtStreetCost6.Top = 3.90625F;
			this.txtStreetCost6.Width = 0.625F;
			// 
			// Line25
			// 
			this.Line25.Height = 0F;
			this.Line25.Left = 3.5F;
			this.Line25.LineWeight = 1F;
			this.Line25.Name = "Line25";
			this.Line25.Top = 4.21875F;
			this.Line25.Width = 2F;
			this.Line25.X1 = 3.5F;
			this.Line25.X2 = 5.5F;
			this.Line25.Y1 = 4.21875F;
			this.Line25.Y2 = 4.21875F;
			// 
			// lblOpen1
			// 
			this.lblOpen1.Height = 0.125F;
			this.lblOpen1.HyperLink = null;
			this.lblOpen1.Left = 3.5625F;
			this.lblOpen1.Name = "lblOpen1";
			this.lblOpen1.Style = "font-size: 6pt";
			this.lblOpen1.Text = "User Defined 1";
			this.lblOpen1.Top = 4.09375F;
			this.lblOpen1.Width = 1.3125F;
			// 
			// txtOpen1
			// 
			this.txtOpen1.Height = 0.125F;
			this.txtOpen1.Left = 4.9375F;
			this.txtOpen1.Name = "txtOpen1";
			this.txtOpen1.Style = "font-size: 6pt; font-weight: bold";
			this.txtOpen1.Text = null;
			this.txtOpen1.Top = 4.09375F;
			this.txtOpen1.Width = 0.5F;
			// 
			// Line26
			// 
			this.Line26.Height = 0F;
			this.Line26.Left = 3.5F;
			this.Line26.LineWeight = 1F;
			this.Line26.Name = "Line26";
			this.Line26.Top = 4.375F;
			this.Line26.Width = 2F;
			this.Line26.X1 = 3.5F;
			this.Line26.X2 = 5.5F;
			this.Line26.Y1 = 4.375F;
			this.Line26.Y2 = 4.375F;
			// 
			// lblOpen2
			// 
			this.lblOpen2.Height = 0.125F;
			this.lblOpen2.HyperLink = null;
			this.lblOpen2.Left = 3.5625F;
			this.lblOpen2.Name = "lblOpen2";
			this.lblOpen2.Style = "font-size: 6pt";
			this.lblOpen2.Text = "User Defined 2";
			this.lblOpen2.Top = 4.25F;
			this.lblOpen2.Width = 1.3125F;
			// 
			// txtOpen2
			// 
			this.txtOpen2.Height = 0.125F;
			this.txtOpen2.Left = 4.9375F;
			this.txtOpen2.Name = "txtOpen2";
			this.txtOpen2.Style = "font-size: 6pt; font-weight: bold";
			this.txtOpen2.Text = null;
			this.txtOpen2.Top = 4.25F;
			this.txtOpen2.Width = 0.5F;
			// 
			// Line27
			// 
			this.Line27.Height = 0F;
			this.Line27.Left = 3.5F;
			this.Line27.LineWeight = 1F;
			this.Line27.Name = "Line27";
			this.Line27.Top = 4.5625F;
			this.Line27.Width = 2F;
			this.Line27.X1 = 3.5F;
			this.Line27.X2 = 5.5F;
			this.Line27.Y1 = 4.5625F;
			this.Line27.Y2 = 4.5625F;
			// 
			// Line28
			// 
			this.Line28.Height = 0F;
			this.Line28.Left = 3.5F;
			this.Line28.LineWeight = 1F;
			this.Line28.Name = "Line28";
			this.Line28.Top = 4.71875F;
			this.Line28.Width = 2F;
			this.Line28.X1 = 3.5F;
			this.Line28.X2 = 5.5F;
			this.Line28.Y1 = 4.71875F;
			this.Line28.Y2 = 4.71875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.125F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.5625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 6pt";
			this.Label18.Text = "Sale Date";
			this.Label18.Top = 4.59375F;
			this.Label18.Width = 0.75F;
			// 
			// txtSaleDate
			// 
			this.txtSaleDate.Height = 0.125F;
			this.txtSaleDate.Left = 4.5F;
			this.txtSaleDate.Name = "txtSaleDate";
			this.txtSaleDate.Style = "font-size: 6pt; font-weight: bold; text-align: right";
			this.txtSaleDate.Text = null;
			this.txtSaleDate.Top = 4.59375F;
			this.txtSaleDate.Width = 0.9166667F;
			// 
			// Line29
			// 
			this.Line29.Height = 0F;
			this.Line29.Left = 3.5F;
			this.Line29.LineWeight = 1F;
			this.Line29.Name = "Line29";
			this.Line29.Top = 4.875F;
			this.Line29.Width = 2F;
			this.Line29.X1 = 3.5F;
			this.Line29.X2 = 5.5F;
			this.Line29.Y1 = 4.875F;
			this.Line29.Y2 = 4.875F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.125F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3.5625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 6pt";
			this.Label19.Text = "Price";
			this.Label19.Top = 4.75F;
			this.Label19.Width = 0.625F;
			// 
			// txtSalePrice
			// 
			this.txtSalePrice.Height = 0.125F;
			this.txtSalePrice.Left = 4.5F;
			this.txtSalePrice.Name = "txtSalePrice";
			this.txtSalePrice.Style = "font-size: 6pt; font-weight: bold; text-align: right";
			this.txtSalePrice.Text = null;
			this.txtSalePrice.Top = 4.75F;
			this.txtSalePrice.Width = 0.9166667F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.125F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 3.5625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 6pt";
			this.Label20.Text = "Sale Type";
			this.Label20.Top = 4.90625F;
			this.Label20.Width = 0.5625F;
			// 
			// txtSaleType
			// 
			this.txtSaleType.Height = 0.125F;
			this.txtSaleType.Left = 4.1875F;
			this.txtSaleType.MultiLine = false;
			this.txtSaleType.Name = "txtSaleType";
			this.txtSaleType.Style = "font-size: 6pt; font-weight: bold";
			this.txtSaleType.Text = null;
			this.txtSaleType.Top = 4.90625F;
			this.txtSaleType.Width = 0.9375F;
			// 
			// Line31
			// 
			this.Line31.Height = 0F;
			this.Line31.Left = 3.5F;
			this.Line31.LineWeight = 1F;
			this.Line31.Name = "Line31";
			this.Line31.Top = 5.46875F;
			this.Line31.Width = 2F;
			this.Line31.X1 = 3.5F;
			this.Line31.X2 = 5.5F;
			this.Line31.Y1 = 5.46875F;
			this.Line31.Y2 = 5.46875F;
			// 
			// txtSaleTypeCost1
			// 
			this.txtSaleTypeCost1.Height = 0.125F;
			this.txtSaleTypeCost1.Left = 3.5F;
			this.txtSaleTypeCost1.Name = "txtSaleTypeCost1";
			this.txtSaleTypeCost1.Style = "font-size: 6pt";
			this.txtSaleTypeCost1.Text = "1.";
			this.txtSaleTypeCost1.Top = 5.0625F;
			this.txtSaleTypeCost1.Width = 0.625F;
			// 
			// txtSaleTypeCost2
			// 
			this.txtSaleTypeCost2.Height = 0.125F;
			this.txtSaleTypeCost2.Left = 3.5F;
			this.txtSaleTypeCost2.Name = "txtSaleTypeCost2";
			this.txtSaleTypeCost2.Style = "font-size: 6pt";
			this.txtSaleTypeCost2.Text = "2.";
			this.txtSaleTypeCost2.Top = 5.1875F;
			this.txtSaleTypeCost2.Width = 0.625F;
			// 
			// txtSaleTypeCost3
			// 
			this.txtSaleTypeCost3.Height = 0.125F;
			this.txtSaleTypeCost3.Left = 3.5F;
			this.txtSaleTypeCost3.Name = "txtSaleTypeCost3";
			this.txtSaleTypeCost3.Style = "font-size: 6pt";
			this.txtSaleTypeCost3.Text = "3.";
			this.txtSaleTypeCost3.Top = 5.3125F;
			this.txtSaleTypeCost3.Width = 0.625F;
			// 
			// txtSaleTypeCost8
			// 
			this.txtSaleTypeCost8.Height = 0.125F;
			this.txtSaleTypeCost8.Left = 4.875F;
			this.txtSaleTypeCost8.Name = "txtSaleTypeCost8";
			this.txtSaleTypeCost8.Style = "font-size: 6pt";
			this.txtSaleTypeCost8.Text = "8.";
			this.txtSaleTypeCost8.Top = 5.1875F;
			this.txtSaleTypeCost8.Width = 0.625F;
			// 
			// txtSaleTypeCost9
			// 
			this.txtSaleTypeCost9.Height = 0.125F;
			this.txtSaleTypeCost9.Left = 4.875F;
			this.txtSaleTypeCost9.Name = "txtSaleTypeCost9";
			this.txtSaleTypeCost9.Style = "font-size: 6pt";
			this.txtSaleTypeCost9.Text = "9.";
			this.txtSaleTypeCost9.Top = 5.3125F;
			this.txtSaleTypeCost9.Width = 0.625F;
			// 
			// txtSaleTypeCost7
			// 
			this.txtSaleTypeCost7.Height = 0.125F;
			this.txtSaleTypeCost7.Left = 4.875F;
			this.txtSaleTypeCost7.Name = "txtSaleTypeCost7";
			this.txtSaleTypeCost7.Style = "font-size: 6pt";
			this.txtSaleTypeCost7.Text = "7.";
			this.txtSaleTypeCost7.Top = 5.0625F;
			this.txtSaleTypeCost7.Width = 0.625F;
			// 
			// txtSaleTypeCost4
			// 
			this.txtSaleTypeCost4.Height = 0.125F;
			this.txtSaleTypeCost4.Left = 4.1875F;
			this.txtSaleTypeCost4.Name = "txtSaleTypeCost4";
			this.txtSaleTypeCost4.Style = "font-size: 6pt";
			this.txtSaleTypeCost4.Text = "4.";
			this.txtSaleTypeCost4.Top = 5.0625F;
			this.txtSaleTypeCost4.Width = 0.625F;
			// 
			// txtSaleTypeCost5
			// 
			this.txtSaleTypeCost5.Height = 0.125F;
			this.txtSaleTypeCost5.Left = 4.1875F;
			this.txtSaleTypeCost5.Name = "txtSaleTypeCost5";
			this.txtSaleTypeCost5.Style = "font-size: 6pt";
			this.txtSaleTypeCost5.Text = "5.";
			this.txtSaleTypeCost5.Top = 5.1875F;
			this.txtSaleTypeCost5.Width = 0.625F;
			// 
			// txtSaleTypeCost6
			// 
			this.txtSaleTypeCost6.Height = 0.125F;
			this.txtSaleTypeCost6.Left = 4.1875F;
			this.txtSaleTypeCost6.Name = "txtSaleTypeCost6";
			this.txtSaleTypeCost6.Style = "font-size: 6pt";
			this.txtSaleTypeCost6.Text = "6.";
			this.txtSaleTypeCost6.Top = 5.3125F;
			this.txtSaleTypeCost6.Width = 0.625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.125F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 3.5625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 6pt";
			this.Label21.Text = "Financing";
			this.Label21.Top = 5.5F;
			this.Label21.Width = 0.5625F;
			// 
			// txtSaleFinancing
			// 
			this.txtSaleFinancing.Height = 0.125F;
			this.txtSaleFinancing.Left = 4.1875F;
			this.txtSaleFinancing.MultiLine = false;
			this.txtSaleFinancing.Name = "txtSaleFinancing";
			this.txtSaleFinancing.Style = "font-size: 6pt; font-weight: bold";
			this.txtSaleFinancing.Text = null;
			this.txtSaleFinancing.Top = 5.5F;
			this.txtSaleFinancing.Width = 0.9375F;
			// 
			// Line33
			// 
			this.Line33.Height = 0F;
			this.Line33.Left = 3.5F;
			this.Line33.LineWeight = 1F;
			this.Line33.Name = "Line33";
			this.Line33.Top = 6.0625F;
			this.Line33.Width = 2F;
			this.Line33.X1 = 3.5F;
			this.Line33.X2 = 5.5F;
			this.Line33.Y1 = 6.0625F;
			this.Line33.Y2 = 6.0625F;
			// 
			// txtFinancingCost1
			// 
			this.txtFinancingCost1.Height = 0.125F;
			this.txtFinancingCost1.Left = 3.5F;
			this.txtFinancingCost1.Name = "txtFinancingCost1";
			this.txtFinancingCost1.Style = "font-size: 6pt";
			this.txtFinancingCost1.Text = "1.";
			this.txtFinancingCost1.Top = 5.65625F;
			this.txtFinancingCost1.Width = 0.625F;
			// 
			// txtFinancingCost2
			// 
			this.txtFinancingCost2.Height = 0.125F;
			this.txtFinancingCost2.Left = 3.5F;
			this.txtFinancingCost2.Name = "txtFinancingCost2";
			this.txtFinancingCost2.Style = "font-size: 6pt";
			this.txtFinancingCost2.Text = "2.";
			this.txtFinancingCost2.Top = 5.78125F;
			this.txtFinancingCost2.Width = 0.625F;
			// 
			// txtFinancingCost3
			// 
			this.txtFinancingCost3.Height = 0.125F;
			this.txtFinancingCost3.Left = 3.5F;
			this.txtFinancingCost3.Name = "txtFinancingCost3";
			this.txtFinancingCost3.Style = "font-size: 6pt";
			this.txtFinancingCost3.Text = "3.";
			this.txtFinancingCost3.Top = 5.90625F;
			this.txtFinancingCost3.Width = 0.625F;
			// 
			// txtFinancingCost8
			// 
			this.txtFinancingCost8.Height = 0.125F;
			this.txtFinancingCost8.Left = 4.875F;
			this.txtFinancingCost8.Name = "txtFinancingCost8";
			this.txtFinancingCost8.Style = "font-size: 6pt";
			this.txtFinancingCost8.Text = "8.";
			this.txtFinancingCost8.Top = 5.78125F;
			this.txtFinancingCost8.Width = 0.625F;
			// 
			// txtFinancingCost9
			// 
			this.txtFinancingCost9.Height = 0.125F;
			this.txtFinancingCost9.Left = 4.875F;
			this.txtFinancingCost9.Name = "txtFinancingCost9";
			this.txtFinancingCost9.Style = "font-size: 6pt";
			this.txtFinancingCost9.Text = "9.";
			this.txtFinancingCost9.Top = 5.90625F;
			this.txtFinancingCost9.Width = 0.625F;
			// 
			// txtFinancingCost7
			// 
			this.txtFinancingCost7.Height = 0.125F;
			this.txtFinancingCost7.Left = 4.875F;
			this.txtFinancingCost7.Name = "txtFinancingCost7";
			this.txtFinancingCost7.Style = "font-size: 6pt";
			this.txtFinancingCost7.Text = "7.";
			this.txtFinancingCost7.Top = 5.65625F;
			this.txtFinancingCost7.Width = 0.625F;
			// 
			// txtFinancingCost4
			// 
			this.txtFinancingCost4.Height = 0.125F;
			this.txtFinancingCost4.Left = 4.1875F;
			this.txtFinancingCost4.Name = "txtFinancingCost4";
			this.txtFinancingCost4.Style = "font-size: 6pt";
			this.txtFinancingCost4.Text = "4.";
			this.txtFinancingCost4.Top = 5.65625F;
			this.txtFinancingCost4.Width = 0.625F;
			// 
			// txtFinancingCost5
			// 
			this.txtFinancingCost5.Height = 0.125F;
			this.txtFinancingCost5.Left = 4.1875F;
			this.txtFinancingCost5.Name = "txtFinancingCost5";
			this.txtFinancingCost5.Style = "font-size: 6pt";
			this.txtFinancingCost5.Text = "5.";
			this.txtFinancingCost5.Top = 5.78125F;
			this.txtFinancingCost5.Width = 0.625F;
			// 
			// txtFinancingCost6
			// 
			this.txtFinancingCost6.Height = 0.125F;
			this.txtFinancingCost6.Left = 4.1875F;
			this.txtFinancingCost6.Name = "txtFinancingCost6";
			this.txtFinancingCost6.Style = "font-size: 6pt";
			this.txtFinancingCost6.Text = "6.";
			this.txtFinancingCost6.Top = 5.90625F;
			this.txtFinancingCost6.Width = 0.625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.125F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3.5625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 6pt";
			this.Label22.Text = "Validity";
			this.Label22.Top = 6.09375F;
			this.Label22.Width = 0.5625F;
			// 
			// txtValidity
			// 
			this.txtValidity.Height = 0.125F;
			this.txtValidity.Left = 4.1875F;
			this.txtValidity.MultiLine = false;
			this.txtValidity.Name = "txtValidity";
			this.txtValidity.Style = "font-size: 6pt; font-weight: bold";
			this.txtValidity.Text = null;
			this.txtValidity.Top = 6.09375F;
			this.txtValidity.Width = 0.9375F;
			// 
			// Line35
			// 
			this.Line35.Height = 0F;
			this.Line35.Left = 3.5F;
			this.Line35.LineWeight = 1F;
			this.Line35.Name = "Line35";
			this.Line35.Top = 6.65625F;
			this.Line35.Width = 2F;
			this.Line35.X1 = 3.5F;
			this.Line35.X2 = 5.5F;
			this.Line35.Y1 = 6.65625F;
			this.Line35.Y2 = 6.65625F;
			// 
			// txtValidityCost1
			// 
			this.txtValidityCost1.Height = 0.125F;
			this.txtValidityCost1.Left = 3.5F;
			this.txtValidityCost1.Name = "txtValidityCost1";
			this.txtValidityCost1.Style = "font-size: 6pt";
			this.txtValidityCost1.Text = "1.";
			this.txtValidityCost1.Top = 6.25F;
			this.txtValidityCost1.Width = 0.625F;
			// 
			// txtValidityCost2
			// 
			this.txtValidityCost2.Height = 0.125F;
			this.txtValidityCost2.Left = 3.5F;
			this.txtValidityCost2.Name = "txtValidityCost2";
			this.txtValidityCost2.Style = "font-size: 6pt";
			this.txtValidityCost2.Text = "2.";
			this.txtValidityCost2.Top = 6.375F;
			this.txtValidityCost2.Width = 0.625F;
			// 
			// txtValidityCost3
			// 
			this.txtValidityCost3.Height = 0.125F;
			this.txtValidityCost3.Left = 3.5F;
			this.txtValidityCost3.Name = "txtValidityCost3";
			this.txtValidityCost3.Style = "font-size: 6pt";
			this.txtValidityCost3.Text = "3.";
			this.txtValidityCost3.Top = 6.5F;
			this.txtValidityCost3.Width = 0.625F;
			// 
			// txtValidityCost8
			// 
			this.txtValidityCost8.Height = 0.125F;
			this.txtValidityCost8.Left = 4.875F;
			this.txtValidityCost8.Name = "txtValidityCost8";
			this.txtValidityCost8.Style = "font-size: 6pt";
			this.txtValidityCost8.Text = "8.";
			this.txtValidityCost8.Top = 6.375F;
			this.txtValidityCost8.Width = 0.625F;
			// 
			// txtValidityCost9
			// 
			this.txtValidityCost9.Height = 0.125F;
			this.txtValidityCost9.Left = 4.875F;
			this.txtValidityCost9.Name = "txtValidityCost9";
			this.txtValidityCost9.Style = "font-size: 6pt";
			this.txtValidityCost9.Text = "9.";
			this.txtValidityCost9.Top = 6.5F;
			this.txtValidityCost9.Width = 0.625F;
			// 
			// txtValidityCost7
			// 
			this.txtValidityCost7.Height = 0.125F;
			this.txtValidityCost7.Left = 4.875F;
			this.txtValidityCost7.Name = "txtValidityCost7";
			this.txtValidityCost7.Style = "font-size: 6pt";
			this.txtValidityCost7.Text = "7.";
			this.txtValidityCost7.Top = 6.25F;
			this.txtValidityCost7.Width = 0.625F;
			// 
			// txtValidityCost4
			// 
			this.txtValidityCost4.Height = 0.125F;
			this.txtValidityCost4.Left = 4.1875F;
			this.txtValidityCost4.Name = "txtValidityCost4";
			this.txtValidityCost4.Style = "font-size: 6pt";
			this.txtValidityCost4.Text = "4.";
			this.txtValidityCost4.Top = 6.25F;
			this.txtValidityCost4.Width = 0.625F;
			// 
			// txtValidityCost5
			// 
			this.txtValidityCost5.Height = 0.125F;
			this.txtValidityCost5.Left = 4.1875F;
			this.txtValidityCost5.Name = "txtValidityCost5";
			this.txtValidityCost5.Style = "font-size: 6pt";
			this.txtValidityCost5.Text = "5.";
			this.txtValidityCost5.Top = 6.375F;
			this.txtValidityCost5.Width = 0.625F;
			// 
			// txtValidityCost6
			// 
			this.txtValidityCost6.Height = 0.125F;
			this.txtValidityCost6.Left = 4.1875F;
			this.txtValidityCost6.Name = "txtValidityCost6";
			this.txtValidityCost6.Style = "font-size: 6pt";
			this.txtValidityCost6.Text = "6.";
			this.txtValidityCost6.Top = 6.5F;
			this.txtValidityCost6.Width = 0.625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.125F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 3.5625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-size: 6pt";
			this.Label23.Text = "Verified";
			this.Label23.Top = 6.6875F;
			this.Label23.Width = 0.5625F;
			// 
			// txtVerified
			// 
			this.txtVerified.Height = 0.125F;
			this.txtVerified.Left = 4.1875F;
			this.txtVerified.MultiLine = false;
			this.txtVerified.Name = "txtVerified";
			this.txtVerified.Style = "font-size: 6pt; font-weight: bold";
			this.txtVerified.Text = null;
			this.txtVerified.Top = 6.6875F;
			this.txtVerified.Width = 0.9375F;
			// 
			// Line37
			// 
			this.Line37.Height = 0F;
			this.Line37.Left = 3.5F;
			this.Line37.LineWeight = 1F;
			this.Line37.Name = "Line37";
			this.Line37.Top = 7.25F;
			this.Line37.Width = 2F;
			this.Line37.X1 = 3.5F;
			this.Line37.X2 = 5.5F;
			this.Line37.Y1 = 7.25F;
			this.Line37.Y2 = 7.25F;
			// 
			// txtVerifiedCost1
			// 
			this.txtVerifiedCost1.Height = 0.125F;
			this.txtVerifiedCost1.Left = 3.5F;
			this.txtVerifiedCost1.Name = "txtVerifiedCost1";
			this.txtVerifiedCost1.Style = "font-size: 6pt";
			this.txtVerifiedCost1.Text = "1.";
			this.txtVerifiedCost1.Top = 6.84375F;
			this.txtVerifiedCost1.Width = 0.625F;
			// 
			// txtVerifiedCost2
			// 
			this.txtVerifiedCost2.Height = 0.125F;
			this.txtVerifiedCost2.Left = 3.5F;
			this.txtVerifiedCost2.Name = "txtVerifiedCost2";
			this.txtVerifiedCost2.Style = "font-size: 6pt";
			this.txtVerifiedCost2.Text = "2.";
			this.txtVerifiedCost2.Top = 6.96875F;
			this.txtVerifiedCost2.Width = 0.625F;
			// 
			// txtVerifiedCost3
			// 
			this.txtVerifiedCost3.Height = 0.125F;
			this.txtVerifiedCost3.Left = 3.5F;
			this.txtVerifiedCost3.Name = "txtVerifiedCost3";
			this.txtVerifiedCost3.Style = "font-size: 6pt";
			this.txtVerifiedCost3.Text = "3.";
			this.txtVerifiedCost3.Top = 7.09375F;
			this.txtVerifiedCost3.Width = 0.625F;
			// 
			// txtVerifiedCost8
			// 
			this.txtVerifiedCost8.Height = 0.125F;
			this.txtVerifiedCost8.Left = 4.875F;
			this.txtVerifiedCost8.Name = "txtVerifiedCost8";
			this.txtVerifiedCost8.Style = "font-size: 6pt";
			this.txtVerifiedCost8.Text = "8.";
			this.txtVerifiedCost8.Top = 6.96875F;
			this.txtVerifiedCost8.Width = 0.625F;
			// 
			// txtVerifiedCost9
			// 
			this.txtVerifiedCost9.Height = 0.125F;
			this.txtVerifiedCost9.Left = 4.875F;
			this.txtVerifiedCost9.Name = "txtVerifiedCost9";
			this.txtVerifiedCost9.Style = "font-size: 6pt";
			this.txtVerifiedCost9.Text = "9.";
			this.txtVerifiedCost9.Top = 7.09375F;
			this.txtVerifiedCost9.Width = 0.625F;
			// 
			// txtVerifiedCost7
			// 
			this.txtVerifiedCost7.Height = 0.125F;
			this.txtVerifiedCost7.Left = 4.875F;
			this.txtVerifiedCost7.Name = "txtVerifiedCost7";
			this.txtVerifiedCost7.Style = "font-size: 6pt";
			this.txtVerifiedCost7.Text = "7.";
			this.txtVerifiedCost7.Top = 6.84375F;
			this.txtVerifiedCost7.Width = 0.625F;
			// 
			// txtVerifiedCost4
			// 
			this.txtVerifiedCost4.Height = 0.125F;
			this.txtVerifiedCost4.Left = 4.1875F;
			this.txtVerifiedCost4.Name = "txtVerifiedCost4";
			this.txtVerifiedCost4.Style = "font-size: 6pt";
			this.txtVerifiedCost4.Text = "4.";
			this.txtVerifiedCost4.Top = 6.84375F;
			this.txtVerifiedCost4.Width = 0.625F;
			// 
			// txtVerifiedCost5
			// 
			this.txtVerifiedCost5.Height = 0.125F;
			this.txtVerifiedCost5.Left = 4.1875F;
			this.txtVerifiedCost5.Name = "txtVerifiedCost5";
			this.txtVerifiedCost5.Style = "font-size: 6pt";
			this.txtVerifiedCost5.Text = "5.";
			this.txtVerifiedCost5.Top = 6.96875F;
			this.txtVerifiedCost5.Width = 0.625F;
			// 
			// txtVerifiedCost6
			// 
			this.txtVerifiedCost6.Height = 0.125F;
			this.txtVerifiedCost6.Left = 4.1875F;
			this.txtVerifiedCost6.Name = "txtVerifiedCost6";
			this.txtVerifiedCost6.Style = "font-size: 6pt";
			this.txtVerifiedCost6.Text = "6.";
			this.txtVerifiedCost6.Top = 7.09375F;
			this.txtVerifiedCost6.Width = 0.625F;
			// 
			// Line6
			// 
			this.Line6.Height = 7.166667F;
			this.Line6.Left = 3.5F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 0.2083333F;
			this.Line6.Width = 0F;
			this.Line6.X1 = 3.5F;
			this.Line6.X2 = 3.5F;
			this.Line6.Y1 = 0.2083333F;
			this.Line6.Y2 = 7.375F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 5.5625F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 10pt; text-align: center";
			this.Label24.Text = "Year";
			this.Label24.Top = 0.40625F;
			this.Label24.Width = 0.4375F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 6.5F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-size: 10pt; text-align: center";
			this.Label25.Text = "Land";
			this.Label25.Top = 0.40625F;
			this.Label25.Width = 0.375F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 7.5625F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-size: 10pt; text-align: center";
			this.Label26.Text = "Buildings";
			this.Label26.Top = 0.40625F;
			this.Label26.Width = 0.6875F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 8.5F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-size: 10pt; text-align: center";
			this.Label27.Text = "Exempt";
			this.Label27.Top = 0.40625F;
			this.Label27.Width = 0.5625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 9.25F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-size: 10pt; text-align: center";
			this.Label28.Text = "Total";
			this.Label28.Top = 0.40625F;
			this.Label28.Width = 0.625F;
			// 
			// Line38
			// 
			this.Line38.Height = 3.25F;
			this.Line38.Left = 6.0625F;
			this.Line38.LineWeight = 1F;
			this.Line38.Name = "Line38";
			this.Line38.Top = 0.40625F;
			this.Line38.Width = 0F;
			this.Line38.X1 = 6.0625F;
			this.Line38.X2 = 6.0625F;
			this.Line38.Y1 = 0.40625F;
			this.Line38.Y2 = 3.65625F;
			// 
			// Line39
			// 
			this.Line39.Height = 3.25F;
			this.Line39.Left = 7.3125F;
			this.Line39.LineWeight = 1F;
			this.Line39.Name = "Line39";
			this.Line39.Top = 0.40625F;
			this.Line39.Width = 0F;
			this.Line39.X1 = 7.3125F;
			this.Line39.X2 = 7.3125F;
			this.Line39.Y1 = 0.40625F;
			this.Line39.Y2 = 3.65625F;
			// 
			// Line40
			// 
			this.Line40.Height = 3.25F;
			this.Line40.Left = 8.4375F;
			this.Line40.LineWeight = 1F;
			this.Line40.Name = "Line40";
			this.Line40.Top = 0.40625F;
			this.Line40.Width = 0F;
			this.Line40.X1 = 8.4375F;
			this.Line40.X2 = 8.4375F;
			this.Line40.Y1 = 0.40625F;
			this.Line40.Y2 = 3.65625F;
			// 
			// Line41
			// 
			this.Line41.Height = 3.25F;
			this.Line41.Left = 9.125F;
			this.Line41.LineWeight = 1F;
			this.Line41.Name = "Line41";
			this.Line41.Top = 0.40625F;
			this.Line41.Width = 0F;
			this.Line41.X1 = 9.125F;
			this.Line41.X2 = 9.125F;
			this.Line41.Y1 = 0.40625F;
			this.Line41.Y2 = 3.65625F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 3.5F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 0.40625F;
			this.Line9.Width = 6.498611F;
			this.Line9.X1 = 3.5F;
			this.Line9.X2 = 9.998611F;
			this.Line9.Y1 = 0.40625F;
			this.Line9.Y2 = 0.40625F;
			// 
			// Line42
			// 
			this.Line42.Height = 0F;
			this.Line42.Left = 5.5F;
			this.Line42.LineWeight = 1F;
			this.Line42.Name = "Line42";
			this.Line42.Top = 0.59375F;
			this.Line42.Width = 4.498611F;
			this.Line42.X1 = 5.5F;
			this.Line42.X2 = 9.998611F;
			this.Line42.Y1 = 0.59375F;
			this.Line42.Y2 = 0.59375F;
			// 
			// Line43
			// 
			this.Line43.Height = 0F;
			this.Line43.Left = 5.5F;
			this.Line43.LineWeight = 1F;
			this.Line43.Name = "Line43";
			this.Line43.Top = 1.03125F;
			this.Line43.Width = 4.498611F;
			this.Line43.X1 = 5.5F;
			this.Line43.X2 = 9.998611F;
			this.Line43.Y1 = 1.03125F;
			this.Line43.Y2 = 1.03125F;
			// 
			// Line44
			// 
			this.Line44.Height = 0F;
			this.Line44.Left = 5.5F;
			this.Line44.LineWeight = 1F;
			this.Line44.Name = "Line44";
			this.Line44.Top = 0.8125F;
			this.Line44.Width = 4.498611F;
			this.Line44.X1 = 5.5F;
			this.Line44.X2 = 9.998611F;
			this.Line44.Y1 = 0.8125F;
			this.Line44.Y2 = 0.8125F;
			// 
			// Line45
			// 
			this.Line45.Height = 0F;
			this.Line45.Left = 5.5F;
			this.Line45.LineWeight = 1F;
			this.Line45.Name = "Line45";
			this.Line45.Top = 1.25F;
			this.Line45.Width = 4.498611F;
			this.Line45.X1 = 5.5F;
			this.Line45.X2 = 9.998611F;
			this.Line45.Y1 = 1.25F;
			this.Line45.Y2 = 1.25F;
			// 
			// Line46
			// 
			this.Line46.Height = 0F;
			this.Line46.Left = 5.5F;
			this.Line46.LineWeight = 1F;
			this.Line46.Name = "Line46";
			this.Line46.Top = 1.46875F;
			this.Line46.Width = 4.498611F;
			this.Line46.X1 = 5.5F;
			this.Line46.X2 = 9.998611F;
			this.Line46.Y1 = 1.46875F;
			this.Line46.Y2 = 1.46875F;
			// 
			// Line47
			// 
			this.Line47.Height = 0F;
			this.Line47.Left = 5.5F;
			this.Line47.LineWeight = 1F;
			this.Line47.Name = "Line47";
			this.Line47.Top = 1.90625F;
			this.Line47.Width = 4.498611F;
			this.Line47.X1 = 5.5F;
			this.Line47.X2 = 9.998611F;
			this.Line47.Y1 = 1.90625F;
			this.Line47.Y2 = 1.90625F;
			// 
			// Line48
			// 
			this.Line48.Height = 0F;
			this.Line48.Left = 5.5F;
			this.Line48.LineWeight = 1F;
			this.Line48.Name = "Line48";
			this.Line48.Top = 1.6875F;
			this.Line48.Width = 4.498611F;
			this.Line48.X1 = 5.5F;
			this.Line48.X2 = 9.998611F;
			this.Line48.Y1 = 1.6875F;
			this.Line48.Y2 = 1.6875F;
			// 
			// Line49
			// 
			this.Line49.Height = 0F;
			this.Line49.Left = 5.5F;
			this.Line49.LineWeight = 1F;
			this.Line49.Name = "Line49";
			this.Line49.Top = 2.125F;
			this.Line49.Width = 4.498611F;
			this.Line49.X1 = 5.5F;
			this.Line49.X2 = 9.998611F;
			this.Line49.Y1 = 2.125F;
			this.Line49.Y2 = 2.125F;
			// 
			// Line50
			// 
			this.Line50.Height = 0F;
			this.Line50.Left = 5.5F;
			this.Line50.LineWeight = 1F;
			this.Line50.Name = "Line50";
			this.Line50.Top = 2.5625F;
			this.Line50.Width = 4.498611F;
			this.Line50.X1 = 5.5F;
			this.Line50.X2 = 9.998611F;
			this.Line50.Y1 = 2.5625F;
			this.Line50.Y2 = 2.5625F;
			// 
			// Line51
			// 
			this.Line51.Height = 0F;
			this.Line51.Left = 5.5F;
			this.Line51.LineWeight = 1F;
			this.Line51.Name = "Line51";
			this.Line51.Top = 2.34375F;
			this.Line51.Width = 4.498611F;
			this.Line51.X1 = 5.5F;
			this.Line51.X2 = 9.998611F;
			this.Line51.Y1 = 2.34375F;
			this.Line51.Y2 = 2.34375F;
			// 
			// Line52
			// 
			this.Line52.Height = 0F;
			this.Line52.Left = 5.5F;
			this.Line52.LineWeight = 1F;
			this.Line52.Name = "Line52";
			this.Line52.Top = 2.78125F;
			this.Line52.Width = 4.498611F;
			this.Line52.X1 = 5.5F;
			this.Line52.X2 = 9.998611F;
			this.Line52.Y1 = 2.78125F;
			this.Line52.Y2 = 2.78125F;
			// 
			// Line53
			// 
			this.Line53.Height = 0F;
			this.Line53.Left = 5.5F;
			this.Line53.LineWeight = 1F;
			this.Line53.Name = "Line53";
			this.Line53.Top = 3F;
			this.Line53.Width = 4.498611F;
			this.Line53.X1 = 5.5F;
			this.Line53.X2 = 9.998611F;
			this.Line53.Y1 = 3F;
			this.Line53.Y2 = 3F;
			// 
			// Line54
			// 
			this.Line54.Height = 0F;
			this.Line54.Left = 5.5F;
			this.Line54.LineWeight = 1F;
			this.Line54.Name = "Line54";
			this.Line54.Top = 3.4375F;
			this.Line54.Width = 4.498611F;
			this.Line54.X1 = 5.5F;
			this.Line54.X2 = 9.998611F;
			this.Line54.Y1 = 3.4375F;
			this.Line54.Y2 = 3.4375F;
			// 
			// Line55
			// 
			this.Line55.Height = 0F;
			this.Line55.Left = 5.5F;
			this.Line55.LineWeight = 1F;
			this.Line55.Name = "Line55";
			this.Line55.Top = 3.21875F;
			this.Line55.Width = 4.498611F;
			this.Line55.X1 = 5.5F;
			this.Line55.X2 = 9.998611F;
			this.Line55.Y1 = 3.21875F;
			this.Line55.Y2 = 3.21875F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 6.6875F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label29.Text = "Land Data";
			this.Label29.Top = 3.65625F;
			this.Label29.Width = 1.875F;
			// 
			// Line56
			// 
			this.Line56.Height = 0F;
			this.Line56.Left = 5.5F;
			this.Line56.LineWeight = 1F;
			this.Line56.Name = "Line56";
			this.Line56.Top = 3.65625F;
			this.Line56.Width = 4.498611F;
			this.Line56.X1 = 5.5F;
			this.Line56.X2 = 9.998611F;
			this.Line56.Y1 = 3.65625F;
			this.Line56.Y2 = 3.65625F;
			// 
			// Line58
			// 
			this.Line58.Height = 3.53125F;
			this.Line58.Left = 6.5F;
			this.Line58.LineWeight = 1F;
			this.Line58.Name = "Line58";
			this.Line58.Top = 3.84375F;
			this.Line58.Width = 0F;
			this.Line58.X1 = 6.5F;
			this.Line58.X2 = 6.5F;
			this.Line58.Y1 = 3.84375F;
			this.Line58.Y2 = 7.375F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 5.5F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label30.Text = "Front Foot";
			this.Label30.Top = 3.84375F;
			this.Label30.Width = 1F;
			// 
			// Line8
			// 
			this.Line8.Height = 7.166667F;
			this.Line8.Left = 5.5F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 0.2083333F;
			this.Line8.Width = 0F;
			this.Line8.X1 = 5.5F;
			this.Line8.X2 = 5.5F;
			this.Line8.Y1 = 0.2083333F;
			this.Line8.Y2 = 7.375F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.28125F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 9.125F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label31.Text = "Influence Codes";
			this.Label31.Top = 3.875F;
			this.Label31.Width = 0.8743055F;
			// 
			// txtLandDesc11
			// 
			this.txtLandDesc11.Height = 0.125F;
			this.txtLandDesc11.Left = 5.5625F;
			this.txtLandDesc11.Name = "txtLandDesc11";
			this.txtLandDesc11.Style = "font-size: 6pt";
			this.txtLandDesc11.Text = "11.";
			this.txtLandDesc11.Top = 4.125F;
			this.txtLandDesc11.Width = 0.875F;
			// 
			// txtLandDesc12
			// 
			this.txtLandDesc12.Height = 0.125F;
			this.txtLandDesc12.Left = 5.5625F;
			this.txtLandDesc12.Name = "txtLandDesc12";
			this.txtLandDesc12.Style = "font-size: 6pt";
			this.txtLandDesc12.Text = "12.";
			this.txtLandDesc12.Top = 4.25F;
			this.txtLandDesc12.Width = 0.875F;
			// 
			// txtLandDesc13
			// 
			this.txtLandDesc13.Height = 0.125F;
			this.txtLandDesc13.Left = 5.5625F;
			this.txtLandDesc13.Name = "txtLandDesc13";
			this.txtLandDesc13.Style = "font-size: 6pt";
			this.txtLandDesc13.Text = "13.";
			this.txtLandDesc13.Top = 4.375F;
			this.txtLandDesc13.Width = 0.875F;
			// 
			// txtLandDesc14
			// 
			this.txtLandDesc14.Height = 0.125F;
			this.txtLandDesc14.Left = 5.5625F;
			this.txtLandDesc14.Name = "txtLandDesc14";
			this.txtLandDesc14.Style = "font-size: 6pt";
			this.txtLandDesc14.Text = "14.";
			this.txtLandDesc14.Top = 4.5F;
			this.txtLandDesc14.Width = 0.875F;
			// 
			// txtLandDesc15
			// 
			this.txtLandDesc15.Height = 0.125F;
			this.txtLandDesc15.Left = 5.5625F;
			this.txtLandDesc15.Name = "txtLandDesc15";
			this.txtLandDesc15.Style = "font-size: 6pt";
			this.txtLandDesc15.Text = "15.";
			this.txtLandDesc15.Top = 4.625F;
			this.txtLandDesc15.Width = 0.875F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.19F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 8.1875F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label32.Text = "Influence";
			this.Label32.Top = 3.84375F;
			this.Label32.Width = 0.8125F;
			// 
			// Line62
			// 
			this.Line62.Height = 3.21875F;
			this.Line62.Left = 8F;
			this.Line62.LineWeight = 1F;
			this.Line62.Name = "Line62";
			this.Line62.Top = 3.84375F;
			this.Line62.Width = 0F;
			this.Line62.X1 = 8F;
			this.Line62.X2 = 8F;
			this.Line62.Y1 = 3.84375F;
			this.Line62.Y2 = 7.0625F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.19F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 7.0625F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label33.Text = "Effective";
			this.Label33.Top = 3.84375F;
			this.Label33.Width = 0.875F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.19F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 6.5625F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label34.Text = "Type";
			this.Label34.Top = 3.9375F;
			this.Label34.Width = 0.375F;
			// 
			// Line57
			// 
			this.Line57.Height = 0F;
			this.Line57.Left = 5.5F;
			this.Line57.LineWeight = 1F;
			this.Line57.Name = "Line57";
			this.Line57.Top = 3.84375F;
			this.Line57.Width = 4.498611F;
			this.Line57.X1 = 5.5F;
			this.Line57.X2 = 9.998611F;
			this.Line57.Y1 = 3.84375F;
			this.Line57.Y2 = 3.84375F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.125F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 7F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-size: 6pt; font-weight: bold; text-align: center";
			this.Label35.Text = "Frontage";
			this.Label35.Top = 4F;
			this.Label35.Width = 0.5F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.125F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 7.5625F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-size: 6pt; font-weight: bold; text-align: center";
			this.Label36.Text = "Depth";
			this.Label36.Top = 4F;
			this.Label36.Width = 0.4375F;
			// 
			// txtFrontage1
			// 
			this.txtFrontage1.Height = 0.125F;
			this.txtFrontage1.Left = 7F;
			this.txtFrontage1.Name = "txtFrontage1";
			this.txtFrontage1.Style = "font-size: 6pt";
			this.txtFrontage1.Text = "11.";
			this.txtFrontage1.Top = 4.125F;
			this.txtFrontage1.Width = 0.5F;
			// 
			// txtDepth1
			// 
			this.txtDepth1.Height = 0.125F;
			this.txtDepth1.Left = 7.5F;
			this.txtDepth1.Name = "txtDepth1";
			this.txtDepth1.Style = "font-size: 6pt";
			this.txtDepth1.Text = "11.";
			this.txtDepth1.Top = 4.125F;
			this.txtDepth1.Width = 0.5F;
			// 
			// txtFrontFootType1
			// 
			this.txtFrontFootType1.Height = 0.125F;
			this.txtFrontFootType1.Left = 6.5625F;
			this.txtFrontFootType1.Name = "txtFrontFootType1";
			this.txtFrontFootType1.Style = "font-size: 6pt";
			this.txtFrontFootType1.Text = "11.";
			this.txtFrontFootType1.Top = 4.125F;
			this.txtFrontFootType1.Width = 0.4375F;
			// 
			// Line65
			// 
			this.Line65.Height = 3.0625F;
			this.Line65.Left = 8.6875F;
			this.Line65.LineWeight = 1F;
			this.Line65.Name = "Line65";
			this.Line65.Top = 4F;
			this.Line65.Width = 0F;
			this.Line65.X1 = 8.6875F;
			this.Line65.X2 = 8.6875F;
			this.Line65.Y1 = 4F;
			this.Line65.Y2 = 7.0625F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.125F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 8.125F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-size: 6pt; font-weight: bold; text-align: center";
			this.Label37.Text = "Factor";
			this.Label37.Top = 4F;
			this.Label37.Width = 0.5F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.125F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 8.75F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-size: 6pt; font-weight: bold; text-align: center";
			this.Label38.Text = "Code";
			this.Label38.Top = 4F;
			this.Label38.Width = 0.3125F;
			// 
			// Line63
			// 
			this.Line63.Height = 0F;
			this.Line63.Left = 7F;
			this.Line63.LineWeight = 1F;
			this.Line63.Name = "Line63";
			this.Line63.Top = 4F;
			this.Line63.Width = 2.125F;
			this.Line63.X1 = 7F;
			this.Line63.X2 = 9.125F;
			this.Line63.Y1 = 4F;
			this.Line63.Y2 = 4F;
			// 
			// txtFrontFootFactor1
			// 
			this.txtFrontFootFactor1.Height = 0.125F;
			this.txtFrontFootFactor1.Left = 8.0625F;
			this.txtFrontFootFactor1.Name = "txtFrontFootFactor1";
			this.txtFrontFootFactor1.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootFactor1.Text = "100";
			this.txtFrontFootFactor1.Top = 4.125F;
			this.txtFrontFootFactor1.Width = 0.375F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.125F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 8.5F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label39.Text = "%";
			this.Label39.Top = 4.125F;
			this.Label39.Width = 0.125F;
			// 
			// txtFrontFootCode1
			// 
			this.txtFrontFootCode1.Height = 0.125F;
			this.txtFrontFootCode1.Left = 8.75F;
			this.txtFrontFootCode1.Name = "txtFrontFootCode1";
			this.txtFrontFootCode1.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootCode1.Text = "100";
			this.txtFrontFootCode1.Top = 4.125F;
			this.txtFrontFootCode1.Width = 0.3125F;
			// 
			// txtFrontage2
			// 
			this.txtFrontage2.Height = 0.125F;
			this.txtFrontage2.Left = 7F;
			this.txtFrontage2.Name = "txtFrontage2";
			this.txtFrontage2.Style = "font-size: 6pt";
			this.txtFrontage2.Text = "11.";
			this.txtFrontage2.Top = 4.25F;
			this.txtFrontage2.Width = 0.5F;
			// 
			// txtDepth2
			// 
			this.txtDepth2.Height = 0.125F;
			this.txtDepth2.Left = 7.5F;
			this.txtDepth2.Name = "txtDepth2";
			this.txtDepth2.Style = "font-size: 6pt";
			this.txtDepth2.Text = "11.";
			this.txtDepth2.Top = 4.25F;
			this.txtDepth2.Width = 0.5F;
			// 
			// txtFrontFootType2
			// 
			this.txtFrontFootType2.Height = 0.125F;
			this.txtFrontFootType2.Left = 6.5625F;
			this.txtFrontFootType2.Name = "txtFrontFootType2";
			this.txtFrontFootType2.Style = "font-size: 6pt";
			this.txtFrontFootType2.Text = "11.";
			this.txtFrontFootType2.Top = 4.25F;
			this.txtFrontFootType2.Width = 0.4375F;
			// 
			// txtFrontFootFactor2
			// 
			this.txtFrontFootFactor2.Height = 0.125F;
			this.txtFrontFootFactor2.Left = 8.0625F;
			this.txtFrontFootFactor2.Name = "txtFrontFootFactor2";
			this.txtFrontFootFactor2.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootFactor2.Text = "100";
			this.txtFrontFootFactor2.Top = 4.25F;
			this.txtFrontFootFactor2.Width = 0.375F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.125F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 8.5F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label40.Text = "%";
			this.Label40.Top = 4.25F;
			this.Label40.Width = 0.125F;
			// 
			// txtFrontFootCode2
			// 
			this.txtFrontFootCode2.Height = 0.125F;
			this.txtFrontFootCode2.Left = 8.75F;
			this.txtFrontFootCode2.Name = "txtFrontFootCode2";
			this.txtFrontFootCode2.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootCode2.Text = "100";
			this.txtFrontFootCode2.Top = 4.25F;
			this.txtFrontFootCode2.Width = 0.3125F;
			// 
			// txtFrontage3
			// 
			this.txtFrontage3.Height = 0.125F;
			this.txtFrontage3.Left = 7F;
			this.txtFrontage3.Name = "txtFrontage3";
			this.txtFrontage3.Style = "font-size: 6pt";
			this.txtFrontage3.Text = "11.";
			this.txtFrontage3.Top = 4.375F;
			this.txtFrontage3.Width = 0.5F;
			// 
			// txtDepth3
			// 
			this.txtDepth3.Height = 0.125F;
			this.txtDepth3.Left = 7.5F;
			this.txtDepth3.Name = "txtDepth3";
			this.txtDepth3.Style = "font-size: 6pt";
			this.txtDepth3.Text = "11.";
			this.txtDepth3.Top = 4.375F;
			this.txtDepth3.Width = 0.5F;
			// 
			// txtFrontFootType3
			// 
			this.txtFrontFootType3.Height = 0.125F;
			this.txtFrontFootType3.Left = 6.5625F;
			this.txtFrontFootType3.Name = "txtFrontFootType3";
			this.txtFrontFootType3.Style = "font-size: 6pt";
			this.txtFrontFootType3.Text = "11.";
			this.txtFrontFootType3.Top = 4.375F;
			this.txtFrontFootType3.Width = 0.4375F;
			// 
			// txtFrontFootFactor3
			// 
			this.txtFrontFootFactor3.Height = 0.125F;
			this.txtFrontFootFactor3.Left = 8.0625F;
			this.txtFrontFootFactor3.Name = "txtFrontFootFactor3";
			this.txtFrontFootFactor3.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootFactor3.Text = "100";
			this.txtFrontFootFactor3.Top = 4.375F;
			this.txtFrontFootFactor3.Width = 0.375F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.125F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 8.5F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label41.Text = "%";
			this.Label41.Top = 4.375F;
			this.Label41.Width = 0.125F;
			// 
			// txtFrontFootCode3
			// 
			this.txtFrontFootCode3.Height = 0.125F;
			this.txtFrontFootCode3.Left = 8.75F;
			this.txtFrontFootCode3.Name = "txtFrontFootCode3";
			this.txtFrontFootCode3.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootCode3.Text = "100";
			this.txtFrontFootCode3.Top = 4.375F;
			this.txtFrontFootCode3.Width = 0.3125F;
			// 
			// txtFrontage4
			// 
			this.txtFrontage4.Height = 0.125F;
			this.txtFrontage4.Left = 7F;
			this.txtFrontage4.Name = "txtFrontage4";
			this.txtFrontage4.Style = "font-size: 6pt";
			this.txtFrontage4.Text = "11.";
			this.txtFrontage4.Top = 4.5F;
			this.txtFrontage4.Width = 0.5F;
			// 
			// txtDepth4
			// 
			this.txtDepth4.Height = 0.125F;
			this.txtDepth4.Left = 7.5F;
			this.txtDepth4.Name = "txtDepth4";
			this.txtDepth4.Style = "font-size: 6pt";
			this.txtDepth4.Text = "11.";
			this.txtDepth4.Top = 4.5F;
			this.txtDepth4.Width = 0.5F;
			// 
			// txtFrontFootType4
			// 
			this.txtFrontFootType4.Height = 0.125F;
			this.txtFrontFootType4.Left = 6.5625F;
			this.txtFrontFootType4.Name = "txtFrontFootType4";
			this.txtFrontFootType4.Style = "font-size: 6pt";
			this.txtFrontFootType4.Text = "11.";
			this.txtFrontFootType4.Top = 4.5F;
			this.txtFrontFootType4.Width = 0.4375F;
			// 
			// txtFrontFootFactor4
			// 
			this.txtFrontFootFactor4.Height = 0.125F;
			this.txtFrontFootFactor4.Left = 8.0625F;
			this.txtFrontFootFactor4.Name = "txtFrontFootFactor4";
			this.txtFrontFootFactor4.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootFactor4.Text = "100";
			this.txtFrontFootFactor4.Top = 4.5F;
			this.txtFrontFootFactor4.Width = 0.375F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.125F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 8.5F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label42.Text = "%";
			this.Label42.Top = 4.5F;
			this.Label42.Width = 0.125F;
			// 
			// txtFrontFootCode4
			// 
			this.txtFrontFootCode4.Height = 0.125F;
			this.txtFrontFootCode4.Left = 8.75F;
			this.txtFrontFootCode4.Name = "txtFrontFootCode4";
			this.txtFrontFootCode4.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootCode4.Text = "100";
			this.txtFrontFootCode4.Top = 4.5F;
			this.txtFrontFootCode4.Width = 0.3125F;
			// 
			// txtFrontage5
			// 
			this.txtFrontage5.Height = 0.125F;
			this.txtFrontage5.Left = 7F;
			this.txtFrontage5.Name = "txtFrontage5";
			this.txtFrontage5.Style = "font-size: 6pt";
			this.txtFrontage5.Text = "11.";
			this.txtFrontage5.Top = 4.625F;
			this.txtFrontage5.Width = 0.5F;
			// 
			// txtDepth5
			// 
			this.txtDepth5.Height = 0.125F;
			this.txtDepth5.Left = 7.5F;
			this.txtDepth5.Name = "txtDepth5";
			this.txtDepth5.Style = "font-size: 6pt";
			this.txtDepth5.Text = "11.";
			this.txtDepth5.Top = 4.625F;
			this.txtDepth5.Width = 0.5F;
			// 
			// txtFrontFootType5
			// 
			this.txtFrontFootType5.Height = 0.125F;
			this.txtFrontFootType5.Left = 6.5625F;
			this.txtFrontFootType5.Name = "txtFrontFootType5";
			this.txtFrontFootType5.Style = "font-size: 6pt";
			this.txtFrontFootType5.Text = "11.";
			this.txtFrontFootType5.Top = 4.625F;
			this.txtFrontFootType5.Width = 0.4375F;
			// 
			// txtFrontFootFactor5
			// 
			this.txtFrontFootFactor5.Height = 0.125F;
			this.txtFrontFootFactor5.Left = 8.0625F;
			this.txtFrontFootFactor5.Name = "txtFrontFootFactor5";
			this.txtFrontFootFactor5.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootFactor5.Text = "100";
			this.txtFrontFootFactor5.Top = 4.625F;
			this.txtFrontFootFactor5.Width = 0.375F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.125F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 8.5F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label43.Text = "%";
			this.Label43.Top = 4.625F;
			this.Label43.Width = 0.125F;
			// 
			// txtFrontFootCode5
			// 
			this.txtFrontFootCode5.Height = 0.125F;
			this.txtFrontFootCode5.Left = 8.75F;
			this.txtFrontFootCode5.Name = "txtFrontFootCode5";
			this.txtFrontFootCode5.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootCode5.Text = "100";
			this.txtFrontFootCode5.Top = 4.625F;
			this.txtFrontFootCode5.Width = 0.3125F;
			// 
			// txtFrontage6
			// 
			this.txtFrontage6.Height = 0.125F;
			this.txtFrontage6.Left = 7F;
			this.txtFrontage6.Name = "txtFrontage6";
			this.txtFrontage6.Style = "font-size: 6pt";
			this.txtFrontage6.Text = "11.";
			this.txtFrontage6.Top = 4.75F;
			this.txtFrontage6.Width = 0.5F;
			// 
			// txtDepth6
			// 
			this.txtDepth6.Height = 0.125F;
			this.txtDepth6.Left = 7.5F;
			this.txtDepth6.Name = "txtDepth6";
			this.txtDepth6.Style = "font-size: 6pt";
			this.txtDepth6.Text = "11.";
			this.txtDepth6.Top = 4.75F;
			this.txtDepth6.Width = 0.5F;
			// 
			// txtFrontFootType6
			// 
			this.txtFrontFootType6.Height = 0.125F;
			this.txtFrontFootType6.Left = 6.5625F;
			this.txtFrontFootType6.Name = "txtFrontFootType6";
			this.txtFrontFootType6.Style = "font-size: 6pt";
			this.txtFrontFootType6.Text = "11.";
			this.txtFrontFootType6.Top = 4.75F;
			this.txtFrontFootType6.Width = 0.4375F;
			// 
			// txtFrontFootFactor6
			// 
			this.txtFrontFootFactor6.Height = 0.125F;
			this.txtFrontFootFactor6.Left = 8.0625F;
			this.txtFrontFootFactor6.Name = "txtFrontFootFactor6";
			this.txtFrontFootFactor6.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootFactor6.Text = "100";
			this.txtFrontFootFactor6.Top = 4.75F;
			this.txtFrontFootFactor6.Width = 0.375F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.125F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 8.5F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label44.Text = "%";
			this.Label44.Top = 4.75F;
			this.Label44.Width = 0.125F;
			// 
			// txtFrontFootCode6
			// 
			this.txtFrontFootCode6.Height = 0.125F;
			this.txtFrontFootCode6.Left = 8.75F;
			this.txtFrontFootCode6.Name = "txtFrontFootCode6";
			this.txtFrontFootCode6.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootCode6.Text = "100";
			this.txtFrontFootCode6.Top = 4.75F;
			this.txtFrontFootCode6.Width = 0.3125F;
			// 
			// txtFrontage7
			// 
			this.txtFrontage7.Height = 0.125F;
			this.txtFrontage7.Left = 7F;
			this.txtFrontage7.Name = "txtFrontage7";
			this.txtFrontage7.Style = "font-size: 6pt";
			this.txtFrontage7.Text = "11.";
			this.txtFrontage7.Top = 4.875F;
			this.txtFrontage7.Width = 0.5F;
			// 
			// txtDepth7
			// 
			this.txtDepth7.Height = 0.125F;
			this.txtDepth7.Left = 7.5F;
			this.txtDepth7.Name = "txtDepth7";
			this.txtDepth7.Style = "font-size: 6pt";
			this.txtDepth7.Text = "11.";
			this.txtDepth7.Top = 4.875F;
			this.txtDepth7.Width = 0.5F;
			// 
			// txtFrontFootType7
			// 
			this.txtFrontFootType7.Height = 0.125F;
			this.txtFrontFootType7.Left = 6.5625F;
			this.txtFrontFootType7.Name = "txtFrontFootType7";
			this.txtFrontFootType7.Style = "font-size: 6pt";
			this.txtFrontFootType7.Text = "11.";
			this.txtFrontFootType7.Top = 4.875F;
			this.txtFrontFootType7.Width = 0.4375F;
			// 
			// txtFrontFootFactor7
			// 
			this.txtFrontFootFactor7.Height = 0.125F;
			this.txtFrontFootFactor7.Left = 8.0625F;
			this.txtFrontFootFactor7.Name = "txtFrontFootFactor7";
			this.txtFrontFootFactor7.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootFactor7.Text = "100";
			this.txtFrontFootFactor7.Top = 4.875F;
			this.txtFrontFootFactor7.Width = 0.375F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.125F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 8.5F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label45.Text = "%";
			this.Label45.Top = 4.875F;
			this.Label45.Width = 0.125F;
			// 
			// txtFrontFootCode7
			// 
			this.txtFrontFootCode7.Height = 0.125F;
			this.txtFrontFootCode7.Left = 8.75F;
			this.txtFrontFootCode7.Name = "txtFrontFootCode7";
			this.txtFrontFootCode7.Style = "font-size: 6pt; text-align: right";
			this.txtFrontFootCode7.Text = "100";
			this.txtFrontFootCode7.Top = 4.875F;
			this.txtFrontFootCode7.Width = 0.3125F;
			// 
			// Line60
			// 
			this.Line60.Height = 0F;
			this.Line60.Left = 6.5F;
			this.Line60.LineWeight = 1F;
			this.Line60.Name = "Line60";
			this.Line60.Top = 4.125F;
			this.Line60.Width = 2.625F;
			this.Line60.X1 = 6.5F;
			this.Line60.X2 = 9.125F;
			this.Line60.Y1 = 4.125F;
			this.Line60.Y2 = 4.125F;
			// 
			// Line66
			// 
			this.Line66.Height = 0F;
			this.Line66.Left = 6.5F;
			this.Line66.LineWeight = 1F;
			this.Line66.Name = "Line66";
			this.Line66.Top = 4.25F;
			this.Line66.Width = 2.625F;
			this.Line66.X1 = 6.5F;
			this.Line66.X2 = 9.125F;
			this.Line66.Y1 = 4.25F;
			this.Line66.Y2 = 4.25F;
			// 
			// Line67
			// 
			this.Line67.Height = 0F;
			this.Line67.Left = 6.5F;
			this.Line67.LineWeight = 1F;
			this.Line67.Name = "Line67";
			this.Line67.Top = 4.375F;
			this.Line67.Width = 2.625F;
			this.Line67.X1 = 6.5F;
			this.Line67.X2 = 9.125F;
			this.Line67.Y1 = 4.375F;
			this.Line67.Y2 = 4.375F;
			// 
			// Line68
			// 
			this.Line68.Height = 0F;
			this.Line68.Left = 6.5F;
			this.Line68.LineWeight = 1F;
			this.Line68.Name = "Line68";
			this.Line68.Top = 4.5F;
			this.Line68.Width = 2.625F;
			this.Line68.X1 = 6.5F;
			this.Line68.X2 = 9.125F;
			this.Line68.Y1 = 4.5F;
			this.Line68.Y2 = 4.5F;
			// 
			// Line69
			// 
			this.Line69.Height = 0F;
			this.Line69.Left = 6.5F;
			this.Line69.LineWeight = 1F;
			this.Line69.Name = "Line69";
			this.Line69.Top = 4.625F;
			this.Line69.Width = 2.625F;
			this.Line69.X1 = 6.5F;
			this.Line69.X2 = 9.125F;
			this.Line69.Y1 = 4.625F;
			this.Line69.Y2 = 4.625F;
			// 
			// Line70
			// 
			this.Line70.Height = 0F;
			this.Line70.Left = 6.5F;
			this.Line70.LineWeight = 1F;
			this.Line70.Name = "Line70";
			this.Line70.Top = 4.75F;
			this.Line70.Width = 2.625F;
			this.Line70.X1 = 6.5F;
			this.Line70.X2 = 9.125F;
			this.Line70.Y1 = 4.75F;
			this.Line70.Y2 = 4.75F;
			// 
			// Line71
			// 
			this.Line71.Height = 0F;
			this.Line71.Left = 6.5F;
			this.Line71.LineWeight = 1F;
			this.Line71.Name = "Line71";
			this.Line71.Top = 4.875F;
			this.Line71.Width = 2.625F;
			this.Line71.X1 = 6.5F;
			this.Line71.X2 = 9.125F;
			this.Line71.Y1 = 4.875F;
			this.Line71.Y2 = 4.875F;
			// 
			// Line64
			// 
			this.Line64.Height = 1F;
			this.Line64.Left = 7.5F;
			this.Line64.LineWeight = 1F;
			this.Line64.Name = "Line64";
			this.Line64.Top = 4F;
			this.Line64.Width = 0F;
			this.Line64.X1 = 7.5F;
			this.Line64.X2 = 7.5F;
			this.Line64.Y1 = 4F;
			this.Line64.Y2 = 5F;
			// 
			// Line61
			// 
			this.Line61.Height = 3.21875F;
			this.Line61.Left = 7F;
			this.Line61.LineWeight = 1F;
			this.Line61.Name = "Line61";
			this.Line61.Top = 3.84375F;
			this.Line61.Width = 0F;
			this.Line61.X1 = 7F;
			this.Line61.X2 = 7F;
			this.Line61.Y1 = 3.84375F;
			this.Line61.Y2 = 7.0625F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.19F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 7.0625F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label46.Text = "Square Feet";
			this.Label46.Top = 5F;
			this.Label46.Width = 0.875F;
			// 
			// txtSqftUnits1
			// 
			this.txtSqftUnits1.Height = 0.125F;
			this.txtSqftUnits1.Left = 7.0625F;
			this.txtSqftUnits1.Name = "txtSqftUnits1";
			this.txtSqftUnits1.Style = "font-size: 6pt; text-align: right";
			this.txtSqftUnits1.Text = "11.";
			this.txtSqftUnits1.Top = 5.15625F;
			this.txtSqftUnits1.Width = 0.875F;
			// 
			// txtSqftType1
			// 
			this.txtSqftType1.Height = 0.125F;
			this.txtSqftType1.Left = 6.5625F;
			this.txtSqftType1.Name = "txtSqftType1";
			this.txtSqftType1.Style = "font-size: 6pt";
			this.txtSqftType1.Text = "11.";
			this.txtSqftType1.Top = 5.15625F;
			this.txtSqftType1.Width = 0.4375F;
			// 
			// txtSqftFactor1
			// 
			this.txtSqftFactor1.Height = 0.125F;
			this.txtSqftFactor1.Left = 8.0625F;
			this.txtSqftFactor1.Name = "txtSqftFactor1";
			this.txtSqftFactor1.Style = "font-size: 6pt; text-align: right";
			this.txtSqftFactor1.Text = "100";
			this.txtSqftFactor1.Top = 5.15625F;
			this.txtSqftFactor1.Width = 0.375F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.125F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 8.5F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label47.Text = "%";
			this.Label47.Top = 5.15625F;
			this.Label47.Width = 0.125F;
			// 
			// txtSqftCode1
			// 
			this.txtSqftCode1.Height = 0.125F;
			this.txtSqftCode1.Left = 8.75F;
			this.txtSqftCode1.Name = "txtSqftCode1";
			this.txtSqftCode1.Style = "font-size: 6pt; text-align: right";
			this.txtSqftCode1.Text = "100";
			this.txtSqftCode1.Top = 5.15625F;
			this.txtSqftCode1.Width = 0.3125F;
			// 
			// txtSqftUnits2
			// 
			this.txtSqftUnits2.Height = 0.125F;
			this.txtSqftUnits2.Left = 7.0625F;
			this.txtSqftUnits2.Name = "txtSqftUnits2";
			this.txtSqftUnits2.Style = "font-size: 6pt; text-align: right";
			this.txtSqftUnits2.Text = "11.";
			this.txtSqftUnits2.Top = 5.28125F;
			this.txtSqftUnits2.Width = 0.875F;
			// 
			// txtSqftType2
			// 
			this.txtSqftType2.Height = 0.125F;
			this.txtSqftType2.Left = 6.5625F;
			this.txtSqftType2.Name = "txtSqftType2";
			this.txtSqftType2.Style = "font-size: 6pt";
			this.txtSqftType2.Text = "11.";
			this.txtSqftType2.Top = 5.28125F;
			this.txtSqftType2.Width = 0.4375F;
			// 
			// txtSqftFactor2
			// 
			this.txtSqftFactor2.Height = 0.125F;
			this.txtSqftFactor2.Left = 8.0625F;
			this.txtSqftFactor2.Name = "txtSqftFactor2";
			this.txtSqftFactor2.Style = "font-size: 6pt; text-align: right";
			this.txtSqftFactor2.Text = "100";
			this.txtSqftFactor2.Top = 5.28125F;
			this.txtSqftFactor2.Width = 0.375F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.125F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 8.5F;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label48.Text = "%";
			this.Label48.Top = 5.28125F;
			this.Label48.Width = 0.125F;
			// 
			// txtSqftCode2
			// 
			this.txtSqftCode2.Height = 0.125F;
			this.txtSqftCode2.Left = 8.75F;
			this.txtSqftCode2.Name = "txtSqftCode2";
			this.txtSqftCode2.Style = "font-size: 6pt; text-align: right";
			this.txtSqftCode2.Text = "100";
			this.txtSqftCode2.Top = 5.28125F;
			this.txtSqftCode2.Width = 0.3125F;
			// 
			// txtSqftUnits3
			// 
			this.txtSqftUnits3.Height = 0.125F;
			this.txtSqftUnits3.Left = 7.0625F;
			this.txtSqftUnits3.Name = "txtSqftUnits3";
			this.txtSqftUnits3.Style = "font-size: 6pt; text-align: right";
			this.txtSqftUnits3.Text = "11.";
			this.txtSqftUnits3.Top = 5.40625F;
			this.txtSqftUnits3.Width = 0.875F;
			// 
			// txtSqftType3
			// 
			this.txtSqftType3.Height = 0.125F;
			this.txtSqftType3.Left = 6.5625F;
			this.txtSqftType3.Name = "txtSqftType3";
			this.txtSqftType3.Style = "font-size: 6pt";
			this.txtSqftType3.Text = "11.";
			this.txtSqftType3.Top = 5.40625F;
			this.txtSqftType3.Width = 0.4375F;
			// 
			// txtSqftFactor3
			// 
			this.txtSqftFactor3.Height = 0.125F;
			this.txtSqftFactor3.Left = 8.0625F;
			this.txtSqftFactor3.Name = "txtSqftFactor3";
			this.txtSqftFactor3.Style = "font-size: 6pt; text-align: right";
			this.txtSqftFactor3.Text = "100";
			this.txtSqftFactor3.Top = 5.40625F;
			this.txtSqftFactor3.Width = 0.375F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.125F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 8.5F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label49.Text = "%";
			this.Label49.Top = 5.40625F;
			this.Label49.Width = 0.125F;
			// 
			// txtSqftCode3
			// 
			this.txtSqftCode3.Height = 0.125F;
			this.txtSqftCode3.Left = 8.75F;
			this.txtSqftCode3.Name = "txtSqftCode3";
			this.txtSqftCode3.Style = "font-size: 6pt; text-align: right";
			this.txtSqftCode3.Text = "100";
			this.txtSqftCode3.Top = 5.40625F;
			this.txtSqftCode3.Width = 0.3125F;
			// 
			// txtSqftUnits4
			// 
			this.txtSqftUnits4.Height = 0.125F;
			this.txtSqftUnits4.Left = 7.0625F;
			this.txtSqftUnits4.Name = "txtSqftUnits4";
			this.txtSqftUnits4.Style = "font-size: 6pt; text-align: right";
			this.txtSqftUnits4.Text = "11.";
			this.txtSqftUnits4.Top = 5.53125F;
			this.txtSqftUnits4.Width = 0.875F;
			// 
			// txtSqftType4
			// 
			this.txtSqftType4.Height = 0.125F;
			this.txtSqftType4.Left = 6.5625F;
			this.txtSqftType4.Name = "txtSqftType4";
			this.txtSqftType4.Style = "font-size: 6pt";
			this.txtSqftType4.Text = "11.";
			this.txtSqftType4.Top = 5.53125F;
			this.txtSqftType4.Width = 0.4375F;
			// 
			// txtSqftFactor4
			// 
			this.txtSqftFactor4.Height = 0.125F;
			this.txtSqftFactor4.Left = 8.0625F;
			this.txtSqftFactor4.Name = "txtSqftFactor4";
			this.txtSqftFactor4.Style = "font-size: 6pt; text-align: right";
			this.txtSqftFactor4.Text = "100";
			this.txtSqftFactor4.Top = 5.53125F;
			this.txtSqftFactor4.Width = 0.375F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.125F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 8.5F;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label50.Text = "%";
			this.Label50.Top = 5.53125F;
			this.Label50.Width = 0.125F;
			// 
			// txtSqftCode4
			// 
			this.txtSqftCode4.Height = 0.125F;
			this.txtSqftCode4.Left = 8.75F;
			this.txtSqftCode4.Name = "txtSqftCode4";
			this.txtSqftCode4.Style = "font-size: 6pt; text-align: right";
			this.txtSqftCode4.Text = "100";
			this.txtSqftCode4.Top = 5.53125F;
			this.txtSqftCode4.Width = 0.3125F;
			// 
			// txtSqftUnits5
			// 
			this.txtSqftUnits5.Height = 0.125F;
			this.txtSqftUnits5.Left = 7.0625F;
			this.txtSqftUnits5.Name = "txtSqftUnits5";
			this.txtSqftUnits5.Style = "font-size: 6pt; text-align: right";
			this.txtSqftUnits5.Text = "11.";
			this.txtSqftUnits5.Top = 5.65625F;
			this.txtSqftUnits5.Width = 0.875F;
			// 
			// txtSqftType5
			// 
			this.txtSqftType5.Height = 0.125F;
			this.txtSqftType5.Left = 6.5625F;
			this.txtSqftType5.Name = "txtSqftType5";
			this.txtSqftType5.Style = "font-size: 6pt";
			this.txtSqftType5.Text = "11.";
			this.txtSqftType5.Top = 5.65625F;
			this.txtSqftType5.Width = 0.4375F;
			// 
			// txtSqftFactor5
			// 
			this.txtSqftFactor5.Height = 0.125F;
			this.txtSqftFactor5.Left = 8.0625F;
			this.txtSqftFactor5.Name = "txtSqftFactor5";
			this.txtSqftFactor5.Style = "font-size: 6pt; text-align: right";
			this.txtSqftFactor5.Text = "100";
			this.txtSqftFactor5.Top = 5.65625F;
			this.txtSqftFactor5.Width = 0.375F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.125F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 8.5F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label51.Text = "%";
			this.Label51.Top = 5.65625F;
			this.Label51.Width = 0.125F;
			// 
			// txtSqftCode5
			// 
			this.txtSqftCode5.Height = 0.125F;
			this.txtSqftCode5.Left = 8.75F;
			this.txtSqftCode5.Name = "txtSqftCode5";
			this.txtSqftCode5.Style = "font-size: 6pt; text-align: right";
			this.txtSqftCode5.Text = "100";
			this.txtSqftCode5.Top = 5.65625F;
			this.txtSqftCode5.Width = 0.3125F;
			// 
			// txtSqftUnits6
			// 
			this.txtSqftUnits6.Height = 0.125F;
			this.txtSqftUnits6.Left = 7.0625F;
			this.txtSqftUnits6.Name = "txtSqftUnits6";
			this.txtSqftUnits6.Style = "font-size: 6pt; text-align: right";
			this.txtSqftUnits6.Text = "11.";
			this.txtSqftUnits6.Top = 5.78125F;
			this.txtSqftUnits6.Width = 0.875F;
			// 
			// txtSqftType6
			// 
			this.txtSqftType6.Height = 0.125F;
			this.txtSqftType6.Left = 6.5625F;
			this.txtSqftType6.Name = "txtSqftType6";
			this.txtSqftType6.Style = "font-size: 6pt";
			this.txtSqftType6.Text = "11.";
			this.txtSqftType6.Top = 5.78125F;
			this.txtSqftType6.Width = 0.4375F;
			// 
			// txtSqftFactor6
			// 
			this.txtSqftFactor6.Height = 0.125F;
			this.txtSqftFactor6.Left = 8.0625F;
			this.txtSqftFactor6.Name = "txtSqftFactor6";
			this.txtSqftFactor6.Style = "font-size: 6pt; text-align: right";
			this.txtSqftFactor6.Text = "100";
			this.txtSqftFactor6.Top = 5.78125F;
			this.txtSqftFactor6.Width = 0.375F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.125F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 8.5F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label52.Text = "%";
			this.Label52.Top = 5.78125F;
			this.Label52.Width = 0.125F;
			// 
			// txtSqftCode6
			// 
			this.txtSqftCode6.Height = 0.125F;
			this.txtSqftCode6.Left = 8.75F;
			this.txtSqftCode6.Name = "txtSqftCode6";
			this.txtSqftCode6.Style = "font-size: 6pt; text-align: right";
			this.txtSqftCode6.Text = "100";
			this.txtSqftCode6.Top = 5.78125F;
			this.txtSqftCode6.Width = 0.3125F;
			// 
			// txtSqftUnits7
			// 
			this.txtSqftUnits7.Height = 0.125F;
			this.txtSqftUnits7.Left = 7.0625F;
			this.txtSqftUnits7.Name = "txtSqftUnits7";
			this.txtSqftUnits7.Style = "font-size: 6pt; text-align: right";
			this.txtSqftUnits7.Text = "11.";
			this.txtSqftUnits7.Top = 5.90625F;
			this.txtSqftUnits7.Width = 0.875F;
			// 
			// txtSqftType7
			// 
			this.txtSqftType7.Height = 0.125F;
			this.txtSqftType7.Left = 6.5625F;
			this.txtSqftType7.Name = "txtSqftType7";
			this.txtSqftType7.Style = "font-size: 6pt";
			this.txtSqftType7.Text = "11.";
			this.txtSqftType7.Top = 5.90625F;
			this.txtSqftType7.Width = 0.4375F;
			// 
			// txtSqftFactor7
			// 
			this.txtSqftFactor7.Height = 0.125F;
			this.txtSqftFactor7.Left = 8.0625F;
			this.txtSqftFactor7.Name = "txtSqftFactor7";
			this.txtSqftFactor7.Style = "font-size: 6pt; text-align: right";
			this.txtSqftFactor7.Text = "100";
			this.txtSqftFactor7.Top = 5.90625F;
			this.txtSqftFactor7.Width = 0.375F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.125F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 8.5F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label53.Text = "%";
			this.Label53.Top = 5.90625F;
			this.Label53.Width = 0.125F;
			// 
			// txtSqftCode7
			// 
			this.txtSqftCode7.Height = 0.125F;
			this.txtSqftCode7.Left = 8.75F;
			this.txtSqftCode7.Name = "txtSqftCode7";
			this.txtSqftCode7.Style = "font-size: 6pt; text-align: right";
			this.txtSqftCode7.Text = "100";
			this.txtSqftCode7.Top = 5.90625F;
			this.txtSqftCode7.Width = 0.3125F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.19F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 7.0625F;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label54.Text = "Acreage/Sites";
			this.Label54.Top = 6.03125F;
			this.Label54.Width = 0.875F;
			// 
			// txtAcreageUnits1
			// 
			this.txtAcreageUnits1.Height = 0.125F;
			this.txtAcreageUnits1.Left = 7.0625F;
			this.txtAcreageUnits1.Name = "txtAcreageUnits1";
			this.txtAcreageUnits1.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageUnits1.Text = "11.";
			this.txtAcreageUnits1.Top = 6.1875F;
			this.txtAcreageUnits1.Width = 0.875F;
			// 
			// txtAcreageType1
			// 
			this.txtAcreageType1.Height = 0.125F;
			this.txtAcreageType1.Left = 6.5625F;
			this.txtAcreageType1.Name = "txtAcreageType1";
			this.txtAcreageType1.Style = "font-size: 6pt";
			this.txtAcreageType1.Text = "11.";
			this.txtAcreageType1.Top = 6.1875F;
			this.txtAcreageType1.Width = 0.4375F;
			// 
			// txtAcreageFactor1
			// 
			this.txtAcreageFactor1.Height = 0.125F;
			this.txtAcreageFactor1.Left = 8.0625F;
			this.txtAcreageFactor1.Name = "txtAcreageFactor1";
			this.txtAcreageFactor1.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageFactor1.Text = "100";
			this.txtAcreageFactor1.Top = 6.1875F;
			this.txtAcreageFactor1.Width = 0.375F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.125F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 8.5F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label55.Text = "%";
			this.Label55.Top = 6.1875F;
			this.Label55.Width = 0.125F;
			// 
			// txtAcreageCode1
			// 
			this.txtAcreageCode1.Height = 0.125F;
			this.txtAcreageCode1.Left = 8.75F;
			this.txtAcreageCode1.Name = "txtAcreageCode1";
			this.txtAcreageCode1.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageCode1.Text = "100";
			this.txtAcreageCode1.Top = 6.1875F;
			this.txtAcreageCode1.Width = 0.3125F;
			// 
			// txtAcreageUnits2
			// 
			this.txtAcreageUnits2.Height = 0.125F;
			this.txtAcreageUnits2.Left = 7.0625F;
			this.txtAcreageUnits2.Name = "txtAcreageUnits2";
			this.txtAcreageUnits2.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageUnits2.Text = "11.";
			this.txtAcreageUnits2.Top = 6.3125F;
			this.txtAcreageUnits2.Width = 0.875F;
			// 
			// txtAcreageType2
			// 
			this.txtAcreageType2.Height = 0.125F;
			this.txtAcreageType2.Left = 6.5625F;
			this.txtAcreageType2.Name = "txtAcreageType2";
			this.txtAcreageType2.Style = "font-size: 6pt";
			this.txtAcreageType2.Text = "11.";
			this.txtAcreageType2.Top = 6.3125F;
			this.txtAcreageType2.Width = 0.4375F;
			// 
			// txtAcreageFactor2
			// 
			this.txtAcreageFactor2.Height = 0.125F;
			this.txtAcreageFactor2.Left = 8.0625F;
			this.txtAcreageFactor2.Name = "txtAcreageFactor2";
			this.txtAcreageFactor2.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageFactor2.Text = "100";
			this.txtAcreageFactor2.Top = 6.3125F;
			this.txtAcreageFactor2.Width = 0.375F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.125F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 8.5F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label56.Text = "%";
			this.Label56.Top = 6.3125F;
			this.Label56.Width = 0.125F;
			// 
			// txtAcreageCode2
			// 
			this.txtAcreageCode2.Height = 0.125F;
			this.txtAcreageCode2.Left = 8.75F;
			this.txtAcreageCode2.Name = "txtAcreageCode2";
			this.txtAcreageCode2.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageCode2.Text = "100";
			this.txtAcreageCode2.Top = 6.3125F;
			this.txtAcreageCode2.Width = 0.3125F;
			// 
			// txtAcreageUnits3
			// 
			this.txtAcreageUnits3.Height = 0.125F;
			this.txtAcreageUnits3.Left = 7.0625F;
			this.txtAcreageUnits3.Name = "txtAcreageUnits3";
			this.txtAcreageUnits3.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageUnits3.Text = "11.";
			this.txtAcreageUnits3.Top = 6.4375F;
			this.txtAcreageUnits3.Width = 0.875F;
			// 
			// txtAcreageType3
			// 
			this.txtAcreageType3.Height = 0.125F;
			this.txtAcreageType3.Left = 6.5625F;
			this.txtAcreageType3.Name = "txtAcreageType3";
			this.txtAcreageType3.Style = "font-size: 6pt";
			this.txtAcreageType3.Text = "11.";
			this.txtAcreageType3.Top = 6.4375F;
			this.txtAcreageType3.Width = 0.4375F;
			// 
			// txtAcreageFactor3
			// 
			this.txtAcreageFactor3.Height = 0.125F;
			this.txtAcreageFactor3.Left = 8.0625F;
			this.txtAcreageFactor3.Name = "txtAcreageFactor3";
			this.txtAcreageFactor3.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageFactor3.Text = "100";
			this.txtAcreageFactor3.Top = 6.4375F;
			this.txtAcreageFactor3.Width = 0.375F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.125F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 8.5F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label57.Text = "%";
			this.Label57.Top = 6.4375F;
			this.Label57.Width = 0.125F;
			// 
			// txtAcreageCode3
			// 
			this.txtAcreageCode3.Height = 0.125F;
			this.txtAcreageCode3.Left = 8.75F;
			this.txtAcreageCode3.Name = "txtAcreageCode3";
			this.txtAcreageCode3.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageCode3.Text = "100";
			this.txtAcreageCode3.Top = 6.4375F;
			this.txtAcreageCode3.Width = 0.3125F;
			// 
			// txtAcreageUnits4
			// 
			this.txtAcreageUnits4.Height = 0.125F;
			this.txtAcreageUnits4.Left = 7.0625F;
			this.txtAcreageUnits4.Name = "txtAcreageUnits4";
			this.txtAcreageUnits4.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageUnits4.Text = "11.";
			this.txtAcreageUnits4.Top = 6.5625F;
			this.txtAcreageUnits4.Width = 0.875F;
			// 
			// txtAcreageType4
			// 
			this.txtAcreageType4.Height = 0.125F;
			this.txtAcreageType4.Left = 6.5625F;
			this.txtAcreageType4.Name = "txtAcreageType4";
			this.txtAcreageType4.Style = "font-size: 6pt";
			this.txtAcreageType4.Text = "11.";
			this.txtAcreageType4.Top = 6.5625F;
			this.txtAcreageType4.Width = 0.4375F;
			// 
			// txtAcreageFactor4
			// 
			this.txtAcreageFactor4.Height = 0.125F;
			this.txtAcreageFactor4.Left = 8.0625F;
			this.txtAcreageFactor4.Name = "txtAcreageFactor4";
			this.txtAcreageFactor4.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageFactor4.Text = "100";
			this.txtAcreageFactor4.Top = 6.5625F;
			this.txtAcreageFactor4.Width = 0.375F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.125F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 8.5F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label58.Text = "%";
			this.Label58.Top = 6.5625F;
			this.Label58.Width = 0.125F;
			// 
			// txtAcreageCode4
			// 
			this.txtAcreageCode4.Height = 0.125F;
			this.txtAcreageCode4.Left = 8.75F;
			this.txtAcreageCode4.Name = "txtAcreageCode4";
			this.txtAcreageCode4.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageCode4.Text = "100";
			this.txtAcreageCode4.Top = 6.5625F;
			this.txtAcreageCode4.Width = 0.3125F;
			// 
			// txtAcreageUnits5
			// 
			this.txtAcreageUnits5.Height = 0.125F;
			this.txtAcreageUnits5.Left = 7.0625F;
			this.txtAcreageUnits5.Name = "txtAcreageUnits5";
			this.txtAcreageUnits5.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageUnits5.Text = "11.";
			this.txtAcreageUnits5.Top = 6.6875F;
			this.txtAcreageUnits5.Width = 0.875F;
			// 
			// txtAcreageType5
			// 
			this.txtAcreageType5.Height = 0.125F;
			this.txtAcreageType5.Left = 6.5625F;
			this.txtAcreageType5.Name = "txtAcreageType5";
			this.txtAcreageType5.Style = "font-size: 6pt";
			this.txtAcreageType5.Text = "11.";
			this.txtAcreageType5.Top = 6.6875F;
			this.txtAcreageType5.Width = 0.4375F;
			// 
			// txtAcreageFactor5
			// 
			this.txtAcreageFactor5.Height = 0.125F;
			this.txtAcreageFactor5.Left = 8.0625F;
			this.txtAcreageFactor5.Name = "txtAcreageFactor5";
			this.txtAcreageFactor5.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageFactor5.Text = "100";
			this.txtAcreageFactor5.Top = 6.6875F;
			this.txtAcreageFactor5.Width = 0.375F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.125F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 8.5F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label59.Text = "%";
			this.Label59.Top = 6.6875F;
			this.Label59.Width = 0.125F;
			// 
			// txtAcreageCode5
			// 
			this.txtAcreageCode5.Height = 0.125F;
			this.txtAcreageCode5.Left = 8.75F;
			this.txtAcreageCode5.Name = "txtAcreageCode5";
			this.txtAcreageCode5.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageCode5.Text = "100";
			this.txtAcreageCode5.Top = 6.6875F;
			this.txtAcreageCode5.Width = 0.3125F;
			// 
			// txtAcreageUnits6
			// 
			this.txtAcreageUnits6.Height = 0.125F;
			this.txtAcreageUnits6.Left = 7.0625F;
			this.txtAcreageUnits6.Name = "txtAcreageUnits6";
			this.txtAcreageUnits6.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageUnits6.Text = "11.";
			this.txtAcreageUnits6.Top = 6.8125F;
			this.txtAcreageUnits6.Width = 0.875F;
			// 
			// txtAcreageType6
			// 
			this.txtAcreageType6.Height = 0.125F;
			this.txtAcreageType6.Left = 6.5625F;
			this.txtAcreageType6.Name = "txtAcreageType6";
			this.txtAcreageType6.Style = "font-size: 6pt";
			this.txtAcreageType6.Text = "11.";
			this.txtAcreageType6.Top = 6.8125F;
			this.txtAcreageType6.Width = 0.4375F;
			// 
			// txtAcreageFactor6
			// 
			this.txtAcreageFactor6.Height = 0.125F;
			this.txtAcreageFactor6.Left = 8.0625F;
			this.txtAcreageFactor6.Name = "txtAcreageFactor6";
			this.txtAcreageFactor6.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageFactor6.Text = "100";
			this.txtAcreageFactor6.Top = 6.8125F;
			this.txtAcreageFactor6.Width = 0.375F;
			// 
			// Label60
			// 
			this.Label60.Height = 0.125F;
			this.Label60.HyperLink = null;
			this.Label60.Left = 8.5F;
			this.Label60.Name = "Label60";
			this.Label60.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label60.Text = "%";
			this.Label60.Top = 6.8125F;
			this.Label60.Width = 0.125F;
			// 
			// txtAcreageCode6
			// 
			this.txtAcreageCode6.Height = 0.125F;
			this.txtAcreageCode6.Left = 8.75F;
			this.txtAcreageCode6.Name = "txtAcreageCode6";
			this.txtAcreageCode6.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageCode6.Text = "100";
			this.txtAcreageCode6.Top = 6.8125F;
			this.txtAcreageCode6.Width = 0.3125F;
			// 
			// txtAcreageUnits7
			// 
			this.txtAcreageUnits7.Height = 0.125F;
			this.txtAcreageUnits7.Left = 7.0625F;
			this.txtAcreageUnits7.Name = "txtAcreageUnits7";
			this.txtAcreageUnits7.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageUnits7.Text = "11.";
			this.txtAcreageUnits7.Top = 6.9375F;
			this.txtAcreageUnits7.Width = 0.875F;
			// 
			// txtAcreageType7
			// 
			this.txtAcreageType7.Height = 0.125F;
			this.txtAcreageType7.Left = 6.5625F;
			this.txtAcreageType7.Name = "txtAcreageType7";
			this.txtAcreageType7.Style = "font-size: 6pt";
			this.txtAcreageType7.Text = "11.";
			this.txtAcreageType7.Top = 6.9375F;
			this.txtAcreageType7.Width = 0.4375F;
			// 
			// txtAcreageFactor7
			// 
			this.txtAcreageFactor7.Height = 0.125F;
			this.txtAcreageFactor7.Left = 8.0625F;
			this.txtAcreageFactor7.Name = "txtAcreageFactor7";
			this.txtAcreageFactor7.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageFactor7.Text = "100";
			this.txtAcreageFactor7.Top = 6.9375F;
			this.txtAcreageFactor7.Width = 0.375F;
			// 
			// Label61
			// 
			this.Label61.Height = 0.125F;
			this.Label61.HyperLink = null;
			this.Label61.Left = 8.5F;
			this.Label61.Name = "Label61";
			this.Label61.Style = "font-size: 6pt; font-weight: bold; text-align: left";
			this.Label61.Text = "%";
			this.Label61.Top = 6.9375F;
			this.Label61.Width = 0.125F;
			// 
			// txtAcreageCode7
			// 
			this.txtAcreageCode7.Height = 0.125F;
			this.txtAcreageCode7.Left = 8.75F;
			this.txtAcreageCode7.Name = "txtAcreageCode7";
			this.txtAcreageCode7.Style = "font-size: 6pt; text-align: right";
			this.txtAcreageCode7.Text = "100";
			this.txtAcreageCode7.Top = 6.9375F;
			this.txtAcreageCode7.Width = 0.3125F;
			// 
			// Line73
			// 
			this.Line73.Height = 0F;
			this.Line73.Left = 6.5F;
			this.Line73.LineWeight = 1F;
			this.Line73.Name = "Line73";
			this.Line73.Top = 5.15625F;
			this.Line73.Width = 2.625F;
			this.Line73.X1 = 6.5F;
			this.Line73.X2 = 9.125F;
			this.Line73.Y1 = 5.15625F;
			this.Line73.Y2 = 5.15625F;
			// 
			// Line74
			// 
			this.Line74.Height = 0F;
			this.Line74.Left = 6.5F;
			this.Line74.LineWeight = 1F;
			this.Line74.Name = "Line74";
			this.Line74.Top = 5.28125F;
			this.Line74.Width = 2.625F;
			this.Line74.X1 = 6.5F;
			this.Line74.X2 = 9.125F;
			this.Line74.Y1 = 5.28125F;
			this.Line74.Y2 = 5.28125F;
			// 
			// Line75
			// 
			this.Line75.Height = 0F;
			this.Line75.Left = 6.5F;
			this.Line75.LineWeight = 1F;
			this.Line75.Name = "Line75";
			this.Line75.Top = 5.40625F;
			this.Line75.Width = 2.625F;
			this.Line75.X1 = 6.5F;
			this.Line75.X2 = 9.125F;
			this.Line75.Y1 = 5.40625F;
			this.Line75.Y2 = 5.40625F;
			// 
			// Line76
			// 
			this.Line76.Height = 0F;
			this.Line76.Left = 6.5F;
			this.Line76.LineWeight = 1F;
			this.Line76.Name = "Line76";
			this.Line76.Top = 5.53125F;
			this.Line76.Width = 2.625F;
			this.Line76.X1 = 6.5F;
			this.Line76.X2 = 9.125F;
			this.Line76.Y1 = 5.53125F;
			this.Line76.Y2 = 5.53125F;
			// 
			// Line77
			// 
			this.Line77.Height = 0F;
			this.Line77.Left = 6.5F;
			this.Line77.LineWeight = 1F;
			this.Line77.Name = "Line77";
			this.Line77.Top = 5.65625F;
			this.Line77.Width = 2.625F;
			this.Line77.X1 = 6.5F;
			this.Line77.X2 = 9.125F;
			this.Line77.Y1 = 5.65625F;
			this.Line77.Y2 = 5.65625F;
			// 
			// Line78
			// 
			this.Line78.Height = 0F;
			this.Line78.Left = 6.5F;
			this.Line78.LineWeight = 1F;
			this.Line78.Name = "Line78";
			this.Line78.Top = 5.78125F;
			this.Line78.Width = 2.625F;
			this.Line78.X1 = 6.5F;
			this.Line78.X2 = 9.125F;
			this.Line78.Y1 = 5.78125F;
			this.Line78.Y2 = 5.78125F;
			// 
			// Line79
			// 
			this.Line79.Height = 0F;
			this.Line79.Left = 6.5F;
			this.Line79.LineWeight = 1F;
			this.Line79.Name = "Line79";
			this.Line79.Top = 5.90625F;
			this.Line79.Width = 2.625F;
			this.Line79.X1 = 6.5F;
			this.Line79.X2 = 9.125F;
			this.Line79.Y1 = 5.90625F;
			this.Line79.Y2 = 5.90625F;
			// 
			// Line81
			// 
			this.Line81.Height = 0F;
			this.Line81.Left = 6.5F;
			this.Line81.LineWeight = 1F;
			this.Line81.Name = "Line81";
			this.Line81.Top = 6.3125F;
			this.Line81.Width = 2.625F;
			this.Line81.X1 = 6.5F;
			this.Line81.X2 = 9.125F;
			this.Line81.Y1 = 6.3125F;
			this.Line81.Y2 = 6.3125F;
			// 
			// Line82
			// 
			this.Line82.Height = 0F;
			this.Line82.Left = 6.5F;
			this.Line82.LineWeight = 1F;
			this.Line82.Name = "Line82";
			this.Line82.Top = 6.4375F;
			this.Line82.Width = 2.625F;
			this.Line82.X1 = 6.5F;
			this.Line82.X2 = 9.125F;
			this.Line82.Y1 = 6.4375F;
			this.Line82.Y2 = 6.4375F;
			// 
			// Line83
			// 
			this.Line83.Height = 0F;
			this.Line83.Left = 6.5F;
			this.Line83.LineWeight = 1F;
			this.Line83.Name = "Line83";
			this.Line83.Top = 6.5625F;
			this.Line83.Width = 2.625F;
			this.Line83.X1 = 6.5F;
			this.Line83.X2 = 9.125F;
			this.Line83.Y1 = 6.5625F;
			this.Line83.Y2 = 6.5625F;
			// 
			// Line84
			// 
			this.Line84.Height = 0F;
			this.Line84.Left = 6.5F;
			this.Line84.LineWeight = 1F;
			this.Line84.Name = "Line84";
			this.Line84.Top = 6.6875F;
			this.Line84.Width = 2.625F;
			this.Line84.X1 = 6.5F;
			this.Line84.X2 = 9.125F;
			this.Line84.Y1 = 6.6875F;
			this.Line84.Y2 = 6.6875F;
			// 
			// Line85
			// 
			this.Line85.Height = 0F;
			this.Line85.Left = 6.5F;
			this.Line85.LineWeight = 1F;
			this.Line85.Name = "Line85";
			this.Line85.Top = 6.8125F;
			this.Line85.Width = 2.625F;
			this.Line85.X1 = 6.5F;
			this.Line85.X2 = 9.125F;
			this.Line85.Y1 = 6.8125F;
			this.Line85.Y2 = 6.8125F;
			// 
			// Line86
			// 
			this.Line86.Height = 0F;
			this.Line86.Left = 6.5F;
			this.Line86.LineWeight = 1F;
			this.Line86.Name = "Line86";
			this.Line86.Top = 6.9375F;
			this.Line86.Width = 2.625F;
			this.Line86.X1 = 6.5F;
			this.Line86.X2 = 9.125F;
			this.Line86.Y1 = 6.9375F;
			this.Line86.Y2 = 6.9375F;
			// 
			// Line87
			// 
			this.Line87.Height = 0F;
			this.Line87.Left = 6.5F;
			this.Line87.LineWeight = 1F;
			this.Line87.Name = "Line87";
			this.Line87.Top = 7.0625F;
			this.Line87.Width = 2.625F;
			this.Line87.X1 = 6.5F;
			this.Line87.X2 = 9.125F;
			this.Line87.Y1 = 7.0625F;
			this.Line87.Y2 = 7.0625F;
			// 
			// Line88
			// 
			this.Line88.Height = 0F;
			this.Line88.Left = 6.5F;
			this.Line88.LineWeight = 1F;
			this.Line88.Name = "Line88";
			this.Line88.Top = 6.1875F;
			this.Line88.Width = 2.625F;
			this.Line88.X1 = 6.5F;
			this.Line88.X2 = 9.125F;
			this.Line88.Y1 = 6.1875F;
			this.Line88.Y2 = 6.1875F;
			// 
			// txtLandDesc16
			// 
			this.txtLandDesc16.Height = 0.125F;
			this.txtLandDesc16.Left = 5.5625F;
			this.txtLandDesc16.MultiLine = false;
			this.txtLandDesc16.Name = "txtLandDesc16";
			this.txtLandDesc16.Style = "font-size: 6pt";
			this.txtLandDesc16.Text = "16.";
			this.txtLandDesc16.Top = 5.1875F;
			this.txtLandDesc16.Width = 0.875F;
			// 
			// txtLandDesc17
			// 
			this.txtLandDesc17.Height = 0.125F;
			this.txtLandDesc17.Left = 5.5625F;
			this.txtLandDesc17.Name = "txtLandDesc17";
			this.txtLandDesc17.Style = "font-size: 6pt";
			this.txtLandDesc17.Text = "17.";
			this.txtLandDesc17.Top = 5.3125F;
			this.txtLandDesc17.Width = 0.875F;
			// 
			// txtLandDesc18
			// 
			this.txtLandDesc18.Height = 0.125F;
			this.txtLandDesc18.Left = 5.5625F;
			this.txtLandDesc18.Name = "txtLandDesc18";
			this.txtLandDesc18.Style = "font-size: 6pt";
			this.txtLandDesc18.Text = "18.";
			this.txtLandDesc18.Top = 5.4375F;
			this.txtLandDesc18.Width = 0.875F;
			// 
			// txtLandDesc19
			// 
			this.txtLandDesc19.Height = 0.125F;
			this.txtLandDesc19.Left = 5.5625F;
			this.txtLandDesc19.Name = "txtLandDesc19";
			this.txtLandDesc19.Style = "font-size: 6pt";
			this.txtLandDesc19.Text = "19.";
			this.txtLandDesc19.Top = 5.5625F;
			this.txtLandDesc19.Width = 0.875F;
			// 
			// txtLandDesc20
			// 
			this.txtLandDesc20.Height = 0.125F;
			this.txtLandDesc20.Left = 5.5625F;
			this.txtLandDesc20.Name = "txtLandDesc20";
			this.txtLandDesc20.Style = "font-size: 6pt";
			this.txtLandDesc20.Text = "20.";
			this.txtLandDesc20.Top = 5.6875F;
			this.txtLandDesc20.Width = 0.875F;
			// 
			// Label62
			// 
			this.Label62.Height = 0.1875F;
			this.Label62.HyperLink = null;
			this.Label62.Left = 5.5625F;
			this.Label62.Name = "Label62";
			this.Label62.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label62.Text = "Square Foot";
			this.Label62.Top = 5F;
			this.Label62.Width = 0.875F;
			// 
			// Line72
			// 
			this.Line72.Height = 0F;
			this.Line72.Left = 5.5F;
			this.Line72.LineWeight = 1F;
			this.Line72.Name = "Line72";
			this.Line72.Top = 5F;
			this.Line72.Width = 3.625F;
			this.Line72.X1 = 5.5F;
			this.Line72.X2 = 9.125F;
			this.Line72.Y1 = 5F;
			this.Line72.Y2 = 5F;
			// 
			// txtInfluenceCost1
			// 
			this.txtInfluenceCost1.Height = 0.125F;
			this.txtInfluenceCost1.Left = 9.125F;
			this.txtInfluenceCost1.Name = "txtInfluenceCost1";
			this.txtInfluenceCost1.Style = "font-size: 6pt";
			this.txtInfluenceCost1.Text = "1.";
			this.txtInfluenceCost1.Top = 4.125F;
			this.txtInfluenceCost1.Width = 0.8743055F;
			// 
			// txtInfluenceCost2
			// 
			this.txtInfluenceCost2.Height = 0.125F;
			this.txtInfluenceCost2.Left = 9.125F;
			this.txtInfluenceCost2.Name = "txtInfluenceCost2";
			this.txtInfluenceCost2.Style = "font-size: 6pt";
			this.txtInfluenceCost2.Text = "2.";
			this.txtInfluenceCost2.Top = 4.25F;
			this.txtInfluenceCost2.Width = 0.8743055F;
			// 
			// txtInfluenceCost3
			// 
			this.txtInfluenceCost3.Height = 0.125F;
			this.txtInfluenceCost3.Left = 9.125F;
			this.txtInfluenceCost3.Name = "txtInfluenceCost3";
			this.txtInfluenceCost3.Style = "font-size: 6pt";
			this.txtInfluenceCost3.Text = "3.";
			this.txtInfluenceCost3.Top = 4.375F;
			this.txtInfluenceCost3.Width = 0.8743055F;
			// 
			// txtInfluenceCost4
			// 
			this.txtInfluenceCost4.Height = 0.125F;
			this.txtInfluenceCost4.Left = 9.125F;
			this.txtInfluenceCost4.Name = "txtInfluenceCost4";
			this.txtInfluenceCost4.Style = "font-size: 6pt";
			this.txtInfluenceCost4.Text = "4.";
			this.txtInfluenceCost4.Top = 4.5F;
			this.txtInfluenceCost4.Width = 0.8743055F;
			// 
			// txtInfluenceCost5
			// 
			this.txtInfluenceCost5.Height = 0.125F;
			this.txtInfluenceCost5.Left = 9.125F;
			this.txtInfluenceCost5.Name = "txtInfluenceCost5";
			this.txtInfluenceCost5.Style = "font-size: 6pt";
			this.txtInfluenceCost5.Text = "5.";
			this.txtInfluenceCost5.Top = 4.625F;
			this.txtInfluenceCost5.Width = 0.8743055F;
			// 
			// txtInfluenceCost6
			// 
			this.txtInfluenceCost6.Height = 0.125F;
			this.txtInfluenceCost6.Left = 9.125F;
			this.txtInfluenceCost6.Name = "txtInfluenceCost6";
			this.txtInfluenceCost6.Style = "font-size: 6pt";
			this.txtInfluenceCost6.Text = "6.";
			this.txtInfluenceCost6.Top = 4.75F;
			this.txtInfluenceCost6.Width = 0.8743055F;
			// 
			// txtInfluenceCost7
			// 
			this.txtInfluenceCost7.Height = 0.125F;
			this.txtInfluenceCost7.Left = 9.125F;
			this.txtInfluenceCost7.Name = "txtInfluenceCost7";
			this.txtInfluenceCost7.Style = "font-size: 6pt";
			this.txtInfluenceCost7.Text = "7.";
			this.txtInfluenceCost7.Top = 4.875F;
			this.txtInfluenceCost7.Width = 0.8743055F;
			// 
			// txtInfluenceCost8
			// 
			this.txtInfluenceCost8.Height = 0.125F;
			this.txtInfluenceCost8.Left = 9.125F;
			this.txtInfluenceCost8.Name = "txtInfluenceCost8";
			this.txtInfluenceCost8.Style = "font-size: 6pt";
			this.txtInfluenceCost8.Text = "8.";
			this.txtInfluenceCost8.Top = 5F;
			this.txtInfluenceCost8.Width = 0.8743055F;
			// 
			// txtInfluenceCost9
			// 
			this.txtInfluenceCost9.Height = 0.125F;
			this.txtInfluenceCost9.Left = 9.125F;
			this.txtInfluenceCost9.Name = "txtInfluenceCost9";
			this.txtInfluenceCost9.Style = "font-size: 6pt";
			this.txtInfluenceCost9.Text = "9.";
			this.txtInfluenceCost9.Top = 5.125F;
			this.txtInfluenceCost9.Width = 0.8743055F;
			// 
			// txtLandDesc21
			// 
			this.txtLandDesc21.Height = 0.125F;
			this.txtLandDesc21.Left = 5.5625F;
			this.txtLandDesc21.Name = "txtLandDesc21";
			this.txtLandDesc21.Style = "font-size: 6pt";
			this.txtLandDesc21.Text = "21.";
			this.txtLandDesc21.Top = 6.15625F;
			this.txtLandDesc21.Width = 0.875F;
			// 
			// txtLandDesc22
			// 
			this.txtLandDesc22.Height = 0.125F;
			this.txtLandDesc22.Left = 5.5625F;
			this.txtLandDesc22.Name = "txtLandDesc22";
			this.txtLandDesc22.Style = "font-size: 6pt";
			this.txtLandDesc22.Text = "22.";
			this.txtLandDesc22.Top = 6.28125F;
			this.txtLandDesc22.Width = 0.875F;
			// 
			// txtLandDesc23
			// 
			this.txtLandDesc23.Height = 0.125F;
			this.txtLandDesc23.Left = 5.5625F;
			this.txtLandDesc23.Name = "txtLandDesc23";
			this.txtLandDesc23.Style = "font-size: 6pt";
			this.txtLandDesc23.Text = "23.";
			this.txtLandDesc23.Top = 6.40625F;
			this.txtLandDesc23.Width = 0.875F;
			// 
			// Label63
			// 
			this.Label63.Height = 0.125F;
			this.Label63.HyperLink = null;
			this.Label63.Left = 5.5625F;
			this.Label63.Name = "Label63";
			this.Label63.Style = "font-size: 6pt; font-weight: bold; text-align: center";
			this.Label63.Text = "Fract. Acre";
			this.Label63.Top = 6.03125F;
			this.Label63.Width = 0.875F;
			// 
			// Line80
			// 
			this.Line80.Height = 0F;
			this.Line80.Left = 5.5F;
			this.Line80.LineWeight = 1F;
			this.Line80.Name = "Line80";
			this.Line80.Top = 6.03125F;
			this.Line80.Width = 3.625F;
			this.Line80.X1 = 5.5F;
			this.Line80.X2 = 9.125F;
			this.Line80.Y1 = 6.03125F;
			this.Line80.Y2 = 6.03125F;
			// 
			// Label64
			// 
			this.Label64.Height = 0.125F;
			this.Label64.HyperLink = null;
			this.Label64.Left = 5.5625F;
			this.Label64.Name = "Label64";
			this.Label64.Style = "font-size: 6pt; font-weight: bold; text-align: center";
			this.Label64.Text = "Acres";
			this.Label64.Top = 6.53125F;
			this.Label64.Width = 0.875F;
			// 
			// txtLandDesc24
			// 
			this.txtLandDesc24.Height = 0.125F;
			this.txtLandDesc24.Left = 5.5625F;
			this.txtLandDesc24.Name = "txtLandDesc24";
			this.txtLandDesc24.Style = "font-size: 6pt";
			this.txtLandDesc24.Text = "24.";
			this.txtLandDesc24.Top = 6.65625F;
			this.txtLandDesc24.Width = 0.875F;
			// 
			// txtLandDesc25
			// 
			this.txtLandDesc25.Height = 0.125F;
			this.txtLandDesc25.Left = 5.5625F;
			this.txtLandDesc25.Name = "txtLandDesc25";
			this.txtLandDesc25.Style = "font-size: 6pt";
			this.txtLandDesc25.Text = "25.";
			this.txtLandDesc25.Top = 6.78125F;
			this.txtLandDesc25.Width = 0.875F;
			// 
			// txtLandDesc26
			// 
			this.txtLandDesc26.Height = 0.125F;
			this.txtLandDesc26.Left = 5.5625F;
			this.txtLandDesc26.Name = "txtLandDesc26";
			this.txtLandDesc26.Style = "font-size: 6pt";
			this.txtLandDesc26.Text = "26.";
			this.txtLandDesc26.Top = 6.90625F;
			this.txtLandDesc26.Width = 0.875F;
			// 
			// txtLandDesc27
			// 
			this.txtLandDesc27.Height = 0.125F;
			this.txtLandDesc27.Left = 5.5625F;
			this.txtLandDesc27.Name = "txtLandDesc27";
			this.txtLandDesc27.Style = "font-size: 6pt";
			this.txtLandDesc27.Text = "27.";
			this.txtLandDesc27.Top = 7.03125F;
			this.txtLandDesc27.Width = 0.875F;
			// 
			// txtLandDesc28
			// 
			this.txtLandDesc28.Height = 0.125F;
			this.txtLandDesc28.Left = 5.5625F;
			this.txtLandDesc28.Name = "txtLandDesc28";
			this.txtLandDesc28.Style = "font-size: 6pt";
			this.txtLandDesc28.Text = "28.";
			this.txtLandDesc28.Top = 7.15625F;
			this.txtLandDesc28.Width = 0.875F;
			// 
			// txtLandDesc29
			// 
			this.txtLandDesc29.Height = 0.125F;
			this.txtLandDesc29.Left = 5.5625F;
			this.txtLandDesc29.Name = "txtLandDesc29";
			this.txtLandDesc29.Style = "font-size: 6pt";
			this.txtLandDesc29.Text = "29.";
			this.txtLandDesc29.Top = 7.28125F;
			this.txtLandDesc29.Width = 0.875F;
			// 
			// Label65
			// 
			this.Label65.Height = 0.125F;
			this.Label65.HyperLink = null;
			this.Label65.Left = 9.125F;
			this.Label65.Name = "Label65";
			this.Label65.Style = "font-size: 6pt; font-weight: bold; text-align: center";
			this.Label65.Text = "Acres";
			this.Label65.Top = 5.25F;
			this.Label65.Width = 0.875F;
			// 
			// txtLandDesc30
			// 
			this.txtLandDesc30.Height = 0.125F;
			this.txtLandDesc30.Left = 9.125F;
			this.txtLandDesc30.Name = "txtLandDesc30";
			this.txtLandDesc30.Style = "font-size: 6pt";
			this.txtLandDesc30.Text = "30.";
			this.txtLandDesc30.Top = 5.375F;
			this.txtLandDesc30.Width = 0.875F;
			// 
			// txtLandDesc31
			// 
			this.txtLandDesc31.Height = 0.125F;
			this.txtLandDesc31.Left = 9.125F;
			this.txtLandDesc31.Name = "txtLandDesc31";
			this.txtLandDesc31.Style = "font-size: 6pt";
			this.txtLandDesc31.Text = "31.";
			this.txtLandDesc31.Top = 5.5F;
			this.txtLandDesc31.Width = 0.875F;
			// 
			// txtLandDesc32
			// 
			this.txtLandDesc32.Height = 0.125F;
			this.txtLandDesc32.Left = 9.125F;
			this.txtLandDesc32.Name = "txtLandDesc32";
			this.txtLandDesc32.Style = "font-size: 6pt";
			this.txtLandDesc32.Text = "32.";
			this.txtLandDesc32.Top = 5.625F;
			this.txtLandDesc32.Width = 0.875F;
			// 
			// txtLandDesc33
			// 
			this.txtLandDesc33.Height = 0.125F;
			this.txtLandDesc33.Left = 9.125F;
			this.txtLandDesc33.Name = "txtLandDesc33";
			this.txtLandDesc33.Style = "font-size: 6pt";
			this.txtLandDesc33.Text = "33.";
			this.txtLandDesc33.Top = 5.75F;
			this.txtLandDesc33.Width = 0.875F;
			// 
			// txtLandDesc34
			// 
			this.txtLandDesc34.Height = 0.125F;
			this.txtLandDesc34.Left = 9.125F;
			this.txtLandDesc34.Name = "txtLandDesc34";
			this.txtLandDesc34.Style = "font-size: 6pt";
			this.txtLandDesc34.Text = "34.";
			this.txtLandDesc34.Top = 5.875F;
			this.txtLandDesc34.Width = 0.875F;
			// 
			// txtLandDesc35
			// 
			this.txtLandDesc35.Height = 0.125F;
			this.txtLandDesc35.Left = 9.125F;
			this.txtLandDesc35.Name = "txtLandDesc35";
			this.txtLandDesc35.Style = "font-size: 6pt";
			this.txtLandDesc35.Text = "35.";
			this.txtLandDesc35.Top = 6F;
			this.txtLandDesc35.Width = 0.875F;
			// 
			// txtLandDesc36
			// 
			this.txtLandDesc36.Height = 0.125F;
			this.txtLandDesc36.Left = 9.125F;
			this.txtLandDesc36.Name = "txtLandDesc36";
			this.txtLandDesc36.Style = "font-size: 6pt";
			this.txtLandDesc36.Text = "36.";
			this.txtLandDesc36.Top = 6.125F;
			this.txtLandDesc36.Width = 0.875F;
			// 
			// txtLandDesc37
			// 
			this.txtLandDesc37.Height = 0.125F;
			this.txtLandDesc37.Left = 9.125F;
			this.txtLandDesc37.Name = "txtLandDesc37";
			this.txtLandDesc37.Style = "font-size: 6pt";
			this.txtLandDesc37.Text = "37.";
			this.txtLandDesc37.Top = 6.25F;
			this.txtLandDesc37.Width = 0.875F;
			// 
			// txtLandDesc38
			// 
			this.txtLandDesc38.Height = 0.125F;
			this.txtLandDesc38.Left = 9.125F;
			this.txtLandDesc38.Name = "txtLandDesc38";
			this.txtLandDesc38.Style = "font-size: 6pt";
			this.txtLandDesc38.Text = "38.";
			this.txtLandDesc38.Top = 6.375F;
			this.txtLandDesc38.Width = 0.875F;
			// 
			// txtLandDesc39
			// 
			this.txtLandDesc39.Height = 0.125F;
			this.txtLandDesc39.Left = 9.125F;
			this.txtLandDesc39.Name = "txtLandDesc39";
			this.txtLandDesc39.Style = "font-size: 6pt";
			this.txtLandDesc39.Text = "39.";
			this.txtLandDesc39.Top = 6.5F;
			this.txtLandDesc39.Width = 0.875F;
			// 
			// txtLandDesc40
			// 
			this.txtLandDesc40.Height = 0.125F;
			this.txtLandDesc40.Left = 9.125F;
			this.txtLandDesc40.Name = "txtLandDesc40";
			this.txtLandDesc40.Style = "font-size: 6pt";
			this.txtLandDesc40.Text = "40.";
			this.txtLandDesc40.Top = 6.625F;
			this.txtLandDesc40.Width = 0.875F;
			// 
			// txtLandDesc41
			// 
			this.txtLandDesc41.Height = 0.125F;
			this.txtLandDesc41.Left = 9.125F;
			this.txtLandDesc41.Name = "txtLandDesc41";
			this.txtLandDesc41.Style = "font-size: 6pt";
			this.txtLandDesc41.Text = "41.";
			this.txtLandDesc41.Top = 6.75F;
			this.txtLandDesc41.Width = 0.875F;
			// 
			// txtLandDesc42
			// 
			this.txtLandDesc42.Height = 0.125F;
			this.txtLandDesc42.Left = 9.125F;
			this.txtLandDesc42.Name = "txtLandDesc42";
			this.txtLandDesc42.Style = "font-size: 6pt";
			this.txtLandDesc42.Text = "42.";
			this.txtLandDesc42.Top = 6.875F;
			this.txtLandDesc42.Width = 0.875F;
			// 
			// txtLandDesc43
			// 
			this.txtLandDesc43.Height = 0.125F;
			this.txtLandDesc43.Left = 9.125F;
			this.txtLandDesc43.Name = "txtLandDesc43";
			this.txtLandDesc43.Style = "font-size: 6pt";
			this.txtLandDesc43.Text = "43.";
			this.txtLandDesc43.Top = 7F;
			this.txtLandDesc43.Width = 0.875F;
			// 
			// txtLandDesc44
			// 
			this.txtLandDesc44.Height = 0.125F;
			this.txtLandDesc44.Left = 9.125F;
			this.txtLandDesc44.Name = "txtLandDesc44";
			this.txtLandDesc44.Style = "font-size: 6pt";
			this.txtLandDesc44.Text = "44.Lot Improvement";
			this.txtLandDesc44.Top = 7.125F;
			this.txtLandDesc44.Width = 0.875F;
			// 
			// Line59
			// 
			this.Line59.Height = 3.53125F;
			this.Line59.Left = 9.125F;
			this.Line59.LineWeight = 1F;
			this.Line59.Name = "Line59";
			this.Line59.Top = 3.84375F;
			this.Line59.Width = 0F;
			this.Line59.X1 = 9.125F;
			this.Line59.X2 = 9.125F;
			this.Line59.Y1 = 3.84375F;
			this.Line59.Y2 = 7.375F;
			// 
			// lblMuni
			// 
			this.lblMuni.Height = 0.21875F;
			this.lblMuni.HyperLink = null;
			this.lblMuni.Left = 0F;
			this.lblMuni.Name = "lblMuni";
			this.lblMuni.Style = "font-size: 12pt; font-weight: bold; text-align: left";
			this.lblMuni.Text = "Trioville, Maine";
			this.lblMuni.Top = 7.21875F;
			this.lblMuni.Width = 3.4375F;
			// 
			// Label67
			// 
			this.Label67.Height = 0.19F;
			this.Label67.HyperLink = null;
			this.Label67.Left = 0F;
			this.Label67.Name = "Label67";
			this.Label67.Style = "font-size: 8.5pt";
			this.Label67.Text = "No./Date";
			this.Label67.Top = 4.8125F;
			this.Label67.Width = 0.5625F;
			// 
			// Label68
			// 
			this.Label68.Height = 0.19F;
			this.Label68.HyperLink = null;
			this.Label68.Left = 0.6875F;
			this.Label68.Name = "Label68";
			this.Label68.Style = "font-size: 8.5pt";
			this.Label68.Text = "Description";
			this.Label68.Top = 4.8125F;
			this.Label68.Width = 1.0625F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.19F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 2.875F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-size: 8.5pt";
			this.Label69.Text = "Date Insp.";
			this.Label69.Top = 4.8125F;
			this.Label69.Width = 0.625F;
			// 
			// Line89
			// 
			this.Line89.Height = 0.875F;
			this.Line89.Left = 0.625F;
			this.Line89.LineWeight = 1F;
			this.Line89.Name = "Line89";
			this.Line89.Top = 4.78125F;
			this.Line89.Width = 0F;
			this.Line89.X1 = 0.625F;
			this.Line89.X2 = 0.625F;
			this.Line89.Y1 = 4.78125F;
			this.Line89.Y2 = 5.65625F;
			// 
			// Line90
			// 
			this.Line90.Height = 0F;
			this.Line90.Left = 0F;
			this.Line90.LineWeight = 1F;
			this.Line90.Name = "Line90";
			this.Line90.Top = 5F;
			this.Line90.Width = 3.5F;
			this.Line90.X1 = 0F;
			this.Line90.X2 = 3.5F;
			this.Line90.Y1 = 5F;
			this.Line90.Y2 = 5F;
			// 
			// Line91
			// 
			this.Line91.Height = 0F;
			this.Line91.Left = 0F;
			this.Line91.LineWeight = 1F;
			this.Line91.Name = "Line91";
			this.Line91.Top = 5.21875F;
			this.Line91.Width = 3.5F;
			this.Line91.X1 = 0F;
			this.Line91.X2 = 3.5F;
			this.Line91.Y1 = 5.21875F;
			this.Line91.Y2 = 5.21875F;
			// 
			// Line92
			// 
			this.Line92.Height = 0F;
			this.Line92.Left = 0F;
			this.Line92.LineWeight = 1F;
			this.Line92.Name = "Line92";
			this.Line92.Top = 5.4375F;
			this.Line92.Width = 3.5F;
			this.Line92.X1 = 0F;
			this.Line92.X2 = 3.5F;
			this.Line92.Y1 = 5.4375F;
			this.Line92.Y2 = 5.4375F;
			// 
			// Line93
			// 
			this.Line93.Height = 0F;
			this.Line93.Left = 0F;
			this.Line93.LineWeight = 1F;
			this.Line93.Name = "Line93";
			this.Line93.Top = 5.65625F;
			this.Line93.Width = 3.5F;
			this.Line93.X1 = 0F;
			this.Line93.X2 = 3.5F;
			this.Line93.Y1 = 5.65625F;
			this.Line93.Y2 = 5.65625F;
			// 
			// Line94
			// 
			this.Line94.Height = 0.875F;
			this.Line94.Left = 2.8125F;
			this.Line94.LineWeight = 1F;
			this.Line94.Name = "Line94";
			this.Line94.Top = 4.78125F;
			this.Line94.Width = 0F;
			this.Line94.X1 = 2.8125F;
			this.Line94.X2 = 2.8125F;
			this.Line94.Y1 = 4.78125F;
			this.Line94.Y2 = 5.65625F;
			// 
			// txtAssYear1
			// 
			this.txtAssYear1.Height = 0.19F;
			this.txtAssYear1.Left = 5.5625F;
			this.txtAssYear1.Name = "txtAssYear1";
			this.txtAssYear1.Style = "font-size: 8.5pt";
			this.txtAssYear1.Text = null;
			this.txtAssYear1.Top = 0.625F;
			this.txtAssYear1.Width = 0.4375F;
			// 
			// txtAssYear2
			// 
			this.txtAssYear2.Height = 0.19F;
			this.txtAssYear2.Left = 5.5625F;
			this.txtAssYear2.Name = "txtAssYear2";
			this.txtAssYear2.Style = "font-size: 8.5pt";
			this.txtAssYear2.Text = null;
			this.txtAssYear2.Top = 0.84375F;
			this.txtAssYear2.Width = 0.4375F;
			// 
			// txtAssYear3
			// 
			this.txtAssYear3.Height = 0.19F;
			this.txtAssYear3.Left = 5.5625F;
			this.txtAssYear3.Name = "txtAssYear3";
			this.txtAssYear3.Style = "font-size: 8.5pt";
			this.txtAssYear3.Text = null;
			this.txtAssYear3.Top = 1.0625F;
			this.txtAssYear3.Width = 0.4375F;
			// 
			// txtAssYear4
			// 
			this.txtAssYear4.Height = 0.19F;
			this.txtAssYear4.Left = 5.5625F;
			this.txtAssYear4.Name = "txtAssYear4";
			this.txtAssYear4.Style = "font-size: 8.5pt";
			this.txtAssYear4.Text = null;
			this.txtAssYear4.Top = 1.28125F;
			this.txtAssYear4.Width = 0.4375F;
			// 
			// txtAssYear5
			// 
			this.txtAssYear5.Height = 0.19F;
			this.txtAssYear5.Left = 5.5625F;
			this.txtAssYear5.Name = "txtAssYear5";
			this.txtAssYear5.Style = "font-size: 8.5pt";
			this.txtAssYear5.Text = null;
			this.txtAssYear5.Top = 1.5F;
			this.txtAssYear5.Width = 0.4375F;
			// 
			// txtAssYear7
			// 
			this.txtAssYear7.Height = 0.19F;
			this.txtAssYear7.Left = 5.5625F;
			this.txtAssYear7.Name = "txtAssYear7";
			this.txtAssYear7.Style = "font-size: 8.5pt";
			this.txtAssYear7.Text = null;
			this.txtAssYear7.Top = 1.9375F;
			this.txtAssYear7.Width = 0.4375F;
			// 
			// txtAssYear8
			// 
			this.txtAssYear8.Height = 0.19F;
			this.txtAssYear8.Left = 5.5625F;
			this.txtAssYear8.Name = "txtAssYear8";
			this.txtAssYear8.Style = "font-size: 8.5pt";
			this.txtAssYear8.Text = null;
			this.txtAssYear8.Top = 2.15625F;
			this.txtAssYear8.Width = 0.4375F;
			// 
			// txtAssYear9
			// 
			this.txtAssYear9.Height = 0.19F;
			this.txtAssYear9.Left = 5.5625F;
			this.txtAssYear9.Name = "txtAssYear9";
			this.txtAssYear9.Style = "font-size: 8.5pt";
			this.txtAssYear9.Text = null;
			this.txtAssYear9.Top = 2.375F;
			this.txtAssYear9.Width = 0.4375F;
			// 
			// txtAssYear10
			// 
			this.txtAssYear10.Height = 0.19F;
			this.txtAssYear10.Left = 5.5625F;
			this.txtAssYear10.Name = "txtAssYear10";
			this.txtAssYear10.Style = "font-size: 8.5pt";
			this.txtAssYear10.Text = null;
			this.txtAssYear10.Top = 2.59375F;
			this.txtAssYear10.Width = 0.4375F;
			// 
			// txtAssYear11
			// 
			this.txtAssYear11.Height = 0.19F;
			this.txtAssYear11.Left = 5.5625F;
			this.txtAssYear11.Name = "txtAssYear11";
			this.txtAssYear11.Style = "font-size: 8.5pt";
			this.txtAssYear11.Text = null;
			this.txtAssYear11.Top = 2.8125F;
			this.txtAssYear11.Width = 0.4375F;
			// 
			// txtAssYear12
			// 
			this.txtAssYear12.Height = 0.19F;
			this.txtAssYear12.Left = 5.5625F;
			this.txtAssYear12.Name = "txtAssYear12";
			this.txtAssYear12.Style = "font-size: 8.5pt";
			this.txtAssYear12.Text = null;
			this.txtAssYear12.Top = 3.03125F;
			this.txtAssYear12.Width = 0.4375F;
			// 
			// txtAssYear13
			// 
			this.txtAssYear13.Height = 0.19F;
			this.txtAssYear13.Left = 5.5625F;
			this.txtAssYear13.Name = "txtAssYear13";
			this.txtAssYear13.Style = "font-size: 8.5pt";
			this.txtAssYear13.Text = null;
			this.txtAssYear13.Top = 3.25F;
			this.txtAssYear13.Width = 0.4375F;
			// 
			// txtAssYear14
			// 
			this.txtAssYear14.Height = 0.19F;
			this.txtAssYear14.Left = 5.5625F;
			this.txtAssYear14.Name = "txtAssYear14";
			this.txtAssYear14.Style = "font-size: 8.5pt";
			this.txtAssYear14.Text = null;
			this.txtAssYear14.Top = 3.46875F;
			this.txtAssYear14.Width = 0.4375F;
			// 
			// txtAssYear6
			// 
			this.txtAssYear6.Height = 0.19F;
			this.txtAssYear6.Left = 5.5625F;
			this.txtAssYear6.Name = "txtAssYear6";
			this.txtAssYear6.Style = "font-size: 8.5pt";
			this.txtAssYear6.Text = null;
			this.txtAssYear6.Top = 1.71875F;
			this.txtAssYear6.Width = 0.4375F;
			// 
			// txtAssLand1
			// 
			this.txtAssLand1.Height = 0.19F;
			this.txtAssLand1.Left = 6.125F;
			this.txtAssLand1.Name = "txtAssLand1";
			this.txtAssLand1.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand1.Text = null;
			this.txtAssLand1.Top = 0.625F;
			this.txtAssLand1.Width = 1.125F;
			// 
			// txtAssLand2
			// 
			this.txtAssLand2.Height = 0.19F;
			this.txtAssLand2.Left = 6.125F;
			this.txtAssLand2.Name = "txtAssLand2";
			this.txtAssLand2.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand2.Text = null;
			this.txtAssLand2.Top = 0.84375F;
			this.txtAssLand2.Width = 1.125F;
			// 
			// txtAssLand3
			// 
			this.txtAssLand3.Height = 0.19F;
			this.txtAssLand3.Left = 6.125F;
			this.txtAssLand3.Name = "txtAssLand3";
			this.txtAssLand3.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand3.Text = null;
			this.txtAssLand3.Top = 1.0625F;
			this.txtAssLand3.Width = 1.125F;
			// 
			// txtAssLand4
			// 
			this.txtAssLand4.Height = 0.19F;
			this.txtAssLand4.Left = 6.125F;
			this.txtAssLand4.Name = "txtAssLand4";
			this.txtAssLand4.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand4.Text = null;
			this.txtAssLand4.Top = 1.28125F;
			this.txtAssLand4.Width = 1.125F;
			// 
			// txtAssLand5
			// 
			this.txtAssLand5.Height = 0.19F;
			this.txtAssLand5.Left = 6.125F;
			this.txtAssLand5.Name = "txtAssLand5";
			this.txtAssLand5.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand5.Text = null;
			this.txtAssLand5.Top = 1.5F;
			this.txtAssLand5.Width = 1.125F;
			// 
			// txtAssLand6
			// 
			this.txtAssLand6.Height = 0.19F;
			this.txtAssLand6.Left = 6.125F;
			this.txtAssLand6.Name = "txtAssLand6";
			this.txtAssLand6.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand6.Text = null;
			this.txtAssLand6.Top = 1.71875F;
			this.txtAssLand6.Width = 1.125F;
			// 
			// txtAssLand7
			// 
			this.txtAssLand7.Height = 0.19F;
			this.txtAssLand7.Left = 6.125F;
			this.txtAssLand7.Name = "txtAssLand7";
			this.txtAssLand7.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand7.Text = null;
			this.txtAssLand7.Top = 1.9375F;
			this.txtAssLand7.Width = 1.125F;
			// 
			// txtAssLand8
			// 
			this.txtAssLand8.Height = 0.19F;
			this.txtAssLand8.Left = 6.125F;
			this.txtAssLand8.Name = "txtAssLand8";
			this.txtAssLand8.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand8.Text = null;
			this.txtAssLand8.Top = 2.15625F;
			this.txtAssLand8.Width = 1.125F;
			// 
			// txtAssLand9
			// 
			this.txtAssLand9.Height = 0.19F;
			this.txtAssLand9.Left = 6.125F;
			this.txtAssLand9.Name = "txtAssLand9";
			this.txtAssLand9.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand9.Text = null;
			this.txtAssLand9.Top = 2.375F;
			this.txtAssLand9.Width = 1.125F;
			// 
			// txtAssLand10
			// 
			this.txtAssLand10.Height = 0.19F;
			this.txtAssLand10.Left = 6.125F;
			this.txtAssLand10.Name = "txtAssLand10";
			this.txtAssLand10.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand10.Text = null;
			this.txtAssLand10.Top = 2.59375F;
			this.txtAssLand10.Width = 1.125F;
			// 
			// txtAssLand11
			// 
			this.txtAssLand11.Height = 0.19F;
			this.txtAssLand11.Left = 6.125F;
			this.txtAssLand11.Name = "txtAssLand11";
			this.txtAssLand11.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand11.Text = null;
			this.txtAssLand11.Top = 2.8125F;
			this.txtAssLand11.Width = 1.125F;
			// 
			// txtAssLand12
			// 
			this.txtAssLand12.Height = 0.19F;
			this.txtAssLand12.Left = 6.125F;
			this.txtAssLand12.Name = "txtAssLand12";
			this.txtAssLand12.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand12.Text = null;
			this.txtAssLand12.Top = 3.03125F;
			this.txtAssLand12.Width = 1.125F;
			// 
			// txtAssLand13
			// 
			this.txtAssLand13.Height = 0.19F;
			this.txtAssLand13.Left = 6.125F;
			this.txtAssLand13.Name = "txtAssLand13";
			this.txtAssLand13.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand13.Text = null;
			this.txtAssLand13.Top = 3.25F;
			this.txtAssLand13.Width = 1.125F;
			// 
			// txtAssLand14
			// 
			this.txtAssLand14.Height = 0.19F;
			this.txtAssLand14.Left = 6.125F;
			this.txtAssLand14.Name = "txtAssLand14";
			this.txtAssLand14.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssLand14.Text = null;
			this.txtAssLand14.Top = 3.46875F;
			this.txtAssLand14.Width = 1.125F;
			// 
			// txtAssBldg1
			// 
			this.txtAssBldg1.Height = 0.19F;
			this.txtAssBldg1.Left = 7.375F;
			this.txtAssBldg1.Name = "txtAssBldg1";
			this.txtAssBldg1.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg1.Text = null;
			this.txtAssBldg1.Top = 0.625F;
			this.txtAssBldg1.Width = 1.0625F;
			// 
			// txtAssBldg2
			// 
			this.txtAssBldg2.Height = 0.19F;
			this.txtAssBldg2.Left = 7.375F;
			this.txtAssBldg2.Name = "txtAssBldg2";
			this.txtAssBldg2.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg2.Text = null;
			this.txtAssBldg2.Top = 0.84375F;
			this.txtAssBldg2.Width = 1.0625F;
			// 
			// txtAssBldg3
			// 
			this.txtAssBldg3.Height = 0.19F;
			this.txtAssBldg3.Left = 7.375F;
			this.txtAssBldg3.Name = "txtAssBldg3";
			this.txtAssBldg3.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg3.Text = null;
			this.txtAssBldg3.Top = 1.0625F;
			this.txtAssBldg3.Width = 1.0625F;
			// 
			// txtAssBldg4
			// 
			this.txtAssBldg4.Height = 0.19F;
			this.txtAssBldg4.Left = 7.375F;
			this.txtAssBldg4.Name = "txtAssBldg4";
			this.txtAssBldg4.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg4.Text = null;
			this.txtAssBldg4.Top = 1.28125F;
			this.txtAssBldg4.Width = 1.0625F;
			// 
			// txtAssBldg5
			// 
			this.txtAssBldg5.Height = 0.19F;
			this.txtAssBldg5.Left = 7.375F;
			this.txtAssBldg5.Name = "txtAssBldg5";
			this.txtAssBldg5.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg5.Text = null;
			this.txtAssBldg5.Top = 1.5F;
			this.txtAssBldg5.Width = 1.0625F;
			// 
			// txtAssBldg6
			// 
			this.txtAssBldg6.Height = 0.19F;
			this.txtAssBldg6.Left = 7.375F;
			this.txtAssBldg6.Name = "txtAssBldg6";
			this.txtAssBldg6.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg6.Text = null;
			this.txtAssBldg6.Top = 1.71875F;
			this.txtAssBldg6.Width = 1.0625F;
			// 
			// txtAssBldg7
			// 
			this.txtAssBldg7.Height = 0.19F;
			this.txtAssBldg7.Left = 7.375F;
			this.txtAssBldg7.Name = "txtAssBldg7";
			this.txtAssBldg7.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg7.Text = null;
			this.txtAssBldg7.Top = 1.9375F;
			this.txtAssBldg7.Width = 1.0625F;
			// 
			// txtAssBldg8
			// 
			this.txtAssBldg8.Height = 0.19F;
			this.txtAssBldg8.Left = 7.375F;
			this.txtAssBldg8.Name = "txtAssBldg8";
			this.txtAssBldg8.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg8.Text = null;
			this.txtAssBldg8.Top = 2.15625F;
			this.txtAssBldg8.Width = 1.0625F;
			// 
			// txtAssBldg9
			// 
			this.txtAssBldg9.Height = 0.19F;
			this.txtAssBldg9.Left = 7.375F;
			this.txtAssBldg9.Name = "txtAssBldg9";
			this.txtAssBldg9.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg9.Text = null;
			this.txtAssBldg9.Top = 2.375F;
			this.txtAssBldg9.Width = 1.0625F;
			// 
			// txtAssBldg10
			// 
			this.txtAssBldg10.Height = 0.19F;
			this.txtAssBldg10.Left = 7.375F;
			this.txtAssBldg10.Name = "txtAssBldg10";
			this.txtAssBldg10.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg10.Text = null;
			this.txtAssBldg10.Top = 2.59375F;
			this.txtAssBldg10.Width = 1.0625F;
			// 
			// txtAssBldg11
			// 
			this.txtAssBldg11.Height = 0.19F;
			this.txtAssBldg11.Left = 7.375F;
			this.txtAssBldg11.Name = "txtAssBldg11";
			this.txtAssBldg11.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg11.Text = null;
			this.txtAssBldg11.Top = 2.8125F;
			this.txtAssBldg11.Width = 1.0625F;
			// 
			// txtAssBldg12
			// 
			this.txtAssBldg12.Height = 0.19F;
			this.txtAssBldg12.Left = 7.375F;
			this.txtAssBldg12.Name = "txtAssBldg12";
			this.txtAssBldg12.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg12.Text = null;
			this.txtAssBldg12.Top = 3.03125F;
			this.txtAssBldg12.Width = 1.0625F;
			// 
			// txtAssBldg13
			// 
			this.txtAssBldg13.Height = 0.19F;
			this.txtAssBldg13.Left = 7.375F;
			this.txtAssBldg13.Name = "txtAssBldg13";
			this.txtAssBldg13.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg13.Text = null;
			this.txtAssBldg13.Top = 3.25F;
			this.txtAssBldg13.Width = 1.0625F;
			// 
			// txtAssBldg14
			// 
			this.txtAssBldg14.Height = 0.19F;
			this.txtAssBldg14.Left = 7.375F;
			this.txtAssBldg14.Name = "txtAssBldg14";
			this.txtAssBldg14.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssBldg14.Text = null;
			this.txtAssBldg14.Top = 3.46875F;
			this.txtAssBldg14.Width = 1.0625F;
			// 
			// txtAssExempt1
			// 
			this.txtAssExempt1.Height = 0.19F;
			this.txtAssExempt1.Left = 8.5F;
			this.txtAssExempt1.Name = "txtAssExempt1";
			this.txtAssExempt1.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt1.Text = null;
			this.txtAssExempt1.Top = 0.625F;
			this.txtAssExempt1.Width = 0.625F;
			// 
			// txtAssExempt2
			// 
			this.txtAssExempt2.Height = 0.19F;
			this.txtAssExempt2.Left = 8.5F;
			this.txtAssExempt2.Name = "txtAssExempt2";
			this.txtAssExempt2.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt2.Text = null;
			this.txtAssExempt2.Top = 0.84375F;
			this.txtAssExempt2.Width = 0.625F;
			// 
			// txtAssExempt3
			// 
			this.txtAssExempt3.Height = 0.19F;
			this.txtAssExempt3.Left = 8.5F;
			this.txtAssExempt3.Name = "txtAssExempt3";
			this.txtAssExempt3.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt3.Text = null;
			this.txtAssExempt3.Top = 1.0625F;
			this.txtAssExempt3.Width = 0.625F;
			// 
			// txtAssExempt4
			// 
			this.txtAssExempt4.Height = 0.19F;
			this.txtAssExempt4.Left = 8.5F;
			this.txtAssExempt4.Name = "txtAssExempt4";
			this.txtAssExempt4.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt4.Text = null;
			this.txtAssExempt4.Top = 1.28125F;
			this.txtAssExempt4.Width = 0.625F;
			// 
			// txtAssExempt5
			// 
			this.txtAssExempt5.Height = 0.19F;
			this.txtAssExempt5.Left = 8.5F;
			this.txtAssExempt5.Name = "txtAssExempt5";
			this.txtAssExempt5.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt5.Text = null;
			this.txtAssExempt5.Top = 1.5F;
			this.txtAssExempt5.Width = 0.625F;
			// 
			// txtAssExempt6
			// 
			this.txtAssExempt6.Height = 0.19F;
			this.txtAssExempt6.Left = 8.5F;
			this.txtAssExempt6.Name = "txtAssExempt6";
			this.txtAssExempt6.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt6.Text = null;
			this.txtAssExempt6.Top = 1.71875F;
			this.txtAssExempt6.Width = 0.625F;
			// 
			// txtAssExempt7
			// 
			this.txtAssExempt7.Height = 0.19F;
			this.txtAssExempt7.Left = 8.5F;
			this.txtAssExempt7.Name = "txtAssExempt7";
			this.txtAssExempt7.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt7.Text = null;
			this.txtAssExempt7.Top = 1.9375F;
			this.txtAssExempt7.Width = 0.625F;
			// 
			// txtAssExempt8
			// 
			this.txtAssExempt8.Height = 0.19F;
			this.txtAssExempt8.Left = 8.5F;
			this.txtAssExempt8.Name = "txtAssExempt8";
			this.txtAssExempt8.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt8.Text = null;
			this.txtAssExempt8.Top = 2.15625F;
			this.txtAssExempt8.Width = 0.625F;
			// 
			// txtAssExempt9
			// 
			this.txtAssExempt9.Height = 0.19F;
			this.txtAssExempt9.Left = 8.5F;
			this.txtAssExempt9.Name = "txtAssExempt9";
			this.txtAssExempt9.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt9.Text = null;
			this.txtAssExempt9.Top = 2.375F;
			this.txtAssExempt9.Width = 0.625F;
			// 
			// txtAssExempt10
			// 
			this.txtAssExempt10.Height = 0.19F;
			this.txtAssExempt10.Left = 8.5F;
			this.txtAssExempt10.Name = "txtAssExempt10";
			this.txtAssExempt10.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt10.Text = null;
			this.txtAssExempt10.Top = 2.59375F;
			this.txtAssExempt10.Width = 0.625F;
			// 
			// txtAssExempt11
			// 
			this.txtAssExempt11.Height = 0.19F;
			this.txtAssExempt11.Left = 8.5F;
			this.txtAssExempt11.Name = "txtAssExempt11";
			this.txtAssExempt11.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt11.Text = null;
			this.txtAssExempt11.Top = 2.8125F;
			this.txtAssExempt11.Width = 0.625F;
			// 
			// txtAssExempt12
			// 
			this.txtAssExempt12.Height = 0.19F;
			this.txtAssExempt12.Left = 8.5F;
			this.txtAssExempt12.Name = "txtAssExempt12";
			this.txtAssExempt12.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt12.Text = null;
			this.txtAssExempt12.Top = 3.03125F;
			this.txtAssExempt12.Width = 0.625F;
			// 
			// txtAssExempt13
			// 
			this.txtAssExempt13.Height = 0.19F;
			this.txtAssExempt13.Left = 8.5F;
			this.txtAssExempt13.Name = "txtAssExempt13";
			this.txtAssExempt13.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt13.Text = null;
			this.txtAssExempt13.Top = 3.25F;
			this.txtAssExempt13.Width = 0.625F;
			// 
			// txtAssExempt14
			// 
			this.txtAssExempt14.Height = 0.19F;
			this.txtAssExempt14.Left = 8.5F;
			this.txtAssExempt14.Name = "txtAssExempt14";
			this.txtAssExempt14.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssExempt14.Text = null;
			this.txtAssExempt14.Top = 3.46875F;
			this.txtAssExempt14.Width = 0.625F;
			// 
			// txtAssTotal1
			// 
			this.txtAssTotal1.Height = 0.19F;
			this.txtAssTotal1.Left = 9.1875F;
			this.txtAssTotal1.Name = "txtAssTotal1";
			this.txtAssTotal1.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal1.Text = null;
			this.txtAssTotal1.Top = 0.625F;
			this.txtAssTotal1.Width = 0.8118055F;
			// 
			// txtAssTotal2
			// 
			this.txtAssTotal2.Height = 0.19F;
			this.txtAssTotal2.Left = 9.1875F;
			this.txtAssTotal2.Name = "txtAssTotal2";
			this.txtAssTotal2.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal2.Text = null;
			this.txtAssTotal2.Top = 0.84375F;
			this.txtAssTotal2.Width = 0.8118055F;
			// 
			// txtAssTotal3
			// 
			this.txtAssTotal3.Height = 0.19F;
			this.txtAssTotal3.Left = 9.1875F;
			this.txtAssTotal3.Name = "txtAssTotal3";
			this.txtAssTotal3.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal3.Text = null;
			this.txtAssTotal3.Top = 1.0625F;
			this.txtAssTotal3.Width = 0.8118055F;
			// 
			// txtAssTotal4
			// 
			this.txtAssTotal4.Height = 0.19F;
			this.txtAssTotal4.Left = 9.1875F;
			this.txtAssTotal4.Name = "txtAssTotal4";
			this.txtAssTotal4.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal4.Text = null;
			this.txtAssTotal4.Top = 1.28125F;
			this.txtAssTotal4.Width = 0.8118055F;
			// 
			// txtAssTotal5
			// 
			this.txtAssTotal5.Height = 0.19F;
			this.txtAssTotal5.Left = 9.1875F;
			this.txtAssTotal5.Name = "txtAssTotal5";
			this.txtAssTotal5.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal5.Text = null;
			this.txtAssTotal5.Top = 1.5F;
			this.txtAssTotal5.Width = 0.8118055F;
			// 
			// txtAssTotal7
			// 
			this.txtAssTotal7.Height = 0.19F;
			this.txtAssTotal7.Left = 9.1875F;
			this.txtAssTotal7.Name = "txtAssTotal7";
			this.txtAssTotal7.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal7.Text = null;
			this.txtAssTotal7.Top = 1.9375F;
			this.txtAssTotal7.Width = 0.8118055F;
			// 
			// txtAssTotal8
			// 
			this.txtAssTotal8.Height = 0.19F;
			this.txtAssTotal8.Left = 9.1875F;
			this.txtAssTotal8.Name = "txtAssTotal8";
			this.txtAssTotal8.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal8.Text = null;
			this.txtAssTotal8.Top = 2.15625F;
			this.txtAssTotal8.Width = 0.8118055F;
			// 
			// txtAssTotal9
			// 
			this.txtAssTotal9.Height = 0.19F;
			this.txtAssTotal9.Left = 9.1875F;
			this.txtAssTotal9.Name = "txtAssTotal9";
			this.txtAssTotal9.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal9.Text = null;
			this.txtAssTotal9.Top = 2.375F;
			this.txtAssTotal9.Width = 0.8118055F;
			// 
			// txtAssTotal10
			// 
			this.txtAssTotal10.Height = 0.19F;
			this.txtAssTotal10.Left = 9.1875F;
			this.txtAssTotal10.Name = "txtAssTotal10";
			this.txtAssTotal10.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal10.Text = null;
			this.txtAssTotal10.Top = 2.59375F;
			this.txtAssTotal10.Width = 0.8118055F;
			// 
			// txtAssTotal11
			// 
			this.txtAssTotal11.Height = 0.19F;
			this.txtAssTotal11.Left = 9.1875F;
			this.txtAssTotal11.Name = "txtAssTotal11";
			this.txtAssTotal11.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal11.Text = null;
			this.txtAssTotal11.Top = 2.8125F;
			this.txtAssTotal11.Width = 0.8118055F;
			// 
			// txtAssTotal12
			// 
			this.txtAssTotal12.Height = 0.19F;
			this.txtAssTotal12.Left = 9.1875F;
			this.txtAssTotal12.Name = "txtAssTotal12";
			this.txtAssTotal12.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal12.Text = null;
			this.txtAssTotal12.Top = 3.03125F;
			this.txtAssTotal12.Width = 0.8118055F;
			// 
			// txtAssTotal13
			// 
			this.txtAssTotal13.Height = 0.19F;
			this.txtAssTotal13.Left = 9.1875F;
			this.txtAssTotal13.Name = "txtAssTotal13";
			this.txtAssTotal13.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal13.Text = null;
			this.txtAssTotal13.Top = 3.25F;
			this.txtAssTotal13.Width = 0.8118055F;
			// 
			// txtAssTotal14
			// 
			this.txtAssTotal14.Height = 0.19F;
			this.txtAssTotal14.Left = 9.1875F;
			this.txtAssTotal14.Name = "txtAssTotal14";
			this.txtAssTotal14.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal14.Text = null;
			this.txtAssTotal14.Top = 3.46875F;
			this.txtAssTotal14.Width = 0.8118055F;
			// 
			// txtAssTotal6
			// 
			this.txtAssTotal6.Height = 0.19F;
			this.txtAssTotal6.Left = 9.1875F;
			this.txtAssTotal6.Name = "txtAssTotal6";
			this.txtAssTotal6.Style = "font-size: 8.5pt; text-align: right";
			this.txtAssTotal6.Text = null;
			this.txtAssTotal6.Top = 1.71875F;
			this.txtAssTotal6.Width = 0.8118055F;
			// 
			// txtLandDesc45
			// 
			this.txtLandDesc45.Height = 0.125F;
			this.txtLandDesc45.Left = 9.125F;
			this.txtLandDesc45.Name = "txtLandDesc45";
			this.txtLandDesc45.Style = "font-size: 6pt";
			this.txtLandDesc45.Text = "45.";
			this.txtLandDesc45.Top = 7.25F;
			this.txtLandDesc45.Width = 0.875F;
			// 
			// txtLandDesc46
			// 
			this.txtLandDesc46.Height = 0.125F;
			this.txtLandDesc46.Left = 9.125F;
			this.txtLandDesc46.Name = "txtLandDesc46";
			this.txtLandDesc46.Style = "font-size: 6pt";
			this.txtLandDesc46.Text = "46.";
			this.txtLandDesc46.Top = 7.375F;
			this.txtLandDesc46.Width = 0.875F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.1875F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 7.0625F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "font-size: 8.5pt; font-weight: bold; text-align: center";
			this.Label70.Text = "Total Acreage";
			this.Label70.Top = 7.1875F;
			this.Label70.Width = 0.875F;
			// 
			// txtTotalAcres
			// 
			this.txtTotalAcres.CanGrow = false;
			this.txtTotalAcres.Height = 0.1875F;
			this.txtTotalAcres.Left = 8.0625F;
			this.txtTotalAcres.Name = "txtTotalAcres";
			this.txtTotalAcres.Style = "font-size: 8.5pt";
			this.txtTotalAcres.Text = null;
			this.txtTotalAcres.Top = 7.1875F;
			this.txtTotalAcres.Width = 0.875F;
			// 
			// srptpdfPropertyCard1
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Duplex = System.Drawing.Printing.Duplex.Vertical;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.999306F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCards)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1L1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2L1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3L1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4L1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5L1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6L1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1L2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2L2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3L2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4L2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5L2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6L2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1L3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2L3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3L3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4L3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5L3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6L3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1L4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2L4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3L4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4L4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5L4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6L4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeighborhood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTreeGrowth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblXCoord)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtXCoord)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYCoord)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYCoord)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopography1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopography2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtility1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtility2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreetCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalePrice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleTypeCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleFinancing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancingCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidityCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerified)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerifiedCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontage7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepth7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootType7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootFactor7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFrontFootCode7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftUnits7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftType7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftFactor7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSqftCode7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageUnits7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageType7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageFactor7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcreageCode7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfluenceCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssYear6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssLand14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssBldg14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssExempt14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssTotal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCard;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCards;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1L1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2L1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3L1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4L1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5L1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6L1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1L2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2L2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3L2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4L2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5L2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6L2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1L3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2L3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3L3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4L3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5L3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6L3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1L4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2L4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3L4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4L4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5L4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6L4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotes;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeighborhood;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTreeGrowth;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblXCoord;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtXCoord;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYCoord;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYCoord;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZone;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecZone;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopography1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopography2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtility1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtility2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreet;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreetCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line25;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line26;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line27;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleDate;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSalePrice;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleType;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleTypeCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleTypeCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleTypeCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleTypeCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleTypeCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleTypeCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleTypeCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleTypeCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleTypeCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleFinancing;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinancingCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinancingCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinancingCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinancingCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinancingCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinancingCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinancingCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinancingCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinancingCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidity;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidityCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidityCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidityCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidityCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidityCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidityCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidityCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidityCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidityCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerified;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerifiedCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerifiedCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerifiedCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerifiedCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerifiedCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerifiedCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerifiedCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerifiedCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerifiedCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line39;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line40;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line41;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line42;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line43;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line44;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line45;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line46;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line47;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line48;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line49;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line50;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line51;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line52;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line53;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line54;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line56;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line58;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line62;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontage1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepth1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootType1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line65;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line63;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootFactor1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootCode1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontage2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepth2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootFactor2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootCode2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontage3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepth3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootType3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootFactor3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootCode3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontage4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepth4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootType4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootFactor4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootCode4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontage5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepth5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootType5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootFactor5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootCode5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontage6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepth6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootType6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootFactor6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootCode6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontage7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepth7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootType7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootFactor7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFrontFootCode7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line60;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line66;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line67;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line68;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line69;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line70;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line71;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line64;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line61;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftUnits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftType1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftFactor1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftCode1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftFactor2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftCode2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftUnits3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftType3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftFactor3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftCode3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftUnits4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftType4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftFactor4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftCode4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftUnits5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftType5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftFactor5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftCode5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftUnits6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftType6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftFactor6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftCode6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftUnits7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftType7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftFactor7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSqftCode7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageUnits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageType1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageFactor1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageCode1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageFactor2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageCode2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageUnits3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageType3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageFactor3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageCode3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageUnits4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageType4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageFactor4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageCode4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageUnits5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageType5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageFactor5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageCode5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageUnits6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageType6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageFactor6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageCode6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageUnits7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageType7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageFactor7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcreageCode7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line73;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line74;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line75;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line76;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line77;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line78;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line79;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line81;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line82;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line83;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line84;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line85;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line86;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line87;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line88;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line72;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfluenceCost1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfluenceCost2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfluenceCost3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfluenceCost4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfluenceCost5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfluenceCost6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfluenceCost7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfluenceCost8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfluenceCost9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line80;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label65;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc38;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc43;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc44;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line59;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line89;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line90;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line91;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line92;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line93;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line94;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssYear6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssLand14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssBldg14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssExempt14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssTotal6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc45;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAcres;
	}
}
