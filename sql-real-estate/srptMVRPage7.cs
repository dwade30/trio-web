﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using fecherFoundation;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage7.
	/// </summary>
	public partial class srptMVRPage7 : FCSectionReport
	{
		public srptMVRPage7()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private MunicipalValuationReturn valuationReturn;
        public srptMVRPage7(MunicipalValuationReturn valuationReturn) : this()
        {
            this.valuationReturn = valuationReturn;
        }
		private void InitializeComponentEx()
		{

		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMunicipality.Text = valuationReturn.Municipality;
			lblTitle.Text = "MAINE REVENUE SERVICES - " + valuationReturn.ReportYear + " MUNICIPAL VALUATION RETURN";
            txtSolarAndWindApproved.Text =
                valuationReturn.ExemptProperty.SolarAndWindApplicationsApproved.FormatAsNumber();
            txtSolarAndWindProcessed.Text =
                valuationReturn.ExemptProperty.SolarAndWindApplicationsProcessed.FormatAsNumber();
            txtSolarAndWindExempt.Text =
                valuationReturn.ExemptProperty.SolarAndWindEquipment.ToInteger().FormatAsNumber();
            var x = 1;
            foreach (var organization in valuationReturn.ExemptProperty.ExemptOrganizations)
            {
                switch (x)
                {
                    case 1:
                        txtOtherExemptName1.Text = organization.Name;
                        txtOtherExemptValue1.Text = organization.ExemptValue.ToInteger().FormatAsNumber();
                        txtOtherExemptProvision1.Text = organization.ProvisionOfLaw;
                        break;
                    case 2:
                        txtOtherExemptName2.Text = organization.Name;
                        txtOtherExemptValue2.Text = organization.ExemptValue.ToInteger().FormatAsNumber();
                        txtOtherExemptProvision2.Text = organization.ProvisionOfLaw;
                        break;
                    case 3:
                        txtOtherExemptName3.Text = organization.Name;
                        txtOtherExemptValue3.Text = organization.ExemptValue.ToInteger().FormatAsNumber();
                        txtOtherExemptProvision3.Text = organization.ProvisionOfLaw;
                        break;
                }

                x++;
            }

            txtOtherExemptTotalValue.Text = valuationReturn.ExemptProperty.ExemptOrganizations.Sum(o => o.ExemptValue).ToInteger()
                .FormatAsNumber();
            txtTotalValueAllExempt.Text =
                (valuationReturn.ExemptProperty.TotalExempt + valuationReturn.VeteranExemptions.Totals.ExemptValue)
                .ToInteger().FormatAsNumber();
            txtHasTaxMaps.Text = valuationReturn.MunicipalRecords.HasTaxMaps ? "YES" : "NO";            

			if (valuationReturn.MunicipalRecords.HasTaxMaps)
            {
                txtTaxMapDate.Text =
                    valuationReturn.MunicipalRecords.DateMapsOriginallyObtained?.FormatAndPadShortDate() ?? "";
                txtTaxMapPreparer.Text = valuationReturn.MunicipalRecords.NameOfOriginalContractor;
                txtTaxMapType.Text = valuationReturn.MunicipalRecords.TaxMapType.ToString();
			}
			else
			{
				txtTaxMapDate.Text = "";
				txtTaxMapPreparer.Text = "";
				txtTaxMapType.Text = "";
			}

            txtNumberParcels.Text = valuationReturn.MunicipalRecords.NumberOfParcelsInMunicipality.FormatAsNumber();
            txtTaxableAcreage.Text = valuationReturn.MunicipalRecords.TotalTaxableAcreage.FormatAsCurrencyNoSymbol();
            txtRevaluationCompleted.Text = valuationReturn.MunicipalRecords.ProfessionalRevaluationWasCompleted ? "YES" : "NO";
            txtLineRevaluationLand.Text = valuationReturn.MunicipalRecords.RevaluationIncludedLand ? "YES" : "NO";
            txtLineRevaluationBuilding.Text =
                valuationReturn.MunicipalRecords.RevaluationIncludedBuildings ? "YES" : "NO";
            txtLineRevaluationPP.Text =
                valuationReturn.MunicipalRecords.RevaluationIncludedPersonalProperty ? "YES" : "NO";
            txtRevaluationEffectiveDate.Text =
                valuationReturn.MunicipalRecords.RevaluationEffectiveDate?.FormatAndPadShortDate() ?? "";
            txtRevaluationContractor.Text = valuationReturn.MunicipalRecords.RevaluationContractorName;
            txtRevaluationCost.Text = valuationReturn.MunicipalRecords.RevaluationCost.FormatAsCurrencyNoSymbol();
		}

		
	}
}
