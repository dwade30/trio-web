﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptSaleData.
	/// </summary>
	partial class srptSaleData
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptSaleData));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtValidity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVerified = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinancing = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSalePrice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerified)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalePrice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.txtValidity,
				this.txtVerified,
				this.txtFinancing,
				this.txtSaleType,
				this.txtSalePrice,
				this.txtSaleDate
			});
			this.Detail.Height = 0.9895833F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Line1,
				this.Line2
			});
			this.GroupHeader1.Height = 0.19625F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Field1
			// 
			this.Field1.Height = 0.19625F;
			this.Field1.Left = 0.875F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field1.Tag = "bold";
			this.Field1.Text = "Sale Data";
			this.Field1.Top = 0F;
			this.Field1.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.09375F;
			this.Line1.Width = 0.75F;
			this.Line1.X1 = 0.8125F;
			this.Line1.X2 = 0.0625F;
			this.Line1.Y1 = 0.09375F;
			this.Line1.Y2 = 0.09375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.875F;
			this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.09375F;
			this.Line2.Width = 0.75F;
			this.Line2.X1 = 1.875F;
			this.Line2.X2 = 2.625F;
			this.Line2.Y1 = 0.09375F;
			this.Line2.Y2 = 0.09375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19625F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'";
			this.Label1.Tag = "text";
			this.Label1.Text = "Sale Date";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 0.875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19625F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'";
			this.Label2.Tag = "text";
			this.Label2.Text = "Sale Price";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 0.875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19625F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'";
			this.Label3.Tag = "text";
			this.Label3.Text = "Sale Type";
			this.Label3.Top = 0.34375F;
			this.Label3.Width = 0.875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19625F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.0625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'";
			this.Label4.Tag = "text";
			this.Label4.Text = "Financing";
			this.Label4.Top = 0.5F;
			this.Label4.Width = 0.875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19625F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'";
			this.Label5.Tag = "text";
			this.Label5.Text = "Verified";
			this.Label5.Top = 0.65625F;
			this.Label5.Width = 0.875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19625F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'";
			this.Label6.Tag = "text";
			this.Label6.Text = "Validity";
			this.Label6.Top = 0.8125F;
			this.Label6.Width = 0.875F;
			// 
			// txtValidity
			// 
			this.txtValidity.Height = 0.19625F;
			this.txtValidity.Left = 0.9375F;
			this.txtValidity.MultiLine = false;
			this.txtValidity.Name = "txtValidity";
			this.txtValidity.Style = "font-family: \'Tahoma\'";
			this.txtValidity.Tag = "text";
			this.txtValidity.Text = null;
			this.txtValidity.Top = 0.8125F;
			this.txtValidity.Width = 1.6875F;
			// 
			// txtVerified
			// 
			this.txtVerified.Height = 0.19625F;
			this.txtVerified.Left = 0.9375F;
			this.txtVerified.MultiLine = false;
			this.txtVerified.Name = "txtVerified";
			this.txtVerified.Style = "font-family: \'Tahoma\'";
			this.txtVerified.Tag = "text";
			this.txtVerified.Text = null;
			this.txtVerified.Top = 0.65625F;
			this.txtVerified.Width = 1.6875F;
			// 
			// txtFinancing
			// 
			this.txtFinancing.Height = 0.19625F;
			this.txtFinancing.Left = 0.9375F;
			this.txtFinancing.MultiLine = false;
			this.txtFinancing.Name = "txtFinancing";
			this.txtFinancing.Style = "font-family: \'Tahoma\'";
			this.txtFinancing.Tag = "text";
			this.txtFinancing.Text = null;
			this.txtFinancing.Top = 0.5F;
			this.txtFinancing.Width = 1.6875F;
			// 
			// txtSaleType
			// 
			this.txtSaleType.Height = 0.19625F;
			this.txtSaleType.Left = 0.9375F;
			this.txtSaleType.MultiLine = false;
			this.txtSaleType.Name = "txtSaleType";
			this.txtSaleType.Style = "font-family: \'Tahoma\'";
			this.txtSaleType.Tag = "text";
			this.txtSaleType.Text = null;
			this.txtSaleType.Top = 0.34375F;
			this.txtSaleType.Width = 1.6875F;
			// 
			// txtSalePrice
			// 
			this.txtSalePrice.Height = 0.19625F;
			this.txtSalePrice.Left = 0.9375F;
			this.txtSalePrice.MultiLine = false;
			this.txtSalePrice.Name = "txtSalePrice";
			this.txtSalePrice.Style = "font-family: \'Tahoma\'";
			this.txtSalePrice.Tag = "text";
			this.txtSalePrice.Text = null;
			this.txtSalePrice.Top = 0.1875F;
			this.txtSalePrice.Width = 1.6875F;
			// 
			// txtSaleDate
			// 
			this.txtSaleDate.Height = 0.19625F;
			this.txtSaleDate.Left = 0.9375F;
			this.txtSaleDate.MultiLine = false;
			this.txtSaleDate.Name = "txtSaleDate";
			this.txtSaleDate.Style = "font-family: \'Tahoma\'";
			this.txtSaleDate.Tag = "text";
			this.txtSaleDate.Text = null;
			this.txtSaleDate.Top = 0.03125F;
			this.txtSaleDate.Width = 1.6875F;
			// 
			// srptSaleData
			//
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 2.708333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValidity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVerified)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinancing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalePrice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValidity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVerified;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinancing;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSalePrice;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
