﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmChangesAuditReport.
	/// </summary>
	public partial class frmChangesAuditReport : BaseForm
	{
		public frmChangesAuditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChangesAuditReport InstancePtr
		{
			get
			{
				return (frmChangesAuditReport)Sys.GetInstance(typeof(frmChangesAuditReport));
			}
		}

		protected frmChangesAuditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmChangesAuditReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmChangesAuditReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChangesAuditReport properties;
			//frmChangesAuditReport.FillStyle	= 0;
			//frmChangesAuditReport.ScaleWidth	= 5880;
			//frmChangesAuditReport.ScaleHeight	= 4185;
			//frmChangesAuditReport.LinkTopic	= "Form2";
			//frmChangesAuditReport.LockControls	= true;
			//frmChangesAuditReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			FillScreenCombo();
		}

		private void FillScreenCombo()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select location from auditchangesarchive group by location");
			cboScreen.Clear();
			cboScreen.AddItem("All");
			while (!rsLoad.EndOfFile())
			{
				cboScreen.AddItem(rsLoad.Get_Fields_String("location"));
				rsLoad.MoveNext();
			}
			cboScreen.SelectedIndex = 0;
		}

		public void mnuExit_Click()
		{
			this.Unload();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			string strSQL;
			string strWhere;
			string strAnd;
			strSQL = "Select * from auditchangesarchive ";
			strWhere = "";
			strAnd = "";
			if (Conversion.Val(txtAccount.Text) > 0)
			{
				strWhere += strAnd + " userfield1 = '" + FCConvert.ToString(Conversion.Val(txtAccount.Text)) + "'";
				strAnd = " and ";
			}
			if (Strings.UCase(cboScreen.Text) != "ALL" && Strings.Trim(cboScreen.Text) != "")
			{
				strWhere += strAnd + " location = '" + cboScreen.Text + "'";
				strAnd = " and ";
			}
			if (Information.IsDate(txtLowDate.Text) || Information.IsDate(txtHighDate.Text))
			{
				if (Information.IsDate(txtLowDate.Text))
				{
					if (Information.IsDate(txtHighDate.Text))
					{
						strWhere += strAnd + " dateupdated >= '" + txtLowDate.Text + "' and dateupdated <= '" + txtHighDate.Text + "'";
					}
					else
					{
						strWhere += strAnd + " dateupdated >= '" + txtLowDate.Text + "'";
					}
				}
				else
				{
					strWhere += strAnd + " dateupdated <= '" + txtHighDate.Text + "'";
				}
				strAnd = " and ";
			}
			if (strWhere != string.Empty)
			{
				strWhere = " where " + strWhere;
			}
			strSQL += strWhere;
			if (cmbOrder.Text == "Date")
			{
				strSQL += " order by dateupdated,timeupdated";
			}
			else if (cmbOrder.Text == "User")
			{
				strSQL += " order by userid,dateupdated,timeupdated";
			}
			else
			{
				strSQL += " order by CHANGEdescription,dateupdated,timeupdated";
			}
			rptAuditArchiveReport.InstancePtr.Init(strSQL, this.Modal, false, "Changes Audit Archive");
		}
	}
}
