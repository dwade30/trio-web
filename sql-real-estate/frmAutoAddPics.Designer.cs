﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmAutoAddPics.
	/// </summary>
	partial class frmAutoAddPics : BaseForm
	{
		public fecherFoundation.FCComboBox cmbNameLen;
		public fecherFoundation.FCLabel lblNameLen;
		public fecherFoundation.FCComboBox cmbFormat;
		public fecherFoundation.FCLabel lblFormat;
		public fecherFoundation.FCComboBox cmbDestDir;
		public fecherFoundation.FCCheckBox chkRename;
		public fecherFoundation.FCCheckBox chkDeleteSource;
		public fecherFoundation.FCCheckBox chkImport;
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCTextBox txtSourcedir;
		public FCGrid GridPictures;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkMiscData;
		public fecherFoundation.FCTextBox txtFileNameLen;
		public fecherFoundation.FCFrame framPosition;
		public FCGrid GridPosition;
		public fecherFoundation.FCComboBox cmbSeparator;
		public fecherFoundation.FCCheckBox chkIncludeNumber;
		public fecherFoundation.FCLabel lblFilenameLen;
		public fecherFoundation.FCLabel lblSeparator;
		public fecherFoundation.FCLabel lblDestDir;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuDelRow;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuLoadFiles;
		public fecherFoundation.FCToolStripMenuItem mnuProcessList;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutoAddPics));
            this.cmbNameLen = new fecherFoundation.FCComboBox();
            this.lblNameLen = new fecherFoundation.FCLabel();
            this.cmbFormat = new fecherFoundation.FCComboBox();
            this.lblFormat = new fecherFoundation.FCLabel();
            this.cmbDestDir = new fecherFoundation.FCComboBox();
            this.chkRename = new fecherFoundation.FCCheckBox();
            this.chkDeleteSource = new fecherFoundation.FCCheckBox();
            this.chkImport = new fecherFoundation.FCCheckBox();
            this.Command1 = new fecherFoundation.FCButton();
            this.txtSourcedir = new fecherFoundation.FCTextBox();
            this.GridPictures = new fecherFoundation.FCGrid();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkMiscData = new fecherFoundation.FCCheckBox();
            this.txtFileNameLen = new fecherFoundation.FCTextBox();
            this.framPosition = new fecherFoundation.FCFrame();
            this.GridPosition = new fecherFoundation.FCGrid();
            this.cmbSeparator = new fecherFoundation.FCComboBox();
            this.chkIncludeNumber = new fecherFoundation.FCCheckBox();
            this.lblFilenameLen = new fecherFoundation.FCLabel();
            this.lblSeparator = new fecherFoundation.FCLabel();
            this.lblDestDir = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLoadFiles = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessList = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdDelRow = new fecherFoundation.FCButton();
            this.cmdLoadFiles = new fecherFoundation.FCButton();
            this.cmdProcessList = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRename)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkImport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPictures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMiscData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPosition)).BeginInit();
            this.framPosition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessList)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessList);
            this.BottomPanel.Location = new System.Drawing.Point(0, 705);
            this.BottomPanel.Size = new System.Drawing.Size(817, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbDestDir);
            this.ClientArea.Controls.Add(this.chkRename);
            this.ClientArea.Controls.Add(this.chkDeleteSource);
            this.ClientArea.Controls.Add(this.chkImport);
            this.ClientArea.Controls.Add(this.Command1);
            this.ClientArea.Controls.Add(this.txtSourcedir);
            this.ClientArea.Controls.Add(this.GridPictures);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.lblDestDir);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(837, 573);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblDestDir, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridPictures, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtSourcedir, 0);
            this.ClientArea.Controls.SetChildIndex(this.Command1, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkImport, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkDeleteSource, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkRename, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbDestDir, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdLoadFiles);
            this.TopPanel.Controls.Add(this.cmdDelRow);
            this.TopPanel.Size = new System.Drawing.Size(837, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelRow, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdLoadFiles, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(195, 28);
            this.HeaderText.Text = "Auto Add Pictures";
            // 
            // cmbNameLen
            // 
            this.cmbNameLen.Items.AddRange(new object[] {
            "Variable Length",
            "Use Separators",
            "Fixed Length"});
            this.cmbNameLen.Location = new System.Drawing.Point(477, 30);
            this.cmbNameLen.Name = "cmbNameLen";
            this.cmbNameLen.Size = new System.Drawing.Size(193, 40);
            this.cmbNameLen.TabIndex = 27;
            this.cmbNameLen.Text = "Variable Length";
            this.cmbNameLen.SelectedIndexChanged += new System.EventHandler(this.cmbNameLen_SelectedIndexChanged);
            // 
            // lblNameLen
            // 
            this.lblNameLen.AutoSize = true;
            this.lblNameLen.Location = new System.Drawing.Point(344, 44);
            this.lblNameLen.Name = "lblNameLen";
            this.lblNameLen.Size = new System.Drawing.Size(99, 16);
            this.lblNameLen.TabIndex = 28;
            this.lblNameLen.Text = "NAME LENGTH";
            // 
            // cmbFormat
            // 
            this.cmbFormat.Items.AddRange(new object[] {
            "Map Lot",
            "Map Lot & Card",
            "Account",
            "Account & Card"});
            this.cmbFormat.Location = new System.Drawing.Point(112, 30);
            this.cmbFormat.Name = "cmbFormat";
            this.cmbFormat.Size = new System.Drawing.Size(193, 40);
            this.cmbFormat.TabIndex = 29;
            this.cmbFormat.Text = "Map Lot";
            this.cmbFormat.SelectedIndexChanged += new System.EventHandler(this.cmbFormat_SelectedIndexChanged);
            // 
            // lblFormat
            // 
            this.lblFormat.AutoSize = true;
            this.lblFormat.Location = new System.Drawing.Point(20, 44);
            this.lblFormat.Name = "lblFormat";
            this.lblFormat.Size = new System.Drawing.Size(59, 16);
            this.lblFormat.TabIndex = 30;
            this.lblFormat.Text = "FORMAT";
            // 
            // cmbDestDir
            // 
            this.cmbDestDir.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDestDir.Location = new System.Drawing.Point(569, 415);
            this.cmbDestDir.Name = "cmbDestDir";
            this.cmbDestDir.Size = new System.Drawing.Size(169, 40);
            this.cmbDestDir.TabIndex = 25;
            this.cmbDestDir.Visible = false;
            // 
            // chkRename
            // 
            this.chkRename.Enabled = false;
            this.chkRename.Location = new System.Drawing.Point(329, 451);
            this.chkRename.Name = "chkRename";
            this.chkRename.Size = new System.Drawing.Size(136, 23);
            this.chkRename.TabIndex = 23;
            this.chkRename.Text = "Rename pictures";
            // 
            // chkDeleteSource
            // 
            this.chkDeleteSource.Enabled = false;
            this.chkDeleteSource.Location = new System.Drawing.Point(329, 414);
            this.chkDeleteSource.Name = "chkDeleteSource";
            this.chkDeleteSource.Size = new System.Drawing.Size(171, 23);
            this.chkDeleteSource.TabIndex = 22;
            this.chkDeleteSource.Text = "Delete source pictures";
            // 
            // chkImport
            // 
            this.chkImport.Location = new System.Drawing.Point(329, 377);
            this.chkImport.Name = "chkImport";
            this.chkImport.Size = new System.Drawing.Size(198, 23);
            this.chkImport.TabIndex = 21;
            this.chkImport.Text = "Copy files to new directory";
            this.chkImport.CheckedChanged += new System.EventHandler(this.chkImport_CheckedChanged);
            // 
            // Command1
            // 
            this.Command1.AppearanceKey = "actionButton";
            this.Command1.Location = new System.Drawing.Point(234, 415);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(75, 40);
            this.Command1.TabIndex = 20;
            this.Command1.Text = "Browse";
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // txtSourcedir
            // 
            this.txtSourcedir.BackColor = System.Drawing.SystemColors.Window;
            this.txtSourcedir.Location = new System.Drawing.Point(30, 415);
            this.txtSourcedir.LockedOriginal = true;
            this.txtSourcedir.Name = "txtSourcedir";
            this.txtSourcedir.ReadOnly = true;
            this.txtSourcedir.Size = new System.Drawing.Size(183, 40);
            this.txtSourcedir.TabIndex = 18;
            // 
            // GridPictures
            // 
            this.GridPictures.Cols = 6;
            this.GridPictures.ExtendLastCol = true;
            this.GridPictures.FixedCols = 0;
            this.GridPictures.Location = new System.Drawing.Point(30, 498);
            this.GridPictures.Name = "GridPictures";
            this.GridPictures.RowHeadersVisible = false;
            this.GridPictures.Rows = 1;
            this.GridPictures.Size = new System.Drawing.Size(708, 207);
            this.GridPictures.TabIndex = 17;
            this.GridPictures.KeyDown += new Wisej.Web.KeyEventHandler(this.GridPictures_KeyDownEvent);
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxLeftBorder";
            this.Frame1.Controls.Add(this.chkMiscData);
            this.Frame1.Controls.Add(this.cmbNameLen);
            this.Frame1.Controls.Add(this.lblNameLen);
            this.Frame1.Controls.Add(this.cmbFormat);
            this.Frame1.Controls.Add(this.lblFormat);
            this.Frame1.Controls.Add(this.txtFileNameLen);
            this.Frame1.Controls.Add(this.framPosition);
            this.Frame1.Controls.Add(this.cmbSeparator);
            this.Frame1.Controls.Add(this.chkIncludeNumber);
            this.Frame1.Controls.Add(this.lblFilenameLen);
            this.Frame1.Controls.Add(this.lblSeparator);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(753, 327);
            this.Frame1.TabIndex = 1;
            this.Frame1.Text = "Filename Format";
            // 
            // chkMiscData
            // 
            this.chkMiscData.Location = new System.Drawing.Point(247, 90);
            this.chkMiscData.Name = "chkMiscData";
            this.chkMiscData.Size = new System.Drawing.Size(152, 23);
            this.chkMiscData.TabIndex = 26;
            this.chkMiscData.Text = "Includes Misc. Data";
            this.ToolTip1.SetToolTip(this.chkMiscData, "Miscellaneous data can be any text or numeric characters not accounted for by the" +
        " other options");
            this.chkMiscData.CheckedChanged += new System.EventHandler(this.chkMiscData_CheckedChanged);
            // 
            // txtFileNameLen
            // 
            this.txtFileNameLen.BackColor = System.Drawing.SystemColors.Window;
            this.txtFileNameLen.Location = new System.Drawing.Point(400, 137);
            this.txtFileNameLen.Name = "txtFileNameLen";
            this.txtFileNameLen.Size = new System.Drawing.Size(60, 40);
            this.txtFileNameLen.TabIndex = 16;
            this.txtFileNameLen.Text = "8";
            this.txtFileNameLen.Visible = false;
            // 
            // framPosition
            // 
            this.framPosition.Controls.Add(this.GridPosition);
            this.framPosition.Enabled = false;
            this.framPosition.Location = new System.Drawing.Point(20, 137);
            this.framPosition.Name = "framPosition";
            this.framPosition.Size = new System.Drawing.Size(340, 170);
            this.framPosition.TabIndex = 13;
            this.framPosition.Text = "Position";
            // 
            // GridPosition
            // 
            this.GridPosition.Cols = 3;
            this.GridPosition.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridPosition.Enabled = false;
            this.GridPosition.ExtendLastCol = true;
            this.GridPosition.Location = new System.Drawing.Point(20, 30);
            this.GridPosition.Name = "GridPosition";
            this.GridPosition.ReadOnly = false;
            this.GridPosition.Rows = 5;
            this.GridPosition.Size = new System.Drawing.Size(300, 120);
            this.GridPosition.TabIndex = 14;
            // 
            // cmbSeparator
            // 
            this.cmbSeparator.BackColor = System.Drawing.SystemColors.Window;
            this.cmbSeparator.Items.AddRange(new object[] {
            "-",
            ".",
            "_"});
            this.cmbSeparator.Location = new System.Drawing.Point(400, 197);
            this.cmbSeparator.Name = "cmbSeparator";
            this.cmbSeparator.Size = new System.Drawing.Size(60, 40);
            this.cmbSeparator.TabIndex = 10;
            this.cmbSeparator.Visible = false;
            // 
            // chkIncludeNumber
            // 
            this.chkIncludeNumber.Location = new System.Drawing.Point(20, 90);
            this.chkIncludeNumber.Name = "chkIncludeNumber";
            this.chkIncludeNumber.Size = new System.Drawing.Size(185, 23);
            this.chkIncludeNumber.TabIndex = 0;
            this.chkIncludeNumber.Text = "Includes Picture Number";
            this.chkIncludeNumber.CheckedChanged += new System.EventHandler(this.chkIncludeNumber_CheckedChanged);
            // 
            // lblFilenameLen
            // 
            this.lblFilenameLen.Location = new System.Drawing.Point(494, 151);
            this.lblFilenameLen.Name = "lblFilenameLen";
            this.lblFilenameLen.Size = new System.Drawing.Size(250, 18);
            this.lblFilenameLen.TabIndex = 15;
            this.lblFilenameLen.Text = "FILENAME LENGTH (WITHOUT EXTENSION)";
            this.lblFilenameLen.Visible = false;
            // 
            // lblSeparator
            // 
            this.lblSeparator.Location = new System.Drawing.Point(494, 211);
            this.lblSeparator.Name = "lblSeparator";
            this.lblSeparator.Size = new System.Drawing.Size(150, 18);
            this.lblSeparator.TabIndex = 11;
            this.lblSeparator.Text = "SEPARATOR CHARACTER";
            this.lblSeparator.Visible = false;
            // 
            // lblDestDir
            // 
            this.lblDestDir.Location = new System.Drawing.Point(569, 377);
            this.lblDestDir.Name = "lblDestDir";
            this.lblDestDir.Size = new System.Drawing.Size(169, 18);
            this.lblDestDir.TabIndex = 24;
            this.lblDestDir.Text = "DESTINATION DIRECTORY";
            this.lblDestDir.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblDestDir.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 377);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(183, 18);
            this.Label1.TabIndex = 19;
            this.Label1.Text = "SOURCE DIRECTORY";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuDelRow,
            this.mnuSepar2,
            this.mnuLoadFiles,
            this.mnuProcessList,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuDelRow
            // 
            this.mnuDelRow.Index = 0;
            this.mnuDelRow.Name = "mnuDelRow";
            this.mnuDelRow.Text = "Delete Row";
            this.mnuDelRow.Click += new System.EventHandler(this.mnuDelRow_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuLoadFiles
            // 
            this.mnuLoadFiles.Index = 2;
            this.mnuLoadFiles.Name = "mnuLoadFiles";
            this.mnuLoadFiles.Text = "Load Files";
            this.mnuLoadFiles.Click += new System.EventHandler(this.mnuLoadFiles_Click);
            // 
            // mnuProcessList
            // 
            this.mnuProcessList.Index = 3;
            this.mnuProcessList.Name = "mnuProcessList";
            this.mnuProcessList.Text = "Process List";
            this.mnuProcessList.Click += new System.EventHandler(this.mnuProcessList_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 4;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdDelRow
            // 
            this.cmdDelRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelRow.Location = new System.Drawing.Point(714, 29);
            this.cmdDelRow.Name = "cmdDelRow";
            this.cmdDelRow.Size = new System.Drawing.Size(95, 24);
            this.cmdDelRow.TabIndex = 1;
            this.cmdDelRow.Text = "Delete Row";
            this.cmdDelRow.Click += new System.EventHandler(this.mnuDelRow_Click);
            // 
            // cmdLoadFiles
            // 
            this.cmdLoadFiles.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdLoadFiles.Location = new System.Drawing.Point(623, 29);
            this.cmdLoadFiles.Name = "cmdLoadFiles";
            this.cmdLoadFiles.Size = new System.Drawing.Size(85, 24);
            this.cmdLoadFiles.TabIndex = 2;
            this.cmdLoadFiles.Text = "Load Files";
            this.cmdLoadFiles.Click += new System.EventHandler(this.mnuLoadFiles_Click);
            // 
            // cmdProcessList
            // 
            this.cmdProcessList.AppearanceKey = "acceptButton";
            this.cmdProcessList.Location = new System.Drawing.Point(339, 44);
            this.cmdProcessList.Name = "cmdProcessList";
            this.cmdProcessList.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessList.Size = new System.Drawing.Size(118, 48);
            this.cmdProcessList.TabIndex = 0;
            this.cmdProcessList.Text = "Process List";
            this.cmdProcessList.Click += new System.EventHandler(this.mnuProcessList_Click);
            // 
            // frmAutoAddPics
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(837, 633);
            this.FillColor = 0;
            this.Name = "frmAutoAddPics";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Auto Add Pictures";
            this.Load += new System.EventHandler(this.frmAutoAddPics_Load);
            this.Resize += new System.EventHandler(this.frmAutoAddPics_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRename)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkImport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPictures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMiscData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPosition)).EndInit();
            this.framPosition.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessList)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessList;
		private FCButton cmdLoadFiles;
		private FCButton cmdDelRow;
	}
}
