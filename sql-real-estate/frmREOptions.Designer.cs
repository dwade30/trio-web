﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmREOptions.
	/// </summary>
	partial class frmREOptions : BaseForm
	{
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public FCGrid GridDefaultSchedule;
		public fecherFoundation.FCFrame Frame1;
		public FCGrid gridIndustrialBldgCodes;
		public fecherFoundation.FCComboBox cmbPPIndPropCat;
		public FCGrid PPGrid;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCFrame Frame16;
		public fecherFoundation.FCCheckBox chkTreeGrowthCertifiedRatio;
		public fecherFoundation.FCCheckBox chkLandFactorTreeGrowth;
		public fecherFoundation.FCCheckBox chkHomesteadCard1;
		public fecherFoundation.FCFrame Frame12;
		public fecherFoundation.FCTextBox txtOpenCode;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtComDep;
		public fecherFoundation.FCTextBox txtMoHoDep;
		public fecherFoundation.FCTextBox txtREDep;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtMixedWood;
		public fecherFoundation.FCTextBox txtHardWood;
		public fecherFoundation.FCTextBox txtSoftWood;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCTextBox txtCertifiedRatio;
		public fecherFoundation.FCComboBox cboRounding;
		public fecherFoundation.FCComboBox cmbRoundContingency;
		public fecherFoundation.FCFrame Frame10;
		public fecherFoundation.FCCheckBox chkRoundExemptions;
		public fecherFoundation.FCLabel Label26;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame Frame14;
		public fecherFoundation.FCCheckBox chkHideLabelAccount;
		public fecherFoundation.FCCheckBox chkAccountOnBottom;
		public fecherFoundation.FCCheckBox chkPrintMapLotOnSide;
		public fecherFoundation.FCCheckBox chkMapLotSplit;
		public fecherFoundation.FCFrame Frame13;
		public fecherFoundation.FCComboBox cmbF2LabelType;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCCheckBox chkShowGridLines;
		public fecherFoundation.FCCheckBox chkDontPrintPropertyPhoto;
		public fecherFoundation.FCCheckBox chkPrintSecondaryPhoto;
		public fecherFoundation.FCCheckBox chkPropertyCardCurrentAmounts;
		public fecherFoundation.FCCheckBox chkPropertyCardComments;
		public fecherFoundation.FCCheckBox chkPropertyCard;
		public fecherFoundation.FCCheckBox chkPrevAssessments;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCCheckBox chkHideRatio;
		public fecherFoundation.FCCheckBox chkShowFirstPic;
		public fecherFoundation.FCCheckBox chkShowPic;
		public fecherFoundation.FCCheckBox chkShowSketch;
		public fecherFoundation.FCComboBox cboLandPricing;
		public fecherFoundation.FCComboBox cboValuationReport;
		public fecherFoundation.FCComboBox cboSFLA;
		public fecherFoundation.FCComboBox cboPerAcreValue;
		public fecherFoundation.FCComboBox cmbLabelOption;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCLabel Label30;
		public fecherFoundation.FCLabel Label31;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtBillYear;
		public fecherFoundation.FCTextBox txtBillRate;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCFrame Frame9;
		public fecherFoundation.FCButton cmdBrowsePocketPC;
		public fecherFoundation.FCTextBox txtPocketPCDirectory;
		public fecherFoundation.FCCheckBox chkPocketPCExternal;
		public fecherFoundation.FCCheckBox chkShowCode;
		public fecherFoundation.FCCheckBox chkToolTips;
		public fecherFoundation.FCCheckBox chkShortDesc;
		public fecherFoundation.FCTextBox txtRef1Label;
		public fecherFoundation.FCTextBox txtRef2Label;
		public fecherFoundation.FCCommonDialog dlg1;
		public FCGrid gridTownCode;
		public fecherFoundation.FCLabel lblCardsUsed;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCLabel lblMaxCards;
		public fecherFoundation.FCLabel Label24;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuCommercialStatistics;
		public fecherFoundation.FCToolStripMenuItem separ2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmREOptions));
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.chkHomesteadCard1 = new fecherFoundation.FCCheckBox();
            this.GridDefaultSchedule = new fecherFoundation.FCGrid();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.gridIndustrialBldgCodes = new fecherFoundation.FCGrid();
            this.cmbPPIndPropCat = new fecherFoundation.FCComboBox();
            this.PPGrid = new fecherFoundation.FCGrid();
            this.Label23 = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.Frame16 = new fecherFoundation.FCFrame();
            this.chkTreeGrowthCertifiedRatio = new fecherFoundation.FCCheckBox();
            this.chkLandFactorTreeGrowth = new fecherFoundation.FCCheckBox();
            this.Frame12 = new fecherFoundation.FCFrame();
            this.txtOpenCode = new fecherFoundation.FCTextBox();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtComDep = new fecherFoundation.FCTextBox();
            this.txtMoHoDep = new fecherFoundation.FCTextBox();
            this.txtREDep = new fecherFoundation.FCTextBox();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtMixedWood = new fecherFoundation.FCTextBox();
            this.txtHardWood = new fecherFoundation.FCTextBox();
            this.txtSoftWood = new fecherFoundation.FCTextBox();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.txtCertifiedRatio = new fecherFoundation.FCTextBox();
            this.cboRounding = new fecherFoundation.FCComboBox();
            this.cmbRoundContingency = new fecherFoundation.FCComboBox();
            this.Frame10 = new fecherFoundation.FCFrame();
            this.chkRoundExemptions = new fecherFoundation.FCCheckBox();
            this.Label26 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.Frame14 = new fecherFoundation.FCFrame();
            this.chkHideLabelAccount = new fecherFoundation.FCCheckBox();
            this.chkAccountOnBottom = new fecherFoundation.FCCheckBox();
            this.chkPrintMapLotOnSide = new fecherFoundation.FCCheckBox();
            this.chkMapLotSplit = new fecherFoundation.FCCheckBox();
            this.Frame13 = new fecherFoundation.FCFrame();
            this.cmbF2LabelType = new fecherFoundation.FCComboBox();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.chkShowGridLines = new fecherFoundation.FCCheckBox();
            this.chkDontPrintPropertyPhoto = new fecherFoundation.FCCheckBox();
            this.chkPrintSecondaryPhoto = new fecherFoundation.FCCheckBox();
            this.chkPropertyCardCurrentAmounts = new fecherFoundation.FCCheckBox();
            this.chkPropertyCardComments = new fecherFoundation.FCCheckBox();
            this.chkPropertyCard = new fecherFoundation.FCCheckBox();
            this.chkPrevAssessments = new fecherFoundation.FCCheckBox();
            this.Frame6 = new fecherFoundation.FCFrame();
            this.chkHideRatio = new fecherFoundation.FCCheckBox();
            this.chkShowFirstPic = new fecherFoundation.FCCheckBox();
            this.chkShowPic = new fecherFoundation.FCCheckBox();
            this.chkShowSketch = new fecherFoundation.FCCheckBox();
            this.cboLandPricing = new fecherFoundation.FCComboBox();
            this.cboValuationReport = new fecherFoundation.FCComboBox();
            this.cboSFLA = new fecherFoundation.FCComboBox();
            this.cboPerAcreValue = new fecherFoundation.FCComboBox();
            this.cmbLabelOption = new fecherFoundation.FCComboBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.Frame9 = new fecherFoundation.FCFrame();
            this.cmdBrowsePocketPC = new fecherFoundation.FCButton();
            this.txtPocketPCDirectory = new fecherFoundation.FCTextBox();
            this.chkPocketPCExternal = new fecherFoundation.FCCheckBox();
            this.Label30 = new fecherFoundation.FCLabel();
            this.Label31 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtBillYear = new fecherFoundation.FCTextBox();
            this.txtBillRate = new fecherFoundation.FCTextBox();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.chkShowCode = new fecherFoundation.FCCheckBox();
            this.chkToolTips = new fecherFoundation.FCCheckBox();
            this.chkShortDesc = new fecherFoundation.FCCheckBox();
            this.txtRef1Label = new fecherFoundation.FCTextBox();
            this.txtRef2Label = new fecherFoundation.FCTextBox();
            this.dlg1 = new fecherFoundation.FCCommonDialog();
            this.gridTownCode = new fecherFoundation.FCGrid();
            this.lblCardsUsed = new fecherFoundation.FCLabel();
            this.Label25 = new fecherFoundation.FCLabel();
            this.lblMaxCards = new fecherFoundation.FCLabel();
            this.Label24 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCommercialStatistics = new fecherFoundation.FCToolStripMenuItem();
            this.separ2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdCommercialStatistics = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkHomesteadCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDefaultSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridIndustrialBldgCodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PPGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame16)).BeginInit();
            this.Frame16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTreeGrowthCertifiedRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLandFactorTreeGrowth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame12)).BeginInit();
            this.Frame12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame10)).BeginInit();
            this.Frame10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRoundExemptions)).BeginInit();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame14)).BeginInit();
            this.Frame14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkHideLabelAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAccountOnBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintMapLotOnSide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMapLotSplit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame13)).BeginInit();
            this.Frame13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowGridLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDontPrintPropertyPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintSecondaryPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropertyCardCurrentAmounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropertyCardComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropertyCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrevAssessments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkHideRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowFirstPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowSketch)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).BeginInit();
            this.Frame9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowsePocketPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPocketPCExternal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkToolTips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShortDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCommercialStatistics)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 865);
            this.BottomPanel.Size = new System.Drawing.Size(1193, 69);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Controls.Add(this.gridTownCode);
            this.ClientArea.Controls.Add(this.lblCardsUsed);
            this.ClientArea.Controls.Add(this.Label25);
            this.ClientArea.Controls.Add(this.lblMaxCards);
            this.ClientArea.Controls.Add(this.Label24);
            this.ClientArea.Size = new System.Drawing.Size(1213, 628);
            this.ClientArea.Controls.SetChildIndex(this.Label24, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblMaxCards, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label25, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCardsUsed, 0);
            this.ClientArea.Controls.SetChildIndex(this.gridTownCode, 0);
            this.ClientArea.Controls.SetChildIndex(this.SSTab1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdCommercialStatistics);
            this.TopPanel.Size = new System.Drawing.Size(1213, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCommercialStatistics, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(117, 28);
            this.HeaderText.Text = "Customize";
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Location = new System.Drawing.Point(0, 60);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
            this.SSTab1.Size = new System.Drawing.Size(1183, 805);
            this.SSTab1.TabIndex = 40;
            this.SSTab1.Text = "Misc. Settings";
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.AutoScroll = true;
            this.SSTab1_Page1.Controls.Add(this.chkHomesteadCard1);
            this.SSTab1_Page1.Controls.Add(this.GridDefaultSchedule);
            this.SSTab1_Page1.Controls.Add(this.Frame1);
            this.SSTab1_Page1.Controls.Add(this.Frame16);
            this.SSTab1_Page1.Controls.Add(this.Frame12);
            this.SSTab1_Page1.Controls.Add(this.Frame3);
            this.SSTab1_Page1.Controls.Add(this.Frame5);
            this.SSTab1_Page1.Controls.Add(this.txtCertifiedRatio);
            this.SSTab1_Page1.Controls.Add(this.cboRounding);
            this.SSTab1_Page1.Controls.Add(this.cmbRoundContingency);
            this.SSTab1_Page1.Controls.Add(this.Frame10);
            this.SSTab1_Page1.Controls.Add(this.Label26);
            this.SSTab1_Page1.Controls.Add(this.Label19);
            this.SSTab1_Page1.Controls.Add(this.Label1);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Size = new System.Drawing.Size(1181, 769);
            this.SSTab1_Page1.Text = "Misc. Settings";
            // 
            // chkHomesteadCard1
            // 
            this.chkHomesteadCard1.Location = new System.Drawing.Point(30, 490);
            this.chkHomesteadCard1.Name = "chkHomesteadCard1";
            this.chkHomesteadCard1.Size = new System.Drawing.Size(216, 22);
            this.chkHomesteadCard1.TabIndex = 0;
            this.chkHomesteadCard1.Text = "Apply Homestead to card 1 only";
            // 
            // GridDefaultSchedule
            // 
            this.GridDefaultSchedule.Cols = 1;
            this.GridDefaultSchedule.ColumnHeadersVisible = false;
            this.GridDefaultSchedule.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridDefaultSchedule.ExtendLastCol = true;
            this.GridDefaultSchedule.FixedCols = 0;
            this.GridDefaultSchedule.FixedRows = 0;
            this.GridDefaultSchedule.Location = new System.Drawing.Point(177, 616);
            this.GridDefaultSchedule.Name = "GridDefaultSchedule";
            this.GridDefaultSchedule.ReadOnly = false;
            this.GridDefaultSchedule.RowHeadersVisible = false;
            this.GridDefaultSchedule.Rows = 1;
            this.GridDefaultSchedule.ShowFocusCell = false;
            this.GridDefaultSchedule.Size = new System.Drawing.Size(261, 42);
            this.GridDefaultSchedule.TabIndex = 12;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.gridIndustrialBldgCodes);
            this.Frame1.Controls.Add(this.cmbPPIndPropCat);
            this.Frame1.Controls.Add(this.PPGrid);
            this.Frame1.Controls.Add(this.Label23);
            this.Frame1.Controls.Add(this.Label21);
            this.Frame1.Location = new System.Drawing.Point(468, 90);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(436, 605);
            this.Frame1.TabIndex = 8;
            this.Frame1.Text = "Municipal Valuation Report";
            // 
            // gridIndustrialBldgCodes
            // 
            this.gridIndustrialBldgCodes.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridIndustrialBldgCodes.Cols = 10;
            this.gridIndustrialBldgCodes.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridIndustrialBldgCodes.ExtendLastCol = true;
            this.gridIndustrialBldgCodes.Location = new System.Drawing.Point(20, 408);
            this.gridIndustrialBldgCodes.Name = "gridIndustrialBldgCodes";
            this.gridIndustrialBldgCodes.ReadOnly = false;
            this.gridIndustrialBldgCodes.Rows = 1;
            this.gridIndustrialBldgCodes.ShowFocusCell = false;
            this.gridIndustrialBldgCodes.Size = new System.Drawing.Size(393, 173);
            this.gridIndustrialBldgCodes.TabIndex = 4;
            // 
            // cmbPPIndPropCat
            // 
            this.cmbPPIndPropCat.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmbPPIndPropCat.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPPIndPropCat.Location = new System.Drawing.Point(20, 328);
            this.cmbPPIndPropCat.Name = "cmbPPIndPropCat";
            this.cmbPPIndPropCat.Size = new System.Drawing.Size(393, 40);
            this.cmbPPIndPropCat.TabIndex = 2;
            // 
            // PPGrid
            // 
            this.PPGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.PPGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.PPGrid.ExtendLastCol = true;
            this.PPGrid.Location = new System.Drawing.Point(20, 30);
            this.PPGrid.Name = "PPGrid";
            this.PPGrid.ReadOnly = false;
            this.PPGrid.Rows = 10;
            this.PPGrid.ShowFocusCell = false;
            this.PPGrid.Size = new System.Drawing.Size(393, 253);
            this.PPGrid.TabIndex = 5;
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(20, 300);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(253, 18);
            this.Label23.TabIndex = 1;
            this.Label23.Text = "PP INDUSTRIAL PROPERTY CATEGORY";
            // 
            // Label21
            // 
            this.Label21.Location = new System.Drawing.Point(20, 379);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(230, 24);
            this.Label21.TabIndex = 3;
            this.Label21.Text = "INDUSTRIAL PROPERTY BLDG CODES";
            this.ToolTip1.SetToolTip(this.Label21, "Building Code used to indicate real estate industrial properties");
            // 
            // Frame16
            // 
            this.Frame16.AppearanceKey = "groupBoxNoBorders";
            this.Frame16.Controls.Add(this.chkTreeGrowthCertifiedRatio);
            this.Frame16.Controls.Add(this.chkLandFactorTreeGrowth);
            this.Frame16.Location = new System.Drawing.Point(30, 537);
            this.Frame16.Name = "Frame16";
            this.Frame16.Size = new System.Drawing.Size(302, 69);
            this.Frame16.TabIndex = 10;
            // 
            // chkTreeGrowthCertifiedRatio
            // 
            this.chkTreeGrowthCertifiedRatio.Location = new System.Drawing.Point(0, 37);
            this.chkTreeGrowthCertifiedRatio.Name = "chkTreeGrowthCertifiedRatio";
            this.chkTreeGrowthCertifiedRatio.Size = new System.Drawing.Size(225, 22);
            this.chkTreeGrowthCertifiedRatio.TabIndex = 1;
            this.chkTreeGrowthCertifiedRatio.Text = "Apply certified ratio to tree growth";
            // 
            // chkLandFactorTreeGrowth
            // 
            this.chkLandFactorTreeGrowth.Name = "chkLandFactorTreeGrowth";
            this.chkLandFactorTreeGrowth.Size = new System.Drawing.Size(244, 22);
            this.chkLandFactorTreeGrowth.TabIndex = 2;
            this.chkLandFactorTreeGrowth.Text = "Don\'t apply land factor to tree growth";
            this.ToolTip1.SetToolTip(this.chkLandFactorTreeGrowth, "Don\'t apply the land factors in the land category table to tree growth land types" +
        "");
            // 
            // Frame12
            // 
            this.Frame12.AppearanceKey = "groupBoxNoBorders";
            this.Frame12.Controls.Add(this.txtOpenCode);
            this.Frame12.Controls.Add(this.Label22);
            this.Frame12.Location = new System.Drawing.Point(30, 124);
            this.Frame12.Name = "Frame12";
            this.Frame12.Size = new System.Drawing.Size(341, 42);
            this.Frame12.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.Frame12, "Indicate which influence code is for open space");
            // 
            // txtOpenCode
            // 
            this.txtOpenCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtOpenCode.Location = new System.Drawing.Point(222, 0);
            this.txtOpenCode.MaxLength = 10;
            this.txtOpenCode.Name = "txtOpenCode";
            this.txtOpenCode.Size = new System.Drawing.Size(68, 40);
            this.txtOpenCode.TabIndex = 1;
            this.txtOpenCode.Text = "00";
            this.ToolTip1.SetToolTip(this.txtOpenCode, "Indicate which influence code is for open space");
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(0, 14);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(176, 15);
            this.Label22.TabIndex = 2;
            this.Label22.Text = "OPEN SPACE INFLUENCE CODE";
            this.ToolTip1.SetToolTip(this.Label22, "Indicate which influence code is for open space");
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtComDep);
            this.Frame3.Controls.Add(this.txtMoHoDep);
            this.Frame3.Controls.Add(this.txtREDep);
            this.Frame3.Controls.Add(this.Label12);
            this.Frame3.Controls.Add(this.Label11);
            this.Frame3.Controls.Add(this.Label4);
            this.Frame3.Location = new System.Drawing.Point(30, 290);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(408, 190);
            this.Frame3.TabIndex = 7;
            this.Frame3.Text = "Depreciation";
            // 
            // txtComDep
            // 
            this.txtComDep.BackColor = System.Drawing.SystemColors.Window;
            this.txtComDep.Location = new System.Drawing.Point(315, 130);
            this.txtComDep.MaxLength = 4;
            this.txtComDep.Name = "txtComDep";
            this.txtComDep.Size = new System.Drawing.Size(73, 40);
            this.txtComDep.TabIndex = 5;
            this.txtComDep.Text = "0000";
            this.txtComDep.Enter += new System.EventHandler(this.txtComDep_Enter);
            // 
            // txtMoHoDep
            // 
            this.txtMoHoDep.BackColor = System.Drawing.SystemColors.Window;
            this.txtMoHoDep.Location = new System.Drawing.Point(315, 80);
            this.txtMoHoDep.MaxLength = 4;
            this.txtMoHoDep.Name = "txtMoHoDep";
            this.txtMoHoDep.Size = new System.Drawing.Size(73, 40);
            this.txtMoHoDep.TabIndex = 3;
            this.txtMoHoDep.Text = "0000";
            this.txtMoHoDep.Enter += new System.EventHandler(this.txtMoHoDep_Enter);
            // 
            // txtREDep
            // 
            this.txtREDep.BackColor = System.Drawing.SystemColors.Window;
            this.txtREDep.Location = new System.Drawing.Point(315, 30);
            this.txtREDep.MaxLength = 4;
            this.txtREDep.Name = "txtREDep";
            this.txtREDep.Size = new System.Drawing.Size(73, 40);
            this.txtREDep.TabIndex = 1;
            this.txtREDep.Text = "0000";
            this.txtREDep.Enter += new System.EventHandler(this.txtREDep_Enter);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(20, 144);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(295, 15);
            this.Label12.TabIndex = 4;
            this.Label12.Text = "DEFAULT DEPRECIATION YEAR COMMERCIAL";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(20, 94);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(307, 15);
            this.Label11.TabIndex = 2;
            this.Label11.Text = "DEFAULT DEPRECIATION YEAR MOBILE HOMES";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(20, 44);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(267, 15);
            this.Label4.TabIndex = 6;
            this.Label4.Text = "DEFAULT DEPRECIATION YEAR R/E ONLY";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.txtMixedWood);
            this.Frame5.Controls.Add(this.txtHardWood);
            this.Frame5.Controls.Add(this.txtSoftWood);
            this.Frame5.Controls.Add(this.Label17);
            this.Frame5.Controls.Add(this.Label16);
            this.Frame5.Controls.Add(this.Label15);
            this.Frame5.Controls.Add(this.Label7);
            this.Frame5.Location = new System.Drawing.Point(30, 176);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(408, 104);
            this.Frame5.TabIndex = 6;
            this.Frame5.Text = "Tree Growth Values";
            // 
            // txtMixedWood
            // 
            this.txtMixedWood.BackColor = System.Drawing.SystemColors.Window;
            this.txtMixedWood.Location = new System.Drawing.Point(212, 54);
            this.txtMixedWood.Name = "txtMixedWood";
            this.txtMixedWood.Size = new System.Drawing.Size(64, 40);
            this.txtMixedWood.TabIndex = 4;
            this.txtMixedWood.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMixedWood.Enter += new System.EventHandler(this.txtMixedWood_Enter);
            // 
            // txtHardWood
            // 
            this.txtHardWood.BackColor = System.Drawing.SystemColors.Window;
            this.txtHardWood.Location = new System.Drawing.Point(300, 54);
            this.txtHardWood.Name = "txtHardWood";
            this.txtHardWood.Size = new System.Drawing.Size(88, 40);
            this.txtHardWood.TabIndex = 6;
            this.txtHardWood.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtHardWood.Enter += new System.EventHandler(this.txtHardWood_Enter);
            // 
            // txtSoftWood
            // 
            this.txtSoftWood.BackColor = System.Drawing.SystemColors.Window;
            this.txtSoftWood.Location = new System.Drawing.Point(110, 54);
            this.txtSoftWood.Name = "txtSoftWood";
            this.txtSoftWood.Size = new System.Drawing.Size(77, 40);
            this.txtSoftWood.TabIndex = 2;
            this.txtSoftWood.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtSoftWood.Enter += new System.EventHandler(this.txtSoftWood_Enter);
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(224, 30);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(39, 14);
            this.Label17.TabIndex = 3;
            this.Label17.Text = "MIXED";
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(300, 30);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(88, 16);
            this.Label16.TabIndex = 5;
            this.Label16.Text = "HARD WOOD";
            this.Label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(111, 30);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(84, 14);
            this.Label15.TabIndex = 1;
            this.Label15.Text = "SOFT WOOD";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 68);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(77, 14);
            this.Label7.TabIndex = 7;
            this.Label7.Text = "AMOUNTS";
            // 
            // txtCertifiedRatio
            // 
            this.txtCertifiedRatio.BackColor = System.Drawing.SystemColors.Window;
            this.txtCertifiedRatio.Location = new System.Drawing.Point(252, 74);
            this.txtCertifiedRatio.Name = "txtCertifiedRatio";
            this.txtCertifiedRatio.Size = new System.Drawing.Size(68, 40);
            this.txtCertifiedRatio.TabIndex = 4;
            this.txtCertifiedRatio.Text = "100";
            this.txtCertifiedRatio.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCertifiedRatio.Enter += new System.EventHandler(this.txtCertifiedRatio_Enter);
            // 
            // cboRounding
            // 
            this.cboRounding.BackColor = System.Drawing.SystemColors.Window;
            this.cboRounding.Location = new System.Drawing.Point(252, 24);
            this.cboRounding.Name = "cboRounding";
            this.cboRounding.Size = new System.Drawing.Size(234, 40);
            this.cboRounding.Sorted = true;
            this.cboRounding.TabIndex = 1;
            this.cboRounding.SelectedIndexChanged += new System.EventHandler(this.cboRounding_SelectedIndexChanged);
            // 
            // cmbRoundContingency
            // 
            this.cmbRoundContingency.BackColor = System.Drawing.SystemColors.Window;
            this.cmbRoundContingency.Location = new System.Drawing.Point(528, 24);
            this.cmbRoundContingency.Name = "cmbRoundContingency";
            this.cmbRoundContingency.Size = new System.Drawing.Size(248, 40);
            this.cmbRoundContingency.TabIndex = 2;
            // 
            // Frame10
            // 
            this.Frame10.AppearanceKey = "groupBoxNoBorders";
            this.Frame10.Controls.Add(this.chkRoundExemptions);
            this.Frame10.Location = new System.Drawing.Point(468, 715);
            this.Frame10.Name = "Frame10";
            this.Frame10.Size = new System.Drawing.Size(301, 32);
            this.Frame10.TabIndex = 13;
            this.Frame10.Visible = false;
            // 
            // chkRoundExemptions
            // 
            this.chkRoundExemptions.Name = "chkRoundExemptions";
            this.chkRoundExemptions.Size = new System.Drawing.Size(255, 22);
            this.chkRoundExemptions.TabIndex = 0;
            this.chkRoundExemptions.Text = "Round Calculated Exemption Amounts";
            this.ToolTip1.SetToolTip(this.chkRoundExemptions, "When homestead values must be calculated (certified ratio is not 100) round the h" +
        "omestead amount");
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Location = new System.Drawing.Point(30, 630);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(137, 15);
            this.Label26.TabIndex = 11;
            this.Label26.Text = "DEFAULT SCHEDULE";
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(30, 88);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(139, 15);
            this.Label19.TabIndex = 3;
            this.Label19.Text = "CERTIFIED RATIO (%)";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(219, 15);
            this.Label1.TabIndex = 14;
            this.Label1.Text = "REAL ESTATE ROUNDING OPTION";
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.Frame14);
            this.SSTab1_Page2.Controls.Add(this.Frame13);
            this.SSTab1_Page2.Controls.Add(this.Frame7);
            this.SSTab1_Page2.Controls.Add(this.Frame6);
            this.SSTab1_Page2.Controls.Add(this.cboLandPricing);
            this.SSTab1_Page2.Controls.Add(this.cboValuationReport);
            this.SSTab1_Page2.Controls.Add(this.cboSFLA);
            this.SSTab1_Page2.Controls.Add(this.cboPerAcreValue);
            this.SSTab1_Page2.Controls.Add(this.cmbLabelOption);
            this.SSTab1_Page2.Controls.Add(this.Label9);
            this.SSTab1_Page2.Controls.Add(this.Label5);
            this.SSTab1_Page2.Controls.Add(this.Label2);
            this.SSTab1_Page2.Controls.Add(this.Label3);
            this.SSTab1_Page2.Controls.Add(this.Label20);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Size = new System.Drawing.Size(1181, 769);
            this.SSTab1_Page2.Text = "Printing";
            // 
            // Frame14
            // 
            this.Frame14.Controls.Add(this.chkHideLabelAccount);
            this.Frame14.Controls.Add(this.chkAccountOnBottom);
            this.Frame14.Controls.Add(this.chkPrintMapLotOnSide);
            this.Frame14.Controls.Add(this.chkMapLotSplit);
            this.Frame14.Location = new System.Drawing.Point(443, 378);
            this.Frame14.Name = "Frame14";
            this.Frame14.Size = new System.Drawing.Size(432, 183);
            this.Frame14.TabIndex = 14;
            this.Frame14.Text = "Labels";
            // 
            // chkHideLabelAccount
            // 
            this.chkHideLabelAccount.Location = new System.Drawing.Point(20, 150);
            this.chkHideLabelAccount.Name = "chkHideLabelAccount";
            this.chkHideLabelAccount.Size = new System.Drawing.Size(245, 22);
            this.chkHideLabelAccount.TabIndex = 3;
            this.chkHideLabelAccount.Text = "Don\'t show account on mailing labels";
            this.ToolTip1.SetToolTip(this.chkHideLabelAccount, "On mailing labels don\'t show the account number");
            // 
            // chkAccountOnBottom
            // 
            this.chkAccountOnBottom.Location = new System.Drawing.Point(20, 110);
            this.chkAccountOnBottom.Name = "chkAccountOnBottom";
            this.chkAccountOnBottom.Size = new System.Drawing.Size(198, 22);
            this.chkAccountOnBottom.TabIndex = 2;
            this.chkAccountOnBottom.Text = "Show account on bottom line";
            this.ToolTip1.SetToolTip(this.chkAccountOnBottom, resources.GetString("chkAccountOnBottom.ToolTip"));
            // 
            // chkPrintMapLotOnSide
            // 
            this.chkPrintMapLotOnSide.Location = new System.Drawing.Point(20, 70);
            this.chkPrintMapLotOnSide.Name = "chkPrintMapLotOnSide";
            this.chkPrintMapLotOnSide.Size = new System.Drawing.Size(205, 22);
            this.chkPrintMapLotOnSide.TabIndex = 1;
            this.chkPrintMapLotOnSide.Text = "Split Map/Lot on side of labels";
            this.ToolTip1.SetToolTip(this.chkPrintMapLotOnSide, resources.GetString("chkPrintMapLotOnSide.ToolTip"));
            // 
            // chkMapLotSplit
            // 
            this.chkMapLotSplit.Location = new System.Drawing.Point(20, 30);
            this.chkMapLotSplit.Name = "chkMapLotSplit";
            this.chkMapLotSplit.Size = new System.Drawing.Size(289, 22);
            this.chkMapLotSplit.TabIndex = 4;
            this.chkMapLotSplit.Text = "When splitting, limit maps and lots to 4 chars";
            // 
            // Frame13
            // 
            this.Frame13.Controls.Add(this.cmbF2LabelType);
            this.Frame13.Location = new System.Drawing.Point(443, 182);
            this.Frame13.Name = "Frame13";
            this.Frame13.Size = new System.Drawing.Size(282, 187);
            this.Frame13.TabIndex = 13;
            this.Frame13.Text = "Property Screen Labels";
            // 
            // cmbF2LabelType
            // 
            this.cmbF2LabelType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbF2LabelType.Location = new System.Drawing.Point(20, 30);
            this.cmbF2LabelType.Name = "cmbF2LabelType";
            this.cmbF2LabelType.Size = new System.Drawing.Size(237, 40);
            this.cmbF2LabelType.TabIndex = 0;
            this.cmbF2LabelType.SelectedIndexChanged += new System.EventHandler(this.cmbF2LabelType_SelectedIndexChanged);
            this.cmbF2LabelType.TextChanged += new System.EventHandler(this.cmbF2LabelType_TextChanged);
            // 
            // Frame7
            // 
            this.Frame7.Controls.Add(this.chkShowGridLines);
            this.Frame7.Controls.Add(this.chkDontPrintPropertyPhoto);
            this.Frame7.Controls.Add(this.chkPrintSecondaryPhoto);
            this.Frame7.Controls.Add(this.chkPropertyCardCurrentAmounts);
            this.Frame7.Controls.Add(this.chkPropertyCardComments);
            this.Frame7.Controls.Add(this.chkPropertyCard);
            this.Frame7.Controls.Add(this.chkPrevAssessments);
            this.Frame7.Location = new System.Drawing.Point(20, 182);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(387, 320);
            this.Frame7.TabIndex = 12;
            this.Frame7.Text = "Property Card";
            // 
            // chkShowGridLines
            // 
            this.chkShowGridLines.Location = new System.Drawing.Point(20, 270);
            this.chkShowGridLines.Name = "chkShowGridLines";
            this.chkShowGridLines.Size = new System.Drawing.Size(161, 22);
            this.chkShowGridLines.TabIndex = 6;
            this.chkShowGridLines.Text = "Show sketch grid lines";
            this.ToolTip1.SetToolTip(this.chkShowGridLines, "When printing more than one account, don\'t print sketches or photos. (Conserves m" +
        "emory usage for large batches.)");
            // 
            // chkDontPrintPropertyPhoto
            // 
            this.chkDontPrintPropertyPhoto.Location = new System.Drawing.Point(20, 230);
            this.chkDontPrintPropertyPhoto.Name = "chkDontPrintPropertyPhoto";
            this.chkDontPrintPropertyPhoto.Size = new System.Drawing.Size(253, 22);
            this.chkDontPrintPropertyPhoto.TabIndex = 5;
            this.chkDontPrintPropertyPhoto.Text = "Do not print photo or sketch for ranges";
            this.ToolTip1.SetToolTip(this.chkDontPrintPropertyPhoto, "When printing more than one account, don\'t print sketches or photos. (Conserves m" +
        "emory usage for large batches.)");
            // 
            // chkPrintSecondaryPhoto
            // 
            this.chkPrintSecondaryPhoto.Location = new System.Drawing.Point(20, 190);
            this.chkPrintSecondaryPhoto.Name = "chkPrintSecondaryPhoto";
            this.chkPrintSecondaryPhoto.Size = new System.Drawing.Size(227, 22);
            this.chkPrintSecondaryPhoto.TabIndex = 4;
            this.chkPrintSecondaryPhoto.Text = "Print secondary photo if no sketch";
            this.ToolTip1.SetToolTip(this.chkPrintSecondaryPhoto, "If there is no sketch present, print the secondary photo in the place reserved fo" +
        "r sketches");
            // 
            // chkPropertyCardCurrentAmounts
            // 
            this.chkPropertyCardCurrentAmounts.Location = new System.Drawing.Point(20, 70);
            this.chkPropertyCardCurrentAmounts.Name = "chkPropertyCardCurrentAmounts";
            this.chkPropertyCardCurrentAmounts.Size = new System.Drawing.Size(290, 22);
            this.chkPropertyCardCurrentAmounts.TabIndex = 1;
            this.chkPropertyCardCurrentAmounts.Text = "Always show current year (Current Amounts)";
            this.ToolTip1.SetToolTip(this.chkPropertyCardCurrentAmounts, "On the property card, show the current calculated amounts for the current billing" +
        " year");
            this.chkPropertyCardCurrentAmounts.CheckedChanged += new System.EventHandler(this.chkPropertyCardCurrentAmounts_CheckedChanged);
            // 
            // chkPropertyCardComments
            // 
            this.chkPropertyCardComments.Location = new System.Drawing.Point(20, 150);
            this.chkPropertyCardComments.Name = "chkPropertyCardComments";
            this.chkPropertyCardComments.Size = new System.Drawing.Size(167, 22);
            this.chkPropertyCardComments.TabIndex = 3;
            this.chkPropertyCardComments.Text = "Do not show comments";
            // 
            // chkPropertyCard
            // 
            this.chkPropertyCard.Location = new System.Drawing.Point(20, 30);
            this.chkPropertyCard.Name = "chkPropertyCard";
            this.chkPropertyCard.Size = new System.Drawing.Size(281, 22);
            this.chkPropertyCard.TabIndex = 7;
            this.chkPropertyCard.Text = "Always show current year (Billing Amounts)";
            this.ToolTip1.SetToolTip(this.chkPropertyCard, "On the property card, show the current billing amounts as the current billing yea" +
        "r amounts even if archived info exists");
            this.chkPropertyCard.CheckedChanged += new System.EventHandler(this.chkPropertyCard_CheckedChanged);
            // 
            // chkPrevAssessments
            // 
            this.chkPrevAssessments.Location = new System.Drawing.Point(20, 110);
            this.chkPrevAssessments.Name = "chkPrevAssessments";
            this.chkPrevAssessments.Size = new System.Drawing.Size(236, 22);
            this.chkPrevAssessments.TabIndex = 2;
            this.chkPrevAssessments.Text = "Do not show previous assessments";
            // 
            // Frame6
            // 
            this.Frame6.Controls.Add(this.chkHideRatio);
            this.Frame6.Controls.Add(this.chkShowFirstPic);
            this.Frame6.Controls.Add(this.chkShowPic);
            this.Frame6.Controls.Add(this.chkShowSketch);
            this.Frame6.Location = new System.Drawing.Point(763, 182);
            this.Frame6.Name = "Frame6";
            this.Frame6.Size = new System.Drawing.Size(350, 187);
            this.Frame6.TabIndex = 15;
            this.Frame6.Text = "Valuation Report";
            // 
            // chkHideRatio
            // 
            this.chkHideRatio.Location = new System.Drawing.Point(20, 110);
            this.chkHideRatio.Name = "chkHideRatio";
            this.chkHideRatio.Size = new System.Drawing.Size(92, 22);
            this.chkHideRatio.TabIndex = 2;
            this.chkHideRatio.Text = "Hide Ratio";
            // 
            // chkShowFirstPic
            // 
            this.chkShowFirstPic.Location = new System.Drawing.Point(183, 30);
            this.chkShowFirstPic.Name = "chkShowFirstPic";
            this.chkShowFirstPic.Size = new System.Drawing.Size(123, 22);
            this.chkShowFirstPic.TabIndex = 3;
            this.chkShowFirstPic.Text = "Show First Only";
            this.ToolTip1.SetToolTip(this.chkShowFirstPic, "Show only the first picture");
            this.chkShowFirstPic.Visible = false;
            // 
            // chkShowPic
            // 
            this.chkShowPic.Location = new System.Drawing.Point(20, 30);
            this.chkShowPic.Name = "chkShowPic";
            this.chkShowPic.Size = new System.Drawing.Size(123, 22);
            this.chkShowPic.TabIndex = 4;
            this.chkShowPic.Text = "Show Picture(s)";
            this.chkShowPic.CheckedChanged += new System.EventHandler(this.chkShowPic_CheckedChanged);
            // 
            // chkShowSketch
            // 
            this.chkShowSketch.Location = new System.Drawing.Point(20, 70);
            this.chkShowSketch.Name = "chkShowSketch";
            this.chkShowSketch.Size = new System.Drawing.Size(107, 22);
            this.chkShowSketch.TabIndex = 1;
            this.chkShowSketch.Text = "Show Sketch";
            // 
            // cboLandPricing
            // 
            this.cboLandPricing.BackColor = System.Drawing.SystemColors.Window;
            this.cboLandPricing.Location = new System.Drawing.Point(289, 30);
            this.cboLandPricing.Name = "cboLandPricing";
            this.cboLandPricing.Size = new System.Drawing.Size(347, 40);
            this.cboLandPricing.Sorted = true;
            this.cboLandPricing.TabIndex = 1;
            // 
            // cboValuationReport
            // 
            this.cboValuationReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboValuationReport.Location = new System.Drawing.Point(289, 80);
            this.cboValuationReport.Name = "cboValuationReport";
            this.cboValuationReport.Size = new System.Drawing.Size(347, 40);
            this.cboValuationReport.Sorted = true;
            this.cboValuationReport.TabIndex = 3;
            // 
            // cboSFLA
            // 
            this.cboSFLA.BackColor = System.Drawing.SystemColors.Window;
            this.cboSFLA.Location = new System.Drawing.Point(289, 130);
            this.cboSFLA.Name = "cboSFLA";
            this.cboSFLA.Size = new System.Drawing.Size(347, 40);
            this.cboSFLA.Sorted = true;
            this.cboSFLA.TabIndex = 5;
            // 
            // cboPerAcreValue
            // 
            this.cboPerAcreValue.BackColor = System.Drawing.SystemColors.Window;
            this.cboPerAcreValue.Location = new System.Drawing.Point(842, 30);
            this.cboPerAcreValue.Name = "cboPerAcreValue";
            this.cboPerAcreValue.Size = new System.Drawing.Size(299, 40);
            this.cboPerAcreValue.Sorted = true;
            this.cboPerAcreValue.TabIndex = 7;
            // 
            // cmbLabelOption
            // 
            this.cmbLabelOption.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLabelOption.Items.AddRange(new object[] {
            "Use Reference 1",
            "Use Book & Page"});
            this.cmbLabelOption.Location = new System.Drawing.Point(842, 80);
            this.cmbLabelOption.Name = "cmbLabelOption";
            this.cmbLabelOption.Size = new System.Drawing.Size(299, 40);
            this.cmbLabelOption.TabIndex = 11;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(30, 44);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(231, 15);
            this.Label9.TabIndex = 16;
            this.Label9.Text = "LAND PRICING/UNIT PRINT FORMAT";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(30, 94);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(188, 15);
            this.Label5.TabIndex = 2;
            this.Label5.Text = "VALUATION REPORT OPTION";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(30, 144);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(266, 15);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "$ PER SQUARE FEET LIVING AREA (SFLA)";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(671, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(170, 15);
            this.Label3.TabIndex = 6;
            this.Label3.Text = "PER ACRE VALUE OPTION";
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(671, 94);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(103, 18);
            this.Label20.TabIndex = 10;
            this.Label20.Text = "LABEL OPTION";
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.Frame9);
            this.SSTab1_Page3.Controls.Add(this.Label30);
            this.SSTab1_Page3.Controls.Add(this.Label31);
            this.SSTab1_Page3.Controls.Add(this.Frame2);
            this.SSTab1_Page3.Controls.Add(this.Frame4);
            this.SSTab1_Page3.Controls.Add(this.chkShowCode);
            this.SSTab1_Page3.Controls.Add(this.chkToolTips);
            this.SSTab1_Page3.Controls.Add(this.chkShortDesc);
            this.SSTab1_Page3.Controls.Add(this.txtRef1Label);
            this.SSTab1_Page3.Controls.Add(this.txtRef2Label);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Size = new System.Drawing.Size(1181, 769);
            this.SSTab1_Page3.Text = "Misc. Settings";
            // 
            // Frame9
            // 
            this.Frame9.Controls.Add(this.cmdBrowsePocketPC);
            this.Frame9.Controls.Add(this.txtPocketPCDirectory);
            this.Frame9.Controls.Add(this.chkPocketPCExternal);
            this.Frame9.Location = new System.Drawing.Point(304, 294);
            this.Frame9.Name = "Frame9";
            this.Frame9.Size = new System.Drawing.Size(527, 128);
            this.Frame9.TabIndex = 7;
            this.Frame9.Text = "Pocket Pc";
            this.Frame9.Visible = false;
            // 
            // cmdBrowsePocketPC
            // 
            this.cmdBrowsePocketPC.AppearanceKey = "actionButton";
            this.cmdBrowsePocketPC.ForeColor = System.Drawing.Color.White;
            this.cmdBrowsePocketPC.Location = new System.Drawing.Point(20, 70);
            this.cmdBrowsePocketPC.Name = "cmdBrowsePocketPC";
            this.cmdBrowsePocketPC.Size = new System.Drawing.Size(98, 40);
            this.cmdBrowsePocketPC.TabIndex = 1;
            this.cmdBrowsePocketPC.Text = "Browse";
            this.cmdBrowsePocketPC.Click += new System.EventHandler(this.cmdBrowsePocketPC_Click);
            // 
            // txtPocketPCDirectory
            // 
            this.txtPocketPCDirectory.BackColor = System.Drawing.SystemColors.Window;
            this.txtPocketPCDirectory.Location = new System.Drawing.Point(20, 60);
            this.txtPocketPCDirectory.Name = "txtPocketPCDirectory";
            this.txtPocketPCDirectory.Size = new System.Drawing.Size(424, 40);
            this.txtPocketPCDirectory.TabIndex = 2;
            // 
            // chkPocketPCExternal
            // 
            this.chkPocketPCExternal.Location = new System.Drawing.Point(20, 30);
            this.chkPocketPCExternal.Name = "chkPocketPCExternal";
            this.chkPocketPCExternal.Size = new System.Drawing.Size(466, 22);
            this.chkPocketPCExternal.TabIndex = 3;
            this.chkPocketPCExternal.Text = "Use external program and special directory to communicate with Pocket PC";
            this.ToolTip1.SetToolTip(this.chkPocketPCExternal, "This option is for networks setup with or like Terminal Services that cannot comm" +
        "unicate with a Pocket PC any other way.");
            // 
            // Label30
            // 
            this.Label30.Location = new System.Drawing.Point(30, 244);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(81, 16);
            this.Label30.TabIndex = 1;
            this.Label30.Text = "REF 1 LABEL";
            // 
            // Label31
            // 
            this.Label31.Location = new System.Drawing.Point(361, 244);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(81, 16);
            this.Label31.TabIndex = 3;
            this.Label31.Text = "REF 2 LABEL";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtZip);
            this.Frame2.Controls.Add(this.txtCity);
            this.Frame2.Controls.Add(this.txtState);
            this.Frame2.Controls.Add(this.Label10);
            this.Frame2.Controls.Add(this.Label18);
            this.Frame2.Controls.Add(this.Label13);
            this.Frame2.Location = new System.Drawing.Point(20, 30);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(352, 187);
            this.Frame2.TabIndex = 4;
            this.Frame2.Text = "City/State/Zip Defaults";
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Location = new System.Drawing.Point(89, 130);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(122, 40);
            this.txtZip.TabIndex = 5;
            this.txtZip.Enter += new System.EventHandler(this.txtZip_Enter);
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(89, 30);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(242, 40);
            this.txtCity.TabIndex = 1;
            this.txtCity.Enter += new System.EventHandler(this.txtCity_Enter);
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.Location = new System.Drawing.Point(89, 80);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(61, 40);
            this.txtState.TabIndex = 3;
            this.txtState.Enter += new System.EventHandler(this.txtState_Enter);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 144);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(41, 18);
            this.Label10.TabIndex = 4;
            this.Label10.Text = "ZIP";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(20, 44);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(66, 16);
            this.Label18.TabIndex = 6;
            this.Label18.Text = "CITY";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 94);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(54, 14);
            this.Label13.TabIndex = 2;
            this.Label13.Text = "STATE";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.txtBillYear);
            this.Frame4.Controls.Add(this.txtBillRate);
            this.Frame4.Controls.Add(this.Label6);
            this.Frame4.Controls.Add(this.Label8);
            this.Frame4.Location = new System.Drawing.Point(399, 30);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(269, 136);
            this.Frame4.TabIndex = 5;
            this.Frame4.Text = "Billing Information";
            // 
            // txtBillYear
            // 
            this.txtBillYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtBillYear.Location = new System.Drawing.Point(145, 30);
            this.txtBillYear.MaxLength = 4;
            this.txtBillYear.Name = "txtBillYear";
            this.txtBillYear.Size = new System.Drawing.Size(104, 40);
            this.txtBillYear.TabIndex = 1;
            this.txtBillYear.Text = "0000";
            this.ToolTip1.SetToolTip(this.txtBillYear, "Billing Year is used to save ");
            this.txtBillYear.Enter += new System.EventHandler(this.txtBillYear_Enter);
            // 
            // txtBillRate
            // 
            this.txtBillRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtBillRate.Location = new System.Drawing.Point(145, 80);
            this.txtBillRate.MaxLength = 8;
            this.txtBillRate.Name = "txtBillRate";
            this.txtBillRate.Size = new System.Drawing.Size(104, 40);
            this.txtBillRate.TabIndex = 3;
            this.txtBillRate.Text = "0";
            this.txtBillRate.Enter += new System.EventHandler(this.txtBillRate_Enter);
            this.txtBillRate.KeyUp += new Wisej.Web.KeyEventHandler(this.txtBillRate_KeyUp);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(20, 44);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(92, 15);
            this.Label6.TabIndex = 4;
            this.Label6.Text = "BILLING YEAR";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(20, 94);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(92, 15);
            this.Label8.TabIndex = 2;
            this.Label8.Text = "BILLING RATE";
            // 
            // chkShowCode
            // 
            this.chkShowCode.Location = new System.Drawing.Point(20, 294);
            this.chkShowCode.Name = "chkShowCode";
            this.chkShowCode.Size = new System.Drawing.Size(250, 22);
            this.chkShowCode.TabIndex = 8;
            this.chkShowCode.Text = "Use code numbers on account screen";
            this.ToolTip1.SetToolTip(this.chkShowCode, "Select drop-downs will show the code.  Typing the description will no longer jump" +
        " to that selection");
            // 
            // chkToolTips
            // 
            this.chkToolTips.Location = new System.Drawing.Point(20, 374);
            this.chkToolTips.Name = "chkToolTips";
            this.chkToolTips.Size = new System.Drawing.Size(185, 22);
            this.chkToolTips.TabIndex = 10;
            this.chkToolTips.Text = "Disable quick help tool tips";
            this.ToolTip1.SetToolTip(this.chkToolTips, "Select drop-downs will show the code.  Typing the description will no longer jump" +
        " to that selection");
            this.chkToolTips.Visible = false;
            this.chkToolTips.CheckedChanged += new System.EventHandler(this.chkToolTips_CheckedChanged);
            // 
            // chkShortDesc
            // 
            this.chkShortDesc.Location = new System.Drawing.Point(20, 334);
            this.chkShortDesc.Name = "chkShortDesc";
            this.chkShortDesc.Size = new System.Drawing.Size(245, 22);
            this.chkShortDesc.TabIndex = 9;
            this.chkShortDesc.Text = "Show short description for land types";
            this.ToolTip1.SetToolTip(this.chkShortDesc, "Select drop-downs will show the code.  Typing the description will no longer jump" +
        " to that selection");
            // 
            // txtRef1Label
            // 
            this.txtRef1Label.BackColor = System.Drawing.SystemColors.Window;
            this.txtRef1Label.Location = new System.Drawing.Point(131, 229);
            this.txtRef1Label.Name = "txtRef1Label";
            this.txtRef1Label.Size = new System.Drawing.Size(195, 40);
            this.txtRef1Label.TabIndex = 2;
            // 
            // txtRef2Label
            // 
            this.txtRef2Label.BackColor = System.Drawing.SystemColors.Window;
            this.txtRef2Label.Location = new System.Drawing.Point(470, 230);
            this.txtRef2Label.Name = "txtRef2Label";
            this.txtRef2Label.Size = new System.Drawing.Size(198, 40);
            this.txtRef2Label.TabIndex = 4;
            // 
            // dlg1
            // 
            this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dlg1.FontName = "Microsoft Sans Serif";
            this.dlg1.Name = "dlg1";
            this.dlg1.Size = new System.Drawing.Size(0, 0);
            // 
            // gridTownCode
            // 
            this.gridTownCode.Cols = 1;
            this.gridTownCode.ColumnHeadersVisible = false;
            this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTownCode.ExtendLastCol = true;
            this.gridTownCode.FixedCols = 0;
            this.gridTownCode.FixedRows = 0;
            this.gridTownCode.Location = new System.Drawing.Point(464, 30);
            this.gridTownCode.Name = "gridTownCode";
            this.gridTownCode.ReadOnly = false;
            this.gridTownCode.RowHeadersVisible = false;
            this.gridTownCode.Rows = 1;
            this.gridTownCode.ShowFocusCell = false;
            this.gridTownCode.Size = new System.Drawing.Size(144, 19);
            this.gridTownCode.TabIndex = 4;
            this.gridTownCode.Visible = false;
            this.gridTownCode.ComboCloseUp += new System.EventHandler(this.gridTownCode_ComboCloseUp);
            this.gridTownCode.Leave += new System.EventHandler(this.gridTownCode_Leave);
            // 
            // lblCardsUsed
            // 
            this.lblCardsUsed.Location = new System.Drawing.Point(365, 30);
            this.lblCardsUsed.Name = "lblCardsUsed";
            this.lblCardsUsed.Size = new System.Drawing.Size(74, 19);
            this.lblCardsUsed.TabIndex = 3;
            this.lblCardsUsed.Visible = false;
            // 
            // Label25
            // 
            this.Label25.Location = new System.Drawing.Point(250, 30);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(90, 20);
            this.Label25.TabIndex = 2;
            this.Label25.Text = "CARDS USED";
            this.Label25.Visible = false;
            // 
            // lblMaxCards
            // 
            this.lblMaxCards.Location = new System.Drawing.Point(134, 30);
            this.lblMaxCards.Name = "lblMaxCards";
            this.lblMaxCards.Size = new System.Drawing.Size(74, 19);
            this.lblMaxCards.TabIndex = 1;
            this.lblMaxCards.Visible = false;
            // 
            // Label24
            // 
            this.Label24.Location = new System.Drawing.Point(30, 30);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(82, 20);
            this.Label24.TabIndex = 41;
            this.Label24.Text = "MAX CARDS";
            this.Label24.Visible = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuCommercialStatistics,
            this.separ2,
            this.mnuSave});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuCommercialStatistics
            // 
            this.mnuCommercialStatistics.Index = 0;
            this.mnuCommercialStatistics.Name = "mnuCommercialStatistics";
            this.mnuCommercialStatistics.Text = "Commercial Statistics";
            this.mnuCommercialStatistics.Click += new System.EventHandler(this.mnuCommercialStatistics_Click);
            // 
            // separ2
            // 
            this.separ2.Index = 1;
            this.separ2.Name = "separ2";
            this.separ2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 2;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = -1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuQuit
            // 
            this.mnuQuit.Index = -1;
            this.mnuQuit.Name = "mnuQuit";
            this.mnuQuit.Text = "Exit";
            this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(445, 10);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(86, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdCommercialStatistics
            // 
            this.cmdCommercialStatistics.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCommercialStatistics.Location = new System.Drawing.Point(1031, 29);
            this.cmdCommercialStatistics.Name = "cmdCommercialStatistics";
            this.cmdCommercialStatistics.Size = new System.Drawing.Size(152, 24);
            this.cmdCommercialStatistics.TabIndex = 1;
            this.cmdCommercialStatistics.Text = "Commercial Statistics";
            this.cmdCommercialStatistics.Click += new System.EventHandler(this.mnuCommercialStatistics_Click);
            // 
            // frmREOptions
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1213, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmREOptions";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Customize";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmREOptions_Load);
            this.Activated += new System.EventHandler(this.frmREOptions_Activated);
            this.Resize += new System.EventHandler(this.frmREOptions_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmREOptions_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmREOptions_KeyPress);
            this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmREOptions_KeyUp);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            this.SSTab1_Page1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkHomesteadCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDefaultSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridIndustrialBldgCodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PPGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame16)).EndInit();
            this.Frame16.ResumeLayout(false);
            this.Frame16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTreeGrowthCertifiedRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLandFactorTreeGrowth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame12)).EndInit();
            this.Frame12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame10)).EndInit();
            this.Frame10.ResumeLayout(false);
            this.Frame10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRoundExemptions)).EndInit();
            this.SSTab1_Page2.ResumeLayout(false);
            this.SSTab1_Page2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame14)).EndInit();
            this.Frame14.ResumeLayout(false);
            this.Frame14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkHideLabelAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAccountOnBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintMapLotOnSide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMapLotSplit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame13)).EndInit();
            this.Frame13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            this.Frame7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowGridLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDontPrintPropertyPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintSecondaryPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropertyCardCurrentAmounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropertyCardComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropertyCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrevAssessments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            this.Frame6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkHideRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowFirstPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowSketch)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            this.SSTab1_Page3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame9)).EndInit();
            this.Frame9.ResumeLayout(false);
            this.Frame9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowsePocketPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPocketPCExternal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkToolTips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShortDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCommercialStatistics)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdCommercialStatistics;
	}
}
