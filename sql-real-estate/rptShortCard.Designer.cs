﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptShortCard.
	/// </summary>
	partial class rptShortCard
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptShortCard));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddr1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddr2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddr3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTelephone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRef1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRef2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTranCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTranDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt1Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt2Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt3Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen1Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen2Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInspectionDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEntranceDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInformationDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoftWoodAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSoftValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHardWoodAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHardValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixedAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixedValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAcres = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeedName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeedName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRef1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRef2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTranCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTranDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt3Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInspectionDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftWoodAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardWoodAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAcres)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeedName1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeedName2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field2,
            this.txtAddr1,
            this.txtAddr2,
            this.txtAddr3,
            this.Field4,
            this.txtLocation,
            this.Field5,
            this.txtTelephone,
            this.Field6,
            this.Field7,
            this.txtRef1,
            this.txtRef2,
            this.Field8,
            this.Field9,
            this.Field10,
            this.txtLandCode,
            this.txtBldgCode,
            this.txtTranCode,
            this.txtLandDesc,
            this.txtBldgDesc,
            this.txtTranDesc,
            this.Field14,
            this.Field15,
            this.Field16,
            this.txtExempt1,
            this.txtExempt2,
            this.txtExempt3,
            this.txtExempt1Desc,
            this.txtExempt2Desc,
            this.txtExempt3Desc,
            this.Field17,
            this.txtMapLot,
            this.txtOpen1Desc,
            this.txtOpen1,
            this.txtOpen2Desc,
            this.txtOpen2,
            this.Field18,
            this.Field19,
            this.Field20,
            this.txtEntranceCode,
            this.txtInformationCode,
            this.txtInspectionDate,
            this.txtEntranceDescription,
            this.txtInformationDescription,
            this.Field21,
            this.Field22,
            this.Field23,
            this.Field24,
            this.Field25,
            this.Field26,
            this.Field27,
            this.txtSoftWoodAcres,
            this.txtSoftValue,
            this.txtHardWoodAcres,
            this.txtHardValue,
            this.txtMixedAcres,
            this.txtMixedValue,
            this.txtOtherAcres,
            this.txtOtherValue,
            this.txtTotalAcres,
            this.Field28,
            this.Field29,
            this.Field30,
            this.txtLand,
            this.txtBuilding,
            this.txtExemption,
            this.Field34,
            this.Field35,
            this.Field36,
            this.Field37,
            this.Field38,
            this.txtLandAcct,
            this.txtBldgAcct,
            this.txtExemptAcct,
            this.Field39,
            this.txtTaxable,
            this.Field41,
            this.Field42,
            this.Field43,
            this.txtTaxRate,
            this.txtTax,
            this.Field44,
            this.txtBookPage,
            this.txtDeedName1,
            this.textBox2,
            this.textBox3,
            this.txtDeedName2});
			this.Detail.Height = 4.927083F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Field2
			// 
			this.Field2.Height = 0.19F;
			this.Field2.Left = 0.0625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field2.Text = "Address";
			this.Field2.Top = 0.46875F;
			this.Field2.Width = 2.5F;
			// 
			// txtAddr1
			// 
			this.txtAddr1.Height = 0.19F;
			this.txtAddr1.Left = 0.0625F;
			this.txtAddr1.Name = "txtAddr1";
			this.txtAddr1.Style = "font-family: \'Tahoma\'";
			this.txtAddr1.Text = null;
			this.txtAddr1.Top = 0.625F;
			this.txtAddr1.Width = 2.5F;
			// 
			// txtAddr2
			// 
			this.txtAddr2.Height = 0.19F;
			this.txtAddr2.Left = 0.0625F;
			this.txtAddr2.Name = "txtAddr2";
			this.txtAddr2.Style = "font-family: \'Tahoma\'";
			this.txtAddr2.Text = null;
			this.txtAddr2.Top = 0.78125F;
			this.txtAddr2.Visible = false;
			this.txtAddr2.Width = 2.5F;
			// 
			// txtAddr3
			// 
			this.txtAddr3.Height = 0.19F;
			this.txtAddr3.Left = 0.0625F;
			this.txtAddr3.Name = "txtAddr3";
			this.txtAddr3.Style = "font-family: \'Tahoma\'";
			this.txtAddr3.Text = null;
			this.txtAddr3.Top = 0.9375F;
			this.txtAddr3.Width = 2.5F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.19F;
			this.Field4.Left = 2.625F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field4.Text = "Location";
			this.Field4.Top = 0.46875F;
			this.Field4.Width = 2.3125F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 2.625F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'";
			this.txtLocation.Text = "Field1";
			this.txtLocation.Top = 0.625F;
			this.txtLocation.Width = 2.3125F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.19F;
			this.Field5.Left = 2.625F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field5.Text = "Telephone";
			this.Field5.Top = 0.78125F;
			this.Field5.Width = 2F;
			// 
			// txtTelephone
			// 
			this.txtTelephone.Height = 0.19F;
			this.txtTelephone.Left = 2.625F;
			this.txtTelephone.MultiLine = false;
			this.txtTelephone.Name = "txtTelephone";
			this.txtTelephone.Style = "font-family: \'Tahoma\'";
			this.txtTelephone.Text = null;
			this.txtTelephone.Top = 0.9375F;
			this.txtTelephone.Width = 2F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.19F;
			this.Field6.Left = 5F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field6.Text = "Reference 1";
			this.Field6.Top = 0.46875F;
			this.Field6.Width = 2.4375F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.19F;
			this.Field7.Left = 5F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field7.Text = "Reference 2";
			this.Field7.Top = 0.78125F;
			this.Field7.Width = 2.4375F;
			// 
			// txtRef1
			// 
			this.txtRef1.Height = 0.19F;
			this.txtRef1.Left = 5F;
			this.txtRef1.MultiLine = false;
			this.txtRef1.Name = "txtRef1";
			this.txtRef1.Style = "font-family: \'Tahoma\'";
			this.txtRef1.Text = null;
			this.txtRef1.Top = 0.625F;
			this.txtRef1.Width = 2.4375F;
			// 
			// txtRef2
			// 
			this.txtRef2.Height = 0.19F;
			this.txtRef2.Left = 5F;
			this.txtRef2.MultiLine = false;
			this.txtRef2.Name = "txtRef2";
			this.txtRef2.Style = "font-family: \'Tahoma\'";
			this.txtRef2.Text = null;
			this.txtRef2.Top = 0.9375F;
			this.txtRef2.Width = 2.4375F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.19F;
			this.Field8.Left = 0.062F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field8.Text = "Land Code";
			this.Field8.Top = 1.638F;
			this.Field8.Width = 1.25F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.19F;
			this.Field9.Left = 0.062F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field9.Text = "Building Code";
			this.Field9.Top = 1.794251F;
			this.Field9.Width = 1.25F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.19F;
			this.Field10.Left = 0.062F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field10.Text = "Tran Code";
			this.Field10.Top = 1.950503F;
			this.Field10.Width = 1.25F;
			// 
			// txtLandCode
			// 
			this.txtLandCode.Height = 0.19F;
			this.txtLandCode.Left = 1.312F;
			this.txtLandCode.Name = "txtLandCode";
			this.txtLandCode.Style = "font-family: \'Tahoma\'";
			this.txtLandCode.Text = "Field11";
			this.txtLandCode.Top = 1.638F;
			this.txtLandCode.Width = 0.375F;
			// 
			// txtBldgCode
			// 
			this.txtBldgCode.Height = 0.19F;
			this.txtBldgCode.Left = 1.312F;
			this.txtBldgCode.Name = "txtBldgCode";
			this.txtBldgCode.Style = "font-family: \'Tahoma\'";
			this.txtBldgCode.Text = "Field11";
			this.txtBldgCode.Top = 1.794251F;
			this.txtBldgCode.Width = 0.375F;
			// 
			// txtTranCode
			// 
			this.txtTranCode.Height = 0.19F;
			this.txtTranCode.Left = 1.312F;
			this.txtTranCode.Name = "txtTranCode";
			this.txtTranCode.Style = "font-family: \'Tahoma\'";
			this.txtTranCode.Text = "Field11";
			this.txtTranCode.Top = 1.950503F;
			this.txtTranCode.Width = 0.375F;
			// 
			// txtLandDesc
			// 
			this.txtLandDesc.Height = 0.19F;
			this.txtLandDesc.Left = 1.7495F;
			this.txtLandDesc.Name = "txtLandDesc";
			this.txtLandDesc.Style = "font-family: \'Tahoma\'";
			this.txtLandDesc.Text = "Field11";
			this.txtLandDesc.Top = 1.638F;
			this.txtLandDesc.Width = 1.625F;
			// 
			// txtBldgDesc
			// 
			this.txtBldgDesc.Height = 0.19F;
			this.txtBldgDesc.Left = 1.7495F;
			this.txtBldgDesc.Name = "txtBldgDesc";
			this.txtBldgDesc.Style = "font-family: \'Tahoma\'";
			this.txtBldgDesc.Text = "Field12";
			this.txtBldgDesc.Top = 1.794251F;
			this.txtBldgDesc.Width = 1.625F;
			// 
			// txtTranDesc
			// 
			this.txtTranDesc.Height = 0.19F;
			this.txtTranDesc.Left = 1.7495F;
			this.txtTranDesc.Name = "txtTranDesc";
			this.txtTranDesc.Style = "font-family: \'Tahoma\'";
			this.txtTranDesc.Text = "Field13";
			this.txtTranDesc.Top = 1.950503F;
			this.txtTranDesc.Width = 1.625F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.19F;
			this.Field14.Left = 3.4995F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field14.Text = "Exempt Code 1";
			this.Field14.Top = 1.638F;
			this.Field14.Width = 1.25F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.19F;
			this.Field15.Left = 3.4995F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field15.Text = "Exempt Code 2";
			this.Field15.Top = 1.794251F;
			this.Field15.Width = 1.25F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.19F;
			this.Field16.Left = 3.4995F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field16.Text = "Exempt Code 3";
			this.Field16.Top = 1.950503F;
			this.Field16.Width = 1.25F;
			// 
			// txtExempt1
			// 
			this.txtExempt1.Height = 0.19F;
			this.txtExempt1.Left = 4.7495F;
			this.txtExempt1.Name = "txtExempt1";
			this.txtExempt1.Style = "font-family: \'Tahoma\'";
			this.txtExempt1.Text = "Field11";
			this.txtExempt1.Top = 1.638F;
			this.txtExempt1.Width = 0.375F;
			// 
			// txtExempt2
			// 
			this.txtExempt2.Height = 0.19F;
			this.txtExempt2.Left = 4.7495F;
			this.txtExempt2.Name = "txtExempt2";
			this.txtExempt2.Style = "font-family: \'Tahoma\'";
			this.txtExempt2.Text = "Field11";
			this.txtExempt2.Top = 1.794251F;
			this.txtExempt2.Width = 0.375F;
			// 
			// txtExempt3
			// 
			this.txtExempt3.Height = 0.19F;
			this.txtExempt3.Left = 4.7495F;
			this.txtExempt3.Name = "txtExempt3";
			this.txtExempt3.Style = "font-family: \'Tahoma\'";
			this.txtExempt3.Text = "Field11";
			this.txtExempt3.Top = 1.950503F;
			this.txtExempt3.Width = 0.375F;
			// 
			// txtExempt1Desc
			// 
			this.txtExempt1Desc.Height = 0.19F;
			this.txtExempt1Desc.Left = 5.187F;
			this.txtExempt1Desc.Name = "txtExempt1Desc";
			this.txtExempt1Desc.Style = "font-family: \'Tahoma\'";
			this.txtExempt1Desc.Text = "Field20";
			this.txtExempt1Desc.Top = 1.638F;
			this.txtExempt1Desc.Width = 2.25F;
			// 
			// txtExempt2Desc
			// 
			this.txtExempt2Desc.Height = 0.19F;
			this.txtExempt2Desc.Left = 5.187F;
			this.txtExempt2Desc.Name = "txtExempt2Desc";
			this.txtExempt2Desc.Style = "font-family: \'Tahoma\'";
			this.txtExempt2Desc.Text = "Field21";
			this.txtExempt2Desc.Top = 1.794251F;
			this.txtExempt2Desc.Width = 2.25F;
			// 
			// txtExempt3Desc
			// 
			this.txtExempt3Desc.Height = 0.19F;
			this.txtExempt3Desc.Left = 5.187F;
			this.txtExempt3Desc.Name = "txtExempt3Desc";
			this.txtExempt3Desc.Style = "font-family: \'Tahoma\'";
			this.txtExempt3Desc.Text = "Field22";
			this.txtExempt3Desc.Top = 1.950503F;
			this.txtExempt3Desc.Width = 2.25F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.19F;
			this.Field17.Left = 5F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field17.Text = "Map / Lot";
			this.Field17.Top = 0F;
			this.Field17.Width = 2.4375F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 5F;
			this.txtMapLot.MultiLine = false;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-family: \'Tahoma\'";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0.15625F;
			this.txtMapLot.Width = 2.4375F;
			// 
			// txtOpen1Desc
			// 
			this.txtOpen1Desc.Height = 0.19F;
			this.txtOpen1Desc.Left = 0.062F;
			this.txtOpen1Desc.Name = "txtOpen1Desc";
			this.txtOpen1Desc.Style = "font-family: \'Tahoma\'";
			this.txtOpen1Desc.Text = "Field18";
			this.txtOpen1Desc.Top = 2.263003F;
			this.txtOpen1Desc.Width = 1.8125F;
			// 
			// txtOpen1
			// 
			this.txtOpen1.Height = 0.19F;
			this.txtOpen1.Left = 1.937F;
			this.txtOpen1.Name = "txtOpen1";
			this.txtOpen1.Style = "font-family: \'Tahoma\'";
			this.txtOpen1.Text = "Field19";
			this.txtOpen1.Top = 2.263003F;
			this.txtOpen1.Width = 0.5625F;
			// 
			// txtOpen2Desc
			// 
			this.txtOpen2Desc.Height = 0.19F;
			this.txtOpen2Desc.Left = 0.062F;
			this.txtOpen2Desc.Name = "txtOpen2Desc";
			this.txtOpen2Desc.Style = "font-family: \'Tahoma\'";
			this.txtOpen2Desc.Text = "Field18";
			this.txtOpen2Desc.Top = 2.419253F;
			this.txtOpen2Desc.Width = 1.8125F;
			// 
			// txtOpen2
			// 
			this.txtOpen2.Height = 0.19F;
			this.txtOpen2.Left = 1.937F;
			this.txtOpen2.Name = "txtOpen2";
			this.txtOpen2.Style = "font-family: \'Tahoma\'";
			this.txtOpen2.Text = "Field19";
			this.txtOpen2.Top = 2.419253F;
			this.txtOpen2.Width = 0.5625F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.19F;
			this.Field18.Left = 2.6245F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field18.Text = "Entrance";
			this.Field18.Top = 2.263003F;
			this.Field18.Width = 1.25F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.19F;
			this.Field19.Left = 2.6245F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field19.Text = "Information";
			this.Field19.Top = 2.419253F;
			this.Field19.Width = 1.25F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.19F;
			this.Field20.Left = 2.6245F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field20.Text = "Last Inspected";
			this.Field20.Top = 2.575503F;
			this.Field20.Width = 1.3125F;
			// 
			// txtEntranceCode
			// 
			this.txtEntranceCode.Height = 0.19F;
			this.txtEntranceCode.Left = 3.9995F;
			this.txtEntranceCode.Name = "txtEntranceCode";
			this.txtEntranceCode.Style = "font-family: \'Tahoma\'";
			this.txtEntranceCode.Text = "Field21";
			this.txtEntranceCode.Top = 2.263003F;
			this.txtEntranceCode.Width = 1F;
			// 
			// txtInformationCode
			// 
			this.txtInformationCode.Height = 0.19F;
			this.txtInformationCode.Left = 3.9995F;
			this.txtInformationCode.Name = "txtInformationCode";
			this.txtInformationCode.Style = "font-family: \'Tahoma\'";
			this.txtInformationCode.Text = "Field22";
			this.txtInformationCode.Top = 2.419253F;
			this.txtInformationCode.Width = 1F;
			// 
			// txtInspectionDate
			// 
			this.txtInspectionDate.Height = 0.19F;
			this.txtInspectionDate.Left = 3.9995F;
			this.txtInspectionDate.Name = "txtInspectionDate";
			this.txtInspectionDate.Style = "font-family: \'Tahoma\'";
			this.txtInspectionDate.Text = "Field23";
			this.txtInspectionDate.Top = 2.575503F;
			this.txtInspectionDate.Width = 1F;
			// 
			// txtEntranceDescription
			// 
			this.txtEntranceDescription.Height = 0.19F;
			this.txtEntranceDescription.Left = 5.062F;
			this.txtEntranceDescription.Name = "txtEntranceDescription";
			this.txtEntranceDescription.Style = "font-family: \'Tahoma\'";
			this.txtEntranceDescription.Text = "Field20";
			this.txtEntranceDescription.Top = 2.263003F;
			this.txtEntranceDescription.Width = 2.375F;
			// 
			// txtInformationDescription
			// 
			this.txtInformationDescription.Height = 0.19F;
			this.txtInformationDescription.Left = 5.062F;
			this.txtInformationDescription.Name = "txtInformationDescription";
			this.txtInformationDescription.Style = "font-family: \'Tahoma\'";
			this.txtInformationDescription.Text = "Field21";
			this.txtInformationDescription.Top = 2.419253F;
			this.txtInformationDescription.Width = 2.375F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.19F;
			this.Field21.Left = 0.062F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field21.Text = "Acres";
			this.Field21.Top = 3.044253F;
			this.Field21.Width = 0.9375F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.19F;
			this.Field22.Left = 0.062F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field22.Text = "Value";
			this.Field22.Top = 3.200503F;
			this.Field22.Width = 0.9375F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.19F;
			this.Field23.Left = 0.9995F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field23.Text = "Soft Wood";
			this.Field23.Top = 2.888003F;
			this.Field23.Width = 0.9375F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.19F;
			this.Field24.Left = 1.937F;
			this.Field24.Name = "Field24";
			this.Field24.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field24.Text = "Hard Wood";
			this.Field24.Top = 2.888003F;
			this.Field24.Width = 0.9375F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.19F;
			this.Field25.Left = 2.8745F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field25.Text = "Mixed";
			this.Field25.Top = 2.888003F;
			this.Field25.Width = 0.9375F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.19F;
			this.Field26.Left = 3.812F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field26.Text = "Other";
			this.Field26.Top = 2.888003F;
			this.Field26.Width = 0.9375F;
			// 
			// Field27
			// 
			this.Field27.Height = 0.19F;
			this.Field27.Left = 4.812F;
			this.Field27.Name = "Field27";
			this.Field27.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field27.Text = "Total";
			this.Field27.Top = 2.888003F;
			this.Field27.Width = 0.9375F;
			// 
			// txtSoftWoodAcres
			// 
			this.txtSoftWoodAcres.Height = 0.19F;
			this.txtSoftWoodAcres.Left = 0.9995F;
			this.txtSoftWoodAcres.Name = "txtSoftWoodAcres";
			this.txtSoftWoodAcres.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtSoftWoodAcres.Text = "Soft Wood";
			this.txtSoftWoodAcres.Top = 3.044253F;
			this.txtSoftWoodAcres.Width = 0.9375F;
			// 
			// txtSoftValue
			// 
			this.txtSoftValue.Height = 0.19F;
			this.txtSoftValue.Left = 0.9995F;
			this.txtSoftValue.Name = "txtSoftValue";
			this.txtSoftValue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtSoftValue.Text = "Soft Wood";
			this.txtSoftValue.Top = 3.200503F;
			this.txtSoftValue.Width = 0.9375F;
			// 
			// txtHardWoodAcres
			// 
			this.txtHardWoodAcres.Height = 0.19F;
			this.txtHardWoodAcres.Left = 1.937F;
			this.txtHardWoodAcres.Name = "txtHardWoodAcres";
			this.txtHardWoodAcres.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtHardWoodAcres.Text = "Soft Wood";
			this.txtHardWoodAcres.Top = 3.044253F;
			this.txtHardWoodAcres.Width = 0.9375F;
			// 
			// txtHardValue
			// 
			this.txtHardValue.Height = 0.19F;
			this.txtHardValue.Left = 1.937F;
			this.txtHardValue.Name = "txtHardValue";
			this.txtHardValue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtHardValue.Text = "Soft Wood";
			this.txtHardValue.Top = 3.200503F;
			this.txtHardValue.Width = 0.9375F;
			// 
			// txtMixedAcres
			// 
			this.txtMixedAcres.Height = 0.19F;
			this.txtMixedAcres.Left = 2.8745F;
			this.txtMixedAcres.Name = "txtMixedAcres";
			this.txtMixedAcres.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMixedAcres.Text = "Soft Wood";
			this.txtMixedAcres.Top = 3.044253F;
			this.txtMixedAcres.Width = 0.9375F;
			// 
			// txtMixedValue
			// 
			this.txtMixedValue.Height = 0.19F;
			this.txtMixedValue.Left = 2.8745F;
			this.txtMixedValue.Name = "txtMixedValue";
			this.txtMixedValue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMixedValue.Text = "Soft Wood";
			this.txtMixedValue.Top = 3.200503F;
			this.txtMixedValue.Width = 0.9375F;
			// 
			// txtOtherAcres
			// 
			this.txtOtherAcres.Height = 0.19F;
			this.txtOtherAcres.Left = 3.812F;
			this.txtOtherAcres.Name = "txtOtherAcres";
			this.txtOtherAcres.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOtherAcres.Text = "Soft Wood";
			this.txtOtherAcres.Top = 3.044253F;
			this.txtOtherAcres.Width = 0.9375F;
			// 
			// txtOtherValue
			// 
			this.txtOtherValue.Height = 0.19F;
			this.txtOtherValue.Left = 3.812F;
			this.txtOtherValue.Name = "txtOtherValue";
			this.txtOtherValue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOtherValue.Text = "Soft Wood";
			this.txtOtherValue.Top = 3.200503F;
			this.txtOtherValue.Width = 0.9375F;
			// 
			// txtTotalAcres
			// 
			this.txtTotalAcres.Height = 0.19F;
			this.txtTotalAcres.Left = 4.812F;
			this.txtTotalAcres.Name = "txtTotalAcres";
			this.txtTotalAcres.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalAcres.Text = "Soft Wood";
			this.txtTotalAcres.Top = 3.044253F;
			this.txtTotalAcres.Width = 0.9375F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.19F;
			this.Field28.Left = 0.062F;
			this.Field28.Name = "Field28";
			this.Field28.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field28.Text = "Land";
			this.Field28.Top = 3.575503F;
			this.Field28.Width = 0.5625F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.2291667F;
			this.Field29.Left = 1.9995F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field29.Text = "Building";
			this.Field29.Top = 3.575503F;
			this.Field29.Width = 0.8125F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.19F;
			this.Field30.Left = 4.6245F;
			this.Field30.Name = "Field30";
			this.Field30.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field30.Text = "Exemption";
			this.Field30.Top = 3.575503F;
			this.Field30.Width = 0.9375F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 0.687F;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLand.Text = "Field11";
			this.txtLand.Top = 3.575503F;
			this.txtLand.Width = 1.25F;
			// 
			// txtBuilding
			// 
			this.txtBuilding.Height = 0.19F;
			this.txtBuilding.Left = 2.8745F;
			this.txtBuilding.Name = "txtBuilding";
			this.txtBuilding.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBuilding.Text = "Field11";
			this.txtBuilding.Top = 3.575503F;
			this.txtBuilding.Width = 1.6875F;
			// 
			// txtExemption
			// 
			this.txtExemption.Height = 0.19F;
			this.txtExemption.Left = 5.6245F;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtExemption.Text = "Field11";
			this.txtExemption.Top = 3.575503F;
			this.txtExemption.Width = 1.5F;
			// 
			// Field34
			// 
			this.Field34.Height = 0.19F;
			this.Field34.Left = 2.187F;
			this.Field34.Name = "Field34";
			this.Field34.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field34.Text = "Total Account Values";
			this.Field34.Top = 3.888008F;
			this.Field34.Width = 3F;
			// 
			// Field35
			// 
			this.Field35.Height = 0.19F;
			this.Field35.Left = 0.9995F;
			this.Field35.Name = "Field35";
			this.Field35.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field35.Text = "Land";
			this.Field35.Top = 4.200512F;
			this.Field35.Width = 0.5625F;
			// 
			// Field36
			// 
			this.Field36.Height = 0.19F;
			this.Field36.Left = 1.687F;
			this.Field36.Name = "Field36";
			this.Field36.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field36.Text = "Assessment";
			this.Field36.Top = 4.044261F;
			this.Field36.Width = 1.25F;
			// 
			// Field37
			// 
			this.Field37.Height = 0.19F;
			this.Field37.Left = 0.9995F;
			this.Field37.Name = "Field37";
			this.Field37.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field37.Text = "Building";
			this.Field37.Top = 4.356762F;
			this.Field37.Width = 0.8125F;
			// 
			// Field38
			// 
			this.Field38.Height = 0.19F;
			this.Field38.Left = 0.9995F;
			this.Field38.Name = "Field38";
			this.Field38.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field38.Text = "Exemption";
			this.Field38.Top = 4.513012F;
			this.Field38.Width = 0.875F;
			// 
			// txtLandAcct
			// 
			this.txtLandAcct.Height = 0.19F;
			this.txtLandAcct.Left = 1.937F;
			this.txtLandAcct.Name = "txtLandAcct";
			this.txtLandAcct.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLandAcct.Text = "Field11";
			this.txtLandAcct.Top = 4.200512F;
			this.txtLandAcct.Width = 1.375F;
			// 
			// txtBldgAcct
			// 
			this.txtBldgAcct.Height = 0.19F;
			this.txtBldgAcct.Left = 1.937F;
			this.txtBldgAcct.Name = "txtBldgAcct";
			this.txtBldgAcct.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBldgAcct.Text = "Field11";
			this.txtBldgAcct.Top = 4.356762F;
			this.txtBldgAcct.Width = 1.375F;
			// 
			// txtExemptAcct
			// 
			this.txtExemptAcct.Height = 0.19F;
			this.txtExemptAcct.Left = 1.937F;
			this.txtExemptAcct.Name = "txtExemptAcct";
			this.txtExemptAcct.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtExemptAcct.Text = "Field11";
			this.txtExemptAcct.Top = 4.513012F;
			this.txtExemptAcct.Width = 1.375F;
			// 
			// Field39
			// 
			this.Field39.Height = 0.19F;
			this.Field39.Left = 0.9995F;
			this.Field39.Name = "Field39";
			this.Field39.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field39.Text = "Taxable";
			this.Field39.Top = 4.669262F;
			this.Field39.Width = 0.875F;
			// 
			// txtTaxable
			// 
			this.txtTaxable.Height = 0.19F;
			this.txtTaxable.Left = 1.937F;
			this.txtTaxable.Name = "txtTaxable";
			this.txtTaxable.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTaxable.Text = "Field11";
			this.txtTaxable.Top = 4.669262F;
			this.txtTaxable.Width = 1.375F;
			// 
			// Field41
			// 
			this.Field41.Height = 0.19F;
			this.Field41.Left = 5.062F;
			this.Field41.Name = "Field41";
			this.Field41.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field41.Text = "Estimated";
			this.Field41.Top = 4.044261F;
			this.Field41.Width = 1F;
			// 
			// Field42
			// 
			this.Field42.Height = 0.19F;
			this.Field42.Left = 4.187F;
			this.Field42.Name = "Field42";
			this.Field42.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field42.Text = "Tax Rate";
			this.Field42.Top = 4.200512F;
			this.Field42.Width = 0.8125F;
			// 
			// Field43
			// 
			this.Field43.Height = 0.19F;
			this.Field43.Left = 4.187F;
			this.Field43.Name = "Field43";
			this.Field43.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field43.Text = "Tax Amount";
			this.Field43.Top = 4.356762F;
			this.Field43.Width = 1F;
			// 
			// txtTaxRate
			// 
			this.txtTaxRate.Height = 0.19F;
			this.txtTaxRate.Left = 5.2495F;
			this.txtTaxRate.Name = "txtTaxRate";
			this.txtTaxRate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTaxRate.Text = "Field11";
			this.txtTaxRate.Top = 4.200512F;
			this.txtTaxRate.Width = 1.375F;
			// 
			// txtTax
			// 
			this.txtTax.Height = 0.19F;
			this.txtTax.Left = 5.2495F;
			this.txtTax.Name = "txtTax";
			this.txtTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTax.Text = "Field11";
			this.txtTax.Top = 4.356762F;
			this.txtTax.Width = 1.375F;
			// 
			// Field44
			// 
			this.Field44.Height = 0.1979167F;
			this.Field44.Left = 0.062F;
			this.Field44.Name = "Field44";
			this.Field44.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field44.Text = "Book Page";
			this.Field44.Top = 1.346333F;
			this.Field44.Width = 0.9270833F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.Height = 0.1979167F;
			this.txtBookPage.Left = 0.9995F;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Style = "font-family: \'Tahoma\'";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 1.346333F;
			this.txtBookPage.Width = 6.458333F;
			// 
			// txtDeedName1
			// 
			this.txtDeedName1.Height = 0.19F;
			this.txtDeedName1.Left = 0.067F;
			this.txtDeedName1.MultiLine = false;
			this.txtDeedName1.Name = "txtDeedName1";
			this.txtDeedName1.Style = "font-family: \'Tahoma\'";
			this.txtDeedName1.Text = null;
			this.txtDeedName1.Top = 0.1769994F;
			this.txtDeedName1.Width = 2.5F;
			// 
			// textBox2
			// 
			this.textBox2.Height = 0.19F;
			this.textBox2.Left = 0.067F;
			this.textBox2.Name = "textBox2";
			this.textBox2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.textBox2.Text = "Name";
			this.textBox2.Top = 0F;
			this.textBox2.Width = 2.5F;
			// 
			// textBox3
			// 
			this.textBox3.Height = 0.19F;
			this.textBox3.Left = 2.629501F;
			this.textBox3.Name = "textBox3";
			this.textBox3.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.textBox3.Text = "Second Owner";
			this.textBox3.Top = 0F;
			this.textBox3.Width = 2.3125F;
			// 
			// txtDeedName2
			// 
			this.txtDeedName2.Height = 0.19F;
			this.txtDeedName2.Left = 2.63F;
			this.txtDeedName2.MultiLine = false;
			this.txtDeedName2.Name = "txtDeedName2";
			this.txtDeedName2.Style = "font-family: \'Tahoma\'";
			this.txtDeedName2.Text = null;
			this.txtDeedName2.Top = 0.1769994F;
			this.txtDeedName2.Width = 2.3125F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMuniname,
            this.txtDate,
            this.txtTime,
            this.txtTitle,
            this.txtAccount,
            this.txtPage});
			this.PageHeader.Height = 0.625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// txtMuniname
			// 
			this.txtMuniname.CanGrow = false;
			this.txtMuniname.Height = 0.18F;
			this.txtMuniname.Left = 0F;
			this.txtMuniname.MultiLine = false;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Style = "font-family: \'Tahoma\'";
			this.txtMuniname.Text = "Field1";
			this.txtMuniname.Top = 0F;
			this.txtMuniname.Width = 1.8125F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.5F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.25F;
			this.txtTitle.Left = 2F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Billing Information";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 3.625F;
			// 
			// txtAccount
			// 
			this.txtAccount.CanGrow = false;
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 2F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'";
			this.txtAccount.Text = "Field1";
			this.txtAccount.Top = 0.25F;
			this.txtAccount.Width = 3.625F;
			// 
			// txtPage
			// 
			this.txtPage.CanGrow = false;
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.25F;
			this.txtPage.Visible = false;
			this.txtPage.Width = 1.4375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptShortCard
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddr3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRef1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRef2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTranCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTranDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt1Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt3Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInspectionDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInformationDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftWoodAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardWoodAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAcres)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeedName1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeedName2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddr3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTelephone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRef1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRef2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTranCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTranDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt1Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt2Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt3Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen1Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen2Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInspectionDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInformationDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoftWoodAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoftValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHardWoodAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHardValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixedAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixedValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAcres;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field37;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field38;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field39;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field43;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field44;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeedName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeedName2;
    }
}
