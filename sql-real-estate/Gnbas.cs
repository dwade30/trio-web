﻿//Fecher vbPorter - Version 1.0.0.40
using System.IO;
using System.Runtime.InteropServices;

namespace TWRE0000
{
	public class modGNBas
	{
		//=========================================================
		public const int MAXCARDS = 50;
		// These Declares are used to get the block cursor
		[DllImport("user32")]
		public static extern int CreateCaret(int hwnd, int hBitmap, int nWidth, int nHeight);

		[DllImport("user32")]
		public static extern int ShowCaret(int hwnd);

		[DllImport("user32")]
		public static extern int GetFocus();

		[DllImport("user32")]
		public static extern object SetCursorPos(int x, int y);
		// screen setup items
		const string Logo = "TRIO Software Corporation";

		[DllImport("kernel32")]
		public static extern void Sleep(int dwMilliseconds);

		[DllImport("kernel32")]
		public static extern int WinExec(string lpCmdLine, int nCmdShow);

		[DllImport("kernel32")]
		public static extern int GlobalAlloc(int wFlags, int dwBytes);

		public struct ControlProportions
		{
			public float WidthProportions;
			public float HeightProportions;
			public float TopProportions;
			public float LeftProportions;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public ControlProportions(int unusedParam)
			{
				this.WidthProportions = 0;
				this.HeightProportions = 0;
				this.TopProportions = 0;
				this.LeftProportions = 0;
			}
		};

		public class StaticVariables
		{
			public bool gboolFormLoaded;
			public bool gboolLoadingDone;
			public bool gboolExiting;
			// vbPorter upgrade warning: gintMaxCards As short --> As int	OnWrite(int, double)
			public int gintMaxCards;
			public int gintCardNumber;
			public bool gboolCommercial;
			public bool gboolViewCommercial;
			public bool boolLAF;
			public bool boolOutbuildingSQFT;
			public bool gboolSave;
			public bool gboolLandSave;
			public int intNewTab;
			public string boolRangeOfAccounts = "";
			// vbPorter upgrade warning: response As object	OnWrite(string, DialogResult)
			public object response;
			// vbPorter upgrade warning: Resp1 As object	OnWrite(string, short, double)	OnRead(string)
			// Public RespKeyword
			public object Resp1;
			// vbPorter upgrade warning: Resp2 As object	OnWrite(string, int, object)	OnRead(string)
			public object Resp2;
			// vbPorter upgrade warning: Resp3 As object	OnWrite(string, object)
			public object Resp3;
			// vbPorter upgrade warning: Resp4 As object	OnWrite(string, object)
			public object Resp4;
			public bool SystemSetup;
			public bool LinkPicture;
			public int Bp;
			// These are used to hold a Message Box Response
			// Message Box Response Variables
			public object MessageResp1;
			public object MessageResp2;
			public object MessageResp3;
			// Prefixes and Variables used for Pictures and Sketches
			public FileInfo SketchFile;
			public FileInfo PictureFile;
			public string PictureFormat = "";
			public string PictureLocation = string.Empty;
			public string DocumentLocationName = "";
			public string SketchLocation = "";
			// Public PPDatabase           As String
			// Prefixes for open statements
			public string TrioEXE = "";
			public string TrioDATA = "";
			public string TrioSDrive = string.Empty;
			// used by search routines
			public string SEQ = string.Empty;
			public int House;
			public string SEARCH = string.Empty;
			public bool LongScreenSearch;
			// used for insert/overtype mode in text boxes
			public int OverType;
			// used in getvalue
			public string NewData = "";
			public int NewValue;
			// Public CurrentApplication   As String * 1
			//
			//
			public int CommentAccount;
			public string CommentName = string.Empty;
			// Dim x                       As Integer
			public int NextNumber;
			//public static extern int sendmessage(int hwnd, int wMsg, int wParam, ref void* lParam);
			//[DllImport("user32", EntryPoint = "SendMessageA")]
			public string Display = "";
			public bool EntryMenuFlag;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
