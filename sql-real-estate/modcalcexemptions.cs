﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class modcalcexemptions
	{
		//=========================================================
		public const int CNSTEXEMPTCATNONE = 0;
		public const int CNSTEXEMPTCATHOMESTEAD = 1;
		public const int CNSTEXEMPTCATDISABLEDVET = 2;
		public const int CNSTEXEMPTCATKOREAN = 3;
		public const int CNSTEXEMPTCATPARAPLEGICVET = 4;
		public const int CNSTEXEMPTCATVETERAN = 5;
		public const int CNSTEXEMPTCATVIETNAM = 6;
		public const int CNSTEXEMPTCATWORLDWARI = 7;
		public const int CNSTEXEMPTCATWORLDWARII = 8;
		public const int CNSTEXEMPTCATUNITEDSTATES = 9;
		public const int CNSTEXEMPTCATSTATE = 10;
		public const int CNSTEXEMPTCATBLIND = 11;
		public const int CNSTEXEMPTCATSCIANDLIT = 12;
		public const int CNSTEXEMPTCATBENEVCHARITABLE = 13;
		public const int CNSTEXEMPTCATPARSONAGE = 14;
		public const int CNSTEXEMPTCATRELIGIOUS = 15;
		public const int CNSTEXEMPTCATVETERANSORG = 16;
		public const int CNSTEXEMPTCATCHAMBERCOMMERCE = 17;
		public const int CNSTEXEMPTCATFRATERNAL = 18;
		public const int CNSTEXEMPTCATMUNIAIRPORT = 19;
		public const int CNSTEXEMPTCATMUNICOUNTY = 20;
		public const int CNSTEXEMPTCATQUASIMUNICIPAL = 21;
		public const int CNSTEXEMPTCATANIMALWASTE = 22;
		public const int CNSTEXEMPTCATPOLLUTIONCONTROL = 23;
		public const int CNSTEXEMPTCATSEWAGEDISPOSAL = 24;
		public const int CNSTEXEMPTCATWATERPIPES = 25;
		public const int CNSTEXEMPTCATNHWATER = 26;
		public const int CNSTEXEMPTCATHOSPITALLEASED = 27;
		public const int CNSTEXEMPTCATPRIVATEAIRPORT = 38;
		public const int CNSTEXEMPTCATWWIWIDOWER = 39;
		public const int CNSTEXEMPTCATWWIIWIDOWER = 40;
		public const int CNSTEXEMPTCATHYDROOUTSIDEMUNI = 41;
		public const int CNSTEXEMPTCATLEBANONPANAMA = 42;
		public const int CNSTEXEMPTCATVETCOOPHOUSING = 43;
        public const int CNSTEXEMPTCATVETMALESURVIVOR = 44;
        public const int CNSTEXEMPTCATSOLARANDWIND = 45;
        public const int CNSTEXEMPTCATVETEARLYVIETNAM = 46;
        public const int CNSTEXEMPTCATMINVETCODE = 2;
		public const int CNSTEXEMPTCATMAXVETCODE = 8;
		public const int CNSTEXEMPTENLISTEDMAINE = 1;
		public const int CNSTEXEMPTENLISTEDNONMAINE = 2;
		public const int CNSTEXEMPTAPPLYTOBLDGFIRST = 0;
		public const int CNSTEXEMPTAPPLYTOLANDFIRST = 1;
		// The next two aren't implemented yet
		public const int CNSTEXEMPTAPPLYTOBLDGONLY = 2;
		public const int CNSTEXEMPTAPPLYTOLANDONLY = 3;

		private struct ExemptRec
		{
			public short[] ExemptCode/*?  = new short[3 + 1] */;
			public int[] ExemptValue/*?  = new int[3 + 1] */;
			public short[] ExemptPct/*?  = new short[3 + 1] */;
			public int HomesteadValue;
			public bool boolHomestead;
			public int Account;
			public int Land;
			public int Bldg;
			public int LandExempt;
			public int BldgExempt;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public ExemptRec(int unusedParam)
			{
				this.ExemptCode = new short[4];
				this.ExemptValue = new int[4];
				this.ExemptPct = new short[4];
				this.HomesteadValue = 0;
				this.boolHomestead = false;
				this.Account = 0;
				this.Land = 0;
				this.Bldg = 0;
				this.LandExempt = 0;
				this.BldgExempt = 0;
			}
		};
	
		public static void NewCalcExemptions()
		{
			clsDRWrapper clsMaster = new clsDRWrapper();
			clsDRWrapper clsExempts = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			string strSQL;
			int[] PlaceHolder = new int[3 + 1];
			string strErrorLog;
			int x;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			ExemptRec CurrExemptRec = new ExemptRec(0);
			int lngTemp = 0;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			int lngTotalExemption = 0;
			// vbPorter upgrade warning: lngCurrExempt As int	OnWrite(double, int)
			int lngCurrExempt = 0;
			string strLandField = "";
			string strBldgField = "";
			string strExemptField = "";
			// vbPorter upgrade warning: lngTaxableRatio As int	OnWrite(int, double)
			int lngTaxableRatio = 0;
			// vbPorter upgrade warning: returncode As short, int --> As DialogResult
			DialogResult returncode;
			int lngNumRecs;
			int lngCRec;
			int lngCard1Land;
			int lngCard1Bldg;
			try
			{
				if (modREMain.Statics.boolShortRealEstate)
				{
					modGlobalVariables.Statics.boolBillingorCorrelated = true;
					strLandField = "lastlandval";
					strBldgField = "lastbldgval";
					strExemptField = "rlexemption";
				}
				else
				{
					strLandField = "rllandval";
					strBldgField = "rlbldgval";
					strExemptField = "correxemption";
					frmWhichExemptions.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					if (modGlobalVariables.Statics.CancelledIt)
						return;
				}
				if (modGlobalVariables.Statics.boolBillingorCorrelated)
				{
					returncode = MessageBox.Show("     WARNING.." + "\r\n" + "This will replace all exemptions" + "\r\n" + "with calculated values.", "Calculate Exemptions", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
					if (returncode == DialogResult.Cancel)
					{
						// Unload Frmcalcexemptions
						return;
					}
					strLandField = "lastlandval";
					strBldgField = "lastbldgval";
					strExemptField = "rlexemption";
				}
				else
				{
					strLandField = "rllandval";
					strBldgField = "rlbldgval";
					strExemptField = "correxemption";
				}
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = 1;
				}
				Statics.CertifiedRatio = modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio;
				if (Statics.CertifiedRatio <= 0)
				{
					MessageBox.Show("The certified ratio must be set before exemptions can be calculated", "Bad Certified Ratio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				Statics.AssessmentRatio = Statics.CertifiedRatio;
				strErrorLog = "";
				clsExempts.OpenRecordset("select * from exemptcode order by code", modGlobalVariables.strREDatabase);
				if (clsExempts.EndOfFile())
				{
					MessageBox.Show("There are no exempt codes defined", "No Exempt Codes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				modGlobalFunctions.AddCYAEntry_26("RE", "Calculated Exemptions", "Calculated Exemptions");
 
				strSQL = "select rsaccount,RIEXEMPTCD1,ritrancode,riexemptcd2,riexemptcd3,ExemptPct1,ExemptPct2,ExemptPct3,TOTLAND,TOTBLDG,master." + strLandField + ",master." + strBldgField + "  from  ";
				strSQL += "(select rsaccount as account,sum(cast(" + strLandField + " as bigint)) as totLand,sum(cast(" + strBldgField + " as bigint)) as totBldg from master where not rsdeleted = 1 group by rsaccount) as tbl1";
				strSQL += "  inner join MASTER  on (master.rsaccount = tbl1.account) where rscard = 1 and not rsdeleted = 1 order by rsaccount";
				clsMaster.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				lngCRec = 0;
				lngNumRecs = clsMaster.RecordCount();
				Frmcalcexemptions.InstancePtr.Initialize();
				Frmcalcexemptions.InstancePtr.ProgressBar1.Maximum = lngNumRecs;
				int lngLastTown;
				lngLastTown = 0;
				while (!clsMaster.EndOfFile() && Frmcalcexemptions.InstancePtr.stillcalculate)
				{
					lngCRec += 1;
					PlaceHolder[1] = 1;
					PlaceHolder[2] = 2;
					PlaceHolder[3] = 3;
					lngTotalExemption = 0;
					FCUtils.EraseSafe(CurrExemptRec.ExemptCode);
					FCUtils.EraseSafe(CurrExemptRec.ExemptValue);
					FCUtils.EraseSafe(CurrExemptRec.ExemptPct);
					CurrExemptRec.Account = FCConvert.ToInt32(clsMaster.Get_Fields_Int32("rsaccount"));
					// TODO Get_Fields: Field [totbldg] not found!! (maybe it is an alias?)
					CurrExemptRec.Bldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMaster.Get_Fields("totbldg"))));
					// TODO Get_Fields: Field [totland] not found!! (maybe it is an alias?)
					CurrExemptRec.Land = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMaster.Get_Fields("totland"))));
					CurrExemptRec.HomesteadValue = 0;
					CurrExemptRec.LandExempt = 0;
					CurrExemptRec.BldgExempt = 0;
					CurrExemptRec.boolHomestead = false;
					if (CurrExemptRec.Account == 747)
					{
						lngLastTown = lngLastTown;
					}
					if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
					{
						if (Conversion.Val(clsMaster.Get_Fields_Int32("ritrancode")) != lngLastTown)
						{
							modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMaster.Get_Fields_Int32("ritrancode"))));
							lngLastTown = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMaster.Get_Fields_Int32("ritrancode"))));
							Statics.AssessmentRatio = modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio;
							Statics.CertifiedRatio = modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio;
							if (Statics.CertifiedRatio == 0 && (Conversion.Val(clsMaster.Get_Fields_Int32("riexemptcd1")) > 0 || Conversion.Val(clsMaster.Get_Fields_Int32("riexemptcd2")) > 0 || Conversion.Val(clsMaster.Get_Fields_Int32("riexemptcd3")) > 0))
							{
								MessageBox.Show("Error calculating account " + clsMaster.Get_Fields_Int32("rsaccount") + "\r\n" + "The certified ratio for town " + modRegionalTown.GetTownKeyName(ref lngLastTown) + " is 0" + "\r\n" + "Cannot continue", "Invalid Certified Ratio", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return;
							}
						}
					}
					if (Conversion.Val(clsMaster.Get_Fields_Int32("riexemptcd1")) > 0)
					{
						CurrExemptRec.ExemptCode[1] = FCConvert.ToInt16(Math.Round(Conversion.Val(clsMaster.Get_Fields_Int32("riexemptcd1"))));
						// TODO Get_Fields: Check the table for the column [Exemptpct1] and replace with corresponding Get_Field method
						CurrExemptRec.ExemptPct[1] = FCConvert.ToInt16(Math.Round(Conversion.Val(clsMaster.Get_Fields("Exemptpct1"))));
					}
					if (Conversion.Val(clsMaster.Get_Fields_Int32("riexemptcd2")) > 0)
					{
						CurrExemptRec.ExemptCode[2] = FCConvert.ToInt16(Math.Round(Conversion.Val(clsMaster.Get_Fields_Int32("riexemptcd2"))));
						// TODO Get_Fields: Check the table for the column [Exemptpct2] and replace with corresponding Get_Field method
						CurrExemptRec.ExemptPct[2] = FCConvert.ToInt16(Math.Round(Conversion.Val(clsMaster.Get_Fields("Exemptpct2"))));
					}
					if (Conversion.Val(clsMaster.Get_Fields_Int32("riexemptcd3")) > 0)
					{
						CurrExemptRec.ExemptCode[3] = FCConvert.ToInt16(Math.Round(Conversion.Val(clsMaster.Get_Fields_Int32("riexemptcd3"))));
						// TODO Get_Fields: Check the table for the column [Exemptpct3] and replace with corresponding Get_Field method
						CurrExemptRec.ExemptPct[3] = FCConvert.ToInt16(Math.Round(Conversion.Val(clsMaster.Get_Fields("Exemptpct3"))));
					}
					// make sure that homestead codes go last
					for (x = 1; x <= 2; x++)
					{
						if (CurrExemptRec.ExemptCode[PlaceHolder[x]] > 0 && CurrExemptRec.ExemptCode[PlaceHolder[x + 1]] > 0)
						{
							if (clsExempts.FindFirst("code = " + FCConvert.ToString(CurrExemptRec.ExemptCode[PlaceHolder[x]])))
							{
								// If clsExempts.FindFirstRecord("Code", CurrExemptRec.ExemptCode(PlaceHolder(x))) Then
								// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
								if (FCConvert.ToInt32(clsExempts.Get_Fields("category")) == CNSTEXEMPTCATHOMESTEAD)
								{
									lngTemp = PlaceHolder[x];
									PlaceHolder[x] = PlaceHolder[x + 1];
									PlaceHolder[x + 1] = lngTemp;
								}
							}
						}
					}
					// x
					// go through the exempts in the new order
					for (x = 1; x <= 3; x++)
					{
						if (CurrExemptRec.ExemptCode[PlaceHolder[x]] > 0)
						{
							// If clsExempts.FindFirstRecord("Code", CurrExemptRec.ExemptCode(PlaceHolder(x))) Then
							if (clsExempts.FindFirst("code = " + FCConvert.ToString(CurrExemptRec.ExemptCode[PlaceHolder[x]])))
							{
								// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
								if (Conversion.Val(clsExempts.Get_Fields("amount")) == 0)
								{
									// totally exempt
									lngTemp = CurrExemptRec.Bldg + CurrExemptRec.Land;
									if (CurrExemptRec.ExemptPct[PlaceHolder[x]] != 100)
									{
										dblTemp = FCConvert.ToDouble(Strings.Format(CurrExemptRec.ExemptPct[PlaceHolder[x]] / 100.0, "0.00"));
										lngCurrExempt = FCConvert.ToInt32(lngTemp * dblTemp);
										if (modGlobalVariables.Statics.CustomizedInfo.boolRoundCalculatedHomesteads)
										{
											lngCurrExempt = modGlobalRoutines.RERound(ref lngCurrExempt);
										}
										if (lngTotalExemption + lngCurrExempt > CurrExemptRec.Bldg + CurrExemptRec.Land)
										{
											lngCurrExempt = CurrExemptRec.Bldg + CurrExemptRec.Land - lngTotalExemption;
										}
										lngTotalExemption += lngCurrExempt;
										if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOBLDGFIRST)
										{
											lngTemp = CurrExemptRec.Bldg - CurrExemptRec.BldgExempt;
											if (lngTemp >= lngCurrExempt)
											{
												CurrExemptRec.BldgExempt += lngCurrExempt;
											}
											else
											{
												CurrExemptRec.BldgExempt = CurrExemptRec.Bldg;
												CurrExemptRec.LandExempt += (lngCurrExempt - lngTemp);
											}
										}
										else if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOLANDFIRST)
										{
											lngTemp = CurrExemptRec.Land - CurrExemptRec.LandExempt;
											if (lngTemp >= lngCurrExempt)
											{
												CurrExemptRec.LandExempt += lngCurrExempt;
											}
											else
											{
												CurrExemptRec.LandExempt = CurrExemptRec.Land;
												CurrExemptRec.LandExempt += (lngCurrExempt - lngTemp);
											}
										}
										else if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOBLDGONLY)
										{
											// not implemented yet
										}
										else if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOLANDONLY)
										{
											// not implemented yet
										}
										CurrExemptRec.ExemptValue[PlaceHolder[x]] = lngCurrExempt;
										if (lngTotalExemption == CurrExemptRec.Bldg + CurrExemptRec.Land)
										{
											// nothing left to calculate
											break;
										}
									}
									else
									{
										// totally exempt
										CurrExemptRec.BldgExempt = CurrExemptRec.Bldg;
										CurrExemptRec.LandExempt = CurrExemptRec.Land;
										CurrExemptRec.ExemptValue[PlaceHolder[x]] = CurrExemptRec.Bldg + CurrExemptRec.Land - lngTotalExemption;
										lngTotalExemption = CurrExemptRec.BldgExempt + CurrExemptRec.LandExempt;
										// nothing left to calculate
										break;
									}
								}
								else
								{
									// regular type of exempt
									// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
									if (FCConvert.ToInt32(clsExempts.Get_Fields("category")) == CNSTEXEMPTCATHOMESTEAD)
									{
										// a homestead exemption
										CurrExemptRec.boolHomestead = true;
										if (!modGlobalVariables.Statics.CustomizedInfo.boolApplyHomesteadToCardOneOnly)
										{
											lngTaxableRatio = CurrExemptRec.Bldg + CurrExemptRec.Land;
										}
										else
										{
											lngTaxableRatio = FCConvert.ToInt32(Conversion.Val(clsMaster.Get_Fields(strLandField)) + Conversion.Val(clsMaster.Get_Fields(strBldgField)));
											lngCard1Land = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMaster.Get_Fields(strLandField))));
											lngCard1Bldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMaster.Get_Fields(strBldgField))));
										}
										lngTaxableRatio = FCConvert.ToInt32((lngTaxableRatio / Statics.CertifiedRatio));
										// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
										lngCurrExempt = FCConvert.ToInt32(clsExempts.Get_Fields("amount") * Statics.CertifiedRatio);
										if (modGlobalVariables.Statics.CustomizedInfo.boolRoundCalculatedHomesteads)
										{
											lngCurrExempt = modGlobalRoutines.RERound(ref lngCurrExempt);
										}

										if (lngCurrExempt + lngTotalExemption > CurrExemptRec.Bldg + CurrExemptRec.Land)
										{
											lngCurrExempt = CurrExemptRec.Bldg + CurrExemptRec.Land - lngTotalExemption;
										}
										CurrExemptRec.ExemptValue[PlaceHolder[x]] = lngCurrExempt;
										lngTotalExemption += lngCurrExempt;
										if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOBLDGFIRST)
										{
											lngTemp = CurrExemptRec.Bldg - CurrExemptRec.BldgExempt;
											if (lngTemp >= lngCurrExempt)
											{
												CurrExemptRec.BldgExempt += lngCurrExempt;
											}
											else
											{
												CurrExemptRec.BldgExempt = CurrExemptRec.Bldg;
												CurrExemptRec.LandExempt += (lngCurrExempt - lngTemp);
											}
										}
										else if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOLANDFIRST)
										{
											lngTemp = CurrExemptRec.Land - CurrExemptRec.LandExempt;
											if (lngTemp >= lngCurrExempt)
											{
												CurrExemptRec.LandExempt += lngCurrExempt;
											}
											else
											{
												CurrExemptRec.LandExempt = CurrExemptRec.Land;
												CurrExemptRec.LandExempt += (lngCurrExempt - lngTemp);
											}
										}
										else if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOBLDGONLY)
										{
											// not implemented yet
										}
										else if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOLANDONLY)
										{
											// not implemented yet
										}
										CurrExemptRec.HomesteadValue += lngCurrExempt;
									}
									else
									{
										// not a homestead and not totally exempt
										// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
										lngCurrExempt = FCConvert.ToInt32(clsExempts.Get_Fields("amount"));
										// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
										if (FCConvert.ToInt32(clsExempts.Get_Fields("category")) != CNSTEXEMPTCATPARSONAGE)
										{
											// parsonages are 20000 not 20000 * the assessment ratio
											//FC:FINAL:MSH - i.issue #1450: incorrect calculating with AssessmentRatio, which less than 1
											//lngCurrExempt *= FCConvert.ToInt32(AssessmentRatio;
											lngCurrExempt = FCConvert.ToInt32(Math.Round(lngCurrExempt * Statics.AssessmentRatio, MidpointRounding.AwayFromZero));
										}
										if (modGlobalVariables.Statics.CustomizedInfo.boolRoundCalculatedHomesteads && lngCurrExempt > 0)
										{
											lngCurrExempt = modGlobalRoutines.RERound(ref lngCurrExempt);
										}
										if (lngCurrExempt + lngTotalExemption > CurrExemptRec.Bldg + CurrExemptRec.Land)
										{
											lngCurrExempt = CurrExemptRec.Bldg + CurrExemptRec.Land - lngTotalExemption;
										}
										lngTotalExemption += lngCurrExempt;
										if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOBLDGFIRST)
										{
											lngTemp = CurrExemptRec.Bldg - CurrExemptRec.BldgExempt;
											if (lngTemp >= lngCurrExempt)
											{
												CurrExemptRec.BldgExempt += lngCurrExempt;
											}
											else
											{
												CurrExemptRec.BldgExempt = CurrExemptRec.Bldg;
												CurrExemptRec.LandExempt += (lngCurrExempt - lngTemp);
											}
										}
										else if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOLANDFIRST)
										{
											lngTemp = CurrExemptRec.Land - CurrExemptRec.LandExempt;
											if (lngTemp >= lngCurrExempt)
											{
												CurrExemptRec.LandExempt += lngCurrExempt;
											}
											else
											{
												CurrExemptRec.LandExempt = CurrExemptRec.Land;
												CurrExemptRec.LandExempt += (lngCurrExempt - lngTemp);
											}
										}
										else if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOBLDGONLY)
										{
											// not implemented yet
										}
										else if (Conversion.Val(clsExempts.Get_Fields_Int16("applyto")) == CNSTEXEMPTAPPLYTOLANDONLY)
										{
											// not implemented yet
										}
										CurrExemptRec.ExemptValue[PlaceHolder[x]] = lngCurrExempt;
									}
								}
							}
							else
							{
								strErrorLog += "Account " + FCConvert.ToString(CurrExemptRec.Account) + " has bad exempt code: " + FCConvert.ToString(CurrExemptRec.ExemptCode[PlaceHolder[x]]) + "\r\n";
							}
						}
					}
					// x
					// now apply the exemptions
					// Call NewApplyExemptions(CurrExemptRec, Not boolShortRealEstate)
					NewApplyExemptions_6(CurrExemptRec, !modGlobalVariables.Statics.boolBillingorCorrelated);
					clsMaster.MoveNext();
					Frmcalcexemptions.InstancePtr.ProgressBar1.Value = lngCRec;
					Frmcalcexemptions.InstancePtr.ProgressBar1.Refresh();
					Frmcalcexemptions.InstancePtr.lblpercentdone.Text = Strings.Format(((FCConvert.ToDouble(lngCRec) / lngNumRecs) * 100), "##0.00") + "%";
					Frmcalcexemptions.InstancePtr.lblpercentdone.Refresh();
					//FC:FINAL:MSH - i.issue #1070: update the progress bar
					FCUtils.ApplicationUpdate(Frmcalcexemptions.InstancePtr);
					//Application.DoEvents();
				}
				Frmcalcexemptions.InstancePtr.Unload();
                FCUtils.ApplicationUpdate(App.MainForm);
                //Application.DoEvents();
                if (modGlobalVariables.Statics.boolBillingorCorrelated)
				{
					if (!clsMaster.EndOfFile() && !Frmcalcexemptions.InstancePtr.stillcalculate)
					{
						modGlobalFunctions.AddCYAEntry_26("RE", "Aborted Exempt Calculation", "Aborted Exempt Calculation");
					}
					else
					{
						clsExempts.Execute("update status set calcexemptions = '" + FCConvert.ToString(DateTime.Now) + "'", modGlobalVariables.strREDatabase);
						MessageBox.Show("Done calculating exemptions", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
						if (MessageBox.Show("Save billing values for use in tracking previous assessments?" + "\r\n" + "These values appear in the Previous Assessment section of the Property Card", "Save in Previous Assessments?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							frmSavePreviousAssessment.InstancePtr.Show(App.MainForm);
						}
					}
				}
				else
				{
					clsExempts.Execute("update status set currentexemptions = '" + FCConvert.ToString(DateTime.Now) + "'", modGlobalVariables.strREDatabase);
					MessageBox.Show("Done calculating exemptions", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				clsMaster.DisposeOf();
				clsExempts.DisposeOf();
				clsSave.DisposeOf();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				Frmcalcexemptions.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In NewCalcExemptions", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				clsMaster.DisposeOf();
				clsExempts.DisposeOf();
				clsSave.DisposeOf();
			}
            finally
            {
                Frmcalcexemptions.InstancePtr.Unload();
            }
		}

		private static void NewApplyExemptions_6(ExemptRec ERec, bool boolCurrOrBill)
		{
			NewApplyExemptions(ref ERec, ref boolCurrOrBill);
		}

		private static void NewApplyExemptions(ref ExemptRec ERec, ref bool boolCurrOrBill)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngExemptLeft;
			// vbPorter upgrade warning: lngTotalTaxable As int	OnWriteFCConvert.ToDouble(
			int lngTotalTaxable = 0;
			string strLandField = "";
			string strBldgField = "";
			string strExemptField = "";
			int lngAccount;
			lngAccount = ERec.Account;
			if (boolCurrOrBill)
			{
				// current
				strLandField = "rllandval";
				strBldgField = "rlbldgval";
				strExemptField = "correxemption";
			}
			else
			{
				// billing
				strLandField = "lastlandval";
				strBldgField = "lastbldgval";
				strExemptField = "Rlexemption";
			}
			lngExemptLeft = ERec.ExemptValue[1] + ERec.ExemptValue[2] + ERec.ExemptValue[3];
			clsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAccount) + " order by rscard", modGlobalVariables.strREDatabase);
			while (!clsSave.EndOfFile())
			{
				clsSave.Edit();
				if (FCConvert.ToInt32(clsSave.Get_Fields_Int32("rscard")) == 1)
				{
					if (boolCurrOrBill)
					{
						clsSave.Set_Fields("currhashomestead", ERec.boolHomestead);
						clsSave.Set_Fields("currhomesteadVALUE", ERec.HomesteadValue);
						clsSave.Set_Fields("currlandexempt", ERec.LandExempt);
						clsSave.Set_Fields("currbldgexempt", ERec.BldgExempt);
						clsSave.Set_Fields("CurrExemptVal1", ERec.ExemptValue[1]);
						clsSave.Set_Fields("CurrExemptVal2", ERec.ExemptValue[2]);
						clsSave.Set_Fields("currexemptval3", ERec.ExemptValue[3]);
					}
					else
					{
						clsSave.Set_Fields("hashomestead", ERec.boolHomestead);
						clsSave.Set_Fields("homesteadvalue", ERec.HomesteadValue);
						clsSave.Set_Fields("landexempt", ERec.LandExempt);
						clsSave.Set_Fields("bldgexempt", ERec.BldgExempt);
						clsSave.Set_Fields("ExemptVal1", ERec.ExemptValue[1]);
						clsSave.Set_Fields("exemptval2", ERec.ExemptValue[2]);
						clsSave.Set_Fields("exemptval3", ERec.ExemptValue[3]);
					}
				}
				lngTotalTaxable = FCConvert.ToInt32(Conversion.Val(clsSave.Get_Fields(strLandField)) + Conversion.Val(clsSave.Get_Fields(strBldgField)));
				if (lngExemptLeft >= lngTotalTaxable)
				{
					clsSave.Set_Fields(strExemptField, lngTotalTaxable);
					lngExemptLeft -= lngTotalTaxable;
				}
				else
				{
					clsSave.Set_Fields(strExemptField, lngExemptLeft);
					lngExemptLeft = 0;
				}
				clsSave.Update();
				clsSave.MoveNext();
			}
			clsSave.DisposeOf();
		}

		public class StaticVariables
		{
            public double AssessmentRatio;
			public double CertifiedRatio;
        }

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
