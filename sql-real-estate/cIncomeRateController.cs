﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cIncomeRateController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cCommercialIncomeRate GetCommercialIncomeRate(int lngId)
		{
			cCommercialIncomeRate GetCommercialIncomeRate = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCommercialIncomeRate incRate = null;
				rsLoad.OpenRecordset("select * from CommercialIncomeRates where id = " + FCConvert.ToString(lngId), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					incRate = new cCommercialIncomeRate();
					// TODO Get_Fields: Field [OccupancyCodeNumber] not found!! (maybe it is an alias?)
					incRate.CommercialOccupancyCodeNumber = FCConvert.ToInt32(rsLoad.Get_Fields("OccupancyCodeNumber"));
					incRate.Rate = rsLoad.Get_Fields_Double("rate");
					incRate.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					incRate.IsUpdated = false;
				}
				GetCommercialIncomeRate = incRate;
				return GetCommercialIncomeRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCommercialIncomeRate;
		}

		public void DeleteIncomeRate(int lngId)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialIncomeRates where id = " + FCConvert.ToString(lngId), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveIncomeRate(ref cCommercialIncomeRate incRate)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (incRate.IsDeleted)
				{
					DeleteIncomeRate(incRate.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commercialincomerates where id = " + incRate.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (incRate.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Income rate record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("OccupancyCodeNumber", incRate.CommercialOccupancyCodeNumber);
				rsSave.Set_Fields("Rate", incRate.Rate);
				rsSave.Update();
				incRate.ID = rsSave.Get_Fields_Int32("ID");
				incRate.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
