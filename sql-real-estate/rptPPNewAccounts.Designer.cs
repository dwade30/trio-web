﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPPNewAccounts.
	/// </summary>
	partial class rptPPNewAccounts
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPPNewAccounts));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblC1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblC2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblC3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblC4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblC5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblC6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblC7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblC8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblC9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBusinessCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCat1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCategory = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbln1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbln2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbln3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbln4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbln5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbln6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbln7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbln8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbln9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusinessCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName,
				this.txtAssessment,
				this.txtLocation,
				this.txtCat2,
				this.txtCat1,
				this.txtCat3,
				this.txtCat4,
				this.txtCat5,
				this.txtCat6,
				this.txtCat7,
				this.txtCat8,
				this.txtCat9,
				this.lblC1,
				this.lblC2,
				this.lblC3,
				this.lblC4,
				this.lblC5,
				this.lblC6,
				this.lblC7,
				this.lblC8,
				this.lblC9,
				this.txtBusinessCode
			});
			this.Detail.Height = 0.6458333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label7,
				this.txtTotal,
				this.txtTotalAssessment,
				this.Label11,
				this.txtTotCat1,
				this.txtTotCat2,
				this.txtTotCat3,
				this.txtTotCat4,
				this.txtTotCat5,
				this.txtTotCat6,
				this.lblCat1,
				this.txtTotCat7,
				this.txttotCat8,
				this.txtTotCat9,
				this.lblCategory,
				this.lbln1,
				this.lbln2,
				this.lbln3,
				this.lbln4,
				this.lbln5,
				this.lbln6,
				this.lbln7,
				this.lbln8,
				this.lbln9,
				this.lblCat2,
				this.lblCat3,
				this.lblCat4,
				this.lblCat5,
				this.lblCat6,
				this.lblCat7,
				this.lblCat8,
				this.lblCat9
			});
			this.ReportFooter.Height = 2.40625F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.lblMuniname,
				this.lblTime,
				this.lblDate,
				this.lblPage,
				this.Label3,
				this.Label10,
				this.Label12,
				this.Label13,
				this.Label34
			});
			this.PageHeader.Height = 0.8125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "New Accounts";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.5F;
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.1875F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2.4375F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 2F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.5625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.875F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.5625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Name";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 0.6875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 6.5F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold; text-align: right";
			this.Label10.Text = "Assessment";
			this.Label10.Top = 0.5625F;
			this.Label10.Width = 0.9375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.5F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "text-align: center";
			this.Label12.Text = "Personal Property";
			this.Label12.Top = 0.25F;
			this.Label12.Width = 2.5F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.4375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold";
			this.Label13.Text = "Location";
			this.Label13.Top = 0.5625F;
			this.Label13.Width = 1.5F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 4.875F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-weight: bold; text-align: right";
			this.Label34.Text = "Business Code";
			this.Label34.Top = 0.5625F;
			this.Label34.Width = 1.375F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 2.375F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.Height = 0.1875F;
			this.txtAssessment.Left = 6.3125F;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 0F;
			this.txtAssessment.Width = 1.125F;
			// 
			// txtLocation
			// 
			this.txtLocation.CanGrow = false;
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 2.4375F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0F;
			this.txtLocation.Width = 2.375F;
			// 
			// txtCat2
			// 
			this.txtCat2.Height = 0.1875F;
			this.txtCat2.Left = 0.8125F;
			this.txtCat2.Name = "txtCat2";
			this.txtCat2.Style = "text-align: right";
			this.txtCat2.Text = null;
			this.txtCat2.Top = 0.375F;
			this.txtCat2.Width = 0.75F;
			// 
			// txtCat1
			// 
			this.txtCat1.Height = 0.1875F;
			this.txtCat1.Left = 0F;
			this.txtCat1.Name = "txtCat1";
			this.txtCat1.Style = "text-align: right";
			this.txtCat1.Text = null;
			this.txtCat1.Top = 0.375F;
			this.txtCat1.Width = 0.75F;
			// 
			// txtCat3
			// 
			this.txtCat3.Height = 0.1875F;
			this.txtCat3.Left = 1.625F;
			this.txtCat3.Name = "txtCat3";
			this.txtCat3.Style = "text-align: right";
			this.txtCat3.Text = null;
			this.txtCat3.Top = 0.375F;
			this.txtCat3.Width = 0.75F;
			// 
			// txtCat4
			// 
			this.txtCat4.Height = 0.1875F;
			this.txtCat4.Left = 2.4375F;
			this.txtCat4.Name = "txtCat4";
			this.txtCat4.Style = "text-align: right";
			this.txtCat4.Text = null;
			this.txtCat4.Top = 0.375F;
			this.txtCat4.Width = 0.75F;
			// 
			// txtCat5
			// 
			this.txtCat5.Height = 0.1875F;
			this.txtCat5.Left = 3.25F;
			this.txtCat5.Name = "txtCat5";
			this.txtCat5.Style = "text-align: right";
			this.txtCat5.Text = null;
			this.txtCat5.Top = 0.375F;
			this.txtCat5.Width = 0.75F;
			// 
			// txtCat6
			// 
			this.txtCat6.Height = 0.1875F;
			this.txtCat6.Left = 4.0625F;
			this.txtCat6.Name = "txtCat6";
			this.txtCat6.Style = "text-align: right";
			this.txtCat6.Text = null;
			this.txtCat6.Top = 0.375F;
			this.txtCat6.Width = 0.75F;
			// 
			// txtCat7
			// 
			this.txtCat7.Height = 0.1875F;
			this.txtCat7.Left = 4.875F;
			this.txtCat7.Name = "txtCat7";
			this.txtCat7.Style = "text-align: right";
			this.txtCat7.Text = null;
			this.txtCat7.Top = 0.375F;
			this.txtCat7.Width = 0.75F;
			// 
			// txtCat8
			// 
			this.txtCat8.Height = 0.1875F;
			this.txtCat8.Left = 5.6875F;
			this.txtCat8.Name = "txtCat8";
			this.txtCat8.Style = "text-align: right";
			this.txtCat8.Text = null;
			this.txtCat8.Top = 0.375F;
			this.txtCat8.Width = 0.75F;
			// 
			// txtCat9
			// 
			this.txtCat9.Height = 0.1875F;
			this.txtCat9.Left = 6.5F;
			this.txtCat9.Name = "txtCat9";
			this.txtCat9.Style = "text-align: right";
			this.txtCat9.Text = null;
			this.txtCat9.Top = 0.375F;
			this.txtCat9.Width = 0.75F;
			// 
			// lblC1
			// 
			this.lblC1.Height = 0.1875F;
			this.lblC1.HyperLink = null;
			this.lblC1.Left = 0F;
			this.lblC1.Name = "lblC1";
			this.lblC1.Style = "font-weight: bold; text-align: right";
			this.lblC1.Text = "Ctgry. 1";
			this.lblC1.Top = 0.1875F;
			this.lblC1.Width = 0.75F;
			// 
			// lblC2
			// 
			this.lblC2.Height = 0.1875F;
			this.lblC2.HyperLink = null;
			this.lblC2.Left = 0.8125F;
			this.lblC2.Name = "lblC2";
			this.lblC2.Style = "font-weight: bold; text-align: right";
			this.lblC2.Text = "Ctgry. 2";
			this.lblC2.Top = 0.1875F;
			this.lblC2.Width = 0.75F;
			// 
			// lblC3
			// 
			this.lblC3.Height = 0.1875F;
			this.lblC3.HyperLink = null;
			this.lblC3.Left = 1.625F;
			this.lblC3.Name = "lblC3";
			this.lblC3.Style = "font-weight: bold; text-align: right";
			this.lblC3.Text = "Ctgry. 3";
			this.lblC3.Top = 0.1875F;
			this.lblC3.Width = 0.75F;
			// 
			// lblC4
			// 
			this.lblC4.Height = 0.1875F;
			this.lblC4.HyperLink = null;
			this.lblC4.Left = 2.4375F;
			this.lblC4.Name = "lblC4";
			this.lblC4.Style = "font-weight: bold; text-align: right";
			this.lblC4.Text = "Ctgry. 4";
			this.lblC4.Top = 0.1875F;
			this.lblC4.Width = 0.75F;
			// 
			// lblC5
			// 
			this.lblC5.Height = 0.1875F;
			this.lblC5.HyperLink = null;
			this.lblC5.Left = 3.25F;
			this.lblC5.Name = "lblC5";
			this.lblC5.Style = "font-weight: bold; text-align: right";
			this.lblC5.Text = "Ctgry. 5";
			this.lblC5.Top = 0.1875F;
			this.lblC5.Width = 0.75F;
			// 
			// lblC6
			// 
			this.lblC6.Height = 0.1875F;
			this.lblC6.HyperLink = null;
			this.lblC6.Left = 4.0625F;
			this.lblC6.Name = "lblC6";
			this.lblC6.Style = "font-weight: bold; text-align: right";
			this.lblC6.Text = "Ctgry. 6";
			this.lblC6.Top = 0.1875F;
			this.lblC6.Width = 0.75F;
			// 
			// lblC7
			// 
			this.lblC7.Height = 0.1875F;
			this.lblC7.HyperLink = null;
			this.lblC7.Left = 4.875F;
			this.lblC7.Name = "lblC7";
			this.lblC7.Style = "font-weight: bold; text-align: right";
			this.lblC7.Text = "Ctgry. 7";
			this.lblC7.Top = 0.1875F;
			this.lblC7.Width = 0.75F;
			// 
			// lblC8
			// 
			this.lblC8.Height = 0.1875F;
			this.lblC8.HyperLink = null;
			this.lblC8.Left = 5.6875F;
			this.lblC8.Name = "lblC8";
			this.lblC8.Style = "font-weight: bold; text-align: right";
			this.lblC8.Text = "Ctgry. 8";
			this.lblC8.Top = 0.1875F;
			this.lblC8.Width = 0.75F;
			// 
			// lblC9
			// 
			this.lblC9.Height = 0.1875F;
			this.lblC9.HyperLink = null;
			this.lblC9.Left = 6.5F;
			this.lblC9.Name = "lblC9";
			this.lblC9.Style = "font-weight: bold; text-align: right";
			this.lblC9.Text = "Ctgry. 9";
			this.lblC9.Top = 0.1875F;
			this.lblC9.Width = 0.75F;
			// 
			// txtBusinessCode
			// 
			this.txtBusinessCode.Height = 0.1875F;
			this.txtBusinessCode.Left = 5.125F;
			this.txtBusinessCode.Name = "txtBusinessCode";
			this.txtBusinessCode.Style = "text-align: right";
			this.txtBusinessCode.Text = null;
			this.txtBusinessCode.Top = 0F;
			this.txtBusinessCode.Width = 1.125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.3125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "New Accounts:";
			this.Label7.Top = 0.125F;
			this.Label7.Width = 1.3125F;
			// 
			// txtTotal
			// 
			this.txtTotal.CanGrow = false;
			this.txtTotal.Height = 0.1875F;
			this.txtTotal.Left = 1.6875F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0.125F;
			this.txtTotal.Width = 1.25F;
			// 
			// txtTotalAssessment
			// 
			this.txtTotalAssessment.Height = 0.1875F;
			this.txtTotalAssessment.Left = 6.3125F;
			this.txtTotalAssessment.Name = "txtTotalAssessment";
			this.txtTotalAssessment.Style = "text-align: right";
			this.txtTotalAssessment.Text = null;
			this.txtTotalAssessment.Top = 0.125F;
			this.txtTotalAssessment.Width = 1.125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.5F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: right";
			this.Label11.Text = "Total:";
			this.Label11.Top = 0.125F;
			this.Label11.Width = 0.75F;
			// 
			// txtTotCat1
			// 
			this.txtTotCat1.Height = 0.1875F;
			this.txtTotCat1.Left = 3.875F;
			this.txtTotCat1.Name = "txtTotCat1";
			this.txtTotCat1.Style = "text-align: right";
			this.txtTotCat1.Text = null;
			this.txtTotCat1.Top = 0.6875F;
			this.txtTotCat1.Width = 1.125F;
			// 
			// txtTotCat2
			// 
			this.txtTotCat2.Height = 0.1875F;
			this.txtTotCat2.Left = 3.875F;
			this.txtTotCat2.Name = "txtTotCat2";
			this.txtTotCat2.Style = "text-align: right";
			this.txtTotCat2.Text = null;
			this.txtTotCat2.Top = 0.875F;
			this.txtTotCat2.Width = 1.125F;
			// 
			// txtTotCat3
			// 
			this.txtTotCat3.Height = 0.1875F;
			this.txtTotCat3.Left = 3.875F;
			this.txtTotCat3.Name = "txtTotCat3";
			this.txtTotCat3.Style = "text-align: right";
			this.txtTotCat3.Text = null;
			this.txtTotCat3.Top = 1.0625F;
			this.txtTotCat3.Width = 1.125F;
			// 
			// txtTotCat4
			// 
			this.txtTotCat4.Height = 0.1875F;
			this.txtTotCat4.Left = 3.875F;
			this.txtTotCat4.Name = "txtTotCat4";
			this.txtTotCat4.Style = "text-align: right";
			this.txtTotCat4.Text = null;
			this.txtTotCat4.Top = 1.25F;
			this.txtTotCat4.Width = 1.125F;
			// 
			// txtTotCat5
			// 
			this.txtTotCat5.Height = 0.1875F;
			this.txtTotCat5.Left = 3.875F;
			this.txtTotCat5.Name = "txtTotCat5";
			this.txtTotCat5.Style = "text-align: right";
			this.txtTotCat5.Text = null;
			this.txtTotCat5.Top = 1.4375F;
			this.txtTotCat5.Width = 1.125F;
			// 
			// txtTotCat6
			// 
			this.txtTotCat6.Height = 0.1875F;
			this.txtTotCat6.Left = 3.875F;
			this.txtTotCat6.Name = "txtTotCat6";
			this.txtTotCat6.Style = "text-align: right";
			this.txtTotCat6.Text = null;
			this.txtTotCat6.Top = 1.625F;
			this.txtTotCat6.Width = 1.125F;
			// 
			// lblCat1
			// 
			this.lblCat1.Height = 0.1875F;
			this.lblCat1.HyperLink = null;
			this.lblCat1.Left = 1.875F;
			this.lblCat1.Name = "lblCat1";
			this.lblCat1.Style = "font-weight: bold";
			this.lblCat1.Text = "Category 1";
			this.lblCat1.Top = 0.6875F;
			this.lblCat1.Width = 1.9375F;
			// 
			// txtTotCat7
			// 
			this.txtTotCat7.Height = 0.1875F;
			this.txtTotCat7.Left = 3.875F;
			this.txtTotCat7.Name = "txtTotCat7";
			this.txtTotCat7.Style = "text-align: right";
			this.txtTotCat7.Text = null;
			this.txtTotCat7.Top = 1.8125F;
			this.txtTotCat7.Width = 1.125F;
			// 
			// txttotCat8
			// 
			this.txttotCat8.Height = 0.1875F;
			this.txttotCat8.Left = 3.875F;
			this.txttotCat8.Name = "txttotCat8";
			this.txttotCat8.Style = "text-align: right";
			this.txttotCat8.Text = null;
			this.txttotCat8.Top = 2F;
			this.txttotCat8.Width = 1.125F;
			// 
			// txtTotCat9
			// 
			this.txtTotCat9.Height = 0.1875F;
			this.txtTotCat9.Left = 3.875F;
			this.txtTotCat9.Name = "txtTotCat9";
			this.txtTotCat9.Style = "text-align: right";
			this.txtTotCat9.Text = null;
			this.txtTotCat9.Top = 2.1875F;
			this.txtTotCat9.Width = 1.125F;
			// 
			// lblCategory
			// 
			this.lblCategory.Height = 0.1875F;
			this.lblCategory.HyperLink = null;
			this.lblCategory.Left = 1.875F;
			this.lblCategory.Name = "lblCategory";
			this.lblCategory.Style = "font-weight: bold";
			this.lblCategory.Text = "Category";
			this.lblCategory.Top = 0.5F;
			this.lblCategory.Width = 1.9375F;
			// 
			// lbln1
			// 
			this.lbln1.Height = 0.1875F;
			this.lbln1.HyperLink = null;
			this.lbln1.Left = 1.625F;
			this.lbln1.Name = "lbln1";
			this.lbln1.Style = "text-align: right";
			this.lbln1.Text = "1";
			this.lbln1.Top = 0.6875F;
			this.lbln1.Width = 0.1875F;
			// 
			// lbln2
			// 
			this.lbln2.Height = 0.1875F;
			this.lbln2.HyperLink = null;
			this.lbln2.Left = 1.625F;
			this.lbln2.Name = "lbln2";
			this.lbln2.Style = "text-align: right";
			this.lbln2.Text = "2";
			this.lbln2.Top = 0.875F;
			this.lbln2.Width = 0.1875F;
			// 
			// lbln3
			// 
			this.lbln3.Height = 0.1875F;
			this.lbln3.HyperLink = null;
			this.lbln3.Left = 1.625F;
			this.lbln3.Name = "lbln3";
			this.lbln3.Style = "text-align: right";
			this.lbln3.Text = "3";
			this.lbln3.Top = 1.0625F;
			this.lbln3.Width = 0.1875F;
			// 
			// lbln4
			// 
			this.lbln4.Height = 0.1875F;
			this.lbln4.HyperLink = null;
			this.lbln4.Left = 1.625F;
			this.lbln4.Name = "lbln4";
			this.lbln4.Style = "text-align: right";
			this.lbln4.Text = "4";
			this.lbln4.Top = 1.25F;
			this.lbln4.Width = 0.1875F;
			// 
			// lbln5
			// 
			this.lbln5.Height = 0.1875F;
			this.lbln5.HyperLink = null;
			this.lbln5.Left = 1.625F;
			this.lbln5.Name = "lbln5";
			this.lbln5.Style = "text-align: right";
			this.lbln5.Text = "5";
			this.lbln5.Top = 1.4375F;
			this.lbln5.Width = 0.1875F;
			// 
			// lbln6
			// 
			this.lbln6.Height = 0.1875F;
			this.lbln6.HyperLink = null;
			this.lbln6.Left = 1.625F;
			this.lbln6.Name = "lbln6";
			this.lbln6.Style = "text-align: right";
			this.lbln6.Text = "6";
			this.lbln6.Top = 1.625F;
			this.lbln6.Width = 0.1875F;
			// 
			// lbln7
			// 
			this.lbln7.Height = 0.1875F;
			this.lbln7.HyperLink = null;
			this.lbln7.Left = 1.625F;
			this.lbln7.Name = "lbln7";
			this.lbln7.Style = "text-align: right";
			this.lbln7.Text = "7";
			this.lbln7.Top = 1.8125F;
			this.lbln7.Width = 0.1875F;
			// 
			// lbln8
			// 
			this.lbln8.Height = 0.1875F;
			this.lbln8.HyperLink = null;
			this.lbln8.Left = 1.625F;
			this.lbln8.Name = "lbln8";
			this.lbln8.Style = "text-align: right";
			this.lbln8.Text = "8";
			this.lbln8.Top = 2F;
			this.lbln8.Width = 0.1875F;
			// 
			// lbln9
			// 
			this.lbln9.Height = 0.1875F;
			this.lbln9.HyperLink = null;
			this.lbln9.Left = 1.625F;
			this.lbln9.Name = "lbln9";
			this.lbln9.Style = "text-align: right";
			this.lbln9.Text = "9";
			this.lbln9.Top = 2.1875F;
			this.lbln9.Width = 0.1875F;
			// 
			// lblCat2
			// 
			this.lblCat2.Height = 0.1875F;
			this.lblCat2.HyperLink = null;
			this.lblCat2.Left = 1.875F;
			this.lblCat2.Name = "lblCat2";
			this.lblCat2.Style = "font-weight: bold";
			this.lblCat2.Text = "Category 2";
			this.lblCat2.Top = 0.875F;
			this.lblCat2.Width = 1.9375F;
			// 
			// lblCat3
			// 
			this.lblCat3.Height = 0.1875F;
			this.lblCat3.HyperLink = null;
			this.lblCat3.Left = 1.875F;
			this.lblCat3.Name = "lblCat3";
			this.lblCat3.Style = "font-weight: bold";
			this.lblCat3.Text = "Category 3";
			this.lblCat3.Top = 1.0625F;
			this.lblCat3.Width = 1.9375F;
			// 
			// lblCat4
			// 
			this.lblCat4.Height = 0.1875F;
			this.lblCat4.HyperLink = null;
			this.lblCat4.Left = 1.875F;
			this.lblCat4.Name = "lblCat4";
			this.lblCat4.Style = "font-weight: bold";
			this.lblCat4.Text = "Category 4";
			this.lblCat4.Top = 1.25F;
			this.lblCat4.Width = 1.9375F;
			// 
			// lblCat5
			// 
			this.lblCat5.Height = 0.1875F;
			this.lblCat5.HyperLink = null;
			this.lblCat5.Left = 1.875F;
			this.lblCat5.Name = "lblCat5";
			this.lblCat5.Style = "font-weight: bold";
			this.lblCat5.Text = "Category 5";
			this.lblCat5.Top = 1.4375F;
			this.lblCat5.Width = 1.9375F;
			// 
			// lblCat6
			// 
			this.lblCat6.Height = 0.1875F;
			this.lblCat6.HyperLink = null;
			this.lblCat6.Left = 1.875F;
			this.lblCat6.Name = "lblCat6";
			this.lblCat6.Style = "font-weight: bold";
			this.lblCat6.Text = "Category 6";
			this.lblCat6.Top = 1.625F;
			this.lblCat6.Width = 1.9375F;
			// 
			// lblCat7
			// 
			this.lblCat7.Height = 0.1875F;
			this.lblCat7.HyperLink = null;
			this.lblCat7.Left = 1.875F;
			this.lblCat7.Name = "lblCat7";
			this.lblCat7.Style = "font-weight: bold";
			this.lblCat7.Text = "Category 7";
			this.lblCat7.Top = 1.8125F;
			this.lblCat7.Width = 1.9375F;
			// 
			// lblCat8
			// 
			this.lblCat8.Height = 0.1875F;
			this.lblCat8.HyperLink = null;
			this.lblCat8.Left = 1.875F;
			this.lblCat8.Name = "lblCat8";
			this.lblCat8.Style = "font-weight: bold";
			this.lblCat8.Text = "Category 8";
			this.lblCat8.Top = 2F;
			this.lblCat8.Width = 1.9375F;
			// 
			// lblCat9
			// 
			this.lblCat9.Height = 0.1875F;
			this.lblCat9.HyperLink = null;
			this.lblCat9.Left = 1.875F;
			this.lblCat9.Name = "lblCat9";
			this.lblCat9.Style = "font-weight: bold";
			this.lblCat9.Text = "Category 9";
			this.lblCat9.Top = 2.1875F;
			this.lblCat9.Width = 1.9375F;
			// 
			// rptPPNewAccounts
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblC9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusinessCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbln9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCat9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblC1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblC2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblC3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblC4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblC5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblC6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblC7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblC8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblC9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBusinessCode;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotCat9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCategory;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbln1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbln2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbln3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbln4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbln5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbln6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbln7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbln8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbln9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat9;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
