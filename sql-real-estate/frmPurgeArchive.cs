﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPurgeArchive.
	/// </summary>
	public partial class frmPurgeArchive : BaseForm
	{
		public frmPurgeArchive()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPurgeArchive InstancePtr
		{
			get
			{
				return (frmPurgeArchive)Sys.GetInstance(typeof(frmPurgeArchive));
			}
		}

		protected frmPurgeArchive _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void cmdPurge_Click(object sender, System.EventArgs e)
		{
			int totalcount = 0;
			DateTime tempdate;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsPurge = new clsDRWrapper();
			if (!Information.IsDate(txtDate.Text))
			{
				MessageBox.Show("This is not a valid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (DateAndTime.DateValue(txtDate.Text).ToOADate() > DateAndTime.DateAdd("yyyy", -1, DateTime.Today).ToOADate())
			{
				MessageBox.Show("You must enter a date that is at least a year in the past.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			tempdate = DateAndTime.DateValue(txtDate.Text);
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			rsPurge.OpenRecordset("SELECT * FROM AuditChangesArchive WHERE DateUpdated <= '" + FCConvert.ToString(tempdate) + "'");
			if (rsPurge.EndOfFile() != true && rsPurge.BeginningOfFile() != true)
			{
				rsPurge.MoveLast();
				rsPurge.MoveFirst();
				totalcount = rsPurge.RecordCount();
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				ans = MessageBox.Show(FCConvert.ToString(totalcount) + " records will be deleted.  Do you wish to continue?", "Delete Records?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					frmWait.InstancePtr.Show();
					frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Deleting Records";
					frmWait.InstancePtr.Refresh();
					modGlobalFunctions.AddCYAEntry_26("RE", "Purged Audit Archive", tempdate.ToString());
					rsPurge.Execute("DELETE  FROM AuditChangesArchive WHERE DateUpdated <= '" + FCConvert.ToString(tempdate) + "'", modGlobalVariables.strREDatabase);
					frmWait.InstancePtr.Unload();
					//Application.DoEvents();
					MessageBox.Show("Purge Complete" + "\r\n" + FCConvert.ToString(totalcount) + " records deleted", "Purge Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					return;
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No records found that were saved before that date.", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			this.Unload();
		}
		// Private Sub Form_Activate()
		// If FormExist(Me) Then
		// Exit Sub
		// End If
		// Me.Refresh
		// End Sub
		private void frmPurgeArchive_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeArchive properties;
			//frmPurgeArchive.FillStyle	= 0;
			//frmPurgeArchive.ScaleWidth	= 5880;
			//frmPurgeArchive.ScaleHeight	= 3960;
			//frmPurgeArchive.LinkTopic	= "Form2";
			//frmPurgeArchive.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmPurgeArchive_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuProcessQuit_Click()
		{
			//mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}
	}
}
