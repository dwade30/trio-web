namespace TWRE0000
{
    /// <summary>
    /// Summary description for rptDeedNames.
    /// </summary>
    partial class rptDeedNames
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptDeedNames));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMap = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDeedName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDeedName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeedName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeedName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCaption,
            this.txtCaption2,
            this.Label6,
            this.Label9,
            this.Label10,
            this.txtMuni,
            this.txtTime,
            this.txtDate,
            this.txtPage,
            this.label1});
            this.pageHeader.Height = 0.7604166F;
            this.pageHeader.Name = "pageHeader";
            // 
            // txtCaption
            // 
            this.txtCaption.Height = 0.21875F;
            this.txtCaption.Left = 1.453F;
            this.txtCaption.Name = "txtCaption";
            this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
            this.txtCaption.Text = "Deed Names";
            this.txtCaption.Top = 0F;
            this.txtCaption.Width = 4.75F;
            // 
            // txtCaption2
            // 
            this.txtCaption2.Height = 0.1875F;
            this.txtCaption2.Left = 1.453F;
            this.txtCaption2.Name = "txtCaption2";
            this.txtCaption2.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 0";
            this.txtCaption2.Text = "Account";
            this.txtCaption2.Top = 0.21875F;
            this.txtCaption2.Width = 4.75F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.1875F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0.07787507F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.Label6.Text = "Account";
            this.Label6.Top = 0.56275F;
            this.Label6.Width = 0.625F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 2.062875F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.Label9.Text = "Deed Name 1";
            this.Label9.Top = 0.56275F;
            this.Label9.Width = 1.3125F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1875F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0.8278751F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.Label10.Text = "Map/Lot";
            this.Label10.Top = 0.56275F;
            this.Label10.Width = 1.125F;
            // 
            // txtMuni
            // 
            this.txtMuni.CanGrow = false;
            this.txtMuni.Height = 0.19F;
            this.txtMuni.Left = 0.07787507F;
            this.txtMuni.MultiLine = false;
            this.txtMuni.Name = "txtMuni";
            this.txtMuni.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtMuni.Text = "Field2";
            this.txtMuni.Top = 0.03075001F;
            this.txtMuni.Width = 1.625F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.19F;
            this.txtTime.Left = 0.07787507F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.txtTime.Text = "Field2";
            this.txtTime.Top = 0.187F;
            this.txtTime.Width = 1.375F;
            // 
            // txtDate
            // 
            this.txtDate.CanGrow = false;
            this.txtDate.Height = 0.19F;
            this.txtDate.Left = 6.318875F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
            this.txtDate.Text = "Field2";
            this.txtDate.Top = 0.03075001F;
            this.txtDate.Width = 1.0625F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1875F;
            this.txtPage.Left = 6.318875F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = null;
            this.txtPage.Top = 0.187F;
            this.txtPage.Width = 1.0625F;
            // 
            // label1
            // 
            this.label1.Height = 0.1875F;
            this.label1.HyperLink = null;
            this.label1.Left = 4.765875F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.label1.Text = "Deed Name 2";
            this.label1.Top = 0.56275F;
            this.label1.Width = 1.3125F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtAccount,
            this.txtMap,
            this.txtDeedName1,
            this.txtDeedName2});
            this.detail.Height = 0.1979166F;
            this.detail.Name = "detail";
            // 
            // txtAccount
            // 
            this.txtAccount.Height = 0.1875F;
            this.txtAccount.Left = 0.078F;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.txtAccount.Text = "Account";
            this.txtAccount.Top = 0F;
            this.txtAccount.Width = 0.6875F;
            // 
            // txtMap
            // 
            this.txtMap.CanGrow = false;
            this.txtMap.Height = 0.1875F;
            this.txtMap.Left = 0.8280001F;
            this.txtMap.MultiLine = false;
            this.txtMap.Name = "txtMap";
            this.txtMap.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.txtMap.Text = "Map/Lot";
            this.txtMap.Top = 0F;
            this.txtMap.Width = 1.177F;
            // 
            // txtDeedName1
            // 
            this.txtDeedName1.CanGrow = false;
            this.txtDeedName1.Height = 0.1875F;
            this.txtDeedName1.Left = 2.063F;
            this.txtDeedName1.MultiLine = false;
            this.txtDeedName1.Name = "txtDeedName1";
            this.txtDeedName1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.txtDeedName1.Text = null;
            this.txtDeedName1.Top = 0F;
            this.txtDeedName1.Width = 2.635F;
            // 
            // txtDeedName2
            // 
            this.txtDeedName2.CanGrow = false;
            this.txtDeedName2.Height = 0.1875F;
            this.txtDeedName2.Left = 4.766F;
            this.txtDeedName2.MultiLine = false;
            this.txtDeedName2.Name = "txtDeedName2";
            this.txtDeedName2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.txtDeedName2.Text = null;
            this.txtDeedName2.Top = 0F;
            this.txtDeedName2.Width = 2.615F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // rptDeedNames
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.46875F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeedName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeedName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMap;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeedName1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeedName2;
    }
}
