﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMVRPage2.
	/// </summary>
	partial class srptMVRPage2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMVRPage2));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Shape29 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape28 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape27 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine16a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape10 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine16b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine16aCalendarYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLine16aFiscalYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape11 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine17b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape12 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine17c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape16 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine19a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape17 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine19b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape18 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape19 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape20 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine21a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape21 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine21b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape22 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine21c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape23 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine21d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape24 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine21e = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label129 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label130 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label131 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label132 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label133 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label134 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSoftwood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMixedWood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHardwood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label152 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label154 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label155 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape26 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine17a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label156 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label157 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label158 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label159 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label160 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape30 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine15a = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label161 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label162 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label163 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape31 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine15c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label164 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label165 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape32 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine15d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label166 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label167 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape33 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine16c = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label168 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label169 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label170 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label171 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label172 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape34 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine15b = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label173 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label174 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label175 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label176 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape35 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLine16d = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label177 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16aCalendarYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16aFiscalYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine19a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine19b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine21a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine21b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine21c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine21d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine21e)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftwood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedWood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardwood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label152)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label154)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label155)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label156)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label157)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label158)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label159)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label160)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine15a)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label161)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label162)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label163)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine15c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label164)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label165)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine15d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label166)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label167)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16c)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label168)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label169)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label170)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label171)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label172)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine15b)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label173)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label174)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16d)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape29,
            this.Shape28,
            this.Shape27,
            this.Label69,
            this.txtMunicipality,
            this.lblTitle,
            this.Label74,
            this.Line3,
            this.Label58,
            this.Shape9,
            this.txtLine16a,
            this.Label37,
            this.Label38,
            this.Label75,
            this.Shape10,
            this.txtLine16b,
            this.Label77,
            this.Label78,
            this.Label79,
            this.Label81,
            this.lblYear,
            this.txtLine16aCalendarYear,
            this.txtLine16aFiscalYear,
            this.Label85,
            this.Label89,
            this.Shape11,
            this.txtLine17b,
            this.Label90,
            this.Shape12,
            this.txtLine17c,
            this.Label91,
            this.Line5,
            this.Label100,
            this.Shape16,
            this.txtLine19a,
            this.Label101,
            this.Label102,
            this.Label103,
            this.Shape17,
            this.txtLine19b,
            this.Label104,
            this.Label105,
            this.Line6,
            this.Label107,
            this.Shape18,
            this.txtLine22,
            this.Label109,
            this.Label110,
            this.Label111,
            this.Shape19,
            this.txtLine20,
            this.Label112,
            this.Label113,
            this.Label115,
            this.Label116,
            this.Label117,
            this.Label119,
            this.Shape20,
            this.txtLine21a,
            this.Label120,
            this.Label121,
            this.Shape21,
            this.txtLine21b,
            this.Label122,
            this.Label123,
            this.Shape22,
            this.txtLine21c,
            this.Label124,
            this.Label125,
            this.Shape23,
            this.txtLine21d,
            this.Label126,
            this.Label127,
            this.Shape24,
            this.txtLine21e,
            this.Label128,
            this.Label129,
            this.Label130,
            this.lblYear3,
            this.Label131,
            this.Label132,
            this.Label133,
            this.Label134,
            this.txtSoftwood,
            this.txtMixedWood,
            this.txtHardwood,
            this.Label152,
            this.Label154,
            this.Label155,
            this.Shape26,
            this.txtLine17a,
            this.Label156,
            this.Label157,
            this.Label158,
            this.Label159,
            this.Line7,
            this.Label160,
            this.Shape30,
            this.txtLine15a,
            this.Label161,
            this.Label162,
            this.Label163,
            this.Shape31,
            this.txtLine15c,
            this.Label164,
            this.Label16,
            this.Label165,
            this.Shape32,
            this.txtLine15d,
            this.Label166,
            this.Label167,
            this.Shape33,
            this.txtLine16c,
            this.Label168,
            this.Label169,
            this.Label170,
            this.lblYear4,
            this.Label171,
            this.Label172,
            this.Shape34,
            this.txtLine15b,
            this.Label173,
            this.Label174,
            this.Label175,
            this.Label176,
            this.Shape35,
            this.txtLine16d,
            this.Label177,
            this.Line4});
			this.Detail.Height = 9.635417F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// Shape29
			// 
			this.Shape29.Height = 0.19F;
			this.Shape29.Left = 5.625F;
			this.Shape29.Name = "Shape29";
			this.Shape29.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape29.Top = 8.830001F;
			this.Shape29.Width = 1.5625F;
			// 
			// Shape28
			// 
			this.Shape28.Height = 0.19F;
			this.Shape28.Left = 5.625F;
			this.Shape28.Name = "Shape28";
			this.Shape28.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape28.Top = 8.580001F;
			this.Shape28.Width = 1.5625F;
			// 
			// Shape27
			// 
			this.Shape27.Height = 0.19F;
			this.Shape27.Left = 5.625F;
			this.Shape27.Name = "Shape27";
			this.Shape27.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape27.Top = 8.330001F;
			this.Shape27.Width = 1.5625F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-style: italic";
			this.Label69.Text = "Municipality:";
			this.Label69.Top = 0.25F;
			this.Label69.Width = 0.7916667F;
			// 
			// txtMunicipality
			// 
			this.txtMunicipality.Height = 0.1875F;
			this.txtMunicipality.Left = 0.8125F;
			this.txtMunicipality.Name = "txtMunicipality";
			this.txtMunicipality.Style = "font-family: \'Times New Roman\'";
			this.txtMunicipality.Text = null;
			this.txtMunicipality.Top = 0.25F;
			this.txtMunicipality.Width = 3.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.1875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.lblTitle.Text = "MUNICIPAL VALUATION RETURN";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 5.5625F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1875F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 2.1875F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-family: \'Times New Roman\'; font-weight: bold";
			this.Label74.Text = "TAX INCREMENT FINANCING (TIF)";
			this.Label74.Top = 1.6875F;
			this.Label74.Width = 2.9375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.6875F;
			this.Line3.Width = 7.25F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 7.25F;
			this.Line3.Y1 = 1.6875F;
			this.Line3.Y2 = 1.6875F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 5.375F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-size: 9pt";
			this.Label58.Text = "16a";
			this.Label58.Top = 1.9375F;
			this.Label58.Width = 0.25F;
			// 
			// Shape9
			// 
			this.Shape9.Height = 0.19F;
			this.Shape9.Left = 5.625F;
			this.Shape9.Name = "Shape9";
			this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape9.Top = 1.9375F;
			this.Shape9.Width = 1.5625F;
			// 
			// txtLine16a
			// 
			this.txtLine16a.Height = 0.19F;
			this.txtLine16a.Left = 5.75F;
			this.txtLine16a.Name = "txtLine16a";
			this.txtLine16a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine16a.Text = " ";
			this.txtLine16a.Top = 1.9375F;
			this.txtLine16a.Width = 1.375F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1875F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 0F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-size: 9pt";
			this.Label37.Text = "16.";
			this.Label37.Top = 1.9375F;
			this.Label37.Width = 0.25F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1875F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 0.375F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label38.Text = "a. Total amount of increased taxable valuation above original assessed value with" +
    "in";
			this.Label38.Top = 1.9375F;
			this.Label38.Width = 5F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.1875F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 5.375F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "font-size: 9pt";
			this.Label75.Text = "16b";
			this.Label75.Top = 2.375F;
			this.Label75.Width = 0.25F;
			// 
			// Shape10
			// 
			this.Shape10.Height = 0.19F;
			this.Shape10.Left = 5.625F;
			this.Shape10.Name = "Shape10";
			this.Shape10.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape10.Top = 2.375F;
			this.Shape10.Width = 1.5625F;
			// 
			// txtLine16b
			// 
			this.txtLine16b.Height = 0.19F;
			this.txtLine16b.Left = 5.75F;
			this.txtLine16b.Name = "txtLine16b";
			this.txtLine16b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine16b.Text = " ";
			this.txtLine16b.Top = 2.375F;
			this.txtLine16b.Width = 1.375F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.1875F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 0.375F;
			this.Label77.Name = "Label77";
			this.Label77.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label77.Text = "b. Amount of captured assessed value within TIF Districts.";
			this.Label77.Top = 2.375F;
			this.Label77.Width = 5F;
			// 
			// Label78
			// 
			this.Label78.Height = 0.1875F;
			this.Label78.HyperLink = null;
			this.Label78.Left = 2.1875F;
			this.Label78.Name = "Label78";
			this.Label78.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label78.Text = "EXCISE TAX";
			this.Label78.Top = 3.625F;
			this.Label78.Width = 2.9375F;
			// 
			// Label79
			// 
			this.Label79.Height = 0.1875F;
			this.Label79.HyperLink = null;
			this.Label79.Left = 0F;
			this.Label79.Name = "Label79";
			this.Label79.Style = "font-size: 9pt";
			this.Label79.Text = "17.";
			this.Label79.Top = 3.875F;
			this.Label79.Width = 0.25F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.1875F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 0.375F;
			this.Label81.Name = "Label81";
			this.Label81.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label81.Text = "b. Motor vehicle excise tax collected.";
			this.Label81.Top = 4.122F;
			this.Label81.Width = 2.1875F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 3.729167F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear.Text = "2009";
			this.lblYear.Top = 3.9345F;
			this.lblYear.Visible = false;
			this.lblYear.Width = 0.375F;
			// 
			// txtLine16aCalendarYear
			// 
			this.txtLine16aCalendarYear.Height = 0.1875F;
			this.txtLine16aCalendarYear.Left = 3F;
			this.txtLine16aCalendarYear.Name = "txtLine16aCalendarYear";
			this.txtLine16aCalendarYear.Style = "text-align: right";
			this.txtLine16aCalendarYear.Text = " ";
			this.txtLine16aCalendarYear.Top = 4.372F;
			this.txtLine16aCalendarYear.Width = 0.1770833F;
			// 
			// txtLine16aFiscalYear
			// 
			this.txtLine16aFiscalYear.Height = 0.1875F;
			this.txtLine16aFiscalYear.Left = 3F;
			this.txtLine16aFiscalYear.Name = "txtLine16aFiscalYear";
			this.txtLine16aFiscalYear.Style = "text-align: right";
			this.txtLine16aFiscalYear.Text = " ";
			this.txtLine16aFiscalYear.Top = 4.122F;
			this.txtLine16aFiscalYear.Width = 0.1770833F;
			// 
			// Label85
			// 
			this.Label85.Height = 0.1875F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 0.375F;
			this.Label85.Name = "Label85";
			this.Label85.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label85.Text = "c. Watercraft excise tax collected.";
			this.Label85.Top = 4.372F;
			this.Label85.Width = 2.052083F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1875F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 5.375F;
			this.Label89.Name = "Label89";
			this.Label89.Style = "font-size: 9pt";
			this.Label89.Text = "17b";
			this.Label89.Top = 4.122F;
			this.Label89.Width = 0.25F;
			// 
			// Shape11
			// 
			this.Shape11.Height = 0.19F;
			this.Shape11.Left = 5.625F;
			this.Shape11.Name = "Shape11";
			this.Shape11.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape11.Top = 4.122F;
			this.Shape11.Width = 1.5625F;
			// 
			// txtLine17b
			// 
			this.txtLine17b.Height = 0.19F;
			this.txtLine17b.Left = 5.75F;
			this.txtLine17b.Name = "txtLine17b";
			this.txtLine17b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine17b.Text = " ";
			this.txtLine17b.Top = 4.122F;
			this.txtLine17b.Width = 1.375F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.1875F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 5.375F;
			this.Label90.Name = "Label90";
			this.Label90.Style = "font-size: 9pt";
			this.Label90.Text = "17c";
			this.Label90.Top = 4.372F;
			this.Label90.Width = 0.25F;
			// 
			// Shape12
			// 
			this.Shape12.Height = 0.19F;
			this.Shape12.Left = 5.625F;
			this.Shape12.Name = "Shape12";
			this.Shape12.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape12.Top = 4.372F;
			this.Shape12.Width = 1.5625F;
			// 
			// txtLine17c
			// 
			this.txtLine17c.Height = 0.19F;
			this.txtLine17c.Left = 5.75F;
			this.txtLine17c.Name = "txtLine17c";
			this.txtLine17c.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine17c.Text = " ";
			this.txtLine17c.Top = 4.372F;
			this.txtLine17c.Width = 1.375F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.1875F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 1.302083F;
			this.Label91.Name = "Label91";
			this.Label91.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label91.Text = "ELECTRICAL GENERATION AND DISTRIBUTION PROPERTY";
			this.Label91.Top = 4.705001F;
			this.Label91.Width = 4.40625F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 4.768372E-07F;
			this.Line5.LineWeight = 3F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 4.705001F;
			this.Line5.Width = 7.25F;
			this.Line5.X1 = 4.768372E-07F;
			this.Line5.X2 = 7.25F;
			this.Line5.Y1 = 4.705001F;
			this.Line5.Y2 = 4.705001F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.1875F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 5.375F;
			this.Label100.Name = "Label100";
			this.Label100.Style = "font-size: 9pt";
			this.Label100.Text = "18";
			this.Label100.Top = 5.048751F;
			this.Label100.Width = 0.25F;
			// 
			// Shape16
			// 
			this.Shape16.Height = 0.19F;
			this.Shape16.Left = 5.625F;
			this.Shape16.Name = "Shape16";
			this.Shape16.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape16.Top = 5.048751F;
			this.Shape16.Width = 1.5625F;
			// 
			// txtLine19a
			// 
			this.txtLine19a.Height = 0.19F;
			this.txtLine19a.Left = 5.75F;
			this.txtLine19a.Name = "txtLine19a";
			this.txtLine19a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine19a.Text = " ";
			this.txtLine19a.Top = 5.048751F;
			this.txtLine19a.Width = 1.375F;
			// 
			// Label101
			// 
			this.Label101.Height = 0.1875F;
			this.Label101.HyperLink = null;
			this.Label101.Left = 4.768372E-07F;
			this.Label101.Name = "Label101";
			this.Label101.Style = "font-size: 9pt; vertical-align: middle";
			this.Label101.Text = "18.";
			this.Label101.Top = 5.017501F;
			this.Label101.Width = 0.25F;
			// 
			// Label102
			// 
			this.Label102.Height = 0.1875F;
			this.Label102.HyperLink = null;
			this.Label102.Left = 0.3750005F;
			this.Label102.Name = "Label102";
			this.Label102.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; vertical-align: middle";
			this.Label102.Text = "Total valuation of distribution and transmission lines owned by electric utility " +
    "companies.";
			this.Label102.Top = 5.017501F;
			this.Label102.Width = 5F;
			// 
			// Label103
			// 
			this.Label103.Height = 0.1875F;
			this.Label103.HyperLink = null;
			this.Label103.Left = 5.375F;
			this.Label103.Name = "Label103";
			this.Label103.Style = "font-size: 9pt";
			this.Label103.Text = "19";
			this.Label103.Top = 5.330001F;
			this.Label103.Width = 0.25F;
			// 
			// Shape17
			// 
			this.Shape17.Height = 0.19F;
			this.Shape17.Left = 5.625F;
			this.Shape17.Name = "Shape17";
			this.Shape17.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape17.Top = 5.330001F;
			this.Shape17.Width = 1.5625F;
			// 
			// txtLine19b
			// 
			this.txtLine19b.Height = 0.19F;
			this.txtLine19b.Left = 5.75F;
			this.txtLine19b.Name = "txtLine19b";
			this.txtLine19b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine19b.Text = " ";
			this.txtLine19b.Top = 5.330001F;
			this.txtLine19b.Width = 1.375F;
			// 
			// Label104
			// 
			this.Label104.Height = 0.1875F;
			this.Label104.HyperLink = null;
			this.Label104.Left = 0.3750005F;
			this.Label104.Name = "Label104";
			this.Label104.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label104.Text = "Total valuation of all electrical generation facilities.";
			this.Label104.Top = 5.330001F;
			this.Label104.Width = 5F;
			// 
			// Label105
			// 
			this.Label105.Height = 0.1875F;
			this.Label105.HyperLink = null;
			this.Label105.Left = 1.375F;
			this.Label105.Name = "Label105";
			this.Label105.Style = "font-family: \'Times New Roman\'; font-weight: bold; text-align: center";
			this.Label105.Text = "FOREST LAND CLASSIFIED UNDER THE TREE GROWTH TAX LAW PROGRAM";
			this.Label105.Top = 5.642501F;
			this.Label105.Width = 4.75F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 4.768372E-07F;
			this.Line6.LineWeight = 3F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 5.642501F;
			this.Line6.Width = 7.25F;
			this.Line6.X1 = 4.768372E-07F;
			this.Line6.X2 = 7.25F;
			this.Line6.Y1 = 5.642501F;
			this.Line6.Y2 = 5.642501F;
			// 
			// Label107
			// 
			this.Label107.Height = 0.1875F;
			this.Label107.HyperLink = null;
			this.Label107.Left = 5.375F;
			this.Label107.Name = "Label107";
			this.Label107.Style = "font-size: 9pt";
			this.Label107.Text = "22";
			this.Label107.Top = 7.892501F;
			this.Label107.Width = 0.25F;
			// 
			// Shape18
			// 
			this.Shape18.Height = 0.1875F;
			this.Shape18.Left = 5.625F;
			this.Shape18.Name = "Shape18";
			this.Shape18.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape18.Top = 7.892501F;
			this.Shape18.Width = 1.5625F;
			// 
			// txtLine22
			// 
			this.txtLine22.Height = 0.1875F;
			this.txtLine22.Left = 5.75F;
			this.txtLine22.Name = "txtLine22";
			this.txtLine22.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine22.Text = " ";
			this.txtLine22.Top = 7.892501F;
			this.txtLine22.Width = 1.375F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.125F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 2.5F;
			this.Label109.Name = "Label109";
			this.Label109.Style = "font-family: \'Times New Roman\'; font-size: 7pt; font-weight: bold";
			this.Label109.Text = "(36 M.R.S.  §§ 571- 584-A)";
			this.Label109.Top = 5.830001F;
			this.Label109.Width = 2.864583F;
			// 
			// Label110
			// 
			this.Label110.Height = 0.1875F;
			this.Label110.HyperLink = null;
			this.Label110.Left = 0.01041715F;
			this.Label110.Name = "Label110";
			this.Label110.Style = "font-family: \'Times New Roman\'; font-size: 9pt; font-weight: bold; text-align: le" +
    "ft";
			this.Label110.Text = "FOREST LAND:";
			this.Label110.Top = 5.913334F;
			this.Label110.Visible = false;
			this.Label110.Width = 1.125F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.1875F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 5.375F;
			this.Label111.Name = "Label111";
			this.Label111.Style = "font-size: 9pt";
			this.Label111.Text = "20";
			this.Label111.Top = 6.080001F;
			this.Label111.Width = 0.25F;
			// 
			// Shape19
			// 
			this.Shape19.Height = 0.19F;
			this.Shape19.Left = 5.625F;
			this.Shape19.Name = "Shape19";
			this.Shape19.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape19.Top = 6.080001F;
			this.Shape19.Width = 1.5625F;
			// 
			// txtLine20
			// 
			this.txtLine20.Height = 0.19F;
			this.txtLine20.Left = 5.75F;
			this.txtLine20.Name = "txtLine20";
			this.txtLine20.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine20.Text = " ";
			this.txtLine20.Top = 6.080001F;
			this.txtLine20.Width = 1.375F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1875F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 4.768372E-07F;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-size: 9pt";
			this.Label112.Text = "20.";
			this.Label112.Top = 6.080001F;
			this.Label112.Width = 0.25F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.1875F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 0.3750005F;
			this.Label113.Name = "Label113";
			this.Label113.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label113.Text = "Average per acre unit value used for undeveloped acreage (land not classified)";
			this.Label113.Top = 6.080001F;
			this.Label113.Width = 5F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1875F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 4.768372E-07F;
			this.Label115.Name = "Label115";
			this.Label115.Style = "font-size: 9pt";
			this.Label115.Text = "21.";
			this.Label115.Top = 6.330001F;
			this.Label115.Width = 0.25F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.1875F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 0.3750005F;
			this.Label116.Name = "Label116";
			this.Label116.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label116.Text = "Classified forest land.";
			this.Label116.Top = 6.330001F;
			this.Label116.Width = 1.5625F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1875F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 1.9375F;
			this.Label117.Name = "Label117";
			this.Label117.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-weight: bold";
			this.Label117.Text = "(Do Not include land classified in Farmland as woodland)";
			this.Label117.Top = 6.330001F;
			this.Label117.Width = 3.4375F;
			// 
			// Label119
			// 
			this.Label119.Height = 0.1875F;
			this.Label119.HyperLink = null;
			this.Label119.Left = 5.375F;
			this.Label119.Name = "Label119";
			this.Label119.Style = "font-size: 9pt";
			this.Label119.Text = "21a";
			this.Label119.Top = 6.642501F;
			this.Label119.Width = 0.25F;
			// 
			// Shape20
			// 
			this.Shape20.Height = 0.19F;
			this.Shape20.Left = 5.625F;
			this.Shape20.Name = "Shape20";
			this.Shape20.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape20.Top = 6.642501F;
			this.Shape20.Width = 1.5625F;
			// 
			// txtLine21a
			// 
			this.txtLine21a.Height = 0.19F;
			this.txtLine21a.Left = 5.75F;
			this.txtLine21a.Name = "txtLine21a";
			this.txtLine21a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine21a.Text = " ";
			this.txtLine21a.Top = 6.642501F;
			this.txtLine21a.Width = 1.375F;
			// 
			// Label120
			// 
			this.Label120.Height = 0.1875F;
			this.Label120.HyperLink = null;
			this.Label120.Left = 0.3750005F;
			this.Label120.Name = "Label120";
			this.Label120.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label120.Text = "a. Number of parcels classified as of April 1,";
			this.Label120.Top = 6.642501F;
			this.Label120.Width = 5F;
			// 
			// Label121
			// 
			this.Label121.Height = 0.1875F;
			this.Label121.HyperLink = null;
			this.Label121.Left = 5.375F;
			this.Label121.Name = "Label121";
			this.Label121.Style = "font-size: 9pt";
			this.Label121.Text = "21b";
			this.Label121.Top = 6.892501F;
			this.Label121.Width = 0.25F;
			// 
			// Shape21
			// 
			this.Shape21.Height = 0.19F;
			this.Shape21.Left = 5.625F;
			this.Shape21.Name = "Shape21";
			this.Shape21.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape21.Top = 6.892501F;
			this.Shape21.Width = 1.5625F;
			// 
			// txtLine21b
			// 
			this.txtLine21b.Height = 0.19F;
			this.txtLine21b.Left = 5.75F;
			this.txtLine21b.Name = "txtLine21b";
			this.txtLine21b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine21b.Text = " ";
			this.txtLine21b.Top = 6.892501F;
			this.txtLine21b.Width = 1.375F;
			// 
			// Label122
			// 
			this.Label122.Height = 0.1875F;
			this.Label122.HyperLink = null;
			this.Label122.Left = 0.3750005F;
			this.Label122.Name = "Label122";
			this.Label122.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label122.Text = "b. Softwood acreage";
			this.Label122.Top = 6.892501F;
			this.Label122.Width = 5F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1875F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 5.375F;
			this.Label123.Name = "Label123";
			this.Label123.Style = "font-size: 9pt";
			this.Label123.Text = "21c";
			this.Label123.Top = 7.142501F;
			this.Label123.Width = 0.25F;
			// 
			// Shape22
			// 
			this.Shape22.Height = 0.19F;
			this.Shape22.Left = 5.625F;
			this.Shape22.Name = "Shape22";
			this.Shape22.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape22.Top = 7.142501F;
			this.Shape22.Width = 1.5625F;
			// 
			// txtLine21c
			// 
			this.txtLine21c.Height = 0.19F;
			this.txtLine21c.Left = 5.75F;
			this.txtLine21c.Name = "txtLine21c";
			this.txtLine21c.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine21c.Text = " ";
			this.txtLine21c.Top = 7.142501F;
			this.txtLine21c.Width = 1.375F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.1875F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 0.3750005F;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label124.Text = "c. Mixed wood acreage";
			this.Label124.Top = 7.142501F;
			this.Label124.Width = 5F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.1875F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 5.375F;
			this.Label125.Name = "Label125";
			this.Label125.Style = "font-size: 9pt";
			this.Label125.Text = "21d";
			this.Label125.Top = 7.392501F;
			this.Label125.Width = 0.25F;
			// 
			// Shape23
			// 
			this.Shape23.Height = 0.19F;
			this.Shape23.Left = 5.625F;
			this.Shape23.Name = "Shape23";
			this.Shape23.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape23.Top = 7.392501F;
			this.Shape23.Width = 1.5625F;
			// 
			// txtLine21d
			// 
			this.txtLine21d.Height = 0.19F;
			this.txtLine21d.Left = 5.75F;
			this.txtLine21d.Name = "txtLine21d";
			this.txtLine21d.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine21d.Text = " ";
			this.txtLine21d.Top = 7.392501F;
			this.txtLine21d.Width = 1.375F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.1875F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 0.3750005F;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label126.Text = "d. Hardwood acreage";
			this.Label126.Top = 7.392501F;
			this.Label126.Width = 5F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.1875F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 5.375F;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-size: 9pt";
			this.Label127.Text = "21e";
			this.Label127.Top = 7.642501F;
			this.Label127.Width = 0.25F;
			// 
			// Shape24
			// 
			this.Shape24.Height = 0.19F;
			this.Shape24.Left = 5.625F;
			this.Shape24.Name = "Shape24";
			this.Shape24.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape24.Top = 7.642501F;
			this.Shape24.Width = 1.5625F;
			// 
			// txtLine21e
			// 
			this.txtLine21e.Height = 0.19F;
			this.txtLine21e.Left = 5.75F;
			this.txtLine21e.Name = "txtLine21e";
			this.txtLine21e.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine21e.Text = " ";
			this.txtLine21e.Top = 7.642501F;
			this.txtLine21e.Width = 1.375F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.1875F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 0.3750005F;
			this.Label128.Name = "Label128";
			this.Label128.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label128.Text = "e. Total number of acres of forest land only (sum of lines 21 b, c, and d above)";
			this.Label128.Top = 7.642501F;
			this.Label128.Width = 5F;
			// 
			// Label129
			// 
			this.Label129.Height = 0.1875F;
			this.Label129.HyperLink = null;
			this.Label129.Left = 4.768372E-07F;
			this.Label129.Name = "Label129";
			this.Label129.Style = "font-size: 9pt";
			this.Label129.Text = "22.";
			this.Label129.Top = 7.892501F;
			this.Label129.Width = 0.25F;
			// 
			// Label130
			// 
			this.Label130.Height = 0.1875F;
			this.Label130.HyperLink = null;
			this.Label130.Left = 0.3750005F;
			this.Label130.Name = "Label130";
			this.Label130.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label130.Text = "Total assessed valuation of all classified forest land for tax year";
			this.Label130.Top = 7.892501F;
			this.Label130.Width = 3.125F;
			// 
			// lblYear3
			// 
			this.lblYear3.Height = 0.1875F;
			this.lblYear3.HyperLink = null;
			this.lblYear3.Left = 3.5F;
			this.lblYear3.Name = "lblYear3";
			this.lblYear3.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear3.Text = "2004";
			this.lblYear3.Top = 7.892501F;
			this.lblYear3.Width = 0.4375F;
			// 
			// Label131
			// 
			this.Label131.Height = 0.1875F;
			this.Label131.HyperLink = null;
			this.Label131.Left = 0.3750005F;
			this.Label131.Name = "Label131";
			this.Label131.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label131.Text = "a. Per acre values used to assess Tree Growth classified forest land value:";
			this.Label131.Top = 8.080001F;
			this.Label131.Width = 3.875F;
			// 
			// Label132
			// 
			this.Label132.Height = 0.1875F;
			this.Label132.HyperLink = null;
			this.Label132.Left = 0.477F;
			this.Label132.Name = "Label132";
			this.Label132.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label132.Text = "(1) Softwood";
			this.Label132.Top = 8.330001F;
			this.Label132.Width = 0.825F;
			// 
			// Label133
			// 
			this.Label133.Height = 0.1875F;
			this.Label133.HyperLink = null;
			this.Label133.Left = 0.4770001F;
			this.Label133.Name = "Label133";
			this.Label133.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label133.Text = "(2) Mixed wood";
			this.Label133.Top = 8.58F;
			this.Label133.Width = 1.229F;
			// 
			// Label134
			// 
			this.Label134.Height = 0.1875F;
			this.Label134.HyperLink = null;
			this.Label134.Left = 0.477F;
			this.Label134.Name = "Label134";
			this.Label134.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label134.Text = "(3) Hardwood";
			this.Label134.Top = 8.830001F;
			this.Label134.Width = 1.323F;
			// 
			// txtSoftwood
			// 
			this.txtSoftwood.Height = 0.19F;
			this.txtSoftwood.Left = 5.75F;
			this.txtSoftwood.Name = "txtSoftwood";
			this.txtSoftwood.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtSoftwood.Text = " ";
			this.txtSoftwood.Top = 8.330001F;
			this.txtSoftwood.Width = 1.375F;
			// 
			// txtMixedWood
			// 
			this.txtMixedWood.Height = 0.19F;
			this.txtMixedWood.Left = 5.75F;
			this.txtMixedWood.Name = "txtMixedWood";
			this.txtMixedWood.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtMixedWood.Text = " ";
			this.txtMixedWood.Top = 8.580001F;
			this.txtMixedWood.Width = 1.375F;
			// 
			// txtHardwood
			// 
			this.txtHardwood.Height = 0.19F;
			this.txtHardwood.Left = 5.75F;
			this.txtHardwood.Name = "txtHardwood";
			this.txtHardwood.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtHardwood.Text = " ";
			this.txtHardwood.Top = 8.830001F;
			this.txtHardwood.Width = 1.375F;
			// 
			// Label152
			// 
			this.Label152.Height = 0.1875F;
			this.Label152.HyperLink = null;
			this.Label152.Left = 3.25F;
			this.Label152.Name = "Label152";
			this.Label152.Style = "font-family: \'Times New Roman\'; font-size: 9pt; text-align: center";
			this.Label152.Text = "-2-";
			this.Label152.Top = 9.017501F;
			this.Label152.Width = 1F;
			// 
			// Label154
			// 
			this.Label154.Height = 0.1875F;
			this.Label154.HyperLink = null;
			this.Label154.Left = 0.375F;
			this.Label154.Name = "Label154";
			this.Label154.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label154.Text = "a. Enter whether excise taxes are collected based on a calendar or fiscal year";
			this.Label154.Top = 3.872F;
			this.Label154.Width = 4.0625F;
			// 
			// Label155
			// 
			this.Label155.Height = 0.1875F;
			this.Label155.HyperLink = null;
			this.Label155.Left = 5.375F;
			this.Label155.Name = "Label155";
			this.Label155.Style = "font-size: 9pt";
			this.Label155.Text = "17a";
			this.Label155.Top = 3.872F;
			this.Label155.Width = 0.25F;
			// 
			// Shape26
			// 
			this.Shape26.Height = 0.19F;
			this.Shape26.Left = 5.625F;
			this.Shape26.Name = "Shape26";
			this.Shape26.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape26.Top = 3.872F;
			this.Shape26.Width = 1.5625F;
			// 
			// txtLine17a
			// 
			this.txtLine17a.Height = 0.19F;
			this.txtLine17a.Left = 5.75F;
			this.txtLine17a.Name = "txtLine17a";
			this.txtLine17a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine17a.Text = " ";
			this.txtLine17a.Top = 3.872F;
			this.txtLine17a.Width = 1.375F;
			// 
			// Label156
			// 
			this.Label156.Height = 0.1875F;
			this.Label156.HyperLink = null;
			this.Label156.Left = 5.1875F;
			this.Label156.Name = "Label156";
			this.Label156.Style = "font-size: 9pt";
			this.Label156.Text = "22a(1)";
			this.Label156.Top = 8.330001F;
			this.Label156.Width = 0.4375F;
			// 
			// Label157
			// 
			this.Label157.Height = 0.1875F;
			this.Label157.HyperLink = null;
			this.Label157.Left = 5.1875F;
			this.Label157.Name = "Label157";
			this.Label157.Style = "font-size: 9pt";
			this.Label157.Text = "22a(2)";
			this.Label157.Top = 8.580001F;
			this.Label157.Width = 0.4375F;
			// 
			// Label158
			// 
			this.Label158.Height = 0.1875F;
			this.Label158.HyperLink = null;
			this.Label158.Left = 5.1875F;
			this.Label158.Name = "Label158";
			this.Label158.Style = "font-size: 9pt";
			this.Label158.Text = "22a(3)";
			this.Label158.Top = 8.830001F;
			this.Label158.Width = 0.4375F;
			// 
			// Label159
			// 
			this.Label159.Height = 0.1875F;
			this.Label159.HyperLink = null;
			this.Label159.Left = 1F;
			this.Label159.Name = "Label159";
			this.Label159.Style = "font-family: \'Times New Roman\'; font-weight: bold";
			this.Label159.Text = "BUSINESS EQUIPMENT TAX EXEMPTION (BETE) REIMBURSEMENT CLAIM";
			this.Label159.Top = 0.5F;
			this.Label159.Width = 5.4375F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 0F;
			this.Line7.LineWeight = 3F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 0.5F;
			this.Line7.Width = 7.25F;
			this.Line7.X1 = 0F;
			this.Line7.X2 = 7.25F;
			this.Line7.Y1 = 0.5F;
			this.Line7.Y2 = 0.5F;
			// 
			// Label160
			// 
			this.Label160.Height = 0.1875F;
			this.Label160.HyperLink = null;
			this.Label160.Left = 5.375F;
			this.Label160.Name = "Label160";
			this.Label160.Style = "font-size: 9pt";
			this.Label160.Text = "15a";
			this.Label160.Top = 0.75F;
			this.Label160.Width = 0.25F;
			// 
			// Shape30
			// 
			this.Shape30.Height = 0.19F;
			this.Shape30.Left = 5.625F;
			this.Shape30.Name = "Shape30";
			this.Shape30.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape30.Top = 0.75F;
			this.Shape30.Width = 1.5625F;
			// 
			// txtLine15a
			// 
			this.txtLine15a.Height = 0.19F;
			this.txtLine15a.Left = 5.75F;
			this.txtLine15a.Name = "txtLine15a";
			this.txtLine15a.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine15a.Text = " ";
			this.txtLine15a.Top = 0.75F;
			this.txtLine15a.Width = 1.375F;
			// 
			// Label161
			// 
			this.Label161.Height = 0.1875F;
			this.Label161.HyperLink = null;
			this.Label161.Left = 0F;
			this.Label161.Name = "Label161";
			this.Label161.Style = "font-size: 9pt";
			this.Label161.Text = "15.";
			this.Label161.Top = 0.75F;
			this.Label161.Width = 0.25F;
			// 
			// Label162
			// 
			this.Label162.Height = 0.1875F;
			this.Label162.HyperLink = null;
			this.Label162.Left = 0.375F;
			this.Label162.Name = "Label162";
			this.Label162.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label162.Text = "c. Total Exempt value of all BETE qualified property";
			this.Label162.Top = 1.125F;
			this.Label162.Width = 5F;
			// 
			// Label163
			// 
			this.Label163.Height = 0.1875F;
			this.Label163.HyperLink = null;
			this.Label163.Left = 5.375F;
			this.Label163.Name = "Label163";
			this.Label163.Style = "font-size: 9pt";
			this.Label163.Text = "15c";
			this.Label163.Top = 1.125F;
			this.Label163.Width = 0.25F;
			// 
			// Shape31
			// 
			this.Shape31.Height = 0.19F;
			this.Shape31.Left = 5.625F;
			this.Shape31.Name = "Shape31";
			this.Shape31.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape31.Top = 1.125F;
			this.Shape31.Width = 1.5625F;
			// 
			// txtLine15c
			// 
			this.txtLine15c.Height = 0.19F;
			this.txtLine15c.Left = 5.75F;
			this.txtLine15c.Name = "txtLine15c";
			this.txtLine15c.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine15c.Text = " ";
			this.txtLine15c.Top = 1.125F;
			this.txtLine15c.Width = 1.375F;
			// 
			// Label164
			// 
			this.Label164.Height = 0.1875F;
			this.Label164.HyperLink = null;
			this.Label164.Left = 0.375F;
			this.Label164.Name = "Label164";
			this.Label164.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label164.Text = "d. Total exempt value of BETE property located in a municipal retention TIF distr" +
    "ict.";
			this.Label164.Top = 1.5F;
			this.Label164.Width = 5F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.5F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-weight: bold";
			this.Label16.Text = "(must match Municipal Tax Rate Calculation Standard Form page 10, line 5a)";
			this.Label16.Top = 1.3125F;
			this.Label16.Width = 4.5F;
			// 
			// Label165
			// 
			this.Label165.Height = 0.1875F;
			this.Label165.HyperLink = null;
			this.Label165.Left = 5.375F;
			this.Label165.Name = "Label165";
			this.Label165.Style = "font-size: 9pt";
			this.Label165.Text = "15d";
			this.Label165.Top = 1.5F;
			this.Label165.Width = 0.25F;
			// 
			// Shape32
			// 
			this.Shape32.Height = 0.19F;
			this.Shape32.Left = 5.625F;
			this.Shape32.Name = "Shape32";
			this.Shape32.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape32.Top = 1.5F;
			this.Shape32.Width = 1.5625F;
			// 
			// txtLine15d
			// 
			this.txtLine15d.Height = 0.19F;
			this.txtLine15d.Left = 5.75F;
			this.txtLine15d.Name = "txtLine15d";
			this.txtLine15d.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine15d.Text = " ";
			this.txtLine15d.Top = 1.5F;
			this.txtLine15d.Width = 1.375F;
			// 
			// Label166
			// 
			this.Label166.Height = 0.1875F;
			this.Label166.HyperLink = null;
			this.Label166.Left = 0.375F;
			this.Label166.Name = "Label166";
			this.Label166.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label166.Text = "a. Number of BETE applications processed for tax year 2013";
			this.Label166.Top = 0.75F;
			this.Label166.Width = 5F;
			// 
			// Label167
			// 
			this.Label167.Height = 0.1875F;
			this.Label167.HyperLink = null;
			this.Label167.Left = 5.375F;
			this.Label167.Name = "Label167";
			this.Label167.Style = "font-size: 9pt";
			this.Label167.Text = "16c";
			this.Label167.Top = 2.8125F;
			this.Label167.Width = 0.25F;
			// 
			// Shape33
			// 
			this.Shape33.Height = 0.19F;
			this.Shape33.Left = 5.625F;
			this.Shape33.Name = "Shape33";
			this.Shape33.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape33.Top = 2.8125F;
			this.Shape33.Width = 1.5625F;
			// 
			// txtLine16c
			// 
			this.txtLine16c.Height = 0.19F;
			this.txtLine16c.Left = 5.75F;
			this.txtLine16c.Name = "txtLine16c";
			this.txtLine16c.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine16c.Text = " ";
			this.txtLine16c.Top = 2.8125F;
			this.txtLine16c.Width = 1.375F;
			// 
			// Label168
			// 
			this.Label168.Height = 0.1875F;
			this.Label168.HyperLink = null;
			this.Label168.Left = 0.375F;
			this.Label168.Name = "Label168";
			this.Label168.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label168.Text = "c. Property tax revenue that is appropriated and deposited into either a project";
			this.Label168.Top = 2.625F;
			this.Label168.Width = 5F;
			// 
			// Label169
			// 
			this.Label169.Height = 0.1875F;
			this.Label169.HyperLink = null;
			this.Label169.Left = 0.5138889F;
			this.Label169.Name = "Label169";
			this.Label169.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label169.Text = "cost account or a sinking fund account";
			this.Label169.Top = 2.8125F;
			this.Label169.Width = 2.125F;
			// 
			// Label170
			// 
			this.Label170.Height = 0.1875F;
			this.Label170.HyperLink = null;
			this.Label170.Left = 0.5138889F;
			this.Label170.Name = "Label170";
			this.Label170.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label170.Text = "TIF Districts";
			this.Label170.Top = 2.125F;
			this.Label170.Width = 5F;
			// 
			// lblYear4
			// 
			this.lblYear4.Height = 0.1875F;
			this.lblYear4.HyperLink = null;
			this.lblYear4.Left = 2.5625F;
			this.lblYear4.Name = "lblYear4";
			this.lblYear4.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.lblYear4.Text = "2004";
			this.lblYear4.Top = 6.642501F;
			this.lblYear4.Visible = false;
			this.lblYear4.Width = 0.4375F;
			// 
			// Label171
			// 
			this.Label171.Height = 0.1875F;
			this.Label171.HyperLink = null;
			this.Label171.Left = 0.375F;
			this.Label171.Name = "Label171";
			this.Label171.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label171.Text = "b. Number of BETE applications approved";
			this.Label171.Top = 0.9375F;
			this.Label171.Width = 5F;
			// 
			// Label172
			// 
			this.Label172.Height = 0.1875F;
			this.Label172.HyperLink = null;
			this.Label172.Left = 5.375F;
			this.Label172.Name = "Label172";
			this.Label172.Style = "font-size: 9pt";
			this.Label172.Text = "15b";
			this.Label172.Top = 0.9375F;
			this.Label172.Width = 0.25F;
			// 
			// Shape34
			// 
			this.Shape34.Height = 0.19F;
			this.Shape34.Left = 5.625F;
			this.Shape34.Name = "Shape34";
			this.Shape34.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape34.Top = 0.9375F;
			this.Shape34.Width = 1.5625F;
			// 
			// txtLine15b
			// 
			this.txtLine15b.Height = 0.19F;
			this.txtLine15b.Left = 5.75F;
			this.txtLine15b.Name = "txtLine15b";
			this.txtLine15b.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine15b.Text = " ";
			this.txtLine15b.Top = 0.9375F;
			this.txtLine15b.Width = 1.375F;
			// 
			// Label173
			// 
			this.Label173.Height = 0.1875F;
			this.Label173.HyperLink = null;
			this.Label173.Left = 0.375F;
			this.Label173.Name = "Label173";
			this.Label173.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt; font-weight: bold";
			this.Label173.Text = "(Lines 16c and 16d combined must match Municipal Tax Rate Calculation Standard Fo" +
    "rm page 10, line 9)";
			this.Label173.Top = 3.4375F;
			this.Label173.Width = 6.3125F;
			// 
			// Label174
			// 
			this.Label174.Height = 0.1875F;
			this.Label174.HyperLink = null;
			this.Label174.Left = 0.375F;
			this.Label174.Name = "Label174";
			this.Label174.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label174.Text = "d. BETE reimbursement revenue that is appropriated and deposited into either a pr" +
    "oject";
			this.Label174.Top = 3.0625F;
			this.Label174.Width = 5F;
			// 
			// Label175
			// 
			this.Label175.Height = 0.1875F;
			this.Label175.HyperLink = null;
			this.Label175.Left = 0.5138889F;
			this.Label175.Name = "Label175";
			this.Label175.Style = "font-family: \'Times New Roman\'; font-size: 8.5pt";
			this.Label175.Text = "cost account or a sinking fund account";
			this.Label175.Top = 3.25F;
			this.Label175.Width = 2.125F;
			// 
			// Label176
			// 
			this.Label176.Height = 0.1875F;
			this.Label176.HyperLink = null;
			this.Label176.Left = 5.375F;
			this.Label176.Name = "Label176";
			this.Label176.Style = "font-size: 9pt";
			this.Label176.Text = "16d";
			this.Label176.Top = 3.25F;
			this.Label176.Width = 0.25F;
			// 
			// Shape35
			// 
			this.Shape35.Height = 0.19F;
			this.Shape35.Left = 5.625F;
			this.Shape35.Name = "Shape35";
			this.Shape35.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape35.Top = 3.25F;
			this.Shape35.Width = 1.5625F;
			// 
			// txtLine16d
			// 
			this.txtLine16d.Height = 0.19F;
			this.txtLine16d.Left = 5.75F;
			this.txtLine16d.Name = "txtLine16d";
			this.txtLine16d.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtLine16d.Text = " ";
			this.txtLine16d.Top = 3.25F;
			this.txtLine16d.Width = 1.375F;
			// 
			// Label177
			// 
			this.Label177.Height = 0.1875F;
			this.Label177.HyperLink = null;
			this.Label177.Left = 4.768372E-07F;
			this.Label177.Name = "Label177";
			this.Label177.Style = "font-size: 9pt; vertical-align: middle";
			this.Label177.Text = "19.";
			this.Label177.Top = 5.330001F;
			this.Label177.Width = 0.25F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0F;
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 3.625F;
			this.Line4.Width = 7.25F;
			this.Line4.X1 = 0F;
			this.Line4.X2 = 7.25F;
			this.Line4.Y1 = 3.625F;
			this.Line4.Y2 = 3.625F;
			// 
			// srptMVRPage2
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.416667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16aCalendarYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16aFiscalYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine19a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine19b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine21a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine21b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine21c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine21d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine21e)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label129)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label130)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label131)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label132)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label133)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label134)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSoftwood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMixedWood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHardwood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label152)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label154)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label155)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine17a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label156)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label157)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label158)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label159)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label160)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine15a)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label161)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label162)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label163)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine15c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label164)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label165)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine15d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label166)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label167)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16c)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label168)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label169)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label170)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label171)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label172)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine15b)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label173)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label174)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label175)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label176)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine16d)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label177)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape29;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape28;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine16a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine16b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label79;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine16aCalendarYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine16aFiscalYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine19a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label103;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine19b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label105;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine21a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine21b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine21c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine21d;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine21e;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label129;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label130;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label131;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label132;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label133;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label134;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSoftwood;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMixedWood;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHardwood;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label152;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label154;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label155;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine17a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label156;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label157;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label158;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label159;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label160;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine15a;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label161;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label162;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label163;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine15c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label164;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label165;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine15d;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label166;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label167;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine16c;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label168;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label169;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label170;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label171;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label172;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine15b;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label173;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label174;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label175;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label176;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine16d;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label177;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
	}
}
