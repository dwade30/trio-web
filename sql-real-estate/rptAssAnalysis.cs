﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptAssAnalysis.
	/// </summary>
	public partial class rptAssAnalysis : BaseSectionReport
	{
		public static rptAssAnalysis InstancePtr
		{
			get
			{
				return (rptAssAnalysis)Sys.GetInstance(typeof(rptAssAnalysis));
			}
		}

		protected rptAssAnalysis _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsTemp?.Dispose();
                clsTemp = null;
            }
			base.Dispose(disposing);
		}

		public rptAssAnalysis()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Assessment Analysis";
		}
		// nObj = 1
		//   0	rptAssAnalysis	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsTemp = new clsDRWrapper();
		// vbPorter upgrade warning: intPage As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		int intPage;

		public void Init()
		{
			int lngTown = 0;
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				lngTown = frmPickRegionalTown.InstancePtr.Init(1);
				if (lngTown < 1)
				{
					return;
				}
				modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = lngTown;
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "AssessmentAnalysis");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			SubReport1.Report = new rptLandCodes();
			SubReport2.Report = new rptNewLandBreakdown();
			SubReport3.Report = new srptoutbuilding();
			SubReport4.Report = new srptDwellAnalysis();
			SubReport5.Report = new srptBldgStyle();
			SubReport6.Report = new srptDwellAge();
			SubReport7.Report = new srptCommercial();
			SubReport8.Report = new srptExemptAnal();
			SubReport9.Report = new srptLandOverrides();
			SubReport10.Report = new srptBldgOV();
			// SubReport1.object.strLand = "RLLANDVAL"
			intPage = 1;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			// vbPorter upgrade warning: lngCount As int	OnWrite(int, double)
			int lngCount;
			// vbPorter upgrade warning: lngTotal As int	OnWrite(int, double)
			int lngTotal;
			string strSQL = "";
			try
			{
				// On Error GoTo ErrorHandler
				if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
				{
					strSQL = "select count(*) as thecount,sum(rllandval) as totland, sum(piacres) as totacres from master where not rsdeleted = 1";
				}
				else
				{
					strSQL = "select count(*) as thecount,sum(rllandval) as totland, sum(piacres) as totacres from master where not rsdeleted = 1 and isnull(ritrancode,0) = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				txtCount1.Text = FCConvert.ToString(clsTemp.GetData("thecount"));
				txtAssess1.Text = Strings.Format(clsTemp.GetData("totland"), "##,###,###,##0");
				txtAcreage1.Text = Strings.Format(clsTemp.GetData("totacres"), "#,###,##0.00");
				lngCount = 0;
				lngTotal = 0;
				for (x = 1; x <= 10; x++)
				{
					if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
					{
						strSQL = "select count(sobc" + FCConvert.ToString(x) + ")as thecount, sum(sobv" + FCConvert.ToString(x) + ") as thetotal from summrecord where isnull(sobc" + FCConvert.ToString(x) + ",0)   <> 0";
					}
					else
					{
						strSQL = "select count(sobc" + FCConvert.ToString(x) + " )as thecount, sum(sobv" + FCConvert.ToString(x) + " ) as thetotal from summrecord inner join master on (master.rsaccount = summrecord.srecordnumber) and (master.rscard = summrecord.cardnumber) where isnull(sobc" + FCConvert.ToString(x) + ",0)   <> 0 and isnull(ritrancode,0) = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
					}
					clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					lngCount += FCConvert.ToInt32(Conversion.Val(clsTemp.GetData("thecount")));
					if (Conversion.Val(clsTemp.GetData("thecount")) > 0)
					{
						lngTotal += FCConvert.ToInt32(Conversion.Val(clsTemp.GetData("thetotal")));
					}
				}
				// x
				txtoutcount.Text = lngCount.ToString();
				txtoutassess.Text = Strings.Format(lngTotal, "###,###,###,##0");
				if (lngCount > 0)
				{
					txtAverage.Text = Strings.Format(FCConvert.ToDouble(lngTotal) / lngCount, "###,###,##0");
				}
				else
				{
					txtAverage.Text = "0";
				}
				// Call clsTemp.OpenRecordset("select count(*) as thecount, sum(rlbldgval) as thetotal from master where rsdwellingcode = 'Y' and val(ribldgcode) > 0", strredatabase)
				// Call clsTemp.OpenRecordset("select count(*) as thecount, sum(val(sdwellrcnld & '')) as thetotal from dwelling inner join (master inner join summrecord on (master.rsaccount = summrecord.srecordnumber) and (master.rscard = summrecord.cardnumber)) on (dwelling.rsaccount = master.rsaccount) and (dwelling.rscard = master.rscard) where  not master.rsdeleted and (master.rsdwellingcode & '') = 'Y'", strredatabase)
				if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
				{
					strSQL = "select count(*) as thecount, sum(isnull(sdwellrcnld ,0)) as thetotal from dwelling inner join (master inner join summrecord on (master.rsaccount = summrecord.srecordnumber) and (master.rscard = summrecord.cardnumber)) on (dwelling.rsaccount = master.rsaccount) and (dwelling.rscard = master.rscard) where  not master.rsdeleted = 1";
				}
				else
				{
					strSQL = "select count(*) as thecount, sum(isnull(sdwellrcnld ,0)) as thetotal from dwelling inner join (master inner join summrecord on (master.rsaccount = summrecord.srecordnumber) and (master.rscard = summrecord.cardnumber)) on (dwelling.rsaccount = master.rsaccount) and (dwelling.rscard = master.rscard) where  not master.rsdeleted = 1 and isnull(ritrancode,0) = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thecount"))));
					// TODO Get_Fields: Field [thetotal] not found!! (maybe it is an alias?)
					lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thetotal"))));
					if (lngCount > 0)
					{
						txtBldgAve.Text = Strings.Format(FCConvert.ToDouble(lngTotal) / lngCount, "###,###,##0");
					}
					else
					{
						txtBldgAve.Text = "0";
					}
				}
				else
				{
					lngCount = 0;
					lngTotal = 0;
					txtBldgAve.Text = "0";
				}
				txtBldgCount.Text = lngCount.ToString();
				txtBldgAssess.Text = Strings.Format(lngTotal, "###,###,###,##0");
				if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
				{
					strSQL = "select count (comval1) as thecount, sum(comval1 + isnull(comval2,0)) as thetotal from summrecord where comval1 > 0";
				}
				else
				{
					strSQL = "select count (comval1) as thecount, sum(comval1 + isnull(comval2,0)) as thetotal from summrecord inner join master on (master.rsaccount = summrecord.srecordnumber) and (master.rscard = summrecord.cardnumber) where comval1 > 0 and isnull(ritrancode,0) = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thecount"))));
					// TODO Get_Fields: Field [thetotal] not found!! (maybe it is an alias?)
					lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thetotal"))));
					if (lngCount == 0)
					{
						txtCommAve.Text = "0";
					}
					else
					{
						txtCommAve.Text = Strings.Format(FCConvert.ToDouble(lngTotal) / lngCount, "###,###,##0");
					}
				}
				else
				{
					lngCount = 0;
					lngTotal = 0;
					txtCommAve.Text = "0";
				}
				txtCommCount.Text = lngCount.ToString();
				txtCommAssess.Text = Strings.Format(lngTotal, "###,###,###,##0");
				// Call clsTemp.OpenRecordset("select count(*) as thecount, sum(rlexemption) as thetotal from master where not rsdeleted = 1 and ((riexemptcd1 > 0) or (riexemptcd2 > 0) or (riexemptcd3 > 0))", strREDatabase)
				if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
				{
					strSQL = "select count(*) as thecount, sum(rlexemption) as thetotal from master where not rsdeleted = 1 ";
				}
				else
				{
					strSQL = "select count(*) as thecount, sum(rlexemption) as thetotal from master where not rsdeleted = 1 and isnull(ritrancode,0) = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thecount"))));
					// TODO Get_Fields: Field [thetotal] not found!! (maybe it is an alias?)
					lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thetotal"))));
					if (lngCount == 0)
					{
						txtEAAve.Text = "0";
					}
					else
					{
						txtEAAve.Text = Strings.Format(FCConvert.ToDouble(lngTotal) / lngCount, "###,###,##0");
					}
				}
				else
				{
					lngCount = 0;
					lngTotal = 0;
					txtEAAve.Text = "0";
				}
				txtEACount.Text = lngCount.ToString();
				txtEAAssess.Text = Strings.Format(lngTotal, "###,###,###,##0");
				if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
				{
					strSQL = "Select count(*) as thecount, sum (hlvalland) as thetotal from master where hivallandcode > 2 and not rsdeleted = 1";
				}
				else
				{
					strSQL = "Select count(*) as thecount, sum (hlvalland) as thetotal from master where hivallandcode > 2 and not rsdeleted = 1 and isnull(ritrancode,0) = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thecount"))));
					// TODO Get_Fields: Field [thetotal] not found!! (maybe it is an alias?)
					lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thetotal"))));
					if (lngCount == 0)
					{
						txtLandOvAve.Text = "0";
					}
					else
					{
						txtLandOvAve.Text = Strings.Format(FCConvert.ToDouble(lngTotal) / lngCount, "###,###,##0");
					}
				}
				else
				{
					lngCount = 0;
					lngTotal = 0;
					txtLandOvAve.Text = "0";
				}
				txtLandOvCount.Text = lngCount.ToString();
				txtLandOvAssess.Text = Strings.Format(lngTotal, "###,###,###,##0");
				if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
				{
					strSQL = "Select count(*) as thecount, sum(hlvalbldg) as thetotal from master where hivalbldgcode > 2 and not rsdeleted = 1";
				}
				else
				{
					strSQL = "Select count(*) as thecount, sum(hlvalbldg) as thetotal from master where hivalbldgcode > 2 and not rsdeleted = 1 and isnull(ritrancode,0) = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thecount"))));
					// TODO Get_Fields: Field [thetotal] not found!! (maybe it is an alias?)
					lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("thetotal"))));
					if (lngCount == 0)
					{
						txtBldgOvAve.Text = "0";
					}
					else
					{
						txtBldgOvAve.Text = Strings.Format(FCConvert.ToDouble(lngTotal) / lngCount, "###,###,##0");
					}
				}
				else
				{
					lngCount = 0;
					lngTotal = 0;
					txtBldgOvAve.Text = "0";
				}
				txtBldgOvCount.Text = lngCount.ToString();
				txtBldgOvAssess.Text = Strings.Format(lngTotal, "#,###,###,###,##0");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
