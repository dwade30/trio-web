﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewValuationReport.
	/// </summary>
	public partial class rptNewValuationReport : BaseSectionReport
	{
		public static rptNewValuationReport InstancePtr
		{
			get
			{
				return (rptNewValuationReport)Sys.GetInstance(typeof(rptNewValuationReport));
			}
		}

		protected rptNewValuationReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
                modDataTypes.Statics.MR.DisposeOf();
			}
			base.Dispose(disposing);
		}

		public rptNewValuationReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "New Valuation Estimate";
		}
		// nObj = 1
		//   0	rptNewValuationReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngYearToUse;
		int lngCount;
		int lngLastAccount;
		int lngAcctCount;
		// Dim dbTemp As DAO.Database
		int NumRecords;
		
		int lngTotAddedLand;
		int lngTotAddedBldg;
		bool boolDifferencesFound;

		public void Init()
		{
			object lngYeartouse = lngYearToUse;
			if (!frmInput.InstancePtr.Init(ref lngYeartouse, "Choose year of commitment", "Commitment Year", 2440, false, modGlobalConstants.InputDTypes.idtWholeNumber, FCConvert.ToString(DateTime.Today.Year), true))
			{
				this.Close();
				return;
			}
			lngYearToUse = FCConvert.ToInt32(lngYeartouse);
			if (lngYearToUse < 100)
			{
				lngYearToUse = 2000 + lngYearToUse;
			}
			frmReportViewer.InstancePtr.Init(this);
			frmReportViewer.InstancePtr.Enabled = false;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int intTemp;
			string strTemp = "";
			//eArgs.EOF = modDataTypes.Statics.MR.EndOfFile() || !(Frmassessmentprogress.InstancePtr.stillcalc);
			//nextaccount:
			//;
			eArgs.EOF = modDataTypes.Statics.MR.EndOfFile() || !(Frmassessmentprogress.InstancePtr?.stillcalc??false);
			if (eArgs.EOF)
            {
				modDataTypes.Statics.MR.MoveLast();
				Frmassessmentprogress.InstancePtr.Unload();
				return;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			boolDifferencesFound = false;
			modGlobalVariables.Statics.LandTypes.Init();
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtmuniname.Text = modGlobalConstants.Statics.MuniName;
			txtdate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtpage.Text = "Page 1";
			Frmassessmentprogress.InstancePtr.Initialize("Only some records were calculated." + "\r\n" + "Cancel anyway?");
			Frmassessmentprogress.InstancePtr.Label2.Text = "Formatting Summary Pages. Please Wait.";
			Frmassessmentprogress.InstancePtr.Text = "Summary";
			Frmassessmentprogress.InstancePtr.cmdcancexempt.Visible = true;
            Frmassessmentprogress.InstancePtr.Show(FCForm.FormShowEnum.Modeless);
			modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where not rsdeleted = 1 order by rsaccount,RSCard", modGlobalVariables.strREDatabase);
			// Call MR.InsertName("OwnerPartyID", "Owner", False, True, False, "RE", False, "", True)
			modDataTypes.Statics.MR.MoveLast();
			modDataTypes.Statics.MR.MoveFirst();
			modDataTypes.Statics.MR.Edit();
			NumRecords = modDataTypes.Statics.MR.RecordCount();
			Frmassessmentprogress.InstancePtr.ProgressBar1.Maximum = NumRecords + 1;
			modGlobalVariables.Statics.boolfromcalcandassessment = true;
			modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
			modGlobalVariables.Statics.gintMaxAccounts = FCConvert.ToInt32(modGlobalRoutines.GetMaxAccounts());
			modGNBas.Statics.gintMaxCards = FCConvert.ToInt32(modREMain.GetRecordCount("Master", "RSAccount"));
			modGlobalVariables.Statics.boolfromcalcandassessment = false;
			modDataTypes.Statics.OUT.OpenRecordset("select * from outbuilding order by rsaccount,rscard", modGlobalVariables.strREDatabase);
			modDataTypes.Statics.CMR.OpenRecordset("select * from commercial order by rsaccount,rscard", modGlobalVariables.strREDatabase);
			modDataTypes.Statics.DWL.OpenRecordset("select * from dwelling order by rsaccount,rscard", modGlobalVariables.strREDatabase);
			modSpeedCalc.REAS03_1075_GET_PARAM_RECORD();
			Array.Resize(ref modSpeedCalc.Statics.SummaryList, NumRecords + 1 + 1);
			// change the depreciation years to be a year old
			if (modGlobalVariables.Statics.DepreciationYear == 0)
			{
				modGlobalVariables.Statics.DepreciationYear = lngYearToUse - 1;
			}
			if (modGlobalVariables.Statics.DepreciationYearMoHo == 0)
			{
				modGlobalVariables.Statics.DepreciationYearMoHo = lngYearToUse - 1;
			}
			if (modGlobalVariables.Statics.CustomizedInfo.DepreciationYear == 0)
			{
				// CustomizedInfo.DepreciationYear = CustomizedInfo.DepreciationYear - 1
				modGlobalVariables.Statics.CustomizedInfo.DepreciationYear = FCConvert.ToInt16(lngYearToUse - 1);
			}
			if (modGlobalVariables.Statics.CustomizedInfo.MHDepreciationYear == 0)
			{
				// CustomizedInfo.MHDepreciationYear = CustomizedInfo.MHDepreciationYear - 1
				modGlobalVariables.Statics.CustomizedInfo.MHDepreciationYear = FCConvert.ToInt16(lngYearToUse - 1);
			}
			if (modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear == 0)
			{
				// CustomizedInfo.CommDepreciationYear = CustomizedInfo.CommDepreciationYear - 1
				modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear = FCConvert.ToInt16(lngYearToUse - 1);
			}
			modSpeedCalc.Statics.SummaryListIndex = 0;
			modProperty.Statics.RUNTYPE = "P";
			modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
			modGlobalVariables.Statics.boolUpdateWhenCalculate = false;
			lngLastAccount = 0;
			lngAcctCount = 0;
			lngTotAddedBldg = 0;
			lngTotAddedLand = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngLand = 0;
			int lngbldg = 0;
			int lngTotal;
			int lngPrevLand = 0;
			int lngPrevBldg = 0;
			int lngAddedLand = 0;
			int lngAddedBldg = 0;
			int intcnum = 0;
			cPartyController tPartyCon = new cPartyController();
			CalcAgain:
			;
			if (!modDataTypes.Statics.MR.EndOfFile() && (Frmassessmentprogress.InstancePtr?.stillcalc ?? false))
			{
				modDataTypes.Statics.MR.Edit();
				// If MR.Fields("rscard") > 1 Then GoTo SkipCalc
				if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
				{
					modREASValuations.Statics.BBPhysicalPercent = 0;
					modREASValuations.Statics.HoldDwellingEconomic = 0;
				}
				if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount")) == 15)
				{
					lngLand = lngLand;
				}
				modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
				modGlobalVariables.Statics.boolfromcalcandassessment = true;
				modSpeedCalc.CalcCard();
				modGlobalVariables.Statics.boolfromcalcandassessment = false;
				SkipCalc:
				;
				intcnum = modGlobalVariables.Statics.intCurrentCard;
				lngLand = modProperty.Statics.lngCorrelatedLand[intcnum];
				lngbldg = modProperty.Statics.lngCorrelatedBldg[intcnum];
				lngPrevLand = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + "")));
				lngPrevBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval") + "")));
				lngAddedLand = lngLand - lngPrevLand;
				if (lngAddedLand < 0)
					lngAddedLand = 0;
				lngAddedBldg = lngbldg - lngPrevBldg;
				if (lngAddedBldg < 0)
					lngAddedBldg = 0;
				lngTotAddedBldg += lngAddedBldg;
				lngTotAddedLand += lngAddedLand;
				Frmassessmentprogress.InstancePtr.BringToFront();
				lngTotal = lngLand + lngbldg;
				if ((lngAddedLand == 0) && (lngAddedBldg == 0))
				{
					modDataTypes.Statics.MR.MoveNext();
					if (!modDataTypes.Statics.MR.EndOfFile())
					{
						Frmassessmentprogress.InstancePtr.ProgressBar1.Value = modDataTypes.Statics.MR.AbsolutePosition();
                        //Application.DoEvents();
						Frmassessmentprogress.InstancePtr.lblpercentdone.Text = Strings.Format(((FCConvert.ToDouble(modDataTypes.Statics.MR.AbsolutePosition()) / NumRecords) * 100), "0.00") + "%";
                        FCUtils.ApplicationUpdate(Frmassessmentprogress.InstancePtr);
                        if (Frmassessmentprogress.InstancePtr.stillcalc)
							goto CalcAgain;
					}
					else
					{
						this.Detail.Visible = false;
						return;
					}
				}
				lngCount += 1;
				if (lngLastAccount != modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"))
				{
					lngLastAccount = modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount");
					lngAcctCount += 1;
				}
				boolDifferencesFound = true;
				txtAddedLand.Text = Strings.Format(lngAddedLand, "#,###,##0");
				txtAddedBuilding.Text = Strings.Format(lngAddedBldg, "#,###,##0");
				txtNewValuation.Text = Strings.Format(lngAddedLand + lngAddedBldg, "#,###,###,##0");
				txtaccount.Text = modGlobalVariables.Statics.gintLastAccountNumber.ToString();
				txtname.Text = "";
				// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
				cParty tParty = new cParty();
				tParty = tPartyCon.GetParty(modDataTypes.Statics.MR.Get_Fields_Int32("OwnerPartyID"), true);
				if (!(tParty == null))
				{
					txtname.Text = tParty.FullNameLastFirst;
				}
				else
				{
					txtname.Text = "";
				}
				// txtName = Trim(MR.Fields("RSNAME") & "")
				if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + "").Length < 6)
				{
					txtLocation.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("RSLOCNUMALPH") + " " + Strings.StrDup(5 - Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("RSLOCNUMALPH") + " ").Length, " ") + modDataTypes.Statics.MR.Get_Fields_String("RSLOCStreet") + " ");
				}
				else
				{
					txtLocation.Text = Strings.Trim(FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph"))) + " " + modDataTypes.Statics.MR.Get_Fields_String("rslocstreet");
				}
				txtmaplot.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + "");
				txtcard.Text = Strings.Format(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + ""), " 0");
				// txtprevland = Format(Val(MR.Fields("lastlandval") & ""), "###,###,##0")
				// txtnewland = Format(lngLand, "###,###,##0")
				// txtprevbldg = Format(Val(MR.Fields("lastbldgval") & ""), "##,###,##0")
				// txtnewbldg = Format(lngBldg, "##,###,##0")
				// varAccount = Val(MR.Fields("rsaccount") & "")
				// varcard = Val(MR.Fields("rscard") & "")
				//Application.DoEvents();
				if (modDataTypes.Statics.MR.EndOfFile() == false)
				{
					Frmassessmentprogress.InstancePtr.ProgressBar1.Value = modDataTypes.Statics.MR.AbsolutePosition();
					Frmassessmentprogress.InstancePtr.lblpercentdone.Text = Strings.Format(((FCConvert.ToDouble(modDataTypes.Statics.MR.AbsolutePosition()) / NumRecords) * 100), "##0.00") + "%";
				}
				else
				{
					Frmassessmentprogress.InstancePtr.ProgressBar1.Value = NumRecords;
					Frmassessmentprogress.InstancePtr.lblpercentdone.Text = "100%";
				}
			}
			modDataTypes.Statics.MR.MoveNext();
			txtCount.Text = Strings.Format(lngAcctCount, "#,###,##0");
			txtNLTotal.Text = Strings.Format(lngTotAddedLand, "#,###,##0");
			txtnbtotal.Text = Strings.Format(lngTotAddedBldg, "#,###,##0");
			txttotaltotal.Text = Strings.Format(lngTotAddedLand + lngTotAddedBldg, "#,###,###,##0");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtpage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// reset the depreciationyears
			frmReportViewer.InstancePtr.Enabled = true;
			modGlobalRoutines.LoadCustomizedInfo();
			modGlobalVariables.Statics.boolUseArrays = false;
			modGlobalVariables.Statics.LandTypes.UnloadLandTypes();
			if (modSpeedCalc.Statics.boolCalcErrors)
			{
				MessageBox.Show("There were errors calculating.  Following is a list.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				//! Load frmShowResults;
				frmShowResults.InstancePtr.Init(ref modSpeedCalc.Statics.CalcLog);
				frmShowResults.InstancePtr.lblScreenTitle.Text = "Calculation Errors Log";
				frmShowResults.InstancePtr.Show();
			}
			modSpeedCalc.Statics.boolCalcErrors = false;
			modSpeedCalc.Statics.CalcLog = "";
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCount.Text = Strings.Format(lngAcctCount, "#,###,##0");
			txtNLTotal.Text = Strings.Format(lngTotAddedLand, "#,###,##0");
			txtnbtotal.Text = Strings.Format(lngTotAddedBldg, "#,###,##0");
			txttotaltotal.Text = Strings.Format(lngTotAddedLand + lngTotAddedBldg, "#,###,###,##0");
		}

		
	}
}
