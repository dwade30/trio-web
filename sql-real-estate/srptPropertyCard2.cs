﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralDocuments.Commands;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptPropertyCard2.
	/// </summary>
	public partial class srptPropertyCard2 : FCSectionReport
	{
		public static srptPropertyCard2 InstancePtr
		{
			get
			{
				return (srptPropertyCard2)Sys.GetInstance(typeof(srptPropertyCard2));
			}
		}

		protected srptPropertyCard2 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsProp_AutoInitialized?.Dispose();
				clsTemp?.Dispose();
				clsOut?.Dispose();
                clsProp_AutoInitialized = null;
                clsTemp = null;
                clsOut = null;
            }
			base.Dispose(disposing);
		}

		public srptPropertyCard2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptPropertyCard2	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsTemp = new clsDRWrapper();
		int lngAcct;
		int intCard;
		clsDRWrapper clsOut = new clsDRWrapper();
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper clsProp = new clsDRWrapper();
			public clsDRWrapper clsProp_AutoInitialized = null;
			public clsDRWrapper clsProp
			{
				get
				{
					if ( clsProp_AutoInitialized == null)
					{
						 clsProp_AutoInitialized = new clsDRWrapper();
					}
					return clsProp_AutoInitialized;
				}
				set
				{
					 clsProp_AutoInitialized = value;
				}
			}
		bool boolShowGridNotSketch;
		bool boolHidePics;

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
		}

		public bool ShowPics
		{
			get
			{
				bool ShowPics = false;
				ShowPics = !boolHidePics;
				return ShowPics;
			}
			set
			{
				boolHidePics = !value;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTemp;
			string[] strAry = null;
			modGlobalRoutines.LoadCustomizedInfo();
			strAry = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
			intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
			if (modGlobalVariables.Statics.CustomizedInfo.ShowSketchGridLines)
			{
                //Frame1.Visible = false;
                ShowSketchGridLines(true);
				imgSketch.Visible = false;
				Line14.Visible = false;
			}
			else
			{
				Line14.Visible = true;
				imgSketch.Visible = true;
                //Frame1.Visible = false;
                ShowSketchGridLines(false);
			}
			clsProp.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
			if (Strings.UCase(FCConvert.ToString(clsProp.Get_Fields_String("rsdwellingcode"))) == "C")
			{
				SubReport1.Report = new srptCommercialCard();
			}
			else
			{
				SubReport1.Report = new srptDwellingCard();
			}
			txtMuni.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				if (!clsProp.EndOfFile())
				{
					txtMuni.Text = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt32(Conversion.Val(clsProp.Get_Fields_Int32("ritrancode"))));
				}
			}
			txtDate.Text = Strings.Format(DateTime.Today, "M/dd/yyyy");
			txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsProp.Get_Fields_String("rsmaplot")));
			txtAccount.Text = lngAcct.ToString();
			txtCard.Text = intCard.ToString();
			strTemp = "";
			if (Conversion.Val(clsProp.Get_Fields_String("rslocnumalph")) > 0)
			{
				strTemp = FCConvert.ToString(Conversion.Val(clsProp.Get_Fields_String("rslocnumalph"))) + Strings.Trim(FCConvert.ToString(clsProp.Get_Fields_String("rslocapt"))) + " ";
			}
			strTemp += Strings.Trim(FCConvert.ToString(clsProp.Get_Fields_String("rslocstreet")));
			strTemp = Strings.Trim(strTemp);
			txtLocation.Text = strTemp;
			txtCard.Text = FCConvert.ToString(clsProp.Get_Fields_Int32("rscard"));
			clsTemp.OpenRecordset("select rsaccount from master where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
			txtCards.Text = clsTemp.RecordCount().ToString();
			FillFromCostFiles();
		}

        //FC:FINAL:AM:#3461 - frames are not supported
        private void ShowSketchGridLines(bool show)
        {
            for(int i = 25; i <= 108; i++)
            {
                this.Detail.Controls["Line" + i].Visible = show;
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			string strTemp = "";
			//FC:FINAL:RPU:#i1029 - make the dynamic field parentReport because the type of parent is unknown untill runtime
			dynamic parentReport = this.ParentReport;
			FillPicAndSketch();
			SubReport1.Report.UserData = FCConvert.ToString(lngAcct) + "," + FCConvert.ToString(intCard);
			clsOut.OpenRecordset("select * from outbuilding where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
			if (!clsOut.EndOfFile())
			{
				for (x = 1; x <= 10; x++)
				{
					// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
					if (Conversion.Val(clsOut.Get_Fields("oitype" + FCConvert.ToString(x))) > 0)
					{
						// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
						strTemp = FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("oitype" + FCConvert.ToString(x)))) + " ";
						// If Me.ParentReport.clsCost.FindFirstRecord("crecordnumber", Val(strTemp)) Then
						if (parentReport.clsCost.FindFirst("crecordnumber = " + FCConvert.ToString(Conversion.Val(strTemp))))
						{
							strTemp += Strings.Trim(parentReport.clsCost.Get_Fields_String("cldesc"));
						}
						(Detail.Controls["txtType" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
						// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
						(Detail.Controls["txtYear" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("oiyear" + FCConvert.ToString(x))));
						// TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
						if (!FCConvert.ToBoolean(clsOut.Get_Fields("oiusesound" + FCConvert.ToString(x))))
						{
							(Detail.Controls["txtSound" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
							// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
							if (Conversion.Val(clsOut.Get_Fields("oitype" + FCConvert.ToString(x))) < 700)
							{
								// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
								(Detail.Controls["txtUnits" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("oiunits" + FCConvert.ToString(x))));
							}
							else
							{
								// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
								strTemp = FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("oiunits" + FCConvert.ToString(x))));
								if (strTemp.Length > 2)
								{
									strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 2) + "x" + Strings.Right(strTemp, 2);
								}
								(Detail.Controls["txtUnits" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
							}
							// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
							(Detail.Controls["txtGrade" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("oigradecd" + FCConvert.ToString(x)))) + " " + FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("oigradepct" + FCConvert.ToString(x))));
							// TODO Get_Fields: Field [Oicond] not found!! (maybe it is an alias?)
							(Detail.Controls["txtCond" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("Oicond" + FCConvert.ToString(x))));
							// TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
							(Detail.Controls["txtPhys" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("oipctphys" + FCConvert.ToString(x))));
							// TODO Get_Fields: Field [oipctfunct] not found!! (maybe it is an alias?)
							(Detail.Controls["txtFunc" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val(clsOut.Get_Fields("oipctfunct" + FCConvert.ToString(x))));
						}
						else
						{
							(Detail.Controls["txtUnits" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
							(Detail.Controls["txtGrade" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
							(Detail.Controls["txtCond" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
							(Detail.Controls["txtPhys" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
							(Detail.Controls["txtFunc" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
							// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
							(Detail.Controls["txtSound" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(Conversion.Val(clsOut.Get_Fields("oisoundvalue" + FCConvert.ToString(x))), "#,###,###,##0");
						}
					}
					else
					{
						(Detail.Controls["txtType" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(Detail.Controls["txtYear" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(Detail.Controls["txtUnits" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(Detail.Controls["txtGrade" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(Detail.Controls["txtCond" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(Detail.Controls["txtPhys" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(Detail.Controls["txtFunc" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						(Detail.Controls["txtSound" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					}
				}
				// x
			}
			else
			{
			}
		}

		private void FillFromCostFiles()
		{
			string strTemp = "";
			int x;
			int intTemp;
			//FC:FINAL:RPU:#i1029 - make the dynamic field parentReport because the type of parent is unknown untill runtime
			dynamic parentReport = this.ParentReport;
			parentReport.clsCost.MoveFirst();
			for (x = 1; x <= 6; x++)
			{
				strTemp = parentReport.clsCost.Get_Fields_String("cldesc");
				strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Trim(strTemp);
				if (strTemp.Length > 14)
					strTemp = Strings.Mid(strTemp, 1, 14);
				(this.Detail.Controls["txtCost" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(x) + "." + strTemp;
				parentReport.clsCost.MoveNext();
			}
			// x
			for (x = 21; x <= 29; x++)
			{
				if (parentReport.clsCost.FindNextRecord("crecordnumber", x))
				{
					strTemp = parentReport.clsCost.Get_Fields_String("cldesc");
					strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Trim(strTemp);
					if (strTemp.Length > 14)
						strTemp = Strings.Mid(strTemp, 1, 14);
					(this.Detail.Controls["txtCost" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(x) + "." + strTemp;
				}
			}
			// x
		}

		private void FillPicAndSketch()
		{
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                string[] strAry = null;
                Guid docGuid;
                Guid doc2Guid = Guid.Empty;
                imgPicture.Image = null;
                if (!boolHidePics)
                {
                    clsTemp.OpenRecordset(
                        "select  * from picturerecord where mraccountnumber = " + FCConvert.ToString(lngAcct) +
                        " and rscard = " + FCConvert.ToString(intCard) + " order by picnum",
                        modGlobalVariables.strREDatabase);
                    while (!clsTemp.EndOfFile() && imgPicture.Image == null)
                    {
                        docGuid = clsTemp.Get_Fields_Guid("DocumentIdentifier");
                        doc2Guid = Guid.Empty;
                        var centralDocument = StaticSettings.GlobalCommandDispatcher
                            .Send(new GetCentralDocumentByIdentifier(docGuid))
                            .Result;
                        if (centralDocument != null && centralDocument.ID > 0)
                        {
                            imgPicture.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
                            imgPicture.Image = FCUtils.PictureFromBytes(centralDocument.ItemData, imgPicture.Width,
                                imgPicture.Height);
                        }

                        clsTemp.MoveNext();
                        if (!clsTemp.EndOfFile() && centralDocument != null && centralDocument.ID > 0)
                        {
                            doc2Guid = clsTemp.Get_Fields_Guid("DocumentIdentifier");
                        }
                    }
                }

                // search for sketches
                imgSketch.ResetImage();
                if (!boolHidePics)
                {
                    var sketchInfos = StaticSettings.GlobalCommandDispatcher
                        .Send(new GetSketchInfos(Guid.Parse(clsProp.Get_Fields_String("CardID")))).Result;
                    var sketches = sketchInfos.ToPropertySketches();
                    sketches.SetToFirstSketch();
                    if (sketches.HasCurrentSketch())
                    {
                        var sketchInfo = sketches.CurrentSketch();
                        var sketch = StaticSettings.GlobalCommandDispatcher.Send((new GetSketchImage(sketchInfo.Id)))
                            .Result;
                        imgSketch.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
                        imgSketch.Image = FCUtils.PictureFromBytes(sketch, imgPicture.Width, imgPicture.Height);
                    }
                }

                if (doc2Guid != Guid.Empty &&
                    modGlobalVariables.Statics.CustomizedInfo.boolPrintSecondaryPicOnPropertyCard &&
                    imgSketch.Image == null)
                {
                    var centralDocument = StaticSettings.GlobalCommandDispatcher
                        .Send(new GetCentralDocumentByIdentifier(doc2Guid))
                        .Result;
                    if (centralDocument != null)
                    {
                        imgSketch.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
                        imgSketch.Image = FCUtils.PictureFromBytes(centralDocument.ItemData, imgPicture.Width,
                            imgPicture.Height);
                    }
                }
                else
                {
                    //imgSketch.Image = null;
                }
            }
        }

		
	}
}
