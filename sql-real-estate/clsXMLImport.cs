//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Xml;

namespace TWRE0000
{
	public class clsXMLImport
	{

		//=========================================================


		// implement the content handler and error handler interfaces here
		Implements MSXML2.IVBSAXContentHandler;
		Implements MSXML2.IVBSAXErrorHandler;

		MSXML2.SAXXMLReader60 SaxObj = new MSXML2.SAXXMLReader60();
		XmlDocument docObj = new XmlDocument();
		MSXML2.IXMLDOMElement curElement = new MSXML2.IXMLDOMElement();
		MSXML2.IXMLDOMElement curAccount = new MSXML2.IXMLDOMElement();

		private string strXMLDoc;

		string strCurrentElement; // name of current element

		string strCurrentType = "";
		int lngCurrentAccount;
		int intCurrentCard;
		string strCurrentElementChars;


		public bool LoadRecords(ref string strFile)
		{
			bool LoadRecords = false;
			try
			{	// On Error GoTo ErrorHandler
				LoadRecords = false;
				strXMLDoc = "";
				// SaxObj.parseURL (strFile)
				docObj.Load(strFile);

				curElement = docObj.SelectSingleNode("//REExport");
				if (MessageBox.Show("This export was made by "+curElement.Attributes(0).Text+" on "+Strings.Format(curElement.Attributes(1).Text, "MM/dd/yyyy")+"\n"+"Do you want to continue?", "Load Data?", MessageBoxButtons.YesNo, MessageBoxIcon.Question)==DialogResult.No) {
					docObj = null;
					return LoadRecords;
				}

				LoadRecords = true;
				return LoadRecords;
			}
            catch (Exception ex)
            {	// ErrorHandler:
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err(ex).Number)+" "+Information.Err(ex).Description+"\n"+"In LoadRecords", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadRecords;
		}

		public clsXMLImport() : base()
		{
			// Set SaxObj = New MSXML2.SAXXMLReader
			// Set SaxObj.contentHandler = Me
			// Set SaxObj.ErrorHandler = Me

			docObj = new XmlDocument();
			//docObj.async = false;

		}

		~clsXMLImport()
		{
			SaxObj = null;
		}

		private void IVBSAXContentHandler_characters(ref string strChars)
		{
			strCurrentElementChars = strChars;
		}

		private void IVBSAXContentHandler_endDocument()
		{

		}

		private void IVBSAXContentHandler_endElement(ref string strNamespaceURI, ref string strLocalName, ref string strQName)
		{
			strCurrentElement = "";
		}

		private void IVBSAXContentHandler_endPrefixMapping(ref string strPrefix)
		{

		}

		private void IVBSAXContentHandler_ignorableWhitespace(ref string strChars)
		{

		}

		private void IVBSAXContentHandler_processingInstruction(ref string strTarget, ref string strData)
		{

		}

		private void IVBSAXContentHandler_skippedEntity(ref string strName)
		{

		}

		private void IVBSAXContentHandler_startElement(ref string strNamespaceURI, ref string strLocalName, ref string strQName, MSXML2.IVBSAXAttributes oAttributes)
		{
			strCurrentElement = strLocalName;
			strCurrentElementChars = "";
			curElement = docObj.CreateNode(NODE_ELEMENT, strLocalName, "");

			int x;
			MSXML2.IXMLDOMNode y = new MSXML2.IXMLDOMNode();


			for(x=0; x<=oAttributes.Length-1; x++) {
				y = docObj.CreateNode(NODE_ATTRIBUTE, oAttributes.getLocalName(x), "");
				y.Text = oAttributes.GetValue(x);
				curElement.appendChild(y);
			} // x

			y.Text = "bb";
			
			if (strLocalName=="REExport")
			{
				strCurrentType = "REExport";
			}
			else if (strLocalName=="ExportType")
			{
				strCurrentType = "ExportType";
			}
			else if (strLocalName=="Account")
			{
				strCurrentType = "Account";
				lngCurrentAccount = (int)Math.Round(Conversion.Val(curElement.Attributes(0).Text));
				intCurrentCard = (int)Math.Round(Conversion.Val(curElement.Attributes(1).Text));
			}
			else if (strLocalName=="Dwelling")
			{
				strCurrentType = "Dwelling";
			}
			else if (strLocalName=="Commercial")
			{
				strCurrentType = "Commercial";
			}
			else if (strLocalName=="Outbuilding")
			{
				strCurrentType = "Outbuilding";
			}
		}

		private void IVBSAXContentHandler_startPrefixMapping(ref string strPrefix, ref string strURI)
		{

		}

		private void IVBSAXErrorHandler_error(MSXML2.IVBSAXLocator oLocatorref, string strErrorMessage, int nErrorCode)
		{
			MessageBox.Show("Error Number "+Convert.ToString(nErrorCode)+" "+strErrorMessage, "Error Reading Document", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}



		private void IVBSAXErrorHandler_fatalError(MSXML2.IVBSAXLocator oLocator, ref string strErrorMessage, int nErrorCode)
		{
			MessageBox.Show("Error Number "+Convert.ToString(nErrorCode)+" "+strErrorMessage+"\n"+"Line: "+oLocator.lineNumber+" Column: "+oLocator.columnNumber, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}




		private MSXML2.IVBSAXLocator IVBSAXContentHandler_documentLocator
		{
			set
			{

			}
		}

		private void IVBSAXContentHandler_startDocument()
		{

		}

		private void IVBSAXErrorHandler_ignorableWarning(MSXML2.IVBSAXLocator oLocatorref, string strErrorMessage, int nErrorCode)
		{

		}

	}
}