﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace TWRE0000
{
	public class cCommercialConstructionClass
	{
		//=========================================================
		private int lngRecordID;
		private int intClassCategory;
		private string strName = string.Empty;
		private int lngCodeNumber;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Name
		{
			set
			{
				strName = value;
			}
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
		}

		public int CodeNumber
		{
			set
			{
				lngCodeNumber = value;
				IsUpdated = true;
			}
			get
			{
				int CodeNumber = 0;
				CodeNumber = lngCodeNumber;
				return CodeNumber;
			}
		}

		public short ClassCategory
		{
			set
			{
				intClassCategory = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short ClassCategory = 0;
				ClassCategory = FCConvert.ToInt16(intClassCategory);
				return ClassCategory;
			}
		}
	}
}
