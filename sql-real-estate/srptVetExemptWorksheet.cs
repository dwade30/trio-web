﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using Microsoft.Ajax.Utilities;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptVetExemptWorksheet.
	/// </summary>
	public partial class srptVetExemptWorksheet : FCSectionReport
	{
		public srptVetExemptWorksheet()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private MunicipalValuationReturn valuationReturn;
        public srptVetExemptWorksheet(MunicipalValuationReturn valuationReturn) : this()
        {
            this.valuationReturn = valuationReturn;
        }
		private void InitializeComponentEx()
		{
		}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
                TXTMuniname.Text = valuationReturn.Municipality;
                txtCYear.Text = valuationReturn.ReportYear.ToString();
				txtCYear2.Text = txtCYear.Text;
				lblTitle.Text = "MAINE REVENUE SERVICES - " + valuationReturn.ReportYear + " MUNICIPAL VALUATION RETURN";

                txtNumExempts1.Text = valuationReturn.VeteranExemptions.SurvivingMale.Count.FormatAsNumber();
                txtValue1.Text = valuationReturn.VeteranExemptions.SurvivingMale.ExemptValue.ToInteger()
                    .FormatAsNumber();

                txtNumExempts2.Text =
                    valuationReturn.VeteranExemptions.ParaplegicRevocableTrusts.Count.FormatAsNumber();
                txtValue2.Text = valuationReturn.VeteranExemptions.ParaplegicRevocableTrusts.ExemptValue.ToInteger()
                    .FormatAsNumber();

                txtNumExempts3.Text = valuationReturn.VeteranExemptions.MiscRevocableTrust.Count.FormatAsNumber();
                txtValue3.Text = valuationReturn.VeteranExemptions.MiscRevocableTrust.ExemptValue.ToInteger()
                    .FormatAsNumber();

                // WWI vets enlisted as a maine resident
                txtValue4.Text = valuationReturn.VeteranExemptions.MaineEnlistedWWIVeteran.ExemptValue.ToInteger().FormatAsNumber();
                txtNumExempts4.Text = valuationReturn.VeteranExemptions.MaineEnlistedWWIVeteran.Count.FormatAsNumber();
				// WWI vets not enlisted as a maine resident
                txtValue5.Text = valuationReturn.VeteranExemptions.NonResidentWWIVeteran.ExemptValue.ToInteger().FormatAsNumber();
                txtNumExempts5.Text = valuationReturn.VeteranExemptions.NonResidentWWIVeteran.Count.FormatAsNumber();

				// Viet Nam
                txtValue12.Text = valuationReturn.VeteranExemptions.EarlyVietnamConflict.ExemptValue.ToInteger()
                    .FormatAsNumber();
                txtNumExempts12.Text = valuationReturn.VeteranExemptions.EarlyVietnamConflict.Count.FormatAsNumber();
				// Paraplegic
                txtValue6.Text = valuationReturn.VeteranExemptions.ParaplegicVeteran.ExemptValue.ToInteger()
                    .FormatAsNumber();
                txtNumExempts6.Text = valuationReturn.VeteranExemptions.ParaplegicVeteran.Count.FormatAsNumber();
                // Coop
                txtValue7.Text = valuationReturn.VeteranExemptions.COOPHousing.ExemptValue.ToInteger().FormatAsNumber();
                txtNumExempts7.Text = valuationReturn.VeteranExemptions.COOPHousing.Count.FormatAsNumber();
				
				// disabled in line of duty
                txtValue10.Text = valuationReturn.VeteranExemptions.DisabledInLineOfDuty.ExemptValue.ToInteger()
                    .FormatAsNumber();
                txtNumExempts10.Text = valuationReturn.VeteranExemptions.DisabledInLineOfDuty.Count.FormatAsNumber();
				
				// all other vets who are maine residents
                txtValue8.Text = valuationReturn.VeteranExemptions.MiscMaineEnlisted.ExemptValue.ToInteger()
                    .FormatAsNumber();
                txtNumExempts8.Text = valuationReturn.VeteranExemptions.MiscMaineEnlisted.Count.FormatAsNumber();				
				// all other vets who are not enlisted as maine residents
                txtValue9.Text = valuationReturn.VeteranExemptions.MiscNonResident.ExemptValue.ToInteger()
                    .FormatAsNumber();
                txtNumExempts9.Text = valuationReturn.VeteranExemptions.MiscNonResident.Count.FormatAsNumber();
                txtValue11.Text = valuationReturn.VeteranExemptions.LebanonPanama.ExemptValue.ToInteger()
                    .FormatAsNumber();
                txtNumExempts11.Text = valuationReturn.VeteranExemptions.LebanonPanama.Count.FormatAsNumber();
				// totals
                txtNumExemptsTotal.Text = valuationReturn.VeteranExemptions.Totals.Count.FormatAsNumber();
                txtValueTotal.Text = valuationReturn.VeteranExemptions.Totals.ExemptValue.ToInteger().FormatAsNumber();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
