﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptReportCodes.
	/// </summary>
	public partial class rptReportCodes : BaseSectionReport
	{
		public static rptReportCodes InstancePtr
		{
			get
			{
				return (rptReportCodes)Sys.GetInstance(typeof(rptReportCodes));
			}
		}

		protected rptReportCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                clsCodeList = null;
            }
			base.Dispose(disposing);
		}

		public rptReportCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Report Codes";
		}
		// nObj = 1
		//   0	rptReportCodes	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		clsReportCodes clsCodeList = new clsReportCodes();

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsCodeList.GetCurrentIndex() < 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			clsCodeList.LoadCodes();
			clsCodeList.MoveFirst();
			intPage = 1;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsReportParameter tCode;
			tCode = clsCodeList.GetCurrentCode();
			if (!(tCode == null))
			{
				txtCode.Text = tCode.Code.ToString();
				txtDescription.Text = tCode.Description;
				txtCategory.Text = tCode.CategoryName;
			}
			clsCodeList.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page  " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		private void rptReportCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReportCodes properties;
			//rptReportCodes.Caption	= "Report Codes";
			//rptReportCodes.Icon	= "rptReportCodes.dsx":0000";
			//rptReportCodes.Left	= 0;
			//rptReportCodes.Top	= 0;
			//rptReportCodes.Width	= 15240;
			//rptReportCodes.Height	= 11115;
			//rptReportCodes.StartUpPosition	= 3;
			//rptReportCodes.SectionData	= "rptReportCodes.dsx":058A;
			//End Unmaped Properties
		}
	}
}
