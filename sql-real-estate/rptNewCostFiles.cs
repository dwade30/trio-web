﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewCostFiles.
	/// </summary>
	public partial class rptNewCostFiles : BaseSectionReport
	{
		public static rptNewCostFiles InstancePtr
		{
			get
			{
				return (rptNewCostFiles)Sys.GetInstance(typeof(rptNewCostFiles));
			}
		}

		protected rptNewCostFiles _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}

		public rptNewCostFiles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptNewCostFiles	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		string strSQL = "";
		string strUnitFormat = "";
		string strBaseFormat = "";

		public void Init(string strCategory, ref string strType)
		{
			// category isn't used yet.  When all of the cost files are done the same way, it might be used
			if (Strings.UCase(strCategory) == "DWELLING")
			{
				strUnitFormat = "#,###,###,##0.00";
				strBaseFormat = "#,###,###,##0";
				if (Strings.UCase(strType) == "STYLE")
				{
					lblUnit.Text = "Factor";
					lblBase.Visible = false;
					txtBase.Visible = false;
					lblTitle.Text = "Dwelling Style";
					// select factor as unit so detail section doesn't have to have a select statement
					strSQL = "select *,factor as unit,0 as base from dwellingstyle order by code";
				}
				else if (Strings.UCase(strType) == "HEAT")
				{
					lblUnit.Text = "Unit";
					lblBase.Visible = true;
					txtBase.Visible = true;
					lblTitle.Text = "Dwelling Heat";
					strSQL = "select * from dwellingheat order by code";
				}
				else if (Strings.UCase(strType) == "EXTERIOR")
				{
					lblUnit.Text = "Factor";
					lblBase.Visible = false;
					txtBase.Visible = false;
					lblTitle.Text = "Dwelling Exterior";
					strSQL = "select *,factor as unit,0 as base from dwellingexterior order by code";
				}
				else if (Strings.UCase(strType) == "ECONOMIC CODE")
				{
					lblUnit.Text = "Factor";
					lblBase.Visible = false;
					txtBase.Visible = false;
					lblTitle.Text = "Dwelling Economic Code";
					strSQL = "Select *,factor as unit,0 as base from economiccode order by code";
				}
			}
			else if (Strings.UCase(strCategory) == "PROPERTY")
			{
				strUnitFormat = "#,###,###,##0.00";
				strBaseFormat = "0";
				if (Strings.UCase(strType) == "LAND")
				{
					lblUnit.Text = "Amount";
					lblBase.Visible = false;
					txtBase.Visible = false;
					lblTitle.Text = "Land Codes";
					strSQL = "Select *,amount as unit,0 as base from tblLandCode order by code";
				}
				else if (Strings.UCase(strType) == "TRAN")
				{
					lblUnit.Text = "Amount";
					lblBase.Visible = false;
					txtBase.Visible = false;
					lblTitle.Text = "Tran Codes";
					strSQL = "Select *,amount as unit,0 as base from tblTranCode order by code";
				}
				else if (Strings.UCase(strType) == "BUILDING")
				{
					lblUnit.Text = "Amount";
					lblBase.Visible = false;
					txtBase.Visible = false;
					lblTitle.Text = "Building Codes";
					strSQL = "Select *,amount as unit,0 as base from tblBldgCode order by code";
				}
				else if (Strings.UCase(strType) == "NEIGHBORHOOD")
				{
					lblUnit.Text = "";
					lblUnit.Visible = false;
					lblBase.Visible = false;
					txtBase.Visible = false;
					txtUnit.Visible = false;
					lblTitle.Text = "Neighborhoods";
					strSQL = "Select *,0 as unit,0 as base from neighborhood order by code";
				}
			}
			//FC:FINAL:MSH - i.issue #1111: set report title
			this.Name = lblTitle.Text;
			//FC:FINAL:SBE - #i1653 - show report viewer as modal dialog, as in the origina lapplication
            //frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal));
            frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: true);
        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No codes found");
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				txtCode.Text = FCConvert.ToString(clsLoad.Get_Fields("code"));
				txtDescription.Text = FCConvert.ToString(clsLoad.Get_Fields_String("Description"));
				txtShortDescription.Text = FCConvert.ToString(clsLoad.Get_Fields_String("shortdescription"));
				// TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
				txtUnit.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("unit")), strUnitFormat);
				// TODO Get_Fields: Check the table for the column [Base] and replace with corresponding Get_Field method
				txtBase.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("Base")), strBaseFormat);
				clsLoad.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
