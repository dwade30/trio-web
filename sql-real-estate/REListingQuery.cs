﻿namespace TWRE0000
{
    public class REListingQuery
    {
        public int MinimumAccount { get; set; } = 0;
        public int MaximumAccount { get; set; } = 0;
        public bool FromExtract { get; set; } = false;

        public string RangeStart { get; set; } = "";
        public string RangeEnd { get; set; } = "";
        public bool ByRange { get; set; } = false;

        public REListingField FieldForOrdering { get; set; } = REListingField.Account;
    }
}