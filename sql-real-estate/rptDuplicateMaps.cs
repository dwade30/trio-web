﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptDuplicateMaps.
	/// </summary>
	public partial class rptDuplicateMaps : BaseSectionReport
	{
		public static rptDuplicateMaps InstancePtr
		{
			get
			{
				return (rptDuplicateMaps)Sys.GetInstance(typeof(rptDuplicateMaps));
			}
		}

		protected rptDuplicateMaps _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}

		public rptDuplicateMaps()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Duplicate Map / Lots";
		}
		// nObj = 1
		//   0	rptDuplicateMaps	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			string strREFullDBName;
			string strMasterJoin;
			strREFullDBName = clsLoad.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoinForJoin();
			// Call clsLoad.OpenRecordset("select rsaccount,rsmaplot,rsname from master where (not rsmaplot = '') and (not rsdeleted = 1) and rscard = 1 and rsmaplot in (select rsmaplot from master where not rsdeleted = 1 and rscard = 1 group by rsmaplot having count(rsmaplot) > 1) order by rsmaplot,rsaccount", strREDatabase)
			clsLoad.OpenRecordset("select rsaccount,rsmaplot,rsname from " + strMasterJoin + " where (not rsmaplot = '') and (not rsdeleted = 1) and rscard = 1 and rsmaplot in (select rsmaplot from master where not rsdeleted = 1 and rscard = 1 group by rsmaplot having count(rsmaplot) > 1) order by rsmaplot,rsaccount", modGlobalVariables.strREDatabase);
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No duplicates found");
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsLoad.EndOfFile())
			{
				txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("rsaccount"));
				txtMapLot.Text = FCConvert.ToString(clsLoad.Get_Fields_String("rsmaplot"));
				txtName.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("rsname")));
				clsLoad.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
