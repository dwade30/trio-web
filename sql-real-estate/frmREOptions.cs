﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using System.IO;
using Wisej.Web;

namespace TWRE0000
{
    /// <summary>
    /// Summary description for frmREOptions.
    /// </summary>
    public partial class frmREOptions : BaseForm
    {
        public frmREOptions()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            //FC:FINAL:RPU:#i1324 - Set the selected tab
            SSTab1.SelectedIndex = 0;
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmREOptions InstancePtr
        {
            get
            {
                return (frmREOptions)Sys.GetInstance(typeof(frmREOptions));
            }
        }

        protected frmREOptions _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // vbPorter upgrade warning: FormLoaded As FixedString	OnWrite(string)
        char[] FormLoaded = new char[1];
        const int CNSTMVRVALPPOTHER = 0;
        const int CNSTMVRVALPPMACHEQUIP = 1;
        const int CNSTMVRVALPPBUSEQUIP = 2;
        const int CNSTGRIDBLDGCODECOLCODE = 0;
        const int CNSTGRIDBLDGCODECOLDESC = 1;
        const int CNSTGRIDBLDGCODECOLUSE = 2;
        int lngLastTownCode;
        int lngCurrentTownNumber;
        bool boolLoading;
        // Private Sub SetupcmbRXMonth()
        // cmbRXMonth.Clear
        // cmbRXMonth.AddItem "January"
        // cmbRXMonth.AddItem "February"
        // cmbRXMonth.AddItem "March"
        // cmbRXMonth.AddItem "April"
        // cmbRXMonth.AddItem "May"
        // cmbRXMonth.AddItem "June"
        // cmbRXMonth.AddItem "July"
        // cmbRXMonth.AddItem "August"
        // cmbRXMonth.AddItem "September"
        // cmbRXMonth.AddItem "October"
        // cmbRXMonth.AddItem "November"
        // cmbRXMonth.AddItem "December"
        // End Sub
        //
        // Private Sub SetupcmbRXDay(ByVal intMonth As Integer)
        //
        // Dim x As Integer
        // Dim intDays As Integer
        // Dim intCurDay As Integer
        // intCurDay = 0
        // If cmbRXDay.ListIndex > -1 Then
        // intCurDay = cmbRXDay.ListIndex + 1
        // End If
        // cmbRXDay.Clear
        // Select Case intMonth
        // Case 1, 3, 5, 7, 8, 10, 12
        // intDays = 31
        // Case 4, 6, 9, 11
        // intDays = 30
        // Case 2
        // intDays = 28
        // Case Else
        // intDays = 0
        // End Select
        //
        // If intDays > 0 Then
        // For x = 1 To intDays
        // cmbRXDay.AddItem x
        // Next x
        // If intCurDay > cmbRXDay.ListCount Then
        // intCurDay = cmbRXDay.ListCount
        // End If
        // cmbRXDay.ListIndex = intCurDay - 1
        // End If
        //
        // End Sub
        private void SetupGridDefaultSchedule()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            string strTemp;
            rsLoad.OpenRecordset("select * from landSCHEDULE WHERe code = -3 and TOWNNUMBER  = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))) + " order by schedule", modGlobalVariables.strREDatabase);
            strTemp = "#0*1;0" + "\t" + "None|";
            while (!rsLoad.EndOfFile())
            {
                strTemp += "#" + rsLoad.Get_Fields_Int32("schedule") + ";" + rsLoad.Get_Fields_Int32("schedule") + "\t" + rsLoad.Get_Fields_String("description") + "|";
                rsLoad.MoveNext();
            }
            strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
            GridDefaultSchedule.ColComboList(0, strTemp);
        }

        private void SetupGridIndustrialBldgCodes()
        {
            gridIndustrialBldgCodes.Rows = 1;
            gridIndustrialBldgCodes.Cols = 3;
            gridIndustrialBldgCodes.ColDataType(CNSTGRIDBLDGCODECOLUSE, FCGrid.DataTypeSettings.flexDTBoolean);
            gridIndustrialBldgCodes.TextMatrix(0, CNSTGRIDBLDGCODECOLCODE, "");
            gridIndustrialBldgCodes.TextMatrix(0, CNSTGRIDBLDGCODECOLDESC, "Description");
            gridIndustrialBldgCodes.TextMatrix(0, CNSTGRIDBLDGCODECOLUSE, "");
        }

        private void ResizeGridIndustrialBldgCodes()
        {
            int GridWidth = 0;
            GridWidth = gridIndustrialBldgCodes.WidthOriginal;
            gridIndustrialBldgCodes.ColWidth(CNSTGRIDBLDGCODECOLCODE, FCConvert.ToInt32(0.1 * GridWidth));
            gridIndustrialBldgCodes.ColWidth(CNSTGRIDBLDGCODECOLDESC, FCConvert.ToInt32(0.7 * GridWidth));
        }

        private void FillGridIndustrialBldgCodes()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            int lngRow = 0;
            string[] strAry = null;
            string strTemp;
            int x;
            gridIndustrialBldgCodes.Rows = 1;
            clsLoad.OpenRecordset("select * from tblbldgcode ORDER by code", modGlobalVariables.Statics.strRECostFileDatabase);
            while (!clsLoad.EndOfFile())
            {
                gridIndustrialBldgCodes.Rows += 1;
                lngRow = gridIndustrialBldgCodes.Rows - 1;
                // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                gridIndustrialBldgCodes.TextMatrix(lngRow, CNSTGRIDBLDGCODECOLCODE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))));
                gridIndustrialBldgCodes.TextMatrix(lngRow, CNSTGRIDBLDGCODECOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                gridIndustrialBldgCodes.TextMatrix(lngRow, CNSTGRIDBLDGCODECOLUSE, FCConvert.ToString(false));
                clsLoad.MoveNext();
            }
            strTemp = Strings.Trim(modGlobalVariables.Statics.CustomizedInfo.REIndPropBldgCodes);
            if (strTemp != string.Empty && gridIndustrialBldgCodes.Rows > 1)
            {
                strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
                for (x = 0; x <= Information.UBound(strAry, 1); x++)
                {
                    lngRow = gridIndustrialBldgCodes.FindRow(FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[x]))), 1, CNSTGRIDBLDGCODECOLCODE);
                    if (lngRow > 0)
                    {
                        gridIndustrialBldgCodes.TextMatrix(lngRow, CNSTGRIDBLDGCODECOLUSE, FCConvert.ToString(true));
                    }
                }
                // x
            }
        }

        private void cboPrinter_SelectedIndexChanged(object sender, System.EventArgs e)
        {
        }


        private void chkShowPic_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkShowPic.CheckState == Wisej.Web.CheckState.Checked)
            {
                chkShowFirstPic.Visible = true;
            }
            else
            {
                chkShowFirstPic.Visible = false;
            }
        }

        private void chkToolTips_CheckedChanged(object sender, System.EventArgs e)
        {
            if (!boolLoading)
            {
                if (chkToolTips.CheckState == Wisej.Web.CheckState.Checked)
                {
                    if (MessageBox.Show("This will disable all quick help tooltips" + "\r\n" + "Are you sure you want to continue?", "Disable Quick Help?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        chkToolTips.CheckState = Wisej.Web.CheckState.Unchecked;
                    }
                }
            }
        }

        private void cmbF2LabelType_TextChanged(object sender, System.EventArgs e)
        {
        }

        private void cmbF2LabelType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
        }

        private void gridTownCode_ComboCloseUp(object sender, EventArgs e)
        {
            modGlobalVariables.Statics.CustomizedInfo.BillRate = Conversion.Val(txtBillRate.Text);
            modGlobalVariables.Statics.CustomizedInfo.BillYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBillYear.Text)));
            modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio = Conversion.Val(txtCertifiedRatio.Text) / 100;
            gridTownCode.EndEdit();
            //Application.DoEvents();
            switch (SSTab1.SelectedIndex)
            {
                case 0:
                    {
                        cboRounding.Focus();
                        break;
                    }
                case 1:
                    {
                        cboLandPricing.Focus();
                        break;
                    }
                case 2:
                    {
                        txtCity.Focus();
                        break;
                    }
            }
            //end switch
            //Application.DoEvents();
        }

        private void SetupGridTownCode()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            string strTemp = "";
            if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
            {
                strTemp = "";
                clsLoad.OpenRecordset("select * from tblTranCode where code > 0 order by code", modGlobalVariables.strREDatabase);
                while (!clsLoad.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields("code") + "|";
                    clsLoad.MoveNext();
                }
                if (strTemp != string.Empty)
                {
                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                }
                gridTownCode.ColComboList(0, strTemp);
                gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
                gridTownCode.Visible = true;
            }
            else
            {
                gridTownCode.Rows = 1;
                gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
                gridTownCode.Visible = false;
            }
        }

        private void ResizeGridTownCode()
        {
            //gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
        }

        private void cboRounding_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            // If cboRounding.listindex = 3 Then
            // round to nearest 1000
            // cmbRoundContingency.Visible = True
            // Else
            // cmbRoundContingency.Visible = False
            // End If
        }

        private void chkPropertyCard_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkPropertyCard.CheckState == Wisej.Web.CheckState.Checked)
            {
                chkPropertyCardCurrentAmounts.CheckState = Wisej.Web.CheckState.Unchecked;
            }
        }

        private void chkPropertyCardCurrentAmounts_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkPropertyCardCurrentAmounts.CheckState == Wisej.Web.CheckState.Checked)
            {
                chkPropertyCard.CheckState = Wisej.Web.CheckState.Unchecked;
            }
        }

        private void cmdBrowsePocketPC_Click(object sender, System.EventArgs e)
        {
            string strLocation;
            modGlobalVariables.Statics.strGetPath = "";
            strLocation = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
            //! Load frmGetDirectory;
            frmGetDirectory.InstancePtr.Text = "Select Pocket PC Data Directory";
            frmGetDirectory.InstancePtr.Label1.Text = "Select the Drive";
            frmGetDirectory.InstancePtr.Label2.Text = "Select the Data Directory";
            if (Strings.Trim(strLocation) != string.Empty)
            {
                frmGetDirectory.InstancePtr.Dir1.Path = strLocation;
                frmGetDirectory.InstancePtr.Drive1.Drive = Strings.Left(strLocation, 1);
            }
            //frmGetDirectory.InstancePtr.cmdCancel.Visible = true;
            frmGetDirectory.InstancePtr.image1.Visible = true;
            frmGetDirectory.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            if (modGlobalVariables.Statics.strGetPath != string.Empty)
            {
                txtPocketPCDirectory.Text = modGlobalVariables.Statics.strGetPath;
            }
        }

        private void cmdQuit_Click()
        {
            Unload();
        }

        private bool cmdSave_Click()
        {
            bool cmdSave_Click = false;
            string BillRate;
            clsDRWrapper clsSave = new clsDRWrapper();
            int x;
            string strTemp = "";
            //FileSystemObject fso = new FileSystemObject();
            int intTemp;
            cmdSave_Click = false;
            if (!modREMain.Statics.boolShortRealEstate)
            {
                GridDefaultSchedule.Col = -1;
            }
            if (chkShowGridLines.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.ShowSketchGridLines = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.ShowSketchGridLines = false;
            }
            // If boolShortRealEstate Then
            clsSave.Execute("update citystatezipdefaults set city = '" + Strings.Trim(txtCity.Text + "").Replace("'", "''") + "',state = '" + Strings.Trim(txtState.Text + "") + "',zip = '" + Strings.Trim(txtZip.Text + "") + "'", modGlobalVariables.strREDatabase);
            clsSave.OpenRecordset("select * from defaults", modGlobalVariables.strREDatabase);
            if (clsSave.EndOfFile())
            {
                clsSave.AddNew();
            }
            else
            {
                clsSave.Edit();
            }
            if (!modREMain.Statics.boolShortRealEstate)
            {
                if (Conversion.Val(txtSoftWood.Text) > 0)
                {
                    clsSave.Set_Fields("softcost", FCConvert.ToString(Conversion.Val(txtSoftWood.Text)));
                }
                if (Conversion.Val(txtMixedWood.Text) > 0)
                {
                    clsSave.Set_Fields("mixedcost", FCConvert.ToString(Conversion.Val(txtMixedWood.Text)));
                }
                if (Conversion.Val(txtHardWood.Text) > 0)
                {
                    clsSave.Set_Fields("hardcost", FCConvert.ToString(Conversion.Val(txtHardWood.Text)));
                }
            }
            else
            {
                clsSave.Set_Fields("softcost", FCConvert.ToString(Conversion.Val(txtSoftWood.Text)));
                clsSave.Set_Fields("mixedcost", FCConvert.ToString(Conversion.Val(txtMixedWood.Text)));
                clsSave.Set_Fields("hardcost", FCConvert.ToString(Conversion.Val(txtHardWood.Text)));
            }
            clsSave.Update();
            // Else
            if (!modREMain.Statics.boolShortRealEstate)
            {
                if (Conversion.Val(txtSoftWood.Text) > 0)
                {
                    // Call clsSave.Execute("update landschedule set amount = " & Val(txtSoftwood.Text) & " where code = " & CustomizedInfo.Softwood, strREDatabase)
                    // Call clsSave.Execute("update (select * from landschedule inner join landtype on (landschedule.code = landtype.code) where category = " & CNSTLANDCATSOFTWOOD & " or category = " & CNSTLANDCATSOFTWOODFARM & ")  set amount = " & Val(txtSoftwood.Text), strREDatabase)
                    clsSave.Execute("update landschedule set amount = " + FCConvert.ToString(Conversion.Val(txtSoftWood.Text)) + " where code in (select code from landtype where category = " + FCConvert.ToString(modREConstants.CNSTLANDCATSOFTWOOD) + " or category = " + FCConvert.ToString(modREConstants.CNSTLANDCATSOFTWOODFARM) + ") ", modGlobalVariables.strREDatabase);
                    // If Val(txtSoft.Text) - 10 > 0 Then
                    // must update the softwood price for all schedules
                    // skip the records that don't belong to the land schedules
                    // Call clsSave.Execute("update landsRECORD set LSRATE" & Format(Val(txtSoft.Text) - 10, "00") & " = " & Val(txtSoftwood.Text) * 100 & " where (lrecordnumber < 31) or (lrecordnumber > 35 and lrecordnumber < 105)", strREDatabase)
                    // End If
                }
                if (Conversion.Val(txtMixedWood.Text) > 0)
                {
                    // Call clsSave.Execute("update landschedule set amount = " & Val(txtMixedWood.Text) & " where code = " & CustomizedInfo.Mixedwood, strREDatabase)
                    // Call clsSave.Execute("update (select * from landschedule inner join landtype on (landschedule.code = landtype.code) where category = " & CNSTLANDCATMIXEDWOOD & " or category = " & CNSTLANDCATMIXEDWOODFARM & ") set amount = " & Val(txtMixedWood.Text), strREDatabase)
                    clsSave.Execute("update landschedule set amount = " + FCConvert.ToString(Conversion.Val(txtMixedWood.Text)) + " where code in (select code from landtype where category = " + FCConvert.ToString(modREConstants.CNSTLANDCATMIXEDWOOD) + " or category = " + FCConvert.ToString(modREConstants.CNSTLANDCATMIXEDWOODFARM) + ") ", modGlobalVariables.strREDatabase);
                    // If Val(txtMixed.Text) - 10 > 0 Then
                    // Call clsSave.Execute("update landsRECORD set LSRATE" & Format(Val(txtMixed.Text) - 10, "00") & " = " & Val(txtMixedWood.Text) * 100 & " where (lrecordnumber < 31) or (lrecordnumber > 35 and lrecordnumber < 105)", strREDatabase)
                    // End If
                }
                if (Conversion.Val(txtHardWood.Text) > 0)
                {
                    // Call clsSave.Execute("update landschedule set amount = " & Val(txtHardwood.Text) & " where code = " & CustomizedInfo.Hardwood, strREDatabase)
                    // Call clsSave.Execute("update (select * from landschedule inner join landtype on (landschedule.code = landtype.code) where category = " & CNSTLANDCATHARDWOOD & " or category = " & CNSTLANDCATHARDWOODFARM & ") set amount = " & Val(txtHardwood.Text), strREDatabase)
                    clsSave.Execute("update landschedule set amount = " + FCConvert.ToString(Conversion.Val(txtHardWood.Text)) + " where code in (select code from landtype where category = " + FCConvert.ToString(modREConstants.CNSTLANDCATHARDWOOD) + " or category = " + FCConvert.ToString(modREConstants.CNSTLANDCATHARDWOODFARM) + ") ", modGlobalVariables.strREDatabase);
                    // If Val(txtHard.Text) - 10 > 0 Then
                    // Call clsSave.Execute("update landsRECORD set LSRATE" & Format(Val(txtHard.Text) - 10, "00") & " = " & Val(txtHardwood.Text) * 100 & " where (lrecordnumber < 31) or (lrecordnumber > 35 and lrecordnumber < 105)", strREDatabase)
                    // End If
                }
            }

            BillRate = Strings.Format(txtBillRate.Text + "", "000.0000");
            if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
            {
                modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
            }
            modGlobalVariables.Statics.CustomizedInfo.Ref1Description = txtRef1Label.Text;
            modGlobalVariables.Statics.CustomizedInfo.Ref2Description = txtRef2Label.Text;
            modGlobalVariables.Statics.CustomizedInfo.BillRate = Conversion.Val(BillRate);
            modGlobalVariables.Statics.CustomizedInfo.BillYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBillYear.Text)));
            modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio = Conversion.Val(txtCertifiedRatio.Text) / 100;
            if (chkDontPrintPropertyPhoto.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges = false;
            }
            if (chkToolTips.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.DisableToolTips = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.DisableToolTips = false;
            }
            if (chkPropertyCard.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolAlwaysShowCurrentOnPropertyCard = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolAlwaysShowCurrentOnPropertyCard = false;
            }
            if (chkPropertyCardCurrentAmounts.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolAlwaysShowCurrentCurrentOnPropertyCard = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolAlwaysShowCurrentCurrentOnPropertyCard = false;
            }
            if (chkHideRatio.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.HideValuationRatio = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.HideValuationRatio = false;
            }
            if (chkPrevAssessments.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolNeverShowPreviousBillingOnPropertyCard = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolNeverShowPreviousBillingOnPropertyCard = false;
            }
            if (chkLandFactorTreeGrowth.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolDontApplyLandFactorToTreeGrowth = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolDontApplyLandFactorToTreeGrowth = false;
            }
            if (chkShowCode.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen = false;
            }
            if (chkAccountOnBottom.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom = false;
            }
            if (chkShortDesc.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.ShowShortDesc = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.ShowShortDesc = false;
            }
            if (chkHideLabelAccount.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.HideLabelAccount = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.HideLabelAccount = false;
            }
            if (chkShowPic.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolShowPicOnVal = true;
                if (chkShowFirstPic.CheckState == Wisej.Web.CheckState.Checked)
                {
                    modGlobalVariables.Statics.CustomizedInfo.boolShowFirstPiconVal = true;
                }
                else
                {
                    modGlobalVariables.Statics.CustomizedInfo.boolShowFirstPiconVal = false;
                }
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolShowPicOnVal = false;
                modGlobalVariables.Statics.CustomizedInfo.boolShowFirstPiconVal = false;
            }
            if (chkShowSketch.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolShowSketchOnVal = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolShowSketchOnVal = false;
            }
            if (chkRoundExemptions.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolRoundCalculatedHomesteads = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolRoundCalculatedHomesteads = false;
            }
            if (chkPropertyCardComments.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolDontShowCommentsOnPropertyCard = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolDontShowCommentsOnPropertyCard = false;
            }
            modGlobalVariables.Statics.CustomizedInfo.DefaultSchedule = FCConvert.ToInt32(Math.Round(Conversion.Val(GridDefaultSchedule.TextMatrix(0, 0))));
            if (chkPocketPCExternal.CheckState == Wisej.Web.CheckState.Checked)
            {
                modRegistry.SaveRegistryKey("REPocketPCExternal", FCConvert.ToString(true));
                modGlobalVariables.Statics.CustomizedInfo.boolPocketPCExternal = true;
            }
            else
            {
                modRegistry.SaveRegistryKey("REPocketPCExternal", FCConvert.ToString(false));
                modGlobalVariables.Statics.CustomizedInfo.boolPocketPCExternal = false;
            }

            modRegistry.SaveRegistryKey("REF2LabelType", FCConvert.ToString(cmbF2LabelType.ItemData(cmbF2LabelType.SelectedIndex)));
            //end switch

            modRegistry.SaveRegistryKey("RELimitMapLotToFour", FCConvert.ToString(chkMapLotSplit.CheckState == Wisej.Web.CheckState.Checked));
            modRegistry.SaveRegistryKey("REPocketPCExternalDirectory", txtPocketPCDirectory.Text);
            if (chkPocketPCExternal.CheckState == Wisej.Web.CheckState.Checked)
            {
                if (!Directory.Exists(txtPocketPCDirectory.Text))
                {
                    MessageBox.Show("Warning.  The Pocket PC data directory is not found." + "\r\n" + "If it is a network connection the connection may just be down at this time.", "Path Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    strTemp = txtPocketPCDirectory.Text;
                    strTemp = Strings.Trim(strTemp);
                    if (Strings.Right(strTemp, 1) != "\\")
                        strTemp += "\\";
                    if (!Directory.Exists(strTemp + "Files"))
                    {
                        Directory.CreateDirectory(strTemp + "Files");
                    }
                }
            }
            if (chkHomesteadCard1.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolApplyHomesteadToCardOneOnly = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolApplyHomesteadToCardOneOnly = false;
            }
            if (chkTreeGrowthCertifiedRatio.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolApplyCertifiedRatioToTreeGrowth = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolApplyCertifiedRatioToTreeGrowth = false;
            }
            if (chkPrintSecondaryPhoto.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolPrintSecondaryPicOnPropertyCard = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolPrintSecondaryPicOnPropertyCard = false;
            }
            if (chkPrintMapLotOnSide.CheckState == Wisej.Web.CheckState.Checked)
            {
                modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide = true;
            }
            else
            {
                modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide = false;
            }
            int RoundingVar = 0;
            if (cboRounding.SelectedIndex == 0)
                RoundingVar = 4;
            // $1
            if (cboRounding.SelectedIndex == 1)
                RoundingVar = 3;
            // $10
            if (cboRounding.SelectedIndex == 2)
                RoundingVar = 2;
            // $100
            if (cboRounding.SelectedIndex == 3)
                RoundingVar = 1;
            // $1000
            modGlobalVariables.Statics.CustomizedInfo.Round = FCConvert.ToInt16(RoundingVar);
            modGlobalVariables.Statics.CustomizedInfo.RoundContingency = FCConvert.ToInt16(cmbRoundContingency.SelectedIndex);
            modGlobalVariables.Statics.CustomizedInfo.ValReportOption = FCConvert.ToInt16(cboValuationReport.SelectedIndex + 1);
            modGlobalVariables.Statics.CustomizedInfo.SquareFootLivingArea = FCConvert.ToInt16(cboSFLA.SelectedIndex + 1);
            modGlobalVariables.Statics.CustomizedInfo.AcreOption = FCConvert.ToInt16(cboPerAcreValue.SelectedIndex + 1);
            modGlobalVariables.Statics.CustomizedInfo.Ref1BookPage = FCConvert.ToInt16(cmbLabelOption.SelectedIndex + 1);
            modGlobalVariables.Statics.CustomizedInfo.OpenCode = FCConvert.ToInt32(Strings.Trim(txtOpenCode.Text));
            strTemp = "";
            // For x = 0 To 4
            // If Val(txtFarmLandCodes(x).Text) > 0 Then
            // strTemp = strTemp & Val(txtFarmLandCodes(x).Text) & ","
            // End If
            // Next x
            // 
            // If strTemp <> vbNullString Then
            // strTemp = Mid(strTemp, 1, Len(strTemp) - 1) 'get rid of last comma
            // End If
            // clsSave.Fields("farmcode") = strTemp
            string nfc;
            nfc = "N";
            if (cboLandPricing.SelectedIndex + 1 == 1)
                nfc = "N";
            if (cboLandPricing.SelectedIndex + 1 == 2)
                nfc = "F";
            if (cboLandPricing.SelectedIndex + 1 == 3)
                nfc = "C";
            modGlobalVariables.Statics.CustomizedInfo.LandUnitType = nfc;
            modGlobalVariables.Statics.CustomizedInfo.DepreciationYear = FCConvert.ToInt16(Math.Round(Conversion.Val(txtREDep.Text)));
            modGlobalVariables.Statics.CustomizedInfo.MHDepreciationYear = FCConvert.ToInt16(Math.Round(Conversion.Val(txtMoHoDep.Text)));
            modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear = FCConvert.ToInt16(Math.Round(Conversion.Val(txtComDep.Text)));
            modGlobalVariables.Statics.CustomizedInfo.PPIndPropCat = FCConvert.ToInt16(cmbPPIndPropCat.SelectedIndex);
            // CustomizedInfo.REIndPropBldgCode = Val(txtREIndPropBldgCode.Text)
            modGlobalVariables.Statics.CustomizedInfo.PPCat1TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(PPGrid.TextMatrix(1, 1))));
            modGlobalVariables.Statics.CustomizedInfo.PPCat2TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(PPGrid.TextMatrix(2, 1))));
            modGlobalVariables.Statics.CustomizedInfo.PPCat3TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(PPGrid.TextMatrix(3, 1))));
            modGlobalVariables.Statics.CustomizedInfo.PPCat4TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(PPGrid.TextMatrix(4, 1))));
            modGlobalVariables.Statics.CustomizedInfo.PPCat5TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(PPGrid.TextMatrix(5, 1))));
            modGlobalVariables.Statics.CustomizedInfo.PPCat6TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(PPGrid.TextMatrix(6, 1))));
            modGlobalVariables.Statics.CustomizedInfo.PPCat7TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(PPGrid.TextMatrix(7, 1))));
            modGlobalVariables.Statics.CustomizedInfo.PPCat8TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(PPGrid.TextMatrix(8, 1))));
            modGlobalVariables.Statics.CustomizedInfo.PPCat9TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(PPGrid.TextMatrix(9, 1))));

            strTemp = "";
            for (x = 1; x <= gridIndustrialBldgCodes.Rows - 1; x++)
            {
                if (FCConvert.CBool(gridIndustrialBldgCodes.TextMatrix(x, CNSTGRIDBLDGCODECOLUSE)) == true)
                {
                    strTemp += gridIndustrialBldgCodes.TextMatrix(x, CNSTGRIDBLDGCODECOLCODE) + ",";
                }
            }
            // x
            if (strTemp != string.Empty)
            {
                strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
            }
            modGlobalVariables.Statics.CustomizedInfo.REIndPropBldgCodes = strTemp;
            if (modGlobalVariables.Statics.CustomizedInfo.SaveCustomInfo())
            {
                MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmdSave_Click = true;
            }
            //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
            App.MainForm.ReloadNavigationMenu();
            return cmdSave_Click;
        }

        private void frmREOptions_Activated(object sender, System.EventArgs e)
        {

        }

        private void frmREOptions_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (frmREOptions.InstancePtr.ActiveControl is FCTextBox)
            {
                if (Shift == 2)
                {
                    if (KeyCode == Keys.Left || KeyCode == Keys.Right)
                        KeyCode = (Keys)0;
                }
                else
                {
                    if (KeyCode == Keys.Left || KeyCode == Keys.Right)
                        (frmREOptions.InstancePtr.ActiveControl as FCTextBox).SelectionLength = 0;
                }
            }
            if (KeyCode == Keys.F10)
            {
                KeyCode = (Keys)0;
                cmdSave_Click();
            }
            else if (KeyCode == Keys.Escape)
            {
                KeyCode = (Keys)0;
                Unload();
            }
        }

        private void frmREOptions_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                Support.SendKeys("{TAB}", false);
                KeyAscii = (Keys)0;
            }
            // these lines check for the backspace key
            if (KeyAscii == Keys.Back)
            {
                if (modGNBas.Statics.OverType == 1)
                {
                    if (frmREOptions.InstancePtr.ActiveControl is FCTextBox)
                    {
                        KeyAscii = (Keys)0;
                        (frmREOptions.InstancePtr.ActiveControl as FCTextBox).SelectionLength = 0;
                        Support.SendKeys("{LEFT}", false);
                        (frmREOptions.InstancePtr.ActiveControl as FCTextBox).SelectionLength = 1;
                    }
                }
            }
            // end of backspace check
            // If TypeOf frmREOptions.ActiveControl Is TextBox Then frmREOptions.ActiveControl.SelLength = OverType
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void FillComboBoxes()
        {
            // Dim clsLoad As New clsDRWrapper
            clsDRWrapper clsTemp = new clsDRWrapper();
            string strTemp = "";
            int x;
            string[] strAry = null;
            int intTemp = 0;
            boolLoading = true;
            txtRef1Label.Text = modGlobalVariables.Statics.CustomizedInfo.Ref1Description;
            txtRef2Label.Text = modGlobalVariables.Statics.CustomizedInfo.Ref2Description;
            if (modGlobalVariables.Statics.CustomizedInfo.MaxCardsAllowed != modGlobalConstants.EleventyBillion)
            {
                lblMaxCards.Text = Strings.Format(modGlobalVariables.Statics.CustomizedInfo.MaxCardsAllowed, "#,###,##0");
            }
            else
            {
                lblMaxCards.Text = "Unlimited";
            }
            lblCardsUsed.Text = Strings.Format(modGlobalVariables.Statics.CustomizedInfo.CardsUsed, "#,###,##0");
            cmbF2LabelType.Clear();
            cmbF2LabelType.AddItem("Dymo 30252");
            cmbF2LabelType.ItemData(cmbF2LabelType.NewIndex, modLabels.CNSTLBLTYPEDYMO30252);
            cmbF2LabelType.AddItem("Dymo 30256");
            cmbF2LabelType.ItemData(cmbF2LabelType.NewIndex, modLabels.CNSTLBLTYPEDYMO30256);
            cmbRoundContingency.Clear();
            cmbRoundContingency.AddItem("Use chosen option only");
            cmbRoundContingency.AddItem("If < option use next lower");
            cmbRoundContingency.AddItem("If < half option make half");
            if (FCConvert.CBool(modRegistry.GetRegistryKey("DisableToolTips", "RE", FCConvert.ToString(false))))
            {
                chkToolTips.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkToolTips.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            // Land Pricing/Unit Print Format
            cboLandPricing.AddItem("1.  Don't Display            ");
            cboLandPricing.AddItem("2.  Display File Amount      ");
            cboLandPricing.AddItem("3.  Display Calculated Amount");
            string nfc;
            nfc = modGlobalVariables.Statics.CustomizedInfo.LandUnitType;
            if (nfc == "N")
                cboLandPricing.SelectedIndex = 0;
            if (nfc == "F")
                cboLandPricing.SelectedIndex = 1;
            if (nfc == "C")
                cboLandPricing.SelectedIndex = 2;
            // Valuation Report Option
            cboValuationReport.AddItem("1.  Print Tran, Land & Building Codes Only");
            cboValuationReport.AddItem("2.  Print Exempt Codes Only               ");
            cboValuationReport.AddItem("3.  Print All Options                     ");
            cboValuationReport.AddItem("4.  Do Not Print any Options              ");
            cboValuationReport.SelectedIndex = modGlobalVariables.Statics.CustomizedInfo.ValReportOption - 1;
            cmbLabelOption.SelectedIndex = modGlobalVariables.Statics.CustomizedInfo.Ref1BookPage - 1;
            // $ Per SFLA Option
            cboSFLA.AddItem("1. Total Land & Building         ");
            cboSFLA.AddItem("2. Land & Living Area of Building");
            cboSFLA.AddItem("3. Total Building                ");
            cboSFLA.AddItem("4. Living Area of Building Only  ");
            cboSFLA.AddItem("5. No Display of SFLA & $/SFLA   ");
            cboSFLA.AddItem("6. Display SFLA without $/SFLA   ");
            cboSFLA.SelectedIndex = modGlobalVariables.Statics.CustomizedInfo.SquareFootLivingArea - 1;
            // Per Acre Valuation Option
            cboPerAcreValue.AddItem("1.  Print Per Acre Value       ");
            cboPerAcreValue.AddItem("2.  Do Not Print Per Acre Value");
            cboPerAcreValue.SelectedIndex = modGlobalVariables.Statics.CustomizedInfo.AcreOption - 1;
            // Rounding Option Combo Box
            cboRounding.AddItem("1.  Round to Nearest 1   ");
            cboRounding.AddItem("2.  Round to Nearest 10  ");
            cboRounding.AddItem("3.  Round to Nearest 100 ");
            cboRounding.AddItem("4.  Round to Nearest 1000");
            int RoundingVar;
            RoundingVar = modGlobalVariables.Statics.CustomizedInfo.Round;
            if (RoundingVar == 4)
                cboRounding.SelectedIndex = 0;
            // $1
            if (RoundingVar == 3)
                cboRounding.SelectedIndex = 1;
            // $10
            if (RoundingVar == 2)
                cboRounding.SelectedIndex = 2;
            // $100
            if (RoundingVar == 1)
                cboRounding.SelectedIndex = 3;
            // $1000
            if (cboRounding.SelectedIndex == 3)
            {
                cmbRoundContingency.Visible = true;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
            {
                chkShowCode.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkShowCode.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.RoundContingency == modGlobalVariables.CNSTROUNDNEXTOPTION)
            {
                cmbRoundContingency.SelectedIndex = modGlobalVariables.CNSTROUNDNEXTOPTION;
            }
            else if (modGlobalVariables.Statics.CustomizedInfo.RoundContingency == modGlobalVariables.CNSTROUNDHALFOPTION)
            {
                cmbRoundContingency.SelectedIndex = modGlobalVariables.CNSTROUNDHALFOPTION;
            }
            else
            {
                cmbRoundContingency.SelectedIndex = modGlobalVariables.CNSTROUNDASOPTION;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.ShowShortDesc)
            {
                chkShortDesc.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkShortDesc.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            //cboPrinter.AddItem("1.  Best Available");
            //cboPrinter.SelectedIndex = 0;
            
            txtCertifiedRatio.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio * 100);
            txtOpenCode.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.OpenCode);

            chkDontPrintPropertyPhoto.CheckState = modGlobalVariables.Statics.CustomizedInfo.HidePicsForPropertyCardRanges
                ? Wisej.Web.CheckState.Checked
                : Wisej.Web.CheckState.Unchecked;

            chkToolTips.CheckState = modGlobalVariables.Statics.CustomizedInfo.DisableToolTips
                ? Wisej.Web.CheckState.Checked
                : Wisej.Web.CheckState.Unchecked;

            chkAccountOnBottom.CheckState = modGlobalVariables.Statics.CustomizedInfo.AccountOnBottom
                ? Wisej.Web.CheckState.Checked
                : Wisej.Web.CheckState.Unchecked;

            chkHideRatio.CheckState = modGlobalVariables.Statics.CustomizedInfo.HideValuationRatio
                ? Wisej.Web.CheckState.Checked
                : Wisej.Web.CheckState.Unchecked;

            if (modGlobalVariables.Statics.CustomizedInfo.boolShowPicOnVal)
            {
                chkShowPic.CheckState = Wisej.Web.CheckState.Checked;
                chkShowFirstPic.Visible = true;
                chkShowFirstPic.CheckState = modGlobalVariables.Statics.CustomizedInfo.boolShowFirstPiconVal ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked;
            }
            else
            {
                chkShowFirstPic.Visible = false;
                chkShowPic.CheckState = Wisej.Web.CheckState.Unchecked;
                chkShowFirstPic.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            chkShowSketch.CheckState = modGlobalVariables.Statics.CustomizedInfo.boolShowSketchOnVal ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked;
            if (modGlobalVariables.Statics.CustomizedInfo.boolAlwaysShowCurrentOnPropertyCard)
            {
                chkPropertyCard.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkPropertyCard.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.boolAlwaysShowCurrentCurrentOnPropertyCard)
            {
                chkPropertyCardCurrentAmounts.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkPropertyCardCurrentAmounts.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.boolNeverShowPreviousBillingOnPropertyCard)
            {
                chkPrevAssessments.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkPrevAssessments.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.boolDontShowCommentsOnPropertyCard)
            {
                chkPropertyCardComments.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkPropertyCardComments.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.boolDontApplyLandFactorToTreeGrowth)
            {
                chkLandFactorTreeGrowth.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkLandFactorTreeGrowth.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.boolPrintSecondaryPicOnPropertyCard)
            {
                chkPrintSecondaryPhoto.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkPrintSecondaryPhoto.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (FCConvert.CBool(modRegistry.GetRegistryKey_3("REPocketPCExternal", FCConvert.ToString(false))))
            {
                chkPocketPCExternal.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkPocketPCExternal.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            txtPocketPCDirectory.Text = Strings.Trim(modRegistry.GetRegistryKey("REPocketPCExternalDirectory"));
            if (FCConvert.CBool(modRegistry.GetRegistryKey_3("RELimitMapLotToFour", FCConvert.ToString(false))))
            {
                chkMapLotSplit.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkMapLotSplit.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.boolApplyHomesteadToCardOneOnly)
            {
                chkHomesteadCard1.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkHomesteadCard1.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.boolApplyCertifiedRatioToTreeGrowth)
            {
                chkTreeGrowthCertifiedRatio.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkTreeGrowthCertifiedRatio.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.boolPrintMapLotOnSide)
            {
                chkPrintMapLotOnSide.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkPrintMapLotOnSide.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            if (modGlobalVariables.Statics.CustomizedInfo.HideLabelAccount)
            {
                chkHideLabelAccount.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkHideLabelAccount.CheckState = Wisej.Web.CheckState.Unchecked;
            }

            intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("REF2LabelType"))));
            for (x = 0; x <= cmbF2LabelType.Items.Count - 1; x++)
            {
                if (cmbF2LabelType.ItemData(x) == intTemp)
                {
                    cmbF2LabelType.SelectedIndex = x;
                    break;
                }
            }
            // x
            if (modGlobalVariables.Statics.CustomizedInfo.boolRoundCalculatedHomesteads)
            {
                chkRoundExemptions.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkRoundExemptions.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            txtBillRate.Text = Strings.Format(modGlobalVariables.Statics.CustomizedInfo.BillRate, "00.000");
            txtBillYear.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.BillYear);
            txtREDep.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.DepreciationYear);
            txtMoHoDep.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.MHDepreciationYear);
            txtComDep.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear);
            clsTemp.OpenRecordset("Select * from defaults", modGlobalVariables.strREDatabase);
            if (!clsTemp.EndOfFile())
            {
                txtSoftWood.Text = Conversion.Val(clsTemp.Get_Fields_Double("softcost")) > 0 
                    ? FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Double("softcost"))) 
                    : "";

                txtMixedWood.Text = Conversion.Val(clsTemp.Get_Fields_Double("mixedcost")) > 0 
                    ? FCConvert
                    .ToString(Conversion.Val(clsTemp.Get_Fields_Double("mixedcost"))) 
                    : "";

                txtHardWood.Text = Conversion.Val(clsTemp.Get_Fields_Double("Hardcost")) > 0 
                    ? FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Double("hardcost")))
                    : "";
            }
            else
            {
                txtSoftWood.Text = "";
                txtMixedWood.Text = "";
                txtHardWood.Text = "";
            }
            if (modGlobalVariables.Statics.CustomizedInfo.ShowSketchGridLines)
            {
                chkShowGridLines.CheckState = Wisej.Web.CheckState.Checked;
            }
            else
            {
                chkShowGridLines.CheckState = Wisej.Web.CheckState.Unchecked;
            }
            PPGrid.TextMatrix(1, 1, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.PPCat1TaxValCat));
            PPGrid.TextMatrix(2, 1, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.PPCat2TaxValCat));
            PPGrid.TextMatrix(3, 1, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.PPCat3TaxValCat));
            PPGrid.TextMatrix(4, 1, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.PPCat4TaxValCat));
            PPGrid.TextMatrix(5, 1, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.PPCat5TaxValCat));
            PPGrid.TextMatrix(6, 1, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.PPCat6TaxValCat));
            PPGrid.TextMatrix(7, 1, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.PPCat7TaxValCat));
            PPGrid.TextMatrix(8, 1, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.PPCat8TaxValCat));
            PPGrid.TextMatrix(9, 1, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.PPCat9TaxValCat));
            // txtREIndPropBldgCode.Text = Val(CustomizedInfo.REIndPropBldgCode)
            cmbPPIndPropCat.SelectedIndex = modGlobalVariables.Statics.CustomizedInfo.PPIndPropCat;
            FillGridIndustrialBldgCodes();
            GridDefaultSchedule.TextMatrix(0, 0, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.DefaultSchedule));
            boolLoading = false;
        }

        private void frmREOptions_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (frmREOptions.InstancePtr.ActiveControl is FCTextBox)
            {
            }
        }

        private void frmREOptions_Load(object sender, System.EventArgs e)
        {
            FormLoaded = "N".ToCharArray();
            modGlobalFunctions.SetFixedSize(this);
            SetupPPGrid();
            SetupGridTownCode();
            SetupGridIndustrialBldgCodes();
            if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
            {
                gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
                modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = 1;
            }
            else
            {
                gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
            }
            PPGrid.Font = new Font(PPGrid.Font.FontFamily, 7);
            //FC:FINAL:AM: adjust the row height
            PPGrid.RowHeight(-1, 350);
            if (!modREMain.Statics.boolShortRealEstate)
            {
                SetupGridDefaultSchedule();
            }
            else
            {
                GridDefaultSchedule.Visible = false;
                Label26.Visible = false;
                GridDefaultSchedule.TextMatrix(0, 0, FCConvert.ToString(0));
            }
            FillCityStateZip();
            FillComboBoxes();
        }

        private void FillCityStateZip()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            clsLoad.OpenRecordset("select * from citystatezipdefaults", modGlobalVariables.strREDatabase);
            if (!clsLoad.EndOfFile())
            {
                txtCity.Text = FCConvert.ToString(clsLoad.Get_Fields_String("City"));
                txtState.Text = FCConvert.ToString(clsLoad.Get_Fields_String("State"));
                txtZip.Text = FCConvert.ToString(clsLoad.Get_Fields_String("Zip"));
            }
            else
            {
                txtCity.Text = "";
                txtState.Text = "";
                txtZip.Text = "";
            }
        }

        private void frmREOptions_Resize(object sender, System.EventArgs e)
        {
            ResizePPGrid();
            ResizeGridTownCode();
            ResizeGridIndustrialBldgCodes();
            //GridDefaultSchedule.Height = GridDefaultSchedule.RowHeight(0) + 30;         
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            //MDIParent.InstancePtr.Show();
            //Application.DoEvents();
        }

        private void gridTownCode_Leave(object sender, System.EventArgs e)
        {
            //Application.DoEvents();
            if (lngLastTownCode == Conversion.Val(gridTownCode.TextMatrix(0, 0)))
                return;
            LoadTownSpecificStuff();
            lngLastTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
        }

        private void LoadTownSpecificStuff()
        {
            int lngTownCode;
            clsDRWrapper clsLoad = new clsDRWrapper();
            lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
            modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = lngTownCode;
            txtBillRate.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.BillRate);
            txtBillYear.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.BillYear);
            txtCertifiedRatio.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio * 100);
            // Call clsLoad.OpenRecordset("select * from TownSpecific where townnumber = " & lngTownCode, strREDatabase)
            // If Not clsLoad.EndOfFile Then
            // txtBillRate.Text = Val(clsLoad.Fields("billingrate"))
            // txtBillYear.Text = clsLoad.Fields("billingyear")
            // txtCertifiedRatio.Text = Val(clsLoad.Fields("certifiedratio")) * 100
            // End If
        }

        private void mnuCommercialStatistics_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            string strSQL;
            int lngTotal;
            try
            {
                // On Error GoTo ErrorHandler
                strSQL = "Select * from commercial inner join master on (commercial.rsaccount = master.rsaccount) and (commercial.rscard = master.rscard) where not rsdeleted = 1";
                rsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                lngTotal = 0;
                while (!rsLoad.EndOfFile())
                {
                    if (Conversion.Val(rsLoad.Get_Fields_Int32("occ1")) > 0)
                    {
                        lngTotal += 1;
                    }
                    if (Conversion.Val(rsLoad.Get_Fields_Int32("occ2")) > 0)
                    {
                        lngTotal += 1;
                    }
                    rsLoad.MoveNext();
                }
                MessageBox.Show("There are " + FCConvert.ToString(lngTotal) + " commercial buildings", "Total Commercial Buildings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CommercialStatistics", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuQuit_Click(object sender, System.EventArgs e)
        {
            cmdQuit_Click();
        }

        public void mnuQuit_Click()
        {
            mnuQuit_Click(mnuQuit, new System.EventArgs());
        }

        private void mnuSave_Click(object sender, System.EventArgs e)
        {
            cmdSave_Click();
        }

        private void mnuSaveExit_Click(object sender, System.EventArgs e)
        {
            if (cmdSave_Click())
            {
                mnuQuit_Click();
            }
        }

        private void txtBillRate_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtBillRate.SelectionStart = 0;
            // txtBillRate.SelLength = 1
            txtBillRate.SelectionLength = txtBillRate.Text.Length;
        }
        // Private Sub txtBillRate_KeyDown(KeyCode As Integer, Shift As Integer)
        // If txtBillRate.SelLength > 0 Then txtBillRate.Text = ""
        // End Sub
        private void txtBillRate_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            // If txtBillRate.SelStart = 3 Then txtBillRate.SelStart = 4
        }

        private void txtBillYear_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtBillYear.SelectionStart = 0;
            txtBillYear.SelectionLength = txtBillYear.Text.Length;
        }

        private void txtCertifiedRatio_Enter(object sender, System.EventArgs e)
        {
            txtCertifiedRatio.SelectionStart = 0;
            txtCertifiedRatio.SelectionLength = txtCertifiedRatio.Text.Length;
        }

        private void txtCity_Enter(object sender, System.EventArgs e)
        {
            txtCity.SelectionStart = 0;
            txtCity.SelectionLength = txtCity.Text.Length;
        }

        private void txtComDep_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtComDep.SelectionStart = 0;
            txtComDep.SelectionLength = txtComDep.Text.Length;
        }

        private void txtHardWood_Enter(object sender, System.EventArgs e)
        {
            txtHardWood.SelectionStart = 0;
            txtHardWood.SelectionLength = txtHardWood.Text.Length;
        }

        private void txtMixedWood_Enter(object sender, System.EventArgs e)
        {
            txtMixedWood.SelectionStart = 0;
            txtMixedWood.SelectionLength = txtMixedWood.Text.Length;
        }
        // Private Sub txtHomesteadCodes_GotFocus(Index As Integer)
        // OverType = 1
        // txthomesteadcodes(Index).SelStart = 0
        // txthomesteadcodes(Index).SelLength = Len(txthomesteadcodes.Text)
        // End Sub
        // Private Sub txtHomesteadCodes_GotFocus()
        // End Sub
        private void txtMoHoDep_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtMoHoDep.SelectionStart = 0;
            txtMoHoDep.SelectionLength = txtMoHoDep.Text.Length;
        }

        private void txtREDep_Enter(object sender, System.EventArgs e)
        {
            // OverType = 1
            txtREDep.SelectionStart = 0;
            txtREDep.SelectionLength = txtREDep.Text.Length;
        }

        private void SetupPPGrid()
        {
            clsDRWrapper clsLoad = new clsDRWrapper();
            int x;
            PPGrid.TextMatrix(0, 0, "Category");
            PPGrid.TextMatrix(0, 1, "Taxable Val. Type");
            PPGrid.ColComboList(1, "#0;Other|#1;Production Mach & Equip|#2;Business Equipment");
            cmbPPIndPropCat.AddItem("None");
            if (modGlobalConstants.Statics.gboolPP)
            {
                clsLoad.OpenRecordset("select * from RATIOTRENDS order by type", modGlobalVariables.strPPDatabase);
                while (!clsLoad.EndOfFile())
                {
                    if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))) != string.Empty)
                    {
                        // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                        PPGrid.TextMatrix(clsLoad.Get_Fields("type"), 0, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                        PPGrid.TextMatrix(clsLoad.Get_Fields("type"), 0, "Category " + clsLoad.Get_Fields("type"));
                    }
                    // TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
                    cmbPPIndPropCat.AddItem(PPGrid.TextMatrix(clsLoad.Get_Fields("type"), 0));
                    clsLoad.MoveNext();
                }
            }
            else
            {
                for (x = 1; x <= 9; x++)
                {
                    PPGrid.TextMatrix(x, 0, "Category " + FCConvert.ToString(x));
                    PPGrid.TextMatrix(x, 1, FCConvert.ToString(CNSTMVRVALPPOTHER));
                    cmbPPIndPropCat.AddItem(PPGrid.TextMatrix(x, 0));
                }
                // x
            }
            PPGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
        }

        private void ResizePPGrid()
        {
            int GridWidth = 0;
            GridWidth = PPGrid.WidthOriginal;
            PPGrid.ColWidth(0, FCConvert.ToInt32(0.5 * GridWidth));
            //PPGrid.Height = PPGrid.RowHeight(0) * 10 + 60;
        }

        private void txtSoftWood_Enter(object sender, System.EventArgs e)
        {
            txtSoftWood.SelectionStart = 0;
            txtSoftWood.SelectionLength = txtSoftWood.Text.Length;
        }

        private void txtState_Enter(object sender, System.EventArgs e)
        {
            txtState.SelectionStart = 0;
            txtState.SelectionLength = txtState.Text.Length;
        }

        private void txtZip_Enter(object sender, System.EventArgs e)
        {
            txtZip.SelectionStart = 0;
            txtZip.SelectionLength = txtZip.Text.Length;
        }
    }
}
