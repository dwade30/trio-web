﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptGlobalChanges.
	/// </summary>
	partial class rptGlobalChanges
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptGlobalChanges));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.ColumnCount = 3;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtCard
			});
			this.Detail.Height = 0.2291667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field6,
				this.txtTitle2,
				this.Field7,
				this.Field8
			});
			this.PageHeader.Height = 0.8645833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
			this.Field1.Left = 2F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "Global Changes Report";
			this.Field1.Top = 0F;
			this.Field1.Width = 3.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.0625F;
			this.txtMuni.Width = 1.8125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.21875F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.0625F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6.4375F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Field2";
			this.txtPage.Top = 0.21875F;
			this.txtPage.Width = 1F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 0.0625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-weight: bold; text-align: right";
			this.Field2.Text = "Account";
			this.Field2.Top = 0.59375F;
			this.Field2.Width = 0.8125F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.875F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-weight: bold; text-align: right";
			this.Field3.Text = "Card";
			this.Field3.Top = 0.59375F;
			this.Field3.Width = 0.4375F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 2.5625F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-weight: bold; text-align: right";
			this.Field4.Text = "Account";
			this.Field4.Top = 0.59375F;
			this.Field4.Width = 0.8125F;
			// 
			// Field6
			// 
			this.Field6.CanGrow = false;
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 3.375F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-weight: bold; text-align: right";
			this.Field6.Text = "Card";
			this.Field6.Top = 0.59375F;
			this.Field6.Width = 0.4375F;
			// 
			// txtTitle2
			// 
			this.txtTitle2.Height = 0.19F;
			this.txtTitle2.Left = 1.5F;
			this.txtTitle2.Name = "txtTitle2";
			this.txtTitle2.Style = "text-align: center";
			this.txtTitle2.Text = "Field2";
			this.txtTitle2.Top = 0.21875F;
			this.txtTitle2.Width = 4.6875F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1875F;
			this.Field7.Left = 5.0625F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-weight: bold; text-align: right";
			this.Field7.Text = "Account";
			this.Field7.Top = 0.59375F;
			this.Field7.Width = 0.8125F;
			// 
			// Field8
			// 
			this.Field8.CanGrow = false;
			this.Field8.Height = 0.1875F;
			this.Field8.Left = 5.875F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-weight: bold; text-align: right";
			this.Field8.Text = "Card";
			this.Field8.Top = 0.59375F;
			this.Field8.Width = 0.4375F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0.0625F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-weight: bold; text-align: right";
			this.txtAccount.Text = "Account";
			this.txtAccount.Top = 0.03125F;
			this.txtAccount.Width = 0.8125F;
			// 
			// txtCard
			// 
			this.txtCard.Height = 0.1875F;
			this.txtCard.Left = 0.875F;
			this.txtCard.Name = "txtCard";
			this.txtCard.Style = "font-weight: bold; text-align: right";
			this.txtCard.Text = "Card";
			this.txtCard.Top = 0.03125F;
			this.txtCard.Width = 0.4375F;
			// 
			// rptGlobalChanges
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCard;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
