﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cExemptByAccountController
	{
		//=========================================================
		private cREAccountController reacctController = new cREAccountController();
		private cPartyController pController = new cPartyController();

		public void FillReport(ref cExemptByAccountReport rept)
		{
			if (!(rept == null))
			{
				string strSQL = "";
				strSQL = "Select * from master where not rsdeleted = 1 and rscard = 1 and (riexemptcd1 > 0 or riexemptcd2 > 0 or riexemptcd3 > 0) ";
				if (rept.ByRange)
				{
					if (Strings.LCase(rept.RangeType) == "account")
					{
						strSQL += " and rsaccount between " + rept.MinRange + " and " + rept.MaxRange;
					}
					else if (Strings.LCase(rept.RangeType) == "exempt")
					{
						strSQL += " and (riexemptcd1 in (" + rept.ExemptionsToUse + ") or riexemptcd2 in (" + rept.ExemptionsToUse + ") or riexemptcd3 in (" + rept.ExemptionsToUse + ") )";
					}
				}
				strSQL += " order by rsaccount";
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsExemptions = new clsDRWrapper();
				cAccountInfo tAcct;
				cExemptAssessInfo tEx;
				string strTemp = "";
				// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
				cParty tParty = new cParty();
				rsExemptions.OpenRecordset("select * from exemptcode order by code", "RealEstate");
				rsLoad.OpenRecordset(strSQL, "TWRE0000.vb1");
				while (!rsLoad.EndOfFile())
				{
					tAcct = new cAccountInfo();
					tAcct.Account = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("rsaccount"));
					tAcct.ExemptTotal = rsLoad.Get_Fields_Int32("rlexemption");
					tAcct.Maplot = FCConvert.ToString(rsLoad.Get_Fields_String("rsmaplot"));
					//tParty = pController.GetParty(rsLoad.Get_Fields_Int32("OwnerPartyID"), true);
					if (rsLoad.Get_Fields_String("DeedName1").Trim() != "")
					{
						tAcct.Owners.Add(rsLoad.Get_Fields_String("DeedName1").Trim());
					}
					//tParty = pController.GetParty(rsLoad.Get_Fields_Int32("SecOwnerPartyID"), true);
					if (rsLoad.Get_Fields_String("DeedName2").Trim() != "")
					{
						tAcct.Owners.Add(rsLoad.Get_Fields_String("DeedName2").Trim());
					}
					int x;
					for (x = 1; x <= 3; x++)
					{
						// TODO Get_Fields: Field [riexemptcd] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsLoad.Get_Fields("riexemptcd" + FCConvert.ToString(x))) > 0)
						{
							tEx = new cExemptAssessInfo();
							tEx.Account = tAcct.Account;
							// TODO Get_Fields: Field [exemptval] not found!! (maybe it is an alias?)
							tEx.Amount = Conversion.Val(rsLoad.Get_Fields("exemptval" + FCConvert.ToString(x)));
							// TODO Get_Fields: Field [riexemptcd] not found!! (maybe it is an alias?)
							tEx.Exemptcode = FCConvert.ToInt32(rsLoad.Get_Fields("riexemptcd" + FCConvert.ToString(x)));
							if (rsExemptions.FindFirst("code = " + FCConvert.ToString(tEx.Exemptcode)))
							{
								tEx.Description = FCConvert.ToString(rsExemptions.Get_Fields_String("Description"));
							}
							else
							{
								tEx.Description = "Unknown";
							}
							tAcct.Exemptions.AddItem(tEx);
						}
					}
					// x
					rept.ExemptionRecords.AddItem(tAcct);
					rsLoad.MoveNext();
				}
			}
		}
	}
}
