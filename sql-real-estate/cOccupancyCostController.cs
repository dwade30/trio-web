﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cOccupancyCostController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cGenericCollection GetOccupancyCostsByOccupancy(int lngId)
		{
			cGenericCollection GetOccupancyCostsByOccupancy = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				return GetOccupancyCostsByOccupancy;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetOccupancyCostsByOccupancy;
		}

		public cOccupancyCost GetOccupancyCost(int lngId)
		{
			cOccupancyCost GetOccupancyCost = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cOccupancyCost occCost = null;
				rsLoad.OpenRecordset("select * from CommercialOccupancyCosts where id = " + FCConvert.ToString(lngId), "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					occCost = new cOccupancyCost();
					// TODO Get_Fields: Field [ConstructionClassID] not found!! (maybe it is an alias?)
					occCost.ConstructionClassID = FCConvert.ToInt32(rsLoad.Get_Fields("ConstructionClassID"));
					// TODO Get_Fields: Field [QualityCode] not found!! (maybe it is an alias?)
					occCost.QualityCode = FCConvert.ToInt32(rsLoad.Get_Fields("QualityCode"));
					occCost.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Field [OccupancyCodeID] not found!! (maybe it is an alias?)
					occCost.OccupancyCodeID = FCConvert.ToInt32(rsLoad.Get_Fields("OccupancyCodeID"));
					// TODO Get_Fields: Field [SqftCost] not found!! (maybe it is an alias?)
					occCost.SqftCost = rsLoad.Get_Fields("SqftCost");
					// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
					occCost.Life = FCConvert.ToInt32(rsLoad.Get_Fields("Life"));
					occCost.IsUpdated = false;
				}
				GetOccupancyCost = occCost;
				return GetOccupancyCost;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetOccupancyCost;
		}

		public void SaveOccupancyCost(ref cOccupancyCost occCost)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (occCost.IsDeleted)
				{
					DeleteOccupancyCost(occCost.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from commercialoccupancycosts where id = " + occCost.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (occCost.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Occupancy cost record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("ConstructionClassID", occCost.ConstructionClassID);
				rsSave.Set_Fields("QualityCode", occCost.QualityCode);
				rsSave.Set_Fields("OccupancyCodeID", occCost.OccupancyCodeID);
				rsSave.Set_Fields("SqftCost", occCost.SqftCost);
				rsSave.Set_Fields("Life", occCost.Life);
				rsSave.Update();
				occCost.ID = rsSave.Get_Fields_Int32("ID");
				occCost.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void DeleteOccupancyCost(int lngId)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CommercialOccupancyCosts where id = " + FCConvert.ToString(lngId), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
