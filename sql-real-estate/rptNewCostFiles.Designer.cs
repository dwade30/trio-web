﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewCostFiles.
	/// </summary>
	partial class rptNewCostFiles
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewCostFiles));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUnit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBase = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtShortDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBase = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShortDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCode,
				this.txtDescription,
				this.txtShortDescription,
				this.txtUnit,
				this.txtBase
			});
			this.Detail.Height = 0.25F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.lblUnit,
				this.lblBase,
				this.Label5,
				this.txtPage,
				this.lblTitle,
				this.txtMuni,
				this.txtTime,
				this.txtDate
			});
			this.PageHeader.Height = 0.8125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Text = "Code";
			this.Label1.Top = 0.625F;
			this.Label1.Width = 0.4375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.5625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Description";
			this.Label2.Top = 0.625F;
			this.Label2.Width = 2.375F;
			// 
			// lblUnit
			// 
			this.lblUnit.Height = 0.1875F;
			this.lblUnit.HyperLink = null;
			this.lblUnit.Left = 5.5625F;
			this.lblUnit.Name = "lblUnit";
			this.lblUnit.Style = "font-weight: bold; text-align: right";
			this.lblUnit.Text = "Unit";
			this.lblUnit.Top = 0.625F;
			this.lblUnit.Width = 0.9375F;
			// 
			// lblBase
			// 
			this.lblBase.Height = 0.1875F;
			this.lblBase.HyperLink = null;
			this.lblBase.Left = 6.5625F;
			this.lblBase.Name = "lblBase";
			this.lblBase.Style = "font-weight: bold; text-align: right";
			this.lblBase.Text = "Base";
			this.lblBase.Top = 0.625F;
			this.lblBase.Width = 0.9375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold";
			this.Label5.Text = "Short Description";
			this.Label5.Top = 0.625F;
			this.Label5.Width = 1.1875F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = null;
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.0625F;
			this.txtMuni.Width = 2.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.375F;
			// 
			// txtCode
			// 
			this.txtCode.Height = 0.1875F;
			this.txtCode.Left = 0F;
			this.txtCode.Name = "txtCode";
			this.txtCode.Style = "text-align: right";
			this.txtCode.Text = null;
			this.txtCode.Top = 0.0625F;
			this.txtCode.Width = 0.5F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 0.5625F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "text-align: left";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0.0625F;
			this.txtDescription.Width = 3.4375F;
			// 
			// txtShortDescription
			// 
			this.txtShortDescription.Height = 0.1875F;
			this.txtShortDescription.Left = 4.0625F;
			this.txtShortDescription.Name = "txtShortDescription";
			this.txtShortDescription.Style = "text-align: left";
			this.txtShortDescription.Text = null;
			this.txtShortDescription.Top = 0.0625F;
			this.txtShortDescription.Width = 1.4375F;
			// 
			// txtUnit
			// 
			this.txtUnit.Height = 0.1875F;
			this.txtUnit.Left = 5.5625F;
			this.txtUnit.Name = "txtUnit";
			this.txtUnit.Style = "text-align: right";
			this.txtUnit.Text = null;
			this.txtUnit.Top = 0.0625F;
			this.txtUnit.Width = 0.9375F;
			// 
			// txtBase
			// 
			this.txtBase.Height = 0.1875F;
			this.txtBase.Left = 6.5625F;
			this.txtBase.Name = "txtBase";
			this.txtBase.Style = "text-align: right";
			this.txtBase.Text = null;
			this.txtBase.Top = 0.0625F;
			this.txtBase.Width = 0.9375F;
			// 
			// rptNewCostFiles
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShortDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShortDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBase;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUnit;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBase;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
