﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmInfoExtract.
	/// </summary>
	public partial class frmInfoExtract : BaseForm
	{
		public frmInfoExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		// vbPorter upgrade warning: xAction As AbaleZipLibraryCtl.abeNotEmptyAction	OnWrite(AbaleZipLibrary.abeNotEmptyAction)
		//FC:FINAL:DDU: replace AbaleZip1 with .NET classes
		//private void AbaleZip1_DiskNotEmpty(object sender, AxAbaleZipLibrary._IAbaleZipEvents_DiskNotEmptyEvent e)
		//{
		//	e.xAction = AbaleZipLibrary.abeNotEmptyAction.anaAppend;
		//}
		//FC:FINAL:DDU: replace AbaleZip1 with .NET classes
		//private void AbaleZip1_FileStatus(object sender, AxAbaleZipLibrary._IAbaleZipEvents_FileStatusEvent e)
		//{
		//	frmWait.InstancePtr.prgProgress.Value = e.nBytesPercent;
		//	frmWait.InstancePtr.prgProgress.Refresh();
		//}
		//FC:FINAL:DDU: replace AbaleZip1 with .NET classes
		//private void AbaleZip1_InsertDisk(object sender, AxAbaleZipLibrary._IAbaleZipEvents_InsertDiskEvent e)
		//{
		//	// vbPorter upgrade warning: intResp As short, int --> As DialogResult
		//	DialogResult intResp;
		//	intResp = MessageBox.Show("Insert disk number "+FCConvert.ToString(e.lDiskNumber)+" and click OK when done.", "Insert Disk", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
		//	if (intResp==DialogResult.Cancel) {
		//		e.bDiskInserted = false;
		//		AbaleZip1.Abort = true;
		//	} else {
		//		e.bDiskInserted = true;
		//	}
		//}
		//FC:FINAL:DDU: replace AbaleZip1 with .NET classes
		//private void AbaleZip1_ProcessCompleted(object sender, AxAbaleZipLibrary._IAbaleZipEvents_ProcessCompletedEvent e)
		//{
		//	frmWait.InstancePtr.Unload();
		//	FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		//}
		//FC:FINAL:DDU: replace AbaleZip1 with .NET classes
		//private void AbaleZip1_Warning(object sender, AxAbaleZipLibrary._IAbaleZipEvents_WarningEvent e)
		//{
		//	// vbPorter upgrade warning: intReturn As short, int --> As DialogResult
		//	DialogResult intReturn;
		//	string strTemp = "";
		//	switch (e.xWarning) {
		//		case 323:
		//		{
		//			// this constant is missing apparently
		//			strTemp = "File "+e.sFilename+" not found";
		//			break;
		//		}
		//		default: {
		//			strTemp = AbaleZip1.GetErrorDescription(AbaleZipLibrary.abeValueType.avtWarning, e.xWarning);
		//			break;
		//		}
		//	} //end switch
		//	intReturn = MessageBox.Show(strTemp, "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
		//	if (intReturn==DialogResult.Cancel) AbaleZip1.Abort = true;
		//}
		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			string strReturn;
			strReturn = frmGetDirectory.InstancePtr.Init("Choose the directory to save the file in", "", true, "Save In", true, fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder);
			if (strReturn != string.Empty && strReturn != "CANCEL")
			{
				txtPath.Text = strReturn;
			}
		}

		private void frmInfoExtract_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmInfoExtract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmInfoExtract properties;
			//frmInfoExtract.FillStyle	= 0;
			//frmInfoExtract.ScaleWidth	= 5880;
			//frmInfoExtract.ScaleHeight	= 4110;
			//frmInfoExtract.LinkTopic	= "Form2";
			//frmInfoExtract.LockControls	= true;
			//frmInfoExtract.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			string strTemp;
			// strTemp = GetRegistryKey("REInfoExtractFileName", "RE")
			// txtFileName.Text = Trim(strTemp)
			strTemp = modRegistry.GetRegistryKey("REInfoExtractPath", "RE");
			txtPath.Text = Strings.Trim(strTemp);
			SetupGridTownCode();
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
		}

		private void frmInfoExtract_Resize(object sender, System.EventArgs e)
		{
			ResizeGridTownCode();
		}

		private void mnuCreateZip_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				//FileSystemObject fso = new FileSystemObject();
				string strPath = "";
				// vbPorter upgrade warning: zipreturn As abeError	OnWrite(AbaleZipLibrary.abeError)
				//AbaleZipLibrary.abeError zipreturn;
				int intTownCode = 0;
				string strMuni = "";
				if (Strings.Trim(txtPath.Text) != string.Empty)
				{
					if (!Directory.Exists(txtPath.Text))
					{
						MessageBox.Show("Directory " + txtPath.Text + " not found", "Path Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					strPath = txtPath.Text;
				}
				else
				{
					strPath = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
				}
				if (Strings.Right(strPath, 1) != "\\")
					strPath += "\\";
				strPath = strPath;
				modRegistry.SaveRegistryKey("REInfoExtractPath", strPath, "RE");
				clsRX obRX = new clsRX();
				this.Enabled = false;
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					strMuni = modGlobalConstants.Statics.MuniName;
					intTownCode = 0;
				}
				else
				{
					intTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
					strMuni = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt16(intTownCode));
				}
				if (obRX.Init(strPath + "IECoreLogic.asc", this.Modal, false, intTownCode))
				{
					// zip the file
					if (File.Exists(strPath + "IECoreLogic.zip"))
					{
						File.Delete(strPath + "IECoreLogic.zip");
					}
					//FC:FINAL:AM: rewrite code using .NET classes
					//AbaleZip1.BasePath = "";
					this.Refresh();
					//AbaleZip1.PreservePaths = false;
					//AbaleZip1.FilesToProcess = "";
					//AbaleZip1.ProcessSubfolders = false;
					//AbaleZip1.SpanMultipleDisks = AbaleZipLibrary.abeDiskSpanning.adsNever;
					//AbaleZip1.ZipFilename = strPath+"IECoreLogic.zip";
					//AbaleZip1.AddFilesToProcess(strPath+"IECoreLogic.asc");
					//frmWait.InstancePtr.Init("Zipping File", true);
					//zipreturn = AbaleZip1.Zip();
					//frmWait.InstancePtr.Unload();
					try
					{
						using (MemoryStream ms = new MemoryStream())
						using (System.IO.Compression.ZipArchive arch = new System.IO.Compression.ZipArchive(ms, System.IO.Compression.ZipArchiveMode.Create))
						{
							arch.CreateEntryFromFile(strPath + "IECoreLogic.asc", "IECoreLogic.asc");
						}
						FCUtils.Download(strPath + "IECoreLogic.zip");
					}
					catch (Exception ex)
					{
						MessageBox.Show("Zipping of extract not successful" + "\r\n" + ex.Message, "Zip Failed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					//switch (zipreturn) {
					//case 0:
					{
						MessageBox.Show("File " + strPath + "IECoreLogic.zip Created Successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
						// obRX.ShowLayout
						if (MessageBox.Show("Would you like to E-mail this file to CoreLogic?", "E-mail", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							frmEMail.InstancePtr.Init(strPath + "IECoreLogic.zip", "DFW.TAF@CoreLogic.com,mmcfadden@CoreLogic.com", strMuni + " RE Assessment File export", "E-mail of " + strMuni + " RE Assessment export file.  " + Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt"), true, true, false, true, true, showAsModalForm: true);
						}
						rptInfoExtract.InstancePtr.Init(strPath + "IECoreLogic.asc", this.Modal);
						//break;
						//default: {
						//	string strTemp = "";
						//	strTemp = "Unknown Error";
						//	if (zipreturn==AbaleZipLibrary.abeError.aerBusy)
						//	{
						//		strTemp = "Busy";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerCannotAccessArray)
						//	{
						//		strTemp = "Cannot Access Array";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerCannotUpdateAndSpan)
						//	{
						//		strTemp = "Cannot Update and Span";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerCannotUpdateSpanned)
						//	{
						//		strTemp = "Cannot Update Spanned";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerCreateTempFile)
						//	{
						//		strTemp = "Create Temp File";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerDiskNotEmptyAbort)
						//	{
						//		strTemp = "Disk Not Empty Abort";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerEmptyZipFile)
						//	{
						//		strTemp = "Empty Zip File";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerEndOfZipFile)
						//	{
						//		strTemp = "End of Zip File";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerFilesSkipped)
						//	{
						//		strTemp = "Files Skipped";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerInsertDiskAbort)
						//	{
						//		strTemp = "Insert Disk Abort";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerInternalError)
						//	{
						//		strTemp = "Internal Error";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerInvalidArrayDimensions)
						//	{
						//		strTemp = "Invalid Array Dimensions";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerInvalidArrayType)
						//	{
						//		strTemp = "Invalid Array Type";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerInvalidSfxProperty)
						//	{
						//		strTemp = "Invalid Sfx Property";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerMemory)
						//	{
						//		strTemp = "Memory Error";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerMoveTempFile)
						//	{
						//		strTemp = "Move Temp File";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerNotAZipFile)
						//	{
						//		strTemp = "Not a Zip File";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerNothingToDo)
						//	{
						//		strTemp = "Nothing to Do";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerNotLicensed)
						//	{
						//		strTemp = "Not Licensed";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerOpenZipFile)
						//	{
						//		strTemp = "Open Zip File";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerProcessStarted)
						//	{
						//		strTemp = "Process Started";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerReadSfxBinary)
						//	{
						//		strTemp = "Read Sfx Binary";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerReadZipFile)
						//	{
						//		strTemp = "Read Zip File";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerRemoveWithoutTemp)
						//	{
						//		strTemp = "Remove Without Temp";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerSeekInZipFile)
						//	{
						//		strTemp = "Seek In Zip File";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerSfxBinaryNotFound)
						//	{
						//		strTemp = "Sfx Binary Not Found";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerSplitSizeTooSmall)
						//	{
						//		strTemp = "Split Size Too Small";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerSuccess)
						//	{
						//		strTemp = "No Error";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerUninitializedArray)
						//	{
						//		strTemp = "Uninitialized Array";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerUninitializedString)
						//	{
						//		strTemp = "Uninitialized String";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerUnsupportedDataType)
						//	{
						//		strTemp = "Unsupported Data Type";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerUserAbort)
						//	{
						//		strTemp = "User Abort";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerWarnings)
						//	{
						//		strTemp = "Warnings";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerWriteTempZipFile)
						//	{
						//		strTemp = "Write Temp Zip File";
						//	}
						//	else if (zipreturn==AbaleZipLibrary.abeError.aerWriteZipFile)
						//	{
						//		strTemp = "Write Zip File";
						//	}
						//	strTemp = AbaleZip1.GetErrorDescription(AbaleZipLibrary.abeValueType.avtError, zipreturn);
						//	MessageBox.Show("Zipping of extract not successful"+"\r\n"+strTemp, "Zip Failed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						//	break;
						//}
						//} //end switch
					}
				}
				this.Enabled = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuCreateZip_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuEmail_Click(object sender, System.EventArgs e)
		{
			string strPath = "";
			//FileSystemObject fso = new FileSystemObject();
			if (Strings.Trim(txtPath.Text) != string.Empty)
			{
				if (!Directory.Exists(txtPath.Text))
				{
					MessageBox.Show("Directory " + txtPath.Text + " not found", "Path Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				strPath = txtPath.Text;
			}
			else
			{
				strPath = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
			}
			if (Strings.Right(strPath, 1) != "\\")
				strPath += "\\";
			if (File.Exists(strPath + "IECoreLogic.zip"))
			{
				string strMuni = "";
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					strMuni = modGlobalConstants.Statics.MuniName;
				}
				else
				{
					strMuni = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0)))));
				}
				frmEMail.InstancePtr.Init(strPath + "IECoreLogic.zip", "DFW.TAF@CoreLogic.com,mmcfadden@CoreLogic.com", strMuni + " Assessment File Export", "E-mail of " + strMuni + " RE assessment export file.  " + Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt"), true, true, false, true, showAsModalForm: true);
			}
			else
			{
				MessageBox.Show("File " + strPath + "IECoreLogic.zip not found", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuFileFormat_Click(object sender, System.EventArgs e)
		{
			clsRX obRX = new clsRX();
			obRX.ShowLayout(this.Modal);
		}

		private void gridTownCode_ComboCloseUp(object sender, EventArgs e)
		{
			gridTownCode.EndEdit();
			//Application.DoEvents();
			cmdBrowse.Focus();
			//Application.DoEvents();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				//FileSystemObject fso = new FileSystemObject();
				string strPath = "";
				int intTownCode = 0;
				// If Trim(txtFileName.Text) = vbNullString Then
				// MsgBox "You must specify a name for the file", vbExclamation, "No Filename"
				// Exit Sub
				// End If
				if (Strings.Trim(txtPath.Text) != string.Empty)
				{
					if (!Directory.Exists(txtPath.Text))
					{
						MessageBox.Show("Directory " + txtPath.Text + " not found", "Path Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					strPath = txtPath.Text;
				}
				else
				{
					strPath = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
				}
				if (Strings.Right(strPath, 1) != "\\")
					strPath += "\\";
				// If UCase(Right("   " & txtFileName.Text, 3)) = "VB1" Then
				// MsgBox "Cannot overwrite vb1 files", vbExclamation, "Bad Filename"
				// Exit Sub
				// End If
				// strPath = strPath & Trim(txtFileName.Text)
				// Call SaveRegistryKey("REInfoExtractFileName", Trim(txtFileName.Text), "RE")
				modRegistry.SaveRegistryKey("REInfoExtractPath", strPath, "RE");
				strPath += "IECoreLogic.asc";
				clsRX obRX = new clsRX();
				this.Enabled = false;
				string strMuni = "";
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					intTownCode = 0;
					strMuni = modGlobalConstants.Statics.MuniName;
				}
				else
				{
					intTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
					strMuni = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt16(intTownCode));
				}
				modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = intTownCode;
				if (obRX.Init(strPath, this.Modal, true, intTownCode))
				{
					if (MessageBox.Show("Would you like to E-mail this file to CoreLogic?", "E-mail", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						frmEMail.InstancePtr.Init(strPath, "DFW.TAF@CoreLogic.com,mmcfadden@CoreLogic.com", strMuni + " Assessment File Export", "E-mail of " + strMuni + " RE assessment export file.  " + Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt"), true, true, false, true, showAsModalForm: true);
						// Call frmEMail.Init(strPath, "coreygray_1@hotmail.com", MuniName & " RE Export File", "Email of " & MuniName & " RE export file.  " & Format(Now, "MM/dd/yyyy HH:MM AMPM"), True, True, , True, True)
					}
				}
				this.Enabled = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				this.Enabled = true;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "in mnuSaveContinue_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from tblTranCode where code > 0 order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = true;
			}
			else
			{
				gridTownCode.Rows = 1;
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = false;
			}
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}
	}
}
