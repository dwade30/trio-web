﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptRevocableLivingTrust.
	/// </summary>
	partial class rptRevocableLivingTrust
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRevocableLivingTrust));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMaplot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaplot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAcct,
				this.txtName,
				this.txtLocation,
				this.txtMaplot
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label11,
				this.txtCount
			});
			this.ReportFooter.Height = 0.3333333F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCaption,
				this.Label6,
				this.Label8,
				this.Label9,
				this.Label10,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage
			});
			this.PageHeader.Height = 0.78125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.21875F;
			this.txtCaption.Left = 1.625F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.txtCaption.Text = "Revocable Living Trusts";
			this.txtCaption.Top = 0F;
			this.txtCaption.Width = 4.75F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.Label6.Text = "Account";
			this.Label6.Top = 0.5625F;
			this.Label6.Width = 0.625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.6875F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.Label8.Text = "Name";
			this.Label8.Top = 0.5625F;
			this.Label8.Width = 0.625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.Label9.Text = "Location";
			this.Label9.Top = 0.5625F;
			this.Label9.Width = 1.3125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.Label10.Text = "Map/Lot";
			this.Label10.Top = 0.5625F;
			this.Label10.Width = 1.125F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 2.75F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.177083F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.260417F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.1875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.25F;
			// 
			// txtAcct
			// 
			this.txtAcct.Height = 0.19F;
			this.txtAcct.Left = 0F;
			this.txtAcct.Name = "txtAcct";
			this.txtAcct.Style = "text-align: right";
			this.txtAcct.Text = null;
			this.txtAcct.Top = 0F;
			this.txtAcct.Width = 0.59375F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.6145833F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 2.666667F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 3.375F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0F;
			this.txtLocation.Width = 2.416667F;
			// 
			// txtMaplot
			// 
			this.txtMaplot.Height = 0.19F;
			this.txtMaplot.Left = 5.875F;
			this.txtMaplot.Name = "txtMaplot";
			this.txtMaplot.Text = null;
			this.txtMaplot.Top = 0F;
			this.txtMaplot.Width = 1.541667F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.125F;
			this.Label11.MultiLine = false;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.Label11.Text = "Count";
			this.Label11.Top = 0.1666667F;
			this.Label11.Width = 0.5F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 0.625F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtCount.Text = null;
			this.txtCount.Top = 0.1666667F;
			this.txtCount.Width = 0.6458333F;
			// 
			// rptRevocableLivingTrust
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMaplot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMaplot;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
