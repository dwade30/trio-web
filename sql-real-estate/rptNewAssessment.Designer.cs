﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewAssessment.
	/// </summary>
	partial class rptNewAssessment
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewAssessment));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtmuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtassessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtdate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtratio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtorder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtpage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaccount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtincdec1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtland1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtland2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtbldg1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtbldg2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtmaplot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtprevland = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtprevbldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtnewland = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtnewbldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtincdec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtsign = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtcard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOV = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDotMatrix = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevBldgGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevLandGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewLandGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewBldgGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCountGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAcctCountGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotaltotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtprevtotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtprevbldgtotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtnltotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtnbtotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotaltotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtmuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtassessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtratio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtorder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtincdec1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmaplot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtprevland)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtprevbldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnewland)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnewbldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtincdec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsign)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtcard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOV)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDotMatrix)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevBldgGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevLandGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewLandGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewBldgGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcctCountGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotaltotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtprevtotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtprevbldgtotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnltotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnbtotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotaltotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtmaplot,
				this.txtname,
				this.txtaccount,
				this.txtprevland,
				this.txtprevbldg,
				this.txtnewland,
				this.txtnewbldg,
				this.txtincdec,
				this.txttotal,
				this.txtsign,
				this.txtcard,
				this.txtOV,
				this.txtDotMatrix
			});
			this.Detail.Height = 0.3333333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtmuniname,
				this.txtassessment,
				this.txtdate,
				this.txtratio,
				this.txtorder,
				this.txtpage,
				this.txtaccount1,
				this.Field4,
				this.Field6,
				this.Field7,
				this.txtincdec1,
				this.txtland1,
				this.txtland2,
				this.txtbldg1,
				this.txtbldg2,
				this.Field8,
				this.Field9,
				this.txtTime
			});
			this.PageHeader.Height = 1.09375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			//
			this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txttotaltotal1,
				this.txtprevtotal,
				this.txtprevbldgtotal,
				this.txtnltotal,
				this.txtnbtotal,
				this.txttotaltotal
			});
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field10,
				this.txtPrevBldgGroup,
				this.txtPrevLandGroup,
				this.txtNewLandGroup,
				this.txtNewBldgGroup,
				this.txtTotalGroup,
				this.txtCountGroup,
				this.txtGroup,
				this.Field11,
				this.Field12,
				this.Field13,
				this.txtAcctCountGroup
			});
			this.GroupFooter1.Height = 0.4270833F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtmuniname
			// 
			this.txtmuniname.Height = 0.1875F;
			this.txtmuniname.Left = 0F;
			this.txtmuniname.Name = "txtmuniname";
			this.txtmuniname.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtmuniname.Text = "Muniname";
			this.txtmuniname.Top = 0F;
			this.txtmuniname.Width = 1.6875F;
			// 
			// txtassessment
			// 
			this.txtassessment.Height = 0.1875F;
			this.txtassessment.Left = 2.0625F;
			this.txtassessment.Name = "txtassessment";
			this.txtassessment.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold";
			this.txtassessment.Text = "   A S S E S S M E N T    R E P O R T";
			this.txtassessment.Top = 0F;
			this.txtassessment.Width = 3.4375F;
			// 
			// txtdate
			// 
			this.txtdate.Height = 0.1875F;
			this.txtdate.Left = 6.1875F;
			this.txtdate.Name = "txtdate";
			this.txtdate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtdate.Text = "DATE";
			this.txtdate.Top = 0F;
			this.txtdate.Width = 1.5F;
			// 
			// txtratio
			// 
			this.txtratio.Height = 0.1875F;
			this.txtratio.Left = 1.75F;
			this.txtratio.Name = "txtratio";
			this.txtratio.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.txtratio.Text = "Ratio";
			this.txtratio.Top = 0.375F;
			this.txtratio.Width = 4.25F;
			// 
			// txtorder
			// 
			this.txtorder.Height = 0.1875F;
			this.txtorder.Left = 1.75F;
			this.txtorder.Name = "txtorder";
			this.txtorder.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.txtorder.Text = "ordered by";
			this.txtorder.Top = 0.1875F;
			this.txtorder.Width = 4.25F;
			// 
			// txtpage
			// 
			this.txtpage.Height = 0.1875F;
			this.txtpage.Left = 6.5625F;
			this.txtpage.Name = "txtpage";
			this.txtpage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtpage.Text = "page";
			this.txtpage.Top = 0.1875F;
			this.txtpage.Width = 1.125F;
			// 
			// txtaccount1
			// 
			this.txtaccount1.Height = 0.1875F;
			this.txtaccount1.Left = 0F;
			this.txtaccount1.Name = "txtaccount1";
			this.txtaccount1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.txtaccount1.Text = "Account";
			this.txtaccount1.Top = 0.90625F;
			this.txtaccount1.Width = 0.625F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 0F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field4.Text = "Map/Lot,Loc, Name";
			this.Field4.Top = 0.71875F;
			this.Field4.Width = 1.5625F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 1.5F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field6.Text = "- - - PREVIOUS - - -";
			this.Field6.Top = 0.71875F;
			this.Field6.Width = 1.9375F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1875F;
			this.Field7.Left = 3.75F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field7.Text = "- - NEW VALUATION - -";
			this.Field7.Top = 0.71875F;
			this.Field7.Width = 2.0625F;
			// 
			// txtincdec1
			// 
			this.txtincdec1.Height = 0.19F;
			this.txtincdec1.Left = 7F;
			this.txtincdec1.Name = "txtincdec1";
			this.txtincdec1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.txtincdec1.Text = "INC/DEC";
			this.txtincdec1.Top = 0.90625F;
			this.txtincdec1.Width = 0.6875F;
			// 
			// txtland1
			// 
			this.txtland1.Height = 0.1875F;
			this.txtland1.Left = 1.5625F;
			this.txtland1.Name = "txtland1";
			this.txtland1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.txtland1.Text = "-LAND-";
			this.txtland1.Top = 0.90625F;
			this.txtland1.Width = 0.6875F;
			// 
			// txtland2
			// 
			this.txtland2.Height = 0.19F;
			this.txtland2.Left = 3.8125F;
			this.txtland2.Name = "txtland2";
			this.txtland2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.txtland2.Text = "-LAND-";
			this.txtland2.Top = 0.90625F;
			this.txtland2.Width = 0.75F;
			// 
			// txtbldg1
			// 
			this.txtbldg1.Height = 0.19F;
			this.txtbldg1.Left = 2.6875F;
			this.txtbldg1.Name = "txtbldg1";
			this.txtbldg1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.txtbldg1.Text = "BUILDINGS";
			this.txtbldg1.Top = 0.90625F;
			this.txtbldg1.Width = 0.875F;
			// 
			// txtbldg2
			// 
			this.txtbldg2.Height = 0.19F;
			this.txtbldg2.Left = 5F;
			this.txtbldg2.Name = "txtbldg2";
			this.txtbldg2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.txtbldg2.Text = "BUILDINGS";
			this.txtbldg2.Top = 0.90625F;
			this.txtbldg2.Width = 0.9375F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.19F;
			this.Field8.Left = 6.4375F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field8.Text = "TOTAL";
			this.Field8.Top = 0.90625F;
			this.Field8.Width = 0.5625F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1875F;
			this.Field9.Left = 0.625F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field9.Text = "Card";
			this.Field9.Top = 0.90625F;
			this.Field9.Width = 0.375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = "Muniname";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.6875F;
			// 
			// txtmaplot
			// 
			this.txtmaplot.Height = 0.19F;
			this.txtmaplot.Left = 0F;
			this.txtmaplot.Name = "txtmaplot";
			this.txtmaplot.Style = "font-family: \'Courier\'; font-size: 8pt";
			this.txtmaplot.Text = "No records found in this Range";
			this.txtmaplot.Top = 0F;
			this.txtmaplot.Width = 1.833333F;
			// 
			// txtname
			// 
			this.txtname.Height = 0.19F;
			this.txtname.Left = 1.916667F;
			this.txtname.Name = "txtname";
			this.txtname.Style = "font-family: \'Courier\'; font-size: 8pt";
			this.txtname.Text = "name";
			this.txtname.Top = 0F;
			this.txtname.Width = 3.583333F;
			// 
			// txtaccount
			// 
			this.txtaccount.CanGrow = false;
			this.txtaccount.Height = 0.19F;
			this.txtaccount.Left = 0F;
			this.txtaccount.MultiLine = false;
			this.txtaccount.Name = "txtaccount";
			this.txtaccount.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtaccount.Text = "00000000";
			this.txtaccount.Top = 0.1666667F;
			this.txtaccount.Width = 0.5833333F;
			// 
			// txtprevland
			// 
			this.txtprevland.CanGrow = false;
			this.txtprevland.Height = 0.19F;
			this.txtprevland.Left = 1.25F;
			this.txtprevland.MultiLine = false;
			this.txtprevland.Name = "txtprevland";
			this.txtprevland.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtprevland.Text = "000000000000000";
			this.txtprevland.Top = 0.1666667F;
			this.txtprevland.Width = 1F;
			// 
			// txtprevbldg
			// 
			this.txtprevbldg.CanGrow = false;
			this.txtprevbldg.Height = 0.19F;
			this.txtprevbldg.Left = 2.333333F;
			this.txtprevbldg.MultiLine = false;
			this.txtprevbldg.Name = "txtprevbldg";
			this.txtprevbldg.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtprevbldg.Text = "00000000000000";
			this.txtprevbldg.Top = 0.1666667F;
			this.txtprevbldg.Width = 1.083333F;
			// 
			// txtnewland
			// 
			this.txtnewland.CanGrow = false;
			this.txtnewland.Height = 0.19F;
			this.txtnewland.Left = 3.5F;
			this.txtnewland.MultiLine = false;
			this.txtnewland.Name = "txtnewland";
			this.txtnewland.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtnewland.Text = "0000000000";
			this.txtnewland.Top = 0.1666667F;
			this.txtnewland.Width = 1F;
			// 
			// txtnewbldg
			// 
			this.txtnewbldg.CanGrow = false;
			this.txtnewbldg.Height = 0.19F;
			this.txtnewbldg.Left = 4.583333F;
			this.txtnewbldg.MultiLine = false;
			this.txtnewbldg.Name = "txtnewbldg";
			this.txtnewbldg.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtnewbldg.Text = "000000000000000";
			this.txtnewbldg.Top = 0.1666667F;
			this.txtnewbldg.Width = 1.25F;
			// 
			// txtincdec
			// 
			this.txtincdec.CanGrow = false;
			this.txtincdec.Height = 0.19F;
			this.txtincdec.Left = 7.166667F;
			this.txtincdec.MultiLine = false;
			this.txtincdec.Name = "txtincdec";
			this.txtincdec.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtincdec.Text = "%+-";
			this.txtincdec.Top = 0.1666667F;
			this.txtincdec.Width = 0.3333333F;
			// 
			// txttotal
			// 
			this.txttotal.CanGrow = false;
			this.txttotal.Height = 0.19F;
			this.txttotal.Left = 5.916667F;
			this.txttotal.MultiLine = false;
			this.txttotal.Name = "txttotal";
			this.txttotal.Style = "font-family: \'Courier New\'; text-align: right";
			this.txttotal.Text = "000000000000000";
			this.txttotal.Top = 0.1666667F;
			this.txttotal.Width = 1.25F;
			// 
			// txtsign
			// 
			this.txtsign.CanGrow = false;
			this.txtsign.Height = 0.19F;
			this.txtsign.Left = 7.5F;
			this.txtsign.MultiLine = false;
			this.txtsign.Name = "txtsign";
			this.txtsign.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtsign.Text = "+";
			this.txtsign.Top = 0.1666667F;
			this.txtsign.Width = 0.1666667F;
			// 
			// txtcard
			// 
			this.txtcard.CanGrow = false;
			this.txtcard.Height = 0.19F;
			this.txtcard.Left = 0.6875F;
			this.txtcard.MultiLine = false;
			this.txtcard.Name = "txtcard";
			this.txtcard.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtcard.Text = "Card";
			this.txtcard.Top = 0.1666667F;
			this.txtcard.Width = 0.3125F;
			// 
			// txtOV
			// 
			this.txtOV.CanShrink = true;
			this.txtOV.Height = 0.19F;
			this.txtOV.Left = 7.25F;
			this.txtOV.Name = "txtOV";
			this.txtOV.Style = "font-family: \'Courier\'; text-align: right; ddo-char-set: 1";
			this.txtOV.Text = "OV";
			this.txtOV.Top = 0F;
			this.txtOV.Width = 0.3333333F;
			// 
			// txtDotMatrix
			// 
			this.txtDotMatrix.CanGrow = false;
			this.txtDotMatrix.Height = 0.19F;
			this.txtDotMatrix.Left = 0F;
			this.txtDotMatrix.Name = "txtDotMatrix";
			this.txtDotMatrix.Style = "ddo-char-set: 1";
			this.txtDotMatrix.Text = "Field10";
			this.txtDotMatrix.Top = 0.4166667F;
			this.txtDotMatrix.Visible = false;
			this.txtDotMatrix.Width = 8.5F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 0F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'";
			this.Field10.Text = "Subtotal:";
			this.Field10.Top = 0.1875F;
			this.Field10.Width = 0.8125F;
			// 
			// txtPrevBldgGroup
			// 
			this.txtPrevBldgGroup.DataField = "fldPrevBldg";
			this.txtPrevBldgGroup.Height = 0.19F;
			this.txtPrevBldgGroup.Left = 2.3125F;
			this.txtPrevBldgGroup.Name = "txtPrevBldgGroup";
			this.txtPrevBldgGroup.Style = "text-align: right";
			this.txtPrevBldgGroup.Text = "prevbldg";
			this.txtPrevBldgGroup.Top = 0.1875F;
			this.txtPrevBldgGroup.Width = 1.125F;
			// 
			// txtPrevLandGroup
			// 
			this.txtPrevLandGroup.DataField = "fldPrevLand";
			this.txtPrevLandGroup.Height = 0.19F;
			this.txtPrevLandGroup.Left = 1.1875F;
			this.txtPrevLandGroup.Name = "txtPrevLandGroup";
			this.txtPrevLandGroup.Style = "text-align: right";
			this.txtPrevLandGroup.Text = "prevland";
			this.txtPrevLandGroup.Top = 0.1875F;
			this.txtPrevLandGroup.Width = 1.0625F;
			// 
			// txtNewLandGroup
			// 
			this.txtNewLandGroup.DataField = "fldNewLand";
			this.txtNewLandGroup.Height = 0.19F;
			this.txtNewLandGroup.Left = 3.5F;
			this.txtNewLandGroup.MultiLine = false;
			this.txtNewLandGroup.Name = "txtNewLandGroup";
			this.txtNewLandGroup.Style = "text-align: right";
			this.txtNewLandGroup.Text = "prevbldg";
			this.txtNewLandGroup.Top = 0.1875F;
			this.txtNewLandGroup.Width = 1F;
			// 
			// txtNewBldgGroup
			// 
			this.txtNewBldgGroup.DataField = "fldNewBldg";
			this.txtNewBldgGroup.Height = 0.19F;
			this.txtNewBldgGroup.Left = 4.5625F;
			this.txtNewBldgGroup.MultiLine = false;
			this.txtNewBldgGroup.Name = "txtNewBldgGroup";
			this.txtNewBldgGroup.Style = "text-align: right";
			this.txtNewBldgGroup.Text = "prevbldg";
			this.txtNewBldgGroup.Top = 0.1875F;
			this.txtNewBldgGroup.Width = 1.25F;
			// 
			// txtTotalGroup
			// 
			this.txtTotalGroup.DataField = "fldNewTotal";
			this.txtTotalGroup.Height = 0.19F;
			this.txtTotalGroup.Left = 5.9375F;
			this.txtTotalGroup.Name = "txtTotalGroup";
			this.txtTotalGroup.Style = "text-align: right";
			this.txtTotalGroup.Text = "prevbldg";
			this.txtTotalGroup.Top = 0.1875F;
			this.txtTotalGroup.Width = 1.25F;
			// 
			// txtCountGroup
			// 
			this.txtCountGroup.Height = 0.19F;
			this.txtCountGroup.Left = 3.875F;
			this.txtCountGroup.Name = "txtCountGroup";
			this.txtCountGroup.Style = "text-align: right";
			this.txtCountGroup.Text = "prevland";
			this.txtCountGroup.Top = 0.03125F;
			this.txtCountGroup.Width = 0.875F;
			// 
			// txtGroup
			// 
			this.txtGroup.Height = 0.19F;
			this.txtGroup.Left = 0.6875F;
			this.txtGroup.Name = "txtGroup";
			this.txtGroup.Style = "text-align: left";
			this.txtGroup.Text = "prevland";
			this.txtGroup.Top = 0.03125F;
			this.txtGroup.Width = 0.625F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.19F;
			this.Field11.Left = 0F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; text-align: left";
			this.Field11.Text = "Group:";
			this.Field11.Top = 0.03125F;
			this.Field11.Width = 0.6875F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.19F;
			this.Field12.Left = 3.125F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "text-align: left";
			this.Field12.Text = "Cards:";
			this.Field12.Top = 0.03125F;
			this.Field12.Width = 0.75F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.19F;
			this.Field13.Left = 1.4375F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "text-align: left";
			this.Field13.Text = "Accounts:";
			this.Field13.Top = 0.03125F;
			this.Field13.Width = 0.875F;
			// 
			// txtAcctCountGroup
			// 
			this.txtAcctCountGroup.Height = 0.19F;
			this.txtAcctCountGroup.Left = 2.3125F;
			this.txtAcctCountGroup.Name = "txtAcctCountGroup";
			this.txtAcctCountGroup.Style = "text-align: right";
			this.txtAcctCountGroup.Text = "prevland";
			this.txtAcctCountGroup.Top = 0.03125F;
			this.txtAcctCountGroup.Width = 0.6875F;
			// 
			// txttotaltotal1
			// 
			this.txttotaltotal1.Height = 0.1875F;
			this.txttotaltotal1.Left = 0F;
			this.txttotaltotal1.Name = "txttotaltotal1";
			this.txttotaltotal1.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txttotaltotal1.Text = "TOTAL";
			this.txttotaltotal1.Top = 0.0625F;
			this.txttotaltotal1.Visible = false;
			this.txttotaltotal1.Width = 0.8125F;
			// 
			// txtprevtotal
			// 
			this.txtprevtotal.Height = 0.19F;
			this.txtprevtotal.Left = 1.083333F;
			this.txtprevtotal.Name = "txtprevtotal";
			this.txtprevtotal.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right";
			this.txtprevtotal.Text = "prevland";
			this.txtprevtotal.Top = 0.08333334F;
			this.txtprevtotal.Visible = false;
			this.txtprevtotal.Width = 1.166667F;
			// 
			// txtprevbldgtotal
			// 
			this.txtprevbldgtotal.Height = 0.19F;
			this.txtprevbldgtotal.Left = 2.333333F;
			this.txtprevbldgtotal.Name = "txtprevbldgtotal";
			this.txtprevbldgtotal.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right";
			this.txtprevbldgtotal.Text = "prevbldg";
			this.txtprevbldgtotal.Top = 0.08333334F;
			this.txtprevbldgtotal.Visible = false;
			this.txtprevbldgtotal.Width = 1.083333F;
			// 
			// txtnltotal
			// 
			this.txtnltotal.Height = 0.19F;
			this.txtnltotal.Left = 3.5F;
			this.txtnltotal.Name = "txtnltotal";
			this.txtnltotal.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right";
			this.txtnltotal.Text = "new land";
			this.txtnltotal.Top = 0.08333334F;
			this.txtnltotal.Visible = false;
			this.txtnltotal.Width = 1F;
			// 
			// txtnbtotal
			// 
			this.txtnbtotal.CanShrink = true;
			this.txtnbtotal.Height = 0.19F;
			this.txtnbtotal.Left = 4.583333F;
			this.txtnbtotal.Name = "txtnbtotal";
			this.txtnbtotal.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right";
			this.txtnbtotal.Text = "Field10";
			this.txtnbtotal.Top = 0.08333334F;
			this.txtnbtotal.Visible = false;
			this.txtnbtotal.Width = 1.25F;
			// 
			// txttotaltotal
			// 
			this.txttotaltotal.Height = 0.19F;
			this.txttotaltotal.Left = 5.916667F;
			this.txttotaltotal.Name = "txttotaltotal";
			this.txttotaltotal.Style = "font-family: \'Courier\'; font-size: 8pt; text-align: right";
			this.txttotaltotal.Text = "total";
			this.txttotaltotal.Top = 0.08333334F;
			this.txttotaltotal.Visible = false;
			this.txttotaltotal.Width = 1.25F;
			// 
			// rptNewAssessment
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            this.ReportEndedAndCanceled += new System.EventHandler(this.RptNewAssessment_ReportEndedAndCanceled);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.3472222F;
			this.PageSettings.Margins.Right = 0.3472222F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.75F;
			this.Script = "\r\n\r\n";
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtmuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtassessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtratio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtorder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtincdec1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmaplot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtprevland)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtprevbldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnewland)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnewbldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtincdec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtsign)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtcard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOV)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDotMatrix)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevBldgGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevLandGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewLandGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewBldgGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcctCountGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotaltotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtprevtotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtprevbldgtotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnltotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtnbtotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotaltotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmaplot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtprevland;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtprevbldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtnewland;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtnewbldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtincdec;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtsign;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtcard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOV;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDotMatrix;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtassessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtdate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtratio;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtorder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtpage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaccount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtincdec1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtland1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtland2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtbldg1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtbldg2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotaltotal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtprevtotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtprevbldgtotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtnltotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtnbtotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotaltotal;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevBldgGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevLandGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewLandGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewBldgGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcctCountGroup;
	}
}
