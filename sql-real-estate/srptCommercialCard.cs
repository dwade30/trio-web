﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptCommercialCard.
	/// </summary>
	public partial class srptCommercialCard : FCSectionReport
	{
		public static srptCommercialCard InstancePtr
		{
			get
			{
				return (srptCommercialCard)Sys.GetInstance(typeof(srptCommercialCard));
			}
		}

		protected srptCommercialCard _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				
				clsComm?.Dispose();
				clsCommCost?.Dispose();
                clsCost = null;
                clsComm = null;
                clsCommCost = null;
            }
			base.Dispose(disposing);
		}

		public srptCommercialCard()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptCommercialCard	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolEmpty;
		clsDRWrapper clsComm = new clsDRWrapper();
		int lngAcct;
		int intCard;
		clsDRWrapper clsCommCost = new clsDRWrapper();
		clsDRWrapper clsCost = rptpdfPropertyCard.InstancePtr == null ? rptPropertyCard.InstancePtr.clsCost : rptpdfPropertyCard.InstancePtr.clsCost;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (this.ParentReport as srptpdfPropertyCard2 != null)
			{
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields_Int32("rsaccount"))));
				intCard = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields_Int32("rscard"))));
			}
			else
			{
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields_Int32("rsaccount"))));
				intCard = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields_Int32("rscard"))));
			}
			Image1.Image = MDIParent.InstancePtr.ImageList1.Images[9 - 1];
			boolEmpty = false;
			clsComm.OpenRecordset("select * from commercial where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
			clsCommCost.OpenRecordset("select * from commercialcost where crecordnumber <= 360 order by crecordnumber", modGlobalVariables.strREDatabase);
			if (clsComm.EndOfFile())
				boolEmpty = true;
			FillFromCostFiles();
		}

		private void FillFromCostFiles()
		{
			string strTemp = "";
			// vbPorter upgrade warning: intTemp As short --> As int	OnWriteFCConvert.ToDouble(
			int intTemp = 0;
			clsCost.MoveFirst();
			if (clsCost.FindNextRecord("crecordnumber", 1701))
			{
				while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1710)
				{
					strTemp = clsCost.Get_Fields_String("csdesc");
					strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
					intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1700);
					strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
					if (strTemp.Length > 10)
					{
						strTemp = Strings.Mid(strTemp, 1, 10);
					}
					(Detail.Controls["txtEntranceCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
					clsCost.MoveNext();
				}
			}
			if (clsCost.FindNextRecord("crecordnumber", 1711))
			{
				while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1720)
				{
					strTemp = clsCost.Get_Fields_String("csdesc");
					strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
					intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1710);
					strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
					if (strTemp.Length > 10)
					{
						strTemp = Strings.Mid(strTemp, 1, 10);
					}
					(Detail.Controls["txtInformationCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
					clsCost.MoveNext();
				}
			}
			if (clsCommCost.FindNextRecord("crecordnumber", 161))
			{
				while (Conversion.Val(clsCommCost.Get_Fields_Int32("crecordnumber")) < 177)
				{
					strTemp = FCConvert.ToString(clsCommCost.Get_Fields_String("csdesc"));
					strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
					intTemp = FCConvert.ToInt32(Conversion.Val(clsCommCost.Get_Fields_Int32("crecordnumber")) - 150);
					strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
					if (strTemp.Length > 10)
					{
						strTemp = Strings.Mid(strTemp, 1, 10);
					}
					(Detail.Controls["txtHeatCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
					clsCommCost.MoveNext();
				}
			}
			if (clsCommCost.FindNextRecord("crecordnumber", 341))
			{
				while (Conversion.Val(clsCommCost.Get_Fields_Int32("crecordnumber")) < 350)
				{
					strTemp = FCConvert.ToString(clsCommCost.Get_Fields_String("csdesc"));
					strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
					intTemp = FCConvert.ToInt32(Conversion.Val(clsCommCost.Get_Fields_Int32("crecordnumber")) - 340);
					strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
					if (strTemp.Length > 10)
					{
						strTemp = Strings.Mid(strTemp, 1, 10);
					}
					(Detail.Controls["txtExteriorCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
					clsCommCost.MoveNext();
				}
			}
			if (clsCommCost.FindNextRecord("crecordnumber", 351))
			{
				while (Conversion.Val(clsCommCost.Get_Fields_Int32("crecordnumber")) < 360)
				{
					strTemp = FCConvert.ToString(clsCommCost.Get_Fields_String("cldesc"));
					strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
					intTemp = FCConvert.ToInt32(Conversion.Val(clsCommCost.Get_Fields_Int32("crecordnumber")) - 350);
					strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
					if (strTemp.Length > 11)
					{
						strTemp = Strings.Mid(strTemp, 1, 11);
					}
					(Detail.Controls["txtConditionCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
					clsCommCost.MoveNext();
				}
			}
			txtClassCost1.Text = "1.Steel";
			txtClassCost2.Text = "2.Rein Conc";
			txtClassCost3.Text = "3.Masonry";
			txtClassCost4.Text = "4.Wood Frm";
			txtClassCost5.Text = "5.Rigid Frm";
			txtQualityCost1.Text = "1.Low Cost";
			txtQualityCost2.Text = "2.Average";
			txtQualityCost3.Text = "3.Good";
			txtQualityCost4.Text = "4.Excellent";
		}
		// vbPorter upgrade warning: intCode As short	OnWrite(string)
		private string GetDesc_26(int lngCostNumber, short intCode, bool boolLong, short intLimit = 50)
		{
			return GetDesc(ref lngCostNumber, ref intCode, ref boolLong, intLimit);
		}

		private string GetDesc(ref int lngCostNumber, ref short intCode, ref bool boolLong, short intLimit = 50)
		{
			string GetDesc = "";
			string strTemp = "";
			if (intCode == 0)
			{
				GetDesc = "0";
				return GetDesc;
			}
			if (FCConvert.ToInt32(clsCommCost.Get_Fields_Int32("crecordnumber")) != lngCostNumber + intCode)
			{
				if (clsCommCost.FindNextRecord("crecordnumber", lngCostNumber + intCode))
				{
					if (boolLong)
					{
						strTemp = Strings.Trim(FCConvert.ToString(clsCommCost.Get_Fields_String("cldesc")));
					}
					else
					{
						strTemp = Strings.Trim(FCConvert.ToString(clsCommCost.Get_Fields_String("csdesc")));
					}
					strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
				}
				else
				{
					strTemp = "";
				}
			}
			else
			{
				if (boolLong)
				{
					strTemp = Strings.Trim(FCConvert.ToString(clsCommCost.Get_Fields_String("cldesc")));
				}
				else
				{
					strTemp = Strings.Trim(FCConvert.ToString(clsCommCost.Get_Fields_String("csdesc")));
				}
				strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
			}
			strTemp = Strings.Trim(strTemp);
			strTemp = FCConvert.ToString(intCode) + " " + strTemp;
			if (strTemp.Length > intLimit)
			{
				strTemp = Strings.Mid(strTemp, 1, intLimit);
			}
			GetDesc = strTemp;
			return GetDesc;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!boolEmpty)
			{
				if (Conversion.Val(clsComm.Get_Fields_Int32("occ1")) > 0)
				{
					FillBldg_2(1);
				}
				else
				{
					ClearBldg_2(1);
				}
				if (Conversion.Val(clsComm.Get_Fields_Int32("occ2")) > 0)
				{
					FillBldg_2(2);
				}
				else
				{
					ClearBldg_2(2);
				}
			}
			else
			{
				ClearBldg_2(1);
				ClearBldg_2(2);
				txtEconomic.Text = "";
				txtEntrance.Text = "";
				txtInformation.Text = "";
				txtDateInspected.Text = "";
			}
		}

		private void ClearBldg_2(short intBldg)
		{
			ClearBldg(ref intBldg);
		}

		private void ClearBldg(ref short intBldg)
		{
			(Detail.Controls["txtOccupancy" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtClass" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtQuality" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtGradeFactor" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtExterior" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtStories" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtHeight" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtArea" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtPerimeter" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtHeatCool" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtYearBuilt" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtRemodeled" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtCondition" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtPhysical" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			(Detail.Controls["txtFunctional" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			if (intBldg == 1)
			{
				txtEconomic.Text = "";
			}
		}

		private void FillBldg_2(short intBldg)
		{
			FillBldg(ref intBldg);
		}

		private void FillBldg(ref short intBldg)
		{
			string strTemp = "";
			int intTemp = 0;
			int lngTemp;
			clsCommCost.MoveFirst();
			// TODO Get_Fields: Field [occ] not found!! (maybe it is an alias?)
			strTemp = GetDesc_26(0, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsComm.Get_Fields("occ" + FCConvert.ToString(intBldg))))), true);
			(Detail.Controls["txtOccupancy" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
			// TODO Get_Fields: Field [dwel] not found!! (maybe it is an alias?)
			strTemp = FCConvert.ToString(Conversion.Val(clsComm.Get_Fields("dwel" + FCConvert.ToString(intBldg))));
			(Detail.Controls["txtUnits" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			if (Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "class")) == 1)
			{
				strTemp = "1 Structural Steel";
			}
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			else if (Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "class")) == 2)
			{
				strTemp = "2 Reinforced Concrete";
			}
				// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
				else if (Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "class")) == 3)
			{
				strTemp = "3 Masonry";
			}
					// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
					else if (Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "class")) == 4)
			{
				strTemp = "4 Wood Frame";
			}
						// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
						else if (Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "class")) == 5)
			{
				strTemp = "5 Rigid Frame";
			}
			else
			{
				// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
				strTemp = FCConvert.ToString(Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "class")));
			}
			(Detail.Controls["txtClass" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			if (Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "quality")) == 1)
			{
				strTemp = "1 Low Cost";
			}
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			else if (Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "quality")) == 2)
			{
				strTemp = "2 Average";
			}
				// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
				else if (Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "quality")) == 3)
			{
				strTemp = "3 Good";
			}
					// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
					else if (Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "quality")) == 4)
			{
				strTemp = "4 Excellent";
			}
			else
			{
				// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
				strTemp = FCConvert.ToString(Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "quality")));
			}
			(Detail.Controls["txtQuality" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			(Detail.Controls["txtGradeFactor" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "grade")) / 100, "0.00");
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			strTemp = GetDesc_26(150, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "heat")))), true);
			(Detail.Controls["txtHeatCool" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			strTemp = GetDesc_26(340, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "extwalls")))), true);
			(Detail.Controls["txtExterior" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			(Detail.Controls["txtStories" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "stories"));
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			(Detail.Controls["txtHeight" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "height"));
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			(Detail.Controls["txtArea" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "floor")), "#,###,##0");
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			(Detail.Controls["txtPerimeter" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "perimeter"));
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			(Detail.Controls["txtYearBuilt" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "built"));
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			(Detail.Controls["txtRemodeled" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "remodel"));
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			strTemp = GetDesc_26(350, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "condition")))), true);
			(Detail.Controls["txtCondition" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			(Detail.Controls["txtPhysical" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "phys"));
			// TODO Get_Fields: Field [c] not found!! (maybe it is an alias?)
			(Detail.Controls["txtFunctional" + intBldg] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(clsComm.Get_Fields("c" + FCConvert.ToString(intBldg) + "funct"));
			if (intBldg == 1)
			{
				txtEconomic.Text = FCConvert.ToString(clsComm.Get_Fields_Int32("cmecon"));
				if (this.ParentReport as srptpdfPropertyCard2 != null)
				{
					if (Information.IsDate((this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields("dateinspected")))
					{
						if ((this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields_DateTime("dateinspected") != DateTime.FromOADate(0))
						{
							txtDateInspected.Text = Strings.Format((this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields_DateTime("dateinspected"), "M/dd/yyyy");
						}
						else
						{
							txtDateInspected.Text = "";
						}
					}
					else
					{
						txtDateInspected.Text = "";
					}
					// TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
					intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields("entrancecode"))));
				}
				else
				{
					if (Information.IsDate((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields("dateinspected")))
					{
						if ((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields_DateTime("dateinspected") != DateTime.FromOADate(0))
						{
							txtDateInspected.Text = Strings.Format((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields_DateTime("dateinspected"), "M/dd/yyyy");
						}
						else
						{
							txtDateInspected.Text = "";
						}
					}
					else
					{
						txtDateInspected.Text = "";
					}
					// TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
					intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields("entrancecode"))));
				}
				if (intTemp > 0)
				{
					// If Me.ParentReport.ParentReport.clsCost.FindFirstRecord("crecordnumber", 1700 + intTemp) Then
					if (clsCost.FindFirst("crecordnumber = " + FCConvert.ToString(1700 + intTemp)))
					{
						txtEntrance.Text = FCConvert.ToString(intTemp) + " " + clsCost.Get_Fields_String("cldesc");
					}
					else
					{
						txtEntrance.Text = "";
					}
				}
				else
				{
					txtEntrance.Text = "";
				}
				if (this.ParentReport as srptpdfPropertyCard2 != null)
				{
					// TODO Get_Fields: Check the table for the column [informationcode] and replace with corresponding Get_Field method
					intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields("informationcode"))));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [informationcode] and replace with corresponding Get_Field method
					intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields("informationcode"))));
				}
				if (intTemp > 0)
				{
					// If Me.ParentReport.ParentReport.clsCost.FindFirstRecord("crecordnumber", 1710 + intTemp) Then
					if (clsCost.FindFirst("crecordnumber = " + FCConvert.ToString(1710 + intTemp)))
					{
						txtInformation.Text = FCConvert.ToString(intTemp) + " " + clsCost.Get_Fields_String("cldesc");
					}
					else
					{
						txtInformation.Text = "";
					}
				}
				else
				{
					txtInformation.Text = "";
				}
			}
		}

		
	}
}
