﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmRECResSizeFactor.
	/// </summary>
	public partial class frmRECResSizeFactor : BaseForm
	{
		public frmRECResSizeFactor()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txt1PGD = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txt1SFLA = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txt2PGD = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txt2SFLA = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txt3PGD = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txt3SFLA = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txt4SFLA = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txt4PGD = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txt5PGD = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txt5SFLA = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txt1PGD.AddControlArrayElement(txt1PGD_0, 0);
			this.txt1PGD.AddControlArrayElement(txt1PGD_1, 1);
			this.txt1PGD.AddControlArrayElement(txt1PGD_2, 2);
			this.txt1PGD.AddControlArrayElement(txt1PGD_3, 3);
			this.txt1PGD.AddControlArrayElement(txt1PGD_4, 4);
			this.txt1SFLA.AddControlArrayElement(txt1SFLA_0, 0);
			this.txt1SFLA.AddControlArrayElement(txt1SFLA_1, 1);
			this.txt1SFLA.AddControlArrayElement(txt1SFLA_2, 2);
			this.txt1SFLA.AddControlArrayElement(txt1SFLA_3, 3);
			this.txt1SFLA.AddControlArrayElement(txt1SFLA_4, 4);
			this.txt2PGD.AddControlArrayElement(txt2PGD_0, 0);
			this.txt2PGD.AddControlArrayElement(txt2PGD_1, 1);
			this.txt2PGD.AddControlArrayElement(txt2PGD_2, 2);
			this.txt2PGD.AddControlArrayElement(txt2PGD_3, 3);
			this.txt2PGD.AddControlArrayElement(txt2PGD_4, 4);
			this.txt2SFLA.AddControlArrayElement(txt2SFLA_0, 0);
			this.txt2SFLA.AddControlArrayElement(txt2SFLA_1, 1);
			this.txt2SFLA.AddControlArrayElement(txt2SFLA_2, 2);
			this.txt2SFLA.AddControlArrayElement(txt2SFLA_3, 3);
			this.txt2SFLA.AddControlArrayElement(txt2SFLA_4, 4);
			this.txt3PGD.AddControlArrayElement(txt3PGD_0, 0);
			this.txt3PGD.AddControlArrayElement(txt3PGD_1, 1);
			this.txt3PGD.AddControlArrayElement(txt3PGD_2, 2);
			this.txt3PGD.AddControlArrayElement(txt3PGD_3, 3);
			this.txt3PGD.AddControlArrayElement(txt3PGD_4, 4);
			this.txt3SFLA.AddControlArrayElement(txt3SFLA_0, 0);
			this.txt3SFLA.AddControlArrayElement(txt3SFLA_1, 1);
			this.txt3SFLA.AddControlArrayElement(txt3SFLA_2, 2);
			this.txt3SFLA.AddControlArrayElement(txt3SFLA_3, 3);
			this.txt3SFLA.AddControlArrayElement(txt3SFLA_4, 4);
			this.txt4SFLA.AddControlArrayElement(txt4SFLA_0, 0);
			this.txt4SFLA.AddControlArrayElement(txt4SFLA_1, 1);
			this.txt4SFLA.AddControlArrayElement(txt4SFLA_2, 2);
			this.txt4SFLA.AddControlArrayElement(txt4SFLA_3, 3);
			this.txt4SFLA.AddControlArrayElement(txt4SFLA_4, 4);
			this.txt4PGD.AddControlArrayElement(txt4PGD_0, 0);
			this.txt4PGD.AddControlArrayElement(txt4PGD_1, 1);
			this.txt4PGD.AddControlArrayElement(txt4PGD_2, 2);
			this.txt4PGD.AddControlArrayElement(txt4PGD_3, 3);
			this.txt4PGD.AddControlArrayElement(txt4PGD_4, 4);
			this.txt5PGD.AddControlArrayElement(txt5PGD_0, 0);
			this.txt5PGD.AddControlArrayElement(txt5PGD_1, 1);
			this.txt5PGD.AddControlArrayElement(txt5PGD_2, 2);
			this.txt5PGD.AddControlArrayElement(txt5PGD_3, 3);
			this.txt5PGD.AddControlArrayElement(txt5PGD_4, 4);
			this.txt5SFLA.AddControlArrayElement(txt5SFLA_0, 0);
			this.txt5SFLA.AddControlArrayElement(txt5SFLA_1, 1);
			this.txt5SFLA.AddControlArrayElement(txt5SFLA_2, 2);
			this.txt5SFLA.AddControlArrayElement(txt5SFLA_3, 3);
			this.txt5SFLA.AddControlArrayElement(txt5SFLA_4, 4);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRECResSizeFactor InstancePtr
		{
			get
			{
				return (frmRECResSizeFactor)Sys.GetInstance(typeof(frmRECResSizeFactor));
			}
		}

		protected frmRECResSizeFactor _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int x;

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}
		//public void cmdQuit_Click()
		//{
		//    cmdQuit_Click(cmdQuit, new System.EventArgs());
		//}
		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			modGlobalVariables.Statics.boolRSZChanged = true;
			clsSave.OpenRecordset("select * from rszrecord", modGlobalVariables.strREDatabase);
			if (clsSave.EndOfFile())
			{
				clsSave.AddNew();
			}
			else
			{
				clsSave.Edit();
			}
			clsSave.Set_Fields("rszrge01", FCConvert.ToString(Conversion.Val(txt1SFLA[0].Text)));
			clsSave.Set_Fields("rszrge02", FCConvert.ToString(Conversion.Val(txt1SFLA[1].Text)));
			clsSave.Set_Fields("rszrge03", FCConvert.ToString(Conversion.Val(txt1SFLA[2].Text)));
			clsSave.Set_Fields("rszrge04", FCConvert.ToString(Conversion.Val(txt1SFLA[3].Text)));
			clsSave.Set_Fields("rszrge05", FCConvert.ToString(Conversion.Val(txt1SFLA[4].Text)));
			clsSave.Set_Fields("rszfct01", FCConvert.ToString(Conversion.Val(txt1PGD[0].Text)));
			clsSave.Set_Fields("rszfct02", FCConvert.ToString(Conversion.Val(txt1PGD[1].Text)));
			clsSave.Set_Fields("rszfct03", FCConvert.ToString(Conversion.Val(txt1PGD[2].Text)));
			clsSave.Set_Fields("rszfct04", FCConvert.ToString(Conversion.Val(txt1PGD[3].Text)));
			clsSave.Set_Fields("rszfct05", FCConvert.ToString(Conversion.Val(txt1PGD[4].Text)));
			clsSave.Set_Fields("rszrge06", FCConvert.ToString(Conversion.Val(txt2SFLA[0].Text)));
			clsSave.Set_Fields("rszrge07", FCConvert.ToString(Conversion.Val(txt2SFLA[1].Text)));
			clsSave.Set_Fields("rszrge08", FCConvert.ToString(Conversion.Val(txt2SFLA[2].Text)));
			clsSave.Set_Fields("rszrge09", FCConvert.ToString(Conversion.Val(txt2SFLA[3].Text)));
			clsSave.Set_Fields("rszrge10", FCConvert.ToString(Conversion.Val(txt2SFLA[4].Text)));
			clsSave.Set_Fields("rszfct06", FCConvert.ToString(Conversion.Val(txt2PGD[0].Text)));
			clsSave.Set_Fields("rszfct07", FCConvert.ToString(Conversion.Val(txt2PGD[1].Text)));
			clsSave.Set_Fields("rszfct08", FCConvert.ToString(Conversion.Val(txt2PGD[2].Text)));
			clsSave.Set_Fields("rszfct09", FCConvert.ToString(Conversion.Val(txt2PGD[3].Text)));
			clsSave.Set_Fields("rszfct10", FCConvert.ToString(Conversion.Val(txt2PGD[4].Text)));
			clsSave.Set_Fields("rszrge11", FCConvert.ToString(Conversion.Val(txt3SFLA[0].Text)));
			clsSave.Set_Fields("rszrge12", FCConvert.ToString(Conversion.Val(txt3SFLA[1].Text)));
			clsSave.Set_Fields("rszrge13", FCConvert.ToString(Conversion.Val(txt3SFLA[2].Text)));
			clsSave.Set_Fields("rszrge14", FCConvert.ToString(Conversion.Val(txt3SFLA[3].Text)));
			clsSave.Set_Fields("rszrge15", FCConvert.ToString(Conversion.Val(txt3SFLA[4].Text)));
			clsSave.Set_Fields("rszfct11", FCConvert.ToString(Conversion.Val(txt3PGD[0].Text)));
			clsSave.Set_Fields("rszfct12", FCConvert.ToString(Conversion.Val(txt3PGD[1].Text)));
			clsSave.Set_Fields("rszfct13", FCConvert.ToString(Conversion.Val(txt3PGD[2].Text)));
			clsSave.Set_Fields("rszfct14", FCConvert.ToString(Conversion.Val(txt3PGD[3].Text)));
			clsSave.Set_Fields("rszfct15", FCConvert.ToString(Conversion.Val(txt3PGD[4].Text)));
			clsSave.Set_Fields("rszrge16", FCConvert.ToString(Conversion.Val(txt4SFLA[0].Text)));
			clsSave.Set_Fields("rszrge17", FCConvert.ToString(Conversion.Val(txt4SFLA[1].Text)));
			clsSave.Set_Fields("rszrge18", FCConvert.ToString(Conversion.Val(txt4SFLA[2].Text)));
			clsSave.Set_Fields("rszrge19", FCConvert.ToString(Conversion.Val(txt4SFLA[3].Text)));
			clsSave.Set_Fields("rszrge20", FCConvert.ToString(Conversion.Val(txt4SFLA[4].Text)));
			clsSave.Set_Fields("rszfct16", FCConvert.ToString(Conversion.Val(txt4PGD[0].Text)));
			clsSave.Set_Fields("rszfct17", FCConvert.ToString(Conversion.Val(txt4PGD[1].Text)));
			clsSave.Set_Fields("rszfct18", FCConvert.ToString(Conversion.Val(txt4PGD[2].Text)));
			clsSave.Set_Fields("rszfct19", FCConvert.ToString(Conversion.Val(txt4PGD[3].Text)));
			clsSave.Set_Fields("rszfct20", FCConvert.ToString(Conversion.Val(txt4PGD[4].Text)));
			clsSave.Set_Fields("rszrge21", FCConvert.ToString(Conversion.Val(txt5SFLA[0].Text)));
			clsSave.Set_Fields("rszrge22", FCConvert.ToString(Conversion.Val(txt5SFLA[1].Text)));
			clsSave.Set_Fields("rszrge23", FCConvert.ToString(Conversion.Val(txt5SFLA[2].Text)));
			clsSave.Set_Fields("rszrge24", FCConvert.ToString(Conversion.Val(txt5SFLA[3].Text)));
			clsSave.Set_Fields("rszrge25", FCConvert.ToString(Conversion.Val(txt5SFLA[4].Text)));
			clsSave.Set_Fields("rszfct21", FCConvert.ToString(Conversion.Val(txt5PGD[0].Text)));
			clsSave.Set_Fields("rszfct22", FCConvert.ToString(Conversion.Val(txt5PGD[1].Text)));
			clsSave.Set_Fields("rszfct23", FCConvert.ToString(Conversion.Val(txt5PGD[2].Text)));
			clsSave.Set_Fields("rszfct24", FCConvert.ToString(Conversion.Val(txt5PGD[3].Text)));
			clsSave.Set_Fields("rszfct25", FCConvert.ToString(Conversion.Val(txt5PGD[4].Text)));
			clsSave.Update();
			MessageBox.Show("Save Complete", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void LoadRSZInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from rszrecord", modGlobalVariables.strREDatabase);
			if (!clsLoad.EndOfFile())
			{
				txt1SFLA[0].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge01")));
				txt1SFLA[1].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge02")));
				txt1SFLA[2].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge03")));
				txt1SFLA[3].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge04")));
				txt1SFLA[4].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge05")));
				txt1PGD[0].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct01")));
				txt1PGD[1].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct02")));
				txt1PGD[2].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct03")));
				txt1PGD[3].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct04")));
				txt1PGD[4].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct05")));
				txt2SFLA[0].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge06")));
				txt2SFLA[1].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge07")));
				txt2SFLA[2].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge08")));
				txt2SFLA[3].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge09")));
				txt2SFLA[4].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge10")));
				txt2PGD[0].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct06")));
				txt2PGD[1].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct07")));
				txt2PGD[2].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct08")));
				txt2PGD[3].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct09")));
				txt2PGD[4].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct10")));
				txt3SFLA[0].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge11")));
				txt3SFLA[1].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge12")));
				txt3SFLA[2].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge13")));
				txt3SFLA[3].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge14")));
				txt3SFLA[4].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge15")));
				txt3PGD[0].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct11")));
				txt3PGD[1].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct12")));
				txt3PGD[2].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct13")));
				txt3PGD[3].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct14")));
				txt3PGD[4].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct15")));
				txt4SFLA[0].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge16")));
				txt4SFLA[1].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge17")));
				txt4SFLA[2].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge18")));
				txt4SFLA[3].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge19")));
				txt4SFLA[4].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge20")));
				txt4PGD[0].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct16")));
				txt4PGD[1].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct17")));
				txt4PGD[2].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct18")));
				txt4PGD[3].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct19")));
				txt4PGD[4].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct20")));
				txt5SFLA[0].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge21")));
				txt5SFLA[1].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge22")));
				txt5SFLA[2].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge23")));
				txt5SFLA[3].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge24")));
				txt5SFLA[4].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszrge25")));
				txt5PGD[0].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct21")));
				txt5PGD[1].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct22")));
				txt5PGD[2].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct23")));
				txt5PGD[3].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct24")));
				txt5PGD[4].Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rszfct25")));
			}
		}

		private void frmRECResSizeFactor_Activated(object sender, System.EventArgs e)
		{
			if (modMDIParent.FormExist(this))
				return;
			// 
			// 
			// frmRECResSizeFactor.Caption = TitleCaption
			txt1SFLA[0].Focus();
			// call SetFixedSize (Me)
		}

		private void frmRECResSizeFactor_Enter(object sender, System.EventArgs e)
		{
			txt1SFLA[0].Focus();
		}

		private void frmRECResSizeFactor_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				this.Unload();
			}
			if (KeyCode == Keys.Down)
			{
				Support.SendKeys("{TAB}" + "{TAB}", false);
			}
			else if (KeyCode == Keys.Up)
			{
				Support.SendKeys("+{TAB}" + "+{TAB}", false);
			}
			else if (KeyCode == Keys.F10)
			{
				mnuSaveExit_Click();
			}
			else if (KeyCode == Keys.F9)
			{
				mnuSave_Click();
			}
		}

		private void frmRECResSizeFactor_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				Support.SendKeys("{TAB}", false);
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmRECResSizeFactor_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRECResSizeFactor properties;
			//frmRECResSizeFactor.ScaleWidth	= 9045;
			//frmRECResSizeFactor.ScaleHeight	= 7185;
			//frmRECResSizeFactor.LinkTopic	= "Form1";
			//frmRECResSizeFactor.LockControls	= true;
			//End Unmaped Properties
			LoadRSZInfo();
			modGlobalFunctions.SetFixedSize(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		//private void mnuQuit_Click(object sender, System.EventArgs e)
		//{
		//    cmdQuit_Click();
		//}
		//public void mnuQuit_Click()
		//{
		//    mnuQuit_Click(mnuQuit, new System.EventArgs());
		//}
		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			//mnuQuit_Click();
		}

		public void mnuSaveExit_Click()
		{
			mnuSaveExit_Click(mnuSaveExit, new System.EventArgs());
		}
		// Private Sub txt1PGD_Change(Index As Integer)
		// If IsNumeric(txt1PGD(x).Text) = False Then
		// D_E_TAG:
		// Resp1 = txt1PGD(x).Text
		// Call CheckNumeric
		//
		// txt1PGD(x).Text = Resp1
		// txt1PGD(x).SetFocus
		// txt1PGD(x).SelStart = Len(Trim(Resp1))
		// End If
		// If InStr(txt1PGD(x).Text, "E") Then GoTo D_E_TAG
		// If InStr(txt1PGD(x).Text, "e") Then GoTo D_E_TAG
		// If InStr(txt1PGD(x).Text, "D") Then GoTo D_E_TAG
		// If InStr(txt1PGD(x).Text, "d") Then GoTo D_E_TAG
		// End Sub
		private void txt1PGD_Enter(short Index, object sender, System.EventArgs e)
		{
			// OverType = 1
			txt1PGD[FCConvert.ToInt16(x)].SelectionStart = 0;
			txt1PGD[FCConvert.ToInt16(x)].SelectionLength = 1;
		}

		private void txt1PGD_Enter(object sender, System.EventArgs e)
		{
			short index = txt1PGD.GetIndex((FCTextBox)sender);
			txt1PGD_Enter(index, sender, e);
		}

		private void txt1PGD_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (x < 4)
			{
				if (txt1PGD[FCConvert.ToInt16(x)].SelectionStart == 3)
					txt1SFLA[FCConvert.ToInt16(x + 1)].Focus();
			}
			else if (x == 4)
			{
				if (txt1PGD[FCConvert.ToInt16(x)].SelectionStart == 3)
					txt2SFLA[0].Focus();
			}
		}

		private void txt1PGD_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txt1PGD.GetIndex((FCTextBox)sender);
			txt1PGD_KeyUp(index, sender, e);
		}

		private void txt1PGD_Leave(short Index, object sender, System.EventArgs e)
		{
			if (Conversion.Val(txt1PGD[FCConvert.ToInt16(x)].Text) < 0 || Conversion.Val(txt1PGD[FCConvert.ToInt16(x)].Text) > 100)
			{
				MessageBox.Show("Percent Good has to be between 0% and 100%" + "\r\n" + "\r\n" + "Your entry was " + txt1PGD[FCConvert.ToInt16(x)].Text + "%", "Percent Good ???", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txt1PGD[FCConvert.ToInt16(x)].Text = "";
				txt1PGD[FCConvert.ToInt16(x)].Focus();
			}
		}

		private void txt1PGD_Leave(object sender, System.EventArgs e)
		{
			short index = txt1PGD.GetIndex((FCTextBox)sender);
			txt1PGD_Leave(index, sender, e);
		}

		private void txt1PGD_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			txt1PGD[Index].Text = FCConvert.ToString(Conversion.Val(txt1PGD[Index].Text));
		}

		private void txt1PGD_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txt1PGD.GetIndex((FCTextBox)sender);
			txt1PGD_Validating(index, sender, e);
		}
		// Private Sub txt1SFLA_Change(Index As Integer)
		// If IsNumeric(txt1SFLA(x).Text) = False Then
		// D_E_TAG:
		// Resp1 = txt1SFLA(x).Text
		// Call CheckNumeric
		//
		// txt1SFLA(x).Text = Resp1
		// txt1SFLA(x).SetFocus
		// txt1SFLA(x).SelStart = Len(Trim(Resp1))
		// End If
		// If InStr(txt1SFLA(x).Text, "E") Then GoTo D_E_TAG
		// If InStr(txt1SFLA(x).Text, "e") Then GoTo D_E_TAG
		// If InStr(txt1SFLA(x).Text, "D") Then GoTo D_E_TAG
		// If InStr(txt1SFLA(x).Text, "d") Then GoTo D_E_TAG
		// End Sub
		private void txt1SFLA_Enter(short Index, object sender, System.EventArgs e)
		{
			// OverType = 1
			txt1SFLA[FCConvert.ToInt16(x)].SelectionStart = 0;
			txt1SFLA[FCConvert.ToInt16(x)].SelectionLength = 1;
		}

		private void txt1SFLA_Enter(object sender, System.EventArgs e)
		{
			short index = txt1SFLA.GetIndex((FCTextBox)sender);
			txt1SFLA_Enter(index, sender, e);
		}

		private void txt1SFLA_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			txt1SFLA[Index].Text = FCConvert.ToString(Conversion.Val(txt1SFLA[Index].Text));
		}

		private void txt1SFLA_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txt1SFLA.GetIndex((FCTextBox)sender);
			txt1SFLA_Validating(index, sender, e);
		}
		// Private Sub txt2PGD_Change(Index As Integer)
		// If IsNumeric(txt2PGD(x).Text) = False Then
		// D_E_TAG:
		// Resp1 = txt2PGD(x).Text
		// Call CheckNumeric
		//
		// txt2PGD(x).Text = Resp1
		// txt2PGD(x).SetFocus
		// txt2PGD(x).SelStart = Len(Trim(Resp1))
		// End If
		// If InStr(txt2PGD(x).Text, "E") Then GoTo D_E_TAG
		// If InStr(txt2PGD(x).Text, "e") Then GoTo D_E_TAG
		// If InStr(txt2PGD(x).Text, "D") Then GoTo D_E_TAG
		// If InStr(txt2PGD(x).Text, "d") Then GoTo D_E_TAG
		// End Sub
		private void txt2PGD_Enter(short Index, object sender, System.EventArgs e)
		{
			// OverType = 1
			txt2PGD[FCConvert.ToInt16(x)].SelectionStart = 0;
			txt2PGD[FCConvert.ToInt16(x)].SelectionLength = 1;
		}

		private void txt2PGD_Enter(object sender, System.EventArgs e)
		{
			short index = txt2PGD.GetIndex((FCTextBox)sender);
			txt2PGD_Enter(index, sender, e);
		}

		private void txt2PGD_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (x < 4)
			{
				if (txt2PGD[FCConvert.ToInt16(x)].SelectionStart == 3)
					txt2SFLA[FCConvert.ToInt16(x + 1)].Focus();
			}
			else if (x == 4)
			{
				if (txt2PGD[FCConvert.ToInt16(x)].SelectionStart == 3)
					txt3SFLA[0].Focus();
			}
		}

		private void txt2PGD_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txt2PGD.GetIndex((FCTextBox)sender);
			txt2PGD_KeyUp(index, sender, e);
		}

		private void txt2PGD_Leave(short Index, object sender, System.EventArgs e)
		{
			if (Conversion.Val(txt2PGD[FCConvert.ToInt16(x)].Text) < 0 || Conversion.Val(txt2PGD[FCConvert.ToInt16(x)].Text) > 100)
			{
				MessageBox.Show("Percent Good has to be between 0% and 100%" + "\r\n" + "\r\n" + "Your entry was " + txt2PGD[FCConvert.ToInt16(x)].Text + "%", "Percent Good ???", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txt2PGD[FCConvert.ToInt16(x)].Text = "";
				txt2PGD[FCConvert.ToInt16(x)].Focus();
			}
		}

		private void txt2PGD_Leave(object sender, System.EventArgs e)
		{
			short index = txt2PGD.GetIndex((FCTextBox)sender);
			txt2PGD_Leave(index, sender, e);
		}

		private void txt2PGD_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			txt2PGD[Index].Text = FCConvert.ToString(Conversion.Val(txt2PGD[Index].Text));
		}

		private void txt2PGD_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txt2PGD.GetIndex((FCTextBox)sender);
			txt2PGD_Validating(index, sender, e);
		}
		// Private Sub txt2SFLA_Change(Index As Integer)
		// If IsNumeric(txt2SFLA(x).Text) = False Then
		// D_E_TAG:
		// Resp1 = txt2SFLA(x).Text
		// Call CheckNumeric
		//
		// txt2SFLA(x).Text = Resp1
		// txt2SFLA(x).SetFocus
		// txt2SFLA(x).SelStart = Len(Trim(Resp1))
		// End If
		// If InStr(txt2SFLA(x).Text, "E") Then GoTo D_E_TAG
		// If InStr(txt2SFLA(x).Text, "e") Then GoTo D_E_TAG
		// If InStr(txt2SFLA(x).Text, "D") Then GoTo D_E_TAG
		// If InStr(txt2SFLA(x).Text, "d") Then GoTo D_E_TAG
		// End Sub
		private void txt2SFLA_Enter(short Index, object sender, System.EventArgs e)
		{
			// OverType = 1
			txt2SFLA[FCConvert.ToInt16(x)].SelectionStart = 0;
			txt2SFLA[FCConvert.ToInt16(x)].SelectionLength = 1;
		}

		private void txt2SFLA_Enter(object sender, System.EventArgs e)
		{
			short index = txt2SFLA.GetIndex((FCTextBox)sender);
			txt2SFLA_Enter(index, sender, e);
		}

		private void txt2SFLA_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			txt2SFLA[Index].Text = FCConvert.ToString(Conversion.Val(txt2SFLA[Index].Text));
		}

		private void txt2SFLA_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txt2SFLA.GetIndex((FCTextBox)sender);
			txt2SFLA_Validating(index, sender, e);
		}
		// Private Sub txt3PGD_Change(Index As Integer)
		// If IsNumeric(txt3PGD(x).Text) = False Then
		// D_E_TAG:
		// Resp1 = txt3PGD(x).Text
		// Call CheckNumeric
		//
		// txt3PGD(x).Text = Resp1
		// txt3PGD(x).SetFocus
		// txt3PGD(x).SelStart = Len(Trim(Resp1))
		// End If
		// If InStr(txt3PGD(x).Text, "E") Then GoTo D_E_TAG
		// If InStr(txt3PGD(x).Text, "e") Then GoTo D_E_TAG
		// If InStr(txt3PGD(x).Text, "D") Then GoTo D_E_TAG
		// If InStr(txt3PGD(x).Text, "d") Then GoTo D_E_TAG
		// End Sub
		private void txt3PGD_Enter(short Index, object sender, System.EventArgs e)
		{
			// OverType = 1
			txt3PGD[FCConvert.ToInt16(x)].SelectionStart = 0;
			txt3PGD[FCConvert.ToInt16(x)].SelectionLength = 1;
		}

		private void txt3PGD_Enter(object sender, System.EventArgs e)
		{
			short index = txt3PGD.GetIndex((FCTextBox)sender);
			txt3PGD_Enter(index, sender, e);
		}

		private void txt3PGD_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (x < 4)
			{
				if (txt3PGD[FCConvert.ToInt16(x)].SelectionStart == 3)
					txt3SFLA[FCConvert.ToInt16(x + 1)].Focus();
			}
			else if (x == 4)
			{
				if (txt3PGD[FCConvert.ToInt16(x)].SelectionStart == 3)
					txt4SFLA[0].Focus();
			}
		}

		private void txt3PGD_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txt3PGD.GetIndex((FCTextBox)sender);
			txt3PGD_KeyUp(index, sender, e);
		}

		private void txt3PGD_Leave(short Index, object sender, System.EventArgs e)
		{
			if (Conversion.Val(txt3PGD[FCConvert.ToInt16(x)].Text) < 0 || Conversion.Val(txt3PGD[FCConvert.ToInt16(x)].Text) > 100)
			{
				MessageBox.Show("Percent Good has to be between 0% and 100%" + "\r\n" + "\r\n" + "Your entry was " + txt3PGD[FCConvert.ToInt16(x)].Text + "%", "Percent Good ???", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txt3PGD[FCConvert.ToInt16(x)].Text = "";
				txt3PGD[FCConvert.ToInt16(x)].Focus();
			}
		}

		private void txt3PGD_Leave(object sender, System.EventArgs e)
		{
			short index = txt3PGD.GetIndex((FCTextBox)sender);
			txt3PGD_Leave(index, sender, e);
		}

		private void txt3PGD_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			txt3PGD[Index].Text = FCConvert.ToString(Conversion.Val(txt3PGD[Index].Text));
		}

		private void txt3PGD_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txt3PGD.GetIndex((FCTextBox)sender);
			txt3PGD_Validating(index, sender, e);
		}
		// Private Sub txt3SFLA_Change(Index As Integer)
		// If IsNumeric(txt3SFLA(x).Text) = False Then
		// D_E_TAG:
		// Resp1 = txt3SFLA(x).Text
		// Call CheckNumeric
		//
		// txt3SFLA(x).Text = Resp1
		// txt3SFLA(x).SetFocus
		// txt3SFLA(x).SelStart = Len(Trim(Resp1))
		// End If
		// If InStr(txt3SFLA(x).Text, "E") Then GoTo D_E_TAG
		// If InStr(txt3SFLA(x).Text, "e") Then GoTo D_E_TAG
		// If InStr(txt3SFLA(x).Text, "D") Then GoTo D_E_TAG
		// If InStr(txt3SFLA(x).Text, "d") Then GoTo D_E_TAG
		// End Sub
		private void txt3SFLA_Enter(short Index, object sender, System.EventArgs e)
		{
			// OverType = 1
			txt3SFLA[FCConvert.ToInt16(x)].SelectionStart = 0;
			txt3SFLA[FCConvert.ToInt16(x)].SelectionLength = 1;
		}

		private void txt3SFLA_Enter(object sender, System.EventArgs e)
		{
			short index = txt3SFLA.GetIndex((FCTextBox)sender);
			txt3SFLA_Enter(index, sender, e);
		}

		private void txt3SFLA_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			txt3SFLA[Index].Text = FCConvert.ToString(Conversion.Val(txt3SFLA[Index].Text));
		}

		private void txt3SFLA_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txt3SFLA.GetIndex((FCTextBox)sender);
			txt3SFLA_Validating(index, sender, e);
		}
		// Private Sub txt4PGD_Change(Index As Integer)
		// If IsNumeric(txt4PGD(x).Text) = False Then
		// D_E_TAG:
		// Resp1 = txt4PGD(x).Text
		// Call CheckNumeric
		//
		// txt4PGD(x).Text = Resp1
		// txt4PGD(x).SetFocus
		// txt4PGD(x).SelStart = Len(Trim(Resp1))
		// End If
		// If InStr(txt4PGD(x).Text, "E") Then GoTo D_E_TAG
		// If InStr(txt4PGD(x).Text, "e") Then GoTo D_E_TAG
		// If InStr(txt4PGD(x).Text, "D") Then GoTo D_E_TAG
		// If InStr(txt4PGD(x).Text, "d") Then GoTo D_E_TAG
		// End Sub
		private void txt4PGD_Enter(short Index, object sender, System.EventArgs e)
		{
			// OverType = 1
			txt4PGD[FCConvert.ToInt16(x)].SelectionStart = 0;
			txt4PGD[FCConvert.ToInt16(x)].SelectionLength = 1;
		}

		private void txt4PGD_Enter(object sender, System.EventArgs e)
		{
			short index = txt4PGD.GetIndex((FCTextBox)sender);
			txt4PGD_Enter(index, sender, e);
		}

		private void txt4PGD_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (x < 4)
			{
				if (txt4PGD[FCConvert.ToInt16(x)].SelectionStart == 3)
					txt4SFLA[FCConvert.ToInt16(x + 1)].Focus();
			}
			else if (x == 4)
			{
				if (txt4PGD[FCConvert.ToInt16(x)].SelectionStart == 3)
					txt5SFLA[0].Focus();
			}
		}

		private void txt4PGD_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txt4PGD.GetIndex((FCTextBox)sender);
			txt4PGD_KeyUp(index, sender, e);
		}

		private void txt4PGD_Leave(short Index, object sender, System.EventArgs e)
		{
			if (Conversion.Val(txt4PGD[FCConvert.ToInt16(x)].Text) < 0 || Conversion.Val(txt4PGD[FCConvert.ToInt16(x)].Text) > 100)
			{
				MessageBox.Show("Percent Good has to be between 0% and 100%" + "\r\n" + "\r\n" + "Your entry was " + txt4PGD[FCConvert.ToInt16(x)].Text + "%", "Percent Good ???", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txt4PGD[FCConvert.ToInt16(x)].Text = "";
				txt4PGD[FCConvert.ToInt16(x)].Focus();
			}
		}

		private void txt4PGD_Leave(object sender, System.EventArgs e)
		{
			short index = txt4PGD.GetIndex((FCTextBox)sender);
			txt4PGD_Leave(index, sender, e);
		}

		private void txt4PGD_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			txt4PGD[Index].Text = FCConvert.ToString(Conversion.Val(txt4PGD[Index].Text));
		}

		private void txt4PGD_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txt4PGD.GetIndex((FCTextBox)sender);
			txt4PGD_Validating(index, sender, e);
		}
		// Private Sub txt4SFLA_Change(Index As Integer)
		// If IsNumeric(txt4SFLA(x).Text) = False Then
		// D_E_TAG:
		// Resp1 = txt4SFLA(x).Text
		// Call CheckNumeric
		//
		// txt4SFLA(x).Text = Resp1
		// txt4SFLA(x).SetFocus
		// txt4SFLA(x).SelStart = Len(Trim(Resp1))
		// End If
		// If InStr(txt4SFLA(x).Text, "E") Then GoTo D_E_TAG
		// If InStr(txt4SFLA(x).Text, "e") Then GoTo D_E_TAG
		// If InStr(txt4SFLA(x).Text, "D") Then GoTo D_E_TAG
		// If InStr(txt4SFLA(x).Text, "d") Then GoTo D_E_TAG
		// End Sub
		private void txt4SFLA_Enter(short Index, object sender, System.EventArgs e)
		{
			// OverType = 1
			txt4SFLA[FCConvert.ToInt16(x)].SelectionStart = 0;
			txt4SFLA[FCConvert.ToInt16(x)].SelectionLength = 1;
		}

		private void txt4SFLA_Enter(object sender, System.EventArgs e)
		{
			short index = txt4SFLA.GetIndex((FCTextBox)sender);
			txt4SFLA_Enter(index, sender, e);
		}

		private void txt4SFLA_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			txt4SFLA[Index].Text = FCConvert.ToString(Conversion.Val(txt4SFLA[Index].Text));
		}

		private void txt4SFLA_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txt4SFLA.GetIndex((FCTextBox)sender);
			txt4SFLA_Validating(index, sender, e);
		}
		// Private Sub txt5PGD_Change(Index As Integer)
		// If IsNumeric(txt5PGD(x).Text) = False Then
		// D_E_TAG:
		// Resp1 = txt5PGD(x).Text
		// Call CheckNumeric
		//
		// txt5PGD(x).Text = Resp1
		// txt5PGD(x).SetFocus
		// txt5PGD(x).SelStart = Len(Trim(Resp1))
		// End If
		// If InStr(txt5PGD(x).Text, "E") Then GoTo D_E_TAG
		// If InStr(txt5PGD(x).Text, "e") Then GoTo D_E_TAG
		// If InStr(txt5PGD(x).Text, "D") Then GoTo D_E_TAG
		// If InStr(txt5PGD(x).Text, "d") Then GoTo D_E_TAG
		// End Sub
		private void txt5PGD_Enter(short Index, object sender, System.EventArgs e)
		{
			// OverType = 1
			txt5PGD[FCConvert.ToInt16(x)].SelectionStart = 0;
			txt5PGD[FCConvert.ToInt16(x)].SelectionLength = 1;
		}

		private void txt5PGD_Enter(object sender, System.EventArgs e)
		{
			short index = txt5PGD.GetIndex((FCTextBox)sender);
			txt5PGD_Enter(index, sender, e);
		}

		private void txt5PGD_KeyUp(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (x < 4)
			{
				if (txt5PGD[FCConvert.ToInt16(x)].SelectionStart == 3)
					txt5SFLA[FCConvert.ToInt16(x + 1)].Focus();
			}
			else if (x == 4)
			{
				if (txt5PGD[FCConvert.ToInt16(x)].SelectionStart == 3)
					txt1SFLA[0].Focus();
			}
		}

		private void txt5PGD_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txt5PGD.GetIndex((FCTextBox)sender);
			txt5PGD_KeyUp(index, sender, e);
		}

		private void txt5PGD_Leave(short Index, object sender, System.EventArgs e)
		{
			if (Conversion.Val(txt5PGD[FCConvert.ToInt16(x)].Text) < 0 || Conversion.Val(txt5PGD[FCConvert.ToInt16(x)].Text) > 100)
			{
				MessageBox.Show("Percent Good has to be between 0% and 100%" + "\r\n" + "\r\n" + "Your entry was " + txt5PGD[FCConvert.ToInt16(x)].Text + "%", "Percent Good ???", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txt5PGD[FCConvert.ToInt16(x)].Text = "";
				txt5PGD[FCConvert.ToInt16(x)].Focus();
			}
		}

		private void txt5PGD_Leave(object sender, System.EventArgs e)
		{
			short index = txt5PGD.GetIndex((FCTextBox)sender);
			txt5PGD_Leave(index, sender, e);
		}

		private void txt5PGD_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			txt5PGD[Index].Text = FCConvert.ToString(Conversion.Val(txt5PGD[Index].Text));
		}

		private void txt5PGD_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txt5PGD.GetIndex((FCTextBox)sender);
			txt5PGD_Validating(index, sender, e);
		}
		// Private Sub txt5SFLA_Change(Index As Integer)
		// If IsNumeric(txt5SFLA(x).Text) = False Then
		// D_E_TAG:
		// Resp1 = txt5SFLA(x).Text
		// Call CheckNumeric
		//
		// txt5SFLA(x).Text = Resp1
		// txt5SFLA(x).SetFocus
		// txt5SFLA(x).SelStart = Len(Trim(Resp1))
		// End If
		// If InStr(txt5SFLA(x).Text, "E") Then GoTo D_E_TAG
		// If InStr(txt5SFLA(x).Text, "e") Then GoTo D_E_TAG
		// If InStr(txt5SFLA(x).Text, "D") Then GoTo D_E_TAG
		// If InStr(txt5SFLA(x).Text, "d") Then GoTo D_E_TAG
		// End Sub
		private void txt5SFLA_Enter(short Index, object sender, System.EventArgs e)
		{
			// OverType = 1
			txt5SFLA[FCConvert.ToInt16(x)].SelectionStart = 0;
			txt5SFLA[FCConvert.ToInt16(x)].SelectionLength = 1;
		}

		private void txt5SFLA_Enter(object sender, System.EventArgs e)
		{
			short index = txt5SFLA.GetIndex((FCTextBox)sender);
			txt5SFLA_Enter(index, sender, e);
		}

		private void txt5SFLA_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			txt5SFLA[Index].Text = FCConvert.ToString(Conversion.Val(txt5SFLA[Index].Text));
		}

		private void txt5SFLA_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txt5SFLA.GetIndex((FCTextBox)sender);
			txt5SFLA_Validating(index, sender, e);
		}
	}
}
