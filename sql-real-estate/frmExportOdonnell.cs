﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExportODonnell.
	/// </summary>
	public partial class frmExportODonnell : BaseForm
	{
		public frmExportODonnell()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExportODonnell InstancePtr
		{
			get
			{
				return (frmExportODonnell)Sys.GetInstance(typeof(frmExportODonnell));
			}
		}

		protected frmExportODonnell _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		public void Init()
		{
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmExportODonnell_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmExportODonnell_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExportODonnell properties;
			//frmExportODonnell.FillStyle	= 0;
			//frmExportODonnell.ScaleWidth	= 5880;
			//frmExportODonnell.ScaleHeight	= 4260;
			//frmExportODonnell.LinkTopic	= "Form2";
			//frmExportODonnell.LockControls	= true;
			//frmExportODonnell.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuExport_Click(object sender, System.EventArgs e)
		{
			if (chkNameAddress.CheckState == Wisej.Web.CheckState.Unchecked && chkProperty.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				MessageBox.Show("You must select at least one option to export", "No Options Chosen", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbRange.Text == "Range")
			{
				if (cmbType.Text == "Account")
				{
					if (Conversion.Val(txtStart.Text) <= 0 || (Conversion.Val(txtStart.Text) > Conversion.Val(txtEnd.Text) && Conversion.Val(txtEnd.Text) != 0))
					{
						MessageBox.Show("You must enter a valid account range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					if (Strings.Trim(txtStart.Text) == string.Empty)
					{
						MessageBox.Show("You must enter a valid map lot range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			if (ExportODonnell())
			{
			}
		}

		private bool ExportODonnell()
		{
			bool ExportODonnell = false;
			try
			{
				// On Error GoTo ErrorHandler
				ExportODonnell = false;
				//FileSystemObject fso = new FileSystemObject();
				string strFile = "";
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL;
				string strSortField;
				int lngCount;
				bool boolNameAddress = false;
				bool boolProperty = false;
				string strExport;
				string strREFullDBName;
				string strMasterJoin;
				string strMasterJoinJoin = "";
				strREFullDBName = rsLoad.Get_GetFullDBName("RealEstate");
				strMasterJoin = modREMain.GetMasterJoin();
				string strMasterJoinQuery;
				strMasterJoinQuery = "(" + strMasterJoin + ") mj";
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNPathMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNCreatePrompt	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				//FC:FINAL:SBE - replace SaveFileDialog with Download file
                //MDIParent.InstancePtr.CommonDialog1.Filter = "XML|*.xml";
				//MDIParent.InstancePtr.CommonDialog1.FilterIndex = 0;
				//MDIParent.InstancePtr.CommonDialog1.ShowSave();
				//strFile = Strings.Trim(MDIParent.InstancePtr.CommonDialog1.FileName);
     //           if (Strings.Trim(strFile) == string.Empty)
					//return ExportODonnell;
                //FC:FINAL:SBE - replace SaveFileDialog with Download file
                //if (File.Exists(strFile))
                //{
                //	File.Delete(strFile);
                //}
                // strSQL = "select * from master where not rsdeleted = 1 "
                strSQL = strMasterJoin + " where not rsdeleted = 1 ";
				strSortField = "rsaccount";
				if (cmbRange.Text == "Range")
				{
					if (cmbType.Text == "Map Lot")
					{
						strSortField = "rsmaplot";
						strSQL += " and rsmaplot between '" + txtStart.Text + "' and '";
						if (Strings.Trim(txtEnd.Text) == string.Empty)
						{
							strSQL += txtStart.Text + "zzz'";
						}
						else
						{
							strSQL += txtEnd.Text + "zzz'";
						}
					}
					else
					{
						if (Conversion.Val(txtEnd.Text) == 0)
						{
							strSQL += " and rsaccount = " + FCConvert.ToString(Conversion.Val(txtStart.Text));
						}
						else
						{
							strSQL += " and rsaccount between " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtEnd.Text));
						}
					}
				}
				strSQL += " order by " + strSortField;
				strExport = "qTrioExportAll";
				if (chkNameAddress.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolNameAddress = true;
				}
				else
				{
					strExport = "qTrioExportProperty";
					boolNameAddress = false;
				}
				if (chkProperty.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolProperty = true;
				}
				else
				{
					boolProperty = false;
					strExport = "qTrioExportName";
				}
				clsXMLExport xDoc = new clsXMLExport();
				string strDir;
				string strFil;
				string strRecord = "";
				string strTemp = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				//strDir = Directory.GetParent(strFile).FullName;
                strDir = Path.Combine(FCFileSystem.Statics.UserDataFolder, "OdonnellExport");
                if (!Directory.Exists(strDir))
                {
                    Directory.CreateDirectory(strDir);
                }
                strFile = Path.Combine(strDir, "ExportODonnell.xml");
                if (File.Exists(strFile))
                {
                    File.Delete(strFile);
                }
                strFil = new FileInfo(strFile).Name;
				lngCount = 0;
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (rsLoad.EndOfFile())
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ExportODonnell;
				}
				xDoc.RootElement = "dataroot";
				xDoc.RootElementAttributeXML = " xmlns:od = " + FCConvert.ToString(Convert.ToChar(34)) + "urn:schemas-microsoft-com:officedata" + FCConvert.ToString(Convert.ToChar(34)) + " generated = " + FCConvert.ToString(Convert.ToChar(34)) + Strings.Format(DateTime.Today, "yyyy-MM-dd") + "T" + Strings.Format(DateTime.Now, "h:mm:ss") + FCConvert.ToString(Convert.ToChar(34)) + " ";
				if (!xDoc.StartDoc(ref strDir, ref strFil))
				{
					MessageBox.Show("Could not create XML document", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ExportODonnell;
				}
				while (!rsLoad.EndOfFile())
				{
					lblProgress.Text = "Saving " + rsLoad.Get_Fields(strSortField);
					lblProgress.Refresh();
					strRecord = "";
					xDoc.CreateElement("TrioAccountNumber", FCConvert.ToString(rsLoad.Get_Fields_Int32("rsaccount")));
					strRecord += xDoc.GetElementXML();
					xDoc.CreateElement("Key", rsLoad.Get_Fields_String("rsmaplot"));
					strRecord += xDoc.GetElementXML();
					if (boolNameAddress)
					{
						xDoc.CreateElement("OwnerName1", rsLoad.Get_Fields_String("rsname"));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("OwnerName2", rsLoad.Get_Fields_String("rssecowner"));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("AddressLine1", rsLoad.Get_Fields_String("rsaddr1"));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("AddressLine2", rsLoad.Get_Fields_String("rsaddr2"));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("City", rsLoad.Get_Fields_String("rsaddr3"));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("State", rsLoad.Get_Fields_String("rsstate"));
						strRecord += xDoc.GetElementXML();
						strTemp = FCConvert.ToString(rsLoad.Get_Fields_String("rszip"));
						if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rszip4"))) != string.Empty)
						{
							strTemp += " " + Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rszip4")));
						}
						xDoc.CreateElement("ZIP", strTemp);
						strRecord += xDoc.GetElementXML();
					}
					if (boolProperty)
					{
						if (Conversion.Val(rsLoad.Get_Fields_String("rslocnumalph")) > 0)
						{
							xDoc.CreateElement("StreetNumber", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_String("rslocnumalph"))));
						}
						else
						{
							xDoc.CreateElement("Streetnumber", "");
						}
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("StreetName", Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rslocstreet"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("LandValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("lastlandval"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("OtherLandValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rsothervalue"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("HardWoodValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rshardvalue"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("MixedWoodValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rsmixedvalue"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("SoftWoodValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rssoftvalue"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("BuildingValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("lastbldgval"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("ExemptionValue", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("rlexemption"))));
						strRecord += xDoc.GetElementXML();
						rsTemp.OpenRecordset("select top 1 * from bookpage where [current] = 1 and account = " + rsLoad.Get_Fields_Int32("rsaccount") + " order by line", modGlobalVariables.strREDatabase);
						if (!rsTemp.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
							xDoc.CreateElement("Book", rsTemp.Get_Fields("book"));
							strRecord += xDoc.GetElementXML();
							// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
							xDoc.CreateElement("Page", rsTemp.Get_Fields("page"));
							strRecord += xDoc.GetElementXML();
						}
						xDoc.CreateElement("TotalAcres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("piacres"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("HardwoodAcres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("rshard"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("SoftwoodAcres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("rssoft"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("MixedwoodAcres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("rsmixed"))));
						strRecord += xDoc.GetElementXML();
						xDoc.CreateElement("OtherLandAcres", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("rsother"))));
						strRecord += xDoc.GetElementXML();
					}
					strRecord = "<" + strExport + ">" + strRecord;
					strRecord += "</" + strExport + ">";
					xDoc.AddToDoc(ref strRecord);
					lngCount += 1;
					rsLoad.MoveNext();
				}
				lblProgress.Text = "";
				if (!xDoc.EndDoc())
				{
					return ExportODonnell;
				}
				ExportODonnell = true;
				MessageBox.Show(FCConvert.ToString(lngCount) + " records exported to file " + strFil, "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FCUtils.Download(strFile, strFil);
				return ExportODonnell;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lblProgress.Text = "";
				if (Information.Err(ex).Number != 32755)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ExportODonnell", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return ExportODonnell;
		}

		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						Frame3.Visible = false;
						break;
					}
				case 1:
					{
						Frame3.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}
	}
}
