﻿//Fecher vbPorter - Version 1.0.0.40
using System.Runtime.InteropServices;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	public class modGlobalVariables
	{
		//=========================================================
		public const string strCLDatabase = "twcl0000.vb1";
		public const string strUTDatabase = "twut0000.vb1";

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}

		public class StaticVariables
		{
			// Public strLastWindowInFocus As String
			public string gstrArchiveParentDir = "";

			public clsCostFiles clsCostRecords = new clsCostFiles();
            public clsLandType LandTypes = new clsLandType();

			public int gintPassQuestion;
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsCostRec = new clsDRWrapper();
			public clsDRWrapper rsCostRec_AutoInitialized = null;
			public clsDRWrapper rsCostRec
			{
				get
				{
					if ( rsCostRec_AutoInitialized == null)
					{
						 rsCostRec_AutoInitialized = new clsDRWrapper();
					}
					return rsCostRec_AutoInitialized;
				}
				set
				{
					 rsCostRec_AutoInitialized = value;
				}
			}
			public bool boolInPendingMode;
			public bool boolInArchiveMode;
			public string strGNDatabase = "";
			public string strRECostFileDatabase = string.Empty;
			public string strTranCodeTownCode = string.Empty;
			public string strBLDatabase = string.Empty;
			public string strCommitDB = string.Empty;
			public bool gboolPrintAsMailing;
			public int gintBasis;
			public double dblOverPayRate;
			public double dblDefaultInterestRate;
			public double gdblCMFAdjustment;
			public double gdblLabelsAdjustment;
			public double gdblLabelsAdjustmentDMH;
			public double gdblLabelsAdjustmentDMV;
			public bool gboolShowLastCLAccountInCR;
			public string gstrLastYearBilled = "";
			public int wmainresponse;
			public string strGetPath = string.Empty;
			public int gintStartPage;
			public int gintEndPage;
			public bool boolFromBilling;
			// vbPorter upgrade warning: glngSaleID As int	OnWrite(string, int)
			public int glngSaleID;
			// Public SoftType As Long
			// Public HardType As Long
			// Public MixedType As Long
			public double[] SoftAcresArray = null;
			public double[] HardAcresArray = null;
			public double[] MixedAcresArray = null;
			public double[] SoftValueArray = null;
			public double[] HardValueArray = null;
			public double[] MixedValueArray = null;
			public double SoftAcres;
			public double HardAcres;
			public double MixedAcres;
			public double SoftValue;
			public double HardValue;
			public double MixedValue;
			public double[] Acres = new double[7 + 1];
			public int AcresIndex;
			public object strCode;
			// BW QUICKFIX 01/04/00 :Temp Fix, Compile errors
			public int Zone;
			// Public searchDB As DAO.Database
			public clsDRWrapper searchRS;
			public bool boolRegRecords;
			// if false you are accessing from the sales record tables
			public bool boolPreviousOwnerRecords;
			public string gcurrsaledate = string.Empty;
			public clsDRWrapper RSRangeSR;
			public int intSearchPosition;
			public bool boolUpdateWhenCalculate;
			public clsDRWrapper gRstemp;
			public bool CancelledIt;
			// tells whether the user cancelled it or not
			// vbPorter upgrade warning: gintAutoID As int	OnWrite(string)
			public int gintAutoID;
			//public bool boolDotMatrix;
			// if true then using an old dotmatrix that prints
			// slow so use print #40 instead of active reports
			public bool boolFromOutbuilding;
			public int intSFLAOption;
			public bool boolShowDel;
			public bool boolIncPrevOwner;
			// vbPorter upgrade warning: CRCUnit As int	OnWrite(string)
			public int CRCUnit;
			// vbPorter upgrade warning: CRCBase As int	OnWrite(string)
			public int CRCBase;
			public bool boolMobileHomePresent;
			// vbPorter upgrade warning: intLastMobileHomeDone As Variant, short --> As int	OnWriteFCConvert.ToInt16(
			public int intLastMobileHomeDone;
			// In a Modules Declarations section
			// Public secDB As DAO.Database
			// vbPorter upgrade warning: reporttotals As double(,)	OnRead(string)
			public double[,] reporttotals;
			public clsDRWrapper rs;
			// Public DB As DAO.Database
			public int gintLastAccountNumber;
			public string gstrCallCalculate = string.Empty;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public SalesAnalysisRec SalesAnalRec = new SalesAnalysisRec(0);
			public bool gboolloadingLS;
			public bool boolRange;
			public object gobjLabel;
			public bool boolPasswordGood;
			public string gstrPassword = "";
			public int gstrUserIndexID;
			public object gobjFormName;
			public string strPreviousForm = "";
			public string strCurrentForm = "";
			public string strFormName = "";
			public bool gboolAllAccounts;
			public int gintMaxAccounts;
			// vbPorter upgrade warning: gintMinAccountRange As int	OnWrite(int, string)	OnRead(string, int)
			public int gintMinAccountRange;
			// vbPorter upgrade warning: gintMaxAccountRange As int	OnWrite(int, string)	OnRead(string)
			public int gintMaxAccountRange;
			public int gintMinAccount;
			public bool gboolRange;
			public bool boolByRange;
			public string strSQL = string.Empty;
			public bool GettingNewAccountNumber;
			public bool GetAccountSearch;
			public bool InvalidAccountNumber;
			public bool AddNewAccountFlag;
			public bool Calculating;
			// vbPorter upgrade warning: Quit As string	OnWrite(bool)
			public string Quit = string.Empty;
			public bool DataChanged;
			public bool ComingFromTheHelpScreen;
			public bool OptClickIgnore;
			public bool AddingACard;
			public bool FormLoaded;
			public bool OutbuildingFormLoaded;
			public bool CommercialFormLoaded;
			public bool IncomeFormLoaded;
			public bool DwellingFormLoaded;
			public bool AddButton;
			public bool SaveButton;
			public bool optClick;
			public bool NextSet;
			public bool PrevSet;
			public bool DosFlag;
			public bool UnloadingForms;
			public bool bolSubMain;
			public bool gboolDefaults;
			public object hold;
			// vbPorter upgrade warning: HoldDwelling As object	OnWrite(string, object)
			public object HoldDwelling;
			public int MaxAccounts;
			public int WindowHandle;
			// Public MKEY                     As Long
			public int CurrentAccountKey;
			public int CurrentAccount;
			// Public MotherAccountNumber      As Long
			public int HoldAcct1;
			
			public int lngCurrentAccount;
			public clsDRWrapper MR2;
			
			public int ActiveCard;
			public string NewCard = "";
			public int PreviousActiveCard;
			// used in addacard routine to determine active card when button is clicked
			public int intSearchScreen;
			public int SFLAOption;
			public int AcreOption;
			public int RoundingVar;
			public int Rounding2;
			public int HoldRounding;
			public int ValuationReportOption;
			public int DepYrCm;
			public int intCurrentCard;
			public int DepreciationYear;
			public int DepreciationYearMoHo;
			public bool boolRE;
			public string TownName = string.Empty;
			// Public olsfl(700)                As Double
			public bool boolfromcalcandassessment;
			public bool boolSeparateLandMethod;
			public object clsCommCost;
			public double dbldwellfunctional;
			public double dbldwellphys;
			public double dbldwelleco;
			public int whatinclude;
			public int lengthoffield;
			public bool boolinctot;
			public bool boolBillingorCorrelated;
			public clsCostRecord[] CostArray = new clsCostRecord[3000 + 1];

			public clsCostRecord CostRec;
			public clsCostRecord ComCostRec;
			public clsLandTableRecord LandTRec;
			public clsLandScheduleRecord LandSRec;
            public clsLandScheduleRecord[] LandSArray = null;
			public string[] RSZRange = new string[30 + 1];
			public string[] RSZFact = new string[30 + 1];
			public bool boolLandTChanged;
			public bool boolLandSChanged;
			public bool boolCostRecChanged;
			public bool boolRSZChanged;
			public bool boolComCostChanged;
			// Public wrkWorkSpace As Workspace
			public bool boolUseArrays;
			// Public clsSecurityClass As clsTrioSecurity
			public bool boolFromSalesAnalysis;
			// Public CustomizedInfo As CustomizedStuff
			public clsCustomize CustomizedInfo = new clsCustomize();
			public int success;
		}

		public static double Round(double dValue, short iDigits)
		{
			return modGlobalRoutines.Round(dValue, iDigits);
		}

		
		// Public strREConnectionString As String
		public const string DEFAULTDATABASE = "twRE0000.vb1";
		public const int INFINITE = -1;
		public const uint SYNCHRONIZE = 0x100000;

		[DllImport("kernel32")]
		public static extern int CloseHandle(int hObject);

		[DllImport("kernel32")]
		public static extern int OpenProcess(uint dwDesiredAccess, int bInheritHandle, int dwProcessId);

		[DllImport("kernel32")]
		public static extern int WaitForSingleObject(int hHandle, int dwMilliseconds);
		#if Win32
		[DllImport("user32")]
		public static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

		// For holding the return value, Must be long in 32-Bit
		
#else
		
		public int success;
		// For holding the return value, Must be Integer in 16-Bit

		#endif
		
		public static string FormatYear(string strYear)
		{
			string FormatYear = "";
			if (Strings.InStr(1, strYear, "-", CompareConstants.vbBinaryCompare) > 0)
			{
				FormatYear = Strings.Left(strYear, 4) + Strings.Right(strYear, 1);
			}
			else
			{
				FormatYear = Strings.Left(strYear, 4) + "-" + Strings.Right(strYear, 1);
			}
			return FormatYear;
		}

		public const string strPPDatabase = "twpp0000.vb1";
		public const string strREDatabase = "RealEstate";
		public const int CNSTENTRYINTOREALESTATE = 1;
		public const int CNSTACCEPTCORRELATION = 9;
		public const int CNSTMNUCALCULATE = 63;
		public const int CNSTAUDITBILLING = 71;
		public const int CNSTCALCEXEMPTION = 21;
		public const int CNSTSUMMARY = 8;
		public const int CNSTTRANSFERTOBILLING = 13;
		public const int CNSTMNUCOSTUPDATE = 24;
		public const int CNSTMNUINCAPPROACH = 68;
		public const int CNSTMNUUPDATE = 30;
		public const int CNSTCUSTOMIZE = 7;
		public const int CNSTFILEMAINTENANCE = 69;
		public const int CNSTASSESSINGREPORTS = 83;
		public const int CNSTBACKUP = 6;
		public const int CNSTCSZDEFAULTS = 81;
		public const int CNSTDATABASEEXTRACT = 84;
		public const int CNSTEXTRACTTODISK = 85;
		public const int CNSTPURGESALESREC = 70;
		public const int CNSTREALESTATESETTINGS = 82;
		public const int CNSTRESTORE = 5;
		public const int CNSTTRANSFERTOSECONDOWNER = 87;
		public const int CNSTUNDELETEACCOUNTS = 67;
		public const int CNSTUSEREXTRACT = 86;
		public const int CNSTLONGSCREEN = 16;
		public const int CNSTADDACCOUNTS = 41;
		public const int CNSTADDCARDS = 42;
		public const int CNSTCALCACCOUNTS = 27;
		public const int CNSTCOMMENTS = 26;
		public const int CNSTDELACCOUNTS = 35;
		public const int CNSTDELCARDS = 36;
		public const int CNSTDELDWELLCOMM = 50;
		public const int CNSTCOMMERCIAL = 76;
		public const int CNSTDWELLING = 75;
		public const int CNSTOUTBUILDING = 77;
		public const int CNSTEDITPROPERTY = 78;
		public const int CNSTEDITSALEINFO = 93;
		public const int CNSTSAVEACCOUNTS = 39;
		public const int CNSTVIEWPICSKETCH = 40;
		public const int CNSTVIEWSKETCHES = 94;
		public const int CNSTPRINTROUTINES = 89;
		public const int CNSTEXTRACTFORLISTINGSANDLABELS = 92;
		public const int CNSTLABELS = 91;
		public const int CNSTLISTINGS = 90;
		public const int CNSTSALESANALYSIS = 22;
		public const int CNSTSALESRECORDS = 65;
		public const int CNSTSHORTSCREEN = 15;
		public const int CNSTEDITADDRESSES = 62;
		public const int CNSTEDITLANDBLDGEXMT = 61;
		public const int CNSTCOSTFILES = 20;
		public const int CNSTEXEMPTIONCODES = 88;
		public const int CNSTFRACACREAGE = 52;
		public const int CNSTLANDCATTABLE = 53;
		public const int CNSTLANDSCHEDTABLE = 54;
		public const int CNSTOUTSQFTTABLE = 55;
		public const int CNSTRESSIZEFACT = 56;
		public const int CNSTADDPICSKETCH = 34;
		public const int CNSTDELPICSKETCH = 37;
		public const int CNSTADDSKETCH = 96;
		public const int CNSTDELSKETCH = 97;
		public const int CNSTINCOMEAPPROACH = 98;
		public const int CNSTEDITMASTERDIRECTLY = 99;
		public const int CNSTGROUPMAINT = 103;
		public const int CNSTMORTGAGEMAINT = 102;
		public const int CNSTASSOCIATION = 100;
		public const int CNSTEDITGROUPS = 107;
		public const int CNSTEDITMORTGAGE = 105;
		public const int CNSTIMPORTEXPORT = 108;
		public const int CNSTIMEXNAMEETC = 109;
		public const int CNSTIMEXPOCKETPC = 110;
		public const int CNSTRUNARCHIVES = 111;
		public const int CNSTCREATEARCHIVES = 112;
		public const int CNSTTAXRATECALCULATOR = 113;
		public const int RE_Permission_PropertyDocuments = 114;
		public const int RE_Permission_EditPropertyDocuments = 115;
		public const int CNSTMVRDATATYPENUMBER = 0;
		public const int CNSTMVRDATATYPETEXT = 1;
		public const int CNSTMVRDATATYPEBOOLEAN = 2;
		public const int CNSTMVRDATATYPEDATE = 3;
		public const int CNSTMVRDATATYPETABLE = 4;
		public const int CNSTMVRDATATYPELIST = 5;
		public const int CNSTMVRDATATYPERICHTEXT = 6;
		public const int CNSTROUNDASOPTION = 0;
		public const int CNSTROUNDNEXTOPTION = 1;
		public const int CNSTROUNDHALFOPTION = 2;
		public const int CNSTREPORTTYPETAXACQUIRED = 1;
		public const int CNSTREPORTTYPEBANKRUPTCY = 2;
		// Public dcChildList As clsDRWrapper
		public struct CustomizedStuff
		{
			public short Round;
			public short MHDepreciationYear;
			public short RoundContingency;
			public short ValReportOption;
			public short AcreOption;
			public short HomesteadCode1;
			public short HomesteadCode2;
			public short HomesteadCode3;
			public short HomesteadCode4;
			public short HomesteadCode5;
			public double CertifiedRatio;
			public short DepreciationYear;
			public string LandUnitType;
			public short SquareFootLivingArea;
			public short CommDepreciationYear;
			//public short DotMatrixPrinter;
			public int Softwood;
			public int Mixedwood;
			public int Hardwood;
			public double BillRate;
			public int BillYear;
			public short Ref1BookPage;
			public bool boolShowPicOnVal;
			public bool boolShowFirstPiconVal;
			public bool boolShowSketchOnVal;
			public bool boolAlwaysShowCurrentOnPropertyCard;
			public bool boolAlwaysShowCurrentCurrentOnPropertyCard;
			public bool boolNeverShowPreviousBillingOnPropertyCard;
			public bool boolDontShowCommentsOnPropertyCard;
			public bool boolPocketPCExternal;
			public bool boolRoundCalculatedHomesteads;
			public string FarmLandCodes;
			// vbPorter upgrade warning: OpenCode As string	OnWriteFCConvert.ToInt32(
			public string OpenCode;
			public bool boolApplyHomesteadToCardOneOnly;
			public bool boolDontApplyLandFactorToTreeGrowth;
			public bool boolApplyCertifiedRatioToTreeGrowth;
			public bool boolPrintSecondaryPicOnPropertyCard;
			public bool boolPrintMapLotOnSide;
			public short PPCat1TaxValCat;
			public short PPCat2TaxValCat;
			public short PPCat3TaxValCat;
			public short PPCat4TaxValCat;
			public short PPCat5TaxValCat;
			public short PPCat6TaxValCat;
			public short PPCat7TaxValCat;
			public short PPCat8TaxValCat;
			public short PPCat9TaxValCat;
			public int REIndPropBldgCode;
			public string REIndPropBldgCodes;
			public short PPIndPropCat;
			public bool boolRegionalTown;
			public bool boolAccountOnBottom;
			public bool boolHideLabelAccount;
			public bool boolShowCodesOnAccountScreen;
			public bool boolDisableToolTips;
			public bool boolShowShortDesc;
			public int DefaultSchedule;
			public bool boolMostRecentBP;
			public bool boolHideValuationRatio;
			public bool boolHidePicsForPropertyCardRanges;
			public short RXAutoUpdateDay;
			public short RXAutoUpdateMonth;
			public short RXAutoLastUpdateYear;
			public bool boolREWebIncludeRef1;
			public bool boolREWebIncludeRef2;
			public string strRef1Description;
			public string strRef2Description;
			public bool boolShowGridLines;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public CustomizedStuff(int unusedPram)
			{
				this.Round = 0;
				this.MHDepreciationYear = 0;
				this.RoundContingency = 0;
				this.ValReportOption = 0;
				this.AcreOption = 0;
				this.HomesteadCode1 = 0;
				this.HomesteadCode2 = 0;
				this.HomesteadCode3 = 0;
				this.HomesteadCode4 = 0;
				this.HomesteadCode5 = 0;
				this.CertifiedRatio = 0;
				this.DepreciationYear = 0;
				this.LandUnitType = string.Empty;
				this.SquareFootLivingArea = 0;
				this.CommDepreciationYear = 0;
				//this.DotMatrixPrinter = 0;
				this.Softwood = 0;
				this.Mixedwood = 0;
				this.Hardwood = 0;
				this.BillRate = 0;
				this.BillYear = 0;
				this.Ref1BookPage = 0;
				this.boolShowPicOnVal = false;
				this.boolShowFirstPiconVal = false;
				this.boolShowSketchOnVal = false;
				this.boolAlwaysShowCurrentOnPropertyCard = false;
				this.boolAlwaysShowCurrentCurrentOnPropertyCard = false;
				this.boolNeverShowPreviousBillingOnPropertyCard = false;
				this.boolDontShowCommentsOnPropertyCard = false;
				this.boolPocketPCExternal = false;
				this.boolRoundCalculatedHomesteads = false;
				this.FarmLandCodes = string.Empty;
				// vbPorter upgrade warning: OpenCode As string	OnWriteFCConvert.ToInt32(
				this.OpenCode = string.Empty;
				this.boolApplyHomesteadToCardOneOnly = false;
				this.boolDontApplyLandFactorToTreeGrowth = false;
				this.boolApplyCertifiedRatioToTreeGrowth = false;
				this.boolPrintSecondaryPicOnPropertyCard = false;
				this.boolPrintMapLotOnSide = false;
				this.PPCat1TaxValCat = 0;
				this.PPCat2TaxValCat = 0;
				this.PPCat3TaxValCat = 0;
				this.PPCat4TaxValCat = 0;
				this.PPCat5TaxValCat = 0;
				this.PPCat6TaxValCat = 0;
				this.PPCat7TaxValCat = 0;
				this.PPCat8TaxValCat = 0;
				this.PPCat9TaxValCat = 0;
				this.REIndPropBldgCode = 0;
				this.REIndPropBldgCodes = string.Empty;
				this.PPIndPropCat = 0;
				this.boolRegionalTown = false;
				this.boolAccountOnBottom = false;
				this.boolHideLabelAccount = false;
				this.boolShowCodesOnAccountScreen = false;
				this.boolDisableToolTips = false;
				this.boolShowShortDesc = false;
				this.DefaultSchedule = 0;
				this.boolMostRecentBP = false;
				this.boolHideValuationRatio = false;
				this.boolHidePicsForPropertyCardRanges = false;
				this.RXAutoUpdateDay = 0;
				this.RXAutoUpdateMonth = 0;
				this.RXAutoLastUpdateYear = 0;
				this.boolREWebIncludeRef1 = false;
				this.boolREWebIncludeRef2 = false;
				this.strRef1Description = string.Empty;
				this.strRef2Description = string.Empty;
				this.boolShowGridLines = false;
			}
		};

		public struct TownSpecificCustomize
		{
			public double CertifiedRatio;
			public double BillRate;
			public double BillYear;
			public int DefaultSchedule;
			public string MuniName;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public TownSpecificCustomize(int unusedPram)
			{
				this.CertifiedRatio = 0;
				this.BillRate = 0;
				this.BillYear = 0;
				this.DefaultSchedule = 0;
				this.MuniName = string.Empty;
			}
		};

		public struct SalesAnalysisRec
		{
			public float AveDev;
			public float StandDev;
			public float Median;
			public float mean;
			public float MidQuart;
			public float Weighted;
			public float CoeffOfDisp;
			public float CoeffofVar;
			public float PriceDifferential;
			public float SaleFactor;
			public float LandFactor;
			public float BldgFactor;
			public int TotSale;
			public int TotValuation;
			public int TotDeviation;
			public float MeantoUse;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public SalesAnalysisRec(int unusedParam)
			{
				this.AveDev = 0;
				this.StandDev = 0;
				this.Median = 0;
				this.mean = 0;
				this.MidQuart = 0;
				this.Weighted = 0;
				this.CoeffOfDisp = 0;
				this.CoeffofVar = 0;
				this.PriceDifferential = 0;
				this.SaleFactor = 0;
				this.LandFactor = 0;
				this.BldgFactor = 0;
				this.TotSale = 0;
				this.TotValuation = 0;
				this.TotDeviation = 0;
				this.MeantoUse = 0;
			}
		};

		public struct CustomCodeRec
		{
			public short Code;
			public string FieldName;
			public string TableName;
			public bool SpecialCase;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public CustomCodeRec(int unusedParam)
			{
				this.Code = 0;
				this.FieldName = string.Empty;
				this.TableName = string.Empty;
				this.SpecialCase = false;
			}
		};

		public struct SummaryRecType
		{
			public string SACCT;
			public string strancode;
			// vbPorter upgrade warning: sdwellrcnld As string	OnWriteFCConvert.ToDouble(
			public string sdwellrcnld;
			public short slandc1;
			// vbPorter upgrade warning: slandv1 As int	OnWriteFCConvert.ToSingle(
			public int slandv1;
			public short slandc2;
			// vbPorter upgrade warning: slandv2 As int	OnWriteFCConvert.ToSingle(
			public int slandv2;
			public short slandc3;
			// vbPorter upgrade warning: slandv3 As int	OnWriteFCConvert.ToSingle(
			public int slandv3;
			public short slandc4;
			// vbPorter upgrade warning: slandv4 As int	OnWriteFCConvert.ToSingle(
			public int slandv4;
			public short slandc5;
			// vbPorter upgrade warning: slandv5 As int	OnWriteFCConvert.ToSingle(
			public int slandv5;
			public short slandc6;
			// vbPorter upgrade warning: slandv6 As int	OnWriteFCConvert.ToSingle(
			public int slandv6;
			public short slandc7;
			// vbPorter upgrade warning: slandv7 As int	OnWriteFCConvert.ToSingle(
			public int slandv7;
			public short sobc1;
			public int sobv1;
			public short sobc2;
			public int sobv2;
			public short sobc3;
			public int sobv3;
			public short sobc4;
			public int sobv4;
			public short sobc5;
			public int sobv5;
			public short sobc6;
			public int sobv6;
			public short sobc7;
			public int sobv7;
			public short sobc8;
			public int sobv8;
			public short sobc9;
			public int sobv9;
			public short sobc10;
			public int sobv10;
			// vbPorter upgrade warning: ssfla As string	OnWriteFCConvert.ToInt32(
			public string ssfla;
			public string sacres;
			public string sovlandc;
			public string sovlanda;
			public string sovbldgc;
			public string sovbldga;
			public string SDate;
			public string scmocc1;
			public string scmval1;
			public string scmocc2;
			public string scmval2;
			public string sbuilt;
			// vbPorter upgrade warning: sstyle As string	OnWrite(string, short)
			public string sstyle;
			public string Scode;
			public int srecordnumber;
			public double slanda1;
			public double slanda2;
			public double slanda3;
			public double slanda4;
			public double slanda5;
			public double slanda6;
			public double slanda7;
			public int dwellcode;
			public int CardNumber;
			public int sage;
			public int comval1;
			public int comval2;
			public short LandCode;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public SummaryRecType(int unusedPram)
			{
				this.SACCT = string.Empty;
				this.strancode = string.Empty;
				this.sdwellrcnld = string.Empty;
				this.slandc1 = 0;
				this.slandv1 = 0;
				this.slandc2 = 0;
				this.slandv2 = 0;
				this.slandc3 = 0;
				this.slandv3 = 0;
				this.slandc4 = 0;
				this.slandv4 = 0;
				this.slandc5 = 0;
				this.slandv5 = 0;
				this.slandc6 = 0;
				this.slandv6 = 0;
				this.slandc7 = 0;
				this.slandv7 = 0;
				this.sobc1 = 0;
				this.sobv1 = 0;
				this.sobc2 = 0;
				this.sobv2 = 0;
				this.sobc3 = 0;
				this.sobv3 = 0;
				this.sobc4 = 0;
				this.sobv4 = 0;
				this.sobc5 = 0;
				this.sobv5 = 0;
				this.sobc6 = 0;
				this.sobv6 = 0;
				this.sobc7 = 0;
				this.sobv7 = 0;
				this.sobc8 = 0;
				this.sobv8 = 0;
				this.sobc9 = 0;
				this.sobv9 = 0;
				this.sobc10 = 0;
				this.sobv10 = 0;
				this.ssfla = string.Empty;
				this.sacres = string.Empty;
				this.sovlandc = string.Empty;
				this.sovlanda = string.Empty;
				this.sovbldgc = string.Empty;
				this.sovbldga = string.Empty;
				this.SDate = string.Empty;
				this.scmocc1 = string.Empty;
				this.scmval1 = string.Empty;
				this.scmocc2 = string.Empty;
				this.scmval2 = string.Empty;
				this.sbuilt = string.Empty;
				this.sstyle = string.Empty;
				this.Scode = string.Empty;
				this.srecordnumber = 0;
				this.slanda1 = 0;
				this.slanda2 = 0;
				this.slanda3 = 0;
				this.slanda4 = 0;
				this.slanda5 = 0;
				this.slanda6 = 0;
				this.slanda7 = 0;
				this.dwellcode = 0;
				this.CardNumber = 0;
				this.sage = 0;
				this.comval1 = 0;
				this.comval2 = 0;
				this.LandCode = 0;
			}
		};

		public struct CalcPropertyType
		{
			public int Account;
			public short CARD;
			public string DwellingOrCommercial;
			public int DwellRcnld;
			public short LandCode1;
			public int[] LandType/*?  = new int[7 + 1] */;
			public string[] LandUnits/*?  = new string[7 + 1] */;
			public string[] LandDesc/*?  = new string[7 + 1] */;
			public string[] LandPricePerUnit/*?  = new string[7 + 1] */;
			public string[] LandTotal/*?  = new string[7 + 1] */;
			public string[] LandFctr/*?  = new string[7 + 1] */;
			public string[] LandInfluence/*?  = new string[7 + 1] */;
			public string[] LandValue/*?  = new string[7 + 1] */;
			public string[] LandAcreage/*?  = new string[7 + 1] */;
			public string[] LandMethod/*?  = new string[7 + 1] */;
			public string Acres;
			public string TotalLandValue;
			public string PerAcreVal;
			public short OverrideLandCode;
			public int OverrideLandValue;
			public short OverrideBldgCode;
			public int OverrideBldgValue;
			public string SaleDate;
			public string SalePrice;
			public string SaleType;
			public string Financing;
			public string Verified;
			public string Validity;
			public short TranCode;
			public short LandCode;
			public short BldgCode;
			public string TranLandBldgCode;
			public string TranLandBldgDesc;
			public short NeighborhoodCode;
			public string NeighborhoodDesc;
			public string TreeGrowth;
			public string Zone;
			public string Topography;
			public string Utilities;
			public string Street;
			public string Open1;
			public string Open1Title;
			public string Open2;
			public string Open2Title;
			public string Ref1;
			public string Ref2;
			public string XCoord;
			public string XCoordTitle;
			public string YCoord;
			public string YCoordTitle;
			public string ExemptCodes;
			// vbPorter upgrade warning: LandSchedule As int	OnWrite(short, double)
			public int LandSchedule;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public CalcPropertyType(int unusedParam)
			{
				this.Account = 0;
				this.CARD = 0;
				this.DwellingOrCommercial = string.Empty;
				this.DwellRcnld = 0;
				this.LandCode1 = 0;
				this.LandType = new int[8];
				this.LandUnits = new string[8];
				this.LandDesc = new string[8];
				this.LandPricePerUnit = new string[8];
				this.LandTotal = new string[8];
				this.LandFctr = new string[8];
				this.LandInfluence = new string[8];
				this.LandValue = new string[8];
				this.LandAcreage = new string[8];
				this.LandMethod = new string[8];
				this.Acres = string.Empty;
				this.TotalLandValue = string.Empty;
				this.PerAcreVal = string.Empty;
				this.OverrideLandCode = 0;
				this.OverrideLandValue = 0;
				this.OverrideBldgCode = 0;
				this.OverrideBldgValue = 0;
				this.SaleDate = string.Empty;
				this.SalePrice = string.Empty;
				this.SaleType = string.Empty;
				this.Financing = string.Empty;
				this.Verified = string.Empty;
				this.Validity = string.Empty;
				this.TranCode = 0;
				this.LandCode = 0;
				this.BldgCode = 0;
				this.TranLandBldgCode = string.Empty;
				this.TranLandBldgDesc = string.Empty;
				this.NeighborhoodCode = 0;
				this.NeighborhoodDesc = string.Empty;
				this.TreeGrowth = string.Empty;
				this.Zone = string.Empty;
				this.Topography = string.Empty;
				this.Utilities = string.Empty;
				this.Street = string.Empty;
				this.Open1 = string.Empty;
				this.Open1Title = string.Empty;
				this.Open2 = string.Empty;
				this.Open2Title = string.Empty;
				this.Ref1 = string.Empty;
				this.Ref2 = string.Empty;
				this.XCoord = string.Empty;
				this.XCoordTitle = string.Empty;
				this.YCoord = string.Empty;
				this.YCoordTitle = string.Empty;
				this.ExemptCodes = string.Empty;
				this.LandSchedule = 0;
			}
		};

		public struct CalcOutBuildingType
		{
			public int Account;
			public short CARD;
			public int[] OutbuildingType/*?  = new int[10 + 1] */;
			public string[] OutbuildingValue/*?  = new string[10 + 1] */;
			public string[] Description/*?  = new string[10 + 1] */;
			public int[] Year/*?  = new int[10 + 1] */;
			// vbPorter upgrade warning: Units As string	OnWrite(string, int)
			public string[] Units/*?  = new string[10 + 1] */;
			public string[] Grade/*?  = new string[10 + 1] */;
			// vbPorter upgrade warning: Recon As int	OnWrite(string)
			public int[] Recon/*?  = new int[10 + 1] */;
			public string[] Condition/*?  = new string[10 + 1] */;
			public string[] Physcial/*?  = new string[10 + 1] */;
			public string[] Functional/*?  = new string[10 + 1] */;
			public string[] Economic/*?  = new string[10 + 1] */;
			public string TotalOutbuilding;
			public string TotalBuilding;
			public string SFLA;
			public string SFLAPerVal;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public CalcOutBuildingType(int unusedParam)
			{
				this.Account = 0;
				this.CARD = 0;
				this.OutbuildingType = new int[11];
				this.OutbuildingValue = new string[11];
				this.Description = new string[11];
				this.Year = new int[11];
				this.Units = new string[11];
				this.Grade = new string[11];
				this.Recon = new int[11];
				this.Condition = new string[11];
				this.Physcial = new string[11];
				this.Functional = new string[11];
				this.Economic = new string[11];
				this.TotalOutbuilding = string.Empty;
				this.TotalBuilding = string.Empty;
				this.SFLA = string.Empty;
				this.SFLAPerVal = string.Empty;
			}
		};

		public struct CalcDwellingType
		{
			public int Account;
			public short CARD;
			public string BuildingType;
			public string Stories;
			public string StoriesShort;
			public string Exterior;
			public string DwellUnits;
			public string Open3;
			public string Open4;
			public string Open3Title;
			public string Open4Title;
			public string Foundation;
			public string FinBasementArea;
			public string Heating;
			public string Rooms;
			public string Bedrooms;
			public string Bath;
			public string Attic;
			public string Fireplaces;
			public string Insulation;
			public string UnfinLivingArea;
			public string Sqft;
			public string Grade;
			public string MasonryTrim;
			public string Roof;
			public string Basement;
			public string BsmtGarage;
			public string Cooling;
			public string Open5;
			public string Open5Title;
			public string AddnFixtures;
			public string HalfBaths;
			public string Base;
			public string Trim;
			public string RoofVal;
			public string Open3Val;
			public string Open4Val;
			public string BasementVal;
			public string FinBasementVal;
			public string HeatVal;
			public string Open5Val;
			public string PlumbingVal;
			public string AtticVal;
			public string FireplaceVal;
			public string InsulationVal;
			public string UnfinishedVal;
			public string YearBuilt;
			public string Renovated;
			public string Kitchens;
			public string Baths;
			public string Condition;
			public string Layout;
			public string Total;
			public string FuncObs;
			public string EconObs;
			public string PhysObs;
			public string FuncPerc;
			public string EconPerc;
			public string PhysPerc;
			public string DwellValue;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public CalcDwellingType(int unusedParam)
			{
				this.Account = 0;
				this.CARD = 0;
				this.BuildingType = string.Empty;
				this.Stories = string.Empty;
				this.StoriesShort = string.Empty;
				this.Exterior = string.Empty;
				this.DwellUnits = string.Empty;
				this.Open3 = string.Empty;
				this.Open4 = string.Empty;
				this.Open3Title = string.Empty;
				this.Open4Title = string.Empty;
				this.Foundation = string.Empty;
				this.FinBasementArea = string.Empty;
				this.Heating = string.Empty;
				this.Rooms = string.Empty;
				this.Bedrooms = string.Empty;
				this.Bath = string.Empty;
				this.Attic = string.Empty;
				this.Fireplaces = string.Empty;
				this.Insulation = string.Empty;
				this.UnfinLivingArea = string.Empty;
				this.Sqft = string.Empty;
				this.Grade = string.Empty;
				this.MasonryTrim = string.Empty;
				this.Roof = string.Empty;
				this.Basement = string.Empty;
				this.BsmtGarage = string.Empty;
				this.Cooling = string.Empty;
				this.Open5 = string.Empty;
				this.Open5Title = string.Empty;
				this.AddnFixtures = string.Empty;
				this.HalfBaths = string.Empty;
				this.Base = string.Empty;
				this.Trim = string.Empty;
				this.RoofVal = string.Empty;
				this.Open3Val = string.Empty;
				this.Open4Val = string.Empty;
				this.BasementVal = string.Empty;
				this.FinBasementVal = string.Empty;
				this.HeatVal = string.Empty;
				this.Open5Val = string.Empty;
				this.PlumbingVal = string.Empty;
				this.AtticVal = string.Empty;
				this.FireplaceVal = string.Empty;
				this.InsulationVal = string.Empty;
				this.UnfinishedVal = string.Empty;
				this.YearBuilt = string.Empty;
				this.Renovated = string.Empty;
				this.Kitchens = string.Empty;
				this.Baths = string.Empty;
				this.Condition = string.Empty;
				this.Layout = string.Empty;
				this.Total = string.Empty;
				this.FuncObs = string.Empty;
				this.EconObs = string.Empty;
				this.PhysObs = string.Empty;
				this.FuncPerc = string.Empty;
				this.EconPerc = string.Empty;
				this.PhysPerc = string.Empty;
				this.DwellValue = string.Empty;
			}
		};

		public struct CalcCommercialType
		{
			public int Account;
			public short CARD;
			public string[] OccupancyType/*?  = new string[2 + 1] */;
			public string[] ClassQuality/*?  = new string[2 + 1] */;
			public string[] DwellUnits/*?  = new string[2 + 1] */;
			public string[] Exterior/*?  = new string[2 + 1] */;
			public string[] Stories/*?  = new string[2 + 1] */;
			public string[] HeatingCool/*?  = new string[2 + 1] */;
			public string[] Built/*?  = new string[2 + 1] */;
			public string[] Remodeled/*?  = new string[2 + 1] */;
			public string[] BaseCost/*?  = new string[2 + 1] */;
			public string[] HeatCoolSqft/*?  = new string[2 + 1] */;
			public string[] Total/*?  = new string[2 + 1] */;
			public string[] SizeFactor/*?  = new string[2 + 1] */;
			public string[] AdjustedCost/*?  = new string[2 + 1] */;
			public string[] TotalSqft/*?  = new string[2 + 1] */;
			public string[] ReplacementCost/*?  = new string[2 + 1] */;
			public string[] Condition/*?  = new string[2 + 1] */;
			public string[] Physical/*?  = new string[2 + 1] */;
			public string[] Functional/*?  = new string[2 + 1] */;
			public string[] subtotal/*?  = new string[2 + 1] */;
			public string EconFactor;
			public string TotalValue;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public CalcCommercialType(int unusedParam)
			{
				this.Account = 0;
				this.CARD = 0;
				this.OccupancyType = new string[3];
				this.ClassQuality = new string[3];
				this.DwellUnits = new string[3];
				this.Exterior = new string[3];
				this.Stories = new string[3];
				this.HeatingCool = new string[3];
				this.Built = new string[3];
				this.Remodeled = new string[3];
				this.BaseCost = new string[3];
				this.HeatCoolSqft = new string[3];
				this.Total = new string[3];
				this.SizeFactor = new string[3];
				this.AdjustedCost = new string[3];
				this.TotalSqft = new string[3];
				this.ReplacementCost = new string[3];
				this.Condition = new string[3];
				this.Physical = new string[3];
				this.Functional = new string[3];
				this.subtotal = new string[3];
				this.EconFactor = string.Empty;
				this.TotalValue = string.Empty;
			}
		};
		// this will be used to store abatements by period in frmRECLStatus, frmTaxService, Reminder Notices, and Bills
		public struct BillPeriods
		{
			public double Per1;
			public double Per2;
			public double Per3;
			public double Per4;
		};

		public enum CommercialClassCategory
		{
			a = 1,
			B = 2,
			c = 3,
			D = 4,
			s = 5,
		}
	}
}
