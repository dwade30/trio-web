﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class modHelpFiles
	{
		// |---------------------------------------------------------------|
		// |* HKEY is used to determine record number (example: map/lot is |
		// |    record 1 and the HKEY is "0001".                           |
		// |* HDESCRIPTION is used for the help comments and to hold all   |
		// |    necessary information that might help user to understand   |
		// |    the field that has focus.                                  |
		// |* HEXPANSION is an expansion field                             |
		// |* HCODE is used to hold two tilde's  "~~" which signifies then |
		// |  end of a record, each record should have this.               |
		// |---------------------------------------------------------------|
		// vbPorter upgrade warning: RowNum As Variant --> As double	OnWrite(int, double)
		// vbPorter upgrade warning: intTownCode As short	OnWriteFCConvert.ToDouble(
		public static void HelpFiles_2(string ControlName, double RowNum = 0, short intTownCode = 0)
		{
			HelpFiles(ControlName, RowNum, intTownCode);
		}

		public static void HelpFiles_20(string ControlName, double RowNum = 0, short intTownCode = 0)
		{
			HelpFiles(ControlName, RowNum, intTownCode);
		}

		public static void HelpFiles(string ControlName, double RowNum = 0, short intTownCode = 0)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			int x;
			int KEY = 0;
			// vbPorter upgrade warning: strTemp As string	OnWrite(string, short)
			string strTemp = "";
			Statics.Helping = true;
			frmHelp.InstancePtr.List1.Clear();
            //FC:FINAL:MSH - issue #1650: add columns to listbox and restyle them (wisej doesn't support spaces and tabs in ListViewItems)
            frmHelp.InstancePtr.List1.Columns.Add("");
            frmHelp.InstancePtr.List1.Columns.Add("");
            frmHelp.InstancePtr.List1.Columns.Add("");
            frmHelp.InstancePtr.List1.Columns.Add("");
            frmHelp.InstancePtr.List1.Columns.Add("");
            int listWidth = frmHelp.InstancePtr.List1.Width;
            frmHelp.InstancePtr.List1.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.085);
            frmHelp.InstancePtr.List1.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.085);
            frmHelp.InstancePtr.List1.Columns[2].Width = FCConvert.ToInt32(listWidth * 0.161);
            frmHelp.InstancePtr.List1.Columns[3].Width = FCConvert.ToInt32(listWidth * 0.161);
            frmHelp.InstancePtr.List1.Columns[4].Width = FCConvert.ToInt32(listWidth * 0.305);
            frmHelp.InstancePtr.List1.Columns[5].Width = FCConvert.ToInt32(listWidth * 0.175);
            frmHelp.InstancePtr.List1.GridLineStyle = GridLineStyle.None;
            // 
            if (Strings.UCase(ControlName) == "PROPERTY")
			{
				if (RowNum == 6)
				{
					OpenHRandFillTitle_8(6, true);
					StateAbbreviations();
				}
				else if (RowNum == 7)
				{
					OpenHRandFillTitle_2(7);
					frmHelp.InstancePtr.Label1.Text = "TOWN";
					frmHelp.InstancePtr.Label2.Visible = false;
					frmHelp.InstancePtr.Label3.Visible = false;
					frmHelp.InstancePtr.Label4.Text = "ZIP";
					frmHelp.InstancePtr.Label5.Visible = false;
					frmHelp.InstancePtr.Label6.Visible = false;
				}
				else if (RowNum == 22)
				{
					OpenHRandFillTitle_8(22, true);
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1901 and 1999 order by crecordnumber", modGlobalVariables.strREDatabase);
					for (x = 1; x <= 99; x++)
					{
						KEY = (1900 + x);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
						clsLoad.MoveNext();
					}
					// x
				}
				else if (RowNum == 145)
				{
					// entrancecode
					OpenHRandFillTitle_8(FCConvert.ToInt16(RowNum), true);
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1701 and 1709 order by crecordnumber", modGlobalVariables.strREDatabase);
					for (x = 1701; x <= 1709; x++)
					{
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(x, "0000") + "\t" + FCConvert.ToString(x - 1700) + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
						clsLoad.MoveNext();
					}
					// x
				}
				else if (RowNum == 146)
				{
					OpenHRandFillTitle_8(FCConvert.ToInt16(RowNum), true);
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1711 and 1719 order by crecordnumber", modGlobalVariables.strREDatabase);
					for (x = 1711; x <= 1719; x++)
					{
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(x, "0000") + "\t" + FCConvert.ToString(x - 1710) + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
						clsLoad.MoveNext();
					}
					// x
				}
				else if (RowNum == 147)
				{
					OpenHRandFillTitle_8(FCConvert.ToInt16(RowNum), false);
				}
				else
				{
					OpenHRandFillTitle_8(FCConvert.ToInt16(RowNum), false);
				}
			}
			else if (Strings.UCase(ControlName) == "OUTBUILDING")
			{
				OpenHRandFillTitle_8(FCConvert.ToInt16(RowNum), true);
				// Call OpenCRTable(CR)
				if (RowNum == -1)
				{
					// outbuilding factor
					// Call OpenHRandFillTitle(293, False)
					OpenHRandFillTitle_8(305, false);
					// Call clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1721 and 1734 and val(TOWNNUMBER & '') = " & intTownCode & " order by crecordnumber", strREDatabase)
					// For x = 1721 To 1734
					// .List1.AddItem Format(x, "0000") & "\t" & Format((Val(clsLoad.Fields("CUnit")) / 100), "000000.00") & "\t" & Format((Val(clsLoad.Fields("CBase")) / 100), "000000.00") & "\t" & clsLoad.Fields("ClDesc") & "\t" & clsLoad.Fields("CSDesc")
					// clsLoad.MoveNext
					// Next x
				}
				else if (RowNum == -3)
				{
					// depreciation
					OpenHRandFillTitle_8(295, true);
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1791 and 1799 and townnumber = " + FCConvert.ToString(intTownCode) + " order by crecordnumber", modGlobalVariables.strREDatabase);
					for (x = 1791; x <= 1799; x++)
					{
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(x, "0000") + "\t" + Strings.Format(x - 1790, "000") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
						clsLoad.MoveNext();
					}
					// x
				}
				else
				{
					// type
					if (RowNum > 699)
					{
						OpenHRandFillTitle_8(294, true);
					}
					else
					{
						OpenHRandFillTitle_8(301, true);
					}
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1 and 999 and townnumber = " + FCConvert.ToString(intTownCode) + " order by crecordnumber", modGlobalVariables.strREDatabase);
					if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
					{
						if (clsLoad.EndOfFile())
						{
							MessageBox.Show("No outbuilding cost records found for town " + FCConvert.ToString(intTownCode), "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //FC:FINAL:MSH - issue #1651: clear status of the help form
                            Statics.Helping = false;
                            return;
						}
					}
					for (x = 1; x <= 999; x++)
					{
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(x, "0000") + "\t" + Strings.Format(x, "000") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
						clsLoad.MoveNext();
					}
					// x
					frmHelp.InstancePtr.List1.SelectedIndex = FCConvert.ToInt32(Conversion.Val(RowNum) - 1);
				}
			}
			else if (Strings.UCase(ControlName) == "VALUATIONSCREEN")
			{
				OpenHRandFillTitle_2(FCConvert.ToInt16(RowNum));
			}
			else if (Strings.UCase(ControlName) == "INCOMEAPPROACH")
			{
				if (RowNum == 1)
				{
					// occupancy codes
					frmHelp.InstancePtr.txtHelpComments.Text = "";
					frmHelp.InstancePtr.Label1.Text = "   Code";
					frmHelp.InstancePtr.Label6.Text = "    Description";
					frmHelp.InstancePtr.Label3.Text = "";
					frmHelp.InstancePtr.Label2.Text = "";
					frmHelp.InstancePtr.Label4.Visible = false;
					frmHelp.InstancePtr.Label5.Visible = false;
                    //FC:FINAL:MSH - format columns for displaying data as in original app
                    frmHelp.InstancePtr.List1.Clear();
                    frmHelp.InstancePtr.List1.Columns.Add("");
                    frmHelp.InstancePtr.List1.Columns.Add("");
                    frmHelp.InstancePtr.List1.Columns.Add("");
                    frmHelp.InstancePtr.List1.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.04);
                    frmHelp.InstancePtr.List1.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.1);
                    frmHelp.InstancePtr.List1.Columns[2].Width = FCConvert.ToInt32(listWidth * 0.35);
                    frmHelp.InstancePtr.List1.Columns[3].Width = FCConvert.ToInt32(listWidth * 0.2);
                    frmHelp.InstancePtr.List1.GridLineStyle = GridLineStyle.None;
                    clsLoad.OpenRecordset("select * from occupancycodes order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						frmHelp.InstancePtr.List1.AddItem("\t" + Strings.Format(clsLoad.Get_Fields("code"), "0000") + "\t" + clsLoad.Get_Fields_String("Description") + "\t" + clsLoad.Get_Fields_Double("rate"));
						clsLoad.MoveNext();
					}
				}
				else if (RowNum == 2)
				{
					// expense codes
					frmHelp.InstancePtr.txtHelpComments.Text = "";
					frmHelp.InstancePtr.Label1.Text = "  Code";
					frmHelp.InstancePtr.Label6.Text = "   Description";
					frmHelp.InstancePtr.Label2.Text = "";
					frmHelp.InstancePtr.Label3.Text = "";
					frmHelp.InstancePtr.Label4.Visible = false;
					frmHelp.InstancePtr.Label5.Visible = false;
                    //FC:FINAL:MSH - format columns for displaying data as in original app
                    frmHelp.InstancePtr.List1.Clear();
                    frmHelp.InstancePtr.List1.Columns.Add("");
                    frmHelp.InstancePtr.List1.Columns.Add("");
                    frmHelp.InstancePtr.List1.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.04);
                    frmHelp.InstancePtr.List1.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.1);
                    frmHelp.InstancePtr.List1.Columns[2].Width = FCConvert.ToInt32(listWidth * 0.6);
                    frmHelp.InstancePtr.List1.GridLineStyle = GridLineStyle.None;
                    clsLoad.OpenRecordset("select * from expensetable order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						frmHelp.InstancePtr.List1.AddItem("\t" + clsLoad.Get_Fields("code") + "\t" + clsLoad.Get_Fields_String("Description"));
						clsLoad.MoveNext();
					}
				}
				// 
				// Tran Code ---  HR.hkey = 24
			}
			else if (Strings.UCase(ControlName) == "TXTMRRITRANCODE")
			{
				// Get #25, 24, HR
				OpenHRandFillTitle_8(24, true);
				frmHelp.InstancePtr.lblHelpHeading.Text = "Tran Code";
				frmHelp.InstancePtr.Label2.Visible = false;
				frmHelp.InstancePtr.Label3.Text = "Amount";
				frmHelp.InstancePtr.Label1.Visible = false;
                //FC:FINAL:MSH - change column sizes for displaying data as in original app
                frmHelp.InstancePtr.List1.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.065);
                frmHelp.InstancePtr.List1.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.065);
                frmHelp.InstancePtr.List1.Columns[2].Width = FCConvert.ToInt32(listWidth * 0.21);
                frmHelp.InstancePtr.List1.Columns[3].Width = FCConvert.ToInt32(listWidth * 0.165);
                frmHelp.InstancePtr.List1.Columns[4].Width = FCConvert.ToInt32(listWidth * 0.29);
                frmHelp.InstancePtr.List1.Columns[5].Width = FCConvert.ToInt32(listWidth * 0.175);
                clsLoad.OpenRecordset("select * from tblTranCode order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                    //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
					//frmHelp.InstancePtr.List1.AddItem("\t" + "\t" + Strings.Format(clsLoad.Get_Fields("Code"), "@@") + "\t" + "\t" + "\t" + Strings.Format(clsLoad.Get_Fields("amount"), "000000.00") + "\t" + Strings.Mid(clsLoad.Get_Fields_String("description") + "\t", 1, 16) + "\t" + clsLoad.Get_Fields_String("ShortDescription"));
					frmHelp.InstancePtr.List1.AddItem("\t" + "\t" + Strings.Format(clsLoad.Get_Fields("Code"), "@@") + "\t" + Strings.Format(clsLoad.Get_Fields("amount"), "000000.00") + "\t" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_String("ShortDescription"));
					clsLoad.MoveNext();
				}
				// Land Code ---  HR.hkey = 25
			}
			else if (Strings.UCase(ControlName) == "TXTMRRILANDCODE")
			{
				// Get #25, 25, HR
				OpenHRandFillTitle_8(25, true);
				frmHelp.InstancePtr.lblHelpHeading.Text = "Land Code";
				frmHelp.InstancePtr.Label2.Visible = false;
				frmHelp.InstancePtr.Label3.Text = "Amount";
				frmHelp.InstancePtr.Label1.Visible = false;
                //FC:FINAL:MSH - change column sizes for displaying data as in original app
                frmHelp.InstancePtr.List1.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.065);
                frmHelp.InstancePtr.List1.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.065);
                frmHelp.InstancePtr.List1.Columns[2].Width = FCConvert.ToInt32(listWidth * 0.21);
                frmHelp.InstancePtr.List1.Columns[3].Width = FCConvert.ToInt32(listWidth * 0.165);
                frmHelp.InstancePtr.List1.Columns[4].Width = FCConvert.ToInt32(listWidth * 0.29);
                frmHelp.InstancePtr.List1.Columns[5].Width = FCConvert.ToInt32(listWidth * 0.175);
                clsLoad.OpenRecordset("select * from tbllandcode order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                    //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
					//frmHelp.InstancePtr.List1.AddItem("\t" + "\t" + Strings.Format(clsLoad.Get_Fields("Code"), "@@") + "\t" + "\t" + "\t" + Strings.Format(clsLoad.Get_Fields("amount"), "000000.00") + "\t" + Strings.Mid(clsLoad.Get_Fields_String("description") + "\t", 1, 16) + "\t" + clsLoad.Get_Fields_String("ShortDescription"));
					frmHelp.InstancePtr.List1.AddItem("\t" + "\t" + Strings.Format(clsLoad.Get_Fields("Code"), "@@") + "\t" + Strings.Format(clsLoad.Get_Fields("amount"), "000000.00") + "\t" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_String("ShortDescription"));
					clsLoad.MoveNext();
				}
				// 
				// Building Code ---  HR.hkey = 26
			}
			else if (Strings.UCase(ControlName) == "TXTMRRIBLDGCODE")
			{
				// Get #25, 26, HR
				OpenHRandFillTitle_8(26, true);
				frmHelp.InstancePtr.lblHelpHeading.Text = "Building Code";
				frmHelp.InstancePtr.Label2.Visible = false;
				frmHelp.InstancePtr.Label3.Text = "Amount";
				frmHelp.InstancePtr.Label1.Visible = false;
                //FC:FINAL:MSH - change column sizes for displaying data as in original app
                frmHelp.InstancePtr.List1.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.065);
                frmHelp.InstancePtr.List1.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.065);
                frmHelp.InstancePtr.List1.Columns[2].Width = FCConvert.ToInt32(listWidth * 0.21);
                frmHelp.InstancePtr.List1.Columns[3].Width = FCConvert.ToInt32(listWidth * 0.165);
                frmHelp.InstancePtr.List1.Columns[4].Width = FCConvert.ToInt32(listWidth * 0.29);
                frmHelp.InstancePtr.List1.Columns[5].Width = FCConvert.ToInt32(listWidth * 0.175);
                clsLoad.OpenRecordset("select * from tblbldgcode order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                    //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
					//frmHelp.InstancePtr.List1.AddItem("\t" + "\t" + Strings.Format(clsLoad.Get_Fields("Code"), "@@@") + "\t" + "\t" + "\t" + Strings.Format(clsLoad.Get_Fields("amount"), "000000.00") + "\t" + Strings.Mid(clsLoad.Get_Fields_String("description") + "\t", 1, 16) + "\t" + clsLoad.Get_Fields_String("shortDescription"));
					frmHelp.InstancePtr.List1.AddItem("\t" + "\t" + Strings.Format(clsLoad.Get_Fields("Code"), "@@@") + "\t" + Strings.Format(clsLoad.Get_Fields("amount"), "000000.00") + "\t" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_String("shortDescription"));
					clsLoad.MoveNext();
				}
				// 
				// Dwelling Style ---  HR.hkey = 101
			}
			else if (Strings.UCase(ControlName) == "GRIDDWELLING1")
			{
				if (RowNum == 0)
				{
					OpenHRandFillTitle_8(101, true);
					clsLoad.OpenRecordset("select * from dwellingstyle order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						KEY = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("id"));
						strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                        //FC:FINAL:MSH - don't need to use this part (will be wrong columns count after parsing)
                        //if (strTemp.Length < 16)
                        //{
                        //	strTemp += Strings.StrDup(16 - strTemp.Length, "\t");
                        //}
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
                        //frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code")))) + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("factor"))), "000000.00") + "  000000.00  " + strTemp + "\t" + clsLoad.Get_Fields_String("shortdescription"));
                        frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("factor")), "000000.00") + "\t" + "000000.00" + "\t" + strTemp + "\t" + clsLoad.Get_Fields_String("shortdescription"));
						clsLoad.MoveNext();
					}
					// 
					// Number of Dwelling Units ---  HR.hkey = 102
				}
				else if (RowNum == 1)
				{
					// Get #25, 102, HR
					OpenHRandFillTitle_8(102, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1470);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1470 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("ClDesc") + "\t" + temp.Get_Fields_String("CSDesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Number of Other Dwelling Units ---  HR.hkey = 103
				}
				else if (RowNum == 2)
				{
					// Get #25, 103, HR
					OpenHRandFillTitle_8(103, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1470);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1470 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("ClDesc") + "\t" + temp.Get_Fields_String("CSDesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Number of Stories ---  HR.hkey = 104
				}
				else if (RowNum == 3)
				{
					OpenHRandFillTitle_8(104, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1480);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1480 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Exterior Wall Type ---  HR.hkey = 105
				}
				else if (RowNum == 4)
				{
					OpenHRandFillTitle_8(105, true);
					clsLoad.OpenRecordset("select * from dwellingexterior order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						KEY = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("id"));
						strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                        //FC:FINAL:MSH - don't need to use this part (will be wrong columns count after parsing)
                        //if (strTemp.Length < 16)
                        //{
                        //	strTemp += Strings.StrDup(16 - strTemp.Length, "\t");
                        //}
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
                        //frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code")))) + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("factor"))), "000000.00") + "  000000.00  " + strTemp + "\t" + clsLoad.Get_Fields_String("shortdescription"));
                        frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("factor")), "000000.00") + "\t" + "000000.00" + "\t" + strTemp + "\t" + clsLoad.Get_Fields_String("shortdescription"));
						clsLoad.MoveNext();
					}
					// 
					// Type of Roof Material
				}
				else if (RowNum == 5)
				{
					OpenHRandFillTitle_8(106, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1510);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1510 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Square Feet of Brick Trim ---  HR.hkey = 107
				}
				else if (RowNum == 6)
				{
					OpenHRandFillTitle_8(107, true);
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1454", modGlobalVariables.strREDatabase);
                    //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
                    //frmHelp.InstancePtr.List1.AddItem("1454" + "\t" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit"))) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase"))) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
                    frmHelp.InstancePtr.List1.AddItem("1454" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					// 
					// Open 3 ---  HR.hkey = 108
				}
				else if (RowNum == 7)
				{
					// Get #25, 108, HR
					OpenHRandFillTitle_8(108, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1520);
					frmHelp.InstancePtr.lblHelpHeading.Text = FCConvert.ToString(temp.Get_Fields_String("cldesc"));
					for (x = 1; x <= 9; x++)
					{
						KEY = (1520 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0000") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Open 4 ---  HR.hkey = 109
				}
				else if (RowNum == 8)
				{
					OpenHRandFillTitle_8(109, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1530);
					frmHelp.InstancePtr.lblHelpHeading.Text = FCConvert.ToString(temp.Get_Fields_String("cldesc"));
					for (x = 1; x <= 9; x++)
					{
						KEY = (1530 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0000") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Year Built ---  HR.hkey = 110
				}
				else if (RowNum == 9)
				{
					OpenHRandFillTitle_2(110);
					// 
					// Year Remodeled ---  HR.hkey = 111
				}
				else if (RowNum == 10)
				{
					OpenHRandFillTitle_2(111);
					// 
					// Foundation Type ---  HR.hkey = 112
				}
				else if (RowNum == 11)
				{
					OpenHRandFillTitle_8(112, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1540);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1540 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Basement Type ---  HR.hkey = 113
				}
				else if (RowNum == 12)
				{
					// Get #25, 113, HR
					OpenHRandFillTitle_8(113, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1550);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1550 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Number of Basement Garages
				}
				else if (RowNum == 13)
				{
					// Get #25, 114, HR
					OpenHRandFillTitle_2(114);
					// Basement Moisture ---  HR.hkey = 115
				}
				else if (RowNum == 14)
				{
					// Get #25, 115, HR
					OpenHRandFillTitle_8(115, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1560);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1560 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// 
				}
				// 
			}
			else if (Strings.UCase(ControlName) == "GRIDDWELLING2")
			{
				if (RowNum == 0)
				{
					// Basement Living Area ---  HR.hkey = 116
					// Get #25, 116, HR
					OpenHRandFillTitle_8(116, true);
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1458", modGlobalVariables.strREDatabase);
                    //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
					//frmHelp.InstancePtr.List1.AddItem("1458" + "\t" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit"))) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase"))) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					frmHelp.InstancePtr.List1.AddItem("1458" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					// 
					// Finished Basement Grade ---  HR.hkey = 117
				}
				else if (RowNum == 1)
				{
					// Get #25, 117, HR
					OpenHRandFillTitle_8(117, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1670);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1670 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Finished Basement Grade Percent Good ---  HR.hkey = 118
				}
				else if (RowNum == 2)
				{
					// Get #25, 117, HR
					OpenHRandFillTitle_2(118);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1670);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1670 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Open 5 ---  HR.hkey = 119
				}
				else if (RowNum == 3)
				{
					OpenHRandFillTitle_8(119, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1570);
					frmHelp.InstancePtr.lblHelpHeading.Text = FCConvert.ToString(temp.Get_Fields_String("cldesc"));
					for (x = 1; x <= 9; x++)
					{
						KEY = (1570 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0000") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Heating Type ---  HR.hkey = 120
				}
				else if (RowNum == 4)
				{
					// Get #25, 120, HR
					OpenHRandFillTitle_8(120, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1590);
					modDataTypes.Statics.CR = temp;
					clsLoad.OpenRecordset("select * from dwellingheat order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						KEY = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("id"));
						strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                        //FC:FINAL:MSH - don't need to use this part (will be wrong columns count after parsing)
						//if (strTemp.Length < 12)
						//{
						//	strTemp += Strings.StrDup(12 - strTemp.Length, "\t");
						//}
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Unit] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Base] and replace with corresponding Get_Field method
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields("Unit")), "000000.00") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields("Base")), "000000.00") + "\t" + strTemp + "\t" + clsLoad.Get_Fields_String("shortdescription"));
						clsLoad.MoveNext();
					}
					// 
					// Percent Heated ---  HR.hkey = 121
				}
				else if (RowNum == 5)
				{
					// Get #25, 121, HR
					OpenHRandFillTitle_2(121);
					// 
					// Cooling Type ---  HR.hkey = 122
				}
				else if (RowNum == 6)
				{
					// Get #25, 122, HR
					OpenHRandFillTitle_8(122, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1600);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1600 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
				}
				else if (RowNum == 7)
				{
					// Percent Cooled ---  HR.hkey = 123
					OpenHRandFillTitle_2(123);
					// Kitchen Style ---  HR.hkey = 124
				}
				else if (RowNum == 8)
				{
					// Get #25, 124, HR
					OpenHRandFillTitle_8(124, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1610);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1610 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Bathroom Style ---  HR.hkey = 125
				}
				else if (RowNum == 9)
				{
					// Get #25, 125, HR
					OpenHRandFillTitle_8(125, true);
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, 1620);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1620 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					modDataTypes.Statics.CR = temp;
					// x
					// 
					// Total # of Rooms ---  HR.hkey = 126
				}
				else if (RowNum == 10)
				{
					OpenHRandFillTitle_2(126);
					// Total # of BedRooms ---  HR.hkey = 127
				}
				else if (RowNum == 11)
				{
					// Get #25, 127, HR
					OpenHRandFillTitle_2(127);
					// Total # of Full BathRooms ---  HR.hkey = 128
				}
				else if (RowNum == 12)
				{
					frmHelp.InstancePtr.txtHelpComments.Text = "This one digit field is used to indicate the number of full bathrooms in the dwelling. A full bath is considered to have three plumbing fixtures, usually a toilet, a sink and a bath tub or a shower or both. The pricing schedule assumes one full bathroom in a dwelling plus one fixture for each additional dwelling unit over one. Less than a full bathroom is a cost deduction, based on the number of fixtures present, and more than a full bathroom is an addition. The cost of three extra plumbing fixtures are added for each extra full bath.  The price per fixture amount can be modified by going to 3. Cost File Update > 1. Update > 4. Dwelling > 4. Other and editing the Unit amount for cost record 1457.";
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1457", modGlobalVariables.strREDatabase);
                    //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
					//frmHelp.InstancePtr.List1.AddItem("1457" + "\t" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit"))) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase"))) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					frmHelp.InstancePtr.List1.AddItem("1457" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					// 
					// Total # of Half BathRooms ---  HR.hkey = 129
				}
				else if (RowNum == 13)
				{
					// Get #25, 129, HR
					OpenHRandFillTitle_8(129, true);
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1457", modGlobalVariables.strREDatabase);
                    //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
					//frmHelp.InstancePtr.List1.AddItem("1457" + "\t" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit"))) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase"))) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					frmHelp.InstancePtr.List1.AddItem("1457" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					// Total # of Additional Fixtures ---  HR.hkey = 130
				}
				else if (RowNum == 14)
				{
					// Get #25, 130, HR
					OpenHRandFillTitle_8(130, true);
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1457", modGlobalVariables.strREDatabase);
                    //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
					//frmHelp.InstancePtr.List1.AddItem("1457" + "\t" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit"))) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase"))) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					frmHelp.InstancePtr.List1.AddItem("1457" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					// 
					// Total # Fireplaces ---  HR.hkey = 131
				}
				else if (RowNum == 15)
				{
					// Get #25, 131, HR
					OpenHRandFillTitle_8(131, true);
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1455", modGlobalVariables.strREDatabase);
                    //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
					//frmHelp.InstancePtr.List1.AddItem("1455" + "\t" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit"))) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase"))) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					frmHelp.InstancePtr.List1.AddItem("1455" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
				}
				// 
			}
			else if (Strings.UCase(ControlName) == "GRIDDWELLING3")
			{
				clsDRWrapper temp = modDataTypes.Statics.CR;
				if (RowNum == 0)
				{
					// Dwelling Layout ---  HR.hkey = 132
					// Get #25, 132, HR
					OpenHRandFillTitle_8(132, true);
					modREMain.OpenCRTable(ref temp, 1630);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1630 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					// x
					// 
					// Attic Type ---  HR.hkey = 133
				}
				else if (RowNum == 1)
				{
					// Get #25, 133, HR
					OpenHRandFillTitle_8(133, true);
					modREMain.OpenCRTable(ref temp, 1640);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1640 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					// x
					// 
					// Amount of Insulation ---  HR.hkey = 134
				}
				else if (RowNum == 2)
				{
					// Get #25, 134, HR
					OpenHRandFillTitle_8(134, true);
					modREMain.OpenCRTable(ref temp, 1650);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1650 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					// x
					// 
					// Building Percent Finished ---  HR.hkey = 135
				}
				else if (RowNum == 3)
				{
					// Get #25, 135, HR
					OpenHRandFillTitle_8(135, true);
					clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1456", modGlobalVariables.strREDatabase);
                    //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
					//frmHelp.InstancePtr.List1.AddItem("1456" + "\t" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit"))) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase"))) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					frmHelp.InstancePtr.List1.AddItem("1456" + "\t" + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					// 
					// Building Grade ---  HR.hkey = 136
				}
				else if (RowNum == 4)
				{
					// Get #25, 136, HR
					OpenHRandFillTitle_8(136, true);
					modREMain.OpenCRTable(ref temp, 1670);
					// .lblHelpHeading.Caption = Trim(HR.Fields("hcode") & "")
					for (x = 1; x <= 9; x++)
					{
						KEY = (1670 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					// x
					// 
					// Building Grade Percent Good ---  HR.hkey = 137
				}
				else if (RowNum == 5)
				{
					frmHelp.InstancePtr.txtHelpComments.Text = "This three digit field is a percentage by which the grade adjusted costs are multiplied. grades and factors are commonly referred to as a grade and a percentage, such as " + FCConvert.ToString(Convert.ToChar(34)) + "C plus 10" + FCConvert.ToString(Convert.ToChar(34)) + " or " + FCConvert.ToString(Convert.ToChar(34)) + "B minus 5" + FCConvert.ToString(Convert.ToChar(34)) + ". The proper way to enter a property that is (say) ten percent better then a standard " + FCConvert.ToString(Convert.ToChar(34)) + "C" + FCConvert.ToString(Convert.ToChar(34)) + " grade is " + FCConvert.ToString(Convert.ToChar(34)) + "3 110" + FCConvert.ToString(Convert.ToChar(34)) + " the " + FCConvert.ToString(Convert.ToChar(34)) + "3" + FCConvert.ToString(Convert.ToChar(34)) + " representing the code for " + FCConvert.ToString(Convert.ToChar(34)) + "C" + FCConvert.ToString(Convert.ToChar(34)) + " grade and the " + FCConvert.ToString(Convert.ToChar(34)) + "110" + FCConvert.ToString(Convert.ToChar(34)) + " indicating that it is 110 percent of a standard " + FCConvert.ToString(Convert.ToChar(34)) + "C" + FCConvert.ToString(Convert.ToChar(34)) + " grade. A " + FCConvert.ToString(Convert.ToChar(34)) + "B minus 5" + FCConvert.ToString(Convert.ToChar(34)) + " grade would be entered as " + FCConvert.ToString(Convert.ToChar(34)) + "4 095" + FCConvert.ToString(Convert.ToChar(34));
					frmHelp.InstancePtr.txtHelpComments.Text = frmHelp.InstancePtr.txtHelpComments.Text + ", or 95 percent of a standard " + FCConvert.ToString(Convert.ToChar(34)) + "B" + FCConvert.ToString(Convert.ToChar(34)) + " grade." + "\r\n";
					frmHelp.InstancePtr.txtHelpComments.Text = frmHelp.InstancePtr.txtHelpComments.Text + "Conversion factors for state method to TRIO are based on the state's default grade percentages and your data may be set up differently." + "\r\n";
					frmHelp.InstancePtr.txtHelpComments.Text = frmHelp.InstancePtr.txtHelpComments.Text + "ST  TRIO      ST  TRIO      ST  TRIO     ST  TRIO     ST  TRIO      ST  TRIO" + "\r\n";
					frmHelp.InstancePtr.txtHelpComments.Text = frmHelp.InstancePtr.txtHelpComments.Text + "2.0  2 100     2.5  2 126     3.0  3 100    3.5  3 117    4.0  4 100     4.5  4 115" + "\r\n";
					frmHelp.InstancePtr.txtHelpComments.Text = frmHelp.InstancePtr.txtHelpComments.Text + "2.1  2 105     2.6  2 131     3.1  3 103    3.6  3 120    4.1  4 104     4.6  4 118" + "\r\n";
					frmHelp.InstancePtr.txtHelpComments.Text = frmHelp.InstancePtr.txtHelpComments.Text + "2.2  2 110     2.7  2 136     3.2  3 107    3.7  3 123    4.2  4 106     4.7  4 122" + "\r\n";
					frmHelp.InstancePtr.txtHelpComments.Text = frmHelp.InstancePtr.txtHelpComments.Text + "2.3  2 115     2.8  2 141     3.3  3 110    3.8  3 126    4.3  4 109     4.8  4 125" + "\r\n";
					frmHelp.InstancePtr.txtHelpComments.Text = frmHelp.InstancePtr.txtHelpComments.Text + "2.4  2 121     2.9  2 146     3.4  3 113    3.9  3 130    4.4  4 112     4.9  4 128";
					//FC:FINAL:DDU:#i1638 - fix show of text beneath the grid
					frmHelp.InstancePtr.txtHelpComments.Height = frmHelp.InstancePtr.Height - 240;
					frmHelp.InstancePtr.txtHelpComments.Top = 30;
					// 
					// Dwelling Square Feet ---  HR.hkey = 138
				}
				else if (RowNum == 6)
				{
					// Get #25, 138, HR
					OpenHRandFillTitle_2(138);
					// 
					// Dwelling Condition ---  HR.hkey = 139
				}
				else if (RowNum == 7)
				{
					// Get #25, 139, HR
					OpenHRandFillTitle_8(139, true);
					modREMain.OpenCRTable(ref temp, 1460);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1460 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					// x
					// 
					// Building Physical Percent Good ---  HR.hkey = 140
				}
				else if (RowNum == 8)
				{
					// Get #25, 140, HR
					OpenHRandFillTitle_2(140);
					// 
					// Building Functional Percent Good ---  HR.hkey = 141
				}
				else if (RowNum == 9)
				{
					// Get #25, 141, HR
					OpenHRandFillTitle_2(141);
					// 
					// Type of Functional Obsolescence ---  HR.hkey = 142
				}
				else if (RowNum == 10)
				{
					// Get #25, 142, HR
					OpenHRandFillTitle_8(142, true);
					modREMain.OpenCRTable(ref temp, 1680);
					for (x = 1; x <= 9; x++)
					{
						KEY = (1680 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cunit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(temp.Get_Fields_String("cbase")) / 100), "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					// x
					// 
					// Building Economic Percent Good ---  HR.hkey = 143
				}
				else if (RowNum == 11)
				{
					// Get #25, 143, HR
					OpenHRandFillTitle_2(143);
					// 
					// Type of Economic Obsolescence ---  HR.hkey = 144
				}
				else if (RowNum == 12)
				{
					frmHelp.InstancePtr.txtHelpComments.Text = "This code is used to record the type of economic obsolescence which affects the property. It is possible to preset specific percentages for specific situations by creating a code and entering a factor in the factor column on the economic code setup screen. The factor is applied to the dwelling and outbuildings present. Other value adjustments which may be unique to this property should be entered into the three digit Percent Good Field above. These economic codes can be modified by going to 3. Cost File Update > 1. Update > 4. Dwelling > 4. Economic Code";
					clsLoad.OpenRecordset("select * from economiccode order by code", modGlobalVariables.strREDatabase);
					while (!clsLoad.EndOfFile())
					{
						KEY = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("id"));
						strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                        //FC:FINAL:MSH - don't need to use this part (will be wrong columns count after parsing)
						//if (strTemp.Length < 16)
						//{
						//	strTemp += Strings.StrDup(16 - strTemp.Length, "\t");
						//}
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        //FC:FINAL:MSH - format input string for splitting data into columns and displaying as in original app
						//frmHelp.InstancePtr.List1.AddItem(Strings.Right(Strings.StrDup(6, "\t") + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code")))), 6) + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("factor"))), "000000.00") + "  000000.00  " + strTemp + "\t" + clsLoad.Get_Fields_String("shortdescription"));
						frmHelp.InstancePtr.List1.AddItem("\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("factor")), "000000.00") + "\t" + "000000.00" + "\t" + strTemp + "\t" + clsLoad.Get_Fields_String("shortdescription"));
						clsLoad.MoveNext();
					}
				}
				modDataTypes.Statics.CR = temp;
				// Commercial Occupancy Codes
			}
			else if ((Strings.UCase(ControlName) == "MEBOCC1") || (Strings.UCase(ControlName) == "MEBOCC2"))
			{
				// Get #25, 171, HR
				OpenHRandFillTitle_8(171, true);
				clsLoad.OpenRecordset("select * from commercialcost where crecordnumber between 1 and 159 order by crecordnumber", modGlobalVariables.strREDatabase);
				for (x = 1; x <= 159; x++)
				{
					frmHelp.InstancePtr.List1.AddItem(Strings.Format(x, "000") + "\t" + Strings.Format(x, "000") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit") + "") / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase") + "") / 100), "000000.00") + "\t" + (clsLoad.Get_Fields_String("ClDesc") + "") + "\t" + (clsLoad.Get_Fields_String("CSDesc") + ""));
					clsLoad.MoveNext();
				}
				// x
				if (Strings.UCase(ControlName) == "MEBOCC1")
				{
					frmHelp.InstancePtr.List1.SelectedIndex = FCConvert.ToInt32(Conversion.Val(frmREProperty.InstancePtr.mebOcc1.Text) - 1);
				}
				else
				{
					frmHelp.InstancePtr.List1.SelectedIndex = FCConvert.ToInt32(Conversion.Val(frmREProperty.InstancePtr.mebOcc2.Text) - 1);
				}
				// 
				// Commercial # of Dwelling Units
			}
			else if ((Strings.UCase(ControlName) == "MEBDWEL1") || (Strings.UCase(ControlName) == "MEBDWEL2"))
			{
				// Get #25, 172, HR
				OpenHRandFillTitle_2(172);
				// 
				// Commercial Building Class
			}
			else if ((Strings.UCase(ControlName) == "MEBC1CLASS") || (Strings.UCase(ControlName) == "MEBC2CLASS"))
			{
				// Get #25, 173, HR
				OpenHRandFillTitle_2(173);
				// 
				// Commercial Quality
			}
			else if ((Strings.UCase(ControlName) == "MEBC1QUALITY") || (Strings.UCase(ControlName) == "MEBC2QUALITY"))
			{
				// Get #25, 174, HR
				OpenHRandFillTitle_2(174);
				// 
				// Commercial Grade
			}
			else if ((Strings.UCase(ControlName) == "MEBC1GRADE") || (Strings.UCase(ControlName) == "MEBC2GRADE"))
			{
				// Get #25, 175, HR
				OpenHRandFillTitle_2(175);
				// 
				// Commercial Exterior Walls
			}
			else if ((Strings.UCase(ControlName) == "MEBC1EXTERIORWALLS") || (Strings.UCase(ControlName) == "MEBC2EXTWALLS"))
			{
				// Get #25, 177, HR
				OpenHRandFillTitle_8(177, true);
				// 
				clsLoad.OpenRecordset("select * from commercialcost where crecordnumber between 341 and 349 order by crecordnumber", modGlobalVariables.strREDatabase);
				for (x = 341; x <= 349; x++)
				{
					// Call OpenCRTable(CR, x)
					// Call OpenCOMMERCIALCOSTTable(CR, x)
					frmHelp.InstancePtr.List1.AddItem(Strings.Format(x, "000") + "\t" + Strings.Format(x - 340, "0") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + (clsLoad.Get_Fields_String("ClDesc") + "") + "\t" + (clsLoad.Get_Fields_String("CSDesc") + ""));
					clsLoad.MoveNext();
				}
				// x
				// 
				// Commercial # of Stories
			}
			else if ((Strings.UCase(ControlName) == "MEBC1STORIES") || (Strings.UCase(ControlName) == "MEBC2STORIES"))
			{
				// Get #25, 178, HR
				OpenHRandFillTitle_8(178, true);
				clsLoad.OpenRecordset("select * from commercialcost where crecordnumber between 381 and 420 order by crecordnumber", modGlobalVariables.strREDatabase);
				for (x = 381; x <= 420; x++)
				{
					frmHelp.InstancePtr.List1.AddItem(Strings.Format(x, "000") + "\t" + Strings.Format(x, "000") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100), "000000.00") + "\t" + Strings.Format((Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100), "000000.00") + "\t" + (clsLoad.Get_Fields_String("ClDesc") + "") + "\t" + (clsLoad.Get_Fields_String("CSDesc") + ""));
					clsLoad.MoveNext();
				}
				// x
				// 
				// Commercial Story Height
			}
			else if ((Strings.UCase(ControlName) == "MEBC1HEIGHT") || (Strings.UCase(ControlName) == "MEBC2HEIGHT"))
			{
				OpenHRandFillTitle_2(179);
				// 
				// commercial sqft
			}
			else if ((Strings.UCase(ControlName) == "MEBC1FLOOR") || (Strings.UCase(ControlName) == "MEBC2FLOOR"))
			{
				OpenHRandFillTitle_2(180);
				// Commercial Perimeter
			}
			else if ((Strings.UCase(ControlName) == "MEBC1PERIMETER") || (Strings.UCase(ControlName) == "MEBC2PERIMETER"))
			{
				// Get #25, 180, HR
				OpenHRandFillTitle_2(182);
				// 
				// Commercial Heating Code
			}
			else if ((Strings.UCase(ControlName) == "MEBC1HEAT") || (Strings.UCase(ControlName) == "MEBC2HEAT"))
			{
				// Get #25, 184, HR
				OpenHRandFillTitle_8(184, true);
				clsLoad.OpenRecordset("select * from commercialcost where crecordnumber between 161 and 339 order by crecordnumber", modGlobalVariables.strREDatabase);
				for (x = 161; x <= 339; x++)
				{
					frmHelp.InstancePtr.List1.AddItem(Strings.Format(x, "000") + "\t" + Strings.Format((x - 160) % 20 + 10, "00") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100, "000000.00") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100, "000000.00") + "\t" + (clsLoad.Get_Fields_String("ClDesc") + "") + "\t" + (clsLoad.Get_Fields_String("CSDesc") + ""));
					clsLoad.MoveNext();
				}
				// x
				// 
				// Commercial Year Built
			}
			else if ((Strings.UCase(ControlName) == "MEBC1BUILT") || (Strings.UCase(ControlName) == "MEBC2BUILT"))
			{
				OpenHRandFillTitle_2(185);
				// 
				// Commercial Year Remodeled
			}
			else if ((Strings.UCase(ControlName) == "MEBC1REMODEL") || (Strings.UCase(ControlName) == "MEBC2REMODEL"))
			{
				OpenHRandFillTitle_2(186);
				// 
				// Commercial Condition
			}
			else if ((Strings.UCase(ControlName) == "MEBC1CONDITION") || (Strings.UCase(ControlName) == "MEBC2CONDITION"))
			{
				// Get #25, 187, HR
				OpenHRandFillTitle_8(187, true);
				clsLoad.OpenRecordset("select * from commercialcost where crecordnumber between 351 and 359 order by crecordnumber", modGlobalVariables.strREDatabase);
				for (x = 351; x <= 359; x++)
				{
					frmHelp.InstancePtr.List1.AddItem(Strings.Format(x, "00") + "\t" + Strings.Format(x - 350, "0") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_String("CUnit")) / 100, "000000.00") + "\t" + Strings.Format(Conversion.Val(clsLoad.Get_Fields_String("CBase")) / 100, "000000.00") + "\t" + clsLoad.Get_Fields_String("ClDesc") + "\t" + clsLoad.Get_Fields_String("CSDesc"));
					clsLoad.MoveNext();
				}
				// x
				// 
				// Commercial Physical Percent
			}
			else if ((Strings.UCase(ControlName) == "MEBC1PHYS") || (Strings.UCase(ControlName) == "MEBC2PHYS"))
			{
				// Get #25, 188, HR
				OpenHRandFillTitle_2(188);
				// Commercial Functional Percent Good
			}
			else if ((Strings.UCase(ControlName) == "MEBC1FUNCT") || (Strings.UCase(ControlName) == "MEBC2FUNCT"))
			{
				// Get #25, 189, HR
				OpenHRandFillTitle_2(189);
				// 
				// Commercial Economic Percent Good
			}
			else if (Strings.UCase(ControlName) == "MEBCMECON")
			{
				// Get #25, 190, HR
				OpenHRandFillTitle_2(190);
				// Commercial Economic Code
			}
			else if (Strings.UCase(ControlName) == "MEBCMECON")
			{
				// Get #25, 212, HR
				OpenHRandFillTitle_2(212);
				// 
				// Outbuilding Code ---  HR.hkey = 301
			}
			else if (Strings.UCase(ControlName) == "TXTMROITYPE1")
			{
				// Get #25, 301, HR
				OpenHRandFillTitle_8(301, true);
				clsDRWrapper temp = modDataTypes.Statics.CR;
				modREMain.OpenCRTable(ref temp);
				for (x = 1; x <= 999; x++)
				{
					frmHelp.InstancePtr.List1.AddItem(Strings.Format(x, "0000") + "\t" + Strings.Format(x, "000") + "\t" + Strings.Format(Conversion.Val(temp.Get_Fields_String("cunit")) / 100, "000000.00") + "\t" + Strings.Format(Conversion.Val(temp.Get_Fields_String("cbase")) / 100, "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					temp.MoveNext();
				}
				// x
				modREMain.OpenCRTable(ref temp, 1);
				modDataTypes.Statics.CR = temp;
				// 
				// Outbuilding Year ---  HR.hkey = 302
			}
			else if (Strings.UCase(ControlName) == "TXTMROIYEAR1")
			{
				// Get #25, 302, HR
				OpenHRandFillTitle_2(302);
				// Number of Outbuilding Units ---  HR.hkey = 303
			}
			else if (Strings.UCase(ControlName) == "TXTMROIUNITS1")
			{
				// Get #25, 303, HR
				OpenHRandFillTitle_2(303);
				// 
				// Outbuilding Grade/Quality ---  HR.hkey = 304
			}
			else if (Strings.UCase(ControlName) == "TXTMROIGRADECD1")
			{
				clsDRWrapper temp = modDataTypes.Statics.CR;
				// Get #25, 304, HR
				OpenHRandFillTitle_8(304, true);
				modREMain.OpenCRTable(ref temp, 1670);
				if (Conversion.Val(RowNum) > 699)
				{
					for (x = 1; x <= 44; x++)
					{
						KEY = (1740 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						if (x % 4 > 0)
						{
							strTemp = FCConvert.ToString((x % 4) + 1);
						}
						else
						{
							strTemp = "5";
						}
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + strTemp + "\t" + Strings.Format(Conversion.Val(temp.Get_Fields_String("cunit")) / 100, "000000.00") + "\t" + Strings.Format(Conversion.Val(temp.Get_Fields_String("cbase")) / 100, "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					// x
				}
				else
				{
					for (x = 1; x <= 9; x++)
					{
						KEY = (1670 + x);
						modREMain.OpenCRTable(ref temp, KEY);
						frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + FCConvert.ToString(x) + "\t" + Strings.Format(Conversion.Val(temp.Get_Fields_String("cunit")) / 100, "000000.00") + "\t" + Strings.Format(Conversion.Val(temp.Get_Fields_String("cbase")) / 100, "000000.00") + "\t" + temp.Get_Fields_String("cldesc") + "\t" + temp.Get_Fields_String("csdesc"));
					}
					// x
				}
				modDataTypes.Statics.CR = temp;
				// 
				// Outbuilding Grade/Quality Percent Good ---  HR.hkey = 305
			}
			else if (Strings.UCase(ControlName) == "TXTMROIGRADEPCT1")
			{
				// Get #25, 304, HR
				OpenHRandFillTitle_2(305);
				// 
				// Outbuilding Condition ---  HR.hkey = 306
			}
			else if (Strings.UCase(ControlName) == "TXTMROICOND1")
			{
				// Get #25, 306, HR
				OpenHRandFillTitle_8(306, true);
				clsDRWrapper temp = modDataTypes.Statics.CR;
				modREMain.OpenCRTable(ref temp, 1460);
				for (x = 1; x <= 9; x++)
				{
					KEY = (1460 + x);
					modREMain.OpenCRTable(ref temp, KEY);
					frmHelp.InstancePtr.List1.AddItem(Strings.Format(KEY, "0000") + "\t" + Strings.Format(x, "0") + "\t" + Strings.Format(Conversion.Val(temp.Get_Fields_String("CUnit")) / 100, "000000.00") + "\t" + Strings.Format(Conversion.Val(temp.Get_Fields_String("CBase")) / 100, "000000.00") + "\t" + temp.Get_Fields_String("ClDesc") + "\t" + temp.Get_Fields_String("CSDesc"));
					// clsLoad.MoveNext
				}
				modDataTypes.Statics.CR = temp;
				// x
				// 
				// Outbuilding Physical Percent Good ---  HR.hkey = 307
			}
			else if (Strings.UCase(ControlName) == "TXTMROIPCTPHYS1")
			{
				// Get #25, 307, HR
				OpenHRandFillTitle_2(307);
				// 
				// Outbuilding Functional Percent Good ---  HR.hkey = 308
			}
			else if (Strings.UCase(ControlName) == "TXTMROIPCTFUNCT1")
			{
				// Get #25, 308, HR
				OpenHRandFillTitle_2(308);
			}
			frmHelp.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			Statics.Helping = false;
		}

		private static void OpenHRandFillTitle_2(short intCode, bool boolShowList = false)
		{
			OpenHRandFillTitle(ref intCode, boolShowList);
		}

		private static void OpenHRandFillTitle_8(short intCode, bool boolShowList = false)
		{
			OpenHRandFillTitle(ref intCode, boolShowList);
		}

		private static void OpenHRandFillTitle(ref short intCode, bool boolShowList = false)
		{
			clsDRWrapper temp = modDataTypes.Statics.HR;
			modREMain.OpenHRTable(ref temp, intCode);
			modDataTypes.Statics.HR = temp;
			// frmHelp.lblHelpHeading.Caption = Trim(HR.Fields("hcode") & "")
			frmHelp.InstancePtr.txtHelpComments.Text = "";
			if (!(modDataTypes.Statics.HR == null))
			{
				if (!modDataTypes.Statics.HR.EndOfFile())
				{
					frmHelp.InstancePtr.txtHelpComments.Text = modDataTypes.Statics.HR.Get_Fields_String("hDescription") + "";
				}
			}
			if (boolShowList)
			{
			}
			else
			{
				//FC:FINAL:DDU:#i1638 - fix show of text beneath the grid
				frmHelp.InstancePtr.txtHelpComments.Height = frmHelp.InstancePtr.Height - 240;
				frmHelp.InstancePtr.txtHelpComments.Top = 30;
			}
		}

		public static void StateAbbreviations()
		{
            //FC:FINAL:MSH - issue #1650: add columns to listbox and restyle them for displaying data similar with original app
            //frmHelp.InstancePtr.List1.AddItem("Alabama            -   AL");
            //frmHelp.InstancePtr.List1.AddItem("Alaska             -   AK");
            //frmHelp.InstancePtr.List1.AddItem("Arizona            -   AZ");
            //frmHelp.InstancePtr.List1.AddItem("Arkansas           -   AR");
            //frmHelp.InstancePtr.List1.AddItem("California         -   CA");
            //frmHelp.InstancePtr.List1.AddItem("Colorado           -   CO");
            //frmHelp.InstancePtr.List1.AddItem("Connetticut        -   CT");
            //frmHelp.InstancePtr.List1.AddItem("Delaware           -   DE");
            //frmHelp.InstancePtr.List1.AddItem("Florida            -   FL");
            //frmHelp.InstancePtr.List1.AddItem("Georgia            -   GA");
            //frmHelp.InstancePtr.List1.AddItem("Hawaii             -   HI");
            //frmHelp.InstancePtr.List1.AddItem("Idaho              -   ID");
            //frmHelp.InstancePtr.List1.AddItem("Illinois           -   IL");
            //frmHelp.InstancePtr.List1.AddItem("Indiana            -   IN");
            //frmHelp.InstancePtr.List1.AddItem("Iowa               -   IA");
            //frmHelp.InstancePtr.List1.AddItem("Kansas             -   KS");
            //frmHelp.InstancePtr.List1.AddItem("Kentucky           -   KY");
            //frmHelp.InstancePtr.List1.AddItem("Louisiana          -   LA");
            //frmHelp.InstancePtr.List1.AddItem("Maine              -   ME");
            //frmHelp.InstancePtr.List1.AddItem("Maryland           -   MD");
            //frmHelp.InstancePtr.List1.AddItem("Massachusets       -   MA");
            //frmHelp.InstancePtr.List1.AddItem("Michigan           -   MI");
            //frmHelp.InstancePtr.List1.AddItem("Minnessota         -   MN");
            //frmHelp.InstancePtr.List1.AddItem("Mississippi        -   MS");
            //frmHelp.InstancePtr.List1.AddItem("Missouri           -   MO");
            //frmHelp.InstancePtr.List1.AddItem("Montana            -   MT");
            //frmHelp.InstancePtr.List1.AddItem("Nebraska           -   NE");
            //frmHelp.InstancePtr.List1.AddItem("Nevada             -   NV");
            //frmHelp.InstancePtr.List1.AddItem("New Hampshire      -   NH");
            //frmHelp.InstancePtr.List1.AddItem("New Jersey         -   NJ");
            //frmHelp.InstancePtr.List1.AddItem("New Mexico         -   NM");
            //frmHelp.InstancePtr.List1.AddItem("New York           -   NY");
            //frmHelp.InstancePtr.List1.AddItem("North Carolina     -   NC");
            //frmHelp.InstancePtr.List1.AddItem("North Dakota       -   ND");
            //frmHelp.InstancePtr.List1.AddItem("Ohio               -   OH");
            //frmHelp.InstancePtr.List1.AddItem("Oklahoma           -   OK");
            //frmHelp.InstancePtr.List1.AddItem("Oregon             -   OR");
            //frmHelp.InstancePtr.List1.AddItem("Pennsylvania       -   PA");
            //frmHelp.InstancePtr.List1.AddItem("Rhode Island       -   RI");
            //frmHelp.InstancePtr.List1.AddItem("South Carolina     -   SC");
            //frmHelp.InstancePtr.List1.AddItem("South Dakota       -   SD");
            //frmHelp.InstancePtr.List1.AddItem("Tennessee          -   TN");
            //frmHelp.InstancePtr.List1.AddItem("Texas              -   TX");
            //frmHelp.InstancePtr.List1.AddItem("Utah               -   UT");
            //frmHelp.InstancePtr.List1.AddItem("Vermont            -   VT");
            //frmHelp.InstancePtr.List1.AddItem("Virginia           -   VA");
            //frmHelp.InstancePtr.List1.AddItem("Washington         -   WA");
            //frmHelp.InstancePtr.List1.AddItem("West Virginia      -   WV");
            //frmHelp.InstancePtr.List1.AddItem("Wisconsin          -   WI");
            //frmHelp.InstancePtr.List1.AddItem("Wyoming            -   WY");
            frmHelp.InstancePtr.List1.Clear();
            frmHelp.InstancePtr.List1.Columns.Add("");
            frmHelp.InstancePtr.List1.Columns.Add("");
            int listWidth = frmHelp.InstancePtr.List1.Width;
            frmHelp.InstancePtr.List1.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.331);
            frmHelp.InstancePtr.List1.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.08);
            frmHelp.InstancePtr.List1.Columns[2].Width = FCConvert.ToInt32(listWidth * 0.54);
            frmHelp.InstancePtr.List1.GridLineStyle = GridLineStyle.None;
            frmHelp.InstancePtr.List1.AddItem("Alabama \t - \t AL");
			frmHelp.InstancePtr.List1.AddItem("Alaska \t - \t AK");
			frmHelp.InstancePtr.List1.AddItem("Arizona \t - \t AZ");
			frmHelp.InstancePtr.List1.AddItem("Arkansas \t - \t AR");
			frmHelp.InstancePtr.List1.AddItem("California \t - \t CA");
			frmHelp.InstancePtr.List1.AddItem("Colorado \t - \t CO");
			frmHelp.InstancePtr.List1.AddItem("Connetticut \t - \t CT");
			frmHelp.InstancePtr.List1.AddItem("Delaware \t - \t DE");
			frmHelp.InstancePtr.List1.AddItem("Florida \t - \t FL");
			frmHelp.InstancePtr.List1.AddItem("Georgia \t - \t GA");
			frmHelp.InstancePtr.List1.AddItem("Hawaii \t - \t HI");
			frmHelp.InstancePtr.List1.AddItem("Idaho \t - \t ID");
			frmHelp.InstancePtr.List1.AddItem("Illinois \t - \t IL");
			frmHelp.InstancePtr.List1.AddItem("Indiana \t - \t IN");
			frmHelp.InstancePtr.List1.AddItem("Iowa \t - \t IA");
			frmHelp.InstancePtr.List1.AddItem("Kansas \t - \t KS");
			frmHelp.InstancePtr.List1.AddItem("Kentucky \t - \t KY");
			frmHelp.InstancePtr.List1.AddItem("Louisiana \t - \t LA");
			frmHelp.InstancePtr.List1.AddItem("Maine \t - \t ME");
			frmHelp.InstancePtr.List1.AddItem("Maryland \t - \t MD");
			frmHelp.InstancePtr.List1.AddItem("Massachusets \t - \t MA");
			frmHelp.InstancePtr.List1.AddItem("Michigan \t - \t MI");
			frmHelp.InstancePtr.List1.AddItem("Minnessota \t - \t MN");
			frmHelp.InstancePtr.List1.AddItem("Mississippi \t - \t MS");
			frmHelp.InstancePtr.List1.AddItem("Missouri \t - \t MO");
			frmHelp.InstancePtr.List1.AddItem("Montana \t - \t MT");
			frmHelp.InstancePtr.List1.AddItem("Nebraska \t - \t NE");
			frmHelp.InstancePtr.List1.AddItem("Nevada \t - \t NV");
			frmHelp.InstancePtr.List1.AddItem("New Hampshire \t - \t NH");
			frmHelp.InstancePtr.List1.AddItem("New Jersey \t - \t NJ");
			frmHelp.InstancePtr.List1.AddItem("New Mexico \t - \t NM");
			frmHelp.InstancePtr.List1.AddItem("New York \t - \t NY");
			frmHelp.InstancePtr.List1.AddItem("North Carolina \t - \t NC");
			frmHelp.InstancePtr.List1.AddItem("North Dakota \t - \t ND");
			frmHelp.InstancePtr.List1.AddItem("Ohio \t - \t OH");
			frmHelp.InstancePtr.List1.AddItem("Oklahoma \t - \t OK");
			frmHelp.InstancePtr.List1.AddItem("Oregon \t - \t OR");
			frmHelp.InstancePtr.List1.AddItem("Pennsylvania \t - \t PA");
			frmHelp.InstancePtr.List1.AddItem("Rhode Island \t - \t RI");
			frmHelp.InstancePtr.List1.AddItem("South Carolina \t - \t SC");
			frmHelp.InstancePtr.List1.AddItem("South Dakota \t - \t SD");
			frmHelp.InstancePtr.List1.AddItem("Tennessee \t - \t TN");
			frmHelp.InstancePtr.List1.AddItem("Texas \t - \t TX");
			frmHelp.InstancePtr.List1.AddItem("Utah \t - \t UT");
			frmHelp.InstancePtr.List1.AddItem("Vermont \t - \t VT");
			frmHelp.InstancePtr.List1.AddItem("Virginia \t - \t VA");
			frmHelp.InstancePtr.List1.AddItem("Washington \t - \t WA");
			frmHelp.InstancePtr.List1.AddItem("West Virginia \t - \t WV");
			frmHelp.InstancePtr.List1.AddItem("Wisconsin \t - \t WI");
			frmHelp.InstancePtr.List1.AddItem("Wyoming \t - \t WY");
		}

		public class StaticVariables
		{
			// is not shown then runtime errors happen
			// performes a SetFocus event to the screen that
			// if double click for help then the program
			// to help keep runtime errors to a minimum
			// variable Helping is used in long screen
			//=========================================================
			public bool Helping;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
