﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Runtime.InteropServices;

namespace TWRE0000
{
	public class Registry
	{
		//=========================================================
		// Reg Key Security Options...
		// Public Const KEY_READ = ((STANDARD_RIGHTS_READ Or KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY) And (Not Synchronize))
		public const uint HKEY_LOCAL_MACHINE = 0x80000001;
		public const uint HKEY_MACHINE = 0x80000002;
		// Global Const HKEY_CURRENT_USER = &H80000001
		public const uint READ_CONTROL = 0x20000;
		public const int KEY_QUERY_VALUE = 0x1;
		public const int KEY_SET_VALUE = 0x2;
		public const int KEY_CREATE_SUB_KEY = 0x4;
		public const int KEY_ENUMERATE_SUB_KEYS = 0x8;
		public const int KEY_NOTIFY = 0x10;
		public const int KEY_CREATE_LINK = 0x20;
		public const int KEY_ALL_ACCESS = KEY_QUERY_VALUE + KEY_SET_VALUE + KEY_CREATE_SUB_KEY + KEY_ENUMERATE_SUB_KEYS + KEY_NOTIFY + KEY_CREATE_LINK + (int)READ_CONTROL;
		// Open/Create Options
		const int REG_OPTION_NON_VOLATILE = 0;
		const int REG_OPTION_VOLATILE = 0x1;
		// Structures Needed For Registry Prototypes
		public struct SECURITY_ATTRIBUTES
		{
			public int nLength;
			public int lpSecurityDescriptor;
			public bool bInheritHandle;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public SECURITY_ATTRIBUTES(int unusedParam)
			{
				this.nLength = 0;
				this.lpSecurityDescriptor = 0;
				this.bInheritHandle = false;
			}
		};

		private struct FILETIME
		{
			public int dwLowDateTime;
			public int dwHighDateTime;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public FILETIME(int unusedParam)
			{
				this.dwLowDateTime = 0;
				this.dwHighDateTime = 0;
			}
		};
		// Reg Key ROOT Types...
		public const int ERROR_SUCCESS = 0;
		public const int REG_SZ = 1;
		// Unicode nul terminated string
		public const int REG_DWORD = 4;
		// 32-bit number
		public const string gREGKEYSYSINFOLOC = "SOFTWARE\\Microsoft\\Shared Tools Location";
		public const string gREGVALSYSINFOLOC = "MSINFO";
		public const string gREGKEYSYSINFO = "SOFTWARE\\Microsoft\\Shared Tools\\MSINFO";
		public const string gREGVALSYSINFO = "PATH";
		public const string gREGKEYDISPLAY = "Config\\0001\\Display\\Settings";
		public const string gREGVALDISPLAY = "Resolution";

		[DllImport("advapi32.dll", EntryPoint = "RegOpenKeyA")]
		public static extern int RegOpenKey(int hKey, string lpSubKey, ref int phkResult);

		[DllImport("advapi32.dll", EntryPoint = "RegCreateKeyA")]
		public static extern int RegCreateKey(int hKey, IntPtr lpSubKey, ref int phkResult);

		public static int RegCreateKeyWrp(int hKey, string lpSubKey, int phkResult)
		{
			IntPtr plpSubKey = FCUtils.GetByteFromString(lpSubKey);
			int ret = RegCreateKey(hKey, plpSubKey, ref phkResult);
			FCUtils.GetStringFromByte(ref lpSubKey, plpSubKey);
			return ret;
		}

		[DllImport("advapi32", EntryPoint = "RegOpenKeyExA")]
		public static extern int RegOpenKeyEx(int hKey, string lpSubKey, int ulOptions, int samDesired, ref int phkResult);

		[DllImport("advapi32", EntryPoint = "RegQueryValueExA")]
		public static extern int RegQueryValueEx(int hKey, string lpValueName, int lpReserved, ref int lpType, string lpData, ref int lpcbData);

		[DllImport("advapi32")]
		public static extern int RegCloseKey(int hKey);
		//[DllImport("advapi32.dll", EntryPoint = "RegSetValueExA")]
		//public static extern int RegSetValueEx(int hKey, string lpValueName, int Reserved, int dwType, ref void* lpData, int cbData);		// Note that if you declare the lpData parameter as String, you must pass it By Value.
		[DllImport("advapi32", EntryPoint = "RegCreateKeyExA")]
		public static extern int RegCreateKeyEx(int hKey, string lpSubKey, int Reserved, string lpClass, int dwOptions, int samDesired, ref SECURITY_ATTRIBUTES lpSecurityAttributes, ref int phkResult, ref int lpdwDisposition);
		// Public Function SaveKeyValue(KeyRoot As Long, KeyName As String, SubKeyRef As String, ByRef KeyVal As String) As Boolean
		// Dim I As Long                                           ' Loop intCounter
		// Dim rc As Long                                          ' Return Code
		// Dim hKey As Long                                        ' Handle To An Open Registry Key
		// Dim hDepth As Long                                      '
		// Dim KeyValType As Long                                  ' Data Type Of A Registry Key
		// Dim tmpVal As String                                    ' Tempory Storage For A Registry Key Value
		// Dim KeyValSize As Long                                  ' Size Of Registry Key Variable
		// Dim tSA As SECURITY_ATTRIBUTES
		// Dim lCreate As Long
		//
		// ------------------------------------------------------------
		// Open RegKey Under KeyRoot {HKEY_LOCAL_MACHINE...}
		// ------------------------------------------------------------
		// TRYAGAIN:
		// rc = RegOpenKeyEx(KeyRoot, KeyName, 0, KEY_ALL_ACCESS, hKey) ' Open Registry Key
		//
		// If (rc <> ERROR_SUCCESS) Then GoTo SaveKeyError
		//
		// ------------------------------------------------------------
		// Save Registry Key Value...
		// ------------------------------------------------------------
		//
		// KeyVal = Trim$(KeyVal) & Null
		// rc = RegSetValueEx(hKey, SubKeyRef, 0,
		// 1, ByVal KeyVal, ByVal Len(KeyVal))    ' Save Key Value
		//
		// SaveKeyValue = True                                     ' Return Success
		// rc = RegCloseKey(hKey)                                  ' Close Registry Key
		// Exit Function                                           ' Exit
		//
		// SaveKeyError:      ' Cleanup After An Error Has Occured...
		// KeyVal = ""                                             ' Set Return Val To Empty String
		// SaveKeyValue = False                                     ' Return Failure
		// rc = RegCloseKey(hKey)                                  ' Close Registry Key
		// End Function
		// Public Function GetKeyValues(KeyRoot As Long, KeyName As String, SubKeyRef As String, ByRef KeyVal As String) As Boolean
		// Dim I As Long                                           ' Loop intCounter
		// Dim rc As Long                                          ' Return Code
		// Dim hKey As Long                                        ' Handle To An Open Registry Key
		// Dim hDepth As Long                                      '
		// Dim KeyValType As Long                                  ' Data Type Of A Registry Key
		// Dim tmpVal As String                                    ' Tempory Storage For A Registry Key Value
		// Dim KeyValSize As Long                                  ' Size Of Registry Key Variable
		// ------------------------------------------------------------
		// Open RegKey Under KeyRoot {HKEY_LOCAL_MACHINE...}
		// ------------------------------------------------------------
		// TRYAGAIN:
		// rc = RegOpenKeyEx(HKEY_LOCAL_MACHINE, REGISTRYKEY, 0, KEY_ALL_ACCESS, hKey) ' Open Registry Key
		//
		// If (rc <> ERROR_SUCCESS) Then
		// Call RegCreateKey(KeyRoot, KeyName, 0)
		// Call RegCreateKey(KeyRoot, KeyName & "Default", 0)
		// GoTo TRYAGAIN
		// GoTo GetKeyError          ' Handle Error...
		// End If
		//
		// tmpVal = String$(1024, 0)                             ' Allocate Variable Space
		// KeyValSize = 1024                                       ' Mark Variable Size
		//
		// ------------------------------------------------------------
		// Retrieve Registry Key Value...
		// ------------------------------------------------------------
		// GetValue:
		//
		// rc = RegQueryValueEx(hKey, SubKeyRef, 0,
		// KeyValType, tmpVal, KeyValSize)    ' Get/Create Key Value
		//
		// If (rc <> ERROR_SUCCESS) Then
		// Call RegSetValueEx(hKey, SubKeyRef, 0, 1, 0, 1)
		// GoTo GetKeyError          ' Handle Errors
		// GoTo GetValue
		// End If
		//
		// If KeyValSize > 0 Then
		// If (Asc(Mid(tmpVal, KeyValSize, 1)) = 0) Then           ' Win95 Adds Null Terminated String...
		// tmpVal = Left(tmpVal, KeyValSize - 1)               ' Null Found, Extract From String
		// Else                                                    ' WinNT Does NOT Null Terminate String...
		// tmpVal = Left(tmpVal, KeyValSize)                   ' Null Not Found, Extract String Only
		// End If
		// End If
		// ------------------------------------------------------------
		// Determine Key Value Type For Conversion...
		// ------------------------------------------------------------
		// Select Case KeyValType                                  ' Search Data Types...
		// Case REG_SZ                                             ' String Registry Key Data Type
		// KeyVal = tmpVal                                     ' Copy String Value
		// Case REG_DWORD                                          ' Double Word Registry Key Data Type
		// For I = Len(tmpVal) To 1 Step -1                    ' Convert Each Bit
		// KeyVal = KeyVal + Hex(Asc(Mid(tmpVal, I, 1)))   ' Build Value Char. By Char.
		// Next
		// KeyVal = Format$("&h" + KeyVal)                     ' Convert Double Word To String
		// End Select
		//
		// GetKeyValues = True                                      ' Return Success
		// rc = RegCloseKey(hKey)                                  ' Close Registry Key
		// Exit Function                                           ' Exit
		//
		// GetKeyError:      ' Cleanup After An Error Has Occured...
		// KeyVal = ""                                             ' Set Return Val To Empty String
		// GetKeyValues = False                                     ' Return Failure
		// rc = RegCloseKey(hKey)                                  ' Close Registry Key
		// End Function
	}
}
