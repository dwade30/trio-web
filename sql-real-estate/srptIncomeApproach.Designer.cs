﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptIncomeApproach.
	/// </summary>
	partial class srptIncomeApproach
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptIncomeApproach));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOCC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSquareFootage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtActualRent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtActualYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMarketYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMarketRent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalSQFT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPotGrossIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOCC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSquareFootage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActualRent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActualYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMarketYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMarketRent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSQFT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPotGrossIncome)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtOCC,
				this.txtDescription,
				this.txtSquareFootage,
				this.txtGN,
				this.txtActualRent,
				this.txtActualYear,
				this.txtMarketYear,
				this.txtMarketRent
			});
			this.Detail.Height = 0.21875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9
			});
			this.ReportHeader.Height = 0.46875F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label10,
				this.txtTotalSQFT,
				this.txtPotGrossIncome,
				this.Label11
			});
			this.ReportFooter.Height = 0.21875F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label1.Text = "OCC";
			this.Label1.Top = 0.25F;
			this.Label1.Width = 0.4166667F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.4166667F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Text = "Description";
			this.Label2.Top = 0.25F;
			this.Label2.Width = 1.916667F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.375F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.416667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label3.Text = "Square Footage";
			this.Label3.Top = 0.08333334F;
			this.Label3.Width = 0.7083333F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.166667F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label4.Text = "G/N";
			this.Label4.Top = 0.25F;
			this.Label4.Width = 0.4166667F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.375F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.583333F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label5.Text = "Actual Rent/Mo.";
			this.Label5.Top = 0.08333334F;
			this.Label5.Width = 0.7083333F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.333333F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label6.Text = "Rent/SF/Year";
			this.Label6.Top = 0.08333334F;
			this.Label6.Width = 1.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.333333F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label7.Text = "Actual";
			this.Label7.Top = 0.25F;
			this.Label7.Width = 0.75F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 5.166667F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label8.Text = "Market";
			this.Label8.Top = 0.25F;
			this.Label8.Width = 0.6666667F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.375F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 5.916667F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label9.Text = "Annual Market Rent";
			this.Label9.Top = 0.08333334F;
			this.Label9.Width = 0.9479167F;
			// 
			// txtOCC
			// 
			this.txtOCC.Height = 0.19F;
			this.txtOCC.Left = 0F;
			this.txtOCC.Name = "txtOCC";
			this.txtOCC.Text = null;
			this.txtOCC.Top = 0F;
			this.txtOCC.Width = 0.4166667F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.19F;
			this.txtDescription.Left = 0.4166667F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 1.916667F;
			// 
			// txtSquareFootage
			// 
			this.txtSquareFootage.Height = 0.19F;
			this.txtSquareFootage.Left = 2.333333F;
			this.txtSquareFootage.Name = "txtSquareFootage";
			this.txtSquareFootage.Style = "text-align: right";
			this.txtSquareFootage.Text = null;
			this.txtSquareFootage.Top = 0F;
			this.txtSquareFootage.Width = 0.78125F;
			// 
			// txtGN
			// 
			this.txtGN.Height = 0.19F;
			this.txtGN.Left = 3.166667F;
			this.txtGN.Name = "txtGN";
			this.txtGN.Style = "text-align: center";
			this.txtGN.Text = null;
			this.txtGN.Top = 0F;
			this.txtGN.Width = 0.4166667F;
			// 
			// txtActualRent
			// 
			this.txtActualRent.Height = 0.19F;
			this.txtActualRent.Left = 3.583333F;
			this.txtActualRent.Name = "txtActualRent";
			this.txtActualRent.Style = "text-align: right";
			this.txtActualRent.Text = null;
			this.txtActualRent.Top = 0F;
			this.txtActualRent.Width = 0.6979167F;
			// 
			// txtActualYear
			// 
			this.txtActualYear.Height = 0.19F;
			this.txtActualYear.Left = 4.333333F;
			this.txtActualYear.Name = "txtActualYear";
			this.txtActualYear.Style = "text-align: right";
			this.txtActualYear.Text = null;
			this.txtActualYear.Top = 0F;
			this.txtActualYear.Width = 0.7604167F;
			// 
			// txtMarketYear
			// 
			this.txtMarketYear.Height = 0.19F;
			this.txtMarketYear.Left = 5.083333F;
			this.txtMarketYear.Name = "txtMarketYear";
			this.txtMarketYear.Style = "text-align: right";
			this.txtMarketYear.Text = null;
			this.txtMarketYear.Top = 0F;
			this.txtMarketYear.Width = 0.7604167F;
			// 
			// txtMarketRent
			// 
			this.txtMarketRent.Height = 0.19F;
			this.txtMarketRent.Left = 5.916667F;
			this.txtMarketRent.Name = "txtMarketRent";
			this.txtMarketRent.Style = "text-align: right";
			this.txtMarketRent.Text = null;
			this.txtMarketRent.Top = 0F;
			this.txtMarketRent.Width = 0.9270833F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.5F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label10.Text = "Total SF";
			this.Label10.Top = 0F;
			this.Label10.Width = 0.7395833F;
			// 
			// txtTotalSQFT
			// 
			this.txtTotalSQFT.Height = 0.19F;
			this.txtTotalSQFT.Left = 2.333333F;
			this.txtTotalSQFT.Name = "txtTotalSQFT";
			this.txtTotalSQFT.Style = "text-align: right";
			this.txtTotalSQFT.Text = null;
			this.txtTotalSQFT.Top = 0F;
			this.txtTotalSQFT.Width = 0.78125F;
			// 
			// txtPotGrossIncome
			// 
			this.txtPotGrossIncome.Height = 0.19F;
			this.txtPotGrossIncome.Left = 5.916667F;
			this.txtPotGrossIncome.Name = "txtPotGrossIncome";
			this.txtPotGrossIncome.Style = "text-align: right";
			this.txtPotGrossIncome.Text = null;
			this.txtPotGrossIncome.Top = 0F;
			this.txtPotGrossIncome.Width = 0.9270833F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.833333F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label11.Text = "Potential Gross Income";
			this.Label11.Top = 0F;
			this.Label11.Width = 1.989583F;
			// 
			// srptIncomeApproach
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.947917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOCC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSquareFootage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActualRent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActualYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMarketYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMarketRent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalSQFT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPotGrossIncome)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOCC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSquareFootage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtActualRent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtActualYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMarketYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMarketRent;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalSQFT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPotGrossIncome;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
	}
}
