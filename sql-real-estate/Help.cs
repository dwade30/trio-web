﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmHelp.
	/// </summary>
	public partial class frmHelp : BaseForm
	{
		public frmHelp()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmHelp InstancePtr
		{
			get
			{
				return (frmHelp)Sys.GetInstance(typeof(frmHelp));
			}
		}

		protected frmHelp _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.ComingFromTheHelpScreen = true;
			modGNBas.Statics.response = "";
			modGNBas.Statics.Resp1 = "";
			frmHelp.InstancePtr.Hide();
		}

		public void cmdQuit_Click()
		{
			cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void frmHelp_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: KeyAscii As Variant, short --> As int	OnWriteFCConvert.ToInt16(
			int KeyAscii;
			// - "AutoDim"
			if (KeyCode == Keys.Escape)
			{
				KeyAscii = 0;
				cmdQuit_Click();
			}
		}

		private void frmHelp_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmHelp properties;
			//frmHelp.ScaleWidth	= 6585;
			//frmHelp.ScaleHeight	= 5250;
			//frmHelp.LinkTopic	= "Form2";
			//End Unmaped Properties
			modGNBas.Statics.response = "";
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobalVariables.Statics.ComingFromTheHelpScreen = false;
		}

		private void List1_DoubleClick(object sender, System.EventArgs e)
		{
			string strTemp;
			int intTemp;
			modGlobalVariables.Statics.ComingFromTheHelpScreen = true;
			modGNBas.Statics.Resp1 = List1.Text;
			strTemp = Strings.Trim(Strings.Mid(FCConvert.ToString(modGNBas.Statics.Resp1), 5, 8));
			intTemp = Strings.InStr(1, strTemp, " ", CompareConstants.vbTextCompare);
			if (intTemp > 0)
			{
				strTemp = Strings.Mid(strTemp, 1, intTemp - 1);
			}
			// response = Mid$(Resp1, 5, 4)
			modGNBas.Statics.response = strTemp;
			frmHelp.InstancePtr.Hide();
		}
	}
}
