﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMisc.
	/// </summary>
	public partial class srptMisc : FCSectionReport
	{
		public static srptMisc InstancePtr
		{
			get
			{
				return (srptMisc)Sys.GetInstance(typeof(srptMisc));
			}
		}

		protected srptMisc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptMisc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptMisc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (Conversion.Val(this.UserData) == -10)
			{
				// Unload Me
				this.Cancel();
				return;
			}
			
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			x = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			if (x < 0)
			{
				// not from valuation screen
				if (!modDataTypes.Statics.MR.EndOfFile())
				{
					modProperty.Statics.WKey = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood"))));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "Property", "Neighborhood", FCConvert.ToInt32(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ritrancode"))));
					lblNeighborhood.Text = "Neighborhood " + modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood");
					txtNeighborhood.Text = modGlobalVariables.Statics.CostRec.ClDesc;
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pistreetcode")) != 0)
					{
						lblTreegrowth.Text = "Tree Growth (Year)";
						// txtTreeGrowth.Text = MR.Fields("pistreetcode")
						txtTreeGrowth.Text = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("pistreetcode"));
					}
					else
					{
						lblTreegrowth.Text = "";
						// txtTreeGrowth.Text = ""
						txtTreeGrowth.Text = "";
					}
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pizone") + "")), "Property", "Zone", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
					modProperty.Statics.WORKA1 = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piseczone") + "")), "Property", "SecZone", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
					// .TextMatrix(2, 0) = "Zoning/Use"
					// txtZoning.Text = WORKA1
					txtZoning.Text = modProperty.Statics.WORKA1;
					// txtZoning.Text = txtZoning.Text & " " & CostRec.ClDesc
					txtZoning.Text = txtZoning.Text + " " + modGlobalVariables.Statics.CostRec.ClDesc;
					modProperty.Statics.WKey = FCConvert.ToInt32(1300 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pitopography1") + ""));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
					modProperty.Statics.WKey = FCConvert.ToInt32(1300 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pitopography2") + ""));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					// .TextMatrix(3, 0) = "Topography"
					txtTopography.Text = modProperty.Statics.WORKA1 + modGlobalVariables.Statics.CostRec.ClDesc;
					modProperty.Statics.WKey = FCConvert.ToInt32(1310 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piutilities1") + ""));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
					modProperty.Statics.WKey = FCConvert.ToInt32(1310 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piutilities2") + ""));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					txtUtilities.Text = modProperty.Statics.WORKA1 + modGlobalVariables.Statics.CostRec.ClDesc;
					modProperty.Statics.WKey = FCConvert.ToInt32(1320 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pistreet") + ""));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
					modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
					txtStreet.Text = modProperty.Statics.WORKA1;
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "") == 0)
					{
						modProperty.Statics.WORKA1 = " ";
						modREASValuations.Statics.WORKA2 = " ";
					}
					else
					{
						modProperty.Statics.WKey = 1330;
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc + "  ";
						if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "") > 0 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "") < 10)
						{
							modProperty.Statics.WKey = FCConvert.ToInt32(1330 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + ""));
							modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
							modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.CostRec.ClDesc;
						}
						else
						{
							modREASValuations.Statics.WORKA2 = modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "";
						}
					}
					lblOpen1.Text = modProperty.Statics.WORKA1;
					txtOpen1.Text = modREASValuations.Statics.WORKA2;
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + " ") == 0)
					{
						modProperty.Statics.WORKA1 = " ";
						modREASValuations.Statics.WORKA2 = " ";
					}
					else
					{
						modProperty.Statics.WKey = 1340;
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
						modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc + "  ";
						if (FCConvert.ToDouble(modGlobalVariables.Statics.CostRec.CUnit) == 99999999 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + "") > 0 && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + "") < 10)
						{
							modProperty.Statics.WKey = FCConvert.ToInt32(1340 + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + " "));
							modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
							modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.CostRec.ClDesc;
						}
						else
						{
							modREASValuations.Statics.WORKA2 = modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + "";
						}
					}
					lblOpen2.Text = modProperty.Statics.WORKA1;
					txtOpen2.Text = modREASValuations.Statics.WORKA2;
					// .TextMatrix(8, 0) = "Ref 1"
					txtReference1.Text = modDataTypes.Statics.MR.Get_Fields_String("rsref1") + "";
					// .TextMatrix(9, 0) = "Ref 2"
					TxtReference2.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsref2") + "");
					if (modGlobalVariables.Statics.ValuationReportOption == 1 || modGlobalVariables.Statics.ValuationReportOption == 3)
					{
						lblTranLandBldg.Text = "Tran/Land/Bldg";
						txtTranLandBldg.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("RITRANCODE") + "")) + " " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("RILANDCODE") + "")) + " " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("RIBLDGCODE") + ""));
					}
					else if (modGlobalVariables.Statics.ValuationReportOption == 2)
					{
						lblTranLandBldg.Text = "Bldg Codes";
						txtTranLandBldg.Text = modDataTypes.Statics.MR.Get_Fields_Int32("RIBLDGCODE") + "";
					}
					// TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
					if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields("pixcoord") + "") != string.Empty || Strings.Trim(modDataTypes.Statics.MR.Get_Fields("piycoord") + "") != string.Empty)
					{
						// TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
						if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields("pixcoord") + "") != string.Empty)
						{
							modProperty.Statics.WKey = 1091;
							modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
							modProperty.Statics.WORKA1 = "X Coordinate";
							if (Strings.Mid(modGlobalVariables.Statics.CostRec.ClDesc, 1, 2) != "..")
								modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
							lblXCoord.Text = modProperty.Statics.WORKA1;
							// TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
							txtXCoord.Text = modDataTypes.Statics.MR.Get_Fields("pixcoord") + "";
						}
						// TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
						if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields("piycoord") + "") != string.Empty)
						{
							modProperty.Statics.WKey = 1092;
							modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
							modProperty.Statics.WORKA1 = "Y Coordinate";
							if (Strings.Mid(modGlobalVariables.Statics.CostRec.ClDesc, 1, 2) != "..")
								modProperty.Statics.WORKA1 = modGlobalVariables.Statics.CostRec.ClDesc;
							lblYCoord.Text = modProperty.Statics.WORKA1;
							// TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
							txtYCoord.Text = modDataTypes.Statics.MR.Get_Fields("piycoord") + "";
						}
					}
					txtExemptions.Text = "";
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd1") + "") != 0 || Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd2") + "") != 0 || Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd3") + "") != 0)
					{
						if (modGlobalVariables.Statics.ValuationReportOption == 2 || modGlobalVariables.Statics.ValuationReportOption == 3)
						{
							// .TextMatrix(13, 0) = "Exemption Codes"
							txtExemptions.Text = modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd1") + "  " + modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd2") + "  " + modDataTypes.Statics.MR.Get_Fields_Int32("riexemptcd3");
						}
					}
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood") + "") == 0)
						return;
					// dont need to process anymore
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood")) == 0)
						modDataTypes.Statics.MR.Set_Fields("pineighborhood", 1);
					modREASValuations.Statics.WorkNeighborhood = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pineighborhood") + "")));
					modREASValuations.Statics.WorkZone = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pizone"))));
					if (modDataTypes.Statics.MR.Get_Fields_Boolean("zoneoverride") && Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piseczone")) > 0)
					{
						modREASValuations.Statics.WorkZone = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("piseczone"))));
					}
					if (modREASValuations.Statics.WorkZone < 1)
						modREASValuations.Statics.WorkZone = 1;
					REAS03_3501_TAGA:
					;
					txtLandSchedule.Text = modGlobalVariables.Statics.LandTRec.Schedule(ref modREASValuations.Statics.WorkNeighborhood, ref modREASValuations.Statics.WorkZone).ToString();
				}
			}
			else
			{
				// With frmNewValuationReport.MiscGrid(x)
				lblNeighborhood.Text = "Neighborhood " + FCConvert.ToString(modSpeedCalc.Statics.CalcPropertyList[x + 1].NeighborhoodCode);
				txtNeighborhood.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].NeighborhoodDesc;
				txtTreeGrowth.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].TreeGrowth;
				if (Conversion.Val(modSpeedCalc.Statics.CalcPropertyList[x + 1].TreeGrowth) > 0)
				{
					lblTreegrowth.Text = "Tree Growth Year";
				}
				else
				{
					lblTreegrowth.Text = "";
				}
				txtZoning.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Zone;
				txtTopography.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Topography;
				txtUtilities.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Utilities;
				txtStreet.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Street;
				lblOpen1.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Open1Title;
				txtOpen1.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Open1;
				lblOpen2.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Open2Title;
				txtOpen2.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Open2;
				txtReference1.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Ref1;
				TxtReference2.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].Ref2;
				txtTranLandBldg.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].TranLandBldgCode;
				lblTranLandBldg.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].TranLandBldgDesc;
				lblXCoord.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].XCoordTitle;
				txtXCoord.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].XCoord;
				lblYCoord.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].YCoordTitle;
				txtYCoord.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].YCoord;
				txtExemptions.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].ExemptCodes;
				txtLandSchedule.Text = modSpeedCalc.Statics.CalcPropertyList[x + 1].LandSchedule.ToString();
			}
		}

		
	}
}
