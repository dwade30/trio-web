﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptBadAccounts.
	/// </summary>
	public partial class rptBadAccounts : BaseSectionReport
	{
		public rptBadAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Bad Accounts";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptBadAccounts InstancePtr
		{
			get
			{
				return (rptBadAccounts)Sys.GetInstance(typeof(rptBadAccounts));
			}
		}

		protected rptBadAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBadAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// goes through each row in gridbadaccounts and puts it in the report
		int lngCRow;
		int lngBadExempts;
		int lngBadExemptVals;
		int lngBadMaps;
		int lngBadUpdated;
		int lngBadDeleted;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngCRow >= frmImportCompetitorXF.InstancePtr.GridBadAccounts.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngCRow = 1;
			txtTotal.Text = Strings.Format(frmImportCompetitorXF.InstancePtr.GridBadAccounts.Rows - 1, "#,###,###,##0");
			lngBadExempts = 0;
			lngBadExemptVals = 0;
			lngBadMaps = 0;
			lngBadUpdated = 0;
			lngBadDeleted = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			txtMapLot.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTMAPLOT));
			txtName.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTOWNER));
			txtTRIOAccount.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTTRIOACCOUNT));
			txtOtherAccount.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTVISIONACCOUNT));
			txtLocation.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTSTREETNUM) + " " + frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTSTREET));
			strTemp = "";
			if (FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTBADEXEMPT)))
			{
				strTemp += "Bad Exempt Code,";
				lngBadExempts += 1;
			}
			if (FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTBADEXEMPTAMOUNT)))
			{
				strTemp += "Bad Exempt Amount,";
				lngBadExemptVals += 1;
			}
			if (FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTBADMAPLOT)))
			{
				strTemp += "Bad Map Lot,";
				lngBadMaps += 1;
			}
			if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTUPDATED)) > 1)
			{
				strTemp += "Updated " + FCConvert.ToString(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTUPDATED))) + " times,";
				lngBadUpdated += 1;
			}
			if (frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTDELETED) == string.Empty)
			{
				frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTDELETED, FCConvert.ToString(false));
			}
			else
			{
				if (FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(lngCRow, modGridConstantsXF.CNSTDELETED)))
				{
					strTemp += "Deleted Account,";
					lngBadDeleted += 1;
				}
			}
			if (strTemp != string.Empty)
			{
				// trim off the comma
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			txtDescription.Text = strTemp;
			lngCRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalExemptCodes.Text = Strings.Format(lngBadExempts, "#,###,###,##0");
			txtTotalExemptValues.Text = Strings.Format(lngBadExemptVals, "#,###,###,##0");
			txtTotalDeleted.Text = Strings.Format(lngBadDeleted, "#,###,###,##0");
			txtTotalMapLots.Text = Strings.Format(lngBadMaps, "#,###,###,##0");
			txtTotalUpdates.Text = Strings.Format(lngBadUpdated, "#,###,###,##0");
		}

		
	}
}
