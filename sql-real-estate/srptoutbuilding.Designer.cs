﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptoutbuilding.
	/// </summary>
	partial class srptoutbuilding
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptoutbuilding));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtout = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaverage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaverage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtout,
				this.txtCount,
				this.txtaverage,
				this.txtAssess
			});
			this.Detail.Height = 0.2291667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Name = "PageFooter";
			// 
			// txtout
			// 
			this.txtout.Height = 0.19F;
			this.txtout.Left = 0.0625F;
			this.txtout.Name = "txtout";
			this.txtout.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtout.Text = "Field1";
			this.txtout.Top = 0.03125F;
			this.txtout.Width = 1.9375F;
			// 
			// txtCount
			// 
			this.txtCount.CanGrow = false;
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 3.4375F;
			this.txtCount.MultiLine = false;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtCount.Text = "Field2";
			this.txtCount.Top = 0.03125F;
			this.txtCount.Width = 0.875F;
			// 
			// txtaverage
			// 
			this.txtaverage.CanGrow = false;
			this.txtaverage.Height = 0.19F;
			this.txtaverage.Left = 6.3125F;
			this.txtaverage.MultiLine = false;
			this.txtaverage.Name = "txtaverage";
			this.txtaverage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtaverage.Text = "Field3";
			this.txtaverage.Top = 0.03125F;
			this.txtaverage.Width = 1.125F;
			// 
			// txtAssess
			// 
			this.txtAssess.CanGrow = false;
			this.txtAssess.Height = 0.19F;
			this.txtAssess.Left = 4.9375F;
			this.txtAssess.MultiLine = false;
			this.txtAssess.Name = "txtAssess";
			this.txtAssess.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAssess.Text = "Field4";
			this.txtAssess.Top = 0.03125F;
			this.txtAssess.Width = 1.3125F;
			// 
			// srptoutbuilding
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaverage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtout;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaverage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssess;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
