﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptGlobalChanges.
	/// </summary>
	public partial class rptGlobalChanges : BaseSectionReport
	{
		public static rptGlobalChanges InstancePtr
		{
			get
			{
				return (rptGlobalChanges)Sys.GetInstance(typeof(rptGlobalChanges));
			}
		}

		protected rptGlobalChanges _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptGlobalChanges()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Global Changes";
		}
		// nObj = 1
		//   0	rptGlobalChanges	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngPage;
		clsDRWrapper clsLoad = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtPage.Text = "Page 1";
			lngPage = 1;
		}

		public void Init(string strSQL, string strTitle, bool modalDialog)
		{
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			txtTitle2.Text = strTitle;
			// Me.Show vbModal, MDIParent
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsLoad.EndOfFile())
			{
				txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("rsaccount"));
				txtCard.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("rscard"));
				clsLoad.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

		
	}
}
