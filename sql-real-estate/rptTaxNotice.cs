﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using Wisej.Web;
using fecherFoundation.Extensions;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptTaxNotice.
	/// </summary>
	public partial class rptTaxNotice : BaseSectionReport
	{
		public static rptTaxNotice InstancePtr
		{
			get
			{
				return (rptTaxNotice)Sys.GetInstance(typeof(rptTaxNotice));
			}
		}

		protected rptTaxNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsSngAccount?.Dispose();
				clsTaxAccounts?.Dispose();
                clsTaxAccounts = null;
                clsSngAccount = null;
            }
			base.Dispose(disposing);
		}

		public rptTaxNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Notice";
		}
		// nObj = 1
		//   0	rptTaxNotice	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolUseFreeForm;
		bool boolEstimatedTax;
		bool boolShowExemptions;
		bool boolIncExemptAccounts;
		string strYear;
		string strRate;
		int lngTotal;
		clsDRWrapper clsSngAccount = new clsDRWrapper();
		clsDRWrapper clsTaxAccounts = new clsDRWrapper();
		int intNoticesPerPage;
		bool boolBillingVals;
		bool boolCondenseWhiteSpace;
		// vbPorter upgrade warning: intWhichAccounts As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intPerPage As short	OnWriteFCConvert.ToInt32(
		public void Start(ref bool boolFreeForm, ref bool boolShowEstimatedTax, ref bool boolShowExempt, ref bool boolIncExempts, ref int intWhichAccounts, ref string strOrder, string strTaxYear = "", string strTaxRate = "", int intPerPage = 3, bool boolBillingValues = true, bool boolCondense = false)
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strREFullDBName;
				string strMasterJoin;
				string strMasterJoinJoin = "";
				strREFullDBName = clsTaxAccounts.Get_GetFullDBName("RealEstate");
				strMasterJoin = modREMain.GetMasterJoin();
				string strMasterJoinQuery;
				strMasterJoinQuery = "(" + strMasterJoin + ") mj";
				string strSQL1 = "";
				string strSQL2 = "";
				string strSelect = "";
				string strExempts = "";
				string strLandField = "";
				string strBldgField = "";
				string strExemptField = "";
				int lngUID;
				boolCondenseWhiteSpace = boolCondense;
				lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
				boolBillingVals = boolBillingValues;
				boolUseFreeForm = boolFreeForm;
				boolEstimatedTax = boolShowEstimatedTax;
				boolShowExemptions = boolShowExempt;
				boolIncExemptAccounts = boolIncExempts;
				if (boolBillingValues)
				{
					strLandField = "lastlandval";
					strBldgField = "lastbldgval";
					strExemptField = "rlexemption";
				}
				else
				{
					strLandField = "rllandval";
					strBldgField = "rlbldgval";
					strExemptField = "CORRexemption";
				}
				intNoticesPerPage = intPerPage;
				strYear = strTaxYear;
				strRate = strTaxRate;
				if (!boolIncExempts)
				{
					// strExempts = " ( ((sum(" & strlandfield & ") + sum(lastbldgval) - sum(rlexemption) > 0)) and ((sum(lastlandval) + sum(lastbldgval)) > 0)) "
					strExempts = " ( ((sum(" + strLandField + ") + sum(" + strBldgField + ") - sum(" + strExemptField + ") > 0)) and ((sum(" + strLandField + ") + sum(" + strBldgField + ")) > 0)) ";
				}
				else
				{
					strExempts = "";
				}
				if (Strings.UCase(strOrder) == "ACCOUNT")
				{
					strSelect = "master";
					// strSelect = "(" & strMasterJoin & " order by rsaccount)"
				}
				else if (Strings.UCase(strOrder) == "MAP\\LOT")
				{
					// strSelect = "(select * from master order by rsmaplot)"
					// strSelect = "(" & strMasterJoin & " order by rsmaplot)"
					strSelect = "(" + strMasterJoin + ") as tbl1";
				}
				else if (Strings.UCase(strOrder) == "NAME")
				{
					// strSelect = "(select * from master order by rsname)"
					strSelect = "(" + strMasterJoin + ") as tbl1";
				}
				else if (Strings.UCase(strOrder) == "LOCATION")
				{
					// strSelect = "(select * from master order by rslocstreet,rslocnumalph)"
					strSelect = "(" + strMasterJoin + ") as tbl1";
				}
				switch (intWhichAccounts)
				{
					case 1:
						{
							strSQL1 = "Select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum(" + strExemptField + ") as exemptsum from " + strSelect + " where not rsdeleted = 1 group by rsaccount ";
							// strSQL2 = "select * from master where rscard = 1 and not rsdeleted = 1 "
							strSQL2 = strMasterJoin + " where rscard = 1 and not rsdeleted = 1 ";
							if (strExempts != string.Empty)
							{
								strSQL1 += "having " + strExempts;
								// strSQL2 = "select * from master where rscard = 1 and (not rsdeleted = 1) and rsaccount in (select rsaccount from master group by rsaccount having " & strExempts & ") "
								strSQL2 = strMasterJoin + " where rscard = 1 and (not rsdeleted = 1) and rsaccount in (select rsaccount from master group by rsaccount having " + strExempts + ") ";
							}
							// Call clsTaxAccounts.OpenRecordset("Select rsaccount,sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum,sum(rlexemption) as exemptsum from master where not rsdeleted = 1 group by rsaccount order by rsaccount", strredatabase)
							// Call clsSngAccount.OpenRecordset("Select * from master where rscard = 1 and not rsdeleted = 1 order by rsaccount", strredatabase, dbOpenForwardOnly)
							break;
						}
					case 2:
						{
							if (Strings.UCase(strOrder) == "ACCOUNT")
							{
								strSQL1 = "select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum(" + strExemptField + ") as exemptsum from master where (rsaccount between " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + ") and (not rsdeleted = 1) group by rsaccount";
								// strSQL2 = "select * from master where (rscard = 1) and (rsaccount between " & gintMinAccountRange & " and " & gintMaxAccountRange & ") and (not rsdeleted = 1)"
								strSQL2 = strMasterJoin + " where (rscard = 1) and (rsaccount between " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + ") and (not rsdeleted = 1)";
							}
							else if (Strings.UCase(strOrder) == "MAP\\LOT")
							{
								strSQL1 = "select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum(" + strExemptField + ") as exemptsum from  master  where (rsmaplot between '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and '" + modPrintRoutines.Statics.gstrMaxAccountRange + "') and (not rsdeleted = 1) group by rsaccount";
								// strSQL2 = "select * from master where (rscard = 1) and (rsmaplot between '" & gstrMinAccountRange & "' and '" & gstrMaxAccountRange & "') and (not rsdeleted = 1)"
								strSQL2 = strMasterJoin + " where (rscard = 1) and (rsmaplot between '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and '" + modPrintRoutines.Statics.gstrMaxAccountRange + "') and (not rsdeleted = 1)";
							}
							else if (Strings.UCase(strOrder) == "NAME")
							{
								// strSQL1 = "select rsaccount,sum(" & strLandField & ") as landsum,sum(" & strBldgField & ") as bldgsum,sum(" & strExemptField & ") as exemptsum from (select * from master ) where (rsname between '" & gstrMinAccountRange & "' and '" & gstrMaxAccountRange & "') and (not rsdeleted = 1) group by rsaccount"
								strSQL1 = "select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum(" + strExemptField + ") as exemptsum from (" + strMasterJoin + " ) as tbl1 where (rsname between '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and '" + modPrintRoutines.Statics.gstrMaxAccountRange + "') and (not rsdeleted = 1) group by rsaccount";
								// strSQL2 = "select * from master where (rscard = 1) and (rsname between '" & gstrMinAccountRange & "' and '" & gstrMaxAccountRange & "') and (not rsdeleted = 1)"
								strSQL2 = strMasterJoin + " where (rscard = 1) and (rsname between '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and '" + modPrintRoutines.Statics.gstrMaxAccountRange + "') and (not rsdeleted = 1)";
							}
							else if (Strings.UCase(strOrder) == "LOCATION")
							{
								// strSQL1 = "select rsaccount,sum(" & strLandField & ") as landsum,sum(" & strBldgField & ") as bldgsum,sum(" & strExemptField & ") as exemptsum from (select * from master order by rslocstreet,val(rslocnumalph)) where (rslocstreet between '" & gstrMinAccountRange & "' and '" & gstrMaxAccountRange & "') and (not rsdeleted = 1) group by rsaccount"
								strSQL1 = "select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum(" + strExemptField + ") as exemptsum from master where (rslocstreet between '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and '" + modPrintRoutines.Statics.gstrMaxAccountRange + "') and (not rsdeleted = 1) group by rsaccount";
								// strSQL2 = "select * from master where (rscard = 1) and (rslocstreet between '" & gstrMinAccountRange & "' and '" & gstrMaxAccountRange & "') and (not rsdeleted = 1)"
								strSQL2 = strMasterJoin + " where (rscard = 1) and (rslocstreet between '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and '" + modPrintRoutines.Statics.gstrMaxAccountRange + "') and (not rsdeleted = 1)";
							}
							if (strExempts != string.Empty)
							{
								strSQL1 += " having " + strExempts;
								strSQL2 += " and rsaccount in (select rsaccount from master group by rsaccount having " + strExempts + ") ";
							}
							break;
						}
					case 3:
						{
							strSQL1 = "select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum(" + strExemptField + ") as exemptsum from ";
							// strSQL2 = "select * from master where (rscard = 1) and (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " & lngUID & ")"
							strSQL2 = strMasterJoin + " where (rscard = 1) and (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ")";
							if (Strings.UCase(strOrder) == "ACCOUNT")
							{
								strSQL1 += "master where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ") group by rsaccount";
							}
							else if (Strings.UCase(strOrder) == "MAP\\LOT")
							{
								strSQL1 += " master where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ") group by rsaccount";
							}
							else if (Strings.UCase(strOrder) == "NAME")
							{
								strSQL1 += "master where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ") group by rsaccount";
							}
							else if (Strings.UCase(strOrder) == "LOCATION")
							{
								strSQL1 += " master where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ") group by rsaccount";
							}
							if (strExempts != string.Empty)
							{
								strSQL1 += " having " + strExempts;
								strSQL2 += " and rsaccount in (select rsaccount from master group by rsaccount having " + strExempts + ") ";
							}
							break;
						}
					case 4:
						{
							strSQL1 = "select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum(" + strExemptField + ") as exemptsum from ";
							// strSQL2 = "select * from master where (rscard = 1) and (not rsdeleted = 1) and rsaccount in (select rsaccount from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from extracttable where userid = " & lngUID & " and groupnumber in (select accountnumber from labelaccounts where userid = " & lngUID & ")))"
							strSQL2 = strMasterJoin + " where (rscard = 1) and (not rsdeleted = 1) and rsaccount in (select rsaccount from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from extracttable where userid = " + FCConvert.ToString(lngUID) + " and groupnumber in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ")))";
							if (Strings.UCase(strOrder) == "ACCOUNT")
							{
								strSQL1 += " master where (not rsdeleted = 1) and rsaccount in (select accountnumber from extracttable where userid = " + FCConvert.ToString(lngUID) + " and groupnumber in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ")) group by rsaccount";
							}
							else if (Strings.UCase(strOrder) == "MAP\\LOT")
							{
								strSQL1 += " master where (not rsdeleted = 1) and rsaccount in (select distinct accountnumber from extracttable where userid = " + FCConvert.ToString(lngUID) + " and groupnumber in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + "))  group by rsaccount ";
							}
							else if (Strings.UCase(strOrder) == "NAME")
							{
								strSQL1 += "master where (not rsdeleted = 1) and rsaccount in (select distinct accountnumber from extracttable where userid = " + FCConvert.ToString(lngUID) + " and groupnumber in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + "))  group by rsaccount ";
							}
							else if (Strings.UCase(strOrder) == "LOCATION")
							{
								strSQL1 += " master where (not rsdeleted = 1) and rsaccount in (select distinct accountnumber from extracttable where userid = " + FCConvert.ToString(lngUID) + " and groupnumber in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + "))  group by rsaccount ";
							}
							if (strExempts != string.Empty)
							{
								strSQL1 += " having " + strExempts;
								strSQL2 += " and rsaccount in (select rsaccount from master group by rsaccount having " + strExempts + ") ";
							}
							break;
						}
				}
				//end switch
				if (Strings.UCase(strOrder) == "ACCOUNT")
				{
					strSQL1 += " order by rsaccount";
					strSQL2 += " order by rsaccount";
				}
				else if (Strings.UCase(strOrder) == "MAP\\LOT")
				{
					// strSQL1 = strSQL1 & " order by rsmaplot"
					strSQL2 += " order by rsmaplot";
				}
				else if (Strings.UCase(strOrder) == "NAME")
				{
					// strSQL1 = strSQL1 & " order by rsname"
					strSQL2 += " order by rsname";
				}
				else if (Strings.UCase(strOrder) == "LOCATION")
				{
					// strSQL1 = strSQL1 & " order by rslocstreet,val(rslocnumalph)"
					strSQL2 += " order by rslocstreet,rslocnumalph";
				}
				if (!clsTaxAccounts.OpenRecordset(strSQL1, modGlobalVariables.strREDatabase))
				{
					MessageBox.Show("Unable to open records to complete this report", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					rptTaxNotice.InstancePtr.Close();
					return;
				}
				if (!clsSngAccount.OpenRecordset(strSQL2, modGlobalVariables.strREDatabase))
				{
					MessageBox.Show("Unable to open records to complete this report", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					rptTaxNotice.InstancePtr.Close();
					return;
				}
				// rptTaxNotice.Show
				frmReportViewer.InstancePtr.Init(this);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				rptTaxNotice.InstancePtr.Close();
				return;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!clsSngAccount.EndOfFile())
				eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                float lngTop;
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                switch (intNoticesPerPage)
                {
                    case 1:
                    {
                        Detail.Height = 15080 / 1440f;
                        if (!boolCondenseWhiteSpace)
                        {
                            rtbMsg1.CanShrink = false;
                        }
                        else
                        {
                            rtbMsg1.CanShrink = true;
                        }

                        break;
                    }
                    case 2:
                    {
                        Detail.Height = 7500 / 1440f;
                        if (!boolCondenseWhiteSpace)
                        {
                            rtbMsg1.CanShrink = false;
                        }
                        else
                        {
                            rtbMsg1.CanShrink = true;
                        }

                        break;
                    }
                    case 3:
                    {
                        // default.  It is already designed this way
                        Detail.Height = 5000 / 1440f;
                        rtbMsg1.CanShrink = false;
                        break;
                    }
                }

                //end switch
                lngTop = Detail.Height - 2825 / 1440f;
                rtbMsg1.Height = (lngTop - rtbMsg1.Top);
                rtbMsg2.Top = lngTop;
                txtAccount.Top = lngTop;
                txtMapLot.Top = lngTop;
                txtLocation.Top = lngTop + 240 / 1440f;
                txtMail1.Top = lngTop + 960 / 1440f;
                txtMail2.Top = lngTop + 1200 / 1440f;
                txtMail3.Top = lngTop + 1;
                txtMail4.Top = lngTop + 1680 / 1440f;
                txtMail5.Top = lngTop + 1920 / 1440f;
                txtLandLabel.Top = lngTop + 720 / 1440f;
                txtBldgLabel.Top = lngTop + 720 / 1440f;
                txtExemptLabel.Top = lngTop + 720 / 1440f;
                txtLand.Top = lngTop + 960 / 1440f;
                txtBldg.Top = lngTop + 960 / 1440f;
                txtExempt.Top = lngTop + 960 / 1440f;
                txtTotalLabel.Top = lngTop + 1;
                txtTotal.Top = lngTop + 1;
                txtTaxLabel.Top = lngTop + 1680 / 1440f;
                txtTax.Top = lngTop + 1680 / 1440f;
                Field1.Top = lngTop + 2325 / 1440f;
                Line1.Y1 = lngTop + 2685 / 1440f;
                Line1.Y2 = Line1.Y1;
                clsLoad.OpenRecordset("select * from defaults", modGlobalVariables.strREDatabase);
                txtTitle.Text = FCConvert.ToString(clsLoad.Get_Fields_String("taxnoticetitle"));
                rtbMsg1.Font = new Font("Tahoma", 10);
                //FC:FINAL:DSE WordWrapping is not working in RichTextBox
                //rtbMsg1.Text = FCConvert.ToString(clsLoad.Get_Fields_String("taxnoticemsg1"));
                rtbMsg1.SetHtmlText(FCConvert.ToString(clsLoad.Get_Fields_String("taxnoticemsg1")));
                if (boolUseFreeForm)
                {
                    rtbMsg2.Font = new Font("Tahoma", 10);
                    //FC:FINAL:DSE WordWrapping is not working in RichTextBox
                    //rtbMsg2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("taxnoticemsg2"));
                    rtbMsg2.SetHtmlText(FCConvert.ToString(clsLoad.Get_Fields_String("taxnoticemsg2")));
                    rtbMsg2.Visible = true;
                    txtLand.Visible = false;
                    txtBldg.Visible = false;
                    txtExempt.Visible = false;
                    txtTotal.Visible = false;
                    txtTotalLabel.Visible = false;
                    txtTaxLabel.Visible = false;
                    txtTax.Visible = false;
                    txtLandLabel.Visible = false;
                    txtBldgLabel.Visible = false;
                    txtExemptLabel.Visible = false;
                }
                else
                {
                    txtLandLabel.Visible = true;
                    txtBldgLabel.Visible = true;
                    txtLand.Visible = true;
                    txtBldg.Visible = true;
                    if (boolShowExemptions)
                    {
                        txtExempt.Visible = true;
                        txtExemptLabel.Visible = true;
                    }
                    else
                    {
                        txtExempt.Visible = false;
                        txtExemptLabel.Visible = false;
                    }

                    txtTotal.Visible = true;
                    txtTotalLabel.Visible = true;
                    if (boolEstimatedTax)
                    {
                        txtTax.Visible = true;
                        txtTaxLabel.Visible = true;
                    }
                    else
                    {
                        txtTax.Visible = false;
                        txtTaxLabel.Visible = false;
                    }
                }
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp;
			int lngLand = 0;
			int lngbldg = 0;
			int lngExempt = 0;
			//cParty tParty = new cParty();
			//cPartyController tPartyCont = new cPartyController();
			// Call clsSngAccount.FindFirstRecord("rsaccount", Val(clsTaxAccounts.GetData("rsaccount")))
			// Call clsTaxAccounts.FindFirstRecord("rsaccount", Val(clsSngAccount.Fields("rsaccount")))
			clsTaxAccounts.FindFirst("rsaccount = " + FCConvert.ToString(Conversion.Val(clsSngAccount.Get_Fields_Int32("rsaccount"))));
			strTemp = clsSngAccount.GetData("rslocnumalph") + " ";
			if (Conversion.Val(strTemp) != 0)
			{
				txtLocation.Text = Conversion.Str(Conversion.Val(strTemp + ""));
			}
			else
			{
				txtLocation.Text = "";
			}
			txtLocation.Text = txtLocation.Text + clsSngAccount.GetData("rslocapt") + " ";
			txtLocation.Text = txtLocation.Text + clsSngAccount.GetData("rslocstreet");
			txtLocation.Text = Strings.Trim(txtLocation.Text + "");
			string[] strAry = new string[5 + 1];
			int x;
			strAry[0] = FCConvert.ToString(clsSngAccount.Get_Fields_String("rsname"));
			x = 1;
			if (Strings.Trim(FCConvert.ToString(clsSngAccount.Get_Fields_String("rssecowner"))) != "")
			{
				strAry[x] = FCConvert.ToString(clsSngAccount.Get_Fields_String("rssecowner"));
				x += 1;
			}
			if (Strings.Trim(FCConvert.ToString(clsSngAccount.Get_Fields_String("rsaddr1"))) != "")
			{
				strAry[x] = FCConvert.ToString(clsSngAccount.GetData("rsaddr1"));
				x += 1;
			}
			if (Strings.Trim(FCConvert.ToString(clsSngAccount.Get_Fields_String("rsaddr2"))) != "")
			{
				strAry[x] = FCConvert.ToString(clsSngAccount.GetData("rsaddr2"));
				x += 1;
			}
			strAry[x] = Strings.Trim(clsSngAccount.Get_Fields_String("rsaddr3") + " " + clsSngAccount.Get_Fields_String("rsstate") + " " + clsSngAccount.Get_Fields_String("rszip") + " " + clsSngAccount.Get_Fields_String("rszip4"));
			txtMail1.Text = strAry[0];
			txtMail2.Text = strAry[1];
			txtMail3.Text = strAry[2];
			txtMail4.Text = strAry[3];
			txtMail5.Text = strAry[4];
			txtAccount.Text = "ACCT: " + clsSngAccount.GetData("rsaccount");
			txtMapLot.Text = "Map/Lot:" + clsSngAccount.GetData("rsmaplot");
			if (!boolUseFreeForm)
			{
				// txtTax10.Text = "    -LAND-    BUILDINGS   EXEMPTION"
				lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTaxAccounts.GetData("landsum"))));
				txtLand.Text = Strings.Format(lngLand, "#,###,##0");
				lngbldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTaxAccounts.GetData("bldgsum"))));
				txtBldg.Text = Strings.Format(lngbldg, "###,###,##0");
				lngTotal = lngLand + lngbldg;
				if (boolShowExemptions)
				{
					lngExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTaxAccounts.GetData("exemptsum"))));
					txtExempt.Text = Strings.Format(lngExempt, "##,###,##0");
					lngTotal -= lngExempt;
				}
				else
				{
					txtExempt.Text = "";
				}
				txtTotal.Text = Strings.Format(lngTotal, "##,###,###,##0");
				if (boolEstimatedTax)
				{
					txtTaxLabel.Text = "ESTIMATED " + strYear + " TAX:";
					txtTax.Text = Strings.Format(GetTax(), "###,##0.00");
				}
			}
			if (txtMail3.Text == "")
			{
				txtMail3.Text = txtMail2.Text;
				txtMail2.Text = txtMail1.Text;
				txtMail1.Text = "";
			}
			if (txtMail2.Text == "")
			{
				txtMail2.Text = txtMail1.Text;
				txtMail1.Text = "";
			}
			clsSngAccount.MoveNext();
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As float
		private float GetTax()
		{
			float GetTax = 0;
			float snglTax;
			snglTax = FCConvert.ToSingle(Conversion.Val(strRate + ""));
			snglTax = snglTax * lngTotal / 1000;
			GetTax = snglTax;
			return GetTax;
		}

		
	}
}
