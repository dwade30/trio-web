﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for ToolTipCtl.
	/// </summary>
	partial class ToolTipCtl : fecherFoundation.FCUserControl
	{
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCTimer Timer1;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCLabel lblToolTip;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmdOK = new fecherFoundation.FCButton();
            this.Timer1 = new fecherFoundation.FCTimer(this.components);
            this.Shape1 = new fecherFoundation.FCLabel();
            this.lblToolTip = new fecherFoundation.FCLabel();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdOK
            // 
            this.cmdOK.Location = new System.Drawing.Point(229, 127);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(68, 23);
            this.cmdOK.TabIndex = 1;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // Timer1
            // 
            this.Timer1.Enabled = false;
            this.Timer1.Interval = 900;
            this.Timer1.Location = new System.Drawing.Point(0, 0);
            this.Timer1.Name = null;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // Shape1
            // 
            this.Shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.Shape1.Font = new System.Drawing.Font("default", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Shape1.Name = "Shape1";
            this.Shape1.Size = new System.Drawing.Size(526, 162);
            this.Shape1.TabIndex = 2;
            // 
            // lblToolTip
            // 
            this.lblToolTip.AutoSize = true;
            this.lblToolTip.Capitalize = false;
            this.lblToolTip.BackColor = System.Drawing.Color.Transparent;
            this.lblToolTip.Font = new System.Drawing.Font("default", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblToolTip.Location = new System.Drawing.Point(28, 22);
            this.lblToolTip.Name = "lblToolTip";
            this.lblToolTip.Size = new System.Drawing.Size(4, 11);
            this.lblToolTip.TabIndex = 3;
            // 
            // ToolTipCtl
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.Shape1);
            this.Controls.Add(this.lblToolTip);
            this.Name = "ToolTipCtl";
            this.Size = new System.Drawing.Size(526, 162);
            this.UserControlBackColor = System.Drawing.Color.FromName("@window");
            this.UserControlHeight = 1944;
            this.UserControlWidth = 6312;
            this.Resize += new System.EventHandler(this.ToolTipCtl_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        #endregion

        private System.ComponentModel.IContainer components;
    }
}
