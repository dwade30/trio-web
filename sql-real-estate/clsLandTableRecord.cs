﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class clsLandTableRecord
	{
		//=========================================================
		private string ltcode = "";
		private string ltfactor = "";
		private string ltbldg = "";
		private double dblLandFactor;
		private double dblBldgFactor;
		private int lngSchedule;
		private bool boolFromArrays;
		private int lngMinZone;

		private struct TRec
		{
			public double Lfact;
			public double Bfact;
			public int Sched;
			public bool Defined;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public TRec(int unusedParm)
			{
				this.Lfact = 0;
				this.Bfact = 0;
				this.Sched = 0;
				this.Defined = false;
			}
		};

		private TRec[,] aryTRec;
		private int lngCurrentTown;

		public int DefaultZone
		{
			get
			{
				int DefaultZone = 0;
				DefaultZone = lngMinZone;
				return DefaultZone;
			}
		}

		public void SetupArrays(int lngTown, bool boolForce = false)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsZone = new clsDRWrapper();
			int lngZone;
			int lngNeigh;
			int lngMaxZone;
			int lngMaxNeigh;
			if (lngTown == lngCurrentTown && !boolForce)
				return;
			lngCurrentTown = lngTown;
			rsLoad.OpenRecordset("select min(code) as minzone from zones where townnumber = " + FCConvert.ToString(lngTown) + " and code > 0", modGlobalVariables.strREDatabase);
			lngMinZone = 0;
			if (!rsLoad.EndOfFile())
			{
				// lngMinZone = Val(rsLoad.Fields("minzone"))
			}
			else
			{
				lngMinZone = 0;
			}
			rsLoad.OpenRecordset("select max(zone) as maxzone,max(neighborhood) as maxnhood from zoneschedule where townnumber = " + FCConvert.ToString(lngTown), modGlobalVariables.strREDatabase);
			if (!rsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [maxzone] not found!! (maybe it is an alias?)
				lngMaxZone = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("maxzone"))));
				// TODO Get_Fields: Field [maxnhood] not found!! (maybe it is an alias?)
				lngMaxNeigh = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("maxnhood"))));
				aryTRec = new TRec[lngMaxZone + 1, lngMaxNeigh + 1];
			}
			rsLoad.OpenRecordset("select * from zoneschedule where townnumber = " + FCConvert.ToString(lngTown), modGlobalVariables.strREDatabase);
			while (!rsLoad.EndOfFile())
			{
				lngZone = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("zone"))));
				lngNeigh = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("neighborhood"))));
				aryTRec[lngZone, lngNeigh].Bfact = Conversion.Val(rsLoad.Get_Fields_Double("bldgfactor"));
				aryTRec[lngZone, lngNeigh].Lfact = Conversion.Val(rsLoad.Get_Fields_Double("landfactor"));
				aryTRec[lngZone, lngNeigh].Sched = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("schedule"))));
				aryTRec[lngZone, lngNeigh].Defined = true;
				rsLoad.MoveNext();
			}
		}

		public double LandFactor(ref int lngNeighborhood, ref int lngZone)
		{
			double LandFactor = 0;
			int lngTown;
			lngTown = modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
			if (modGlobalVariables.Statics.boolUseArrays)
			{
				if (lngZone > Information.UBound(aryTRec, 1) || lngNeighborhood > Information.UBound(aryTRec, 2))
				{
					LandFactor = 0;
				}
				else
				{
					LandFactor = aryTRec[lngZone, lngNeighborhood].Lfact;
				}
			}
			else
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from zoneschedule where zone = " + FCConvert.ToString(lngZone) + " and neighborhood = " + FCConvert.ToString(lngNeighborhood) + " and townnumber = " + FCConvert.ToString(lngTown), modGlobalVariables.strREDatabase);
				if (!rsLoad.EndOfFile())
				{
					LandFactor = Conversion.Val(rsLoad.Get_Fields_Double("landfactor"));
				}
				else
				{
					LandFactor = 0;
				}
			}
			return LandFactor;
		}

		public double BldgFactor(ref int lngNeighborhood, ref int lngZone)
		{
			double BldgFactor = 0;
			int lngTown;
			lngTown = modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
			if (modGlobalVariables.Statics.boolUseArrays)
			{
				if (lngZone > Information.UBound(aryTRec, 1) || lngNeighborhood > Information.UBound(aryTRec, 2))
				{
					BldgFactor = 1;
				}
				else
				{
					if (aryTRec[lngZone, lngNeighborhood].Defined)
					{
						BldgFactor = aryTRec[lngZone, lngNeighborhood].Bfact;
					}
					else
					{
						BldgFactor = 1;
					}
				}
			}
			else
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from zoneschedule where zone = " + FCConvert.ToString(lngZone) + " and neighborhood = " + FCConvert.ToString(lngNeighborhood) + " and townnumber = " + FCConvert.ToString(lngTown), modGlobalVariables.strREDatabase);
				if (!rsLoad.EndOfFile())
				{
					BldgFactor = Conversion.Val(rsLoad.Get_Fields_Double("bldgfactor"));
				}
				else
				{
					BldgFactor = 1;
				}
			}
			return BldgFactor;
		}

		public double Schedule(ref int lngNeighborhood, ref int lngZone)
		{
			double Schedule = 0;
			int lngTown;
			lngTown = modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
			if (modGlobalVariables.Statics.boolUseArrays)
			{
				if (lngZone > Information.UBound(aryTRec, 1) || lngNeighborhood > Information.UBound(aryTRec, 2))
				{
					Schedule = 0;
				}
				else
				{
					Schedule = aryTRec[lngZone, lngNeighborhood].Sched;
				}
			}
			else
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from zoneschedule where zone = " + FCConvert.ToString(lngZone) + " and neighborhood = " + FCConvert.ToString(lngNeighborhood) + " and townnumber = " + FCConvert.ToString(lngTown), modGlobalVariables.strREDatabase);
				if (!rsLoad.EndOfFile())
				{
					Schedule = Conversion.Val(rsLoad.Get_Fields_Int32("schedule"));
				}
				else
				{
					Schedule = 0;
				}
			}
			return Schedule;
		}
	}
}
