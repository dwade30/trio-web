﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptOut.
	/// </summary>
	public partial class srptOut : FCSectionReport
	{
		public static srptOut InstancePtr
		{
			get
			{
				return (srptOut)Sys.GetInstance(typeof(srptOut));
			}
		}

		protected srptOut _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptOut()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptOut	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCurC;
		short intCurLine;
		
		int intHighestOutbldgNum;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			intCurLine += 1;
			if (intCurC < 0)
			{
				eArgs.EOF = intCurLine > 10;
				if (eArgs.EOF)
					return;
				// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
				if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields("oitype" + FCConvert.ToString(intCurLine)) + "") > 0)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			else
			{
				// EOF = intCurLine > (frmNewValuationReport.OutBuildingGrid(intCurC).Rows - 4)
				eArgs.EOF = intCurLine > intHighestOutbldgNum;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int x;
			intCurC = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			if (intCurC == -10)
			{
				// Unload Me
				this.Close();
				return;
			}
			if (intCurC >= 0)
			{
				for (x = 1; x <= 10; x++)
				{
					if (Strings.Trim(modSpeedCalc.Statics.CalcOutList[intCurC + 1].Description[x]) != string.Empty)
					{
						intHighestOutbldgNum = x;
					}
				}
				// x
			}
			intCurLine = 0;
			
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (Conversion.Val(this.UserData) != -10)
			{
				if (intCurC < 0)
				{
					// not from valuation screen
					double dblGrade = 0;
					int lngOIUnits = 0;
					int intCond = 0;
					string[] strAry = null;
					if (modOutbuilding.Statics.intOIType[intCurLine] > 699 && !modOutbuilding.Statics.boolUseSoundValue[intCurLine])
					{
						modDwelling.Statics.HoldGrade1 = modOutbuilding.Statics.intOIGradeCd[intCurLine];
						modDwelling.Statics.HoldGrade2 = modOutbuilding.Statics.intOIGradePct[intCurLine];
						modDwelling.Statics.HoldCondition = modOutbuilding.Statics.intOICondition[intCurLine];
					}
					else if (modSpeedCalc.Statics.gstrDwellCode != "Y" && (modSpeedCalc.Statics.gstrDwellCode != "C" || !modGNBas.Statics.gboolCommercial))
					{
						if (modOutbuilding.Statics.intOIGradeCd[intCurLine] != 0 && modOutbuilding.Statics.intOIGradeCd[intCurLine] != 9)
							modDwelling.Statics.HoldGrade1 = modOutbuilding.Statics.intOIGradeCd[intCurLine];
						if (modOutbuilding.Statics.intOIGradePct[intCurLine] != 0)
							modDwelling.Statics.HoldGrade2 = modOutbuilding.Statics.intOIGradePct[intCurLine];
						if (modOutbuilding.Statics.intOICondition[intCurLine] != 0)
							modDwelling.Statics.HoldCondition = modOutbuilding.Statics.intOICondition[intCurLine];
					}
					// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(FCConvert.ToInt16(Conversion.Val(modDataTypes.Statics.OUT.Get_Fields("oitype" + FCConvert.ToString(intCurLine)) + "")));
					txtDescription.Text = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
					txtYear.Text = modOutbuilding.Statics.intOIYear[intCurLine].ToString();
					// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
					lngOIUnits = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.OUT.Get_Fields("oiunits" + FCConvert.ToString(intCurLine)) + "")));
					// If lngOIUnits = 9999 Then
					if (modOutbuilding.Statics.boolUseSoundValue[intCurLine])
					{
						txtUnits.Width = txtEcon.Left - txtUnits.Left;
						txtUnits.Text = "    - - - - S O U N D  V A L U E - - - -    ";
						txtGrade.Visible = false;
						txtRCN.Visible = false;
						txtCond.Visible = false;
						txtPhy.Visible = false;
						txtFunc.Visible = false;
						txtEcon.Visible = false;
						// GoTo REAS03_6101_TAG
					}
					else
					{
						txtUnits.Width = 810 / 1440f;
						txtGrade.Visible = true;
						txtRCN.Visible = true;
						txtCond.Visible = true;
						txtPhy.Visible = true;
						txtFunc.Visible = true;
						txtEcon.Visible = true;
						// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
						if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields("oitype" + FCConvert.ToString(intCurLine)) + "") > 699)
						{
							txtUnits.Text = Strings.Format(lngOIUnits / 100, "#0") + "X" + Strings.Format(lngOIUnits - ((lngOIUnits / 100) * 100), "#0");
						}
						else
						{
							txtUnits.Text = lngOIUnits.ToString();
						}
						if (modOutbuilding.Statics.intOIType[intCurLine] > 699)
						{
							dblGrade = modOutbuilding.REAS03_6210_MOHOS(ref intCurLine);
						}
						else
						{
							dblGrade = modOutbuilding.REAS03_6200_REGULAR_OUTBUILDINGS(ref intCurLine);
						}
						txtGrade.Text = Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2) + Strings.Format(dblGrade * 100, "###");
						// txtGrade.Text = Mid(CR.Fields("CSDesc"), 1, 2) & Format(dblGrade * 100, "###")
						if (Conversion.Val(modGlobalVariables.Statics.CostRec.CSDesc) == 0)
						{
							txtGrade.Text = Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2) + Strings.Format(dblGrade * 100, "###");
						}
						else
						{
							strAry = Strings.Split(modGlobalVariables.Statics.CostRec.CSDesc, " ", -1, CompareConstants.vbTextCompare);
							if (Information.UBound(strAry, 1) > 0)
							{
								txtGrade.Text = Strings.Mid(strAry[1], 1, 2) + " " + Strings.Format(dblGrade * 100, "###");
							}
							else
							{
								txtGrade.Text = Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 2) + Strings.Format(dblGrade * 100, "###");
							}
						}
						txtRCN.Text = Strings.Format(modOutbuilding.Statics.lngRepCostNew[intCurLine], "###,####,##0");
						// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
						intCond = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.OUT.Get_Fields("oicond" + FCConvert.ToString(intCurLine)) + "")));
						// If (intCond = 9 Or intCond = 0) And (UCase(MR.Fields("rsdwellingcode")) = "Y" Or (UCase(MR.Fields("rsdwellingcode")) = "C" And gboolCommercial)) Then
						if ((intCond == 9 || intCond == 0) && (Strings.UCase(modSpeedCalc.Statics.gstrDwellCode) == "Y" || (Strings.UCase(modSpeedCalc.Statics.gstrDwellCode) == "C" && modGNBas.Statics.gboolCommercial)))
						{
							modProperty.Statics.WKey = 1460 + modDwelling.Statics.HoldCondition;
						}
						else
						{
							if (intCond > 0)
							{
								modProperty.Statics.WKey = 1460 + intCond;
							}
							else
							{
								if (modGlobalVariables.Statics.boolMobileHomePresent)
								{
									// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
									modProperty.Statics.WKey = FCConvert.ToInt32(1460 + Conversion.Val(modDataTypes.Statics.OUT.Get_Fields("oicond" + FCConvert.ToString(modGlobalVariables.Statics.intLastMobileHomeDone)) + ""));
								}
								else if (modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode") == "Y")
								{
									modProperty.Statics.WKey = FCConvert.ToInt32(1460 + Conversion.Val(modDataTypes.Statics.DWL.Get_Fields_Int32("diCONDITION") + ""));
								}
								else if (FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("rsdwellingcode")) == "C" && modGNBas.Statics.gboolCommercial)
								{
									modProperty.Statics.WKey = FCConvert.ToInt32(1460 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1condition") + ""));
								}
							}
						}
						if (modProperty.Statics.WKey == 1460)
						{
							txtCond.Text = "None";
						}
						else
						{
							modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
							txtCond.Text = Strings.Mid(modGlobalVariables.Statics.CostRec.CSDesc, 1, 4);
						}
						txtPhy.Text = Strings.Format(fecherFoundation.FCUtils.iDiv((modOutbuilding.Statics.dblPhysical[intCurLine] * 100), 1), "##0") + "%";
						txtFunc.Text = Strings.Format(fecherFoundation.FCUtils.iDiv(modOutbuilding.Statics.dblFunctional[intCurLine], 1), "##0") + "%";
						txtEcon.Text = Strings.Format(fecherFoundation.FCUtils.iDiv((modOutbuilding.Statics.OutEconomic * 100), 1), "##0") + "%";
					}
					REAS03_6101_TAG:
					;
					txtValue.Text = Strings.Format(modOutbuilding.Statics.lngRepCostNewLD[intCurLine], "#,##0");
				}
				else
				{
					// With frmNewValuationReport.OutBuildingGrid(intCurC)
					// txtDescription.Text = Trim(.TextMatrix(intCurLine, 0) & "")
					// txtYear.Text = .TextMatrix(intCurLine, 1)
					txtDescription.Text = Strings.Trim(modSpeedCalc.Statics.CalcOutList[intCurC + 1].Description[intCurLine]);
					txtYear.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].Year[intCurLine].ToString();
					// If Trim(.TextMatrix(intCurLine, 2)) = "- - - - S O U N D  V A L U E - - - -" Then
					if (Strings.Trim(modSpeedCalc.Statics.CalcOutList[intCurC + 1].Units[intCurLine]) == "- - - - S O U N D  V A L U E - - - -")
					{
						// txtUnits.CanGrow = True
						txtUnits.Width = txtEcon.Left - txtUnits.Left;
						// txtUnits.Text = Trim(.TextMatrix(intCurLine, 2))
						txtUnits.Text = Strings.Trim(modSpeedCalc.Statics.CalcOutList[intCurC + 1].Units[intCurLine]);
						txtGrade.Visible = false;
						txtRCN.Visible = false;
						txtCond.Visible = false;
						txtPhy.Visible = false;
						txtFunc.Visible = false;
						txtEcon.Visible = false;
					}
					else
					{
						// txtUnits.CanGrow = False
						// txtUnits.CanShrink = True
						// txtUnits.CanShrink = False
						txtUnits.Width = 810 / 1440f;
						txtGrade.Visible = true;
						txtRCN.Visible = true;
						txtCond.Visible = true;
						txtPhy.Visible = true;
						txtFunc.Visible = true;
						txtEcon.Visible = true;
						// txtUnits.Text = .TextMatrix(intCurLine, 2)
						// txtGrade.Text = .TextMatrix(intCurLine, 3)
						// txtRCN.Text = .TextMatrix(intCurLine, 4)
						// txtCond.Text = .TextMatrix(intCurLine, 5)
						// txtPhy.Text = .TextMatrix(intCurLine, 6)
						// txtFunc.Text = .TextMatrix(intCurLine, 7)
						// txtEcon.Text = .TextMatrix(intCurLine, 8)
						txtUnits.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].Units[intCurLine];
						txtGrade.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].Grade[intCurLine];
						txtRCN.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].Recon[intCurLine].ToString();
						txtCond.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].Condition[intCurLine];
						txtPhy.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].Physcial[intCurLine];
						txtFunc.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].Functional[intCurLine];
						txtEcon.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].Economic[intCurLine];
					}
					// txtValue.Text = .TextMatrix(intCurLine, 9)
					txtValue.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].OutbuildingValue[intCurLine];
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// Dim intCurrentCard
			// intCurrentCard = MR.Fields("rscard")
			// intCurrentCard = gintCardNumber
			if (intCurC < 0)
			{
				if (modDwelling.Statics.intSFLA != 0 && modGlobalVariables.Statics.intSFLAOption != 5)
				{
					if (modGlobalVariables.Statics.intSFLAOption == 1)
					{
						modOutbuilding.Statics.dblPerVal = (modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard] + modProperty.Statics.LandTotal[modGlobalVariables.Statics.intCurrentCard]) / modDwelling.Statics.intSFLA;
					}
					else if (modGlobalVariables.Statics.intSFLAOption == 2)
					{
						modOutbuilding.Statics.dblPerVal = (modProperty.Statics.LandTotal[modGlobalVariables.Statics.intCurrentCard] + modDwelling.Statics.lngSFLATotal) / modDwelling.Statics.intSFLA;
					}
					else if (modGlobalVariables.Statics.intSFLAOption == 3)
					{
						modOutbuilding.Statics.dblPerVal = modProperty.Statics.BuildingTotal[modGlobalVariables.Statics.intCurrentCard] / modDwelling.Statics.intSFLA;
					}
					else if (modGlobalVariables.Statics.intSFLAOption == 4)
					{
						modOutbuilding.Statics.dblPerVal = FCConvert.ToDouble(modDwelling.Statics.lngSFLATotal) / modDwelling.Statics.intSFLA;
					}
					// If MR.Fields("rsdwellingcode") = "C" And gboolCommercial Then
					if (modSpeedCalc.Statics.gstrDwellCode == "C" && modGNBas.Statics.gboolCommercial)
					{
						txtSFLA.Text = Strings.Format(modDwelling.Statics.intSFLA, "#,##0") + " " + "SF";
						if (modGlobalVariables.Statics.intSFLAOption != 6)
						{
							txtPerSFLA.Text = Strings.Format(modOutbuilding.Statics.dblPerVal, "#,##0.00") + " = $/SF (" + FCConvert.ToString(modGlobalVariables.Statics.intSFLAOption) + ")";
						}
					}
					else
					{
						txtSFLA.Text = Strings.Format(modDwelling.Statics.intSFLA, "#,##0") + " " + "SFLA";
						if (modGlobalVariables.Statics.intSFLAOption != 6)
						{
							txtPerSFLA.Text = Strings.Format(modOutbuilding.Statics.dblPerVal, "#,##0.00") + " = $/SFLA (" + FCConvert.ToString(modGlobalVariables.Statics.intSFLAOption) + ")";
						}
					}
				}
				else
				{
					txtSFLA.Text = "";
					txtPerSFLA.Text = "";
				}
				// txtOutTotal.Text = Format(lngOutbuildingTotal(MR.Fields("rscard")), "#,##0")
				txtOutTotal.Text = Strings.Format(modOutbuilding.Statics.lngOutbuildingTotal[modGlobalVariables.Statics.intCurrentCard], "#,##0");
			}
			else
			{
				// With frmNewValuationReport.OutBuildingGrid(intCurC)
				// txtSFLA.Text = .TextMatrix(.Rows - 2, 0)
				// txtPerSFLA.Text = .TextMatrix(.Rows - 2, 2)
				// txtOutTotal.Text = .TextMatrix(.Rows - 2, 9)
				txtSFLA.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].SFLA;
				txtPerSFLA.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].SFLAPerVal;
				txtOutTotal.Text = modSpeedCalc.Statics.CalcOutList[intCurC + 1].TotalOutbuilding;
				// txtBldgTotal.Text = .TextMatrix(.Rows - 1, 9)
			}
		}

		
	}
}
