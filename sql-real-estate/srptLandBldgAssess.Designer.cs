﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptLandBldgAssess.
	/// </summary>
	partial class srptLandBldgAssess
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptLandBldgAssess));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txttotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtexempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtbldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtdesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txttotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtexempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txttotal,
				this.txtexempt,
				this.txtbldg,
				this.txtLand,
				this.txtCount,
				this.txtdesc
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txttotal
			// 
			this.txttotal.CanGrow = false;
			this.txttotal.Height = 0.1875F;
			this.txttotal.Left = 6F;
			this.txttotal.MultiLine = false;
			this.txttotal.Name = "txttotal";
			this.txttotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txttotal.Text = "Field1";
			this.txttotal.Top = 0F;
			this.txttotal.Width = 1.4375F;
			// 
			// txtexempt
			// 
			this.txtexempt.CanGrow = false;
			this.txtexempt.Height = 0.1875F;
			this.txtexempt.Left = 4.5F;
			this.txtexempt.MultiLine = false;
			this.txtexempt.Name = "txtexempt";
			this.txtexempt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtexempt.Text = "Field2";
			this.txtexempt.Top = 0F;
			this.txtexempt.Width = 1.4375F;
			// 
			// txtbldg
			// 
			this.txtbldg.CanGrow = false;
			this.txtbldg.Height = 0.19F;
			this.txtbldg.Left = 3F;
			this.txtbldg.MultiLine = false;
			this.txtbldg.Name = "txtbldg";
			this.txtbldg.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtbldg.Text = "Field3";
			this.txtbldg.Top = 0F;
			this.txtbldg.Width = 1.4375F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 1.6875F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLand.Text = "Field4";
			this.txtLand.Top = 0F;
			this.txtLand.Width = 1.25F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 1.1875F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCount.Text = "Field5";
			this.txtCount.Top = 0F;
			this.txtCount.Width = 0.4375F;
			// 
			// txtdesc
			// 
			this.txtdesc.CanGrow = false;
			this.txtdesc.Height = 0.1875F;
			this.txtdesc.Left = 0F;
			this.txtdesc.MultiLine = false;
			this.txtdesc.Name = "txtdesc";
			this.txtdesc.Style = "font-family: \'Tahoma\'";
			this.txtdesc.Text = "Field1";
			this.txtdesc.Top = 0F;
			this.txtdesc.Width = 1.125F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// srptLandBldgAssess
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txttotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtexempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtexempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtbldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtdesc;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
