﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmaudit.
	/// </summary>
	partial class frmaudit : BaseForm
	{
		public fecherFoundation.FCComboBox cmbtBilOrCor;
		public fecherFoundation.FCLabel lbltBilOrCor;
		public fecherFoundation.FCComboBox cmbtorder;
		public fecherFoundation.FCLabel lbltorder;
		public fecherFoundation.FCComboBox cmbtatype;
		public fecherFoundation.FCLabel lbltatype;
		public fecherFoundation.FCComboBox cmbtinclude;
		public fecherFoundation.FCButton cmdcancel;
		public fecherFoundation.FCButton cmdContinue;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCCheckBox chkIncTot;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCComboBox Combo1;
		public fecherFoundation.FCLabel lblfield;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmaudit));
            this.cmbtBilOrCor = new fecherFoundation.FCComboBox();
            this.lbltBilOrCor = new fecherFoundation.FCLabel();
            this.cmbtorder = new fecherFoundation.FCComboBox();
            this.lbltorder = new fecherFoundation.FCLabel();
            this.cmbtatype = new fecherFoundation.FCComboBox();
            this.lbltatype = new fecherFoundation.FCLabel();
            this.cmbtinclude = new fecherFoundation.FCComboBox();
            this.cmdcancel = new fecherFoundation.FCButton();
            this.cmdContinue = new fecherFoundation.FCButton();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.chkIncTot = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.Combo1 = new fecherFoundation.FCComboBox();
            this.lblfield = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdcancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncTot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 443);
            this.BottomPanel.Size = new System.Drawing.Size(686, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdContinue);
            this.ClientArea.Controls.Add(this.cmbtBilOrCor);
            this.ClientArea.Controls.Add(this.lbltBilOrCor);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.cmbtorder);
            this.ClientArea.Controls.Add(this.lbltorder);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbtatype);
            this.ClientArea.Controls.Add(this.lbltatype);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Size = new System.Drawing.Size(686, 383);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdcancel);
            this.TopPanel.Size = new System.Drawing.Size(686, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdcancel, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(272, 30);
            this.HeaderText.Text = "Audit of Billing Amounts";
            // 
            // cmbtBilOrCor
            // 
            this.cmbtBilOrCor.Items.AddRange(new object[] {
            "Billing",
            "Current"});
            this.cmbtBilOrCor.Location = new System.Drawing.Point(150, 90);
            this.cmbtBilOrCor.Name = "cmbtBilOrCor";
            this.cmbtBilOrCor.Size = new System.Drawing.Size(183, 40);
            this.cmbtBilOrCor.TabIndex = 3;
            this.cmbtBilOrCor.Text = "Billing";
            // 
            // lbltBilOrCor
            // 
            this.lbltBilOrCor.Location = new System.Drawing.Point(20, 104);
            this.lbltBilOrCor.Name = "lbltBilOrCor";
            this.lbltBilOrCor.Size = new System.Drawing.Size(33, 18);
            this.lbltBilOrCor.TabIndex = 2;
            this.lbltBilOrCor.Text = "USE";
            // 
            // cmbtorder
            // 
            this.cmbtorder.Items.AddRange(new object[] {
            "Account",
            "Map/Lot",
            "Name",
            "Location"});
            this.cmbtorder.Location = new System.Drawing.Point(149, 150);
            this.cmbtorder.Name = "cmbtorder";
            this.cmbtorder.Size = new System.Drawing.Size(183, 40);
            this.cmbtorder.TabIndex = 5;
            this.cmbtorder.Text = "Account";
            // 
            // lbltorder
            // 
            this.lbltorder.Location = new System.Drawing.Point(20, 164);
            this.lbltorder.Name = "lbltorder";
            this.lbltorder.Size = new System.Drawing.Size(65, 18);
            this.lbltorder.TabIndex = 4;
            this.lbltorder.Text = "ORDER BY";
            // 
            // cmbtatype
            // 
            this.cmbtatype.Items.AddRange(new object[] {
            "Regular",
            "Exempt"});
            this.cmbtatype.Location = new System.Drawing.Point(150, 30);
            this.cmbtatype.Name = "cmbtatype";
            this.cmbtatype.Size = new System.Drawing.Size(183, 40);
            this.cmbtatype.TabIndex = 1;
            this.cmbtatype.Text = "Regular";
            this.cmbtatype.SelectedIndexChanged += new System.EventHandler(this.cmbtatype_SelectedIndexChanged);
            // 
            // lbltatype
            // 
            this.lbltatype.Location = new System.Drawing.Point(20, 44);
            this.lbltatype.Name = "lbltatype";
            this.lbltatype.Size = new System.Drawing.Size(95, 18);
            this.lbltatype.TabIndex = 14;
            this.lbltatype.Text = "TYPE OF AUDIT";
            // 
            // cmbtinclude
            // 
            this.cmbtinclude.Items.AddRange(new object[] {
            "Detail",
            "Subtotal",
            "Both",
            "Totals Only"});
            this.cmbtinclude.Location = new System.Drawing.Point(20, 30);
            this.cmbtinclude.Name = "cmbtinclude";
            this.cmbtinclude.Size = new System.Drawing.Size(233, 40);
            this.cmbtinclude.TabIndex = 3;
            this.cmbtinclude.Text = "Detail";
            // 
            // cmdcancel
            // 
            this.cmdcancel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdcancel.Location = new System.Drawing.Point(574, 29);
            this.cmdcancel.Name = "cmdcancel";
            this.cmdcancel.Size = new System.Drawing.Size(73, 24);
            this.cmdcancel.TabIndex = 14;
            this.cmdcancel.Text = "Cancel";
            this.cmdcancel.Visible = false;
            this.cmdcancel.Click += new System.EventHandler(this.cmdcancel_Click);
            // 
            // cmdContinue
            // 
            this.cmdContinue.AppearanceKey = "acceptButton";
            this.cmdContinue.Location = new System.Drawing.Point(20, 319);
            this.cmdContinue.Name = "cmdContinue";
            this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdContinue.Size = new System.Drawing.Size(112, 48);
            this.cmdContinue.TabIndex = 13;
            this.cmdContinue.Text = "Continue";
            this.cmdContinue.Click += new System.EventHandler(this.cmdok_Click);
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.chkIncTot);
            this.Frame5.Location = new System.Drawing.Point(20, 210);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(313, 77);
            this.Frame5.TabIndex = 7;
            this.Frame5.Text = "Include Total Exemptions";
            // 
            // chkIncTot
            // 
            this.chkIncTot.Checked = true;
            this.chkIncTot.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkIncTot.Location = new System.Drawing.Point(20, 30);
            this.chkIncTot.Name = "chkIncTot";
            this.chkIncTot.Size = new System.Drawing.Size(226, 23);
            this.chkIncTot.Text = "Include Totally Exempt Properties";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cmbRange);
            this.Frame2.Location = new System.Drawing.Point(352, 30);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(273, 90);
            this.Frame2.TabIndex = 8;
            this.Frame2.Text = "Records To Audit";
            // 
            // cmbRange
            // 
            this.cmbRange.BackColor = System.Drawing.SystemColors.Window;
            this.cmbRange.Location = new System.Drawing.Point(20, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(233, 40);
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.Combo1);
            this.Frame4.Controls.Add(this.cmbtinclude);
            this.Frame4.Controls.Add(this.lblfield);
            this.Frame4.Location = new System.Drawing.Point(352, 140);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(273, 150);
            this.Frame4.TabIndex = 6;
            this.Frame4.Text = "Display";
            // 
            // Combo1
            // 
            this.Combo1.BackColor = System.Drawing.SystemColors.Window;
            this.Combo1.Location = new System.Drawing.Point(173, 90);
            this.Combo1.Name = "Combo1";
            this.Combo1.Size = new System.Drawing.Size(80, 40);
            this.Combo1.TabIndex = 2;
            // 
            // lblfield
            // 
            this.lblfield.Location = new System.Drawing.Point(20, 104);
            this.lblfield.Name = "lblfield";
            this.lblfield.Size = new System.Drawing.Size(120, 18);
            this.lblfield.TabIndex = 1;
            this.lblfield.Text = "LENGTH OF FIELD";
            this.lblfield.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuContinue,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuContinue
            // 
            this.mnuContinue.Index = 0;
            this.mnuContinue.Name = "mnuContinue";
            this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuContinue.Text = "Continue";
            this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmaudit
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(686, 551);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmaudit";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Audit of Billing Amounts";
            this.Load += new System.EventHandler(this.frmaudit_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmaudit_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdcancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncTot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
