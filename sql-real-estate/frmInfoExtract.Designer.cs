﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using System.IO.Compression;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmInfoExtract.
	/// </summary>
	partial class frmInfoExtract : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtPath;
		public fecherFoundation.FCButton cmdBrowse;
		public FCGrid gridTownCode;
		//public AxAbaleZipLibrary.AxAbaleZip AbaleZip1;
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInfoExtract));
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtPath = new fecherFoundation.FCTextBox();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.gridTownCode = new fecherFoundation.FCGrid();
			this.cmdShow = new fecherFoundation.FCButton();
			this.cmdCreate = new fecherFoundation.FCButton();
			this.cmdEmail = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdShow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEmail)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdCreate);
			this.BottomPanel.Location = new System.Drawing.Point(0, 452);
			this.BottomPanel.Size = new System.Drawing.Size(711, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.gridTownCode);
			this.ClientArea.Size = new System.Drawing.Size(711, 392);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdEmail);
			this.TopPanel.Controls.Add(this.cmdShow);
			this.TopPanel.Size = new System.Drawing.Size(711, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdShow, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdEmail, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(219, 30);
			this.HeaderText.Text = "Information Extract";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtPath);
			this.Frame1.Controls.Add(this.cmdBrowse);
			this.Frame1.Location = new System.Drawing.Point(30, 90);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(450, 90);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Extract File Path";
			// 
			// txtPath
			// 
			this.txtPath.AutoSize = false;
			this.txtPath.BackColor = System.Drawing.SystemColors.Window;
			this.txtPath.Enabled = false;
			this.txtPath.LinkItem = null;
			this.txtPath.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPath.LinkTopic = null;
			this.txtPath.Location = new System.Drawing.Point(20, 30);
			this.txtPath.Name = "txtPath";
			this.txtPath.Size = new System.Drawing.Size(300, 40);
			this.txtPath.TabIndex = 2;
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "actionButton";
			this.cmdBrowse.Location = new System.Drawing.Point(340, 30);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(90, 40);
			this.cmdBrowse.TabIndex = 1;
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// gridTownCode
			// 
			this.gridTownCode.AllowSelection = false;
			this.gridTownCode.AllowUserToResizeColumns = false;
			this.gridTownCode.AllowUserToResizeRows = false;
			this.gridTownCode.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridTownCode.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridTownCode.BackColorBkg = System.Drawing.Color.Empty;
			this.gridTownCode.BackColorFixed = System.Drawing.Color.Empty;
			this.gridTownCode.BackColorSel = System.Drawing.Color.Empty;
			this.gridTownCode.Cols = 1;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridTownCode.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.gridTownCode.ColumnHeadersHeight = 30;
			this.gridTownCode.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridTownCode.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridTownCode.DefaultCellStyle = dataGridViewCellStyle4;
			this.gridTownCode.DragIcon = null;
			this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridTownCode.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridTownCode.ExtendLastCol = true;
			this.gridTownCode.FixedCols = 0;
			this.gridTownCode.FixedRows = 0;
			this.gridTownCode.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridTownCode.FrozenCols = 0;
			this.gridTownCode.GridColor = System.Drawing.Color.Empty;
			this.gridTownCode.GridColorFixed = System.Drawing.Color.Empty;
			this.gridTownCode.Location = new System.Drawing.Point(30, 30);
			this.gridTownCode.Name = "gridTownCode";
			this.gridTownCode.RowHeadersVisible = false;
			this.gridTownCode.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridTownCode.RowHeightMin = 0;
			this.gridTownCode.Rows = 1;
			this.gridTownCode.ScrollTipText = null;
			this.gridTownCode.ShowColumnVisibilityMenu = false;
			this.gridTownCode.Size = new System.Drawing.Size(150, 40);
			this.gridTownCode.StandardTab = true;
			this.gridTownCode.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridTownCode.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridTownCode.TabIndex = 3;
			this.gridTownCode.Visible = false;
			this.gridTownCode.ComboCloseUp += new System.EventHandler(this.gridTownCode_ComboCloseUp);
			// 
			// cmdShow
			// 
			this.cmdShow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdShow.AppearanceKey = "toolbarButton";
			this.cmdShow.Location = new System.Drawing.Point(553, 29);
			this.cmdShow.Name = "cmdShow";
			this.cmdShow.Size = new System.Drawing.Size(130, 24);
			this.cmdShow.TabIndex = 2;
			this.cmdShow.Text = "Show File Format";
			this.cmdShow.Click += new System.EventHandler(this.mnuFileFormat_Click);
			// 
			// cmdCreate
			// 
			this.cmdCreate.AppearanceKey = "acceptButton";
			this.cmdCreate.Location = new System.Drawing.Point(263, 30);
			this.cmdCreate.Name = "cmdCreate";
			this.cmdCreate.Size = new System.Drawing.Size(170, 48);
			this.cmdCreate.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdCreate.TabIndex = 2;
			this.cmdCreate.Text = "Create Extract File";
			this.cmdCreate.Click += new System.EventHandler(this.mnuCreateZip_Click);
			// 
			// cmdEmail
			// 
			this.cmdEmail.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdEmail.AppearanceKey = "toolbarButton";
			this.cmdEmail.Location = new System.Drawing.Point(497, 29);
			this.cmdEmail.Name = "cmdEmail";
			this.cmdEmail.Size = new System.Drawing.Size(50, 24);
			this.cmdEmail.TabIndex = 3;
			this.cmdEmail.Text = "Email";
			this.cmdEmail.Click += new System.EventHandler(this.mnuEmail_Click);
			// 
			// frmInfoExtract
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(711, 560);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmInfoExtract";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Information Extract";
			this.Load += new System.EventHandler(this.frmInfoExtract_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmInfoExtract_KeyDown);
			this.Resize += new System.EventHandler(this.frmInfoExtract_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdShow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEmail)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdShow;
		public FCButton cmdCreate;
		public FCButton cmdEmail;
	}
}
