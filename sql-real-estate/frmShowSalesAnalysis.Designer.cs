﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmShowSalesAnalysis.
	/// </summary>
	partial class frmShowSalesAnalysis : BaseForm
	{
		public fecherFoundation.FCTextBox txtDeviation;
		public fecherFoundation.FCTextBox txtValuation;
		public fecherFoundation.FCTextBox txtSalePrice;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCGrid Grid2;
		public fecherFoundation.FCButton cmdRecalculate;
		public fecherFoundation.FCTextBox Text6;
		public fecherFoundation.FCTextBox Text5;
		public fecherFoundation.FCTextBox Text4;
		public fecherFoundation.FCTextBox txtBldgFactor;
		public fecherFoundation.FCTextBox txtLandFactor;
		public fecherFoundation.FCTextBox txtSaleFactor;
		public fecherFoundation.FCGrid Grid1;
		public fecherFoundation.FCLabel lblSalesTot;
		public fecherFoundation.FCLabel lblAdjusted;
		public fecherFoundation.FCLabel lblTrended;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuOpt;
		public fecherFoundation.FCToolStripMenuItem mnuView;
		public fecherFoundation.FCToolStripMenuItem mnuDel;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;
        public fecherFoundation.FCButton cmdView;
        public fecherFoundation.FCButton cmdDel;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.txtDeviation = new fecherFoundation.FCTextBox();
			this.txtValuation = new fecherFoundation.FCTextBox();
			this.txtSalePrice = new fecherFoundation.FCTextBox();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.Grid2 = new fecherFoundation.FCGrid();
			this.cmdRecalculate = new fecherFoundation.FCButton();
			this.Text6 = new fecherFoundation.FCTextBox();
			this.Text5 = new fecherFoundation.FCTextBox();
			this.Text4 = new fecherFoundation.FCTextBox();
			this.txtBldgFactor = new fecherFoundation.FCTextBox();
			this.txtLandFactor = new fecherFoundation.FCTextBox();
			this.txtSaleFactor = new fecherFoundation.FCTextBox();
			this.Grid1 = new fecherFoundation.FCGrid();
			this.lblSalesTot = new fecherFoundation.FCLabel();
			this.lblAdjusted = new fecherFoundation.FCLabel();
			this.lblTrended = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuOpt = new fecherFoundation.FCToolStripMenuItem();
			this.mnuView = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDel = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
            this.cmdView = new fecherFoundation.FCButton();
            this.cmdDel = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRecalculate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDel)).BeginInit();
            this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 649);
			this.BottomPanel.Size = new System.Drawing.Size(1129, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtDeviation);
			this.ClientArea.Controls.Add(this.txtValuation);
			this.ClientArea.Controls.Add(this.txtSalePrice);
			this.ClientArea.Controls.Add(this.Grid2);
			this.ClientArea.Controls.Add(this.cmdRecalculate);
			this.ClientArea.Controls.Add(this.Text6);
			this.ClientArea.Controls.Add(this.Text5);
			this.ClientArea.Controls.Add(this.Text4);
			this.ClientArea.Controls.Add(this.txtBldgFactor);
			this.ClientArea.Controls.Add(this.txtLandFactor);
			this.ClientArea.Controls.Add(this.txtSaleFactor);
			this.ClientArea.Controls.Add(this.Grid1);
			this.ClientArea.Controls.Add(this.lblSalesTot);
			this.ClientArea.Controls.Add(this.lblAdjusted);
			this.ClientArea.Controls.Add(this.lblTrended);
			this.ClientArea.Controls.Add(this.Label10);
			this.ClientArea.Controls.Add(this.Label9);
			this.ClientArea.Controls.Add(this.Label8);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblTitle);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1129, 589);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdView);
            this.TopPanel.Controls.Add(this.cmdDel);
			this.TopPanel.Size = new System.Drawing.Size(1129, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDel, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdView, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(172, 30);
			this.HeaderText.Text = "Sales Analysis";
			// 
			// txtDeviation
			// 
			this.txtDeviation.AutoSize = false;
			this.txtDeviation.BackColor = System.Drawing.SystemColors.Window;
			this.txtDeviation.LinkItem = null;
			this.txtDeviation.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDeviation.LinkTopic = null;
			this.txtDeviation.Location = new System.Drawing.Point(594, 561);
			this.txtDeviation.LockedOriginal = true;
			this.txtDeviation.Name = "txtDeviation";
			this.txtDeviation.ReadOnly = true;
			this.txtDeviation.Size = new System.Drawing.Size(120, 40);
			this.txtDeviation.TabIndex = 27;
			this.txtDeviation.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtValuation
			// 
			this.txtValuation.AutoSize = false;
			this.txtValuation.BackColor = System.Drawing.SystemColors.Window;
			this.txtValuation.LinkItem = null;
			this.txtValuation.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtValuation.LinkTopic = null;
			this.txtValuation.Location = new System.Drawing.Point(594, 511);
			this.txtValuation.LockedOriginal = true;
			this.txtValuation.Name = "txtValuation";
			this.txtValuation.ReadOnly = true;
			this.txtValuation.Size = new System.Drawing.Size(120, 40);
			this.txtValuation.TabIndex = 26;
			this.txtValuation.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtSalePrice
			// 
			this.txtSalePrice.AutoSize = false;
			this.txtSalePrice.BackColor = System.Drawing.SystemColors.Window;
			this.txtSalePrice.LinkItem = null;
			this.txtSalePrice.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSalePrice.LinkTopic = null;
			this.txtSalePrice.Location = new System.Drawing.Point(594, 461);
			this.txtSalePrice.LockedOriginal = true;
			this.txtSalePrice.Name = "txtSalePrice";
			this.txtSalePrice.ReadOnly = true;
			this.txtSalePrice.Size = new System.Drawing.Size(120, 40);
			this.txtSalePrice.TabIndex = 25;
			this.txtSalePrice.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(527, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(80, 48);
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.TabIndex = 23;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// Grid2
			// 
			this.Grid2.AllowSelection = false;
			this.Grid2.AllowUserToResizeColumns = false;
			this.Grid2.AllowUserToResizeRows = false;
			this.Grid2.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid2.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid2.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid2.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid2.BackColorSel = System.Drawing.Color.Empty;
			this.Grid2.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid2.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid2.ColumnHeadersHeight = 30;
			this.Grid2.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.Grid2.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid2.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid2.DragIcon = null;
			this.Grid2.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid2.FixedRows = 0;
			this.Grid2.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid2.FrozenCols = 0;
			this.Grid2.GridColor = System.Drawing.Color.Empty;
			this.Grid2.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid2.Location = new System.Drawing.Point(30, 657);
			this.Grid2.Name = "Grid2";
			this.Grid2.ReadOnly = true;
			this.Grid2.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid2.RowHeightMin = 0;
			this.Grid2.Rows = 9;
			this.Grid2.ScrollTipText = null;
			this.Grid2.ShowColumnVisibilityMenu = false;
			this.Grid2.Size = new System.Drawing.Size(428, 363);
			this.Grid2.StandardTab = true;
			this.Grid2.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid2.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid2.TabIndex = 22;
			this.Grid2.Click += new System.EventHandler(this.Grid2_ClickEvent);
			// 
			// cmdRecalculate
			// 
			this.cmdRecalculate.AppearanceKey = "actionButton";
			this.cmdRecalculate.Location = new System.Drawing.Point(334, 461);
			this.cmdRecalculate.Name = "cmdRecalculate";
			this.cmdRecalculate.Size = new System.Drawing.Size(110, 40);
			this.cmdRecalculate.TabIndex = 16;
			this.cmdRecalculate.Text = "Recalculate";
			this.cmdRecalculate.Click += new System.EventHandler(this.cmdRecalculate_Click);
			// 
			// Text6
			// 
			this.Text6.Appearance = 0;
			this.Text6.AutoSize = false;
			this.Text6.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Text6.LinkItem = null;
			this.Text6.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text6.LinkTopic = null;
			this.Text6.Location = new System.Drawing.Point(249, 561);
			this.Text6.LockedOriginal = true;
			this.Text6.Name = "Text6";
			this.Text6.ReadOnly = true;
			this.Text6.Size = new System.Drawing.Size(40, 40);
			this.Text6.TabIndex = 12;
			this.Text6.Text = "%";
			// 
			// Text5
			// 
			this.Text5.Appearance = 0;
			this.Text5.AutoSize = false;
			this.Text5.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Text5.LinkItem = null;
			this.Text5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text5.LinkTopic = null;
			this.Text5.Location = new System.Drawing.Point(249, 511);
			this.Text5.LockedOriginal = true;
			this.Text5.Name = "Text5";
			this.Text5.ReadOnly = true;
			this.Text5.Size = new System.Drawing.Size(40, 40);
			this.Text5.TabIndex = 11;
			this.Text5.Text = "%";
			// 
			// Text4
			// 
			this.Text4.Appearance = 0;
			this.Text4.AutoSize = false;
			this.Text4.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Text4.LinkItem = null;
			this.Text4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text4.LinkTopic = null;
			this.Text4.Location = new System.Drawing.Point(249, 461);
			this.Text4.LockedOriginal = true;
			this.Text4.Name = "Text4";
			this.Text4.ReadOnly = true;
			this.Text4.Size = new System.Drawing.Size(65, 40);
			this.Text4.TabIndex = 10;
			this.Text4.Text = "%/YR";
			// 
			// txtBldgFactor
			// 
			this.txtBldgFactor.Appearance = 0;
			this.txtBldgFactor.AutoSize = false;
			this.txtBldgFactor.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.txtBldgFactor.LinkItem = null;
			this.txtBldgFactor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtBldgFactor.LinkTopic = null;
			this.txtBldgFactor.Location = new System.Drawing.Point(149, 561);
			this.txtBldgFactor.Name = "txtBldgFactor";
			this.txtBldgFactor.Size = new System.Drawing.Size(80, 40);
			this.txtBldgFactor.TabIndex = 9;
			this.txtBldgFactor.Text = "Text3";
			this.txtBldgFactor.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtBldgFactor.Validating += new System.ComponentModel.CancelEventHandler(this.txtBldgFactor_Validating);
			// 
			// txtLandFactor
			// 
			this.txtLandFactor.Appearance = 0;
			this.txtLandFactor.AutoSize = false;
			this.txtLandFactor.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.txtLandFactor.LinkItem = null;
			this.txtLandFactor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLandFactor.LinkTopic = null;
			this.txtLandFactor.Location = new System.Drawing.Point(149, 511);
			this.txtLandFactor.Name = "txtLandFactor";
			this.txtLandFactor.Size = new System.Drawing.Size(80, 40);
			this.txtLandFactor.TabIndex = 8;
			this.txtLandFactor.Text = "Text2";
			this.txtLandFactor.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtLandFactor.Validating += new System.ComponentModel.CancelEventHandler(this.txtLandFactor_Validating);
			// 
			// txtSaleFactor
			// 
			this.txtSaleFactor.Appearance = 0;
			this.txtSaleFactor.AutoSize = false;
			this.txtSaleFactor.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.txtSaleFactor.LinkItem = null;
			this.txtSaleFactor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSaleFactor.LinkTopic = null;
			this.txtSaleFactor.Location = new System.Drawing.Point(149, 461);
			this.txtSaleFactor.Name = "txtSaleFactor";
			this.txtSaleFactor.Size = new System.Drawing.Size(80, 40);
			this.txtSaleFactor.TabIndex = 7;
			this.txtSaleFactor.Text = "Text1";
			this.txtSaleFactor.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtSaleFactor.Validating += new System.ComponentModel.CancelEventHandler(this.txtSaleFactor_Validating);
			// 
			// Grid1
			// 
			this.Grid1.AllowSelection = false;
			this.Grid1.AllowUserToResizeColumns = false;
			this.Grid1.AllowUserToResizeRows = false;
			this.Grid1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid1.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid1.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid1.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid1.BackColorSel = System.Drawing.Color.Empty;
			this.Grid1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid1.Cols = 11;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.Grid1.ColumnHeadersHeight = 30;
			this.Grid1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid1.DefaultCellStyle = dataGridViewCellStyle4;
			this.Grid1.DragIcon = null;
			this.Grid1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.Grid1.FixedCols = 0;
			this.Grid1.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid1.FrozenCols = 0;
			this.Grid1.GridColor = System.Drawing.Color.Empty;
			this.Grid1.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid1.Location = new System.Drawing.Point(30, 78);
			this.Grid1.Name = "Grid1";
			this.Grid1.ReadOnly = true;
			this.Grid1.RowHeadersVisible = false;
			this.Grid1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid1.RowHeightMin = 0;
			this.Grid1.Rows = 1;
			this.Grid1.ScrollTipText = null;
			this.Grid1.ShowColumnVisibilityMenu = false;
			this.Grid1.Size = new System.Drawing.Size(1058, 305);
			this.Grid1.StandardTab = true;
			this.Grid1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid1.TabIndex = 0;
			this.Grid1.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.Grid1_MouseDownEvent);
			//FC:FINAL:MSH - i.issue #1344: in original app AfterSort will be handled only after loading form
			//this.Grid1.Sorted += new System.EventHandler(this.Grid1_AfterSort);
			this.Grid1.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid1_KeyDownEvent);
			// 
			// lblSalesTot
			// 
			this.lblSalesTot.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.lblSalesTot.Location = new System.Drawing.Point(30, 403);
			this.lblSalesTot.Name = "lblSalesTot";
			this.lblSalesTot.Size = new System.Drawing.Size(1058, 14);
			this.lblSalesTot.TabIndex = 30;
			this.lblSalesTot.Text = "TOTAL SALES";
			this.lblSalesTot.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblAdjusted
			// 
			this.lblAdjusted.Location = new System.Drawing.Point(737, 525);
			this.lblAdjusted.Name = "lblAdjusted";
			this.lblAdjusted.Size = new System.Drawing.Size(75, 16);
			this.lblAdjusted.TabIndex = 29;
			this.lblAdjusted.Text = "(ADJUSTED)";
			this.lblAdjusted.Visible = false;
			// 
			// lblTrended
			// 
			this.lblTrended.Location = new System.Drawing.Point(739, 475);
			this.lblTrended.Name = "lblTrended";
			this.lblTrended.Size = new System.Drawing.Size(75, 16);
			this.lblTrended.TabIndex = 28;
			this.lblTrended.Text = "(TRENDED)";
			this.lblTrended.Visible = false;
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(30, 621);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(155, 16);
			this.Label10.TabIndex = 21;
			this.Label10.Text = "SALES  RATIO  STATISTICS";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(476, 575);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(83, 16);
			this.Label9.TabIndex = 20;
			this.Label9.Text = "DEVIATION";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(476, 525);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(83, 16);
			this.Label8.TabIndex = 19;
			this.Label8.Text = "VALUATION";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(476, 475);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(83, 16);
			this.Label7.TabIndex = 18;
			this.Label7.Text = "SALE PRICE";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(476, 437);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(65, 18);
			this.Label6.TabIndex = 17;
			this.Label6.Text = "TOTALS";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(30, 575);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(85, 16);
			this.Label5.TabIndex = 6;
			this.Label5.Text = "BLDG FACTOR";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 525);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(100, 14);
			this.Label4.TabIndex = 5;
			this.Label4.Text = "LAND FACTOR";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 475);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(85, 14);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "SALE FACTOR";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 437);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(47, 16);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "TRENDS";
			// 
			// lblTitle
			// 
			this.lblTitle.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.lblTitle.Location = new System.Drawing.Point(30, 54);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(1058, 14);
			this.lblTitle.TabIndex = 2;
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label1
			// 
			this.Label1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(1058, 14);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "SALES ANALYSIS";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.Label1.Visible = false;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuOpt
			});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuOpt
			// 
			this.mnuOpt.Index = 0;
			this.mnuOpt.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuView,
				this.mnuDel
			});
			this.mnuOpt.Name = "mnuOpt";
			this.mnuOpt.Text = "Options";
			// 
			// mnuView
			// 
			this.mnuView.Index = 0;
			this.mnuView.Name = "mnuView";
			this.mnuView.Text = "View Sales Record";
			this.mnuView.Click += new System.EventHandler(this.mnuView_Click);
			// 
			// mnuDel
			// 
			this.mnuDel.Index = 1;
			this.mnuDel.Name = "mnuDel";
			this.mnuDel.Text = "Delete Entry";
			this.mnuDel.Click += new System.EventHandler(this.mnuDel_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = -1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuCancel
			// 
			this.mnuCancel.Index = -1;
			this.mnuCancel.Name = "mnuCancel";
			this.mnuCancel.Text = "Cancel";
            // 
            // cmdView
            // 
            this.cmdView.AppearanceKey = "toolbarButton";
            this.cmdView.Location = new System.Drawing.Point(876, 24);
            this.cmdView.Name = "cmdView";
            this.cmdView.Size = new System.Drawing.Size(128, 26);
            this.cmdView.TabIndex = 168;
			this.cmdView.Text = "View Sales Record";
            this.cmdView.Click += new System.EventHandler(this.mnuView_Click);
            // 
            // cmdDel
            // 
            this.cmdDel.AppearanceKey = "toolbarButton";
            this.cmdDel.Location = new System.Drawing.Point(1011, 24);
            this.cmdDel.Name = "cmdDel";
            this.cmdDel.Size = new System.Drawing.Size(88, 26);
            this.cmdDel.TabIndex = 168;
			this.cmdDel.Text = "Delete Entry";
            this.cmdDel.Click += new System.EventHandler(this.mnuDel_Click);
            // 
            // frmShowSalesAnalysis
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1129, 757);
			this.FillColor = 0;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmShowSalesAnalysis";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Sales Analysis";
			this.Load += new System.EventHandler(this.frmShowSalesAnalysis_Load);
			this.Resize += new System.EventHandler(this.frmShowSalesAnalysis_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRecalculate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDel)).EndInit();
            this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
