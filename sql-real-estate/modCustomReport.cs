﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class modCustomReport
	{
		public const int GRIDTEXT = 1;
		public const int GRIDDATE = 2;
		public const int GRIDCOMBOIDTEXT = 3;
		public const int GRIDNUMRANGE = 4;
		public const int GRIDCOMBOIDNUM = 5;
		public const int GRIDCOMBOTEXT = 6;
		public const int GRIDTEXTRANGE = 7;
		public const string CUSTOMREPORTDATABASE = "Twck0000.vb1";
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string FixQuotes(string strValue)
		{
			string FixQuotes = "";
			FixQuotes = strValue.Replace("'", "''");
			return FixQuotes;
		}

		public static void SetColumnCaptions(dynamic FormName)
		{
			// THIS ROUTINE GETS THE CAPTIONS THAT ARE TO BE DISPLAYED ON
			// THE REPORT ITSELF
			// THIS ARRAY (strFieldCaptions) HOLDS ONLY THE FIELD NAMES THAT ARE
			// TO BE DISPLAYED WHERE THE ARRAY (strCaptions) HOLDS ALL POSSIBLE
			// FIELD NAMES
			int intCounter;
			int intCount;
			intCount = 0;
			for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				if (FormName.lstFields.Selected(intCounter))
				{
					Statics.strFieldCaptions[intCount] = Statics.strCaptions[FormName.lstFields.ItemData(intCounter)];
					intCount += 1;
				}
			}
		}

		public static void GetNumberOfFields(string strSQL)
		{
			// THIS ROUTINE GETS THE NUMBER OF FIELDS TO REPORT ON.
			// ALSO...THIS FILLS THE ARRAY (strFieldNames) WITH ALL OF THE FIELDS
			// TO SHOW IN THE REPORT. THESE ARE NOT THE DISPLAY CAPTIONS BUT THE
			// ACTUAL DATABASE FIELD NAMES TO BE USED IN THE SQL STATEMENT
			// vbPorter upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			object strTemp2;
			int intCount;
			int intCounter;
			if (Strings.Trim(strSQL) == string.Empty)
			{
				Statics.intNumberOfSQLFields = -1;
				return;
			}
			strTemp = Strings.Split(strSQL, "from", -1, CompareConstants.vbBinaryCompare);
			strSQL = Strings.Mid(strTemp[0], 7, FCConvert.ToString(strTemp[0]).Length - 6);
			strTemp = Strings.Split(strSQL, ",", -1, CompareConstants.vbBinaryCompare);
			Statics.intNumberOfSQLFields = Information.UBound(strTemp, 1);
			for (intCounter = 0; intCounter <= Statics.intNumberOfSQLFields; intCounter++)
			{
				if (Strings.Trim(strTemp[intCounter]) != string.Empty)
				{
					// CHECK TO SEE IF AN ALIAS WAS USED IN THE SQL STATEMENT
					Statics.strFieldNames[intCounter] = FCConvert.ToString(CheckForAS(strTemp[intCounter]));
					// NEED TO SET THE CAPTIONS ARRAY SO THAT IF SHOWING A SAVED
					// REPORT THE CORRECT CAPTION WILL SHOW ON THE REPORT
					for (intCount = 0; intCount <= Statics.intNumberOfSQLFields; intCount++)
					{
						if (Statics.strFields[intCount] == Strings.Trim(strTemp[intCounter]))
						{
							Statics.strFieldCaptions[intCounter] = Statics.strCaptions[intCount];
							break;
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		public static object CheckForAS(string strFieldName, short intSegment = 2)
		{
			object CheckForAS = null;
			// THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
			// IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
			// DATABASE FIELD NAME
			// vbPorter upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
			{
				// SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				if (Strings.UCase(strTemp[1]) == "AS")
				{
					// RETURN THE SQL REFERENCE FIELD NAME
					if (intSegment == 1)
					{
						CheckForAS = strTemp[0];
					}
					else
					{
						CheckForAS = strTemp[2];
					}
				}
				else
				{
					// RETURN THE ACTUAL DATABASE FIELD NAME
					CheckForAS = strTemp[0];
				}
			}
			else
			{
				// AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				CheckForAS = Strings.Trim(strTemp[0]);
			}
			return CheckForAS;
		}

		public static void ClearComboListArray()
		{
			// CLEAR THE COMBO LIST ARRAY.
			// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
			int intCount;
			for (intCount = 0; intCount <= 50; intCount++)
			{
				Statics.strComboList[intCount, 0] = string.Empty;
				Statics.strComboList[intCount, 1] = string.Empty;
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmCustomLabels)
		public static void SetFormFieldCaptions(Form FormName, string ReportType)
		{
			// THIS IS THE MAIN ROUTINE THAT FILLS THE CUSTOM REPORT FORM.
			// 
			// ****************************************************************
			// THIS IS THE ONLY ROUTINE THAT NEEDS TO BE ALTERED WHEN YOU WANT
			// TO ADD A NEW REPORT TYPE.
			// ****************************************************************
			Statics.strReportType = Strings.UCase(ReportType);
			// CLEAR THE COMBO LIST ARRAY
			ClearComboListArray();
			if (Strings.UCase(ReportType) == "MORTGAGEHOLDER")
			{
				SetMortgageHolderParameters(FormName);
			}
			// POPULATE THE SORT LIST WITH THE SAME FIELDS AS THE FIELD LIST BOX
			// Call LoadSortList(FormName)
			// POPULATE THE WHERE GRID WITH THE FIELDS FROM THE FIELD LIST BOX
			// Call LoadWhereGrid(FormName)
			// LOAD THE SAVED REPORT COMBO ON THE FORM
			// CallByName FormName, "LoadCombo", VbMethod
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetMortgageHolderParameters(dynamic FormName)
		{
			Statics.strCustomTitle = "Mortgage Holder Labels";
			FormName.lstFields.AddItem("Mortgage Holder");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Real Estate");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Personal Property");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			LoadWhereGrid(FormName);
			LoadSortList(FormName);
		}

		public static void SetHeaderGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE HEADINGS
			switch (Strings.UCase(Statics.strReportType))
			{
			// Case "DOGS"
			// 
			// 
			// Case "BIRTHS"
				default:
					{
						// GridName.BackColor = vbBlue
						// GridName.ForeColor = vbWhite
						break;
					}
			}
			//end switch
			// GridName.Select 0, 0, 0, GridName.Cols - 1
			// GridName.CellFontBold = True
		}

		public static void SetDataGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE REPORTS DATA
			if (Strings.UCase(Statics.strReportType) == "DOGS")
			{
			}
			else if (Strings.UCase(Statics.strReportType) == "BIRTHS")
			{
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmCustomLabels)
		public static void LoadSortList(dynamic FormName)
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			int intCounter;
			FormName.lstSort.Clear();
			for (intCounter = 0; intCounter <= FormName.vsWhere.Rows - 1; intCounter++)
			{
				FormName.lstSort.AddItem(FormName.vsWhere.TextMatrix(intCounter, 0));
				FormName.lstSort.ItemData(FormName.lstSort.NewIndex, intCounter);
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmCustomLabels)
		public static void LoadWhereGrid(dynamic FormName)
		{
			// LOAD THE WHERE GRID WITH FIELDS FROM THE FIELD LIST BOX
			int intCounter;
			int GridWidth;
			FormName.vsWhere.Rows = 0;
			// SET THE NUMBER OF COLUMNS AND THE WIDTH OF THOSE COLUMNS
			GridWidth = FormName.vsWhere.WidthOriginal;
			FormName.vsWhere.Cols = 4;
			//FC:FINAL:MSH - i.issue #1104: rounding and converting double to int for correct call of method
			//FormName.vsWhere.ColWidth(0, 0.36 * GridWidth);
			//FormName.vsWhere.ColWidth(1, 0.31 * GridWidth);
			//FormName.vsWhere.ColWidth(2, 0.31 * GridWidth);
			FormName.vsWhere.ColWidth(0, FCConvert.ToInt32(Math.Round(0.36 * GridWidth)));
			FormName.vsWhere.ColWidth(1, FCConvert.ToInt32(Math.Round(0.31 * GridWidth)));
			FormName.vsWhere.ColWidth(2, FCConvert.ToInt32(Math.Round(0.31 * GridWidth)));
			FormName.vsWhere.ColWidth(3, 0);
			if (Statics.strReportType == "MORTGAGEHOLDER")
			{
				FormName.vsWhere.AddItem("Name" + "\t" + "\t" + "\t" + "name");
				FormName.vsWhere.AddItem("Holder Number" + "\t" + "\t" + "\t" + "mortgageholderid");
				// FormName.vsWhere.AddItem ("Location" & vbTab & vbTab & vbTab & "streetname")
				FormName.vsWhere.AddItem("Zip" + "\t" + "\t" + "\t" + "zip");
			}
			else if (Statics.strReportType == "REALESTATE")
			{
				FormName.vsWhere.AddItem("Name" + "\t" + "\t" + "\t" + "rsname");
				FormName.vsWhere.AddItem("Account" + "\t" + "\t" + "\t" + "rsaccount");
				FormName.vsWhere.AddItem("Location" + "\t" + "\t" + "\t" + "rslocstreet");
				FormName.vsWhere.AddItem("Zip" + "\t" + "\t" + "\t" + "rszip");
				FormName.vsWhere.AddItem("Map / Lot" + "\t" + "\t" + "\t" + "rsmaplot");
			}
			else if (Statics.strReportType == "PERSONALPROPERTY")
			{
				FormName.vsWhere.AddItem("Name" + "\t" + "\t" + "\t" + "Name");
				FormName.vsWhere.AddItem("Account" + "\t" + "\t" + "\t" + "account");
				FormName.vsWhere.AddItem("Location" + "\t" + "\t" + "\t" + "street");
				FormName.vsWhere.AddItem("Zip" + "\t" + "\t" + "\t" + "zip");
				FormName.vsWhere.AddItem("Business Code" + "\t" + "\t" + "\t" + "businesscode");
			}
		}

		private static void LoadGridCellAsCombo(ref Form FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "")
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				Statics.strComboList[intRowNumber, 0] = strSQL;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter;
				clsDRWrapper rsCombo = new clsDRWrapper();
				rsCombo.OpenRecordset(strSQL, "twre0000.vb1");
				while (!rsCombo.EndOfFile())
				{
					Statics.strComboList[intRowNumber, 0] += "|";
					Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
					Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
					rsCombo.MoveNext();
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool IsValidDate(string DateToCheck)
		{
			bool IsValidDate = false;
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As short --> As int	OnWrite(string)
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As short --> As int	OnWrite(string)
			int intDay;
			// hold day
			int intYear;
			// hold year
			int intCheckFormat;
			// make sure date is format 'mm/dd'
			strAnnTemp = DateToCheck;
			intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
			intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							MessageBox.Show("Invalid Month on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return IsValidDate;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							MessageBox.Show("Invalid day on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return IsValidDate;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							MessageBox.Show("Invalid day on Date", "ErTRIO Softwarevror", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return IsValidDate;
						}
						break;
					}
				default:
					{
						// not even a month
						MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return IsValidDate;
					}
			}
			//end switch
			IsValidDate = true;
			return IsValidDate;
		}

		public class StaticVariables
		{
			//=========================================================
			public int intNumberOfSQLFields;
			public int[] strFieldWidth = new int[50 + 1];
			public string strPreSetReport = "";
			public string strCustomSQL = "";
			public string strColumnCaptions = "";
			public string[] strFields = new string[50 + 1];
			public string[] strFieldNames = new string[50 + 1];
			public string[] strFieldCaptions = new string[50 + 1];
			public string[] strCaptions = new string[50 + 1];
			public string strCustomTitle = string.Empty;
			public string strReportType = "";
			public string[,] strComboList = new string[50 + 1, 50 + 1];
			public string[] strWhereType = new string[50 + 1];
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
