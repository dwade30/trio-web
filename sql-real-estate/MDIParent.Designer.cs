//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for MDIParent.
	/// </summary>
	partial class MDIParent : BaseForm
	{
		public fecherFoundation.FCPictureBox picArchive;
		public fecherFoundation.FCCommonDialog CommonDialog1;
		public fecherFoundation.FCTextBox txtCommSource;
		public fecherFoundation.FCTextBox txtCommDest;
		public fecherFoundation.FCPictureBox imgArchive;
		public Wisej.Web.StatusBar StatusBar1;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel1;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel2;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel3;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel4;
		public FCGrid GRID;
		public Wisej.Web.ImageList ImageList1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuMenucolor;
		public fecherFoundation.FCToolStripMenuItem mnuFileCentralParties;
		public fecherFoundation.FCToolStripMenuItem mnuFExit;
		public fecherFoundation.FCToolStripMenuItem mnuForms;
		public fecherFoundation.FCToolStripMenuItem mnuPrintForms;
		public fecherFoundation.FCToolStripMenuItem mnuOMaxForms;
		public fecherFoundation.FCToolStripMenuItem mnuHelp;
		public fecherFoundation.FCToolStripMenuItem mnuRedbookHelp;
		public fecherFoundation.FCToolStripMenuItem mnuBudgetaryHelp;
		public fecherFoundation.FCToolStripMenuItem mnuCashReceiptsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuClerkHelp;
		public fecherFoundation.FCToolStripMenuItem mnuCodeEnforcementHelp;
		public fecherFoundation.FCToolStripMenuItem mnuEnhanced911Help;
		public fecherFoundation.FCToolStripMenuItem mnuFixedAssetsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuGeneralEntryHelp;
		public fecherFoundation.FCToolStripMenuItem mnuMotorVehicleRegistrationHelp;
		public fecherFoundation.FCToolStripMenuItem mnuPayrollHelp;
		public fecherFoundation.FCToolStripMenuItem mnuPersonalPropertyHelp;
		public fecherFoundation.FCToolStripMenuItem mnuRealEstateHelp;
		public fecherFoundation.FCToolStripMenuItem mnuTaxBillingHelp;
		public fecherFoundation.FCToolStripMenuItem mnuTaxCollectionsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuUtilityBillingHelp;
		public fecherFoundation.FCToolStripMenuItem mnuVoterRegistrationHelp;
		public fecherFoundation.FCToolStripMenuItem mnuHelpSepar;
		public fecherFoundation.FCToolStripMenuItem mnuHAbout;
		public fecherFoundation.FCToolStripMenuItem mnuCustomFunctions;
		public fecherFoundation.FCToolStripMenuItem mnuImportBriteside;
		public fecherFoundation.FCToolStripMenuItem mnuImportODonnell;
		public fecherFoundation.FCToolStripMenuItem mnuExportODonnell;
		public fecherFoundation.FCToolStripMenuItem mnuNewSharonImport;
		public fecherFoundation.FCToolStripMenuItem mnuImportJFRyan;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParent));
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))), "Clear");
			Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images1"))), "Left");
			Wisej.Web.ImageListEntry imageListEntry3 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images2"))), "Right");
			Wisej.Web.ImageListEntry imageListEntry4 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images3"))));
			Wisej.Web.ImageListEntry imageListEntry5 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images4"))));
			Wisej.Web.ImageListEntry imageListEntry6 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images5"))));
			Wisej.Web.ImageListEntry imageListEntry7 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images6"))));
			Wisej.Web.ImageListEntry imageListEntry8 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images7"))));
			Wisej.Web.ImageListEntry imageListEntry9 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images8"))));
			this.picArchive = new fecherFoundation.FCPictureBox();
			this.txtCommSource = new fecherFoundation.FCTextBox();
			this.txtCommDest = new fecherFoundation.FCTextBox();
			this.imgArchive = new fecherFoundation.FCPictureBox();
			this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
			this.StatusBar1 = new Wisej.Web.StatusBar();
			this.StatusBar1_Panel1 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel2 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel3 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel4 = new Wisej.Web.StatusBarPanel();
			this.GRID = new fecherFoundation.FCGrid();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMenucolor = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileCentralParties = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOMaxForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRedbookHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBudgetaryHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCashReceiptsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClerkHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCodeEnforcementHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEnhanced911Help = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFixedAssetsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGeneralEntryHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMotorVehicleRegistrationHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPayrollHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPersonalPropertyHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRealEstateHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTaxBillingHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTaxCollectionsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuUtilityBillingHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuVoterRegistrationHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHelpSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHAbout = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustomFunctions = new fecherFoundation.FCToolStripMenuItem();
			this.mnuImportBriteside = new fecherFoundation.FCToolStripMenuItem();
			this.mnuImportODonnell = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExportODonnell = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNewSharonImport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuImportJFRyan = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picArchive)).BeginInit();
			this.picArchive.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgArchive)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GRID)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 328);
			this.BottomPanel.Size = new System.Drawing.Size(518, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.picArchive);
			this.ClientArea.Controls.Add(this.StatusBar1);
			this.ClientArea.Controls.Add(this.GRID);
			this.ClientArea.Size = new System.Drawing.Size(518, 268);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(518, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// picArchive
			// 
			this.picArchive.AllowDrop = true;
			this.picArchive.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left)));
			this.picArchive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.picArchive.Controls.Add(this.txtCommSource);
			this.picArchive.Controls.Add(this.txtCommDest);
			this.picArchive.Controls.Add(this.imgArchive);
			this.picArchive.DrawStyle = ((short)(0));
			this.picArchive.DrawWidth = ((short)(1));
			this.picArchive.FillColor = 16777215;
			this.picArchive.FillStyle = ((short)(1));
			this.picArchive.FontTransparent = true;
			this.picArchive.Image = ((System.Drawing.Image)(resources.GetObject("picArchive.Image")));
			this.picArchive.Location = new System.Drawing.Point(168, 24);
			this.picArchive.Name = "picArchive";
			this.picArchive.Picture = ((System.Drawing.Image)(resources.GetObject("picArchive.Picture")));
			this.picArchive.Size = new System.Drawing.Size(809, 148);
			this.picArchive.TabIndex = 2;
			// 
			// txtCommSource
			// 
			this.txtCommSource.AutoSize = false;
			this.txtCommSource.BackColor = System.Drawing.SystemColors.Window;
			this.txtCommSource.LinkItem = null;
			this.txtCommSource.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCommSource.LinkTopic = null;
			this.txtCommSource.Location = new System.Drawing.Point(180, 182);
			this.txtCommSource.Name = "txtCommSource";
			this.txtCommSource.Size = new System.Drawing.Size(166, 40);
			this.txtCommSource.TabIndex = 4;
			this.txtCommSource.Visible = false;
			// 
			// txtCommDest
			// 
			this.txtCommDest.AutoSize = false;
			this.txtCommDest.BackColor = System.Drawing.SystemColors.Window;
			this.txtCommDest.LinkItem = null;
			this.txtCommDest.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCommDest.LinkTopic = null;
			this.txtCommDest.Location = new System.Drawing.Point(186, 239);
			this.txtCommDest.Name = "txtCommDest";
			this.txtCommDest.Size = new System.Drawing.Size(166, 40);
			this.txtCommDest.TabIndex = 3;
			this.txtCommDest.Visible = false;
			// 
			// imgArchive
			// 
			this.imgArchive.AllowDrop = true;
			this.imgArchive.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgArchive.DrawStyle = ((short)(0));
			this.imgArchive.DrawWidth = ((short)(1));
			this.imgArchive.FillColor = 16777215;
			this.imgArchive.FillStyle = ((short)(1));
			this.imgArchive.FontTransparent = true;
			this.imgArchive.Image = ((System.Drawing.Image)(resources.GetObject("imgArchive.Image")));
			this.imgArchive.Location = new System.Drawing.Point(14, 40);
			this.imgArchive.Name = "imgArchive";
			this.imgArchive.Picture = ((System.Drawing.Image)(resources.GetObject("imgArchive.Picture")));
			this.imgArchive.Size = new System.Drawing.Size(291, 73);
			this.imgArchive.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgArchive.TabIndex = 5;
			this.imgArchive.Visible = false;
			// 
			// CommonDialog1
			// 
			this.CommonDialog1.Color = System.Drawing.Color.Black;
			this.CommonDialog1.DefaultExt = null;
			this.CommonDialog1.FilterIndex = ((short)(0));
			this.CommonDialog1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.CommonDialog1.FontName = "Microsoft Sans Serif";
			this.CommonDialog1.FontSize = 8.25F;
			this.CommonDialog1.ForeColor = System.Drawing.Color.Black;
			this.CommonDialog1.FromPage = 0;
			this.CommonDialog1.Location = new System.Drawing.Point(0, 0);
			this.CommonDialog1.Name = "CommonDialog1";
			this.CommonDialog1.PrinterSettings = null;
			this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
			this.CommonDialog1.TabIndex = 0;
			this.CommonDialog1.ToPage = 0;
			// 
			// StatusBar1
			// 
			this.StatusBar1.Location = new System.Drawing.Point(0, 232);
			this.StatusBar1.Name = "StatusBar1";
			this.StatusBar1.Panels.AddRange(new Wisej.Web.StatusBarPanel[] {
				this.StatusBar1_Panel1,
				this.StatusBar1_Panel2,
				this.StatusBar1_Panel3,
				this.StatusBar1_Panel4
			});
			this.StatusBar1.Size = new System.Drawing.Size(501, 19);
			this.StatusBar1.SizingGrip = false;
			this.StatusBar1.TabIndex = 0;
			// 
			// StatusBar1_Panel1
			// 
			this.StatusBar1_Panel1.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Contents;
			this.StatusBar1_Panel1.MinWidth = 142;
			this.StatusBar1_Panel1.Name = "StatusBar1_Panel1";
			this.StatusBar1_Panel1.Text = "TRIO Software Corporation";
			this.StatusBar1_Panel1.Width = 166;
			// 
			// StatusBar1_Panel2
			// 
			this.StatusBar1_Panel2.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel2.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Spring;
			this.StatusBar1_Panel2.BorderStyle = Wisej.Web.StatusBarPanelBorderStyle.None;
			this.StatusBar1_Panel2.Name = "StatusBar1_Panel2";
			this.StatusBar1_Panel2.Width = 191;
			// 
			// StatusBar1_Panel3
			// 
			this.StatusBar1_Panel3.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel3.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Contents;
			this.StatusBar1_Panel3.MinWidth = 67;
			this.StatusBar1_Panel3.Name = "StatusBar1_Panel3";
			this.StatusBar1_Panel3.Width = 70;
			// 
			// StatusBar1_Panel4
			// 
			this.StatusBar1_Panel4.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel4.MinWidth = 67;
			this.StatusBar1_Panel4.Name = "StatusBar1_Panel4";
			this.StatusBar1_Panel4.Width = 67;
			// 
			// GRID
			// 
			this.GRID.AllowSelection = false;
			this.GRID.AllowUserToResizeColumns = false;
			this.GRID.AllowUserToResizeRows = false;
			this.GRID.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GRID.BackColorAlternate = System.Drawing.Color.Empty;
			this.GRID.BackColorBkg = System.Drawing.Color.Empty;
			this.GRID.BackColorFixed = System.Drawing.Color.Empty;
			this.GRID.BackColorSel = System.Drawing.Color.Empty;
			this.GRID.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GRID.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GRID.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GRID.ColumnHeadersHeight = 30;
			this.GRID.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GRID.DefaultCellStyle = dataGridViewCellStyle2;
			this.GRID.DragIcon = null;
			this.GRID.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GRID.FixedCols = 0;
			this.GRID.ForeColorFixed = System.Drawing.Color.Empty;
			this.GRID.FrozenCols = 0;
			this.GRID.GridColor = System.Drawing.Color.Empty;
			this.GRID.GridColorFixed = System.Drawing.Color.Empty;
			this.GRID.Location = new System.Drawing.Point(0, 24);
			this.GRID.Name = "GRID";
			this.GRID.ReadOnly = true;
			this.GRID.RowHeadersVisible = false;
			this.GRID.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GRID.RowHeightMin = 0;
			this.GRID.Rows = 45;
			this.GRID.ScrollTipText = null;
			this.GRID.ShowColumnVisibilityMenu = false;
			this.GRID.ShowFocusCell = false;
			this.GRID.Size = new System.Drawing.Size(168, 417);
			this.GRID.StandardTab = true;
			this.GRID.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GRID.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GRID.TabIndex = 1;
			this.GRID.CellMouseMove += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GRID_MouseMoveEvent);
			this.GRID.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GRID_MouseDownEvent);
			this.GRID.KeyDown += new Wisej.Web.KeyEventHandler(this.GRID_KeyDownEvent);
			this.GRID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.GRID_KeyPressEvent);
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
				imageListEntry1,
				imageListEntry2,
				imageListEntry3,
				imageListEntry4,
				imageListEntry5,
				imageListEntry6,
				imageListEntry7,
				imageListEntry8,
				imageListEntry9
			});
			this.ImageList1.ImageSize = new System.Drawing.Size(17, 19);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFile,
				this.mnuForms,
				this.mnuHelp,
				this.mnuCustomFunctions
			});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuMenucolor,
				this.mnuFileCentralParties,
				this.mnuFExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuMenucolor
			// 
			this.mnuMenucolor.Index = 0;
			this.mnuMenucolor.Name = "mnuMenucolor";
			this.mnuMenucolor.Text = "Set Menu Color";
			this.mnuMenucolor.Visible = false;
			this.mnuMenucolor.Click += new System.EventHandler(this.mnuMenucolor_Click);
			// 
			// mnuFileCentralParties
			// 
			this.mnuFileCentralParties.Index = 1;
			this.mnuFileCentralParties.Name = "mnuFileCentralParties";
			this.mnuFileCentralParties.Text = "Central Parties";
			this.mnuFileCentralParties.Click += new System.EventHandler(this.mnuFileCentralParties_Click);
			// 
			// mnuFExit
			// 
			this.mnuFExit.Index = 2;
			this.mnuFExit.Name = "mnuFExit";
			this.mnuFExit.Text = "Exit";
			this.mnuFExit.Click += new System.EventHandler(this.mnuFExit_Click);
			// 
			// mnuForms
			// 
			this.mnuForms.Index = 1;
			this.mnuForms.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuPrintForms,
				this.mnuOMaxForms
			});
			this.mnuForms.Name = "mnuForms";
			this.mnuForms.Text = "Forms";
			// 
			// mnuPrintForms
			// 
			this.mnuPrintForms.Index = 0;
			this.mnuPrintForms.Name = "mnuPrintForms";
			this.mnuPrintForms.Text = "Print Form(s)";
			this.mnuPrintForms.Click += new System.EventHandler(this.mnuPrintForms_Click);
			// 
			// mnuOMaxForms
			// 
			this.mnuOMaxForms.Index = 1;
			this.mnuOMaxForms.Name = "mnuOMaxForms";
			this.mnuOMaxForms.Text = "Maximize Forms";
			this.mnuOMaxForms.Click += new System.EventHandler(this.mnuOMaxForms_Click);
			// 
			// mnuHelp
			// 
			this.mnuHelp.Index = 2;
			this.mnuHelp.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuRedbookHelp,
				this.mnuBudgetaryHelp,
				this.mnuCashReceiptsHelp,
				this.mnuClerkHelp,
				this.mnuCodeEnforcementHelp,
				this.mnuEnhanced911Help,
				this.mnuFixedAssetsHelp,
				this.mnuGeneralEntryHelp,
				this.mnuMotorVehicleRegistrationHelp,
				this.mnuPayrollHelp,
				this.mnuPersonalPropertyHelp,
				this.mnuRealEstateHelp,
				this.mnuTaxBillingHelp,
				this.mnuTaxCollectionsHelp,
				this.mnuUtilityBillingHelp,
				this.mnuVoterRegistrationHelp,
				this.mnuHelpSepar,
				this.mnuHAbout
			});
			this.mnuHelp.Name = "mnuHelp";
			this.mnuHelp.Text = "Help";
			// 
			// mnuRedbookHelp
			// 
			this.mnuRedbookHelp.Index = 0;
			this.mnuRedbookHelp.Name = "mnuRedbookHelp";
			this.mnuRedbookHelp.Text = "Blue Book";
			this.mnuRedbookHelp.Click += new System.EventHandler(this.mnuRedbookHelp_Click);
			// 
			// mnuBudgetaryHelp
			// 
			this.mnuBudgetaryHelp.Index = 1;
			this.mnuBudgetaryHelp.Name = "mnuBudgetaryHelp";
			this.mnuBudgetaryHelp.Text = "Budgetary";
			this.mnuBudgetaryHelp.Click += new System.EventHandler(this.mnuBudgetaryHelp_Click);
			// 
			// mnuCashReceiptsHelp
			// 
			this.mnuCashReceiptsHelp.Index = 2;
			this.mnuCashReceiptsHelp.Name = "mnuCashReceiptsHelp";
			this.mnuCashReceiptsHelp.Text = "Cash Receipting";
			this.mnuCashReceiptsHelp.Click += new System.EventHandler(this.mnuCashReceiptsHelp_Click);
			// 
			// mnuClerkHelp
			// 
			this.mnuClerkHelp.Index = 3;
			this.mnuClerkHelp.Name = "mnuClerkHelp";
			this.mnuClerkHelp.Text = "Clerk";
			this.mnuClerkHelp.Click += new System.EventHandler(this.mnuClerkHelp_Click);
			// 
			// mnuCodeEnforcementHelp
			// 
			this.mnuCodeEnforcementHelp.Index = 4;
			this.mnuCodeEnforcementHelp.Name = "mnuCodeEnforcementHelp";
			this.mnuCodeEnforcementHelp.Text = "Code Enforcement";
			this.mnuCodeEnforcementHelp.Click += new System.EventHandler(this.mnuCodeEnforcementHelp_Click);
			// 
			// mnuEnhanced911Help
			// 
			this.mnuEnhanced911Help.Index = 5;
			this.mnuEnhanced911Help.Name = "mnuEnhanced911Help";
			this.mnuEnhanced911Help.Text = "Enhanced 911";
			this.mnuEnhanced911Help.Click += new System.EventHandler(this.mnuEnhanced911Help_Click);
			// 
			// mnuFixedAssetsHelp
			// 
			this.mnuFixedAssetsHelp.Index = 6;
			this.mnuFixedAssetsHelp.Name = "mnuFixedAssetsHelp";
			this.mnuFixedAssetsHelp.Text = "Fixed Assets";
			this.mnuFixedAssetsHelp.Click += new System.EventHandler(this.mnuFixedAssetsHelp_Click);
			// 
			// mnuGeneralEntryHelp
			// 
			this.mnuGeneralEntryHelp.Index = 7;
			this.mnuGeneralEntryHelp.Name = "mnuGeneralEntryHelp";
			this.mnuGeneralEntryHelp.Text = "General Entry";
			this.mnuGeneralEntryHelp.Click += new System.EventHandler(this.mnuGeneralEntryHelp_Click);
			// 
			// mnuMotorVehicleRegistrationHelp
			// 
			this.mnuMotorVehicleRegistrationHelp.Index = 8;
			this.mnuMotorVehicleRegistrationHelp.Name = "mnuMotorVehicleRegistrationHelp";
			this.mnuMotorVehicleRegistrationHelp.Text = "Motor Vehicle Registration";
			this.mnuMotorVehicleRegistrationHelp.Click += new System.EventHandler(this.mnuMotorVehicleRegistrationHelp_Click);
			// 
			// mnuPayrollHelp
			// 
			this.mnuPayrollHelp.Index = 9;
			this.mnuPayrollHelp.Name = "mnuPayrollHelp";
			this.mnuPayrollHelp.Text = "Payroll";
			this.mnuPayrollHelp.Click += new System.EventHandler(this.mnuPayrollHelp_Click);
			// 
			// mnuPersonalPropertyHelp
			// 
			this.mnuPersonalPropertyHelp.Index = 10;
			this.mnuPersonalPropertyHelp.Name = "mnuPersonalPropertyHelp";
			this.mnuPersonalPropertyHelp.Text = "Personal Property";
			this.mnuPersonalPropertyHelp.Click += new System.EventHandler(this.mnuPersonalPropertyHelp_Click);
			// 
			// mnuRealEstateHelp
			// 
			this.mnuRealEstateHelp.Index = 11;
			this.mnuRealEstateHelp.Name = "mnuRealEstateHelp";
			this.mnuRealEstateHelp.Text = "Real Estate";
			this.mnuRealEstateHelp.Click += new System.EventHandler(this.mnuRealEstateHelp_Click);
			// 
			// mnuTaxBillingHelp
			// 
			this.mnuTaxBillingHelp.Index = 12;
			this.mnuTaxBillingHelp.Name = "mnuTaxBillingHelp";
			this.mnuTaxBillingHelp.Text = "Tax Billing";
			this.mnuTaxBillingHelp.Click += new System.EventHandler(this.mnuTaxBillingHelp_Click);
			// 
			// mnuTaxCollectionsHelp
			// 
			this.mnuTaxCollectionsHelp.Index = 13;
			this.mnuTaxCollectionsHelp.Name = "mnuTaxCollectionsHelp";
			this.mnuTaxCollectionsHelp.Text = "Tax Collections";
			this.mnuTaxCollectionsHelp.Click += new System.EventHandler(this.mnuTaxCollectionsHelp_Click);
			// 
			// mnuUtilityBillingHelp
			// 
			this.mnuUtilityBillingHelp.Index = 14;
			this.mnuUtilityBillingHelp.Name = "mnuUtilityBillingHelp";
			this.mnuUtilityBillingHelp.Text = "Utility Billing";
			this.mnuUtilityBillingHelp.Click += new System.EventHandler(this.mnuUtilityBillingHelp_Click);
			// 
			// mnuVoterRegistrationHelp
			// 
			this.mnuVoterRegistrationHelp.Index = 15;
			this.mnuVoterRegistrationHelp.Name = "mnuVoterRegistrationHelp";
			this.mnuVoterRegistrationHelp.Text = "Voter Registration";
			this.mnuVoterRegistrationHelp.Click += new System.EventHandler(this.mnuVoterRegistrationHelp_Click);
			// 
			// mnuHelpSepar
			// 
			this.mnuHelpSepar.Index = 16;
			this.mnuHelpSepar.Name = "mnuHelpSepar";
			this.mnuHelpSepar.Text = "-";
			// 
			// mnuHAbout
			// 
			this.mnuHAbout.Index = 17;
			this.mnuHAbout.Name = "mnuHAbout";
			this.mnuHAbout.Text = "About";
			this.mnuHAbout.Click += new System.EventHandler(this.mnuHAbout_Click);
			// 
			// mnuCustomFunctions
			// 
			this.mnuCustomFunctions.Index = 3;
			this.mnuCustomFunctions.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuImportBriteside,
				this.mnuImportODonnell,
				this.mnuExportODonnell,
				this.mnuNewSharonImport,
				this.mnuImportJFRyan
			});
			this.mnuCustomFunctions.Name = "mnuCustomFunctions";
			this.mnuCustomFunctions.Text = "Custom Functions";
			this.mnuCustomFunctions.Visible = false;
			// 
			// mnuImportBriteside
			// 
			this.mnuImportBriteside.Index = 0;
			this.mnuImportBriteside.Name = "mnuImportBriteside";
			this.mnuImportBriteside.Text = "Import Briteside File";
			this.mnuImportBriteside.Visible = false;
			this.mnuImportBriteside.Click += new System.EventHandler(this.mnuImportBriteside_Click);
			// 
			// mnuImportODonnell
			// 
			this.mnuImportODonnell.Index = 1;
			this.mnuImportODonnell.Name = "mnuImportODonnell";
			this.mnuImportODonnell.Text = "Import ODonnell File";
			this.mnuImportODonnell.Click += new System.EventHandler(this.mnuImportODonnell_Click);
			// 
			// mnuExportODonnell
			// 
			this.mnuExportODonnell.Index = 2;
			this.mnuExportODonnell.Name = "mnuExportODonnell";
			this.mnuExportODonnell.Text = "Export ODonnell File";
			this.mnuExportODonnell.Click += new System.EventHandler(this.mnuExportODonnell_Click);
			// 
			// mnuNewSharonImport
			// 
			this.mnuNewSharonImport.Index = 3;
			this.mnuNewSharonImport.Name = "mnuNewSharonImport";
			this.mnuNewSharonImport.Text = "Import From Excel";
			this.mnuNewSharonImport.Visible = false;
			this.mnuNewSharonImport.Click += new System.EventHandler(this.mnuNewSharonImport_Click);
			// 
			// mnuImportJFRyan
			// 
			this.mnuImportJFRyan.Index = 4;
			this.mnuImportJFRyan.Name = "mnuImportJFRyan";
			this.mnuImportJFRyan.Text = "Import from JF Ryan";
			this.mnuImportJFRyan.Visible = false;
			this.mnuImportJFRyan.Click += new System.EventHandler(this.mnuImportJFRyan_Click);
			// 
			// MDIParent
			// 
			this.BackColor = System.Drawing.SystemColors.Menu;
			this.ClientSize = new System.Drawing.Size(518, 436);
			this.FillColor = -2147483644;
			this.IsMdiContainer = true;
			this.Menu = this.MainMenu1;
			this.Name = "MDIParent";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Real Estate";
			this.WindowState = Wisej.Web.FormWindowState.Maximized;
			this.Activated += new System.EventHandler(this.MDIParent_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.MDIForm_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.MDIForm_QueryUnload);
			this.LinkExecute += MDIParent_LinkExecute;
			this.Resize += new System.EventHandler(this.MDIParent_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.picArchive)).EndInit();
			this.picArchive.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgArchive)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GRID)).EndInit();
			this.ResumeLayout(false);
		}

        #endregion

        private System.ComponentModel.IContainer components;
	}
}