﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptZoneNeighborhoodDifference.
	/// </summary>
	partial class rptZoneNeighborhoodDifference
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptZoneNeighborhoodDifference));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtZone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNeighborhood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeighborhood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtaccount,
				this.txtName,
				this.txtCard,
				this.txtZone,
				this.txtNeighborhood
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.Label13,
				this.txtDate,
				this.Label15,
				this.txtPage,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.txtTime
			});
			this.PageHeader.Height = 0.7083333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.CanShrink = true;
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0F;
			this.txtMuni.Width = 2.25F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.Label13.Text = "REAL ESTATE";
			this.Label13.Top = 0F;
			this.Label13.Width = 2.875F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.125F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.1875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.0625F;
			this.Label15.MultiLine = false;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label15.Text = "ZONE/NEIGHBORHOOD DIFFERENCE BETWEEN CARDS";
			this.Label15.Top = 0.1875F;
			this.Label15.Width = 3.75F;
			// 
			// txtPage
			// 
			this.txtPage.CanGrow = false;
			this.txtPage.Height = 0.25F;
			this.txtPage.Left = 6.333333F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.1666667F;
			this.txtPage.Width = 1F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.3125F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label17.Text = "ACCT";
			this.Label17.Top = 0.5F;
			this.Label17.Width = 0.4375F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.8125F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label18.Text = "NAME";
			this.Label18.Top = 0.5F;
			this.Label18.Width = 0.875F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 5.0625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label19.Text = "CARD";
			this.Label19.Top = 0.53125F;
			this.Label19.Width = 0.5F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 5.625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label20.Text = "ZONE";
			this.Label20.Top = 0.53125F;
			this.Label20.Width = 0.4375F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 6.1875F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label21.Text = "NEIGHBORHOOD";
			this.Label21.Top = 0.53125F;
			this.Label21.Width = 1.125F;
			// 
			// txtTime
			// 
			this.txtTime.CanGrow = false;
			this.txtTime.CanShrink = true;
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.1875F;
			// 
			// txtaccount
			// 
			this.txtaccount.CanGrow = false;
			this.txtaccount.Height = 0.19F;
			this.txtaccount.Left = 0.0625F;
			this.txtaccount.MultiLine = false;
			this.txtaccount.Name = "txtaccount";
			this.txtaccount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtaccount.Text = null;
			this.txtaccount.Top = 0.03125F;
			this.txtaccount.Width = 0.625F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.19F;
			this.txtName.Left = 0.8125F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtName.Text = "No Records have differences between cards";
			this.txtName.Top = 0.03125F;
			this.txtName.Width = 4.0625F;
			// 
			// txtCard
			// 
			this.txtCard.CanGrow = false;
			this.txtCard.Height = 0.19F;
			this.txtCard.Left = 5.0625F;
			this.txtCard.MultiLine = false;
			this.txtCard.Name = "txtCard";
			this.txtCard.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtCard.Text = null;
			this.txtCard.Top = 0.03125F;
			this.txtCard.Width = 0.375F;
			// 
			// txtZone
			// 
			this.txtZone.CanGrow = false;
			this.txtZone.Height = 0.19F;
			this.txtZone.Left = 5.625F;
			this.txtZone.MultiLine = false;
			this.txtZone.Name = "txtZone";
			this.txtZone.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtZone.Text = null;
			this.txtZone.Top = 0.03125F;
			this.txtZone.Width = 0.375F;
			// 
			// txtNeighborhood
			// 
			this.txtNeighborhood.CanGrow = false;
			this.txtNeighborhood.Height = 0.19F;
			this.txtNeighborhood.Left = 6.5F;
			this.txtNeighborhood.MultiLine = false;
			this.txtNeighborhood.Name = "txtNeighborhood";
			this.txtNeighborhood.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtNeighborhood.Text = null;
			this.txtNeighborhood.Top = 0.03125F;
			this.txtNeighborhood.Width = 0.4375F;
			// 
			// rptZoneNeighborhoodDifference
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeighborhood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeighborhood;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
