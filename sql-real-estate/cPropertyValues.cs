//Fecher vbPorter - Version 1.0.0.90

namespace TWRE0000
{
	public class cPropertyValues
	{

		//=========================================================


		private int lngAccount;
		private double dblLand;
		private double dblBuilding;
		private double dblExemption;
		private string strMapLot;
		private string strName;

		public int Account
		{
			set
			{
				lngAccount = value;
			}

			get
			{
					int Account = 0;
				Account = lngAccount;
				return Account;
			}
		}



		public double Land
		{
			set
			{
				dblLand = value;
			}

			get
			{
					double Land = 0;
				Land = dblLand;
				return Land;
			}
		}



		public double Building
		{
			set
			{
				dblBuilding = value;
			}

			get
			{
					double Building = 0;
				Building = dblBuilding;
				return Building;
			}
		}



		public double Exemption
		{
			set
			{
				dblExemption = value;
			}

			get
			{
					double Exemption = 0;
				Exemption = dblExemption;
				return Exemption;
			}
		}



		public string MapLot
		{
			set
			{
				strMapLot = value;
			}

			get
			{
					string MapLot = "";
				MapLot = strMapLot;
				return MapLot;
			}
		}



		public string Name
		{
			set
			{
				strName = value;
			}

			get
			{
					string Name = "";
				Name = strName;
				return Name;
			}
		}



	}
}
