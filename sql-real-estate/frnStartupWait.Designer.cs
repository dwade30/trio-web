﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmStartupWait.
	/// </summary>
	partial class frmStartupWait : BaseForm
	{
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStartupWait));
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 335);
			this.BottomPanel.Size = new System.Drawing.Size(459, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Image1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(459, 275);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(459, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// Image1
			// 
			this.Image1.AllowDrop = true;
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.DrawStyle = ((short)(0));
			this.Image1.DrawWidth = ((short)(1));
			this.Image1.FillStyle = ((short)(1));
			this.Image1.FontTransparent = true;
			this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
			this.Image1.Location = new System.Drawing.Point(197, 135);
			this.Image1.Name = "Image1";
			this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
			this.Image1.Size = new System.Drawing.Size(60, 66);
			this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.Image1.TabIndex = 0;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 60);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(412, 68);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "CHANGES ARE BEING MADE TO YOUR DATABASE. MAKE SURE NO ONE ELSE RUNS THIS PROGRAM " + "UNTIL THIS MESSAGE DISAPPEARS";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(290, 26);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "PLEASE WAIT";
			// 
			// frmStartupWait
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(459, 443);
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
			this.Name = "frmStartupWait";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.Activated += new System.EventHandler(this.frmStartupWait_Activated);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
