﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	public partial class frmCustomReport : BaseForm
	{
		public frmCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomReport InstancePtr
		{
			get
			{
				return (frmCustomReport)Sys.GetInstance(typeof(frmCustomReport));
			}
		}

		protected frmCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int TperP;
		bool boolScroll;
		bool boolSaleStuff;
		// if true then use the sales tables
		bool boolRnge;
		// range of accounts?
		bool boolEXTRCT;
		// from the extract?
		double dblPicResizeRatio;
		int intStart;
		private clsReportCodes clCodeList = new clsReportCodes();
		public int lngCurrentReportNumber;
		public bool boolDelete;
		const int CNSTORDERACCOUNT = 0;
		const int CNSTORDERNAME = 1;
		const int CNSTORDERSECOWNER = 2;
		const int CNSTORDERMAPLOT = 3;
		const int CNSTORDERLOCATION = 4;
		const int CNSTORDERZIP = 5;
		const int CNSTORDERREF1 = 6;
		const int CNSTORDERREF2 = 7;
		const int CNSTORDERLANDCODE = 8;
		const int CNSTORDERTRANCODE = 9;
		const int CNSTORDERBLDGCODE = 10;
		const int CNSTORDERNEIGHBORHOOD = 11;
		const int CNSTORDERZONE = 12;
		const int CNSTORDEROPEN1 = 13;
		const int CNSTORDEROPEN2 = 14;

		private void ResizeGrid()
		{
			int x;
			for (x = 0; x <= Grid1.Cols - 1; x++)
			{
				Grid1.ColWidth(x, FCConvert.ToInt32(FCConvert.ToDouble(Grid1.ColData(x))));
			}
		}

		private void frmCustomReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCustomReport_Load(object sender, System.EventArgs e)
		{
			int ImWidth;
			dblPicResizeRatio = 250;
			Image1.SizeMode = PictureBoxSizeMode.Normal;
			Image1.Image = ImageList1.Images[0];

			lngCurrentReportNumber = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			SetupGrid();
			NewSetupGrid2();
			SetupOrderList();
		}

		private void SetupGrid()
		{
			//Grid1.Width = Image1.Width;
			Grid1.Rows = 2;
			Grid1.Cols = 1;
			Grid1.ColWidth(0, FCConvert.ToInt32(1440));
			Grid1.ColData(0, 1440);
			Grid1.Row = 1;
			Grid1.Col = 0;
		}

		private void NewSetupGrid2()
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strCategories;
				clsDRWrapper clsCost = new clsDRWrapper();
				string strTemp = string.Empty;
				string strItem = string.Empty;
				strCategories = string.Empty;
				grid2.Cols = 4;
				grid2.ColWidth(0, 1440);
				// grid2.ColWidth(1) = 4320
				grid2.Rows = 1;
				grid2.TextMatrix(0, 0, "Data");
				grid2.TextMatrix(0, 1, "Description");
				grid2.ColHidden(2, true);
				grid2.ColHidden(3, true);
				grid2.AddItem(0 + "\t" + "Clear Cell");
				clCodeList.LoadCodes();
				clCodeList.MoveFirst();
				clCodeList.MovePrevious();
				clsReportParameter tCode;
				bool boolSkip = false;
				while (clCodeList.MoveNext() > 0)
				{
					tCode = clCodeList.GetCurrentCode();
					boolSkip = false;
					strItem = FCConvert.ToString(tCode.Code) + "\t";
					int vbPorterVar = tCode.Code;
					if (vbPorterVar == 43)
					{
						boolSkip = true;
					}
					else if (vbPorterVar >= 98 && vbPorterVar <= 146)
					{
						boolSkip = true;
					}
					else if (vbPorterVar > 400)
					{
						boolSkip = true;
					}
					else if (vbPorterVar >= 111 && vbPorterVar <= 146)
					{
						boolSkip = true;
					}
					else if (vbPorterVar == 500)
					{
						boolSkip = true;
					}
					else
					{
						strTemp = tCode.Description;
						strTemp = strTemp.Replace(",", " ");
						strItem += strTemp;
					}
					if (!boolSkip)
					{
						strItem += "\t" + tCode.FieldName + "\t" + tCode.TableName;
						grid2.AddItem(strItem);
						if (tCode.Code < 200)
						{
							grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, (grid2.Rows - 1), 0, grid2.Rows - 1, grid2.Cols - 1, Information.RGB(155, 193, 223));
						}
						else if (tCode.Code < 300)
						{
							grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, (grid2.Rows - 1), 0, grid2.Rows - 1, grid2.Cols - 1, Information.RGB(247, 161, 153));
						}
						else if (tCode.Code < 400)
						{
							grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, (grid2.Rows - 1), 0, grid2.Rows - 1, grid2.Cols - 1, Information.RGB(191, 223, 153));
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In function SetupGrid2", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGrid2()
		{
			// Dim strTemp As String
			// Dim strtemp2 As String
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper clsCost = new clsDRWrapper();
			// Dim strMast As String
			// Dim strDwell As String
			// Dim strComm As String
			// Dim strOut As String
			int intTemp;
			string strItem = string.Empty;
			string strTemp = string.Empty;
			try
			{
				// On Error GoTo ErrorHandler
				// strMast = "master"
				// strDwell = "dwelling"
				// strComm = "commercial"
				// strOut = "outbuilding"
				// 
				// If boolSaleStuff Then
				// strMast = "sr" & strMast
				// strDwell = "sr" & strDwell
				// strComm = "sr" & strComm
				// strOut = "sr" & strOut
				// End If
				// 
				grid2.Cols = 4;
				grid2.ColWidth(0, 1440);
				// grid2.ColWidth(1) = 4320
				grid2.Rows = 1;
				grid2.TextMatrix(0, 0, "Data");
				grid2.TextMatrix(0, 1, "Description");
				grid2.ColHidden(2, true);
				grid2.ColHidden(3, true);
				// 
				// 
				// grid2.AddItem ("Bldg Billing" & vbTab & "Last years bldg value (billing)" & vbTab & "lastbldgval" & vbTab & strMast)
				// grid2.AddItem ("Land Billing" & vbTab & "Last years land value (billing)" & vbTab & "lastlandval" & vbTab & strMast)
				// grid2.AddItem ("Neighborhood Code" & vbTab & "Neighborhood Code" & vbTab & "pineighborhood" & vbTab & strMast)
				// 
				// grid2.AddItem ("Tree Growth" & vbTab & "Tree Growth (Year)" & vbTab & "pistreetcode" & vbTab & strMast)
				// 
				// Call clsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1091", strredatabase)
				// strTemp = clsTemp.GetData("cldesc")
				// strtemp2 = clsTemp.GetData("csdesc")
				// grid2.AddItem (strtemp2 & vbTab & "X coord (user defined as " & strTemp & ")" & vbTab & "pixcoord" & vbTab & strMast)
				// 
				// Call clsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1092", strredatabase)
				// strTemp = clsTemp.GetData("cldesc")
				// strtemp2 = clsTemp.GetData("csdesc")
				// grid2.AddItem (strtemp2 & vbTab & "Y coord (user defined as " & strTemp & ")" & vbTab & "piycoord" & vbTab & strMast)
				// 
				// grid2.AddItem ("Zone" & vbTab & "Zone" & vbTab & "pizone" & vbTab & strMast)
				// grid2.AddItem ("Secondary Zone" & vbTab & "Secondary Zone" & vbTab & "piseczone" & vbTab & strMast)
				// grid2.AddItem ("Topography one" & vbTab & "First Topography Code" & vbTab & "pitopography1" & vbTab & strMast)
				// grid2.AddItem ("Topography two" & vbTab & "Second Topography Code" & vbTab & "pitopography2" & vbTab & strMast)
				// grid2.AddItem ("Utility 1" & vbTab & "Utilities Code one" & vbTab & "piutilities1" & vbTab & strMast)
				// grid2.AddItem ("Utility 2" & vbTab & "Utilities Code two" & vbTab & "piutilities2" & vbTab & strMast)
				// grid2.AddItem ("Street Code" & vbTab & "Street Code" & vbTab & "pistreet" & vbTab & strMast)
				// 
				// Call clsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1330", strredatabase)
				// strTemp = clsTemp.GetData("cldesc")
				// strtemp2 = clsTemp.GetData("csdesc")
				// grid2.AddItem (strtemp2 & vbTab & "Open 1 (user defined as " & strTemp & ")" & vbTab & "piopen1" & vbTab & strMast)
				// 
				// Call clsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1340", strredatabase)
				// strTemp = clsTemp.GetData("cldesc")
				// strtemp2 = clsTemp.GetData("csdesc")
				// grid2.AddItem (strtemp2 & vbTab & "Open 2 (user defined as " & strTemp & ")" & vbTab & "piopen2" & vbTab & strMast)
				// 
				// grid2.AddItem ("Sale Price" & vbTab & "Sale Price" & vbTab & "pisaleprice" & vbTab & strMast)
				// grid2.AddItem ("Sale Code" & vbTab & "Sale Type Code" & vbTab & "pisaletype" & vbTab & strMast)
				// grid2.AddItem ("Finance Code" & vbTab & "Sale Financing Code" & vbTab & "pisalefinancing" & vbTab & strMast)
				// grid2.AddItem ("Verify Code" & vbTab & "Sale Verified Code" & vbTab & "pisaleverified" & vbTab & strMast)
				// grid2.AddItem ("Validity" & vbTab & "Sale Validity Code" & vbTab & "pisalevalidity" & vbTab & strMast)
				// grid2.AddItem ("Sale Date" & vbTab & "Sale Date" & vbTab & "saledate" & vbTab & strMast)
				// grid2.AddItem ("Acres" & vbTab & "Total Number of Acres" & vbTab & "piacres" & vbTab & strMast)
				// grid2.AddItem ("Map/Lot" & vbTab & "Map and Lot number" & vbTab & "rsmaplot" & vbTab & strMast)
				// grid2.AddItem ("Name" & vbTab & "Owner's Name" & vbTab & "rsname" & vbTab & strMast)
				// grid2.AddItem ("Sec. Name" & vbTab & "Second Owner's Name" & vbTab & "rssecowner" & vbTab & strMast)
				// grid2.AddItem ("Address" & vbTab & "Owner Address" & vbTab & "owneraddress" & vbTab & strMast)
				// grid2.AddItem ("Telephone" & vbTab & "Telephone number" & vbTab & "rstelephone" & vbTab & strMast)
				// grid2.AddItem ("Loc Num" & vbTab & "Location number" & vbTab & "rslocnumalph" & vbTab & strMast)
				// grid2.AddItem ("Apt. No." & vbTab & "Apartment no./letter" & vbTab & "rslocapt" & vbTab & strMast)
				// grid2.AddItem ("Location" & vbTab & "Location / Street" & vbTab & "rslocstreet" & vbTab & strMast)
				// grid2.AddItem ("Ref 1" & vbTab & "Reference Field one" & vbTab & "rsref1" & vbTab & strMast)
				// grid2.AddItem ("Ref 2" & vbTab & "Reference Field two" & vbTab & "rsref2" & vbTab & strMast)
				// grid2.AddItem ("Land Code" & vbTab & "Land Category Code" & vbTab & "rilandcode" & vbTab & strMast)
				// grid2.AddItem ("Bldg Code" & vbTab & "Building Category Code" & vbTab & "ribldgcode" & vbTab & strMast)
				// grid2.AddItem ("Current Land" & vbTab & "Correlated Land Value" & vbTab & "rllandval" & vbTab & strMast)
				// grid2.AddItem ("Current Bldg" & vbTab & "Correlated Building Value" & vbTab & "rlbldgval" & vbTab & strMast)
				// grid2.AddItem ("Exemption" & vbTab & "Exemption value" & vbTab & "rlexemption" & vbTab & strMast)
				// grid2.AddItem ("Exempt 1" & vbTab & "Exempt Code One" & vbTab & "riexemptcd1" & vbTab & strMast)
				// grid2.AddItem ("Exempt 2" & vbTab & "Exempt Code Two" & vbTab & "riexemptcd2" & vbTab & strMast)
				// grid2.AddItem ("Exempt 3" & vbTab & "Exempt Code Three" & vbTab & "riexemptcd3" & vbTab & strMast)
				// 
				// grid2.AddItem ("Tran Code (User Defined)" & vbTab & "ritrancode" & vbTab & strMast)
				// 
				// grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, grid2.Rows - 1, 3) = RGB(155, 193, 223)
				// intTemp = grid2.Rows
				// 
				// grid2.AddItem ("Occ One" & vbTab & "Occupancy Code One" & vbTab & "occ1" & vbTab & strComm)
				// grid2.AddItem ("Dwell 1" & vbTab & "Dwelling Code One" & vbTab & "dwel1" & vbTab & strComm)
				// grid2.AddItem ("Class 1" & vbTab & "Class Code One" & vbTab & "c1class" & vbTab & strComm)
				// grid2.AddItem ("Quality 1" & vbTab & "Quality code one" & vbTab & "c1quality" & vbTab & strComm)
				// grid2.AddItem ("Grade 1" & vbTab & "Grade Code One" & vbTab & "c1grade" & vbTab & strComm)
				// grid2.AddItem ("Ext Walls 1" & vbTab & "Exterior Walls Code 1" & vbTab & "c1extwalls" & vbTab & strComm)
				// grid2.AddItem ("Stories 1" & vbTab & "# Stories one" & vbTab & "c1stories" & vbTab & strComm)
				// grid2.AddItem ("Height 1" & vbTab & "Height One" & vbTab & "c1height" & vbTab & strComm)
				// grid2.AddItem ("Floor 1" & vbTab & "Floor Area One" & vbTab & "c1floor" & vbTab & strComm)
				// grid2.AddItem ("Perimeter 1" & vbTab & "Perimeter/Units One" & vbTab & "c1perimeter" & vbTab & strComm)
				// grid2.AddItem ("Heat 1" & vbTab & "Heat Type One" & vbTab & "c1heat" & vbTab & strComm)
				// grid2.AddItem ("Year Built 1" & vbTab & "Year Built One" & vbTab & "c1built" & vbTab & strComm)
				// grid2.AddItem ("Remodel 1" & vbTab & "Year Remodeled One" & vbTab & "c1remodel" & vbTab & strComm)
				// grid2.AddItem ("Condition 1" & vbTab & "Condition Code One" & vbTab & "c1condition" & vbTab & strComm)
				// grid2.AddItem ("Phys 1" & vbTab & "Physical Percent One" & vbTab & "c1phys" & vbTab & strComm)
				// grid2.AddItem ("Funct 1" & vbTab & "Functional Percent One" & vbTab & "c1funct" & vbTab & strComm)
				// 
				// grid2.AddItem ("Occ Two" & vbTab & "Occupancy Code Two" & vbTab & "occ2" & vbTab & strComm)
				// grid2.AddItem ("Dwell 2" & vbTab & "Dwelling Code Two" & vbTab & "dwel2" & vbTab & strComm)
				// grid2.AddItem ("Class 2" & vbTab & "Class Code Two" & vbTab & "c2class" & vbTab & strComm)
				// grid2.AddItem ("Quality 2" & vbTab & "Quality code Two" & vbTab & "c2quality" & vbTab & strComm)
				// grid2.AddItem ("Grade 2" & vbTab & "Grade Code two" & vbTab & "c2grade" & vbTab & strComm)
				// grid2.AddItem ("Ext Walls 2" & vbTab & "Exterior Walls Code 2" & vbTab & "c2extwalls" & vbTab & strComm)
				// grid2.AddItem ("Stories 2" & vbTab & "# Stories two" & vbTab & "c2stories" & vbTab & strComm)
				// grid2.AddItem ("Height 2" & vbTab & "Height two" & vbTab & "c2height" & vbTab & strComm)
				// grid2.AddItem ("Floor 2" & vbTab & "Floor Area two" & vbTab & "c2floor" & vbTab & strComm)
				// grid2.AddItem ("Perimeter 2" & vbTab & "Perimeter/Units two" & vbTab & "c2perimeter" & vbTab & strComm)
				// grid2.AddItem ("Heat 2" & vbTab & "Heat Type two" & vbTab & "c2heat" & vbTab & strComm)
				// grid2.AddItem ("Year Built 2" & vbTab & "Year Built two" & vbTab & "c2built" & vbTab & strComm)
				// grid2.AddItem ("Remodel 2" & vbTab & "Year Remodeled two" & vbTab & "c2remodel" & vbTab & strComm)
				// grid2.AddItem ("Condition 2" & vbTab & "Condition Code two" & vbTab & "c2condition" & vbTab & strComm)
				// grid2.AddItem ("Phys 2" & vbTab & "Physical Percent two" & vbTab & "c2phys" & vbTab & strComm)
				// grid2.AddItem ("Funct 2" & vbTab & "Functional Percent two" & vbTab & "c2funct" & vbTab & strComm)
				// 
				// grid2.AddItem ("Econ Perc" & vbTab & "Economic Percent Good" & vbTab & "cmecon" & vbTab & strComm)
				// grid2.AddItem ("Entrance" & vbTab & "Entrance Code" & vbTab & "ent" & vbTab & strComm)
				// grid2.AddItem ("Info" & vbTab & "Information Code" & vbTab & "inf" & vbTab & strComm)
				// grid2.AddItem ("Insp. Date" & vbTab & "Inspection Date" & vbTab & "oidateinspected" & strOut)
				// 
				// 
				// grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intTemp, 0, grid2.Rows - 1, 3) = RGB(247, 161, 153)
				// intTemp = grid2.Rows
				// 
				// 
				// 
				// grid2.AddItem ("D Style" & vbTab & "Dwelling Style Code" & vbTab & "distyle" & vbTab & strDwell)
				// grid2.AddItem ("D Units" & vbTab & "Dwelling Units" & vbTab & "diunitsdwelling" & vbTab & strDwell)
				// grid2.AddItem ("Units Other" & vbTab & "Other Dwelling Units" & vbTab & "diunitsother" & vbTab & strDwell)
				// grid2.AddItem ("D Stories" & vbTab & "# Stories" & vbTab & "distories" & vbTab & strDwell)
				// grid2.AddItem ("Ext Walls" & vbTab & "Exterior Wall Code" & vbTab & "diextwalls" & vbTab & strDwell)
				// grid2.AddItem ("Roof" & vbTab & "Roof Type" & vbTab & "diroof" & vbTab & strDwell)
				// grid2.AddItem ("SFMasonry" & vbTab & "Square Foot. of Masonry" & vbTab & "disfmasonry" & vbTab & strDwell)
				// 
				// Call clsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1521", strredatabase)
				// strTemp = clsTemp.GetData("cldesc")
				// strtemp2 = clsTemp.GetData("csdesc")
				// grid2.AddItem (strtemp2 & vbTab & "Open 3 (user defined as " & strTemp & ")" & vbTab & "diopen3" & vbTab & strDwell)
				// 
				// Call clsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1531", strredatabase)
				// strTemp = clsTemp.GetData("cldesc")
				// strtemp2 = clsTemp.GetData("csdesc")
				// grid2.AddItem (strtemp2 & vbTab & "Open 4 (user defined as " & strTemp & ")" & vbTab & "diopen4" & vbTab & strDwell)
				// 
				// grid2.AddItem ("Built" & vbTab & "Year Dwelling built" & vbTab & "diyearbuilt" & vbTab & strDwell)
				// grid2.AddItem ("Remodel" & vbTab & "Year Dwelling Remodeled" & vbTab & "diyearremodel" & vbTab & strDwell)
				// grid2.AddItem ("Foundation" & vbTab & "Foundation Type" & vbTab & "difoundation" & vbTab & strDwell)
				// grid2.AddItem ("Basement" & vbTab & "Basement Type" & vbTab & "dibsmt" & vbTab & strDwell)
				// grid2.AddItem ("Bsmt/Gar" & vbTab & "# cars in Bsmt Garage" & vbTab & "dibsmtgar" & vbTab & strDwell)
				// grid2.AddItem ("Wet Bsmt" & vbTab & "Wet Basement Code" & vbTab & "diwetbsmt" & vbTab & strDwell)
				// grid2.AddItem ("S.F.Liv. Bsmt" & vbTab & "Sq. Ft. Basement Living Area" & vbTab & "disfbsmtliving" & vbTab & strDwell)
				// grid2.AddItem ("B. Grade 1" & vbTab & "Finished Bsmt. Grade 1" & vbTab & "dibsmtfingrade1" & vbTab & strDwell)
				// grid2.AddItem ("B. Grade 2" & vbTab & "Finished Bsmt. Grade 2" & vbTab & "dibsmtfingrade2" & vbTab & strDwell)
				// 
				// Call clsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1571", strredatabase)
				// strTemp = clsTemp.GetData("cldesc")
				// strtemp2 = clsTemp.GetData("csdesc")
				// grid2.AddItem (strtemp2 & vbTab & "Open 5 (user defined as " & strTemp & ")" & vbTab & "diopen5" & vbTab & strDwell)
				// 
				// grid2.AddItem ("Heat" & vbTab & "Heat Type Code" & vbTab & "diheat" & vbTab & strDwell)
				// grid2.AddItem ("Pct. Heat" & vbTab & "Percent Heated" & vbTab & "dipctheat" & vbTab & strDwell)
				// grid2.AddItem ("Cool" & vbTab & "Cool Type Code" & vbTab & "dicool" & vbTab & strDwell)
				// grid2.AddItem ("Pct. Cool" & vbTab & "Percent Cooled" & vbTab & "dipctcool" & vbTab & strDwell)
				// grid2.AddItem ("Kitchen" & vbTab & "Kitchen Type Code" & vbTab & "dikitchens" & vbTab & strDwell)
				// grid2.AddItem ("baths" & vbTab & "# Bathrooms" & vbTab & "dibaths" & vbTab & strDwell)
				// grid2.AddItem ("Rooms" & vbTab & "# Rooms" & vbTab & "dirooms" & vbTab & strDwell)
				// grid2.AddItem ("Bedrooms" & vbTab & "# Bedrooms" & vbTab & "dibedrooms" & vbTab & strDwell)
				// grid2.AddItem ("FullBaths" & vbTab & "# Full Baths" & vbTab & "difullbaths" & vbTab & strDwell)
				// grid2.AddItem ("HalfBaths" & vbTab & "# Half Baths" & vbTab & "dihalfbaths" & vbTab & strDwell)
				// grid2.AddItem ("Add. Fixt." & vbTab & "# Additional Fixtures" & vbTab & "diaddnfixtures" & vbTab & strDwell)
				// grid2.AddItem ("FirePlaces" & vbTab & "# FirePlaces" & vbTab & "difireplaces" & vbTab & strDwell)
				// grid2.AddItem ("Layout" & vbTab & "Layout Code" & vbTab & "dilayout" & vbTab & strDwell)
				// grid2.AddItem ("Attic" & vbTab & "Attic Code" & vbTab & "diattic" & vbTab & strDwell)
				// grid2.AddItem ("Insulation" & vbTab & "Insulation Code" & vbTab & "diinsulation" & vbTab & strDwell)
				// grid2.AddItem ("Pct. Unf." & vbTab & "Percent Unfinished" & vbTab & "dipctunfinished" & vbTab & strDwell)
				// grid2.AddItem ("Grade 1" & vbTab & "Grade code One" & vbTab & "digrade1" & vbTab & strDwell)
				// grid2.AddItem ("Grade 2" & vbTab & "Grade code Two" & vbTab & "digrade2" & vbTab & strDwell)
				// grid2.AddItem ("SQFT" & vbTab & "Square Footage" & vbTab & "disqft" & vbTab & strDwell)
				// grid2.AddItem ("Condition" & vbTab & "Condition Code" & vbTab & "dicondition" & vbTab & strDwell)
				// grid2.AddItem ("Pct Phys" & vbTab & "Physical Percent (entered)" & vbTab & "dipctphys" & vbTab & strDwell)
				// grid2.AddItem ("Funct Pct" & vbTab & "Functional Percent (entered)" & vbTab & "dipctfunct" & vbTab & strDwell)
				// grid2.AddItem ("Funct Code" & vbTab & "Functional Code" & vbTab & "difunctcode" & vbTab & strDwell)
				// grid2.AddItem ("Econ Pct" & vbTab & "Economic Percent (entered)" & vbTab & "dipctecon" & vbTab & strDwell)
				// grid2.AddItem ("Econ Code" & vbTab & "Economic Percent Code" & vbTab & "dieconcode" & vbTab & strDwell)
				// grid2.AddItem ("Ent. Code" & vbTab & "Entrance Code" & vbTab & "dientrancecode" & vbTab & strDwell)
				// grid2.AddItem ("Inf. Code" & vbTab & "Information Code" & vbTab & "diinformation" & vbTab & strDwell)
				// grid2.AddItem ("SFLA" & vbTab & "Square Footage Living Area" & vbTab & "disfla" & vbTab & strDwell)
				// 
				// 
				// grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intTemp, 0, grid2.Rows - 1, 3) = RGB(191, 223, 153)
				// intTemp = grid2.Rows
				// 
				grid2.AddItem(0 + "\t" + "Clear Cell");
				clsTemp.OpenRecordset("select * from reportcodes order by code", modGlobalVariables.strREDatabase);
				while (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					strItem = clsTemp.Get_Fields("Code") + "\t";
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					if ((Conversion.Val(clsTemp.Get_Fields("code")) >= 98) && (Conversion.Val(clsTemp.Get_Fields("code")) <= 146))
					{
						goto SkipCode;
					}
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					else if (Conversion.Val(clsTemp.Get_Fields("Code")) >= 400)
					{
						goto SkipCode;
					}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						else if (Conversion.Val(clsTemp.Get_Fields("Code")) < 111 || Conversion.Val(clsTemp.Get_Fields("Code")) > 146)
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						if (Conversion.Val(clsTemp.Get_Fields("code")) == 3)
						{
							strTemp = string.Empty;
							clsCost.OpenRecordset("select * from costrecord where crecordnumber = 1091", modGlobalVariables.Statics.strRECostFileDatabase);
							if (!clsCost.EndOfFile())
							{
								strTemp = Strings.Trim(FCConvert.ToString(clsCost.Get_Fields_String("cldesc")));
							}
							if (strTemp == string.Empty)
							{
								strTemp = "X Coordinate / User Defined";
							}
							strItem += strTemp;
						}
								// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
								else if (Conversion.Val(clsTemp.Get_Fields("code")) == 4)
						{
							strTemp = string.Empty;
							clsCost.OpenRecordset("select * from costrecord where crecordnumber = 1092", modGlobalVariables.Statics.strRECostFileDatabase);
							if (!clsCost.EndOfFile())
							{
								strTemp = Strings.Trim(FCConvert.ToString(clsCost.Get_Fields_String("cldesc")));
							}
							if (strTemp == string.Empty)
							{
								strTemp = "Y Coordinate / User Defined";
							}
							strItem += strTemp;
						}
									// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
									else if (Conversion.Val(clsTemp.Get_Fields("code")) == 10)
						{
							strTemp = string.Empty;
							clsCost.OpenRecordset("select * from costrecord where crecordnumber = 1330", modGlobalVariables.Statics.strRECostFileDatabase);
							if (!clsCost.EndOfFile())
							{
								strTemp = Strings.Trim(FCConvert.ToString(clsCost.Get_Fields_String("cldesc")));
							}
							if (strTemp == string.Empty)
							{
								strTemp = "Open 1";
							}
							strItem += strTemp;
						}
										// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
										else if (Conversion.Val(clsTemp.Get_Fields("code")) == 11)
						{
							strTemp = string.Empty;
							clsCost.OpenRecordset("select * from costrecord where crecordnumber = 1340", modGlobalVariables.Statics.strRECostFileDatabase);
							if (!clsCost.EndOfFile())
							{
								strTemp = Strings.Trim(FCConvert.ToString(clsCost.Get_Fields_String("cldesc")));
							}
							if (strTemp == string.Empty)
							{
								strTemp = "Open 2";
							}
							strItem += strTemp;
						}
											// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
											else if (Conversion.Val(clsTemp.Get_Fields("code")) == 210)
						{
							strTemp = string.Empty;
							clsCost.OpenRecordset("select * from costrecord where crecordnumber = 1520", modGlobalVariables.Statics.strRECostFileDatabase);
							if (!clsCost.EndOfFile())
							{
								strTemp = Strings.Trim(FCConvert.ToString(clsCost.Get_Fields_String("cldesc")));
							}
							if (strTemp == string.Empty)
							{
								strTemp = "Open 3";
							}
							strItem += strTemp;
						}
												// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
												else if (Conversion.Val(clsTemp.Get_Fields("code")) == 211)
						{
							strTemp = string.Empty;
							clsCost.OpenRecordset("select * from costrecord where crecordnumber = 1530", modGlobalVariables.Statics.strRECostFileDatabase);
							if (!clsCost.EndOfFile())
							{
								strTemp = Strings.Trim(FCConvert.ToString(clsCost.Get_Fields_String("cldesc")));
							}
							if (strTemp == string.Empty)
							{
								strTemp = "Open 4";
							}
							strItem += strTemp;
						}
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						else if (Conversion.Val(clsTemp.Get_Fields("code")) == 221)
						{
							strTemp = string.Empty;
							clsCost.OpenRecordset("select * from costrecord where crecordnumber = 1570", modGlobalVariables.Statics.strRECostFileDatabase);
							if (!clsCost.EndOfFile())
							{
								strTemp = Strings.Trim(FCConvert.ToString(clsCost.Get_Fields_String("cldesc")));
							}
							if (strTemp == string.Empty)
							{
								strTemp = "Open 5";
							}
							strItem += strTemp;
						}
						else
						{
							strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("Description"));
							strTemp = strTemp.Replace(",", " ");
							strItem += strTemp;
						}
						// Skip outbuildingcode since there are 10 to choose from.  How would you decide what to print?
					}
					else
					{
						goto SkipCode;
						// skip landcodes since there could be 7 to choose from. How would you decide what to print?
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						clsCost.OpenRecordset("Select * from costrecord where crecordnumber = " + FCConvert.ToString(1280 + clsTemp.Get_Fields("Code")), modGlobalVariables.strREDatabase);
						strTemp = FCConvert.ToString(clsCost.Get_Fields_String("CLDesc"));
						strTemp = strTemp.Replace(",", " ");
						strItem += strTemp;
					}
					strItem += "\t" + clsTemp.Get_Fields_String("fieldname") + "\t" + clsTemp.Get_Fields_String("tablename");
					grid2.AddItem(strItem);
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					if (Conversion.Val(clsTemp.Get_Fields("code")) < 200)
					{
						grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, (grid2.Rows - 1), 0, grid2.Rows - 1, grid2.Cols - 1, Information.RGB(155, 193, 223));
					}
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					else if (Conversion.Val(clsTemp.Get_Fields("code")) < 300)
					{
						grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, (grid2.Rows - 1), 0, grid2.Rows - 1, grid2.Cols - 1, Information.RGB(247, 161, 153));
					}
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						else if (Conversion.Val(clsTemp.Get_Fields("code")) < 400)
					{
						grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, (grid2.Rows - 1), 0, grid2.Rows - 1, grid2.Cols - 1, Information.RGB(191, 223, 153));
					}
					SkipCode:
					;
					clsTemp.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In function SetupGrid", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmCustomReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid1_RowHeightChanged(object sender, DataGridViewRowEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			Grid1_AfterUserResize(grid.GetFlexRowIndex(e.RowIndex), -1);
		}

		private void Grid1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			Grid1_AfterUserResize(-1, grid.GetFlexColIndex(e.Column.Index));
		}

		private void Grid1_AfterUserResize(int row, int col)
		{
			Grid1.ColData(col, Grid1.ColWidth(col));
		}

		private void Grid1_MouseDownEvent(object sender, MouseEventArgs e)
		{
			if ((MouseButtons)e.Button == MouseButtons.Right)
			{
				{
					Grid1.Row = Grid1.MouseRow;
					Grid1.Col = Grid1.MouseCol;
					this.PopupMenu(mnuOptions);
				}
			}
		}

		private void Grid1_RowColChange(object sender, System.EventArgs e)
		{
			string strTemp = string.Empty;
			int intTemp;
			string[] strAry = null;
			if ((Grid1.Row > 0) && (Grid1.Col >= 0))
			{
				var x = Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col);
				if (x != null && FCConvert.ToString(x).Length > 1)
				{
					// intTemp = InStr(1, Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col), ",", vbTextCompare)
					// If intTemp > 0 Then
					// strTemp = Mid(Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col), 1, intTemp - 1)
					// txtCode.Text = strTemp
					// 
					// intStart = intTemp + 1
					// intTemp = InStr(intStart, Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col), ",", vbTextCompare)
					// strTemp = Mid(Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col), intStart, intTemp - intStart)
					// txtDescription.Text = strTemp
					strAry = Strings.Split(FCConvert.ToString(Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col)), ",", -1, CompareConstants.vbTextCompare);
					txtCode.Text = strAry[0];
					txtDescription.Text = strAry[1];
					if (strAry[2] == "R")
					{
						cmbJust.Text = "Right";
					}
					else
					{
						cmbJust.Text = "Left";
					}
					if (Conversion.Val(strAry[3]) == 0)
					{
						// none
						cmbTotal.Text = "None";
					}
					else if (Conversion.Val(strAry[3]) == 1)
					{
						// total
						cmbTotal.Text = "Total";
					}
					else if (Conversion.Val(strAry[3]) == 2)
					{
						// count
						cmbTotal.Text = "Count";
					}
				}
				else
				{
					txtCode.Text = string.Empty;
					txtDescription.Text = string.Empty;
					cmbJust.Text = "Left";
					cmbTotal.Text = "None";
				}
			}
		}

		private void grid2_DblClick(object sender, System.EventArgs e)
		{
			string strTemp = string.Empty;
			if (Grid1.Row < 1)
				return;
			if (Grid1.Col < 0)
				return;
			if (Conversion.Val(grid2.TextMatrix(grid2.Row, 0)) > 0)
			{
				if (cmbJust.Text == "Left")
				{
					strTemp = "L";
				}
				else
				{
					strTemp = "R";
				}
				if (cmbTotal.Text == "None")
				{
					strTemp += ",0";
				}
				else if (cmbTotal.Text == "Total")
				{
					strTemp += ",1";
				}
				else
				{
					strTemp += ",2";
				}
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col, grid2.TextMatrix(grid2.Row, 0) + "," + grid2.TextMatrix(grid2.Row, 1) + "," + strTemp);
				// & "," & grid2.TextMatrix(grid2.Row, 2) & "," & grid2.TextMatrix(grid2.Row, 3) & "," & strTemp
				Grid1.TextMatrix(Grid1.Row, Grid1.Col, grid2.TextMatrix(grid2.Row, 1));
				txtCode.Text = grid2.TextMatrix(grid2.Row, 0);
				txtDescription.Text = grid2.TextMatrix(grid2.Row, 1);
			}
			else
			{
				strTemp = ",,L,0";
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col, strTemp);
				Grid1.TextMatrix(Grid1.Row, Grid1.Col, "");
				txtCode.Text = FCConvert.ToString(0);
				txtDescription.Text = "Clear Field";
			}
		}

		private void mnuAddCol_Click(object sender, System.EventArgs e)
		{
			int x;
			string strTemp = string.Empty;
			Grid1.Cols += 1;
			Grid1.ColWidth(Grid1.Cols - 1, FCConvert.ToInt32(1440/* dblPicResizeRatio*/));
			Grid1.ColData(Grid1.Cols - 1, 1440);
			for (x = 1; x <= Grid1.Rows - 1; x++)
			{
				// default the settings
				strTemp = ",,L,0";
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, x, Grid1.Cols - 1, strTemp);
			}
			// x
		}

		public void mnuAddCol_Click()
		{
			mnuAddCol_Click(mnuAddCol, new System.EventArgs());
		}

		private void mnuAddCol2_Click(object sender, System.EventArgs e)
		{
			mnuAddCol_Click();
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			Grid1.Rows += 1;
		}

		public void mnuAddRow_Click()
		{
			mnuAddRow_Click(mnuAddRow, new System.EventArgs());
		}

		private void mnuAddRow2_Click(object sender, System.EventArgs e)
		{
			mnuAddRow_Click();
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			lngCurrentReportNumber = 0;
			Grid1.Rows = 1;
			Grid1.Cols = 1;
			Grid1.ColWidth(0, FCConvert.ToInt32(1440/*/ dblPicResizeRatio*/));
			Grid1.ColData(0, 1440);
			txtCode.Text = string.Empty;
			txtDescription.Text = string.Empty;
			txtTitle.Text = string.Empty;
			cmbJust.Text = "Left";
			Grid1.Rows = 2;
			Grid1.Row = 1;
			Grid1.Col = 0;
		}

		public void mnuClear_Click()
		{
			mnuClear_Click(cmdClear, new System.EventArgs());
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			boolDelete = true;
            //FC:FINAL:AM: #1649 - Show form as modal
            //frmChooseCustom.InstancePtr.Show();
            frmChooseCustom.InstancePtr.Show(FormShowEnum.Modal);
		}

		private void mnuDeleteColumn_Click(object sender, System.EventArgs e)
		{
			if (Grid1.Col < 0)
				return;
			if (Grid1.Cols > 1)
			{
				Grid1.ColPosition(Grid1.Col, Grid1.Cols - 1);
				Grid1.Cols -= 1;
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void mnuDeleteColumn_Click()
		{
			mnuDeleteColumn_Click(mnuDeleteColumn, new System.EventArgs());
		}

		private void mnuDeleteColumn2_Click(object sender, System.EventArgs e)
		{
			mnuDeleteColumn_Click();
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			if (Grid1.Row < 1)
				return;
			if (Grid1.Rows > 2)
			{
				Grid1.RemoveItem(Grid1.Row);
			}
			else
			{
				MessageBox.Show("You must keep at least one row in a report.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void mnuDeleteRow_Click()
		{
			mnuDeleteRow_Click(mnuDeleteRow, new System.EventArgs());
		}

		private void mnuDeleteRow2_Click(object sender, System.EventArgs e)
		{
			mnuDeleteRow_Click();
		}

		public void mnuExit_Click()
		{
			this.Unload();
		}

		private void mnuExport_Click(object sender, System.EventArgs e)
		{
			// export custom report
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			string strFile = string.Empty;
			string strList;
			string strTemp = string.Empty;
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select reportnumber,reporttitle from customreports where trim(reporttitle & '') <> '' order by reportnumber", modGlobalVariables.strREDatabase);
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("There are no saved reports to export", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				while (!clsLoad.EndOfFile())
				{
					strTemp += clsLoad.Get_Fields_String("ReportTitle") + "|" + clsLoad.Get_Fields_Int32("reportnumber") + ";";
					clsLoad.MoveNext();
				}
				if (Strings.Trim(strTemp) != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				strList = frmListChoice.InstancePtr.Init(strTemp, "Choose reports to Export", "", true, "", false, true);
				if (strList == "-1" || strList == string.Empty)
				{
					// didn't make a choice
					return;
				}
				strList = Strings.Replace(strList, ";", ",", 1, -1, CompareConstants.vbTextCompare);
				// make comma delimited
				MDIParent.InstancePtr.CommonDialog1.DefaultExt = "json";
				MDIParent.InstancePtr.CommonDialog1.FileName = "ReportExport.json";
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNOverwritePrompt+vbPorterConverter.cdlOFNPathMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				MDIParent.InstancePtr.CommonDialog1.Filter = "JSON|*.json";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.ShowSave();
				strFile = MDIParent.InstancePtr.CommonDialog1.FileName;
				if (Strings.Trim(strFile) == string.Empty)
					return;
				// now make a database
				if (MakeExportDatabase(ref strFile))
				{
					// fill database
					if (FillExportDatabase(ref strFile, ref strList))
					{
						MessageBox.Show("Export Successful", "Exported", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 32755)
					return;
				// cancel button
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuExport", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool FillExportDatabase(ref string strFile, ref string strList)
		{
			bool FillExportDatabase = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strDBPath;
				string strDBFile;
				//FileSystemObject fso = new FileSystemObject();
				clsDRWrapper clsLoad = new clsDRWrapper();
				// Dim clsSave As New clsDRWrapper
				GenericJSONTableSerializer tJSONSer = new GenericJSONTableSerializer();
				TableCollection tTabColl = new TableCollection();
				tTabColl.DBName = "CustomReports";
				FillExportDatabase = false;
				strDBPath = Directory.GetParent(strFile).FullName;
				if (strDBPath != string.Empty)
				{
					if (Strings.Right(strDBPath, 1) != "\\")
					{
						strDBPath += "\\";
					}
					// clsSave.Path = strDBPath
				}
				strDBFile = new FileInfo(strFile).Name;
				clsLoad.OpenRecordset("select * from customreports where  reportnumber in (" + strList + ") order by reportnumber,rownumber,colnumber", modGlobalVariables.strREDatabase);
				string strJSON;
				TableItem tTab = new TableItem();
				tTab = clsLoad.TableToTableItem();
				if (!(tTab == null))
				{
					tTabColl.AddItem(tTab);
				}
				strJSON = tJSONSer.GetJSONFromCollection(ref tTabColl);
				StreamWriter ts;
				ts = File.CreateText(strFile);
				ts.Write(strJSON);
				ts.Close();
				// Call clsSave.OpenRecordset("select * from customreports", strDBFile)
				// Do While Not clsLoad.EndOfFile
				// clsSave.AddNew
				// clsSave.Fields("Reportnumber") = clsLoad.Fields("reportnumber")
				// If Val(clsLoad.Fields("rownumber")) > 0 Then
				// clsSave.Fields("RowNumber") = clsLoad.Fields("RowNumber")
				// clsSave.Fields("ColNumber") = clsLoad.Fields("ColNumber")
				// clsSave.Fields("ColWidth") = clsLoad.Fields("ColWidth")
				// clsSave.Fields("Code") = clsLoad.Fields("Code")
				// clsSave.Fields("Header") = clsLoad.Fields("Header")
				// clsSave.Fields("Justification") = clsLoad.Fields("Justification")
				// clsSave.Fields("Format") = clsLoad.Fields("Format")
				// clsSave.Fields("DefaultReport") = clsLoad.Fields("DefaultReport")
				// clsSave.Fields("TotalOption") = clsLoad.Fields("TotalOption")
				// Else
				// clsSave.Fields("ReportTitle") = clsLoad.Fields("reportTitle")
				// End If
				// clsSave.Update
				// clsLoad.MoveNext
				// Loop
				FillExportDatabase = true;
				return FillExportDatabase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillExportDatabase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillExportDatabase;
		}

		private bool MakeExportDatabase(ref string strFile)
		{
			//FileSystemObject fso = new FileSystemObject();
			bool MakeExportDatabase = false;
			try
			{
				// On Error GoTo ErrorHandler
				// Dim clsSave As New clsDRWrapper
				// Dim strDBPath As String
				// Dim strDBFile As String
				MakeExportDatabase = false;
				if (File.Exists(strFile))
				{
					File.Delete(strFile);
				}
				// Call DBEngine.CreateDatabase(strFile, dbLangGeneral, dbVersion40)
				// strDBPath = fso.GetParentFolderName(strFile)
				// strDBFile = fso.GetFileName(strFile)
				// 
				// If Trim(strDBPath) <> vbNullString Then
				// If Right(strDBPath, 1) <> "\" Then
				// strDBPath = strDBPath & "\"
				// End If
				// clsSave.Path = strDBPath
				// End If
				// 
				// If clsSave.CreateNewDatabaseTable("CustomReports", strDBFile) Then
				// Call clsSave.AddTableField("ReportNumber", dbLong)
				// Call clsSave.AddTableField("ReportTitle", dbText, 255)
				// Call clsSave.SetFieldAllowZeroLength("ReportTitle", True)
				// Call clsSave.AddTableField("RowNumber", dbLong)
				// Call clsSave.AddTableField("ColNumber", dbLong)
				// Call clsSave.AddTableField("ColWidth", dbInteger)
				// Call clsSave.AddTableField("Code", dbInteger)
				// Call clsSave.AddTableField("Header", dbText, 255)
				// Call clsSave.SetFieldAllowZeroLength("Header", True)
				// Call clsSave.AddTableField("Justification", dbText, 255)
				// Call clsSave.SetFieldAllowZeroLength("Justification", True)
				// Call clsSave.AddTableField("Format", dbText, 255)
				// Call clsSave.SetFieldAllowZeroLength("Format", True)
				// Call clsSave.AddTableField("defaultreport", dbBoolean)
				// Call clsSave.AddTableField("TotalOption", dbInteger)
				// clsSave.UpdateTableCreation
				// Else
				// Exit Function
				// End If
				// 
				MakeExportDatabase = true;
				return MakeExportDatabase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MakeExportDatabase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeExportDatabase;
		}

		private void mnuImport_Click(object sender, System.EventArgs e)
		{
			// IMPORT custom report
			// Dim clsLoad As New clsDRWrapper
			clsDRWrapper clsSave = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			// vbPorter upgrade warning: lngRNumber As int	OnWrite(short, double)
			int lngRNumber = 0;
			int lngLoadNumber;
			string strFile;
			string strJSON = string.Empty;
			GenericJSONTableSerializer tJSONSer = new GenericJSONTableSerializer();
			TableCollection tTabColl = new TableCollection();
			try
			{
				// On Error GoTo ErrorHandler
				strFile = string.Empty;
				MDIParent.InstancePtr.CommonDialog1.DefaultExt = "json";
				// .FileName = "*.mdb"
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNFileMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				MDIParent.InstancePtr.CommonDialog1.Filter = "JSON|*.json";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strFile = MDIParent.InstancePtr.CommonDialog1.FileName;
				if (Strings.Trim(strFile) == string.Empty)
					return;
				if (!System.IO.File.Exists(strFile))
					return;
				lngLoadNumber = 0;
				clsSave.OpenRecordset("select * from customreports where reportnumber = -100", modGlobalVariables.strREDatabase);
				// clsLoad.Path = fso.GetParentFolderName(strFile)
				// strFile = fso.GetFileName(strFile)
				StreamReader ts;
				ts = System.IO.File.OpenText(strFile);
				if (!(ts == null))
				{
					strJSON = ts.ReadToEnd();
					tTabColl = tJSONSer.GetCollectionFromJSON(strJSON);
					if (!(tTabColl == null))
					{
						TableItem tTabItem = new TableItem();
						tTabItem = tTabColl.Item("CustomReports");
						if (!(tTabItem == null))
						{
							RecordItem tRec = new RecordItem();
							int x;
							for (x = 0; x <= tTabItem.RecordCount() - 1; x++)
							{
								tRec = tTabItem.GetRecord(x);
								if (!(tRec == null))
								{
									if (Conversion.Val(tRec.GetItem("ReportNumber")) != lngLoadNumber)
									{
										// new report
										lngRNumber = 1;
										clsTemp.OpenRecordset("select max(reportnumber) as maxnum from customreports", modGlobalVariables.strREDatabase);
										if (!clsTemp.EndOfFile())
										{
											// TODO Get_Fields: Field [maxnum] not found!! (maybe it is an alias?)
											lngRNumber = FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("maxnum")) + 1);
										}
									}
									lngLoadNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(tRec.GetItem("ReportNumber"))));
									clsSave.AddNew();
									clsSave.Set_Fields("reportnumber", lngRNumber);
									if (Conversion.Val(tRec.GetItem("RowNumber")) > 0)
									{
										clsSave.Set_Fields("rownumber", tRec.GetItem("rownumber"));
										clsSave.Set_Fields("colnumber", tRec.GetItem("colnumber"));
										clsSave.Set_Fields("colwidth", tRec.GetItem("colwidth"));
										clsSave.Set_Fields("code", tRec.GetItem("code"));
										clsSave.Set_Fields("header", tRec.GetItem("header"));
										clsSave.Set_Fields("justification", tRec.GetItem("justification"));
										clsSave.Set_Fields("format", tRec.GetItem("format"));
										clsSave.Set_Fields("defaultreport", tRec.GetItem("defaultreport"));
										clsSave.Set_Fields("totaloption", tRec.GetItem("totaloption"));
									}
									else
									{
										clsSave.Set_Fields("reporttitle", tRec.GetItem("reporttitle"));
									}
									clsSave.Update();
								}
							}
							// x
						}
					}
				}
				// Call clsLoad.OpenRecordset("select * from customreports order by reportnumber,rownumber,colnumber", strFile)
				// Do While Not clsLoad.EndOfFile
				// If Val(clsLoad.Fields("reportnumber")) <> lngLoadNumber Then
				// new report
				// lngRNumber = 1
				// Call clsTemp.OpenRecordset("select max(reportnumber) as maxnum from customreports", strREDatabase)
				// If Not clsTemp.EndOfFile Then
				// lngRNumber = Val(clsTemp.Fields("maxnum")) + 1
				// End If
				// End If
				// lngLoadNumber = Val(clsLoad.Fields("reportnumber"))
				// clsSave.AddNew
				// clsSave.Fields("reportnumber") = lngRNumber
				// 
				// If Val(clsLoad.Fields("rownumber")) > 0 Then
				// clsSave.Fields("rownumber") = clsLoad.Fields("rownumber")
				// clsSave.Fields("colnumber") = clsLoad.Fields("colnumber")
				// clsSave.Fields("colwidth") = clsLoad.Fields("colwidth")
				// clsSave.Fields("code") = clsLoad.Fields("code")
				// clsSave.Fields("header") = clsLoad.Fields("header")
				// clsSave.Fields("justification") = clsLoad.Fields("justification")
				// clsSave.Fields("format") = clsLoad.Fields("format")
				// clsSave.Fields("defaultreport") = clsLoad.Fields("defaultreport")
				// clsSave.Fields("totaloption") = clsLoad.Fields("totaloption")
				// Else
				// clsSave.Fields("reporttitle") = clsLoad.Fields("reporttitle")
				// End If
				// clsSave.Update
				// clsLoad.MoveNext
				// Loop
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 32755)
					return;
				// cancel button
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuImport_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuInsertColumn_Click(object sender, System.EventArgs e)
		{
			Grid1.Cols += 1;
			Grid1.ColPosition(Grid1.Cols - 1, Grid1.Col);
		}

		public void mnuInsertColumn_Click()
		{
			mnuInsertColumn_Click(mnuInsertColumn, new System.EventArgs());
		}

		private void mnuInsertColumn2_Click(object sender, System.EventArgs e)
		{
			mnuInsertColumn_Click();
		}

		private void mnuInsertRow_Click(object sender, System.EventArgs e)
		{
			if (Grid1.Row < 1)
				return;
			Grid1.AddItem("", Grid1.Row);
		}

		public void mnuInsertRow_Click()
		{
			mnuInsertRow_Click(mnuInsertRow, new System.EventArgs());
		}

		public void Start(ref bool boolR, bool boolE)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			boolRnge = boolR;
			boolEXTRCT = boolE;
			if (boolEXTRCT)
			{
				clsTemp.OpenRecordset("select * from extract where userid = " + FCConvert.ToString(lngUID) + " and reportnumber = 0", modGlobalVariables.strREDatabase);
				boolSaleStuff = Strings.UCase(FCConvert.ToString(clsTemp.Get_Fields_String("exttype"))) == "SALE";
			}
			this.Show(App.MainForm);
		}

		private void mnuInsertRow2_Click(object sender, System.EventArgs e)
		{
			mnuInsertRow_Click();
		}

		private void mnuLoad_Click(object sender, System.EventArgs e)
		{
			boolDelete = false;
			//FC:FINAL:CHN - issue #1649: Show Load form as modal.
			// frmChooseCustom.InstancePtr.Show();
			frmChooseCustom.InstancePtr.Show(FormShowEnum.Modal);
		}

		private void mnuPrintCodes_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptReportCodes.InstancePtr);
		}

		private void mnuRunReport_Click(object sender, System.EventArgs e)
		{
			try
			{
				int intCPI;
				string strFont = string.Empty;
				string strPrinterName;
				bool boolUseFont;
				int NumFonts;
				int x;
				string strOrderBy;
				mnuSave_Click();
				if (lngCurrentReportNumber == 0)
				{
					MessageBox.Show("You must save the report format before you can print it.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				Information.Err().Clear();

				if (Information.Err().Number != 0)
				{
					Information.Err().Clear();
					return;
				}
				strPrinterName = string.Empty;
				boolUseFont = false;

				strOrderBy = GetOrderbyClause();
				rptNewCustomReport.InstancePtr.Init(ref lngCurrentReportNumber, ref boolEXTRCT, ref strPrinterName, ref strFont, ref boolUseFont, strOrderBy);
				return;
			}
			catch (Exception ex)
			{
				//ErrorHandler: ;
			}

		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngRNumber As int	OnWrite(int, double)
			int lngRNumber;
			clsDRWrapper clsTemp = new clsDRWrapper();
			// vbPorter upgrade warning: Resp As short, int --> As DialogResult
			DialogResult Resp;
			string strSQL = string.Empty;
			string strCols;
			string strCols2;
			int intCounter;
			int ColCounter;
			int intpos;
			string strTemp = string.Empty;
			string[] strAry = null;
			bool boolEmail;
			if (Strings.Trim(txtTitle.Text + "") == string.Empty)
			{
				MessageBox.Show("You must enter something for a title before saving.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			lngRNumber = lngCurrentReportNumber;
			if (lngCurrentReportNumber == 0)
			{
				clsTemp.OpenRecordset("select max(reportnumber) as maxnumber from customreports", modGlobalVariables.strREDatabase);
				if (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Field [maxnumber] not found!! (maybe it is an alias?)
					lngRNumber = FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("maxnumber")) + 1);
				}
				else
				{
					lngRNumber = 1;
				}
			}
			else
			{
				Resp = MessageBox.Show("Do you wish to overwrite the saved report?" + "\r\n" + "Choose no to save as a new report.", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (Resp == DialogResult.Yes)
				{
					// Dont need to do anything
				}
				else
				{
					clsTemp.OpenRecordset("select max(reportnumber) as maxnumber from customreports", modGlobalVariables.strREDatabase);
					if (!clsTemp.EndOfFile())
					{
						// TODO Get_Fields: Field [maxnumber] not found!! (maybe it is an alias?)
						lngRNumber = clsTemp.Get_Fields("maxnumber") + 1;
					}
					else
					{
						lngRNumber = 1;
					}
				}
			}
			clsTemp.Execute("Delete from customreports where reportnumber = " + FCConvert.ToString(lngRNumber), modGlobalVariables.strREDatabase);
			clsTemp.Execute("insert into customreports (reportnumber,reporttitle) values (" + FCConvert.ToString(lngRNumber) + ",'" + txtTitle.Text + "')", modGlobalVariables.strREDatabase);
			strCols = "(ReportNumber,RowNumber,ColNumber,Header,code,Justification,Format)";
			strCols2 = "(reportnumber,rownumber,colnumber,colwidth,header,code,justification,format,totaloption)";
			for (intCounter = 1; intCounter <= (Grid1.Rows - 1); intCounter++)
			{
				for (ColCounter = 0; ColCounter <= (Grid1.Cols - 1); ColCounter++)
				{
					strTemp = FCConvert.ToString(Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, intCounter, ColCounter));
					strSQL = "(";
					strSQL += FCConvert.ToString(lngRNumber);
					strSQL += "," + FCConvert.ToString(intCounter) + "," + FCConvert.ToString(ColCounter);
					strSQL += "," + FCConvert.ToString(Conversion.Int(Conversion.Val(Grid1.ColData(ColCounter))));
					strSQL += ",'" + Grid1.TextMatrix(intCounter, ColCounter) + "'";
					if (Strings.Trim(strTemp + "").Length > 0)
					{
						strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
					}
					else
					{
						strAry = new string[5 + 1];
						strAry[0] = " ";
						strAry[1] = " ";
						strAry[2] = "L";
						strAry[3] = "0";
					}
					if (Strings.Trim(strAry[0]) != string.Empty)
					{
						strSQL += "," + strAry[0];
					}
					else
					{
						strSQL += ",0";
					}
					// strSQL = strSQL & ",'" & strary(2) & "'"      'fieldname
					// strSQL = strSQL & ",'" & strary(3) & "'"      'tablename
					strSQL += ",'" + strAry[2] + "'";
					// justification
					strSQL += ",' '";
					strSQL += "," + FCConvert.ToString(Conversion.Val(strAry[3]));
					strSQL += ")";
					FCUtils.EraseSafe(strAry);
					// If intCounter = 1 Then
					clsTemp.Execute("insert into customreports " + strCols2 + " values " + strSQL, modGlobalVariables.strREDatabase);
					// Else
					// Call clsTemp.Execute("insert into customreports " & strCols & " values " & strSQL, strredatabase)
					// End If
				}
				// ColCounter
			}
			// intCounter
			lngCurrentReportNumber = lngRNumber;
		}

		public void mnuSave_Click()
		{
			//FC:FINAL:MSH - i.issue #1172: buttons changed and for call method need to use cmdSave btn
			//mnuSave_Click(cmdRunReport, new System.EventArgs());
			mnuSave_Click(cmdSave, new System.EventArgs());
		}

		private void optJust_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			string strTemp = string.Empty;
			string[] strAry = null;
			if (Grid1.Row < 1)
				return;
			if (Grid1.Col < 0)
				return;
			strTemp = FCConvert.ToString(Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col));
			//FC:FINAL:DDU:#i1170 - format string if null
			if (string.IsNullOrEmpty(strTemp))
			{
				strTemp = ",,,";
			}
			strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
			if (strTemp.Length > 0)
			{
				if (cmbJust.Text == "Left")
				{
					// Mid(strTemp, Len(strTemp)) = "L"
					strAry[2] = "L";
				}
				else
				{
					// Mid(strTemp, Len(strTemp)) = "R"
					strAry[2] = "R";
				}
				strTemp = strAry[0] + "," + strAry[1] + "," + strAry[2] + "," + strAry[3];
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col, strTemp);
			}
		}

		private void optJust_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbJust.SelectedIndex;
			optJust_CheckedChanged(index, sender, e);
		}

		public void FillGrid1()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			int intTemp;
			int intRCounter;
			int intCCounter;
			int intNCols;
			string strTemp = string.Empty;
			string strDesc = string.Empty;
			string strCode = string.Empty;
			string strData = string.Empty;
			try
			{
				// On Error GoTo ErrorHandler
				if (lngCurrentReportNumber == 0)
				{
					mnuClear_Click();
					return;
				}
				clsTemp.OpenRecordset("select * from customreports where REPORTNUMBER = " + FCConvert.ToString(lngCurrentReportNumber) + " and reporttitle <> ''", modGlobalVariables.strREDatabase);
				txtTitle.Text = FCConvert.ToString(clsTemp.Get_Fields_String("reporttitle"));
				clsTemp.OpenRecordset("select max(rownumber) as numrows,max(colnumber) as numcols from customreports where reportnumber = " + FCConvert.ToString(lngCurrentReportNumber), modGlobalVariables.strREDatabase);
				// TODO Get_Fields: Field [numrows] not found!! (maybe it is an alias?)
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("numrows"))));
				// TODO Get_Fields: Field [numcols] not found!! (maybe it is an alias?)
				intNCols = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("numcols"))));
				Grid1.Rows = intTemp + 1;
				Grid1.Cols = intNCols + 1;
				clsTemp.OpenRecordset("select * from customreports where reportnumber = " + FCConvert.ToString(lngCurrentReportNumber) + " and isnull(reporttitle,'') = '' order by rownumber,colnumber", modGlobalVariables.strREDatabase);
				while (!clsTemp.EndOfFile())
				{
					Grid1.Row = FCConvert.ToInt32(clsTemp.Get_Fields_Int16("rownumber"));
					Grid1.Col = FCConvert.ToInt32(clsTemp.Get_Fields_Int16("colnumber"));
					if (Grid1.Row == 1)
					{
						Grid1.ColData(Grid1.Col, Conversion.Val(clsTemp.Get_Fields_Int16("colwidth")));
						Grid1.ColWidth(Grid1.Col, FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields_Int16("colwidth"))));
					}
					Grid1.TextMatrix(Grid1.Row, Grid1.Col, FCConvert.ToString(clsTemp.Get_Fields_String("header")));
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strCode = FCConvert.ToString(clsTemp.Get_Fields("code"));
					if (strCode == "0")
					{
						strData = "0, ";
					}
					else
					{
						strData = strCode;
						// strDesc = clsTemp.Fields("description")
						strDesc = grid2.TextMatrix(grid2.FindRow(strCode, -1, 0), 1);
						strData += "," + strDesc;
						// strData = strData & "," & clsTemp.Fields("fieldname")
						// strData = strData & "," & clsTemp.Fields("tablename")
					}
					strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("justification"));
					if (strTemp.Length > 0)
					{
						strData += "," + strTemp;
					}
					else
					{
						strData += ",L";
					}
					strData += "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int16("TotalOption")));
					Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col, strData);
					clsTemp.MoveNext();
				}
				if (strTemp == "L")
				{
					cmbJust.Text = "Left";
				}
				else
				{
					cmbJust.Text = "Right";
				}
				txtDescription.Text = strDesc;
				txtCode.Text = strCode;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "in FillGrid1", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupOrderList()
		{
			// fill with fields from master table you can order by
			clsDRWrapper clsLoad = new clsDRWrapper();
			int x;
			lstOrderBy.Clear();
			lstOrderBy.AddItem("Account");
			lstOrderBy.AddItem("Owner Name");
			lstOrderBy.AddItem("Second Owner Name");
			lstOrderBy.AddItem("Map Lot");
			lstOrderBy.AddItem("Location");
			lstOrderBy.AddItem("Zip code");
			lstOrderBy.AddItem("Reference 1");
			lstOrderBy.AddItem("Reference 2");
			lstOrderBy.AddItem("Land Code");
			lstOrderBy.AddItem("Tran Code");
			lstOrderBy.AddItem("Building Code");
			lstOrderBy.AddItem("Neighborhood Code");
			lstOrderBy.AddItem("Zone");
            //lstOrderBy.Columns[0].Width = FCConvert.ToInt32((lstOrderBy.Width * 0.96));
            lstOrderBy.GridLineStyle = GridLineStyle.None;
            clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1330 ", modGlobalVariables.strREDatabase);
			if (!clsLoad.EndOfFile())
			{
				if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"))) != string.Empty && Strings.Left(clsLoad.Get_Fields_String("cldesc") + ".", 1) != ".")
				{
					lstOrderBy.AddItem(Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"))));
				}
				else
				{
					lstOrderBy.AddItem("Open 1");
				}
			}
			clsLoad.OpenRecordset("select * from costrecord where crecordnumber = 1340", modGlobalVariables.strREDatabase);
			if (!clsLoad.EndOfFile())
			{
				if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"))) != string.Empty && Strings.Left(clsLoad.Get_Fields_String("cldesc") + ".", 1) != ".")
				{
					lstOrderBy.AddItem(Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"))));
				}
				else
				{
					lstOrderBy.AddItem("Open 2");
				}
			}
			for (x = 0; x <= lstOrderBy.Items.Count - 1; x++)
			{
				lstOrderBy.ItemData(x, x);
			}
			// x
		}

		private string GetOrderbyClause()
		{
			string GetOrderbyClause = string.Empty;
			int x;
			string strOrder;
			try
			{
				// On Error GoTo ErrorHandler
				strOrder = string.Empty;
				for (x = 0; x <= lstOrderBy.Items.Count - 1; x++)
				{
					if (lstOrderBy.Selected(x))
					{
						switch (lstOrderBy.ItemData(x))
						{
							case CNSTORDERACCOUNT:
								{
									strOrder += " master.rsaccount,master.rscard,";
									break;
								}
							case CNSTORDERBLDGCODE:
								{
									strOrder += " master.ribldgcode,";
									break;
								}
							case CNSTORDERLANDCODE:
								{
									strOrder += " master.rilandcode,";
									break;
								}
							case CNSTORDERLOCATION:
								{
									strOrder += " master.rslocstreet,val(master.rslocnumalph & ''),";
									break;
								}
							case CNSTORDERMAPLOT:
								{
									strOrder += " master.rsmaplot,";
									break;
								}
							case CNSTORDERNAME:
								{
									strOrder += " master.rsname,";
									break;
								}
							case CNSTORDERNEIGHBORHOOD:
								{
									strOrder += " master.pineighborhood,";
									break;
								}
							case CNSTORDEROPEN1:
								{
									strOrder += " master.piopen1,";
									break;
								}
							case CNSTORDEROPEN2:
								{
									strOrder += " master.piopen2,";
									break;
								}
							case CNSTORDERREF1:
								{
									strOrder += " master.rsref1,";
									break;
								}
							case CNSTORDERREF2:
								{
									strOrder += " master.rsref2,";
									break;
								}
							case CNSTORDERSECOWNER:
								{
									strOrder += " master.rssecowner,";
									break;
								}
							case CNSTORDERTRANCODE:
								{
									strOrder += " master.ritrancode,";
									break;
								}
							case CNSTORDERZIP:
								{
									strOrder += " master.rszip,";
									break;
								}
							case CNSTORDERZONE:
								{
									strOrder += " master.pizone,";
									break;
								}
						}
						//end switch
					}
				}
				// x
				if (strOrder != string.Empty)
				{
					strOrder = Strings.Mid(strOrder, 1, strOrder.Length - 1);
					strOrder = " order by " + strOrder;
				}
				GetOrderbyClause = strOrder;
				return GetOrderbyClause;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetOrderByClause", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetOrderbyClause;
		}

		private void optTotal_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			string strTemp = string.Empty;
			// vbPorter upgrade warning: strAry As string()	OnWrite(string(), short())
			string[] strAry = null;
			if (Grid1.Row < 1)
				return;
			if (Grid1.Col < 0)
				return;
			strTemp = FCConvert.ToString(Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col));
			//FC:FINAL:DDU:#i1170 - format string if null
			if (string.IsNullOrEmpty(strTemp))
			{
				strTemp = ",,,";
			}
			strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
			if (strTemp.Length > 0)
			{
				if (cmbTotal.Text == "None")
				{
					strAry[3] = FCConvert.ToString(0);
				}
				else if (cmbTotal.Text == "Total")
				{
					strAry[3] = FCConvert.ToString(1);
				}
				else
				{
					strAry[3] = FCConvert.ToString(2);
				}
				strTemp = strAry[0] + "," + strAry[1] + "," + strAry[2] + "," + strAry[3];
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col, strTemp);
			}
			else
			{
				strTemp = ",,L";
				if (cmbTotal.Text == "None")
				{
					strTemp += ",0";
				}
				else if (cmbTotal.Text == "Total")
				{
					strTemp += ",1";
				}
				else
				{
					strTemp += ",2";
				}
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpData, Grid1.Row, Grid1.Col, strTemp);
			}
		}

		private void optTotal_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbTotal.SelectedIndex;
			optTotal_CheckedChanged(index, sender, e);
		}
	}
}
