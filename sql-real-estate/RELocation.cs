﻿namespace TWRE0000
{
    public class RELocation
    {
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }

        public string Apartment { get; set; }

        public override string ToString()
        {
            return ((StreetName.Trim() + " " + Apartment.Trim()).Trim() + " " + StreetName).Trim();
        }
    }
}