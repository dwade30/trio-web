﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptValuation.
	/// </summary>
	partial class rptValuation
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptValuation));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumCards = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSecName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.subSaleData = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubMisc = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.subLandDesc = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.subDwellComm = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.subOutBuilding = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.lblLandTot = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBldgTot = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageBreak1 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
			this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Image2 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Image3 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Image4 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Image5 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Image6 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.lblRatio = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRatio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumCards)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLandTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBldgTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRatio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRatio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.subSaleData,
				this.SubMisc,
				this.subLandDesc,
				this.subDwellComm,
				this.subOutBuilding,
				this.lblLandTot,
				this.txtLand,
				this.lblBldgTot,
				this.txtBldg,
				this.Label9,
				this.txtTotal,
				this.Line2,
				this.PageBreak1,
				this.Image1,
				this.Image2,
				this.Image3,
				this.Image4,
				this.Image5,
				this.Image6,
				this.lblRatio,
				this.txtRatio
			});
			this.Detail.Height = 0.5104167F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1
			});
			this.ReportFooter.Height = 0.09375F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtName,
				this.Label1,
				this.Label2,
				this.txtMapLot,
				this.Label3,
				this.txtAccount,
				this.Label4,
				this.txtCard,
				this.Label5,
				this.txtLocation,
				this.Label6,
				this.txtNumCards,
				this.Line1,
				this.Field1,
				this.txtDate,
				this.txtMuni,
				this.txtSecName,
				this.txtPage
			});
			this.PageHeader.Height = 0.65625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.19625F;
			this.txtName.Left = 0.5F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = null;
			this.txtName.Top = 0.15625F;
			this.txtName.Width = 3.5F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1770833F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'";
			this.Label1.Text = "Name:";
			this.Label1.Top = 0.15625F;
			this.Label1.Width = 0.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19625F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 4F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label2.Text = "Map/Lot:";
			this.Label2.Top = 0.3020833F;
			this.Label2.Width = 0.75F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.19F;
			this.txtMapLot.Left = 4.8125F;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0.3020833F;
			this.txtMapLot.Width = 2.625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'";
			this.Label3.Text = "Account:";
			this.Label3.Top = 0.4722222F;
			this.Label3.Width = 0.8333333F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0.9166667F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.4722222F;
			this.txtAccount.Width = 0.5833333F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.666667F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'";
			this.Label4.Text = "Card:";
			this.Label4.Top = 0.4722222F;
			this.Label4.Width = 0.5F;
			// 
			// txtCard
			// 
			this.txtCard.Height = 0.19F;
			this.txtCard.Left = 2.166667F;
			this.txtCard.Name = "txtCard";
			this.txtCard.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCard.Text = null;
			this.txtCard.Top = 0.4722222F;
			this.txtCard.Width = 0.4166667F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.17625F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.9375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label5.Text = "Location:";
			this.Label5.Top = 0.4583333F;
			this.Label5.Width = 0.8125F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.19F;
			this.txtLocation.Left = 4.8125F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLocation.Text = "Field2";
			this.txtLocation.Top = 0.4583333F;
			this.txtLocation.Width = 2.625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 2.666667F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'";
			this.Label6.Text = "of";
			this.Label6.Top = 0.4722222F;
			this.Label6.Width = 0.25F;
			// 
			// txtNumCards
			// 
			this.txtNumCards.Height = 0.19F;
			this.txtNumCards.Left = 2.916667F;
			this.txtNumCards.Name = "txtNumCards";
			this.txtNumCards.Style = "font-family: \'Tahoma\'";
			this.txtNumCards.Text = null;
			this.txtNumCards.Top = 0.4722222F;
			this.txtNumCards.Width = 0.4166667F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.08333334F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.625F;
			this.Line1.Width = 7.333333F;
			this.Line1.X1 = 0.08333334F;
			this.Line1.X2 = 7.416667F;
			this.Line1.Y1 = 0.625F;
			this.Line1.Y2 = 0.625F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.19F;
			this.Field1.Left = 2.25F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field1.Text = "Valuation Report";
			this.Field1.Top = 0F;
			this.Field1.Width = 3F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 5.9375F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.5F;
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0F;
			this.txtMuni.Width = 1.625F;
			// 
			// txtSecName
			// 
			this.txtSecName.CanGrow = false;
			this.txtSecName.Height = 0.19F;
			this.txtSecName.Left = 0.5F;
			this.txtSecName.Name = "txtSecName";
			this.txtSecName.Style = "font-family: \'Tahoma\'";
			this.txtSecName.Text = null;
			this.txtSecName.Top = 0.3125F;
			this.txtSecName.Width = 3.416667F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 5.927083F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = "Field2";
			this.txtPage.Top = 0.1458333F;
			this.txtPage.Width = 1.510417F;
			// 
			// subSaleData
			// 
			this.subSaleData.CloseBorder = false;
			this.subSaleData.Height = 0.0625F;
			this.subSaleData.Left = 4.75F;
			this.subSaleData.Name = "subSaleData";
			this.subSaleData.Report = null;
			this.subSaleData.Top = 0F;
			this.subSaleData.Width = 2.709028F;
			// 
			// SubMisc
			// 
			this.SubMisc.CloseBorder = false;
			this.SubMisc.Height = 0.0625F;
			this.SubMisc.Left = 0F;
			this.SubMisc.Name = "SubMisc";
			this.SubMisc.Report = null;
			this.SubMisc.Top = 0F;
			this.SubMisc.Width = 4.5625F;
			// 
			// subLandDesc
			// 
			this.subLandDesc.CanShrink = false;
			this.subLandDesc.CloseBorder = false;
			this.subLandDesc.Height = 0.0625F;
			this.subLandDesc.Left = 0.006944444F;
			this.subLandDesc.Name = "subLandDesc";
			this.subLandDesc.Report = null;
			this.subLandDesc.Top = 0.0625F;
			this.subLandDesc.Width = 7.368055F;
			// 
			// subDwellComm
			// 
			this.subDwellComm.CanShrink = false;
			this.subDwellComm.CloseBorder = false;
			this.subDwellComm.Height = 0.0625F;
			this.subDwellComm.Left = 0F;
			this.subDwellComm.Name = "subDwellComm";
			this.subDwellComm.Report = null;
			this.subDwellComm.Top = 0.125F;
			this.subDwellComm.Width = 7.4375F;
			// 
			// subOutBuilding
			// 
			this.subOutBuilding.CanShrink = false;
			this.subOutBuilding.CloseBorder = false;
			this.subOutBuilding.Height = 0.09375F;
			this.subOutBuilding.Left = 0F;
			this.subOutBuilding.Name = "subOutBuilding";
			this.subOutBuilding.Report = null;
			this.subOutBuilding.Top = 0.1875F;
			this.subOutBuilding.Width = 7.4375F;
			// 
			// lblLandTot
			// 
			this.lblLandTot.Height = 0.19625F;
			this.lblLandTot.HyperLink = null;
			this.lblLandTot.Left = 1.3125F;
			this.lblLandTot.MultiLine = false;
			this.lblLandTot.Name = "lblLandTot";
			this.lblLandTot.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblLandTot.Tag = "bold";
			this.lblLandTot.Text = "Accpt Land";
			this.lblLandTot.Top = 0.34375F;
			this.lblLandTot.Width = 0.9375F;
			// 
			// txtLand
			// 
			this.txtLand.CanGrow = false;
			this.txtLand.Height = 0.19625F;
			this.txtLand.Left = 2.3125F;
			this.txtLand.MultiLine = false;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLand.Tag = "text";
			this.txtLand.Text = null;
			this.txtLand.Top = 0.34375F;
			this.txtLand.Width = 1F;
			// 
			// lblBldgTot
			// 
			this.lblBldgTot.Height = 0.19625F;
			this.lblBldgTot.HyperLink = null;
			this.lblBldgTot.Left = 3.375F;
			this.lblBldgTot.MultiLine = false;
			this.lblBldgTot.Name = "lblBldgTot";
			this.lblBldgTot.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblBldgTot.Tag = "bold";
			this.lblBldgTot.Text = "Accepted Bldg";
			this.lblBldgTot.Top = 0.34375F;
			this.lblBldgTot.Width = 1.3125F;
			// 
			// txtBldg
			// 
			this.txtBldg.CanGrow = false;
			this.txtBldg.Height = 0.19625F;
			this.txtBldg.Left = 4.75F;
			this.txtBldg.MultiLine = false;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBldg.Tag = "text";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0.34375F;
			this.txtBldg.Width = 1F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19625F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 5.8125F;
			this.Label9.MultiLine = false;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label9.Tag = "bold";
			this.Label9.Text = "Total";
			this.Label9.Top = 0.34375F;
			this.Label9.Width = 0.5625F;
			// 
			// txtTotal
			// 
			this.txtTotal.CanGrow = false;
			this.txtTotal.Height = 0.19625F;
			this.txtTotal.Left = 6.4375F;
			this.txtTotal.MultiLine = false;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotal.Tag = "text";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0.34375F;
			this.txtTotal.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.0625F;
			this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.3125F;
			this.Line2.Width = 7.375F;
			this.Line2.X1 = 0.0625F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 0.3125F;
			this.Line2.Y2 = 0.3125F;
			// 
			// PageBreak1
			// 
			this.PageBreak1.Enabled = false;
			this.PageBreak1.Height = 0.05555556F;
			this.PageBreak1.Left = 0F;
			this.PageBreak1.Name = "PageBreak1";
			this.PageBreak1.Size = new System.Drawing.SizeF(7.493055F, 0.05555556F);
			this.PageBreak1.Top = 0.5319445F;
			this.PageBreak1.Width = 7.493055F;
			// 
			// Image1
			// 
			this.Image1.Height = 2F;
			this.Image1.HyperLink = null;
			this.Image1.ImageData = null;
			this.Image1.Left = 0.375F;
			this.Image1.LineWeight = 1F;
			this.Image1.Name = "Image1";
			this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.Image1.Top = 0.625F;
			this.Image1.Width = 3F;
			// 
			// Image2
			// 
			this.Image2.Height = 2F;
			this.Image2.HyperLink = null;
			this.Image2.ImageData = null;
			this.Image2.Left = 3.9375F;
			this.Image2.LineWeight = 1F;
			this.Image2.Name = "Image2";
			this.Image2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.Image2.Top = 0.625F;
			this.Image2.Width = 3F;
			// 
			// Image3
			// 
			this.Image3.Height = 2F;
			this.Image3.HyperLink = null;
			this.Image3.ImageData = null;
			this.Image3.Left = 0.375F;
			this.Image3.LineWeight = 1F;
			this.Image3.Name = "Image3";
			this.Image3.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.Image3.Top = 2.875F;
			this.Image3.Width = 3F;
			// 
			// Image4
			// 
			this.Image4.Height = 2F;
			this.Image4.HyperLink = null;
			this.Image4.ImageData = null;
			this.Image4.Left = 3.9375F;
			this.Image4.LineWeight = 1F;
			this.Image4.Name = "Image4";
			this.Image4.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.Image4.Top = 2.875F;
			this.Image4.Width = 3F;
			// 
			// Image5
			// 
			this.Image5.Height = 2F;
			this.Image5.HyperLink = null;
			this.Image5.ImageData = null;
			this.Image5.Left = 0.375F;
			this.Image5.LineWeight = 1F;
			this.Image5.Name = "Image5";
			this.Image5.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.Image5.Top = 5.125F;
			this.Image5.Width = 3F;
			// 
			// Image6
			// 
			this.Image6.Height = 2F;
			this.Image6.HyperLink = null;
			this.Image6.ImageData = null;
			this.Image6.Left = 3.9375F;
			this.Image6.LineWeight = 1F;
			this.Image6.Name = "Image6";
			this.Image6.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.Image6.Top = 5.125F;
			this.Image6.Width = 3F;
			// 
			// lblRatio
			// 
			this.lblRatio.Height = 0.19625F;
			this.lblRatio.HyperLink = null;
			this.lblRatio.Left = 0F;
			this.lblRatio.MultiLine = false;
			this.lblRatio.Name = "lblRatio";
			this.lblRatio.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblRatio.Tag = "bold";
			this.lblRatio.Text = "Ratio";
			this.lblRatio.Top = 0.34375F;
			this.lblRatio.Visible = false;
			this.lblRatio.Width = 0.4375F;
			// 
			// txtRatio
			// 
			this.txtRatio.CanGrow = false;
			this.txtRatio.Height = 0.19625F;
			this.txtRatio.Left = 0.5F;
			this.txtRatio.MultiLine = false;
			this.txtRatio.Name = "txtRatio";
			this.txtRatio.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtRatio.Tag = "text";
			this.txtRatio.Text = "100%";
			this.txtRatio.Top = 0.34375F;
			this.txtRatio.Visible = false;
			this.txtRatio.Width = 0.5F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0F;
			this.SubReport1.Width = 7.4375F;
			// 
			// rptValuation
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.493055F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumCards)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLandTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBldgTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRatio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRatio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subSaleData;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubMisc;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subLandDesc;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subDwellComm;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subOutBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLandTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBldgTot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageBreak PageBreak1;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image2;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image3;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image4;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image5;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRatio;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRatio;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCard;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumCards;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
