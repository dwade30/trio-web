﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmNewValuationAnalysis.
	/// </summary>
	public partial class frmNewValuationAnalysis : BaseForm
	{
		public frmNewValuationAnalysis()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmNewValuationAnalysis InstancePtr
		{
			get
			{
				return (frmNewValuationAnalysis)Sys.GetInstance(typeof(frmNewValuationAnalysis));
			}
		}

		protected frmNewValuationAnalysis _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTCHGTYPEDELETE = -1;
		// In case there is not change and we need to delete the row
		const int CNSTCHGTYPEUNKNOWN = 0;
		const int CNSTCHGTYPENEW = 1;
		const int CNSTCHGTYPEPREVTOTEXEMPT = 2;
		const int CNSTCHGTYPEOVERRIDE = 3;
		const int CNSTCHGTYPEDATACHANGE = 4;
		const int CNSTCHGTYPEPREVOVERRIDE = 5;
		const int CNSTGRIDCOLACCOUNT = 0;
		const int CNSTGRIDCOLMAPLOT = 1;
		const int CNSTGRIDCOLPREVLAND = 2;
		const int CNSTGRIDCOLPREVBLDG = 3;
		const int CNSTGRIDCOLPREVEXEMPTION = 4;
		const int CNSTGRIDCOLCURLAND = 5;
		const int CNSTGRIDCOLCURBLDG = 6;
		const int CNSTGRIDCOLCUREXEMPTION = 7;
		const int CNSTGRIDCOLNEWVALUATION = 8;
		const int CNSTGRIDCOLNAME = 9;
		const int CNSTGRIDDETAILCOLACCOUNT = 0;
		const int CNSTGRIDDETAILCOLDESCRIPTION = 1;
		const int CNSTGRIDDETAILCOLDETAIL = 2;
		const int CNSTGRIDDETAILCOLPREVIOUS = 3;
		const int CNSTGRIDDETAILCOLCURRENT = 4;
		const int CNSTGRIDDETAILCOLUSE = 5;
		const int CNSTGRIDDETAILCOLVALUATION = 6;
		const int CNSTGRIDDETAILCOLROWNUM = 7;
		const int CNSTOUTARYCOLTYPE = 0;
		const int CNSTOUTARYCOLYEAR = 1;
		const int CNSTOUTARYCOLUNITS = 2;
		const int CNSTOUTARYCOLUSESOUND = 3;
		const int CNSTOUTARYCOLVALUE = 4;
		const int CNSTLANDARYCOLTYPE = 0;
		const int CNSTLANDARYCOLUNITS = 1;
		const int CNSTLANDARYCOLVALUE = 2;
		const int CNSTLANDARYCOLACRES = 3;
		int lngLatestYear;
		int lngPreviousYear;
		bool boolRecalcYear;
		bool boolRecalcPrevYear;
		int lngDeprYearToUse;
		int lngMHDepYearToUse;
		int lngCDepYearToUse;
		bool boolAutoChoose;
		bool boolShowUnexplainedAccounts;
		string strWhere;
		string strOrderBy;
		double dblTotalCurVal;
		double dblTotalOldVal;
		int lngTotalCount;
		double dblTotalNewVal;
		double dblTempNewVal;
		// vbPorter upgrade warning: lngDeprYear As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: lngMDepYear As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: lngCDepYear As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intRangeOption As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intRangeType As short	OnWriteFCConvert.ToInt32(
		public void Init(ref int lngYear, ref int lngPrevYear, ref bool ReCalcYear, ref bool ReCalcPrevYear, int lngDeprYear, int lngMDepYear, int lngCDepYear, ref int intRangeOption, ref int intRangeType, ref string strMin, ref string strMax, ref bool boolAutoDetermine, bool boolShowUnexplained)
		{
			// accepts the two years to compare and whether to recalculate them or not.  Also accepts the years to use for the calculation
			// Accepts whether it is a range,all etc.  Accepts type of range (if applicable) and the min and max of the range.
			// also accepts whether to show all valuation changes or to determine which accounts are eligible
			clsDRWrapper clsLoad = new clsDRWrapper();
			lngLatestYear = lngYear;
			lngPreviousYear = lngPrevYear;
			boolRecalcYear = ReCalcYear;
			boolRecalcPrevYear = ReCalcPrevYear;
			lngDeprYearToUse = lngDeprYear;
			lngMHDepYearToUse = lngMDepYear;
			lngCDepYearToUse = lngCDepYear;
			boolAutoChoose = boolAutoDetermine;
			boolShowUnexplainedAccounts = boolShowUnexplained;
			strWhere = " where not rsdeleted = 1 ";
			switch (intRangeOption)
			{
				case 0:
					{
						// case all
						break;
					}
				case 1:
					{
						// range
						switch (intRangeType)
						{
							case 0:
								{
									// account
									strWhere += " and rsaccount between " + FCConvert.ToString(Conversion.Val(strMin)) + " and " + FCConvert.ToString(Conversion.Val(strMax));
									break;
								}
							case 1:
								{
									// maplot
									strWhere += " and rsmaplot between '" + strMin + "' and '" + strMax + "' ";
									break;
								}
							case 2:
								{
									// location
									strWhere += " and rslocstreet between '" + strMin + "' and '" + strMax + "' ";
									break;
								}
						}
						//end switch
						break;
					}
				case 2:
					{
						// from extract
						// currently not supporting this since you can't inner join on tables from different databases
						break;
					}
			}
			//end switch
			strOrderBy = " order by rsaccount,rscard ";
			modGlobalVariables.Statics.strSQL = "select top 1 * from master " + strWhere + " and commitmentyear = " + FCConvert.ToString(lngLatestYear);
			clsLoad.OpenRecordset(modGlobalVariables.Statics.strSQL, modGlobalVariables.Statics.strCommitDB);
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No records with the chosen options for commitment year " + FCConvert.ToString(lngLatestYear) + " were found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Unload();
				return;
			}
			modGlobalVariables.Statics.strSQL = "select top 1 * from master " + strWhere + " and commitmentyear = " + FCConvert.ToString(lngPreviousYear);
			clsLoad.OpenRecordset(modGlobalVariables.Statics.strSQL, modGlobalVariables.Statics.strCommitDB);
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No records with the chosen options for commitment year " + FCConvert.ToString(lngPreviousYear) + " were found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Unload();
				return;
			}
			this.Show(App.MainForm);
		}

		private void ReCalc(ref int lngYear)
		{
			// recalc for commitment year lngyear
			string strSQL;
			int NumRecords;
			int intcnum = 0;
			int lngLand;
			int lngbldg;
			int lngBillLand;
			int lngBillBldg;
			clsDRWrapper clsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				// set customized options
				modGlobalVariables.Statics.CustomizedInfo.MHDepreciationYear = FCConvert.ToInt16(lngMHDepYearToUse);
				modGlobalVariables.Statics.CustomizedInfo.DepreciationYear = FCConvert.ToInt16(lngDeprYearToUse);
				modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear = FCConvert.ToInt16(lngCDepYearToUse);
				modGlobalVariables.Statics.boolUseArrays = true;
				modGlobalRoutines.Check_Arrays();
				modSpeedCalc.Statics.boolCalcErrors = false;
				modSpeedCalc.Statics.CalcLog = "";
				modGlobalVariables.Statics.boolUpdateWhenCalculate = true;
				Frmassessmentprogress.InstancePtr.Initialize("Only some records were calculated." + "\r\n" + "Cancel anyway?");
				Frmassessmentprogress.InstancePtr.Label2.Text = "Calculating Accounts for " + FCConvert.ToString(lngYear) + ". Please Wait.";
				Frmassessmentprogress.InstancePtr.Text = "Calculating";
				Frmassessmentprogress.InstancePtr.cmdcancexempt.Visible = true;
				Frmassessmentprogress.InstancePtr.Show();
				strSQL = "select * from master " + strWhere + " and commitmentyear = " + FCConvert.ToString(lngYear);
				modDataTypes.Statics.MR.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				modDataTypes.Statics.MR.MoveLast();
				modDataTypes.Statics.MR.MoveFirst();
				modDataTypes.Statics.MR.Edit();
				NumRecords = modDataTypes.Statics.MR.RecordCount();
				Frmassessmentprogress.InstancePtr.ProgressBar1.Maximum = NumRecords + 1;
				modGlobalVariables.Statics.boolfromcalcandassessment = true;
				modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
				modGlobalVariables.Statics.gintMaxAccounts = FCConvert.ToInt32(modGlobalRoutines.GetMaxAccounts());
				clsTemp.OpenRecordset("select count(rsaccount) as thecount from master where rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + " and commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				modGNBas.Statics.gintMaxCards = FCConvert.ToInt32(clsTemp.Get_Fields("thecount"));
				modGlobalVariables.Statics.boolfromcalcandassessment = false;
				// Set MR = dbTemp.OpenRecordset("select * from master where not rsdeleted = 1 order by rsaccount,rscard",strredatabase)
				modDataTypes.Statics.OUT.OpenRecordset("select * from outbuilding where commitmentyear = " + FCConvert.ToString(lngYear) + " order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				modDataTypes.Statics.CMR.OpenRecordset("select * from commercial where commitmentyear = " + FCConvert.ToString(lngYear) + " order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				modDataTypes.Statics.DWL.OpenRecordset("select * from dwelling where commitmentyear = " + FCConvert.ToString(lngYear) + " order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				modSpeedCalc.REAS03_1075_GET_PARAM_RECORD();
				Array.Resize(ref modSpeedCalc.Statics.SummaryList, NumRecords + 1 + 1);
				modSpeedCalc.Statics.SummaryListIndex = 0;
				modProperty.Statics.RUNTYPE = "P";
				modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
				// everything is initialized, now do calculation
				while (!modDataTypes.Statics.MR.EndOfFile() && Frmassessmentprogress.InstancePtr.stillcalc)
				{
					modDataTypes.Statics.MR.Edit();
					if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
					{
						modREASValuations.Statics.BBPhysicalPercent = 0;
						modREASValuations.Statics.HoldDwellingEconomic = 0;
					}
					modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
					modGlobalVariables.Statics.boolfromcalcandassessment = true;
					modSpeedCalc.CalcCard();
					modGlobalVariables.Statics.boolfromcalcandassessment = false;
					intcnum = modGlobalVariables.Statics.intCurrentCard;
					lngLand = modProperty.Statics.lngCorrelatedLand[intcnum];
					lngbldg = modProperty.Statics.lngCorrelatedBldg[intcnum];
					lngBillLand = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + "")));
					lngBillBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval") + "")));
					//Application.DoEvents();
					if (modDataTypes.Statics.MR.EndOfFile() == false)
					{
						Frmassessmentprogress.InstancePtr.ProgressBar1.Value = modDataTypes.Statics.MR.AbsolutePosition();
						Frmassessmentprogress.InstancePtr.lblpercentdone.Text = Strings.Format(((FCConvert.ToDouble(modDataTypes.Statics.MR.AbsolutePosition()) / NumRecords) * 100), "##0.00") + "%";
					}
					else
					{
						Frmassessmentprogress.InstancePtr.ProgressBar1.Value = NumRecords;
						Frmassessmentprogress.InstancePtr.lblpercentdone.Text = "100%";
					}
					modDataTypes.Statics.MR.MoveNext();
				}
				modGlobalVariables.Statics.boolUpdateWhenCalculate = false;
				Frmassessmentprogress.InstancePtr.Unload();
				modGlobalVariables.Statics.boolUseArrays = false;
				modGlobalVariables.Statics.LandTypes.UnloadLandTypes();
				modSpeedCalc.SaveSummary(lngYear);
				if (modSpeedCalc.Statics.boolCalcErrors)
				{
					MessageBox.Show("There were errors calculating.  Following is a list.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
					//! Load frmShowResults;
					frmShowResults.InstancePtr.Init(ref modSpeedCalc.Statics.CalcLog);
					frmShowResults.InstancePtr.lblScreenTitle.Text = "Calculation Errors Log";
					frmShowResults.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				}
				modSpeedCalc.Statics.boolCalcErrors = false;
				modSpeedCalc.Statics.CalcLog = "";
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				Frmassessmentprogress.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Recalc", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadGrid()
		{
			string strSQL;
			clsDRWrapper clsOld = new clsDRWrapper();
			clsDRWrapper clsCurrent = new clsDRWrapper();
			int lngRow = 0;
			string strLandField = "";
			string strBldgField = "";
			int lngNewLand = 0;
			int lngNewBldg = 0;
			int lngOldLand = 0;
			int lngOldBldg = 0;
			int lngTemp;
			int lngTemp2;
			int lngOldExempt = 0;
			int lngNewExempt = 0;
			bool boolAddRecord = false;
			bool boolFoundRecord = false;
			int lngOldAssessment = 0;
			int lngCardNewLand = 0;
			int lngCardNewBldg = 0;
			int lngCardNewExempt;
			int lngCardOldLand = 0;
			int lngCardOldBldg = 0;
			int lngCardOldExempt;
			// vbPorter upgrade warning: lngCurrNewValuation As int	OnWrite(int, double)
			int lngCurrNewValuation = 0;
			// vbPorter upgrade warning: lngCurrNewBldg As int	OnWrite(int, double)
			int lngCurrNewBldg = 0;
			int x = 0;
			int[,] lngAryNewOut = new int[10 + 1, 5 + 1];
			// type,year,units,use sound value,value
			int[,] lngAryOldOut = new int[10 + 1, 5 + 1];
			// vbPorter upgrade warning: lngAryNewLand As string	OnWrite(string, short)
			string[,] lngAryNewLand = new string[7 + 1, 4 + 1];
			// vbPorter upgrade warning: lngAryOldLand As string	OnWrite(string, short)
			string[,] lngAryOldLand = new string[7 + 1, 4 + 1];
			// Dim clsOldTemp As New clsDRWrapper
			// Dim clsNewTemp As New clsDRWrapper
			clsDRWrapper clsNewSummary = new clsDRWrapper();
			clsDRWrapper clsOldSummary = new clsDRWrapper();
			clsDRWrapper clsNewOut = new clsDRWrapper();
			clsDRWrapper clsOldOut = new clsDRWrapper();
			clsDRWrapper clsNewDwell = new clsDRWrapper();
			clsDRWrapper clsOldDwell = new clsDRWrapper();
			clsDRWrapper clsNewComm = new clsDRWrapper();
			clsDRWrapper clsOldComm = new clsDRWrapper();
			int intOld;
			int intCurrent = 0;
			bool boolTemp = false;
			bool boolNoOldAccount = false;
			int lngDetailRow = 0;
			string strDetail = "";
			bool boolUsedUp = false;
			bool boolAddToVal = false;
			int lngSubRow = 0;
			// vbPorter upgrade warning: aryFields As Variant --> As string
			string[] aryFields = new string[1 + 1];
			aryFields[0] = "rsaccount";
			aryFields[1] = "rscard";
			object[] aryValues = new object[1 + 1];
			try
			{
				// On Error GoTo ErrorHandler
				if (boolRecalcYear)
				{
					ReCalc(ref lngLatestYear);
				}
				if (boolRecalcPrevYear)
				{
					ReCalc(ref lngPreviousYear);
				}
				// reset customized info after recalc
				modGlobalRoutines.LoadCustomizedInfo();
				strSQL = "select sum(lastlandval) as landsum,sum(rllandval) as calcland,sum(lastbldgval) as bldgsum,sum(rlbldgval) as calcbldg,sum(rlexemption) as exemptsum,rsaccount from master " + strWhere + " and commitmentyear = " + FCConvert.ToString(lngLatestYear) + " group by rsaccount having sum(lastlandval) + sum(lastbldgval) - sum(rlexemption) > 0 order by rsaccount ";
				clsCurrent.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
				strSQL = "select sum(lastlandval) as landsum,sum(rllandval) as calcland,sum(lastbldgval) as bldgsum,sum(rlbldgval) as calcbldg,sum(rlexemption) as exemptsum,rsaccount from master " + strWhere + " and commitmentyear = " + FCConvert.ToString(lngPreviousYear) + " group by rsaccount order by rsaccount ";
				// having sum(lastlandval) + sum(lastbldgval) - sum(rlexemption) > 0 order by rsaccount "
				clsOld.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
				frmWait.InstancePtr.Init("Comparing commitment data", true);
				Grid.Rows = 1;
				// Now compare
				lngTotalCount = 0;
				dblTotalCurVal = 0;
				dblTotalOldVal = 0;
				dblTotalNewVal = 0;
				dblTempNewVal = 0;
				while (!clsCurrent.EndOfFile())
				{
					// if no match with clsold then this is a new account so is all new valuation
					frmWait.InstancePtr.lblMessage.Text = "Loading account " + clsCurrent.Get_Fields_Int32("rsaccount");
					frmWait.InstancePtr.lblMessage.Refresh();
					//Application.DoEvents();
					if (!boolRecalcYear)
					{
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						lngNewLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCurrent.Get_Fields("landsum"))));
						// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
						lngNewBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCurrent.Get_Fields("bldgsum"))));
						// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
						lngNewExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCurrent.Get_Fields("exemptsum"))));
					}
					else
					{
						// TODO Get_Fields: Field [calcland] not found!! (maybe it is an alias?)
						lngNewLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCurrent.Get_Fields("calcland"))));
						// TODO Get_Fields: Field [calcbldg] not found!! (maybe it is an alias?)
						lngNewBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCurrent.Get_Fields("calcbldg"))));
						// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
						lngNewExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCurrent.Get_Fields("exemptsum"))));
					}
					boolAddRecord = false;
					boolFoundRecord = false;
					// Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT) = clsCurrent.Fields("rsmaplot")
					if (!clsOld.EndOfFile())
					{
						if (clsOld.Get_Fields_Int32("rsaccount") == clsCurrent.Get_Fields_Int32("rsaccount"))
							boolFoundRecord = true;
					}
					if (!clsOld.FindNextRecord("rsaccount", clsCurrent.Get_Fields_Int32("rsaccount")) && !boolFoundRecord)
					{
						// totally new card.  Whole card is new valuation
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						boolAddRecord = true;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVBLDG, FCConvert.ToString(0));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVLAND, FCConvert.ToString(0));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVEXEMPTION, FCConvert.ToString(0));
						Grid.RowData(lngRow, CNSTCHGTYPENEW);
						GridSavedDetail.Rows += 1;
						lngDetailRow = GridSavedDetail.Rows - 1;
						GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields_Int32("rsaccount"))));
						GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "New Account");
						GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
						GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngNewLand + lngNewBldg));
						GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
						GridSavedDetail.IsSubtotal(lngDetailRow, true);
						lngOldLand = 0;
						lngOldBldg = 0;
						lngOldExempt = 0;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNEWVALUATION, Strings.Format(lngNewLand + lngNewBldg, "#,###,###,###,##0"));
						dblTempNewVal += lngNewLand + lngNewBldg;
						// ?count exemption or not?
					}
					else
					{
						// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
						lngOldBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOld.Get_Fields("bldgsum"))));
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						lngOldLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOld.Get_Fields("landsum"))));
						// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
						lngOldExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOld.Get_Fields("exemptsum"))));
						lngOldAssessment = lngOldBldg + lngOldLand - lngOldExempt;
						if (boolRecalcPrevYear)
						{
							// TODO Get_Fields: Field [calcland] not found!! (maybe it is an alias?)
							lngOldLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOld.Get_Fields("calcland"))));
							// TODO Get_Fields: Field [calcbldg] not found!! (maybe it is an alias?)
							lngOldBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOld.Get_Fields("calcbldg"))));
							// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
							lngOldExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOld.Get_Fields("exemptsum"))));
							if (lngOldAssessment > 0)
							{
								// if it was totally exempt then leave it that way
								lngOldAssessment = lngOldLand + lngOldBldg - lngOldExempt;
							}
						}
						if ((lngNewLand + lngNewBldg > lngOldBldg + lngOldLand) || ((lngOldAssessment == 0) && (lngNewLand + lngNewBldg - lngNewExempt > 0)))
						{
							boolAddRecord = true;
							Grid.Rows += 1;
							lngRow = Grid.Rows - 1;
							Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVBLDG, Strings.Format(lngOldBldg, "#,###,###,##0"));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVEXEMPTION, Strings.Format(lngOldExempt, "#,###,###,##0"));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVLAND, Strings.Format(lngOldLand, "#,###,###,##0"));
							Grid.RowData(lngRow, CNSTCHGTYPEUNKNOWN);
							// initialize it
							if (lngOldAssessment > 0 || (lngNewExempt == lngOldExempt && lngOldExempt == 0))
							{
								Grid.TextMatrix(lngRow, CNSTGRIDCOLNEWVALUATION, Strings.Format(lngNewLand + lngNewBldg - lngOldLand - lngOldBldg, "#,###,###,##0"));
								dblTempNewVal += lngNewLand + lngNewBldg - lngOldLand - lngOldBldg;
							}
							else
							{
								if (lngOldExempt > 0)
								{
									clsNewOut.OpenRecordset("select riexemptcd1,riexemptcd2,riexemptcd3 from master where commitmentyear = " + FCConvert.ToString(lngLatestYear) + " and rsaccount = " + FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields_Int32("rsaccount"))) + " and rscard = 1", modGlobalVariables.Statics.strCommitDB);
									clsOldOut.OpenRecordset("select riexemptcd1,riexemptcd2,riexemptcd3 from master where commitmentyear = " + FCConvert.ToString(lngPreviousYear) + " and rsaccount = " + FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields_Int32("rsaccount"))) + " and rscard = 1", modGlobalVariables.Statics.strCommitDB);
									if (Conversion.Val(clsNewOut.Get_Fields_Int32("riexemptcd1")) != Conversion.Val(clsOldOut.Get_Fields_Int32("riexemptcd1")) || Conversion.Val(clsNewOut.Get_Fields_Int32("riexemptcd2")) != Conversion.Val(clsOldOut.Get_Fields_Int32("riexemptcd2")) || Conversion.Val(clsNewOut.Get_Fields_Int32("riexemptcd3")) != Conversion.Val(clsOldOut.Get_Fields_Int32("riexemptcd3")))
									{
										Grid.RowData(lngRow, CNSTCHGTYPEPREVTOTEXEMPT);
										GridSavedDetail.Rows += 1;
										lngDetailRow = GridSavedDetail.Rows - 1;
										GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
										GridSavedDetail.IsSubtotal(lngDetailRow, true);
										GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
										GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Previously Totally Exempt");
										GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
										GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngNewLand + lngNewBldg));
									}
								}
								else
								{
								}
								Grid.TextMatrix(lngRow, CNSTGRIDCOLNEWVALUATION, Strings.Format(lngNewLand + lngNewBldg, "#,###,###,##0"));
								dblTempNewVal += lngNewLand + lngNewBldg;
							}
						}
					}
					if (boolAddRecord)
					{
						lngTotalCount += 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCURBLDG, Strings.Format(lngNewBldg, "#,###,###,##0"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCUREXEMPTION, Strings.Format(lngNewExempt, "#,###,###,##0"));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCURLAND, Strings.Format(lngNewLand, "#,###,###,##0"));
						dblTotalCurVal += lngNewBldg + lngNewLand;
						// - lngNewExempt
						dblTotalOldVal += lngOldLand + lngOldBldg;
					}
					// If lngOldAssessment > 0 Then
					// dblTotalOldVal = dblTotalOldVal + lngOldAssessment
					// End If
					clsCurrent.MoveNext();
				}
				if (boolAutoChoose && !modREMain.Statics.boolShortRealEstate)
				{
					strSQL = "select * from master where not rsdeleted = 1 and  commitmentyear = " + FCConvert.ToString(lngLatestYear) + " order by rsaccount,rscard";
					clsCurrent.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
					strSQL = "select * from master where not rsdeleted = 1 and  commitmentyear = " + FCConvert.ToString(lngPreviousYear) + " order by rsaccount,rscard";
					clsOld.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
					strSQL = "Select * from outbuilding where  commitmentyear = " + FCConvert.ToString(lngLatestYear) + " order by rsaccount,rscard";
					clsNewOut.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
					strSQL = "Select * from outbuilding where  commitmentyear = " + FCConvert.ToString(lngPreviousYear) + " order by rsaccount,rscard";
					clsOldOut.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
					strSQL = "select * from dwelling where commitmentyear = " + FCConvert.ToString(lngLatestYear) + " order by rsaccount,rscard";
					clsNewDwell.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
					strSQL = "select * from dwelling where commitmentyear = " + FCConvert.ToString(lngPreviousYear) + " order by rsaccount,rscard";
					clsOldDwell.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
					strSQL = "select * from commercial where commitmentyear = " + FCConvert.ToString(lngLatestYear) + " order by rsaccount,rscard";
					clsNewComm.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
					strSQL = "select * from commercial where commitmentyear = " + FCConvert.ToString(lngPreviousYear) + " order by rsaccount,rscard";
					clsOldComm.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
					// if not short screen only then determine why there is a change if possible
					// initialize the landtypes because we will need to use it
					modGlobalVariables.Statics.LandTypes.Init();
					for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
					{
						// get the maplot
						// if not already accounted for, then determine why change happened
						Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, CNSTGRIDCOLCUREXEMPTION, Color.White);
						if (!boolAutoChoose || FCConvert.ToInt32(Grid.RowData(lngRow)) != CNSTCHGTYPEUNKNOWN)
						{
							if (!clsCurrent.EndOfFile())
							{
								if (clsCurrent.FindNextRecord("rsaccount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT))))
								{
									Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(clsCurrent.Get_Fields_String("rsmaplot")));
									Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(clsCurrent.Get_Fields_String("rsname")));
								}
							}
						}
						if (FCConvert.ToInt32(Grid.RowData(lngRow)) == CNSTCHGTYPEUNKNOWN && boolAutoChoose)
						{
							// new and previously totally exempt are already taken care of
							// just need to check for overrides and data changes
							frmWait.InstancePtr.lblMessage.Text = "Checking account " + Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT);
							frmWait.InstancePtr.lblMessage.Refresh();
							//Application.DoEvents();
							lngCurrNewValuation = 0;
							if (!clsCurrent.EndOfFile())
							{
								if (clsCurrent.Get_Fields_Int32("rsaccount") != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT)))
								{
									clsCurrent.FindNextRecord("rsaccount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT)));
								}
							}
							// strSQL = "select * from master where rsaccount = " & Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT)) & " and commitmentyear = " & lngLatestYear & " order by rscard"
							// Call clsCurrent.OpenRecordset(strSQL, strCommitDB)
							// strSQL = "select * from master where rsaccount = " & Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT)) & " and commitmentyear = " & lngPreviousYear & " order by rscard"
							// Call clsOld.OpenRecordset(strSQL, strCommitDB)
							if (!clsOld.EndOfFile())
							{
								while (!clsCurrent.EndOfFile())
								{
									boolUsedUp = false;
									aryValues[0] = clsCurrent.Get_Fields_Int32("rsaccount");
									aryValues[1] = clsCurrent.Get_Fields_Int32("rscard");
									if (clsCurrent.Get_Fields_Int32("rsaccount") == Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT)))
									{
										if (FCConvert.ToInt32(clsCurrent.Get_Fields_Int32("rscard")) == 1)
										{
											Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(clsCurrent.Get_Fields_String("rsmaplot")));
											Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(clsCurrent.Get_Fields_String("rsname")));
										}
										lngCardNewLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCurrent.Get_Fields_Int32("lastlandval"))));
										lngCardNewBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCurrent.Get_Fields_Int32("lastbldgval"))));
										lngCardNewExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCurrent.Get_Fields_Int32("rlexemption"))));
										lngCurrNewBldg = 0;
										// TODO Get_Fields: Field [commitmentyear] not found!! (maybe it is an alias?)
										strSQL = "select * from summrecord where srecordnumber = " + clsCurrent.Get_Fields_Int32("rsaccount") + " and cardnumber = " + clsCurrent.Get_Fields_Int32("rscard") + " and commitmentyear = " + clsCurrent.Get_Fields("commitmentyear");
										clsNewSummary.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
										if (clsOld.EndOfFile())
										{
											boolNoOldAccount = true;
										}
										else
										{
											// If clsOld.FindNextRecord(, , "rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard")) Then
											aryValues[0] = clsCurrent.Get_Fields_Int32("rsaccount");
											aryValues[1] = clsCurrent.Get_Fields_Int32("rscard");
											if (clsOld.FindNextRecord2(aryFields, aryValues))
											{
												boolNoOldAccount = false;
											}
											else
											{
												boolNoOldAccount = true;
											}
										}
										if (boolNoOldAccount)
										{
											// this is a new card so we can count all of it as new valuation
											Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
											GridSavedDetail.Rows += 1;
											lngDetailRow = GridSavedDetail.Rows - 1;
											GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
											GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "New Card");
											GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
											GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngCardNewLand + lngCardNewBldg));
											GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
											GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
											GridSavedDetail.IsSubtotal(lngDetailRow, true);
											lngCurrNewValuation += lngCardNewLand + lngCardNewBldg;
											Grid.TextMatrix(lngRow, CNSTGRIDCOLNEWVALUATION, Strings.Format(lngCurrNewValuation, "#,###,###,##0"));
										}
										else
										{
											// TODO Get_Fields: Field [commitmentyear] not found!! (maybe it is an alias?)
											strSQL = "select * from summrecord where srecordnumber = " + clsOld.Get_Fields_Int32("rsaccount") + " and cardnumber = " + clsOld.Get_Fields_Int32("rscard") + " and commitmentyear = " + clsOld.Get_Fields("commitmentyear");
											clsOldSummary.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
											// check to see if this card is different or not
											lngCardOldLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOld.Get_Fields_Int32("lastlandval"))));
											lngCardOldBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOld.Get_Fields_Int32("lastbldgval"))));
											lngCardOldExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOld.Get_Fields_Int32("rlexemption"))));
											if (lngCardOldLand + lngCardOldBldg < lngCardNewLand + lngCardNewBldg)
											{
												if (lngCardOldLand < lngCardNewLand)
												{
													// check for overrides first
													if (Conversion.Val(clsCurrent.Get_Fields_Int32("hivallandcode")) > 1 || Conversion.Val(clsOld.Get_Fields_Int32("hivallandcode")) > 1)
													{
														// overrides. Nothing more to check
														// lngCurrNewValuation = lngCurrNewValuation + lngCardNewLand
														if (FCConvert.ToInt32(Grid.RowData(lngRow)) == CNSTCHGTYPEUNKNOWN)
														{
															Grid.RowData(lngRow, CNSTCHGTYPEOVERRIDE);
														}
														GridSavedDetail.Rows += 1;
														lngDetailRow = GridSavedDetail.Rows - 1;
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
														if (Conversion.Val(clsCurrent.Get_Fields_Int32("hivallandcode")) > 1)
														{
															GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Land Override");
														}
														else if (Conversion.Val(clsOld.Get_Fields_Int32("hivallandcode")) > 1)
														{
															GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Previous Land Override");
														}
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngCardNewLand - lngCardOldLand));
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(lngCardNewLand));
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(lngCardOldLand));
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
														GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
														GridSavedDetail.IsSubtotal(lngDetailRow, true);
													}
													else
													{
														for (x = 1; x <= 7; x++)
														{
															// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
															lngAryNewLand[x, CNSTLANDARYCOLTYPE] = FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields("piland" + FCConvert.ToString(x) + "type")));
															// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
															lngAryOldLand[x, CNSTLANDARYCOLTYPE] = FCConvert.ToString(Conversion.Val(clsOld.Get_Fields("piland" + FCConvert.ToString(x) + "type")));
															if (Conversion.Val(lngAryNewLand[x, CNSTLANDARYCOLTYPE]) > 0 && Conversion.Val(lngAryNewLand[x, CNSTLANDARYCOLTYPE]) != modREConstants.CNSTLANDTYPEEXTRAINFLUENCE)
															{
																modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(lngAryNewLand[x, CNSTLANDARYCOLTYPE])));
																switch (modGlobalVariables.Statics.LandTypes.UnitsType)
																{
																	case modREConstants.CNSTLANDTYPEFRONTFOOT:
																	case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
																		{
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			lngAryNewLand[x, CNSTLANDARYCOLUNITS] = FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa"))) + "x" + FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb")));
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			lngAryNewLand[x, CNSTLANDARYCOLACRES] = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa"))) * FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb")))) / 43560, "0.00");
																			break;
																		}
																	case modREConstants.CNSTLANDTYPESQUAREFEET:
																	case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
																		{
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			lngAryNewLand[x, CNSTLANDARYCOLUNITS] = Strings.Format(FCConvert.ToDouble(Strings.Format(FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb"))), "000")) / 43560, "0.00");
																			lngAryNewLand[x, CNSTLANDARYCOLACRES] = FCConvert.ToString(Conversion.Val(lngAryNewLand[x, CNSTLANDARYCOLUNITS]));
																			break;
																		}
																	case modREConstants.CNSTLANDTYPEACRES:
																	case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
																	case modREConstants.CNSTLANDTYPESITE:
																	case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
																	case modREConstants.CNSTLANDTYPELINEARFEET:
																		{
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			lngAryNewLand[x, CNSTLANDARYCOLUNITS] = Strings.Format(FCConvert.ToDouble(Strings.Format(FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(clsCurrent.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb"))), "000")) / 100, "0.00");
																			lngAryNewLand[x, CNSTLANDARYCOLACRES] = FCConvert.ToString(Conversion.Val(lngAryNewLand[x, CNSTLANDARYCOLUNITS]));
																			break;
																		}
																}
																//end switch
																if (!clsNewSummary.EndOfFile())
																{
																	// TODO Get_Fields: Field [SLANDV] not found!! (maybe it is an alias?)
																	lngAryNewLand[x, CNSTLANDARYCOLVALUE] = FCConvert.ToString(Conversion.Val(clsNewSummary.Get_Fields("SLANDV" + FCConvert.ToString(x))));
																}
																else
																{
																	lngAryNewLand[x, CNSTLANDARYCOLVALUE] = FCConvert.ToString(0);
																}
															}
															else
															{
																lngAryNewLand[x, CNSTLANDARYCOLTYPE] = FCConvert.ToString(0);
																lngAryNewLand[x, CNSTLANDARYCOLUNITS] = "";
																lngAryNewLand[x, CNSTLANDARYCOLVALUE] = FCConvert.ToString(0);
															}
															modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(lngAryOldLand[x, CNSTLANDARYCOLTYPE]));
															if (Conversion.Val(lngAryOldLand[x, CNSTLANDARYCOLTYPE]) > 0 && Conversion.Val(lngAryOldLand[x, CNSTLANDARYCOLTYPE]) != modREConstants.CNSTLANDTYPEEXTRAINFLUENCE)
															{
																switch (modGlobalVariables.Statics.LandTypes.UnitsType)
																{
																	case modREConstants.CNSTLANDTYPEFRONTFOOT:
																	case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
																		{
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			lngAryOldLand[x, CNSTLANDARYCOLUNITS] = FCConvert.ToString(Conversion.Val(clsOld.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa"))) + "x" + FCConvert.ToString(Conversion.Val(clsOld.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb")));
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			lngAryOldLand[x, CNSTLANDARYCOLACRES] = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsOld.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa"))) * FCConvert.ToString(Conversion.Val(clsOld.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb")))) / 43560, "0.00");
																			break;
																		}
																	case modREConstants.CNSTLANDTYPESQUAREFEET:
																	case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
																		{
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			lngAryOldLand[x, CNSTLANDARYCOLUNITS] = Strings.Format(FCConvert.ToDouble(Strings.Format(FCConvert.ToString(Conversion.Val(clsOld.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(clsOld.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb"))), "000")) / 43560, "0.00");
																			lngAryOldLand[x, CNSTLANDARYCOLACRES] = FCConvert.ToString(Conversion.Val(lngAryOldLand[x, CNSTLANDARYCOLUNITS]));
																			break;
																		}
																	case modREConstants.CNSTLANDTYPEACRES:
																	case modREConstants.CNSTLANDTYPEFRACTIONALACREAGE:
																	case modREConstants.CNSTLANDTYPESITE:
																	case modREConstants.CNSTLANDTYPEIMPROVEMENTS:
																	case modREConstants.CNSTLANDTYPELINEARFEET:
																		{
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
																			lngAryOldLand[x, CNSTLANDARYCOLUNITS] = Strings.Format(FCConvert.ToDouble(Strings.Format(FCConvert.ToString(Conversion.Val(clsOld.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa"))), "000") + Strings.Format(FCConvert.ToString(Conversion.Val(clsOld.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb"))), "000")) / 100, "0.00");
																			lngAryOldLand[x, CNSTLANDARYCOLACRES] = FCConvert.ToString(Conversion.Val(lngAryOldLand[x, CNSTLANDARYCOLUNITS]));
																			break;
																		}
																}
																//end switch
																if (!clsOldSummary.EndOfFile())
																{
																	// TODO Get_Fields: Field [Slandv] not found!! (maybe it is an alias?)
																	lngAryOldLand[x, CNSTLANDARYCOLVALUE] = FCConvert.ToString(Conversion.Val(clsOldSummary.Get_Fields("Slandv" + FCConvert.ToString(x))));
																}
																else
																{
																	lngAryOldLand[x, CNSTLANDARYCOLVALUE] = FCConvert.ToString(0);
																}
															}
															else
															{
																lngAryOldLand[x, CNSTLANDARYCOLTYPE] = FCConvert.ToString(0);
																lngAryOldLand[x, CNSTLANDARYCOLUNITS] = "";
																lngAryOldLand[x, CNSTLANDARYCOLVALUE] = FCConvert.ToString(0);
															}
														}
														// x
														for (intCurrent = 1; intCurrent <= 7; intCurrent++)
														{
															if (Conversion.Val(lngAryNewLand[intCurrent, CNSTLANDARYCOLTYPE]) > 0)
															{
																boolTemp = false;
																for (intOld = 1; intOld <= 7; intOld++)
																{
																	if (Conversion.Val(lngAryOldLand[intOld, CNSTLANDARYCOLTYPE]) == Conversion.Val(lngAryNewLand[intCurrent, CNSTLANDARYCOLTYPE]))
																	{
																		// don't match this one again
																		lngAryOldLand[intOld, CNSTLANDARYCOLTYPE] = FCConvert.ToString(0);
																		boolAddToVal = false;
																		boolTemp = true;
																		strDetail = "";
																		if (Conversion.Val(lngAryNewLand[intCurrent, CNSTLANDARYCOLVALUE]) > Conversion.Val(lngAryOldLand[intOld, CNSTLANDARYCOLVALUE]))
																		{
																			if (Conversion.Val(lngAryNewLand[intCurrent, CNSTLANDARYCOLACRES]) > Conversion.Val(lngAryOldLand[intOld, CNSTLANDARYCOLACRES]))
																			{
																				Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																				boolAddToVal = true;
																				GridSavedDetail.Rows += 1;
																				lngDetailRow = GridSavedDetail.Rows - 1;
																				GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																				GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
																				GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
																				GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(Conversion.Val(lngAryNewLand[intCurrent, CNSTLANDARYCOLVALUE]) - Conversion.Val(lngAryOldLand[intOld, CNSTLANDARYCOLVALUE])));
																				GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Land Parcel " + FCConvert.ToString(intCurrent) + " Card " + clsCurrent.Get_Fields_Int32("rscard"));
																				lngCurrNewValuation += FCConvert.ToInt32(Conversion.Val(lngAryNewLand[intCurrent, CNSTLANDARYCOLVALUE]) - Conversion.Val(lngAryOldLand[intOld, CNSTLANDARYCOLVALUE]));
																				GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
																				GridSavedDetail.IsSubtotal(lngDetailRow, true);
																				GridSavedDetail.Rows += 1;
																				lngSubRow = GridSavedDetail.Rows - 1;
																				GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																				GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Units");
																				GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, lngAryNewLand[intCurrent, CNSTLANDARYCOLUNITS]);
																				GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, lngAryOldLand[intOld, CNSTLANDARYCOLUNITS]);
																				GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																				GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																				GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																				GridSavedDetail.IsSubtotal(lngSubRow, true);
																			}
																		}
																		break;
																	}
																}
																// intOld
																if (!boolTemp)
																{
																	// new land type
																	Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																	GridSavedDetail.Rows += 1;
																	lngDetailRow = GridSavedDetail.Rows - 1;
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(Conversion.Val(lngAryNewLand[intCurrent, CNSTLANDARYCOLVALUE])));
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Land Parcel " + FCConvert.ToString(intCurrent) + " Card " + clsCurrent.Get_Fields_Int32("rscard"));
																	GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
																	GridSavedDetail.IsSubtotal(lngDetailRow, true);
																	lngCurrNewValuation += FCConvert.ToInt32(Conversion.Val(lngAryNewLand[intCurrent, CNSTLANDARYCOLVALUE]));
																	GridSavedDetail.Rows += 1;
																	lngSubRow = GridSavedDetail.Rows - 1;
																	GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																	GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																	GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																	GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																	GridSavedDetail.IsSubtotal(lngSubRow, true);
																	GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "New Parcel");
																}
															}
														}
														// intCurrent
													}
												}
												if (lngCardOldBldg < lngCardNewBldg)
												{
													if (Conversion.Val(clsCurrent.Get_Fields_Int32("hivalbldgcode")) > 1 || Conversion.Val(clsOld.Get_Fields_Int32("hivalbldgcode")) > 1)
													{
														// lngCurrNewValuation = lngCurrNewValuation + lngCardNewBldg - lngCardOldBldg
														if (FCConvert.ToInt32(Grid.RowData(lngRow)) == CNSTCHGTYPEUNKNOWN)
														{
															Grid.RowData(lngRow, CNSTCHGTYPEOVERRIDE);
														}
														// lngCurrNewBldg = lngCardNewBldg - lngCardOldBldg
														boolUsedUp = true;
														GridSavedDetail.Rows += 1;
														lngDetailRow = GridSavedDetail.Rows - 1;
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
														if (Conversion.Val(clsCurrent.Get_Fields_Int32("hivalbldgcode")) > 1)
														{
															GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Building Override");
														}
														else
														{
															GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Previous Bldg Override");
														}
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(lngCardNewBldg));
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(lngCardOldBldg));
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
														GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngCardNewBldg - lngCardOldBldg));
														GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
													}
													else
													{
														// check for any building changes
														// check outbuildings first
														// strSQL = "Select * from outbuilding where rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard") & " and commitmentyear = " & clsCurrent.Fields("commitmentyear")
														// Call clsNewTemp.OpenRecordset(strSQL, strCommitDB)
														// If clsNewOut.FindNextRecord(, , "rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard")) Then
														aryValues[0] = clsCurrent.Get_Fields_Int32("rsaccount");
														aryValues[1] = clsCurrent.Get_Fields_Int32("rscard");
														if (clsNewOut.FindNextRecord2(aryFields, aryValues))
														{
															// strSQL = "Select * from outbuilding where rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard") & " and commitmentyear = " & clsOld.Fields("commitmentyear")
															// Call clsOldTemp.OpenRecordset(strSQL, strCommitDB)
															// If clsOldOut.FindNextRecord(, , "rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard")) Then
															if (clsOldOut.FindNextRecord2(aryFields, aryValues))
															{
																for (x = 1; x <= 10; x++)
																{
																	// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
																	lngAryNewOut[x, CNSTOUTARYCOLTYPE] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsNewOut.Get_Fields("oitype" + FCConvert.ToString(x)))));
																	// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
																	lngAryOldOut[x, CNSTOUTARYCOLTYPE] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOldOut.Get_Fields("oitype" + FCConvert.ToString(x)))));
																	// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
																	lngAryNewOut[x, CNSTOUTARYCOLYEAR] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsNewOut.Get_Fields("oiyear" + FCConvert.ToString(x)))));
																	// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
																	lngAryOldOut[x, CNSTOUTARYCOLYEAR] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOldOut.Get_Fields("oiyear" + FCConvert.ToString(x)))));
																	// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
																	lngAryNewOut[x, CNSTOUTARYCOLUNITS] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsNewOut.Get_Fields("oiunits" + FCConvert.ToString(x)))));
																	// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
																	lngAryOldOut[x, CNSTOUTARYCOLUNITS] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOldOut.Get_Fields("oiunits" + FCConvert.ToString(x)))));
																	// TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
																	lngAryNewOut[x, CNSTOUTARYCOLUSESOUND] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsNewOut.Get_Fields("oiusesound" + FCConvert.ToString(x)))));
																	// TODO Get_Fields: Field [OiuseSound] not found!! (maybe it is an alias?)
																	lngAryOldOut[x, CNSTOUTARYCOLUSESOUND] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOldOut.Get_Fields("OiuseSound" + FCConvert.ToString(x)))));
																	// TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
																	if (FCConvert.ToBoolean(clsNewOut.Get_Fields("oiusesound" + FCConvert.ToString(x))))
																	{
																		// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
																		lngAryNewOut[x, CNSTOUTARYCOLVALUE] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsNewOut.Get_Fields("oisoundvalue" + FCConvert.ToString(x)))));
																	}
																	else if (clsNewSummary.EndOfFile())
																	{
																		lngAryNewOut[x, CNSTOUTARYCOLVALUE] = 0;
																	}
																	else
																	{
																		// TODO Get_Fields: Field [sobv] not found!! (maybe it is an alias?)
																		lngAryNewOut[x, CNSTOUTARYCOLVALUE] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsNewSummary.Get_Fields("sobv" + FCConvert.ToString(x)))));
																	}
																	// TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
																	if (FCConvert.ToBoolean(clsOldOut.Get_Fields("oiusesound" + FCConvert.ToString(x))))
																	{
																		// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
																		lngAryOldOut[x, CNSTOUTARYCOLVALUE] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOldOut.Get_Fields("oisoundvalue" + FCConvert.ToString(x)))));
																	}
																	else if (clsOldSummary.EndOfFile())
																	{
																		lngAryOldOut[x, CNSTOUTARYCOLVALUE] = 0;
																	}
																	else
																	{
																		// TODO Get_Fields: Field [sobv] not found!! (maybe it is an alias?)
																		lngAryOldOut[x, CNSTOUTARYCOLVALUE] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOldSummary.Get_Fields("sobv" + FCConvert.ToString(x)))));
																	}
																}
																// x
																// have the arrays full, now compare the values
																for (intCurrent = 1; intCurrent <= 10; intCurrent++)
																{
																	if (lngAryNewOut[intCurrent, CNSTOUTARYCOLTYPE] > 0)
																	{
																		boolTemp = false;
																		for (intOld = 1; intOld <= 10; intOld++)
																		{
																			if (lngAryNewOut[intCurrent, CNSTOUTARYCOLTYPE] == lngAryOldOut[intOld, CNSTOUTARYCOLTYPE])
																			{
																				// assume a match
																				boolAddToVal = false;
																				boolTemp = true;
																				strDetail = "";
																				lngAryOldOut[intOld, CNSTOUTARYCOLTYPE] = 0;
																				// don't try to match to this one again
																				if (lngAryNewOut[intCurrent, CNSTOUTARYCOLVALUE] > lngAryOldOut[intOld, CNSTOUTARYCOLVALUE])
																				{
																					if (FCConvert.CBool(lngAryNewOut[intCurrent, CNSTOUTARYCOLUSESOUND]) || FCConvert.CBool(lngAryOldOut[intOld, CNSTOUTARYCOLUSESOUND]))
																					{
																						lngCurrNewBldg += (lngAryNewOut[intCurrent, CNSTOUTARYCOLVALUE] - lngAryOldOut[intOld, CNSTOUTARYCOLVALUE]);
																						Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																						strDetail = "Sound Value";
																						GridSavedDetail.Rows += 1;
																						lngDetailRow = GridSavedDetail.Rows - 1;
																						GridSavedDetail.Rows += 1;
																						lngSubRow = GridSavedDetail.Rows - 1;
																						GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(lngAryOldOut[intOld, CNSTOUTARYCOLVALUE]));
																						GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(lngAryNewOut[intCurrent, CNSTOUTARYCOLVALUE]));
																						GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Sound Value");
																						GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																						GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																						GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																						GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																						GridSavedDetail.IsSubtotal(lngSubRow, true);
																						boolAddToVal = true;
																					}
																					else
																					{
																						if (lngAryNewOut[intCurrent, CNSTOUTARYCOLYEAR] > lngAryOldOut[intOld, CNSTOUTARYCOLYEAR])
																						{
																							lngCurrNewBldg += (lngAryNewOut[intCurrent, CNSTOUTARYCOLVALUE] - lngAryOldOut[intOld, CNSTOUTARYCOLVALUE]);
																							Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																							strDetail = "Year";
																							if (!boolAddToVal)
																							{
																								GridSavedDetail.Rows += 1;
																								lngDetailRow = GridSavedDetail.Rows - 1;
																								boolAddToVal = true;
																							}
																							GridSavedDetail.Rows += 1;
																							lngSubRow = GridSavedDetail.Rows - 1;
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																							GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																							GridSavedDetail.IsSubtotal(lngSubRow, true);
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, strDetail);
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(lngAryOldOut[intOld, CNSTOUTARYCOLYEAR]));
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(lngAryNewOut[intCurrent, CNSTOUTARYCOLYEAR]));
																						}
																						if (lngAryNewOut[intCurrent, CNSTOUTARYCOLUNITS] > lngAryOldOut[intOld, CNSTOUTARYCOLUNITS])
																						{
																							lngCurrNewBldg += (lngAryNewOut[intCurrent, CNSTOUTARYCOLVALUE] - lngAryOldOut[intOld, CNSTOUTARYCOLVALUE]);
																							Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																							strDetail = "Units";
																							if (!boolAddToVal)
																							{
																								GridSavedDetail.Rows += 1;
																								lngDetailRow = GridSavedDetail.Rows - 1;
																								boolAddToVal = true;
																							}
																							GridSavedDetail.Rows += 1;
																							lngSubRow = GridSavedDetail.Rows - 1;
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																							GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																							GridSavedDetail.IsSubtotal(lngSubRow, true);
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, strDetail);
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(lngAryOldOut[intOld, CNSTOUTARYCOLUNITS]));
																							GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(lngAryNewOut[intCurrent, CNSTOUTARYCOLUNITS]));
																						}
																					}
																					if (boolAddToVal)
																					{
																						// GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDETAIL) = strDetail
																						GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																						GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
																						GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Outbuilding " + FCConvert.ToString(intCurrent) + " card " + clsCurrent.Get_Fields_Int32("rscard"));
																						GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngAryNewOut[intCurrent, CNSTOUTARYCOLVALUE] - lngAryOldOut[intOld, CNSTOUTARYCOLVALUE]));
																						GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
																					}
																				}
																				break;
																			}
																		}
																		// intOld
																		if (!boolTemp)
																		{
																			// no match, this is a new outbuilding
																			lngCurrNewBldg += lngAryNewOut[intCurrent, CNSTOUTARYCOLVALUE];
																			Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Outbuilding " + FCConvert.ToString(intCurrent) + " card " + clsCurrent.Get_Fields_Int32("rscard"));
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngAryNewOut[intCurrent, CNSTOUTARYCOLVALUE]));
																			GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
																			GridSavedDetail.IsSubtotal(lngDetailRow, true);
																			GridSavedDetail.Rows += 1;
																			lngSubRow = GridSavedDetail.Rows - 1;
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																			GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																			GridSavedDetail.IsSubtotal(lngSubRow, true);
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "New Outbuilding");
																		}
																	}
																}
																// intCurrent
															}
															else
															{
																Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																for (x = 1; x <= 10; x++)
																{
																	// TODO Get_Fields: Field [oiusesound] not found!! (maybe it is an alias?)
																	if (FCConvert.ToBoolean(clsNewOut.Get_Fields("oiusesound" + FCConvert.ToString(x))))
																	{
																		// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
																		lngCurrNewBldg += FCConvert.ToInt32(Conversion.Val(clsNewOut.Get_Fields("oisoundvalue" + FCConvert.ToString(x))));
																		// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
																		lngAryNewOut[x, CNSTOUTARYCOLVALUE] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsNewOut.Get_Fields("oisoundvalue" + FCConvert.ToString(x)))));
																	}
																	else if (clsNewSummary.EndOfFile())
																	{
																		lngCurrNewBldg = lngCardNewBldg - lngCardOldBldg;
																		boolUsedUp = true;
																		if (x > 1)
																		{
																			lngAryNewOut[x, CNSTOUTARYCOLVALUE] = 0;
																		}
																		else
																		{
																			lngAryNewOut[x, CNSTOUTARYCOLVALUE] = lngCardNewBldg - lngCardOldBldg;
																		}
																	}
																	else
																	{
																		// TODO Get_Fields: Field [sobv] not found!! (maybe it is an alias?)
																		lngCurrNewBldg += FCConvert.ToInt32(Conversion.Val(clsNewSummary.Get_Fields("sobv" + FCConvert.ToString(x))));
																		// TODO Get_Fields: Field [sobv] not found!! (maybe it is an alias?)
																		lngAryNewOut[x, CNSTOUTARYCOLVALUE] = FCConvert.ToInt32(Math.Round(Conversion.Val(clsNewSummary.Get_Fields("sobv" + FCConvert.ToString(x)))));
																	}
																	// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
																	if (Conversion.Val(clsNewOut.Get_Fields("oitype" + FCConvert.ToString(x))) > 0)
																	{
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Outbuilding " + FCConvert.ToString(intCurrent) + " card " + clsCurrent.Get_Fields_Int32("rscard"));
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngAryNewOut[intCurrent, CNSTOUTARYCOLVALUE]));
																		GridSavedDetail.IsSubtotal(lngDetailRow, true);
																		GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "New Outbuilding");
																	}
																}
																// x
															}
														}
														// if clsnewtemp.endoffile
														// now check dwelling info
														// strSQL = "select * from dwelling where rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard") & " and commitmentyear = " & clsCurrent.Fields("commitmentyear")
														// Call clsNewTemp.OpenRecordset(strSQL, strCommitDB)
														aryValues[0] = clsCurrent.Get_Fields_Int32("rsaccount");
														aryValues[1] = clsCurrent.Get_Fields_Int32("rscard");
														// If clsNewDwell.FindNextRecord(, , "rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard")) Then
														if (clsNewDwell.FindNextRecord2(aryFields, aryValues))
														{
															// strSQL = "select * from dwelling where rsaccount = " & clsOld.Fields("rsaccount") & " and rscard = " & clsOld.Fields("rscard") & " and commitmentyear = " & clsOld.Fields("commitmentyear")
															// Call clsOldTemp.OpenRecordset(strSQL, strCommitDB)
															strDetail = "";
															// If clsOldDwell.FindNextRecord(, , "rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard")) Then
															if (clsOldDwell.FindNextRecord2(aryFields, aryValues))
															{
																// now check if it's different
																boolTemp = false;
																if (Conversion.Val(clsNewDwell.Get_Fields_Int32("distyle")) > 0 && Conversion.Val(clsOldDwell.Get_Fields_Int32("distyle")) == 0 && Conversion.Val(clsNewSummary.Get_Fields_Int32("sdwellrcnld")) > 0 && Conversion.Val(clsOldSummary.Get_Fields_Int32("sdwellrcnld")) == 0)
																{
																	boolTemp = true;
																	// new dwelling
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDETAIL, "");
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "New Dwelling, Card " + clsCurrent.Get_Fields_Int32("rsCARD"));
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(0));
																	if (!clsNewSummary.EndOfFile())
																	{
																		if (!boolUsedUp)
																		{
																			lngCurrNewBldg += FCConvert.ToInt32(Conversion.Val(clsNewSummary.Get_Fields_Int32("sdwellrcnld")));
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(Conversion.Val(clsNewSummary.Get_Fields_Int32("sdwellrcnld"))));
																		}
																	}
																	else
																	{
																		lngCurrNewBldg = lngCardNewBldg - lngCardOldBldg;
																		if (!boolUsedUp)
																		{
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngCardNewBldg - lngCardOldBldg - lngCurrNewBldg));
																		}
																	}
																	Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																}
																else
																{
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("distories")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("distories")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Stories");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("distories"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("distories"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "Stories,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("disfmasonry")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("disfmasonry")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "SF Masonry Trim");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("disfmasonry"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("disfmasonry"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "SF Masonry Trim,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("diyearbuilt")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("diyearbuilt")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Year Built");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("DIyearbuilt"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("diyearbuilt"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "Year built,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("diyearremodel")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("diyearremodel")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Year Remodeled");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("diyearremodel"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("diyearremodel"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "Year Remodeled,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("disfbsmtliving")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("disfbsmtliving")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "SF Bsmt Living");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("disfbsmtliving"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("disfbsmtliving"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "SF Bsmt Living,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("dipctheat")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("dipctheat")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "% Heated");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("dipctheat"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("dipctheat"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "% heated,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("dipctcool")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("dipctcool")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "% cooled");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("dipctcool"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("dipctcool"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "% cooled,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("difullbaths")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("difullbaths")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Full Baths");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("difullbaths"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("difullbaths"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "Full Baths,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("dihalfbaths")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("dihalfbaths")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Half Baths");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("dihalfbaths"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("dihalfbaths"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "Half Baths,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("diaddnfixtures")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("diaddnfixtures")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Additional Fixtures");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("diaddnfixtures"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("diaddnfixtures"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "Additional Fixtures,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("difireplaces")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("difireplaces")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Fireplaces");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("difireplaces"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("difireplaces"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "Fireplaces,";
																	}
																	if (Conversion.Val(clsNewDwell.Get_Fields_Int32("disqft")) > Conversion.Val(clsOldDwell.Get_Fields_Int32("disqft")))
																	{
																		if (!boolTemp)
																		{
																			GridSavedDetail.Rows += 1;
																			lngDetailRow = GridSavedDetail.Rows - 1;
																		}
																		GridSavedDetail.Rows += 1;
																		lngSubRow = GridSavedDetail.Rows - 1;
																		GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Square Feet");
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewDwell.Get_Fields_Int32("disqft"))));
																		GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldDwell.Get_Fields_Int32("disqft"))));
																		GridSavedDetail.IsSubtotal(lngSubRow, true);
																		boolTemp = true;
																		strDetail += "Sqft,";
																	}
																	if (strDetail != string.Empty)
																	{
																		strDetail = Strings.Mid(strDetail, 1, strDetail.Length - 1);
																	}
																	if (boolTemp)
																	{
																		// there was a datachange that affected value
																		Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																		GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
																		GridSavedDetail.IsSubtotal(lngDetailRow, true);
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
																		// GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDETAIL) = strDetail
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Dwelling, Card " + clsCurrent.Get_Fields_Int32("rscard"));
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(0));
																		if (!clsNewSummary.EndOfFile() && !clsOldSummary.EndOfFile())
																		{
																			if (!boolUsedUp && (Conversion.Val(clsNewSummary.Get_Fields_Int32("sdwellrcnld")) - Conversion.Val(clsOldSummary.Get_Fields_Int32("sdwellrcnld"))) > 0)
																			{
																				lngCurrNewBldg += FCConvert.ToInt32(Conversion.Val(clsNewSummary.Get_Fields_Int32("sdwellrcnld")) - Conversion.Val(clsOldSummary.Get_Fields_Int32("sdwellrcnld")));
																				GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(Conversion.Val(clsNewSummary.Get_Fields_Int32("sdwellrcnld")) - Conversion.Val(clsOldSummary.Get_Fields_Int32("sdwellrcnld"))));
																			}
																		}
																		else
																		{
																			if (!boolUsedUp)
																			{
																				if (lngCurrNewBldg == 0)
																				{
																					GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngCardNewBldg - lngCardOldBldg));
																				}
																				else
																				{
																					GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngCardNewBldg - lngCardOldBldg - lngCurrNewBldg));
																					lngCurrNewBldg = lngCardNewBldg - lngCardOldBldg;
																				}
																			}
																			lngCurrNewBldg = lngCardNewBldg - lngCardOldBldg;
																			boolUsedUp = true;
																		}
																	}
																}
															}
															else
															{
																// new dwelling
																GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
																GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
																GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDETAIL, "");
																GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "New Dwelling, Card " + clsCurrent.Get_Fields_Int32("rsCARD"));
																GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(0));
																if (!clsNewSummary.EndOfFile())
																{
																	if (!boolUsedUp)
																	{
																		lngCurrNewBldg += FCConvert.ToInt32(Conversion.Val(clsNewSummary.Get_Fields_Int32("sdwellrcnld")));
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(Conversion.Val(clsNewSummary.Get_Fields_Int32("sdwellrcnld"))));
																	}
																}
																else
																{
																	lngCurrNewBldg = lngCardNewBldg - lngCardOldBldg;
																	if (!boolUsedUp)
																	{
																		GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngCardNewBldg - lngCardOldBldg - lngCurrNewBldg));
																	}
																}
																Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
															}
														}
														// strSQL = "select * from commercial where rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard") & " and commitmentyear = " & clsCurrent.Fields("commitmentyear")
														// Call clsNewTemp.OpenRecordset(strSQL, strCommitDB)
														// If clsNewComm.FindNextRecord(, , "rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard")) Then
														if (clsNewComm.FindNextRecord2(aryFields, aryValues))
														{
															// strSQL = "select * from commercial where rsaccount = " & clsOld.Fields("rsaccount") & " and rscard = " & clsOld.Fields("rscard") & " and commitmentyear = " & clsOld.Fields("commitmentyear")
															// Call clsOldTemp.OpenRecordset(strSQL, strCommitDB)
															// If clsOldComm.FindNextRecord(, , "rsaccount = " & clsCurrent.Fields("rsaccount") & " and rscard = " & clsCurrent.Fields("rscard")) Then
															if (clsOldComm.FindNextRecord2(aryFields, aryValues))
															{
																// two commercial buildings to check
																for (x = 1; x <= 2; x++)
																{
																	boolTemp = false;
																	// TODO Get_Fields: Field [occ] not found!! (maybe it is an alias?)
																	if (Conversion.Val(clsNewComm.Get_Fields("occ" + FCConvert.ToString(x))) > 0)
																	{
																		// TODO Get_Fields: Field [occ] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [occ] not found!! (maybe it is an alias?)
																		if (Conversion.Val(clsOldComm.Get_Fields("occ" + FCConvert.ToString(x))) != Conversion.Val(clsNewComm.Get_Fields("occ" + FCConvert.ToString(x))))
																		{
																			if (!boolTemp)
																			{
																				GridSavedDetail.Rows += 1;
																				lngDetailRow = GridSavedDetail.Rows - 1;
																			}
																			boolTemp = true;
																			GridSavedDetail.Rows += 1;
																			lngSubRow = GridSavedDetail.Rows - 1;
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																			GridSavedDetail.IsSubtotal(lngSubRow, true);
																			GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Occupancy Code");
																			// TODO Get_Fields: Field [occ] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewComm.Get_Fields("occ" + FCConvert.ToString(x)))));
																			// TODO Get_Fields: Field [occ] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldComm.Get_Fields("occ" + FCConvert.ToString(x)))));
																		}
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		if (Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "stories")) > Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Stories")))
																		{
																			if (!boolTemp)
																			{
																				GridSavedDetail.Rows += 1;
																				lngDetailRow = GridSavedDetail.Rows - 1;
																			}
																			boolTemp = true;
																			GridSavedDetail.Rows += 1;
																			lngSubRow = GridSavedDetail.Rows - 1;
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																			GridSavedDetail.IsSubtotal(lngSubRow, true);
																			GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Stories");
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "Stories"))));
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Stories"))));
																		}
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		if (Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "Height")) > Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Height")))
																		{
																			if (!boolTemp)
																			{
																				GridSavedDetail.Rows += 1;
																				lngDetailRow = GridSavedDetail.Rows - 1;
																			}
																			boolTemp = true;
																			GridSavedDetail.Rows += 1;
																			lngSubRow = GridSavedDetail.Rows - 1;
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																			GridSavedDetail.IsSubtotal(lngSubRow, true);
																			GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Height");
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "Height"))));
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Height"))));
																		}
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		if (Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "Floor")) > Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Floor")))
																		{
																			if (!boolTemp)
																			{
																				GridSavedDetail.Rows += 1;
																				lngDetailRow = GridSavedDetail.Rows - 1;
																			}
																			boolTemp = true;
																			GridSavedDetail.Rows += 1;
																			lngSubRow = GridSavedDetail.Rows - 1;
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																			GridSavedDetail.IsSubtotal(lngSubRow, true);
																			GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Floor");
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "Floor"))));
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Floor"))));
																		}
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		if (Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "Perimeter")) > Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Perimeter")))
																		{
																			if (!boolTemp)
																			{
																				GridSavedDetail.Rows += 1;
																				lngDetailRow = GridSavedDetail.Rows - 1;
																			}
																			boolTemp = true;
																			GridSavedDetail.Rows += 1;
																			lngSubRow = GridSavedDetail.Rows - 1;
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																			GridSavedDetail.IsSubtotal(lngSubRow, true);
																			GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Perimeter");
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "Perimeter"))));
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Perimeter"))));
																		}
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		if (Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "Built")) > Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Built")))
																		{
																			if (!boolTemp)
																			{
																				GridSavedDetail.Rows += 1;
																				lngDetailRow = GridSavedDetail.Rows - 1;
																			}
																			boolTemp = true;
																			GridSavedDetail.Rows += 1;
																			lngSubRow = GridSavedDetail.Rows - 1;
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																			GridSavedDetail.IsSubtotal(lngSubRow, true);
																			GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Year Built");
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "built"))));
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "built"))));
																		}
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																		if (Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "Remodel")) > Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Remodel")))
																		{
																			if (!boolTemp)
																			{
																				GridSavedDetail.Rows += 1;
																				lngDetailRow = GridSavedDetail.Rows - 1;
																			}
																			boolTemp = true;
																			GridSavedDetail.Rows += 1;
																			lngSubRow = GridSavedDetail.Rows - 1;
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																			GridSavedDetail.IsSubtotal(lngSubRow, true);
																			GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "Year Remodeled");
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLCURRENT, FCConvert.ToString(Conversion.Val(clsNewComm.Get_Fields("C" + FCConvert.ToString(x) + "Remodel"))));
																			// TODO Get_Fields: Field [C] not found!! (maybe it is an alias?)
																			GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToString(Conversion.Val(clsOldComm.Get_Fields("C" + FCConvert.ToString(x) + "Remodel"))));
																		}
																		if (boolTemp)
																		{
																			GridSavedDetail.IsSubtotal(lngDetailRow, true);
																			GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Commercial Bldg " + FCConvert.ToString(x) + " Card " + clsCurrent.Get_Fields_Int32("rscard"));
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
																			GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
																			if (!clsNewSummary.EndOfFile() && !clsOldSummary.EndOfFile())
																			{
																				// TODO Get_Fields: Field [comval] not found!! (maybe it is an alias?)
																				// TODO Get_Fields: Field [comval] not found!! (maybe it is an alias?)
																				if (Conversion.Val(clsNewSummary.Get_Fields("comval" + FCConvert.ToString(x))) > Conversion.Val(clsOldSummary.Get_Fields("comval" + FCConvert.ToString(x))))
																				{
																					// TODO Get_Fields: Field [comval] not found!! (maybe it is an alias?)
																					// TODO Get_Fields: Field [comval] not found!! (maybe it is an alias?)
																					lngCurrNewBldg += (Conversion.Val(clsNewSummary.Get_Fields("comval" + FCConvert.ToString(x))) - Conversion.Val(clsOldSummary.Get_Fields("comval" + FCConvert.ToString(x))));
																					// TODO Get_Fields: Field [comval] not found!! (maybe it is an alias?)
																					// TODO Get_Fields: Field [comval] not found!! (maybe it is an alias?)
																					GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString((Conversion.Val(clsNewSummary.Get_Fields("comval" + FCConvert.ToString(x))) - Conversion.Val(clsOldSummary.Get_Fields("comval" + FCConvert.ToString(x))))));
																					Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																				}
																			}
																			else
																			{
																				Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																				GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngCurrNewBldg - (lngCardNewBldg - lngCardOldBldg)));
																				lngCurrNewBldg = lngCardNewBldg - lngCardOldBldg;
																			}
																		}
																	}
																}
																// x
															}
															else
															{
																// the new commercials are totally new valuation
																GridSavedDetail.Rows += 1;
																lngDetailRow = GridSavedDetail.Rows - 1;
																GridSavedDetail.IsSubtotal(lngDetailRow, true);
																GridSavedDetail.RowOutlineLevel(lngDetailRow, 0);
																GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Commercial Bldg(s) " + FCConvert.ToString(x) + " Card " + clsCurrent.Get_Fields_Int32("rscard"));
																GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
																GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
																Grid.RowData(lngRow, CNSTCHGTYPEDATACHANGE);
																if (!clsNewSummary.EndOfFile())
																{
																	lngCurrNewBldg += FCConvert.ToInt32(Conversion.Val(clsNewSummary.Get_Fields_Int32("comval1")) + Conversion.Val(clsNewSummary.Get_Fields_Int32("comval2")));
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(Conversion.Val(clsNewSummary.Get_Fields_Int32("comval1")) + Conversion.Val(clsNewSummary.Get_Fields_Int32("comval2"))));
																}
																else
																{
																	GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngCurrNewBldg - (lngCardNewBldg - lngCardOldBldg)));
																	lngCurrNewBldg = lngCardNewBldg - lngCardOldBldg;
																}
																GridSavedDetail.Rows += 1;
																lngSubRow = GridSavedDetail.Rows - 1;
																GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(clsCurrent.Get_Fields_Int32("rsaccount")));
																GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngSubRow));
																GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLDETAIL, "New Commercial Bldg(s)");
																GridSavedDetail.TextMatrix(lngSubRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
																GridSavedDetail.IsSubtotal(lngSubRow, true);
																GridSavedDetail.RowOutlineLevel(lngSubRow, 1);
															}
															// if old commercial
														}
														// if there is a commercial
														if (lngCurrNewBldg > lngCardNewBldg)
															lngCurrNewBldg = lngCardNewBldg;
														lngCurrNewValuation += lngCurrNewBldg;
													}
													// if override
												}
												// if old bldg value < new bldg value
												if (lngCurrNewValuation > 0)
												{
													Grid.TextMatrix(lngRow, CNSTGRIDCOLNEWVALUATION, Strings.Format(lngCurrNewValuation, "#,###,###,##0"));
												}
												else
												{
												}
											}
											// if oldvaluation is < newvaluation
										}
										// if new card
										clsCurrent.MoveNext();
										// If Not clsOld.EndOfFile Then
										// clsOld.MoveNext
										// End If
									}
									else
									{
										break;
									}
									// if same account number
								}
							}
							else
							{
								// shouldn't be able to get here
								// Grid.RowData(lngRow) = CNSTCHGTYPENEW
								// If lngNewExempt < lngNewLand + lngNewBldg Then
								// lngCurrNewValuation = lngNewLand + lngNewBldg '- lngNewExempt
								// Else
								// lngCurrNewValuation = 0
								// End If
							}
						}
						if (FCConvert.ToInt32(Grid.RowData(lngRow)) == CNSTCHGTYPEUNKNOWN || FCConvert.ToInt32(Grid.RowData(lngRow)) == CNSTCHGTYPEOVERRIDE || FCConvert.ToInt32(Grid.RowData(lngRow)) == CNSTCHGTYPEPREVOVERRIDE)
						{
							if (FCConvert.ToInt32(Grid.RowData(lngRow)) == CNSTCHGTYPEUNKNOWN)
							{
								lngCurrNewValuation = 0;
								if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVLAND)) > 0)
								{
									lngOldLand = FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVLAND)));
								}
								else
								{
									lngOldLand = 0;
								}
								if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVBLDG)) > 0)
								{
									lngOldBldg = FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVBLDG)));
								}
								else
								{
									lngOldBldg = 0;
								}
								// 
								if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCURLAND)) > 0)
								{
									lngNewLand = FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLCURLAND)));
								}
								else
								{
									lngNewLand = 0;
								}
								if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCURBLDG)) > 0)
								{
									lngNewBldg = FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLCURBLDG)));
								}
								else
								{
									lngNewBldg = 0;
								}
							}
							if (boolShowUnexplainedAccounts)
							{
								Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, Grid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								Grid.TextMatrix(lngRow, CNSTGRIDCOLNEWVALUATION, FCConvert.ToString(0));
								if (FCConvert.ToInt32(Grid.RowData(lngRow)) == CNSTCHGTYPEUNKNOWN)
								{
									GridSavedDetail.Rows += 1;
									lngDetailRow = GridSavedDetail.Rows - 1;
									GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLACCOUNT, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT))));
									GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
									GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngDetailRow));
									GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLDESCRIPTION, "Unknown valuation change");
									lngCurrNewValuation = lngNewBldg + lngNewLand - lngOldBldg - lngOldLand;
									GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(lngCurrNewValuation));
									lngCurrNewValuation = 0;
								}
							}
							else
							{
								dblTotalCurVal += -lngNewLand - lngNewBldg;
								dblTotalOldVal += -lngOldLand - lngOldBldg;
							}
						}
						else
						{
							dblTotalNewVal += lngCurrNewValuation;
						}
						// dblTotalNewVal = dblTotalNewVal + lngCurrNewValuation
					}
					// lngRow
					modGlobalVariables.Statics.LandTypes.UnloadLandTypes();
					if (!boolShowUnexplainedAccounts)
					{
						// delete out all of the unwanted rows
						for (x = Grid.Rows - 1; x >= 1; x--)
						{
							if (FCConvert.ToInt32(Grid.RowData(x)) == CNSTCHGTYPEUNKNOWN || FCConvert.ToInt32(Grid.RowData(x)) == CNSTCHGTYPEOVERRIDE || FCConvert.ToInt32(Grid.RowData(x)) == CNSTCHGTYPEPREVOVERRIDE)
							{
								Grid.RemoveItem(x);
							}
						}
						// x
					}
				}
				else
				{
					dblTotalNewVal = dblTempNewVal;
					strSQL = "select * from master where not rsdeleted = 1 and  commitmentyear = " + FCConvert.ToString(lngLatestYear) + " order by rsaccount,rscard";
					clsCurrent.OpenRecordset(strSQL, modGlobalVariables.Statics.strCommitDB);
					for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
					{
						if (clsCurrent.FindNextRecord("rsaccount", Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT))))
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(clsCurrent.Get_Fields_String("rsmaplot")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(clsCurrent.Get_Fields_String("rsname")));
						}
					}
					// lngRow
				}
				txtPreviousVal.Text = Strings.Format(dblTotalOldVal, "#,###,###,##0");
				txtCurrentVal.Text = Strings.Format(dblTotalCurVal, "#,###,###,##0");
				txtTotalNewValuation.Text = Strings.Format(dblTotalNewVal, "#,###,###,##0");
				ReCalcTotals();
				frmWait.InstancePtr.Unload();
                modColorScheme.ColorGrid(GridSavedDetail);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdBack_Click(object sender, System.EventArgs e)
		{
			int lngAcct;
			int x;
			int lngTotal;
			int lngOldTotal;
			int lngDetailRow;
			try
			{
				// On Error GoTo ErrorHandler
				GridDetail.Row = 0;
				GridDetail.Col = 0;
				// update any changes
				lngTotal = 0;
				if (GridDetail.Rows > 1)
				{
					for (x = 1; x <= GridDetail.Rows - 1; x++)
					{
						// only take the top level rows.  The others aren't supposed to be used for values
						if (GridDetail.RowOutlineLevel(x) == 0)
						{
							if (GridDetail.TextMatrix(x, CNSTGRIDDETAILCOLUSE) != string.Empty)
							{
								if (FCConvert.CBool(GridDetail.TextMatrix(x, CNSTGRIDDETAILCOLUSE)))
								{
									if (Conversion.Val(GridDetail.TextMatrix(x, CNSTGRIDDETAILCOLVALUATION)) > 0)
									{
										lngTotal += FCConvert.ToInt32(FCConvert.ToDouble(GridDetail.TextMatrix(x, CNSTGRIDDETAILCOLVALUATION)));
									}
								}
							}
							// save in gridsaveddetail
							lngDetailRow = FCConvert.ToInt32(Math.Round(Conversion.Val(GridDetail.TextMatrix(x, CNSTGRIDDETAILCOLROWNUM))));
							if (Conversion.Val(GridDetail.TextMatrix(x, CNSTGRIDDETAILCOLVALUATION)) > 0)
							{
								GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(GridDetail.TextMatrix(x, CNSTGRIDDETAILCOLVALUATION)))));
							}
							else
							{
								GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLVALUATION, FCConvert.ToString(0));
							}
							if (GridDetail.TextMatrix(x, CNSTGRIDDETAILCOLUSE) != string.Empty)
							{
								if (FCConvert.CBool(GridDetail.TextMatrix(x, CNSTGRIDDETAILCOLUSE)))
								{
									GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(true));
								}
								else
								{
									GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
								}
							}
							else
							{
								GridSavedDetail.TextMatrix(lngDetailRow, CNSTGRIDDETAILCOLUSE, FCConvert.ToString(false));
							}
						}
					}
					// x
				}
				lngOldTotal = 0;
				if (Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLNEWVALUATION)) > 0)
				{
					lngOldTotal = FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLNEWVALUATION)));
				}
				Grid.TextMatrix(Grid.Row, CNSTGRIDCOLNEWVALUATION, Strings.Format(lngTotal, "#,###,###,##0"));
				dblTotalNewVal -= lngOldTotal;
				dblTotalNewVal += lngTotal;
				txtTotalNewValuation.Text = Strings.Format(dblTotalNewVal, "#,###,###,###,##0");
				ReCalcTotals();
				// now save in gridsaveddetail
				framDetail.Visible = false;
				cmdDelete.Visible = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In cmdBack_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				GridDetail.Row = 0;
				GridDetail.Col = 0;
				framDetail.Visible = false;
				cmdDelete.Visible = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In cmdCancel_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmNewValuationAnalysis_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmNewValuationAnalysis_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmNewValuationAnalysis properties;
			//frmNewValuationAnalysis.FillStyle	= 0;
			//frmNewValuationAnalysis.ScaleWidth	= 9300;
			//frmNewValuationAnalysis.ScaleHeight	= 7620;
			//frmNewValuationAnalysis.LinkTopic	= "Form2";
			//frmNewValuationAnalysis.LockControls	= true;
			//frmNewValuationAnalysis.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			SetupGridDetail();
			LoadGrid();
			if (boolAutoChoose)
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				Label4.Visible = false;
				Shape1.Visible = false;
			}
			GridDetail.OutlineBar = FCGrid.OutlineBarSettings.flexOutlineBarSimple;
			//GridDetail.OutlineCol = CNSTGRIDDETAILCOLDESCRIPTION;
			GridSavedDetail.OutlineBar = FCGrid.OutlineBarSettings.flexOutlineBarSimple;
		}

		private void frmNewValuationAnalysis_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridDetail();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
			{
				return;
			}
			if (modREMain.Statics.boolShortRealEstate || !boolAutoChoose)
				return;
			cmdDelete.Visible = false;
			ShowValuationDetail(FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLACCOUNT)));
		}

		private void SetupGridDetail()
		{
			GridDetail.Rows = 1;
			GridDetail.Cols = 8;
			GridDetail.ColHidden(CNSTGRIDDETAILCOLACCOUNT, true);
			GridDetail.ColHidden(CNSTGRIDDETAILCOLROWNUM, true);
			GridDetail.TextMatrix(0, CNSTGRIDDETAILCOLDESCRIPTION, "Description");
			GridDetail.TextMatrix(0, CNSTGRIDDETAILCOLDETAIL, "");
			GridDetail.TextMatrix(0, CNSTGRIDDETAILCOLPREVIOUS, "Previous");
			GridDetail.TextMatrix(0, CNSTGRIDDETAILCOLCURRENT, "Current");
			GridDetail.TextMatrix(0, CNSTGRIDDETAILCOLUSE, "Use");
			GridDetail.TextMatrix(0, CNSTGRIDDETAILCOLVALUATION, "Valuation");
			GridDetail.ColDataType(CNSTGRIDDETAILCOLUSE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridSavedDetail.Rows = 0;
			GridSavedDetail.Cols = GridDetail.Cols;
			GridSavedDetail.ColDataType(CNSTGRIDDETAILCOLUSE, FCGrid.DataTypeSettings.flexDTBoolean);
		}

		private void ResizeGridDetail()
		{
			int GridWidth = 0;
			GridWidth = GridDetail.WidthOriginal;
			GridDetail.ColWidth(CNSTGRIDDETAILCOLDESCRIPTION, FCConvert.ToInt32(0.3 * GridWidth));
			GridDetail.ColWidth(CNSTGRIDDETAILCOLDETAIL, FCConvert.ToInt32(0.3 * GridWidth));
			GridDetail.ColWidth(CNSTGRIDDETAILCOLPREVIOUS, FCConvert.ToInt32(0.1 * GridWidth));
			GridDetail.ColWidth(CNSTGRIDDETAILCOLCURRENT, FCConvert.ToInt32(0.1 * GridWidth));
			GridDetail.ColWidth(CNSTGRIDDETAILCOLUSE, FCConvert.ToInt32(0.06 * GridWidth));
		}
		// vbPorter upgrade warning: lngAcct As int	OnWrite(string)
		private void ShowValuationDetail(int lngAcct)
		{
			// fill grid will all the details of the changes
			int lngRow = 0;
			int lngDRow;
			int x;
			// Dim clsLoad As New clsDRWrapper
			framDetail.Visible = true;
			framDetail.BringToFront();
			GridDetail.Rows = 1;
			lblAccount.Text = Grid.TextMatrix(Grid.Row, CNSTGRIDCOLACCOUNT);
			lblMapLot.Text = Grid.TextMatrix(Grid.Row, CNSTGRIDCOLMAPLOT);
			lblName.Text = Grid.TextMatrix(Grid.Row, CNSTGRIDCOLNAME);
			// Call clsLoad.OpenRecordset("select rsmaplot,rsname from master where rscard = 1 and rsaccount = " & Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLACCOUNT)), strREDatabase)
			// If Not clsLoad.EndOfFile Then
			// lblMapLot.Caption = clsLoad.Fields("rsmaplot")
			// lblName.Caption = clsLoad.Fields("rsname")
			// Else
			// lblMapLot.Caption = ""
			// lblName.Caption = ""
			// End If
			if (GridSavedDetail.Rows > 0)
			{
				lngRow = GridSavedDetail.FindRow(lngAcct, 0, CNSTGRIDDETAILCOLACCOUNT);
				while (lngRow >= 0)
				{
					// copy info over
					GridDetail.Rows += 1;
					lngDRow = GridDetail.Rows - 1;
					GridDetail.RowOutlineLevel(lngDRow, GridSavedDetail.RowOutlineLevel(lngRow));
					GridDetail.IsSubtotal(lngDRow, true);
					GridDetail.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngDRow, 0, lngDRow, CNSTGRIDDETAILCOLCURRENT, Color.White);
					for (x = 0; x <= GridDetail.Cols - 1; x++)
					{
						GridDetail.TextMatrix(lngDRow, x, GridSavedDetail.TextMatrix(lngRow, x));
					}
					// x
					// point to row so we can save changes if need be
					GridDetail.TextMatrix(lngDRow, CNSTGRIDDETAILCOLROWNUM, FCConvert.ToString(lngRow));
					if (lngRow < GridSavedDetail.Rows - 1)
					{
						lngRow = GridSavedDetail.FindRow(lngAcct, lngRow + 1, CNSTGRIDDETAILCOLACCOUNT);
					}
					else
					{
						lngRow = -1;
					}
				}
			}
            modColorScheme.ColorGrid(GridDetail);
		}

		private void SetupGrid()
		{
			Grid.Rows = 2;
			Grid.Cols = 10;
			Grid.FixedCols = 8;
			Grid.TextMatrix(0, CNSTGRIDCOLACCOUNT, "Account");
			Grid.TextMatrix(0, CNSTGRIDCOLMAPLOT, "MapLot");
			Grid.TextMatrix(0, CNSTGRIDCOLPREVLAND, "Prev Land");
			Grid.TextMatrix(0, CNSTGRIDCOLPREVBLDG, "Prev Bldg");
			Grid.TextMatrix(0, CNSTGRIDCOLPREVEXEMPTION, "Pr Exempt");
			Grid.TextMatrix(0, CNSTGRIDCOLCURLAND, "Land");
			Grid.TextMatrix(0, CNSTGRIDCOLCURBLDG, "Building");
			Grid.TextMatrix(0, CNSTGRIDCOLCUREXEMPTION, "Exemption");
			Grid.TextMatrix(0, CNSTGRIDCOLNEWVALUATION, "New Valuation");
			Grid.ColHidden(CNSTGRIDCOLNAME, true);
			Grid.RowHeight(0, 2 * Grid.RowHeight(1));
			Grid.Rows = 1;
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLACCOUNT, FCConvert.ToInt32(0.08 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLMAPLOT, FCConvert.ToInt32(0.16 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLPREVLAND, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLPREVBLDG, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLPREVEXEMPTION, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCURLAND, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCURBLDG, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCUREXEMPTION, FCConvert.ToInt32(0.1 * GridWidth));
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (Grid.Row <= 0)
				return;
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						// delete current row
						mnuDeleteEntry_Click();
						break;
					}
			}
			//end switch
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            int lngDifference = 0;
			int lngOld = 0;
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = Grid.GetFlexRowIndex(e.RowIndex);
			int col = Grid.GetFlexColIndex(e.ColumnIndex);
			try
			{
				// On Error GoTo ErrorHandler
				if (row < 1)
					return;
				switch (col)
				{
					case CNSTGRIDCOLNEWVALUATION:
						{
							if (boolAutoChoose)
								return;
							lngOld = 0;
							if (Conversion.Val(Grid.TextMatrix(row, CNSTGRIDCOLNEWVALUATION)) > 0)
							{
								lngOld = FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(row, CNSTGRIDCOLNEWVALUATION)));
							}
							lngDifference = 0;
							if (Conversion.Val(Grid.EditText) > 0)
							{
								lngDifference = FCConvert.ToInt32(FCConvert.ToDouble(Grid.EditText));
							}
							lngDifference -= lngOld;
							dblTotalNewVal += lngDifference;
							txtTotalNewValuation.Text = Strings.Format(dblTotalNewVal, "#,###,###,###,##0");
							// recalctotals
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Grid_ValidateEdit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuDeleteEntry_Click(object sender, System.EventArgs e)
		{
			int lngAcct;
			int lngRow;
			int lngNVal;
			int lngPVal;
			int lngCVal;
			// remove the row and remove corresponding entries in griddetail
			if (Grid.Row <= 0)
				return;
			Grid.Col = 0;
			lngRow = Grid.Row;
			lngPVal = 0;
			lngCVal = 0;
			lngNVal = 0;
			if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVLAND)) > 0)
			{
				lngPVal += FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVLAND)));
			}
			if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVBLDG)) > 0)
			{
				lngPVal += FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLPREVBLDG)));
			}
			if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCURBLDG)) > 0)
			{
				lngCVal += FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLCURBLDG)));
			}
			if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCURLAND)) > 0)
			{
				lngCVal += FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLCURLAND)));
			}
			if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLNEWVALUATION)) > 0)
			{
				lngNVal = FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLNEWVALUATION)));
			}
			dblTotalCurVal -= lngCVal;
			dblTotalOldVal -= lngPVal;
			dblTotalNewVal -= lngNVal;
			txtCurrentVal.Text = Strings.Format(dblTotalCurVal, "#,###,###,###,##0");
			txtPreviousVal.Text = Strings.Format(dblTotalOldVal, "#,###,###,###,##0");
			txtTotalNewValuation.Text = Strings.Format(dblTotalNewVal, "#,##,###,###,##0");
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT))));
			Grid.RemoveItem(lngRow);
			lngRow = GridSavedDetail.FindRow(lngAcct, -1, CNSTGRIDDETAILCOLACCOUNT);
			while (lngRow > 0)
			{
				GridSavedDetail.RemoveItem(lngRow);
				if (lngRow < GridSavedDetail.Rows - 1)
				{
					lngRow = GridSavedDetail.FindRow(lngAcct, lngRow, CNSTGRIDDETAILCOLACCOUNT);
				}
				else
				{
					lngRow = -1;
				}
			}
		}

		public void mnuDeleteEntry_Click()
		{
			mnuDeleteEntry_Click(cmdDelete, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			rptNewValEstimate.InstancePtr.Init(ref lngLatestYear, ref lngPreviousYear, this.Modal);
		}

		private void ReCalcTotals()
		{
			int lngRow;
			double dblTotal;
			dblTotal = 0;
			for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
			{
				if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLNEWVALUATION)) > 0)
				{
					dblTotal += FCConvert.ToInt32(FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLNEWVALUATION)));
				}
			}
			// lngRow
			txtTotalNewValuation.Text = Strings.Format(dblTotal, "#,###,###,##0");
		}
	}
}
