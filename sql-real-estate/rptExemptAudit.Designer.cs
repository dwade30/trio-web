﻿using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptExemptAudit.
	/// </summary>
	partial class rptExemptAudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptExemptAudit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			//this.DetailGrid = new GrapeCity.ActiveReports.SectionReportModel.CustomControl();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.txtTotAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotAccts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotTotExempts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtmuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtdate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtpage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtorderby = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.txtGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtSubAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSubLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumAccts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalExempts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			//((System.ComponentModel.ISupportInitialize)(this.DetailGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAccts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTotExempts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtorderby)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumAccts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExempts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAssessment,
				this.txtExempt,
				this.txtBldg,
				this.txtLand,
				this.txtAccount,
				this.txtName,
				this.txtMapLot
			});
			this.Detail.Height = 0.4479167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtAssessment
			// 
			this.txtAssessment.Height = 0.1875F;
			this.txtAssessment.Left = 6.375F;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 0.1875F;
			this.txtAssessment.Width = 1.0625F;
			// 
			// txtExempt
			// 
			this.txtExempt.Height = 0.1875F;
			this.txtExempt.Left = 5.25F;
			this.txtExempt.Name = "txtExempt";
			this.txtExempt.Style = "text-align: right";
			this.txtExempt.Text = null;
			this.txtExempt.Top = 0.1875F;
			this.txtExempt.Width = 1.0625F;
			// 
			// txtBldg
			// 
			this.txtBldg.Height = 0.1875F;
			this.txtBldg.Left = 3.9375F;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0.1875F;
			this.txtBldg.Width = 1.25F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.1875F;
			this.txtLand.Left = 2.75F;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Text = null;
			this.txtLand.Top = 0.1875F;
			this.txtLand.Width = 1.125F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.5F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.5625F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 2.25F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.1875F;
			this.txtMapLot.Left = 0.5625F;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0.1875F;
			this.txtMapLot.Width = 1.8125F;
			// 
			// ReportHeader
			// 
			//this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
			//         this.DetailGrid});
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// DetailGrid
			// 
			//this.DetailGrid.Height = 0.125F;
			//this.DetailGrid.Left = 0.75F;
			//this.DetailGrid.Name = "DetailGrid";
			//this.DetailGrid.Top = 0.0625F;
			//this.DetailGrid.Type = typeof(fecherFoundation.FCGrid);
			//this.DetailGrid.Width = 3.875F;
			// 
			// ReportFooter
			//
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotAssessment,
				this.txtTotExempt,
				this.txtTotBldg,
				this.txtTotLand,
				this.Label8,
				this.Label9,
				this.txtTotAccts,
				this.Label10,
				this.txtTotTotExempts,
				this.Line2
			});
			this.ReportFooter.Height = 0.6666667F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// txtTotAssessment
			// 
			this.txtTotAssessment.Height = 0.1875F;
			this.txtTotAssessment.Left = 6.375F;
			this.txtTotAssessment.Name = "txtTotAssessment";
			this.txtTotAssessment.Style = "text-align: right";
			this.txtTotAssessment.Text = null;
			this.txtTotAssessment.Top = 0.25F;
			this.txtTotAssessment.Width = 1.0625F;
			// 
			// txtTotExempt
			// 
			this.txtTotExempt.Height = 0.1875F;
			this.txtTotExempt.Left = 5.1875F;
			this.txtTotExempt.Name = "txtTotExempt";
			this.txtTotExempt.Style = "text-align: right";
			this.txtTotExempt.Text = null;
			this.txtTotExempt.Top = 0.25F;
			this.txtTotExempt.Width = 1.125F;
			// 
			// txtTotBldg
			// 
			this.txtTotBldg.Height = 0.1875F;
			this.txtTotBldg.Left = 3.9375F;
			this.txtTotBldg.Name = "txtTotBldg";
			this.txtTotBldg.Style = "text-align: right";
			this.txtTotBldg.Text = null;
			this.txtTotBldg.Top = 0.25F;
			this.txtTotBldg.Width = 1.25F;
			// 
			// txtTotLand
			// 
			this.txtTotLand.Height = 0.1875F;
			this.txtTotLand.Left = 2.75F;
			this.txtTotLand.Name = "txtTotLand";
			this.txtTotLand.Style = "text-align: right";
			this.txtTotLand.Text = null;
			this.txtTotLand.Top = 0.25F;
			this.txtTotLand.Width = 1.125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.0625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "";
			this.Label8.Text = "Total";
			this.Label8.Top = 0.25F;
			this.Label8.Width = 0.75F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "";
			this.Label9.Text = "Accounts";
			this.Label9.Top = 0.4375F;
			this.Label9.Width = 0.75F;
			// 
			// txtTotAccts
			// 
			this.txtTotAccts.Height = 0.1875F;
			this.txtTotAccts.Left = 0.8125F;
			this.txtTotAccts.Name = "txtTotAccts";
			this.txtTotAccts.Text = null;
			this.txtTotAccts.Top = 0.4375F;
			this.txtTotAccts.Width = 0.6875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "";
			this.Label10.Text = "Totally Exempt";
			this.Label10.Top = 0.4375F;
			this.Label10.Width = 1.125F;
			// 
			// txtTotTotExempts
			// 
			this.txtTotTotExempts.Height = 0.1875F;
			this.txtTotTotExempts.Left = 2.75F;
			this.txtTotTotExempts.Name = "txtTotTotExempts";
			this.txtTotTotExempts.Text = null;
			this.txtTotTotExempts.Top = 0.4375F;
			this.txtTotTotExempts.Width = 0.6875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.0625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.1875F;
			this.Line2.Width = 7.375F;
			this.Line2.X1 = 0.0625F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 0.1875F;
			this.Line2.Y2 = 0.1875F;
			// 
			// PageHeader
			//
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.txtmuni,
				this.Field2,
				this.txtdate,
				this.txtpage,
				this.txtorderby,
				this.txtTime
			});
			this.PageHeader.Height = 0.6875F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 3.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Label1.Text = "-LAND-";
			this.Label1.Top = 0.4375F;
			this.Label1.Width = 0.8125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 4F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Label2.Text = "-BUILDINGS-";
			this.Label2.Top = 0.4375F;
			this.Label2.Width = 1.1875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 5.3125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
			this.Label3.Text = "-EXEMPTION-";
			this.Label3.Top = 0.4375F;
			this.Label3.Width = 1F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.4375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label4.Text = "ASSESSMENT";
			this.Label4.Top = 0.4375F;
			this.Label4.Width = 1F;
			// 
			// txtmuni
			// 
			this.txtmuni.Height = 0.1875F;
			this.txtmuni.Left = 0.0625F;
			this.txtmuni.Name = "txtmuni";
			this.txtmuni.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtmuni.Text = null;
			this.txtmuni.Top = 0F;
			this.txtmuni.Width = 1.4375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 2.125F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field2.Text = "AUDIT OF BILLING AMOUNTS";
			this.Field2.Top = 0F;
			this.Field2.Width = 3.125F;
			// 
			// txtdate
			// 
			this.txtdate.Height = 0.1875F;
			this.txtdate.Left = 6.25F;
			this.txtdate.Name = "txtdate";
			this.txtdate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtdate.Text = null;
			this.txtdate.Top = 0F;
			this.txtdate.Width = 1.1875F;
			// 
			// txtpage
			// 
			this.txtpage.Height = 0.1875F;
			this.txtpage.Left = 6.25F;
			this.txtpage.Name = "txtpage";
			this.txtpage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtpage.Text = null;
			this.txtpage.Top = 0.1875F;
			this.txtpage.Width = 1.1875F;
			// 
			// txtorderby
			// 
			this.txtorderby.Height = 0.1875F;
			this.txtorderby.Left = 2.25F;
			this.txtorderby.Name = "txtorderby";
			this.txtorderby.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.txtorderby.Text = null;
			this.txtorderby.Top = 0.1875F;
			this.txtorderby.Width = 2.875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroup
			});
			this.GroupHeader1.Height = 0.2083333F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.DataField = "EndData";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// txtGroup
			// 
			this.txtGroup.Height = 0.1875F;
			this.txtGroup.Left = 2F;
			this.txtGroup.Name = "txtGroup";
			this.txtGroup.Style = "text-align: center";
			this.txtGroup.Text = null;
			this.txtGroup.Top = 0F;
			this.txtGroup.Width = 3.5625F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtSubAssessment,
				this.txtSubExemption,
				this.txtSubBldg,
				this.txtSubLand,
				this.Label5,
				this.Label6,
				this.txtNumAccts,
				this.Label7,
				this.txtTotalExempts,
				this.Line1
			});
			this.GroupFooter1.Height = 0.46875F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtSubAssessment
			// 
			this.txtSubAssessment.Height = 0.1875F;
			this.txtSubAssessment.Left = 6.375F;
			this.txtSubAssessment.Name = "txtSubAssessment";
			this.txtSubAssessment.Style = "text-align: right";
			this.txtSubAssessment.Text = null;
			this.txtSubAssessment.Top = 0.0625F;
			this.txtSubAssessment.Width = 1.0625F;
			// 
			// txtSubExemption
			// 
			this.txtSubExemption.Height = 0.1875F;
			this.txtSubExemption.Left = 5.25F;
			this.txtSubExemption.Name = "txtSubExemption";
			this.txtSubExemption.Style = "text-align: right";
			this.txtSubExemption.Text = null;
			this.txtSubExemption.Top = 0.0625F;
			this.txtSubExemption.Width = 1.0625F;
			// 
			// txtSubBldg
			// 
			this.txtSubBldg.Height = 0.1875F;
			this.txtSubBldg.Left = 3.9375F;
			this.txtSubBldg.Name = "txtSubBldg";
			this.txtSubBldg.Style = "text-align: right";
			this.txtSubBldg.Text = null;
			this.txtSubBldg.Top = 0.0625F;
			this.txtSubBldg.Width = 1.25F;
			// 
			// txtSubLand
			// 
			this.txtSubLand.Height = 0.1875F;
			this.txtSubLand.Left = 2.75F;
			this.txtSubLand.Name = "txtSubLand";
			this.txtSubLand.Style = "text-align: right";
			this.txtSubLand.Text = null;
			this.txtSubLand.Top = 0.0625F;
			this.txtSubLand.Width = 1.125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "";
			this.Label5.Text = "Subtotal";
			this.Label5.Top = 0.0625F;
			this.Label5.Width = 0.75F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "";
			this.Label6.Text = "Accounts";
			this.Label6.Top = 0.25F;
			this.Label6.Width = 0.75F;
			// 
			// txtNumAccts
			// 
			this.txtNumAccts.Height = 0.1875F;
			this.txtNumAccts.Left = 0.8125F;
			this.txtNumAccts.Name = "txtNumAccts";
			this.txtNumAccts.Text = null;
			this.txtNumAccts.Top = 0.25F;
			this.txtNumAccts.Width = 0.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 1.625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "";
			this.Label7.Text = "Totally Exempt";
			this.Label7.Top = 0.25F;
			this.Label7.Width = 1.125F;
			// 
			// txtTotalExempts
			// 
			this.txtTotalExempts.Height = 0.1875F;
			this.txtTotalExempts.Left = 2.75F;
			this.txtTotalExempts.Name = "txtTotalExempts";
			this.txtTotalExempts.Text = null;
			this.txtTotalExempts.Top = 0.25F;
			this.txtTotalExempts.Width = 0.6875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 2.75F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 4.6875F;
			this.Line1.X1 = 2.75F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 0F;
			// 
			// rptExemptAudit
			//
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			//((System.ComponentModel.ISupportInitialize)(this.DetailGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotAccts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotTotExempts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtorderby)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSubLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumAccts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExempts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		//public GrapeCity.ActiveReports.SectionReportModel.CustomControl DetailGrid;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotAccts;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotTotExempts;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtdate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtpage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtorderby;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSubLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumAccts;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalExempts;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
	}
}
