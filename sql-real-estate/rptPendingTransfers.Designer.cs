﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptPendingTransfers.
	partial class srptPendingTransfers
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptPendingTransfers));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAction = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtXfer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMinDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAction)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtXfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMinDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtAction,
				this.txtXfer,
				this.txtMinDate,
				this.txtSaleDate
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field13
			});
			this.ReportHeader.Height = 0.3229167F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field11,
				this.Field12
			});
			this.GroupHeader1.Height = 0.4479167F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 2F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field13.Text = "Processed Pending Transfers";
			this.Field13.Top = 0.125F;
			this.Field13.Width = 3.5625F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.19F;
			this.Field8.Left = 0F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Field8.Text = "Account";
			this.Field8.Top = 0.25F;
			this.Field8.Width = 0.75F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.19F;
			this.Field9.Left = 0.8125F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Field9.Text = "Action";
			this.Field9.Top = 0.25F;
			this.Field9.Width = 1.375F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.34375F;
			this.Field10.Left = 4.5F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field10.Text = "Transfer Account";
			this.Field10.Top = 0.09375F;
			this.Field10.Width = 1F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.34375F;
			this.Field11.Left = 5.5F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field11.Text = "Min Transfer Date";
			this.Field11.Top = 0.09375F;
			this.Field11.Width = 1F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.19F;
			this.Field12.Left = 6.5F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field12.Text = "Sale Date";
			this.Field12.Top = 0.25F;
			this.Field12.Width = 0.9375F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.03125F;
			this.txtAccount.Width = 0.75F;
			// 
			// txtAction
			// 
			this.txtAction.CanShrink = true;
			this.txtAction.Height = 0.19F;
			this.txtAction.Left = 0.8125F;
			this.txtAction.Name = "txtAction";
			this.txtAction.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtAction.Text = null;
			this.txtAction.Top = 0.03125F;
			this.txtAction.Width = 3.75F;
			// 
			// txtXfer
			// 
			this.txtXfer.Height = 0.19F;
			this.txtXfer.Left = 4.625F;
			this.txtXfer.Name = "txtXfer";
			this.txtXfer.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtXfer.Text = null;
			this.txtXfer.Top = 0.03125F;
			this.txtXfer.Width = 0.75F;
			// 
			// txtMinDate
			// 
			this.txtMinDate.CanGrow = false;
			this.txtMinDate.Height = 0.19F;
			this.txtMinDate.Left = 5.5625F;
			this.txtMinDate.Name = "txtMinDate";
			this.txtMinDate.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtMinDate.Text = null;
			this.txtMinDate.Top = 0.03125F;
			this.txtMinDate.Width = 0.875F;
			// 
			// txtSaleDate
			// 
			this.txtSaleDate.CanGrow = false;
			this.txtSaleDate.Height = 0.19F;
			this.txtSaleDate.Left = 6.5F;
			this.txtSaleDate.Name = "txtSaleDate";
			this.txtSaleDate.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtSaleDate.Text = null;
			this.txtSaleDate.Top = 0.03125F;
			this.txtSaleDate.Width = 0.9375F;
			// 
			// srptPendingTransfers
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAction)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtXfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMinDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAction;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtXfer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMinDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleDate;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
