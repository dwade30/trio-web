﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptAssessmentComparison.
	/// </summary>
	public partial class rptAssessmentComparison : BaseSectionReport
	{
		public static rptAssessmentComparison InstancePtr
		{
			get
			{
				return (rptAssessmentComparison)Sys.GetInstance(typeof(rptAssessmentComparison));
			}
		}

		protected rptAssessmentComparison _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}

		public rptAssessmentComparison()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Assessment Comparison Report";
		}
		// nObj = 1
		//   0	rptAssessmentComparison	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolOnlyChanges;
		clsDRWrapper clsLoad = new clsDRWrapper();
		// vbPorter upgrade warning: lngTotalCurrLand As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalCurrLand;
		// vbPorter upgrade warning: lngTotalCurrBldg As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalCurrBldg;
		// vbPorter upgrade warning: lngTotalCurrAssess As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalCurrAssess;
		// vbPorter upgrade warning: lngTotalBillLand As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalBillLand;
		// vbPorter upgrade warning: lngTotalBillBldg As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalBillBldg;
		// vbPorter upgrade warning: lngTotalBillAssess As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalBillAssess;
		// vbPorter upgrade warning: lngTotalBillExempt As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalBillExempt;
		// vbPorter upgrade warning: lngTotalcurrExempt As Decimal	OnWrite(short, Decimal)
		Decimal lngTotalcurrExempt;
		bool boolUseMaplot;
		bool boolUseRange;
		bool boolFromExtract;
		string strMinRange = "";
		string strMaxRange = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strGroupBy = "";
			string strField = "";
			string strWhere = "";
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngTotalCurrLand = 0;
			lngTotalCurrBldg = 0;
			lngTotalCurrAssess = 0;
			lngTotalBillLand = 0;
			lngTotalBillBldg = 0;
			lngTotalBillAssess = 0;
			lngTotalBillExempt = 0;
			lngTotalcurrExempt = 0;
			if (boolUseMaplot)
			{
				strGroupBy = " rsmaplot,rsaccount";
				lblAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				txtAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				lblAccount.Text = "Map / Lot";
				strField = "rsmaplot";
				strWhere = " and trim(rsmaplot & '') <> '' ";
			}
			else
			{
				strGroupBy = " rsaccount";
				lblAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				txtAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				lblAccount.Text = "Account";
				strField = "rsaccount";
				strWhere = "";
			}
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolUseRange)
			{
				strWhere += " and ";
				if (boolUseMaplot)
				{
					strWhere += " rsmaplot between '" + strMinRange + "' and '" + strMaxRange + "' ";
				}
				else
				{
					strWhere += " rsaccount between " + FCConvert.ToString(Conversion.Val(strMinRange)) + " and " + FCConvert.ToString(Conversion.Val(strMaxRange)) + " ";
				}
			}
			else if (boolFromExtract)
			{
				strWhere += " and ";
				strWhere += " rsaccount in (select extracttable.accountnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " + FCConvert.ToString(lngUID) + ")";
				clsLoad.OpenRecordset("select TITLE FROM EXTRACT WHERe reportnumber = 0 and userid = " + FCConvert.ToString(lngUID), modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					if (FCConvert.ToString(clsLoad.Get_Fields_String("title")) != string.Empty)
					{
						lblTitle2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("title"));
					}
				}
			}
			// If Not boolUseMaplot Then
			// If boolOnlyChanges Then
			// Call clsLoad.OpenRecordset("select rsaccount,sum(lastlandval) as BillLand,sum(lastbldgval) as BillBldg,sum(rlexemption) as BillExempt,sum(rllandval) as currland,sum(rlbldgval) as CurrBldg,sum(correxemption) as CurrExempt from master where not rsdeleted = 1 group by rsaccount having (sum(rllandval & '') + sum(rlbldgval & '') - sum(correxemption & '')) <> (sum(lastlandval & '') + sum(lastbldgval & '') - sum(correxemption & '')) order by rsaccount", strREDatabase)
			// Else
			// Call clsLoad.OpenRecordset("select rsaccount,sum(lastlandval) as BillLand,sum(lastbldgval) as billBldg,sum(rlexemption) as Billexempt,sum(rllandval) as currland,sum(rlbldgval) as currbldg,sum(correxemption) as currexempt from master where not rsdeleted = 1 group by rsaccount order by rsaccount", strREDatabase)
			// End If
			// Else
			if (boolOnlyChanges)
			{
				clsLoad.OpenRecordset("select " + strField + ",sum(lastlandval) as BillLand,sum(lastbldgval) as BillBldg,sum(rlexemption) as BillExempt,sum(rllandval) as currland,sum(rlbldgval) as CurrBldg,sum(correxemption) as CurrExempt from master where not rsdeleted = 1 " + strWhere + " group by " + strGroupBy + " having (sum(rllandval & '') + sum(rlbldgval & '') - sum(correxemption & '')) <> (sum(lastlandval & '') + sum(lastbldgval & '') - sum(correxemption & '')) order by " + strField, modGlobalVariables.strREDatabase);
			}
			else
			{
				clsLoad.OpenRecordset("select " + strField + ",sum(lastlandval) as BillLand,sum(lastbldgval) as billBldg,sum(rlexemption) as Billexempt,sum(rllandval) as currland,sum(rlbldgval) as currbldg,sum(correxemption) as currexempt from master where not rsdeleted = 1 " + strWhere + " group by " + strGroupBy + " order by " + strField, modGlobalVariables.strREDatabase);
			}
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngCurrLand = 0;
			int lngCurrBldg = 0;
			int lngCurrAssess = 0;
			int lngBillLand = 0;
			int lngBillBldg = 0;
			int lngBillAssess = 0;
			int lngBillExempt = 0;
			int lngCurrExempt = 0;
			double dblIncDec = 0;
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [billland] not found!! (maybe it is an alias?)
				lngBillLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("billland"))));
				// TODO Get_Fields: Field [billbldg] not found!! (maybe it is an alias?)
				lngBillBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("billbldg"))));
				// TODO Get_Fields: Field [billexempt] not found!! (maybe it is an alias?)
				lngBillExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("billexempt"))));
				lngBillAssess = lngBillLand + lngBillBldg - lngBillExempt;
				lngTotalBillLand += lngBillLand;
				lngTotalBillBldg += lngBillBldg;
				lngTotalBillExempt += lngBillExempt;
				lngTotalBillAssess += lngBillAssess;
				// TODO Get_Fields: Field [currland] not found!! (maybe it is an alias?)
				lngCurrLand = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("currland"))));
				// TODO Get_Fields: Field [currbldg] not found!! (maybe it is an alias?)
				lngCurrBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("currbldg"))));
				// TODO Get_Fields: Field [currexempt] not found!! (maybe it is an alias?)
				lngCurrExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("currexempt"))));
				lngCurrAssess = lngCurrLand + lngCurrBldg - lngCurrExempt;
				lngTotalCurrLand += lngCurrLand;
				lngTotalCurrBldg += lngCurrBldg;
				lngTotalcurrExempt += lngCurrExempt;
				lngTotalCurrAssess += lngCurrAssess;
				if (lngBillAssess == lngCurrAssess)
				{
					txtIncDec.Text = "0% ";
				}
				else if (lngBillAssess > lngCurrAssess && lngBillAssess > 0)
				{
					if (lngCurrAssess == 0)
					{
						txtIncDec.Text = "100% -";
					}
					else
					{
						dblIncDec = FCConvert.ToDouble(lngCurrAssess) / lngBillAssess;
						dblIncDec *= 100;
						dblIncDec = 100 - dblIncDec;
						txtIncDec.Text = Strings.Format(dblIncDec, "##0.0") + "% -";
					}
				}
				else
				{
					if (lngBillAssess == 0)
					{
						txtIncDec.Text = "100% +";
					}
					else
					{
						dblIncDec = FCConvert.ToDouble(lngCurrAssess) / lngBillAssess;
						dblIncDec *= 100;
						dblIncDec -= 100;
						txtIncDec.Text = Strings.Format(dblIncDec, "##0.0") + "% +";
					}
				}
				if (!boolUseMaplot)
				{
					txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("rsaccount"));
				}
				else
				{
					txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields_String("rsmaplot"));
				}
				txtPrevLand.Text = Strings.Format(lngBillLand, "#,###,###,##0");
				txtPrevBldg.Text = Strings.Format(lngBillBldg, "#,###,###,##0");
				txtBillExempt.Text = Strings.Format(lngBillExempt, "#,###,###,##0");
				txtBillAssess.Text = Strings.Format(lngBillAssess, "#,###,###,##0");
				txtCurrLand.Text = Strings.Format(lngCurrLand, "#,###,###,##0");
				txtCurrBldg.Text = Strings.Format(lngCurrBldg, "#,###,###,##0");
				txtCurrExempt.Text = Strings.Format(lngCurrExempt, "#,###,###,##0");
				txtCurrTotal.Text = Strings.Format(lngCurrAssess, "#,###,###,##0");
				clsLoad.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		public void Init(bool boolChangesOnly, bool boolByMapLot = false, bool boolByRange = false, bool boolByExtract = false, string strmin = "", string strmax = "")
		{
			int intReturn;
			boolOnlyChanges = boolChangesOnly;
			boolUseMaplot = boolByMapLot;
			boolUseRange = boolByRange;
			boolFromExtract = boolByExtract;
			strMinRange = strmin;
			strMaxRange = strmax;
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                rsLoad.OpenRecordset("select * from status", modGlobalVariables.strREDatabase);
                if (!rsLoad.EndOfFile())
                {
                    if (Information.IsDate(rsLoad.Get_Fields("currentexemptions")))
                    {
                        if (rsLoad.Get_Fields_DateTime("currentexemptions") != DateTime.FromOADate(0))
                        {
                            bool boolAsk = false;
                            boolAsk = false;
                            if (Information.IsDate(rsLoad.Get_Fields("Batchcalc")))
                            {
                                if (rsLoad.Get_Fields_DateTime("batchcalc") != DateTime.FromOADate(0))
                                {
                                    if (DateAndTime.DateDiff("n", (DateTime) rsLoad.Get_Fields_DateTime("batchcalc"),
                                        (DateTime) rsLoad.Get_Fields_DateTime("currentexemptions")) < 0)
                                    {
                                        boolAsk = true;
                                    }
                                }
                            }

                            if (Information.IsDate(rsLoad.Get_Fields("calculated")))
                            {
                                if (rsLoad.Get_Fields_DateTime("calculated") != DateTime.FromOADate(0))
                                {
                                    if (DateAndTime.DateDiff("n", (DateTime) rsLoad.Get_Fields_DateTime("calculated"),
                                        (DateTime) rsLoad.Get_Fields_DateTime("currentexemptions")) < 0)
                                    {
                                        boolAsk = true;
                                    }
                                }
                            }

                            if (boolAsk)
                            {
                                if (MessageBox.Show(
                                    "Current exemptions have not been recalculated since account(s) have been recalculated" +
                                    "\r\n" + "Current exempt and assessment amounts may be incorrect" + "\r\n" +
                                    "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question) != DialogResult.Yes)
                                {
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (MessageBox.Show(
                            "Current exemptions have not been recalculated since account(s) have been recalculated" +
                            "\r\n" + "Current exempt and assessment amounts may be incorrect" + "\r\n" +
                            "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) != DialogResult.Yes)
                        {
                            return;
                        }
                    }
                }
            }

            frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "AssessmentComparison");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: dblIncDec As double	OnWrite(Decimal, double)
			double dblIncDec = 0;
			txtTotalPrevLand.Text = Strings.Format(lngTotalBillLand, "#,###,###,##0");
			txtTotalPrevBldg.Text = Strings.Format(lngTotalBillBldg, "#,###,###,##0");
			txtTotalBillExempt.Text = Strings.Format(lngTotalBillExempt, "#,###,###,##0");
			txtTotalCurrLand.Text = Strings.Format(lngTotalCurrLand, "#,###,###,##0");
			txtTotalCurrBldg.Text = Strings.Format(lngTotalCurrBldg, "#,###,###,##0");
			txtTotalTotal.Text = Strings.Format(lngTotalCurrAssess, "#,###,###,##0");
			txtTotalBillAssess.Text = Strings.Format(lngTotalBillAssess, "#,###,###,##0");
			txtTotalCurrExempt.Text = Strings.Format(lngTotalcurrExempt, "#,###,###,##0");
			if (lngTotalBillAssess == lngTotalCurrAssess)
			{
				txttotalIncDec.Text = "0% ";
			}
			else if (lngTotalBillAssess > lngTotalCurrAssess)
			{
				if (lngTotalCurrAssess == 0)
				{
					txttotalIncDec.Text = "100% -";
				}
				else
				{
					dblIncDec = FCConvert.ToDouble(lngTotalCurrAssess / lngTotalBillAssess);
					dblIncDec *= 100;
					dblIncDec = 100 - dblIncDec;
					txttotalIncDec.Text = Strings.Format(dblIncDec, "##0.0") + "% -";
				}
			}
			else
			{
				if (lngTotalBillAssess == 0)
				{
					txttotalIncDec.Text = "100% +";
				}
				else
				{
					dblIncDec = FCConvert.ToDouble(lngTotalCurrAssess / lngTotalBillAssess);
					dblIncDec *= 100;
					dblIncDec -= 100;
					txttotalIncDec.Text = Strings.Format(dblIncDec, "##0.0") + "% +";
				}
			}
		}

		
	}
}
