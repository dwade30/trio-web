﻿//Fecher vbPorter - Version 1.0.0.40
using Global;

namespace TWRE0000
{
	public class cLandSchedule
	{
		//=========================================================
		private string strDescription = string.Empty;
		private int lngTownCode;
		private int lngRecordID;
		private int lngScheduleNumber;
		private double dblStandardWidth;
		private double dblStandardDepth;
		private double dblStandardLot;
		private cGenericCollection collDetails = new cGenericCollection();
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public int TownCode
		{
			set
			{
				lngTownCode = value;
				IsUpdated = true;
			}
			get
			{
				int TownCode = 0;
				TownCode = lngTownCode;
				return TownCode;
			}
		}

		public int ScheduleNumber
		{
			set
			{
				lngScheduleNumber = value;
				IsUpdated = true;
			}
			get
			{
				int ScheduleNumber = 0;
				ScheduleNumber = lngScheduleNumber;
				return ScheduleNumber;
			}
		}

		public double StandardWidth
		{
			set
			{
				dblStandardWidth = value;
				IsUpdated = true;
			}
			get
			{
				double StandardWidth = 0;
				StandardWidth = dblStandardWidth;
				return StandardWidth;
			}
		}

		public double StandardDepth
		{
			set
			{
				dblStandardDepth = value;
				IsUpdated = true;
			}
			get
			{
				double StandardDepth = 0;
				StandardDepth = dblStandardDepth;
				return StandardDepth;
			}
		}

		public double StandardLot
		{
			set
			{
				dblStandardLot = value;
				IsUpdated = true;
			}
			get
			{
				double StandardLot = 0;
				StandardLot = dblStandardLot;
				return StandardLot;
			}
		}

		public cGenericCollection Details
		{
			get
			{
				cGenericCollection Details = null;
				Details = collDetails;
				return Details;
			}
		}
	}
}
