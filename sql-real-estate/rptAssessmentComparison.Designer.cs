﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptAssessmentComparison.
	/// </summary>
	partial class rptAssessmentComparison
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAssessmentComparison));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCurrLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPrevLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBillAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCurrExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBillExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtIncDec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCurrBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCurrLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalPrevBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalPrevLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalBillAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalCurrExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalBillExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotalIncDec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBillAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBillExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIncDec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPrevBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPrevLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBillAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBillExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotalIncDec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCurrTotal,
				this.txtCurrBldg,
				this.txtCurrLand,
				this.txtPrevBldg,
				this.txtPrevLand,
				this.txtAccount,
				this.txtBillAssess,
				this.Label13,
				this.txtCurrExempt,
				this.txtBillExempt,
				this.Label12,
				this.txtIncDec
			});
			this.Detail.Height = 0.4583333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label10,
				this.txtTotalTotal,
				this.txtTotalCurrBldg,
				this.txtTotalCurrLand,
				this.txtTotalPrevBldg,
				this.txtTotalPrevLand,
				this.txtTotalBillAssess,
				this.txtTotalCurrExempt,
				this.txtTotalBillExempt,
				this.txttotalIncDec
			});
			this.ReportFooter.Height = 0.625F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Label1,
				this.lblAccount,
				this.Label4,
				this.Label5,
				this.Label14,
				this.Label15,
				this.Label9,
				this.lblTitle2
			});
			this.PageHeader.Height = 0.8125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.HyperLink = null;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.25F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.HyperLink = null;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.21875F;
			this.txtTime.Width = 1.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.25F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.25F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.21875F;
			this.txtPage.Width = 1.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.25F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Assessment Comparison Report";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 4.9375F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.19F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-weight: bold; text-align: left";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.59375F;
			this.lblAccount.Width = 1.239583F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "Land";
			this.Label4.Top = 0.59375F;
			this.Label4.Width = 1F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.3125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: right";
			this.Label5.Text = "Building";
			this.Label5.Top = 0.59375F;
			this.Label5.Width = 1.0625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.572917F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Text = "Assessment";
			this.Label14.Top = 0.59375F;
			this.Label14.Width = 1.052083F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 4.4375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: right";
			this.Label15.Text = "Exemption";
			this.Label15.Top = 0.59375F;
			this.Label15.Width = 1.0625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 6.6875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Text = "Inc/Dec";
			this.Label9.Top = 0.59375F;
			this.Label9.Width = 0.75F;
			// 
			// lblTitle2
			// 
			this.lblTitle2.Height = 0.1875F;
			this.lblTitle2.HyperLink = null;
			this.lblTitle2.Left = 1.21875F;
			this.lblTitle2.Name = "lblTitle2";
			this.lblTitle2.Style = "text-align: center";
			this.lblTitle2.Text = null;
			this.lblTitle2.Top = 0.2395833F;
			this.lblTitle2.Width = 4.947917F;
			// 
			// txtCurrTotal
			// 
			this.txtCurrTotal.Height = 0.19F;
			this.txtCurrTotal.Left = 5.572917F;
			this.txtCurrTotal.Name = "txtCurrTotal";
			this.txtCurrTotal.Style = "text-align: right";
			this.txtCurrTotal.Text = null;
			this.txtCurrTotal.Top = 0.1875F;
			this.txtCurrTotal.Width = 1.052083F;
			// 
			// txtCurrBldg
			// 
			this.txtCurrBldg.Height = 0.19F;
			this.txtCurrBldg.Left = 3.25F;
			this.txtCurrBldg.Name = "txtCurrBldg";
			this.txtCurrBldg.Style = "text-align: right";
			this.txtCurrBldg.Text = null;
			this.txtCurrBldg.Top = 0.1875F;
			this.txtCurrBldg.Width = 1.125F;
			// 
			// txtCurrLand
			// 
			this.txtCurrLand.Height = 0.19F;
			this.txtCurrLand.Left = 2.239583F;
			this.txtCurrLand.Name = "txtCurrLand";
			this.txtCurrLand.Style = "text-align: right";
			this.txtCurrLand.Text = null;
			this.txtCurrLand.Top = 0.1875F;
			this.txtCurrLand.Width = 0.9479167F;
			// 
			// txtPrevBldg
			// 
			this.txtPrevBldg.Height = 0.19F;
			this.txtPrevBldg.Left = 3.25F;
			this.txtPrevBldg.Name = "txtPrevBldg";
			this.txtPrevBldg.Style = "text-align: right";
			this.txtPrevBldg.Text = null;
			this.txtPrevBldg.Top = 0.03125F;
			this.txtPrevBldg.Width = 1.125F;
			// 
			// txtPrevLand
			// 
			this.txtPrevLand.Height = 0.19F;
			this.txtPrevLand.Left = 2.239583F;
			this.txtPrevLand.Name = "txtPrevLand";
			this.txtPrevLand.Style = "text-align: right";
			this.txtPrevLand.Text = null;
			this.txtPrevLand.Top = 0.03125F;
			this.txtPrevLand.Width = 0.9479167F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "text-align: left";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0.03125F;
			this.txtAccount.Width = 1.239583F;
			// 
			// txtBillAssess
			// 
			this.txtBillAssess.Height = 0.19F;
			this.txtBillAssess.Left = 5.572917F;
			this.txtBillAssess.Name = "txtBillAssess";
			this.txtBillAssess.Style = "text-align: right";
			this.txtBillAssess.Text = null;
			this.txtBillAssess.Top = 0.03125F;
			this.txtBillAssess.Width = 1.052083F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.3125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: left";
			this.Label13.Text = "Current";
			this.Label13.Top = 0.1875F;
			this.Label13.Width = 0.875F;
			// 
			// txtCurrExempt
			// 
			this.txtCurrExempt.Height = 0.19F;
			this.txtCurrExempt.Left = 4.4375F;
			this.txtCurrExempt.Name = "txtCurrExempt";
			this.txtCurrExempt.Style = "text-align: right";
			this.txtCurrExempt.Text = null;
			this.txtCurrExempt.Top = 0.1875F;
			this.txtCurrExempt.Width = 1.0625F;
			// 
			// txtBillExempt
			// 
			this.txtBillExempt.Height = 0.19F;
			this.txtBillExempt.Left = 4.427083F;
			this.txtBillExempt.Name = "txtBillExempt";
			this.txtBillExempt.Style = "text-align: right";
			this.txtBillExempt.Text = null;
			this.txtBillExempt.Top = 0.03125F;
			this.txtBillExempt.Width = 1.072917F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 1.3125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: left";
			this.Label12.Text = "Billing";
			this.Label12.Top = 0.03125F;
			this.Label12.Width = 0.875F;
			// 
			// txtIncDec
			// 
			this.txtIncDec.CanGrow = false;
			this.txtIncDec.Height = 0.19F;
			this.txtIncDec.Left = 6.6875F;
			this.txtIncDec.Name = "txtIncDec";
			this.txtIncDec.Style = "text-align: right";
			this.txtIncDec.Text = null;
			this.txtIncDec.Top = 0.1875F;
			this.txtIncDec.Width = 0.75F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.0625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "Total";
			this.Label10.Top = 0.28125F;
			this.Label10.Width = 0.5625F;
			// 
			// txtTotalTotal
			// 
			this.txtTotalTotal.Height = 0.19F;
			this.txtTotalTotal.Left = 5.5625F;
			this.txtTotalTotal.Name = "txtTotalTotal";
			this.txtTotalTotal.Style = "text-align: right";
			this.txtTotalTotal.Text = null;
			this.txtTotalTotal.Top = 0.4375F;
			this.txtTotalTotal.Width = 1.0625F;
			// 
			// txtTotalCurrBldg
			// 
			this.txtTotalCurrBldg.Height = 0.19F;
			this.txtTotalCurrBldg.Left = 3.25F;
			this.txtTotalCurrBldg.Name = "txtTotalCurrBldg";
			this.txtTotalCurrBldg.Style = "text-align: right";
			this.txtTotalCurrBldg.Text = null;
			this.txtTotalCurrBldg.Top = 0.4375F;
			this.txtTotalCurrBldg.Width = 1.125F;
			// 
			// txtTotalCurrLand
			// 
			this.txtTotalCurrLand.Height = 0.19F;
			this.txtTotalCurrLand.Left = 2.1875F;
			this.txtTotalCurrLand.Name = "txtTotalCurrLand";
			this.txtTotalCurrLand.Style = "text-align: right";
			this.txtTotalCurrLand.Text = null;
			this.txtTotalCurrLand.Top = 0.4375F;
			this.txtTotalCurrLand.Width = 1F;
			// 
			// txtTotalPrevBldg
			// 
			this.txtTotalPrevBldg.Height = 0.19F;
			this.txtTotalPrevBldg.Left = 3.25F;
			this.txtTotalPrevBldg.Name = "txtTotalPrevBldg";
			this.txtTotalPrevBldg.Style = "text-align: right";
			this.txtTotalPrevBldg.Text = null;
			this.txtTotalPrevBldg.Top = 0.28125F;
			this.txtTotalPrevBldg.Width = 1.125F;
			// 
			// txtTotalPrevLand
			// 
			this.txtTotalPrevLand.Height = 0.19F;
			this.txtTotalPrevLand.Left = 2.197917F;
			this.txtTotalPrevLand.Name = "txtTotalPrevLand";
			this.txtTotalPrevLand.Style = "text-align: right";
			this.txtTotalPrevLand.Text = null;
			this.txtTotalPrevLand.Top = 0.28125F;
			this.txtTotalPrevLand.Width = 0.9895833F;
			// 
			// txtTotalBillAssess
			// 
			this.txtTotalBillAssess.Height = 0.19F;
			this.txtTotalBillAssess.Left = 5.5625F;
			this.txtTotalBillAssess.Name = "txtTotalBillAssess";
			this.txtTotalBillAssess.Style = "text-align: right";
			this.txtTotalBillAssess.Text = null;
			this.txtTotalBillAssess.Top = 0.28125F;
			this.txtTotalBillAssess.Width = 1.0625F;
			// 
			// txtTotalCurrExempt
			// 
			this.txtTotalCurrExempt.Height = 0.19F;
			this.txtTotalCurrExempt.Left = 4.447917F;
			this.txtTotalCurrExempt.Name = "txtTotalCurrExempt";
			this.txtTotalCurrExempt.Style = "text-align: right";
			this.txtTotalCurrExempt.Text = null;
			this.txtTotalCurrExempt.Top = 0.4375F;
			this.txtTotalCurrExempt.Width = 1.052083F;
			// 
			// txtTotalBillExempt
			// 
			this.txtTotalBillExempt.Height = 0.19F;
			this.txtTotalBillExempt.Left = 4.4375F;
			this.txtTotalBillExempt.Name = "txtTotalBillExempt";
			this.txtTotalBillExempt.Style = "text-align: right";
			this.txtTotalBillExempt.Text = null;
			this.txtTotalBillExempt.Top = 0.28125F;
			this.txtTotalBillExempt.Width = 1.0625F;
			// 
			// txttotalIncDec
			// 
			this.txttotalIncDec.CanGrow = false;
			this.txttotalIncDec.Height = 0.19F;
			this.txttotalIncDec.Left = 6.6875F;
			this.txttotalIncDec.Name = "txttotalIncDec";
			this.txttotalIncDec.Style = "text-align: right";
			this.txttotalIncDec.Text = null;
			this.txttotalIncDec.Top = 0.4375F;
			this.txttotalIncDec.Width = 0.75F;
			// 
			// rptAssessmentComparison
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPrevLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBillAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCurrExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBillExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIncDec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPrevBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalPrevLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBillAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCurrExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBillExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotalIncDec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPrevLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBillAssess;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCurrExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBillExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIncDec;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCurrBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCurrLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPrevBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalPrevLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBillAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCurrExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBillExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotalIncDec;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
