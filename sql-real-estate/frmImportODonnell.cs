﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Xml;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportODonnell.
	/// </summary>
	public partial class frmImportODonnell : BaseForm
	{
		public frmImportODonnell()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmImportODonnell InstancePtr
		{
			get
			{
				return (frmImportODonnell)Sys.GetInstance(typeof(frmImportODonnell));
			}
		}

		protected frmImportODonnell _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		XmlDocument docObj;
		// vbPorter upgrade warning: curAcct As MSXML2.IXMLDOMElement	OnWrite(MSXML2.IXMLDOMNode)
		XmlNode curAcct;
		bool boolUnload;
		const int CNSTGRIDCOLACCOUNT = 0;
		const int CNSTGRIDCOLNAME = 1;
		const int CNSTGRIDCOLMAPLOT = 2;
		const int CNSTGRIDCOLLAND = 3;
		const int CNSTGRIDCOLBLDG = 4;
		const int CNSTGRIDCOLEXEMPTION = 5;
		const int CNSTGRIDCOLNEW = 6;
		const int CNSTGRIDCOLTOTACRES = 7;
		const int CNSTGRIDCOLSOFTACRES = 8;
		const int CNSTGRIDCOLMIXEDACRES = 9;
		const int CNSTGRIDCOLHARDACRES = 10;
		const int CNSTGRIDCOLSOFTVALUE = 11;
		const int CNSTGRIDCOLMIXEDVALUE = 12;
		const int CNSTGRIDCOLHARDVALUE = 13;
		const int CNSTGRIDNOTUPDATEDCOLACCOUNT = 0;
		const int CNSTGRIDNOTUPDATEDCOLNAME = 1;
		const int CNSTGRIDNOTUPDATEDCOLMAPLOT = 2;
		const int CNSTGRIDNOTUPDATEDCOLLAND = 3;
		const int CNSTGRIDNOTUPDATEDCOLBLDG = 4;
		const int CNSTGRIDNOTUPDATEDCOLEXEMPTION = 5;
		const int CNSTGRIDNOTUPDATEDCOLTOTACRES = 6;
		const int CNSTGRIDNOTUPDATEDCOLSOFTACRES = 7;
		const int CNSTGRIDNOTUPDATEDCOLMIXEDACRES = 8;
		const int CNSTGRIDNOTUPDATEDCOLHARDACRES = 9;
		const int CNSTGRIDNOTUPDATEDCOLSOFTVALUE = 10;
		const int CNSTGRIDNOTUPDATEDCOLMIXEDVALUE = 11;
		const int CNSTGRIDNOTUPDATEDCOLHARDVALUE = 12;
		private bool boolFirstTime;
		private int lngUpdated;
		private int lngNew;
		private int lngTotal;
		private object[] arParties = null;
		PartyUtil tPu = new PartyUtil();

		public void Init()
		{
			boolFirstTime = false;
			boolUnload = false;
			tPu = new PartyUtil();
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmImportODonnell_Activated(object sender, System.EventArgs e)
		{
			// If Not boolFirstTime Then
			// boolFirstTime = True
			// ImportFile ("test.xml")
			// End If
		}

		private void frmImportODonnell_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmImportODonnell_Load(object sender, System.EventArgs e)
		{
            //FC:FINAL:AM:#3496 - increase the form's size
            //modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuClearAcreage_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.Execute("update master set piacres = 0,rsother = 0,rssoft = 0,rsmixed = 0,rshard = 0", modGlobalVariables.strREDatabase);
			MessageBox.Show("Acreage information cleared", "Acreage Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuClearBldg_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.Execute("update master set lastbldgval = 0", modGlobalVariables.strREDatabase);
			MessageBox.Show("Building values cleared", "Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuClearExempts_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.Execute("update master set rlexemption = 0,exemptval1 = 0,exemptval2 = 0,exemptval3 = 0,riexemptcd1 = 0,riexemptcd2 = 0,riexemptcd3 = 0", modGlobalVariables.strREDatabase);
			MessageBox.Show("Exemptions cleared", "Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuClearLand_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.Execute("update master set lastlandval = 0,rsothervalue = 0,rshardvalue = 0,rsmixedvalue = 0,rssoftvalue = 0", modGlobalVariables.strREDatabase);
			MessageBox.Show("Land values cleared", "Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuClearTGAcreage_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.Execute("update master set rsother = piacres,rsmixed = 0,rshard = 0,rssoft = 0", modGlobalVariables.strREDatabase);
			MessageBox.Show("Tree growth acreage cleared", "Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuClearTreeGrowth_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.Execute("update master set rsmixedvalue = 0,rshardvalue = 0,rssoftvalue = 0,rsothervalue = lastlandval", modGlobalVariables.strREDatabase);
			MessageBox.Show("Tree growth values cleared", "Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			boolUnload = true;
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private bool ImportFile(ref string strFile)
		{
			bool ImportFile = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strGeneratedDate = "";
				XmlNode tmpElement;
				// vbPorter upgrade warning: curElement As MSXML2.IXMLDOMNode	OnRead(MSXML2.IXMLDOMElement)
				XmlNode curElement;
				int intCount = 0;
				clsDRWrapper rsExempt = new clsDRWrapper();
				int lngRow;
				string strREFullDBName = "";
				string strMasterJoin;
				string strMasterJoinJoin = "";
				// strREFullDBName = rsTemp.GetFullDBName("RealEstate")
				strMasterJoin = modREMain.GetMasterJoin();
				string strMasterJoinQuery;
				strMasterJoinQuery = "(" + strMasterJoin + ") mj";
				cmdSummary.Enabled = false;
				cmdImport.Enabled = false;
				Array.Resize(ref arParties, 0 + 1);
				tPu.ConnectionString = rsExempt.Get_ConnectionInformation("CentralParties");
				// Call rsExempt.OpenRecordset("select rsaccount,rsname,rsmaplot,lastlandval,lastbldgval,rlexemption,rshard,rshardvalue,rsmixed,rsmixedvalue,rssoft,rssoftvalue,piacres from master where not rsdeleted = 1 and rscard = 1 order by rsaccount", strREDatabase)
				rsExempt.OpenRecordset(strMasterJoin + " where not rsdeleted = 1 and rscard = 1 order by rsaccount", modGlobalVariables.strREDatabase);
				while (!rsExempt.EndOfFile())
				{
					gridNotUpdated.Rows += 1;
					lngRow = gridNotUpdated.Rows - 1;
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLACCOUNT, FCConvert.ToString(rsExempt.Get_Fields_Int32("rsaccount")));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLBLDG, FCConvert.ToString(rsExempt.Get_Fields_Int32("lastbldgval")));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLEXEMPTION, FCConvert.ToString(rsExempt.Get_Fields_Int32("rlexemption")));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLLAND, FCConvert.ToString(rsExempt.Get_Fields_Int32("lastlandval")));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLMAPLOT, FCConvert.ToString(rsExempt.Get_Fields_String("rsmaplot")));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLNAME, FCConvert.ToString(rsExempt.Get_Fields_String("rsname")));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLHARDACRES, FCConvert.ToString(Conversion.Val(rsExempt.Get_Fields_Double("rshard"))));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLHARDVALUE, FCConvert.ToString(Conversion.Val(rsExempt.Get_Fields_Int32("rshardvalue"))));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLMIXEDACRES, FCConvert.ToString(Conversion.Val(rsExempt.Get_Fields_Double("rsmixed"))));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLMIXEDVALUE, FCConvert.ToString(Conversion.Val(rsExempt.Get_Fields_Int32("rsmixedvalue"))));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLSOFTACRES, FCConvert.ToString(Conversion.Val(rsExempt.Get_Fields_Double("rssoft"))));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLSOFTVALUE, FCConvert.ToString(Conversion.Val(rsExempt.Get_Fields_Int32("rssoftvalue"))));
					gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLTOTACRES, FCConvert.ToString(Conversion.Val(rsExempt.Get_Fields_Double("piacres"))));
					gridNotUpdated.RowData(lngRow, rsExempt.Get_Fields_Int32("rsaccount"));
					rsExempt.MoveNext();
				}
				ImportFile = false;
				docObj = new XmlDocument();
				//docObj.async = false;
				docObj.Load(strFile);
				lblUpdated.Text = "0";
				lblNew.Text = "0";
				lblTotal.Text = "0";
				lngUpdated = 0;
				lngNew = 0;
				lngTotal = 0;
				rsExempt.OpenRecordset("select code,description,shortdescription from exemptcode order by code", modGlobalVariables.strREDatabase);
				if (docObj.DocumentElement.Name == "dataroot")
				{
					tmpElement = docObj.SelectSingleNode("//dataroot");
					// strGeneratedDate = tmpElement.getAttribute("generated")
					modGlobalFunctions.AddCYAEntry_8("RE", "Imported ODonnell file");
					// Set curElement = tmpElement.firstChild
					foreach (XmlNode curElement_foreach in tmpElement.ChildNodes)
					{
						curElement = curElement_foreach;
						if (Strings.UCase(Strings.Left(curElement.Name + Strings.StrDup(11, " "), 11)) == "QTRIOEXPORT")
						{
							if (boolUnload)
							{
								return ImportFile;
							}
							curAcct = curElement;
							if (!ProcessAccount(ref rsExempt))
							{
								MessageBox.Show("Error processing account" + "\r\n" + "Cannot continue import", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								lblProgress.Text = "";
								cmdSummary.Enabled = true;
								cmdImport.Enabled = true;
								return ImportFile;
							}
							intCount += 1;
							lngTotal += 1;
							if (intCount > 10)
							{
								intCount = 0;
								//Application.DoEvents();
							}
						}
						lblUpdated.Text = FCConvert.ToString(lngUpdated);
						lblNew.Text = FCConvert.ToString(lngNew);
						lblTotal.Text = FCConvert.ToString(lngTotal);
						lblUpdated.Refresh();
						lblNew.Refresh();
						lblTotal.Refresh();
						curElement = null;
					}
					// curElement
				}
				else
				{
					MessageBox.Show("This file is not in the expected format", "Incorrect File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					lblProgress.Text = "";
					cmdSummary.Enabled = true;
					cmdImport.Enabled = true;
					return ImportFile;
				}
				cmdSummary.Enabled = true;
				cmdImport.Enabled = true;
				lblProgress.Text = "";
				ImportFile = true;
				MessageBox.Show("File " + strFile + " imported", "Imported", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return ImportFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lblProgress.Text = "";
				cmdSummary.Enabled = true;
				cmdImport.Enabled = true;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ImportFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ImportFile;
		}

		private bool ProcessAccount(ref clsDRWrapper rsExempt)
		{
			bool ProcessAccount = false;
			// VB6 Bad Scope Dim:
			cCreateGUID tGuid = new cCreateGUID();
			try
			{
				// On Error GoTo ErrorHandler
				string strTemp = "";
				string strMapLot = "";
				int lngAccount;
				int lngLand;
				int lngBuilding;
				int lngExemption;
				string strOwner1;
				string strOwner2;
				// vbPorter upgrade warning: dblAcres As double	OnWrite(short, string, double)
				double dblAcres;
				// vbPorter upgrade warning: dblHardwood As double	OnWrite(short, string)
				double dblHardwood;
				// vbPorter upgrade warning: dblSoftwood As double	OnWrite(short, string)
				double dblSoftwood;
				// vbPorter upgrade warning: dblMixedWood As double	OnWrite(short, string)
				double dblMixedWood;
				// vbPorter upgrade warning: dblOther As double	OnWrite(short, string, double)
				double dblOther;
				string strBook;
				string strPage;
				string strAddress1;
				string strAddress2;
				string strCity;
				string strState;
				string strZip;
				int lngStreetNumber;
				string strStreetName = "";
				bool boolUseAddress;
				bool boolAcreage;
				bool boolBillingValues;
				bool boolStreet;
				bool boolNew;
				bool boolRef1;
				string strREF1;
				bool boolPhone;
				string[] strPhone = null;
				int intPhone;
				bool boolTGYear;
				int intTGYear;
				bool boolTGPlanDue;
				int intTGDue;
				int lngSoftValue;
				int lngMixedValue;
				int lngHardValue;
				int lngOtherValue;
				bool boolTGValue;
				string[] strExempt = new string[3 + 1];
				int[] lngExempt = new int[3 + 1];
				bool boolExemptBreakdown;
				bool boolExempt;
				int intExemptIndex;
				int intExemptDescIndex;
				bool boolSale;
				bool boolLand;
				bool boolBldg;
				bool boolImportNew;
				string strDate;
				int lngSalePrice;
				int x;
				cPartyController tPC = new cPartyController();
				strPhone = new string[1 + 1];
				intPhone = 0;
				ProcessAccount = false;
				boolExempt = false;
				boolBldg = false;
				boolLand = false;
				boolSale = false;
				boolUseAddress = false;
				boolAcreage = false;
				boolBillingValues = false;
				boolStreet = false;
				boolNew = false;
				boolRef1 = false;
				boolPhone = false;
				boolTGYear = false;
				boolTGPlanDue = false;
				boolTGValue = false;
				boolExemptBreakdown = false;
				boolImportNew = false;
				strDate = "";
				lngSalePrice = 0;
				intExemptIndex = 0;
				intExemptDescIndex = 0;
				strExempt[1] = "";
				strExempt[2] = "";
				strExempt[3] = "";
				lngExempt[1] = 0;
				lngExempt[2] = 0;
				lngExempt[3] = 0;
				lngSoftValue = 0;
				lngHardValue = 0;
				lngMixedValue = 0;
				lngAccount = 0;
				intTGDue = 0;
				lngLand = 0;
				lngBuilding = 0;
				lngExemption = 0;
				strOwner1 = "";
				strOwner2 = "";
				dblAcres = 0;
				dblHardwood = 0;
				dblSoftwood = 0;
				dblMixedWood = 0;
				dblOther = 0;
				lngOtherValue = 0;
				strBook = "";
				strPage = "";
				strAddress1 = "";
				strAddress2 = "";
				strCity = "";
				strState = "";
				strZip = "";
				strREF1 = "";
				lngStreetNumber = 0;
				intTGYear = 0;
				if (cmbUpdate.Text == "Import new")
				{
					boolImportNew = true;
				}
				else
				{
					boolImportNew = false;
				}
				foreach (XmlNode subNode in curAcct.ChildNodes)
				{
					if (subNode.NodeType == XmlNodeType.Element)
					{
						if (Strings.UCase(subNode.Name) == "KEY")
						{
							// maplot
							strMapLot = subNode.InnerText;
							if (Strings.Trim(strMapLot) == string.Empty)
							{
								strMapLot = " ";
							}
						}
						else if ((Strings.UCase(subNode.Name) == "TRIOACCOUNT") || (Strings.UCase(subNode.Name) == "TRIOACCOUNTNUMBER"))
						{
							// account
							lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
						}
						else if (Strings.UCase(subNode.Name) == "LANDVALUE")
						{
							boolLand = true;
							lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
							boolBillingValues = true;
						}
						else if (Strings.UCase(subNode.Name) == "BUILDINGVALUE")
						{
							lngBuilding = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
							boolBillingValues = true;
							boolBldg = true;
						}
						else if (Strings.UCase(subNode.Name) == "PERSONALVALUE")
						{
						}
						else if (Strings.UCase(subNode.Name) == "EXEMPTIONVALUE")
						{
							boolExempt = true;
							lngExemption = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
							boolBillingValues = true;
						}
						else if (Strings.UCase(subNode.Name) == "OWNERNAME1")
						{
							strOwner1 = subNode.InnerText;
						}
						else if (Strings.UCase(subNode.Name) == "OWNERNAME2")
						{
							strOwner2 = subNode.InnerText;
						}
						else if (Strings.UCase(subNode.Name) == "TOTALACRES")
						{
							dblAcres = FCConvert.ToDouble(Strings.Format(Conversion.Val(subNode.InnerText), "0.00"));
							boolAcreage = true;
						}
						else if (Strings.UCase(subNode.Name) == "HARDWOODACRES")
						{
							dblHardwood = FCConvert.ToDouble(Strings.Format(Conversion.Val(subNode.InnerText), "0.00"));
							boolAcreage = true;
						}
						else if (Strings.UCase(subNode.Name) == "SOFTWOODACRES")
						{
							dblSoftwood = FCConvert.ToDouble(Strings.Format(Conversion.Val(subNode.InnerText), "0.00"));
							boolAcreage = true;
						}
						else if (Strings.UCase(subNode.Name) == "MIXEDWOODACRES")
						{
							dblMixedWood = FCConvert.ToDouble(Strings.Format(Conversion.Val(subNode.InnerText), "0.00"));
							boolAcreage = true;
						}
						else if (Strings.UCase(subNode.Name) == "OTHERLANDACRES")
						{
							dblOther = FCConvert.ToDouble(Strings.Format(Conversion.Val(subNode.InnerText), "0.00"));
							boolAcreage = true;
						}
						else if (Strings.UCase(subNode.Name) == "SOFTWOODVALUE")
						{
							boolTGValue = true;
							lngSoftValue = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
						}
						else if (Strings.UCase(subNode.Name) == "MIXEDWOODVALUE")
						{
							boolTGValue = true;
							lngMixedValue = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
						}
						else if (Strings.UCase(subNode.Name) == "HARDWOODVALUE")
						{
							boolTGValue = true;
							lngHardValue = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
						}
						else if (Strings.UCase(subNode.Name) == "OTHERLANDVALUE")
						{
							lngOtherValue = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
							boolTGValue = true;
						}
						else if (Strings.UCase(subNode.Name) == "BOOK")
						{
							strBook = subNode.InnerText;
						}
						else if (Strings.UCase(subNode.Name) == "PAGE")
						{
							strPage = subNode.InnerText;
						}
						else if (Strings.UCase(subNode.Name) == "ADDRESSLINE1")
						{
							strAddress1 = subNode.InnerText;
							boolUseAddress = true;
						}
						else if (Strings.UCase(subNode.Name) == "ADDRESSLINE2")
						{
							strAddress2 = subNode.InnerText;
							boolUseAddress = true;
						}
						else if (Strings.UCase(subNode.Name) == "CITY")
						{
							strCity = subNode.InnerText;
							boolUseAddress = true;
						}
						else if (Strings.UCase(subNode.Name) == "STATE")
						{
							strState = subNode.InnerText;
							boolUseAddress = true;
						}
						else if (Strings.UCase(subNode.Name) == "ZIP")
						{
							strZip = subNode.InnerText;
							boolUseAddress = true;
						}
						else if ((Strings.UCase(subNode.Name) == "STREETNUMBER") || (Strings.UCase(subNode.Name) == "STREETSUFFIX"))
						{
							lngStreetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
							boolStreet = true;
						}
						else if (Strings.UCase(subNode.Name) == "STREETNAME")
						{
							strStreetName = subNode.InnerText;
							boolStreet = true;
						}
						else if (Strings.UCase(subNode.Name) == "TOTALLAKEFRONTAGE")
						{
						}
						else if ((Strings.UCase(subNode.Name) == "EXEMPTDESCRIPTION") || (Strings.UCase(subNode.Name) == "EXEMPTDESCRIPTION1") || (Strings.UCase(subNode.Name) == "EXEMPTDESCRIPTION2") || (Strings.UCase(subNode.Name) == "EXEMPTDESCRIPTION3") || (Strings.UCase(subNode.Name) == "EXEMPTCATEGORY1") || (Strings.UCase(subNode.Name) == "EXEMPTCATEGORY2") || (Strings.UCase(subNode.Name) == "EXEMPTCATEGORY3") || (Strings.UCase(subNode.Name) == "EXEMPTCATEGORY"))
						{
							boolExemptBreakdown = true;
							intExemptDescIndex += 1;
							strExempt[intExemptDescIndex] = subNode.InnerText;
						}
						else if ((Strings.UCase(subNode.Name) == "EXEMPTAMOUNT") || (Strings.UCase(subNode.Name) == "EXEMPTAMOUNT1") || (Strings.UCase(subNode.Name) == "EXEMPTAMOUNT2") || (Strings.UCase(subNode.Name) == "EXEMPTAMOUNT3") || (Strings.UCase(subNode.Name) == "EXEMPTIONVALUE") || (Strings.UCase(subNode.Name) == "EXEMPTIONVALUE1") || (Strings.UCase(subNode.Name) == "EXEMPTIONVALUE2") || (Strings.UCase(subNode.Name) == "EXEMPTIONVALUE3"))
						{
							boolExemptBreakdown = true;
							intExemptIndex += 1;
							lngExempt[intExemptIndex] = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
						}
						else if ((Strings.UCase(subNode.Name) == "COMMENTS") || (Strings.UCase(subNode.Name) == "REFERENCE1") || (Strings.UCase(subNode.Name) == "REF1"))
						{
							boolRef1 = true;
							strREF1 = subNode.InnerText;
							if (strREF1.Length > 50)
							{
								strREF1 = Strings.Mid(strREF1, 1, 50);
							}
						}
						else if ((Strings.UCase(subNode.Name) == "PHONE") || (Strings.UCase(subNode.Name) == "PHONENUMBER") || (Strings.UCase(subNode.Name) == "TELEPHONE") || (Strings.UCase(subNode.Name) == "PHONE1") || (Strings.UCase(subNode.Name) == "PHONE2"))
						{
							boolPhone = true;
							intPhone += 1;
							if (intPhone > Information.UBound(strPhone, 1))
							{
								Array.Resize(ref strPhone, intPhone + 1);
							}
							strTemp = Strings.Trim(subNode.InnerText);
							strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
							strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
							strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
							strTemp = Strings.Right("0000000000" + strTemp, 10);
							strPhone[intPhone] = strTemp;
						}
						else if ((Strings.UCase(subNode.Name) == "YEARFIRSTCLASSIFIED") || (Strings.UCase(subNode.Name) == "TREEGROWTHYEAR"))
						{
							boolTGYear = true;
							intTGYear = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
						}
						else if ((Strings.UCase(subNode.Name) == "MANAGEMENTPLANDUE") || (Strings.UCase(subNode.Name) == "TREEGROWTHPLANDUE"))
						{
							boolTGPlanDue = true;
							intTGDue = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
						}
						else if (Strings.UCase(subNode.Name) == "SALEDATE")
						{
							boolSale = true;
							strDate = subNode.InnerText;
						}
						else if (Strings.UCase(subNode.Name) == "SALEPRICE")
						{
							boolSale = true;
							lngSalePrice = FCConvert.ToInt32(Math.Round(Conversion.Val(subNode.InnerText)));
						}
						else
						{
						}
					}
				}
				// subNode
				clsDRWrapper rsSave = new clsDRWrapper();
				// vbPorter upgrade warning: lngTemp As int	OnWrite(int, double)
				int lngTemp = 0;
				string[] strAry = null;
				int lngRow = 0;
				if (lngAccount > 0 || Strings.Trim(strMapLot) == "")
				{
					rsSave.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = 1", modGlobalVariables.strREDatabase);
				}
				else
				{
					// try to prevent recreating the new accounts again if the file was already imported
					rsSave.OpenRecordset("select * from master where rsmaplot = '" + strMapLot + "' and rscard = 1", modGlobalVariables.strREDatabase);
					if (!rsSave.EndOfFile())
					{
						if (rsSave.RecordCount() < 2)
						{
							lngAccount = FCConvert.ToInt32(rsSave.Get_Fields_Int32("rsaccount"));
						}
					}
				}
				if (!rsSave.EndOfFile() && lngAccount > 0)
				{
					if (boolImportNew)
					{
						ProcessAccount = true;
						return ProcessAccount;
					}
					rsSave.Edit();
					lngTemp = gridNotUpdated.FindRow(lngAccount);
					if (lngTemp > 0)
					{
						gridNotUpdated.RemoveItem(lngTemp);
					}
					lblProgress.Text = "Importing Account " + FCConvert.ToString(lngAccount);
					lngUpdated += 1;
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					boolNew = false;
				}
				else
				{
					if (!boolImportNew)
					{
						ProcessAccount = true;
						return ProcessAccount;
					}
					rsSave.AddNew();
					boolNew = true;
					if (lngAccount == 0)
					{
						lngAccount = modGlobalRoutines.GetNextAccountToCreate();
					}
					lngNew += 1;
					rsSave.Set_Fields("rsaccount", lngAccount);
					rsSave.Set_Fields("rscard", 1);
					rsSave.Set_Fields("AccountID", tGuid.CreateGUID());
					rsSave.Set_Fields("CardID", rsSave.Get_Fields_String("AccountID"));
					rsSave.Set_Fields("DateCreated", Strings.Format(DateTime.Now, "MM/dd/yyyy"));
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLNEW, FCConvert.ToString(true));
					lblProgress.Text = "Creating Account " + FCConvert.ToString(lngAccount);
				}
				rsSave.Set_Fields("rsdeleted", false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNEW, FCConvert.ToString(boolNew));
				lblProgress.Refresh();
				if (boolTGYear && (chkTGYear.CheckState == Wisej.Web.CheckState.Checked || boolNew))
				{
					rsSave.Set_Fields("pistreetcode", intTGYear);
				}
				if (boolTGPlanDue && (chkTGPlanDue.CheckState == Wisej.Web.CheckState.Checked || boolNew))
				{
					rsSave.Set_Fields("piopen1", intTGDue);
				}
				if (boolBillingValues)
				{
					if (boolLand && (chkLand.CheckState == Wisej.Web.CheckState.Checked || boolNew))
					{
						if (FCConvert.ToInt32(rsSave.Get_Fields_Int32("lastlandval")) != lngLand)
						{
							lngTemp = FCConvert.ToInt32(Conversion.Val(rsSave.Get_Fields_Int32("rsmixedvalue")) + Conversion.Val(rsSave.Get_Fields_Int32("rshardvalue")) + Conversion.Val(rsSave.Get_Fields_Int32("rssoftvalue")));
							if (lngTemp == 0 && lngMixedValue + lngHardValue + lngSoftValue == 0)
							{
								rsSave.Set_Fields("rsothervalue", lngLand);
							}
							else
							{
								// recalculate tree growth
								if (lngMixedValue + lngHardValue + lngSoftValue > 0)
								{
									rsSave.Set_Fields("rsothervalue", lngLand - (lngMixedValue + lngHardValue + lngSoftValue));
									if (rsSave.Get_Fields_Int32("rsothervalue") < 0)
										rsSave.Set_Fields("rsothervalue", 0);
								}
								else
								{
									rsSave.Set_Fields("rsothervalue", lngLand - lngTemp);
									if (rsSave.Get_Fields_Int32("rsothervalue") < 0)
									{
										rsSave.Set_Fields("rsothervalue", 0);
									}
								}
							}
						}
						else if ((lngMixedValue + lngHardValue + lngSoftValue) > 0)
						{
							rsSave.Set_Fields("rsothervalue", lngLand - (lngMixedValue + lngHardValue + lngSoftValue));
						}
						rsSave.Set_Fields("lastlandval", lngLand);
					}
					if (boolBldg && (chkBldg.CheckState == Wisej.Web.CheckState.Checked || boolNew))
					{
						rsSave.Set_Fields("lastbldgval", lngBuilding);
					}
					if (boolExempt && (chkExemption.CheckState == Wisej.Web.CheckState.Checked || boolNew))
					{
						rsSave.Set_Fields("rlexemption", lngExemption);
					}
				}
				if (boolTGValue && (chkLand.CheckState == Wisej.Web.CheckState.Checked || boolNew))
				{
					if (lngLand <= 0)
					{
						// lngLand = rsSave.Fields("lastlandval")
						// If lngLand <= 0 Then
						lngLand = lngMixedValue + lngHardValue + lngSoftValue + lngOtherValue;
						// End If
					}
					if (lngLand < lngMixedValue + lngHardValue + lngSoftValue + lngOtherValue)
					{
						lngLand = lngMixedValue + lngHardValue + lngSoftValue + lngOtherValue;
					}
					rsSave.Set_Fields("lastlandval", lngLand);
					rsSave.Set_Fields("rsothervalue", lngLand - lngMixedValue - lngHardValue - lngSoftValue);
					rsSave.Set_Fields("rsmixedvalue", lngMixedValue);
					rsSave.Set_Fields("rshardvalue", lngHardValue);
					rsSave.Set_Fields("rssoftvalue", lngSoftValue);
				}
				if (boolExemptBreakdown && (chkExemption.CheckState == Wisej.Web.CheckState.Checked || boolNew))
				{
					lngExemption = 0;
					for (x = 1; x <= 3; x++)
					{
						lngExemption += lngExempt[x];
						rsSave.Set_Fields("exemptval" + x, lngExempt[x]);
						if (Strings.Trim(strExempt[x]) != string.Empty)
						{
							// If rsExempt.FindFirstRecord("Description", strExempt(x)) Then
							if (rsExempt.FindFirst("description = " + strExempt[x]))
							{
								// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
								rsSave.Set_Fields("riexemptcd" + x, rsExempt.Get_Fields("code"));
								// ElseIf rsExempt.FindFirstRecord("ShortDescription", strExempt(x)) Then
							}
							else if (rsExempt.FindFirst("shortdescription = " + strExempt[x]))
							{
								// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
								rsSave.Set_Fields("riexemptcd" + x, rsExempt.Get_Fields("code"));
							}
							else
							{
								rsSave.Set_Fields("riexemptcd" + x, 0);
							}
						}
						else
						{
							rsSave.Set_Fields("riexemptcd" + x, 0);
						}
					}
					// x
					rsSave.Set_Fields("rlexemption", lngExemption);
				}
				if (boolSale && (chkSale.CheckState == Wisej.Web.CheckState.Checked || boolNew))
				{
					rsSave.Set_Fields("pisaleprice", lngSalePrice);
					if (Information.IsDate(strDate))
					{
						rsSave.Set_Fields("saledate", strDate);
					}
					else
					{
						rsSave.Set_Fields("saledate", 0);
					}
				}
				if (boolRef1 && (chkRef1.CheckState == Wisej.Web.CheckState.Checked || boolNew))
				{
					rsSave.Set_Fields("rsref1", strREF1);
				}
				if (boolStreet && (chkLocation.CheckState == Wisej.Web.CheckState.Checked || boolNew))
				{
					if (lngStreetNumber > 0)
					{
						rsSave.Set_Fields("rslocnumalph", lngStreetNumber);
					}
					else
					{
						rsSave.Set_Fields("rslocnumalph", "");
					}
					rsSave.Set_Fields("rslocstreet", strStreetName);
					rsSave.Set_Fields("rslocapt", "");
				}
				if (boolAcreage && (chkAcreage.CheckState == Wisej.Web.CheckState.Checked || boolNew))
				{
					if (dblAcres <= dblHardwood + dblMixedWood + dblSoftwood + dblOther)
					{
						dblAcres = dblHardwood + dblMixedWood + dblSoftwood + dblOther;
					}
					else
					{
						dblOther = dblAcres - dblHardwood - dblMixedWood - dblSoftwood;
						if (dblOther < 0)
							dblOther = 0;
					}
					rsSave.Set_Fields("piacres", dblAcres);
					rsSave.Set_Fields("rshard", dblHardwood);
					rsSave.Set_Fields("rsmixed", dblMixedWood);
					rsSave.Set_Fields("rssoft", dblSoftwood);
					rsSave.Set_Fields("rsother", dblOther);
					// If rsSave.Fields("rsother") < 0 Then rsSave.Fields("rsother") = 0
				}
				if (strMapLot != string.Empty && (chkMapLot.CheckState == Wisej.Web.CheckState.Checked || boolNew))
				{
					rsSave.Set_Fields("rsmaplot", Strings.Trim(strMapLot));
				}
				if (chkNameAddress.CheckState == Wisej.Web.CheckState.Checked || boolNew)
				{
					if (Strings.Trim(strOwner1) == "")
					{
						strOwner1 = Strings.Trim(strOwner2);
						strOwner2 = "";
					}
                    rsSave.Set_Fields("DeedName1",strOwner1);
                    rsSave.Set_Fields("DeedName2",strOwner2);
					if (strOwner1 != "")
					{
						// Dim tGUID As New cCreateGUID
						// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
						cParty tParty = new cParty();
						cParty oParty = new cParty();
						cParty soParty = new cParty();
						UtilParty uParty = new UtilParty();
						tParty = tPC.GetParty(rsSave.Get_Fields_Int32("ownerpartyid"));
						// vbPorter upgrade warning: tVar As object	OnRead(UtilParty)
						UtilParty[] tVar = null;
						if (!(tParty == null))
						{
                            if (tParty.ID == 0)
                            {
                                //' Nope.Try to create it then, creating just the party
                                    if (!tPC.SaveParty(ref tParty, true))
                                    {
                                        tParty.ID = 0;
                                    }
                            }


                            if (tParty.ID > 0)
							{
								if (Strings.LCase(tParty.FullNameLastFirst) != Strings.LCase(strOwner1))
								{
									rsSave.Set_Fields("Ownerpartyid", 0);
									rsSave.Set_Fields("secownerpartyid", 0);
									oParty.Clear();
									oParty.DateCreated = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
									oParty.PartyGUID = tGuid.CreateGUID();
									oParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
									soParty.Clear();
									soParty.DateCreated = oParty.DateCreated;
									soParty.PartyGUID = tGuid.CreateGUID();
									soParty.CreatedBy = oParty.CreatedBy;
									if (Strings.Trim(strOwner2) == "")
									{
										tVar = tPu.SplitName(strOwner1, true);
										if (Information.UBound(tVar, 1) >= 0)
										{
											if (Information.UBound(tVar, 1) > 0)
											{
												uParty = tVar[1];
												soParty.Designation = uParty.Designation;
												soParty.FirstName = uParty.FirstName;
												soParty.MiddleName = uParty.MiddleName;
												soParty.LastName = uParty.LastName;
												soParty.PartyType = uParty.PartyType;
											}
											uParty = tVar[0];
											oParty.Designation = uParty.Designation;
											oParty.FirstName = uParty.FirstName;
											oParty.MiddleName = uParty.MiddleName;
											oParty.LastName = uParty.LastName;
											oParty.PartyType = uParty.PartyType;
										}
									}
									else
									{
										tVar = tPu.SplitName(strOwner1, false);
										if (Information.UBound(tVar, 1) >= 0)
										{
											uParty = tVar[0];
											oParty.Designation = uParty.Designation;
											oParty.FirstName = uParty.FirstName;
											oParty.MiddleName = uParty.MiddleName;
											oParty.LastName = uParty.LastName;
											oParty.PartyType = uParty.PartyType;
										}
										tVar = tPu.SplitName(strOwner2, false);
										if (Information.UBound(tVar, 1) >= 0)
										{
											uParty = tVar[0];
											soParty.Designation = uParty.Designation;
											soParty.FirstName = uParty.FirstName;
											soParty.MiddleName = uParty.MiddleName;
											soParty.LastName = uParty.LastName;
											soParty.PartyType = uParty.PartyType;
										}
									}
									if (oParty.FirstName != "" || oParty.LastName != "")
									{
										if (boolUseAddress)
										{
											cPartyAddress tAddr = new cPartyAddress();
											tAddr.Address1 = strAddress1;
											tAddr.Address2 = strAddress2;
											tAddr.Address3 = "";
											tAddr.City = strCity;
											tAddr.State = strState;
											tAddr.Zip = strZip;
											tAddr.AddressType = "Primary";
											oParty.Addresses.Add(tAddr);
										}
										if (boolPhone && (chkPhone.CheckState == Wisej.Web.CheckState.Checked || boolNew))
										{
											cPartyPhoneNumber tPhone = new cPartyPhoneNumber();
											for (x = 1; x <= Information.UBound(strPhone, 1); x++)
											{
												tPhone = new cPartyPhoneNumber();
												tPhone.PhoneNumber = strPhone[x];
												tPhone.PhoneOrder = x;
												oParty.PhoneNumbers.Add(tPhone);
											}
											// x
										}
										tPC.SaveParty(ref oParty, false);
										rsSave.Set_Fields("ownerpartyid", oParty.ID);
										Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
										arParties[Information.UBound(arParties)] = oParty.ID;
									}
									if (soParty.FirstName != "" || soParty.LastName != "")
									{
										tPC.SaveParty(ref soParty, true);
										rsSave.Set_Fields("secownerpartyid", soParty.ID);
										Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
										arParties[Information.UBound(arParties)] = soParty.ID;
									}
								}
							}
						}
					}
				}
				Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(lngAccount));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(rsSave.Get_Fields_String("RSMAPLOT")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, strOwner1);
				// rsSave.Fields("rsname")
				Grid.TextMatrix(lngRow, CNSTGRIDCOLLAND, FCConvert.ToString(rsSave.Get_Fields_Int32("lastlandval")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLBLDG, FCConvert.ToString(rsSave.Get_Fields_Int32("lastbldgval")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLEXEMPTION, FCConvert.ToString(rsSave.Get_Fields_Int32("rlexemption")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLHARDACRES, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Double("rshard"))));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLHARDVALUE, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Int32("rshardvalue"))));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLMIXEDACRES, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Double("rsmixed"))));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLMIXEDVALUE, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Int32("rsmixedvalue"))));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLSOFTACRES, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Double("rssoft"))));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLSOFTVALUE, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Int32("rssoftvalue"))));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTOTACRES, FCConvert.ToString(Conversion.Val(rsSave.Get_Fields_Double("piacres"))));
				rsSave.Update();
				if (strBook != "" && strPage != "" && (chkBookPage.CheckState == Wisej.Web.CheckState.Checked || boolNew))
				{
					rsSave.Execute("delete from bookpage where account = " + FCConvert.ToString(lngAccount), modGlobalVariables.strREDatabase);
					rsSave.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAccount), modGlobalVariables.strREDatabase);
					rsSave.AddNew();
					rsSave.Set_Fields("book", strBook);
					rsSave.Set_Fields("page", strPage);
					rsSave.Set_Fields("current", true);
					rsSave.Set_Fields("line", 1);
					rsSave.Set_Fields("card", 1);
					rsSave.Set_Fields("Account", lngAccount);
					rsSave.Update();
				}
				ProcessAccount = true;
				return ProcessAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ProcessAmount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ProcessAccount;
		}

		private void mnuImport_Click(object sender, System.EventArgs e)
		{
			string strFile = "";
			try
			{
				// On Error GoTo ErrorHandler
				Grid.Rows = 0;
				gridNotUpdated.Rows = 0;
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNPathMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNFileMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.Filter = "XML|*.xml";
				MDIParent.InstancePtr.CommonDialog1.FilterIndex = 0;
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strFile = Strings.Trim(MDIParent.InstancePtr.CommonDialog1.FileName);
				if (strFile != string.Empty)
				{
					ImportFile(ref strFile);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number != 32755)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuImport_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void mnuImportedReport_Click(object sender, System.EventArgs e)
		{
			rptImportOdonnell.InstancePtr.Init(this.Modal);
		}

		private void mnuMarkDeleted_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.Execute("update master set rsdeleted = 1", modGlobalVariables.strREDatabase);
			MessageBox.Show("All accounts are now marked as deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuPrintNotUpdated_Click(object sender, System.EventArgs e)
		{
			rptOdonnellNotUpdated.InstancePtr.Init(this.Modal);
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			rptOdonellSummary.InstancePtr.Init(this.Modal);
		}

	}
}
