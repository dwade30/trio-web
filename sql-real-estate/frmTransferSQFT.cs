﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmTransferSQFT.
	/// </summary>
	public partial class frmTransferSQFT : BaseForm
	{
		public frmTransferSQFT()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTransferSQFT InstancePtr
		{
			get
			{
				return (frmTransferSQFT)Sys.GetInstance(typeof(frmTransferSQFT));
			}
		}

		protected frmTransferSQFT _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public bool boolFromCommercial;

		private void frmTransferSQFT_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuTransfer_Click();
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				this.Unload();
				return;
			}
		}

		private void frmTransferSQFT_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTransferSQFT properties;
			//frmTransferSQFT.ScaleWidth	= 9045;
			//frmTransferSQFT.ScaleHeight	= 7275;
			//frmTransferSQFT.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this);
		}

		private void frmTransferSQFT_Resize(object sender, System.EventArgs e)
		{
			ResizeGridinfo();
			ResizeGridBuildings();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void ResizeGridinfo()
		{
			int GridWidth = 0;
			GridWidth = GridInfo.WidthOriginal;
			GridInfo.ColWidth(0, FCConvert.ToInt32(0.13 * GridWidth));
			GridInfo.ColWidth(1, FCConvert.ToInt32(0.7 * GridWidth));
			GridInfo.ColWidth(2, FCConvert.ToInt32(0.13 * GridWidth));
			//GridInfo.Height = GridInfo.RowHeight(0) * GridInfo.Rows + 50;
		}

		private void ResizeGridBuildings()
		{
			int GridWidth = 0;
			GridWidth = GridBuildings.WidthOriginal;
			GridBuildings.ColWidth(0, FCConvert.ToInt32(GridWidth / 3.0));
			GridBuildings.ColWidth(1, FCConvert.ToInt32(GridWidth / 3.0));
			//GridBuildings.Height = GridBuildings.RowHeight(0) * GridBuildings.Rows + 50;
		}

		private void mnuTransfer_Click(object sender, System.EventArgs e)
		{
			int x;
			string strTemp = "";
			if (boolFromCommercial)
			{
				// transferring commercial data
				for (x = 1; x <= 2; x++)
				{
					if (Conversion.Val(GridBuildings.TextMatrix(x, 2)) > 0)
					{
						if (Conversion.Val(GridBuildings.TextMatrix(x, 2)) < GridInfo.Rows)
						{
							modDataTypes.Statics.CMR.Set_Fields("c" + x + "floor", FCConvert.ToString(Conversion.Val(GridInfo.TextMatrix(FCConvert.ToInt32(GridBuildings.TextMatrix(x, 2)), 2))));
							if (x == 1)
							{
								frmREProperty.InstancePtr.mebC1Floor.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("c1floor"));
							}
							else
							{
								frmREProperty.InstancePtr.mebC2Floor.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("c2floor"));
							}
						}
					}
				}
				// x
			}
			else
			{
				// outbuildings
				for (x = 1; x <= 10; x++)
				{
					if (Conversion.Val(GridBuildings.TextMatrix(x, 2)) > 0)
					{
						if (Conversion.Val(GridBuildings.TextMatrix(x, 2)) < GridInfo.Rows)
						{
							// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
							if (Conversion.Val(modDataTypes.Statics.OUT.Get_Fields("oitype" + FCConvert.ToString(x))) == 0)
							{
								strTemp = GridInfo.TextMatrix(FCConvert.ToInt32(GridBuildings.TextMatrix(x, 2)), 1);
								if (Strings.InStr(1, Strings.Trim(strTemp), " ", CompareConstants.vbTextCompare) > 0)
								{
									strTemp = Strings.Mid(strTemp, 1, Strings.InStr(1, Strings.Trim(strTemp), " ", CompareConstants.vbTextCompare) - 1);
								}
								modDataTypes.Statics.OUT.Set_Fields("oitype" + x, FCConvert.ToString(Conversion.Val(strTemp)));
								frmREProperty.InstancePtr.txtMROIType1[FCConvert.ToInt16(x - 1)].Text = FCConvert.ToString(Conversion.Val(strTemp));
							}
							modDataTypes.Statics.OUT.Set_Fields("oiunits" + x, FCConvert.ToString(Conversion.Val(GridInfo.TextMatrix(FCConvert.ToInt32(GridBuildings.TextMatrix(x, 2)), 2))));
							frmREProperty.InstancePtr.txtMROIUnits1[FCConvert.ToInt16(x - 1)].Text = FCConvert.ToString(Conversion.Val(GridInfo.TextMatrix(FCConvert.ToInt32(GridBuildings.TextMatrix(x, 2)), 2)));
						}
					}
				}
				// x
			}
			this.Unload();
		}

		public void mnuTransfer_Click()
		{
			mnuTransfer_Click(mnuTransfer, new System.EventArgs());
		}
	}
}
