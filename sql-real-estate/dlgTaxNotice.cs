﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for dlgTaxNotice.
	/// </summary>
	public partial class dlgTaxNotice : BaseForm
	{
		public dlgTaxNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static dlgTaxNotice InstancePtr
		{
			get
			{
				return (dlgTaxNotice)Sys.GetInstance(typeof(dlgTaxNotice));
			}
		}

		protected dlgTaxNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intWhatOrder;
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		public void Init(ref short intOrder)
		{
			intWhatOrder = intOrder;
			//vbPorterConverter2.ShowModeless(this);
			this.Show();
		}

		private void chkInclude_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkInclude_1.CheckState != Wisej.Web.CheckState.Checked)
			{
				Label1.Visible = false;
				Label2.Visible = false;
				txtYear.Visible = false;
				txtRate.Visible = false;
			}
			else
			{
				if (cmbFormat.Text == "Change of Assessment")
				{
					Label1.Visible = true;
					Label2.Visible = true;
					txtYear.Visible = true;
					txtRate.Visible = true;
				}
			}
		}

		private void cmdOkay_Click()
		{
			string strReturn = "";
			// vbPorter upgrade warning: minaccount As int	OnWrite(string)
			int minaccount = 0;
			int maxaccount;
			string strOrder = "";
			string strResponse = "";
			int intARorS = 0;
			// All, Range or Specific
			int intPerPage = 0;
			bool boolBillingVals = false;
			bool boolCondense = false;
			if (cmbtPerPage.Text == "1")
			{
				// 1
				intPerPage = 1;
				if (cmbAddress.Text == "Align address for envelope")
				{
					boolCondense = false;
				}
				else
				{
					boolCondense = true;
				}
			}
			else if (cmbtPerPage.Text == "2")
			{
				// 2
				intPerPage = 2;
				if (cmbAddress.Text == "Align address for envelope")
				{
					boolCondense = false;
				}
				else
				{
					boolCondense = true;
				}
			}
			else
			{
				// 3
				intPerPage = 3;
				boolCondense = false;
			}
			switch (intWhatOrder)
			{
				case 1:
					{
						strOrder = "Account";
						break;
					}
				case 2:
					{
						strOrder = "Name";
						break;
					}
				case 3:
					{
						strOrder = "Map\\Lot";
						break;
					}
				case 4:
					{
						strOrder = "Location";
						break;
					}
			}
			//end switch
			if (cmbaccounts.Text == "All")
			{
				intARorS = 1;
			}
			else if (cmbaccounts.Text == "Range")
			{
				intARorS = 2;
			}
			else if (cmbaccounts.Text == "Specific Accounts")
			{
				intARorS = 3;
			}
			else
			{
				intARorS = 4;
			}
			if (cmbValues.Text == "Billing")
			{
				boolBillingVals = true;
			}
			else
			{
				boolBillingVals = false;
			}
			if (cmbFormat.Text == "Change of Assessment" && (chkInclude_1.CheckState == Wisej.Web.CheckState.Checked))
			{
				if ((txtRate.Text == string.Empty) || (txtYear.Text == string.Empty))
				{
					MessageBox.Show("You must fill in both the tax rate and the tax year to continue", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbaccounts.Text == "Range")
			{
				minagain:
				;
				strResponse = Interaction.InputBox("Please enter the minimum " + strOrder + " value", "Enter Minimum", null/*, -1, -1*/);
				if (strResponse == string.Empty)
					return;
				if (intWhatOrder == 1)
				{
					if (Conversion.Val(strResponse) < 1)
					{
						MessageBox.Show("This is an invalid minimum account", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						goto minagain;
					}
					minaccount = FCConvert.ToInt32(Strings.Trim(strResponse));
					modGlobalVariables.Statics.gintMinAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(strResponse) + "")));
				}
				else
				{
					modPrintRoutines.Statics.gstrMinAccountRange = Strings.Trim(strResponse);
				}
				maxagain:
				;
				strResponse = Interaction.InputBox("Please enter the maximum " + strOrder + " value", "Enter Maximum", null/*, -1, -1*/);
				if (strResponse == string.Empty)
					return;
				if (intWhatOrder == 1)
				{
					if ((Conversion.Val(strResponse) < 1) || (Conversion.Val(strResponse) < minaccount))
					{
						MessageBox.Show("This is an invalid range", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						goto minagain;
					}
					modGlobalVariables.Statics.gintMaxAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(strResponse) + "")));
				}
				else
				{
					modPrintRoutines.Statics.gstrMaxAccountRange = Strings.Trim(strResponse);
				}
			}
			else if (cmbaccounts.Text == "Specific Accounts")
			{
				// Me.Hide
				frmGetAccountsToPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				// Me.Show
			}
			else if (cmbaccounts.Text == "From Extract for Listings")
			{
				modGlobalVariables.Statics.CancelledIt = false;
				frmGetGroupsToPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				if (modGlobalVariables.Statics.CancelledIt)
					return;
			}
			
			//! Load dlgTaxMessage;
			this.Unload();
			dlgTaxMessage.InstancePtr.Init(cmbFormat.Text == "Free Form", chkInclude_1.CheckState == Wisej.Web.CheckState.Checked, chkInclude_2.CheckState == Wisej.Web.CheckState.Checked, chkInclude_3.CheckState == Wisej.Web.CheckState.Checked, chkInclude_0.CheckState == Wisej.Web.CheckState.Checked, txtYear.Text, txtRate.Text, ref intARorS, ref strOrder, ref intPerPage, ref boolBillingVals, boolCondense);
		}

		private void dlgTaxNotice_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void dlgTaxNotice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//dlgTaxNotice properties;
			//dlgTaxNotice.ScaleWidth	= 5910;
			//dlgTaxNotice.ScaleHeight	= 4125;
			//dlgTaxNotice.LinkTopic	= "Form1";
			//dlgTaxNotice.LockControls	= true;
			//End Unmaped Properties
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			if (modREMain.Statics.boolShortRealEstate)
			{
				cmbValues.Clear();
				cmbValues.Items.Add("Billing");
				cmbValues.Text = "Billing";
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdOkay_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void optFormat_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				chkInclude_1.CheckState = Wisej.Web.CheckState.Unchecked;
				chkInclude_1.Enabled = false;
				chkInclude_2.Enabled = false;
				chkInclude_2.CheckState = Wisej.Web.CheckState.Unchecked;
				Label1.Visible = false;
				Label2.Visible = false;
				txtYear.Visible = false;
				txtRate.Visible = false;
			}
			else
			{
				chkInclude_1.Enabled = true;
				chkInclude_2.Enabled = true;
				if (chkInclude_1.CheckState == Wisej.Web.CheckState.Checked)
				{
					Label1.Visible = true;
					Label2.Visible = true;
					txtYear.Visible = true;
					txtRate.Visible = true;
				}
			}
		}

		private void optFormat_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbFormat.SelectedIndex;
			optFormat_CheckedChanged(index, sender, e);
		}

		private void OptPerPage_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 2:
					{
						cmbAddress.Enabled = false;
						break;
					}
				default:
					{
						cmbAddress.Enabled = true;
						break;
					}
			}
			//end switch
		}

		private void OptPerPage_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtPerPage.SelectedIndex;
			OptPerPage_CheckedChanged(index, sender, e);
		}

		private void cmbFormat_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFormat.SelectedIndex == 0)
			{
				optFormat_CheckedChanged(sender, e);
			}
			else if (cmbFormat.SelectedIndex == 1)
			{
				optFormat_CheckedChanged(sender, e);
			}
		}

		private void cmbtPerPage_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbtPerPage.SelectedIndex == 0)
			{
				OptPerPage_CheckedChanged(sender, e);
			}
			else if (cmbtPerPage.SelectedIndex == 1)
			{
				OptPerPage_CheckedChanged(sender, e);
			}
			else if (cmbtPerPage.SelectedIndex == 2)
			{
				OptPerPage_CheckedChanged(sender, e);
			}
		}
	}
}
