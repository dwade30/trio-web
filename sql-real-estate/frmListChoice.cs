﻿using System;
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmListChoice.
	/// </summary>
	public partial class frmListChoice : BaseForm
	{
		public frmListChoice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmListChoice InstancePtr
		{
			get
			{
				return (frmListChoice)Sys.GetInstance(typeof(frmListChoice));
			}
		}

		protected frmListChoice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 11/03/2004
		// Inputs a list of items to list in a grid with check boxes
		// you specify whether to allow multiple selections
		// and if a selection must be made or not
		// optionally you can specify which items to default to true
		// a return of nullstring indicates no choices
		// a return of -1 means it was cancelled
		// ********************************************************
		string strReturn;
		bool boolForceSelection;
		bool boolAllowMultiple;

		public string Init(string strList, string strTitle = "", string strMessage = "", bool boolAllowMultiSelect = false, string strDefaultList = "", bool boolMustSelect = false, bool boolReturnCodes = false)
		{
			string Init = "";
			// takes a semicolon delimited list of items to choose from
			// the default list is semicolon delimited and is a list of items to be set to true
			// lists passed to and from init are not zero based
			// the return value is a list of the selected items
			string[] strAry = null;
			int x;
			strReturn = "-1";
			boolForceSelection = boolMustSelect;
			boolAllowMultiple = boolAllowMultiSelect;
			SetupGrid();
			strAry = Strings.Split(strList, ";", -1, CompareConstants.vbTextCompare);
			for (x = 0; x <= Information.UBound(strAry, 1); x++)
			{
				Grid.Rows += 1;
				Grid.TextMatrix(Grid.Rows - 1, 1, strAry[x]);
			}
			// x
			if (Strings.Trim(strDefaultList) != string.Empty)
			{
				strAry = Strings.Split(strDefaultList, ";", -1, CompareConstants.vbTextCompare);
				for (x = 0; x <= Information.UBound(strAry, 1); x++)
				{
					Grid.TextMatrix(FCConvert.ToInt32(Conversion.Val(strAry[x]) - 1), 0, FCConvert.ToString(true));
				}
				// x
			}
			ResizeGrid();
			this.Text = strTitle;
			lblMessage.Text = strMessage;
			//FC:FINAL:MSH - i.issue #1260: show the form by using Show for correct positioning and filling control values
			//this.ShowDialog();
			this.Show(FormShowEnum.Modal);
			Init = strReturn;
			return Init;
		}

		private void frmListChoice_KeyDown(object sender, KeyEventArgs e)
		{
			Keys KeyCode = (Keys)e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = 0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmListChoice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmListChoice.FillStyle	= 0;
			//frmListChoice.ScaleWidth	= 5880;
			//frmListChoice.ScaleHeight	= 3810;
			//frmListChoice.KeyPreview	= -1  'True;
			//frmListChoice.LinkTopic	= "Form2";
			//frmListChoice.LockControls	= -1  'True;
			//frmListChoice.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void SetupGrid()
		{
			Grid.Cols = 2;
			Grid.Rows = 0;
			Grid.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

		private void frmListChoice_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			int x;
			if (!boolAllowMultiple)
			{
				if (FCConvert.ToBoolean(Grid.TextMatrix(Grid.Row, 0)))
				{
					for (x = 0; x <= Grid.Rows - 1; x++)
					{
						if (Grid.TextMatrix(x, 0) != string.Empty)
						{
							if (FCConvert.ToBoolean(Grid.TextMatrix(x, 0)) && x != Grid.Row)
							{
								Grid.TextMatrix(x, 0, FCConvert.ToString(false));
							}
						}
					}
					// x
				}
			}
		}

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			if (Grid.Col == 0)
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			int x;
			string strTemp;
			bool boolHasATrue;
			Grid.Row = -1;
			//Application.DoEvents();
			strTemp = "";
			boolHasATrue = false;
			for (x = 0; x <= Grid.Rows - 1; x++)
			{
				//FC:FINAL:MSH - i.issue #1259: converting exception
				//if (FCConvert.ToBoolean(Grid.TextMatrix(x, 0)))
				if (FCConvert.CBool(Grid.TextMatrix(x, 0)))
				{
					strTemp += FCConvert.ToString(x + 1) + ";";
					boolHasATrue = true;
				}
			}
			// x
			if (boolForceSelection && !boolHasATrue)
			{
				MessageBox.Show("You must make a selection before continuing", "No Selection Made", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			strReturn = strTemp;
			Close();
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			//FC:FINAL:MSH - i.issue #1259: restore resizing of the grid
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.1 * GridWidth));
			if (Grid.Rows > 10)
			{
				Grid.HeightOriginal = (Grid.RowHeight(0) * 10) + 60;
			}
			else if (Grid.Rows > 0)
			{
				Grid.HeightOriginal = (Grid.RowHeight(0) * Grid.Rows) + 60;
			}
		}
	}
}
