﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;

namespace TWRE0000
{
	public class cExemptByAccountReport
	{
		//=========================================================
		private cGenericCollection collRecords = new cGenericCollection();
		private bool boolByRange;
		private string strRangeType = string.Empty;
		private string strMinRange = string.Empty;
		private string strMaxRange = string.Empty;
		private int intTownNumberToUse;
		private string strExemptionsToUse = string.Empty;

		public string ExemptionsToUse
		{
			set
			{
				strExemptionsToUse = value;
			}
			get
			{
				string ExemptionsToUse = "";
				ExemptionsToUse = strExemptionsToUse;
				return ExemptionsToUse;
			}
		}

		public short TownNumber
		{
			set
			{
				intTownNumberToUse = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short TownNumber = 0;
				TownNumber = FCConvert.ToInt16(intTownNumberToUse);
				return TownNumber;
			}
		}

		public bool ByRange
		{
			set
			{
				boolByRange = value;
			}
			get
			{
				bool ByRange = false;
				ByRange = boolByRange;
				return ByRange;
			}
		}

		public string RangeType
		{
			set
			{
				strRangeType = value;
			}
			get
			{
				string RangeType = "";
				RangeType = strRangeType;
				return RangeType;
			}
		}

		public string MinRange
		{
			set
			{
				strMinRange = value;
			}
			get
			{
				string MinRange = "";
				MinRange = strMinRange;
				return MinRange;
			}
		}

		public string MaxRange
		{
			set
			{
				strMaxRange = value;
			}
			get
			{
				string MaxRange = "";
				MaxRange = strMaxRange;
				return MaxRange;
			}
		}

		public cGenericCollection ExemptionRecords
		{
			get
			{
				cGenericCollection ExemptionRecords = null;
				ExemptionRecords = collRecords;
				return ExemptionRecords;
			}
		}

		public cExemptByAccountReport() : base()
		{
			boolByRange = false;
		}
	}
}
