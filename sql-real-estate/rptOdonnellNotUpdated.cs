﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptOdonnellNotUpdated.
	/// </summary>
	public partial class rptOdonnellNotUpdated : BaseSectionReport
	{
		public static rptOdonnellNotUpdated InstancePtr
		{
			get
			{
				return (rptOdonnellNotUpdated)Sys.GetInstance(typeof(rptOdonnellNotUpdated));
			}
		}

		protected rptOdonnellNotUpdated _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptOdonnellNotUpdated()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.Name = "Not Updated";
        }
		// nObj = 1
		//   0	rptOdonnellNotUpdated	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngRow;
		const int CNSTGRIDNOTUPDATEDCOLACCOUNT = 0;
		const int CNSTGRIDNOTUPDATEDCOLNAME = 1;
		const int CNSTGRIDNOTUPDATEDCOLMAPLOT = 2;
		const int CNSTGRIDNOTUPDATEDCOLLAND = 3;
		const int CNSTGRIDNOTUPDATEDCOLBLDG = 4;
		const int CNSTGRIDNOTUPDATEDCOLEXEMPTION = 5;
		const int CNSTGRIDNOTUPDATEDCOLTOTACRES = 6;
		const int CNSTGRIDNOTUPDATEDCOLSOFTACRES = 7;
		const int CNSTGRIDNOTUPDATEDCOLMIXEDACRES = 8;
		const int CNSTGRIDNOTUPDATEDCOLHARDACRES = 9;
		const int CNSTGRIDNOTUPDATEDCOLSOFTVALUE = 10;
		const int CNSTGRIDNOTUPDATEDCOLMIXEDVALUE = 11;
		const int CNSTGRIDNOTUPDATEDCOLHARDVALUE = 12;
		double dblLand;
		double dblBldg;
		double dblExempt;
		double dblSoftAcres;
		private double dblMixedAcres;
		private double dblHardAcres;
		private double dblTotAcres;
		private double dblSoftValue;
		private double dblMixedValue;
		private double dblHardValue;

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "Not Updated", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngRow == frmImportODonnell.InstancePtr.gridNotUpdated.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngRow = 0;
			txtNumNotUpdated.Text = FCConvert.ToString(frmImportODonnell.InstancePtr.gridNotUpdated.Rows);
			dblLand = 0;
			dblBldg = 0;
			dblExempt = 0;
			dblSoftAcres = 0;
			dblMixedAcres = 0;
			dblHardAcres = 0;
			dblSoftValue = 0;
			dblMixedValue = 0;
			dblHardValue = 0;
			dblTotAcres = 0;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngRow < frmImportODonnell.InstancePtr.gridNotUpdated.Rows)
			{
				txtAcct.Text = frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLACCOUNT);
				txtName.Text = frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLNAME);
				txtMapLot.Text = frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLMAPLOT);
				txtExemption.Text = Strings.Format(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLEXEMPTION), "#,###,###,##0");
				txtLand.Text = Strings.Format(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLLAND), "#,###,###,##0");
				txtBldg.Text = Strings.Format(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLBLDG), "#,###,###,##0");
				dblLand += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLLAND));
				dblBldg += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLBLDG));
				dblExempt += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLEXEMPTION));
				dblSoftAcres += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLSOFTACRES));
				dblMixedAcres += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLMIXEDACRES));
				dblHardAcres += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLHARDACRES));
				dblTotAcres += Conversion.Val(frmImportODonnell.InstancePtr.gridNotUpdated.TextMatrix(lngRow, CNSTGRIDNOTUPDATEDCOLTOTACRES));
				lngRow += 1;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotBuilding.Text = Strings.Format(dblBldg, "#,###,###,##0");
			txtTotLand.Text = Strings.Format(dblLand, "#,###,###,##0");
			txtTotExemption.Text = Strings.Format(dblExempt, "#,###,###,##0");
			txtSoftAcres.Text = Strings.Format(dblSoftAcres, "#,###,###,##0.00");
			txtMixedAcres.Text = Strings.Format(dblMixedAcres, "#,###,###,##0.00");
			txtHardAcres.Text = Strings.Format(dblHardAcres, "#,###,###,##0.00");
			txtTotalAcres.Text = Strings.Format(dblTotAcres, "#,###,###,##0.00");
			txtSoftValue.Text = Strings.Format(dblSoftValue, "#,###,###,##0");
			txtMixedValue.Text = Strings.Format(dblMixedValue, "#,###,###,##0");
			txtHardValue.Text = Strings.Format(dblHardValue, "#,###,###,##0");
			txtTotalLand.Text = txtTotLand.Text;
			txtOtherAcres.Text = Strings.Format((dblTotAcres - (dblMixedAcres + dblSoftAcres + dblHardAcres)), "#,###,###,##0.00");
			txtOtherValue.Text = Strings.Format(dblLand - (dblSoftValue + dblMixedValue + dblHardValue), "#,###,###,###,##0");
		}

		
	}
}
