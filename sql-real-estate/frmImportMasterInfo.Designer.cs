﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Xml;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportMasterInfo.
	/// </summary>
	partial class frmImportMasterInfo : BaseForm
	{
		public FCGrid Grid;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkInspection;
		public fecherFoundation.FCCheckBox chkPictures;
		public fecherFoundation.FCCheckBox ChkNameAddress;
		public fecherFoundation.FCCheckBox chkMapLot;
		public fecherFoundation.FCCheckBox ChkRef1;
		public fecherFoundation.FCCheckBox chkRef2;
		public fecherFoundation.FCCheckBox ChkTranLandBldg;
		public fecherFoundation.FCCheckBox chkTaxAcquired;
		public fecherFoundation.FCCheckBox chkBankruptcy;
		public fecherFoundation.FCCheckBox chkBookPage;
		public fecherFoundation.FCCheckBox chkLandInformation;
		public fecherFoundation.FCCheckBox chkPropertyInfo;
		public fecherFoundation.FCCheckBox chkSaleData;
		public fecherFoundation.FCCheckBox chkExemptInfo;
		public fecherFoundation.FCCheckBox chkLocation;
		public fecherFoundation.FCCheckBox chkDwelling;
		public fecherFoundation.FCCheckBox chkCommercial;
		public fecherFoundation.FCCheckBox chkOutbuilding;
		public fecherFoundation.FCCheckBox chkPreviousOwners;
		public fecherFoundation.FCCheckBox chkInterestedParties;
		public fecherFoundation.FCLabel lblUpdating;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImportMasterInfo));
            this.Grid = new fecherFoundation.FCGrid();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkDeedNames = new fecherFoundation.FCCheckBox();
            this.chkInspection = new fecherFoundation.FCCheckBox();
            this.chkPictures = new fecherFoundation.FCCheckBox();
            this.ChkNameAddress = new fecherFoundation.FCCheckBox();
            this.chkMapLot = new fecherFoundation.FCCheckBox();
            this.ChkRef1 = new fecherFoundation.FCCheckBox();
            this.chkRef2 = new fecherFoundation.FCCheckBox();
            this.ChkTranLandBldg = new fecherFoundation.FCCheckBox();
            this.chkTaxAcquired = new fecherFoundation.FCCheckBox();
            this.chkBankruptcy = new fecherFoundation.FCCheckBox();
            this.chkBookPage = new fecherFoundation.FCCheckBox();
            this.chkLandInformation = new fecherFoundation.FCCheckBox();
            this.chkPropertyInfo = new fecherFoundation.FCCheckBox();
            this.chkSaleData = new fecherFoundation.FCCheckBox();
            this.chkExemptInfo = new fecherFoundation.FCCheckBox();
            this.chkLocation = new fecherFoundation.FCCheckBox();
            this.chkDwelling = new fecherFoundation.FCCheckBox();
            this.chkCommercial = new fecherFoundation.FCCheckBox();
            this.chkOutbuilding = new fecherFoundation.FCCheckBox();
            this.chkPreviousOwners = new fecherFoundation.FCCheckBox();
            this.chkInterestedParties = new fecherFoundation.FCCheckBox();
            this.lblUpdating = new fecherFoundation.FCLabel();
            this.cmdSave = new Wisej.Web.Button();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeedNames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPictures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNameAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRef1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRef2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTranLandBldg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTaxAcquired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBankruptcy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBookPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLandInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropertyInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSaleData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExemptInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDwelling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCommercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOutbuilding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreviousOwners)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterestedParties)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 730);
            this.BottomPanel.Size = new System.Drawing.Size(827, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.lblUpdating);
            this.ClientArea.Size = new System.Drawing.Size(847, 450);
            this.ClientArea.Controls.SetChildIndex(this.lblUpdating, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Grid, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(847, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(196, 30);
            this.HeaderText.Text = "Import Name Etc";
            // 
            // Grid
            // 
            this.Grid.Cols = 4;
            this.Grid.ColumnHeadersVisible = false;
            this.Grid.FixedCols = 0;
            this.Grid.FixedRows = 0;
            this.Grid.Location = new System.Drawing.Point(564, 96);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 0;
            this.Grid.Size = new System.Drawing.Size(11, 47);
            this.Grid.TabIndex = 19;
            this.Grid.Visible = false;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkDeedNames);
            this.Frame1.Controls.Add(this.chkInspection);
            this.Frame1.Controls.Add(this.chkPictures);
            this.Frame1.Controls.Add(this.ChkNameAddress);
            this.Frame1.Controls.Add(this.chkMapLot);
            this.Frame1.Controls.Add(this.ChkRef1);
            this.Frame1.Controls.Add(this.chkRef2);
            this.Frame1.Controls.Add(this.ChkTranLandBldg);
            this.Frame1.Controls.Add(this.chkTaxAcquired);
            this.Frame1.Controls.Add(this.chkBankruptcy);
            this.Frame1.Controls.Add(this.chkBookPage);
            this.Frame1.Controls.Add(this.chkLandInformation);
            this.Frame1.Controls.Add(this.chkPropertyInfo);
            this.Frame1.Controls.Add(this.chkSaleData);
            this.Frame1.Controls.Add(this.chkExemptInfo);
            this.Frame1.Controls.Add(this.chkLocation);
            this.Frame1.Controls.Add(this.chkDwelling);
            this.Frame1.Controls.Add(this.chkCommercial);
            this.Frame1.Controls.Add(this.chkOutbuilding);
            this.Frame1.Controls.Add(this.chkPreviousOwners);
            this.Frame1.Controls.Add(this.chkInterestedParties);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(517, 700);
            this.Frame1.TabIndex = 18;
            this.Frame1.Text = "Data To Import";
            // 
            // chkDeedNames
            // 
            this.chkDeedNames.Anchor = Wisej.Web.AnchorStyles.Left;
            this.chkDeedNames.Enabled = false;
            this.chkDeedNames.Location = new System.Drawing.Point(20, 67);
            this.chkDeedNames.Name = "chkDeedNames";
            this.chkDeedNames.Size = new System.Drawing.Size(106, 24);
            this.chkDeedNames.TabIndex = 1;
            this.chkDeedNames.Text = "Deed Names";
            // 
            // chkInspection
            // 
            this.chkInspection.Location = new System.Drawing.Point(312, 104);
            this.chkInspection.Name = "chkInspection";
            this.chkInspection.Size = new System.Drawing.Size(170, 24);
            this.chkInspection.TabIndex = 20;
            this.chkInspection.Text = "Inspection Information";
            // 
            // chkPictures
            // 
            this.chkPictures.Anchor = Wisej.Web.AnchorStyles.Left;
            this.chkPictures.Location = new System.Drawing.Point(312, 67);
            this.chkPictures.Name = "chkPictures";
            this.chkPictures.Size = new System.Drawing.Size(110, 24);
            this.chkPictures.TabIndex = 19;
            this.chkPictures.Text = "Picture Links";
            // 
            // ChkNameAddress
            // 
            this.ChkNameAddress.Enabled = false;
            this.ChkNameAddress.Location = new System.Drawing.Point(20, 30);
            this.ChkNameAddress.Name = "ChkNameAddress";
            this.ChkNameAddress.Size = new System.Drawing.Size(75, 24);
            this.ChkNameAddress.TabIndex = 21;
            this.ChkNameAddress.Text = "Owners";
            // 
            // chkMapLot
            // 
            this.chkMapLot.Enabled = false;
            this.chkMapLot.Location = new System.Drawing.Point(20, 104);
            this.chkMapLot.Name = "chkMapLot";
            this.chkMapLot.Size = new System.Drawing.Size(85, 24);
            this.chkMapLot.TabIndex = 2;
            this.chkMapLot.Text = "Map / Lot";
            // 
            // ChkRef1
            // 
            this.ChkRef1.Enabled = false;
            this.ChkRef1.Location = new System.Drawing.Point(20, 178);
            this.ChkRef1.Name = "ChkRef1";
            this.ChkRef1.Size = new System.Drawing.Size(102, 24);
            this.ChkRef1.TabIndex = 4;
            this.ChkRef1.Text = "Reference 1";
            // 
            // chkRef2
            // 
            this.chkRef2.Enabled = false;
            this.chkRef2.Location = new System.Drawing.Point(20, 215);
            this.chkRef2.Name = "chkRef2";
            this.chkRef2.Size = new System.Drawing.Size(102, 24);
            this.chkRef2.TabIndex = 5;
            this.chkRef2.Text = "Reference 2";
            // 
            // ChkTranLandBldg
            // 
            this.ChkTranLandBldg.Enabled = false;
            this.ChkTranLandBldg.Location = new System.Drawing.Point(20, 289);
            this.ChkTranLandBldg.Name = "ChkTranLandBldg";
            this.ChkTranLandBldg.Size = new System.Drawing.Size(225, 24);
            this.ChkTranLandBldg.TabIndex = 7;
            this.ChkTranLandBldg.Text = "Tran / Land / Bldg / Prop Codes";
            // 
            // chkTaxAcquired
            // 
            this.chkTaxAcquired.Enabled = false;
            this.chkTaxAcquired.Location = new System.Drawing.Point(20, 326);
            this.chkTaxAcquired.Name = "chkTaxAcquired";
            this.chkTaxAcquired.Size = new System.Drawing.Size(142, 24);
            this.chkTaxAcquired.TabIndex = 8;
            this.chkTaxAcquired.Text = "Tax Acquired Flag";
            // 
            // chkBankruptcy
            // 
            this.chkBankruptcy.Enabled = false;
            this.chkBankruptcy.Location = new System.Drawing.Point(20, 363);
            this.chkBankruptcy.Name = "chkBankruptcy";
            this.chkBankruptcy.Size = new System.Drawing.Size(131, 24);
            this.chkBankruptcy.TabIndex = 9;
            this.chkBankruptcy.Text = "Bankruptcy Flag";
            // 
            // chkBookPage
            // 
            this.chkBookPage.Enabled = false;
            this.chkBookPage.Location = new System.Drawing.Point(20, 252);
            this.chkBookPage.Name = "chkBookPage";
            this.chkBookPage.Size = new System.Drawing.Size(108, 24);
            this.chkBookPage.TabIndex = 6;
            this.chkBookPage.Text = "Book & Page";
            // 
            // chkLandInformation
            // 
            this.chkLandInformation.Enabled = false;
            this.chkLandInformation.Location = new System.Drawing.Point(20, 400);
            this.chkLandInformation.Name = "chkLandInformation";
            this.chkLandInformation.Size = new System.Drawing.Size(135, 24);
            this.chkLandInformation.TabIndex = 10;
            this.chkLandInformation.Text = "Land Information";
            // 
            // chkPropertyInfo
            // 
            this.chkPropertyInfo.Enabled = false;
            this.chkPropertyInfo.Location = new System.Drawing.Point(20, 437);
            this.chkPropertyInfo.Name = "chkPropertyInfo";
            this.chkPropertyInfo.Size = new System.Drawing.Size(158, 24);
            this.chkPropertyInfo.TabIndex = 11;
            this.chkPropertyInfo.Text = "Property Information";
            // 
            // chkSaleData
            // 
            this.chkSaleData.Enabled = false;
            this.chkSaleData.Location = new System.Drawing.Point(20, 474);
            this.chkSaleData.Name = "chkSaleData";
            this.chkSaleData.Size = new System.Drawing.Size(86, 24);
            this.chkSaleData.TabIndex = 12;
            this.chkSaleData.Text = "Sale Data";
            // 
            // chkExemptInfo
            // 
            this.chkExemptInfo.Enabled = false;
            this.chkExemptInfo.Location = new System.Drawing.Point(20, 511);
            this.chkExemptInfo.Name = "chkExemptInfo";
            this.chkExemptInfo.Size = new System.Drawing.Size(152, 24);
            this.chkExemptInfo.TabIndex = 13;
            this.chkExemptInfo.Text = "Exempt Information";
            // 
            // chkLocation
            // 
            this.chkLocation.Enabled = false;
            this.chkLocation.Location = new System.Drawing.Point(20, 141);
            this.chkLocation.Name = "chkLocation";
            this.chkLocation.Size = new System.Drawing.Size(82, 24);
            this.chkLocation.TabIndex = 3;
            this.chkLocation.Text = "Location";
            // 
            // chkDwelling
            // 
            this.chkDwelling.Enabled = false;
            this.chkDwelling.Location = new System.Drawing.Point(20, 622);
            this.chkDwelling.Name = "chkDwelling";
            this.chkDwelling.Size = new System.Drawing.Size(81, 24);
            this.chkDwelling.TabIndex = 16;
            this.chkDwelling.Text = "Dwelling";
            // 
            // chkCommercial
            // 
            this.chkCommercial.Enabled = false;
            this.chkCommercial.Location = new System.Drawing.Point(20, 659);
            this.chkCommercial.Name = "chkCommercial";
            this.chkCommercial.Size = new System.Drawing.Size(102, 24);
            this.chkCommercial.TabIndex = 17;
            this.chkCommercial.Text = "Commercial";
            // 
            // chkOutbuilding
            // 
            this.chkOutbuilding.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.chkOutbuilding.Enabled = false;
            this.chkOutbuilding.Location = new System.Drawing.Point(312, 30);
            this.chkOutbuilding.Name = "chkOutbuilding";
            this.chkOutbuilding.Size = new System.Drawing.Size(101, 24);
            this.chkOutbuilding.TabIndex = 18;
            this.chkOutbuilding.Text = "Outbuilding";
            // 
            // chkPreviousOwners
            // 
            this.chkPreviousOwners.Enabled = false;
            this.chkPreviousOwners.Location = new System.Drawing.Point(20, 548);
            this.chkPreviousOwners.Name = "chkPreviousOwners";
            this.chkPreviousOwners.Size = new System.Drawing.Size(185, 24);
            this.chkPreviousOwners.TabIndex = 14;
            this.chkPreviousOwners.Text = "Previous Owner Records";
            // 
            // chkInterestedParties
            // 
            this.chkInterestedParties.Enabled = false;
            this.chkInterestedParties.Location = new System.Drawing.Point(20, 585);
            this.chkInterestedParties.Name = "chkInterestedParties";
            this.chkInterestedParties.Size = new System.Drawing.Size(138, 24);
            this.chkInterestedParties.TabIndex = 15;
            this.chkInterestedParties.Text = "Interested Parties";
            // 
            // lblUpdating
            // 
            this.lblUpdating.Location = new System.Drawing.Point(589, 124);
            this.lblUpdating.Name = "lblUpdating";
            this.lblUpdating.Size = new System.Drawing.Size(231, 28);
            this.lblUpdating.TabIndex = 20;
            this.lblUpdating.Text = "UPDATING ACCOUNT";
            this.lblUpdating.Visible = false;
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(351, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(150, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmImportMasterInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(847, 510);
            this.FillColor = 0;
            this.Name = "frmImportMasterInfo";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Import Name Etc";
            this.Load += new System.EventHandler(this.frmImportMasterInfo_Load);
            this.Activated += new System.EventHandler(this.frmImportMasterInfo_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmImportMasterInfo_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeedNames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPictures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNameAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRef1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRef2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTranLandBldg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTaxAcquired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBankruptcy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBookPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLandInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPropertyInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSaleData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExemptInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDwelling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCommercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOutbuilding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreviousOwners)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterestedParties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdSave;
        public FCCheckBox chkDeedNames;
    }
}
