﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmReassignSketch.
	/// </summary>
	partial class frmReassignSketch : BaseForm
	{
		public fecherFoundation.FCTextBox txtSrcSketch;
		public fecherFoundation.FCTextBox txtDestCard;
		public fecherFoundation.FCTextBox txtSrcCard;
		public fecherFoundation.FCTextBox txtDestAccount;
		public fecherFoundation.FCTextBox txtSrcAcct;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdSaveContinue;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReassignSketch));
            this.txtSrcSketch = new fecherFoundation.FCTextBox();
            this.txtDestCard = new fecherFoundation.FCTextBox();
            this.txtSrcCard = new fecherFoundation.FCTextBox();
            this.txtDestAccount = new fecherFoundation.FCTextBox();
            this.txtSrcAcct = new fecherFoundation.FCTextBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 360);
            this.BottomPanel.Size = new System.Drawing.Size(365, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtSrcSketch);
            this.ClientArea.Controls.Add(this.txtDestCard);
            this.ClientArea.Controls.Add(this.txtSrcCard);
            this.ClientArea.Controls.Add(this.txtDestAccount);
            this.ClientArea.Controls.Add(this.txtSrcAcct);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(385, 552);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label7, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtSrcAcct, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDestAccount, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtSrcCard, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDestCard, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtSrcSketch, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(385, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(205, 30);
            this.HeaderText.Text = "Re-assign Sketch";
            // 
            // txtSrcSketch
            // 
            this.txtSrcSketch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSrcSketch.Location = new System.Drawing.Point(125, 170);
            this.txtSrcSketch.Name = "txtSrcSketch";
            this.txtSrcSketch.Size = new System.Drawing.Size(64, 40);
            this.txtSrcSketch.TabIndex = 6;
            this.txtSrcSketch.Text = "1";
            // 
            // txtDestCard
            // 
            this.txtDestCard.BackColor = System.Drawing.SystemColors.Window;
            this.txtDestCard.Location = new System.Drawing.Point(125, 320);
            this.txtDestCard.Name = "txtDestCard";
            this.txtDestCard.Size = new System.Drawing.Size(64, 40);
            this.txtDestCard.TabIndex = 11;
            // 
            // txtSrcCard
            // 
            this.txtSrcCard.BackColor = System.Drawing.SystemColors.Window;
            this.txtSrcCard.Location = new System.Drawing.Point(125, 120);
            this.txtSrcCard.Name = "txtSrcCard";
            this.txtSrcCard.Size = new System.Drawing.Size(64, 40);
            this.txtSrcCard.TabIndex = 4;
            // 
            // txtDestAccount
            // 
            this.txtDestAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtDestAccount.Location = new System.Drawing.Point(125, 270);
            this.txtDestAccount.Name = "txtDestAccount";
            this.txtDestAccount.Size = new System.Drawing.Size(182, 40);
            this.txtDestAccount.TabIndex = 9;
            // 
            // txtSrcAcct
            // 
            this.txtSrcAcct.BackColor = System.Drawing.SystemColors.Window;
            this.txtSrcAcct.Location = new System.Drawing.Point(125, 70);
            this.txtSrcAcct.Name = "txtSrcAcct";
            this.txtSrcAcct.Size = new System.Drawing.Size(182, 40);
            this.txtSrcAcct.TabIndex = 2;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 234);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(255, 20);
            this.Label7.TabIndex = 7;
            this.Label7.Text = "MOVE SKETCH TO";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 30);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(255, 20);
            this.Label6.Text = "SKETCH IS CURRENTLY ON";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(30, 334);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(62, 17);
            this.Label5.TabIndex = 10;
            this.Label5.Text = "CARD";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 284);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(62, 17);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "ACCOUNT";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 184);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(62, 17);
            this.Label3.TabIndex = 5;
            this.Label3.Text = "SKETCH";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 134);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(62, 17);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "CARD";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 84);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(62, 17);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "ACCOUNT";
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(100, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(185, 48);
            this.cmdSaveContinue.Text = "Save & Continue";
            this.cmdSaveContinue.Click += new System.EventHandler(this.cmdReAssign_Click);
            // 
            // frmReassignSketch
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(385, 612);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmReassignSketch";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Re-assign Sketch";
            this.Load += new System.EventHandler(this.frmReassignSketch_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReassignSketch_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
