﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetAccountsToPrint.
	/// </summary>
	public partial class frmGetAccountsToPrint : BaseForm
	{
		//FC:FINAL:RPU: #i1302 - Add a flag to see if the continue button was pressed
		private bool continueFlag = false;

		public frmGetAccountsToPrint()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetAccountsToPrint InstancePtr
		{
			get
			{
				return (frmGetAccountsToPrint)Sys.GetInstance(typeof(frmGetAccountsToPrint));
			}
		}

		protected frmGetAccountsToPrint _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strREFullDBName;
		string strMasterJoin;
		string strMasterJoinJoin = "";
		string strMasterJoinQuery;
		// Dim dbTemp As DAO.Database
		clsDRWrapper rsTemp = new clsDRWrapper();
		string sqlString;
		int lngUID;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			this.Unload();
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(new Button(), new System.EventArgs());
		}

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void cmdDone_Click()
		{
			cmdDone_Click(cmdDone, new System.EventArgs());
		}

		private void frmGetAccountsToPrint_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGetAccountsToPrint_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetAccountsToPrint properties;
			//frmGetAccountsToPrint.ScaleWidth	= 5880;
			//frmGetAccountsToPrint.ScaleHeight	= 4830;
			//frmGetAccountsToPrint.LinkTopic	= "Form1";
			//frmGetAccountsToPrint.LockControls	= true;
			//End Unmaped Properties
			strREFullDBName = rsTemp.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			clsDRWrapper dbTemp = new clsDRWrapper();
			dbTemp.Execute("delete from labelaccounts where userid = " + FCConvert.ToString(lngUID), modGlobalVariables.strREDatabase);
		}

		private void Label10_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'H%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label11_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'I%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label12_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'J%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label13_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'K%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label14_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'L%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label15_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'M%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label16_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'N%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label17_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'O%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label18_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'P%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label19_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'Q%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label20_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'R%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label21_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'S%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label22_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'T%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label23_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'U%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label24_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'V%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label25_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'W%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label26_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'X%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label27_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'Y%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label28_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'Z%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label29_Click(object sender, System.EventArgs e)
		{
			sqlString = strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and ((rsname not like 'A%' and rsname not like 'B%' and rsname not like 'C%' and rsname not like 'D%' and rsname not like 'E%' and rsname not like 'F%' and rsname not like 'G%' and rsname not like 'H%' and rsname not like 'I%'";
			sqlString += " and rsname not like 'J%' and rsname not like 'K%' and rsname not like 'L%' and rsname not like 'M%' and rsname not like 'N%' and rsname not like 'O%' and rsname not like 'P%'";
			sqlString += " and rsname not like 'Q%' and rsname not like 'R%' and rsname not like 'S%' and rsname not like 'T%' and rsname not like 'U%' and rsname not like 'V%' and rsname not like 'W%'";
			sqlString += " and rsname not like 'X%' and rsname not like 'Y%' and rsname not like 'Z%'))order by rsname";
			rsTemp.OpenRecordset(sqlString, modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label3_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'A%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label4_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'B%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label5_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'C%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label6_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'D%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void FillListAccounts()
		{
			lstAccounts.Clear();
			lstAccounts.AddItem("No accounts in this group.");
			if (!rsTemp.EndOfFile())
			{
				lstAccounts.Clear();
			}
			while (!rsTemp.EndOfFile())
			{
				lstAccounts.AddItem(Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("rsaccount")), "@@@@@@") + "  " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSNAME"))));
				rsTemp.MoveNext();
			}
		}

		private void Label7_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + " where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'E%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void GetLikeLetter(string strLetter)
		{
			rsTemp.OpenRecordset(strMasterJoin + "  where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like '" + strLetter + "%') order by rsname", modGlobalVariables.strREDatabase);
		}

		private void Label8_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + "  where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'F%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void Label9_Click(object sender, System.EventArgs e)
		{
			rsTemp.OpenRecordset(strMasterJoin + "  where (rsdeleted = 0) and rscard = 1 and (upper(rsname) like 'G%') order by rsname", modGlobalVariables.strREDatabase);
			FillListAccounts();
		}

		private void List1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = (Keys)0;
						if (List1.SelectedIndex >= 0)
						{
							List1.Items.RemoveAt(List1.SelectedIndex);
						}
						break;
					}
			}
			//end switch
		}

		private void lstAccounts_Click(object sender, System.EventArgs e)
		{
			int lngANum;
			lngANum = FCConvert.ToInt32(Math.Round(Conversion.Val(lstAccounts.Items[lstAccounts.SelectedIndex].Text)));
			// dbTemp.Execute ("insert into LabelAccounts (accountnumber,userid) values (" & lngANum & "," & lngUID & ")")
			// MsgBox "Account number " & lngANum & " recorded for printing", vbOKOnly, "Account Number Saved"
			List1.AddItem(lngANum.ToString());
			List1.TopIndex = List1.Items.Count - 1;
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			int x;
			clsDRWrapper rsExec = new clsDRWrapper();
			int lngANum = 0;
			if (List1.Items.Count > 0)
			{
				for (x = 0; x <= List1.Items.Count - 1; x++)
				{
					lngANum = FCConvert.ToInt32(Math.Round(Conversion.Val(List1.Items[x].Text)));
					rsExec.Execute("insert into LabelAccounts (accountnumber,userid) values (" + FCConvert.ToString(lngANum) + "," + FCConvert.ToString(lngUID) + ")", modGlobalVariables.strREDatabase);
				}
				// x
			}
			//FC:FINAL:RPU:#i1302 - Set flag true to know that the continue button was pressed
			continueFlag = true;
			cmdDone_Click();
			continueFlag = false;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdCancel_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void txtAcctNum_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int lngANum = 0;
			if (KeyCode == Keys.Return)
			{
				lngANum = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAcctNum.Text + "")));
				// dbTemp.Execute ("insert into labelaccounts (accountnumber,userid) values (" & lngANum & "," & lngUID & ")")
				// MsgBox "Account number " & lngANum & " recorded for printing", vbOKOnly, "Account Number Saved"
				List1.AddItem(lngANum.ToString());
				List1.TopIndex = List1.Items.Count - 1;
				txtAcctNum.Text = "";
			}
		}
		//FC:FINAL:RPU: #i1302 - Made formClosing event and call mnuExit if the continue button was pressed
		private void frmGetAccountsToPrint_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!continueFlag)
			{
				mnuExit_Click();
			}
		}
	}
}
