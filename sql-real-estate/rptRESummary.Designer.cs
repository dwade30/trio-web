﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptRESummary.
	/// </summary>
	partial class rptRESummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRESummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLandExceed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBldgExceed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandPercUp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandPercDown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgPercUp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgPercDown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandUp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandDown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgUp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgDown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptUp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptDown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandChgVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgChgVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptChgVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNoMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeleted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBadCodes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBadExemptVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBadMaps = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBadUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNotUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNewBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOtherAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtlandNotUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgNotUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptNotUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalNetNotUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLandNotXfered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgNotXfered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptNotXfered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNetNotXfered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLandExceed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBldgExceed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandPercUp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandPercDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgPercUp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgPercDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgUp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptUp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandChgVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgChgVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptChgVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBadCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBadExemptVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBadMaps)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBadUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtlandNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNetNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandNotXfered)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgNotXfered)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptNotXfered)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetNotXfered)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.lblLandExceed,
				this.Label11,
				this.Label12,
				this.lblBldgExceed,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.txtTotalLand,
				this.txtTotalBuilding,
				this.txtTotalExempt,
				this.txtTotalNet,
				this.txtLandPercUp,
				this.txtLandPercDown,
				this.txtBldgPercUp,
				this.txtBldgPercDown,
				this.txtLandUp,
				this.txtLandDown,
				this.txtBldgUp,
				this.txtBldgDown,
				this.txtExemptUp,
				this.txtExemptDown,
				this.txtLandChgVal,
				this.txtBldgChgVal,
				this.txtExemptChgVal,
				this.Label22,
				this.lblMuniname,
				this.lblTime,
				this.lblDate,
				this.txtUpdated,
				this.txtNoMatch,
				this.txtDeleted,
				this.txtBadCodes,
				this.txtBadExemptVal,
				this.txtBadMaps,
				this.txtBadUpdated,
				this.txtNotUpdated,
				this.txtNewLand,
				this.txtNewBldg,
				this.Label23,
				this.Label24,
				this.txtOtherAmount,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.txtlandNotUpdated,
				this.txtBldgNotUpdated,
				this.txtExemptNotUpdated,
				this.txtTotalNetNotUpdated,
				this.Label30,
				this.Label31,
				this.Label32,
				this.Label33,
				this.Label34,
				this.Label35,
				this.txtLandNotXfered,
				this.txtBldgNotXfered,
				this.txtExemptNotXfered,
				this.txtNetNotXfered
			});
			this.Detail.Height = 6.833333F;
			this.Detail.Name = "Detail";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold";
			this.Label1.Text = "Will Be Updated";
			this.Label1.Top = 0.6875F;
			this.Label1.Width = 1.6875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Bad Deleted";
			this.Label2.Top = 1.0625F;
			this.Label2.Width = 1.6875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.0625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "Bad Exempt Codes";
			this.Label4.Top = 1.25F;
			this.Label4.Width = 1.6875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold";
			this.Label5.Text = "Bad Exempt Amount";
			this.Label5.Top = 1.4375F;
			this.Label5.Width = 1.6875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold";
			this.Label6.Text = "Bad Map / Lot";
			this.Label6.Top = 1.625F;
			this.Label6.Width = 1.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Updated More Than Once";
			this.Label7.Top = 1.8125F;
			this.Label7.Width = 2F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.0625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "Not Updated";
			this.Label8.Top = 2F;
			this.Label8.Width = 1.6875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Text = "No Match";
			this.Label9.Top = 0.875F;
			this.Label9.Width = 1.6875F;
			// 
			// lblLandExceed
			// 
			this.lblLandExceed.Height = 0.1875F;
			this.lblLandExceed.HyperLink = null;
			this.lblLandExceed.Left = 0.0625F;
			this.lblLandExceed.Name = "lblLandExceed";
			this.lblLandExceed.Style = "font-weight: bold";
			this.lblLandExceed.Text = "Land Exceeds % Change";
			this.lblLandExceed.Top = 2.875F;
			this.lblLandExceed.Width = 2.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.8125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: right";
			this.Label11.Text = "Up";
			this.Label11.Top = 2.6875F;
			this.Label11.Width = 0.375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 3.75F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: right";
			this.Label12.Text = "Down";
			this.Label12.Top = 2.6875F;
			this.Label12.Width = 0.5625F;
			// 
			// lblBldgExceed
			// 
			this.lblBldgExceed.Height = 0.1875F;
			this.lblBldgExceed.HyperLink = null;
			this.lblBldgExceed.Left = 0.0625F;
			this.lblBldgExceed.Name = "lblBldgExceed";
			this.lblBldgExceed.Style = "font-weight: bold";
			this.lblBldgExceed.Text = "Building Exceeds % Change";
			this.lblBldgExceed.Top = 3.0625F;
			this.lblBldgExceed.Width = 2.3125F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.0625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold";
			this.Label14.Text = "Land Value Change";
			this.Label14.Top = 3.5F;
			this.Label14.Width = 1.6875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.0625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold";
			this.Label15.Text = "Building Value Change";
			this.Label15.Top = 3.6875F;
			this.Label15.Width = 2.0625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.0625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold";
			this.Label16.Text = "Exemption Change";
			this.Label16.Top = 3.875F;
			this.Label16.Width = 1.6875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.25F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 1.25F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold; text-align: right";
			this.Label17.Text = "Total";
			this.Label17.Top = 5.583333F;
			this.Label17.Width = 0.8333333F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.583333F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold; text-align: right";
			this.Label18.Text = "Land";
			this.Label18.Top = 5.416667F;
			this.Label18.Width = 0.5833333F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3.5F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold; text-align: right";
			this.Label19.Text = "Building";
			this.Label19.Top = 5.416667F;
			this.Label19.Width = 0.8333333F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 4.666667F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold; text-align: right";
			this.Label20.Text = "Exempt";
			this.Label20.Top = 5.416667F;
			this.Label20.Width = 0.6666667F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 5.416667F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-weight: bold; text-align: right";
			this.Label21.Text = "Net";
			this.Label21.Top = 5.416667F;
			this.Label21.Width = 0.9166667F;
			// 
			// txtTotalLand
			// 
			this.txtTotalLand.Height = 0.1875F;
			this.txtTotalLand.Left = 2.25F;
			this.txtTotalLand.Name = "txtTotalLand";
			this.txtTotalLand.Style = "text-align: right";
			this.txtTotalLand.Text = "0";
			this.txtTotalLand.Top = 5.604167F;
			this.txtTotalLand.Width = 0.9166667F;
			// 
			// txtTotalBuilding
			// 
			this.txtTotalBuilding.Height = 0.1875F;
			this.txtTotalBuilding.Left = 3.416667F;
			this.txtTotalBuilding.Name = "txtTotalBuilding";
			this.txtTotalBuilding.Style = "text-align: right";
			this.txtTotalBuilding.Text = "0";
			this.txtTotalBuilding.Top = 5.604167F;
			this.txtTotalBuilding.Width = 0.9166667F;
			// 
			// txtTotalExempt
			// 
			this.txtTotalExempt.Height = 0.1875F;
			this.txtTotalExempt.Left = 4.416667F;
			this.txtTotalExempt.Name = "txtTotalExempt";
			this.txtTotalExempt.Style = "text-align: right";
			this.txtTotalExempt.Text = "0";
			this.txtTotalExempt.Top = 5.583333F;
			this.txtTotalExempt.Width = 0.9166667F;
			// 
			// txtTotalNet
			// 
			this.txtTotalNet.Height = 0.1875F;
			this.txtTotalNet.Left = 5.416667F;
			this.txtTotalNet.Name = "txtTotalNet";
			this.txtTotalNet.Style = "text-align: right";
			this.txtTotalNet.Text = "0";
			this.txtTotalNet.Top = 5.583333F;
			this.txtTotalNet.Width = 0.9166667F;
			// 
			// txtLandPercUp
			// 
			this.txtLandPercUp.Height = 0.1875F;
			this.txtLandPercUp.Left = 2.4375F;
			this.txtLandPercUp.Name = "txtLandPercUp";
			this.txtLandPercUp.Style = "text-align: right";
			this.txtLandPercUp.Text = "0";
			this.txtLandPercUp.Top = 2.875F;
			this.txtLandPercUp.Width = 0.75F;
			// 
			// txtLandPercDown
			// 
			this.txtLandPercDown.Height = 0.1875F;
			this.txtLandPercDown.Left = 3.375F;
			this.txtLandPercDown.Name = "txtLandPercDown";
			this.txtLandPercDown.Style = "text-align: right";
			this.txtLandPercDown.Text = "0";
			this.txtLandPercDown.Top = 2.875F;
			this.txtLandPercDown.Width = 0.9375F;
			// 
			// txtBldgPercUp
			// 
			this.txtBldgPercUp.Height = 0.1875F;
			this.txtBldgPercUp.Left = 2.4375F;
			this.txtBldgPercUp.Name = "txtBldgPercUp";
			this.txtBldgPercUp.Style = "text-align: right";
			this.txtBldgPercUp.Text = "0";
			this.txtBldgPercUp.Top = 3.0625F;
			this.txtBldgPercUp.Width = 0.75F;
			// 
			// txtBldgPercDown
			// 
			this.txtBldgPercDown.Height = 0.1875F;
			this.txtBldgPercDown.Left = 3.375F;
			this.txtBldgPercDown.Name = "txtBldgPercDown";
			this.txtBldgPercDown.Style = "text-align: right";
			this.txtBldgPercDown.Text = "0";
			this.txtBldgPercDown.Top = 3.0625F;
			this.txtBldgPercDown.Width = 0.9375F;
			// 
			// txtLandUp
			// 
			this.txtLandUp.Height = 0.1875F;
			this.txtLandUp.Left = 2.25F;
			this.txtLandUp.Name = "txtLandUp";
			this.txtLandUp.Style = "text-align: right";
			this.txtLandUp.Text = "0";
			this.txtLandUp.Top = 3.5F;
			this.txtLandUp.Width = 0.9375F;
			// 
			// txtLandDown
			// 
			this.txtLandDown.Height = 0.1875F;
			this.txtLandDown.Left = 3.375F;
			this.txtLandDown.Name = "txtLandDown";
			this.txtLandDown.Style = "text-align: right";
			this.txtLandDown.Text = "0";
			this.txtLandDown.Top = 3.5F;
			this.txtLandDown.Width = 0.9375F;
			// 
			// txtBldgUp
			// 
			this.txtBldgUp.Height = 0.1875F;
			this.txtBldgUp.Left = 2.4375F;
			this.txtBldgUp.Name = "txtBldgUp";
			this.txtBldgUp.Style = "text-align: right";
			this.txtBldgUp.Text = "0";
			this.txtBldgUp.Top = 3.6875F;
			this.txtBldgUp.Width = 0.75F;
			// 
			// txtBldgDown
			// 
			this.txtBldgDown.Height = 0.1875F;
			this.txtBldgDown.Left = 3.375F;
			this.txtBldgDown.Name = "txtBldgDown";
			this.txtBldgDown.Style = "text-align: right";
			this.txtBldgDown.Text = "0";
			this.txtBldgDown.Top = 3.6875F;
			this.txtBldgDown.Width = 0.9375F;
			// 
			// txtExemptUp
			// 
			this.txtExemptUp.Height = 0.1875F;
			this.txtExemptUp.Left = 2.25F;
			this.txtExemptUp.Name = "txtExemptUp";
			this.txtExemptUp.Style = "text-align: right";
			this.txtExemptUp.Text = "0";
			this.txtExemptUp.Top = 3.875F;
			this.txtExemptUp.Width = 0.9375F;
			// 
			// txtExemptDown
			// 
			this.txtExemptDown.Height = 0.1875F;
			this.txtExemptDown.Left = 3.375F;
			this.txtExemptDown.Name = "txtExemptDown";
			this.txtExemptDown.Style = "text-align: right";
			this.txtExemptDown.Text = "0";
			this.txtExemptDown.Top = 3.875F;
			this.txtExemptDown.Width = 0.9375F;
			// 
			// txtLandChgVal
			// 
			this.txtLandChgVal.Height = 0.1875F;
			this.txtLandChgVal.Left = 5.375F;
			this.txtLandChgVal.Name = "txtLandChgVal";
			this.txtLandChgVal.Style = "text-align: right";
			this.txtLandChgVal.Text = "0";
			this.txtLandChgVal.Top = 3.5F;
			this.txtLandChgVal.Width = 0.9375F;
			// 
			// txtBldgChgVal
			// 
			this.txtBldgChgVal.Height = 0.1875F;
			this.txtBldgChgVal.Left = 5.375F;
			this.txtBldgChgVal.Name = "txtBldgChgVal";
			this.txtBldgChgVal.Style = "text-align: right";
			this.txtBldgChgVal.Text = "0";
			this.txtBldgChgVal.Top = 3.6875F;
			this.txtBldgChgVal.Width = 0.9375F;
			// 
			// txtExemptChgVal
			// 
			this.txtExemptChgVal.Height = 0.1875F;
			this.txtExemptChgVal.Left = 5.375F;
			this.txtExemptChgVal.Name = "txtExemptChgVal";
			this.txtExemptChgVal.Style = "text-align: right";
			this.txtExemptChgVal.Text = "0";
			this.txtExemptChgVal.Top = 3.875F;
			this.txtExemptChgVal.Width = 0.9375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.25F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 2.5F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label22.Text = "Billing Transfer Summary";
			this.Label22.Top = 0F;
			this.Label22.Width = 2.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniname.Height = 0.1875F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniName";
			this.lblMuniname.Style = "";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2.25F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 2.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.8125F;
			// 
			// txtUpdated
			// 
			this.txtUpdated.Height = 0.1875F;
			this.txtUpdated.Left = 2F;
			this.txtUpdated.Name = "txtUpdated";
			this.txtUpdated.Style = "text-align: right";
			this.txtUpdated.Text = "0";
			this.txtUpdated.Top = 0.6875F;
			this.txtUpdated.Width = 0.625F;
			// 
			// txtNoMatch
			// 
			this.txtNoMatch.Height = 0.1875F;
			this.txtNoMatch.Left = 1.875F;
			this.txtNoMatch.Name = "txtNoMatch";
			this.txtNoMatch.Style = "text-align: right";
			this.txtNoMatch.Text = "0";
			this.txtNoMatch.Top = 0.875F;
			this.txtNoMatch.Width = 0.75F;
			// 
			// txtDeleted
			// 
			this.txtDeleted.Height = 0.1875F;
			this.txtDeleted.Left = 1.875F;
			this.txtDeleted.Name = "txtDeleted";
			this.txtDeleted.Style = "text-align: right";
			this.txtDeleted.Text = "0";
			this.txtDeleted.Top = 1.0625F;
			this.txtDeleted.Width = 0.75F;
			// 
			// txtBadCodes
			// 
			this.txtBadCodes.Height = 0.1875F;
			this.txtBadCodes.Left = 1.875F;
			this.txtBadCodes.Name = "txtBadCodes";
			this.txtBadCodes.Style = "text-align: right";
			this.txtBadCodes.Text = "0";
			this.txtBadCodes.Top = 1.25F;
			this.txtBadCodes.Width = 0.75F;
			// 
			// txtBadExemptVal
			// 
			this.txtBadExemptVal.Height = 0.1875F;
			this.txtBadExemptVal.Left = 1.875F;
			this.txtBadExemptVal.Name = "txtBadExemptVal";
			this.txtBadExemptVal.Style = "text-align: right";
			this.txtBadExemptVal.Text = "0";
			this.txtBadExemptVal.Top = 1.4375F;
			this.txtBadExemptVal.Width = 0.75F;
			// 
			// txtBadMaps
			// 
			this.txtBadMaps.Height = 0.1875F;
			this.txtBadMaps.Left = 1.875F;
			this.txtBadMaps.Name = "txtBadMaps";
			this.txtBadMaps.Style = "text-align: right";
			this.txtBadMaps.Text = "0";
			this.txtBadMaps.Top = 1.625F;
			this.txtBadMaps.Width = 0.75F;
			// 
			// txtBadUpdated
			// 
			this.txtBadUpdated.Height = 0.1875F;
			this.txtBadUpdated.Left = 2F;
			this.txtBadUpdated.Name = "txtBadUpdated";
			this.txtBadUpdated.Style = "text-align: right";
			this.txtBadUpdated.Text = "0";
			this.txtBadUpdated.Top = 1.8125F;
			this.txtBadUpdated.Width = 0.625F;
			// 
			// txtNotUpdated
			// 
			this.txtNotUpdated.Height = 0.1875F;
			this.txtNotUpdated.Left = 1.875F;
			this.txtNotUpdated.Name = "txtNotUpdated";
			this.txtNotUpdated.Style = "text-align: right";
			this.txtNotUpdated.Text = "0";
			this.txtNotUpdated.Top = 2F;
			this.txtNotUpdated.Width = 0.75F;
			// 
			// txtNewLand
			// 
			this.txtNewLand.Height = 0.1875F;
			this.txtNewLand.Left = 4.375F;
			this.txtNewLand.Name = "txtNewLand";
			this.txtNewLand.Style = "text-align: right";
			this.txtNewLand.Text = "0";
			this.txtNewLand.Top = 3.5F;
			this.txtNewLand.Width = 0.9375F;
			// 
			// txtNewBldg
			// 
			this.txtNewBldg.Height = 0.1875F;
			this.txtNewBldg.Left = 4.375F;
			this.txtNewBldg.Name = "txtNewBldg";
			this.txtNewBldg.Style = "text-align: right";
			this.txtNewBldg.Text = "0";
			this.txtNewBldg.Top = 3.6875F;
			this.txtNewBldg.Width = 0.9375F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 4.75F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-weight: bold; text-align: right";
			this.Label23.Text = "New";
			this.Label23.Top = 3.3125F;
			this.Label23.Width = 0.5625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.125F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-weight: bold; text-align: left";
			this.Label24.Text = "Other Amount";
			this.Label24.Top = 4.3125F;
			this.Label24.Width = 1.125F;
			// 
			// txtOtherAmount
			// 
			this.txtOtherAmount.Height = 0.1875F;
			this.txtOtherAmount.Left = 1.4375F;
			this.txtOtherAmount.Name = "txtOtherAmount";
			this.txtOtherAmount.Style = "text-align: right";
			this.txtOtherAmount.Text = "0";
			this.txtOtherAmount.Top = 4.3125F;
			this.txtOtherAmount.Width = 0.9375F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.19F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.08333334F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-weight: bold; text-align: right";
			this.Label25.Text = "Total w/o Not Updateds";
			this.Label25.Top = 6.083333F;
			this.Label25.Width = 2F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 2.583333F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-weight: bold; text-align: right";
			this.Label26.Text = "Land";
			this.Label26.Top = 5.916667F;
			this.Label26.Width = 0.5833333F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 3.5F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-weight: bold; text-align: right";
			this.Label27.Text = "Building";
			this.Label27.Top = 5.916667F;
			this.Label27.Width = 0.8333333F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 4.666667F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-weight: bold; text-align: right";
			this.Label28.Text = "Exempt";
			this.Label28.Top = 5.916667F;
			this.Label28.Width = 0.6666667F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 5.416667F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-weight: bold; text-align: right";
			this.Label29.Text = "Net";
			this.Label29.Top = 5.916667F;
			this.Label29.Width = 0.9166667F;
			// 
			// txtlandNotUpdated
			// 
			this.txtlandNotUpdated.Height = 0.1875F;
			this.txtlandNotUpdated.Left = 2.25F;
			this.txtlandNotUpdated.Name = "txtlandNotUpdated";
			this.txtlandNotUpdated.Style = "text-align: right";
			this.txtlandNotUpdated.Text = "0";
			this.txtlandNotUpdated.Top = 6.083333F;
			this.txtlandNotUpdated.Width = 0.9166667F;
			// 
			// txtBldgNotUpdated
			// 
			this.txtBldgNotUpdated.Height = 0.1875F;
			this.txtBldgNotUpdated.Left = 3.416667F;
			this.txtBldgNotUpdated.Name = "txtBldgNotUpdated";
			this.txtBldgNotUpdated.Style = "text-align: right";
			this.txtBldgNotUpdated.Text = "0";
			this.txtBldgNotUpdated.Top = 6.083333F;
			this.txtBldgNotUpdated.Width = 0.9166667F;
			// 
			// txtExemptNotUpdated
			// 
			this.txtExemptNotUpdated.Height = 0.1875F;
			this.txtExemptNotUpdated.Left = 4.416667F;
			this.txtExemptNotUpdated.Name = "txtExemptNotUpdated";
			this.txtExemptNotUpdated.Style = "text-align: right";
			this.txtExemptNotUpdated.Text = "0";
			this.txtExemptNotUpdated.Top = 6.083333F;
			this.txtExemptNotUpdated.Width = 0.9166667F;
			// 
			// txtTotalNetNotUpdated
			// 
			this.txtTotalNetNotUpdated.Height = 0.1875F;
			this.txtTotalNetNotUpdated.Left = 5.416667F;
			this.txtTotalNetNotUpdated.Name = "txtTotalNetNotUpdated";
			this.txtTotalNetNotUpdated.Style = "text-align: right";
			this.txtTotalNetNotUpdated.Text = "0";
			this.txtTotalNetNotUpdated.Top = 6.083333F;
			this.txtTotalNetNotUpdated.Width = 0.9166667F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.25F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 1.25F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "text-align: center";
			this.Label30.Text = "(Totals do not include figures from accounts in the Bad Accounts list)";
			this.Label30.Top = 6.333333F;
			this.Label30.Width = 4.75F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.3333333F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0.08333334F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-weight: bold; text-align: right";
			this.Label31.Text = "New Accounts Not To Be Transferred";
			this.Label31.Top = 4.916667F;
			this.Label31.Width = 2F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 2.583333F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-weight: bold; text-align: right";
			this.Label32.Text = "Land";
			this.Label32.Top = 4.916667F;
			this.Label32.Width = 0.5833333F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 3.5F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-weight: bold; text-align: right";
			this.Label33.Text = "Building";
			this.Label33.Top = 4.916667F;
			this.Label33.Width = 0.8333333F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 4.666667F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-weight: bold; text-align: right";
			this.Label34.Text = "Exempt";
			this.Label34.Top = 4.916667F;
			this.Label34.Width = 0.6666667F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 5.416667F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-weight: bold; text-align: right";
			this.Label35.Text = "Net";
			this.Label35.Top = 4.916667F;
			this.Label35.Width = 0.9166667F;
			// 
			// txtLandNotXfered
			// 
			this.txtLandNotXfered.Height = 0.1875F;
			this.txtLandNotXfered.Left = 2.25F;
			this.txtLandNotXfered.Name = "txtLandNotXfered";
			this.txtLandNotXfered.Style = "text-align: right";
			this.txtLandNotXfered.Text = "0";
			this.txtLandNotXfered.Top = 5.083333F;
			this.txtLandNotXfered.Width = 0.9166667F;
			// 
			// txtBldgNotXfered
			// 
			this.txtBldgNotXfered.Height = 0.1875F;
			this.txtBldgNotXfered.Left = 3.416667F;
			this.txtBldgNotXfered.Name = "txtBldgNotXfered";
			this.txtBldgNotXfered.Style = "text-align: right";
			this.txtBldgNotXfered.Text = "0";
			this.txtBldgNotXfered.Top = 5.083333F;
			this.txtBldgNotXfered.Width = 0.9166667F;
			// 
			// txtExemptNotXfered
			// 
			this.txtExemptNotXfered.Height = 0.1875F;
			this.txtExemptNotXfered.Left = 4.416667F;
			this.txtExemptNotXfered.Name = "txtExemptNotXfered";
			this.txtExemptNotXfered.Style = "text-align: right";
			this.txtExemptNotXfered.Text = "0";
			this.txtExemptNotXfered.Top = 5.083333F;
			this.txtExemptNotXfered.Width = 0.9166667F;
			// 
			// txtNetNotXfered
			// 
			this.txtNetNotXfered.Height = 0.1875F;
			this.txtNetNotXfered.Left = 5.416667F;
			this.txtNetNotXfered.Name = "txtNetNotXfered";
			this.txtNetNotXfered.Style = "text-align: right";
			this.txtNetNotXfered.Text = "0";
			this.txtNetNotXfered.Top = 5.083333F;
			this.txtNetNotXfered.Width = 0.9166667F;
			// 
			// rptRESummary
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.Script = "\r\nSub ActiveReport_ReportStart\r\n dim x as integer\r\nx = 5\r\nEnd Sub\r\n";
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLandExceed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBldgExceed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandPercUp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandPercDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgPercUp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgPercDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgUp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptUp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandChgVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgChgVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptChgVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBadCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBadExemptVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBadMaps)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBadUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNewBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtlandNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNetNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandNotXfered)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgNotXfered)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptNotXfered)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetNotXfered)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLandExceed;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBldgExceed;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandPercUp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandPercDown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgPercUp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgPercDown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandUp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandDown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgUp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgDown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptUp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptDown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandChgVal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgChgVal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptChgVal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNoMatch;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeleted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBadCodes;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBadExemptVal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBadMaps;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBadUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNewBldg;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtlandNotUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgNotUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptNotUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalNetNotUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandNotXfered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgNotXfered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptNotXfered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNetNotXfered;
	}
}
