﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for FrmChooseSR.
	/// </summary>
	partial class FrmChooseSR : BaseForm
	{
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCListBox List1;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmChooseSR));
			this.cmdCancel = new fecherFoundation.FCButton();
			this.List1 = new fecherFoundation.FCListBox();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 489);
			this.BottomPanel.Size = new System.Drawing.Size(656, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.List1);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(656, 429);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(656, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(251, 30);
			this.HeaderText.Text = "Choose Sales Record";
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(30, 354);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(70, 40);
			this.cmdCancel.TabIndex = 1;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// List1
			// 
			this.List1.Appearance = 0;
			this.List1.BackColor = System.Drawing.SystemColors.Window;
			this.List1.Location = new System.Drawing.Point(30, 57);
			this.List1.MultiSelect = 0;
			this.List1.Name = "List1";
			this.List1.Size = new System.Drawing.Size(596, 277);
			this.List1.Sorted = false;
			this.List1.TabIndex = 0;
			this.List1.DoubleClick += new System.EventHandler(this.List1_DoubleClick);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(380, 30);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(66, 17);
			this.Label4.TabIndex = 5;
			this.Label4.Text = "SALE DATE";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(249, 30);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(60, 17);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "MAP/LOT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(100, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(40, 18);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "NAME";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(60, 17);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "ACCOUNT";
			// 
			// FrmChooseSR
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(656, 597);
			this.ControlBox = false;
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "FrmChooseSR";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Choose Sales Record";
			this.Load += new System.EventHandler(this.FrmChooseSR_Load);
			this.Activated += new System.EventHandler(this.FrmChooseSR_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.FrmChooseSR_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
