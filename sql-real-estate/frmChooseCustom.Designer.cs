﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmChooseCustom.
	/// </summary>
	partial class frmChooseCustom : BaseForm
	{
		public fecherFoundation.FCButton cmdLoad;
		public fecherFoundation.FCGrid Grid1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChooseCustom));
			this.cmdLoad = new fecherFoundation.FCButton();
			this.Grid1 = new fecherFoundation.FCGrid();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoad)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdLoad);
			this.BottomPanel.Location = new System.Drawing.Point(0, 450);
			this.BottomPanel.Size = new System.Drawing.Size(636, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Grid1);
			this.ClientArea.Size = new System.Drawing.Size(636, 390);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(636, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(147, 30);
			this.HeaderText.Text = "Load Report";
			// 
			// cmdLoad
			// 
			this.cmdLoad.AppearanceKey = "acceptButton";
			this.cmdLoad.Location = new System.Drawing.Point(279, 30);
			this.cmdLoad.Name = "cmdLoad";
			this.cmdLoad.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdLoad.Size = new System.Drawing.Size(103, 48);
			this.cmdLoad.TabIndex = 1;
			this.cmdLoad.Text = "Load";
			//FC:FINAL:MSH - i.issue #1171: show 'Load/Delete' btn
			//this.cmdLoad.Visible = false;
			this.cmdLoad.Click += new System.EventHandler(this.cmdLoad_Click);
			// 
			// Grid1
			// 
			this.Grid1.AllowSelection = false;
			this.Grid1.AllowUserToResizeColumns = false;
			this.Grid1.AllowUserToResizeRows = false;
			this.Grid1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid1.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid1.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid1.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid1.BackColorSel = System.Drawing.Color.Empty;
			this.Grid1.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid1.ColumnHeadersHeight = 30;
			this.Grid1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid1.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid1.DragIcon = null;
			this.Grid1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid1.ExtendLastCol = true;
			this.Grid1.FixedCols = 0;
			this.Grid1.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid1.FrozenCols = 0;
			this.Grid1.GridColor = System.Drawing.Color.Empty;
			this.Grid1.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid1.Location = new System.Drawing.Point(30, 30);
			this.Grid1.Name = "Grid1";
			this.Grid1.ReadOnly = true;
			this.Grid1.RowHeadersVisible = false;
			this.Grid1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid1.RowHeightMin = 0;
			this.Grid1.Rows = 50;
			this.Grid1.ScrollTipText = null;
			this.Grid1.ShowColumnVisibilityMenu = false;
			this.Grid1.Size = new System.Drawing.Size(586, 340);
			this.Grid1.StandardTab = true;
			this.Grid1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid1.TabIndex = 0;
			this.Grid1.DoubleClick += new System.EventHandler(this.Grid1_DblClick);
			// 
			// frmChooseCustom
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(636, 558);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmChooseCustom";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Load Report";
			this.Load += new System.EventHandler(this.frmChooseCustom_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmChooseCustom_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoad)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
