﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptListingNameAddrLocMapAsses.
	/// </summary>
	partial class rptListingNameAddrLocMapAsses
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptListingNameAddrLocMapAsses));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMap = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSecondOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCards = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMap)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCards)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtName,
				this.txtLocation,
				this.txtMap,
				this.txtCard,
				this.txtLand,
				this.txtBuilding,
				this.txtExemption,
				this.txtTotal,
				this.txtAddress,
				this.txtaddress2,
				this.txtaddress3,
				this.txtSecondOwner
			});
			this.Detail.Height = 1.322917F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotalLand,
				this.txtTotalBuilding,
				this.txtTotalExemption,
				this.txtTotalTotal,
				this.Line2,
				this.Label8,
				this.Label9,
				this.txtCount,
				this.lblCards
			});
			this.ReportFooter.Height = 0.3333333F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCaption,
				this.txtCaption2,
				this.Label1,
				this.Line1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage
			});
			this.PageHeader.Height = 0.78125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.21875F;
			this.txtCaption.Left = 1.4375F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.txtCaption.Text = "Account";
			this.txtCaption.Top = 0.03125F;
			this.txtCaption.Width = 4.25F;
			// 
			// txtCaption2
			// 
			this.txtCaption2.Height = 0.1875F;
			this.txtCaption2.Left = 1.4375F;
			this.txtCaption2.Name = "txtCaption2";
			this.txtCaption2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.txtCaption2.Text = "Account";
			this.txtCaption2.Top = 0.25F;
			this.txtCaption2.Width = 4.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.MultiLine = false;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.Label1.Text = "Account";
			this.Label1.Top = 0.5F;
			this.Label1.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.75F;
			this.Line1.Width = 7F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.0625F;
			this.Line1.Y1 = 0.75F;
			this.Line1.Y2 = 0.75F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.125F;
			this.Label2.MultiLine = false;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.Label2.Text = "Name / Address / Location / Map-Lot";
			this.Label2.Top = 0.5F;
			this.Label2.Width = 2.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.25F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 0";
			this.Label3.Text = "Total";
			this.Label3.Top = 0.5F;
			this.Label3.Width = 0.875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.5F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 0";
			this.Label4.Text = "Land";
			this.Label4.Top = 0.5F;
			this.Label4.Width = 0.6875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.625F;
			this.Label5.MultiLine = false;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.Label5.Text = "Card #";
			this.Label5.Top = 0.5F;
			this.Label5.Width = 0.4375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.4375F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 0";
			this.Label6.Text = "Building";
			this.Label6.Top = 0.5F;
			this.Label6.Width = 0.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.25F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 0";
			this.Label7.Text = "Exemption";
			this.Label7.Top = 0.5F;
			this.Label7.Width = 0.8125F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.19F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 9pt";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 6.125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.0625F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.0625F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.txtAccount.Text = "Account";
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.5625F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 1F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.txtName.Text = "Name";
			this.txtName.Top = 0F;
			this.txtName.Width = 2.1875F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 1F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.txtLocation.Text = "Location";
			this.txtLocation.Top = 0.9375F;
			this.txtLocation.Width = 2.1875F;
			// 
			// txtMap
			// 
			this.txtMap.Height = 0.1875F;
			this.txtMap.Left = 1F;
			this.txtMap.MultiLine = false;
			this.txtMap.Name = "txtMap";
			this.txtMap.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.txtMap.Text = "Map/Lot";
			this.txtMap.Top = 1.125F;
			this.txtMap.Width = 2.1875F;
			// 
			// txtCard
			// 
			this.txtCard.Height = 0.1875F;
			this.txtCard.Left = 0.625F;
			this.txtCard.Name = "txtCard";
			this.txtCard.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.txtCard.Text = "Card";
			this.txtCard.Top = 0F;
			this.txtCard.Width = 0.3125F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.1875F;
			this.txtLand.Left = 3.25F;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtLand.Text = "Land";
			this.txtLand.Top = 0F;
			this.txtLand.Width = 0.875F;
			// 
			// txtBuilding
			// 
			this.txtBuilding.Height = 0.1875F;
			this.txtBuilding.Left = 4.1875F;
			this.txtBuilding.Name = "txtBuilding";
			this.txtBuilding.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtBuilding.Text = "Building";
			this.txtBuilding.Top = 0F;
			this.txtBuilding.Width = 0.9375F;
			// 
			// txtExemption
			// 
			this.txtExemption.Height = 0.1875F;
			this.txtExemption.Left = 5.1875F;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtExemption.Text = "Exemption";
			this.txtExemption.Top = 0F;
			this.txtExemption.Width = 0.875F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.1875F;
			this.txtTotal.Left = 6.125F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtTotal.Text = "Total";
			this.txtTotal.Top = 0F;
			this.txtTotal.Width = 1.0625F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1875F;
			this.txtAddress.Left = 1F;
			this.txtAddress.MultiLine = false;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.txtAddress.Text = "Address";
			this.txtAddress.Top = 0.375F;
			this.txtAddress.Width = 2.1875F;
			// 
			// txtaddress2
			// 
			this.txtaddress2.Height = 0.1875F;
			this.txtaddress2.Left = 1F;
			this.txtaddress2.Name = "txtaddress2";
			this.txtaddress2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.txtaddress2.Text = "Field1";
			this.txtaddress2.Top = 0.5625F;
			this.txtaddress2.Width = 2.1875F;
			// 
			// txtaddress3
			// 
			this.txtaddress3.Height = 0.1875F;
			this.txtaddress3.Left = 1F;
			this.txtaddress3.Name = "txtaddress3";
			this.txtaddress3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.txtaddress3.Text = "Field2";
			this.txtaddress3.Top = 0.75F;
			this.txtaddress3.Width = 2.1875F;
			// 
			// txtSecondOwner
			// 
			this.txtSecondOwner.CanShrink = true;
			this.txtSecondOwner.Height = 0.1875F;
			this.txtSecondOwner.Left = 1F;
			this.txtSecondOwner.MultiLine = false;
			this.txtSecondOwner.Name = "txtSecondOwner";
			this.txtSecondOwner.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.txtSecondOwner.Text = "Address";
			this.txtSecondOwner.Top = 0.1875F;
			this.txtSecondOwner.Width = 2.1875F;
			// 
			// txtTotalLand
			// 
			this.txtTotalLand.Height = 0.19F;
			this.txtTotalLand.Left = 3.25F;
			this.txtTotalLand.Name = "txtTotalLand";
			this.txtTotalLand.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtTotalLand.Text = null;
			this.txtTotalLand.Top = 0.1666667F;
			this.txtTotalLand.Width = 0.9166667F;
			// 
			// txtTotalBuilding
			// 
			this.txtTotalBuilding.Height = 0.19F;
			this.txtTotalBuilding.Left = 4.166667F;
			this.txtTotalBuilding.Name = "txtTotalBuilding";
			this.txtTotalBuilding.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtTotalBuilding.Text = null;
			this.txtTotalBuilding.Top = 0.1666667F;
			this.txtTotalBuilding.Width = 1F;
			// 
			// txtTotalExemption
			// 
			this.txtTotalExemption.Height = 0.19F;
			this.txtTotalExemption.Left = 5.166667F;
			this.txtTotalExemption.Name = "txtTotalExemption";
			this.txtTotalExemption.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtTotalExemption.Text = null;
			this.txtTotalExemption.Top = 0.1666667F;
			this.txtTotalExemption.Width = 0.9166667F;
			// 
			// txtTotalTotal
			// 
			this.txtTotalTotal.Height = 0.19F;
			this.txtTotalTotal.Left = 6.166667F;
			this.txtTotalTotal.Name = "txtTotalTotal";
			this.txtTotalTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtTotalTotal.Text = null;
			this.txtTotalTotal.Top = 0.1666667F;
			this.txtTotalTotal.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.333333F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.08333334F;
			this.Line2.Width = 3.833333F;
			this.Line2.X1 = 3.333333F;
			this.Line2.X2 = 7.166667F;
			this.Line2.Y1 = 0.08333334F;
			this.Line2.Y2 = 0.08333334F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.5F;
			this.Label8.MultiLine = false;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 0";
			this.Label8.Text = "Total";
			this.Label8.Top = 0.1666667F;
			this.Label8.Width = 0.6666667F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.MultiLine = false;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.Label9.Text = "Count";
			this.Label9.Top = 0.1666667F;
			this.Label9.Width = 0.5F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 0.5F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.txtCount.Text = null;
			this.txtCount.Top = 0.1666667F;
			this.txtCount.Width = 0.5833333F;
			// 
			// lblCards
			// 
			this.lblCards.Height = 0.19F;
			this.lblCards.HyperLink = null;
			this.lblCards.Left = 1.166667F;
			this.lblCards.MultiLine = false;
			this.lblCards.Name = "lblCards";
			this.lblCards.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.lblCards.Text = "Cards";
			this.lblCards.Top = 0.1666667F;
			this.lblCards.Visible = false;
			this.lblCards.Width = 0.5F;
			// 
			// rptListingNameAddrLocMapAsses
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.25F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMap)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCards)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMap;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondOwner;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalExemption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCards;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
