﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	public class clsCustomize
	{
		//=========================================================
		//FC:FINAL:RPU - Use custom constructor to initialize fields
		private modGlobalVariables.CustomizedStuff CustomData = new modGlobalVariables.CustomizedStuff(0);
		private modGlobalVariables.TownSpecificCustomize[] TownSpecifics = null;
		private int lngCurrentTownNumber;
		private int lngMaxCardsAllowed;
		private int lngCardsUsed;
		private string strTaxMapLabel = string.Empty;
		private cSettingsController settingCont = new cSettingsController();

		public bool SaveCustomInfo()
		{
			bool SaveCustomInfo = false;
			string strSQL = "";
			int x;
			clsDRWrapper clsSave = new clsDRWrapper();
			if (CustomData.boolRegionalTown)
			{
				for (x = 1; x <= Information.UBound(TownSpecifics, 1); x++)
				{
					// strSQL = "update townspecific set certifiedratio = " & CustomData.CertifiedRatio
					// strSQL = strSQL & ",billingrate = " & CustomData.BillRate
					// strSQL = strSQL & ",billingyear = " & CustomData.BillYear
					strSQL = "update townspecific set certifiedratio = " + FCConvert.ToString(TownSpecifics[x].CertifiedRatio);
					strSQL += ",billingrate = " + FCConvert.ToString(TownSpecifics[x].BillRate);
					strSQL += ",billingyear = " + FCConvert.ToString(TownSpecifics[x].BillYear);
					strSQL += ",defaultschedule = " + FCConvert.ToString(TownSpecifics[x].DefaultSchedule);
					strSQL += " where townnumber = " + FCConvert.ToString(x);
					clsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
				}
				// x
			}
			if (CustomData.boolShowGridLines)
			{
				settingCont.SaveSetting("true", "ShowSketchGridLines", "RealEstate", "", "", "");
			}
			else
			{
				settingCont.SaveSetting("false", "ShowSketchGridLines", "RealEstate", "", "", "");
			}
			clsSave.OpenRecordset("select * from customize", modGlobalVariables.strREDatabase);
			clsSave.Edit();
			if (CustomData.boolDisableToolTips)
			{
				modRegistry.SaveRegistryKey("DisableToolTips", FCConvert.ToString(true), "RE");
			}
			else
			{
				modRegistry.SaveRegistryKey("DisableToolTips", FCConvert.ToString(false), "RE");
			}
			clsSave.Set_Fields("RXAutoUpdateDay", CustomData.RXAutoUpdateDay);
			clsSave.Set_Fields("RXAutoUpdateMonth", CustomData.RXAutoUpdateMonth);
			// clssave.Fields("RXAutoLastUpdateYear") = customdata.RXAutoLastUpdateYear
			clsSave.Set_Fields("billrate", CustomData.BillRate);
			clsSave.Set_Fields("defaultschedule", CustomData.DefaultSchedule);
			clsSave.Set_Fields("billyear", CustomData.BillYear);
			clsSave.Set_Fields("PrintFirstPicOnValReport", CustomData.boolShowFirstPiconVal);
			clsSave.Set_Fields("certifiedratio", CustomData.CertifiedRatio);            
			clsSave.Set_Fields("AlwaysShowBilling", CustomData.boolAlwaysShowCurrentOnPropertyCard);
			clsSave.Set_Fields("AlwaysShowCurrent", CustomData.boolAlwaysShowCurrentCurrentOnPropertyCard);
			clsSave.Set_Fields("NeverShowPreviousBilling", CustomData.boolNeverShowPreviousBillingOnPropertyCard);
			clsSave.Set_Fields("DontApplyLandFactorToTreeGrowth", CustomData.boolDontApplyLandFactorToTreeGrowth);
			clsSave.Set_Fields("ShowPicOnVal", CustomData.boolShowPicOnVal);
			clsSave.Set_Fields("Showsketchonval", CustomData.boolShowSketchOnVal);
			clsSave.Set_Fields("roundcalculatedhomesteads", CustomData.boolRoundCalculatedHomesteads);
			clsSave.Set_Fields("DontShowCommentsOnPropertyCard", CustomData.boolDontShowCommentsOnPropertyCard);
			clsSave.Set_Fields("MostRecentBP", CustomData.boolMostRecentBP);
			clsSave.Set_Fields("HidePicsPropertyCardRanges", CustomData.boolHidePicsForPropertyCardRanges);
			if (CustomData.boolPocketPCExternal)
			{
				modRegistry.SaveRegistryKey("REPocketPCExternal", FCConvert.ToString(true));
			}
			else
			{
				modRegistry.SaveRegistryKey("REPocketPCExternal", FCConvert.ToString(false));
			}
			if (CustomData.boolShowCodesOnAccountScreen)
			{
				modRegistry.SaveRegistryKey("ShowCodesOnAccountScreen", FCConvert.ToString(true), "RE");
			}
			else
			{
				modRegistry.SaveRegistryKey("ShowCodesOnAccountScreen", FCConvert.ToString(false), "RE");
			}
			if (CustomData.boolShowShortDesc)
			{
				modRegistry.SaveRegistryKey("ShowShortDesc", FCConvert.ToString(true), "RE");
			}
			else
			{
				modRegistry.SaveRegistryKey("ShowShortDesc", FCConvert.ToString(false), "RE");
			}
			if (CustomData.boolHideValuationRatio)
			{
				modRegistry.SaveRegistryKey("HideValuationRatio", FCConvert.ToString(true), "RE");
			}
			else
			{
				modRegistry.SaveRegistryKey("HideValuationRatio", FCConvert.ToString(false), "RE");
			}
			clsSave.Set_Fields("AccountOnBottom", CustomData.boolAccountOnBottom);
			clsSave.Set_Fields("ApplyHomesteadToCardOneOnly", CustomData.boolApplyHomesteadToCardOneOnly);
			clsSave.Set_Fields("ApplyCertifiedRatioToTreeGrowth", CustomData.boolApplyCertifiedRatioToTreeGrowth);
			clsSave.Set_Fields("PrintSecondaryPicOnPropertyCard", CustomData.boolPrintSecondaryPicOnPropertyCard);
			clsSave.Set_Fields("PrintMapLotOnSide", CustomData.boolPrintMapLotOnSide);
			clsSave.Set_Fields("round", CustomData.Round);
			clsSave.Set_Fields("roundcontingency", CustomData.RoundContingency);
			clsSave.Set_Fields("ValReportOption", CustomData.ValReportOption);
			clsSave.Set_Fields("squarefootlivingarea", CustomData.SquareFootLivingArea);
			clsSave.Set_Fields("acreoption", CustomData.AcreOption);
			clsSave.Set_Fields("ref1bookpage", CustomData.Ref1BookPage);
			clsSave.Set_Fields("opencode", CustomData.OpenCode);
			clsSave.Set_Fields("landunittype", CustomData.LandUnitType);
			clsSave.Set_Fields("depreciationyear", CustomData.DepreciationYear);
			clsSave.Set_Fields("mhdepreciationyear", CustomData.MHDepreciationYear);
			clsSave.Set_Fields("commdepreciationyear", CustomData.CommDepreciationYear);
			clsSave.Set_Fields("ppindpropcat", CustomData.PPIndPropCat);
			clsSave.Set_Fields("reindpropbldgcodes", CustomData.REIndPropBldgCodes);
			// clsSave.Fields("reindpropbldgcode") = CustomData.REIndPropBldgCode
			clsSave.Set_Fields("ppcat1taxvalcat", CustomData.PPCat1TaxValCat);
			clsSave.Set_Fields("ppcat2taxvalcat", CustomData.PPCat2TaxValCat);
			clsSave.Set_Fields("ppcat3taxvalcat", CustomData.PPCat3TaxValCat);
			clsSave.Set_Fields("ppcat4taxvalcat", CustomData.PPCat4TaxValCat);
			clsSave.Set_Fields("ppcat5taxvalcat", CustomData.PPCat5TaxValCat);
			clsSave.Set_Fields("ppcat6taxvalcat", CustomData.PPCat6TaxValCat);
			clsSave.Set_Fields("ppcat7taxvalcat", CustomData.PPCat7TaxValCat);
			clsSave.Set_Fields("ppcat8taxvalcat", CustomData.PPCat8TaxValCat);
			clsSave.Set_Fields("ppcat9taxvalcat", CustomData.PPCat9TaxValCat);
			//clsSave.Set_Fields("dotmatrixprinter", CustomData.DotMatrixPrinter);
			clsSave.Set_Fields("HideLabelAccount", CustomData.boolHideLabelAccount);
			clsSave.Set_Fields("ExportRef1ToWeb", CustomData.boolREWebIncludeRef1);
			clsSave.Set_Fields("ExportRef2ToWeb", CustomData.boolREWebIncludeRef2);
			clsSave.Set_Fields("Ref1Description", CustomData.strRef1Description);
			clsSave.Set_Fields("Ref2Description", CustomData.strRef2Description);
			clsSave.Update();
			SaveCustomInfo = true;
            LoadGlobalRESettingsFromCustomized(CustomData);
            return SaveCustomInfo;
		}

		public void LoadCustomInfo()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			int intTemp;
			string strTemp;

            try
            {
                // On Error GoTo ErrorHandler
                strTaxMapLabel = "Map/Lot";

                if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "ORONO")
                {
                    strTaxMapLabel = "Map-Sub-Lot-Sub";
                }

                if (modRegionalTown.IsRegionalTown())
                {
                    CustomData.boolRegionalTown = true;
                }
                else
                {
                    CustomData.boolRegionalTown = false;
                }

                // Call clsTemp.OpenRecordset("select * from modules", "SystemSettings")
                lngMaxCardsAllowed = modGlobalConstants.EleventyBillion;

                // If Not clsTemp.EndOfFile Then
                // lngMaxCardsAllowed = Val(clsTemp.Fields("RE_MaxAccounts"))
                // If lngMaxCardsAllowed = 0 Then
                // lngMaxCardsAllowed = EleventyBillion
                // End If
                // End If
                strTemp = settingCont.GetSettingValue("ShowSketchGridLines", "RealEstate", "", "", "");

                if (strTemp == "")
                {
                    CustomData.boolShowGridLines = false;
                    settingCont.SaveSetting(FCConvert.ToString(CustomData.boolShowGridLines), "ShowSketchGridLines", "RealEstate", "", "", "");
                }
                else
                {
                    CustomData.boolShowGridLines = FCConvert.CBool(strTemp);
                }

                if (FCConvert.CBool(modRegistry.GetRegistryKey("DisableToolTips", "RE", FCConvert.ToString(false))))
                {
                    CustomData.boolDisableToolTips = true;
                }
                else
                {
                    CustomData.boolDisableToolTips = false;
                }

                clsTemp.OpenRecordset("select * from customize", modGlobalVariables.strREDatabase);

                if (clsTemp.EndOfFile())
                {
                    MessageBox.Show("Your customized settings could not be found.  Default ones will be created." + "\r\n" + "Please go to M. File Maintenance / Customize  to check these settings.", "Missing Settings", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    clsTemp.AddNew();

                    //clsTemp.Set_Fields("dotmatrixprinter", 1);
                    clsTemp.Set_Fields("billrate", 1);
                    clsTemp.Set_Fields("billyear", DateTime.Now.Year);
                    clsTemp.Set_Fields("commdepreciationyear", 0);
                    clsTemp.Set_Fields("depreciationyear", 0);
                    clsTemp.Set_Fields("mhdepreciationyear", 0);
                    clsTemp.Set_Fields("squarefootlivingarea", 1);
                    clsTemp.Set_Fields("acreoption", 1);
                    clsTemp.Set_Fields("softwood", 37);
                    clsTemp.Set_Fields("mixedwood", 38);
                    clsTemp.Set_Fields("hardwood", 39);
                    clsTemp.Set_Fields("valreportoption", 1);
                    clsTemp.Set_Fields("landunittype", "F");
                    clsTemp.Set_Fields("round", 2);
                    clsTemp.Set_Fields("roundcontingency", 0);
                    clsTemp.Set_Fields("DontShowCommentsOnPropertyCard", false);
                    clsTemp.Set_Fields("RoundCalculatedHomesteads", false);
                    clsTemp.Set_Fields("ApplyHomesteadToCardOneOnly", false);
                    clsTemp.Set_Fields("DontApplyLandFactorToTreeGrowth", false);
                    clsTemp.Set_Fields("ApplyCertifiedRatioToTreeGrowth", false);
                    clsTemp.Set_Fields("PrintSecondaryPicOnPropertyCard", false);
                    clsTemp.Set_Fields("PrintMapLotOnSide", false);
                    clsTemp.Set_Fields("PPCat1TaxValCat", 0);
                    clsTemp.Set_Fields("ppcat2taxvalcat", 0);
                    clsTemp.Set_Fields("ppcat3taxvalcat", 0);
                    clsTemp.Set_Fields("ppcat4taxvalcat", 0);
                    clsTemp.Set_Fields("ppcat5taxvalcat", 0);
                    clsTemp.Set_Fields("ppcat6taxvalcat", 0);
                    clsTemp.Set_Fields("ppcat7taxvalcat", 0);
                    clsTemp.Set_Fields("ppcat8taxvalcat", 0);
                    clsTemp.Set_Fields("ppcat9taxvalcat", 0);
                    clsTemp.Set_Fields("PrintFirstPicOnValReport", false);

                    // clsTemp.Fields("REIndPropBldgCode") = -1
                    clsTemp.Set_Fields("PPIndPropCat", 0);
                    clsTemp.Set_Fields("HideLabelAccount", false);
                    clsTemp.Set_Fields("DefaultSchedule", 0);
                    clsTemp.Set_Fields("MostRecentBP", false);
                    clsTemp.Set_Fields("RXAutoUpdateDay", 0);
                    clsTemp.Set_Fields("RXAutoUpdateMonth", 0);
                    clsTemp.Set_Fields("RXAutoLastUpdateYear", 0);
                    clsTemp.Set_Fields("ExportRef1ToWeb", false);
                    clsTemp.Set_Fields("ExportRef2ToWeb", false);
                    clsTemp.Set_Fields("Ref1Description", "Reference 1");
                    clsTemp.Set_Fields("Ref2Description", "Reference 2");
                    clsTemp.Update();
                    clsTemp.MoveFirst();
                }

                CustomData.boolPocketPCExternal = FCConvert.CBool(modRegistry.GetRegistryKey_3("REPocketPCExternal", FCConvert.ToString(false)));
                CustomData.boolShowCodesOnAccountScreen = FCConvert.CBool(modRegistry.GetRegistryKey("ShowCodesOnAccountScreen", "RE", FCConvert.ToString(false)));
                CustomData.boolShowShortDesc = FCConvert.CBool(modRegistry.GetRegistryKey("ShowShortDesc", "RE", FCConvert.ToString(false)));
                CustomData.boolHideValuationRatio = FCConvert.CBool(modRegistry.GetRegistryKey("HideValuationRatio", "RE", FCConvert.ToString(false)));
                CustomData.boolAccountOnBottom = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("accountonbottom"));
                CustomData.PPCat1TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("ppcat1taxvalcat"))));
                CustomData.PPCat2TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("ppcat2taxvalcat"))));
                CustomData.PPCat3TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("ppcat3taxvalcat"))));
                CustomData.PPCat4TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("ppcat4taxvalcat"))));
                CustomData.PPCat5TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("ppcat5taxvalcat"))));
                CustomData.PPCat6TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("ppcat6taxvalcat"))));
                CustomData.PPCat7TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("ppcat7taxvalcat"))));
                CustomData.PPCat8TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("ppcat8taxvalcat"))));
                CustomData.PPCat9TaxValCat = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("ppcat9taxvalcat"))));
                CustomData.boolMostRecentBP = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("MostRecentBP"));
                CustomData.LandUnitType = FCConvert.ToString(clsTemp.Get_Fields_String("landunittype"));

                // CustomData.REIndPropBldgCode = Val(clsTemp.Fields("reindpropbldgcode"))
                CustomData.REIndPropBldgCodes = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("reindpropbldgcodes")));
                CustomData.PPIndPropCat = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("ppindpropcat"))));
                CustomData.boolPrintSecondaryPicOnPropertyCard = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("PrintSecondaryPicOnPropertyCard"));
                CustomData.boolApplyCertifiedRatioToTreeGrowth = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("ApplyCertifiedRatioToTreeGrowth"));
                CustomData.boolRoundCalculatedHomesteads = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("roundcalculatedhomesteads"));
                CustomData.boolShowPicOnVal = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("ShowPicOnVal"));
                CustomData.boolShowFirstPiconVal = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("PrintFirstPicOnValReport"));
                CustomData.boolShowSketchOnVal = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("ShowSketchOnVal"));
                CustomData.boolApplyHomesteadToCardOneOnly = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("ApplyHomesteadtocardoneonly"));
                CustomData.boolDontApplyLandFactorToTreeGrowth = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("DontApplyLandFactorToTreeGrowth"));
                CustomData.boolAlwaysShowCurrentOnPropertyCard = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("AlwaysShowBilling"));
                CustomData.boolAlwaysShowCurrentCurrentOnPropertyCard = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("AlwaysShowCurrent"));
                CustomData.boolNeverShowPreviousBillingOnPropertyCard = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("NeverShowPreviousBilling"));
                CustomData.boolDontShowCommentsOnPropertyCard = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("DontShowCommentsOnPropertyCard"));
                CustomData.Ref1BookPage = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("ref1bookpage"))));

                //CustomData.DotMatrixPrinter = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("dotmatrixprinter"))));
                CustomData.BillRate = Conversion.Val(clsTemp.Get_Fields_Double("billrate"));

                // TODO Get_Fields: Check the table for the column [billyear] and replace with corresponding Get_Field method
                CustomData.BillYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("billyear"))));
                CustomData.OpenCode = FCConvert.ToString(clsTemp.Get_Fields_String("opencode"));
                CustomData.FarmLandCodes = FCConvert.ToString(clsTemp.Get_Fields_String("farmcode"));
                CustomData.HomesteadCode1 = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("homesteadcode1"))));
                CustomData.HomesteadCode2 = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("homesteadcode2"))));
                CustomData.HomesteadCode3 = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("homesteadcode3"))));
                CustomData.HomesteadCode4 = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("homesteadcode4"))));
                CustomData.HomesteadCode5 = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("homesteadcode5"))));
                CustomData.CertifiedRatio = Conversion.Val(clsTemp.Get_Fields_Double("CertifiedRatio"));
                CustomData.CommDepreciationYear = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("commdepreciationyear"))));
                CustomData.boolPrintMapLotOnSide = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("printmaplotonside"));
                CustomData.boolHideLabelAccount = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("HideLabelAccount"));
                CustomData.RXAutoLastUpdateYear = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields("RXAutoLastUpdateYear"))));
                CustomData.RXAutoUpdateDay = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("RXAutoUpdateDay"))));
                CustomData.RXAutoUpdateMonth = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("RXAutoUpdateMonth"))));
                modGlobalVariables.Statics.DepYrCm = CustomData.CommDepreciationYear;
                CustomData.DepreciationYear = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("depreciationyear"))));
                DepreciationYear = CustomData.DepreciationYear;
                CustomData.MHDepreciationYear = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("mhdepreciationyear"))));
                modGlobalVariables.Statics.DepreciationYearMoHo = CustomData.MHDepreciationYear;
                CustomData.SquareFootLivingArea = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("squarefootlivingarea"))));
                modGlobalVariables.Statics.SFLAOption = CustomData.SquareFootLivingArea;
                CustomData.AcreOption = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("acreoption"))));
                CustomData.DefaultSchedule = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("DefaultSchedule"))));
                AcreOption = CustomData.AcreOption;
                modGlobalConstants.Statics.MuniName = Strings.Trim(modGlobalConstants.Statics.MuniName);

               
                CustomData.ValReportOption = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("valreportoption"))));
                modGlobalVariables.Statics.ValuationReportOption = CustomData.ValReportOption;

                CustomData.Round = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("round"))));
                CustomData.RoundContingency = FCConvert.ToInt16(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int16("roundcontingency"))));
                if (CustomData.Round == 0) CustomData.Round = 2;
                if (CustomData.Round == 1) modGlobalVariables.Statics.RoundingVar = 1000;
                if (CustomData.Round == 2) modGlobalVariables.Statics.RoundingVar = 100;
                if (CustomData.Round == 3) modGlobalVariables.Statics.RoundingVar = 10;
                if (CustomData.Round == 4) modGlobalVariables.Statics.RoundingVar = 1;
                modGlobalVariables.Statics.HoldRounding = modGlobalVariables.Statics.RoundingVar;
                modGlobalVariables.Statics.Rounding2 = modGlobalVariables.Statics.RoundingVar;

                CustomData.boolHidePicsForPropertyCardRanges = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("HidePicsPropertyCardRanges"));
                CustomData.boolREWebIncludeRef1 = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("ExportRef1ToWeb"));
                CustomData.boolREWebIncludeRef2 = FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("ExportRef2ToWeb"));
                this.Ref1Description = clsTemp.Get_Fields_String("Ref1Description");
                this.Ref2Description = clsTemp.Get_Fields_String("Ref2Description");

                if (CustomData.boolRegionalTown)
                {
                    int x;
                    clsTemp.OpenRecordset("select max (townnumber) as maxcode from tblRegions", modGlobalVariables.Statics.strGNDatabase);

                    // TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
                    intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("maxcode"))));
                    TownSpecifics = new modGlobalVariables.TownSpecificCustomize[intTemp + 1];
                    clsTemp.OpenRecordset("select * from townspecific", modGlobalVariables.strREDatabase);

                    while (!clsTemp.EndOfFile())
                    {
                        var townSpecificCustomize = TownSpecifics[FCConvert.ToInt32(clsTemp.Get_Fields("townnumber"))];
                        townSpecificCustomize.BillRate = Conversion.Val(clsTemp.Get_Fields_Double("billingrate"));
                        townSpecificCustomize.BillYear = Conversion.Val(clsTemp.Get_Fields_Int32("Billingyear"));
                        townSpecificCustomize.CertifiedRatio = Conversion.Val(clsTemp.Get_Fields_Double("certifiedratio"));
                        townSpecificCustomize.DefaultSchedule = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("defaultSchedule"))));
                        townSpecificCustomize.MuniName = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("townnumber")))));
                        clsTemp.MoveNext();
                    }
                }

                LoadGlobalRESettingsFromCustomized(CustomData);
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadCustomInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                clsTemp.DisposeOf();
            }
		}

		public bool HideValuationRatio
		{
			get
			{
				bool HideValuationRatio = false;
				HideValuationRatio = CustomData.boolHideValuationRatio;
				return HideValuationRatio;
			}
			set
			{
				CustomData.boolHideValuationRatio = value;
			}
		}

		public int MaxCardsAllowed
		{
			get
			{
				int MaxCardsAllowed = 0;
				MaxCardsAllowed = lngMaxCardsAllowed;
				return MaxCardsAllowed;
			}
		}

		public int CardsUsed
		{
			get
			{
				int CardsUsed = 0;
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsLoad.OpenRecordset("select count(rsaccount) as TheCount from master", modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					CardsUsed = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("thecount"))));
				}
				else
				{
					CardsUsed = 0;
				}
				return CardsUsed;
			}
		}

		public int CurrentTownNumber
		{
			set
			{
				lngCurrentTownNumber = value;
			}
			get
			{
				int CurrentTownNumber = 0;
				CurrentTownNumber = lngCurrentTownNumber;
				return CurrentTownNumber;
			}
		}

		public double CertifiedRatio
		{
			set
			{
				if (!CustomData.boolRegionalTown)
				{
					CustomData.CertifiedRatio = value;
				}
				else
				{
					TownSpecifics[lngCurrentTownNumber].CertifiedRatio = value;
				}
			}
			get
			{
				double CertifiedRatio = 0;
				if (!CustomData.boolRegionalTown)
				{
					CertifiedRatio = CustomData.CertifiedRatio;
				}
				else
				{
					CertifiedRatio = TownSpecifics[lngCurrentTownNumber].CertifiedRatio;
				}
				return CertifiedRatio;
			}
		}

		public double BillRate
		{
			set
			{
				if (!CustomData.boolRegionalTown)
				{
					CustomData.BillRate = value;
				}
				else
				{
					TownSpecifics[lngCurrentTownNumber].BillRate = value;
				}
			}
			get
			{
				double BillRate = 0;
				if (!CustomData.boolRegionalTown)
				{
					BillRate = CustomData.BillRate;
				}
				else
				{
					BillRate = TownSpecifics[lngCurrentTownNumber].BillRate;
				}
				return BillRate;
			}
		}

		public int BillYear
		{
			set
			{
				if (!CustomData.boolRegionalTown)
				{
					CustomData.BillYear = value;
				}
				else
				{
					TownSpecifics[lngCurrentTownNumber].BillYear = value;
				}
			}
			// vbPorter upgrade warning: 'Return' As int	OnWrite(int, double)
			get
			{
				int BillYear = 0;
				if (!CustomData.boolRegionalTown)
				{
					BillYear = CustomData.BillYear;
				}
				else
				{
					BillYear = FCConvert.ToInt32(TownSpecifics[lngCurrentTownNumber].BillYear);
				}
				return BillYear;
			}
		}

		public short Round
		{
			set
			{
				CustomData.Round = value;
			}
			get
			{
				short Round = 0;
				Round = CustomData.Round;
				return Round;
			}
		}

		public short MHDepreciationYear
		{
			set
			{
				CustomData.MHDepreciationYear = value;
			}
			get
			{
				short MHDepreciationYear = 0;
				MHDepreciationYear = CustomData.MHDepreciationYear;
				return MHDepreciationYear;
			}
		}

		public short RoundContingency
		{
			set
			{
				CustomData.RoundContingency = value;
			}
			get
			{
				short RoundContingency = 0;
				RoundContingency = CustomData.RoundContingency;
				return RoundContingency;
			}
		}

		public short ValReportOption
		{
			set
			{
				CustomData.ValReportOption = value;
			}
			get
			{
				short ValReportOption = 0;
				ValReportOption = CustomData.ValReportOption;
				return ValReportOption;
			}
		}

		public short AcreOption
		{
			set
			{
				CustomData.AcreOption = value;
			}
			get
			{
				short AcreOption = 0;
				AcreOption = CustomData.AcreOption;
				return AcreOption;
			}
		}

		public short DepreciationYear
		{
			set
			{
				CustomData.DepreciationYear = value;
			}
			get
			{
				short DepreciationYear = 0;
				DepreciationYear = CustomData.DepreciationYear;
				return DepreciationYear;
			}
		}

		public string LandUnitType
		{
			set
			{
				CustomData.LandUnitType = value;
			}
			get
			{
				string LandUnitType = "";
				LandUnitType = CustomData.LandUnitType;
				return LandUnitType;
			}
		}

		public short SquareFootLivingArea
		{
			set
			{
				CustomData.SquareFootLivingArea = value;
			}
			get
			{
				short SquareFootLivingArea = 0;
				SquareFootLivingArea = CustomData.SquareFootLivingArea;
				return SquareFootLivingArea;
			}
		}

		public short CommDepreciationYear
		{
			set
			{
				CustomData.CommDepreciationYear = value;
			}
			get
			{
				short CommDepreciationYear = 0;
				CommDepreciationYear = CustomData.CommDepreciationYear;
				return CommDepreciationYear;
			}
		}

		public short Ref1BookPage
		{
			set
			{
				CustomData.Ref1BookPage = value;
			}
			get
			{
				short Ref1BookPage = 0;
				Ref1BookPage = CustomData.Ref1BookPage;
				return Ref1BookPage;
			}
		}

		public bool boolShowPicOnVal
		{
			set
			{
				CustomData.boolShowPicOnVal = value;
			}
			get
			{
				bool boolShowPicOnVal = false;
				boolShowPicOnVal = CustomData.boolShowPicOnVal;
				return boolShowPicOnVal;
			}
		}

		public bool boolShowSketchOnVal
		{
			set
			{
				CustomData.boolShowSketchOnVal = value;
			}
			get
			{
				bool boolShowSketchOnVal = false;
				boolShowSketchOnVal = CustomData.boolShowSketchOnVal;
				return boolShowSketchOnVal;
			}
		}

		public bool boolShowFirstPiconVal
		{
			set
			{
				CustomData.boolShowFirstPiconVal = value;
			}
			get
			{
				bool boolShowFirstPiconVal = false;
				boolShowFirstPiconVal = CustomData.boolShowFirstPiconVal;
				return boolShowFirstPiconVal;
			}
		}

		public bool HidePicsForPropertyCardRanges
		{
			get
			{
				bool HidePicsForPropertyCardRanges = false;
				HidePicsForPropertyCardRanges = CustomData.boolHidePicsForPropertyCardRanges;
				return HidePicsForPropertyCardRanges;
			}
			set
			{
				CustomData.boolHidePicsForPropertyCardRanges = value;
			}
		}

		public bool boolAlwaysShowCurrentOnPropertyCard
		{
			set
			{
				CustomData.boolAlwaysShowCurrentOnPropertyCard = value;
			}
			get
			{
				bool boolAlwaysShowCurrentOnPropertyCard = false;
				boolAlwaysShowCurrentOnPropertyCard = CustomData.boolAlwaysShowCurrentOnPropertyCard;
				return boolAlwaysShowCurrentOnPropertyCard;
			}
		}

		public bool boolAlwaysShowCurrentCurrentOnPropertyCard
		{
			set
			{
				CustomData.boolAlwaysShowCurrentCurrentOnPropertyCard = value;
			}
			get
			{
				bool boolAlwaysShowCurrentCurrentOnPropertyCard = false;
				boolAlwaysShowCurrentCurrentOnPropertyCard = CustomData.boolAlwaysShowCurrentCurrentOnPropertyCard;
				return boolAlwaysShowCurrentCurrentOnPropertyCard;
			}
		}

		public bool boolNeverShowPreviousBillingOnPropertyCard
		{
			set
			{
				CustomData.boolNeverShowPreviousBillingOnPropertyCard = value;
			}
			get
			{
				bool boolNeverShowPreviousBillingOnPropertyCard = false;
				boolNeverShowPreviousBillingOnPropertyCard = CustomData.boolNeverShowPreviousBillingOnPropertyCard;
				return boolNeverShowPreviousBillingOnPropertyCard;
			}
		}

		public bool boolDontShowCommentsOnPropertyCard
		{
			set
			{
				CustomData.boolDontShowCommentsOnPropertyCard = value;
			}
			get
			{
				bool boolDontShowCommentsOnPropertyCard = false;
				boolDontShowCommentsOnPropertyCard = CustomData.boolDontShowCommentsOnPropertyCard;
				return boolDontShowCommentsOnPropertyCard;
			}
		}

		public bool boolPocketPCExternal
		{
			set
			{
				CustomData.boolPocketPCExternal = value;
			}
			get
			{
				bool boolPocketPCExternal = false;
				boolPocketPCExternal = CustomData.boolPocketPCExternal;
				return boolPocketPCExternal;
			}
		}

		public bool boolRoundCalculatedHomesteads
		{
			set
			{
				CustomData.boolRoundCalculatedHomesteads = value;
			}
			get
			{
				bool boolRoundCalculatedHomesteads = false;
				boolRoundCalculatedHomesteads = CustomData.boolRoundCalculatedHomesteads;
				return boolRoundCalculatedHomesteads;
			}
		}

		public bool MostRecentBP
		{
			set
			{
				CustomData.boolMostRecentBP = value;
			}
			get
			{
				bool MostRecentBP = false;
				MostRecentBP = CustomData.boolMostRecentBP;
				return MostRecentBP;
			}
		}
		// vbPorter upgrade warning: lngCode As int	OnRead(string)
		public int OpenCode
		{
			set
			{
				CustomData.OpenCode = FCConvert.ToString(value);
			}
			get
			{
				int OpenCode = 0;
				OpenCode = FCConvert.ToInt32(Math.Round(Conversion.Val(CustomData.OpenCode)));
				return OpenCode;
			}
		}

		public bool boolApplyHomesteadToCardOneOnly
		{
			set
			{
				CustomData.boolApplyHomesteadToCardOneOnly = value;
			}
			get
			{
				bool boolApplyHomesteadToCardOneOnly = false;
				boolApplyHomesteadToCardOneOnly = CustomData.boolApplyHomesteadToCardOneOnly;
				return boolApplyHomesteadToCardOneOnly;
			}
		}

		public bool boolDontApplyLandFactorToTreeGrowth
		{
			set
			{
				CustomData.boolDontApplyLandFactorToTreeGrowth = value;
			}
			get
			{
				bool boolDontApplyLandFactorToTreeGrowth = false;
				boolDontApplyLandFactorToTreeGrowth = CustomData.boolDontApplyLandFactorToTreeGrowth;
				return boolDontApplyLandFactorToTreeGrowth;
			}
		}

		public bool boolApplyCertifiedRatioToTreeGrowth
		{
			set
			{
				CustomData.boolApplyCertifiedRatioToTreeGrowth = value;
			}
			get
			{
				bool boolApplyCertifiedRatioToTreeGrowth = false;
				boolApplyCertifiedRatioToTreeGrowth = CustomData.boolApplyCertifiedRatioToTreeGrowth;
				return boolApplyCertifiedRatioToTreeGrowth;
			}
		}

		public bool boolPrintSecondaryPicOnPropertyCard
		{
			set
			{
				CustomData.boolPrintSecondaryPicOnPropertyCard = value;
			}
			get
			{
				bool boolPrintSecondaryPicOnPropertyCard = false;
				boolPrintSecondaryPicOnPropertyCard = CustomData.boolPrintSecondaryPicOnPropertyCard;
				return boolPrintSecondaryPicOnPropertyCard;
			}
		}

		public bool boolPrintMapLotOnSide
		{
			set
			{
				CustomData.boolPrintMapLotOnSide = value;
			}
			get
			{
				bool boolPrintMapLotOnSide = false;
				boolPrintMapLotOnSide = CustomData.boolPrintMapLotOnSide;
				return boolPrintMapLotOnSide;
			}
		}

		public short RXAutoUpdateDay
		{
			set
			{
				CustomData.RXAutoUpdateDay = value;
			}
			get
			{
				short RXAutoUpdateDay = 0;
				RXAutoUpdateDay = CustomData.RXAutoUpdateDay;
				return RXAutoUpdateDay;
			}
		}

		public short RXAutoUpdateMonth
		{
			set
			{
				CustomData.RXAutoUpdateMonth = value;
			}
			get
			{
				short RXAutoUpdateMonth = 0;
				RXAutoUpdateMonth = CustomData.RXAutoUpdateMonth;
				return RXAutoUpdateMonth;
			}
		}

		public short RXAutoLastUpdateYear
		{
			set
			{
				CustomData.RXAutoLastUpdateYear = value;
			}
			get
			{
				short RXAutoLastUpdateYear = 0;
				RXAutoLastUpdateYear = CustomData.RXAutoLastUpdateYear;
				return RXAutoLastUpdateYear;
			}
		}

		public bool AccountOnBottom
		{
			set
			{
				CustomData.boolAccountOnBottom = value;
			}
			get
			{
				bool AccountOnBottom = false;
				AccountOnBottom = CustomData.boolAccountOnBottom;
				return AccountOnBottom;
			}
		}

		public bool HideLabelAccount
		{
			set
			{
				CustomData.boolHideLabelAccount = value;
			}
			get
			{
				bool HideLabelAccount = false;
				HideLabelAccount = CustomData.boolHideLabelAccount;
				return HideLabelAccount;
			}
		}

		public short PPCat1TaxValCat
		{
			set
			{
				CustomData.PPCat1TaxValCat = value;
			}
			get
			{
				short PPCat1TaxValCat = 0;
				PPCat1TaxValCat = CustomData.PPCat1TaxValCat;
				return PPCat1TaxValCat;
			}
		}

		public short PPCat2TaxValCat
		{
			set
			{
				CustomData.PPCat2TaxValCat = value;
			}
			get
			{
				short PPCat2TaxValCat = 0;
				PPCat2TaxValCat = CustomData.PPCat2TaxValCat;
				return PPCat2TaxValCat;
			}
		}

		public short PPCat3TaxValCat
		{
			set
			{
				CustomData.PPCat3TaxValCat = value;
			}
			get
			{
				short PPCat3TaxValCat = 0;
				PPCat3TaxValCat = CustomData.PPCat3TaxValCat;
				return PPCat3TaxValCat;
			}
		}

		public short PPCat4TaxValCat
		{
			set
			{
				CustomData.PPCat4TaxValCat = value;
			}
			get
			{
				short PPCat4TaxValCat = 0;
				PPCat4TaxValCat = CustomData.PPCat4TaxValCat;
				return PPCat4TaxValCat;
			}
		}

		public short PPCat5TaxValCat
		{
			set
			{
				CustomData.PPCat5TaxValCat = value;
			}
			get
			{
				short PPCat5TaxValCat = 0;
				PPCat5TaxValCat = CustomData.PPCat5TaxValCat;
				return PPCat5TaxValCat;
			}
		}

		public short PPCat6TaxValCat
		{
			set
			{
				CustomData.PPCat6TaxValCat = value;
			}
			get
			{
				short PPCat6TaxValCat = 0;
				PPCat6TaxValCat = CustomData.PPCat6TaxValCat;
				return PPCat6TaxValCat;
			}
		}

		public short PPCat7TaxValCat
		{
			set
			{
				CustomData.PPCat7TaxValCat = value;
			}
			get
			{
				short PPCat7TaxValCat = 0;
				PPCat7TaxValCat = CustomData.PPCat7TaxValCat;
				return PPCat7TaxValCat;
			}
		}

		public short PPCat8TaxValCat
		{
			set
			{
				CustomData.PPCat8TaxValCat = value;
			}
			get
			{
				short PPCat8TaxValCat = 0;
				PPCat8TaxValCat = CustomData.PPCat8TaxValCat;
				return PPCat8TaxValCat;
			}
		}

		public short PPCat9TaxValCat
		{
			set
			{
				CustomData.PPCat9TaxValCat = value;
			}
			get
			{
				short PPCat9TaxValCat = 0;
				PPCat9TaxValCat = CustomData.PPCat9TaxValCat;
				return PPCat9TaxValCat;
			}
		}
		// Public Property Let REIndPropBldgCode(lngCode As Long)
		// CustomData.REIndPropBldgCode = lngCode
		// End Property
		//
		// Public Property Get REIndPropBldgCode() As Long
		// REIndPropBldgCode = CustomData.REIndPropBldgCode
		// End Property
		public string REIndPropBldgCodes
		{
			set
			{
				CustomData.REIndPropBldgCodes = value;
			}
			get
			{
				string REIndPropBldgCodes = "";
				REIndPropBldgCodes = CustomData.REIndPropBldgCodes;
				return REIndPropBldgCodes;
			}
		}

		public short PPIndPropCat
		{
			set
			{
				CustomData.PPIndPropCat = value;
			}
			get
			{
				short PPIndPropCat = 0;
				PPIndPropCat = CustomData.PPIndPropCat;
				return PPIndPropCat;
			}
		}

		public bool boolRegionalTown
		{
			set
			{
				CustomData.boolRegionalTown = value;
			}
			get
			{
				bool boolRegionalTown = false;
				boolRegionalTown = CustomData.boolRegionalTown;
				return boolRegionalTown;
			}
		}

		public bool boolShowCodeOnAccountScreen
		{
			set
			{
				CustomData.boolShowCodesOnAccountScreen = value;
			}
			get
			{
				bool boolShowCodeOnAccountScreen = false;
				boolShowCodeOnAccountScreen = CustomData.boolShowCodesOnAccountScreen;
				return boolShowCodeOnAccountScreen;
			}
		}

		public bool DisableToolTips
		{
			set
			{
				CustomData.boolDisableToolTips = value;
			}
			get
			{
				bool DisableToolTips = false;
				DisableToolTips = CustomData.boolDisableToolTips;
				return DisableToolTips;
			}
		}

		public bool ShowShortDesc
		{
			set
			{
				CustomData.boolShowShortDesc = value;
			}
			get
			{
				bool ShowShortDesc = false;
				ShowShortDesc = CustomData.boolShowShortDesc;
				return ShowShortDesc;
			}
		}

		public int DefaultSchedule
		{
			set
			{
				if (!CustomData.boolRegionalTown)
				{
					CustomData.DefaultSchedule = value;
				}
				else
				{
					TownSpecifics[lngCurrentTownNumber].DefaultSchedule = value;
				}
			}
			get
			{
				int DefaultSchedule = 0;
				if (!CustomData.boolRegionalTown)
				{
					DefaultSchedule = CustomData.DefaultSchedule;
				}
				else
				{
					DefaultSchedule = TownSpecifics[lngCurrentTownNumber].DefaultSchedule;
				}
				return DefaultSchedule;
			}
		}

		public bool ExportRef1ToWeb
		{
			set
			{
				CustomData.boolREWebIncludeRef1 = value;
			}
			get
			{
				bool ExportRef1ToWeb = false;
				ExportRef1ToWeb = CustomData.boolREWebIncludeRef1;
				return ExportRef1ToWeb;
			}
		}

		public bool ExportRef2ToWeb
		{
			set
			{
				CustomData.boolREWebIncludeRef2 = value;
			}
			get
			{
				bool ExportRef2ToWeb = false;
				ExportRef2ToWeb = CustomData.boolREWebIncludeRef2;
				return ExportRef2ToWeb;
			}
		}

		public string Ref1Description
		{
			set
			{
				if (Strings.Trim(value) != "")
				{
					CustomData.strRef1Description = Strings.Trim(value);
				}
				else
				{
					CustomData.strRef1Description = "Reference 1";
				}
			}
			get
			{
				string Ref1Description = "";
				Ref1Description = CustomData.strRef1Description;
				return Ref1Description;
			}
		}

		public string Ref2Description
		{
			set
			{
				if (Strings.Trim(value) != "")
				{
					CustomData.strRef2Description = Strings.Trim(value);
				}
				else
				{
					CustomData.strRef2Description = "Reference 2";
				}
			}
			get
			{
				string Ref2Description = "";
				Ref2Description = CustomData.strRef2Description;
				return Ref2Description;
			}
		}

		public string CurrentTownName
		{
			get
			{
				string CurrentTownName = "";
				if (boolRegionalTown)
				{
					if (CurrentTownNumber > 0 && CurrentTownNumber <= Information.UBound(TownSpecifics, 1))
					{
						CurrentTownName = TownSpecifics[CurrentTownNumber].MuniName;
					}
					else
					{
						CurrentTownName = "";
					}
				}
				else
				{
					CurrentTownName = modGlobalConstants.Statics.MuniName;
				}
				return CurrentTownName;
			}
		}

		public bool ShowSketchGridLines
		{
			set
			{
				CustomData.boolShowGridLines = value;
			}
			get
			{
				bool ShowSketchGridLines = false;
				ShowSketchGridLines = CustomData.boolShowGridLines;
				return ShowSketchGridLines;
			}
		}

        private static void LoadGlobalRESettingsFromCustomized(modGlobalVariables.CustomizedStuff customize)
        {
            StaticSettings.RealEstateSettings.CertifiedRatio = customize.CertifiedRatio.ToDecimal();
            StaticSettings.RealEstateSettings.PersonalPropertyCategories.SetCategoryType(1, (PersonalPropertyMVRCategory)customize.PPCat1TaxValCat);
            StaticSettings.RealEstateSettings.PersonalPropertyCategories.SetCategoryType(2, (PersonalPropertyMVRCategory)customize.PPCat2TaxValCat);
            StaticSettings.RealEstateSettings.PersonalPropertyCategories.SetCategoryType(3, (PersonalPropertyMVRCategory)customize.PPCat3TaxValCat);
            StaticSettings.RealEstateSettings.PersonalPropertyCategories.SetCategoryType(4, (PersonalPropertyMVRCategory)customize.PPCat4TaxValCat);
            StaticSettings.RealEstateSettings.PersonalPropertyCategories.SetCategoryType(5, (PersonalPropertyMVRCategory)customize.PPCat5TaxValCat);
            StaticSettings.RealEstateSettings.PersonalPropertyCategories.SetCategoryType(6, (PersonalPropertyMVRCategory)customize.PPCat6TaxValCat);
            StaticSettings.RealEstateSettings.PersonalPropertyCategories.SetCategoryType(7, (PersonalPropertyMVRCategory)customize.PPCat7TaxValCat);
            StaticSettings.RealEstateSettings.PersonalPropertyCategories.SetCategoryType(8, (PersonalPropertyMVRCategory)customize.PPCat8TaxValCat);
            StaticSettings.RealEstateSettings.PersonalPropertyCategories.SetCategoryType(9, (PersonalPropertyMVRCategory)customize.PPCat9TaxValCat);
            StaticSettings.RealEstateSettings.PersonalPropertyIndustrialCategory =
                customize.PPIndPropCat;
            StaticSettings.RealEstateSettings.ApplyCertifiedRatioToTreeGrowthRates =
                customize.boolApplyCertifiedRatioToTreeGrowth;
            StaticSettings.RealEstateSettings.IndustrialBuildingCodes.Clear();
            var strAry = Strings.Split(customize.REIndPropBldgCodes, ",", -1, CompareConstants.vbBinaryCompare);
            foreach (var code in strAry)
            {
                if (code.ToIntegerValue() > 0)
                {
                    StaticSettings.RealEstateSettings.IndustrialBuildingCodes.Add(code.ToIntegerValue());
                }
            }           
        }
    }
}
