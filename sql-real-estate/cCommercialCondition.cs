﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cCommercialCondition
	{
		//=========================================================
		private int lngRecordID;
		private string strFullDescription = string.Empty;
		private double dblHighValue;
		private double dblLowValue;
		private double dblExponent;
		private int lngConditionCode;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int CodeNumber
		{
			set
			{
				lngConditionCode = value;
				IsUpdated = true;
			}
			get
			{
				int CodeNumber = 0;
				CodeNumber = lngConditionCode;
				return CodeNumber;
			}
		}

		public string Description
		{
			set
			{
				strFullDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strFullDescription;
				return Description;
			}
		}

		public double High
		{
			set
			{
				dblHighValue = value;
				IsUpdated = true;
			}
			get
			{
				double High = 0;
				High = dblHighValue;
				return High;
			}
		}

		public double Low
		{
			set
			{
				dblLowValue = value;
				IsUpdated = true;
			}
			get
			{
				double Low = 0;
				Low = dblLowValue;
				return Low;
			}
		}

		public double Exponent
		{
			set
			{
				dblExponent = value;
				IsUpdated = true;
			}
			get
			{
				double Exponent = 0;
				Exponent = dblExponent;
				return Exponent;
			}
		}
	}
}
