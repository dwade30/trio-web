//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmTransferSQFT.
	/// </summary>
	partial class frmTransferSQFT : BaseForm
	{
		public FCGrid GridBuildings;
		public FCGrid GridInfo;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuTransfer;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTransferSQFT));
			this.GridBuildings = new fecherFoundation.FCGrid();
			this.GridInfo = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTransfer = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdTransfer = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridBuildings)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTransfer)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdTransfer);
			this.BottomPanel.Location = new System.Drawing.Point(0, 540);
			this.BottomPanel.Size = new System.Drawing.Size(855, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridBuildings);
			this.ClientArea.Controls.Add(this.GridInfo);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(855, 480);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(855, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(287, 30);
			this.HeaderText.Text = "Transfer Square Footage";
			// 
			// GridBuildings
			// 
			this.GridBuildings.AllowSelection = false;
			this.GridBuildings.AllowUserToResizeColumns = false;
			this.GridBuildings.AllowUserToResizeRows = false;
			this.GridBuildings.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridBuildings.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridBuildings.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridBuildings.BackColorBkg = System.Drawing.Color.Empty;
			this.GridBuildings.BackColorFixed = System.Drawing.Color.Empty;
			this.GridBuildings.BackColorSel = System.Drawing.Color.Empty;
			this.GridBuildings.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridBuildings.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridBuildings.ColumnHeadersHeight = 30;
			this.GridBuildings.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridBuildings.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridBuildings.DragIcon = null;
			this.GridBuildings.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridBuildings.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridBuildings.ExtendLastCol = true;
			this.GridBuildings.FixedCols = 2;
			this.GridBuildings.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridBuildings.FrozenCols = 0;
			this.GridBuildings.GridColor = System.Drawing.Color.Empty;
			this.GridBuildings.GridColorFixed = System.Drawing.Color.Empty;
			this.GridBuildings.Location = new System.Drawing.Point(30, 270);
			this.GridBuildings.Name = "GridBuildings";
			this.GridBuildings.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridBuildings.RowHeightMin = 0;
			this.GridBuildings.Rows = 50;
			this.GridBuildings.ScrollTipText = null;
			this.GridBuildings.ShowColumnVisibilityMenu = false;
			this.GridBuildings.Size = new System.Drawing.Size(796, 190);
			this.GridBuildings.StandardTab = true;
			this.GridBuildings.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridBuildings.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridBuildings.TabIndex = 2;
			// 
			// GridInfo
			// 
			this.GridInfo.AllowSelection = false;
			this.GridInfo.AllowUserToResizeColumns = false;
			this.GridInfo.AllowUserToResizeRows = false;
			this.GridInfo.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridInfo.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridInfo.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridInfo.BackColorBkg = System.Drawing.Color.Empty;
			this.GridInfo.BackColorFixed = System.Drawing.Color.Empty;
			this.GridInfo.BackColorSel = System.Drawing.Color.Empty;
			this.GridInfo.Cols = 3;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridInfo.ColumnHeadersHeight = 30;
			this.GridInfo.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridInfo.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridInfo.DragIcon = null;
			this.GridInfo.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridInfo.ExtendLastCol = true;
			this.GridInfo.FixedCols = 3;
			this.GridInfo.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridInfo.FrozenCols = 0;
			this.GridInfo.GridColor = System.Drawing.Color.Empty;
			this.GridInfo.GridColorFixed = System.Drawing.Color.Empty;
			this.GridInfo.Location = new System.Drawing.Point(30, 30);
			this.GridInfo.Name = "GridInfo";
			this.GridInfo.ReadOnly = true;
			this.GridInfo.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridInfo.RowHeightMin = 0;
			this.GridInfo.Rows = 50;
			this.GridInfo.ScrollTipText = null;
			this.GridInfo.ShowColumnVisibilityMenu = false;
			this.GridInfo.Size = new System.Drawing.Size(796, 180);
			this.GridInfo.StandardTab = true;
			this.GridInfo.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridInfo.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridInfo.TabIndex = 0;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 232);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(817, 18);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "IN THE AREA COLUMN BELOW, TYPE THE AREA NUMBER WHICH CONTAINS THE SQUARE FOOTAGE " + "TO BE TRANSFERRED TO THAT BUILDING";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuTransfer,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuTransfer
			// 
			this.mnuTransfer.Index = 0;
			this.mnuTransfer.Name = "mnuTransfer";
			this.mnuTransfer.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuTransfer.Text = "Transfer";
			this.mnuTransfer.Click += new System.EventHandler(this.mnuTransfer_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdTransfer
			// 
			this.cmdTransfer.AppearanceKey = "acceptButton";
			this.cmdTransfer.Location = new System.Drawing.Point(345, 30);
			this.cmdTransfer.Name = "cmdTransfer";
			this.cmdTransfer.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdTransfer.Size = new System.Drawing.Size(100, 48);
			this.cmdTransfer.TabIndex = 0;
			this.cmdTransfer.Text = "Transfer";
			this.cmdTransfer.Click += new System.EventHandler(this.mnuTransfer_Click);
			// 
			// frmTransferSQFT
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(855, 648);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmTransferSQFT";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Transfer Square Footage";
			this.Load += new System.EventHandler(this.frmTransferSQFT_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTransferSQFT_KeyDown);
			this.Resize += new System.EventHandler(this.frmTransferSQFT_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridBuildings)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdTransfer)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdTransfer;
	}
}