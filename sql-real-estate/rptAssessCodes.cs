﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptAssessCodes.
	/// </summary>
	public partial class rptAssessCodes : BaseSectionReport
	{
		public static rptAssessCodes InstancePtr
		{
			get
			{
				return (rptAssessCodes)Sys.GetInstance(typeof(rptAssessCodes));
			}
		}

		protected rptAssessCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsTemp?.Dispose();
                clsTemp = null;
            }
			base.Dispose(disposing);
		}

		public rptAssessCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Assessment Summary By Codes";
		}
		// nObj = 1
		//   0	rptAssessCodes	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		clsDRWrapper clsTemp = new clsDRWrapper();
		double lngTotal;
		double lngExempt;
		double lngbldg;
		double lngLand;

		public void Init()
		{
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                rsLoad.OpenRecordset("select * from status", modGlobalVariables.strREDatabase);
                if (!rsLoad.EndOfFile())
                {
                    if (Information.IsDate(rsLoad.Get_Fields("currentexemptions")))
                    {
                        if (rsLoad.Get_Fields_DateTime("currentexemptions") != DateTime.FromOADate(0))
                        {
                            bool boolAsk = false;
                            boolAsk = false;
                            if (Information.IsDate(rsLoad.Get_Fields("Batchcalc")))
                            {
                                if (rsLoad.Get_Fields_DateTime("batchcalc") != DateTime.FromOADate(0))
                                {
                                    if (DateAndTime.DateDiff("n", (DateTime) rsLoad.Get_Fields_DateTime("batchcalc"),
                                        (DateTime) rsLoad.Get_Fields_DateTime("currentexemptions")) < 0)
                                    {
                                        boolAsk = true;
                                    }
                                }
                            }

                            if (Information.IsDate(rsLoad.Get_Fields("calculated")))
                            {
                                if (rsLoad.Get_Fields_DateTime("calculated") != DateTime.FromOADate(0))
                                {
                                    if (DateAndTime.DateDiff("n", (DateTime) rsLoad.Get_Fields_DateTime("calculated"),
                                        (DateTime) rsLoad.Get_Fields_DateTime("currentexemptions")) < 0)
                                    {
                                        boolAsk = true;
                                    }
                                }
                            }

                            if (boolAsk)
                            {
                                if (MessageBox.Show(
                                    "Current exemptions have not been recalculated since account(s) have been recalculated" +
                                    "\r\n" + "Current exempt and assessment amounts may be incorrect" + "\r\n" +
                                    "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question) != DialogResult.Yes)
                                {
                                    this.Close();
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (MessageBox.Show(
                            "Current exemptions have not been recalculated since account(s) have been recalculated" +
                            "\r\n" + "Current exempt and assessment amounts may be incorrect" + "\r\n" +
                            "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) != DialogResult.Yes)
                        {
                            this.Close();
                            return;
                        }
                    }
                }
            }

            frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "AssessmentByCode");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			SubReport1.Report = new srptLandBldgAssess();
			SubReport2.Report = new srptlandcode();
			SubReport2.Report.UserData = "L";
			SubReport3.Report = new srptLastTran();
			SubReport3.Report.UserData = "L";
			SubReport4.Report = new srptCYBldgCode();
			SubReport5.Report = new srptlandcode();
			SubReport5.Report.UserData = "C";
			SubReport6.Report = new srptLastTran();
			SubReport6.Report.UserData = "C";
			intPage = 1;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsTemp.OpenRecordset("select count(*) as thecount,sum(cast(lastlandval as bigint)) as landtot,sum(cast(lastbldgval as bigint)) as bldgtot,sum(cast(rlexemption as bigint)) as exempttot from master where not rsdeleted = 1", modGlobalVariables.strREDatabase);
			txtLBCount.Text = FCConvert.ToString(Conversion.Val(clsTemp.GetData("thecount")));
			lngLand = Conversion.Val(clsTemp.GetData("landtot"));
			txtLBLand.Text = Strings.Format(lngLand, "#,###,###,##0");
			lngbldg = Conversion.Val(clsTemp.GetData("bldgtot"));
			txtLBBldg.Text = Strings.Format(lngbldg, "###,###,###,##0");
			lngExempt = Conversion.Val(clsTemp.GetData("exempttot"));
			txtLBExempt.Text = Strings.Format(lngExempt, "###,###,###,##0");
			lngTotal = lngLand - lngExempt + lngbldg;
			txtLBTotal.Text = Strings.Format(lngTotal, "#,###,###,###,##0");
			txtLLCount.Text = FCConvert.ToString(Conversion.Val(clsTemp.GetData("thecount")));
			txtLLLand.Text = Strings.Format(lngLand, "#,###,###,##0");
			txtLLBldg.Text = Strings.Format(lngbldg, "###,###,###,##0");
			txtLLExempt.Text = Strings.Format(lngExempt, "###,###,###,##0");
			txtLLTotal.Text = Strings.Format(lngTotal, "#,###,###,###,##0");
			txtLTCount.Text = FCConvert.ToString(Conversion.Val(clsTemp.GetData("thecount")));
			txtLTLand.Text = Strings.Format(lngLand, "#,###,###,##0");
			txtLTBldg.Text = Strings.Format(lngbldg, "###,###,###,##0");
			txtLTExempt.Text = Strings.Format(lngExempt, "###,###,###,##0");
			txtLTTotal.Text = Strings.Format(lngTotal, "#,###,###,###,##0");
			clsTemp.OpenRecordset("select count(*) as thecount,sum(cast(rllandval as bigint)) as landtot,sum(cast(rlbldgval as bigint)) as bldgtot,sum(cast(correxemption as bigint)) as exempttot from master where not rsdeleted = 1", modGlobalVariables.strREDatabase);
			txtCBCount.Text = FCConvert.ToString(Conversion.Val(clsTemp.GetData("thecount")));
			lngLand = Conversion.Val(clsTemp.GetData("landtot"));
			txtCBLand.Text = Strings.Format(lngLand, "#,###,###,##0");
			lngbldg = Conversion.Val(clsTemp.GetData("bldgtot"));
			txtCBBldg.Text = Strings.Format(lngbldg, "###,###,###,##0");
			lngExempt = Conversion.Val(clsTemp.GetData("exempttot"));
			txtCBExempt.Text = Strings.Format(lngExempt, "###,###,###,##0");
			lngTotal = lngLand - lngExempt + lngbldg;
			txtCBTotal.Text = Strings.Format(lngTotal, "#,###,###,###,##0");
			txtCLCount.Text = FCConvert.ToString(Conversion.Val(clsTemp.GetData("thecount")));
			txtCLLand.Text = Strings.Format(lngLand, "#,###,###,##0");
			txtCLBldg.Text = Strings.Format(lngbldg, "###,###,###,##0");
			txtCLExempt.Text = Strings.Format(lngExempt, "###,###,###,##0");
			txtCLTotal.Text = Strings.Format(lngTotal, "#,###,###,###,##0");
			txtCTCount.Text = FCConvert.ToString(Conversion.Val(clsTemp.GetData("thecount")));
			txtCTLand.Text = Strings.Format(lngLand, "#,###,###,##0");
			txtCTBldg.Text = Strings.Format(lngbldg, "###,###,###,##0");
			txtCTExempt.Text = Strings.Format(lngExempt, "###,###,###,##0");
			txtCTTotal.Text = Strings.Format(lngTotal, "#,###,###,###,##0");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

	
	}
}
