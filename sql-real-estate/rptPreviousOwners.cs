﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPreviousOwners.
	/// </summary>
	public partial class rptPreviousOwners : BaseSectionReport
	{
		public static rptPreviousOwners InstancePtr
		{
			get
			{
				return (rptPreviousOwners)Sys.GetInstance(typeof(rptPreviousOwners));
			}
		}

		protected rptPreviousOwners _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
                clsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptPreviousOwners()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Previous Owners Report";
		}
		// nObj = 1
		//   0	rptPreviousOwners	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();
		int lngPage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lngAccount As int	OnWriteFCConvert.ToDouble(
		public void Init(ref short intOrder, bool boolofAccount, int lngAccount = 0, string strStartDate = "", string strEndDate = "")
		{
			string strOrder = "";
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = clsReport.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			if (boolofAccount)
			{
				if (intOrder == 0)
				{
					strOrder = "previousowner.SaleDate";
				}
				else
				{
					strOrder = "Name";
				}
				// Call clsReport.OpenRecordset("select previousowner.*,rsname,rssecowner from previousowner inner join master on (master.rsaccount = previousowner.account) where rscard = 1 and account = " & lngAccount & " order by " & strOrder, strREDatabase)
				clsReport.OpenRecordset("select previousowner.*,rsname,rssecowner from " + strMasterJoinQuery + " inner join previousowner on (mj.rsaccount = previousowner.account) where rscard = 1 and account = " + FCConvert.ToString(lngAccount) + " order by " + strOrder, modGlobalVariables.strREDatabase);
			}
			else
			{
				if (intOrder == 0)
				{
					strOrder = "previousowner.SaleDate";
				}
				else if (intOrder == 1)
				{
					strOrder = "Name";
				}
				else
				{
					strOrder = "Account";
				}
				if (MessageBox.Show("Include records with missing sale dates?", "Include unknown dates?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsReport.OpenRecordset("select previousowner.*,rsname,rssecowner from " + strMasterJoinQuery + " inner join previousowner on (mj.rsaccount = previousowner.account) where previousowner.saledate between '" + strStartDate + "' and '" + strEndDate + "' or previousowner.saledate = 0 order by " + strOrder, modGlobalVariables.strREDatabase);
				}
				else
				{
					clsReport.OpenRecordset("select previousowner.*,rsname,rssecowner from " + strMasterJoinQuery + " inner join previousowner on (mj.rsaccount = previousowner.account) where previousowner.saledate between '" + strStartDate + "' and '" + strEndDate + "'  order by " + strOrder, modGlobalVariables.strREDatabase);
				}
			}
			// Me.Show , MDIParent
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "PreviousOwners");
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtPage.Text = "Page 1";
			lngPage = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			DateTime dttemp;
			if (!clsReport.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = FCConvert.ToString(clsReport.Get_Fields("account"));
				txtName.Text = FCConvert.ToString(clsReport.Get_Fields_String("name"));
				txtSecOwner.Text = FCConvert.ToString(clsReport.Get_Fields_String("secowner"));
				txtCurrentName.Text = FCConvert.ToString(clsReport.Get_Fields_String("rsname"));
				txtCurrentSecOwner.Text = FCConvert.ToString(clsReport.Get_Fields_String("rssecowner"));
				txtAddress1.Text = FCConvert.ToString(clsReport.Get_Fields_String("address1"));
				txtAddress2.Text = FCConvert.ToString(clsReport.Get_Fields_String("address2"));
				txtCity.Text = FCConvert.ToString(clsReport.Get_Fields_String("city"));
				txtState.Text = FCConvert.ToString(clsReport.Get_Fields_String("state"));
				txtZip.Text = FCConvert.ToString(clsReport.Get_Fields_String("zip") + " " + clsReport.Get_Fields_String("zip4"));
				dttemp = (DateTime)clsReport.Get_Fields_DateTime("saledate");
				if (dttemp.ToOADate() == 0)
				{
					txtSaleDate.Text = "";
				}
				else
				{
					txtSaleDate.Text = Strings.Format(dttemp, "MM/dd/yyyy");
				}
				clsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

		
	}
}
