﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPPSummary.
	/// </summary>
	public partial class rptPPSummary : BaseSectionReport
	{
		public rptPPSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Personal Property Summary";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPPSummary InstancePtr
		{
			get
			{
				return (rptPPSummary)Sys.GetInstance(typeof(rptPPSummary));
			}
		}

		protected rptPPSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		double lngTotAssess;
		bool boolUseExceeds;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngNumUpdatedTooMany = 0;
			int lngNumNew = 0;
			int lngChange = 0;
			int lngOldValue = 0;
			int lngNewValue = 0;
			int lngNumDeleted = 0;
			int lngUps = 0;
			int lngDowns = 0;
			double lngTotValueChange = 0;
			int lngExceedsUp = 0;
			int lngExceedsDown = 0;
			double dblChange = 0;
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngTotAssess = 0;
			if (frmPPImportXF.InstancePtr.cmbUpdateType.ItemData(frmPPImportXF.InstancePtr.cmbUpdateType.SelectedIndex) == modGlobalVariablesXF.CNSTUPDATEINFOONLY)
			{
				// don't show amounts
				Detail.Height = 2500 / 1440F;
				Detail.CanGrow = false;
			}
			if (boolUseExceeds)
			{
				dblChange = Conversion.Val(frmPPImportXF.InstancePtr.txtPercentChg.Text);
				lblExceed.Text = "Exceeds " + FCConvert.ToString(dblChange) + "% Change";
			}
			txtNoMatch.Text = Strings.Format(frmPPImportXF.InstancePtr.GridNew.Rows - 1, "#,###,##0");
			txtNotUpdated.Text = Strings.Format(frmPPImportXF.InstancePtr.GridNotUpdated.Rows - 1, "#,###,##0");
			txtUpdated.Text = Strings.Format(frmPPImportXF.InstancePtr.GridGood.Rows - 1, "#,###,##0");
			lngNumUpdatedTooMany = 0;
			lngNumNew = 0;
			lngTotValueChange = 0;
			lngDowns = 0;
			lngUps = 0;
			lngTotValueChange = 0;
			lngExceedsUp = 0;
			lngExceedsDown = 0;
			for (x = 1; x <= (frmPPImportXF.InstancePtr.GridBadAccounts.Rows - 1); x++)
			{
				if (fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPDELETED)) != string.Empty)
				{
					if (FCConvert.CBool(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPDELETED)))
					{
						lngNumDeleted += 1;
						if (frmPPImportXF.InstancePtr.chkUndeleteAccounts.CheckState == Wisej.Web.CheckState.Checked)
						{
							lngTotAssess += Conversion.Val(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE));
						}
					}
				}
				if (FCConvert.ToBoolean(Conversion.Val(frmPPImportXF.InstancePtr.GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPUPDATED))))
				{
					lngNumUpdatedTooMany += 1;
				}
			}
			// x
			// process the unchanged stuff
			// need to account for it in the totals
			if (!(frmPPImportXF.InstancePtr.chkMarkDeleted.CheckState == Wisej.Web.CheckState.Checked))
			{
				for (x = 1; x <= (frmPPImportXF.InstancePtr.GridNotUpdated.Rows - 1); x++)
				{
					if (Conversion.Val(frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE)) > 0)
					{
						lngTotAssess += Conversion.Val(frmPPImportXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE));
					}
				}
				// x
			}
			// process the new stuff
			for (x = 1; x <= (frmPPImportXF.InstancePtr.GridNew.Rows - 1); x++)
			{
				if (FCConvert.CBool(frmPPImportXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPUSEROW)))
				{
					if (Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE)) > 0)
					{
						lngNumNew += 1;
						lngTotValueChange += Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE));
						lngTotAssess += Conversion.Val(frmPPImportXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE));
					}
				}
			}
			// x
			// process the changed stuff
			for (x = 1; x <= (frmPPImportXF.InstancePtr.GridGood.Rows - 1); x++)
			{
				lngNewValue = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE))));
				lngOldValue = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPORIGINALVALUE))));
				lngTotAssess += lngNewValue;
				if (lngNewValue != lngOldValue)
				{
					// value is different
					if (lngNewValue > lngOldValue)
					{
						lngChange = lngNewValue - lngOldValue;
						lngTotValueChange += lngChange;
						lngUps += 1;
						if (Conversion.Val(frmPPImportXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPEXCEED)) != 0 && boolUseExceeds)
						{
							lngExceedsUp += 1;
						}
					}
					else
					{
						lngChange = lngOldValue - lngNewValue;
						lngTotValueChange -= lngChange;
						lngDowns += 1;
						if (Conversion.Val(frmPPImportXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPEXCEED)) != 0 && boolUseExceeds)
						{
							lngExceedsDown += 1;
						}
					}
				}
			}
			// x
			txtDeleted.Text = Strings.Format(lngNumDeleted, "#,###,###,##0");
			txtBadUpdated.Text = Strings.Format(lngNumUpdatedTooMany, "#,###,###,##0");
			txtChgVal.Text = Strings.Format(lngTotValueChange, "#,###,###,##0");
			txtDown.Text = Strings.Format(lngDowns, "#,###,###,##0");
			txtUp.Text = Strings.Format(lngUps, "#,###,###,##0");
			txtTotalNet.Text = Strings.Format(lngTotAssess, "#,###,###,##0");
			txtNew.Text = Strings.Format(lngNumNew, "#,###,###,##0");
			txtPercUp.Text = Strings.Format(lngExceedsUp, "#,###,###,##0");
			txtPercDown.Text = Strings.Format(lngExceedsDown, "#,###,###,##0");
		}

		public void Init(bool boolExceeds)
		{
			boolUseExceeds = boolExceeds;
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "PPSummary");
		}

		
	}
}
