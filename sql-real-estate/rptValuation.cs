﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using fecherFoundation;
using Wisej.Web;
using Global;
using SharedApplication.CentralDocuments.Commands;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptValuation.
	/// </summary>
	public partial class rptValuation : BaseSectionReport
	{
		public static rptValuation InstancePtr
		{
			get
			{
				return (rptValuation)Sys.GetInstance(typeof(rptValuation));
			}
		}

		protected rptValuation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptValuation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Valuation Report";
		}
		// nObj = 1
		//   0	rptValuation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCCard;
		// current card to process
		int intBeginCard;
		int intEndCard;
		int intNumCards;
		bool boolUseAccepted;
		public string strFonttoUse = "";
		
		string strMaster;
		private bool boolDidntUpdateHeader;
		int intPage;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int const_printtoolid = 0;
			int cnt;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			boolDidntUpdateHeader = false;


		}

		public void Init(short intBCard, short intECard, ref int intNcards, bool modalDialog, bool boolUseAcceptedValues = true)
		{
			// number of total cards, not number between beginning and end of range
			// Dim prt As Printer
			strMaster = "Master";
			if (!modGlobalVariables.Statics.boolRegRecords)
			{
				strMaster = "SRMaster";
			}
			boolUseAccepted = boolUseAcceptedValues;
			intBeginCard = intBCard;
			intEndCard = intECard;
			intCCard = intBeginCard - 1;
			intNumCards = intNcards;

				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
			
			// Me.Show vbModal
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int lngTotLand = 0;
			int lngTotBldg = 0;
			int lngTotTot;
			intCCard += 1;
			eArgs.EOF = intCCard > intEndCard;
			if (eArgs.EOF)
				return;
			if (Strings.Trim(modSpeedCalc.Statics.CalcPropertyList[intCCard + 1].SaleDate) != string.Empty)
			{
				subSaleData.Report = new srptSaleData();
				subSaleData.Report.UserData = intCCard;
			}
			else
			{
				subSaleData.Report = null;
			}
			if (Strings.Trim(modSpeedCalc.Statics.CalcPropertyList[intCCard + 1].NeighborhoodDesc) != string.Empty)
			{
				SubMisc.Report = new srptMisc();
				SubMisc.Report.UserData = intCCard;
			}
			else
			{
				SubMisc.Report = null;
			}
			if (Strings.Trim(modSpeedCalc.Statics.CalcPropertyList[intCCard + 1].LandDesc[1]) != string.Empty)
			{
				subLandDesc.Report = new srptLandDescription();
				subLandDesc.Report.UserData = intCCard;
			}
			else
			{
				subLandDesc.Report = null;
			}
			if (Strings.Trim(modSpeedCalc.Statics.CalcPropertyList[intCCard + 1].DwellingOrCommercial) == "C")
			{
				subDwellComm.Report = new srptComm();
				subDwellComm.Report.UserData = intCCard;
			}
			else
			{
				if (Strings.Trim(modSpeedCalc.Statics.CalcPropertyList[intCCard + 1].DwellingOrCommercial) == "Y")
				{
					subDwellComm.Report = new srptDwell();
					subDwellComm.Report.UserData = intCCard;
				}
				else
				{
					subDwellComm.Report = null;
				}
			}
			if (FCConvert.ToInt16(modSpeedCalc.Statics.CalcOutList[intCCard + 1].OutbuildingType[1]) != 0)
			{
				subOutBuilding.Report = new srptOut();
				subOutBuilding.Report.UserData = intCCard;
			}
			else
			{
				subOutBuilding.Report = null;
			}
			if (boolUseAccepted)
			{
				// If Val(.txtLandFrom(intCCard)) > 2 Then
				if (FCConvert.ToInt16(modProperty.Statics.intOverRideCodeLand[intCCard + 1]) > 2)
				{
					lblLandTot.Text = "Land Override";
				}
				// If Val(.txtBldgFrom(intCCard)) > 2 Then
				if (FCConvert.ToInt16(modProperty.Statics.intOverRideCodeBldg[intCCard + 1]) > 2)
				{
					lblBldgTot.Text = "Bldg Override";
				}
			}
			else
			{
				lblLandTot.Text = "Calc. Land";
				lblBldgTot.Text = "Calc. Bldg";
			}
			if (boolUseAccepted)
			{
				lngTotLand = FCConvert.ToInt32(FCConvert.ToDouble(frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intCCard + 1, 4)));
				lngTotBldg = FCConvert.ToInt32(FCConvert.ToDouble(frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intCCard + 1, 5)));
				// lngTotLand = ((lngTotLand / RoundingVar) \ 1) * RoundingVar
				lngTotLand = modGlobalRoutines.RERound(ref lngTotLand);
				// lngTotBldg = BuildingTotal(intCCard + 1)
			}
			else
			{
				lngTotLand = FCConvert.ToInt32(FCConvert.ToDouble(frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intCCard + 1, 1)));
				lngTotBldg = FCConvert.ToInt32(FCConvert.ToDouble(frmNewValuationReport.InstancePtr.TotalGrid.TextMatrix(intCCard + 1, 2)));
				// lngTotLand = ((lngTotLand / RoundingVar) \ 1) * RoundingVar
				lngTotLand = modGlobalRoutines.RERound(ref lngTotLand);
			}
			// lngTotBldg = ((lngTotBldg / RoundingVar) \ 1) * RoundingVar
			lngTotBldg = modGlobalRoutines.RERound(ref lngTotBldg);
			txtLand.Text = Strings.Format(lngTotLand, "#,###,##0");
			txtBldg.Text = Strings.Format(lngTotBldg, "#,###,##0");
			txtTotal.Text = Strings.Format(lngTotLand + lngTotBldg, "#,###,###,##0");
			txtRatio.Text = FCConvert.ToString(frmNewValuationReport.InstancePtr.dblRatioAssess * 100) + "%";
			if (Conversion.Val(frmNewValuationReport.InstancePtr.dblRatioAssess) != 1 && boolUseAccepted && !modGlobalVariables.Statics.CustomizedInfo.HideValuationRatio)
			{
				txtRatio.Visible = true;
				lblRatio.Visible = true;
			}
			else
			{
				txtRatio.Visible = false;
				lblRatio.Visible = false;
			}
		}
        // vbPorter upgrade warning: intCard As short	OnWriteFCConvert.ToInt32(
        //private bool ShowPicsSketches_26(int lngAcct, int lngID, short intCard)
        //{
        //	return ShowPicsSketches(ref lngAcct, ref lngID, ref intCard);
        //}
        private bool ShowPicsSketches(int lngAcct, int lngID, short intCard, Guid cardIdentifier)
        {
            bool ShowPicsSketches = false;
            bool boolPics;
            bool boolSketch;
            string strSketches;
            string[] strAry = null;
            string strPics;
            int intMaxPics;
            int x;
           
            ShowPicsSketches = false;
            Image1.Image = null;
            Image2.Image = null;
            Image3.Image = null;
            Image4.Image = null;
            Image5.Image = null;
            Image6.Image = null;
            Image1.Height = 2880 / 1440f;
            Image2.Height = 2880 / 1440f;
            Image3.Height = 2880 / 1440f;
            Image4.Height = 2880 / 1440f;
            Image5.Height = 2880 / 1440f;
            Image6.Height = 2880 / 1440f;
            Image1.Width = 3;
            Image2.Width = 3;
            Image3.Width = 3;
            Image4.Width = 3;
            Image5.Width = 3;
            Image6.Width = 3;
            Image5.Top = 7380 / 1440f;
            boolPics = modGlobalVariables.Statics.CustomizedInfo.boolShowPicOnVal;
            boolSketch = modGlobalVariables.Statics.CustomizedInfo.boolShowSketchOnVal;
            //strSketches = "";
            int sketchId = 0;
            if (boolSketch)
            {
                // Call clsLoad.OpenRecordset("select id from master where rsaccount = " & lngAcct & " and rscard = " & intCard, strREDatabase)
               // strSketches = modGlobalRoutines.GetListOfSketches(ref lngID);
                var sketchInfos = StaticSettings.GlobalCommandDispatcher.Send(new GetSketchInfos(cardIdentifier)).Result.ToList();
                if (!sketchInfos.Any())
                {
                    boolSketch = false;
                }
                else
                {
                    sketchId = sketchInfos.FirstOrDefault().Id;
                }
            }
            strPics = "";
            List<PictureRecord> picList = new List<PictureRecord>();
            IPropertyPicturesViewModel picturesVM = null;


            if (boolPics)
            {
                picturesVM = StaticSettings.GlobalCommandDispatcher.Send(new GetPropertyPictureViewModel()).Result;
                picturesVM.LoadPictures(lngAcct, intCard, cardIdentifier);
                if (picturesVM.Pictures.Count() < 1)
                {
                    boolPics = false;
                }
            }
            intMaxPics = 0;
            if (boolPics && boolSketch)
            {
                intMaxPics = 5;
            }
            else if (boolPics)
            {
                intMaxPics = 6;
            }
            else if (boolSketch)
            {
                intMaxPics = 0;
            }
            if (boolPics && picturesVM != null)
            {
                picturesVM.Pictures.SetToFirstPicture();
                //int limit = intMaxPics;
                if (modGlobalVariables.Statics.CustomizedInfo.boolShowFirstPiconVal)
                    intMaxPics = 1;
                x = 0;
                if (picturesVM.Pictures.Count() < intMaxPics)
                {
                    intMaxPics = picturesVM.Pictures.Count();
                }
                if (intMaxPics == 1)
                {
                    Detail.Controls["Image1"].Width = 6;
                    Detail.Controls["Image1"].Height = 4;
                }

                while (picturesVM.Pictures.HasCurrentPicture() && x < intMaxPics)
                {
                    x++;
                    var picData = picturesVM.GetCurrentPicture();
                    var picControl =
                        (Detail.Controls["Image" + x] as GrapeCity.ActiveReports.SectionReportModel.Picture);

                    picControl.Image = FCUtils.PictureFromBytes(picData, picControl.Width, picControl.Height);
                    picturesVM.Pictures.NextPicture();
                }

            }

            if (boolSketch)
            {
                Detail.Controls["Image" + (intMaxPics + 1)].Height = 4;
                int imageNo = intMaxPics + 1;
                if (intMaxPics == 0)
                {
                    Detail.Controls["Image" + (intMaxPics + 1)].Width = 6;
                    Detail.Controls["Image" + (intMaxPics + 1)].Height = 8;
                }
                else if (intMaxPics == 1 || intMaxPics == 2)
                {
                    Detail.Controls["Image5"].Width = 6;
                    Detail.Controls["Image5"].Height = 4;
                    imageNo = 5;
                    Detail.Controls["Image5"].Top = Detail.Controls["Image1"].Top + Detail.Controls["Image1"].Height + .5f;
                }
                var width = Detail.Controls["Image" + imageNo].Width;
                var height = Detail.Controls["Image" + imageNo].Height;

                (Detail.Controls["Image" + imageNo] as GrapeCity.ActiveReports.SectionReportModel.Picture).Image = FCUtils.PictureFromBytes(StaticSettings.GlobalCommandDispatcher.Send(new GetSketchImage(sketchId)).Result, width, height);
            }
            if (boolSketch || boolPics)
            {
                ShowPicsSketches = true;
            }
            if (ShowPicsSketches)
            {
                Image1.Visible = true;
                Image2.Visible = true;
                Image3.Visible = true;
                Image4.Visible = true;
                Image5.Visible = true;
                Image6.Visible = true;
            }
            return ShowPicsSketches;
        }
     
        private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			subDwellComm.Report = null;
			subLandDesc.Report = null;
			SubMisc.Report = null;
			subOutBuilding.Report = null;
			SubReport1.Report = null;
			subSaleData.Report = null;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intCCard = intBeginCard - 1;
			intPage = 1;
		}


		private void Detail_Format(object sender, EventArgs e)
		{
			PageBreak1.Enabled = false;
			Image1.Visible = false;
			Image2.Visible = false;
			Image3.Visible = false;
			Image4.Visible = false;
			Image5.Visible = false;
			Image6.Visible = false;

			if (modGlobalVariables.Statics.boolRegRecords)
			{
				HandlePicsSketches();
			}
			txtCard.Text = (intCCard + 1).ToString();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                if (intCCard <= intEndCard)
                {
                    if (!PageBreak1.Enabled || boolDidntUpdateHeader)
                    {
                        boolDidntUpdateHeader = false;
                        if (Conversion.Val(txtCard.Text) == intCCard + 1)
                        {
                            txtCard.Visible = false;
                            Label4.Visible = false;
                            Label6.Visible = false;
                            txtNumCards.Visible = false;
                        }

                        txtCard.Visible = true;
                        Label4.Visible = true;
                        Label6.Visible = true;
                        txtNumCards.Visible = true;
                        txtNumCards.Text = intNumCards.ToString();
                        txtAccount.Text = modGlobalVariables.Statics.gintLastAccountNumber.ToString();
                        txtName.Text = "";
                        txtSecName.Text = "";
                        if (modGlobalVariables.Statics.boolRegRecords)
                        {
                            txtName.Text = modDataTypes.Statics.MR.Get_Fields_String("DeedName1");
                            txtSecName.Text = modDataTypes.Statics.MR.Get_Fields_String("DeedName2");
                        }
                        else
                        {
                            txtName.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsname") + "");
                            if (Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rssecowner") + "") !=
                                string.Empty)
                            {
                                txtSecName.Text =
                                    Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rssecowner") + "");
                            }
                            else
                            {
                                txtSecName.Text = "";
                            }
                        }

                        txtMapLot.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + "");
                        if (modGlobalVariables.Statics.boolRegRecords)
                        {
                            clsTemp.OpenRecordset(
                                "Select rslocstreet,rslocapt,rslocnumalph from " + strMaster + " where rsaccount = " +
                                FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) +
                                " and rscard = " + FCConvert.ToString(intCCard + 1), modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            clsTemp.OpenRecordset(
                                "Select rslocstreet,rslocapt,rslocnumalph from " + strMaster + " where rsaccount = " +
                                FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) +
                                " and rscard = " + FCConvert.ToString(intCCard + 1) + " and saleid = " +
                                FCConvert.ToString(modGlobalVariables.Statics.glngSaleID),
                                modGlobalVariables.strREDatabase);
                        }

                        txtLocation.Text =
                            Strings.Trim(
                                Strings.Trim(clsTemp.Get_Fields_String("rslocnumalph") + " " +
                                             clsTemp.Get_Fields_String("rslocapt")) + " " +
                                clsTemp.Get_Fields_String("rslocstreet"));
                    }
                    else
                    {
                        boolDidntUpdateHeader = true;
                    }
                }
                else
                {
                    txtCard.Visible = false;
                    Label4.Visible = false;
                    Label6.Visible = false;
                    txtNumCards.Visible = false;
                }

                txtPage.Text = "Page " + FCConvert.ToString(intPage);
                intPage += 1;
            }
        }

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (intBeginCard != intEndCard)
			{
				this.SubReport1.Report = new srptCalcTotal();
			}
			else
			{
				this.SubReport1.Report = null;
				ReportFooter.Height = 0;
				ReportFooter.Visible = false;
			}
		}

		private void HandlePicsSketches()
		{
			PageBreak1.Enabled = false;
			Image1.Visible = false;
			Image2.Visible = false;
			Image3.Visible = false;
			Image4.Visible = false;
			Image5.Visible = false;
			Image6.Visible = false;
			if (!modDataTypes.Statics.MR.EndOfFile())
			{
				if (modGlobalVariables.Statics.CustomizedInfo.boolShowPicOnVal || modGlobalVariables.Statics.CustomizedInfo.boolShowSketchOnVal)
				{
					if (ShowPicsSketches(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modDataTypes.Statics.MR.Get_Fields_Int32("id"), FCConvert.ToInt16(intCCard + 1),Guid.Parse(modDataTypes.Statics.MR.Get_Fields_String("CardId"))))
					{
						PageBreak1.Enabled = true;
						// MR.MoveNext
					}
					else
					{
						PageBreak1.Enabled = false;
					}
				}
			}
		}

		
	}
}
