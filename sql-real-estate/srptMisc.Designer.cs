﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptMisc.
	/// </summary>
	partial class srptMisc
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptMisc));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblNeighborhood = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTreegrowth = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOpen1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOpen2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTranLandBldg = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblXCoord = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYCoord = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNeighborhood = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTopography = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtilities = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReference1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.TxtReference2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTranLandBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtXCoord = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYCoord = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptions = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandSchedule = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTreeGrowth = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtZoning = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblNeighborhood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTreegrowth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTranLandBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblXCoord)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYCoord)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeighborhood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopography)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilities)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReference1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TxtReference2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTranLandBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtXCoord)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYCoord)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandSchedule)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTreeGrowth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZoning)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblNeighborhood,
				this.lblTreegrowth,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.lblOpen1,
				this.lblOpen2,
				this.Label7,
				this.Label8,
				this.lblTranLandBldg,
				this.lblXCoord,
				this.lblYCoord,
				this.Label9,
				this.Label10,
				this.txtNeighborhood,
				this.txtTopography,
				this.txtUtilities,
				this.txtStreet,
				this.txtOpen1,
				this.txtOpen2,
				this.txtReference1,
				this.TxtReference2,
				this.txtTranLandBldg,
				this.txtXCoord,
				this.txtYCoord,
				this.txtExemptions,
				this.txtLandSchedule,
				this.Field1,
				this.txtTreeGrowth,
				this.txtZoning
			});
			this.Detail.Height = 2.03125F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblNeighborhood
			// 
			this.lblNeighborhood.Height = 0.19625F;
			this.lblNeighborhood.HyperLink = null;
			this.lblNeighborhood.Left = 0.0625F;
			this.lblNeighborhood.MultiLine = false;
			this.lblNeighborhood.Name = "lblNeighborhood";
			this.lblNeighborhood.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblNeighborhood.Tag = "text";
			this.lblNeighborhood.Text = null;
			this.lblNeighborhood.Top = 0F;
			this.lblNeighborhood.Width = 1.125F;
			// 
			// lblTreegrowth
			// 
			this.lblTreegrowth.Height = 0.19625F;
			this.lblTreegrowth.HyperLink = null;
			this.lblTreegrowth.Left = 0.0625F;
			this.lblTreegrowth.MultiLine = false;
			this.lblTreegrowth.Name = "lblTreegrowth";
			this.lblTreegrowth.Style = "font-family: \'Tahoma\'";
			this.lblTreegrowth.Tag = "text";
			this.lblTreegrowth.Text = null;
			this.lblTreegrowth.Top = 0.15625F;
			this.lblTreegrowth.Width = 1.125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19625F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.0625F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'";
			this.Label3.Tag = "text";
			this.Label3.Text = "Zoning/Use";
			this.Label3.Top = 0.3125F;
			this.Label3.Width = 1.125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19625F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.0625F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'";
			this.Label4.Tag = "text";
			this.Label4.Text = "Topography";
			this.Label4.Top = 0.46875F;
			this.Label4.Width = 1.125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19625F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.0625F;
			this.Label5.MultiLine = false;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'";
			this.Label5.Tag = "text";
			this.Label5.Text = "Utilities";
			this.Label5.Top = 0.625F;
			this.Label5.Width = 1.125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19625F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.0625F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'";
			this.Label6.Tag = "text";
			this.Label6.Text = "Street";
			this.Label6.Top = 0.78125F;
			this.Label6.Width = 1.125F;
			// 
			// lblOpen1
			// 
			this.lblOpen1.Height = 0.19625F;
			this.lblOpen1.HyperLink = null;
			this.lblOpen1.Left = 0.0625F;
			this.lblOpen1.MultiLine = false;
			this.lblOpen1.Name = "lblOpen1";
			this.lblOpen1.Style = "font-family: \'Tahoma\'";
			this.lblOpen1.Tag = "text";
			this.lblOpen1.Text = "Open 1";
			this.lblOpen1.Top = 0.9375F;
			this.lblOpen1.Width = 1.125F;
			// 
			// lblOpen2
			// 
			this.lblOpen2.Height = 0.19625F;
			this.lblOpen2.HyperLink = null;
			this.lblOpen2.Left = 0.0625F;
			this.lblOpen2.MultiLine = false;
			this.lblOpen2.Name = "lblOpen2";
			this.lblOpen2.Style = "font-family: \'Tahoma\'";
			this.lblOpen2.Tag = "text";
			this.lblOpen2.Text = "Open 2";
			this.lblOpen2.Top = 1.09375F;
			this.lblOpen2.Width = 1.125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19625F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.0625F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'";
			this.Label7.Tag = "text";
			this.Label7.Text = "Reference 1";
			this.Label7.Top = 1.25F;
			this.Label7.Width = 1.125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19625F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.0625F;
			this.Label8.MultiLine = false;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'";
			this.Label8.Tag = "text";
			this.Label8.Text = "Reference 2";
			this.Label8.Top = 1.40625F;
			this.Label8.Width = 1.125F;
			// 
			// lblTranLandBldg
			// 
			this.lblTranLandBldg.Height = 0.19625F;
			this.lblTranLandBldg.HyperLink = null;
			this.lblTranLandBldg.Left = 0.0625F;
			this.lblTranLandBldg.MultiLine = false;
			this.lblTranLandBldg.Name = "lblTranLandBldg";
			this.lblTranLandBldg.Style = "font-family: \'Tahoma\'";
			this.lblTranLandBldg.Tag = "text";
			this.lblTranLandBldg.Text = null;
			this.lblTranLandBldg.Top = 1.5625F;
			this.lblTranLandBldg.Width = 1.125F;
			// 
			// lblXCoord
			// 
			this.lblXCoord.Height = 0.19625F;
			this.lblXCoord.HyperLink = null;
			this.lblXCoord.Left = 0.0625F;
			this.lblXCoord.MultiLine = false;
			this.lblXCoord.Name = "lblXCoord";
			this.lblXCoord.Style = "font-family: \'Tahoma\'";
			this.lblXCoord.Tag = "text";
			this.lblXCoord.Text = "X Coord";
			this.lblXCoord.Top = 1.71875F;
			this.lblXCoord.Width = 1.125F;
			// 
			// lblYCoord
			// 
			this.lblYCoord.Height = 0.19625F;
			this.lblYCoord.HyperLink = null;
			this.lblYCoord.Left = 2.0625F;
			this.lblYCoord.MultiLine = false;
			this.lblYCoord.Name = "lblYCoord";
			this.lblYCoord.Style = "font-family: \'Tahoma\'";
			this.lblYCoord.Tag = "text";
			this.lblYCoord.Text = "Y Coord";
			this.lblYCoord.Top = 1.71875F;
			this.lblYCoord.Width = 1.1875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19625F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.0625F;
			this.Label9.MultiLine = false;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'";
			this.Label9.Tag = "text";
			this.Label9.Text = "Exemption(s)";
			this.Label9.Top = 1.875F;
			this.Label9.Width = 1.125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19625F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.3125F;
			this.Label10.MultiLine = false;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'";
			this.Label10.Tag = "text";
			this.Label10.Text = "Land Schedule";
			this.Label10.Top = 1.875F;
			this.Label10.Width = 1.25F;
			// 
			// txtNeighborhood
			// 
			this.txtNeighborhood.Height = 0.19625F;
			this.txtNeighborhood.HyperLink = null;
			this.txtNeighborhood.Left = 1.25F;
			this.txtNeighborhood.MultiLine = false;
			this.txtNeighborhood.Name = "txtNeighborhood";
			this.txtNeighborhood.Style = "font-family: \'Tahoma\'";
			this.txtNeighborhood.Tag = "text";
			this.txtNeighborhood.Text = null;
			this.txtNeighborhood.Top = 0F;
			this.txtNeighborhood.Width = 2.875F;
			// 
			// txtTopography
			// 
			this.txtTopography.Height = 0.19625F;
			this.txtTopography.Left = 1.25F;
			this.txtTopography.MultiLine = false;
			this.txtTopography.Name = "txtTopography";
			this.txtTopography.Style = "font-family: \'Tahoma\'";
			this.txtTopography.Tag = "text";
			this.txtTopography.Text = null;
			this.txtTopography.Top = 0.46875F;
			this.txtTopography.Width = 2.875F;
			// 
			// txtUtilities
			// 
			this.txtUtilities.Height = 0.19625F;
			this.txtUtilities.Left = 1.25F;
			this.txtUtilities.MultiLine = false;
			this.txtUtilities.Name = "txtUtilities";
			this.txtUtilities.Style = "font-family: \'Tahoma\'";
			this.txtUtilities.Tag = "text";
			this.txtUtilities.Text = null;
			this.txtUtilities.Top = 0.625F;
			this.txtUtilities.Width = 2.875F;
			// 
			// txtStreet
			// 
			this.txtStreet.Height = 0.19625F;
			this.txtStreet.Left = 1.25F;
			this.txtStreet.MultiLine = false;
			this.txtStreet.Name = "txtStreet";
			this.txtStreet.Style = "font-family: \'Tahoma\'";
			this.txtStreet.Tag = "text";
			this.txtStreet.Text = null;
			this.txtStreet.Top = 0.78125F;
			this.txtStreet.Width = 2.875F;
			// 
			// txtOpen1
			// 
			this.txtOpen1.Height = 0.19625F;
			this.txtOpen1.Left = 1.25F;
			this.txtOpen1.MultiLine = false;
			this.txtOpen1.Name = "txtOpen1";
			this.txtOpen1.Style = "font-family: \'Tahoma\'";
			this.txtOpen1.Tag = "text";
			this.txtOpen1.Text = null;
			this.txtOpen1.Top = 0.9375F;
			this.txtOpen1.Width = 2.875F;
			// 
			// txtOpen2
			// 
			this.txtOpen2.Height = 0.19625F;
			this.txtOpen2.Left = 1.25F;
			this.txtOpen2.MultiLine = false;
			this.txtOpen2.Name = "txtOpen2";
			this.txtOpen2.Style = "font-family: \'Tahoma\'";
			this.txtOpen2.Tag = "text";
			this.txtOpen2.Text = null;
			this.txtOpen2.Top = 1.09375F;
			this.txtOpen2.Width = 2.875F;
			// 
			// txtReference1
			// 
			this.txtReference1.Height = 0.19625F;
			this.txtReference1.Left = 1.25F;
			this.txtReference1.MultiLine = false;
			this.txtReference1.Name = "txtReference1";
			this.txtReference1.Style = "font-family: \'Tahoma\'";
			this.txtReference1.Tag = "text";
			this.txtReference1.Text = null;
			this.txtReference1.Top = 1.25F;
			this.txtReference1.Width = 2.875F;
			// 
			// TxtReference2
			// 
			this.TxtReference2.Height = 0.19625F;
			this.TxtReference2.Left = 1.25F;
			this.TxtReference2.MultiLine = false;
			this.TxtReference2.Name = "TxtReference2";
			this.TxtReference2.Style = "font-family: \'Tahoma\'";
			this.TxtReference2.Tag = "text";
			this.TxtReference2.Text = null;
			this.TxtReference2.Top = 1.40625F;
			this.TxtReference2.Width = 2.875F;
			// 
			// txtTranLandBldg
			// 
			this.txtTranLandBldg.Height = 0.19625F;
			this.txtTranLandBldg.Left = 1.25F;
			this.txtTranLandBldg.MultiLine = false;
			this.txtTranLandBldg.Name = "txtTranLandBldg";
			this.txtTranLandBldg.Style = "font-family: \'Tahoma\'";
			this.txtTranLandBldg.Tag = "text";
			this.txtTranLandBldg.Text = null;
			this.txtTranLandBldg.Top = 1.5625F;
			this.txtTranLandBldg.Width = 2.875F;
			// 
			// txtXCoord
			// 
			this.txtXCoord.Height = 0.19625F;
			this.txtXCoord.Left = 1.25F;
			this.txtXCoord.MultiLine = false;
			this.txtXCoord.Name = "txtXCoord";
			this.txtXCoord.Style = "font-family: \'Tahoma\'";
			this.txtXCoord.Tag = "text";
			this.txtXCoord.Text = null;
			this.txtXCoord.Top = 1.71875F;
			this.txtXCoord.Width = 0.75F;
			// 
			// txtYCoord
			// 
			this.txtYCoord.Height = 0.19625F;
			this.txtYCoord.Left = 3.3125F;
			this.txtYCoord.MultiLine = false;
			this.txtYCoord.Name = "txtYCoord";
			this.txtYCoord.Style = "font-family: \'Tahoma\'";
			this.txtYCoord.Tag = "text";
			this.txtYCoord.Text = null;
			this.txtYCoord.Top = 1.71875F;
			this.txtYCoord.Width = 0.8125F;
			// 
			// txtExemptions
			// 
			this.txtExemptions.Height = 0.19625F;
			this.txtExemptions.Left = 1.25F;
			this.txtExemptions.MultiLine = false;
			this.txtExemptions.Name = "txtExemptions";
			this.txtExemptions.Style = "font-family: \'Tahoma\'";
			this.txtExemptions.Tag = "text";
			this.txtExemptions.Text = null;
			this.txtExemptions.Top = 1.875F;
			this.txtExemptions.Width = 1F;
			// 
			// txtLandSchedule
			// 
			this.txtLandSchedule.Height = 0.19625F;
			this.txtLandSchedule.Left = 3.625F;
			this.txtLandSchedule.MultiLine = false;
			this.txtLandSchedule.Name = "txtLandSchedule";
			this.txtLandSchedule.Style = "font-family: \'Tahoma\'";
			this.txtLandSchedule.Tag = "text";
			this.txtLandSchedule.Text = null;
			this.txtLandSchedule.Top = 1.875F;
			this.txtLandSchedule.Width = 0.5F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.19625F;
			this.Field1.Left = 4.25F;
			this.Field1.Name = "Field1";
			this.Field1.Text = " ";
			this.Field1.Top = 0.15625F;
			this.Field1.Width = 1.0625F;
			// 
			// txtTreeGrowth
			// 
			this.txtTreeGrowth.Height = 0.19625F;
			this.txtTreeGrowth.HyperLink = null;
			this.txtTreeGrowth.Left = 1.25F;
			this.txtTreeGrowth.MultiLine = false;
			this.txtTreeGrowth.Name = "txtTreeGrowth";
			this.txtTreeGrowth.Style = "font-family: \'Tahoma\'";
			this.txtTreeGrowth.Tag = "text";
			this.txtTreeGrowth.Text = null;
			this.txtTreeGrowth.Top = 0.15625F;
			this.txtTreeGrowth.Width = 2.875F;
			// 
			// txtZoning
			// 
			this.txtZoning.Height = 0.19625F;
			this.txtZoning.HyperLink = null;
			this.txtZoning.Left = 1.25F;
			this.txtZoning.MultiLine = false;
			this.txtZoning.Name = "txtZoning";
			this.txtZoning.Style = "font-family: \'Tahoma\'";
			this.txtZoning.Tag = "text";
			this.txtZoning.Text = null;
			this.txtZoning.Top = 0.3125F;
			this.txtZoning.Width = 2.875F;
			// 
			// srptMisc
			//
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblNeighborhood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTreegrowth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTranLandBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblXCoord)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYCoord)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeighborhood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopography)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilities)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReference1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TxtReference2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTranLandBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtXCoord)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYCoord)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandSchedule)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTreeGrowth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZoning)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNeighborhood;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTreegrowth;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTranLandBldg;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblXCoord;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYCoord;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtNeighborhood;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopography;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilities;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReference1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox TxtReference2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTranLandBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtXCoord;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYCoord;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptions;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandSchedule;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTreeGrowth;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtZoning;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
