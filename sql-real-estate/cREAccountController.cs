﻿using System;
using Global;
using fecherFoundation;

namespace TWRE0000
{
	public class cREAccountController
	{
		//=========================================================
		private string strConnectionName = string.Empty;
		private string strLastError = string.Empty;
		private int lngLastError;

		private void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// VBto upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public bool LoadAccount(ref cREAccount tAccount, int lngAccountNumber)
		{
			bool LoadAccount = false;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				LoadAccount = false;
				if (tAccount == null)
				{
					tAccount = new cREAccount();
				}
				tAccount.Clear();
				if (lngAccountNumber > 0)
				{
					clsDRWrapper rsLoad = new clsDRWrapper();
					int x;
					rsLoad.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAccountNumber) + " and rscard = 1", modGlobalVariables.strREDatabase);
					if (!rsLoad.EndOfFile())
					{
						tAccount.Account = lngAccountNumber;
						tAccount.ID = rsLoad.Get_Fields_Int32("id");
						tAccount.AccountID = rsLoad.Get_Fields_String("AccountID");
						if (!FCUtils.IsNull(rsLoad.Get_Fields_Boolean("inbankruptcy")))
						{
							tAccount.InBankruptcy = rsLoad.Get_Fields_Boolean("InBankruptcy");
						}
						if (!FCUtils.IsNull(rsLoad.Get_Fields_Boolean("TaxAcquired")))
						{
							tAccount.TaxAcquired = rsLoad.Get_Fields_Boolean("TaxAcquired");
						}
						if (!FCUtils.IsNull(rsLoad.Get_Fields_Boolean("revocabletrust")))
						{
							tAccount.RevocableTrust = rsLoad.Get_Fields_Boolean("revocabletrust");
						}
						for (x = 1; x <= 3; x++)
						{
							// TODO Get_Fields: Field [riexemptcd] not found!! (maybe it is an alias?)
							if (Conversion.Val(rsLoad.Get_Fields("riexemptcd" + FCConvert.ToString(x))) > 0)
							{
								// TODO Get_Fields: Field [riexemptcd] not found!! (maybe it is an alias?)
								tAccount.Set_ExemptCode(x, rsLoad.Get_Fields("riexemptcd" + FCConvert.ToString(x)));
								// TODO Get_Fields: Field [ExemptPct] not found!! (maybe it is an alias?)
								tAccount.Set_ExemptPct(x, Conversion.Val(rsLoad.Get_Fields("ExemptPct" + FCConvert.ToString(x))));
								// TODO Get_Fields: Field [ExemptVal] not found!! (maybe it is an alias?)
								tAccount.Set_ExemptVal(x, Conversion.Val(rsLoad.Get_Fields("ExemptVal" + FCConvert.ToString(x))));
							}
						}
						// x
						tAccount.MapLot = rsLoad.Get_Fields_String("rsmaplot");
						tAccount.OwnerID = rsLoad.Get_Fields_Int32("OwnerPartyID");
						tAccount.SecOwnerID = rsLoad.Get_Fields_Int32("SecOwnerPartyID");
						tAccount.TranCode = rsLoad.Get_Fields_Int32("ritrancode");
						tAccount.PreviousOwner = rsLoad.Get_Fields_String("rspreviousmaster");
                        tAccount.DeedName1 = rsLoad.Get_Fields_String("DeedName1");
                        tAccount.DeedName2 = rsLoad.Get_Fields_String("DeedName2");
						tAccount.SaleFinancing = rsLoad.Get_Fields_Int32("pisalefinancing");
						tAccount.SaleType = rsLoad.Get_Fields_Int32("pisaletype");
						tAccount.SaleValidity = rsLoad.Get_Fields_Int32("pisalevalidity");
						tAccount.SaleVerified = rsLoad.Get_Fields_Int32("pisaleverified");
						tAccount.SalePrice = rsLoad.Get_Fields_Int32("pisaleprice");
						if (Information.IsDate(rsLoad.Get_Fields("saledate")))
						{
							if (rsLoad.Get_Fields_DateTime("saledate").ToOADate() != 0)
							{
								tAccount.SaleDate = rsLoad.Get_Fields_DateTime("saledate");
							}
						}
						if (tAccount.OwnerID > 0 || tAccount.SecOwnerID > 0)
						{
							cPartyController tCont = new cPartyController();
							if (tAccount.OwnerID > 0)
							{
								cParty tParty = tAccount.OwnerParty;
								tCont.LoadParty(ref tParty, tAccount.OwnerID);
							}
							if (tAccount.SecOwnerID > 0)
							{
								cParty tParty = tAccount.SecOwnerParty;
								tCont.LoadParty(ref tParty, tAccount.SecOwnerID);
							}
						}
					}
				}
				LoadAccount = true;
				return LoadAccount;
			}
			catch
			{
				// ErrorHandler:
				lngLastError = Information.Err().Number;
				strLastError = Information.Err().Description;
			}
			return LoadAccount;
		}

		public object SaveAccount(cREAccount tAccount)
		{
			object SaveAccount = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from master where rsaccount = " + tAccount.Account, strConnectionName);
				while (!rsSave.EndOfFile())
				{
					rsSave.Set_Fields("OwnerPartyID", tAccount.OwnerID);
					rsSave.Set_Fields("SecOwnerPartyID", tAccount.SecOwnerID);
					rsSave.Set_Fields("RSMaplot", tAccount.MapLot);
					rsSave.Set_Fields("RevocableTrust", tAccount.RevocableTrust);
					rsSave.Set_Fields("InBankruptcy", tAccount.InBankruptcy);
					rsSave.Set_Fields("TaxAcquired", tAccount.TaxAcquired);
					rsSave.Set_Fields("AccountID", tAccount.AccountID);
					rsSave.Set_Fields("ritrancode", tAccount.TranCode);
					rsSave.Set_Fields("rspreviousmaster", tAccount.PreviousOwner);
                    rsSave.Set_Fields("DeedName1",tAccount.DeedName1);
                    rsSave.Set_Fields("DeedName2",tAccount.DeedName2);
					if (Information.IsDate(tAccount.SaleDate))
					{
						rsSave.Set_Fields("SaleDate", tAccount.SaleDate);
					}
					rsSave.Set_Fields("pisalefinancing", tAccount.SaleFinancing);
					rsSave.Set_Fields("pisaletype", tAccount.SaleType);
					rsSave.Set_Fields("pisalevalidity", tAccount.SaleValidity);
					rsSave.Set_Fields("pisaleverified", tAccount.SaleVerified);
					rsSave.Set_Fields("piSalePrice", tAccount.SalePrice);
					if (FCConvert.ToInt32(rsSave.Get_Fields_Int32("Rscard")) == 1)
					{
						int x;
						for (x = 1; x <= 3; x++)
						{
							rsSave.Set_Fields("riexemptcd" + x, tAccount.Get_ExemptCode(x));
							rsSave.Set_Fields("ExemptPct" + x, tAccount.Get_ExemptPct(x));
							rsSave.Set_Fields("ExemptVal" + x, tAccount.Get_ExemptVal(x));
						}
						// x
					}
					rsSave.Update();
					rsSave.MoveNext();
				}
				return SaveAccount;
			}
			catch
			{
				// ErrorHandler:
				lngLastError = Information.Err().Number;
				strLastError = Information.Err().Description;
			}
			return SaveAccount;
		}

		public cREAccountController() : base()
		{
			strConnectionName = "RealEstate";
		}

		public string ConnectionName
		{
			set
			{
				strConnectionName = value;
			}
			get
			{
				string ConnectionName = "";
				ConnectionName = strConnectionName;
				return ConnectionName;
			}
		}

		public cREFullAccount LoadREFullAccountByAccount(int lngAccount)
		{
			cREFullAccount LoadREFullAccountByAccount = null;
			ClearErrors();
			try
			{
				cREFullAccount returnAccount = null;
				clsDRWrapper rsLoad = new clsDRWrapper();
				LoadREFullAccountByAccount = null;
				rsLoad.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = 1", strConnectionName);
				if (!rsLoad.EndOfFile())
				{
					FillREFullAccount(returnAccount, rsLoad);
					FillCards(returnAccount);
					returnAccount.IsUpdated = false;
					LoadREFullAccountByAccount = returnAccount;
				}
				return LoadREFullAccountByAccount;
			}
			catch (Exception ex)
			{
				//ErrorHandler: ;
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
				return LoadREFullAccountByAccount;
			}
		}

		public cREFullAccount LoadREFullAccountWithPartiesByAccount(int lngAccount)
		{
			cREFullAccount LoadREFullAccountWithPartiesByAccount = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				return LoadREFullAccountWithPartiesByAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return LoadREFullAccountWithPartiesByAccount;
		}

		private void FillREFullAccount(cREFullAccount rAccount, clsDRWrapper rs)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!(rAccount == null))
				{
					if (!(rs == null))
					{
						int x;
						rAccount.AccountID = rs.Get_Fields_String("AccountID");
						rAccount.AccountNumber = rs.Get_Fields_Int32("rsaccount");
						rAccount.InBankruptcy = rs.Get_Fields_Boolean("InBankruptcy");
						rAccount.RevocableTrust = rs.Get_Fields_Boolean("RevocableTrust");
						rAccount.MapLot = rs.Get_Fields_String("rsmaplot");
						rAccount.OwnerID = rs.Get_Fields_Int32("OwnerPartyID");
						rAccount.SecondOwnerID = rs.Get_Fields_Int32("SecOwnerPartyID");
						rAccount.TaxAcquired = rs.Get_Fields_Boolean("taxacquired");
						rAccount.IsInactive = rs.Get_Fields_Boolean("rsdeleted");
						rAccount.TranCode = rs.Get_Fields_Int32("ritrancode");
						rAccount.SaleInformation.FinancingCode = FCConvert.ToInt16(rs.Get_Fields_Int32("pisalefinancing"));
						rAccount.SaleInformation.Price = Conversion.Val(rs.Get_Fields_Int32("pisaleprice"));
						rAccount.SaleInformation.SaleTypeCode = FCConvert.ToInt16(rs.Get_Fields_Int32("pisaletype"));
						rAccount.SaleInformation.ValidityCode = FCConvert.ToInt16(rs.Get_Fields_Int32("pisalevalidity"));
						rAccount.SaleInformation.VerificationCode = FCConvert.ToInt16(rs.Get_Fields_Int32("pisaleverified"));
						if (Information.IsDate(rs.Get_Fields("saledate")))
						{
							if (rs.Get_Fields_DateTime("Saledate").ToOADate() != 0)
							{
								rAccount.SaleInformation.SaleDate = FCConvert.ToString(rs.Get_Fields_DateTime("saledate"));
							}
							else
							{
								rAccount.SaleInformation.SaleDate = "";
							}
						}
						else
						{
							rAccount.SaleInformation.SaleDate = "";
						}
						cREPropertyExemption propExempt;
						rAccount.Exemptions.ClearList();
						for (x = 1; x <= 3; x++)
						{
							// TODO Get_Fields: Field [riexemptcd] not found!! (maybe it is an alias?)
							if (rs.Get_Fields("riexemptcd" + FCConvert.ToString(x)) > 0)
							{
								propExempt = new cREPropertyExemption();
								// TODO Get_Fields: Field [riexemptcd] not found!! (maybe it is an alias?)
								propExempt = rs.Get_Fields("riexemptcd" + FCConvert.ToString(x));
								// TODO Get_Fields: Field [ExemptVal] not found!! (maybe it is an alias?)
								propExempt.ExemptValue = Conversion.Val(rs.Get_Fields("ExemptVal" + FCConvert.ToString(x)));
								// TODO Get_Fields: Field [ExemptPct] not found!! (maybe it is an alias?)
								propExempt.Percentage = Conversion.Val(rs.Get_Fields("ExemptPct" + FCConvert.ToString(x)));
								rAccount.Exemptions.AddItem(propExempt);
							}
						}
						// x
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		private void FillCards(cREFullAccount rAccount)
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset("select * from master where rsaccount = " + rAccount.AccountNumber + " order by rscard");
				cRECard rCard;
				rAccount.Cards.ClearList();
				short x = 0;
				cRELandItem lItem;
				while (!rs.EndOfFile())
				{
					rCard = new cRECard();
					rCard.BillingValues.Building = Conversion.Val(rs.Get_Fields_Int32("LastBldgVal"));
					rCard.BillingValues.Land = Conversion.Val(rs.Get_Fields_Int32("LastLandVal"));
					rCard.BillingValues.Exemption = Conversion.Val(rs.Get_Fields_Int32("rlexemption"));
					rCard.Land.ClearList();
					for (x = 1; x <= 7; x++)
					{
						lItem = new cRELandItem();
						// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
						lItem.Influence = Conversion.Val(rs.Get_Fields("piland" + FCConvert.ToString(x) + "Inf"));
						// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
						lItem.InfluenceCode = FCConvert.ToInt32(Conversion.Val(rs.Get_Fields("piland" + FCConvert.ToString(x) + "InfCode")));
						// TODO Get_Fields: Field [PIland] not found!! (maybe it is an alias?)
						lItem.LandCode = FCConvert.ToInt32(Conversion.Val(rs.Get_Fields("PIland" + FCConvert.ToString(x) + "Type")));
						lItem.LandNumber = x;
						// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
						lItem.UnitsA = FCConvert.ToString(rs.Get_Fields("piland" + FCConvert.ToString(x) + "UnitsA"));
						// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
						lItem.UnitsB = FCConvert.ToString(rs.Get_Fields("piland" + FCConvert.ToString(x) + "UnitsB"));
						lItem.IsUpdated = false;
						rCard.Land.AddItem(lItem);
					}
					// x
					rCard.IsUpdated = false;
					rAccount.Cards.AddItem(rCard);
					rs.MoveNext();
				}
				return;
			}
			catch
			{
				// ErrorHandler:
				SetError(Information.Err().Number, Information.Err().Description);
			}
		}

		private void SetError(int lngError, string strError)
		{
			strLastError = strError;
			lngLastError = lngError;
		}
	}
}
