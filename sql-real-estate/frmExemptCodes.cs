﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExemptCodes.
	/// </summary>
	public partial class frmExemptCodes : BaseForm
	{
		public frmExemptCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExemptCodes InstancePtr
		{
			get
			{
				return (frmExemptCodes)Sys.GetInstance(typeof(frmExemptCodes));
			}
		}

		protected frmExemptCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const int CNSTCOLAUTOID = 0;
		const int CNSTCOLCODE = 1;
		const int CNSTCOLAMOUNT = 2;
		const int CNSTCOLDESCRIPTION = 3;
		const int CNSTCOLSHORTDESCRIPTION = 4;
		const int CNSTCOLCATEGORY = 5;
		const int CNSTCOLENLISTED = 6;
		const int CNSTCOLAPPLYTO = 7;
		bool boolDataChanged;

		private class ExemptCat
		{
			public int Code;
			public string Title;
			public bool Active;
		}

		private List<ExemptCat> exemptCats = new List<ExemptCat>();

		private void frmExemptCodes_Activated(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void frmExemptCodes_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmExemptCodes_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			SetupExemptCats();
			SetupGrid();
			FillGrid();
			boolDataChanged = false;
		}

		private void FillGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int intTemp = 0;
			string strTemp = "";

			clsLoad.OpenRecordset("select * from exemptcode order by code", modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
				Grid.AddItem(clsLoad.Get_Fields_Int32("id") + "\t" + clsLoad.Get_Fields("code") + "\t" + clsLoad.Get_Fields("amount") + "\t" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_String("shortdescription"));
				Grid.RowData(Grid.Rows - 1, false);
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("category"))));
				Grid.Cell(FCGrid.CellPropertySettings.flexcpData, Grid.Rows - 1, CNSTCOLCATEGORY, intTemp);
				var tempCat = exemptCats.FirstOrDefault(x => x.Code == intTemp);
				strTemp = tempCat != null ? tempCat.Title : "NONE";
				Grid.TextMatrix(Grid.Rows - 1, CNSTCOLCATEGORY, strTemp);
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("enlisted"))));
				Grid.Cell(FCGrid.CellPropertySettings.flexcpData, Grid.Rows - 1, CNSTCOLENLISTED, intTemp);
				switch (intTemp)
				{
					case modcalcexemptions.CNSTEXEMPTENLISTEDMAINE:
						{
							strTemp = "Maine Enlisted";
							break;
						}
					case modcalcexemptions.CNSTEXEMPTENLISTEDNONMAINE:
						{
							strTemp = "Non-Maine Enlisted";
							break;
						}
					default:
						{
							strTemp = "";
							break;
						}
				}
				//end switch
				Grid.TextMatrix(Grid.Rows - 1, CNSTCOLENLISTED, strTemp);
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("applyto"))));
				Grid.Cell(FCGrid.CellPropertySettings.flexcpData, Grid.Rows - 1, CNSTCOLAPPLYTO, intTemp);
				switch (intTemp)
				{
					case modcalcexemptions.CNSTEXEMPTAPPLYTOBLDGFIRST:
						{
							strTemp = "Bldg then Land";
							break;
						}
					case modcalcexemptions.CNSTEXEMPTAPPLYTOLANDFIRST:
						{
							strTemp = "Land then Bldg";
							break;
						}
					default:
						{
							// default to bldgfirst
							strTemp = "Bldg then Land";
							break;
						}
				}
				//end switch
				Grid.TextMatrix(Grid.Rows - 1, CNSTCOLAPPLYTO, strTemp);
				clsLoad.MoveNext();
			}
		}

		private void mnuContinue_Click()
		{
			this.Unload();
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				if (MessageBox.Show("Data has changed. Do you wish to save first?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveData())
						e.Cancel = true;
				}
			}
		}

		private void frmExemptCodes_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			Grid.RowData(Grid.Row, true);
			switch (Grid.Col)
			{
				case CNSTCOLCATEGORY:
					{
						if ((Strings.Trim(Strings.UCase(Grid.TextMatrix(Grid.Row, CNSTCOLCATEGORY))) == "NONE") || (Strings.Trim(Strings.UCase(Grid.TextMatrix(Grid.Row, CNSTCOLCATEGORY))) == "HOMESTEAD") || (Strings.Trim(Strings.UCase(Grid.TextMatrix(Grid.Row, CNSTCOLCATEGORY))) == ""))
						{
							if (Strings.Trim(Grid.TextMatrix(Grid.Row, CNSTCOLENLISTED)) != string.Empty)
							{
								// mark this as a change
								Grid.RowData(Grid.Row, true);
							}
							Grid.TextMatrix(Grid.Row, CNSTCOLENLISTED, "");
						}
						break;
					}
			}
			//end switch
			boolDataChanged = true;
		}

		private void Grid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			switch (Grid.Col)
			{
				case CNSTCOLENLISTED:
					{
						// If UCase(Grid.TextMatrix(Row, CNSTCOLCATEGORY)) <> "NONE" And UCase(Grid.TextMatrix(Row, CNSTCOLCATEGORY)) <> "HOMESTEAD" And UCase(Grid.TextMatrix(Row, CNSTCOLCATEGORY)) <> "" And Val(GetCategoryCode(Grid.TextMatrix(Row, CNSTCOLCATEGORY))) <= CNSTEXEMPTCATMAXVETCODE Then
						if (!IsAVeteranExempt_2(FCConvert.ToString(GetCategoryCode(Grid.TextMatrix(Grid.Row, CNSTCOLCATEGORY)))))
						{
							// is not a veteran exemption
							if (Strings.Trim(Grid.TextMatrix(Grid.Row, Grid.Col)) != string.Empty)
							{
								// mark this as a change
								Grid.RowData(Grid.Row, true);
							}
							Grid.TextMatrix(Grid.Row, Grid.Col, "");
							e.Cancel = true;
						}
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
		}
		// vbPorter upgrade warning: lngCode As string	OnWriteFCConvert.ToInt16(
		private bool IsAVeteranExempt_2(string lngCode)
		{
			return IsAVeteranExempt(ref lngCode);
		}

		private bool IsAVeteranExempt(ref string lngCode)
		{
			bool IsAVeteranExempt = false;
			IsAVeteranExempt = false;
			switch (FCConvert.ToInt32(lngCode))
			{
				case modcalcexemptions.CNSTEXEMPTCATDISABLEDVET:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATKOREAN:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATPARAPLEGICVET:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATVETERAN:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATVIETNAM:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATWORLDWARI:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATWORLDWARII:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATWWIIWIDOWER:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATWWIWIDOWER:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATVETCOOPHOUSING:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATLEBANONPANAMA:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATVETMALESURVIVOR:
					{
						IsAVeteranExempt = true;
						break;
					}
				case modcalcexemptions.CNSTEXEMPTCATVETEARLYVIETNAM:
				{
					IsAVeteranExempt = true;
					break;
				}
			}
			//end switch
			return IsAVeteranExempt;
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						mnuDeleteCode_Click();
						break;
					}
			}
			//end switch
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = Grid.GetFlexRowIndex(e.RowIndex);
			int col = Grid.GetFlexColIndex(e.ColumnIndex);
			Grid.RowData(row, true);
			boolDataChanged = true;
			switch (col)
			{
				case CNSTCOLCATEGORY:
					{
						if ((Strings.UCase(Grid.EditText) == "HOMESTEAD") || (Strings.UCase(Grid.EditText) == "NONE") || (Strings.UCase(Grid.EditText) == ""))
						{
							Grid.TextMatrix(row, CNSTCOLENLISTED, "");
							Grid.Cell(FCGrid.CellPropertySettings.flexcpData, row, CNSTCOLENLISTED, 0);
						}
						else
						{
						}
						break;
					}
			}
			//end switch
		}

		private void mnuAddCode_Click(object sender, System.EventArgs e)
		{
			Grid.Rows += 1;
			Grid.TextMatrix(Grid.Rows - 1, CNSTCOLCATEGORY, "None");
			Grid.TextMatrix(Grid.Rows - 1, CNSTCOLAPPLYTO, "Bldg then Land");
			Grid.TopRow = Grid.Rows - 1;
		}

		private void mnuDeleteCode_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			// check to see if it's used first
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngCode;
			lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTCOLCODE))));
			if (lngCode > 0)
			{
				clsLoad.OpenRecordset("select rsaccount from master where not rsdeleted = 1 and (riexemptcd1 = " + FCConvert.ToString(lngCode) + " or riexemptcd2 = " + FCConvert.ToString(lngCode) + " or riexemptcd3 = " + FCConvert.ToString(lngCode) + ")", modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					MessageBox.Show("There are still accounts using exempt code " + FCConvert.ToString(lngCode) + "\r\n" + "Cannot delete code", "Code In Use", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			GridDelete.AddItem(Grid.TextMatrix(Grid.Row, CNSTCOLAUTOID));
			Grid.RemoveItem(Grid.Row);
		}

		public void mnuDeleteCode_Click()
		{
			mnuDeleteCode_Click(cmdDeleteCode, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrid()
		{
			string strList = "";
			Grid.Cols = 8;
			Grid.ColHidden(CNSTCOLAUTOID, true);
			Grid.TextMatrix(0, CNSTCOLCODE, "Code");
			Grid.TextMatrix(0, CNSTCOLAMOUNT, "Amount");
			Grid.TextMatrix(0, CNSTCOLDESCRIPTION, "Description");
			Grid.TextMatrix(0, CNSTCOLSHORTDESCRIPTION, "Short Desc");
			Grid.TextMatrix(0, CNSTCOLCATEGORY, "Category");
			Grid.TextMatrix(0, CNSTCOLENLISTED, "Enlisted");
			Grid.TextMatrix(0, CNSTCOLAPPLYTO, "Apply To");
			strList = GetExemptCatList();
			Grid.ColComboList(CNSTCOLCATEGORY, strList);
			strList = "Maine Enlisted|Non-Maine Enlisted";
			Grid.ColComboList(CNSTCOLENLISTED, strList);
			strList = "Bldg then Land|Land then Bldg";
			Grid.ColComboList(CNSTCOLAPPLYTO, strList);
            //FC:FINAL:CHN - issue 1672: Form redesign.
            Grid.ColAlignment(CNSTCOLCODE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(CNSTCOLAMOUNT, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTCOLCODE, FCConvert.ToInt32(0.06 * GridWidth));
			Grid.ColWidth(CNSTCOLAMOUNT, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTCOLDESCRIPTION, FCConvert.ToInt32(0.22 * GridWidth));
			Grid.ColWidth(CNSTCOLSHORTDESCRIPTION, FCConvert.ToInt32(0.12 * GridWidth));
			Grid.ColWidth(CNSTCOLCATEGORY, FCConvert.ToInt32(0.22 * GridWidth));
			Grid.ColWidth(CNSTCOLENLISTED, FCConvert.ToInt32(0.12 * GridWidth));
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			rptExemptCodes.InstancePtr.Init();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			Grid.Col = 0;
			//Application.DoEvents();
			SaveData();
		}

		private bool SaveData()
		{
			bool SaveData = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			int x;
			int lngTemp = 0;
			string strTemp1 = "";
			string strTemp2 = "";
			int intCategory = 0;
			int intApplyTo = 0;
			int intEnlisted = 0;
			try
			{
				SaveData = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// get rid of deleteds first
				for (x = 0; x <= GridDelete.Rows - 1; x++)
				{
					clsSave.Execute("delete from exemptcode where id = " + GridDelete.TextMatrix(x, 0), modGlobalVariables.strREDatabase);
				}
				// x
				GridDelete.Rows = 0;
				// CHECK for duplicate codes
				if (Grid.Rows > 1)
				{
					for (x = 1; x <= Grid.Rows - 2; x++)
					{
						lngTemp = Grid.FindRow(Grid.TextMatrix(x, CNSTCOLCODE), x + 1, CNSTCOLCODE);
						if (lngTemp >= 0)
						{
							MessageBox.Show("Cannot save because you have two exemptions with the same code", "Duplicate Codes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							return SaveData;
						}
					}
					// x
				}
				// now save the changed ones
				lngTemp = Grid.FindRow(true, 1);
				while (lngTemp > 0)
				{
					strTemp1 = Grid.TextMatrix(lngTemp, CNSTCOLDESCRIPTION);
					strTemp2 = Grid.TextMatrix(lngTemp, CNSTCOLSHORTDESCRIPTION);
					intCategory = GetCategoryCode(Grid.TextMatrix(lngTemp, CNSTCOLCATEGORY));
					intApplyTo = GetApplyToCode(Grid.TextMatrix(lngTemp, CNSTCOLAPPLYTO));
					intEnlisted = GetEnlistedCode(Grid.TextMatrix(lngTemp, CNSTCOLENLISTED));
					if (Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLCODE)) <= 0)
					{
						MessageBox.Show("Code " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLCODE))) + " is not valid" + "\r\n" + "All exempt codes must be greater than 0", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return SaveData;
					}
					if (Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLAUTOID)) > 0)
					{
						modGlobalRoutines.escapequote(ref strTemp1);
						modGlobalRoutines.escapequote(ref strTemp2);
						modGlobalVariables.Statics.strSQL = "amount = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLAMOUNT))) + "";
						modGlobalVariables.Statics.strSQL += ",description = '" + strTemp1 + "'";
						modGlobalVariables.Statics.strSQL += ",shortdescription = '" + strTemp2 + "'";
						modGlobalVariables.Statics.strSQL += ",code = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLCODE)));
						modGlobalVariables.Statics.strSQL += ",category = " + FCConvert.ToString(intCategory);
						modGlobalVariables.Statics.strSQL += ",applyto = " + FCConvert.ToString(intApplyTo);
						modGlobalVariables.Statics.strSQL += ",enlisted = " + FCConvert.ToString(intEnlisted);
						clsSave.Execute("update exemptcode set " + modGlobalVariables.Statics.strSQL + " where id = " + Grid.TextMatrix(lngTemp, CNSTCOLAUTOID), modGlobalVariables.strREDatabase);
					}
					else
					{
						// new
						clsSave.OpenRecordset("select * from exemptcode where id = -1", modGlobalVariables.strREDatabase);
						clsSave.AddNew();
						Grid.TextMatrix(lngTemp, CNSTCOLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("id")));
						clsSave.Set_Fields("code", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLCODE))));
						clsSave.Set_Fields("amount", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLAMOUNT))));
						clsSave.Set_Fields("description", strTemp1);
						clsSave.Set_Fields("shortdescription", strTemp2);
						clsSave.Set_Fields("category", intCategory);
						clsSave.Set_Fields("applyto", intApplyTo);
						clsSave.Set_Fields("enlisted", intEnlisted);
						clsSave.Update();
					}
					Grid.RowData(lngTemp, false);
					lngTemp = Grid.FindRow(true, lngTemp + 1);
				}
				boolDataChanged = false;
				SaveData = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				//ErrorHandler:;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetCategoryCode(string strDescription)
		{
			ExemptCat tmpCat = exemptCats.FirstOrDefault(x => x.Title.ToUpper() == strDescription.ToUpper());
			short tmpCode = tmpCat != null ? Convert.ToInt16(tmpCat.Code) : Convert.ToInt16(modcalcexemptions.CNSTEXEMPTCATNONE);
			return tmpCode;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetEnlistedCode(string strDescription)
		{
			short GetEnlistedCode = 0;
			if (Strings.UCase(strDescription) == "MAINE ENLISTED")
			{
				GetEnlistedCode = modcalcexemptions.CNSTEXEMPTENLISTEDMAINE;
			}
			else if (Strings.UCase(strDescription) == "NON-MAINE ENLISTED")
			{
				GetEnlistedCode = modcalcexemptions.CNSTEXEMPTENLISTEDNONMAINE;
			}
			else
			{
				GetEnlistedCode = 0;
			}
			return GetEnlistedCode;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetApplyToCode(string strDescription)
		{
			short GetApplyToCode = 0;
			if (Strings.UCase(strDescription) == "BLDG THEN LAND")
			{
				GetApplyToCode = modcalcexemptions.CNSTEXEMPTAPPLYTOBLDGFIRST;
			}
			else if (Strings.UCase(strDescription) == "LAND THEN BLDG")
			{
				GetApplyToCode = modcalcexemptions.CNSTEXEMPTAPPLYTOLANDFIRST;
			}
			else
			{
				GetApplyToCode = modcalcexemptions.CNSTEXEMPTAPPLYTOBLDGFIRST;
			}
			return GetApplyToCode;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			Grid.Col = 0;
			//Application.DoEvents();
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private void SetupExemptCats()
		{
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATNONE,
				Title = "NONE",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATHOMESTEAD,
				Title = "Homestead",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATDISABLEDVET,
				Title = "Disabled Vet",
				Active = true
			});
			exemptCats.Add(new ExemptCat
				{
					Code = modcalcexemptions.CNSTEXEMPTCATKOREAN,
					Title = "Korean",
					Active = true
				});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATPARAPLEGICVET,
				Title = "Paraplegic Vet",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATVETERAN,
				Title = "Veteran (General)",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATVIETNAM,
				Title = "Vietnam",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATVETEARLYVIETNAM,
				Title = "Early Vietnam(Feb 27, 1961 - Aug 5, 1964)",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATWORLDWARI,
				Title = "World War I",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATWORLDWARII,
				Title = "World War II",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATUNITEDSTATES,
				Title = "United States",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATSTATE,
				Title = "State of Maine",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATBLIND,
				Title = "Blind",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATSCIANDLIT,
				Title = "Literary/Scientific",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATBENEVCHARITABLE,
				Title = "Benevolent/Charitable",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATPARSONAGE,
				Title = "Parsonage",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATRELIGIOUS,
				Title = "Religious",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATVETERANSORG,
				Title = "Veterans Organization",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATCHAMBERCOMMERCE,
				Title = "Chamber of Comm/Board of Trade",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATFRATERNAL,
				Title = "Fraternal Organization",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATMUNIAIRPORT,
				Title = "Municipal Airport",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATMUNICOUNTY,
				Title = "Municipal/County",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATQUASIMUNICIPAL,
				Title = "Quasi-Municipal",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATANIMALWASTE,
				Title = "Animal Waste Facility",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATPOLLUTIONCONTROL,
				Title = "Pollution Control",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATSEWAGEDISPOSAL,
				Title = "Sewage Disposal",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATWATERPIPES,
				Title = "Water Supply",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATNHWATER,
				Title = "NH Water Board",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATHOSPITALLEASED,
				Title = "Hospital Leased",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATPRIVATEAIRPORT,
				Title = "Private Airport",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATWWIWIDOWER,
				Title = "World War I Widower",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATWWIIWIDOWER,
				Title = "World War II Widower",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATHYDROOUTSIDEMUNI,
				Title = "Hydro Outside Muni Corporation",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATLEBANONPANAMA,
				Title = "Veteran Lebanon/Panama",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATVETCOOPHOUSING,
				Title = "Cooperative Housing Corporation Veteran",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATVETMALESURVIVOR,
				Title = "Male Survivor of Deceased Veteran",
				Active = true
			});
			exemptCats.Add(new ExemptCat
			{
				Code = modcalcexemptions.CNSTEXEMPTCATSOLARANDWIND,
				Title = "Solar and Wind Energy Equipment",
				Active = true
			});
		}

		private string GetExemptCatList()
		{
			string strTemp = "";
			foreach (ExemptCat tmpCat in exemptCats)
			{
				if (strTemp != "")
				{
					strTemp = strTemp + "|";
				}
				strTemp += tmpCat.Title;
			}
			return strTemp;
		}

	}
}
