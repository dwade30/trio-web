﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptListingNameAddrLocMapAsses.
	/// </summary>
	public partial class rptListingNameAddrLocMapAsses : BaseSectionReport
	{
		public static rptListingNameAddrLocMapAsses InstancePtr
		{
			get
			{
				return (rptListingNameAddrLocMapAsses)Sys.GetInstance(typeof(rptListingNameAddrLocMapAsses));
			}
		}

		protected rptListingNameAddrLocMapAsses _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTemp?.Dispose();
                rsTemp = null;
				clsTemp?.Dispose();
                clsTemp = null;

            }
			base.Dispose(disposing);
		}

		public rptListingNameAddrLocMapAsses()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Name Address Location Map/Lot Assessment";
		}
		// nObj = 1
		//   0	rptListingNameAddrLocMapAsses	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsTemp = new clsDRWrapper();
		// Dim dbTemp As DAO.Database
		object varAccount;
		int intPage;
		clsDRWrapper clsTemp = new clsDRWrapper();
		bool boolByCard;
		string strland = "";
		string strbldg = "";
		string strExempt = "";
		double dblTotalLand;
		double dblTotalBldg;
		double dblTotalExempt;
		double dblTotalTotal;
		int lngCount;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string strTemp = "";
			string strSQL = "";
			string strCountry;
			nextaccount:
			;
			if (rsTemp.EndOfFile())
				return;
			strCountry = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("country")));
			if (Strings.LCase(strCountry) == "united states")
			{
				strCountry = "";
			}
			txtAccount.Text = Strings.Format(rsTemp.Get_Fields_Int32("RSAccount"), "00000");
			txtCard.Text = Strings.Format(rsTemp.Get_Fields_Int32("RSCard"), "000");
			txtName.Text = Strings.Trim(rsTemp.Get_Fields_String("RSName") + " ");
			if (Information.IsNumeric(rsTemp.Get_Fields_String("rslocnumalph") + ""))
			{
				strTemp = Conversion.Str(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_String("rslocnumalph") + "")));
				if (Conversion.Val(rsTemp.Get_Fields_String("rslocnumalph") + "") == 0)
					strTemp = " ";
			}
			else
			{
				strTemp = rsTemp.Get_Fields_String("rslocnumalph") + "";
			}
			strTemp += " " + rsTemp.Get_Fields_String("rslocapt") + "";
			txtLocation.Text = Strings.Trim(strTemp + " " + rsTemp.Get_Fields_String("RSLOCStreet") + " ");
			txtMap.Text = Strings.Trim(rsTemp.Get_Fields_String("RSMAPLOT") + " ");
			txtSecondOwner.Text = Strings.Trim(rsTemp.Get_Fields_String("rssecowner") + "");
			txtAddress.Text = Strings.Trim(rsTemp.Get_Fields_String("RSAddr1") + " ");
			txtaddress2.Text = Strings.Trim(rsTemp.Get_Fields_String("rsaddr2") + "");
			txtaddress3.Text = Strings.Trim(Strings.Trim(rsTemp.Get_Fields_String("rsaddr3") + "") + " " + rsTemp.Get_Fields_String("rsstate") + " " + rsTemp.Get_Fields_String("rszip") + " " + rsTemp.Get_Fields_String("rszip4"));
			if (Strings.Trim(txtaddress2.Text) == string.Empty)
			{
				txtaddress2.Text = txtaddress3.Text;
				txtaddress3.Text = "";
			}
			if (Strings.Trim(txtAddress.Text) == string.Empty)
			{
				txtAddress.Text = txtaddress2.Text;
				txtaddress2.Text = txtaddress3.Text;
				txtaddress3.Text = "";
			}
			if (Strings.Trim(txtSecondOwner.Text) == string.Empty)
			{
				txtSecondOwner.Text = txtAddress.Text;
				txtAddress.Text = txtaddress2.Text;
				txtaddress2.Text = txtaddress3.Text;
				txtaddress3.Text = "";
			}
			if (txtaddress2.Text == "")
			{
				txtaddress2.Text = strCountry;
			}
			else if (txtaddress3.Text == "")
			{
				txtaddress3.Text = strCountry;
			}
			if (Strings.Trim(txtLocation.Text) == string.Empty)
			{
				txtLocation.Text = txtMap.Text;
				txtMap.Text = "";
			}
			if (Strings.Trim(txtaddress3.Text) == string.Empty)
			{
				txtaddress3.Text = txtLocation.Text;
				txtLocation.Text = txtMap.Text;
				txtMap.Text = "";
			}
			if (Strings.Trim(txtaddress2.Text) == string.Empty)
			{
				txtaddress2.Text = txtaddress3.Text;
				txtaddress3.Text = txtLocation.Text;
				txtLocation.Text = txtMap.Text;
				txtMap.Text = "";
			}
			if (Strings.Trim(txtAddress.Text) == string.Empty)
			{
				txtAddress.Text = txtaddress2.Text;
				txtaddress2.Text = txtaddress3.Text;
				txtaddress3.Text = txtLocation.Text;
				txtLocation.Text = txtMap.Text;
				txtMap.Text = "";
			}
			if (Strings.Trim(txtSecondOwner.Text) == string.Empty)
			{
				txtSecondOwner.Text = txtAddress.Text;
				txtAddress.Text = txtaddress2.Text;
				txtaddress2.Text = txtaddress3.Text;
				txtaddress3.Text = txtLocation.Text;
				txtLocation.Text = txtMap.Text;
				txtMap.Text = "";
			}
			if (boolByCard)
			{
				if (modGlobalVariables.Statics.boolBillingorCorrelated)
				{
					txtLand.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("lastLANDVAL") + ""), "##,###,##0");
					txtBuilding.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("lastBLDGVAL") + ""), "##,###,##0");
					txtExemption.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("rlEXEMPTION") + ""), "##,###,##0");
					txtTotal.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("lastLANDVAL") + "") + Conversion.Val(rsTemp.Get_Fields_Int32("lastBLDGVAL") + "") - Conversion.Val(rsTemp.Get_Fields_Int32("RLEXEMPTION") + ""), "###,###,##0");
					dblTotalLand += Conversion.Val(rsTemp.Get_Fields_Int32("lastlandval"));
					dblTotalBldg += Conversion.Val(rsTemp.Get_Fields_Int32("lastbldgval"));
					dblTotalExempt += Conversion.Val(rsTemp.Get_Fields_Int32("rlexemption"));
				}
				else
				{
					txtLand.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("RLLANDVAL") + ""), "##,###,##0");
					txtBuilding.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("RLBLDGVAL") + ""), "##,###,##0");
					txtExemption.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("RLEXEMPTION") + ""), "##,###,##0");
					txtTotal.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("RLLANDVAL") + "") + Conversion.Val(rsTemp.Get_Fields_Int32("RLBLDGVAL") + "") - Conversion.Val(rsTemp.Get_Fields_Int32("RLEXEMPTION") + ""), "###,###,##0");
					dblTotalLand += Conversion.Val(rsTemp.Get_Fields_Int32("rllandval"));
					dblTotalBldg += Conversion.Val(rsTemp.Get_Fields_Int32("rlbldgval"));
					dblTotalExempt += Conversion.Val(rsTemp.Get_Fields_Int32("correxemption"));
				}
			}
			else
			{
				strSQL = "select sum(" + strExempt + ") as exemptsum,sum(" + strland + ") as landsum, sum(" + strbldg + ") as bldgsum from master where rsaccount = " + rsTemp.Get_Fields_Int32("rsaccount") + " and not rsdeleted = 1";
				clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				txtLand.Text = Strings.Format(Conversion.Val(clsTemp.GetData("landsum") + ""), "##,###,##0");
				txtBuilding.Text = Strings.Format(Conversion.Val(clsTemp.GetData("bldgsum")), "###,###,##0");
				txtExemption.Text = Strings.Format(Conversion.Val(clsTemp.GetData("exemptsum")), "###,###,##0");
				txtTotal.Text = Strings.Format(Conversion.Val(clsTemp.GetData("landsum")) - Conversion.Val(clsTemp.GetData("exemptsum")) + Conversion.Val(clsTemp.GetData("bldgsum")), "#,###,###,##0");
				// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
				dblTotalLand += Conversion.Val(clsTemp.Get_Fields("landsum") + "");
				// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
				dblTotalBldg += Conversion.Val(clsTemp.Get_Fields("bldgsum"));
				// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
				dblTotalExempt += Conversion.Val(clsTemp.Get_Fields("exemptsum"));
			}
			lngCount += 1;
			varAccount = rsTemp.Get_Fields_Int32("rsaccount");
			if (!rsTemp.EndOfFile())
				rsTemp.MoveNext();
			eArgs.EOF = false;
		}

		private void ActiveReport_Initialize()
		{
			// If rptListingNameAddrLocMapAsses.Printer <> "" Then
			// rptListingNameAddrLocMapAsses.Printer.PrintQuality = ddPQMedium
			// End If
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			// If rptListingNameAddrLocMapAsses.Printer <> "" Then
			// rptListingNameAddrLocMapAsses.Printer.PrintQuality = ddPQMedium
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (Strings.Trim(this.Document.Printer.PrinterName) != string.Empty)
			{
				//this.Document.Printer.RenderMode = 1;
			}
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtCaption.Text = "Real Estate";
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			dblTotalLand = 0;
			dblTotalBldg = 0;
			dblTotalExempt = 0;
			dblTotalTotal = 0;
			lngCount = 0;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			intPage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
		}

		public void Start(bool boolByExtract, bool boolByAccount)
		{
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = rsTemp.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			int lngUID;
			string strSQL = "";
			boolByCard = !boolByAccount;
			if (boolByCard)
				lblCards.Visible = true;
			if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
			{
				txtCaption2.Text = "Account List by Account";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
			{
				txtCaption2.Text = "Account List by Location";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSMAPLOT")
			{
				txtCaption2.Text = "Account List by Map/Lot";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSNAME")
			{
				txtCaption2.Text = "Account List by Name";
			}
			if (modGlobalVariables.Statics.boolRange)
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
				{
					txtCaption2.Text = txtCaption2.Text + " (" + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " - " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + ")";
				}
				else
				{
					txtCaption2.Text = txtCaption2.Text + " (" + modPrintRoutines.Statics.gstrMinAccountRange + " - " + modPrintRoutines.Statics.gstrMaxAccountRange + ")";
				}
			}
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolByExtract)
			{
				if (boolByAccount)
				{
					strSQL = strMasterJoin + " where rsaccount in (select extracttable.accountnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " + FCConvert.ToString(lngUID) + ") AND rsdeleted = 0 ";
				}
				else
				{
					// strSQL = "select  master.* from (master inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and master.rsdeleted = 0"
					strSQL = "select  mj.* from (" + strMasterJoinQuery + " inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and mj.rsdeleted = 0";
				}
				rsTemp.OpenRecordset("select TITLE FROM EXTRACT WHERe reportnumber = 0 and userid = " + FCConvert.ToString(lngUID), modGlobalVariables.strREDatabase);
				if (!rsTemp.EndOfFile())
				{
					if (FCConvert.ToString(rsTemp.Get_Fields_String("title")) != string.Empty)
					{
						txtCaption2.Text = rsTemp.Get_Fields_String("title");
					}
				}
			}
			else
			{
				strSQL = strMasterJoin + " where rsdeleted = 0";
			}
			if (boolByAccount)
			{
				strSQL += " and rscard = 1";
			}
			if (modGlobalVariables.Statics.boolRange)
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " + modPrintRoutines.Statics.gstrFieldName + " <= " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + " order by " + modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
				}
				else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' order by " + modPrintRoutines.Statics.gstrFieldName + ",CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float)  ", modGlobalVariables.strREDatabase);
				}
				else
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,RSCard", modGlobalVariables.strREDatabase);
				}
			}
			else
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
				{
					rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float)  ", modGlobalVariables.strREDatabase);
				}
				else
				{
					if (Strings.LCase(modPrintRoutines.Statics.gstrFieldName) != "rsaccount")
					{
						rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,rscard", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",rscard", modGlobalVariables.strREDatabase);
					}
				}
			}
			if (modGlobalVariables.Statics.boolBillingorCorrelated)
			{
				strland = "lastlandval";
				strbldg = "lastbldgval";
				strExempt = "rlexemption";
			}
			else
			{
				strland = "rllandval";
				strbldg = "rlbldgval";
				strExempt = "correxemption";
			}
			// Me.Show
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "NameAddrLocMapAssess");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalLand.Text = Strings.Format(dblTotalLand, "#,###,###,##0");
			txtTotalBuilding.Text = Strings.Format(dblTotalBldg, "#,###,###,##0");
			txtTotalExemption.Text = Strings.Format(dblTotalExempt, "#,###,###,##0");
			dblTotalTotal = dblTotalLand + dblTotalBldg - dblTotalExempt;
			txtTotalTotal.Text = Strings.Format(dblTotalTotal, "#,###,###,##0");
			txtCount.Text = Strings.Format(lngCount, "#,###,###,##0");
		}

		
	}
}
