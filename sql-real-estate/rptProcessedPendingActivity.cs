﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptProcessedPendingActivity.
	/// </summary>
	public partial class rptProcessedPendingActivity : BaseSectionReport
	{
		public static rptProcessedPendingActivity InstancePtr
		{
			get
			{
				return (rptProcessedPendingActivity)Sys.GetInstance(typeof(rptProcessedPendingActivity));
			}
		}

		protected rptProcessedPendingActivity _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptProcessedPendingActivity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Processed Pending Activity";
		}
		// nObj = 1
		//   0	rptProcessedPendingActivity	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolChanges;
		bool boolTransfers;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int x;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			boolChanges = false;
			boolTransfers = false;
			for (x = 0; x <= frmPendingTransfers.InstancePtr.GridReport.Rows - 1; x++)
			{
				if (frmPendingTransfers.InstancePtr.GridReport.TextMatrix(x, 5) == "Changes")
				{
					boolChanges = true;
				}
				else if (frmPendingTransfers.InstancePtr.GridReport.TextMatrix(x, 5) == "Transfers")
				{
					boolTransfers = true;
				}
			}
			// x
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			//srptPendingChanges.InstancePtr.Close();
			//srptPendingTransfers.InstancePtr.Close();
			//Application.DoEvents();
			//FC:FINAL:RPU:#i1472 - Unload frmPendingTransfers when this is closed
			frmPendingTransfers.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolChanges)
				SubReport1.Report = new srptPendingChanges();
			if (boolTransfers)
				SubReport2.Report = new srptPendingTransfers();
		}

		
	}
}
