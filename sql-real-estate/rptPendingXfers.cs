﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPendingXfers.
	/// </summary>
	public partial class rptPendingXfers : BaseSectionReport
	{
		public static rptPendingXfers InstancePtr
		{
			get
			{
				return (rptPendingXfers)Sys.GetInstance(typeof(rptPendingXfers));
			}
		}

		protected rptPendingXfers _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
                clsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptPendingXfers()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptPendingXfers	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();
		string[] aryType = new string[9 + 1];
		string[] aryFinance = new string[9 + 1];
		string[] aryVerified = new string[9 + 1];
		string[] aryValidity = new string[9 + 1];
        cPartyController partCont = new cPartyController();

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			clsDRWrapper clsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
                txtMuni.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
                strSQL = "Select * from pending order by mindate,id";
                clsReport.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                if (clsReport.EndOfFile())
                {
                    MessageBox.Show("No pending transfers", "No Transfers", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    this.Close();
                }

                clsLoad.OpenRecordset(
                    "select * from costrecord where crecordnumber between 1351 and 1359 order by crecordnumber",
                    modGlobalVariables.strREDatabase);
                aryType[0] = "";
                while (!clsLoad.EndOfFile())
                {
                    aryType[clsLoad.Get_Fields_Int32("crecordnumber") - 1350] =
                        Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                    clsLoad.MoveNext();
                }

                strSQL = "Select * from costrecord where crecordnumber between 1361 and 1369 order by crecordnumber";
                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                aryFinance[0] = "";
                while (!clsLoad.EndOfFile())
                {
                    aryFinance[clsLoad.Get_Fields_Int32("crecordnumber") - 1360] =
                        Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                    clsLoad.MoveNext();
                }

                strSQL = "Select * from costrecord where crecordnumber between 1371 and 1379 order by crecordnumber";
                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                aryVerified[0] = "";
                while (!clsLoad.EndOfFile())
                {
                    aryVerified[clsLoad.Get_Fields_Int32("crecordnumber") - 1370] =
                        Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                    clsLoad.MoveNext();
                }

                strSQL = "Select * from costrecord where crecordnumber between 1381 and 1389 order by crecordnumber";
                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                aryValidity[0] = "";
                while (!clsLoad.EndOfFile())
                {
                    aryValidity[clsLoad.Get_Fields_Int32("crecordnumber") - 1380] =
                        Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                    clsLoad.MoveNext();
                }

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In ReportStart", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				clsLoad.Dispose();
            }
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                if (!clsReport.EndOfFile())
                {
                    txtSaleDate.Text = "";
                    txtSalePrice.Text = "";
                    txtSaleFinance.Text = "";
                    txtSaleType.Text = "";
                    txtSaleValidity.Text = "";
                    txtSaleVerified.Text = "";
                    txtNewAddress1.Text = "";
                    txtNewAddress2.Text = "";
                    txtNewCity.Text = "";
                    txtNewState.Text = "";
                    txtNewZip.Text = "";

                    var own = partCont.GetParty(clsReport.Get_Fields_Int32("OwnerPartyId"));
                    if (own != null)
                    {
                        var adr = own.GetPrimaryAddress();
                        if (adr != null)
                        {
                            txtNewAddress1.Text = adr.Address1.Trim();
                            txtNewAddress2.Text = adr.Address2.Trim();
                            txtNewCity.Text = adr.City.Trim();
                            txtNewState.Text = adr.State.Trim();
                            txtNewZip.Text = adr.Zip.Trim();
                        }
                    }

                    txtNewName.Text = Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("DeedName1")));
                    txtNewSecondOwner.Text = Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("DeedName2")));
                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                    txtAccount.Text = FCConvert.ToString(clsReport.Get_Fields("account"));
                    txtMinDate.Text = Strings.Format(clsReport.Get_Fields_DateTime("mindate"), "MM/dd/yyyy");
                    // TODO Get_Fields: Field [key] not found!! (maybe it is an alias?)
                    txtTransaction.Text = FCConvert.ToString(clsReport.Get_Fields("id"));
                    if (Information.IsDate(clsReport.Get_Fields("saledate")))
                    {
                        if (clsReport.Get_Fields_DateTime("saledate") != DateTime.FromOADate(0))
                        {
                            txtSaleDate.Text = Strings.Format(clsReport.Get_Fields_DateTime("saledate"), "MM/dd/yyyy");
                        }

                        if (Conversion.Val(clsReport.Get_Fields_Int32("saleprice")) > 0)
                        {
                            txtSalePrice.Text =
                                Strings.Format(clsReport.Get_Fields_Int32("saleprice"), "#,###,###,##0");
                        }

                        if (Conversion.Val(clsReport.Get_Fields_Int32("salefinancing")) < 10)
                        {
                            txtSaleFinance.Text = aryFinance[clsReport.Get_Fields_Int32("salefinancing")];
                        }

                        if (Conversion.Val(clsReport.Get_Fields_Int32("saletype")) < 10)
                        {
                            txtSaleType.Text = aryType[clsReport.Get_Fields_Int32("saletype")];
                        }

                        if (Conversion.Val(clsReport.Get_Fields_Int32("salevalidity")) < 10)
                        {
                            txtSaleValidity.Text = aryValidity[clsReport.Get_Fields_Int32("salevalidity")];
                        }

                        if (Conversion.Val(clsReport.Get_Fields_Int32("saleverified")) < 10)
                        {
                            txtSaleVerified.Text = aryVerified[clsReport.Get_Fields_Int32("salevalidity")];
                        }
                    }

                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                    txtName.Text = "";
                    txtSecondOwner.Text = "";
                    txtAddress1.Text = "";
                    txtAddress2.Text = "";
                    txtCity.Text = "";
                    txtState.Text = "";
                    txtZip.Text = "";
                    txtMapLot.Text = "";
                    clsLoad.OpenRecordset(
                        "select * from master where rscard = 1 and rsaccount = " + clsReport.Get_Fields("account"),
                        modGlobalVariables.strREDatabase);
                    if (!clsLoad.EndOfFile())
                    {
                        txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("rsmaplot")));
                        txtName.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("DeedName1")));
                        txtSecondOwner.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("DeedName2")));
                        var origOwn = partCont.GetParty(clsLoad.Get_Fields_Int32("OwnerPartyID"));
                        if (origOwn != null)
                        {
                            var origAdr = origOwn.GetPrimaryAddress();
                            if (origAdr != null)
                            {
                                txtAddress1.Text = origAdr.Address1.Trim();
                                txtAddress2.Text = origAdr.Address2.Trim();
                                txtCity.Text = origAdr.City.Trim();
                                txtZip.Text = origAdr.Zip.Trim();
                            }
                        }
                    }

                    if (clsReport.Get_Fields_Int32("transfertype") == 0)
                    {
                        txtTransfer.Text = "Account";
                    }
                    else if (clsReport.Get_Fields_Int32("transfertype") == 1)
                    {
                        txtTransfer.Text = "Card(s)";
                    }

                    if (clsReport.Get_Fields_Int32("transferwhat") == 0)
                    {
                        txtXferTo.Text = "Same";
                    }
                    else if (clsReport.Get_Fields_Int32("transferwhat") == 1)
                    {
                        txtXferTo.Text = "New";
                    }
                    else if (clsReport.Get_Fields_Int32("transferwhat") == 2)
                    {
                        txtXferTo.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("toaccount"));
                    }

                    clsReport.MoveNext();
                }

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In Detail_Format", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
                clsLoad.Dispose();
            }
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
