﻿//Fecher vbPorter - Version 1.0.0.40
using Wisej.Web;
using System.Drawing;
using fecherFoundation;
using Global;
using System;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPurgeSaleFile.
	/// </summary>
	partial class frmPurgeSaleFile : BaseForm
	{
		public Global.T2KDateBox t2kDate;
		public fecherFoundation.FCProgressBar pbDelete;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurgeSaleFile));
            this.t2kDate = new Global.T2KDateBox();
            this.pbDelete = new fecherFoundation.FCProgressBar();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 367);
            this.BottomPanel.Size = new System.Drawing.Size(619, 20);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Controls.Add(this.t2kDate);
            this.ClientArea.Controls.Add(this.pbDelete);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(619, 307);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(619, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(193, 30);
            this.HeaderText.Text = "Purge Sale Files";
            // 
            // t2kDate
            // 
            this.t2kDate.Location = new System.Drawing.Point(128, 30);
            this.t2kDate.Name = "t2kDate";
            this.t2kDate.Size = new System.Drawing.Size(115, 40);
            this.t2kDate.TabIndex = 1;
            // 
            // pbDelete
            // 
            this.pbDelete.Location = new System.Drawing.Point(240, 35);
            this.pbDelete.Name = "pbDelete";
            this.pbDelete.Size = new System.Drawing.Size(91, 23);
            this.pbDelete.TabIndex = 2;
            this.pbDelete.Visible = false;
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(30, 144);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(87, 48);
            this.cmdProcess.TabIndex = 5;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(89, 15);
            this.Label2.Text = "ENTER DATE";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 94);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(577, 27);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "ALL SALES RECORDS WITH SALE DATES ON OR PRIOR TO THE DATE ENTERED WILL BE PURGED";
            // 
            // frmPurgeSaleFile
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(619, 387);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmPurgeSaleFile";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Purge Sale Files";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPurgeSaleFile_Load);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeSaleFile_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
