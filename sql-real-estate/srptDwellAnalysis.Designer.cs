﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptDwellAnalysis.
	/// </summary>
	partial class srptDwellAnalysis
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptDwellAnalysis));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtBldgCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssess = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAve = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAve)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtBldgCode,
				this.txtCount,
				this.txtAssess,
				this.txtAve
			});
			this.Detail.Height = 0.25F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Name = "PageFooter";
			// 
			// txtBldgCode
			// 
			this.txtBldgCode.Height = 0.19F;
			this.txtBldgCode.Left = 0F;
			this.txtBldgCode.Name = "txtBldgCode";
			this.txtBldgCode.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtBldgCode.Text = "Field1";
			this.txtBldgCode.Top = 0.0625F;
			this.txtBldgCode.Width = 2F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.1875F;
			this.txtCount.Left = 3.4375F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtCount.Text = "Field1";
			this.txtCount.Top = 0.03125F;
			this.txtCount.Width = 0.875F;
			// 
			// txtAssess
			// 
			this.txtAssess.Height = 0.1875F;
			this.txtAssess.Left = 5.0625F;
			this.txtAssess.Name = "txtAssess";
			this.txtAssess.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAssess.Text = "Field1";
			this.txtAssess.Top = 0.03125F;
			this.txtAssess.Width = 1.1875F;
			// 
			// txtAve
			// 
			this.txtAve.Height = 0.1875F;
			this.txtAve.Left = 6.3125F;
			this.txtAve.Name = "txtAve";
			this.txtAve.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAve.Text = "Field1";
			this.txtAve.Top = 0.03125F;
			this.txtAve.Width = 1.125F;
			// 
			// srptDwellAnalysis
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtBldgCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAve)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssess;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAve;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
