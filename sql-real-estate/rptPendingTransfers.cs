﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptPendingTransfers.
	/// </summary>
	public partial class srptPendingTransfers : BaseSectionReport
	{
		public srptPendingTransfers()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Processed Pending Transfers";
		}

		public static srptPendingTransfers InstancePtr
		{
			get
			{
				return (srptPendingTransfers)Sys.GetInstance(typeof(srptPendingTransfers));
			}
		}

		protected srptPendingTransfers _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptPendingTransfers	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngCurrentRow;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngCurrentRow >= frmPendingTransfers.InstancePtr.GridReport.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngCurrentRow = 0;
			while (lngCurrentRow < frmPendingTransfers.InstancePtr.GridReport.Rows)
			{
				if (frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 5) == "Transfers")
					break;
				lngCurrentRow += 1;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngCurrentRow < frmPendingTransfers.InstancePtr.GridReport.Rows)
			{
				txtAccount.Text = frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 0);
				txtAction.Text = frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 1);
				txtXfer.Text = frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 2);
				txtMinDate.Text = frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 3);
				txtSaleDate.Text = frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 4);
				lngCurrentRow += 1;
			}
		}

		
	}
}
