﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptLandSchedule.
	/// </summary>
	public partial class rptLandSchedule : BaseSectionReport
	{
		public static rptLandSchedule InstancePtr
		{
			get
			{
				return (rptLandSchedule)Sys.GetInstance(typeof(rptLandSchedule));
			}
		}

		protected rptLandSchedule _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLand?.Dispose();
				clsReport?.Dispose();
                clsLand = null;
                clsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptLandSchedule()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Land Schedule";
			this.Fields.Add("GroupHeader1");
			GroupHeader1.DataField = "GroupHeader1";
		}
		// nObj = 1
		//   0	rptLandSchedule	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();
		clsDRWrapper clsLand = new clsDRWrapper();
		int lngPageNumber;

		public void Init(int lngScheduleNum)
		{
			string strSQL = "";
			lngPageNumber = 1;
			if (lngScheduleNum <= 0)
			{
				// do em all
				strSQL = "Select * from landschedule order by schedule,code";
			}
			else
			{
				// specific one
				strSQL = "select * from landschedule where schedule = " + FCConvert.ToString(lngScheduleNum) + " order by code";
			}
			clsReport.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			clsLand.OpenRecordset("select * from landtype order by code", modGlobalVariables.strREDatabase);
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
			if (!clsReport.EndOfFile())
			{
				this.Fields["GroupHeader1"].Value = FCConvert.ToString(clsReport.Get_Fields_Int32("schedule"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			//this.Fields["GroupHeader1"].Value = "0";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsReport.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(clsReport.Get_Fields("code")) < 0)
				{
					Detail.Visible = false;
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					int vbPorterVar = FCConvert.ToInt32(clsReport.Get_Fields("code"));
					if (vbPorterVar == modREConstants.CNSTLANDSCHEDULESTANDARDDEPTH)
					{
						// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
						txtStandardDepth.Text = Strings.Format(clsReport.Get_Fields("amount"), "0.00");
					}
					else if (vbPorterVar == modREConstants.CNSTLANDSCHEDULESTANDARDLOT)
					{
						// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
						txtStandardLot.Text = Strings.Format(clsReport.Get_Fields("amount"), "0.00");
					}
					else if (vbPorterVar == modREConstants.CNSTLANDSCHEDULEDESCRIPTION)
					{
						// lblScheduleNumber.Caption = clsReport.Fields("schedule") & " " & clsReport.Fields("description")
					}
					else if (vbPorterVar == modREConstants.CNSTLANDSCHEDULESTANDARDWIDTH)
					{
						// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
						txtStandardWidth.Text = Strings.Format(clsReport.Get_Fields("amount"), "0.00");
					}
				}
				else
				{
					Detail.Visible = true;
					// If clsLand.FindFirstRecord("code", clsReport.Fields("code")) Then
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					if (clsLand.FindFirst("code = " + FCConvert.ToString(clsReport.Get_Fields("code"))))
					{
						txtDescription.Text = FCConvert.ToString(clsLand.Get_Fields_String("description"));
						// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(clsLand.Get_Fields("type")) == modREConstants.CNSTLANDTYPEACRES)
						{
							txtType.Text = "Acres";
						}
						// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
						else if (FCConvert.ToInt32(clsLand.Get_Fields("type")) == modREConstants.CNSTLANDTYPEFRACTIONALACREAGE)
						{
							txtType.Text = "Fractional Acres";
						}
							// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
							else if (FCConvert.ToInt32(clsLand.Get_Fields("type")) == modREConstants.CNSTLANDTYPEFRONTFOOT)
						{
							txtType.Text = "Front Foot";
						}
								// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
								else if (FCConvert.ToInt32(clsLand.Get_Fields("type")) == modREConstants.CNSTLANDTYPESITE)
						{
							txtType.Text = "Site";
						}
									// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
									else if (FCConvert.ToInt32(clsLand.Get_Fields("type")) == modREConstants.CNSTLANDTYPEIMPROVEMENTS)
						{
							txtType.Text = "Improvements";
						}
										// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
										else if (FCConvert.ToInt32(clsLand.Get_Fields("type")) == modREConstants.CNSTLANDTYPELINEARFEET)
						{
							txtType.Text = "Linear Feet";
						}
											// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
											else if (FCConvert.ToInt32(clsLand.Get_Fields("type")) == modREConstants.CNSTLANDTYPESQUAREFEET)
						{
							txtType.Text = "Square Feet";
						}
												// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
												else if (FCConvert.ToInt32(clsLand.Get_Fields("type")) == modREConstants.CNSTLANDTYPEFRONTFOOTSCALED)
						{
							txtType.Text = "Front Foot - Width";
						}
													// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
													else if (FCConvert.ToInt32(clsLand.Get_Fields("type")) == modREConstants.CNSTLANDTYPELINEARFEETSCALED)
						{
							txtType.Text = "Linear Feet - Width";
						}
					}
					else
					{
						txtType.Text = "";
						txtDescription.Text = "";
					}
				}
				// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
				txtAmount.Text = Strings.Format(clsReport.Get_Fields("amount"), "#,###,##0.00");
				txtExponent1.Text = Strings.Format(clsReport.Get_Fields_Double("exponent1"), "0.00");
				txtExponent2.Text = Strings.Format(clsReport.Get_Fields_Double("exponent2"), "0.00");
				//FC:FINAL:AAKV .excption handled
				//txtCode.Text = clsReport.Get_Fields("code");
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				txtCode.Text = clsReport.Get_Fields("code").ToString();
				txtWidthExponent1.Text = Strings.Format(clsReport.Get_Fields_Double("widthexponent1"), "0.00");
				txtWidthExponent2.Text = Strings.Format(clsReport.Get_Fields_Double("Widthexponent2"), "0.00");
				clsReport.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + FCConvert.ToString(lngPageNumber);
			lngPageNumber += 1;
			if (!clsReport.EndOfFile())
			{
				//FC:FINAL:AAKV -exception error cleared
				//lblScheduleNumber.Text = clsReport.Get_Fields("schedule");
				lblScheduleNumber.Text = clsReport.Get_Fields_Int32("schedule").ToString();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from landschedule where schedule = " + clsReport.Get_Fields_Int32("schedule") + " and code = " + FCConvert.ToString(modREConstants.CNSTLANDSCHEDULEDESCRIPTION), modGlobalVariables.strREDatabase);
				if (!rsLoad.EndOfFile())
				{
					lblScheduleNumber.Text = clsReport.Get_Fields_Int32("schedule") + " " + rsLoad.Get_Fields_String("description");
				}
			}
		}

		
	}
}
