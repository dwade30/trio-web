﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCalcSales.
	/// </summary>
	public partial class frmCalcSales : BaseForm
	{
		public frmCalcSales()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCalcSales InstancePtr
		{
			get
			{
				return (frmCalcSales)Sys.GetInstance(typeof(frmCalcSales));
			}
		}

		protected frmCalcSales _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolCalcAll;
		bool boolCalcAcctRange;
		bool boolCalcDateRange;

		private void cmdCalc_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSaleRecs = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strSQL = "";
			int lngCurPos;
			int reccount;
            //FC:FINAL:AM: enable cancel button
			//Click2();
            this.ShowLoader = false;
            FCUtils.StartTask(this, () =>
            {
                Click2();
                FCUtils.UnlockUserInterface();
            });
            return;
		}

		public void cmdCalc_Click()
		{
			cmdCalc_Click(cmdCalc, new System.EventArgs());
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void cmdCancel_Click()
		{
			//cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void frmCalcSales_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCalcSales_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCalcSales properties;
			//frmCalcSales.ScaleWidth	= 5880;
			//frmCalcSales.ScaleHeight	= 4470;
			//frmCalcSales.LinkTopic	= "Form1";
			//frmCalcSales.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdCalc_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdCancel_Click();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void optCalc_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index > 0)
			{
				txtMin.Enabled = true;
				txtMax.Enabled = true;
				txtMin.Text = "";
				txtMax.Text = "";
			}
			else
			{
				txtMin.Enabled = false;
				txtMax.Enabled = false;
			}
		}

		private void optCalc_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbCalc.SelectedIndex;
			optCalc_CheckedChanged(index, sender, e);
		}

		private void Click2()
		{
			clsDRWrapper clsSaleRecs = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strSQL = "";
			int lngCurPos;
			int reccount;
			string strWhere = "";
			try
			{
				// On Error GoTo ErrorHandler
				boolCalcAll = false;
				boolCalcAcctRange = false;
				boolCalcDateRange = false;
				modSpeedCalc.Statics.boolCalcErrors = false;
				modSpeedCalc.Statics.CalcLog = "";
				if (cmbCalc.Text == "All")
				{
					boolCalcAll = true;
					strSQL = "select * from srmaster WHERE not rsdeleted = 1 order by saledate,rsaccount,saleid,rscard";
					strWhere = " order by saledate,rsaccount,rscard";
				}
				if (cmbCalc.Text == "Range by Account")
				{
					if ((Conversion.Val(txtMin.Text) == 0) || (Conversion.Val(txtMax.Text) == 0))
					{
						MessageBox.Show("You must choose a max and min Account number before proceeding.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (Conversion.Val(txtMax.Text) < Conversion.Val(txtMin.Text))
					{
						MessageBox.Show("The max you chose is less than the min you chose.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					boolCalcAcctRange = true;
					strSQL = "select * from srmaster where not rsdeleted = 1 AND  rsaccount between " + FCConvert.ToString(Conversion.Val(txtMin.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtMax.Text)) + " order by saledate,rsaccount,saleid,rscard";
					strWhere = " where  rsaccount between " + FCConvert.ToString(Conversion.Val(txtMin.Text)) + " and " + FCConvert.ToString(Conversion.Val(txtMax.Text)) + " order by saledate,rsaccount,rscard";
				}
				if (cmbCalc.Text == "Range by Sale Date")
				{
					if (!Information.IsDate(txtMin.Text))
					{
						MessageBox.Show("The min date you specified is not a valid date", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (!Information.IsDate(txtMax.Text))
					{
						MessageBox.Show("The max date you specified is not a valid date", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					boolCalcDateRange = true;
					strSQL = "select * from srmaster where not rsdeleted = 1 and saledate between '" + txtMin.Text + "' and '" + txtMax.Text + "' order by saledate,rsaccount,rscard";
					strWhere = " where saledate between '" + txtMin.Text + "' and '" + txtMax.Text + "' order by saledate,rsaccount,rscard";
				}
				modSpeedCalc.REAS03_1075_GET_PARAM_RECORD();
				// Call clsSaleRecs.OpenRecordset(strSQL, strredatabase, , True)
				modDataTypes.Statics.MR.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				modDataTypes.Statics.OUT.OpenRecordset("select * from sroutbuilding " + strWhere, modGlobalVariables.strREDatabase);
				modDataTypes.Statics.DWL.OpenRecordset("select * from srdwelling " + strWhere, modGlobalVariables.strREDatabase);
				modDataTypes.Statics.CMR.OpenRecordset("select * from srcommercial " + strWhere, modGlobalVariables.strREDatabase);
				modGlobalVariables.Statics.boolUseArrays = true;
				modGlobalRoutines.Check_Arrays();
				modGlobalVariables.Statics.boolRegRecords = false;
				modGlobalVariables.Statics.boolUpdateWhenCalculate = true;
				Frmassessmentprogress.InstancePtr.Initialize();
				Frmassessmentprogress.InstancePtr.Label2.Text = "Calculating Sale Record(s). Please Wait.";
				Frmassessmentprogress.InstancePtr.Text = "Sale Calculation";
				Frmassessmentprogress.InstancePtr.cmdcancexempt.Visible = true;
				//FC:FINAL:RPU:#i1191 - Show the form modeless
				//Frmassessmentprogress.InstancePtr.Show();
				Frmassessmentprogress.InstancePtr.Show(FormShowEnum.Modeless);
				lngCurPos = 1;
				reccount = modDataTypes.Statics.MR.RecordCount();
				while (!modDataTypes.Statics.MR.EndOfFile())
				{
					//Application.DoEvents();
					if (Frmassessmentprogress.InstancePtr.stillcalc == false)
					{
						goto DoneCalc;
					}
					modDataTypes.Statics.MR.Edit();
					modGlobalVariables.Statics.gcurrsaledate = modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "";
					if (Information.IsDate(modGlobalVariables.Statics.gcurrsaledate))
					{
						// Call clsTemp.OpenRecordset("select max(rscard) as maxcard from srmaster where rsaccount = " & clsSaleRecs.Fields("rsaccount") & " and saledate = #" & clsSaleRecs.Fields("saledate") & "#", strredatabase)
						// gintMaxCards = Val(clsTemp.Fields("maxcard"))
						modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
						modGlobalVariables.Statics.boolfromcalcandassessment = true;
						// if true, calcroutine doesn't display or do other things, it just calculates
						modProperty.Statics.RUNTYPE = "P";
						modGlobalVariables.Statics.boolUpdateWhenCalculate = true;
						modSpeedCalc.CalcCard();
						modGlobalVariables.Statics.boolfromcalcandassessment = false;
						// Call DisplayCalculate
					}
					Frmassessmentprogress.InstancePtr.ProgressBar1.Value = FCConvert.ToInt32((FCConvert.ToDouble(modDataTypes.Statics.MR.AbsolutePosition()) / reccount) * 100);
					Frmassessmentprogress.InstancePtr.lblpercentdone.Text = Strings.Format(FCConvert.ToDouble(modDataTypes.Statics.MR.AbsolutePosition()) / reccount * 100, "##0.0") + "%";
					Frmassessmentprogress.InstancePtr.BringToFront();
					//FC:FINAL:RPU: #i1191 - Update the form to show progress
					FCUtils.ApplicationUpdate(Frmassessmentprogress.InstancePtr);
					//Application.DoEvents();
					modDataTypes.Statics.MR.MoveNext();
				}
				DoneCalc:
				;
				modGlobalVariables.Statics.boolUseArrays = false;
				modGlobalVariables.Statics.boolfromcalcandassessment = false;
				Frmassessmentprogress.InstancePtr.Unload();
				MessageBox.Show("Sales calculation complete.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				if (modSpeedCalc.Statics.boolCalcErrors)
				{
					MessageBox.Show("There were errors during calculation. Following is a list.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
					//! Load frmShowResults;
					frmShowResults.InstancePtr.Init(ref modSpeedCalc.Statics.CalcLog);
					frmShowResults.InstancePtr.lblScreenTitle.Text = "Calculation Errors Log";
					frmShowResults.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				}
				modGlobalVariables.Statics.boolUseArrays = false;
				modSpeedCalc.Statics.boolCalcErrors = false;
				modSpeedCalc.Statics.CalcLog = "";
				//MDIParent.InstancePtr.Show();
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				Frmassessmentprogress.InstancePtr.Unload();
				modGlobalVariables.Statics.boolUseArrays = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CalcSales", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMax_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.Help && KeyAscii <= Keys.D9)
			{
			}
			else if ((KeyAscii == Keys.F16) || (KeyAscii == Keys.Back))
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMin_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.Help && KeyAscii <= Keys.D9)
			{
			}
			else if ((KeyAscii == Keys.F16) || (KeyAscii == Keys.Back))
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
