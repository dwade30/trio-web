﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace TWRE0000
{
	public class cREPropertyExemption
	{
		//=========================================================
		private int intExemptCode;
		private double dblExemptValue;
		private double dblPercentage;

		public double Percentage
		{
			set
			{
				dblPercentage = value;
			}
			get
			{
				double Percentage = 0;
				Percentage = dblPercentage;
				return Percentage;
			}
		}

		public short ExemptCode
		{
			set
			{
				intExemptCode = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short ExemptCode = 0;
				ExemptCode = FCConvert.ToInt16(intExemptCode);
				return ExemptCode;
			}
		}

		public double ExemptValue
		{
			set
			{
				dblExemptValue = value;
			}
			get
			{
				double ExemptValue = 0;
				ExemptValue = dblExemptValue;
				return ExemptValue;
			}
		}
	}
}
