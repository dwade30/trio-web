﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmAssessExtract.
	/// </summary>
	public partial class frmAssessExtract : BaseForm
	{
		public frmAssessExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAssessExtract InstancePtr
		{
			get
			{
				return (frmAssessExtract)Sys.GetInstance(typeof(frmAssessExtract));
			}
		}

		protected frmAssessExtract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const int CNSTGRIDCOLINCLUDE = 0;
		const int CNSTGRIDCOLREPORTNUMBER = 1;
		const int CNSTGRIDCOLREPORTTITLE = 2;
		const int CNSTGRIDCOLREPORTDESCRIPTION = 3;
		const int CNSTGRIDCOLUSERNAME = 4;
		const int CNSTGRIDCOLUSERID = 5;
		bool boolUseRegular;
		bool boolUseSales;

		private void cmdExtract_Click()
		{
			clsDRWrapper RSExtract = new clsDRWrapper();
			int x;
			int intResp;
			int intGroup;
			int lngID;
			try
			{
				// On Error GoTo ErrorHandler
				if (cmbRorS.Text == "Regular")
				{
					boolUseRegular = true;
					boolUseSales = false;
				}
				else
				{
					boolUseRegular = false;
					boolUseSales = true;
				}
				lngID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
				RSExtract.Execute("Delete  from Extract where userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
				RSExtract.Execute("Delete from ExtractTable where userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
				RSExtract.OpenRecordset("select * from extract where userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
				RSExtract.AddNew();
				RSExtract.Set_Fields("reportnumber", 0);
				RSExtract.Set_Fields("Title", Strings.Trim(txtTitle.Text + ""));
				RSExtract.Set_Fields("extractdate", DateTime.Now);
				RSExtract.Set_Fields("groupnumber", 0);
				RSExtract.Set_Fields("userid", lngID);
				RSExtract.Update();
				intGroup = 1;
				for (x = 1; x <= Grid.Rows - 1; x++)
				{
					// If Grid.RowIsVisible(x) Then
					if (!Grid.RowHidden(x))
					{
						if (FCConvert.CBool(Grid.TextMatrix(x, CNSTGRIDCOLINCLUDE)))
						{
							RSExtract.AddNew();
							RSExtract.Set_Fields("reportnumber", Grid.TextMatrix(x, CNSTGRIDCOLREPORTNUMBER));
							RSExtract.Set_Fields("Title", Grid.TextMatrix(x, CNSTGRIDCOLREPORTTITLE));
							RSExtract.Set_Fields("groupnumber", intGroup);
							RSExtract.Set_Fields("Userid", lngID);
							RSExtract.Update();
							intGroup += 1;
						}
					}
				}
				// x
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (boolUseRegular)
				{
					modAssessExtract.MakeExtractionRecord_2("Master");
				}
				else
				{
					modAssessExtract.MakeExtractionRecord_2("SRMaster");
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Extract_Click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdQuit_Click()
		{
			this.Unload();
		}

		private void frmAssessExtract_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmAssessExtract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAssessExtract properties;
			//frmAssessExtract.ScaleWidth	= 9045;
			//frmAssessExtract.ScaleHeight	= 7260;
			//frmAssessExtract.LockControls	= true;
			//End Unmaped Properties
			// Dim clsTemp As New clsDRWrapper
			// Dim strSQL As String
			// 
			// 
			// 
			// Call clsTemp.OpenRecordset("Select * from reportcodes where code = 147", strREDatabase)
			// If clsTemp.RecordCount < 1 Then
			// strSQL = "insert into reportcodes (code,description,category,tablename,fieldname,specialcase,hdescription) "
			// strSQL = strSQL & " values (147,'Total Acreage','Land Information','Master','piacres',false,'This is the total acreage calculated for this card.')"
			// Call clsTemp.Execute(strSQL, strREDatabase)
			// End If
			// 
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			FillGrid();
		}

		private void frmAssessExtract_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			mnuEditReport_Click();
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						if (Grid.Row > 0)
						{
							mnuDelete_Click();
						}
						break;
					}
			}
			//end switch
		}

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			switch (Grid.Col)
			{
				case CNSTGRIDCOLINCLUDE:
					{
						Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						Grid.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			int intReportNum;
			int intOldIndex;
			clsDRWrapper dbTemp = new clsDRWrapper();
			int x;
			if (Grid.Row < 1)
			{
				MessageBox.Show("There is no Report currently selected to delete", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (MessageBox.Show("Are you sure you want to delete this report?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			{
				return;
			}
			// delete from group box
			intReportNum = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLREPORTNUMBER))));
			Grid.RemoveItem(Grid.Row);
			// delete from database
			dbTemp.Execute("Delete  from reporttitles where reportnumber = " + FCConvert.ToString(intReportNum), modGlobalVariables.strREDatabase);
			dbTemp.Execute("Delete  from reporttable where reportnumber = " + FCConvert.ToString(intReportNum), modGlobalVariables.strREDatabase);
		}

		public void mnuDelete_Click()
		{
			mnuDelete_Click(mnuDelete, new System.EventArgs());
		}

		private void mnuEditReport_Click(object sender, System.EventArgs e)
		{
			int intReportNum;
			string strTemp = "";
			int intTemp;
			if (Grid.Row < 1)
			{
				MessageBox.Show("No group selected to edit", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// strTemp = lstGroups.List(lstGroups.listindex)
			// intTemp = InStr(1, strTemp, " ", vbTextCompare)
			// If intTemp > 1 Then strTemp = Left(strTemp, intTemp - 1)
			intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLREPORTNUMBER))));
			intReportNum = intTemp;
			frmNewAssessReports.InstancePtr.Init(intReportNum);
			RefillGrid();
		}

		public void mnuEditReport_Click()
		{
			mnuEditReport_Click(mnuEditReport, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuExtract_Click(object sender, System.EventArgs e)
		{
			cmdExtract_Click();
		}

		private void SetupGrid()
		{
			// sets up the grid
			Grid.Rows = 1;
			Grid.Cols = 6;
			Grid.TextMatrix(0, CNSTGRIDCOLINCLUDE, "Include");
			Grid.TextMatrix(0, CNSTGRIDCOLREPORTNUMBER, "#");
			Grid.TextMatrix(0, CNSTGRIDCOLREPORTTITLE, "Report");
			Grid.TextMatrix(0, CNSTGRIDCOLREPORTDESCRIPTION, "Description");
			Grid.TextMatrix(0, CNSTGRIDCOLUSERNAME, "User ID");
			Grid.ColHidden(CNSTGRIDCOLUSERID, true);
			Grid.ColDataType(CNSTGRIDCOLINCLUDE, FCGrid.DataTypeSettings.flexDTBoolean);

            //FC:FINAL:CHN: Set ColDataType to correct column ordering.
            Grid.ColDataType(CNSTGRIDCOLREPORTNUMBER, FCGrid.DataTypeSettings.flexDTLong);

            Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLINCLUDE, FCConvert.ToInt32(0.08 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLREPORTNUMBER, FCConvert.ToInt32(0.06 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLREPORTTITLE, FCConvert.ToInt32(0.37 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLREPORTDESCRIPTION, FCConvert.ToInt32(0.36 * GridWidth));
			// .ColWidth(CNSTGRIDCOLUSERNAME) = 0.16 * GridWidth
		}

		private void mnuNewReport_Click(object sender, System.EventArgs e)
		{
			// frmAssessReports.Init (0)
			frmNewAssessReports.InstancePtr.Init(0);
			RefillGrid();
			Grid.TopRow = Grid.Rows - 1;
		}

		private void FillGrid()
		{
			// loads all of the reports
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsIDs = new clsDRWrapper();
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				clsIDs.OpenRecordset("select * from Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' order by id", "ClientSettings");
				Grid.Rows = 1;
				clsLoad.OpenRecordset("select * from  reporttitles order by reportnumber", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLINCLUDE, FCConvert.ToString(false));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTNUMBER, FCConvert.ToString(clsLoad.Get_Fields_Int32("reportnumber")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTTITLE, FCConvert.ToString(clsLoad.Get_Fields_String("Title")));
					// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("userid"))));
					// If clsIDs.FindFirstRecord("id", Val(clsLoad.Fields("userid"))) Then
					// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
					if (clsIDs.FindFirst("id = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("userid")))))
					{
						// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
						Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERNAME, FCConvert.ToString(clsIDs.Get_Fields("userid")));
					}
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void RefillGrid()
		{
			try
			{
				// reloads grid so as to leave check boxes as they are
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsIDs = new clsDRWrapper();
				int lngRow = 0;
				clsIDs.OpenRecordset("select * from Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' order by id", "ClientSettings");
				clsLoad.OpenRecordset("select * from  reporttitles order by reportnumber", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					lngRow = Grid.FindRow(clsLoad.Get_Fields_Int32("reportnumber"), -1, CNSTGRIDCOLREPORTNUMBER);
					if (lngRow < 0)
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLINCLUDE, FCConvert.ToString(false));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTNUMBER, FCConvert.ToString(clsLoad.Get_Fields_Int32("reportnumber")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTTITLE, FCConvert.ToString(clsLoad.Get_Fields_String("Title")));
						// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
						Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("userid"))));
						// If clsIDs.FindFirstRecord("id", Val(clsLoad.Fields("userid"))) Then
						// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
						if (clsIDs.FindFirst("id = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("userid")))))
						{
							// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
							Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERNAME, FCConvert.ToString(clsIDs.Get_Fields("userid")));
						}
						else
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERNAME, "");
						}
					}
					else
					{
						Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLREPORTTITLE, FCConvert.ToString(clsLoad.Get_Fields_String("Title")));
						// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
						Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("userid"))));
						// If clsIDs.FindFirstRecord("id", Val(clsLoad.Fields("userid"))) Then
						// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
						if (clsIDs.FindFirst("id = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("userid")))))
						{
							// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
							Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERNAME, FCConvert.ToString(clsIDs.Get_Fields("userid")));
						}
						else
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERNAME, "");
						}
					}
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				//ErrorHandler:;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In RefillGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void optReports_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			int lngID;
			int x;
			if (Grid.Rows <= 1)
				return;
			lngID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			switch (Index)
			{
				case 0:
					{
						// show all reports
						for (x = 1; x <= Grid.Rows - 1; x++)
						{
							Grid.RowHidden(x, false);
						}
						// x
						break;
					}
				case 1:
					{
						// show only those that match this user
						for (x = 1; x <= Grid.Rows - 1; x++)
						{
							if (FCConvert.ToDouble(Grid.TextMatrix(x, CNSTGRIDCOLUSERID)) == lngID)
							{
								Grid.RowHidden(x, false);
							}
							else
							{
								Grid.RowHidden(x, true);
							}
						}
						// x
						break;
					}
			}
			//end switch
		}

		private void optReports_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReports.SelectedIndex;
			optReports_CheckedChanged(index, sender, e);
		}

		private void cmbReports_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbReports.SelectedIndex == 0)
			{
				optReports_CheckedChanged(sender, e);
			}
			else if (cmbReports.SelectedIndex == 1)
			{
				optReports_CheckedChanged(sender, e);
			}
		}
	}
}
