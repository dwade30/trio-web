﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetAccount.
	/// </summary>
	partial class frmGetAccount : BaseForm
	{
		public fecherFoundation.FCComboBox cmbtsearchtype;
		public fecherFoundation.FCLabel lbltsearchtype;
		public fecherFoundation.FCComboBox cmbtcontain;
		public fecherFoundation.FCLabel lbltcontain;
		public fecherFoundation.FCComboBox cmbtrecordtype;
		public fecherFoundation.FCLabel lbltrecordtype;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCTextBox txtSearch2;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCCheckBox chkShowAll;
		public fecherFoundation.FCCheckBox chkPrevOwner;
		public fecherFoundation.FCCheckBox chkShowDel;
		public fecherFoundation.FCLabel lblBook;
		public fecherFoundation.FCLabel lblPage;
		public fecherFoundation.FCLabel lblSearchInfo;
		public Global.T2KDateBox T2KPendingDate;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetAccount));
			this.cmbtsearchtype = new fecherFoundation.FCComboBox();
			this.lbltsearchtype = new fecherFoundation.FCLabel();
			this.cmbtcontain = new fecherFoundation.FCComboBox();
			this.lbltcontain = new fecherFoundation.FCLabel();
			this.cmbtrecordtype = new fecherFoundation.FCComboBox();
			this.lbltrecordtype = new fecherFoundation.FCLabel();
			this.cmdGetAccountNumber = new fecherFoundation.FCButton();
			this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.txtSearch2 = new fecherFoundation.FCTextBox();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.cmdClear = new fecherFoundation.FCButton();
			this.Frame5 = new fecherFoundation.FCFrame();
			this.chkShowAll = new fecherFoundation.FCCheckBox();
			this.chkPrevOwner = new fecherFoundation.FCCheckBox();
			this.chkShowDel = new fecherFoundation.FCCheckBox();
			this.lblBook = new fecherFoundation.FCLabel();
			this.lblPage = new fecherFoundation.FCLabel();
			this.lblSearchInfo = new fecherFoundation.FCLabel();
			this.T2KPendingDate = new Global.T2KDateBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.javascript1 = new Wisej.Web.JavaScript(this.components);
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
			this.Frame5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrevOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowDel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KPendingDate)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdGetAccountNumber);
			this.BottomPanel.Location = new System.Drawing.Point(0, 653);
			this.BottomPanel.Size = new System.Drawing.Size(1301, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.T2KPendingDate);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.cmbtcontain);
			this.ClientArea.Controls.Add(this.cmbtrecordtype);
			this.ClientArea.Controls.Add(this.lbltrecordtype);
			this.ClientArea.Controls.Add(this.lbltcontain);
			this.ClientArea.Controls.Add(this.txtSearch);
			this.ClientArea.Controls.Add(this.txtGetAccountNumber);
			this.ClientArea.Controls.Add(this.cmbtsearchtype);
			this.ClientArea.Controls.Add(this.lbltsearchtype);
			this.ClientArea.Controls.Add(this.Frame5);
			this.ClientArea.Controls.Add(this.txtSearch2);
			this.ClientArea.Controls.Add(this.lblBook);
			this.ClientArea.Controls.Add(this.lblPage);
			this.ClientArea.Controls.Add(this.lblSearchInfo);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1301, 593);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Controls.Add(this.cmdSearch);
			this.TopPanel.Size = new System.Drawing.Size(1301, 60);
			this.TopPanel.TabIndex = 0;
			this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(147, 30);
			this.HeaderText.Text = "Get Account";
			// 
			// cmbtsearchtype
			// 
			this.cmbtsearchtype.Items.AddRange(new object[] {
            "Name",
            "Location",
            "Map / Lot",
            "Book / Page",
            "Ref 1",
            "Ref 2",
            "Open 1",
            "Open 2",
            "Sale Date Range",
            "Sale Date"});
			this.cmbtsearchtype.Location = new System.Drawing.Point(200, 150);
			this.cmbtsearchtype.Name = "cmbtsearchtype";
			this.cmbtsearchtype.Size = new System.Drawing.Size(240, 40);
			this.cmbtsearchtype.TabIndex = 3;
			this.cmbtsearchtype.Text = "Name";
			this.cmbtsearchtype.SelectedIndexChanged += new System.EventHandler(this.cmbtsearchtype_SelectedIndexChanged);
			// 
			// lbltsearchtype
			// 
			this.lbltsearchtype.AutoSize = true;
			this.lbltsearchtype.Location = new System.Drawing.Point(30, 164);
			this.lbltsearchtype.Name = "lbltsearchtype";
			this.lbltsearchtype.Size = new System.Drawing.Size(85, 16);
			this.lbltsearchtype.TabIndex = 2;
			this.lbltsearchtype.Text = "SEARCH BY";
			// 
			// cmbtcontain
			// 
			this.cmbtcontain.Items.AddRange(new object[] {
            "Contain Search Criteria",
            "Start With Search Criteria",
            "End With Search Criteria"});
			this.cmbtcontain.Location = new System.Drawing.Point(682, 210);
			this.cmbtcontain.Name = "cmbtcontain";
			this.cmbtcontain.Size = new System.Drawing.Size(240, 40);
			this.cmbtcontain.TabIndex = 14;
			this.cmbtcontain.Text = "Contain Search Criteria";
			this.cmbtcontain.SelectedIndexChanged += new System.EventHandler(this.cmbtcontain_SelectedIndexChanged);
			// 
			// lbltcontain
			// 
			this.lbltcontain.AutoSize = true;
			this.lbltcontain.Location = new System.Drawing.Point(497, 224);
			this.lbltcontain.Name = "lbltcontain";
			this.lbltcontain.Size = new System.Drawing.Size(174, 16);
			this.lbltcontain.TabIndex = 13;
			this.lbltcontain.Text = "DISPLAY RECORDS THAT";
			// 
			// cmbtrecordtype
			// 
			this.cmbtrecordtype.Items.AddRange(new object[] {
            "Regular Records",
            "Sales Records",
            "Previous Owners",
            "Open for Pending Changes"});
			this.cmbtrecordtype.Location = new System.Drawing.Point(200, 90);
			this.cmbtrecordtype.Name = "cmbtrecordtype";
			this.cmbtrecordtype.Size = new System.Drawing.Size(263, 40);
			this.cmbtrecordtype.TabIndex = 9;
			this.cmbtrecordtype.Text = "Regular Records";
			this.cmbtrecordtype.SelectedIndexChanged += new System.EventHandler(this.cmbtrecordtype_SelectedIndexChanged);
			// 
			// lbltrecordtype
			// 
			this.lbltrecordtype.AutoSize = true;
			this.lbltrecordtype.Location = new System.Drawing.Point(30, 104);
			this.lbltrecordtype.Name = "lbltrecordtype";
			this.lbltrecordtype.Size = new System.Drawing.Size(105, 16);
			this.lbltrecordtype.TabIndex = 8;
			this.lbltrecordtype.Text = "RECORD TYPE";
			// 
			// cmdGetAccountNumber
			// 
			this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
			this.cmdGetAccountNumber.Location = new System.Drawing.Point(521, 30);
			this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
			this.cmdGetAccountNumber.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdGetAccountNumber.Size = new System.Drawing.Size(172, 48);
			this.cmdGetAccountNumber.Text = "Process";
			this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
			// 
			// txtGetAccountNumber
			// 
			this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtGetAccountNumber.Location = new System.Drawing.Point(200, 30);
			this.txtGetAccountNumber.Name = "txtGetAccountNumber";
			this.txtGetAccountNumber.Size = new System.Drawing.Size(120, 40);
			this.txtGetAccountNumber.TabIndex = 1;
			this.txtGetAccountNumber.Enter += new System.EventHandler(this.txtGetAccountNumber_Enter);
			this.txtGetAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGetAccountNumber_KeyDown);
			// 
			// txtSearch
			// 
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			clientEvent1.Event = "keypress";
			clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
			this.javascript1.GetJavaScriptEvents(this.txtSearch).Add(clientEvent1);
			this.txtSearch.Location = new System.Drawing.Point(763, 90);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(224, 40);
			this.txtSearch.TabIndex = 7;
			this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
			this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
			// 
			// txtSearch2
			// 
			this.txtSearch2.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch2.Location = new System.Drawing.Point(552, 90);
			this.txtSearch2.Name = "txtSearch2";
			this.txtSearch2.Size = new System.Drawing.Size(84, 40);
			this.txtSearch2.TabIndex = 5;
			this.txtSearch2.Visible = false;
			this.txtSearch2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch2_KeyDown);
			// 
			// cmdSearch
			// 
			this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSearch.ImageSource = "button-search";
			this.cmdSearch.Location = new System.Drawing.Point(1201, 29);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(81, 24);
			this.cmdSearch.TabIndex = 2;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.Location = new System.Drawing.Point(1087, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(100, 24);
			this.cmdClear.TabIndex = 1;
			this.cmdClear.Text = "Clear Search";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// Frame5
			// 
			this.Frame5.AppearanceKey = "groupBoxNoBorders";
			this.Frame5.Controls.Add(this.chkShowAll);
			this.Frame5.Controls.Add(this.chkPrevOwner);
			this.Frame5.Controls.Add(this.chkShowDel);
			this.Frame5.Location = new System.Drawing.Point(487, 148);
			this.Frame5.Name = "Frame5";
			this.Frame5.Size = new System.Drawing.Size(653, 47);
			this.Frame5.TabIndex = 10;
			// 
			// chkShowAll
			// 
			this.chkShowAll.Location = new System.Drawing.Point(465, 10);
			this.chkShowAll.Name = "chkShowAll";
			this.chkShowAll.Size = new System.Drawing.Size(132, 23);
			this.chkShowAll.TabIndex = 2;
			this.chkShowAll.Text = "Show All Matches";
			this.ToolTip1.SetToolTip(this.chkShowAll, "Show the same account multiple times if, for instance, you match on both owners");
			// 
			// chkPrevOwner
			// 
			this.chkPrevOwner.Location = new System.Drawing.Point(243, 10);
			this.chkPrevOwner.Name = "chkPrevOwner";
			this.chkPrevOwner.Size = new System.Drawing.Size(168, 23);
			this.chkPrevOwner.TabIndex = 1;
			this.chkPrevOwner.Text = "Include Previous Owner";
			// 
			// chkShowDel
			// 
			this.chkShowDel.Location = new System.Drawing.Point(10, 10);
			this.chkShowDel.Name = "chkShowDel";
			this.chkShowDel.Size = new System.Drawing.Size(178, 23);
			this.chkShowDel.TabIndex = 3;
			this.chkShowDel.Text = "Include Deleted Accounts";
			// 
			// lblBook
			// 
			this.lblBook.Location = new System.Drawing.Point(469, 104);
			this.lblBook.Name = "lblBook";
			this.lblBook.Size = new System.Drawing.Size(45, 17);
			this.lblBook.TabIndex = 4;
			this.lblBook.Text = "BOOK";
			this.lblBook.Visible = false;
			// 
			// lblPage
			// 
			this.lblPage.Location = new System.Drawing.Point(668, 104);
			this.lblPage.Name = "lblPage";
			this.lblPage.Size = new System.Drawing.Size(56, 17);
			this.lblPage.TabIndex = 6;
			this.lblPage.Text = "PAGE";
			this.lblPage.Visible = false;
			// 
			// lblSearchInfo
			// 
			this.lblSearchInfo.AutoSize = true;
			this.lblSearchInfo.Location = new System.Drawing.Point(684, 7);
			this.lblSearchInfo.Name = "lblSearchInfo";
			this.lblSearchInfo.Size = new System.Drawing.Size(179, 16);
			this.lblSearchInfo.TabIndex = 27;
			this.lblSearchInfo.Text = "ENTER SEARCH CRITERIA";
			this.lblSearchInfo.Visible = false;
			// 
			// T2KPendingDate
			// 
			this.T2KPendingDate.Location = new System.Drawing.Point(200, 210);
			this.T2KPendingDate.Mask = "##/##/####";
			this.T2KPendingDate.MaxLength = 10;
			this.T2KPendingDate.Name = "T2KPendingDate";
			this.T2KPendingDate.Size = new System.Drawing.Size(263, 22);
			this.T2KPendingDate.TabIndex = 12;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(26, 224);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(155, 18);
			this.Label3.TabIndex = 11;
			this.Label3.Text = "MINIMUM DATE TO APPLY";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(200, 5);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(368, 18);
			this.Label1.TabIndex = 29;
			this.Label1.Text = "ENTER AN ACCOUNT NUMBER OR 0 TO ADD A NEW ACCOUNT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(200, 18);
			this.Label2.TabIndex = 28;
			this.Label2.Text = "LAST ACCOUNT ACCESSED";
			// 
			// frmGetAccount
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1301, 761);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmGetAccount";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Get Account";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmGetAccount_Load);
			this.Activated += new System.EventHandler(this.frmGetAccount_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetAccount_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
			this.Frame5.ResumeLayout(false);
			this.Frame5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrevOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowDel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.T2KPendingDate)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private JavaScript javascript1;
		private JavaScript.ClientEvent keyPressOnlyNumbersEvent;
	}
}
