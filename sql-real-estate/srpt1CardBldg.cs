﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srpt1CardBldg.
	/// </summary>
	public partial class srpt1CardBldg : FCSectionReport
	{
		public static srpt1CardBldg InstancePtr
		{
			get
			{
				return (srpt1CardBldg)Sys.GetInstance(typeof(srpt1CardBldg));
			}
		}

		protected srpt1CardBldg _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srpt1CardBldg()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srpt1CardBldg	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngAcct;
		int intCard;
		bool boolPrintTest;
		int intIndex;
		double dblTotalBldg;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(((rpt1PagePropertyCard)this.ParentReport).CurrentAccount)));
			intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(((rpt1PagePropertyCard)this.ParentReport).CurrentCard)));
			boolPrintTest = ((rpt1PagePropertyCard)this.ParentReport).PrintTest;
			intIndex = 1;
			dblTotalBldg = 0;
			txtSoundValue.Visible = false;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = intIndex > ((rpt1PagePropertyCard)this.ParentReport).BldgSummary.Count;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intIndex <= ((rpt1PagePropertyCard)this.ParentReport).BldgSummary.Count)
			{
				txtDesc.Text = ((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].Description;
				txtValue.Text = ((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].BldgValue;
				if (((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].Area != "    - - - - S O U N D  V A L U E - - - -    ")
				{
					txtArea.Text = ((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].Area;
					txtGrade.Text = ((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].Grade;
					txtCondition.Text = ((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].Condition;
					txtPhys.Text = ((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].Phys;
					txtFunc.Text = ((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].Func;
					txtEcon.Text = ((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].Econ;
					txtFloors.Text = ((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].Stories;
					txtSoundValue.Visible = false;
				}
				else
				{
					txtArea.Text = "";
					txtGrade.Text = "";
					txtCondition.Text = "";
					txtPhys.Text = "";
					txtFunc.Text = "";
					txtEcon.Text = "";
					txtFloors.Text = "";
					txtSoundValue.Visible = true;
				}
				if (((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].BldgValue != "")
				{
					dblTotalBldg += FCConvert.ToDouble(((rpt1PagePropertyCard)this.ParentReport).BldgSummary[intIndex].BldgValue);
				}
				intIndex += 1;
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalValue.Text = Strings.Format(dblTotalBldg, "#,###,###,##0");
		}

		
	}
}
