﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptpdfPropertyCard1.
	/// </summary>
	public partial class srptpdfPropertyCard1 : FCSectionReport
	{
		public static srptpdfPropertyCard1 InstancePtr
		{
			get
			{
				return (srptpdfPropertyCard1)Sys.GetInstance(typeof(srptpdfPropertyCard1));
			}
		}

		protected srptpdfPropertyCard1 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsTemp?.Dispose();
                clsTemp = null;
            }
			base.Dispose(disposing);
		}

		public srptpdfPropertyCard1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptpdfPropertyCard1	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsTemp = new clsDRWrapper();
		int lngAcct;
		int intCard;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			lngAcct = FCConvert.ToInt32((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("rsaccount"));
			intCard = FCConvert.ToInt32((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("rscard"));
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "M/dd/yyyy");
			FillCardFromCostFiles();
		}

		private void FillCardFromCostFiles()
		{
			// fill the descriptions from the cost files already opened in the parent report
			string strTemp = "";
			// vbPorter upgrade warning: intTemp As short --> As int	OnWrite(double, int)
			int intTemp = 0;
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                int x;
                (this.ParentReport as rptpdfPropertyCard).clsCost.MoveFirst();
                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1091))
                {
                    strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc");
                    strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                    strTemp = Strings.Trim(strTemp);
                    if (strTemp != string.Empty)
                    {
                        (this.Detail.Controls["lblxcoord"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text =
                            strTemp;
                    }
                    else
                    {
                        (this.Detail.Controls["lblxcoord"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text =
                            "X Coordinate";
                    }
                }
                else
                {
                    (this.Detail.Controls["lblxcoord"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text =
                        "X Coordinate";
                }

                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1092))
                {
                    strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc");
                    strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                    strTemp = Strings.Trim(strTemp);
                    if (strTemp != string.Empty)
                    {
                        (this.Detail.Controls["lblycoord"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text =
                            strTemp;
                    }
                    else
                    {
                        (this.Detail.Controls["lblycoord"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text =
                            "Y Coordinate";
                    }
                }
                else
                {
                    (this.Detail.Controls["lblycoord"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text =
                        "Y Coordinate";
                }

                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1301))
                {
                    while ((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber") < 1310)
                    {
                        strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        strTemp = Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        intTemp = FCConvert.ToInt32(
                            Conversion.Val(
                                (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber")) -
                            1300);
                        (this.Detail.Controls["txtTopCost" + intTemp] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            FCConvert.ToString(intTemp) + "." + strTemp;
                        (this.ParentReport as rptpdfPropertyCard).clsCost.MoveNext();
                    }
                }

                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1311))
                {
                    while ((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber") < 1320)
                    {
                        strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        strTemp = Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        intTemp = FCConvert.ToInt32(
                            Conversion.Val(
                                (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber")) -
                            1310);
                        (this.Detail.Controls["txtUtilCost" + intTemp] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            FCConvert.ToString(intTemp) + "." + strTemp;
                        (this.ParentReport as rptpdfPropertyCard).clsCost.MoveNext();
                    }
                }

                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1321))
                {
                    while ((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber") < 1330)
                    {
                        strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        strTemp = Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        intTemp = FCConvert.ToInt32(
                            Conversion.Val(
                                (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber")) -
                            1320);
                        (this.Detail.Controls["txtStreetCost" + intTemp] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            FCConvert.ToString(intTemp) + "." + strTemp;
                        (this.ParentReport as rptpdfPropertyCard).clsCost.MoveNext();
                    }
                }

                // If Me.ParentReport.clsCost.FindFirstRecord("crecordnumber", 1330) Then
                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindFirst("Crecordnumber = 1330"))
                {
                    strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc");
                    strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                    strTemp = Strings.Trim(strTemp);
                    (this.Detail.Controls["lblopen1"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text =
                        strTemp;
                }

                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1340))
                {
                    strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc");
                    strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                    strTemp = Strings.Trim(strTemp);
                    (this.Detail.Controls["lblopen2"] as GrapeCity.ActiveReports.SectionReportModel.Label).Text =
                        strTemp;
                }

                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1351))
                {
                    while ((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber") < 1360)
                    {
                        strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        strTemp = Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        intTemp = FCConvert.ToInt32(
                            Conversion.Val(
                                (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber")) -
                            1350);
                        (this.Detail.Controls["txtSaleTypeCost" + intTemp] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            FCConvert.ToString(intTemp) + "." + strTemp;
                        (this.ParentReport as rptpdfPropertyCard).clsCost.MoveNext();
                    }
                }

                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1361))
                {
                    while ((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber") < 1370)
                    {
                        strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        strTemp = Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        intTemp = FCConvert.ToInt32(
                            Conversion.Val(
                                (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber")) -
                            1360);
                        (this.Detail.Controls["txtFinancingCost" + intTemp] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            FCConvert.ToString(intTemp) + "." + strTemp;
                        (this.ParentReport as rptpdfPropertyCard).clsCost.MoveNext();
                    }
                }

                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1371))
                {
                    while ((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber") < 1380)
                    {
                        strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        strTemp = Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        intTemp = FCConvert.ToInt32(
                            Conversion.Val(
                                (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber")) -
                            1370);
                        (this.Detail.Controls["txtVerifiedCost" + intTemp] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            FCConvert.ToString(intTemp) + "." + strTemp;
                        (this.ParentReport as rptpdfPropertyCard).clsCost.MoveNext();
                    }
                }

                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1381))
                {
                    while ((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber") < 1390)
                    {
                        strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        strTemp = Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        intTemp = FCConvert.ToInt32(
                            Conversion.Val(
                                (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber")) -
                            1380);
                        (this.Detail.Controls["txtvalidityCost" + intTemp] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            FCConvert.ToString(intTemp) + "." + strTemp;
                        (this.ParentReport as rptpdfPropertyCard).clsCost.MoveNext();
                    }
                }

                if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    if (!(this.ParentReport as rptpdfPropertyCard).clsProp.EndOfFile())
                    {
                        lblMuni.Text = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt32(
                            Conversion.Val(
                                (this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("ritrancode"))));
                    }
                }

                // land
                x = 11;
                clsTemp.OpenRecordset("select * from landtype order by code", modGlobalVariables.strREDatabase);
                while (!clsTemp.EndOfFile() && x < 47)
                {
                    strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("description"));
                    strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                    strTemp = Strings.Trim(strTemp);
                    if (strTemp.Length > 14)
                        strTemp = Strings.Mid(strTemp, 1, 14);
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields("code"))));
                    (this.Detail.Controls["txtLandDesc" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text
                        = FCConvert.ToString(intTemp) + "." + strTemp;
                    x += 1;
                    clsTemp.MoveNext();
                }

                intTemp = x;
                for (x = intTemp; x <= 46; x++)
                {
                    (this.Detail.Controls["txtlanddesc" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                        .Text = "";
                }

                // x
                // If Me.ParentReport.clsCost.FindNextRecord("crecordnumber", 1391) Then
                // Do While Me.ParentReport.clsCost.Fields("crecordnumber") < 1427
                // strTemp = Me.ParentReport.clsCost.Fields("cldesc")
                // strTemp = Replace(strTemp, ".", " ", , , vbTextCompare)
                // strTemp = Trim(strTemp)
                // If Len(strTemp) > 14 Then strTemp = Mid(strTemp, 1, 14)
                // intTemp = Val(Me.ParentReport.clsCost.Fields("crecordnumber")) - 1380
                // Me.Detail.Controls["txtLandDesc" & intTemp] = intTemp & "." & strTemp
                // Me.ParentReport.clsCost.MoveNext
                // Loop
                // End If
                // If Me.ParentReport.clsCost.FindFirstRecord("crecordnumber", 1441) Then
                if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindFirst("Crecordnumber = 1441"))
                {
                    while ((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber") < 1450)
                    {
                        strTemp = (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("CLDESC");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        strTemp = Strings.Trim(strTemp);
                        if (strTemp.Length > 14)
                            strTemp = Strings.Mid(strTemp, 1, 14);
                        intTemp = FCConvert.ToInt32(
                            Conversion.Val(
                                (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_Int32("crecordnumber")) -
                            1440);
                        (this.Detail.Controls["txtInfluenceCost" + intTemp] as
                                GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            FCConvert.ToString(intTemp) + "." + strTemp;
                        (this.ParentReport as rptpdfPropertyCard).clsCost.MoveNext();
                    }
                }
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			// vbPorter upgrade warning: intTemp As short --> As int	OnRead(string)
			int intTemp = 0;
			// vbPorter upgrade warning: strTemp As string	OnWrite(string, int)
			string strTemp = "";
			int intFrontFoot = 0;
			int intSquareFoot = 0;
			int intAcre = 0;
			int[] intLandType = new int[7 + 1];
			double dblTemp = 0;
			// vbPorter upgrade warning: lngMaxYear As int	OnWrite(int, bool)
			int lngMaxYear = 0;
			bool boolAlwaysShowBilling = false;
			// show current bill info even if not in previous table?
			bool boolAlwaysShowCurrent = false;
			bool boolShowCurrent = false;
			// do we need to show current this time or is it already in the recordset?
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp = 0;
			bool boolNeverShowPrevious = false;
			int lngCurYear = 0;
			if (!(this.ParentReport as rptpdfPropertyCard).clsProp.EndOfFile())
			{
				lngCurYear = modGlobalVariables.Statics.CustomizedInfo.BillYear;
				if (lngCurYear == 0)
					lngCurYear = DateTime.Today.Year;
				boolAlwaysShowBilling = modGlobalVariables.Statics.CustomizedInfo.boolAlwaysShowCurrentOnPropertyCard;
				boolAlwaysShowCurrent = modGlobalVariables.Statics.CustomizedInfo.boolAlwaysShowCurrentCurrentOnPropertyCard;
				boolShowCurrent = false;
				boolNeverShowPrevious = modGlobalVariables.Statics.CustomizedInfo.boolNeverShowPreviousBillingOnPropertyCard;
				// previous assessment
				x = 0;
				if (boolAlwaysShowBilling || boolAlwaysShowCurrent)
				{
					clsTemp.OpenRecordset("select * from previousassessment where account = " + FCConvert.ToString(lngAcct) + " and card = " + FCConvert.ToString(intCard) + " and taxyear <> " + FCConvert.ToString(lngCurYear) + " order by taxyear", modGlobalVariables.strREDatabase);
				}
				else
				{
					clsTemp.OpenRecordset("select * from previousassessment where account = " + FCConvert.ToString(lngAcct) + " and card = " + FCConvert.ToString(intCard) + "  order by taxyear", modGlobalVariables.strREDatabase);
				}
				if (!clsTemp.EndOfFile())
				{
					clsTemp.MoveLast();
					lngMaxYear = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("taxyear"))));
					if (lngMaxYear < 1000)
					{
						if (lngMaxYear > 88)
						{
							lngMaxYear += 1900;
						}
						else
						{
							lngMaxYear = (lngMaxYear == 2000 ? -1 : 0);
						}
					}
					clsTemp.MoveFirst();
					intTemp = clsTemp.RecordCount();
				}
				// can only do 14 so move recordset so we get 14 most recent
				if (intTemp > 14)
				{
					for (x = 1; x <= (intTemp - 14); x++)
					{
						clsTemp.MoveNext();
					}
					// x
					if (boolAlwaysShowBilling)
					{
						if (modGlobalVariables.Statics.CustomizedInfo.BillYear > 1995)
						{
							// is a legit year
							// If CustomizedInfo.BillYear > lngMaxYear Then
							// move ahead another one to allow for this years data
							boolShowCurrent = true;
							clsTemp.MoveNext();
							// End If
						}
						else if (modGlobalVariables.Statics.CustomizedInfo.BillYear == 0)
						{
							// If Year(Date) > lngMaxYear Then
							boolShowCurrent = true;
							clsTemp.MoveNext();
							// End If
						}
					}
					else if (boolAlwaysShowCurrent)
					{
						if (lngCurYear > 1995)
						{
							boolShowCurrent = true;
							clsTemp.MoveNext();
						}
					}
				}
				else
				{
					if (boolAlwaysShowBilling)
					{
						if (modGlobalVariables.Statics.CustomizedInfo.BillYear > 1995)
						{
							// If CustomizedInfo.BillYear > lngMaxYear Then
							boolShowCurrent = true;
							// End If
						}
						else if (modGlobalVariables.Statics.CustomizedInfo.BillYear == 0)
						{
							// If Year(Date) > lngMaxYear Then
							boolShowCurrent = true;
							// End If
						}
					}
					else if (boolAlwaysShowCurrent)
					{
						if (lngCurYear > 1995)
						{
							boolShowCurrent = true;
						}
					}
					// End If
					if (intTemp == 14)
					{
						if (boolShowCurrent)
						{
							clsTemp.MoveNext();
						}
					}
				}
				x = 0;
				while (!clsTemp.EndOfFile())
				{
					if (!boolNeverShowPrevious || ((FCConvert.ToInt32(clsTemp.Get_Fields_Int32("taxyear")) == modGlobalVariables.Statics.CustomizedInfo.BillYear) && boolAlwaysShowBilling))
					{
						x += 1;
						if (x > 14)
							break;
						// shouldn't be able to happen
						(this.Detail.Controls["txtAssYear" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(clsTemp.Get_Fields_Int32("taxyear"));
						(this.Detail.Controls["txtAssLand" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTemp.Get_Fields_Int32("Land"), "#,###,###,##0");
						(this.Detail.Controls["txtAssBldg" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTemp.Get_Fields_Int32("Building"), "#,###,###,##0");
						(this.Detail.Controls["txtAssExempt" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTemp.Get_Fields_Int32("Exempt"), "#,###,###,##0");
						(this.Detail.Controls["txtAssTotal" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTemp.Get_Fields_Int32("Assessment"), "#,###,###,##0");
					}
					clsTemp.MoveNext();
				}
				if (boolShowCurrent)
				{
					x += 1;
					if (x <= 14)
					{
						// shouldn't be able to be > 14
						clsTemp.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
						if (!clsTemp.EndOfFile())
						{
							// If CustomizedInfo.BillYear > 1995 Then
							// .Controls("txtassyear" & x).Text = CustomizedInfo.BillYear
							// ElseIf CustomizedInfo.BillYear = 0 Then
							// .Controls("txtassyear" & x).Text = Year(Date)
							// End If
							(this.Detail.Controls["txtassyear" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(lngCurYear);
							if (boolAlwaysShowBilling)
							{
								(this.Detail.Controls["txtassland" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTemp.Get_Fields_Int32("lastlandval"), "#,###,###,##0");
								(this.Detail.Controls["txtassbldg" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTemp.Get_Fields_Int32("lastbldgval"), "#,###,###,##0");
								(this.Detail.Controls["txtAssExempt" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTemp.Get_Fields_Int32("rlexemption"), "#,###,###,##0");
								lngTemp = FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields_Int32("lastlandval")) + Conversion.Val(clsTemp.Get_Fields_Int32("lastbldgval")) - Conversion.Val(clsTemp.Get_Fields_Int32("rlexemption")));
							}
							else
							{
								(this.Detail.Controls["txtassyear" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Calc.";
								(this.Detail.Controls["txtassland" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTemp.Get_Fields_Int32("rllandval"), "#,###,###,##0");
								(this.Detail.Controls["txtassbldg" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTemp.Get_Fields_Int32("RLbldgval"), "#,###,###,##0");
								(this.Detail.Controls["txtassexempt" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(clsTemp.Get_Fields_Int32("correxemption"), "#,###,###,##0");
								lngTemp = FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields_Int32("rllandval")) + Conversion.Val(clsTemp.Get_Fields_Int32("rlbldgval")) - Conversion.Val(clsTemp.Get_Fields_Int32("correxemption")));
							}
							(this.Detail.Controls["txtAssTotal" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(lngTemp, "#,###,###,##0");
						}
					}
				}
				intTemp = x + 1;
				for (x = intTemp; x <= 14; x++)
				{
					(this.Detail.Controls["txtassyear" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					(this.Detail.Controls["txtassland" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					(this.Detail.Controls["txtassbldg" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					(this.Detail.Controls["txtassexempt" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					(this.Detail.Controls["txtasstotal" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
				// x
				// currentowner
				txtLine1L1.Text = Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rsname"));
				txtLine2L1.Text = Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rssecowner"));
				txtLine3L1.Text = Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rsaddr1"));
				txtLine4L1.Text = Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rsaddr2"));
				txtLine5L1.Text = Strings.Trim(Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rsaddr3")) + " " + Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rsstate")) + " " + Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rszip")) + " " + Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rszip4")));
				txtLine6L1.Text = Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("Country"));
				if (Strings.LCase(txtLine6L1.Text) == "united states")
				{
					txtLine6L1.Text = "";
				}
				if (txtLine5L1.Text == "")
				{
					txtLine5L1.Text = txtLine6L1.Text;
					txtLine6L1.Text = "";
				}
				if (txtLine4L1.Text == "")
				{
					txtLine4L1.Text = txtLine5L1.Text;
					txtLine5L1.Text = txtLine6L1.Text;
					txtLine6L1.Text = "";
				}
				if (txtLine3L1.Text == "")
				{
					txtLine3L1.Text = txtLine4L1.Text;
					txtLine4L1.Text = txtLine5L1.Text;
					txtLine5L1.Text = txtLine6L1.Text;
					txtLine6L1.Text = "";
				}
				if (txtLine2L1.Text == "")
				{
					txtLine2L1.Text = txtLine3L1.Text;
					txtLine3L1.Text = txtLine4L1.Text;
					txtLine4L1.Text = txtLine5L1.Text;
					txtLine5L1.Text = txtLine6L1.Text;
					txtLine6L1.Text = "";
				}
				if (txtLine6L1.Text == "")
				{
					// book page
					clsTemp.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAcct) + " and [current] = 1 order by line", modGlobalVariables.strREDatabase);
					strTemp = "";
					while (!clsTemp.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
						strTemp += "B" + clsTemp.Get_Fields("book") + "P" + clsTemp.Get_Fields("Page") + " ";
						clsTemp.MoveNext();
					}
					strTemp = Strings.Trim(strTemp);
					txtLine6L1.Text = strTemp;
				}
				for (x = 2; x <= 4; x++)
				{
					(this.Detail.Controls["txtline1l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					(this.Detail.Controls["txtline2l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					(this.Detail.Controls["txtline3l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					(this.Detail.Controls["txtline4l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					(this.Detail.Controls["txtline5l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					(this.Detail.Controls["txtline6l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
				}
				// x
				txtOpen1.Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("piopen1")));
				txtOpen2.Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("piopen2")));
				// previous owner
				x = 1;
				clsTemp.OpenRecordset("select top 3 * from previousowner where account = " + FCConvert.ToString(lngAcct) + " ORDER BY saledate desc", modGlobalVariables.strREDatabase);
				while (!clsTemp.EndOfFile())
				{
					x += 1;
					if (x > 4)
						break;
					(this.Detail.Controls["txtline1l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Previous Owner";
					(this.Detail.Controls["txtline2l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("name")));
					if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("secowner"))) != string.Empty)
					{
						(this.Detail.Controls["txtline3l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("secowner")));
						(this.Detail.Controls["txtline4l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("address1")));
						(this.Detail.Controls["txtline5l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("city"))) + " " + clsTemp.Get_Fields_String("state") + " " + clsTemp.Get_Fields_String("zip") + " " + clsTemp.Get_Fields_String("zip4"));
					}
					else
					{
						(this.Detail.Controls["txtline3l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("address1")));
						(this.Detail.Controls["txtline4l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("address2")));
						(this.Detail.Controls["txtline5l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("city"))) + " " + clsTemp.Get_Fields_String("state") + " " + clsTemp.Get_Fields_String("zip") + " " + clsTemp.Get_Fields_String("zip4"));
					}
					if (Information.IsDate(clsTemp.Get_Fields("saledate")))
					{
						if (clsTemp.Get_Fields_DateTime("saledate").ToOADate() != 0)
						{
							(this.Detail.Controls["txtline6l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Sale Date: " + Strings.Format(clsTemp.Get_Fields_DateTime("saledate"), "M/dd/yyyy");
						}
						else
						{
							(this.Detail.Controls["txtline6l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
						}
					}
					else
					{
						(this.Detail.Controls["txtline6l" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
					}
					clsTemp.MoveNext();
				}
				// notes
				txtNotes.Text = "";
				if (!modGlobalVariables.Statics.CustomizedInfo.boolDontShowCommentsOnPropertyCard)
				{
					clsTemp.OpenRecordset("select * from commrec where crecordnumber = " + FCConvert.ToString(lngAcct) + " and RSCARD = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
					if (!clsTemp.EndOfFile())
					{
						txtNotes.Text = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("CCOMMENT")));
					}
				}
				// MAPLOT
				txtMapLot.Text = Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rsmaplot"));
				txtAccount.Text = lngAcct.ToString();
				strTemp = "";
				if (Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rslocnumalph")) > 0)
				{
					strTemp = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rslocnumalph"))) + Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rslocapt")) + " ";
				}
				strTemp += Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_String("rslocstreet"));
				strTemp = Strings.Trim(strTemp);
				txtLocation.Text = strTemp;
				txtCard.Text = FCConvert.ToString((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("rscard"));
				clsTemp.OpenRecordset("select rsaccount from master where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
				txtCards.Text = FCConvert.ToString(clsTemp.RecordCount());
				(this.ParentReport as rptpdfPropertyCard).clsCost.MoveFirst();
				// zone and secondary zone
				txtZone.Text = "";
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pizone"))));
				if (intTemp > 0)
				{
					if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
					{
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(intTemp, "Property", "Zone");
					}
					else
					{
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(intTemp, "Property", "Zone", FCConvert.ToInt32(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("ritrancode"))));
					}
					// If Me.ParentReport.clsCost.FindNextRecord("crecordnumber", 1000 + intTemp) Then
					strTemp = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
					txtZone.Text = FCConvert.ToString(intTemp) + " " + strTemp;
					// End If
				}
				(this.ParentReport as rptpdfPropertyCard).clsCost.MoveFirst();
				txtSecZone.Text = "";
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("piseczone"))));
				strTemp = FCConvert.ToString(intTemp);
				// If intTemp > 50 Then intTemp = intTemp - 40
				if (intTemp > 0)
				{
					// If Me.ParentReport.clsCost.FindFirstRecord("crecordnumber", 1100 + intTemp) Then
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(intTemp, "Property", "SecZone");
					strTemp += " " + Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
					txtSecZone.Text = strTemp;
					// End If
					// ElseIf intTemp > 11 Then
					// intTemp = intTemp - 40
					// If Me.ParentReport.clsCost.FindFirstRecord("crecordnumber", 1110 + intTemp) Then
					// strTemp = strTemp & " " & Trim(Me.ParentReport.clsCost.Fields("cldesc"))
					// txtSecZone.Text = strTemp
					// End If
				}
				// neighborhood
				txtNeighborhood.Text = "";
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pineighborhood"))));
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					clsTemp.OpenRecordset("select * from neighborhood where code = " + FCConvert.ToString(intTemp), modGlobalVariables.Statics.strRECostFileDatabase);
				}
				else
				{
					clsTemp.OpenRecordset("select * from neighborhood where code = " + FCConvert.ToString(intTemp) + " and townnumber = " + FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("ritrancode"))), modGlobalVariables.Statics.strRECostFileDatabase);
				}
				// If intTemp > 0 And intTemp < 100 Then
				if (!clsTemp.EndOfFile())
				{
					strTemp = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
					txtNeighborhood.Text = FCConvert.ToString(intTemp) + " " + strTemp;
				}
				// End If
				txtTreeGrowth.Text = FCConvert.ToString((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pistreetcode"));
				// TODO Get_Fields: Check the table for the column [pixcoord] and replace with corresponding Get_Field method
				txtXCoord.Text = (this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("pixcoord");
				// TODO Get_Fields: Check the table for the column [piycoord] and replace with corresponding Get_Field method
				txtYCoord.Text = (this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piycoord");
				// topography
				txtTopography1.Text = "";
				txtTopography2.Text = "";
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pitopography1"))));
				if (intTemp > 0 && intTemp < 10)
				{
					if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1300 + intTemp))
					{
						txtTopography1.Text = FCConvert.ToString(intTemp) + " " + Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc"));
					}
				}
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pitopography2"))));
				if (intTemp > 0 && intTemp < 10)
				{
					// If Me.ParentReport.clsCost.FindFirstRecord("crecordnumber", 1300 + intTemp) Then
					if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindFirst("crecordnumber = " + FCConvert.ToString(1300 + intTemp)))
					{
						txtTopography2.Text = FCConvert.ToString(intTemp) + " " + Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc"));
					}
				}
				// utilities
				txtUtility1.Text = "";
				txtUtility2.Text = "";
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("Piutilities1"))));
				if (intTemp > 0 && intTemp < 10)
				{
					if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1310 + intTemp))
					{
						txtUtility1.Text = FCConvert.ToString(intTemp) + " " + Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc"));
					}
				}
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("piutilities2"))));
				if (intTemp > 0 && intTemp < 10)
				{
					// If Me.ParentReport.clsCost.FindFirstRecord("crecordnumber", 1310 + intTemp) Then
					if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindFirst("crecordnumber = " + FCConvert.ToString(1310 + intTemp)))
					{
						txtUtility2.Text = FCConvert.ToString(intTemp) + " " + Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc"));
					}
				}
				txtStreet.Text = "";
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pistreet"))));
				if (intTemp > 0 && intTemp < 10)
				{
					if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1320 + intTemp))
					{
						txtStreet.Text = FCConvert.ToString(intTemp) + " " + Strings.Trim((this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc"));
					}
				}
				// sale information
				txtSaleDate.Text = "";
				if (Information.IsDate((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("saledate")))
				{
					if ((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_DateTime("saledate").ToOADate() != 0)
					{
						txtSaleDate.Text = Strings.Format((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_DateTime("saledate"), "M/dd/yyyy");
					}
				}
				txtSalePrice.Text = "";
				if (Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pisaleprice")) > 0)
				{
					txtSalePrice.Text = Strings.Format((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pisaleprice"), "#,###,###,##0");
				}
				txtSaleType.Text = "";
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pisaletype"))));
				if (intTemp > 0 && intTemp < 10)
				{
					if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1350 + intTemp))
					{
						txtSaleType.Text = FCConvert.ToString(intTemp) + " " + (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc");
					}
				}
				txtSaleFinancing.Text = "";
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pisalefinancing"))));
				if (intTemp > 0 && intTemp < 10)
				{
					if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1360 + intTemp))
					{
						txtSaleFinancing.Text = FCConvert.ToString(intTemp) + " " + (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc");
					}
				}
				txtVerified.Text = "";
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pisaleverified"))));
				if (intTemp > 0 && intTemp < 10)
				{
					if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1370 + intTemp))
					{
						txtVerified.Text = FCConvert.ToString(intTemp) + " " + (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc");
					}
				}
				txtValidity.Text = "";
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Int32("pisalevalidity"))));
				if (intTemp > 0 && intTemp < 10)
				{
					if ((this.ParentReport as rptpdfPropertyCard).clsCost.FindNextRecord("crecordnumber", 1380 + intTemp))
					{
						txtValidity.Text = FCConvert.ToString(intTemp) + " " + (this.ParentReport as rptpdfPropertyCard).clsCost.Get_Fields_String("cldesc");
					}
				}
				// land data
				// set all to be on first line
				intSquareFoot = 1;
				intAcre = 1;
				intFrontFoot = 1;
				for (x = 1; x <= 7; x++)
				{
					// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
					intLandType[x] = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "type"))));
				}
				// x
				for (x = 1; x <= 7; x++)
				{
					if (modGlobalVariables.Statics.LandTypes.FindCode(intLandType[x]))
					{
						switch (modGlobalVariables.Statics.LandTypes.UnitsType)
						{
						// Select Case intLandType(x)
							case modREConstants.CNSTLANDTYPEFRONTFOOT:
							case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
								{
									// Case 11 To 15
									(this.Detail.Controls["txtFrontFootType" + intFrontFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(intLandType[x]);
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									(this.Detail.Controls["txtfrontage" + intFrontFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "unitsA")));
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									(this.Detail.Controls["txtdepth" + intFrontFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb")));
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									(this.Detail.Controls["txtfrontfootfactor" + intFrontFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "inf")));
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									(this.Detail.Controls["txtfrontfootcode" + intFrontFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "infcode")));
									intFrontFoot += 1;
									if (x < 7)
									{
										if (intLandType[x + 1] == 99)
										{
											(this.Detail.Controls["txtfrontfoottype" + intFrontFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "99";
											(this.Detail.Controls["txtfrontage" + intFrontFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
											(this.Detail.Controls["txtdepth" + intFrontFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
											// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
											(this.Detail.Controls["txtfrontfootfactor" + intFrontFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x + 1) + "inf")));
											// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
											(this.Detail.Controls["txtfrontfootcode" + intFrontFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x + 1) + "infcode")));
											intFrontFoot += 1;
										}
									}
									break;
								}
							case modREConstants.CNSTLANDTYPESQUAREFEET:
								{
									// Case 16 To 20
									(this.Detail.Controls["txtsqfttype" + intSquareFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(intLandType[x]);
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									strTemp = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa") + (this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb")));
									strTemp = Strings.Format(strTemp, "#,###,###,##0");
									(this.Detail.Controls["txtsqftunits" + intSquareFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									(this.Detail.Controls["txtsqftfactor" + intSquareFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "inf")));
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									(this.Detail.Controls["txtsqftcode" + intSquareFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "infcode")));
									intSquareFoot += 1;
									if (x < 7)
									{
										if (intLandType[x + 1] == 99)
										{
											(this.Detail.Controls["txtsqfttype" + intSquareFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "99";
											(this.Detail.Controls["txtsqftunits" + intSquareFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
											// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
											(this.Detail.Controls["txtsqftfactor" + intSquareFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x + 1) + "inf")));
											// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
											(this.Detail.Controls["txtsqftcode" + intSquareFoot] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x + 1) + "infcode")));
											intSquareFoot += 1;
										}
									}
									break;
								}
							default:
								{
									// Case 21 To 46
									(this.Detail.Controls["txtAcreageType" + intAcre] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(intLandType[x]);
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									strTemp = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa") + (this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb")));
									dblTemp = Conversion.Val(strTemp);
									dblTemp /= 100;
									strTemp = Strings.Format(dblTemp, "0.00");
									(this.Detail.Controls["txtacreageunits" + intAcre] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									(this.Detail.Controls["txtacreagefactor" + intAcre] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "inf")));
									// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
									(this.Detail.Controls["txtacreagecode" + intAcre] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x) + "infcode")));
									intAcre += 1;
									if (x < 7)
									{
										if (intLandType[x + 1] == 99)
										{
											(this.Detail.Controls["txtacreagetype" + intAcre] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "99";
											(this.Detail.Controls["txtacreageunits" + intAcre] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
											// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
											(this.Detail.Controls["txtacreagefactor" + intAcre] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x + 1) + "inf")));
											// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
											(this.Detail.Controls["txtacreagecode" + intAcre] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields("piland" + FCConvert.ToString(x + 1) + "infcode")));
											intAcre += 1;
										}
									}
								}
								//end switch
								break;
								for (x = intFrontFoot; x <= 7; x++)
								{
									(this.Detail.Controls["txtfrontfoottype" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
									(this.Detail.Controls["txtfrontage" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
									(this.Detail.Controls["txtdepth" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
									(this.Detail.Controls["txtfrontfootfactor" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
									(this.Detail.Controls["txtfrontfootcode" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
								}
								// x
								for (x = intSquareFoot; x <= 7; x++)
								{
									(this.Detail.Controls["txtsqfttype" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
									(this.Detail.Controls["txtsqftunits" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
									(this.Detail.Controls["txtsqftfactor" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
									(this.Detail.Controls["txtsqftcode" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
								}
								// x
								for (x = intAcre; x <= 7; x++)
								{
									(this.Detail.Controls["txtacreagetype" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
									(this.Detail.Controls["txtacreageunits" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
									(this.Detail.Controls["txtacreagefactor" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
									(this.Detail.Controls["txtacreagecode" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
								}
								// x
								txtTotalAcres.Text = Strings.Format(Conversion.Val((this.ParentReport as rptpdfPropertyCard).clsProp.Get_Fields_Double("piacres")), "#,###,##0.00");
						}
					}
				}
			}
		}

		
	}
}
