﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewLandBreakdown.
	/// </summary>
	public partial class rptNewLandBreakdown : BaseSectionReport
	{
		public static rptNewLandBreakdown InstancePtr
		{
			get
			{
				return (rptNewLandBreakdown)Sys.GetInstance(typeof(rptNewLandBreakdown));
			}
		}

		protected rptNewLandBreakdown _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLand?.Dispose();
				clsCost?.Dispose();
				clsLandCode?.Dispose();
                clsCost = null;
                clsLandCode = null;
                clsLand = null;
            }
			base.Dispose(disposing);
		}

		public rptNewLandBreakdown()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptNewLandBreakdown	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLand = new clsDRWrapper();
		bool boolFirstCategory;
		int intCurrLandCode;
		clsDRWrapper clsCost = new clsDRWrapper();
		clsDRWrapper clsLandCode = new clsDRWrapper();
		int intLandCode;
		int totCount;
		int totAssessment;
		double totAcreage;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLand.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			strSQL = "(select srecordnumber,landcode,slandc1 as landc,slandv1 as landv,slanda1 as landa from summrecord) union all (select srecordnumber,landcode, slandc2 as landc,slandv2 as landv,slanda2 as landa from summrecord) UNION all (select srecordnumber,landcode, slandc3 as landc,slandv3 as landv, slanda3 as landa from summrecord)";
			strSQL += " union all (select srecordnumber,landcode,slandc4 as landc,slandv4 as landv,slanda4 as landa from summrecord) union all (select srecordnumber,landcode, slandc5 as landc,slandv5 as landv,slanda5 as landa from summrecord) UNION all (select srecordnumber,landcode, slandc6 as landc,slandv6 as landv, slanda6 as landa from summrecord)";
			strSQL += " union all (select srecordnumber,landcode,slandc7 as landc,slandv7 as landv,slanda7 as landa from summrecord) ";
			// Call clsLand.CreateStoredProcedure("LandBreakdown", strSQL, strREDatabase)
			if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
			{
				// strSQL = "SELECT landcode, landc, count(landc) AS thecount, sum(landv) AS landval, sum(landa) AS landac From LandBreakdown Where LandCode >= 0 and isnull(landc,0) > 0 GROUP BY landcode, landc"
				strSQL = "SELECT landcode, landc, count(landc) AS thecount, sum(cast(landv as bigint)) AS landval, sum(landa) AS landac From (" + strSQL + ") LandBreakDown Where LandCode >= 0 and isnull(landc,0) > 0 GROUP BY landcode, landc";
			}
			else
			{
				// strSQL = "SELECT landcode, landc, count(landc) AS thecount, sum(landv) AS landval, sum(landa) AS landac From LandBreakdown inner join master on (master.rsaccount = landbreakdown.srecordnumber) Where rscard = 1  and  LandCode >= 0 and val(landc & '') > 0 and val(ritrancode & '') = " & CustomizedInfo.CurrentTownNumber & " GROUP BY landcode, landc"
				strSQL = "SELECT landcode, landc, count(landc) AS thecount, sum(cast(landv as bigint)) AS landval, sum(landa) AS landac From (" + strSQL + ") LandBreakdown inner join master on (master.rsaccount = landbreakdown.srecordnumber) Where rscard = 1  and  LandCode >= 0 and landc  > 0 ritrancode = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " GROUP BY landcode, landc";
			}
			clsLand.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			// Call clsCost.OpenRecordset("select * from costrecord where crecordnumber between 1390 and 1426 order by crecordnumber", strREDatabase)
			clsCost.OpenRecordset("select * from landtype order by code", modGlobalVariables.strREDatabase);
			// Call clsLandCode.OpenRecordset("select * from costrecord where crecordnumber between 1000 and 1009 order by crecordnumber", strredatabase)
			clsLandCode.OpenRecordset("select * from tbllandcode order by CODE", modGlobalVariables.strREDatabase);
			totCount = 0;
			totAssessment = 0;
			totAcreage = 0;
			boolFirstCategory = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsLand.EndOfFile())
			{
				this.Detail.CanShrink = true;
				this.Detail.Height = 300 / 1440f;
				this.Detail.CanGrow = false;
				// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
				intCurrLandCode = FCConvert.ToInt32(clsLand.Get_Fields("landcode"));
				if (boolFirstCategory)
				{
					txtCode.Visible = true;
					// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
					intLandCode = FCConvert.ToInt32(clsLand.Get_Fields("landcode"));
					// Call clsLandCode.FindFirstRecord("code", intLandCode)
					if (!clsLandCode.FindFirst("code = " + FCConvert.ToString(intLandCode)) || intLandCode == 0)
					{
						// If intLandCode = 0 Or clsLandCode.NoMatch Then
						txtCode.Text = "0 Uncoded";
					}
					else
					{
						txtCode.Text = FCConvert.ToString(intLandCode) + " " + Strings.Trim(FCConvert.ToString(clsLandCode.Get_Fields_String("description")));
					}
					boolFirstCategory = false;
				}
				else
				{
					txtCode.Visible = false;
				}
				// Call clsCost.FindFirstRecord("crecordnumber", 1380 + clsLand.Fields("landc"))
				// Call clsCost.FindFirstRecord("code", clsLand.Fields("landc"))
				// TODO Get_Fields: Field [landc] not found!! (maybe it is an alias?)
				clsCost.FindFirst("code = " + clsLand.Get_Fields("landc"));
				// txtCategory.Text = clsLand.Fields("landc") & " " & Trim(clsCost.Fields("cldesc"))
				// TODO Get_Fields: Field [landc] not found!! (maybe it is an alias?)
				txtCategory.Text = clsLand.Get_Fields("landc") + " " + Strings.Trim(FCConvert.ToString(clsCost.Get_Fields_String("description")));
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				txtCount.Text = FCConvert.ToString(clsLand.Get_Fields("thecount"));
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				totCount += FCConvert.ToInt32(clsLand.Get_Fields("thecount"));
				// TODO Get_Fields: Field [landval] not found!! (maybe it is an alias?)
				txtAssess.Text = Strings.Format(clsLand.Get_Fields("landval"), "#,###,###,##0");
				// TODO Get_Fields: Field [landval] not found!! (maybe it is an alias?)
				totAssessment += FCConvert.ToInt32(clsLand.Get_Fields("landval"));
				// TODO Get_Fields: Field [landac] not found!! (maybe it is an alias?)
				txtAcreage.Text = Strings.Format(clsLand.Get_Fields("landac"), "#,###,##0.00");
				// TODO Get_Fields: Field [landac] not found!! (maybe it is an alias?)
				totAcreage += FCConvert.ToDouble(clsLand.Get_Fields("landac"));
				clsLand.MoveNext();
				if (!clsLand.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [landcode] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(clsLand.Get_Fields("landcode")) != intCurrLandCode)
					{
						boolFirstCategory = true;
						this.Detail.CanGrow = true;
						this.Detail.Height = 720 / 1440f;
						this.Detail.CanShrink = false;
						txtTotCount.Text = Strings.Format(totCount, "#,###,##0");
						txtTotAssess.Text = Strings.Format(totAssessment, "#,###,###,##0");
						txtTotAcreage.Text = Strings.Format(totAcreage, "#,###,###,##0.00");
						totCount = 0;
						totAssessment = 0;
						totAcreage = 0;
					}
				}
				else
				{
					this.Detail.CanGrow = true;
					this.Detail.Height = 720 / 1440f;
					this.Detail.CanShrink = false;
					txtTotCount.Text = Strings.Format(totCount, "#,###,##0");
					txtTotAssess.Text = Strings.Format(totAssessment, "#,###,###,##0");
					txtTotAcreage.Text = Strings.Format(totAcreage, "#,###,###,##0.00");
				}
			}
		}

		
	}
}
