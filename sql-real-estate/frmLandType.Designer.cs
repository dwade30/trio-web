﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLandType.
	/// </summary>
	partial class frmLandType : BaseForm
	{
		public FCGrid Grid;
		public FCGrid GridDelete;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLandType));
			this.Grid = new fecherFoundation.FCGrid();
			this.GridDelete = new fecherFoundation.FCGrid();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdAdd = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 400);
			this.BottomPanel.Size = new System.Drawing.Size(627, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Controls.Add(this.GridDelete);
			this.ClientArea.Size = new System.Drawing.Size(627, 340);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdAdd);
			this.TopPanel.Size = new System.Drawing.Size(627, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAdd, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(140, 30);
			this.HeaderText.Text = "Land Types";
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.Cols = 7;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid.DragIcon = null;
			this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ExtendLastCol = true;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 30);
			this.Grid.Name = "Grid";
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(577, 290);
			this.Grid.StandardTab = true;
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			//FC:FINAL:MSH - i.issue #1110: change TabBehavior for moving from one column to another on 'Tab' pressing
			//this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.Grid.TabIndex = 0;
			this.Grid.ComboCloseUp += new System.EventHandler(this.Grid_ComboCloseUp);
			this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
			this.Grid.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.Grid_MouseMoveEvent);
			this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
			// 
			// GridDelete
			// 
			this.GridDelete.AllowSelection = false;
			this.GridDelete.AllowUserToResizeColumns = false;
			this.GridDelete.AllowUserToResizeRows = false;
			this.GridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDelete.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDelete.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDelete.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.BackColorSel = System.Drawing.Color.Empty;
			this.GridDelete.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridDelete.ColumnHeadersHeight = 30;
			this.GridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDelete.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDelete.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridDelete.DragIcon = null;
			this.GridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDelete.FixedCols = 0;
			this.GridDelete.FixedRows = 0;
			this.GridDelete.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.FrozenCols = 0;
			this.GridDelete.GridColor = System.Drawing.Color.Empty;
			this.GridDelete.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.Location = new System.Drawing.Point(0, 24);
			this.GridDelete.Name = "GridDelete";
			this.GridDelete.ReadOnly = true;
			this.GridDelete.RowHeadersVisible = false;
			this.GridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDelete.RowHeightMin = 0;
			this.GridDelete.Rows = 0;
			this.GridDelete.ScrollTipText = null;
			this.GridDelete.ShowColumnVisibilityMenu = false;
			this.GridDelete.Size = new System.Drawing.Size(17, 2);
			this.GridDelete.StandardTab = true;
			this.GridDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDelete.TabIndex = 1;
			this.GridDelete.Visible = false;
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(284, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdAdd
			// 
			this.cmdAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAdd.AppearanceKey = "toolbarButton";
			this.cmdAdd.Location = new System.Drawing.Point(535, 29);
			this.cmdAdd.Name = "cmdAdd";
			this.cmdAdd.Size = new System.Drawing.Size(72, 24);
			this.cmdAdd.TabIndex = 1;
			this.cmdAdd.Text = "Add Code";
			this.cmdAdd.Click += new System.EventHandler(this.mnuAddCode_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(444, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(85, 24);
			this.cmdDelete.TabIndex = 2;
			this.cmdDelete.Text = "Delete Code";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDeleteCode_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(388, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(50, 24);
			this.cmdPrint.TabIndex = 3;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// frmLandType
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(627, 508);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLandType";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Land Types";
			this.Load += new System.EventHandler(this.frmLandType_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLandType_KeyDown);
			this.Resize += new System.EventHandler(this.frmLandType_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdAdd;
		private FCButton cmdDelete;
		private FCButton cmdPrint;
	}
}
