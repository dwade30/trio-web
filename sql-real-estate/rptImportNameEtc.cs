﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptImportNameEtc.
	/// </summary>
	public partial class rptImportNameEtc : BaseSectionReport
	{
		public static rptImportNameEtc InstancePtr
		{
			get
			{
				return (rptImportNameEtc)Sys.GetInstance(typeof(rptImportNameEtc));
			}
		}

		protected rptImportNameEtc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptImportNameEtc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:MSH - i.issue #1557: restore missing report name
			this.Name = "Import Name Etc.";
		}
		// nObj = 1
		//   0	rptImportNameEtc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		int lngCurrRow;

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "Import", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngCurrRow >= frmImportMasterInfo.InstancePtr.Grid.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTemp;
			txtMuni.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			txtMuniHeader.Text = txtMuni.Text;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtDateHeader.Text = txtDate.Text;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtTimeHeader.Text = txtTime.Text;
			intPage = 1;
			lngCurrRow = 0;
			strTemp = "";
			if (frmImportMasterInfo.InstancePtr.ChkNameAddress.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Name & Address,";
			}

            if (frmImportMasterInfo.InstancePtr.chkDeedNames.CheckState == CheckState.Checked)
            {
                strTemp += " Deed Names,";
            }
			if (frmImportMasterInfo.InstancePtr.chkMapLot.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Map/Lot,";
			}
			if (frmImportMasterInfo.InstancePtr.chkLocation.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Location,";
			}
			if (frmImportMasterInfo.InstancePtr.ChkRef1.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Reference 1,";
			}
			if (frmImportMasterInfo.InstancePtr.chkRef2.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Reference 2,";
			}
			if (frmImportMasterInfo.InstancePtr.chkBookPage.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Book & Page,";
			}
			if (frmImportMasterInfo.InstancePtr.chkLandInformation.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Land Information,";
			}
			if (frmImportMasterInfo.InstancePtr.ChkTranLandBldg.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Tran/Land/Bldg/Prop Codes,";
			}
			if (frmImportMasterInfo.InstancePtr.chkBankruptcy.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Bankruptcy,";
			}
			if (frmImportMasterInfo.InstancePtr.chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Tax Acquired,";
			}
			if (frmImportMasterInfo.InstancePtr.chkPropertyInfo.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Property Info,";
			}
			if (frmImportMasterInfo.InstancePtr.chkSaleData.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Sale Data,";
			}
			if (frmImportMasterInfo.InstancePtr.chkExemptInfo.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Exempt Information,";
			}
			if (frmImportMasterInfo.InstancePtr.chkDwelling.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Dwelling Information,";
			}
			if (frmImportMasterInfo.InstancePtr.chkCommercial.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Commercial Information,";
			}
			if (frmImportMasterInfo.InstancePtr.chkOutbuilding.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Outbuilding Information,";
			}
			if (frmImportMasterInfo.InstancePtr.chkInterestedParties.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Interested Parties,";
			}
			if (frmImportMasterInfo.InstancePtr.chkPreviousOwners.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Previous Owners,";
			}
			if (frmImportMasterInfo.InstancePtr.chkPictures.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Picture Links,";
			}
			if (frmImportMasterInfo.InstancePtr.chkInspection.CheckState == Wisej.Web.CheckState.Checked)
			{
				strTemp += " Inspection Info,";
			}
			strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			txtImpCategories.Text = strTemp;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1557: unload form only after initializing of the report to avoid data loss
			frmImportMasterInfo.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtAccount.Text = FCConvert.ToString(Conversion.Val(frmImportMasterInfo.InstancePtr.Grid.TextMatrix(lngCurrRow, 0)));
			lngCurrRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			if (intPage == 1)
			{
				PageHeader.Visible = false;
			}
			else
			{
				PageHeader.Visible = true;
			}
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		
	}
}
