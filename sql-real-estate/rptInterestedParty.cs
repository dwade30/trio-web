﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptInterestedParty.
	/// </summary>
	public partial class rptInterestedParty : BaseSectionReport
	{
		public static rptInterestedParty InstancePtr
		{
			get
			{
				return (rptInterestedParty)Sys.GetInstance(typeof(rptInterestedParty));
			}
		}

		protected rptInterestedParty _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptInterestedParty()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Interested Party Master List";
		}
		// nObj = 1
		//   0	rptInterestedParty	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();
		private int lngCount;
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public void Init(bool boolRange, short intType, string strmin = "", string strmax = "", bool boolFromExtract = false)
		{
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin;
			strREFullDBName = rsReport.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			strMasterJoinJoin = modREMain.GetMasterJoinForJoin();
			// Dim strMasterJoinJoinQuery As String
			// strMasterJoinJoinQuery = "(" & strMasterJoinJoin & ") mj"
			string strSQL = "";
			string strWhere = "";
			string strOrderBy = "";
			string strWhere2;
			strWhere2 = "";
			switch (intType)
			{
				case 1:
					{
						// account
						strOrderBy = " order by rsaccount, name ";
						strWhere = " and rsaccount between " + FCConvert.ToString(Conversion.Val(strmin)) + " and " + FCConvert.ToString(Conversion.Val(strmax));
						break;
					}
				case 2:
					{
						// name
						strOrderBy = " order by name,rsaccount";
						strWhere = "";
						strWhere2 = " and name between '" + strmin + "' and '" + strmax + "'";
						break;
					}
				case 3:
					{
						// map lot
						strOrderBy = " order by rsmaplot,rsaccount, rsname ";
						strWhere = " and rsmaplot between '" + strmin + "' and '" + strmax + "'";
						break;
					}
				case 4:
					{
						// location
						strOrderBy = " order by rslocstreet,CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float),rsaccount";
						strWhere = " and rslocstreet between '" + strmin + "' and '" + strmax + "'";
						break;
					}
			}
			//end switch
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolFromExtract)
			{
				// strSQL = "select * from OWNERS inner join (select master.rsaccount,master.rscard,master.rsmaplot,master.rsname,rslocnumalph,rslocstreet from (master inner join  (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and not rsdeleted = 1 and rscard = 1) as tbl1  on (tbl1.rsaccount = owners.account) where associd = 0 " & strWhere2 & strOrderBy
				strSQL = "select * from OWNERS inner join (select mj.rsaccount,mj.rscard,master.rsmaplot,mj.rsname,rslocnumalph,rslocstreet from (" + strMasterJoinQuery + " inner join  (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and not rsdeleted = 1 and rscard = 1) as tbl1  on (tbl1.rsaccount = owners.account) where associd = 0 " + strWhere2 + strOrderBy;
			}
			else
			{
				if (!boolRange)
				{
					strWhere = "";
				}
				if (boolRange && intType == 2)
				{
					// strSQL = "select * from owners inner join (select rsaccount,rsmaplot,rslocnumalph,rslocstreet from master  where rscard = 1 and not rsdeleted = 1 " & strWhere & ") as tbl1 on (tbl1.rsaccount = owners.account) where associd = 0" & strWhere2 & strOrderBy
					strSQL = "select * from owners inner join (select rsaccount,rsmaplot,rslocnumalph,rslocstreet from " + strMasterJoinJoin + "  where rscard = 1 and not rsdeleted = 1 " + strWhere + ") as tbl1 on (tbl1.rsaccount = owners.account) where associd = 0" + strWhere2 + strOrderBy;
				}
				else
				{
					// strSQL = "select * from owners inner join (select rsaccount,rsmaplot,rslocnumalph,rslocstreet from master  where rscard = 1 and not rsdeleted = 1 " & strWhere & ") as tbl1 on (tbl1.rsaccount = owners.account) where associd = 0" & strOrderBy
					strSQL = "select * from owners inner join (select rsaccount,rsmaplot,rslocnumalph,rslocstreet from " + strMasterJoinJoin + "  where rscard = 1 and not rsdeleted = 1 " + strWhere + ") as tbl1 on (tbl1.rsaccount = owners.account) where associd = 0" + strOrderBy;
				}
			}
			rsReport.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Revocable Living Trusts");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			lblPage.Text = "Page 1";
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCount = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// VB6 Bad Scope Dim:
			int x = 0;
			if (!rsReport.EndOfFile())
			{
				fldAccount.Text = FCConvert.ToString(rsReport.Get_Fields_Int32("rsaccount"));
				fldName.Text = FCConvert.ToString(rsReport.Get_Fields_String("Name"));
				string[] straddr = new string[3 + 1];
				x = 0;
				if (Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("address1"))) != "")
				{
					straddr[x] = Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("address1")));
					x += 1;
				}
				if (Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("address2"))) != "")
				{
					straddr[x] = Strings.Trim(FCConvert.ToString(rsReport.Get_Fields_String("address2")));
					x += 1;
				}
				straddr[x] = Strings.Trim(Strings.Trim(Strings.Trim(rsReport.Get_Fields_String("city") + " " + rsReport.Get_Fields_String("state")) + "  " + rsReport.Get_Fields_String("zip")) + " " + rsReport.Get_Fields_String("zip4"));
				fldAddress1.Text = straddr[0];
				fldAddress2.Text = straddr[1];
				fldAddress3.Text = straddr[2];
				if (Strings.Trim(straddr[1]) == "")
				{
					fldAddress2.Visible = false;
				}
				else
				{
					fldAddress2.Visible = true;
				}
				if (Strings.Trim(straddr[2]) == "")
				{
					fldAddress3.Visible = false;
				}
				else
				{
					fldAddress3.Visible = true;
				}
				txtmaplot.Text = FCConvert.ToString(rsReport.Get_Fields_String("rsmaplot"));
				if (Conversion.Val(rsReport.Get_Fields_String("rslocnumalph")) > 0)
				{
					txtLocation.Text = FCConvert.ToString(rsReport.Get_Fields_String("rslocnumalph") + " " + rsReport.Get_Fields_String("rslocstreet"));
				}
				else
				{
					txtLocation.Text = FCConvert.ToString(rsReport.Get_Fields_String("rslocstreet"));
				}
				lngCount += 1;
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCount.Text = lngCount.ToString();
		}

		
	}
}
