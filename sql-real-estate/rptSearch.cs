﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptSearch.
	/// </summary>
	public partial class rptSearch : BaseSectionReport
	{
		public static rptSearch InstancePtr
		{
			get
			{
				return (rptSearch)Sys.GetInstance(typeof(rptSearch));
			}
		}

		protected rptSearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptSearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.Name = "Search";
		}
		// nObj = 1
		//   0	rptSearch	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngCurrRow;
		//private void ActiveReport_Error(int Number, DDActiveReports2.IReturnString Description, int Scode, string Source, string HelpFile, int HelpContext, DDActiveReports2.IReturnBool CancelDisplay)
		//{
		//	MessageBox.Show("Error Number " + FCConvert.ToString(Number) + " " + Description + "\r\n" + "Code: " + FCConvert.ToString(Scode) + "\r\n" + "Source " + Source, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal:false);
		//}
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (lngCurrRow >= frmRESearch.InstancePtr.Grid.Rows)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTitle = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngCurrRow = 1;
			txtMuni.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			txtPage.Text = "1";
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm");
			if (modGNBas.Statics.SEQ == "L")
			{
				// location
				strTitle += "Location ";
			}
			else if (modGNBas.Statics.SEQ == "N")
			{
				// name
				strTitle += "Name ";
			}
			else if (modGNBas.Statics.SEQ == "M")
			{
				// maplot
				strTitle += "Map/Lot ";
			}
			else if ((modGNBas.Statics.SEQ == "S") || (modGNBas.Statics.SEQ == "R"))
			{
				// sale dates
				strTitle += "SaleDate ";
			}
			if (!(modGNBas.Statics.SEQ == "R"))
			{
				switch (modGlobalVariables.Statics.intSearchPosition)
				{
					case 1:
						{
							// contains
							strTitle += "contains ";
							break;
						}
					case 2:
						{
							// starts with
							strTitle += "starts with ";
							break;
						}
					case 3:
						{
							// ends with
							strTitle += "ends with ";
							break;
						}
				}
				//end switch
				strTitle += modGNBas.Statics.Resp2;
			}
			else if (modGNBas.Statics.SEQ == "R")
			{
				strTitle = frmRESearch.InstancePtr.lblSearch.Text;
			}
			txtTitle2.Text = strTitle;
			if (!modGlobalVariables.Statics.boolPreviousOwnerRecords)
			{
			}
			else
			{
				Label2.Width = 4860 / 1440f;
				Label3.Left = 5850 / 1440f;
				Label3.Width = 3600 / 1440f;
				Label3.Text = "Second Owner";
				txtTwo.Width = 4860 / 1440f;
				txt3.Left = 5850 / 1440f;
				txt3.Width = 3600 / 1440f;
				txt4.Visible = false;
				Label4.Visible = false;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngCurrRow >= frmRESearch.InstancePtr.Grid.Rows)
				return;
			if (!modGlobalVariables.Statics.boolPreviousOwnerRecords)
			{
				txtOne.Text = frmRESearch.InstancePtr.Grid.TextMatrix(lngCurrRow, 5);
				txtTwo.Text = frmRESearch.InstancePtr.Grid.TextMatrix(lngCurrRow, 0);
				txt3.Text = Strings.Trim(frmRESearch.InstancePtr.Grid.TextMatrix(lngCurrRow, 1) + " " + frmRESearch.InstancePtr.Grid.TextMatrix(lngCurrRow, 2));
				txt4.Text = frmRESearch.InstancePtr.Grid.TextMatrix(lngCurrRow, 4);
				txt5.Text = Strings.Format(frmRESearch.InstancePtr.Grid.TextMatrix(lngCurrRow, 3), "MM/dd/yyyy");
			}
			else
			{
				txtOne.Text = frmRESearch.InstancePtr.Grid.TextMatrix(lngCurrRow, 2);
				txtTwo.Text = frmRESearch.InstancePtr.Grid.TextMatrix(lngCurrRow, 0);
				txt3.Text = frmRESearch.InstancePtr.Grid.TextMatrix(lngCurrRow, 1);
				txt5.Text = Strings.Format(frmRESearch.InstancePtr.Grid.TextMatrix(lngCurrRow, 3), "MM/dd/yyyy");
			}
			lngCurrRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
