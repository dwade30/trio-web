﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cCommercialConversionService
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public bool ConvertSections()
		{
			bool ConvertSections = false;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.Execute("delete from CommercialSections", "RealEstate");
				rsSave.Execute("delete from CommercialConstructionClasses", "RealEstate");
				short x;
				int y;
				// vbPorter upgrade warning: lngKey As int	OnWrite(int, double)
				int lngKey = 0;
				// vbPorter upgrade warning: dblTemp As double	OnWrite(short, double, string)
				double dblTemp = 0;
				// vbPorter upgrade warning: lngLife As int	OnWriteFCConvert.ToDouble(
				int lngLife = 0;
				clsDRWrapper rsOCCCodes = new clsDRWrapper();
				modGlobalVariables.Statics.ComCostRec = new clsCostRecord();
				for (x = 1; x <= 9; x++)
				{
					rsSave.Execute("insert into commercialsections (Name,Description) values ('" + FCConvert.ToString(x) + "','')", "RealEstate");
				}
				cCommercialConstructionClass conClass;
				cConstructionClassController conClassController = new cConstructionClassController();
				for (x = 1; x <= 5; x++)
				{
					conClass = new cCommercialConstructionClass();
					conClass.ClassCategory = x;
					conClass.CodeNumber = x;
					switch ((modGlobalVariables.CommercialClassCategory)x)
					{
						case modGlobalVariables.CommercialClassCategory.a:
							{
								conClass.Name = "A";
								break;
							}
						case modGlobalVariables.CommercialClassCategory.B:
							{
								conClass.Name = "B";
								break;
							}
						case modGlobalVariables.CommercialClassCategory.c:
							{
								conClass.Name = "C";
								break;
							}
						case modGlobalVariables.CommercialClassCategory.D:
							{
								conClass.Name = "D";
								break;
							}
						case modGlobalVariables.CommercialClassCategory.s:
							{
								conClass.Name = "S";
								break;
							}
					}
					//end switch
					conClassController.SaveCommercialConstructionClass(ref conClass);
				}
				cConstructionClassCollection constructionClasses;
				constructionClasses = conClassController.GetCommercialConstructionClasses();
				cCommercialSectionController sectController = new cCommercialSectionController();
				cGenericCollection sections;
				sections = sectController.GetSections();
				cCommercialSection currSection;
				cCommercialTrend commTrend;
				cCommercialTrendController trendController = new cCommercialTrendController();
				trendController.DeleteCommercialTrends();
				// commercial trends
				for (x = 1; x <= 9; x++)
				{
					currSection = (cCommercialSection)sections.GetItemByIndex(x);
					for (y = 1; y <= 5; y++)
					{
						conClass = constructionClasses.FindFirstCode(FCConvert.ToInt16(y));
						if (!(conClass == null))
						{
							commTrend = new cCommercialTrend();
							commTrend.ConstructionClassID = conClass.ID;
							commTrend.SectionID = currSection.ID;
							lngKey = 140 + (20 * x) + y;
							modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(lngKey, "COMMERCIAL", "COMMERCIAL");
							dblTemp = 0;
							if (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) < 99990000)
							{
								dblTemp = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
							}
							commTrend.Multiplier = dblTemp;
							trendController.SaveCommercialTrend(ref commTrend);
						}
					}
				}
				// x
				rsSave.Execute("delete from commercialoccupancycodes", "RealEstate");
				rsSave.Execute("delete from commercialoccupancycosts", "RealEstate");
				rsSave.Execute("Delete from CommercialExteriorWalls", "RealEstate");
				cOccupancyCodeController oControl = new cOccupancyCodeController();
				cGenericCollection oCodes = new cGenericCollection();
				cCommercialOccupancyCode occCode;
				int lngTemp;
				double dblClass;
				string strTemp = "";
				int intSectionCode = 0;
				cOccupancyCostController occCostController = new cOccupancyCostController();
				rsLoad.OpenRecordset("select * from commercialcost where crecordnumber between 341 and 349 order by crecordnumber", "RealEstate");
				cCommercialExteriorWall extwall;
				cCommExteriorWallsController extWallController = new cCommExteriorWallsController();
				while (!rsLoad.EndOfFile())
				{
					extwall = new cCommercialExteriorWall();
					extwall.FullDescription = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("cldesc")));
					extwall.ShortDescription = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("csdesc")));
					extwall.CodeNumber = rsLoad.Get_Fields_Int32("crecordnumber") - 340;
					extWallController.SaveCommercialExteriorWall(ref extwall);
					rsLoad.MoveNext();
				}
				rsSave.Execute("delete from CommercialIncomeRates", "RealEstate");
				cOccupancyCost cCurrOccCost;
				rsOCCCodes.OpenRecordset("select * from occupancycodes order by code", "RealEstate");
				cIncomeRateController incCont = new cIncomeRateController();
				cCommercialIncomeRate incRate;
				rsLoad.OpenRecordset("select * from commercialcost where crecordnumber between 1 and 159 order by crecordnumber", "RealEstate");
				while (!rsLoad.EndOfFile())
				{
					lngTemp = FCConvert.ToInt32(rsLoad.Get_Fields_String("cbase"));
					strTemp = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("cldesc")).Replace(".", ""));
					if (strTemp != "")
					{
						currSection = null;
						intSectionCode = 0;
						switch (lngTemp)
						{
							case 100:
								{
									intSectionCode = 1;
									break;
								}
							case 200:
								{
									intSectionCode = 2;
									break;
								}
							case 300:
								{
									intSectionCode = 3;
									break;
								}
							case 400:
								{
									intSectionCode = 4;
									break;
								}
							case 500:
								{
									intSectionCode = 5;
									break;
								}
							case 600:
								{
									intSectionCode = 6;
									break;
								}
							case 700:
								{
									intSectionCode = 7;
									break;
								}
							case 800:
								{
									intSectionCode = 8;
									break;
								}
							case 900:
								{
									intSectionCode = 9;
									break;
								}
						}
						//end switch
						if (intSectionCode > 0)
						{
							currSection = (cCommercialSection)sections.GetItemByIndex(intSectionCode);
						}
						if (!(currSection == null))
						{
							occCode = new cCommercialOccupancyCode();
							occCode.SectionID = currSection.ID;
							occCode.Name = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("cldesc")));
							occCode.Code = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("csdesc")));
							occCode.OccupancyCodeNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Crecordnumber"));
							oControl.SaveOccupancyCode(ref occCode);
							if (rsOCCCodes.FindFirst("Code = " + rsLoad.Get_Fields_Int32("crecordnumber")))
							{
								incRate = new cCommercialIncomeRate();
								// incRate.CommercialOccupancyCodeID = occCode.ID
								incRate.CommercialOccupancyCodeNumber = occCode.OccupancyCodeNumber;
								incRate.Rate = rsOCCCodes.Get_Fields_Double("Rate");
								incCont.SaveIncomeRate(ref incRate);
							}
							// Call oCodes.AddItem(occCode)
							for (x = 1; x <= 5; x++)
							{
								// 5 classes
								for (y = 1; y <= 4; y++)
								{
									// four qualities
									if (FCConvert.ToInt32(rsLoad.Get_Fields_String("cunit")) != 99999999)
									{
										lngKey = FCConvert.ToInt32(rsLoad.Get_Fields_String("cunit")) / 100 + ((x * 4) - 4) + y - 1;
										modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(lngKey, "COMMERCIAL", "COMMERCIAL");
										if ((FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase) < 90000000) && (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) < 90000000))
										{
											dblTemp = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
											lngLife = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
											cCurrOccCost = new cOccupancyCost();
											cCurrOccCost.OccupancyCodeID = occCode.ID;
											cCurrOccCost.QualityCode = y;
											cCurrOccCost.SqftCost = dblTemp;
											cCurrOccCost.Life = FCConvert.ToInt16(lngLife);
											conClass = constructionClasses.FindFirstCode(FCConvert.ToInt16(x));
											if (!(conClass == null))
											{
												cCurrOccCost.ConstructionClassID = conClass.ID;
											}
											occCostController.SaveOccupancyCost(ref cCurrOccCost);
										}
									}
								}
								// y
							}
							// x
						}
					}
					rsLoad.MoveNext();
				}
				cCommercialHeatingCooling heatCode;
				cHeatingCoolingCollection heatCodes;
				cHeatingCoolingController heatController = new cHeatingCoolingController();
				cHeatingCoolingCost heatCost;
				heatController.DeleteCommercialHeatingCoolingCosts();
				heatController.DeleteCommercialHeatingCoolings();
				for (x = 1; x <= 19; x++)
				{
					lngKey = 160 + x;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(lngKey, "COMMERCIAL", "COMMERCIAL");
					heatCode = new cCommercialHeatingCooling();
					heatCode.FullDescription = Strings.Trim(modGlobalVariables.Statics.ComCostRec.ClDesc);
					heatCode.ShortDescription = Strings.Trim(modGlobalVariables.Statics.ComCostRec.CSDesc);
					heatCode.CodeNumber = x + 10;
					heatController.SaveCommercialHeatingCooling(ref heatCode);
				}
				// x
				heatCodes = heatController.GetCommercialHeatingCoolings();
				sections.MoveFirst();
				for (x = 1; x <= 9; x++)
				{
					currSection = (cCommercialSection)sections.GetItemByIndex(x);
					for (y = 11; y <= 29; y++)
					{
						lngKey = 140 + (20 * x) + y - 10;
						modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(lngKey, "COMMERCIAL", "COMMERCIAL");
						if (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) < 99999999)
						{
							heatCode = heatCodes.FindFirstCode(y);
							if (!(heatCode == null))
							{
								heatCost = new cHeatingCoolingCost();
								heatCost.HeatingCoolingID = heatCode.ID;
								heatCost.SectionID = currSection.ID;
								heatCost.MildClimateCost = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
								heatCost.ModerateClimateCost = heatCost.MildClimateCost;
								heatCost.ExtremeClimateCost = heatCost.MildClimateCost;
								heatController.SaveHeatingCoolingCost(ref heatCost);
							}
						}
					}
					// y
				}
				// x
				// story height multipliers
				sections.MoveFirst();
				double dblBaseHeight = 0;
				double dblHeightFactor = 0;
				double dblMultiplier = 0;
				cStoryHeightMultiplier storyMultiplier;
				cStoryHeightController shController = new cStoryHeightController();
				rsSave.Execute("delete from storyheightmultipliers", "RealEstate");
				for (x = 1; x <= 9; x++)
				{
					lngKey = 370 + x;
					currSection = (cCommercialSection)sections.GetItemByIndex(x);
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(lngKey, "COMMERCIAL", "COMMERCIAL");
					dblBaseHeight = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
					dblHeightFactor = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 100;
					for (y = 7; y <= 120; y++)
					{
						dblMultiplier = ((y - dblBaseHeight) / dblHeightFactor) + 1;
						storyMultiplier = new cStoryHeightMultiplier();
						storyMultiplier.SectionID = currSection.ID;
						storyMultiplier.SqftMultiplier = dblMultiplier;
						storyMultiplier.WallHeight = y;
						shController.SaveStoryHeightMultiplier(ref storyMultiplier);
					}
					// y
				}
				// x
				// commercial conditions
				cCommercialConditionController condCont = new cCommercialConditionController();
				cCommercialCondition commCond;
				double dblHigh = 0;
				double dblLow = 0;
				double dblExp = 0;
				rsSave.Execute("delete from commercialconditions", "RealEstate");
				for (x = 1; x <= 8; x++)
				{
					lngKey = 350 + x;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(lngKey, "COMMERCIAL", "COMMERCIAL");
					dblTemp = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase);
					if (dblTemp < 99000000)
					{
						dblHigh = fecherFoundation.FCUtils.iDiv((FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CUnit) / 1000000), 1);
						dblLow = (FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CUnit) - (dblHigh * 1000000));
						dblExp = FCConvert.ToInt32(modGlobalVariables.Statics.ComCostRec.CBase) / 100;
						commCond = new cCommercialCondition();
						commCond.CodeNumber = x;
						commCond.Description = Strings.Trim(modGlobalVariables.Statics.ComCostRec.ClDesc);
						commCond.Exponent = dblExp;
						commCond.High = dblHigh;
						commCond.Low = dblLow;
						condCont.SaveCommercialCondition(ref commCond);
					}
				}
				// x
				// location multipliers
				rsSave.Execute("delete from CommercialLocations", "RealEstate");
				rsSave.Execute("delete from CommercialLocationMultipliers", "RealEstate");
				cCommercialLocationMultiplier locMult;
				cCommercialLocation commLoc;
				cLocationMultiplierController locCont = new cLocationMultiplierController();
				commLoc = new cCommercialLocation();
				commLoc.Location = "Unknown";
				commLoc.State = "Maine";
				locCont.SaveCommercialLocation(ref commLoc);
				for (x = 1; x <= 5; x++)
				{
					locMult = new cCommercialLocationMultiplier();
					locMult.CommercialLocationID = commLoc.ID;
					locMult.ClassCategory = x;
					lngKey = 170 + x;
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(lngKey, "COMMERCIAL", "COMMERCIAL");
					dblTemp = FCConvert.ToDouble(modGlobalVariables.Statics.ComCostRec.CBase);
					if (dblTemp > 0)
					{
						dblTemp /= 100;
					}
					locMult.Multiplier = dblTemp;
					locCont.SaveLocationMultiplier(ref locMult);
				}
				// x
				ConvertSections = true;
				return ConvertSections;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return ConvertSections;
		}
	}
}
