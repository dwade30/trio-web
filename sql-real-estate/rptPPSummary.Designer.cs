﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPPSummary.
	/// </summary>
	partial class rptPPSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPPSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExceed = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPercUp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPercDown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUp = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDown = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtChgVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNoMatch = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDeleted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBadUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNotUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExceed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercUp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUp)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChgVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBadUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label7,
				this.Label8,
				this.Label9,
				this.lblExceed,
				this.Label11,
				this.Label12,
				this.Label14,
				this.Label17,
				this.txtTotalNet,
				this.txtPercUp,
				this.txtPercDown,
				this.txtUp,
				this.txtDown,
				this.txtChgVal,
				this.Label22,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.txtUpdated,
				this.txtNoMatch,
				this.txtDeleted,
				this.txtBadUpdated,
				this.txtNotUpdated,
				this.txtNew,
				this.Label23,
				this.Label24
			});
			this.Detail.Height = 3.09375F;
			this.Detail.Name = "Detail";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold";
			this.Label1.Text = "Will Be Updated";
			this.Label1.Top = 0.6875F;
			this.Label1.Width = 1.6875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Bad Deleted";
			this.Label2.Top = 1.0625F;
			this.Label2.Width = 1.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.0625F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Updated More Than Once";
			this.Label7.Top = 1.25F;
			this.Label7.Width = 2F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.0625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "Not Updated";
			this.Label8.Top = 1.4375F;
			this.Label8.Width = 1.6875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Text = "No Match";
			this.Label9.Top = 0.875F;
			this.Label9.Width = 1.6875F;
			// 
			// lblExceed
			// 
			this.lblExceed.Height = 0.1875F;
			this.lblExceed.HyperLink = null;
			this.lblExceed.Left = 0.0625F;
			this.lblExceed.Name = "lblExceed";
			this.lblExceed.Style = "font-weight: bold";
			this.lblExceed.Text = "Exceeds % Change";
			this.lblExceed.Top = 2F;
			this.lblExceed.Width = 2.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.8125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: right";
			this.Label11.Text = "Up";
			this.Label11.Top = 1.8125F;
			this.Label11.Width = 0.375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 3.75F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: right";
			this.Label12.Text = "Down";
			this.Label12.Top = 1.8125F;
			this.Label12.Width = 0.5625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.0625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold";
			this.Label14.Text = "Value Change";
			this.Label14.Top = 2.5F;
			this.Label14.Width = 1.6875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.5F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold; text-align: right";
			this.Label17.Text = "Total";
			this.Label17.Top = 2.875F;
			this.Label17.Width = 0.8125F;
			// 
			// txtTotalNet
			// 
			this.txtTotalNet.Height = 0.1875F;
			this.txtTotalNet.Left = 1.5625F;
			this.txtTotalNet.Name = "txtTotalNet";
			this.txtTotalNet.Style = "text-align: right";
			this.txtTotalNet.Text = "0";
			this.txtTotalNet.Top = 2.875F;
			this.txtTotalNet.Width = 0.9375F;
			// 
			// txtPercUp
			// 
			this.txtPercUp.Height = 0.1875F;
			this.txtPercUp.Left = 2.4375F;
			this.txtPercUp.Name = "txtPercUp";
			this.txtPercUp.Style = "text-align: right";
			this.txtPercUp.Text = "0";
			this.txtPercUp.Top = 2F;
			this.txtPercUp.Width = 0.75F;
			// 
			// txtPercDown
			// 
			this.txtPercDown.Height = 0.1875F;
			this.txtPercDown.Left = 3.375F;
			this.txtPercDown.Name = "txtPercDown";
			this.txtPercDown.Style = "text-align: right";
			this.txtPercDown.Text = "0";
			this.txtPercDown.Top = 2F;
			this.txtPercDown.Width = 0.9375F;
			// 
			// txtUp
			// 
			this.txtUp.Height = 0.1875F;
			this.txtUp.Left = 2.25F;
			this.txtUp.Name = "txtUp";
			this.txtUp.Style = "text-align: right";
			this.txtUp.Text = "0";
			this.txtUp.Top = 2.5F;
			this.txtUp.Width = 0.9375F;
			// 
			// txtDown
			// 
			this.txtDown.Height = 0.1875F;
			this.txtDown.Left = 3.375F;
			this.txtDown.Name = "txtDown";
			this.txtDown.Style = "text-align: right";
			this.txtDown.Text = "0";
			this.txtDown.Top = 2.5F;
			this.txtDown.Width = 0.9375F;
			// 
			// txtChgVal
			// 
			this.txtChgVal.Height = 0.1875F;
			this.txtChgVal.Left = 5.375F;
			this.txtChgVal.Name = "txtChgVal";
			this.txtChgVal.Style = "text-align: right";
			this.txtChgVal.Text = "0";
			this.txtChgVal.Top = 2.5F;
			this.txtChgVal.Width = 0.9375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.25F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 2.5F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label22.Text = "Billing Transfer Summary";
			this.Label22.Top = 0F;
			this.Label22.Width = 2.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.25F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 2.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.8125F;
			// 
			// txtUpdated
			// 
			this.txtUpdated.Height = 0.1875F;
			this.txtUpdated.Left = 2F;
			this.txtUpdated.Name = "txtUpdated";
			this.txtUpdated.Style = "text-align: right";
			this.txtUpdated.Text = "0";
			this.txtUpdated.Top = 0.6875F;
			this.txtUpdated.Width = 0.625F;
			// 
			// txtNoMatch
			// 
			this.txtNoMatch.Height = 0.1875F;
			this.txtNoMatch.Left = 1.875F;
			this.txtNoMatch.Name = "txtNoMatch";
			this.txtNoMatch.Style = "text-align: right";
			this.txtNoMatch.Text = "0";
			this.txtNoMatch.Top = 0.875F;
			this.txtNoMatch.Width = 0.75F;
			// 
			// txtDeleted
			// 
			this.txtDeleted.Height = 0.1875F;
			this.txtDeleted.Left = 1.875F;
			this.txtDeleted.Name = "txtDeleted";
			this.txtDeleted.Style = "text-align: right";
			this.txtDeleted.Text = "0";
			this.txtDeleted.Top = 1.0625F;
			this.txtDeleted.Width = 0.75F;
			// 
			// txtBadUpdated
			// 
			this.txtBadUpdated.Height = 0.1875F;
			this.txtBadUpdated.Left = 2F;
			this.txtBadUpdated.Name = "txtBadUpdated";
			this.txtBadUpdated.Style = "text-align: right";
			this.txtBadUpdated.Text = "0";
			this.txtBadUpdated.Top = 1.25F;
			this.txtBadUpdated.Width = 0.625F;
			// 
			// txtNotUpdated
			// 
			this.txtNotUpdated.Height = 0.1875F;
			this.txtNotUpdated.Left = 1.875F;
			this.txtNotUpdated.Name = "txtNotUpdated";
			this.txtNotUpdated.Style = "text-align: right";
			this.txtNotUpdated.Text = "0";
			this.txtNotUpdated.Top = 1.4375F;
			this.txtNotUpdated.Width = 0.75F;
			// 
			// txtNew
			// 
			this.txtNew.Height = 0.1875F;
			this.txtNew.Left = 4.375F;
			this.txtNew.Name = "txtNew";
			this.txtNew.Style = "text-align: right";
			this.txtNew.Text = "0";
			this.txtNew.Top = 2.5F;
			this.txtNew.Width = 0.9375F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 4.75F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-weight: bold; text-align: right";
			this.Label23.Text = "New";
			this.Label23.Top = 2.3125F;
			this.Label23.Width = 0.5625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 2.5F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "text-align: center";
			this.Label24.Text = "Personal Property";
			this.Label24.Top = 0.25F;
			this.Label24.Width = 2.5F;
			// 
			// rptPPSummary
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExceed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercUp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUp)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtChgVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNoMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBadUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExceed;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPercUp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPercDown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUp;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDown;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtChgVal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNoMatch;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDeleted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBadUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNotUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
	}
}
