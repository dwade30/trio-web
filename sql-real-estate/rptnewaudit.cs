﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptaudit.
	/// </summary>
	public partial class rptaudit : BaseSectionReport
	{
		public rptaudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Audit";
		}

		public static rptaudit InstancePtr
		{
			get
			{
				return (rptaudit)Sys.GetInstance(typeof(rptaudit));
			}
		}

		protected rptaudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsTemp?.Dispose();
                rsTemp = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptaudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int LngNumTotExempts;
		bool boolFirstRec;
		int LngNumAccts;
		clsDRWrapper rsTemp = new clsDRWrapper();
	
		// Dim dbTemp As DAO.Database
		bool alldone;
		// used to tell if we are done the report. if so, the footer is made visible
		Modexemptionrpt.ExemptionAudit[] exemptlist = null;
		// an array and linked list of records. used for exempt only report
		bool boolinrange;
		// is the record in the range we are displaying
		double totland;
		// total for all accounts
		double totbuilding;
		// total for all accounts
		double totexemption;
		// total for all accounts
		double totAssessment;
		// total for all accounts
		int acctassessment;
		// acctx are the figures for that account
		int acctland;
		int acctbuilding;
		int acctexemption;
		int intPage;
		string txtorder = "";
		bool PRINTED;
		// true if the record processed was printed in the report
		double subland;
		// subtotals for groups
		double subbldg;
		double subexempt;
		double subExemptTotal;
		// vbPorter upgrade warning: subtotal As double	OnReadFCConvert.ToInt32(
		double subtotal;
		bool boolbinderchanged;
		// used to test if the groupbinder has changed
		int lengthofexemptlist;
		// used to redim to the correct length
		int ExemptIndex;
		// index into the exemptlist
		int currentgroup;
		// the current exemptcode being processed
		bool boolupdatetime;
		// in exempt only, is it time to update?
		int lowestcode;
		// used to initialize the groupbinder so the first code doesn't fire a new group
		bool anyinthisgroupprinted;
		double[] sland = new double[199 + 1];
		// sland,bldg, exempt are used to hold
		double[] sbldg = new double[199 + 1];
		// the subtotals for each code
		double[] sexempt = new double[199 + 1];
		// this is because of the order that group events fire
		int[] saccts = new int[199 + 1];
		int[] stexempts = new int[199 + 1];
		// vbPorter upgrade warning: subtotalexempts As int	OnWriteFCConvert.ToDouble(
		int[] subtotalexempts = new int[199 + 1];
		int numTexempts;
		int lastland;
		int lastbldg;
		int lastexempt;
		int lastgroup;
		// used to save the number of the group before
		string strLandFieldName = "";
		string strBldgFieldName = "";
		string strExemptionField = "";
		string strRangeWhere = "";
		bool boolAbortReport;
		string strTownName = "";
		// vbPorter upgrade warning: lngTownCode As int	OnRead(string)
		int lngTownCode;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			// InitializeData
			if (boolAbortReport)
			{
				this.Close();
				return;
			}
			Fields.Add("thebinder");
			// thebinder is used by activereport to tell if a group has changed
			if (modPrintRoutines.Statics.allorexempt == "exempt")
				rptaudit.InstancePtr.Fields["thebinder"].Value = -1;
			if (modPrintRoutines.Statics.allorexempt == "all")
				rptaudit.InstancePtr.Fields["thebinder"].Value = "xxxx";
			if (modPrintRoutines.Statics.allorexempt == "exempt")
			{
				Fields.Add("NumAccts");
				Fields.Add("NumTotExempts");
				// txtNumAccts.DataField = numaccts
				txtNumAccts.SummaryGroup = "GroupHeader1";
				txtNumAccts.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			}
			LngNumAccts = 0;
		}
		// vbPorter upgrade warning: intInclude As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intOrder As short	OnWriteFCConvert.ToInt32(
		public void Init(bool boolExempt, ref int intInclude, ref int intOrder, ref bool boolBilling, ref bool boolUseRange, ref string strMinRange, ref string strMaxRange, bool boolSubTot = false, int intTownCode = 0)
		{
			if (boolExempt)
			{
				modPrintRoutines.Statics.allorexempt = "exempt";
				if (boolSubTot)
				{
					this.Detail.Visible = false;
				}
			}
			else
			{
				modPrintRoutines.Statics.allorexempt = "all";
			}
			modGlobalVariables.Statics.boolRange = boolUseRange;
			modGlobalVariables.Statics.whatinclude = intInclude;
			modPrintRoutines.Statics.intwhichorder = intOrder;
			modGlobalVariables.Statics.boolBillingorCorrelated = boolBilling;
			modPrintRoutines.Statics.gstrMinAccountRange = strMinRange;
			modPrintRoutines.Statics.gstrMaxAccountRange = strMaxRange;
			modGlobalVariables.Statics.gintMinAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(strMinRange)));
			modGlobalVariables.Statics.gintMaxAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(strMaxRange)));
			lngTownCode = intTownCode;
			if (lngTownCode > 0)
			{
                var clsLoad = new clsDRWrapper();

                try
                {
                    clsLoad.OpenRecordset("select townname from tblregions where townnumber = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strGNDatabase);
                    if (!clsLoad.EndOfFile())
                    {
                        strTownName = FCConvert.ToString(clsLoad.Get_Fields_String("townname"));
                    }
                }
                finally
                {
                    clsLoad.DisposeOf();
                }
			}
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			uphere:
			;
			if (modPrintRoutines.Statics.allorexempt == "all" && modGlobalVariables.Statics.whatinclude == 4)
			{
				this.Detail.Visible = false;
				GroupHeader1.Visible = false;
				Label1.Visible = false;
				Label2.Visible = false;
				Label3.Visible = false;
				Label4.Visible = false;
				eArgs.EOF = true;
				return;
			}
			// If UCase(allorexempt) = "EXEMPT" Then
			// If whatinclude = 2 Then
			// Me.Detail.Visible = False
			// End If
			// End If
			if (modPrintRoutines.Statics.allorexempt == "all")
			{
				if ((modGlobalVariables.Statics.whatinclude == 2) || (modGlobalVariables.Statics.whatinclude == 3))
				{
					// include subtotals
					boolbinderchanged = false;
					checkthebinder();
					if (!boolFirstRec)
					{
						updatethebinder();
					}
				}
			}
			PRINTED = false;
			// assume we aren't printing anything
			if (modPrintRoutines.Statics.allorexempt == "all")
			{
				allaccounts();
			}
			else
			{
				ExemptIndex = exemptlist[ExemptIndex].next;
				if (ExemptIndex == 0)
				{
					if ((modPrintRoutines.Statics.intwhichorder == 2) || (modPrintRoutines.Statics.intwhichorder == 4) || (modPrintRoutines.Statics.intwhichorder == 6))
					{
						// if we are displaying by group
						if (currentgroup == modGlobalVariables.Statics.gintMaxAccountRange)
						{
							// only 1901 through 1999 are valid records in costrecord
							// because 1 through 99 are valid exempt codes
							alldone = true;
							goto getout;
						}
						else
						{
							currentgroup += 1;
							anyinthisgroupprinted = false;
							if ((modPrintRoutines.Statics.intwhichorder == 2) || (modPrintRoutines.Statics.intwhichorder == 4) || (modPrintRoutines.Statics.intwhichorder == 6))
							{
								updateexemptbinder();
							}
							ExemptIndex = exemptlist[ExemptIndex].next;
						}
					}
					else
					{
						alldone = true;
						goto getout;
					}
				}
				exemptaccounts(ref ExemptIndex);
			}
			getout:
			;
			if (alldone)
			{
				eArgs.EOF = true;
				if (modPrintRoutines.Statics.allorexempt == "all")
				{
					// last subtotals won't update correctly so force em
					if (modPrintRoutines.Statics.intwhichorder != 2)
					{
						// txtsubland = Format(subland, "###,###,###,##0")
						// txtsubbldg = Format(subbldg, "###,###,###,##0")
						// txtsubexempt = Format(subexempt, "###,###,###,##0")
						// txtsubtotal = Format(subtotal, "###,###,###,##0")
					}
				}
				return;
			}
			else
			{
				eArgs.EOF = false;
				if ((modGlobalVariables.Statics.whatinclude == 2) && (modPrintRoutines.Statics.allorexempt == "all"))
					PRINTED = true;
				if (!PRINTED)
					goto uphere;
				// if we don't want to print this line then dont leave fetchdata
				// everytime you leave and reenter fetchdata it will print a line
			}
		}

		private void InitializeData()
		{
			// for dotmatrix printers ddpqhigh is too slow
			// ddpqlow and ddpqdraft arent legible on some dotmatrix printers
			// If rptaudit.Printer <> "" Then
			// rptaudit.Printer.PrintQuality = ddPQMedium
			// End If
			// 
			boolAbortReport = false;
			ExemptIndex = 0;
			anyinthisgroupprinted = false;
			lastgroup = 0;
			if (!modGlobalVariables.Statics.boolBillingorCorrelated)
			{
				Field2.Text = "CORRELATED AMOUNT AUDIT REPORT";
				strLandFieldName = "rllandval";
				strBldgFieldName = "rlbldgval";
				strExemptionField = "correxemption";
			}
			else
			{
				strLandFieldName = "lastlandval";
				strBldgFieldName = "lastbldgval";
				strExemptionField = "rlexemption";
			}
			currentgroup = modGlobalVariables.Statics.gintMinAccountRange;
			// currentgroup is preincremented and we must start at 0
			if (modPrintRoutines.Statics.allorexempt == "all")
			{
				if (modGlobalVariables.Statics.whatinclude == 1)
				{
					this.GroupFooter1.Visible = false;
				}
				if (modGlobalVariables.Statics.whatinclude == 2)
				{
					// doing subtotals only so make text boxes invisible
					Detail.Height = 0;
				}
				switch (modPrintRoutines.Statics.intwhichorder)
				{
					case 1:
						{
							// by account
							if (lngTownCode > 0)
							{
								txtorderby.Text = strTownName;
							}
							else if (modGlobalVariables.Statics.boolRange)
							{
								txtorderby.Text = "From " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " To " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange);
							}
							else
							{
								txtorderby.Text = "All";
							}
							txtorderby.Text = txtorderby.Text + " Ordered by Account";
							this.GroupFooter1.Visible = false;
							break;
						}
					case 2:
					case 3:
					case 4:
						{
							if (lngTownCode > 0)
							{
								txtorderby.Text = strTownName;
							}
							else if (modGlobalVariables.Statics.boolRange)
							{
								txtorderby.Text = "From " + modPrintRoutines.Statics.gstrMinAccountRange + " TO " + modPrintRoutines.Statics.gstrMaxAccountRange;
							}
							else
							{
								txtorderby.Text = "All";
							}
							switch (modPrintRoutines.Statics.intwhichorder)
							{
								case 2:
									{
										txtorderby.Text = txtorderby.Text + " Ordered by Map/Lot";
										break;
									}
								case 3:
									{
										txtorderby.Text = txtorderby.Text + " Ordered by Name";
										break;
									}
								case 4:
									{
										txtorderby.Text = txtorderby.Text + " Ordered by Location";
										break;
									}
							}
							//end switch
							break;
						}
				}
				//end switch
				getallrecordset();
				if (rsTemp.EndOfFile())
				{
					//FC:FINAL:MSH - i.issue #1199: show message box in not modal mode
					//MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning, modal:false);
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
					boolAbortReport = true;
					return;
				}
			}
			else
			{
				// txtNumAccts.Visible = False
				// txtnumtotexempts.Visible = False
				// txtTotalnumaccts.Visible = False
				// txttotaltotallyexempt.Visible = False
				switch (modGlobalVariables.Statics.whatinclude)
				{
					case 1:
						{
							// partial exempts
							txtorderby.Text = "Partial Exempts";
							break;
						}
					case 2:
						{
							txtorderby.Text = "Total Exempts";
							break;
						}
					case 3:
						{
							txtorderby.Text = "All";
							break;
						}
					case 4:
						{
							txtorderby.Text = "Code " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange);
							break;
						}
					case 5:
						{
							txtorderby.Text = "From " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " To " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange);
							break;
						}
				}
				//end switch
				switch (modPrintRoutines.Statics.intwhichorder)
				{
					case 1:
						{
							txtorderby.Text = txtorderby.Text + " Ordered by Name";
							GroupHeader1.Visible = false;
							GroupFooter1.Visible = false;
							break;
						}
					case 2:
						{
							txtorderby.Text = txtorderby.Text + " By Name within Code";
							break;
						}
					case 3:
						{
							txtorderby.Text = txtorderby.Text + " Ordered by Map/Lot";
							GroupHeader1.Visible = false;
							GroupFooter1.Visible = false;
							break;
						}
					case 4:
						{
							txtorderby.Text = txtorderby.Text + " By Map/Lot within Code";
							break;
						}
					case 5:
						{
							txtorderby.Text = txtorderby.Text + " Ordered by Account";
							GroupHeader1.Visible = false;
							GroupFooter1.Visible = false;
							break;
						}
					case 6:
						{
							txtorderby.Text = txtorderby.Text + " By Account within Code";
							break;
						}
				}
				//end switch
				setupexemptlist();
			}
			totland = 0;
			totbuilding = 0;
			totexemption = 0;
			totAssessment = 0;
			txtAccount.Left = 90 / 1440f;
			txtAccount.Top = 0;
			txtmaplot.Left = 1800 / 1440f;
			txtmaplot.Top = 270 / 1440f;
			if (modPrintRoutines.Statics.intwhichorder == 2)
			{
				txtAccount.Left = 1800 / 1440f;
				txtAccount.Top = 270 / 1440f;
				txtmaplot.Left = 90 / 1440f;
				txtmaplot.Top = 0;
			}
			if (modPrintRoutines.Statics.intwhichorder == 4)
			{
				txtmaplot.Left = 540 / 1440f;
				txtmaplot.Width = 2880 / 1440f;
			}
			alldone = false;
			// rptaudit.Zoom = 90
			txtmuni.Text = modGlobalConstants.Statics.MuniName;
			txtdate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			intPage = 1;
			subland = 0;
			subbldg = 0;
			subexempt = 0;
			subtotal = 0;
			// If (whatinclude < 2) Or (whatinclude > 3) Then
			// txtsubland.Visible = False
			// txtsubbldg.Visible = False
			// txtsubexempt.Visible = False
			// txtsubtotal.Visible = False
			// txtgroupname.Visible = False
			// txtNumAccts.Visible = False
			// txtnumtotexempts.Visible = False
			// Field6.Visible = False
			// Field10.Visible = False
			// Field4.Visible = False
			// Field3.Visible = False
			// End If
		}

		private void allaccounts()
		{
			// prints the next line.  We are processing all account or within a range
			// we are not concerned whether exemptions exist or not
			nextaccount:
			;
			if (rsTemp.EndOfFile())
			{
				alldone = true;
				return;
			}
			//FC:FINAL:DDU:#i1068 - extra check needed, as in original if string is empty, the pass over automatically
			// TODO Get_Fields: Field [master.rsaccount] not found!! (maybe it is an alias?)
			if (rsTemp.Get_Fields("master.rsaccount") != "")
			{
				// TODO Get_Fields: Field [master.rsaccount] not found!! (maybe it is an alias?)
				while (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("rsdeleted")) || FCConvert.ToInt32(rsTemp.Get_Fields("master.rsaccount")) < 1)
				{
					rsTemp.MoveNext();
					if (rsTemp.EndOfFile())
					{
						alldone = true;
						return;
					}
				}
			}
			if (!((modGlobalVariables.Statics.whatinclude == 2) || (modGlobalVariables.Statics.whatinclude == 3)))
			{
				LngNumAccts += 1;
			}
			// acctland = Val(rsTemp.Fields(strLandFieldName) & "")
			// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
			acctland = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("landsum") + "")));
			// acctbuilding = Val(rsTemp.Fields(strBldgFieldName) & "")
			// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
			acctbuilding = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("bldgsum") + "")));
			// acctexemption = Val(rsTemp.Fields(strExemptionField) & "")
			// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
			acctexemption = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("exemptsum") + "")));
			acctassessment = acctbuilding + acctland - acctexemption;
			if ((acctexemption < (acctbuilding + acctland)) || (acctexemption == 0) || (modGlobalVariables.Statics.boolinctot))
			{
				// don't run this code if not showing totally exempts
				totbuilding += acctbuilding;
				totland += acctland;
				totexemption += acctexemption;
				totAssessment += acctassessment;
				subland += acctland;
				subbldg += acctbuilding;
				subexempt += acctexemption;
				subtotal = subland + subbldg - subexempt;
			}
			if ((modGlobalVariables.Statics.whatinclude == 1) || (modGlobalVariables.Statics.whatinclude == 3))
			{
				PRINTED = true;
				txtname.Text = Strings.Trim(rsTemp.Get_Fields_String("rsname") + "");
				// txtAccount = Trim(rsTemp.Fields("master.rsaccount") & "")
				txtAccount.Text = Strings.Trim(rsTemp.Get_Fields_Int32("rsaccount") + "");
				txtmaplot.Text = Strings.Trim(rsTemp.Get_Fields_String("rsmaplot") + "");
				if (modPrintRoutines.Statics.intwhichorder == 4)
				{
					// by location
					txtmaplot.Text = Strings.Trim(rsTemp.Get_Fields_String("rslocnumalph") + "") + " " + Strings.Trim(rsTemp.Get_Fields_String("rslocstreet") + "");
				}
				txtland.Text = Strings.Format(acctland, "###,###,###,##0");
				txtbldg.Text = Strings.Format(acctbuilding, "###,###,###,##0");
				txtexemption.Text = Strings.Format(acctexemption, "###,###,###,##0");
				txttotal.Text = Strings.Format(acctassessment, "###,###,###,##0");
				if ((acctassessment == acctexemption) && !modGlobalVariables.Statics.boolinctot)
				{
					PRINTED = false;
				}
			}
			// if whatinclude = 1 or 3
			if (!rsTemp.EndOfFile())
			{
				rsTemp.MoveNext();
				if (!rsTemp.EndOfFile())
				{
					if ((modGlobalVariables.Statics.whatinclude == 2) || (modGlobalVariables.Statics.whatinclude == 3))
					{
						// If rptaudit.Fields("thebinder").Value <> UCase(Left(rstemp.fields("rsname"), lengthoffield)) Then
						boolbinderchanged = false;
						checkthebinder();
						if (boolbinderchanged)
						{
							if (boolFirstRec)
							{
								updatethebinder();
								boolFirstRec = false;
							}
							// boolFirstRec = True
							// txtsubland = Format(subland, "###,###,###,##0")
							// txtsubbldg = Format(subbldg, "###,###,###,##0")
							// txtsubexempt = Format(subexempt, "###,###,###,##0")
							// txtsubtotal = Format(subtotal, "###,###,###,##0")
							subland = 0;
							subbldg = 0;
							subexempt = 0;
							subtotal = 0;
							PRINTED = true;
						}
					}
				}
			}
			// eof = False
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			InitializeData();
			if (boolAbortReport)
			{
				this.Close();
				return;
			}
			boolFirstRec = true;
			if (modPrintRoutines.Statics.allorexempt == "exempt")
				rptaudit.InstancePtr.Fields["thebinder"].Value = lowestcode;
			if (modPrintRoutines.Statics.allorexempt == "all")
			{
				if ((modGlobalVariables.Statics.whatinclude == 2) || (modGlobalVariables.Statics.whatinclude == 3))
				{
					updatethebinder();
				}
			}
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtdate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtmuni.Text = modGlobalConstants.Statics.MuniName;
			//this.Document.Printer.RenderMode = 1;
		}

		private void Detail_AfterPrint(object sender, EventArgs e)
		{
			// when does this fire?
		}

		private void Detail_BeforePrint(object sender, EventArgs e)
		{
			// when does this fire?
			if (modPrintRoutines.Statics.allorexempt == "all" && modGlobalVariables.Statics.whatinclude == 2)
			{
				Detail.Height = 0;
				// don't allow it to print details
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// when does this fire?
		}

		private void GroupFooter1_BeforePrint(object sender, EventArgs e)
		{
			// only concerned with exempt since the reports
			// work just different enough that the events are fired
			// at slightly different times
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// when does this fire?
			if (modPrintRoutines.Statics.allorexempt == "all")
			{
			}
			if (modPrintRoutines.Statics.allorexempt == "exempt")
			{
				txtsubland.Text = Strings.Format(sland[lastgroup], "###,###,###,##0");
				txtsubbldg.Text = Strings.Format(sbldg[lastgroup], "###,###,###,##0");
				txtsubexempt.Text = Strings.Format(sexempt[lastgroup], "###,###,###,##0");
				txtsubtotal.Text = Strings.Format(subtotalexempts[lastgroup], "###,###,###,##0");
				txtNumAccts.Text = Strings.Format(saccts[lastgroup], "###,###,###,##0");
				txtnumtotexempts.Text = Strings.Format(stexempts[lastgroup], "###,###,###,##0");
				txtTotalnumaccts.Text = LngNumAccts.ToString();
				switch (modPrintRoutines.Statics.intwhichorder)
				{
					case 1:
					case 3:
					case 5:
						{
							// 1 3 and 5 are name maplot and account
							// we dont want subtotals
							// 2 4 and 6 are name maplot and account within code
							txtsubland.Visible = false;
							txtsubbldg.Visible = false;
							txtsubexempt.Visible = false;
							txtsubtotal.Visible = false;
							Field3.Visible = false;
							Line1.Visible = false;
							break;
						}
				}
				//end switch
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			string tstring = "";
            using (clsDRWrapper dbTemp = new clsDRWrapper())
            {
                int subland = 0;
                int subbldg = 0;
                int subexempt = 0;
                int subtotal = 0;
                int subnumaccts = 0;
                int subtotexempts = 0;
                string strSQL = "";
                txtgroupname.Text = "Group = " + rptaudit.InstancePtr.Fields["thebinder"].Value;
                if (modPrintRoutines.Statics.allorexempt == "all" &&
                    ((modGlobalVariables.Statics.whatinclude == 2) || (modGlobalVariables.Statics.whatinclude == 3)))
                {
                    // If intwhichorder = 2 Then
                    const string numericFormat = "###,###,###,##0";

                    if (Strings.Trim(rptaudit.InstancePtr.Fields["thebinder"].Value + "") == string.Empty ||
                        Strings.Trim(rptaudit.InstancePtr.Fields["thebinder"].Value + "") == "'")
                    {
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            dbTemp.OpenRecordset(
                                "select count(rsaccount) as numaccts from (select rsaccount from master where (not rsdeleted = 1) and isnull(" +
                                modPrintRoutines.Statics.gstrFieldName + " , '') = '' " + strRangeWhere +
                                " group by rsaccount) as tbl", modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            dbTemp.OpenRecordset(
                                "select count(rsaccount) as numaccts from (select rsaccount from master where (not rsdeleted = 1) and isnull(" +
                                modPrintRoutines.Statics.gstrFieldName + " , '') = '' " + strRangeWhere +
                                " group by rsaccount having sum (val(" + strExemptionField +
                                " & '')) = 0 or (sum(val(" + strExemptionField + " & '')) < sum(" + strLandFieldName +
                                " + " + strBldgFieldName + ") )) as tbl", modGlobalVariables.strREDatabase);
                        }

                        subnumaccts = FCConvert.ToInt32(Math.Round(Conversion.Val(dbTemp.GetData("numaccts"))));
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            dbTemp.OpenRecordset(
                                "select  sum(" + strLandFieldName +
                                ") as thesum from master where (not rsdeleted = 1) and isnull(" +
                                modPrintRoutines.Statics.gstrFieldName + " , '') = '' " + strRangeWhere,
                                modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            dbTemp.OpenRecordset(
                                "select sum(landsum) as thesum from (select  sum(" + strLandFieldName +
                                ") as landsum from master where (not rsdeleted = 1) and isnull(" +
                                modPrintRoutines.Statics.gstrFieldName + " , '') = '' " + strRangeWhere +
                                " group by rsaccount having sum (val(" + strExemptionField +
                                " & '')) = 0 or (sum(val(" + strExemptionField + " & '')) < sum(" + strLandFieldName +
                                " + " + strBldgFieldName + ") )) as tbl", modGlobalVariables.strREDatabase);
                        }

                        subland = FCConvert.ToInt32(Math.Round(Conversion.Val(dbTemp.GetData("thesum"))));
                        txtsubland.Text = Strings.Format(subland, numericFormat);
                        txtNumAccts.Text = subnumaccts.ToString();
                        LngNumAccts += subnumaccts;
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            dbTemp.OpenRecordset(
                                "select sum(" + strBldgFieldName +
                                ") as thesum from master where (not rsdeleted = 1) and trim(rsmaplot) = '' " +
                                strRangeWhere, modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            dbTemp.OpenRecordset(
                                "select sum(bldgsum) as thesum from (select sum(" + strBldgFieldName +
                                ") as bldgsum from master where (not rsdeleted = 1) and trim(rsmaplot) = '' " +
                                strRangeWhere + " group by rsaccount having sum (val(" + strExemptionField +
                                " & '')) = 0 or (sum(val(" + strExemptionField + " & '')) < sum(" + strLandFieldName +
                                " + " + strBldgFieldName + ") )) as tbl", modGlobalVariables.strREDatabase);
                        }

                        subbldg = FCConvert.ToInt32(Math.Round(Conversion.Val(dbTemp.GetData("thesum"))));
                        txtsubbldg.Text = Strings.Format(subbldg, numericFormat);
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            dbTemp.OpenRecordset(
                                "select sum(" + strExemptionField +
                                ") as thesum from master where (not rsdeleted = 1) and trim(rsmaplot) = '' " +
                                strRangeWhere, modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            dbTemp.OpenRecordset(
                                "select sum(exemptsum) as thesum from (select sum(" + strExemptionField +
                                ") as exemptsum from master where (not rsdeleted = 1) and trim(rsmaplot) = '' " +
                                strRangeWhere + " group by rsaccount having sum (val(" + strExemptionField +
                                " & '')) = 0 or (sum(val(" + strExemptionField + " & '')) < sum(" + strLandFieldName +
                                " + " + strBldgFieldName + ") )) as tbl", modGlobalVariables.strREDatabase);
                        }

                        subexempt = FCConvert.ToInt32(Math.Round(Conversion.Val(dbTemp.GetData("thesum"))));
                        txtsubexempt.Text = Strings.Format(subexempt, numericFormat);
                        subtotal = subbldg - subexempt + subland;
                        txtsubtotal.Text = Strings.Format(subtotal, numericFormat);
                        // strsql = "select count(rsaccount) as numtotexempts from master where (((" & strBldgFieldName & " + " & strLandFieldName & " ) = " & strExemptionField & ") and (" & strExemptionField & " > 0))"
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            strSQL =
                                "select count(rsaccount) as numtotexempts from (select rsaccount from master where ";
                            strSQL += " not (rsdeleted = 1)";
                            strSQL += " and rtrim(rsmaplot) = '' " + strRangeWhere +
                                      " group by rsaccount having ((sum(" + strBldgFieldName + ") + sum(" +
                                      strLandFieldName + ") ) = sum(" + strExemptionField + ")) and (sum(" +
                                      strExemptionField + ") > 0)) as tbl";
                            dbTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                            if (!dbTemp.EndOfFile())
                            {
                                subtotexempts =
                                    FCConvert.ToInt32(Math.Round(Conversion.Val(dbTemp.GetData("numtotexempts"))));
                            }
                            else
                            {
                                subtotexempts = 0;
                            }
                        }
                        else
                        {
                            subtotexempts = 0;
                        }

                        LngNumTotExempts += subtotexempts;
                        txtnumtotexempts.Text = subtotexempts.ToString();
                    }
                    else
                    {
                        if (Strings.LCase(modPrintRoutines.Statics.gstrFieldName) != "rsaccount")
                        {
                            if (modGlobalVariables.Statics.boolinctot)
                            {
                                dbTemp.OpenRecordset(
                                    "select count(rsaccount) as numaccts  from master where (not rsdeleted = 1) and " +
                                    modPrintRoutines.Statics.gstrFieldName + " like '" +
                                    rptaudit.InstancePtr.Fields["thebinder"].Value + "%' " + strRangeWhere,
                                    "RealEstate");

                            }
                            else
                            {
                                dbTemp.OpenRecordset(
                                    "select count(rsaccount) as numaccts  from (select rsaccount from master where (not rsdeleted = 1) and " +
                                    modPrintRoutines.Statics.gstrFieldName + " like '" +
                                    rptaudit.InstancePtr.Fields["thebinder"].Value + @"%' " + strRangeWhere +
                                    " group by rsaccount having sum (val(" + strExemptionField +
                                    " & '')) = 0 or (sum(val(" + strExemptionField + " & '')) < sum(" +
                                    strLandFieldName + " + " + strBldgFieldName + ") )  ) as tbl",
                                    modGlobalVariables.strREDatabase);
                            }
                        }
                        else
                        {
                            if (modGlobalVariables.Statics.boolinctot)
                            {
                                dbTemp.OpenRecordset(
                                    "select count(rsaccount) as numaccts  from (select rsaccount from master where not rsdeleted = 1  " +
                                    strRangeWhere + " group by rsaccount) as tbl", modGlobalVariables.strREDatabase);
                            }
                            else
                            {
                                dbTemp.OpenRecordset(
                                    "select count(rsaccount) as numaccts  from (select rsaccount from master where (not rsdeleted = 1  " +
                                    strRangeWhere + " group by rsaccount having sum (val(" + strExemptionField +
                                    " & '')) = 0 or (sum(val(" + strExemptionField + " & '')) < sum(" +
                                    strLandFieldName + " + " + strBldgFieldName + ") )  ) as tbl",
                                    modGlobalVariables.strREDatabase);
                            }
                        }

                        // TODO Get_Fields: Field [numaccts] not found!! (maybe it is an alias?)
                        subnumaccts = FCConvert.ToInt32(Math.Round(Conversion.Val(dbTemp.Get_Fields("numaccts"))));
                        // Call dbTemp.OpenRecordset("select sum(" & strLandFieldName & ") as thesum from master where (not rsdeleted = 1) and rsmaplot like '" & rptaudit.Fields("thebinder").Value & "*'", strredatabase)
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            dbTemp.OpenRecordset(
                                "select sum(" + strLandFieldName +
                                ") as thesum from master where (not rsdeleted = 1) and " +
                                modPrintRoutines.Statics.gstrFieldName + " like '" +
                                rptaudit.InstancePtr.Fields["thebinder"].Value + @"%' " + strRangeWhere,
                                modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            dbTemp.OpenRecordset(
                                "select sum(thelandsum) as thesum from (select sum(" + strLandFieldName +
                                ") as thelandsum from master where (not rsdeleted = 1) and " +
                                modPrintRoutines.Statics.gstrFieldName + " like '" +
                                rptaudit.InstancePtr.Fields["thebinder"].Value + @"%' " + strRangeWhere +
                                " group by rsaccount having sum (val(" + strExemptionField +
                                " & '')) = 0 or (sum(val(" + strExemptionField + " & '')) < sum(" + strLandFieldName +
                                " + " + strBldgFieldName + ") ) ) as tbl ", modGlobalVariables.strREDatabase);
                        }

                        // TODO Get_Fields: Field [thesum] not found!! (maybe it is an alias?)
                        subland = FCConvert.ToInt32(Math.Round(Conversion.Val(dbTemp.Get_Fields("thesum"))));
                        // subnumaccts = dbTemp.GetData("numaccts")
                        txtNumAccts.Text = subnumaccts.ToString();
                        LngNumAccts += subnumaccts;
                        txtsubland.Text = Strings.Format(subland, numericFormat);
                        // Call dbTemp.OpenRecordset("select sum(" & strBldgFieldName & ") as thesum from master where (not rsdeleted = 1) and rsmaplot like '" & rptaudit.Fields("thebinder").Value & "*'", strredatabase)
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            dbTemp.OpenRecordset(
                                "select sum(" + strBldgFieldName +
                                ") as thesum from master where (not rsdeleted = 1) and " +
                                modPrintRoutines.Statics.gstrFieldName + " like '" +
                                rptaudit.InstancePtr.Fields["thebinder"].Value + @"%' " + strRangeWhere,
                                modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            dbTemp.OpenRecordset(
                                "select sum(bldgsum) as thesum from (select sum(" + strBldgFieldName +
                                ") as bldgsum from master where (not rsdeleted = 1) and " +
                                modPrintRoutines.Statics.gstrFieldName + " like '" +
                                rptaudit.InstancePtr.Fields["thebinder"].Value + @"%' " + strRangeWhere +
                                " group by rsaccount having sum (val(" + strExemptionField +
                                " & '')) = 0 or (sum(val(" + strExemptionField + " & '')) < sum(" + strLandFieldName +
                                " + " + strBldgFieldName + ") )) as tbl", modGlobalVariables.strREDatabase);
                        }

                        subbldg = FCConvert.ToInt32(Math.Round(Conversion.Val(dbTemp.GetData("thesum"))));
                        txtsubbldg.Text = Strings.Format(subbldg, numericFormat);
                        // Call dbTemp.OpenRecordset("select sum(" & strExemptionField & ") as thesum from master where (not rsdeleted = 1) and rsmaplot like '" & rptaudit.Fields("thebinder").Value & "*'", strredatabase)
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            dbTemp.OpenRecordset(
                                "select sum(" + strExemptionField +
                                ") as thesum from master where (not rsdeleted = 1) and " +
                                modPrintRoutines.Statics.gstrFieldName + " like '" +
                                rptaudit.InstancePtr.Fields["thebinder"].Value + "*' " + strRangeWhere,
                                modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            dbTemp.OpenRecordset(
                                "select sum(exemptsum) as thesum from (select sum(" + strExemptionField +
                                ") as exemptsum from master where (not rsdeleted = 1) and " +
                                modPrintRoutines.Statics.gstrFieldName + " like '" +
                                rptaudit.InstancePtr.Fields["thebinder"].Value + "*' " + strRangeWhere +
                                " group by rsaccount having sum (val(" + strExemptionField +
                                " & '')) = 0 or (sum(val(" + strExemptionField + " & '')) < sum(" + strLandFieldName +
                                " + " + strBldgFieldName + ") )) as tbl", modGlobalVariables.strREDatabase);
                        }

                        subexempt = FCConvert.ToInt32(Math.Round(Conversion.Val(dbTemp.GetData("thesum"))));
                        txtsubexempt.Text = Strings.Format(subexempt, numericFormat);
                        subtotal = subbldg - subexempt + subland;
                        txtsubtotal.Text = Strings.Format(subtotal, numericFormat);
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            strSQL =
                                "select count(rsaccount) as numtotexempts from (select rsaccount from master where ";
                            strSQL += " not rsdeleted = 1";
                            // strSQL = strSQL & " and rsmaplot like '" & rptaudit.Fields("thebinder").Value & "*' group by rsaccount having ((sum(" & strBldgFieldName & ") + sum(" & strLandFieldName & ") ) = sum(" & strExemptionField & ")) and (sum(" & strExemptionField & ") > 0))"
                            if (Strings.LCase(modPrintRoutines.Statics.gstrFieldName) != "rsaccount")
                            {
                                strSQL += " and " + modPrintRoutines.Statics.gstrFieldName + " like '" +
                                          rptaudit.InstancePtr.Fields["thebinder"].Value + @"%' " + strRangeWhere +
                                          " group by rsaccount having ((sum(" + strBldgFieldName + ") + sum(" +
                                          strLandFieldName + ") , sum(" + strExemptionField + ")) and (sum(" +
                                          strExemptionField + ") > 0)) as tbl";
                            }
                            else
                            {
                                strSQL += "  " + strRangeWhere + " group by rsaccount having ((sum(" +
                                          strBldgFieldName + ") + sum(" + strLandFieldName + ") ) = sum(" +
                                          strExemptionField + ")) and (sum(" + strExemptionField + ") > 0)) as tbl";
                            }

                            dbTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                            if (!dbTemp.EndOfFile())
                            {
                                subtotexempts =
                                    FCConvert.ToInt32(Math.Round(Conversion.Val(dbTemp.GetData("numtotexempts"))));
                            }
                            else
                            {
                                subtotexempts = 0;
                            }
                        }
                        else
                        {
                            subtotexempts = 0;
                        }

                        LngNumTotExempts += subtotexempts;
                        txtnumtotexempts.Text = subtotexempts.ToString();
                    }

                    // End If
                }

                if (modPrintRoutines.Statics.allorexempt == "exempt")
                {
                    clsDRWrapper temp = modDataTypes.Statics.CR;
                    modREMain.OpenCRTable(ref temp, 1900 + currentgroup);
                    modDataTypes.Statics.CR = temp;
                    tstring = " " + Strings.Trim(modDataTypes.Statics.CR.Get_Fields_String("cldesc") + "");
                    if (currentgroup == 0)
                        tstring = "";
                    txtgroupname.Text = "CODE " + FCConvert.ToString(currentgroup) + tstring;
                    lastgroup = currentgroup;
                }
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtpage.Text = "Page " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		private void getallrecordset()
		{
			clsDRWrapper dcTemp = new clsDRWrapper();
			string strSQL = "";
			string strTemp = "";
			string strAuditQuery = "";
            try
            {
                // On Error GoTo ErrorHandler
                if (!modGlobalVariables.Statics.boolinctot)
                {
                    strTemp = ",having (sum(" + strLandFieldName + ") + sum(" + strBldgFieldName + ") - sum(" +
                              strExemptionField + ") > 0) or (sum(" + strExemptionField + ") = 0)";
                }
                else
                {
                    strTemp = "";
                }

                string strMasterJoin;
                strMasterJoin = modREMain.GetMasterJoin();
                switch (modPrintRoutines.Statics.intwhichorder)
                {
                    case 1:
                    {
                        // by account
                        modPrintRoutines.Statics.gstrFieldName = "RSACCOUNT";
                        txtorder = "mparty.rsaccount";
                        break;
                    }
                    case 3:
                    {
                        // by name
                        modPrintRoutines.Statics.gstrFieldName = "DeedName1";
                        txtorder = "DeedName1,mparty.rsaccount";
                        break;
                    }
                    case 2:
                    {
                        // by maplot
                        modPrintRoutines.Statics.gstrFieldName = "rsmaplot";
                        txtorder = "rsmaplot,mparty.rsaccount";
                        break;
                    }
                    case 4:
                    {
                        // by location
                        modPrintRoutines.Statics.gstrFieldName = "rslocstreet";
                        txtorder = "rslocstreet,rslocnumalph,mparty.rsaccount";
                        break;
                    }
                }

                //end switch
                strRangeWhere = "";
                if (lngTownCode > 0)
                {
                    strRangeWhere = " and ritrancode = " + FCConvert.ToString(lngTownCode);
                    modPrintRoutines.Statics.gstrMinAccountRange = FCConvert.ToString(lngTownCode);
                    modPrintRoutines.Statics.gstrMaxAccountRange = FCConvert.ToString(lngTownCode);
                    if (modGlobalVariables.Statics.boolinctot)
                    {
                        // Call dcTemp.CreateStoredProcedure("AuditQuery", "select rsaccount, sum(" & strLandFieldName & ") as landsum,sum(" & strBldgFieldName & ") as bldgsum,sum(" & strExemptionField & ") as exemptsum from master where (not rsdeleted = 1) and ritrancode = " & lngTownCode & " group by rsaccount", strREDatabase)
                        strAuditQuery = "(select rsaccount, sum(" + strLandFieldName + ") as landsum,sum(" +
                                        strBldgFieldName + ") as bldgsum,sum(" + strExemptionField +
                                        ") as exemptsum from master where (not rsdeleted = 1) and ritrancode = " +
                                        FCConvert.ToString(lngTownCode) + " group by rsaccount) AuditQuery";
                    }
                    else
                    {
                        // Call dcTemp.CreateStoredProcedure("AuditQuery", "select rsaccount, sum(" & strLandFieldName & ") as landsum,sum(" & strBldgFieldName & ") as bldgsum,sum(" & strExemptionField & ") as exemptsum from master where (not rsdeleted = 1) and ritrancode = " & lngTownCode & " group by rsaccount having (sum(" & strLandFieldName & ") + sum(" & strBldgFieldName & ") - sum(" & strExemptionField & ") > 0) or (sum(" & strExemptionField & ") = 0)", strREDatabase)
                        strAuditQuery = "(select rsaccount, sum(" + strLandFieldName + ") as landsum,sum(" +
                                        strBldgFieldName + ") as bldgsum,sum(" + strExemptionField +
                                        ") as exemptsum from master where (not rsdeleted = 1) and ritrancode = " +
                                        FCConvert.ToString(lngTownCode) + " group by rsaccount having (sum(" +
                                        strLandFieldName + ") + sum(" + strBldgFieldName + ") - sum(" +
                                        strExemptionField + ") > 0) or (sum(" + strExemptionField +
                                        ") = 0)) AuditQuery";
                    }

                    rsTemp.OpenRecordset(
                        strMasterJoin + " inner join " + strAuditQuery +
                        " on (auditquery.rsaccount = mparty.rsaccount) where master.mparty = 1 order by " + txtorder,
                        modGlobalVariables.strREDatabase);
                }
                else if (modGlobalVariables.Statics.boolRange)
                {
                    if (modPrintRoutines.Statics.intwhichorder == 1)
                    {
                        strRangeWhere = " and " + modPrintRoutines.Statics.gstrFieldName + " between " +
                                        FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " +
                                        FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange);
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            strAuditQuery = "(select rsaccount, sum(" + strLandFieldName + ") as landsum,sum(" +
                                            strBldgFieldName + ") as bldgsum,sum(" + strExemptionField +
                                            ") as exemptsum from master where (not rsdeleted = 1) and " +
                                            modPrintRoutines.Statics.gstrFieldName + " >= " +
                                            FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) +
                                            " and " + modPrintRoutines.Statics.gstrFieldName + " <= " +
                                            FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) +
                                            " group by rsaccount) AuditQuery";
                        }
                        else
                        {
                            strAuditQuery = "(select rsaccount, sum(" + strLandFieldName + ") as landsum,sum(" +
                                            strBldgFieldName + ") as bldgsum,sum(" + strExemptionField +
                                            ") as exemptsum from master where (not rsdeleted = 1) and " +
                                            modPrintRoutines.Statics.gstrFieldName + " >= " +
                                            FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) +
                                            " and " + modPrintRoutines.Statics.gstrFieldName + " <= " +
                                            FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) +
                                            " group by rsaccount having (sum(" + strLandFieldName + ") + sum(" +
                                            strBldgFieldName + ") - sum(" + strExemptionField + ") > 0) or (sum(" +
                                            strExemptionField + ") = 0)) AuditQuery";
                        }

                        rsTemp.OpenRecordset(
                            strMasterJoin + " inner join " + strAuditQuery +
                            " on (auditquery.rsaccount = mparty.rsaccount) where mparty.rscard = 1 order by " +
                            txtorder, modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        strRangeWhere = " and " + modPrintRoutines.Statics.gstrFieldName + " between '" +
                                        modPrintRoutines.Statics.gstrMinAccountRange + "' and '" +
                                        modPrintRoutines.Statics.gstrMaxAccountRange + "' ";
                        dcTemp.CreateStoredProcedure("AuditQuery",
                            "select rsaccount,sum(" + strLandFieldName + ") as landsum,sum(" + strBldgFieldName +
                            ") as bldgsum, sum(" + strExemptionField +
                            ") as exemptsum from master where not rsdeleted = 1 and " +
                            modPrintRoutines.Statics.gstrFieldName + " >= '" +
                            modPrintRoutines.Statics.gstrMinAccountRange + "' and " +
                            modPrintRoutines.Statics.gstrFieldName + " <= '" +
                            modPrintRoutines.Statics.gstrMaxAccountRange + "' group by rsaccount",
                            modGlobalVariables.strREDatabase);
                        strAuditQuery = "(select rsaccount,sum(" + strLandFieldName + ") as landsum,sum(" +
                                        strBldgFieldName + ") as bldgsum, sum(" + strExemptionField +
                                        ") as exemptsum from master where not rsdeleted = 1 and " +
                                        modPrintRoutines.Statics.gstrFieldName + " >= '" +
                                        modPrintRoutines.Statics.gstrMinAccountRange + "' and " +
                                        modPrintRoutines.Statics.gstrFieldName + " <= '" +
                                        modPrintRoutines.Statics.gstrMaxAccountRange +
                                        "' group by rsaccount) AuditQuery";
                        rsTemp.OpenRecordset(
                            strMasterJoin + " inner join " + strAuditQuery +
                            " on (auditquery.rsaccount = mparty.rsaccount) where mparty.rscard = 1 order by " +
                            txtorder, modGlobalVariables.strREDatabase);
                    }
                }
                else
                {
                    if (modGlobalVariables.Statics.boolinctot)
                    {
                        strAuditQuery = "(select rsaccount,sum(" + strLandFieldName + ") as landsum, sum(" +
                                        strBldgFieldName + ") as bldgsum,sum(" + strExemptionField +
                                        ") as exemptsum from master where not rsdeleted = 1 group by rsaccount) AuditQuery";
                    }
                    else
                    {
                        strAuditQuery = "select rsaccount,sum(" + strLandFieldName + ") as landsum, sum(" +
                                        strBldgFieldName + ") as bldgsum,sum(" + strExemptionField +
                                        ") as exemptsum from master where not rsdeleted = 1 group by rsaccount having (sum(" +
                                        strLandFieldName + ") + sum(" + strBldgFieldName + ") - sum(" +
                                        strExemptionField + ") > 0) or (sum(" + strExemptionField + ") = 0)";
                    }

                    rsTemp.OpenRecordset(
                        strMasterJoin + " inner join " + strAuditQuery +
                        " on (auditquery.rsaccount = mparty.rsaccount) where mparty.rscard = 1 order by " + txtorder,
                        modGlobalVariables.strREDatabase);
                }

                if (modPrintRoutines.Statics.allorexempt == "all")
                {
                    txtfinaltotal.Visible = false;
                    txttotalaccts.Visible = false;
                    txtTotalnumaccts.Visible = false;
                    txttotaltotallyexempt.Visible = false;
                    txttotnumtotexempts.Visible = false;
                    txttotland.Visible = false;
                    txttotbuilding.Visible = false;
                    txttotexemption.Visible = false;
                    txttotassessment.Visible = false;
                    // If (whatinclude < 2) Or (whatinclude > 3) Then
                    if (lngTownCode > 0)
                    {
                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            dcTemp.OpenRecordset(
                                "select count(rsaccount) as thecount from (SELECT rsaccount FROM Master where (not rsdeleted = 1) and ritrancode = " +
                                FCConvert.ToString(lngTownCode) + " group by rsaccount) as tbl",
                                modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            dcTemp.OpenRecordset(
                                "select count(rsaccount) as thecount from (SELECT rsaccount FROM Master where (not rsdeleted = 1) and ritrancode = " +
                                FCConvert.ToString(lngTownCode) + " group by rsaccount) having (sum(" +
                                strLandFieldName + ") + sum(" + strBldgFieldName + ") - sum(" + strExemptionField +
                                ") > 0) or (sum(" + strExemptionField + ") = 0) as tbl",
                                modGlobalVariables.strREDatabase);
                        }

                        if (!dcTemp.EndOfFile())
                        {
                            // TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
                            LngNumAccts = FCConvert.ToInt32(Math.Round(Conversion.Val(dcTemp.Get_Fields("thecount"))));
                        }
                        else
                        {
                            LngNumAccts = 0;
                        }

                        strSQL = "select count(rsaccount) as numtotexempts from (select rsaccount from master where ";
                        strSQL += " not (rsdeleted)";
                        strSQL += " and ritrancode = " + FCConvert.ToString(lngTownCode) +
                                  " group by rsaccount having ((sum(" + strBldgFieldName + ") + sum(" +
                                  strLandFieldName + ") ) = sum(" + strExemptionField + ")) and (sum(" +
                                  strExemptionField + ") > 0)) as tbl";
                        SubReport1.Report = new srptAuditTotals();
                        if (modGlobalVariables.Statics.boolBillingorCorrelated)
                        {
                            SubReport1.Report.UserData =
                                "L, and ritrancode = " + FCConvert.ToString(lngTownCode) + strTemp;
                        }
                        else
                        {
                            SubReport1.Report.UserData =
                                "C, and ritrancode = " + FCConvert.ToString(lngTownCode) + strTemp;
                        }

                        dcTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                        LngNumTotExempts = 0;
                        if (!dcTemp.EndOfFile() && modGlobalVariables.Statics.boolinctot)
                        {
                            LngNumTotExempts =
                                FCConvert.ToInt32(Math.Round(Conversion.Val(dcTemp.GetData("numtotexempts"))));
                        }
                    }
                    else if (modGlobalVariables.Statics.boolRange)
                    {
                        if (modPrintRoutines.Statics.intwhichorder == 1)
                        {
                            if (modGlobalVariables.Statics.boolinctot)
                            {
                                dcTemp.OpenRecordset(
                                    "select count(rsaccount) as thecount from (SELECT rsaccount FROM Master where (not rsdeleted = 1) and " +
                                    modPrintRoutines.Statics.gstrFieldName + " >= " +
                                    FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " +
                                    modPrintRoutines.Statics.gstrFieldName + " <= " +
                                    FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) +
                                    " group by rsaccount) as tbl", modGlobalVariables.strREDatabase);
                            }
                            else
                            {
                                dcTemp.OpenRecordset(
                                    "select count(rsaccount) as thecount from (SELECT rsaccount FROM Master where (not rsdeleted = 1) and " +
                                    modPrintRoutines.Statics.gstrFieldName + " >= " +
                                    FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " +
                                    modPrintRoutines.Statics.gstrFieldName + " <= " +
                                    FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) +
                                    " group by rsaccount having (sum(" + strLandFieldName + ") + sum(" +
                                    strBldgFieldName + ") - sum(" + strExemptionField + ") > 0) or (sum(" +
                                    strExemptionField + ") = 0)) as tbl", modGlobalVariables.strREDatabase);
                            }

                            if (!dcTemp.EndOfFile())
                            {
                                LngNumAccts = FCConvert.ToInt32(Math.Round(Conversion.Val(dcTemp.GetData("thecount"))));
                            }
                            else
                            {
                                LngNumAccts = 0;
                            }

                            strSQL =
                                "select count(rsaccount) as numtotexempts from (select rsaccount from master where ";
                            strSQL += " not (rsdeleted = 1)";
                            strSQL += " and " + modPrintRoutines.Statics.gstrFieldName + " >= " +
                                      FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " +
                                      modPrintRoutines.Statics.gstrFieldName + " <= " +
                                      FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) +
                                      " group by rsaccount having ((sum(" + strBldgFieldName + ") + sum(" +
                                      strLandFieldName + ") ) = sum(" + strExemptionField + ")) and (sum(" +
                                      strExemptionField + ") > 0)) as tbl";
                            SubReport1.Report = new srptAuditTotals();
                            if (modGlobalVariables.Statics.boolBillingorCorrelated)
                            {
                                SubReport1.Report.UserData = "L, and " + modPrintRoutines.Statics.gstrFieldName +
                                                             " >= " +
                                                             FCConvert.ToString(modGlobalVariables.Statics
                                                                 .gintMinAccountRange) + " and " +
                                                             modPrintRoutines.Statics.gstrFieldName + " <= " +
                                                             FCConvert.ToString(modGlobalVariables.Statics
                                                                 .gintMaxAccountRange) + strTemp;
                            }
                            else
                            {
                                SubReport1.Report.UserData = "C, and " + modPrintRoutines.Statics.gstrFieldName +
                                                             " >= " +
                                                             FCConvert.ToString(modGlobalVariables.Statics
                                                                 .gintMinAccountRange) + " and " +
                                                             modPrintRoutines.Statics.gstrFieldName + " <= " +
                                                             FCConvert.ToString(modGlobalVariables.Statics
                                                                 .gintMaxAccountRange) + strTemp;
                            }

                            dcTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                            LngNumTotExempts = 0;
                            if (!dcTemp.EndOfFile() && modGlobalVariables.Statics.boolinctot)
                            {
                                LngNumTotExempts =
                                    FCConvert.ToInt32(Math.Round(Conversion.Val(dcTemp.GetData("numtotexempts"))));
                            }
                        }
                        else
                        {
                            if (modGlobalVariables.Statics.boolinctot)
                            {
                                dcTemp.OpenRecordset(
                                    "select count(rsaccount) as thecount from (SELECT rsaccount FROM Master where (not rsdeleted = 1) and " +
                                    modPrintRoutines.Statics.gstrFieldName + " >= '" +
                                    modPrintRoutines.Statics.gstrMinAccountRange + "' and " +
                                    modPrintRoutines.Statics.gstrFieldName + " <= '" +
                                    modPrintRoutines.Statics.gstrMaxAccountRange + "' group by rsaccount) as tbl",
                                    modGlobalVariables.strREDatabase);
                            }
                            else
                            {
                                dcTemp.OpenRecordset(
                                    "select count(rsaccount) as thecount from (select rsaccount from master where (not rsdeleted = 1) and " +
                                    modPrintRoutines.Statics.gstrFieldName + " >= '" +
                                    modPrintRoutines.Statics.gstrMinAccountRange + "' and " +
                                    modPrintRoutines.Statics.gstrFieldName + " <= '" +
                                    modPrintRoutines.Statics.gstrMaxAccountRange + "' group by rsaccount having (sum(" +
                                    strLandFieldName + ") + sum(" + strBldgFieldName + ") - sum(" + strExemptionField +
                                    ") > 0) or (sum(" + strExemptionField + ") = 0)) as tbl ",
                                    modGlobalVariables.strREDatabase);
                            }

                            if (!dcTemp.EndOfFile())
                            {
                                LngNumAccts = FCConvert.ToInt32(Math.Round(Conversion.Val(dcTemp.GetData("thecount"))));
                            }
                            else
                            {
                                LngNumAccts = 0;
                            }

                            strSQL =
                                "select count(rsaccount) as numtotexempts from (select rsaccount from master where ";
                            strSQL += " not (rsdeleted = 1)";
                            strSQL += " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" +
                                      modPrintRoutines.Statics.gstrMinAccountRange + "' and " +
                                      modPrintRoutines.Statics.gstrFieldName + " <= '" +
                                      modPrintRoutines.Statics.gstrMaxAccountRange +
                                      "' group by rsaccount having ((sum(" + strBldgFieldName + ") + sum(" +
                                      strLandFieldName + ") ) = sum(" + strExemptionField + ")) and (sum(" +
                                      strExemptionField + ") > 0)) as tbl";
                            SubReport1.Report = new srptAuditTotals();
                            if (modGlobalVariables.Statics.boolBillingorCorrelated)
                            {
                                SubReport1.Report.UserData = "L, and " + modPrintRoutines.Statics.gstrFieldName +
                                                             " >= '" + modPrintRoutines.Statics.gstrMinAccountRange +
                                                             "' and " + modPrintRoutines.Statics.gstrFieldName +
                                                             " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange +
                                                             "'" + strTemp + strTemp;
                            }
                            else
                            {
                                SubReport1.Report.UserData = "C, and " + modPrintRoutines.Statics.gstrFieldName +
                                                             " >= '" + modPrintRoutines.Statics.gstrMinAccountRange +
                                                             "' and " + modPrintRoutines.Statics.gstrFieldName +
                                                             " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange +
                                                             "'" + strTemp + strTemp;
                            }

                            dcTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                            LngNumTotExempts = 0;
                            if (!dcTemp.EndOfFile() && modGlobalVariables.Statics.boolinctot)
                            {
                                LngNumTotExempts =
                                    FCConvert.ToInt32(Math.Round(Conversion.Val(dcTemp.GetData("numtotexempts"))));
                            }
                        }
                    }
                    else
                    {
                        SubReport1.Report = new srptAuditTotals();
                        if (modGlobalVariables.Statics.boolBillingorCorrelated)
                        {
                            SubReport1.Report.UserData = "L, " + strTemp;
                        }
                        else
                        {
                            SubReport1.Report.UserData = "C, " + strTemp;
                        }

                        if (modGlobalVariables.Statics.boolinctot)
                        {
                            dcTemp.OpenRecordset(
                                "select count(rsaccount) as thecount from (select rsaccount from master where not rsdeleted = 1 group by rsaccount) a",
                                modGlobalVariables.strREDatabase);
                        }
                        else
                        {
                            dcTemp.OpenRecordset(
                                "select count(rsaccount) as thecount from (select rsaccount from master where not rsdeleted = 1 group by rsaccount having (sum(" +
                                strLandFieldName + ") + sum(" + strBldgFieldName + ") - sum(" + strExemptionField +
                                ") > 0) or (sum(" + strExemptionField + ") = 0)) a", modGlobalVariables.strREDatabase);
                        }

                        if (!dcTemp.EndOfFile())
                        {
                            LngNumAccts = FCConvert.ToInt32(Math.Round(Conversion.Val(dcTemp.GetData("thecount"))));
                        }
                        else
                        {
                            LngNumAccts = 0;
                        }

                        strSQL = "select count(rsaccount) as numtotexempts from (select rsaccount from master where ";
                        strSQL += " not (rsdeleted = 1)";
                        strSQL += " group by rsaccount having ((sum(" + strBldgFieldName + ") + sum(" +
                                  strLandFieldName + ") ) = sum(" + strExemptionField + ")) and (sum(" +
                                  strExemptionField + ") > 0)) a";
                        dcTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                        LngNumTotExempts = 0;
                        if (!dcTemp.EndOfFile() && modGlobalVariables.Statics.boolinctot)
                        {
                            LngNumTotExempts =
                                FCConvert.ToInt32(Math.Round(Conversion.Val(dcTemp.GetData("numtotexempts"))));
                        }
                    }

                    // End If
                }

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                //FC:FINAL:MSH - i.issue #1199: show message box in not modal mode
                //MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Get All Recordset", null, MessageBoxButtons.OK, MessageBoxIcon.Hand, modal:false);
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In Get All Recordset", null, MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				dcTemp.Dispose();
            }
		}

		private void updatethebinder()
		{
			// for use with groups but use updateexemptbinder if using exempt
			// only report
			if (modPrintRoutines.Statics.intwhichorder == 3)
			{
				// name
				if (!rsTemp.EndOfFile())
					rptaudit.InstancePtr.Fields["thebinder"].Value = Strings.UCase(Strings.Left(rsTemp.Get_Fields_String("RSNAME") + "          ", modGlobalVariables.Statics.lengthoffield));
			}
			if (modPrintRoutines.Statics.intwhichorder == 4)
			{
				// location
				if (!rsTemp.EndOfFile())
				{
					rptaudit.InstancePtr.Fields["thebinder"].Value = Strings.UCase(Strings.Left(rsTemp.Get_Fields_String("rslocstreet") + "          ", modGlobalVariables.Statics.lengthoffield));
				}
			}
			if (modPrintRoutines.Statics.intwhichorder == 2)
			{
				// maplot
				if (!rsTemp.EndOfFile())
				{
					// Dim tstring As String
					// Dim intpos As Integer
					// Dim x As Integer
					// Dim holdstring
					// 
					// tstring = Trim(rsTemp.fields("rsmaplot") & "")
					// intpos = InStr(1, tstring, "-", vbTextCompare)
					// If intpos <> 0 Then
					// intpos = 1
					// For x = 1 To lengthoffield
					// intpos = InStr(intpos, tstring, "-", vbTextCompare)
					// If intpos > 0 Then holdstring = Left(tstring, intpos - 1)
					// If intpos = 0 Then
					// holdstring = tstring
					// Exit For
					// End If
					// intpos = intpos + 1
					// Next x
					// Else
					// holdstring = Left(tstring, lengthoffield * 4)
					// End If
					rptaudit.InstancePtr.Fields["thebinder"].Value = Strings.UCase(Strings.Left(rsTemp.Get_Fields_String("rsmaplot") + "          ", modGlobalVariables.Statics.lengthoffield));
				}
			}
		}

		private void checkthebinder()
		{
			// checks to see if the binder value has changed
			if (!rsTemp.EndOfFile())
			{
				if (modPrintRoutines.Statics.intwhichorder == 3)
				{
					// name
					if (rptaudit.InstancePtr.Fields["thebinder"].Value == Strings.UCase(Strings.Left(FCConvert.ToString(rsTemp.Get_Fields_String("RSNAME")), modGlobalVariables.Statics.lengthoffield)))
					{
						boolbinderchanged = false;
					}
					else
					{
						boolbinderchanged = true;
					}
				}
				// intwhichorder = 3
				if (modPrintRoutines.Statics.intwhichorder == 4)
				{
					if (rptaudit.InstancePtr.Fields["thebinder"].Value == Strings.UCase(rsTemp.Get_Fields_String("rslocstreet") + ""))
					{
						boolbinderchanged = false;
					}
					else
					{
						boolbinderchanged = true;
					}
				}
				if (modPrintRoutines.Statics.intwhichorder == 2)
				{
					string tstring = "";
					int intpos = 0;
					int x;
					// vbPorter upgrade warning: holdstring As Variant --> As string
					string holdstring = "";
					tstring = Strings.Trim(rsTemp.Get_Fields_String("rsmaplot") + "");
					intpos = Strings.InStr(1, tstring, "-", CompareConstants.vbTextCompare);
					if (intpos != 0)
					{
						intpos = 1;
						for (x = 1; x <= modGlobalVariables.Statics.lengthoffield; x++)
						{
							intpos = Strings.InStr(intpos, tstring, "-", CompareConstants.vbTextCompare);
							if (intpos > 0)
								holdstring = Strings.Left(tstring, intpos - 1);
							if (intpos == 0)
							{
								holdstring = tstring;
								break;
							}
							intpos += 1;
						}
						// x
					}
					else
					{
						holdstring = Strings.Left(tstring, modGlobalVariables.Statics.lengthoffield * 4);
					}
					if (rptaudit.InstancePtr.Fields["thebinder"].Value == Strings.UCase(holdstring + ""))
					{
						boolbinderchanged = false;
					}
					else
					{
						boolbinderchanged = true;
					}
				}
			}
		}

		private void exemptaccounts(ref int ListIndex)
		{
			int forindex;
			// processes the exemptonly report
			switch (modPrintRoutines.Statics.intwhichorder)
			{
			// 1 3 and 5 are sorted by name maplot or account
			// 2 4 and 6 are name maplot and account within a code
				case 1:
				case 3:
				case 5:
					{
						acctland = exemptlist[ListIndex].Land;
						acctbuilding = exemptlist[ListIndex].Building;
						acctexemption = exemptlist[ListIndex].totalexemption;
						acctassessment = acctbuilding + acctland - acctexemption;
						totbuilding += acctbuilding;
						totland += acctland;
						totexemption += acctexemption;
						totAssessment += acctassessment;
						subland += acctland;
						subbldg += acctbuilding;
						subexempt += acctexemption;
						subExemptTotal = subexempt;
						subtotal = subland + subbldg - subexempt;
						LngNumAccts += 1;
						PRINTED = true;
						txtname.Text = exemptlist[ListIndex].Name;
						txtAccount.Text = exemptlist[ListIndex].AccountNumber.ToString();
						txtmaplot.Text = exemptlist[ListIndex].MapLot;
						txtland.Text = Strings.Format(acctland, "###,###,###,###");
						txtbldg.Text = Strings.Format(acctbuilding, "###,###,###,###");
						txtexemption.Text = Strings.Format(acctexemption, "###,###,###,###");
						txttotal.Text = Strings.Format(acctassessment, "###,###,###,###");
						if (acctassessment == 0 && (acctland + acctbuilding) > 0)
						{
							LngNumTotExempts += 1;
						}
						switch (modGlobalVariables.Statics.whatinclude)
						{
							case 1:
								{
									break;
								}
							case 2:
								{
									break;
								}
							case 3:
								{
									// include both total and partial exempts
									// dont need to do anything extra
									break;
								}
							case 4:
								{
									break;
								}
							case 5:
								{
									break;
								}
						}
						//end switch
						break;
					}
				case 2:
				case 4:
				case 6:
					{
						int texempt = 0;
						PRINTED = false;
						if (currentgroup == 0)
						{
							if ((exemptlist[ListIndex].Code[1] == 0) && (exemptlist[ListIndex].Code[2] == 0) && (exemptlist[ListIndex].Code[3] == 0))
							{
								texempt = exemptlist[ListIndex].totalexemption;
								PRINTED = true;
							}
						}
						else
						{
							texempt = 0;
							if (exemptlist[ListIndex].Code[1] == currentgroup)
							{
								PRINTED = true;
								texempt = exemptlist[ListIndex].Exemption[1];
							}
							if (exemptlist[ListIndex].Code[2] == currentgroup)
							{
								PRINTED = true;
								texempt += exemptlist[ListIndex].Exemption[2];
							}
							if (exemptlist[ListIndex].Code[3] == currentgroup)
							{
								PRINTED = true;
								texempt += exemptlist[ListIndex].Exemption[3];
							}
						}
						// current group = 0
						if (PRINTED == true)
						{
							this.Fields["numaccts"].Value = Conversion.Val(this.Fields["numaccts"].Value) + 1;
							if (exemptlist[ListIndex].beenprinted == 0)
							{
								LngNumAccts += 1;
							}
							// LngNumAccts = LngNumAccts + 1
							anyinthisgroupprinted = true;
							acctland = exemptlist[ListIndex].Land;
							acctbuilding = exemptlist[ListIndex].Building;
							acctexemption = texempt;
							// acctassessment = acctbuilding - acctexemption + acctland
							acctassessment = acctbuilding - exemptlist[ListIndex].totalexemption + acctland;
							totbuilding += acctbuilding;
							totland += acctland;
							totexemption += acctexemption;
							totAssessment += acctassessment;
							subland += acctland;
							subbldg += acctbuilding;
							subexempt += acctexemption;
							subExemptTotal += exemptlist[ListIndex].totalexemption;
							// subtotal = subbldg - subexempt + subland
							subtotal = subbldg - subExemptTotal + subland;
							txtname.Text = exemptlist[ListIndex].Name;
							txtAccount.Text = exemptlist[ListIndex].AccountNumber.ToString();
							txtmaplot.Text = exemptlist[ListIndex].MapLot;
							txtland.Text = Strings.Format(acctland, "###,###,###,##0");
							txtbldg.Text = Strings.Format(acctbuilding, "###,###,###,##0");
							txtexemption.Text = Strings.Format(acctexemption, "###,###,###,##0");
							txttotal.Text = Strings.Format(acctassessment, "###,###,###,##0");
							if (acctland + acctbuilding > 0)
							{
								if (acctassessment == 0)
								{
									numTexempts += 1;
									if (exemptlist[ListIndex].beenprinted == 0)
									{
										LngNumTotExempts += 1;
									}
								}
							}
							exemptlist[ListIndex].beenprinted += 1;
						}
						boolupdatetime = false;
						if ((modPrintRoutines.Statics.intwhichorder == 2) || (modPrintRoutines.Statics.intwhichorder == 4) || (modPrintRoutines.Statics.intwhichorder == 6))
						{
							if (exemptlist[ListIndex].next == 0)
							{
								boolupdatetime = true;
								if (anyinthisgroupprinted)
								{
									anyinthisgroupprinted = false;
								}
								sland[currentgroup] = subland;
								sbldg[currentgroup] = subbldg;
								sexempt[currentgroup] = subexempt;
								saccts[currentgroup] = FCConvert.ToInt32(Math.Round(Conversion.Val(this.Fields["numaccts"])));
								stexempts[currentgroup] = numTexempts;
								subtotalexempts[currentgroup] = FCConvert.ToInt32(subtotal);
								numTexempts = 0;
								subland = 0;
								subbldg = 0;
								subexempt = 0;
								subExemptTotal = 0;
								subtotal = 0;
								this.Fields["numaccts"].Value = 0;
							}
							// exemplist.next = 0
						}
						// intwhichorder
						break;
					}
			}
			//end switch
		}

		private void setupexemptlist()
		{
			int numaccounts;
			int maxaccount;
			int forindex;
			
			int ListIndex;
			int caccount = 0;
			// vbPorter upgrade warning: examt As int	OnWriteFCConvert.ToDouble(
			double examt = 0;
			clsDRWrapper clsCR = new clsDRWrapper();
			int[] arHomestead = new int[5 + 1];
			int x;
			int y;
			int intTemp;
			int lngCount;
			try
			{
				// On Error GoTo ErrorHandler
				lowestcode = 99;
				// 
				// arHomestead(1) = CustomizedInfo.HomesteadCode1
				// arHomestead(2) = CustomizedInfo.HomesteadCode2
				// arHomestead(3) = CustomizedInfo.HomesteadCode3
				// arHomestead(4) = CustomizedInfo.HomesteadCode4
				// arHomestead(5) = CustomizedInfo.HomesteadCode5
				// 
				// For x = 1 To 5
				// If arHomestead(x) = 0 Then arHomestead(x) = -2     'so can't match on zero
				// Next x
				string strMasterJoin;
				strMasterJoin = modREMain.GetMasterJoin();
				// Call rsTemp.OpenRecordset("select * from master where rlexemption > 0 order by rsaccount,rscard", strREDatabase)
				rsTemp.OpenRecordset(strMasterJoin + " where rlexemption > 0 order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				clsCR.OpenRecordset("select * from costrecord where crecordnumber between 1901 and 1999 order by crecordnumber", modGlobalVariables.strREDatabase);
				rsTemp.MoveLast();
				exemptlist = new Modexemptionrpt.ExemptionAudit[rsTemp.RecordCount() + 1];
				rsTemp.MoveFirst();
				exemptlist[0].next = 1;
				ListIndex = 1;
				Frmassessmentprogress.InstancePtr.Show();
				Frmassessmentprogress.InstancePtr.Text = "Audit Report";
				Frmassessmentprogress.InstancePtr.cmdcancexempt.Visible = false;
				Frmassessmentprogress.InstancePtr.Label2.Text = "Please wait while exempt lists are created.";
				Frmassessmentprogress.InstancePtr.BringToFront();
				lngCount = 0;
				whileloop:
				;
				while (!rsTemp.EndOfFile())
				{
					lngCount += 1;
					while ((rsTemp.Get_Fields_Boolean("rsdeleted")) || (Conversion.Val(rsTemp.Get_Fields_Int32("rsaccount") + "") < 1))
					{
						rsTemp.MoveNext();
						if (rsTemp.EndOfFile())
							goto whileloop;
					}
					caccount = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("rsaccount"));
					exemptlist[ListIndex].totalexemption = 0;
					exemptlist[ListIndex].Land = 0;
					exemptlist[ListIndex].Building = 0;
					exemptlist[ListIndex].assessment = 0;
					exemptlist[ListIndex].Code[1] = FCConvert.ToInt16(rsTemp.Get_Fields_Int32("riexemptcd1"));
					exemptlist[ListIndex].Code[2] = FCConvert.ToInt16(rsTemp.Get_Fields_Int32("riexemptcd2"));
					exemptlist[ListIndex].Code[3] = FCConvert.ToInt16(rsTemp.Get_Fields_Int32("riexemptcd3"));
					// the calc routine knows to re-order exemptions so homesteads are last
					// we must do the same
					// For y = 1 To 2
					// For x = 1 To 5
					// If arHomestead(x) = exemptlist(listindex).Code(y) Then
					// If exemptlist(listindex).Code(y + 1) > 0 Then
					// intTemp = exemptlist(listindex).Code(y + 1)
					// exemptlist(listindex).Code(y + 1) = exemptlist(listindex).Code(y)
					// exemptlist(listindex).Code(y) = intTemp
					// End If
					// End If
					// Next x
					// Next y
					// 
					exemptlist[ListIndex].Exemption[1] = 0;
					exemptlist[ListIndex].Exemption[2] = 0;
					exemptlist[ListIndex].Exemption[3] = 0;
					exemptlist[ListIndex].Exemption[1] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields_Int32("EXEMPTVAL1") + "")));
					exemptlist[ListIndex].Exemption[2] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields_Int32("exemptval2") + "")));
					exemptlist[ListIndex].Exemption[3] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields_Int32("exemptval3") + "")));
					exemptlist[ListIndex].Name = (rsTemp.Get_Fields_String("rsname") + "");
					exemptlist[ListIndex].AccountNumber = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("rsaccount"));
					exemptlist[ListIndex].MapLot = rsTemp.Get_Fields_String("rsmaplot") + "";
					exemptlist[ListIndex].Location = Strings.Trim(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_String("rslocnumalph") + "")) + (rsTemp.Get_Fields_String("rslocstreet") + ""));
					exemptlist[ListIndex].beenprinted = 0;
					exemptlist[ListIndex].previous = ListIndex - 1;
					exemptlist[ListIndex].next = ListIndex + 1;
					while (caccount == FCConvert.ToInt32(rsTemp.Get_Fields_Int32("rsaccount")))
					{
						exemptlist[ListIndex].Land += FCConvert.ToInt32(rsTemp.Get_Fields(strLandFieldName) + "");
						exemptlist[ListIndex].Building += FCConvert.ToInt32(Conversion.Val(rsTemp.Get_Fields(strBldgFieldName) + ""));
						exemptlist[ListIndex].assessment = exemptlist[ListIndex].Building + exemptlist[ListIndex].Land;
						exemptlist[ListIndex].totalexemption += FCConvert.ToInt32(Conversion.Val(rsTemp.Get_Fields(strExemptionField) + ""));
						rsTemp.MoveNext();
						if (rsTemp.EndOfFile())
							break;
					}
					if (exemptlist[ListIndex].totalexemption == exemptlist[ListIndex].assessment)
					{
						exemptlist[ListIndex].booltotallyexempt = true;
					}
					else
					{
						exemptlist[ListIndex].booltotallyexempt = false;
					}
					// now look up the exemption amounts
					if (exemptlist[ListIndex].Code[1] != 0)
					{
						// Call OpenCRTable(CR, 1900 + exemptlist(listindex).Code(1))
						// If clsCR.FindFirstRecord("crecordnumber", 1900 + exemptlist(ListIndex).Code(1)) Then
						if (clsCR.FindFirst("crecordnumber = " + FCConvert.ToString(1900 + exemptlist[ListIndex].Code[1])))
						{
							examt = FCConvert.ToInt32(clsCR.Get_Fields_String("CUnit"));
							if (examt == 99999999)
								goto exitif;
						}
						else
						{
							goto exitif;
						}
						examt = FCConvert.ToInt32(examt * modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio);
						if (examt == 0)
						{
							// exemptlist(listindex).booltotallyexempt = True
							// exemptlist(listindex).Exemption(1) = exemptlist(listindex).assessment
							if (exemptlist[ListIndex].Code[2] > 0)
							{
								// exemptlist(listindex).Exemption(1) = (exemptlist(listindex).Exemption(1) * exemptlist(listindex).Code(2)) / 100
								// exemptlist(listindex).Exemption(1) = (((exemptlist(listindex).Exemption(1)) / RoundingVar) \ 1) * RoundingVar
								exemptlist[ListIndex].Code[2] = 0;
							}
							goto ThirdCode;
						}
						// examt = (((examt / 100) / RoundingVar) \ 1) * RoundingVar
						examt /= 100;
						if (examt >= exemptlist[ListIndex].totalexemption)
						{
							// exemptlist(listindex).Exemption(1) = exemptlist(listindex).totalexemption
							goto exitif;
						}
						// exemptlist(listindex).Exemption(1) = examt
					}
					secondcode:
					;
					if (exemptlist[ListIndex].Code[2] != 0)
					{
						// Call OpenCRTable(CR, 1900 + exemptlist(listindex).Code(2))
						// If clsCR.FindFirstRecord("crecordnumber", 1900 + exemptlist(ListIndex).Code(2)) Then
						if (clsCR.FindFirst("crecordnumber = " + FCConvert.ToString(1900 + exemptlist[ListIndex].Code[2])))
						{
							examt = FCConvert.ToInt32(clsCR.Get_Fields_String("CUnit"));
							if (examt == 99999999)
								goto exitif;
						}
						else
						{
							goto exitif;
						}
						examt = FCConvert.ToInt32(examt * modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio);
						if (examt == 0)
						{
							// exemptlist(listindex).booltotallyexempt = True
							// exemptlist(listindex).Exemption(2) = exemptlist(listindex).assessment
							if (exemptlist[ListIndex].Code[3] > 0)
							{
								// exemptlist(listindex).Exemption(2) = exemptlist(listindex).Exemption(2) * exemptlist(listindex).Code(3) / 100
								// exemptlist(listindex).Exemption(2) = ((exemptlist(listindex).Exemption(2) / RoundingVar) \ 1) * RoundingVar
								exemptlist[ListIndex].Code[3] = 0;
								if (exemptlist[ListIndex].Exemption[2] + exemptlist[ListIndex].Exemption[1] >= exemptlist[ListIndex].totalexemption)
								{
									// exemptlist(listindex).Exemption(2) = exemptlist(listindex).totalexemption - exemptlist(listindex).Exemption(1)
									goto exitif;
								}
							}
							else
							{
								// exemptlist(listindex).Exemption(2) = exemptlist(listindex).totalexemption - exemptlist(listindex).Exemption(1)
							}
							goto exitif;
						}
						// examt = (((examt / 100) / RoundingVar) \ 1) * RoundingVar
						examt /= 100;
						if (examt + exemptlist[ListIndex].Exemption[1] >= exemptlist[ListIndex].totalexemption)
						{
							// exemptlist(listindex).Exemption(2) = exemptlist(listindex).totalexemption - exemptlist(listindex).Exemption(1)
							goto exitif;
						}
						// exemptlist(listindex).Exemption(2) = examt
					}
					ThirdCode:
					;
					if (exemptlist[ListIndex].Code[3] != 0)
					{
						// Call OpenCRTable(CR, 1900 + exemptlist(listindex).Code(3))
						// If clsCR.FindFirstRecord("crecordnumber", 1900 + exemptlist(ListIndex).Code(3)) Then
						if (clsCR.FindFirst("crecordnumber = " + FCConvert.ToString(1900 + exemptlist[ListIndex].Code[3])))
						{
							examt = FCConvert.ToInt32(clsCR.Get_Fields_String("CUnit"));
							if (examt == 99999999)
								goto exitif;
						}
						else
						{
							goto exitif;
						}
						examt = FCConvert.ToInt32(examt * modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio);
						// If examt = 0 Then exemptlist(listindex).booltotallyexempt = True
						// exemptlist(listindex).Exemption(3) = exemptlist(listindex).totalexemption - (exemptlist(listindex).Exemption(1) + exemptlist(listindex).Exemption(2))
					}
					exitif:
					;
					// Dim x
					for (x = 1; x <= 3; x++)
					{
						if ((exemptlist[ListIndex].Code[1] == 0) && (exemptlist[ListIndex].Code[2] == 0) && (exemptlist[ListIndex].Code[3] == 0))
							lowestcode = 0;
						if (exemptlist[ListIndex].Code[x] < lowestcode)
						{
							if (exemptlist[ListIndex].Code[x] > 0)
							{
								lowestcode = exemptlist[ListIndex].Code[x];
							}
						}
					}
					// x
					// if the user elected to only view partial or only total exempts then
					// you must dump this off the list if it is unwanted
					switch (modGlobalVariables.Statics.whatinclude)
					{
						case 1:
							{
								// show partial exemptions only
								// booltotallyexempt will be false or
								// if its true then it was a fraction totally exempt
								if (exemptlist[ListIndex].assessment == exemptlist[ListIndex].totalexemption)
								{
									// this is totally exempt so get rid of it
									ListIndex -= 1;
								}
								break;
							}
						case 2:
							{
								// show total exemptions only
								if (exemptlist[ListIndex].assessment != exemptlist[ListIndex].totalexemption)
								{
									// this is partially exempt so get rid of it
									ListIndex -= 1;
								}
								break;
							}
						case 3:
							{
								// showing both exemptions so dont do anything else
								break;
							}
						case 4:
							{
								// gintminaccountrange and gintmaxaccountrange are set to the specific code to use
								if ((exemptlist[ListIndex].Code[1] != modGlobalVariables.Statics.gintMinAccountRange) && (exemptlist[ListIndex].Code[2] != modGlobalVariables.Statics.gintMinAccountRange) && (exemptlist[ListIndex].Code[3] != modGlobalVariables.Statics.gintMinAccountRange))
								{
									// not the code were looking for so take off list
									ListIndex -= 1;
								}
								break;
							}
						case 5:
							{
								// gintminaccountrange and gintmaxaccountrange are the min and max codes to look for
								boolinrange = false;
								if ((exemptlist[ListIndex].Code[1] >= modGlobalVariables.Statics.gintMinAccountRange) && (exemptlist[ListIndex].Code[1] <= modGlobalVariables.Statics.gintMaxAccountRange))
								{
									boolinrange = true;
								}
								else if ((exemptlist[ListIndex].Code[2] >= modGlobalVariables.Statics.gintMinAccountRange) && (exemptlist[ListIndex].Code[2] <= modGlobalVariables.Statics.gintMaxAccountRange))
								{
									boolinrange = true;
								}
								else if ((exemptlist[ListIndex].Code[3] >= modGlobalVariables.Statics.gintMinAccountRange) && (exemptlist[ListIndex].Code[3] <= modGlobalVariables.Statics.gintMaxAccountRange))
								{
									boolinrange = true;
								}
								if (!boolinrange)
								{
									ListIndex -= 1;
								}
								break;
							}
					}
					//end switch
					ListIndex += 1;
					//Application.DoEvents();
					if (!rsTemp.EndOfFile())
					{
						Frmassessmentprogress.InstancePtr.ProgressBar1.Value = FCConvert.ToInt32(FCConvert.ToDouble(lngCount) / rsTemp.RecordCount());
						Frmassessmentprogress.InstancePtr.lblpercentdone.Text = Strings.Format(FCConvert.ToDouble(lngCount) / rsTemp.RecordCount(), "##0") + "%";
					}
				}
				Frmassessmentprogress.InstancePtr.Unload();
				lengthofexemptlist = ListIndex - 1;
				Array.Resize(ref exemptlist, lengthofexemptlist + 1);
				exemptlist[0].previous = ListIndex - 1;
				exemptlist[ListIndex - 1].next = 0;
				sortexemptlist();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				//FC:FINAL:MSH - i.issue #1199: show message box in not modal mode
				//MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " in setupexemptlist", null, MessageBoxButtons.OK, MessageBoxIcon.Hand, modal:false);
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " in setupexemptlist", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void updateexemptbinder()
		{
			rptaudit.InstancePtr.Fields["thebinder"].Value = currentgroup;
		}

		private void checkexemptbinder()
		{
			if (currentgroup != FCConvert.ToInt32(rptaudit.InstancePtr.Fields["thebinder"].Value))
				boolbinderchanged = true;
		}

		private void sortexemptlist()
		{
			int x;
			string holdvalue = "";
			int listpointer = 0;
			int tprev = 0;
			int tnext = 0;
			try
			{
				// On Error GoTo ErrorHandler
				if (modPrintRoutines.Statics.intwhichorder > 4)
				{
					// already in the correct order
					return;
				}
				// outer loop cycles through every element of the array
				// it uses an innerloop to place the element in the correct spot in the linked list
				Frmassessmentprogress.InstancePtr.Show();
				Frmassessmentprogress.InstancePtr.Text = "Audit Report";
				if (lengthofexemptlist > 0)
				{
					Frmassessmentprogress.InstancePtr.ProgressBar1.Maximum = lengthofexemptlist;
				}
				Frmassessmentprogress.InstancePtr.cmdcancexempt.Visible = false;
				Frmassessmentprogress.InstancePtr.Label2.Text = "Please wait while exempt lists are sorted.";
				Frmassessmentprogress.InstancePtr.BringToFront();
				for (x = 1; x <= lengthofexemptlist; x++)
				{
					switch (modPrintRoutines.Statics.intwhichorder)
					{
						case 1:
						case 2:
							{
								holdvalue = Strings.UCase(exemptlist[x].Name + "");
								break;
							}
						case 3:
						case 4:
							{
								holdvalue = Strings.UCase(exemptlist[x].MapLot + "");
								break;
							}
					}
					//end switch
					listpointer = exemptlist[0].next;
					// exemptlist(0) is the list header and footer.
					while (listpointer != 0)
					{
						switch (modPrintRoutines.Statics.intwhichorder)
						{
							case 1:
							case 2:
								{
									if (Strings.CompareString(holdvalue, "<", Strings.UCase(exemptlist[listpointer].Name + "")))
									{
										tprev = exemptlist[x].previous;
										tnext = exemptlist[x].next;
										exemptlist[tprev].next = tnext;
										exemptlist[tnext].previous = tprev;
										exemptlist[x].previous = exemptlist[listpointer].previous;
										exemptlist[x].next = listpointer;
										exemptlist[listpointer].previous = x;
										exemptlist[exemptlist[x].previous].next = x;
										break;
									}
									break;
								}
							case 3:
							case 4:
								{
									if (Strings.CompareString(holdvalue, "<", Strings.UCase(exemptlist[listpointer].MapLot + "")))
									{
										tprev = exemptlist[x].previous;
										tnext = exemptlist[x].next;
										exemptlist[tprev].next = tnext;
										exemptlist[tnext].previous = tprev;
										exemptlist[x].previous = exemptlist[listpointer].previous;
										exemptlist[x].next = listpointer;
										exemptlist[listpointer].previous = x;
										exemptlist[exemptlist[x].previous].next = x;
										break;
									}
									break;
								}
						}
						//end switch
						listpointer = exemptlist[listpointer].next;
					}
					//Application.DoEvents();
					Frmassessmentprogress.InstancePtr.ProgressBar1.Value = x;
					Frmassessmentprogress.InstancePtr.lblpercentdone.Text = Strings.Format((FCConvert.ToDouble(x) / lengthofexemptlist) * 100, "##0") + "%";
				}
				// x
				Frmassessmentprogress.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				//FC:FINAL:MSH - i.issue #1199: show message box in not modal mode
				//MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + " in sortexemptlist", null, MessageBoxButtons.OK, MessageBoxIcon.Hand, modal:false);
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + " in sortexemptlist", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (alldone && Strings.UCase(modPrintRoutines.Statics.allorexempt) == "EXEMPT")
			{
				// were on the last page so make these visible
				txtfinaltotal.Visible = true;
				txttotland.Text = Strings.Format(totland, "###,###,###,##0");
				txttotland.Visible = true;
				txttotbuilding.Text = Strings.Format(totbuilding, "###,###,###,##0");
				txttotbuilding.Visible = true;
				txttotexemption.Text = Strings.Format(totexemption, "###,###,###,##0");
				txttotexemption.Visible = true;
				txttotassessment.Text = Strings.Format(totAssessment, "###,###,###,##0");
				txttotassessment.Visible = true;
				txtTotalnumaccts.Visible = true;
				txtTotalnumaccts.Text = LngNumAccts.ToString();
				txttotalaccts.Visible = true;
				txttotnumtotexempts.Visible = true;
				txttotnumtotexempts.Text = LngNumTotExempts.ToString();
				txttotaltotallyexempt.Visible = true;
			}
		}

		
	}
}
