﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetAccount.
	/// </summary>
	public partial class frmGetAccount : BaseForm
	{
		public frmGetAccount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetAccount InstancePtr
		{
			get
			{
				return (frmGetAccount)Sys.GetInstance(typeof(frmGetAccount));
			}
		}

		protected frmGetAccount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool FormActivated;
		int intProblem;
		bool boolDeleting;
		bool boolLoadingOptions;
		cSettingsController setCont = new cSettingsController();
		string cmbtsearchtype_item8 = "Open 1";
		string cmbtsearchtype_item9 = "Open 2";

		public void HandlePartialPermission()
		{
			// dummy sub
		}

		public void cmdClear_Click(object sender, System.EventArgs e)
		{
			frmRESearch.InstancePtr.Unload();
			txtSearch.Text = "";
			txtSearch2.Text = "";
			cmbtsearchtype.Text = "";
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorTag
				// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
				DialogResult intResponse;
				int intNextAccount;
				// Dim dbTemp As DAO.Database
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsTemp = new clsDRWrapper();
				int lngTemp = 0;
				boolDeleting = false;
				if (cmbtrecordtype.Text == "Previous Owners")
				{
					modGlobalVariables.Statics.boolPreviousOwnerRecords = true;
					if (Conversion.Val(txtGetAccountNumber.Text) > 0)
					{
						clsLoad.OpenRecordset("select * from previousowner where account = " + FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)) + " order by saledate", modGlobalVariables.strREDatabase);
						if (clsLoad.EndOfFile())
						{
							//Application.DoEvents();
							if (MessageBox.Show("There are no previous owners for this account recorded in the database." + "\r\n" + "Do you wish to create a record?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								//Application.DoEvents();
								frmPreviousOwner.InstancePtr.Init(-2, FCConvert.ToInt32(Conversion.Val(txtGetAccountNumber.Text)));
								// invalid id so it will create a new record
							}
						}
						else
						{
							if (clsLoad.RecordCount() > 1)
							{
								modGlobalVariables.Statics.intSearchPosition = 0;
								modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(txtGetAccountNumber.Text)));
								frmRESearch.InstancePtr.Show(App.MainForm);
							}
							else
							{
								frmPreviousOwner.InstancePtr.Init(clsLoad.Get_Fields_Int32("id"));
							}
						}
					}
					else
					{
						MessageBox.Show("You must enter a valid account number to view previous owners.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					return;
				}
				modGlobalVariables.Statics.boolPreviousOwnerRecords = false;
				if (cmbtrecordtype.Text == "Open for Pending Changes")
				{
					modGlobalVariables.Statics.boolInPendingMode = true;
				}
				else
				{
					modGlobalVariables.Statics.boolInPendingMode = false;
				}
				if (modGlobalVariables.Statics.boolInPendingMode && !Information.IsDate(T2KPendingDate.Text))
				{
					MessageBox.Show("You have chosen to edit in pending mode but haven't supplied a valid minimum date.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTSALESRECORDS, false))
				{
					cmbtrecordtype.Clear();
					cmbtrecordtype.Items.Add("Regular Records");
					cmbtrecordtype.Items.Add("Previous Owners");
					cmbtrecordtype.Items.Add("Open for Pending Changes");
				}
				if (modGlobalRoutines.Formisloaded_2("frmSHREBLUpdate") == true)
					frmSHREBLUpdate.InstancePtr.Unload();
				// Set dbTemp = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				modGlobalVariables.Statics.AddNewAccountFlag = false;
				if (txtGetAccountNumber.Text == "0")
					modGlobalVariables.Statics.CurrentAccount = 0;
				if (modGlobalVariables.Statics.CurrentAccount == 0)
					modGlobalVariables.Statics.CurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(txtGetAccountNumber.Text)));
				if (Conversion.Val(txtGetAccountNumber.Text) != 0)
					modGlobalVariables.Statics.CurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(txtGetAccountNumber.Text)));
				intNextAccount = FCConvert.ToInt32(modGlobalRoutines.GetMaxAccounts());

				modGlobalVariables.Statics.gintLastAccountNumber = modGlobalVariables.Statics.CurrentAccount;
				modGNBas.Statics.response = "X";
				modGNBas.Statics.gintCardNumber = 1;
				// corey
				if (modGlobalVariables.Statics.CurrentAccount > 0)
				{
					modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
					modGlobalVariables.Statics.gintLastAccountNumber = (modRegistry.GetRegistryKey("RELastAccountNumber") != string.Empty ? FCConvert.ToInt32(Math.Round(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn))) : 0);
				}
				clsTemp.OpenRecordset("select * from modules", modGlobalVariables.Statics.strGNDatabase);
				modGlobalVariables.Statics.MaxAccounts = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("RE_Maxaccounts"))));
				// clsTemp.DisconnectDatabase
				if (modGlobalVariables.Statics.CurrentAccount > 0)
				{
					// this on error checks to see if record is locked
					object ans;
					TryOver:
					;
					/*? On Error Resume Next  */
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						rsTemp.OpenRecordset(" select * from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.CurrentAccount), modGlobalVariables.strREDatabase);
						if (rsTemp.EndOfFile())
						{
							if (modGlobalVariables.Statics.boolInPendingMode)
							{
								MessageBox.Show("This account doesn't Exist.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
								return;
							}
							intResponse = MessageBox.Show("This account doesn't exist. Would you like to create it?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							if (intResponse == DialogResult.Yes)
							{
								if (!modGlobalVariables.Statics.boolRegRecords)
								{
									MessageBox.Show("You cannot create a new account while in Sales Record mode", "Invalid Action", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
								if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTADDACCOUNTS, false))
								{
									MessageBox.Show("You do not have permission or your permission is missing to create a new account.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
								//Application.DoEvents();
								modGlobalFunctions.AddCYAEntry_8("RE", "Created New Account by requesting " + FCConvert.ToString(modGlobalVariables.Statics.CurrentAccount));
								modGlobalVariables.Statics.ActiveCard = 1;
								// Call OpenForm(frmREProperty)
								// If Formisloaded("frmreproperty") Then
								frmREProperty.InstancePtr.Unload();
								frmNewREProperty.InstancePtr.Unload();
								// End If
								// If Formisloaded("frmshreblupdate") Then
								frmSHREBLUpdate.InstancePtr.Unload();
								// End If
								if (modGlobalVariables.Statics.wmainresponse == 1)
								{
                                    //Application.DoEvents();
                                    // Load frmREProperty
                                    //FC:FINAL:BSE:#3688 activate form after is fully loaded 
                                    //frmNewREProperty.InstancePtr.Show(App.MainForm);
                                    //// DoEvents
                                    //frmNewREProperty.InstancePtr.Acct_Click(modGlobalVariables.Statics.CurrentAccount);
                                    frmNewREProperty.InstancePtr.LoadForm();
                                    frmNewREProperty.InstancePtr.Acct_Click(modGlobalVariables.Statics.CurrentAccount);
                                    frmNewREProperty.InstancePtr.Show(App.MainForm);
                                    this.Unload();
									return;
									// DoEvents
								}
								else
								{
									//Application.DoEvents();
									txtGetAccountNumber.Focus();
									//Application.DoEvents();
									clsDRWrapper temp1 = modDataTypes.Statics.MR;
									modREMain.OpenMasterTable(ref temp1, modGlobalVariables.Statics.gintLastAccountNumber, 1);
									modDataTypes.Statics.MR = temp1;
									modDataTypes.Statics.MR.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
									modDataTypes.Statics.MR.Set_Fields("rscard", 1);
									modDataTypes.Statics.MR.Set_Fields("rsdeleted", false);
									modDataTypes.Statics.MR.Update();
									modDataTypes.Statics.MR = temp1;
									modREMain.OpenMasterTable(ref temp1, modGlobalVariables.Statics.gintLastAccountNumber, 1);
									modDataTypes.Statics.MR = temp1;
									frmSHREBLUpdate.InstancePtr.Init(modGlobalVariables.Statics.gintLastAccountNumber, 1, false);
									// DoEvents
									this.Unload();
									return;
								}
							}
							else
							{
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							}
							return;
						}
						clsDRWrapper temp = modDataTypes.Statics.MR3;
						modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.CurrentAccount);
						modDataTypes.Statics.MR3 = temp;
					}
					else
					{
						if (modGlobalVariables.Statics.gRstemp == null)
						{
							modGlobalVariables.Statics.gRstemp = new clsDRWrapper();
						}
						if (modDataTypes.Statics.MR3 == null)
						{
						}
						modGlobalVariables.Statics.gRstemp.OpenRecordset("select * from srmaster where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.CurrentAccount) + " and rscard = 1 and rsdeleted = 0", modGlobalVariables.strREDatabase);
						if (!modGlobalVariables.Statics.gRstemp.EndOfFile())
						{
							modGlobalVariables.Statics.gRstemp.MoveLast();
							modGlobalVariables.Statics.gRstemp.MoveFirst();
							if (modGlobalVariables.Statics.gRstemp.RecordCount() > 0)
							{
								modGlobalVariables.Statics.gRstemp.Edit();
								modGlobalVariables.Statics.CancelledIt = false;
								if (modGlobalVariables.Statics.gRstemp.RecordCount() > 1)
								{
									FrmChooseSR.InstancePtr.Show(FCForm.FormShowEnum.Modal);
								}
								if (!modGlobalVariables.Statics.CancelledIt)
								{
									modDataTypes.Statics.MR3.OpenRecordset("SELECT * FROM SRMaster where id = " + modGlobalVariables.Statics.gRstemp.Get_Fields_Int32("id"), modGlobalVariables.strREDatabase);
									modGlobalVariables.Statics.glngSaleID = FCConvert.ToInt32(modDataTypes.Statics.MR3.Get_Fields_Int32("saleid"));
                                    if (modGlobalVariables.Statics.glngSaleID == 0)
                                    {
                                        MessageBox.Show(
                                            "The sale record is missing a valid sale id and cannot be opened",
                                            "Bad Sale Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }
								}
								else
								{
									return;
								}
							}
							else
							{
								MessageBox.Show("Invalid Account Number. Please try another.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
						else
						{
							MessageBox.Show("No such sale record exists.  Please try another.", "Invalid Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							return;
						}
					}
					if (FCConvert.ToBoolean(modDataTypes.Statics.MR3.Get_Fields_Boolean("rsdeleted")))
					{
						boolDeleting = true;
						if (!FCConvert.ToBoolean(modDataTypes.Statics.MR3.Get_Fields_Boolean("AccountLocked")))
						{
							intResponse = MessageBox.Show(" The Account you have selected is a Deleted Account." + "\r\n" + "Would you like to un-delete it?", "Deleted Account", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
							if (intResponse == DialogResult.Yes)
							{
								if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTUNDELETEACCOUNTS, false))
								{
									MessageBox.Show("You do not have permission to undelete account, or your permissions are missing", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
									FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
									boolDeleting = false;
									return;
								}
								if (modGlobalVariables.Statics.boolRegRecords)
								{
									modGlobalRoutines.UndeleteMasterRecord(modGlobalVariables.Statics.CurrentAccount);
								}
								else
								{
									modGlobalRoutines.UndeleteMasterRecord(modGlobalVariables.Statics.CurrentAccount, modDataTypes.Statics.MR3.Get_Fields_Int32("id"));
								}
							}
						}
						else
						{
							MessageBox.Show("The account you have selected is a deleted account and cannot be undeleted because it is locked" + "\r\n" + "\r\n" + "Reason locked: " + modDataTypes.Statics.MR3.Get_Fields_String("ReasonAccountLocked"), "Deleted and Locked", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						boolDeleting = false;
						txtGetAccountNumber.Focus();
						txtGetAccountNumber.SelectionStart = 0;
						txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
					}
					if (!FCConvert.ToBoolean(modDataTypes.Statics.MR3.Get_Fields_Boolean("rsdeleted")))
					{
						// corey
						// this code didn't specify a cardnumber
						// assume this is a bad thing
						modGlobalVariables.Statics.gcurrsaledate = modDataTypes.Statics.MR3.Get_Fields_DateTime("SaleDate") + "";
						clsDRWrapper temp = modDataTypes.Statics.MR;
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.CurrentAccount, 1);
						}
						else
						{
							modGlobalVariables.Statics.glngSaleID = FCConvert.ToInt32(modDataTypes.Statics.MR3.Get_Fields_Int32("saleid"));
							modREMain.OpenSRMasterTable_39(ref temp, modGlobalVariables.Statics.glngSaleID);
							// Call MR.OpenRecordset("SELECT * FROM SRMaster where id = " & MR3.Fields("id"), strREDatabase)
							modDataTypes.Statics.MR.Edit();
						}
						modDataTypes.Statics.MR = temp;
						modGlobalVariables.Statics.gcurrsaledate = modDataTypes.Statics.MR3.Get_Fields_DateTime("SaleDate") + "";
						temp = modDataTypes.Statics.CMR;
						modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.CurrentAccount, 1);
						modDataTypes.Statics.CMR = temp;
						temp = modDataTypes.Statics.DWL;
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							modREMain.OpenDwellingTable_27(ref temp, modGlobalVariables.Statics.CurrentAccount, 1, modDataTypes.Statics.MR.Get_Fields_String("AccountID"));
						}
						else
						{
							modREMain.OpenDwellingTable(ref temp, modGlobalVariables.Statics.CurrentAccount, 1);
						}
						modDataTypes.Statics.DWL = temp;
						temp = modDataTypes.Statics.OUT;
						modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.CurrentAccount, 1);
						modDataTypes.Statics.OUT = temp;
						// Call OpenIncomeTable(INCVB, CurrentAccount)
						// Call OpenWWKTable(WWK)
						modGlobalVariables.Statics.gintLastAccountNumber = modGlobalVariables.Statics.CurrentAccount;
						modGNBas.Statics.gintCardNumber = 1;
						if (modGlobalVariables.Statics.wmainresponse == 1)
						{
							if (modGlobalVariables.Statics.boolRegRecords)
							{
								// If Formisloaded("frmREProperty") Then
								frmNewREProperty.InstancePtr.Unload();
								// End If
								//Application.DoEvents();
								//txtGetAccountNumber.Focus();
								frmNewREProperty.InstancePtr.Show(App.MainForm);
								// frmREProperty.Show , MDIParent
								frmNewREProperty.InstancePtr.dtMinPendingDate = FCConvert.ToDateTime(T2KPendingDate.Text);
							}
							else
							{
								frmREProperty.InstancePtr.Unload();
								//Application.DoEvents();
								txtGetAccountNumber.Focus();
								frmREProperty.InstancePtr.Show(App.MainForm);
								frmREProperty.InstancePtr.dtMinPendingDate = FCConvert.ToDateTime(T2KPendingDate.Text);
							}
							this.Unload();
						}
						else
						{
							// If Formisloaded("frmSHREBLUpdate") Then
							frmSHREBLUpdate.InstancePtr.Unload();
							// End If
							// DoEvents
							txtGetAccountNumber.Focus();
							//Application.DoEvents();
							frmSHREBLUpdate.InstancePtr.Show(App.MainForm);
							frmSHREBLUpdate.InstancePtr.dtMinPendingDate = FCConvert.ToDateTime(T2KPendingDate.Text);
						}
						modGNBas.Statics.gboolLoadingDone = true;
						if (modGlobalVariables.Statics.InvalidAccountNumber == false)
						{
							FCGlobal.Screen.MousePointer = 0;
						}
						else
						{
							txtGetAccountNumber.Text = "";
							txtGetAccountNumber.Focus();
							modGlobalVariables.Statics.InvalidAccountNumber = false;
						}
					}
					// if they hit enter for last accessed account
				}
				else if (txtGetAccountNumber.Text == "")
				{
					return;
				}
				else if (txtGetAccountNumber.Text == "0")
				{
					CreateAccount:
					;
					if (!modGlobalVariables.Statics.boolRegRecords)
					{
						MessageBox.Show("You cannot create a new account while in Sales Record mode", "Invalid Action", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return;
					}
					if (modGlobalVariables.Statics.boolInPendingMode)
					{
						MessageBox.Show("You cannot create a new account while in pending changes mode.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return;
					}
					modGlobalVariables.Statics.ActiveCard = 1;
					if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTADDACCOUNTS, false))
					{
						MessageBox.Show("You do not have permission or your permission is missing to create a new account", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					// 
					modGlobalVariables.Statics.DataChanged = false;
					if (modGlobalVariables.Statics.wmainresponse == 1)
					{
						// If Formisloaded("frmreproperty") Then
						frmREProperty.InstancePtr.Unload();
						frmNewREProperty.InstancePtr.Unload();
						// End If
						//Application.DoEvents();
						//! Load frmNewREProperty;
						txtGetAccountNumber.Focus();
                        //FC:FINAL:BSE:#3688 activate form after is fully loaded
                        //frmNewREProperty.InstancePtr.Show(App.MainForm);
                        //// frmREProperty.mnuAddNewAccount_Click
                        //int AcctNum = 0;
                        //lngTemp = frmNewREProperty.InstancePtr.Acct_Click(AcctNum, true);
                        frmNewREProperty.InstancePtr.LoadForm();
                        int AcctNum = 0;
                        lngTemp = frmNewREProperty.InstancePtr.Acct_Click(AcctNum, true);
                        frmNewREProperty.InstancePtr.Show(App.MainForm);
                        modGlobalFunctions.AddCYAEntry_80("RE", "Created New Account " + FCConvert.ToString(lngTemp), "In Long Screen", "Entered 0 from Get Account");
						this.Unload();
					}
					else
					{
						// If Formisloaded("frmSHREBLUpdate") Then
						frmSHREBLUpdate.InstancePtr.Unload();
						// End If
						modGlobalFunctions.AddCYAEntry_80("RE", "Created New Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "In Short Screen", "Entered 0 from Get Account");
						//Application.DoEvents();
						txtGetAccountNumber.Focus();
						//! Load frmSHREBLUpdate;
						frmSHREBLUpdate.InstancePtr.Show(App.MainForm);
					}
					FCGlobal.Screen.MousePointer = 0;
				}
				FCGlobal.Screen.MousePointer = 0;
				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In form Get Account", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				FCGlobal.Screen.MousePointer = 0;
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			// Call UnlockALLRecords
			// Call OpenForm(menuREAS)
			this.Unload();
		}

		public void cmdQuit_Click()
		{
			//cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void SaveLastSearchOptions()
		{
			// saves the options in the registry
			int intSearchBy = 0;
			int intDisplay = 0;
			bool boolIncludeDel = false;
			bool boolIncludePrev = false;
			// vbPorter upgrade warning: strShowAll As bool	OnWrite(string)
			bool strShowAll = false;
			if (cmbtsearchtype.Text == "Name")
			{
				intSearchBy = 0;
			}
			else if (cmbtsearchtype.Text == "Location")
			{
				intSearchBy = 1;
			}
			else if (cmbtsearchtype.Text == "Map / Lot")
			{
				intSearchBy = 2;
			}
			else if (cmbtsearchtype.Text == "Sale Date Range")
			{
				intSearchBy = 3;
			}
			else if (cmbtsearchtype.Text == "Sale Date")
			{
				intSearchBy = 4;
			}
			modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastSearchBy", FCConvert.ToString(FCConvert.ToInt16(intSearchBy)));
			if (cmbtcontain.Text == "Contain Search Criteria")
			{
				intDisplay = 0;
			}
			else if (cmbtcontain.Text == "Start With Search Criteria")
			{
				intDisplay = 1;
			}
			else if (cmbtcontain.Text == "End With Search Criteria")
			{
				intDisplay = 2;
			}
			modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastDisplayRecordsThat", FCConvert.ToString(FCConvert.ToInt16(intDisplay)));
			if (chkShowDel.CheckState == Wisej.Web.CheckState.Checked)
			{
				boolIncludeDel = true;
			}
			else
			{
				boolIncludeDel = false;
			}
			modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastIncludeDeleted", FCConvert.ToString(FCConvert.CBool(boolIncludeDel)));
			if (chkPrevOwner.CheckState == Wisej.Web.CheckState.Checked)
			{
				boolIncludePrev = true;
			}
			else
			{
				boolIncludePrev = false;
			}
			modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastIncludePreviousOwner", FCConvert.ToString(FCConvert.CBool(boolIncludePrev)));
			if (chkShowAll.CheckState == Wisej.Web.CheckState.Checked)
			{
				strShowAll = FCConvert.CBool("True");
			}
			else
			{
				strShowAll = FCConvert.CBool("False");
			}
			setCont.SaveSetting(FCConvert.ToString(strShowAll), "AccountSearchShowAll", "RealEstate", "User", FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserID()), "");
		}

		private void LoadLastSearchOptions()
		{
			string strReturn = "";
			int intTemp;
			boolLoadingOptions = true;
			modRegistry.GetKeyValues(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastSearchBy", ref strReturn);
			intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strReturn)));
			if (cmbtsearchtype.Index >= intTemp)
			{
				cmbtsearchtype.SelectedIndex = intTemp;
			}
			else
			{
				cmbtsearchtype.Text = "Name";
			}
			modRegistry.GetKeyValues(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastDisplayRecordsThat", ref strReturn);
			if (strReturn != string.Empty)
			{
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strReturn)));
			}
			else
			{
				intTemp = 1;
			}
            if (cmbtcontain.Enabled)
            {
                cmbtcontain.SelectedIndex = intTemp;
            }
            else
            {
                cmbtcontain.SelectedIndex = 1;
            }

            modRegistry.GetKeyValues(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastIncludeDeleted", ref strReturn);
			if (strReturn != string.Empty)
			{
				if (FCConvert.CBool(strReturn))
				{
					if (chkShowDel.Enabled)
					{
						chkShowDel.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkShowDel.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					chkShowDel.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkShowDel.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			modRegistry.GetKeyValues(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastIncludePreviousOwner", ref strReturn);
			if (strReturn != string.Empty)
			{
				if (FCConvert.CBool(strReturn))
				{
					if (chkPrevOwner.Enabled)
					{
						chkPrevOwner.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkPrevOwner.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					chkPrevOwner.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkPrevOwner.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			strReturn = setCont.GetSettingValue("AccountSearchShowAll", "RealEstate", "User", FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserID()), "");
			if (strReturn != string.Empty)
			{
				if (FCConvert.CBool(strReturn))
				{
					chkShowAll.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkShowAll.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkShowAll.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			boolLoadingOptions = false;
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo errtag
				string strTText;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				SaveLastSearchOptions();
				if (cmbtsearchtype.Text == cmbtsearchtype_item8 || cmbtsearchtype.Text == cmbtsearchtype_item9)
				{
					if (!Information.IsNumeric(txtSearch.Text))
					{
						FCGlobal.Screen.MousePointer = 0;
						MessageBox.Show("This search option can accept numeric inputs only", "Invalid Search", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				if (cmbtrecordtype.Text == "Previous Owners")
				{
					frmRESearch.InstancePtr.Unload();
					modGlobalVariables.Statics.boolPreviousOwnerRecords = true;
					modGNBas.Statics.Resp2 = txtSearch.Text;
					if (cmbtcontain.Text == "Contain Search Criteria")
						modGlobalVariables.Statics.intSearchPosition = 1;
					if (cmbtcontain.Text == "Start With Search Criteria")
						modGlobalVariables.Statics.intSearchPosition = 2;
					if (cmbtcontain.Text == "End With Search Criteria")
						modGlobalVariables.Statics.intSearchPosition = 3;
					if (cmbtsearchtype.Text == "Name")
						modGNBas.Statics.Resp1 = "N";
					if (cmbtsearchtype.Text == "Location")
					{
						modGNBas.Statics.Resp1 = "L";
						strTText = txtSearch.Text + "," + txtSearch2.Text;
					}
					if (cmbtsearchtype.Text == "Map / Lot")
						modGNBas.Statics.Resp1 = "M";
					if (cmbtsearchtype.Text == "Sale Date Range")
						modGNBas.Statics.Resp1 = "R";
					if (cmbtsearchtype.Text == "Sale Date")
						modGNBas.Statics.Resp1 = "S";
					if (cmbtsearchtype.Text == "Book / Page")
					{
						modGNBas.Statics.Resp1 = "B";
						strTText = txtSearch.Text + "," + txtSearch2.Text;
					}
					frmRESearch.InstancePtr.Show(App.MainForm);
					return;
				}
				if (cmbtrecordtype.Text == "Open for Pending Changes" == true)
				{
					modGlobalVariables.Statics.boolInPendingMode = true;
				}
				else
				{
					modGlobalVariables.Statics.boolInPendingMode = false;
				}
				if (modGlobalRoutines.Formisloaded_2("frmRESearch"))
				{
					frmRESearch.InstancePtr.Unload();
				}
				strTText = txtSearch.Text;
				// Call escapequote(strTText)
				if (chkPrevOwner.CheckState == Wisej.Web.CheckState.Checked)
				{
					modGlobalVariables.Statics.boolIncPrevOwner = true;
				}
				else
				{
					modGlobalVariables.Statics.boolIncPrevOwner = false;
				}
				if (chkShowDel.CheckState == Wisej.Web.CheckState.Checked)
				{
					modGlobalVariables.Statics.boolShowDel = true;
				}
				else
				{
					modGlobalVariables.Statics.boolShowDel = false;
				}
				if (cmbtsearchtype.Text == "Location")
				{
					// location
					if (Conversion.Val(txtSearch.Text) == 0 && Strings.Trim(txtSearch2.Text) == string.Empty)
					{
						MessageBox.Show("You must type a street name and/or number to search for", "No Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				else if (strTText == "" && cmbtsearchtype.Text != "Sale Date Range")
				{
					MessageBox.Show("You must type something to search for in the white box.", "No Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtSearch.Focus();
					FCGlobal.Screen.MousePointer = 0;
					return;
				}
				if (cmbtsearchtype.Text == "Name")
					modGNBas.Statics.Resp1 = "N";
				if (cmbtsearchtype.Text == "Location")
				{
					modGNBas.Statics.Resp1 = "L";
					strTText = txtSearch.Text + "," + txtSearch2.Text;
				}
				if (cmbtsearchtype.Text == "Map / Lot")
					modGNBas.Statics.Resp1 = "M";
				if (cmbtsearchtype.Text == "Sale Date Range")
					modGNBas.Statics.Resp1 = "R";
				if (cmbtsearchtype.Text == "Sale Date")
					modGNBas.Statics.Resp1 = "S";
				if (cmbtsearchtype.Text == "Book / Page")
				{
					modGNBas.Statics.Resp1 = "B";
					strTText = txtSearch.Text + "," + txtSearch2.Text;
				}
				if (cmbtsearchtype.Text == "Ref 1")
				{
					modGNBas.Statics.Resp1 = "Ref1";
				}
				if (cmbtsearchtype.Text == "Ref 2")
				{
					modGNBas.Statics.Resp1 = "Ref2";
				}
				if (cmbtsearchtype.Text == cmbtsearchtype_item8)
				{
					modGNBas.Statics.Resp1 = "Open1";
				}
				if (cmbtsearchtype.Text == cmbtsearchtype_item9)
				{
					modGNBas.Statics.Resp1 = "Open2";
				}
				if (cmbtcontain.Text == "Contain Search Criteria")
					modGlobalVariables.Statics.intSearchPosition = 1;
				if (cmbtcontain.Text == "Start With Search Criteria")
					modGlobalVariables.Statics.intSearchPosition = 2;
				if (cmbtcontain.Text == "End With Search Criteria")
					modGlobalVariables.Statics.intSearchPosition = 3;
				if (strTText == " ")
				{
					// want to search for blank fields
					modGNBas.Statics.Resp2 = " ";
				}
				else
				{
					modGlobalRoutines.escapequote(ref strTText);
					modGNBas.Statics.Resp2 = Strings.UCase(Strings.Trim(strTText));
				}
				txtSearch.Text = string.Empty;
				modGNBas.Statics.LongScreenSearch = true;
				frmRESearch.InstancePtr.Show();
				// corey
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				modGNBas.Statics.LongScreenSearch = false;
				FCGlobal.Screen.MousePointer = 0;
				// corey
				this.Unload();
				return;
			}
			catch
			{
				// errtag:
				FCGlobal.Screen.MousePointer = 0;
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmGetAccount_Activated(object sender, System.EventArgs e)
		{
			try
			{
				//FC:FINAL:MSH - I.Issue #963: on btn clicking will be again executed Activate() method and values will be overwritten, 
				// so the existence of the form must be checked earlier
				if (modMDIParent.FormExist(this))
					return;
				// On Error GoTo errtag
				intProblem = 1;
				if (boolDeleting)
					return;
				modGlobalVariables.Statics.boolRegRecords = cmbtrecordtype.Text == "Regular Records" || cmbtrecordtype.Text == "Open for Pending Changes";
				if (!modGlobalVariables.Statics.boolRegRecords == true)
				{
					if (!cmbtsearchtype.Items.Contains("Sale Date Range"))
					{
						cmbtsearchtype.Items.Add("Sale Date Range");
					}
					if (!cmbtsearchtype.Items.Contains("Sale Date"))
					{
						cmbtsearchtype.Items.Add("Sale Date");
					}
				}
				else
				{
					if (cmbtsearchtype.Items.Contains("Sale Date Range"))
					{
						cmbtsearchtype.Items.Remove("Sale Date Range");
					}
					if (cmbtsearchtype.Items.Contains("Sale Date"))
					{
						cmbtsearchtype.Items.Remove("Sale Date");
					}
				}
				if (!modGlobalVariables.Statics.boolRegRecords)
				{
					Label1.Text = "Enter account number";
				}
				else
				{
					Label1.Text = "Enter account number. Enter 0 to add new account";
				}
				boolLoadingOptions = true;
				//FC:FINAL:DDU:#1045 - standardize form as requested
				cmbtcontain.Text = "Contain Search Criteria";
				if (!FormActivated)
				{
					LoadLastSearchOptions();
				}
				boolLoadingOptions = false;
				FormActivated = true;
				// corey
				txtGetAccountNumber.Text = FCConvert.ToString(modRegistry.GetRegistryKey("RELastAccountNumber") != string.Empty ? FCConvert.ToString(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn)) : "0");
				// lblShortOption.Visible = True
				intProblem = 2;
				// Call OpenWWKTable(WWK, 1)
				intProblem = 3;
				if (modGlobalVariables.Statics.wmainresponse == 1)
				{
					this.Text = "Account Maintenance";
					// lblShortOption.Caption = "Account Maintenance"
				}
				else
				{
					this.Text = "Short Maintenance";
					// lblShortOption.Caption = "Short Maintenance"
				}
				intProblem = 4;
				//if (modMDIParent.FormExist(this)) return;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// Call UnlockALLRecords
				// lblLastAccount.AutoSize = True
				intProblem = 5;
				modGlobalVariables.Statics.FormLoaded = false;
				intProblem = 6;
					Label2.Visible = true;
					// lblLastAccount.Caption = Val(gintLastAccountNumber)
					txtGetAccountNumber.Text = FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
					if (modGlobalVariables.Statics.GetAccountSearch == false)
						txtGetAccountNumber.Focus();
				//}
				intProblem = 7;
				// lblLastAccount.AutoSize = True
				FCGlobal.Screen.MousePointer = 0;
				return;
			}
			catch (Exception ex)
			{
				// errtag:
				// If Err.Number = 3260 Then
				// MsgBox " Error 3260.  Record locked. Got to position " & intProblem & " in frmGetAccount_activate", vbCritical
				// Else
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "Position " + FCConvert.ToString(intProblem) + " in FrmGetAccount_Activate ", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				// End If
				return;
			}
		}

		private void frmGetAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				cmdQuit_Click();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmGetAccount_Load(object sender, System.EventArgs e)
		{
			FormActivated = false;
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalVariables.Statics.boolInPendingMode = false;
			T2KPendingDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			T2KPendingDate.Enabled = false;
			//FC:FINAL:MSH - change color to gray for disabled field
			//T2KPendingDate.BackColor = this.BackColor;
			T2KPendingDate.BackColor = System.Drawing.Color.LightGray;
			modGlobalVariables.Statics.gstrCallCalculate = (modRegistry.GetRegistryKey("CallCalculate") != string.Empty ? FCConvert.ToString(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn)) : "0");
			modGlobalVariables.Statics.gintLastAccountNumber = (modRegistry.GetRegistryKey("RELastAccountNumber") != string.Empty ? FCConvert.ToInt32(Math.Round(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn))) : 0);
			modREASValuations.Statics.Account = modGlobalVariables.Statics.gintLastAccountNumber;
			//FC:FINAL:DDU#i1604 - fixed combobox items
			if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1330))
			{
				if (Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) != "")
				{
					cmbtsearchtype_item8 = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
				}
			}
			else
			{
				cmbtsearchtype_item8 = "Open 1";
			}
			if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1340))
			{
				if (Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) != "")
				{
					cmbtsearchtype_item9 = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
				}
			}
			else
			{
				cmbtsearchtype_item9 = "Open 2";
			}
            Optrecordtype_CheckedChanged(null, System.EventArgs.Empty);
            if (modGlobalVariables.Statics.gstrCallCalculate == "01")
			{
				modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "gstrCallCalculate", "00");
				if (modGlobalVariables.Statics.gintLastAccountNumber > 0)
				{
					modGlobalVariables.Statics.CurrentAccount = modGlobalVariables.Statics.gintLastAccountNumber;
					// cmdGetAccountNumber_Click
				}
			}
			// Call LoadLastSearchOptions
			modGlobalFunctions.SetFixedSize(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			cmdQuit_Click();
		}

		private void Optcontain_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (!boolLoadingOptions)
			{
				txtSearch.Focus();
			}
		}

		private void Optcontain_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtcontain.SelectedIndex;
			Optcontain_CheckedChanged(index, sender, e);
		}

		private void Optrecordtype_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.boolRegRecords = cmbtrecordtype.Text == "Regular Records" || cmbtrecordtype.Text == "Open for Pending Changes";
			// chkPending.Enabled = boolRegRecords
			if (!modGlobalVariables.Statics.boolRegRecords)
			{
				Label1.Text = "Enter account number";
			}
			else
			{
				Label1.Text = "Enter account number. Enter 0 to add new account";
			}
			if (cmbtrecordtype.Text == "Previous Owners")
			{
				cmbtsearchtype.Clear();
				cmbtsearchtype.Items.Add("Name");
				cmbtsearchtype.Items.Add("Map / Lot");
				cmbtsearchtype.Items.Add("Sale Date Range");
				cmbtsearchtype.Items.Add("Sale Date");
				cmbtsearchtype.Text = "Name";
				chkPrevOwner.CheckState = Wisej.Web.CheckState.Unchecked;
				chkPrevOwner.Enabled = false;
				chkShowDel.CheckState = Wisej.Web.CheckState.Unchecked;
				chkShowDel.Enabled = false;
				T2KPendingDate.Enabled = false;
				//T2KPendingDate.BackColor = frmGetAccount.InstancePtr.BackColor;   
				// FC:FINAL:VGE - #i959 Changing color to LightGrey on disable.
				T2KPendingDate.BackColor = System.Drawing.Color.LightGray;
			}
			//FC:FINAL:MSH - i.issue #959: replaced incorrect compare operation
			//else if (cmbtrecordtype.Text == "Open for Pending Changes")
			else if (cmbtrecordtype.Text != "Open for Pending Changes")
			{
				chkPrevOwner.Enabled = true;
				chkShowDel.Enabled = true;
				cmbtsearchtype.Clear();
				cmbtsearchtype.Items.Add("Name");
				cmbtsearchtype.Items.Add("Location");
				cmbtsearchtype.Items.Add("Map / Lot");
				if (!modGlobalVariables.Statics.boolRegRecords)
				{
					cmbtsearchtype.Items.Add("Sale Date Range");
					cmbtsearchtype.Items.Add("Sale Date");
				}
				cmbtsearchtype.Items.Add("Book / Page");
				cmbtsearchtype.Items.Add("Ref 1");
				cmbtsearchtype.Items.Add("Ref 2");
				cmbtsearchtype.Items.Add(cmbtsearchtype_item8);
				cmbtsearchtype.Items.Add(cmbtsearchtype_item9);
                //FC:FINAL:BSE #3349 selected index lost when clearing and reinitializing items 
                cmbtsearchtype.SelectedIndex = 0;
				T2KPendingDate.Enabled = false;
				//FC:FINAL:MSH - change color to gray for disabled field
				//T2KPendingDate.BackColor = frmGetAccount.InstancePtr.BackColor;
				T2KPendingDate.BackColor = System.Drawing.Color.LightGray;
			}
			else
			{
				chkPrevOwner.Enabled = true;
				chkShowDel.CheckState = Wisej.Web.CheckState.Unchecked;
				chkShowDel.Enabled = false;
				cmbtsearchtype.Clear();
				cmbtsearchtype.Items.Add("Name");
				cmbtsearchtype.Items.Add("Location");
				cmbtsearchtype.Items.Add("Map / Lot");
				cmbtsearchtype.Items.Add("Book / Page");
				cmbtsearchtype.Items.Add("Ref 1");
				cmbtsearchtype.Items.Add("Ref 2");
				cmbtsearchtype.Items.Add(cmbtsearchtype_item8);
				cmbtsearchtype.Items.Add(cmbtsearchtype_item9);
				T2KPendingDate.Enabled = true;
				T2KPendingDate.BackColor = txtSearch.BackColor;
			}
		}

		private void Optrecordtype_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtrecordtype.SelectedIndex;
			Optrecordtype_CheckedChanged(index, sender, e);
		}

		private void Optsearchtype_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			//FC:FINAL:DDU:#1045 - standardize form as requested
			// txtSearch.Text = ""
			txtSearch.Width = 245;
			if (cmbtsearchtype.Text == "Name")
			{
				chkPrevOwner.Visible = true;
			}
			else
			{
				chkPrevOwner.Visible = false;
			}
			if (cmbtsearchtype.Text == "Sale Date Range")
			{
				txtSearch.Text = "Don't Specify Sale Date Range";
			}
			txtSearch.MaxLength = 0;
			if (cmbtsearchtype.Text == "Book / Page")
			{
				//txtSearch.Width = cmdClear.Width;
				txtSearch2.Visible = true;
				//txtSearch2.Width = cmdSearch.Width;
				//txtSearch2.Left = cmdSearch.Left;
				cmbtcontain.Enabled = false;
				lblBook.Visible = true;
				lblPage.Visible = true;
				lblBook.Left = cmbtsearchtype.Left + cmbtsearchtype.Width + 30;
				txtSearch.Width = 150;
				txtSearch2.Width = 150;
				txtSearch.Left = lblBook.Left + lblBook.Width + 30;
				lblPage.Left = txtSearch.Left + txtSearch.Width + 30;
				txtSearch2.Left = lblPage.Left + lblPage.Width + 30;
				lblPage.Alignment = HorizontalAlignment.Center;
				lblPage.Alignment = HorizontalAlignment.Center;
				lblBook.Text = "Book";
				lblPage.Text = "Page";
			}
			else if (cmbtsearchtype.Text == cmbtsearchtype_item8 || cmbtsearchtype.Text == cmbtsearchtype_item9)
			{
				txtSearch2.Visible = false;
				//txtSearch.Width = cmdSearch.Left + cmdSearch.Width - cmdClear.Left;
				txtSearch.Left = cmbtsearchtype.Left + cmbtsearchtype.Width + 30;
				cmbtcontain.Enabled = false;
				lblBook.Visible = false;
				lblPage.Visible = false;
				txtSearch.MaxLength = 4;
			}
			else if (cmbtsearchtype.Text == "Location")
			{
				txtSearch.MaxLength = 6;
				txtSearch2.Visible = true;
				cmbtcontain.Enabled = true;
				lblBook.Visible = true;
				lblPage.Visible = true;
				txtSearch.Width = 84;
				txtSearch2.Width = 224;
				lblBook.Left = cmbtsearchtype.Left + cmbtsearchtype.Width + 30;
				txtSearch.Left = lblBook.Left + lblBook.Width + 30;
				lblPage.Left = txtSearch.Left + txtSearch.Width + 30;
				txtSearch2.Left = lblPage.Left + lblPage.Width + 30;
				lblBook.Text = "St #";
				lblPage.Text = "Street";
				lblSearchInfo.Visible = false;
				lblPage.Alignment = HorizontalAlignment.Left;
				lblPage.Alignment = HorizontalAlignment.Left;
			}
			else
			{
				txtSearch2.Visible = false;
				//txtSearch.Width = cmdSearch.Left + cmdSearch.Width - cmdClear.Left;
				txtSearch.Left = cmbtsearchtype.Left + cmbtsearchtype.Width + 30;
				cmbtcontain.Enabled = true;
				lblBook.Visible = false;
				lblPage.Visible = false;
			}
            if (cmbtsearchtype.Text != "Sale Date Range" && cmbtsearchtype.Text != "Sale Date")
            {
				txtSearch.Focus();
			}
		}

		private void Optsearchtype_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtsearchtype.SelectedIndex;
			Optsearchtype_CheckedChanged(index, sender, e);
		}

		private void txtGetAccountNumber_Enter(object sender, System.EventArgs e)
		{
			txtGetAccountNumber.SelectionStart = 0;
			if (txtGetAccountNumber.Text.Length != 0)
			{
				txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
			}
		}

		private void txtGetAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				cmdGetAccountNumber_Click();
			}
		}

		private void txtSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				cmdSearch_Click();
			}
		}

		private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (cmbtsearchtype.Text == "Location")
			{
				// location
				if ((KeyAscii >= Keys.A && KeyAscii <= Keys.Z) || (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11))
				{
					KeyAscii = (Keys)0;
					return;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSearch2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				cmdSearch_Click();
			}
		}

		private void cmbtrecordtype_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//if (cmbtrecordtype.SelectedIndex == 0)
			// FC:FINAL:VGE - #i959 Changing condition to match expected logic
			if (cmbtrecordtype.SelectedIndex != -1)
			{
				Optrecordtype_CheckedChanged(sender, e);
			}
		}

		private void cmbtcontain_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbtcontain.SelectedIndex == 0)
			{
				Optcontain_CheckedChanged(sender, e);
			}
		}

		private void cmbtsearchtype_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//if (cmbtsearchtype.SelectedIndex == 0)
			// FC:FINAL:VGE - #i959 Changing condition to match expected logic
			if (cmbtsearchtype.SelectedIndex != -1)
			{
				Optsearchtype_CheckedChanged(sender, e);
			}
		}
	}
}
