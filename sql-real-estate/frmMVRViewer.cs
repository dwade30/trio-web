﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

//using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmMVRViewer.
	/// </summary>
	public partial class frmMVRViewer : BaseForm
	{
		public frmMVRViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMVRViewer InstancePtr
		{
			get
			{
				return (frmMVRViewer)Sys.GetInstance(typeof(frmMVRViewer));
			}
		}

		protected frmMVRViewer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		// vbPorter upgrade warning: lngX As int	OnWriteFCConvert.ToSingle(
		int lngX;
		// vbPorter upgrade warning: lngY As int	OnWriteFCConvert.ToSingle(
		int lngY;
		int lngPageX;
		int lngPageY;
		int intCurrentPage;
		// this is a multi page report, but only one is in the viewer at any one time
		int intMaxPage;
		string strFiscalStart = "";
		const int CNSTCURRENTMVRREPORTYEAR = 2017;
		const int CNSTMVRVALPPOTHER = 0;
		const int CNSTMVRVALPPMACHEQUIP = 1;
		const int CNSTMVRVALPPBUSEQUIP = 2;
		const string CNSTEXCELDOCNAME = "2017_mvr_fillable.xlsx";
		private string strExcelDocName = string.Empty;
		


		private void ARViewer21_MouseDownEvent(object sender, MouseEventArgs e)
		{
			lngX = FCConvert.ToInt32(e.X);
			lngY = FCConvert.ToInt32(e.Y);
		}

		private void ARViewer21_MouseOver(object sender, EventArgs e)
		{
			lngPageX = ARViewer21.Location.X;
			lngPageY = ARViewer21.Location.Y;
		}

		private void cmbInput_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strTemp = "";
			if (cmbInput.Visible == false)
				return;
			switch (cmbInput.ItemData(cmbInput.SelectedIndex))
			{
				case modGlobalVariables.CNSTMVRDATATYPEBOOLEAN:
					{
						if (cmbInput.SelectedIndex == 0)
						{
							strTemp = "FALSE";
						}
						else
						{
							strTemp = "TRUE";
						}
						break;
					}
				case modGlobalVariables.CNSTMVRDATATYPELIST:
					{
						strTemp = cmbInput.Text;
						break;
					}
			}
			//end switch
			UpdateTables_2(FCConvert.ToInt32(Conversion.Val(cmbInput.Tag)), strTemp);
			UpdateFromInput();
		}

		public void cmbInput_Click()
		{
			cmbInput_SelectedIndexChanged(cmbInput, new System.EventArgs());
		}

		private void frmMVRViewer_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmMVRViewer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMVRViewer properties;
			//frmMVRViewer.FillStyle	= 0;
			//frmMVRViewer.ScaleWidth	= 9300;
			//frmMVRViewer.ScaleHeight	= 7845;
			//frmMVRViewer.LinkTopic	= "Form2";
			//frmMVRViewer.LockControls	= true;
			//frmMVRViewer.PaletteMode	= 1  'UseZOrder;
			//txtRichText properties;
			//txtRichText.TextRTF	= $"frmMVRViewer.frx":058A;
			//End Unmaped Properties
			int x;
			int lngNumPages;
			//FileSystemObject fso = new FileSystemObject();
			int lngTemp = 0;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			strExcelDocName = CNSTEXCELDOCNAME;
			//if (!File.Exists(strExcelDocName))
			//{
			//	strExcelDocName = FCConvert.ToString(CNSTCURRENTMVRREPORTYEAR) + " MVR Electronic Copy.xlsx";
			//	if (!File.Exists(strExcelDocName))
			//	{
			//		cmdExcelDoc.Enabled = false;
			//	}
			//}
			modGlobalRoutines.LoadCustomizedInfo();
			if (modGlobalConstants.Statics.gboolPP)
			{
				lngTemp = modGlobalVariables.Statics.CustomizedInfo.PPCat1TaxValCat + modGlobalVariables.Statics.CustomizedInfo.PPCat2TaxValCat + modGlobalVariables.Statics.CustomizedInfo.PPCat3TaxValCat + modGlobalVariables.Statics.CustomizedInfo.PPCat4TaxValCat;
				lngTemp += modGlobalVariables.Statics.CustomizedInfo.PPCat5TaxValCat + modGlobalVariables.Statics.CustomizedInfo.PPCat6TaxValCat + modGlobalVariables.Statics.CustomizedInfo.PPCat7TaxValCat;
				lngTemp += modGlobalVariables.Statics.CustomizedInfo.PPCat8TaxValCat + modGlobalVariables.Statics.CustomizedInfo.PPCat9TaxValCat + modGlobalVariables.Statics.CustomizedInfo.PPIndPropCat;
				if (lngTemp == 0)
				{
					// apparently nothing is setup
					MessageBox.Show("You do not have personal property categories set up in the MVR section of the customize screen" + "\r\n" + "The personal property sections of the MVR will not be calculated correctly", "Personal Property Not Set Up", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			modGlobalVariables.Statics.LandTypes.Init();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//modGlobalFunctions.SetTRIOColors(this);
			SetupGridValuation();
			SetupGrid();
			intCurrentPage = 1;
			// ARViewer21.ReportSource = rptmvr
			// ARViewer21.Zoom = -1
			// Grid.TextMatrix(CNSTMVRROWYEAR, 0) = Year(Date)
			Grid.TextMatrix(modMVR.CNSTMVRROWYEAR, 0, FCConvert.ToString(CNSTCURRENTMVRREPORTYEAR));
			//Application.DoEvents();
			FillGrid();
			UpdateFromInput();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void frmMVRViewer_Resize(object sender, System.EventArgs e)
		{
			ResizeGridValuation();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void GridValuation_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = 0;
						GridValuation.Row = 0;
						GridValuation.Col = 0;
						GridValuation.Visible = false;
						UpdateFromInput();
						break;
					}
			}
			//end switch
		}

		private void GridValuation_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case (Keys)188:
					{
						// comma
						KeyCode = 0;
						break;
					}
				case Keys.Return:
					{
						KeyCode = 0;
						GridValuation.Row = 0;
						GridValuation.Col = 0;
						GridValuation.Visible = false;
						UpdateFromInput();
						break;
					}
			}
			//end switch
		}

		private void GridValuation_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == Convert.ToByte(","[0]))
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}
		//FC:TODO commented because of Excel missing part
		private void mnuExcelDoc_Click(object sender, System.EventArgs e)
		{
			//			Excel.Application xlApp;
			//			Excel.Workbook xlWorkbook;
			//            //Excel.Worksheet xlSheet;
			//            string strTemp = "";
			//			//FileSystemObject fso = new FileSystemObject();
			//			clsDRWrapper clsLoad = new clsDRWrapper();
			//			// vbPorter upgrade warning: lngHalfHomestead As int	OnWriteFCConvert.ToDouble(
			//			int lngHalfHomestead = 0;
			//			double lngREValuation = 0;
			//			double lngPPValuation = 0;
			//			double dblTaxRate = 0;
			//			double dblTotVal = 0;
			//			double dblTotalAppropriations = 0;
			//			double dblTotalRevenue = 0;
			//			double dblNetToBeRaised = 0;
			//			double dblMaxTax = 0;
			//			// vbPorter upgrade warning: dblMaxOverLay As double	OnWrite(string)
			//			double dblMaxOverLay = 0;
			//			double dblTotTaxed = 0;
			//			double dblHomesteadReim = 0;
			//			 /*? On Error Resume Next  */
			//			try {
			//			xlApp = (Excel.Application) Interaction.CreateObject("Excel.Application");
			//			if (Information.Err().Number!=0) {
			//				MessageBox.Show("Could not open excel document"+"\r\n"+"Check to make sure that excel is installed on your system", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			//				return;
			//			}
			//			try
			//			{	// On Error GoTo ErrorHandler
			//				if (File.Exists(strExcelDocName)) {
			//					xlWorkbook = xlApp.Workbooks.Open(Environment.CurrentDirectory+"\\"+strExcelDocName);
			//				} else {
			//					MessageBox.Show("Could not find file: "+Environment.CurrentDirectory+"\\"+strExcelDocName, "No File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			//					xlApp = null;
			//					return;
			//				}
			//				xlApp.Visible = false;
			//				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			//				foreach (Excel.Worksheet xlSheet in xlWorkbook.Sheets) {
			//					strTemp = xlSheet.Name;
			//					if (Strings.UCase(xlSheet.Name)=="PAGE 1")
			//					{
			//						xlSheet.Cells[5, 10] = Grid.TextMatrix(modMVR.CNSTMVRROWCOMMITMENTDATE, 0);
			//						xlSheet.Cells[5, 5] = Grid.TextMatrix(modMVR.CNSTMVRROWCOUNTY, 0);
			//						xlSheet.Cells[7, 6] = Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0);
			//						xlSheet.Cells[10, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWCERTIFIEDRATIO, 0))/100;
			//						xlSheet.Cells[16, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALLAND, 0));
			//						xlSheet.Cells[18, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALBLDG, 0));
			//						xlSheet.Cells[20, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTRE, 0));
			//						xlSheet.Cells[24, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALMACHEQUIP, 0));
			//						xlSheet.Cells[26, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALFURNFIX, 0));
			//						xlSheet.Cells[28, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTOTHERPP, 0));
			//						xlSheet.Cells[30, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTPP, 0));
			//						xlSheet.Cells[35, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTREPP, 0));
			//						xlSheet.Cells[37, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXRATE, 0)); // * 1000
			//						xlSheet.Cells[39, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXLEVY, 0));
			//						xlSheet.Cells[46, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTFULLGRANTED, 0));
			//						xlSheet.Cells[48, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEFULLGRANTED, 0));
			//						xlSheet.Cells[50, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTFULLEXEMPTGRANTED, 0));
			//						xlSheet.Cells[53, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEFULLEXEMPTGRANTED, 0));
			//						xlSheet.Cells[56, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTGRANTED, 0));
			//						xlSheet.Cells[58, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEGRANTED, 0));
			//						xlSheet.Cells[61, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTASSESSED, 0));
			//					}
			//					else if (Strings.UCase(xlSheet.Name)=="PAGE 2")
			//					{
			//						xlSheet.Cells[3, 4] = Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0);
			//						xlSheet.Cells[11, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTOTBETE, 0));
			//						xlSheet.Cells[13, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTIFBETE, 0));
			//						xlSheet.Cells[9, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRNUMBETE, 0));
			//						xlSheet.Cells[7, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTOTBETEAPPROVED, 0));
			//						xlSheet.Cells[17, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTIFINCREASE, 0));
			//						xlSheet.Cells[19, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTIFASSESSED, 0));
			//						xlSheet.Cells[22, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTIFTAXREVENUE, 0));
			//						xlSheet.Cells[24, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTIFBETEDEPOSITED, 0));
			//						xlSheet.Cells[31, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWMVEXCISECOLLECTED, 0));
			//						xlSheet.Cells[29, 8] = Grid.TextMatrix(modMVR.CNSTMVRROWMVEXCISECALENDARFISCAL, 0);
			//						// If UCase(.TextMatrix(CNSTMVRROWMVEXCISECALENDARFISCAL, 0)) = "FISCAL YEAR" Then
			//						// xlSheet.Cells[16, 5] = ""
			//						// xlSheet.Cells[17, 5] = "X"
			//						// xlSheet.Cells[19, 5] = ""
			//						// xlSheet.Cells[20, 5] = "X"
			//						// Else
			//						// xlSheet.Cells[16, 5] = "X"
			//						// xlSheet.Cells[17, 5] = ""
			//						// xlSheet.Cells[19, 5] = "X"
			//						// xlSheet.Cells[20, 5] = ""
			//						// End If
			//						xlSheet.Cells[33, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWWATERCRAFTEXCISECOLLECTED, 0));
			//						// xlSheet.Cells[34, 8] = Val(.TextMatrix(CNSTMVRINDUSTRIALRE, 0))
			//						// xlSheet.Cells[37, 8] = Val(.TextMatrix(CNSTMVRINDUSTRIALPP, 0))
			//						// xlSheet.Cells[39, 8] = Val(.TextMatrix(CNSTMVRINDUSTRIALREPP, 0))
			//						xlSheet.Cells[37, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRINDUSTRIALTRANSMISSIONDISTRIBUTIONLINES, 0));
			//						xlSheet.Cells[39, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRELECTRICALGENERATIONFACILITIES, 0));
			//						// xlSheet.Cells[43, 8] = Val(.TextMatrix(CNSTMVRINDUSTRIALDAMS, 0))
			//						xlSheet.Cells[43, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHPERACREUNDEVELOPED, 0));
			//						xlSheet.Cells[46, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHNUMPARCELS, 0));
			//						xlSheet.Cells[48, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHSOFTACREAGE, 0));
			//						xlSheet.Cells[50, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHMIXEDACREAGE, 0));
			//						xlSheet.Cells[52, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHHARDACREAGE, 0));
			//						xlSheet.Cells[54, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHTOTACREAGE, 0));
			//						xlSheet.Cells[56, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHTOTASSESSED, 0));
			//						xlSheet.Cells[58, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHSOFTPERACRE, 0));
			//						xlSheet.Cells[60, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHMIXEDPERACRE, 0));
			//						xlSheet.Cells[62, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHHARDPERACRE, 0));
			//					}
			//					else if (Strings.UCase(xlSheet.Name)=="PAGE 3")
			//					{
			//						xlSheet.Cells[3, 4] = Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0);
			//						xlSheet.Cells[6, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHTOTACREAGECLASSIFIEDCURRENT, 0));
			//						// xlSheet.Cells[9, 8] = Val(.TextMatrix(CNSTMVRTREEGROWTHWITHDRAWNCHANGES, 0))
			//						// xlSheet.Cells[12, 8] = Val(.TextMatrix(CNSTMVRTREEGROWTHWITHDRAWNREQUEST, 0))
			//						xlSheet.Cells[10, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHTOTALWITHDRAWN, 0));
			//						xlSheet.Cells[12, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHTOTALACRESWITHDRAWN, 0));
			//						xlSheet.Cells[14, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHTOTALPENALTIES, 0));
			//						xlSheet.Cells[16, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHNUMTOTALPENALTIES, 0));
			//						if (Strings.Trim(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHTRANSFERREDYESNO, 0))!=string.Empty) {
			//							if (FCConvert.CBool(Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHTRANSFERREDYESNO, 0))) {
			//								xlSheet.Cells[18, 8] = "YES";
			//							} else {
			//								xlSheet.Cells[18, 8] = "NO";
			//							}
			//						} else {
			//							xlSheet.Cells[18, 8] = "NO";
			//						}
			//						xlSheet.Cells[23, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMPARCELSCLASSIFIED, 0));
			//						xlSheet.Cells[25, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMACREAGENEWCLASSIFIED, 0));
			//						xlSheet.Cells[27, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMTOTACREAGETILLABLE, 0));
			//						xlSheet.Cells[30, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMTOTVALTILLABLE, 0));
			//						xlSheet.Cells[40, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMTOTACREAGEWOODLAND, 0));
			//						xlSheet.Cells[42, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMTOTVALWOODLAND, 0));
			//						xlSheet.Cells[34, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMSOFTACRES, 0));
			//						xlSheet.Cells[36, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMMIXEDACRES, 0));
			//						xlSheet.Cells[38, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMHARDACRES, 0));
			//						xlSheet.Cells[44, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMSOFTPERACRE, 0));
			//						xlSheet.Cells[46, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMMIXEDPERACRE, 0));
			//						xlSheet.Cells[48, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMHARDPERACRE, 0));
			//						xlSheet.Cells[52, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMTOTALWITHDRAWN, 0));
			//						xlSheet.Cells[54, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMTOTALACRESWITHDRAWN, 0));
			//						xlSheet.Cells[57, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRFARMTOTALPENALTIES, 0));
			//						xlSheet.Cells[60, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVROPENPARCELS, 0));
			//						xlSheet.Cells[62, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVROPENNEWCLASSIFIED, 0));
			//						xlSheet.Cells[64, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVROPENTOTACREAGECLASSIFIED, 0));
			//						xlSheet.Cells[66, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVROPENTOTVALCLASSIFIED, 0));
			//					}
			//					else if (Strings.UCase(xlSheet.Name)=="PAGE 4")
			//					{
			//						xlSheet.Cells[3, 4] = Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0);
			//						xlSheet.Cells[8, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVROPENPARCELSWITHDRAWN, 0));
			//						xlSheet.Cells[10, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVROPENACRESWITHDRAWN, 0));
			//						xlSheet.Cells[12, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVROPENTOTALPENALTIES, 0));
			//						xlSheet.Cells[17, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRWATERFRONTPARCELS, 0));
			//						xlSheet.Cells[19, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRWATERFRONTFIRSTCLASSIFIED, 0));
			//						xlSheet.Cells[21, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRWATERFRONTCURRENTACREAGE, 0));
			//						xlSheet.Cells[23, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRWATERFRONTCURRENTVALUATION, 0));
			//						xlSheet.Cells[27, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRWATERFRONTTOTALWITHDRAWN, 0));
			//						xlSheet.Cells[29, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRWATERFRONTTOTALACRESWITHDRAWN, 0));
			//						xlSheet.Cells[31, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRWATERFRONTTOTALPENALTIES, 0));
			//						xlSheet.Cells[38, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTUS, 0));
			//						xlSheet.Cells[40, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTME, 0));
			//						xlSheet.Cells[42, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTUSME, 0));
			//						xlSheet.Cells[45, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTNHWATER, 0));
			//						xlSheet.Cells[48, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTQUASIMUNI, 0));
			//						xlSheet.Cells[53, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTHYDROOUTSIDEMUNI, 0));
			//						xlSheet.Cells[57, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTPUBLICAIRPORT, 0));
			//						xlSheet.Cells[60, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTPRIVATEAIRPORT, 0));
			//						xlSheet.Cells[63, 8] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTSEWAGEDISPOSAL, 0));
			//					}
			//					else if (Strings.UCase(xlSheet.Name)=="PAGE 5")
			//					{
			//						xlSheet.Cells[3, 5] = Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0);
			//						xlSheet.Cells[7, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTBENEVOLENT, 0));
			//						xlSheet.Cells[9, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTLITERARY, 0));
			//						xlSheet.Cells[15, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTVETORGS, 0));
			//						xlSheet.Cells[18, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTVETORGSADMINISTRATIVE, 0));
			//						xlSheet.Cells[21, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTCHAMBERCOMMERCE, 0));
			//						xlSheet.Cells[26, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTNUMPARSONAGES, 0));
			//						xlSheet.Cells[28, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTEXEMPTPARSONAGES, 0));
			//						xlSheet.Cells[30, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTTAXABLEPARSONAGES, 0));
			//						xlSheet.Cells[32, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTEXEMPTCHURCHES, 0));
			//						xlSheet.Cells[35, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTTOTALRELIGIOUS, 0));
			//						xlSheet.Cells[39, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTFRATERNALORGS, 0));
			//						xlSheet.Cells[43, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTLEASEDHOSPITAL, 0));
			//						xlSheet.Cells[48, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTBLIND, 0));
			//						xlSheet.Cells[51, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTWATERPIPES, 0));
			//						xlSheet.Cells[55, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTANIMALWASTE, 0));
			//						xlSheet.Cells[58, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTPOLLUTION, 0));
			//						xlSheet.Cells[61, 10] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTSNOWGROOMING, 0));
			//					}
			//					else if (Strings.UCase(xlSheet.Name)=="PAGE 6")
			//					{
			//						xlSheet.Cells[3, 6] = Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0);
			//						// xlSheet.Cells[13, 10] = Val(.TextMatrix(CNSTMVRVETEXEMPTNUMWWIWIDOWERS, 0))
			//						// xlSheet.Cells[13, 14] = Val(.TextMatrix(CNSTMVRVETEXEMPTWWIWIDOWERS, 0))
			//						xlSheet.Cells[10, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMWWIIWIDOWERS, 0));
			//						xlSheet.Cells[10, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTWWIIWIDOWERS, 0));
			//						// xlSheet.Cells[21, 10] = Val(.TextMatrix(CNSTMVRVETEXEMPTNUMWWILIVINGTRUST, 0))
			//						// xlSheet.Cells[21, 14] = Val(.TextMatrix(CNSTMVRVETEXEMPTWWILIVINGTRUST, 0))
			//						xlSheet.Cells[15, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMPARAPLEGICLIVINGTRUST, 0));
			//						xlSheet.Cells[15, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTPARAPLEGICLIVINGTRUST, 0));
			//						xlSheet.Cells[18, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMOTHERLIVINGTRUST, 0));
			//						xlSheet.Cells[18, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTOTHERLIVINGTRUST, 0));
			//						xlSheet.Cells[23, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMWWIRESIDENT, 0));
			//						xlSheet.Cells[23, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTWWIRESIDENT, 0));
			//						xlSheet.Cells[26, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMWWINONRESIDENT, 0));
			//						xlSheet.Cells[26, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTWWINONRESIDENT, 0));
			//						xlSheet.Cells[57, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMVIETNAM, 0));
			//						xlSheet.Cells[57, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTVIETNAM, 0));
			//						xlSheet.Cells[31, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMPARAPLEGIC, 0));
			//						xlSheet.Cells[31, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTPARAPLEGIC, 0));
			//						xlSheet.Cells[41, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTTOTNUMOTHERRESIDENT, 0));
			//						xlSheet.Cells[41, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTOTHERRESIDENT, 0));
			//						xlSheet.Cells[44, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMOTHERNONRESIDENT, 0));
			//						xlSheet.Cells[44, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTOTHERNONRESIDENT, 0));
			//						xlSheet.Cells[50, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMVETLINEOFDUTY, 0));
			//						xlSheet.Cells[50, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTVETLINEOFDUTY, 0));
			//						xlSheet.Cells[53, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMLEBANONPANAMA, 0));
			//						xlSheet.Cells[53, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTLEBANONPANAMA, 0));
			//						xlSheet.Cells[62, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTTOTNUM, 0));
			//						xlSheet.Cells[64, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTTOTAL, 0));
			//						xlSheet.Cells[36, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMCOOP, 0));
			//						xlSheet.Cells[36, 14] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTCOOP, 0));
			//					}
			//					else if (Strings.UCase(xlSheet.Name)=="PAGE 7")
			//					{
			//						xlSheet.Cells[3, 4] = Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0);
			//						xlSheet.Cells[19, 1] = Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERNAME1, 0);
			//						xlSheet.Cells[19, 5] = Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERPROVISION1, 0);
			//						xlSheet.Cells[19, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERVALUE1, 0));
			//						xlSheet.Cells[21, 1] = Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERNAME2, 0);
			//						xlSheet.Cells[21, 5] = Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERPROVISION2, 0);
			//						xlSheet.Cells[21, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERVALUE2, 0));
			//						xlSheet.Cells[23, 1] = Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERNAME3, 0);
			//						xlSheet.Cells[23, 5] = Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERPROVISION3, 0);
			//						xlSheet.Cells[23, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERVALUE3, 0));
			//						xlSheet.Cells[25, 1] = Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERNAME4, 0);
			//						xlSheet.Cells[25, 5] = Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERPROVISION4, 0);
			//						xlSheet.Cells[25, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERVALUE4, 0));
			//						xlSheet.Cells[27, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERTOTAL, 0));
			//						xlSheet.Cells[29, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTTOTALVALUE, 0));
			//						if (Strings.Trim(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSTAXMAPSYESNO, 0))!=string.Empty) {
			//							if (FCConvert.CBool(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSTAXMAPSYESNO, 0))) {
			//								xlSheet.Cells[34, 9] = "YES";
			//							} else {
			//								xlSheet.Cells[34, 9] = "NO";
			//							}
			//						} else {
			//							xlSheet.Cells[34, 9] = "NO";
			//						}
			//						xlSheet.Cells[38, 7] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSTAXMAPSDATE, 0);
			//						xlSheet.Cells[40, 7] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSTAXMAPSCONTRACTOR, 0);
			//						xlSheet.Cells[42, 7] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSTAXMAPSTYPE, 0);
			//						xlSheet.Cells[45, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSLANDPARCELS, 0));
			//						xlSheet.Cells[47, 11] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSTAXABLEACREAGE, 0));
			//						if (Strings.Trim(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONYESNO, 0))!=string.Empty) {
			//							if (FCConvert.CBool(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONYESNO, 0))) {
			//								xlSheet.Cells[51, 9] = "YES";
			//							} else {
			//								xlSheet.Cells[51, 9] = "NO";
			//							}
			//						} else {
			//							xlSheet.Cells[51, 9] = "NO";
			//						}
			//						if (Strings.Trim(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONLAND, 0))!=string.Empty) {
			//							if (FCConvert.CBool(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONLAND, 0))) {
			//								xlSheet.Cells[56, 9] = "YES";
			//							} else {
			//								xlSheet.Cells[56, 9] = "NO";
			//							}
			//						} else {
			//							xlSheet.Cells[56, 9] = "NO";
			//						}
			//						if (Strings.Trim(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONBUILDINGS, 0))!=string.Empty) {
			//							if (FCConvert.CBool(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONBUILDINGS, 0))) {
			//								xlSheet.Cells[58, 9] = "YES";
			//							} else {
			//								xlSheet.Cells[58, 9] = "NO";
			//							}
			//						} else {
			//							xlSheet.Cells[58, 9] = "NO";
			//						}
			//						if (Strings.Trim(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONPERSONALPROPERTY, 0))!=string.Empty) {
			//							if (FCConvert.CBool(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONPERSONALPROPERTY, 0))) {
			//								xlSheet.Cells[60, 9] = "YES";
			//							} else {
			//								xlSheet.Cells[60, 9] = "NO";
			//							}
			//						} else {
			//							xlSheet.Cells[60, 9] = "NO";
			//						}
			//						xlSheet.Cells[62, 9] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONEFFECTIVEDATE, 0);
			//						xlSheet.Cells[64, 9] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONCONTRACTORNAME, 0);
			//						xlSheet.Cells[66, 9] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSREVALUATIONCOST, 0));
			//					}
			//					else if (Strings.UCase(xlSheet.Name)=="PAGE 8")
			//					{
			//						xlSheet.Cells[3, 5] = Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0);
			//						xlSheet.Cells[9, 5] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSASSESSMENTFUNCTIONSINGLEASSESSOR, 0);
			//						xlSheet.Cells[11, 5] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSASSESSMENTFUNCTIONSINGLEASSESSORNAME, 0);
			//						xlSheet.Cells[13, 5] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSEMAIL, 0);
			//						// Select Case UCase(.TextMatrix(CNSTMVRMUNICIPALRECSASSESSMENTFUNCTIONSINGLEASSESSOR, 0))
			//						// Case "ASSESSORS AGENT"
			//						// xlSheet.Cells[10, 4] = "X"
			//						// xlSheet.Cells[10, 8] = .TextMatrix(CNSTMVRMUNICIPALRECSASSESSMENTFUNCTIONASSESSORSAGENTNAME, 0)
			//						// xlSheet.Cells[9, 4] = ""
			//						// xlSheet.Cells[9, 8] = ""
			//						// xlSheet.Cells[11, 4] = ""
			//						// xlSheet.Cells[11, 8] = ""
			//						// xlSheet.Cells[12, 4] = ""
			//						// xlSheet.Cells[12, 8] = ""
			//						// Case "ELECTED OFFICIAL"
			//						// Case "SELECTMANASSESSOR"
			//						// xlSheet.Cells[11, 4] = "X"
			//						// xlSheet.Cells[10, 4] = ""
			//						// xlSheet.Cells[9, 4] = ""
			//						// xlSheet.Cells[9, 8] = ""
			//						// xlSheet.Cells[10, 8] = ""
			//						// xlSheet.Cells[12, 4] = ""
			//						// Case "BOARD OF ASSESSORS"
			//						// xlSheet.Cells[12, 4] = "X"
			//						// xlSheet.Cells[10, 4] = ""
			//						// xlSheet.Cells[9, 4] = ""
			//						// xlSheet.Cells[9, 8] = ""
			//						// xlSheet.Cells[10, 8] = ""
			//						// xlSheet.Cells[11, 4] = ""
			//						// Case Else
			//						// xlSheet.Cells[9, 4] = "X"
			//						// xlSheet.Cells[9, 8] = .TextMatrix(CNSTMVRMUNICIPALRECSASSESSMENTFUNCTIONSINGLEASSESSORNAME, 0)
			//						// xlSheet.Cells[10, 4] = ""
			//						// xlSheet.Cells[10, 8] = ""
			//						// xlSheet.Cells[11, 4] = ""
			//						// xlSheet.Cells[12, 4] = ""
			//						// End Select
			//						xlSheet.Cells[17, 6] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSFISCALSTART, 0);
			//						xlSheet.Cells[17, 9] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSFISCALEND, 0);
			//						xlSheet.Cells[20, 9] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSINTERESTRATE, 0));
			//						xlSheet.Cells[23, 7] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSDATEDUE1, 0);
			//						xlSheet.Cells[23, 9] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSDATEDUE2, 0);
			//						xlSheet.Cells[24, 7] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSDATEDUE3, 0);
			//						xlSheet.Cells[24, 9] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSDATEDUE4, 0);
			//						if (Strings.Trim(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSCOMPUTERIZEDYESNO, 0))!=string.Empty) {
			//							if (FCConvert.CBool(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSCOMPUTERIZEDYESNO, 0))) {
			//								xlSheet.Cells[28, 4] = "YES";
			//								xlSheet.Cells[28, 8] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSCOMPUTERIZEDCONTRACTOR, 0);
			//							} else {
			//								xlSheet.Cells[28, 4] = "NO";
			//								xlSheet.Cells[28, 8] = "";
			//							}
			//						} else {
			//							xlSheet.Cells[28, 4] = "NO";
			//							xlSheet.Cells[28, 8] = "";
			//						}
			//						if (Strings.Trim(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSLOCALTAXRELIEFYESNO, 0))!=string.Empty) {
			//							if (FCConvert.CBool(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSLOCALTAXRELIEFYESNO, 0))) {
			//								xlSheet.Cells[32, 4] = "YES";
			//								xlSheet.Cells[32, 9] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSLOCALTAXRELIEFHOWMANY, 0));
			//								xlSheet.Cells[34, 9] = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSLOCALTAXRELIEFHOWMUCH, 0));
			//							} else {
			//								xlSheet.Cells[32, 4] = "NO";
			//								xlSheet.Cells[32, 9] = "";
			//								xlSheet.Cells[34, 9] = "";
			//							}
			//						} else {
			//							xlSheet.Cells[32, 4] = "NO";
			//							xlSheet.Cells[32, 9] = "";
			//							xlSheet.Cells[34, 9] = "";
			//						}
			//						// xlSheet.Cells[3, 5] = .TextMatrix(CNSTMVRROWMUNICIPALITY, 0)
			//						xlSheet.Cells[41, 6] = Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0);
			//						xlSheet.Cells[50, 3] = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			//						if (Strings.Trim(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALELDERLYTAXCREDITYESNO, 0))!=string.Empty) {
			//							if (FCConvert.CBool(Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALELDERLYTAXCREDITYESNO, 0))) {
			//								xlSheet.Cells[37, 4] = "YES";
			//								xlSheet.Cells[37, 9] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALELDERLYTAXCREDITHOWMANY, 0);
			//								xlSheet.Cells[39, 9] = Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALELDERLYTAXCREDITHOWMUCH, 0);
			//							} else {
			//								xlSheet.Cells[37, 4] = "NO";
			//								xlSheet.Cells[37, 9] = "";
			//								xlSheet.Cells[39, 9] = "";
			//							}
			//						} else {
			//							xlSheet.Cells[37, 4] = "NO";
			//							xlSheet.Cells[37, 9] = "";
			//							xlSheet.Cells[39, 9] = "";
			//						}
			//					}
			//					else if (Strings.UCase(xlSheet.Name)=="PAGE 9")
			//					{
			//						xlSheet.Cells[4, 3] = Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0);
			//						xlSheet.Cells[4, 6] = Grid.TextMatrix(modMVR.CNSTMVRROWCOUNTY, 0);
			//						xlSheet.Cells[12, 3] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNEW, modMVR.CNSTMVRGRIDVALUATIONCOLONEFAMILY));
			//						xlSheet.Cells[12, 4] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNEW, modMVR.CNSTMVRGRIDVALUATIONCOLTWOFAMILY));
			//						xlSheet.Cells[12, 5] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNEW, modMVR.CNSTMVRGRIDVALUATIONCOLTHREEFAMILY));
			//						xlSheet.Cells[12, 6] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNEW, modMVR.CNSTMVRGRIDVALUATIONCOLOVERFOUR));
			//						xlSheet.Cells[12, 7] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNEW, modMVR.CNSTMVRGRIDVALUATIONCOLMOBILEHOMES));
			//						xlSheet.Cells[12, 8] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNEW, modMVR.CNSTMVRGRIDVALUATIONCOLSEASONALHOMES));
			//						xlSheet.Cells[13, 3] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWDEMOLISHED, modMVR.CNSTMVRGRIDVALUATIONCOLONEFAMILY));
			//						xlSheet.Cells[13, 4] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWDEMOLISHED, modMVR.CNSTMVRGRIDVALUATIONCOLTWOFAMILY));
			//						xlSheet.Cells[13, 5] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWDEMOLISHED, modMVR.CNSTMVRGRIDVALUATIONCOLTHREEFAMILY));
			//						xlSheet.Cells[13, 6] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWDEMOLISHED, modMVR.CNSTMVRGRIDVALUATIONCOLOVERFOUR));
			//						xlSheet.Cells[13, 7] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWDEMOLISHED, modMVR.CNSTMVRGRIDVALUATIONCOLMOBILEHOMES));
			//						xlSheet.Cells[13, 8] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWDEMOLISHED, modMVR.CNSTMVRGRIDVALUATIONCOLSEASONALHOMES));
			//						xlSheet.Cells[14, 3] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWCONVERTED, modMVR.CNSTMVRGRIDVALUATIONCOLONEFAMILY));
			//						xlSheet.Cells[14, 4] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWCONVERTED, modMVR.CNSTMVRGRIDVALUATIONCOLTWOFAMILY));
			//						xlSheet.Cells[14, 5] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWCONVERTED, modMVR.CNSTMVRGRIDVALUATIONCOLTHREEFAMILY));
			//						xlSheet.Cells[14, 6] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWCONVERTED, modMVR.CNSTMVRGRIDVALUATIONCOLOVERFOUR));
			//						xlSheet.Cells[14, 7] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWCONVERTED, modMVR.CNSTMVRGRIDVALUATIONCOLMOBILEHOMES));
			//						xlSheet.Cells[14, 8] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWCONVERTED, modMVR.CNSTMVRGRIDVALUATIONCOLSEASONALHOMES));
			//						xlSheet.Cells[16, 3] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWINCREASE, modMVR.CNSTMVRGRIDVALUATIONCOLONEFAMILY));
			//						xlSheet.Cells[16, 4] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWINCREASE, modMVR.CNSTMVRGRIDVALUATIONCOLTWOFAMILY));
			//						xlSheet.Cells[16, 5] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWINCREASE, modMVR.CNSTMVRGRIDVALUATIONCOLTHREEFAMILY));
			//						xlSheet.Cells[16, 6] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWINCREASE, modMVR.CNSTMVRGRIDVALUATIONCOLOVERFOUR));
			//						xlSheet.Cells[16, 7] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWINCREASE, modMVR.CNSTMVRGRIDVALUATIONCOLMOBILEHOMES));
			//						xlSheet.Cells[16, 8] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWINCREASE, modMVR.CNSTMVRGRIDVALUATIONCOLSEASONALHOMES));
			//						xlSheet.Cells[17, 3] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWLOSS, modMVR.CNSTMVRGRIDVALUATIONCOLONEFAMILY));
			//						xlSheet.Cells[17, 4] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWLOSS, modMVR.CNSTMVRGRIDVALUATIONCOLTWOFAMILY));
			//						xlSheet.Cells[17, 5] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWLOSS, modMVR.CNSTMVRGRIDVALUATIONCOLTHREEFAMILY));
			//						xlSheet.Cells[17, 6] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWLOSS, modMVR.CNSTMVRGRIDVALUATIONCOLOVERFOUR));
			//						xlSheet.Cells[17, 7] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWLOSS, modMVR.CNSTMVRGRIDVALUATIONCOLMOBILEHOMES));
			//						xlSheet.Cells[17, 8] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWLOSS, modMVR.CNSTMVRGRIDVALUATIONCOLSEASONALHOMES));
			//						xlSheet.Cells[19, 3] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNET, modMVR.CNSTMVRGRIDVALUATIONCOLONEFAMILY));
			//						xlSheet.Cells[19, 4] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNET, modMVR.CNSTMVRGRIDVALUATIONCOLTWOFAMILY));
			//						xlSheet.Cells[19, 5] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNET, modMVR.CNSTMVRGRIDVALUATIONCOLTHREEFAMILY));
			//						xlSheet.Cells[19, 6] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNET, modMVR.CNSTMVRGRIDVALUATIONCOLOVERFOUR));
			//						xlSheet.Cells[19, 7] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNET, modMVR.CNSTMVRGRIDVALUATIONCOLMOBILEHOMES));
			//						xlSheet.Cells[19, 8] = Conversion.Val(GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNET, modMVR.CNSTMVRGRIDVALUATIONCOLSEASONALHOMES));
			//						xlSheet.Cells[24, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION2A, 0);
			//						xlSheet.Cells[25, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION2B, 0);
			//						xlSheet.Cells[26, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION2C, 0);
			//						xlSheet.Cells[27, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION2D, 0);
			//						xlSheet.Cells[28, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION2E, 0);
			//						xlSheet.Cells[29, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION2F, 0);
			//						xlSheet.Cells[30, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION2G, 0);
			//						xlSheet.Cells[31, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION2H, 0);
			//						xlSheet.Cells[36, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION3A, 0);
			//						xlSheet.Cells[37, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION3B, 0);
			//						xlSheet.Cells[38, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION3C, 0);
			//						xlSheet.Cells[39, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION3D, 0);
			//						xlSheet.Cells[40, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION3E, 0);
			//						xlSheet.Cells[41, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION3F, 0);
			//						xlSheet.Cells[42, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION3G, 0);
			//						xlSheet.Cells[43, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION3H, 0);
			//						xlSheet.Cells[48, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION4A, 0);
			//						xlSheet.Cells[49, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION4B, 0);
			//						xlSheet.Cells[50, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION4C, 0);
			//						xlSheet.Cells[51, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION4D, 0);
			//						xlSheet.Cells[52, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION4E, 0);
			//						xlSheet.Cells[53, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION4F, 0);
			//						xlSheet.Cells[54, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION4G, 0);
			//						xlSheet.Cells[55, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION4H, 0);
			//						xlSheet.Cells[56, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION4I, 0);
			//						xlSheet.Cells[57, 2] = Grid.TextMatrix(modMVR.CNSTMVRVALUATIONQUESTION4J, 0);
			//					}
			//					else if (Strings.UCase(xlSheet.Name)=="PAGE 10")
			//					{
			//						xlSheet.Cells[7, 8] = Strings.Format(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTRE, 0)), "#,###,###,##0");
			//						lngREValuation = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTRE, 0));
			//						lngPPValuation = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTPP, 0));
			//						xlSheet.Cells[9, 8] = Strings.Format(lngPPValuation, "#,###,###,##0");
			//						xlSheet.Cells[12, 10] = Strings.Format(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTREPP, 0)), "#,###,###,##0");
			//						xlSheet.Cells[14, 8] = Strings.Format(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEGRANTED, 0)), "#,###,###,##0");
			//						lngHalfHomestead = FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEGRANTED, 0))/2);
			//						xlSheet.Cells[16, 8] = Strings.Format(lngHalfHomestead, "#,###,###,##0");
			//						double lngBETE = 0;
			//						double lngtotbete = 0;
			//						lngtotbete = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRTOTBETE, 0));
			//						xlSheet.Cells[18, 8] = Strings.Format(lngtotbete, "#,###,###,##0");
			//						lngBETE = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRBETEReimbursementValue, 0));
			//						xlSheet.Cells[20, 8] = Strings.Format(lngBETE, "#,###,###,##0");
			//						dblTotVal = lngREValuation+lngPPValuation+lngHalfHomestead+lngBETE;
			//						xlSheet.Cells[24, 10] = Strings.Format(dblTotVal, "#,###,###,##0");
			//						dblTaxRate = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXRATE, 0));
			//						clsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0) = "+FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), modGlobalVariables.strREDatabase);
			//						// xlSheet.Cells[22, 5] = Format(clsLoad.Fields("fiscalstartdate"), "MM/dd/yy")
			//						// xlSheet.Cells[22, 8] = Format(clsLoad.Fields("fiscalenddate"), "MM/dd/yy")
			//						xlSheet.Cells[28, 8] = Strings.Format(Conversion.Val(clsLoad.Get_Fields("countytax"))), "#,###,###,##0.00");
			//						xlSheet.Cells[30, 8] = Strings.Format(Conversion.Val(clsLoad.Get_Fields("municipalappropriation"))), "#,###,###,##0.00");
			//						xlSheet.Cells[32, 8] = Strings.Format(Conversion.Val(clsLoad.Get_Fields("tif"))), "#,###,###,##0.00");
			//						xlSheet.Cells[34, 8] = Strings.Format(Conversion.Val(clsLoad.Get_Fields("education"))), "#,###,###,##0.00");
			//						dblTotalAppropriations = Conversion.Val(clsLoad.Get_Fields("countytax")))+Conversion.Val(clsLoad.Get_Fields("municipalappropriation")))+Conversion.Val(clsLoad.Get_Fields("tif")))+Conversion.Val(clsLoad.Get_Fields("education")));
			//						xlSheet.Cells[37, 10] = Strings.Format(dblTotalAppropriations, "#,###,###,##0.00");
			//						xlSheet.Cells[41, 8] = Strings.Format(Conversion.Val(clsLoad.Get_Fields("statesharing"))), "#,###,###,##0.00");
			//						xlSheet.Cells[43, 8] = Strings.Format(Conversion.Val(clsLoad.Get_Fields("other"))), "#,###,###,##0.00");
			//						dblTotalRevenue = Conversion.Val(clsLoad.Get_Fields("Statesharing")))+Conversion.Val(clsLoad.Get_Fields("other")));
			//						xlSheet.Cells[47, 10] = Strings.Format(dblTotalRevenue, "#,###,###,##0.00");
			//						dblNetToBeRaised = dblTotalAppropriations-dblTotalRevenue;
			//						xlSheet.Cells[49, 10] = Strings.Format(dblNetToBeRaised, "#,###,###,##0.00");
			//						xlSheet.Cells[51, 3] = Strings.Format(dblNetToBeRaised, "#,###,###,##0.00");
			//						xlSheet.Cells[53, 3] = Strings.Format(dblNetToBeRaised, "#,###,###,##0.00");
			//						xlSheet.Cells[59, 3] = Strings.Format(dblNetToBeRaised, "#,###,###,##0.00");
			//						xlSheet.Cells[65, 5] = Strings.Format(dblNetToBeRaised, "#,###,###,##0.00");
			//						xlSheet.Cells[53, 5] = Strings.Format(dblTotVal, "#,###,###,##0");
			//						xlSheet.Cells[55, 5] = Strings.Format(dblTotVal, "#,###,###,##0");
			//						xlSheet.Cells[57, 3] = Strings.Format(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTREPP, 0)), "#,###,###,##0");
			//						xlSheet.Cells[61, 3] = Strings.Format(lngHalfHomestead, "#,###,###,##0");
			//						xlSheet.Cells[57, 8] = Strings.Format(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXLEVY, 0)), "#,###,###,##0.00");
			//						xlSheet.Cells[57, 5] = dblTaxRate;
			//						xlSheet.Cells[61, 5] = dblTaxRate;
			//						xlSheet.Cells[63, 5] = dblTaxRate;
			//						dblMaxOverLay = FCConvert.ToDouble(Strings.Format(modGlobalRoutines.Round(dblNetToBeRaised*0.05, 2), "#,###,##0.00"));
			//						xlSheet.Cells[59, 8] = Strings.Format(dblMaxOverLay, "#,###,###,##0.00");
			//						dblMaxTax = modGlobalRoutines.Round(dblMaxOverLay+dblNetToBeRaised, 2);
			//						xlSheet.Cells[51, 8] = Strings.Format(dblMaxTax, "#,###,###,##0.00");
			//						xlSheet.Cells[55, 3] = Strings.Format(dblMaxTax, "#,###,###,##0.00");
			//						xlSheet.Cells[53, 8] = modGlobalRoutines.Round(dblNetToBeRaised/dblTotVal, 6);
			//						xlSheet.Cells[55, 8] = modGlobalRoutines.Round(dblMaxTax/dblTotVal, 6);
			//						double dblBETEReim = 0;
			//						dblBETEReim = modGlobalRoutines.Round(dblTaxRate*lngBETE, 2);
			//						dblTotTaxed = Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWTAXLEVY, 0));
			//						dblHomesteadReim = modGlobalRoutines.Round(dblTaxRate*lngHalfHomestead, 2);
			//						xlSheet.Cells[61, 8] = Strings.Format(dblHomesteadReim, "#,###,###,##0.00");
			//						xlSheet.Cells[65, 3] = Strings.Format(dblTotTaxed+dblHomesteadReim+dblBETEReim, "#,###,###,##0.00");
			//						xlSheet.Cells[65, 8] = Strings.Format(dblTotTaxed+dblHomesteadReim+dblBETEReim-dblNetToBeRaised, "#,###,###,##0.00");
			//					}
			//				} // xlSheet
			//				xlWorkbook.Close(true);
			//				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			//				//Application.DoEvents();
			//				//xlSheet = null;
			//				xlWorkbook = null;
			//				xlApp = null;
			//				MessageBox.Show("Excel Report Updated"+"\r\n"+"The file is named "+strExcelDocName+" and is in "+Environment.CurrentDirectory, "File Updated", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			//				return;
			//			}
			//			catch (Exception ex)
			//                {	// ErrorHandler:
			//				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			//				MessageBox.Show("Error Number "+FCConvert.ToString(Information.Err(ex).Number)+"  "+Information.Err(ex).Description+"\r\n"+"In mnuExcelDoc_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			//			}
			//} catch { }
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void mnuLoad_Click()
		{
			LoadGrid();
		}

		private void mnuLoadLast_Click(object sender, System.EventArgs e)
		{
			txtRichText.Visible = false;
			txtInput.Visible = false;
			t2kInput.Visible = false;
			cmbInput.Visible = false;
			LoadGrid();
			if (GridValuation.Visible == true)
			{
				GridValuation.Row = 0;
				GridValuation.Col = 0;
				GridValuation.Visible = false;
			}
			intCurrentPage = 1;
			int x;
			int lngNumPages;
			int lngCurrPage;
			ARViewer21.ReportSource = new rptMVR();
			ARViewer21.ReportSource.Restart();
			ARViewer21.ReportSource.Run(false);
			intMaxPage = ARViewer21.ReportSource.Document.Pages.Count;
			ARViewer21.ReportSource.Document.Pages.Insert(0, ARViewer21.ReportSource.Document.Pages[intCurrentPage - 1]);
			//ARViewer21.ReportSource.Close();
			for (x = 1; x <= ARViewer21.ReportSource.Document.Pages.Count - 1; x++)
			{
				ARViewer21.ReportSource.Document.Pages.Remove(ARViewer21.ReportSource.Document.Pages[1]);
			}
			// x
			//ARViewer21.ReportSource = null;
			//ARViewer21.ReportSource.Document.Pages.Commit();
			//ARViewer21.Refresh();
			//Application.DoEvents();
			//Application.DoEvents();
		}

		private void mnuNextPage_Click(object sender, System.EventArgs e)
		{
			int lngPage;
			// If ARViewer21.TOC.CurrentPage < (ARViewer21.ReportSource.Document.Pages.Count - 1) Then
			// lngPage = ARViewer21.TOC.CurrentPage + 1
			// ARViewer21.TOC.GotoPage (lngPage)
			// End If
			if (intCurrentPage < intMaxPage)
			{
				if (GridValuation.Visible == true)
				{
					GridValuation.Row = 0;
					GridValuation.Col = 0;
					GridValuation.Visible = false;
				}
				intCurrentPage += 1;
				UpdateFromInput();
			}
		}

		private void mnuPreviousPage_Click(object sender, System.EventArgs e)
		{
			int lngPage;
			// If ARViewer21.TOC.CurrentPage > 0 Then
			// lngPage = ARViewer21.TOC.CurrentPage - 1
			// ARViewer21.TOC.GotoPage (lngPage)
			// End If
			if (intCurrentPage > 1)
			{
				if (GridValuation.Visible == true)
				{
					GridValuation.Row = 0;
					GridValuation.Col = 0;
					GridValuation.Visible = false;
				}
				intCurrentPage -= 1;
				UpdateFromInput();
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// rerun the report since there is only one page in the viewer
			// vbPorter upgrade warning: tempRpt As DDActiveReports2.ActiveReport	OnWrite(rptMVR)
			rptMVR tempRpt = new rptMVR();
			tempRpt = new rptMVR();
			tempRpt.PrintReport(true);
			tempRpt.Close();
			tempRpt = null;
			// ARViewer21.ReportSource = rptmvr
			// ARViewer21.ReportSource.Restart
			// ARViewer21.ReportSource.Run (False)
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveGrid();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveGrid())
			{
				this.Unload();
			}
		}

		private void t2kInput_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = 0;
						UpdateTables_2(FCConvert.ToInt32(Conversion.Val(t2kInput.Tag)), t2kInput.Text);
						UpdateFromInput();
						break;
					}
				case Keys.Tab:
					{
						KeyCode = 0;
						UpdateTables_2(FCConvert.ToInt32(Conversion.Val(t2kInput.Tag)), t2kInput.Text);
						UpdateFromInput();
						break;
					}
			}
			//end switch
		}

		private void t2kInput_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (t2kInput.Visible == true)
			{
				UpdateTables_2(FCConvert.ToInt32(Conversion.Val(t2kInput.Tag)), t2kInput.Text);
				UpdateFromInput();
			}
		}

		private void txtRichText_KeyDown(ref Keys KeyCode, ref short Shift)
		{
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = 0;
						UpdateTables_2(FCConvert.ToInt32(Conversion.Val(txtRichText.Tag)), txtRichText.Text);
						UpdateFromInput();
						break;
					}
				case Keys.Tab:
					{
						KeyCode = 0;
						UpdateTables_2(FCConvert.ToInt32(Conversion.Val(txtRichText.Tag)), txtRichText.Text);
						UpdateFromInput();
						break;
					}
			}
			//end switch
		}

		private void txtInput_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int x;
			int lngNumPages;
			int lngCurrPage;
			//lngCurrPage = ARViewer21.TOC.CurrentPage;
			lngNumPages = ARViewer21.ReportSource.Document.Pages.Count;
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						UpdateTables_8(FCConvert.ToInt32(Conversion.Val(txtInput.Tag)), txtInput.Text);
						UpdateFromInput();
						break;
					}
				case Keys.Tab:
					{
						KeyCode = (Keys)0;
						UpdateTables_8(FCConvert.ToInt32(Conversion.Val(txtInput.Tag)), txtInput.Text);
						UpdateFromInput();
						break;
					}
			}
			//end switch
		}

		private void txtInput_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtInput.Visible == true)
			{
				UpdateTables_8(FCConvert.ToInt32(Conversion.Val(txtInput.Tag)), txtInput.Text);
				UpdateFromInput();
			}
		}
		// vbPorter upgrade warning: lngType As int	OnWriteFCConvert.ToDouble(
		private void UpdateTables_2(int lngType, string strInput)
		{
			UpdateTables(ref lngType, strInput);
		}

		private void UpdateTables_8(int lngType, string strInput)
		{
			UpdateTables(ref lngType, strInput);
		}

		private void UpdateTables(ref int lngType, string strInput)
		{
			// takes a code as a parameter.  The code tells where to place the input sent in strInput
			Grid.TextMatrix(lngType, 0, strInput);
		}

		private void UpdateFromInput()
		{
			int x;
			int lngNumPages;
			int lngCurrPage;
			txtRichText.Visible = false;
			txtInput.Visible = false;
			t2kInput.Visible = false;
			cmbInput.Visible = false;
			// gstrMinAccount = txtInput.Text
			// Grid.TextMatrix(Val(txtInput.Tag), 0) = txtInput.Text
			RecalcSections();
			ARViewer21.ReportSource = new rptMVR();
			ARViewer21.ReportSource.Restart();
			ARViewer21.ReportSource.Run(false);
			intMaxPage = ARViewer21.ReportSource.Document.Pages.Count;
			// For x = ARViewer21.ReportSource.Document.Pages.Count - 1 To 0 Step -1
			// Call ARViewer21.ReportSource.Document.Pages.Insert(0, ARViewer21.ReportSource.Pages(x))
			// Next x
			ARViewer21.ReportSource.Document.Pages.Insert(0, ARViewer21.ReportSource.Document.Pages[intCurrentPage - 1]);
			ARViewer21.ReportSource.Close();
			for (x = 1; x <= ARViewer21.ReportSource.Document.Pages.Count - 1; x++)
			{
				ARViewer21.ReportSource.Document.Pages.Remove(ARViewer21.ReportSource.Document.Pages[1]);
			}
			// x
			//ARViewer21.ReportSource = null;
			//  //ARViewer21.ReportSource.Document.Pages.Commit();
			//ARViewer21.Refresh();
			//Application.DoEvents();
			//Application.DoEvents();
			// ARViewer21.TOC.Count
			// ARViewer21.TOC.GotoPage (lngCurrPage)
			// GotoPage (lngCurrPage)
		}

		private void SetupGridValuation()
		{
			GridValuation.TextMatrix(0, modMVR.CNSTMVRGRIDVALUATIONCOLONEFAMILY, "One Family");
			GridValuation.TextMatrix(0, modMVR.CNSTMVRGRIDVALUATIONCOLTWOFAMILY, "Two Family");
			GridValuation.TextMatrix(0, modMVR.CNSTMVRGRIDVALUATIONCOLTHREEFAMILY, "3-4 Family");
			GridValuation.TextMatrix(0, modMVR.CNSTMVRGRIDVALUATIONCOLOVERFOUR, "5 Familiy Plus");
			GridValuation.TextMatrix(0, modMVR.CNSTMVRGRIDVALUATIONCOLMOBILEHOMES, "Mobile Homes");
			GridValuation.TextMatrix(0, modMVR.CNSTMVRGRIDVALUATIONCOLSEASONALHOMES, "Seasonal Homes");
			GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNEW, 0, "New");
			GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWDEMOLISHED, 0, "Demolished");
			GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWCONVERTED, 0, "Converted");
			GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWVALUATION, 0, "");
			GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWINCREASE, 0, "Valuation Increase (+)");
			GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWLOSS, 0, "Valuation Loss (-)");
			GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNET, 0, "Net");
			GridValuation.Cell(FCGrid.CellPropertySettings.flexcpBackColor, modMVR.CNSTMVRGRIDVALUATIONROWVALUATION, 0, modMVR.CNSTMVRGRIDVALUATIONROWVALUATION, GridValuation.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			GridValuation.Cell(FCGrid.CellPropertySettings.flexcpBackColor, modMVR.CNSTMVRGRIDVALUATIONROWNETSPACER, 0, modMVR.CNSTMVRGRIDVALUATIONROWNETSPACER, GridValuation.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
		}
		// vbPorter upgrade warning: lngWidth As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: lngHeight As int	OnWriteFCConvert.ToDouble(
		private void ResizeGridValuation(int lngWidth = 0, int lngHeight = 0)
		{
			int GridWidth = 0;
			// vbPorter upgrade warning: lngRowHeight As int	OnWriteFCConvert.ToDouble(
			int lngRowHeight = 0;
			if (lngWidth > 0 && lngHeight > 0)
			{
				GridValuation.Width = lngWidth;
				GridValuation.Height = lngHeight;
				lngRowHeight = FCConvert.ToInt32(FCConvert.ToDouble(lngHeight) / (GridValuation.Rows - 1));
				GridValuation.Font = new Font(GridValuation.Font.Name, (GridValuation.WidthOriginal / 8655) * 8);
				GridValuation.RowHeightMin = lngRowHeight;
				GridValuation.RowHeightMax = lngRowHeight;
				GridValuation.RowHeight(modMVR.CNSTMVRGRIDVALUATIONROWVALUATION, FCConvert.ToInt32(lngRowHeight / 2.0));
				GridValuation.RowHeight(modMVR.CNSTMVRGRIDVALUATIONROWNETSPACER, FCConvert.ToInt32(lngRowHeight / 2.0));
				GridValuation.RowHeight(0, lngRowHeight);
				GridValuation.RowHeight(modMVR.CNSTMVRGRIDVALUATIONROWNEW, lngRowHeight);
				GridValuation.RowHeight(modMVR.CNSTMVRGRIDVALUATIONROWDEMOLISHED, lngRowHeight);
				GridValuation.RowHeight(modMVR.CNSTMVRGRIDVALUATIONROWCONVERTED, lngRowHeight);
				GridValuation.RowHeight(modMVR.CNSTMVRGRIDVALUATIONROWINCREASE, lngRowHeight);
				GridValuation.RowHeight(modMVR.CNSTMVRGRIDVALUATIONROWLOSS, lngRowHeight);
				GridValuation.RowHeight(modMVR.CNSTMVRGRIDVALUATIONROWNET, lngRowHeight);
			}
			// .RowHeight(CNSTMVRGRIDVALUATIONROWVALUATION) = lngRowHeight / 2
			// .RowHeight(CNSTMVRGRIDVALUATIONROWNETSPACER) = lngRowHeight / 2
			GridWidth = GridValuation.WidthOriginal;
			GridValuation.Font = new Font(GridValuation.Font.Name, (GridValuation.WidthOriginal / 8655) * 8);
			GridValuation.ColWidth(0, FCConvert.ToInt32(0.2 * GridWidth));
			GridValuation.ColWidth(modMVR.CNSTMVRGRIDVALUATIONCOLONEFAMILY, FCConvert.ToInt32(0.126 * GridWidth));
			GridValuation.ColWidth(modMVR.CNSTMVRGRIDVALUATIONCOLTWOFAMILY, FCConvert.ToInt32(0.126 * GridWidth));
			GridValuation.ColWidth(modMVR.CNSTMVRGRIDVALUATIONCOLTHREEFAMILY, FCConvert.ToInt32(0.126 * GridWidth));
			GridValuation.ColWidth(modMVR.CNSTMVRGRIDVALUATIONCOLOVERFOUR, FCConvert.ToInt32(0.131 * GridWidth));
			GridValuation.ColWidth(modMVR.CNSTMVRGRIDVALUATIONCOLMOBILEHOMES, FCConvert.ToInt32(0.131 * GridWidth));
			GridValuation.ColWidth(modMVR.CNSTMVRGRIDVALUATIONCOLSEASONALHOMES, FCConvert.ToInt32(0.16 * GridWidth));
			GridValuation.Height = (GridValuation.Rows) * lngRowHeight + 60;
			// two half-height rows
		}

		private void SetupGrid()
		{
			Grid.Cols = 1;
			Grid.Rows = modMVR.CNSTMVRMAXROWS;
		}

		private void CalculateBETESection()
		{
			if (modGlobalConstants.Statics.gboolPP || modGlobalConstants.Statics.gboolBL)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL = "";
				if (System.IO.File.Exists("twpp0000.vb1"))
				{
					strSQL = "Select count(ppvaluations.valuekey) as thecount,sum(CAT1EXEMPT) AS sUM1,sum(cat2exempt) as sum2,sum(cat3exempt) as sum3,sum(cat4exempt) as sum4,sum(cat5exempt) as sum5,sum(cat6exempt) as sum6,sum(cat7exempt) as sum7,sum(cat8exempt) as sum8,sum(cat9exempt) as sum9 from ppvaluations inner join ppmaster on (ppmaster.account = ppvaluations.valuekey) where not isnull(deleted,0) = 1 and (cat1exempt > 0 or cat2exempt > 0 or cat3exempt > 0 or cat4exempt > 0 or cat5exempt > 0 or cat6exempt > 0 or cat7exempt > 0 or cat8exempt > 0 or cat9exempt > 0)";
					rsLoad.OpenRecordset(strSQL, modGlobalVariables.strPPDatabase);
					if (!rsLoad.EndOfFile())
					{
						Grid.TextMatrix(modMVR.CNSTMVRTOTBETE, 0, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("sum1")) + Conversion.Val(rsLoad.Get_Fields("sum2")) + Conversion.Val(rsLoad.Get_Fields("sum3")) + Conversion.Val(rsLoad.Get_Fields("sum4")) + Conversion.Val(rsLoad.Get_Fields("sum5")) + Conversion.Val(rsLoad.Get_Fields("sum6")) + Conversion.Val(rsLoad.Get_Fields("sum7")) + Conversion.Val(rsLoad.Get_Fields("sum8")) + Conversion.Val(rsLoad.Get_Fields("sum9"))));
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						Grid.TextMatrix(modMVR.CNSTMVRNUMBETE, 0, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("thecount"))));
					}
					else
					{
						Grid.TextMatrix(modMVR.CNSTMVRTOTBETE, 0, FCConvert.ToString(0));
						Grid.TextMatrix(modMVR.CNSTMVRNUMBETE, 0, FCConvert.ToString(0));
						Grid.TextMatrix(modMVR.CNSTMVRBETEReimbursementValue, 0, FCConvert.ToString(0));
					}
				}
			}
		}

		private void CalculateHomesteadReimbursementSection()
		{
			double dblRatio;
			string strHomesteads;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			string strQuery1;
			string strQuery2;
			string strQuery3;
			string strWhere = "";
			string strWhere2 = "";
			string strQuery4;
			int lngNumRegHomesteads = 0;
			int lngNumFullyExemptHomesteads = 0;
			int lngTotExemptions;
			// vbPorter upgrade warning: lngAssessment As int	OnWrite(short, double)
			int lngAssessment;
			// vbPorter upgrade warning: lngTotHomesteadVal As int	OnWrite(short, double)
			int lngTotHomesteadVal;
			// vbPorter upgrade warning: lngValRegHomesteads As int	OnWrite(short, double)
			int lngValRegHomesteads;
			// vbPorter upgrade warning: lngValFullyExemptHomesteads As int	OnWrite(short, double)
			int lngValFullyExemptHomesteads;
			string strWhereMulti;
			strHomesteads = "";
			dblRatio = modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio;
			clsLoad.OpenRecordset("select * from exemptcode where category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATHOMESTEAD), modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				strHomesteads += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + ",";
				clsLoad.MoveNext();
			}
			if (strHomesteads != string.Empty)
			{
				strHomesteads = Strings.Mid(strHomesteads, 1, strHomesteads.Length - 1);
				strHomesteads = "(" + strHomesteads + ")";
			}
			else
			{
				// all zeroes
				Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTFULLGRANTED, 0, FCConvert.ToString(0));
				Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEFULLGRANTED, 0, FCConvert.ToString(0));
				Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTFULLEXEMPTGRANTED, 0, FCConvert.ToString(0));
				Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEFULLEXEMPTGRANTED, 0, FCConvert.ToString(0));
				Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTGRANTED, 0, FCConvert.ToString(0));
				Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEGRANTED, 0, FCConvert.ToString(0));
				// .TextMatrix(CNSTMVRROWLINE14G, 0) = 0
				Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTASSESSED, 0, FCConvert.ToString(0));
				return;
			}
			lngAssessment = 0;
			lngTotHomesteadVal = 0;
			strWhereMulti = "";
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				strWhereMulti = " and ritrancode = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
			}
			strQuery1 = "select rsaccount from master where rscard = 1 and not rsdeleted = 1 and riexemptcd1 in " + strHomesteads;
			strQuery2 = "select rsaccount from master where rscard = 1 and not rsdeleted = 1 and riexemptcd2 in " + strHomesteads + " " + strWhere;
			strQuery3 = "select rsaccount from master where rscard = 1 and not rsdeleted = 1 and riexemptcd3 in " + strHomesteads + " " + strWhere;
			strQuery4 = "select * from ((" + strQuery1 + ") union all (" + strQuery2 + ") union all (" + strQuery3 + ")) tbl1 ";
			strSQL = "Select count (master.rsaccount) as NumHomesteads,master.rsaccount,sum(lastlandval + lastbldgval) as Assessment,sum(homesteadVALUE) as totHomeExempt from master inner join (" + strQuery4 + ") as homesteadquery4 on (homesteadquery4.rsaccount = master.rsaccount) group by master.rsaccount having sum(homesteadvalue) >= " + FCConvert.ToString(Conversion.Int(10000 * dblRatio));
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			lngValRegHomesteads = 0;
			if (!clsLoad.EndOfFile())
			{
				lngNumRegHomesteads = clsLoad.RecordCount();
				while (!clsLoad.EndOfFile())
				{
					lngAssessment += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("assessment")));
					// TODO Get_Fields: Field [tothomeexempt] not found!! (maybe it is an alias?)
					lngTotHomesteadVal += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("tothomeexempt")));
					// TODO Get_Fields: Field [tothomeexempt] not found!! (maybe it is an alias?)
					lngValRegHomesteads += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("tothomeexempt")));
					clsLoad.MoveNext();
				}
			}
			else
			{
				lngNumRegHomesteads = 0;
				lngValRegHomesteads = 0;
			}
			// strSQL = "Select count (master.rsaccount) as NumHomesteads,master.rsaccount,sum(lastlandval + lastbldgval) as Assessment,sum(homesteadVALUE) as totHomeExempt from master inner join homesteadquery4 on (homesteadquery4.rsaccount = master.rsaccount) group by master.rsaccount having sum(lastlandval + lastbldgval) < " & Int(10000 * dblRatio)
			// strSQL = "Select count (master.rsaccount) as NumHomesteads,master.rsaccount,sum(lastlandval + lastbldgval) as Assessment,sum(homesteadVALUE) as totHomeExempt from master inner join homesteadquery4 on (homesteadquery4.rsaccount = master.rsaccount) group by master.rsaccount having  sum(homesteadvalue) < " & Int(10000 * dblRatio)
			strSQL = "Select count (master.rsaccount) as NumHomesteads,master.rsaccount,sum(lastlandval + lastbldgval) as Assessment,sum(homesteadVALUE) as totHomeExempt from master inner join (" + strQuery4 + ") as homesteadquery4 on (homesteadquery4.rsaccount = master.rsaccount) group by master.rsaccount having  sum(homesteadvalue) < " + FCConvert.ToString(Conversion.Int(10000 * dblRatio));
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			lngValFullyExemptHomesteads = 0;
			if (!clsLoad.EndOfFile())
			{
				lngNumFullyExemptHomesteads = clsLoad.RecordCount();
				while (!clsLoad.EndOfFile())
				{
					lngAssessment += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("assessment")));
					// TODO Get_Fields: Field [tothomeexempt] not found!! (maybe it is an alias?)
					lngTotHomesteadVal += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("tothomeexempt")));
					// TODO Get_Fields: Field [tothomeexempt] not found!! (maybe it is an alias?)
					lngValFullyExemptHomesteads += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("tothomeexempt")));
					clsLoad.MoveNext();
				}
			}
			else
			{
				lngNumFullyExemptHomesteads = 0;
			}

			Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTFULLGRANTED, 0, FCConvert.ToString(lngNumRegHomesteads));
			Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEFULLGRANTED, 0, FCConvert.ToString(lngValRegHomesteads));
			Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTFULLEXEMPTGRANTED, 0, FCConvert.ToString(lngNumFullyExemptHomesteads));
			Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEFULLEXEMPTGRANTED, 0, FCConvert.ToString(lngValFullyExemptHomesteads));
			Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTGRANTED, 0, FCConvert.ToString(lngNumRegHomesteads + lngNumFullyExemptHomesteads));
			Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTVALUEGRANTED, 0, FCConvert.ToString(lngTotHomesteadVal));
			Grid.TextMatrix(modMVR.CNSTMVRROWHOMESTEADREIMTOTASSESSED, 0, FCConvert.ToString(lngAssessment));
		}

		private void RecalcSections()
		{
			// CalculateMVSection
			RetotalExempts();
		}

		private void CalculateWoodLand()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			double dblSoft;
			double dblHard;
			double dblMixed;
			int x;
			string strTemp = "";
			double dblAcres;
			int lngTotalParcels;
			bool boolHadSome = false;
			bool boolHadSomeThisTime = false;
			// vbPorter upgrade warning: lngValue As int	OnWrite(short, double)
			int lngValue;
			// vbPorter upgrade warning: lngTillable As int	OnWrite(short, double)
			int lngTillable;
			double dblTillableAcres;
			int lngCount;
			int lngAcct;
			bool boolOKToUse = false;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strWhereMulti;
			try
			{
				// On Error GoTo ErrorHandler
				strWhereMulti = "";
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					strWhereMulti = " and val(ritrancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				modGlobalVariables.Statics.LandTypes.Init();
				// Jim P requested open code not be allowed to be 0
				// Says many people will not have open space set up and it will default to 0, causing the figures
				// to be off
				if (modGlobalVariables.Statics.CustomizedInfo.OpenCode == 0)
					modGlobalVariables.Statics.CustomizedInfo.OpenCode = 10;
				strSQL = "select count(rsaccount) as thecount from master where not rsdeleted = 1 " + strWhereMulti + " and (isnull(piland1type ,0) > 0 or isnull(piland2type,0) > 0 or isnull(piland3type,0) > 0 or isnull(piland4type,0) > 0 or isnull(piland5type,0) > 0 or isnull(piland6type ,0) > 0 or isnull(piland7type ,0) > 0) group by rsaccount";
				lngTotalParcels = 0;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					lngTotalParcels = clsLoad.RecordCount();
				}
				Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSLANDPARCELS, 0, FCConvert.ToString(lngTotalParcels));
				
				clsTemp.OpenRecordset("select * from exemptcode order by code", modGlobalVariables.strREDatabase);
				strSQL = "select rsaccount,rssoft,rshard,rsmixed,piacres,riexemptcd1,riexemptcd2,riexemptcd3,piland1unitsa,piland1unitsb,piland1infcode,piland1type";
				strSQL += ",piland2unitsa,piland2unitsb,piland2infcode,piland2type,piland3unitsa,piland3unitsb,piland3infcode,piland3type,piland4unitsa,piland4unitsb,piland4infcode,piland4type";
				strSQL += ",piland5unitsa,piland5unitsb,piland5infcode,piland5type,pilanD6unitsa,piland6unitsb,piland6infcode,piland6type";
				// strSQL = strSQL & ",piland7unitsa,piland7unitsb,piland7infcode,piland7type from master where not rsdeleted = 1 and (rssoft > 0 or rshard > 0 or rsmixed > 0)"
				strSQL += ",piland7unitsa,piland7unitsb,piland7infcode,piland7type from master where not rsdeleted = 1 " + strWhereMulti + " order by rsaccount ";
				dblAcres = 0;
				dblSoft = 0;
				dblMixed = 0;
				dblHard = 0;
				lngTotalParcels = 0;
				int lngLastAccount;
				lngLastAccount = 0;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					boolOKToUse = true;
					for (x = 1; x <= 3; x++)
					{
						// TODO Get_Fields: Field [riexemptcd] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsLoad.Get_Fields("riexemptcd" + FCConvert.ToString(x))) > 0)
						{
							// If clsTemp.FindFirstRecord("code", clsLoad.Fields("riexemptcd" & x)) Then
							// TODO Get_Fields: Field [riexemptcd] not found!! (maybe it is an alias?)
							if (clsTemp.FindFirst("code = " + clsLoad.Get_Fields("riexemptcd" + FCConvert.ToString(x))))
							{
								// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
								if (FCConvert.ToInt32(clsTemp.Get_Fields("amount")) == 0)
									boolOKToUse = false;
							}
						}
					}
					// x
					if (boolOKToUse)
					{
						// isn't totally exempt
						dblAcres += Conversion.Val(clsLoad.Get_Fields_Double("piacres"));
						boolHadSome = false;
						for (x = 1; x <= 7; x++)
						{
							// TODO Get_Fields: Field [PIland] not found!! (maybe it is an alias?)
							if (Conversion.Val(clsLoad.Get_Fields("PIland" + FCConvert.ToString(x) + "infcode")) != modGlobalVariables.Statics.CustomizedInfo.OpenCode)
							{
								if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("PILand" + FCConvert.ToString(x) + "Type")))))
								{
									if (modGlobalVariables.Statics.LandTypes.IsSoftWood() && !modGlobalVariables.Statics.LandTypes.IsFarmTreeGrowth())
									{
										// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
										// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
										strTemp = Strings.Trim(clsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa") + clsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb"));
										dblSoft += Conversion.Val(strTemp) / 100;
										boolHadSome = true;
									}
									else if (modGlobalVariables.Statics.LandTypes.IsHardWood() && !modGlobalVariables.Statics.LandTypes.IsFarmTreeGrowth())
									{
										// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
										// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
										strTemp = Strings.Trim(clsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa") + clsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb"));
										dblHard += Conversion.Val(strTemp) / 100;
										boolHadSome = true;
									}
									else if (modGlobalVariables.Statics.LandTypes.IsMixedWood() && !modGlobalVariables.Statics.LandTypes.IsFarmTreeGrowth())
									{
										// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
										// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
										strTemp = Strings.Trim(clsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "unitsa") + clsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "unitsb"));
										dblMixed += Conversion.Val(strTemp) / 100;
										boolHadSome = true;
									}
								}
							}
						}
						// x
						if (boolHadSome)
						{
							if (lngLastAccount != FCConvert.ToInt32(clsLoad.Get_Fields_Int32("rsaccount")))
							{
								lngTotalParcels += 1;
							}
							lngLastAccount = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("rsaccount"));
						}
					}
					clsLoad.MoveNext();
				}
				Grid.TextMatrix(modMVR.CNSTMVRMUNICIPALRECSTAXABLEACREAGE, 0, FCConvert.ToString(dblAcres));
				Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHNUMPARCELS, 0, FCConvert.ToString(lngTotalParcels));
				Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHSOFTACREAGE, 0, FCConvert.ToString(dblSoft));
				Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHMIXEDACREAGE, 0, FCConvert.ToString(dblMixed));
				Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHHARDACREAGE, 0, FCConvert.ToString(dblHard));
				Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHTOTACREAGE, 0, FCConvert.ToString(dblSoft + dblMixed + dblHard));
				// **********
				// open space
				strSQL = "select * from master where not rsdeleted = 1 " + strWhereMulti + " order by rsaccount,rscard";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				strSQL = "select * from summrecord order by SRECORDNUMBER,cardnumber";
				clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				lngCount = 0;
				lngLastAccount = 0;
				dblAcres = 0;
				lngValue = 0;
				double dblWaterfrontAcreage;
				double dblWaterfrontValue;
				dblWaterfrontAcreage = 0;
				dblWaterfrontValue = 0;
				int lngLastWaterfrontAccount;
				int lngWaterfrontCount;
				lngLastWaterfrontAccount = 0;
				lngWaterfrontCount = 0;
				// vbPorter upgrade warning: aryFields As Variant --> As string
				string[] aryFields = new string[1 + 1];
				aryFields[0] = "sRecordNumber";
				aryFields[1] = "CardNumber";
				string[] aryValues = new string[1 + 1];
				while (!clsLoad.EndOfFile())
				{
					boolHadSome = false;
					for (x = 1; x <= 7; x++)
					{
						boolHadSomeThisTime = false;
						// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "infcode")) == modGlobalVariables.Statics.CustomizedInfo.OpenCode)
						{
							boolHadSome = true;
							boolHadSomeThisTime = true;
						}
						else
						{
							// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
							if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "type")))))
							{
								if (modGlobalVariables.Statics.LandTypes.Category == modREConstants.CNSTLANDCATOPENSPACE)
								{
									boolHadSome = true;
									boolHadSomeThisTime = true;
								}
							}
						}
						if (!clsTemp.EndOfFile() && boolHadSomeThisTime)
						{
							aryValues[0] = FCConvert.ToString(clsLoad.Get_Fields_Int32("rsaccount"));
							aryValues[1] = FCConvert.ToString(clsLoad.Get_Fields_Int32("rscard"));
							Array tempFields = aryFields as Array;
							Array tempValues = aryValues as Array;
							if (clsTemp.Get_Fields_Int32("srecordnumber") == clsLoad.Get_Fields_Int32("rsaccount") && clsTemp.Get_Fields_Int32("cardnumber") == clsLoad.Get_Fields_Int32("rscard"))
							{
								// TODO Get_Fields: Field [slanda] not found!! (maybe it is an alias?)
								dblAcres += Conversion.Val(clsTemp.Get_Fields("slanda" + FCConvert.ToString(x)));
								// TODO Get_Fields: Field [slandv] not found!! (maybe it is an alias?)
								lngValue += FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("slandv" + FCConvert.ToString(x))));
								// ElseIf clsTemp.FindNextRecord(, , "srecordnumber = " & clsLoad.Fields("rsaccount") & " and cardnumber = " & clsLoad.Fields("rscard")) Then
							}
							else if (clsTemp.FindNextRecord2(tempFields, tempValues))
							{
								// TODO Get_Fields: Field [slanda] not found!! (maybe it is an alias?)
								dblAcres += Conversion.Val(clsTemp.Get_Fields("slanda" + FCConvert.ToString(x)));
								// TODO Get_Fields: Field [slandv] not found!! (maybe it is an alias?)
								lngValue += FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields("slandv" + FCConvert.ToString(x))));
							}
							aryFields = tempFields as string[];
							aryValues = tempValues as string[];
						}
					}
					// x
					if (boolHadSome && lngLastAccount != FCConvert.ToInt32(clsLoad.Get_Fields_Int32("rsaccount")))
					{
						lngLastAccount = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("rsaccount"));
						lngCount += 1;
					}
					clsLoad.MoveNext();
				}
				Grid.TextMatrix(modMVR.CNSTMVROPENPARCELS, 0, FCConvert.ToString(lngCount));
				
				Grid.TextMatrix(modMVR.CNSTMVROPENTOTACREAGECLASSIFIED, 0, FCConvert.ToString(dblAcres));
				Grid.TextMatrix(modMVR.CNSTMVROPENTOTVALCLASSIFIED, 0, FCConvert.ToString(lngValue));
				// FARM
				strSQL = "select * from master inner join summrecord on (master.rsaccount = summrecord.srecordnumber) and (master.rscard = summrecord.cardnumber) where not rsdeleted = 1 " + strWhereMulti + " order by rsaccount,rscard";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				dblAcres = 0;
				lngValue = 0;
				// vbPorter upgrade warning: lngTGValue As int	OnWrite(short, double)
				int lngTGValue;
				lngTGValue = 0;
				dblSoft = 0;
				dblHard = 0;
				dblMixed = 0;
				dblTillableAcres = 0;
				lngTillable = 0;
				lngCount = 0;
				lngAcct = 0;
				while (!clsLoad.EndOfFile())
				{
					for (x = 1; x <= 7; x++)
					{
						// TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsLoad.Get_Fields("piland" + FCConvert.ToString(x) + "infcode")) != modGlobalVariables.Statics.CustomizedInfo.OpenCode)
						{
							// TODO Get_Fields: Field [slandc] not found!! (maybe it is an alias?)
							if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("slandc" + FCConvert.ToString(x))))))
							{
								if (modGlobalVariables.Statics.LandTypes.IsTillable())
								{
									// TODO Get_Fields: Field [slanda] not found!! (maybe it is an alias?)
									dblTillableAcres += Conversion.Val(clsLoad.Get_Fields("slanda" + FCConvert.ToString(x)));
									// TODO Get_Fields: Field [slandv] not found!! (maybe it is an alias?)
									lngTillable += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("slandv" + FCConvert.ToString(x))));
									if (lngAcct != Conversion.Val(clsLoad.Get_Fields_Int32("rsaccount")))
									{
										lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("rsaccount"))));
										lngCount += 1;
									}
								}
								else if (modGlobalVariables.Statics.LandTypes.IsFarmTreeGrowth())
								{
									// TODO Get_Fields: Field [slanda] not found!! (maybe it is an alias?)
									dblAcres += Conversion.Val(clsLoad.Get_Fields("slanda" + FCConvert.ToString(x)));
									// TODO Get_Fields: Field [slandv] not found!! (maybe it is an alias?)
									lngValue += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("slandv" + FCConvert.ToString(x))));
									switch (modGlobalVariables.Statics.LandTypes.Category)
									{
										case modREConstants.CNSTLANDCATHARDWOODFARM:
											{
												// TODO Get_Fields: Field [slanda] not found!! (maybe it is an alias?)
												dblHard += Conversion.Val(clsLoad.Get_Fields("slanda" + FCConvert.ToString(x)));
												break;
											}
										case modREConstants.CNSTLANDCATSOFTWOODFARM:
											{
												// TODO Get_Fields: Field [slanda] not found!! (maybe it is an alias?)
												dblSoft += Conversion.Val(clsLoad.Get_Fields("slanda" + FCConvert.ToString(x)));
												break;
											}
										case modREConstants.CNSTLANDCATMIXEDWOODFARM:
											{
												// TODO Get_Fields: Field [slanda] not found!! (maybe it is an alias?)
												dblMixed += Conversion.Val(clsLoad.Get_Fields("slanda" + FCConvert.ToString(x)));
												break;
											}
									}
									//end switch
									if (lngAcct != Conversion.Val(clsLoad.Get_Fields_Int32("rsaccount")))
									{
										lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("rsaccount"))));
										lngCount += 1;
									}
								}
								else if (modGlobalVariables.Statics.LandTypes.IsPlainTreeGrowth())
								{
									// TODO Get_Fields: Field [slandv] not found!! (maybe it is an alias?)
									lngTGValue += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("slandv" + FCConvert.ToString(x))));
								}
								else if (modGlobalVariables.Statics.LandTypes.IsWaterfront())
								{
									// TODO Get_Fields: Field [slandv] not found!! (maybe it is an alias?)
									dblWaterfrontValue += Conversion.Val(clsLoad.Get_Fields("slandv" + FCConvert.ToString(x)));
									// TODO Get_Fields: Field [slanda] not found!! (maybe it is an alias?)
									dblWaterfrontAcreage += Conversion.Val(clsLoad.Get_Fields("slanda" + FCConvert.ToString(x)));
									if (lngLastWaterfrontAccount != Conversion.Val(clsLoad.Get_Fields_Int32("rsaccount")))
									{
										lngLastWaterfrontAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("rsaccount"))));
										lngWaterfrontCount += 1;
									}
								}
							}
						}
					}
					// x
					clsLoad.MoveNext();
				}
				Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHTOTASSESSED, 0, FCConvert.ToString(lngTGValue));
				Grid.TextMatrix(modMVR.CNSTMVRFARMTOTACREAGETILLABLE, 0, FCConvert.ToString(dblTillableAcres));
				Grid.TextMatrix(modMVR.CNSTMVRFARMTOTACREAGEWOODLAND, 0, FCConvert.ToString(dblAcres));
				Grid.TextMatrix(modMVR.CNSTMVRFARMTOTVALTILLABLE, 0, FCConvert.ToString(lngTillable));
				Grid.TextMatrix(modMVR.CNSTMVRFARMTOTVALWOODLAND, 0, FCConvert.ToString(lngValue));
				Grid.TextMatrix(modMVR.CNSTMVRFARMPARCELSCLASSIFIED, 0, FCConvert.ToString(lngCount));
				Grid.TextMatrix(modMVR.CNSTMVRFARMSOFTACRES, 0, FCConvert.ToString(dblSoft));
				Grid.TextMatrix(modMVR.CNSTMVRFARMMIXEDACRES, 0, FCConvert.ToString(dblMixed));
				Grid.TextMatrix(modMVR.CNSTMVRFARMHARDACRES, 0, FCConvert.ToString(dblHard));
				Grid.TextMatrix(modMVR.CNSTMVRWATERFRONTPARCELS, 0, FCConvert.ToString(lngWaterfrontCount));
				Grid.TextMatrix(modMVR.CNSTMVRWATERFRONTCURRENTACREAGE, 0, FCConvert.ToString(dblWaterfrontAcreage));
				Grid.TextMatrix(modMVR.CNSTMVRWATERFRONTCURRENTVALUATION, 0, FCConvert.ToString(dblWaterfrontValue));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CalculateWoodLand", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CalculateVetExempts()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			double dblRatio = 0;
			string strSQL = "";
			int x;
			double dblTemp;
			// vbPorter upgrade warning: lngTemp As int	OnWrite(short, double)
			int lngTemp = 0;
			int lngTotalExempts = 0;
			int lngTotalValue = 0;
			// vbPorter upgrade warning: lngValue As int	OnWriteFCConvert.ToDouble(
			int lngValue = 0;
			string strWhereMulti;
			try
			{
				// On Error GoTo ErrorHandler
				strWhereMulti = "";
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					strWhereMulti = " and val(ritrancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				dblRatio = modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio;
				lngTotalExempts = 0;
				lngTotalValue = 0;
				// wwi widower
				// lngTemp = 0
				// For x = 1 To 3
				// strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" & x & " = exemptcode.code) where category = " & CNSTEXEMPTCATWWIWIDOWER & " and rscard = 1 and not rsdeleted = 1 and not revocabletrust " & strWhereMulti
				// Call clsLoad.OpenRecordset(strSQL, strREDatabase)
				// If Not clsLoad.EndOfFile Then
				// lngTemp = lngTemp + Val(clsLoad.Fields("thecount"))
				// End If
				// Next x
				// lngTotalExempts = lngTotalExempts + lngTemp
				// .TextMatrix(CNSTMVRVETEXEMPTNUMWWIWIDOWERS, 0) = lngTemp
				// lngValue = lngTemp * 6000 * dblRatio
				// lngTotalValue = lngTotalValue + lngValue
				// .TextMatrix(CNSTMVRVETEXEMPTWWIWIDOWERS, 0) = lngValue
				// wwii widower
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWWIIWIDOWER) + " and rscard = 1 and not rsdeleted = 1 and not revocabletrust = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMWWIIWIDOWERS, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 6000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTWWIIWIDOWERS, 0, FCConvert.ToString(lngValue));
				// WWI vets with revocable trust
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode ON (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where CATEGORY = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWORLDWARI) + " AND rscard = 1 and revocabletrust = 1 and not rsdeleted = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMWWILIVINGTRUST, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 7000 * dblRatio);
				// lngTotalValue = lngTotalValue + lngValue
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTWWILIVINGTRUST, 0, FCConvert.ToString(lngValue));
				// Paraplegic vets with revocable trust
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPARAPLEGICVET) + " and rscard = 1 and revocabletrust = 1 and not rsdeleted = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMPARAPLEGICLIVINGTRUST, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 50000 * dblRatio);
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTPARAPLEGICLIVINGTRUST, 0, FCConvert.ToString(lngValue));
				lngTotalValue += lngValue;
				// all other vets who are the beneficiary of a revocable living trust
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as thecount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPARAPLEGICVET) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWORLDWARI) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATDISABLEDVET) + " and category between " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATKOREAN) + " and " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWORLDWARII) + "  and rscard = 1 and revocabletrust = 1 and not rsdeleted = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMOTHERLIVINGTRUST, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 6000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTOTHERLIVINGTRUST, 0, FCConvert.ToString(lngValue));
				// WWI vets enlisted as a maine resident
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					// strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" & x & " = exemptcode.code) where category = " & CNSTEXEMPTCATWORLDWARI & " and enlisted = " & CNSTEXEMPTENLISTEDMAINE & " and rscard = 1 and not rsdeleted = 1 and not revocabletrust " & strWhereMulti
					strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWORLDWARI) + " and enlisted = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTENLISTEDMAINE) + " and rscard = 1 and not rsdeleted = 1  " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMWWIRESIDENT, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 7000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTWWIRESIDENT, 0, FCConvert.ToString(lngValue));
				// WWI vets not enlisted as a maine resident
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWORLDWARI) + " and not enlisted = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTENLISTEDMAINE) + " and rscard = 1 and not rsdeleted = 1 and not revocabletrust = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMWWINONRESIDENT, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 7000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTWWINONRESIDENT, 0, FCConvert.ToString(lngValue));
				// Viet Nam
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATVIETNAM) + " and rscard = 1 and not rsdeleted = 1 and not revocabletrust = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMVIETNAM, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 6000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTVIETNAM, 0, FCConvert.ToString(lngValue));
				// coop
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATVETCOOPHOUSING) + " and rscard = 1 and not rsdeleted = 1 and not revocabletrust = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMCOOP, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 6000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTCOOP, 0, FCConvert.ToString(lngValue));
				// paraplegic
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPARAPLEGICVET) + " and rscard = 1 and not revocabletrust = 1 and not rsdeleted = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMPARAPLEGIC, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 50000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTPARAPLEGIC, 0, FCConvert.ToString(lngValue));
				// disabled in line of duty
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATDISABLEDVET) + " and rscard = 1 and not rsdeleted = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMVETLINEOFDUTY, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 6000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTVETLINEOFDUTY, 0, FCConvert.ToString(lngValue));
				// Lebanon/panama
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as TheCount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATLEBANONPANAMA) + " and rscard = 1 and not rsdeleted = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMLEBANONPANAMA, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 6000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTLEBANONPANAMA, 0, FCConvert.ToString(lngValue));
				// all other vets who are maine residents
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as thecount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATVETCOOPHOUSING) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPARAPLEGICVET) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWWIWIDOWER) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWWIIWIDOWER) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWORLDWARI) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATDISABLEDVET) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATVIETNAM) + "  and category between " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATKOREAN) + " and " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWORLDWARII) + " and enlisted = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTENLISTEDMAINE) + " and rscard = 1 and not revocabletrust = 1 and not rsdeleted = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTTOTNUMOTHERRESIDENT, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 6000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTOTHERRESIDENT, 0, FCConvert.ToString(lngValue));
				// all other vets who are not enlisted as maine residents
				lngTemp = 0;
				for (x = 1; x <= 3; x++)
				{
					strSQL = "select count(rsaccount) as thecount from master inner join exemptcode on (master.riexemptcd" + FCConvert.ToString(x) + " = exemptcode.code) where not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATVETCOOPHOUSING) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPARAPLEGICVET) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWWIWIDOWER) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWWIIWIDOWER) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWORLDWARI) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATDISABLEDVET) + " and not category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATVIETNAM) + "  and category between " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATKOREAN) + " and " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWORLDWARII) + " and not enlisted = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTENLISTEDMAINE) + " and rscard = 1 and not revocabletrust = 1 and not rsdeleted = 1 " + strWhereMulti;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
						lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					}
				}
				// x
				lngTotalExempts += lngTemp;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTNUMOTHERNONRESIDENT, 0, FCConvert.ToString(lngTemp));
				lngValue = FCConvert.ToInt32(lngTemp * 6000 * dblRatio);
				lngTotalValue += lngValue;
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTOTHERNONRESIDENT, 0, FCConvert.ToString(lngValue));
				// totals
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTTOTNUM, 0, FCConvert.ToString(lngTotalExempts));
				frmMVRViewer.InstancePtr.Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTTOTAL, 0, FCConvert.ToString(lngTotalValue));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CalculateVetExempts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CalculateExemptsForMVR()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			// vbPorter upgrade warning: lngTemp As int	OnWrite(int, double)
			int lngTemp = 0;
			// vbPorter upgrade warning: lngTemp2 As int	OnWrite(short, double)
			int lngTemp2;
			// vbPorter upgrade warning: lngTemp3 As int	OnWrite(short, double)
			int lngTemp3;
			string strWhereMulti;
			try
			{
				// On Error GoTo ErrorHandler
				strWhereMulti = "";
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					strWhereMulti = " and val(ritrancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATUNITEDSTATES) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("exemptsum"))));
				}
				else
				{
					lngTemp = 0;
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATUNITEDSTATES) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATUNITEDSTATES) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTUS, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATSTATE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATSTATE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATSTATE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTME, 0, FCConvert.ToString(lngTemp));
				lngTemp += FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTUS, 0)));
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTUSME, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATNHWATER) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATNHWATER) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATNHWATER) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTNHWATER, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND (exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATMUNICOUNTY) + " or exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATQUASIMUNICIPAL) + ")" + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and (exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATMUNICOUNTY) + " or exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATQUASIMUNICIPAL) + ")" + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and (exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATMUNICOUNTY) + " or exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATQUASIMUNICIPAL) + ")" + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTQUASIMUNI, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWATERPIPES) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWATERPIPES) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATWATERPIPES) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTWATERPIPES, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATHYDROOUTSIDEMUNI) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATHYDROOUTSIDEMUNI) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATHYDROOUTSIDEMUNI) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTHYDROOUTSIDEMUNI, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATMUNIAIRPORT) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATMUNIAIRPORT) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATMUNIAIRPORT) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTPUBLICAIRPORT, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATSEWAGEDISPOSAL) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATSEWAGEDISPOSAL) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATSEWAGEDISPOSAL) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTSEWAGEDISPOSAL, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATBENEVCHARITABLE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATBENEVCHARITABLE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATBENEVCHARITABLE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTBENEVOLENT, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATSCIANDLIT) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATSCIANDLIT) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATSCIANDLIT) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTLITERARY, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATVETERANSORG) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATVETERANSORG) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATVETERANSORG) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTVETORGS, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATCHAMBERCOMMERCE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATCHAMBERCOMMERCE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATCHAMBERCOMMERCE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTCHAMBERCOMMERCE, 0, FCConvert.ToString(lngTemp));
				// parsonages and churches
				lngTemp = 0;
				lngTemp2 = 0;
				lngTemp3 = 0;
				strSQL = "select sum(exemptval1) as exemptsum,count(rsaccount) as thecount,sum(lastlandval + lastbldgval) as assesssum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPARSONAGE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngTemp2 += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					// TODO Get_Fields: Field [assesssum] not found!! (maybe it is an alias?)
					lngTemp3 += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("assesssum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum,count(rsaccount) as thecount,sum(lastlandval + lastbldgval) as assesssum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPARSONAGE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngTemp2 += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					// TODO Get_Fields: Field [assesssum] not found!! (maybe it is an alias?)
					lngTemp3 += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("assesssum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum,count(rsaccount) as thecount,sum(lastlandval + lastbldgval) as assesssum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPARSONAGE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngTemp2 += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("thecount")));
					// TODO Get_Fields: Field [assesssum] not found!! (maybe it is an alias?)
					lngTemp3 += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("assesssum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTEXEMPTPARSONAGES, 0, FCConvert.ToString(lngTemp));
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTNUMPARSONAGES, 0, FCConvert.ToString(lngTemp2));
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTTAXABLEPARSONAGES, 0, FCConvert.ToString(lngTemp3 - lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATRELIGIOUS) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATRELIGIOUS) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATRELIGIOUS) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTEXEMPTCHURCHES, 0, FCConvert.ToString(lngTemp));
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTTOTALRELIGIOUS, 0, FCConvert.ToString(lngTemp + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTEXEMPTPARSONAGES, 0))));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATFRATERNAL) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATFRATERNAL) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATFRATERNAL) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTFRATERNALORGS, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATHOSPITALLEASED) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATHOSPITALLEASED) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATHOSPITALLEASED) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTLEASEDHOSPITAL, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATBLIND) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATBLIND) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATBLIND) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTBLIND, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATANIMALWASTE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATANIMALWASTE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATANIMALWASTE) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTANIMALWASTE, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPRIVATEAIRPORT) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPRIVATEAIRPORT) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPRIVATEAIRPORT) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTPRIVATEAIRPORT, 0, FCConvert.ToString(lngTemp));
				lngTemp = 0;
				strSQL = "select sum(exemptval1) as exemptsum from master inner join exemptcode on (master.riexemptcd1 = exemptcode.code) where not rsdeleted = 1 and master.riexemptcd1 > 0 and rscard = 1 AND exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPOLLUTIONCONTROL) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval2) as exemptsum from master inner join exemptcode on (master.riexemptcd2 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd2 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPOLLUTIONCONTROL) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				strSQL = "select sum(exemptval3) as exemptsum from master inner join exemptcode on (master.riexemptcd3 = exemptcode.code) where not rsdeleted = 1 and rscard = 1 and master.riexemptcd3 > 0 and exemptcode.category = " + FCConvert.ToString(modcalcexemptions.CNSTEXEMPTCATPOLLUTIONCONTROL) + strWhereMulti;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngTemp += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
				}
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTPOLLUTION, 0, FCConvert.ToString(lngTemp));
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERTOTAL, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERVALUE1, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERVALUE2, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERVALUE3, 0))));
				lngTemp = FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTUSME, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTNHWATER, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTQUASIMUNI, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTHYDROOUTSIDEMUNI, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTPUBLICAIRPORT, 0)));
				lngTemp += FCConvert.ToInt32((Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTPRIVATEAIRPORT, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTSEWAGEDISPOSAL, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTBENEVOLENT, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTLITERARY, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTVETORGS, 0))));
				lngTemp += FCConvert.ToInt32((Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTEXEMPTPARSONAGES, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTEXEMPTCHURCHES, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTCHAMBERCOMMERCE, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTFRATERNALORGS, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTLEASEDHOSPITAL, 0))));
				lngTemp += FCConvert.ToInt32((Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTBLIND, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTWATERPIPES, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTANIMALWASTE, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTPOLLUTION, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTTOTAL, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTSNOWGROOMING, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERTOTAL, 0))));
				Grid.TextMatrix(modMVR.CNSTMVREXEMPTTOTALVALUE, 0, FCConvert.ToString(lngTemp));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CalculateExemptsForMVR", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void RetotalExempts()
		{
			// vbPorter upgrade warning: lngTemp As int	OnWrite(short, double)
			int lngTemp;
			lngTemp = 0;
			Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERTOTAL, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERVALUE1, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERVALUE2, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERVALUE3, 0))));
			lngTemp = FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTUSME, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTNHWATER, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTQUASIMUNI, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTHYDROOUTSIDEMUNI, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTPUBLICAIRPORT, 0)));
			lngTemp += FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTPRIVATEAIRPORT, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTSEWAGEDISPOSAL, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTBENEVOLENT, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTLITERARY, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTVETORGS, 0)));
			lngTemp += FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTEXEMPTPARSONAGES, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTEXEMPTCHURCHES, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTCHAMBERCOMMERCE, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTFRATERNALORGS, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTLEASEDHOSPITAL, 0)));
			lngTemp += FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTBLIND, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTWATERPIPES, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTANIMALWASTE, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTPOLLUTION, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRVETEXEMPTTOTAL, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTSNOWGROOMING, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVREXEMPTOTHERTOTAL, 0)));
			Grid.TextMatrix(modMVR.CNSTMVREXEMPTTOTALVALUE, 0, FCConvert.ToString(lngTemp));
		}

		private void CalculateMVSection()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vbPorter upgrade warning: dtStartDate As DateTime	OnWrite(string)
			DateTime dtStartDate;
			// vbPorter upgrade warning: dtEndDate As DateTime	OnWrite(string, DateTime)
			DateTime dtEndDate;
			bool boolCalendar;
			//FileSystemObject fso = new FileSystemObject();
			if (Strings.Trim(Grid.TextMatrix(modMVR.CNSTMVRROWMVEXCISECALENDARFISCAL, 0)) == string.Empty)
			{
				Grid.TextMatrix(modMVR.CNSTMVRROWMVEXCISECALENDARFISCAL, 0, "Calendar");
			}
			if (modGlobalConstants.Statics.gboolMV)
			{
				if (File.Exists("TWMV0000.vb1"))
				{
					if (modGlobalConstants.Statics.gboolBD)
					{
						// check if they are fiscal or calendar
						clsLoad.OpenRecordset("select fiscalstart from budgetary", "twbd0000.vb1");
						if (Conversion.Val(clsLoad.Get_Fields_String("fiscalstart")) == 1)
						{
							dtStartDate = FCConvert.ToDateTime("01/01/" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWYEAR, 0)) - 1));
							dtEndDate = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWYEAR, 0)) - 1));
							Grid.TextMatrix(modMVR.CNSTMVRROWMVEXCISECALENDARFISCAL, 0, "Calendar");
						}
						else
						{
							dtStartDate = FCConvert.ToDateTime(Strings.Format(clsLoad.Get_Fields_String("fiscalstart"), "00") + "/01/" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWYEAR, 0)) - 1));
							dtEndDate = FCConvert.ToDateTime(Strings.Format(dtStartDate.Month, "00") + "/01/" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWYEAR, 0))));
							dtEndDate = DateAndTime.DateAdd("d", -1, dtEndDate);
							Grid.TextMatrix(modMVR.CNSTMVRROWMVEXCISECALENDARFISCAL, 0, "Fiscal");
						}
					}
					else
					{
						if (Strings.UCase(Grid.TextMatrix(modMVR.CNSTMVRROWMVEXCISECALENDARFISCAL, 0)) == "FISCAL YEAR" || Strings.UCase(Grid.TextMatrix(modMVR.CNSTMVRROWMVEXCISECALENDARFISCAL, 0)) == "FISCAL")
						{
							if (Strings.Trim(strFiscalStart) == string.Empty)
							{
								object temp = strFiscalStart;
								if (!frmInput.InstancePtr.Init(ref temp, "First Fiscal Month", "Enter the first month of the fiscal year", 825, false, modGlobalConstants.InputDTypes.idtString))
								{
									temp = "";
									return;
								}
								else
								{
									if (Conversion.Val(temp) > 0 && Conversion.Val(temp) < 13)
									{
									}
									else
									{
										temp = "";
										return;
									}
								}
								strFiscalStart = temp.ToString();
							}
							dtStartDate = FCConvert.ToDateTime(Strings.Format(Conversion.Val(strFiscalStart), "00") + "/01/" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWYEAR, 0)) - 1));
							dtEndDate = FCConvert.ToDateTime(Strings.Format(Conversion.Val(strFiscalStart), "00") + "/01/" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWYEAR, 0))));
							dtEndDate = DateAndTime.DateAdd("d", -1, dtEndDate);
						}
						else
						{
							dtStartDate = FCConvert.ToDateTime("01/01/" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWYEAR, 0)) - 1));
							dtEndDate = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRROWYEAR, 0)) - 1));
						}
					}
					clsLoad.OpenRecordset("select sum (excisetaxcharged - EXcisecreditused) as excisetotal from activitymaster where excisecreditused < excisetaxcharged and excisetaxdate between '" + Strings.Format(dtStartDate, "MM/dd/yyyy") + "' and '" + Strings.Format(dtEndDate, "MM/dd/yyyy") + "'", "twmv0000.vb1");
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [excisetotal] not found!! (maybe it is an alias?)
						Grid.TextMatrix(modMVR.CNSTMVRROWMVEXCISECOLLECTED, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("excisetotal"))));
					}
				}
			}
		}

		private void LoadGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string[] strTemp = null;
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select * from mvrreport where code = " + FCConvert.ToString(modMVR.CNSTMVRROWDATELASTSAVED) + " and townnumber  = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					if (Convert.ToDateTime(clsLoad.Get_Fields_String("datavalue")).Year < DateTime.Today.Year)
					{
						// not this years data
						MessageBox.Show("The MVR has not previously been saved for this year", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				clsLoad.OpenRecordset("select * from mvrreport where code >= 0 and townnumber  = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					if (Conversion.Val(clsLoad.Get_Fields("code")) == modMVR.CNSTMVRVALGRIDROWCONVERTED)
					{
						strTemp = Strings.Split(FCConvert.ToString(clsLoad.Get_Fields_String("datavalue")), ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= Information.UBound(strTemp, 1); x++)
						{
							GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWCONVERTED, x + 1, strTemp[x]);
						}
						// x
					}
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					else if (Conversion.Val(clsLoad.Get_Fields("code")) == modMVR.CNSTMVRVALGRIDROWDEMOLISHED)
					{
						strTemp = Strings.Split(FCConvert.ToString(clsLoad.Get_Fields_String("datavalue")), ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= Information.UBound(strTemp, 1); x++)
						{
							GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWDEMOLISHED, x + 1, strTemp[x]);
						}
						// x
					}
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						else if (Conversion.Val(clsLoad.Get_Fields("code")) == modMVR.CNSTMVRVALGRIDROWINCREASE)
					{
						strTemp = Strings.Split(FCConvert.ToString(clsLoad.Get_Fields_String("datavalue")), ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= Information.UBound(strTemp, 1); x++)
						{
							GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWINCREASE, x + 1, strTemp[x]);
						}
						// x
					}
							// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
							else if (Conversion.Val(clsLoad.Get_Fields("code")) == modMVR.CNSTMVRVALGRIDROWLOSS)
					{
						strTemp = Strings.Split(FCConvert.ToString(clsLoad.Get_Fields_String("datavalue")), ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= Information.UBound(strTemp, 1); x++)
						{
							GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWLOSS, x + 1, strTemp[x]);
						}
						// x
					}
								// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
								else if (Conversion.Val(clsLoad.Get_Fields("code")) == modMVR.CNSTMVRVALGRIDROWNET)
					{
						strTemp = Strings.Split(FCConvert.ToString(clsLoad.Get_Fields_String("datavalue")), ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= Information.UBound(strTemp, 1); x++)
						{
							GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNET, x + 1, strTemp[x]);
						}
						// x
					}
									// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
									else if (Conversion.Val(clsLoad.Get_Fields("code")) == modMVR.CNSTMVRVALGRIDROWNEW)
					{
						strTemp = Strings.Split(FCConvert.ToString(clsLoad.Get_Fields_String("datavalue")), ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= Information.UBound(strTemp, 1); x++)
						{
							GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWNEW, x + 1, strTemp[x]);
						}
						// x
					}
										// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
										else if (Conversion.Val(clsLoad.Get_Fields("code")) == modMVR.CNSTMVRVALGRIDROWVALUATION)
					{
						strTemp = Strings.Split(FCConvert.ToString(clsLoad.Get_Fields_String("datavalue")), ",", -1, CompareConstants.vbTextCompare);
						for (x = 0; x <= Information.UBound(strTemp, 1); x++)
						{
							GridValuation.TextMatrix(modMVR.CNSTMVRGRIDVALUATIONROWVALUATION, x + 1, strTemp[x]);
						}
						// x
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						Grid.TextMatrix(clsLoad.Get_Fields("code"), 0, FCConvert.ToString(clsLoad.Get_Fields_String("datavalue")));
					}
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CalculateIndustrial()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int x;
			double dblSum = 0;
			string[] strAry = null;
			string strWhereMulti;
			string strWherePP;
			strWhereMulti = "";
			strWherePP = "";
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				strWhereMulti = " and val(ritrancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				strWherePP = " and val(trancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
			}
			if (modGlobalConstants.Statics.gboolPP)
			{
				if (modGlobalVariables.Statics.CustomizedInfo.PPIndPropCat > 0)
				{
					clsLoad.OpenRecordset("select sum(Category" + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.PPIndPropCat) + ") as catsum from ppvaluations inner join ppmaster on (ppmaster.account = ppvaluations.valuekey) where not deleted " + strWherePP, modGlobalVariables.strPPDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO Get_Fields: Field [catsum] not found!! (maybe it is an alias?)
						Grid.TextMatrix(modMVR.CNSTMVRINDUSTRIALPP, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("catsum"))));
					}
				}
			}
			int lngSum;
			if (Strings.Trim(modGlobalVariables.Statics.CustomizedInfo.REIndPropBldgCodes) != string.Empty)
			{
				strAry = Strings.Split(modGlobalVariables.Statics.CustomizedInfo.REIndPropBldgCodes, ",", -1, CompareConstants.vbBinaryCompare);
				dblSum = 0;
				for (x = 0; x <= Information.UBound(strAry, 1); x++)
				{
					if (Conversion.Val(strAry[x]) > 0)
					{
						clsLoad.OpenRecordset("select sum(lastlandval + lastbldgval - rlexemption) as cardsum from master where not rsdeleted = 1 and ribldgcode = " + FCConvert.ToString(Conversion.Val(strAry[x])) + strWhereMulti, modGlobalVariables.strREDatabase);
						if (!clsLoad.EndOfFile())
						{
							// TODO Get_Fields: Field [cardsum] not found!! (maybe it is an alias?)
							dblSum += Conversion.Val(clsLoad.Get_Fields("cardsum"));
						}
					}
				}
				// x
				Grid.TextMatrix(modMVR.CNSTMVRINDUSTRIALRE, 0, FCConvert.ToString(dblSum));
			}
			Grid.TextMatrix(modMVR.CNSTMVRINDUSTRIALREPP, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRINDUSTRIALRE, 0)) + Conversion.Val(Grid.TextMatrix(modMVR.CNSTMVRINDUSTRIALPP, 0))));
		}

		private void CalculateTaxableValuation()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngOtherVal = 0;
			// vbPorter upgrade warning: lngMachEq As int	OnWrite(short, double)
			int lngMachEq = 0;
			// vbPorter upgrade warning: lngBusEq As int	OnWrite(short, double)
			int lngBusEq = 0;
			int x;
			string strSQL = "";
			int intCode = 0;
			int lngExemption = 0;
			string strWhereMulti;
			string strWherePP;
			strWhereMulti = "";
			strWherePP = "";
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				strWhereMulti = " and val(ritrancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				strWherePP = " and val(trancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
			}
			if (modGlobalConstants.Statics.gboolPP)
			{
				lngOtherVal = 0;
				lngMachEq = 0;
				lngBusEq = 0;
				lngExemption = 0;
				strSQL = "Select sum(isnull(exemption,0)) as exemptsum,sum(isnull([value] ,0) ) as valsum from ppmaster where not deleted  = 1" + strWherePP;
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strPPDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					lngExemption = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("exemptsum"))));
					// TODO Get_Fields: Field [valsum] not found!! (maybe it is an alias?)
					lngOtherVal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("valsum"))));
					lngOtherVal -= lngExemption;
				}
				for (x = 1; x <= 9; x++)
				{
					strSQL = "Select sum(category" + FCConvert.ToString(x) + ") as catsum from ppvaluations inner join ppmaster on (ppmaster.account = ppvaluations.valuekey) where not deleted = 1 and isnull([value] ,0) > 0 AND not orcode  = 'Y'" + strWherePP;
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strPPDatabase);
					if (!clsLoad.EndOfFile())
					{
						switch (x)
						{
							case 1:
								{
									intCode = modGlobalVariables.Statics.CustomizedInfo.PPCat1TaxValCat;
									break;
								}
							case 2:
								{
									intCode = modGlobalVariables.Statics.CustomizedInfo.PPCat2TaxValCat;
									break;
								}
							case 3:
								{
									intCode = modGlobalVariables.Statics.CustomizedInfo.PPCat3TaxValCat;
									break;
								}
							case 4:
								{
									intCode = modGlobalVariables.Statics.CustomizedInfo.PPCat4TaxValCat;
									break;
								}
							case 5:
								{
									intCode = modGlobalVariables.Statics.CustomizedInfo.PPCat5TaxValCat;
									break;
								}
							case 6:
								{
									intCode = modGlobalVariables.Statics.CustomizedInfo.PPCat6TaxValCat;
									break;
								}
							case 7:
								{
									intCode = modGlobalVariables.Statics.CustomizedInfo.PPCat7TaxValCat;
									break;
								}
							case 8:
								{
									intCode = modGlobalVariables.Statics.CustomizedInfo.PPCat8TaxValCat;
									break;
								}
							case 9:
								{
									intCode = modGlobalVariables.Statics.CustomizedInfo.PPCat9TaxValCat;
									break;
								}
						}
						//end switch
						switch (intCode)
						{
							case CNSTMVRVALPPBUSEQUIP:
								{
									// TODO Get_Fields: Field [catsum] not found!! (maybe it is an alias?)
									lngBusEq += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("catsum")));
									break;
								}
							case CNSTMVRVALPPMACHEQUIP:
								{
									// TODO Get_Fields: Field [catsum] not found!! (maybe it is an alias?)
									lngMachEq += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("catsum")));
									break;
								}
							default:
								{
									break;
								}
						}
						//end switch
					}
				}
				// x
				lngOtherVal += -lngBusEq - lngMachEq;
				Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALFURNFIX, 0, FCConvert.ToString(lngBusEq));
				Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALMACHEQUIP, 0, FCConvert.ToString(lngMachEq));
				Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTOTHERPP, 0, FCConvert.ToString(lngOtherVal));
				Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTPP, 0, FCConvert.ToString(lngBusEq + lngMachEq + lngOtherVal));
			}
		}

		private void FillGrid()
		{
			// load grid then put in calculated fields
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strWhereMulti;
			try
			{
				// On Error GoTo ErrorHandler
				strWhereMulti = "";
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					strWhereMulti = " and val(ritrancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0, modGlobalConstants.Statics.MuniName);
				}
				else
				{
					Grid.TextMatrix(modMVR.CNSTMVRROWMUNICIPALITY, 0, modRegionalTown.GetTownKeyName_2(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber));
				}
				Grid.TextMatrix(modMVR.CNSTMVRDATESIGNED, 0, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				LoadGrid();
				CalculateHomesteadReimbursementSection();
				CalculateBETESection();
				CalculateWoodLand();
				CalculateMVSection();
				CalculateVetExempts();
				CalculateExemptsForMVR();
				CalculateIndustrial();
				CalculateTaxableValuation();
				// .TextMatrix(CNSTMVRROWYEAR, 0) = Year(Date)
				Grid.TextMatrix(modMVR.CNSTMVRROWYEAR, 0, FCConvert.ToString(CNSTCURRENTMVRREPORTYEAR));
				Grid.TextMatrix(modMVR.CNSTMVRROWCERTIFIEDRATIO, 0, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio * 100));
				clsLoad.OpenRecordset("select sum(convert(decimal,LASTLANDVAL)) as landsum,sum(landexempt) as landexemptsum,sum(convert(Decimal,lastbldgval)) as bldgsum,sum(bldgexempt) as bldgexemptsum from master where not rsdeleted = 1 " + strWhereMulti, modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [landexemptsum] not found!! (maybe it is an alias?)
					Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALLAND, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("landsum")) - Conversion.Val(clsLoad.Get_Fields("landexemptsum"))));
					// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [bldgexemptsum] not found!! (maybe it is an alias?)
					Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALBLDG, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("bldgexemptsum"))));
					// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [landexemptsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [bldgexemptsum] not found!! (maybe it is an alias?)
					Grid.TextMatrix(modMVR.CNSTMVRROWTAXABLEVALTOTRE, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("landexemptsum")) - Conversion.Val(clsLoad.Get_Fields("bldgexemptsum"))));
				}
				clsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0)  = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					Grid.TextMatrix(modMVR.CNSTMVRROWTAXRATE, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("taxrate"))));
					Grid.TextMatrix(modMVR.CNSTMVRBETEReimbursementValue, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("betereimbursevalue"))));
				}
				clsLoad.OpenRecordset("select * from DEFAULTS", modGlobalVariables.strREDatabase);
				if (!modGlobalVariables.Statics.CustomizedInfo.boolApplyCertifiedRatioToTreeGrowth)
				{
					Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHHARDPERACRE, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("hardcost"))));
					Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHMIXEDPERACRE, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("mixedcost"))));
					Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHSOFTPERACRE, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("softcost"))));
					Grid.TextMatrix(modMVR.CNSTMVRFARMHARDPERACRE, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("hardcost"))));
					Grid.TextMatrix(modMVR.CNSTMVRFARMMIXEDPERACRE, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("mixedcost"))));
					Grid.TextMatrix(modMVR.CNSTMVRFARMSOFTPERACRE, 0, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("softcost"))));
				}
				else
				{
					Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHHARDPERACRE, 0, Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("hardcost")))) * modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio, "0.00"));
					Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHMIXEDPERACRE, 0, Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("mixedcost")))) * modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio, "0.00"));
					Grid.TextMatrix(modMVR.CNSTMVRTREEGROWTHSOFTPERACRE, 0, Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("softcost")))) * modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio, "0.00"));
					Grid.TextMatrix(modMVR.CNSTMVRFARMHARDPERACRE, 0, Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("hardcost")))) * modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio, "0.00"));
					Grid.TextMatrix(modMVR.CNSTMVRFARMMIXEDPERACRE, 0, Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("mixedcost")))) * modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio, "0.00"));
					Grid.TextMatrix(modMVR.CNSTMVRFARMSOFTPERACRE, 0, Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("softcost")))) * modGlobalVariables.Statics.CustomizedInfo.CertifiedRatio, "0.00"));
				}
				// Call clsLoad.OpenRecordset("select sum(rssoftvalue) as softsum,sum(rshardvalue) as hardsum,sum(rsmixedvalue) as mixedsum from master where not rsdeleted = 1", strREDatabase)
				// If Not clsLoad.EndOfFile Then
				// .TextMatrix(CNSTMVRTREEGROWTHTOTASSESSED, 0) = Val(clsLoad.Fields("softsum")) + Val(clsLoad.Fields("hardsum")) + Val(clsLoad.Fields("mixedsum"))
				// Else
				// .TextMatrix(CNSTMVRTREEGROWTHTOTASSESSED, 0) = 0
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveGrid()
		{
			bool SaveGrid = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			int x;
			int y;
			string[] strTemp = new string[9 + 1];
			try
			{
				// On Error GoTo ErrorHandler
				SaveGrid = false;
				if (GridValuation.Visible)
				{
					GridValuation.Row = 0;
					GridValuation.Col = 0;
					GridValuation.Visible = false;
					UpdateFromInput();
				}
				else if (txtInput.Visible == true)
				{
					UpdateTables_8(FCConvert.ToInt32(Conversion.Val(txtInput.Tag)), txtInput.Text);
					UpdateFromInput();
				}
				else if (t2kInput.Visible == true)
				{
					UpdateTables_2(FCConvert.ToInt32(Conversion.Val(t2kInput.Tag)), t2kInput.Text);
					UpdateFromInput();
				}
				else if (txtRichText.Visible)
				{
					UpdateTables_2(FCConvert.ToInt32(Conversion.Val(txtRichText.Tag)), txtRichText.Text);
					UpdateFromInput();
				}
				clsSave.OpenRecordset("select * from mvrreport where code = " + FCConvert.ToString(modMVR.CNSTMVRROWDATELASTSAVED) + " and townnumber  = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber), modGlobalVariables.strREDatabase);
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
					clsSave.Set_Fields("code", modMVR.CNSTMVRROWDATELASTSAVED);
					clsSave.Set_Fields("townnumber", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				}
				clsSave.Set_Fields("datavalue", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				clsSave.Update();
				for (x = modMVR.CNSTMVRGRIDVALUATIONROWNEW; x <= modMVR.CNSTMVRGRIDVALUATIONROWNET; x++)
				{
					strTemp[x] = "";
					for (y = modMVR.CNSTMVRGRIDVALUATIONCOLONEFAMILY; y <= modMVR.CNSTMVRGRIDVALUATIONCOLSEASONALHOMES; y++)
					{
						strTemp[x] += GridValuation.TextMatrix(x, y) + ",";
					}
					// y
					if (strTemp[x] != string.Empty)
					{
						// get rid of the last comma
						strTemp[x] = Strings.Mid(strTemp[x], 1, strTemp[x].Length - 1);
					}
				}
				// x
				Grid.TextMatrix(modMVR.CNSTMVRVALGRIDROWNEW, 0, strTemp[1]);
				Grid.TextMatrix(modMVR.CNSTMVRVALGRIDROWCONVERTED, 0, strTemp[modMVR.CNSTMVRVALGRIDROWCONVERTED - modMVR.CNSTMVRVALGRIDROWNEW + 1]);
				Grid.TextMatrix(modMVR.CNSTMVRVALGRIDROWDEMOLISHED, 0, strTemp[modMVR.CNSTMVRVALGRIDROWDEMOLISHED - modMVR.CNSTMVRVALGRIDROWNEW + 1]);
				Grid.TextMatrix(modMVR.CNSTMVRVALGRIDROWINCREASE, 0, strTemp[modMVR.CNSTMVRVALGRIDROWINCREASE - modMVR.CNSTMVRVALGRIDROWNEW + 1]);
				Grid.TextMatrix(modMVR.CNSTMVRVALGRIDROWLOSS, 0, strTemp[modMVR.CNSTMVRVALGRIDROWLOSS - modMVR.CNSTMVRVALGRIDROWNEW + 1]);
				Grid.TextMatrix(modMVR.CNSTMVRVALGRIDROWNET, 0, strTemp[modMVR.CNSTMVRVALGRIDROWNET - modMVR.CNSTMVRVALGRIDROWNEW + 1]);
				Grid.TextMatrix(modMVR.CNSTMVRVALGRIDROWVALUATION, 0, strTemp[modMVR.CNSTMVRVALGRIDROWVALUATION - modMVR.CNSTMVRVALGRIDROWNEW + 1]);
				clsSave.OpenRecordset("select * from mvrreport where code >= 0 order by code", modGlobalVariables.strREDatabase);
				for (x = 0; x <= Grid.Rows - 1; x++)
				{
					// If clsSave.FindFirstRecord("code", x) Then
					if (clsSave.FindFirst("code = " + FCConvert.ToString(x)))
					{
						clsSave.Edit();
					}
					else
					{
						clsSave.AddNew();
						clsSave.Set_Fields("code", x);
					}
					clsSave.Set_Fields("datavalue", Grid.TextMatrix(x, 0));
					clsSave.Update();
				}
				// x
				MessageBox.Show("Save successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveGrid = true;
				return SaveGrid;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGrid;
		}
	}
}
