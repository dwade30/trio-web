﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for dlgTaxMessage.
	/// </summary>
	public partial class dlgTaxMessage : BaseForm
	{
		public dlgTaxMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtNotice = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtNotice.AddControlArrayElement(txtNotice_14, 14);
			this.txtNotice.AddControlArrayElement(txtNotice_13, 13);
			this.txtNotice.AddControlArrayElement(txtNotice_12, 12);
			this.txtNotice.AddControlArrayElement(txtNotice_11, 11);
			this.txtNotice.AddControlArrayElement(txtNotice_10, 10);
			this.txtNotice.AddControlArrayElement(txtNotice_9, 9);
			this.txtNotice.AddControlArrayElement(txtNotice_8, 8);
			this.txtNotice.AddControlArrayElement(txtNotice_7, 7);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static dlgTaxMessage InstancePtr
		{
			get
			{
				return (dlgTaxMessage)Sys.GetInstance(typeof(dlgTaxMessage));
			}
		}

		protected dlgTaxMessage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intMaxTextBox;
		bool boolIncTax;
		string strYear;
		bool boolFreeForm;
		bool boolIncExempt;
		bool boolIncTotExempts;
		bool boolUseExisting;
		string strRate;
		int intWhichAccounts;
		string strWhatOrder;
		int lngMaxLines;
		// max lines for the first box
		int lngMaxLines2;
		// max lines for the 2nd box
		// vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
		double dblSizeRatio;
		int intNoticesPerPage;
		bool boolBillingValues;
		bool boolCondenseWhiteSpace;
		// vbPorter upgrade warning: intARorS As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intPerPage As short	OnWriteFCConvert.ToInt32(
		public void Init(bool boolUseFreeForm, bool boolShowEstimatedTax, bool boolShowExempt, bool boolUseExistingNotice, bool boolIncludeTotallyExempts, string strTaxYear, string strTaxRate, ref int intARorS, ref string strOrder, ref int intPerPage, ref bool boolBillingVals, bool boolCondense = false)
		{
			// vbPorter upgrade warning: x As Variant, short --> As int	OnWriteFCConvert.ToInt16(
			int x;
			clsDRWrapper clsLoad = new clsDRWrapper();
			boolCondenseWhiteSpace = boolCondense;
			boolBillingValues = boolBillingVals;
			intNoticesPerPage = intPerPage;
			boolFreeForm = boolUseFreeForm;
			boolIncTax = boolShowEstimatedTax;
			strYear = strTaxYear;
			boolIncExempt = boolShowExempt;
			boolIncTotExempts = boolIncludeTotallyExempts;
			boolUseExisting = boolUseExistingNotice;
			strRate = strTaxRate;
			intWhichAccounts = intARorS;
			strWhatOrder = strOrder;
			switch (intPerPage)
			{
				case 1:
					{
						lngMaxLines = 48;
						break;
					}
				case 2:
					{
						lngMaxLines = 17;
						break;
					}
				case 3:
					{
						lngMaxLines = 7;
						break;
					}
			}
			//end switch
			lngMaxLines2 = 8;
			clsLoad.OpenRecordset("select * from defaults", modGlobalVariables.strREDatabase);
			RichTextBox1.Text = FCConvert.ToString(clsLoad.Get_Fields_String("taxnoticemsg1"));
			txtHeading.Text = FCConvert.ToString(clsLoad.Get_Fields_String("taxnoticetitle"));
			RichTextBox2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("taxnoticemsg2"));
			if (RichTextBox1.Text.Length > 1)
			{
				while (RichTextBox1.GetLineFromChar(RichTextBox1.Text.Length) >= lngMaxLines)
				{
					RichTextBox1.Text = Strings.Mid(RichTextBox1.Text, 1, RichTextBox1.Text.Length - 1);
					if (!(RichTextBox1.Text.Length > 0))
					{
						break;
					}
				}
			}
			if (RichTextBox2.Text.Length > 1)
			{
				while (RichTextBox2.GetLineFromChar(RichTextBox2.Text.Length) >= lngMaxLines2)
				{
					RichTextBox2.Text = Strings.Mid(RichTextBox2.Text, 1, RichTextBox2.Text.Length - 1);
					if (!(RichTextBox2.Text.Length > 0))
					{
						break;
					}
				}
			}
			if (boolFreeForm)
			{
				intMaxTextBox = 14;
				RichTextBox2.Visible = true;
				for (x = 7; x <= 14; x++)
				{
					txtNotice[FCConvert.ToInt16(x)].Visible = false;
				}
				// x
			}
			else
			{
				intMaxTextBox = 6;
				RichTextBox2.Visible = false;
				for (x = 7; x <= 14; x++)
				{
					txtNotice[FCConvert.ToInt16(x)].Enabled = false;
				}
				// x
				txtNotice[7].Text = "";
				txtNotice[10].Text = "";
				txtNotice[13].Text = "";
				txtNotice[14].Text = "";
				txtNotice[8].Text = "      Land     Buildings";
				if (boolIncExempt)
					txtNotice[8].Text = txtNotice[8].Text + "     Exemption";
				txtNotice[9].Text = "00,000,000 0,000,000,000";
				if (boolIncExempt)
					txtNotice[9].Text = txtNotice[9].Text + " 0,000,000,000";
				txtNotice[11].Text = "                Total   00,000,000,000";
				if (boolIncTax)
					txtNotice[12].Text = "Estimated " + strYear + " Tax          000,000.00";
			}
			this.Show();
			//.ShowModeless(this);
		}

		private void dlgTaxMessage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuCancel_Click();
						break;
					}
			}
			//end switch
		}

		private void dlgTaxMessage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//dlgTaxMessage properties;
			//dlgTaxMessage.ScaleWidth	= 9075;
			//dlgTaxMessage.ScaleHeight	= 6960;
			//dlgTaxMessage.LinkTopic	= "Form1";
			//dlgTaxMessage.LockControls	= true;
			//RichTextBox2 properties;
			//RichTextBox2.ScrollBars	= 3;
			//RichTextBox2.RightMargin	= 5040;
			//RichTextBox2.TextRTF	= $"dlgTaxMessage.frx":058A;
			//RichTextBox1 properties;
			//RichTextBox1.ScrollBars	= 3;
			//RichTextBox1.RightMargin	= 10440;
			//RichTextBox1.TextRTF	= $"dlgTaxMessage.frx":060E;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this);
			lngMaxLines = 7;
		}

		private void dlgTaxMessage_Resize(object sender, System.EventArgs e)
		{
			dblSizeRatio = (LineTest.X2 - LineTest.X1) / 1440F;
			// how much has this form been resized
			//RichTextBox1.RightMargin = Conversion.Int(10440*dblSizeRatio); // approx 7.25 inches
			//RichTextBox2.RightMargin = Conversion.Int(5040*dblSizeRatio); // approx 3.5 inches
		}

		private void mnuCancel_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuCancel_Click()
		{
			mnuCancel_Click(mnuCancel, new System.EventArgs());
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			RichTextBox1.Text = "";
			RichTextBox2.Text = "";
		}

		private void mnuDone_Click(object sender, System.EventArgs e)
		{
			StreamReader tsTaxNotice;
			int x;
			string strTemp = "";
			clsDRWrapper clsSave = new clsDRWrapper();
			if (RichTextBox1.Text.Length > 1)
			{
				while (RichTextBox1.GetLineFromChar(RichTextBox1.Text.Length) >= lngMaxLines)
				{
					RichTextBox1.Text = Strings.Mid(RichTextBox1.Text, 1, RichTextBox1.Text.Length - 1);
					if (!(RichTextBox1.Text.Length > 0))
					{
						break;
					}
				}
			}
			if (RichTextBox2.Text.Length > 1)
			{
				while (RichTextBox2.GetLineFromChar(RichTextBox2.Text.Length) >= lngMaxLines2)
				{
					RichTextBox2.Text = Strings.Mid(RichTextBox2.Text, 1, RichTextBox2.Text.Length - 1);
					if (!(RichTextBox2.Text.Length > 0))
					{
						break;
					}
				}
			}
			clsSave.OpenRecordset("select * from defaults", modGlobalVariables.strREDatabase);
			clsSave.Edit();
			clsSave.Set_Fields("taxnoticetitle", txtHeading.Text);
			clsSave.Set_Fields("taxnoticemsg1", RichTextBox1.Text);
			clsSave.Set_Fields("taxnoticemsg2", RichTextBox2.Text);
			clsSave.Update();
			
			this.Unload();
			rptTaxNotice.InstancePtr.Start(ref boolFreeForm, ref boolIncTax, ref boolIncExempt, ref boolIncTotExempts, ref intWhichAccounts, ref strWhatOrder, strYear, strRate, intNoticesPerPage, boolBillingValues, boolCondenseWhiteSpace);
		}

		private void RichTextBox1_KeyDown(object sender, KeyEventArgs e)
		{
			int lngCharPos = 0;
			Wisej.Web.Keys keyCode = e.KeyCode;
			switch (keyCode)
			{
				case Keys.F12:
					{
						mnuDone_Click(new object(), new EventArgs());
						return;
					}
				case Keys.Delete:
					{
						break;
					}
				case Keys.Insert:
					{
						break;
					}
				case Keys.End:
					{
						break;
					}
				case Keys.PageDown:
				case Keys.PageUp:
					{
						break;
					}
				case Keys.Back:
					{
						break;
					}
				case Keys.Left:
				case Keys.Right:
				case Keys.Up:
					{
						break;
					}
				default:
					{
						lngCharPos = RichTextBox1.SelStart + 1;
						if (RichTextBox1.GetLineFromChar(lngCharPos) >= lngMaxLines)
						{
							keyCode = 0;
							return;
						}
						if (keyCode == Keys.Return)
						{
							lngCharPos = RichTextBox1.Text.Length;
							if (RichTextBox1.GetLineFromChar(lngCharPos) >= lngMaxLines - 1)
							{
								keyCode = 0;
								return;
							}
						}
						break;
					}
			}
			//end switch
		}

		private void RichTextBox2_KeyDown(object sender, KeyEventArgs e)
		{
			int lngCharPos = 0;
			Wisej.Web.Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.F12:
					{
						mnuDone_Click(new object(), new EventArgs());
						return;
					}
				case Keys.Delete:
					{
						break;
					}
				case Keys.Insert:
					{
						break;
					}
				case Keys.End:
					{
						break;
					}
				case Keys.PageDown:
				case Keys.PageUp:
					{
						break;
					}
				case Keys.Back:
					{
						break;
					}
				case Keys.Left:
				case Keys.Right:
				case Keys.Up:
					{
						break;
					}
				default:
					{
						lngCharPos = RichTextBox2.SelStart + 1;
						if (RichTextBox2.GetLineFromChar(lngCharPos) >= lngMaxLines2)
						{
							KeyCode = 0;
							return;
						}
						// now must stop from putting the cursor in the middle, hitting enter and forcing lines beyond limit
						if (KeyCode == Keys.Return)
						{
							lngCharPos = RichTextBox2.Text.Length;
							if (RichTextBox2.GetLineFromChar(lngCharPos) >= lngMaxLines2 - 1)
							{
								KeyCode = 0;
								return;
							}
						}
						break;
					}
			}
			//end switch
		}

		private void txtHeading_Enter(object sender, System.EventArgs e)
		{
			txtHeading.SelectionStart = 0;
			txtHeading.SelectionLength = 0;
		}

		private void txtHeading_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// If Len(txtHeading.Text) >= (txtHeading.MaxLength - 1) Then
			// txtNotice(0).SetFocus
			// End If
		}

		private void txtHeading_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				RichTextBox1.Focus();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
