﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmReassignSketch.
	/// </summary>
	public partial class frmReassignSketch : BaseForm
	{
		public frmReassignSketch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReassignSketch InstancePtr
		{
			get
			{
				return (frmReassignSketch)Sys.GetInstance(typeof(frmReassignSketch));
			}
		}

		protected frmReassignSketch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void cmdReAssign_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (Conversion.Val(txtSrcAcct.Text) <= 0)
				{
					MessageBox.Show("Invalid source account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (Conversion.Val(txtSrcCard.Text) <= 0)
				{
					MessageBox.Show("Invalid source card", "Invalid Card", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (Conversion.Val(txtDestAccount.Text) <= 0)
				{
					MessageBox.Show("Invalid destination account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (Conversion.Val(txtDestCard.Text) <= 0)
				{
					MessageBox.Show("Invalid destination card", "Invalid Card", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (Conversion.Val(txtSrcSketch.Text) <= 0)
					txtSrcSketch.Text = FCConvert.ToString(1);
				int lngSrcAcct;
				int intSrcCard;
				int lngDestAcct;
				int intDestCard;
				int intSketchNum;
				string strSrcSketchName;
				string strDestSketchName;
				string strTemp = "";
				int lngSrcID = 0;
				int lngDestID = 0;
                Guid srcIdentifier;
                Guid destIdentifier;
				lngSrcAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtSrcAcct.Text)));
				intSrcCard = FCConvert.ToInt32(Math.Round(Conversion.Val(txtSrcCard.Text)));
				lngDestAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtDestAccount.Text)));
				intDestCard = FCConvert.ToInt32(Math.Round(Conversion.Val(txtDestCard.Text)));
				intSketchNum = FCConvert.ToInt32(Math.Round(Conversion.Val(txtSrcSketch.Text)));
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select id,rsaccount,rscard,cardid from master where rsaccount = " + FCConvert.ToString(lngSrcAcct) + " and rscard = " + FCConvert.ToString(intSrcCard), modGlobalVariables.strREDatabase);
				if (!rsLoad.EndOfFile())
				{
					lngSrcID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
                    srcIdentifier = Guid.Parse(rsLoad.Get_Fields_String("CardID"));
                }
				else
				{
					MessageBox.Show("Account " + FCConvert.ToString(lngSrcAcct) + " Card " + FCConvert.ToString(intSrcCard) + " could not be found");
					return;
				}
				rsLoad.OpenRecordset("select id,rsaccount,rscard ,cardid from master where rsaccount = " + FCConvert.ToString(lngDestAcct) + " and rscard = " + FCConvert.ToString(intDestCard), modGlobalVariables.strREDatabase);
				if (!rsLoad.EndOfFile())
				{
					lngDestID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
                    destIdentifier = Guid.Parse(rsLoad.Get_Fields_String("CardID"));
                }
				else
				{
					MessageBox.Show("Account " + FCConvert.ToString(lngDestAcct) + " Card " + FCConvert.ToString(intDestCard) + " could not be found");
					return;
				}

                StaticSettings.GlobalCommandDispatcher.Send(new MoveSketches(srcIdentifier, destIdentifier));

                MessageBox.Show("Sketch Moved", "Moved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmReassignSketch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmReassignSketch_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}
		// vbPorter upgrade warning: intCard As short	OnWriteFCConvert.ToInt32(
		public void Init(ref int lngAcct, ref int intCard)
		{
			txtSrcAcct.Text = FCConvert.ToString(lngAcct);
			txtSrcCard.Text = FCConvert.ToString(intCard);
			this.Show(FCForm.FormShowEnum.Modal);
		}

		public void mnuExit_Click()
		{
			this.Unload();
		}
	}
}
