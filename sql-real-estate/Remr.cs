﻿//Fecher vbPorter - Version 1.0.0.40
using Wisej.Web;
using fecherFoundation;
using Global;
using System;
using SharedApplication.Enums;
using TWSharedLibrary;

namespace TWRE0000
{
	public class modDataTypes
	{
		public class StaticVariables
		{
			//=========================================================
			public string ans = "";
			// Public MHDB As DAO.Database
			// Public MH As clsDRWrapper
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper LS = new clsDRWrapper();
			public clsDRWrapper LS_AutoInitialized = null;
			public clsDRWrapper LS
			{
				get
				{
					if ( LS_AutoInitialized == null)
					{
						 LS_AutoInitialized = new clsDRWrapper();
					}
					return LS_AutoInitialized;
				}
				set
				{
					 LS_AutoInitialized = value;
				}
			}
			// Public LSDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper MR = new clsDRWrapper();
			public clsDRWrapper MR_AutoInitialized = null;
			public clsDRWrapper MR
			{
				get
				{
					if ( MR_AutoInitialized == null)
					{
						 MR_AutoInitialized = new clsDRWrapper();
					}
					return MR_AutoInitialized;
				}
				set
				{
					 MR_AutoInitialized = value;
				}
			}
			// Public MRDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper SRMR = new clsDRWrapper();
			public clsDRWrapper SRMR_AutoInitialized = null;
			public clsDRWrapper SRMR
			{
				get
				{
					if ( SRMR_AutoInitialized == null)
					{
						 SRMR_AutoInitialized = new clsDRWrapper();
					}
					return SRMR_AutoInitialized;
				}
				set
				{
					 SRMR_AutoInitialized = value;
				}
			}
			// Public SRMRDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper M2 = new clsDRWrapper();
			public clsDRWrapper M2_AutoInitialized = null;
			public clsDRWrapper M2
			{
				get
				{
					if ( M2_AutoInitialized == null)
					{
						 M2_AutoInitialized = new clsDRWrapper();
					}
					return M2_AutoInitialized;
				}
				set
				{
					 M2_AutoInitialized = value;
				}
			}
			// Public M2DB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper MR3 = new clsDRWrapper();
			public clsDRWrapper MR3_AutoInitialized = null;
			public clsDRWrapper MR3
			{
				get
				{
					if ( MR3_AutoInitialized == null)
					{
						 MR3_AutoInitialized = new clsDRWrapper();
					}
					return MR3_AutoInitialized;
				}
				set
				{
					 MR3_AutoInitialized = value;
				}
			}
			// Public MR3DB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper PIC = new clsDRWrapper();
			public clsDRWrapper PIC_AutoInitialized = null;
			public clsDRWrapper PIC
			{
				get
				{
					if ( PIC_AutoInitialized == null)
					{
						 PIC_AutoInitialized = new clsDRWrapper();
					}
					return PIC_AutoInitialized;
				}
				set
				{
					 PIC_AutoInitialized = value;
				}
			}
			// Public PICDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper CMR = new clsDRWrapper();
			public clsDRWrapper CMR_AutoInitialized = null;
			public clsDRWrapper CMR
			{
				get
				{
					if ( CMR_AutoInitialized == null)
					{
						 CMR_AutoInitialized = new clsDRWrapper();
					}
					return CMR_AutoInitialized;
				}
				set
				{
					 CMR_AutoInitialized = value;
				}
			}
			// Public CMRDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper CMR2 = new clsDRWrapper();
			public clsDRWrapper CMR2_AutoInitialized = null;
			public clsDRWrapper CMR2
			{
				get
				{
					if ( CMR2_AutoInitialized == null)
					{
						 CMR2_AutoInitialized = new clsDRWrapper();
					}
					return CMR2_AutoInitialized;
				}
				set
				{
					 CMR2_AutoInitialized = value;
				}
			}
			// Public CMR2DB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper OUT = new clsDRWrapper();
			public clsDRWrapper OUT_AutoInitialized = null;
			public clsDRWrapper OUT
			{
				get
				{
					if ( OUT_AutoInitialized == null)
					{
						 OUT_AutoInitialized = new clsDRWrapper();
					}
					return OUT_AutoInitialized;
				}
				set
				{
					 OUT_AutoInitialized = value;
				}
			}
			// Public OUTDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper DWL = new clsDRWrapper();
			public clsDRWrapper DWL_AutoInitialized = null;
			public clsDRWrapper DWL
			{
				get
				{
					if ( DWL_AutoInitialized == null)
					{
						 DWL_AutoInitialized = new clsDRWrapper();
					}
					return DWL_AutoInitialized;
				}
				set
				{
					 DWL_AutoInitialized = value;
				}
			}
			// Public CMRArrayDB As DAO.Database
			// Public CMRArray(999) As clsDRWrapper
			// Public DWLDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper CC = new clsDRWrapper();
			public clsDRWrapper CC_AutoInitialized = null;
			public clsDRWrapper CC
			{
				get
				{
					if ( CC_AutoInitialized == null)
					{
						 CC_AutoInitialized = new clsDRWrapper();
					}
					return CC_AutoInitialized;
				}
				set
				{
					 CC_AutoInitialized = value;
				}
			}
			// Public CCDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper CR = new clsDRWrapper();
			public clsDRWrapper CR_AutoInitialized = null;
			public clsDRWrapper CR
			{
				get
				{
					if ( CR_AutoInitialized == null)
					{
						 CR_AutoInitialized = new clsDRWrapper();
					}
					return CR_AutoInitialized;
				}
				set
				{
					 CR_AutoInitialized = value;
				}
			}
			// Public LS2DB As DAO.Database
			// Public LS2 As clsDRWrapper
			// Public LSRDB As DAO.Database
			// Public LSR As clsDRWrapper
			// Public CRDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper RSZ = new clsDRWrapper();
			public clsDRWrapper RSZ_AutoInitialized = null;
			public clsDRWrapper RSZ
			{
				get
				{
					if ( RSZ_AutoInitialized == null)
					{
						 RSZ_AutoInitialized = new clsDRWrapper();
					}
					return RSZ_AutoInitialized;
				}
				set
				{
					 RSZ_AutoInitialized = value;
				}
			}
			// Public LTRDB As DAO.Database
			// Public LTR As clsDRWrapper
			// Public VRDB As DAO.Database
			// Public VR As clsDRWrapper
			// Public PMRDB As DAO.Database
			// Public PMR As clsDRWrapper
			// Public RSZDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper HR = new clsDRWrapper();
			public clsDRWrapper HR_AutoInitialized = null;
			public clsDRWrapper HR
			{
				get
				{
					if ( HR_AutoInitialized == null)
					{
						 HR_AutoInitialized = new clsDRWrapper();
					}
					return HR_AutoInitialized;
				}
				set
				{
					 HR_AutoInitialized = value;
				}
			}
			// Public TMDB As DAO.Database
			// Public TM As clsDRWrapper
			// Public HRDB As DAO.Database
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper SR = new clsDRWrapper();
			public clsDRWrapper SR_AutoInitialized = null;
			public clsDRWrapper SR
			{
				get
				{
					if ( SR_AutoInitialized == null)
					{
						 SR_AutoInitialized = new clsDRWrapper();
					}
					return SR_AutoInitialized;
				}
				set
				{
					 SR_AutoInitialized = value;
				}
			}
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
