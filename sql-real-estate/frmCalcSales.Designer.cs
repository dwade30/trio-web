﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCalcSales.
	/// </summary>
	partial class frmCalcSales : BaseForm
	{
		public fecherFoundation.FCComboBox cmbCalc;
		public fecherFoundation.FCLabel lblCalc;
		public fecherFoundation.FCTextBox txtMax;
		public fecherFoundation.FCTextBox txtMin;
		public fecherFoundation.FCButton cmdCalc;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCalcSales));
			this.cmbCalc = new fecherFoundation.FCComboBox();
			this.lblCalc = new fecherFoundation.FCLabel();
			this.txtMax = new fecherFoundation.FCTextBox();
			this.txtMin = new fecherFoundation.FCTextBox();
			this.cmdCalc = new fecherFoundation.FCButton();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCalc)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 434);
			this.BottomPanel.Size = new System.Drawing.Size(558, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtMax);
			this.ClientArea.Controls.Add(this.txtMin);
			this.ClientArea.Controls.Add(this.cmdCalc);
			this.ClientArea.Controls.Add(this.cmbCalc);
			this.ClientArea.Controls.Add(this.lblCalc);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(558, 374);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(558, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(270, 30);
			this.HeaderText.Text = "Calculate Sale Records";
			// 
			// cmbCalc
			// 
			this.cmbCalc.AutoSize = false;
			this.cmbCalc.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCalc.FormattingEnabled = true;
			this.cmbCalc.Items.AddRange(new object[] {
				"All",
				"Range by Account",
				"Range by Sale Date"
			});
			this.cmbCalc.Location = new System.Drawing.Point(183, 30);
			this.cmbCalc.Name = "cmbCalc";
			this.cmbCalc.Size = new System.Drawing.Size(193, 40);
			this.cmbCalc.TabIndex = 8;
			this.cmbCalc.Text = "All";
			this.cmbCalc.SelectedIndexChanged += new System.EventHandler(this.optCalc_CheckedChanged);
			// 
			// lblCalc
			// 
			this.lblCalc.AutoSize = true;
			this.lblCalc.Location = new System.Drawing.Point(30, 44);
			this.lblCalc.Name = "lblCalc";
			this.lblCalc.Size = new System.Drawing.Size(80, 15);
			this.lblCalc.TabIndex = 9;
			this.lblCalc.Text = "CALCULATE";
			// 
			// txtMax
			// 
			this.txtMax.AutoSize = false;
			this.txtMax.BackColor = System.Drawing.SystemColors.Window;
			this.txtMax.Enabled = false;
			this.txtMax.LinkItem = null;
			this.txtMax.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMax.LinkTopic = null;
			this.txtMax.Location = new System.Drawing.Point(183, 150);
			this.txtMax.Name = "txtMax";
			this.txtMax.Size = new System.Drawing.Size(193, 40);
			this.txtMax.TabIndex = 7;
			this.txtMax.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMax_KeyPress);
			// 
			// txtMin
			// 
			this.txtMin.AutoSize = false;
			this.txtMin.BackColor = System.Drawing.SystemColors.Window;
			this.txtMin.Enabled = false;
			this.txtMin.LinkItem = null;
			this.txtMin.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMin.LinkTopic = null;
			this.txtMin.Location = new System.Drawing.Point(183, 90);
			this.txtMin.Name = "txtMin";
			this.txtMin.Size = new System.Drawing.Size(193, 40);
			this.txtMin.TabIndex = 6;
			this.txtMin.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMin_KeyPress);
			// 
			// cmdCalc
			// 
			this.cmdCalc.AppearanceKey = "acceptButton";
			this.cmdCalc.Location = new System.Drawing.Point(30, 210);
			this.cmdCalc.Name = "cmdCalc";
			this.cmdCalc.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdCalc.Size = new System.Drawing.Size(113, 48);
			this.cmdCalc.TabIndex = 5;
			this.cmdCalc.Text = "Calculate";
			this.cmdCalc.Click += new System.EventHandler(this.cmdCalc_Click);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 164);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(65, 18);
			this.Label2.TabIndex = 9;
			this.Label2.Text = "MAXIMUM";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 104);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(65, 18);
			this.Label1.TabIndex = 8;
			this.Label1.Text = "MINIMUM";
			// 
			// frmCalcSales
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(558, 542);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCalcSales";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Calculate Sale Records";
			this.Load += new System.EventHandler(this.frmCalcSales_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCalcSales_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCalc)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
