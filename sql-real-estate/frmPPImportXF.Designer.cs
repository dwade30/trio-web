//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPPImport.
	/// </summary>
	partial class frmPPImportXF
	{
		public fecherFoundation.FCCheckBox chkFlagChanges;
		public fecherFoundation.FCTextBox txtPercentChg;
		public fecherFoundation.FCComboBox cmbUpdateType;
		public FCGrid GridLoad;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public FCGrid GridGood;
		public fecherFoundation.FCCheckBox chkShowChangesOnly;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCButton cmdUnselect;
		public fecherFoundation.FCCheckBox chkShowAddedOnly;
		public FCGrid GridNew;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public FCGrid GridNotUpdated;
		public fecherFoundation.FCCheckBox chkMarkDeleted;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public fecherFoundation.FCCheckBox chkUndeleteAccounts;
		public FCGrid GridBadAccounts;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCLabel lblCurAssess;
		public fecherFoundation.FCLabel Label4;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuLoad;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAccountsToUpdate;
		public fecherFoundation.FCToolStripMenuItem mnuPrintBadAccount;
		public fecherFoundation.FCToolStripMenuItem mnuPrintNewAccount;
		public fecherFoundation.FCToolStripMenuItem mnuPrintNotUpdated;
		public fecherFoundation.FCToolStripMenuItem mnuPrintSummary;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.chkFlagChanges = new fecherFoundation.FCCheckBox();
			this.txtPercentChg = new fecherFoundation.FCTextBox();
			this.cmbUpdateType = new fecherFoundation.FCComboBox();
			this.GridLoad = new fecherFoundation.FCGrid();
			this.SSTab1 = new fecherFoundation.FCTabControl();
			this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
			this.GridGood = new fecherFoundation.FCGrid();
			this.chkShowChangesOnly = new fecherFoundation.FCCheckBox();
			this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
			this.cmdSelect = new fecherFoundation.FCButton();
			this.cmdUnselect = new fecherFoundation.FCButton();
			this.chkShowAddedOnly = new fecherFoundation.FCCheckBox();
			this.GridNew = new fecherFoundation.FCGrid();
			this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
			this.GridNotUpdated = new fecherFoundation.FCGrid();
			this.chkMarkDeleted = new fecherFoundation.FCCheckBox();
			this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
			this.chkUndeleteAccounts = new fecherFoundation.FCCheckBox();
			this.GridBadAccounts = new fecherFoundation.FCGrid();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Shape1 = new fecherFoundation.FCLabel();
			this.lblCurAssess = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuPrintAccountsToUpdate = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintBadAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintNewAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintNotUpdated = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintSummary = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLoad = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdUpdate = new fecherFoundation.FCButton();
			this.fcButton1 = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkFlagChanges)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridLoad)).BeginInit();
			this.SSTab1.SuspendLayout();
			this.SSTab1_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridGood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowChangesOnly)).BeginInit();
			this.SSTab1_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUnselect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowAddedOnly)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridNew)).BeginInit();
			this.SSTab1_Page3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMarkDeleted)).BeginInit();
			this.SSTab1_Page4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkUndeleteAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridBadAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fcButton1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdUpdate);
			this.BottomPanel.Location = new System.Drawing.Point(0, 587);
			this.BottomPanel.Size = new System.Drawing.Size(708, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkFlagChanges);
			this.ClientArea.Controls.Add(this.txtPercentChg);
			this.ClientArea.Controls.Add(this.cmbUpdateType);
			this.ClientArea.Controls.Add(this.GridLoad);
			this.ClientArea.Controls.Add(this.SSTab1);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Shape1);
			this.ClientArea.Controls.Add(this.lblCurAssess);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Size = new System.Drawing.Size(728, 606);
			this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblCurAssess, 0);
			this.ClientArea.Controls.SetChildIndex(this.Shape1, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
			this.ClientArea.Controls.SetChildIndex(this.SSTab1, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridLoad, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbUpdateType, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtPercentChg, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkFlagChanges, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.fcButton1);
			this.TopPanel.Size = new System.Drawing.Size(728, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.fcButton1, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(375, 28);
			this.HeaderText.Text = "Import Personal Property for Billing";
			// 
			// chkFlagChanges
			// 
			this.chkFlagChanges.Location = new System.Drawing.Point(235, 34);
			this.chkFlagChanges.Name = "chkFlagChanges";
			this.chkFlagChanges.Size = new System.Drawing.Size(252, 22);
			this.chkFlagChanges.TabIndex = 10;
			this.chkFlagChanges.Text = "Flag accounts with changes in value >";
			this.chkFlagChanges.CheckedChanged += new System.EventHandler(this.chkFlagChanges_CheckedChanged);
			// 
			// txtPercentChg
			// 
			this.txtPercentChg.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercentChg.Location = new System.Drawing.Point(545, 30);
			this.txtPercentChg.Name = "txtPercentChg";
			this.txtPercentChg.Size = new System.Drawing.Size(60, 40);
			this.txtPercentChg.TabIndex = 9;
			this.txtPercentChg.Text = "0";
			this.txtPercentChg.Validating += new System.ComponentModel.CancelEventHandler(this.txtPercentChg_Validating);
			// 
			// cmbUpdateType
			// 
			this.cmbUpdateType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbUpdateType.Items.AddRange(new object[] {
            "Update Amounts",
            "Update Information Only"});
			this.cmbUpdateType.Location = new System.Drawing.Point(235, 90);
			this.cmbUpdateType.Name = "cmbUpdateType";
			this.cmbUpdateType.Size = new System.Drawing.Size(290, 40);
			this.cmbUpdateType.TabIndex = 1001;
			this.ToolTip1.SetToolTip(this.cmbUpdateType, "Choose Update Information Only if you do not want to update amounts");
			// 
			// GridLoad
			// 
			this.GridLoad.Cols = 10;
			this.GridLoad.ColumnHeadersVisible = false;
			this.GridLoad.FixedCols = 0;
			this.GridLoad.FixedRows = 0;
			this.GridLoad.Location = new System.Drawing.Point(545, 90);
			this.GridLoad.Name = "GridLoad";
			this.GridLoad.RowHeadersVisible = false;
			this.GridLoad.Rows = 0;
			this.GridLoad.Size = new System.Drawing.Size(114, 18);
			this.GridLoad.TabIndex = 3;
			this.GridLoad.Visible = false;
			// 
			// SSTab1
			// 
			this.SSTab1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.SSTab1.Controls.Add(this.SSTab1_Page1);
			this.SSTab1.Controls.Add(this.SSTab1_Page2);
			this.SSTab1.Controls.Add(this.SSTab1_Page3);
			this.SSTab1.Controls.Add(this.SSTab1_Page4);
			this.SSTab1.Location = new System.Drawing.Point(30, 150);
			this.SSTab1.Name = "SSTab1";
			this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
			this.SSTab1.Size = new System.Drawing.Size(670, 437);
			this.SSTab1.TabIndex = 4;
			this.SSTab1.Text = "Bad Accounts";
			// 
			// SSTab1_Page1
			// 
			this.SSTab1_Page1.Controls.Add(this.GridGood);
			this.SSTab1_Page1.Controls.Add(this.chkShowChangesOnly);
			this.SSTab1_Page1.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page1.Name = "SSTab1_Page1";
			this.SSTab1_Page1.Size = new System.Drawing.Size(668, 401);
			this.SSTab1_Page1.Text = "Update";
			// 
			// GridGood
			// 
			this.GridGood.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridGood.Cols = 1;
			this.GridGood.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.GridGood.FixedCols = 0;
			this.GridGood.Location = new System.Drawing.Point(4, 60);
			this.GridGood.Name = "GridGood";
			this.GridGood.RowHeadersVisible = false;
			this.GridGood.Rows = 1;
			this.GridGood.Size = new System.Drawing.Size(659, 336);
			this.GridGood.TabIndex = 5;
			// 
			// chkShowChangesOnly
			// 
			this.chkShowChangesOnly.Location = new System.Drawing.Point(20, 20);
			this.chkShowChangesOnly.Name = "chkShowChangesOnly";
			this.chkShowChangesOnly.Size = new System.Drawing.Size(201, 22);
			this.chkShowChangesOnly.TabIndex = 12;
			this.chkShowChangesOnly.Text = "Show changed accounts only";
			this.chkShowChangesOnly.CheckedChanged += new System.EventHandler(this.chkShowChangesOnly_CheckedChanged);
			// 
			// SSTab1_Page2
			// 
			this.SSTab1_Page2.Controls.Add(this.cmdSelect);
			this.SSTab1_Page2.Controls.Add(this.cmdUnselect);
			this.SSTab1_Page2.Controls.Add(this.chkShowAddedOnly);
			this.SSTab1_Page2.Controls.Add(this.GridNew);
			this.SSTab1_Page2.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page2.Name = "SSTab1_Page2";
			this.SSTab1_Page2.Size = new System.Drawing.Size(668, 401);
			this.SSTab1_Page2.Text = "New Accounts";
			// 
			// cmdSelect
			// 
			this.cmdSelect.AppearanceKey = "actionButton";
			this.cmdSelect.Location = new System.Drawing.Point(253, 20);
			this.cmdSelect.Name = "cmdSelect";
			this.cmdSelect.Size = new System.Drawing.Size(118, 40);
			this.cmdSelect.TabIndex = 16;
			this.cmdSelect.Text = "Select All";
			this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
			// 
			// cmdUnselect
			// 
			this.cmdUnselect.AppearanceKey = "actionButton";
			this.cmdUnselect.Location = new System.Drawing.Point(390, 20);
			this.cmdUnselect.Name = "cmdUnselect";
			this.cmdUnselect.Size = new System.Drawing.Size(146, 40);
			this.cmdUnselect.TabIndex = 15;
			this.cmdUnselect.Text = "Un Select All";
			this.cmdUnselect.Click += new System.EventHandler(this.cmdUnselect_Click);
			// 
			// chkShowAddedOnly
			// 
			this.chkShowAddedOnly.Location = new System.Drawing.Point(20, 20);
			this.chkShowAddedOnly.Name = "chkShowAddedOnly";
			this.chkShowAddedOnly.Size = new System.Drawing.Size(187, 22);
			this.chkShowAddedOnly.TabIndex = 13;
			this.chkShowAddedOnly.Text = "Show accounts to add only";
			this.chkShowAddedOnly.CheckedChanged += new System.EventHandler(this.chkShowAddedOnly_CheckedChanged);
			// 
			// GridNew
			// 
			this.GridNew.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridNew.Cols = 1;
			this.GridNew.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.GridNew.FixedCols = 0;
			this.GridNew.Location = new System.Drawing.Point(4, 60);
			this.GridNew.Name = "GridNew";
			this.GridNew.RowHeadersVisible = false;
			this.GridNew.Rows = 1;
			this.GridNew.Size = new System.Drawing.Size(659, 336);
			this.GridNew.TabIndex = 6;
			this.GridNew.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridNew_ValidateEdit);
			this.GridNew.CurrentCellChanged += new System.EventHandler(this.GridNew_RowColChange);
			// 
			// SSTab1_Page3
			// 
			this.SSTab1_Page3.Controls.Add(this.GridNotUpdated);
			this.SSTab1_Page3.Controls.Add(this.chkMarkDeleted);
			this.SSTab1_Page3.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page3.Name = "SSTab1_Page3";
			this.SSTab1_Page3.Size = new System.Drawing.Size(668, 401);
			this.SSTab1_Page3.Text = "Not Updated";
			// 
			// GridNotUpdated
			// 
			this.GridNotUpdated.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridNotUpdated.Cols = 1;
			this.GridNotUpdated.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.GridNotUpdated.FixedCols = 0;
			this.GridNotUpdated.Location = new System.Drawing.Point(4, 60);
			this.GridNotUpdated.Name = "GridNotUpdated";
			this.GridNotUpdated.RowHeadersVisible = false;
			this.GridNotUpdated.Rows = 1;
			this.GridNotUpdated.Size = new System.Drawing.Size(659, 336);
			this.GridNotUpdated.TabIndex = 7;
			// 
			// chkMarkDeleted
			// 
			this.chkMarkDeleted.Location = new System.Drawing.Point(20, 20);
			this.chkMarkDeleted.Name = "chkMarkDeleted";
			this.chkMarkDeleted.Size = new System.Drawing.Size(300, 22);
			this.chkMarkDeleted.TabIndex = 17;
			this.chkMarkDeleted.Text = "Mark all Not Updateds as deleted upon update";
			// 
			// SSTab1_Page4
			// 
			this.SSTab1_Page4.Controls.Add(this.chkUndeleteAccounts);
			this.SSTab1_Page4.Controls.Add(this.GridBadAccounts);
			this.SSTab1_Page4.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page4.Name = "SSTab1_Page4";
			this.SSTab1_Page4.Size = new System.Drawing.Size(668, 401);
			this.SSTab1_Page4.Text = "Bad Accounts";
			// 
			// chkUndeleteAccounts
			// 
			this.chkUndeleteAccounts.Location = new System.Drawing.Point(20, 20);
			this.chkUndeleteAccounts.Name = "chkUndeleteAccounts";
			this.chkUndeleteAccounts.Size = new System.Drawing.Size(422, 22);
			this.chkUndeleteAccounts.TabIndex = 14;
			this.chkUndeleteAccounts.Text = "If only error is Deleted status, un-delete account and transfer values";
			// 
			// GridBadAccounts
			// 
			this.GridBadAccounts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridBadAccounts.Cols = 1;
			this.GridBadAccounts.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.GridBadAccounts.FixedCols = 0;
			this.GridBadAccounts.Location = new System.Drawing.Point(4, 60);
			this.GridBadAccounts.Name = "GridBadAccounts";
			this.GridBadAccounts.RowHeadersVisible = false;
			this.GridBadAccounts.Rows = 1;
			this.GridBadAccounts.Size = new System.Drawing.Size(659, 336);
			this.GridBadAccounts.TabIndex = 8;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(626, 44);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(33, 18);
			this.Label5.TabIndex = 11;
			this.Label5.Text = "% AS";
			// 
			// Shape1
			// 
			this.Shape1.BackColor = System.Drawing.Color.FromArgb(255, 255, 0);
			this.Shape1.Location = new System.Drawing.Point(674, 41);
			this.Shape1.Name = "Shape1";
			this.Shape1.Size = new System.Drawing.Size(17, 18);
			this.Shape1.TabIndex = 12;
			// 
			// lblCurAssess
			// 
			this.lblCurAssess.Location = new System.Drawing.Point(131, 30);
			this.lblCurAssess.Name = "lblCurAssess";
			this.lblCurAssess.Size = new System.Drawing.Size(82, 18);
			this.lblCurAssess.TabIndex = 2;
			this.lblCurAssess.Text = "0";
			this.lblCurAssess.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 30);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(82, 18);
			this.Label4.TabIndex = 1;
			this.Label4.Text = "CURRENT NET";
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintAccountsToUpdate,
            this.mnuPrintBadAccount,
            this.mnuPrintNewAccount,
            this.mnuPrintNotUpdated,
            this.mnuPrintSummary});
			this.MainMenu1.Name = null;
			// 
			// mnuPrintAccountsToUpdate
			// 
			this.mnuPrintAccountsToUpdate.Index = 0;
			this.mnuPrintAccountsToUpdate.Name = "mnuPrintAccountsToUpdate";
			this.mnuPrintAccountsToUpdate.Text = "Print Accounts to Update Report";
			this.mnuPrintAccountsToUpdate.Click += new System.EventHandler(this.mnuPrintAccountsToUpdate_Click);
			// 
			// mnuPrintBadAccount
			// 
			this.mnuPrintBadAccount.Index = 1;
			this.mnuPrintBadAccount.Name = "mnuPrintBadAccount";
			this.mnuPrintBadAccount.Text = "Print Bad Account Report";
			this.mnuPrintBadAccount.Click += new System.EventHandler(this.mnuPrintBadAccount_Click);
			// 
			// mnuPrintNewAccount
			// 
			this.mnuPrintNewAccount.Index = 2;
			this.mnuPrintNewAccount.Name = "mnuPrintNewAccount";
			this.mnuPrintNewAccount.Text = "Print New Account Report";
			this.mnuPrintNewAccount.Click += new System.EventHandler(this.mnuPrintNewAccount_Click);
			// 
			// mnuPrintNotUpdated
			// 
			this.mnuPrintNotUpdated.Index = 3;
			this.mnuPrintNotUpdated.Name = "mnuPrintNotUpdated";
			this.mnuPrintNotUpdated.Text = "Print Not Updated Report";
			this.mnuPrintNotUpdated.Click += new System.EventHandler(this.mnuPrintNotUpdated_Click);
			// 
			// mnuPrintSummary
			// 
			this.mnuPrintSummary.Index = 4;
			this.mnuPrintSummary.Name = "mnuPrintSummary";
			this.mnuPrintSummary.Text = "Print Summary";
			this.mnuPrintSummary.Click += new System.EventHandler(this.mnuPrintSummary_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "";
			// 
			// mnuLoad
			// 
			this.mnuLoad.Index = -1;
			this.mnuLoad.Name = "mnuLoad";
			this.mnuLoad.Text = "Load Records";
			this.mnuLoad.Click += new System.EventHandler(this.mnuLoad_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = -1;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = -1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = -1;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Update TRIO";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = -1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = -1;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdUpdate
			// 
			this.cmdUpdate.AppearanceKey = "acceptButton";
			this.cmdUpdate.Location = new System.Drawing.Point(284, 30);
			this.cmdUpdate.Name = "cmdUpdate";
			this.cmdUpdate.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdUpdate.Size = new System.Drawing.Size(150, 48);
			this.cmdUpdate.TabIndex = 0;
			this.cmdUpdate.Text = "Update TRIO";
			this.cmdUpdate.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// fcButton1
			// 
			this.fcButton1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.fcButton1.Location = new System.Drawing.Point(596, 29);
			this.fcButton1.Name = "fcButton1";
			this.fcButton1.Size = new System.Drawing.Size(104, 24);
			this.fcButton1.TabIndex = 1;
			this.fcButton1.Text = "Load Records";
			this.fcButton1.Click += new System.EventHandler(this.mnuLoad_Click);
			// 
			// frmPPImportXF
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(728, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmPPImportXF";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Import Personal Property for Billing";
			this.Load += new System.EventHandler(this.frmPPImport_Load);
			this.Resize += new System.EventHandler(this.frmPPImport_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPPImport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkFlagChanges)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridLoad)).EndInit();
			this.SSTab1.ResumeLayout(false);
			this.SSTab1_Page1.ResumeLayout(false);
			this.SSTab1_Page1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridGood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowChangesOnly)).EndInit();
			this.SSTab1_Page2.ResumeLayout(false);
			this.SSTab1_Page2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUnselect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowAddedOnly)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridNew)).EndInit();
			this.SSTab1_Page3.ResumeLayout(false);
			this.SSTab1_Page3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMarkDeleted)).EndInit();
			this.SSTab1_Page4.ResumeLayout(false);
			this.SSTab1_Page4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkUndeleteAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridBadAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fcButton1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdUpdate;
		private FCButton fcButton1;
	}
}