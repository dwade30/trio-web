﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptDuplicateRecords.
	/// </summary>
	public partial class rptDuplicateRecords : BaseSectionReport
	{
		public static rptDuplicateRecords InstancePtr
		{
			get
			{
				return (rptDuplicateRecords)Sys.GetInstance(typeof(rptDuplicateRecords));
			}
		}

		protected rptDuplicateRecords _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}

		public rptDuplicateRecords()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Duplicate Records";
		}
		// nObj = 1
		//   0	rptDuplicateRecords	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		public void Init(int intOrder, bool modalDialog, bool boolModal = false)
		{
			string strSQL = "";
			// 0 is by date
			// 1 is by category then date
			// 2 is by account,card then date
			switch (intOrder)
			{
				case 1:
					{
						strSQL = "select * from cya where description1 = 'Duplicate Record(s) Corrected' order by description2,cyadate,cast(isnull(description3,0) as int),cast(isnull(description4,0) as int)";
						break;
					}
				case 2:
					{
						strSQL = "select * from cya where description1 = 'Duplicate Record(s) Corrected' order by cast(isnull(description3,0) as int),cast(isnull(description4,0) as int),cyadate,description2";
						break;
					}
				default:
					{
						strSQL = "select * from cya where description1 = 'Duplicate Record(s) Corrected' order by cyadate,description2,cast(isnull(description3,0) as int),cast(isnull(description4,0) as int)";
						break;
					}
			}
			//end switch
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				return;
			}
			if (!boolModal)
			{
				frmReportViewer.InstancePtr.Init(this, "", showModal: modalDialog);
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Field [Date] not found!! (maybe it is an alias?)
				txtCYADate.Text = Strings.Format(clsLoad.Get_Fields("Date"), "MM/dd/yyyy");
				txtType.Text = FCConvert.ToString(clsLoad.Get_Fields_String("description2"));
				txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields_String("description3"));
				txtCard.Text = FCConvert.ToString(clsLoad.Get_Fields_String("description4"));
				// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
				txtUser.Text = FCConvert.ToString(clsLoad.Get_Fields("UserID"));
				clsLoad.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
