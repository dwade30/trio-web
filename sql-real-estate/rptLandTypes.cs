﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptLandTypes.
	/// </summary>
	public partial class rptLandTypes : BaseSectionReport
	{
		public static rptLandTypes InstancePtr
		{
			get
			{
				return (rptLandTypes)Sys.GetInstance(typeof(rptLandTypes));
			}
		}

		protected rptLandTypes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
                clsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptLandTypes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Land Types";
		}
		// nObj = 1
		//   0	rptLandTypes	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			clsReport.OpenRecordset("select * from landtype order by code", modGlobalVariables.strREDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			if (!clsReport.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				txtCode.Text = FCConvert.ToString(Conversion.Val(clsReport.Get_Fields("code")));
				txtDescription.Text = clsReport.Get_Fields_String("description");
				txtShortDesc.Text = clsReport.Get_Fields_String("shortDescription");
				txtType.Text = "";
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				if (Conversion.Val(clsReport.Get_Fields("type")) == modREConstants.CNSTLANDTYPEACRES)
				{
					txtType.Text = "Acres";
				}
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("type")) == modREConstants.CNSTLANDTYPEFRACTIONALACREAGE)
				{
					txtType.Text = "Fractional Acres";
				}
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("type")) == modREConstants.CNSTLANDTYPEFRONTFOOT)
				{
					txtType.Text = "Front Foot";
				}
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("type")) == modREConstants.CNSTLANDTYPESITE)
				{
					txtType.Text = "Site";
				}
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("type")) == modREConstants.CNSTLANDTYPEIMPROVEMENTS)
				{
					txtType.Text = "Improvements";
				}
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("type")) == modREConstants.CNSTLANDTYPELINEARFEET)
				{
					txtType.Text = "Linear Feet";
				}
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("type")) == modREConstants.CNSTLANDTYPESQUAREFEET)
				{
					txtType.Text = "Square Feet";
				}
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("type")) == modREConstants.CNSTLANDTYPEFRONTFOOTSCALED)
				{
					txtType.Text = "Front Foot - Width";
				}
				// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("type")) == modREConstants.CNSTLANDTYPELINEARFEETSCALED)
				{
					txtType.Text = "Linear Feet - Width";
				}
				strTemp = "None";
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				if (Conversion.Val(clsReport.Get_Fields("category")) == modREConstants.CNSTLANDCATCROP)
				{
					strTemp = "Crop Land";
				}
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("category")) == modREConstants.CNSTLANDCATHARDWOOD)
				{
					strTemp = "Hardwood";
				}
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("category")) == modREConstants.CNSTLANDCATHARDWOODFARM)
				{
					strTemp = "Hardwood (Farm)";
				}
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("category")) == modREConstants.CNSTLANDCATMIXEDWOOD)
				{
					strTemp = "Mixed Wood";
				}
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("category")) == modREConstants.CNSTLANDCATMIXEDWOODFARM)
				{
					strTemp = "Mixed Wood (Farm)";
				}
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("category")) == modREConstants.CNSTLANDCATNONE)
				{
					strTemp = "None";
				}
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("category")) == modREConstants.CNSTLANDCATORCHARD)
				{
					strTemp = "Orchard Land";
				}
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("category")) == modREConstants.CNSTLANDCATPASTURE)
				{
					strTemp = "Pasture Land";
				}
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("category")) == modREConstants.CNSTLANDCATSOFTWOOD)
				{
					strTemp = "Softwood";
				}
				// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsReport.Get_Fields("category")) == modREConstants.CNSTLANDCATSOFTWOODFARM)
				{
					strTemp = "Softwood (Farm)";
				}
				txtCategory.Text = strTemp;
				txtFactor.Text = FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Double("factor")));
				clsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

	
	}
}
