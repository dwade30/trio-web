﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptComm.
	/// </summary>
	public partial class srptComm : FCSectionReport
	{
		public srptComm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptComm InstancePtr
		{
			get
			{
				return (srptComm)Sys.GetInstance(typeof(srptComm));
			}
		}

		protected srptComm _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptComm	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCCard;
		

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Label22.Caption = "Data used for calculations supplied by Marshall & Swift which hereby reserves all rights herein.  Copyright " & Year(Date) & ", Marshall & Swift."
			intCCard = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			if (intCCard == -10)
			{
				// Unload Me
				this.Close();
				return;
			}

		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intCCard < 0)
			{
				int intOCode = 0;
				modREASValuations.Statics.OccCode2 = Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("OCC2") + "");
				intOCode = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("Occ1") + "")));
				if (intOCode == 0)
				{
					txtOcc1.Text = "";
					txtCQ1.Text = "";
					txtDwell1.Text = "";
					txtExt1.Text = "";
					txtStories1.Text = "";
					txtHeating1.Text = "";
					txtBuilt1.Text = "";
					txtRemodeled1.Text = "";
					txtBase1.Text = "";
					txtHeatCool1.Text = "";
					txtTotal1.Text = "";
					txtSize1.Text = "";
					txtAdjusted1.Text = "";
					txtCondition1.Text = "";
					txtPhys1.Text = "";
					txtFunctional1.Text = "";
					txtSub1.Text = "";
					modCommercial.Statics.dblBase1 = 0;
					modCommercial.Statics.dblHeat1 = 0;
					modCommercial.Statics.dblTotal1 = 0;
					modCommercial.Statics.dblSize1 = 0;
					modCommercial.Statics.dblAdjustment1 = 0;
					modCommercial.Statics.intSQFT1 = 0;
					modCommercial.Statics.dblReplacement1 = 0;
					modCommercial.Statics.lngSubTotal1 = 0;
				}
				if (modREASValuations.Statics.OccCode2 == 0)
				{
					txtOcc2.Text = "";
					txtCQ2.Text = "";
					txtDwell2.Text = "";
					txtExt2.Text = "";
					txtStories2.Text = "";
					txtHeating2.Text = "";
					txtBuilt2.Text = "";
					txtRemodeled2.Text = "";
					txtBase2.Text = "";
					txtHeatCool2.Text = "";
					txtTotal2.Text = "";
					txtSize2.Text = "";
					txtAdjusted2.Text = "";
					txtTotalSqft2.Text = "";
					txtReplacement2.Text = "";
					txtCondition2.Text = "";
					txtPhys2.Text = "";
					txtFunctional2.Text = "";
					txtSub2.Text = "";
					lblTotSQFT.Visible = false;
					txtTotTotSqft.Visible = false;
				}
				else
				{
					if (modOutbuilding.Statics.lngOutbuildingTotal[modGlobalVariables.Statics.intCurrentCard] > 0)
					{
						lblTotSQFT.Visible = false;
						txtTotTotSqft.Visible = false;
					}
					else
					{
						lblTotSQFT.Visible = true;
						txtTotTotSqft.Visible = true;
						txtTotTotSqft.Text = Strings.Format(modCommercial.Statics.intSQFT1 + modCommercial.Statics.intSQFT2, "#,###,###,##0");
					}
				}
				if (intOCode == 0 && FCConvert.ToInt32(modREASValuations.Statics.OccCode2) == 0)
				{
					modCommercial.Statics.lngCTotal = 0;
				}
				txtOcc1.Text = Strings.Trim(modCommercial.Statics.strOccupancy1);
				if (modREASValuations.Statics.OccCode2 != 0)
					txtOcc2.Text = Strings.Trim(modCommercial.Statics.strOccupancy2);
				txtCQ1.Text = Strings.Trim(modCommercial.Statics.strClassQuality1);
				if (modREASValuations.Statics.OccCode2 != 0)
					txtCQ2.Text = Strings.Trim(modCommercial.Statics.strclassquality2);
				txtDwell1.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("dwel1"));
				if (modREASValuations.Statics.OccCode2 != 0)
					txtDwell2.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("DWEL2"));
				modProperty.Statics.WKey = FCConvert.ToInt32(340 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1extwalls") + ""));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				modProperty.Statics.WORKA1 = modGlobalVariables.Statics.ComCostRec.ClDesc;
				modREASValuations.Statics.WORKA2 = "";
				if (modREASValuations.Statics.OccCode2 != 0)
				{
					modProperty.Statics.WKey = FCConvert.ToInt32(340 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2extwalls") + ""));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
					modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.ComCostRec.ClDesc + "";
				}
				txtExt1.Text = Strings.Trim(modProperty.Statics.WORKA1);
				txtExt2.Text = Strings.Trim(modREASValuations.Statics.WORKA2);
				txtStories1.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1stories") + "")) + " STORY @ " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1height") + "")) + "'";
				if (modREASValuations.Statics.OccCode2 != 0)
				{
					txtStories2.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2stories") + "")) + " STORY @ " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2height") + "")) + "'";
				}
				txtHeating1.Text = modCommercial.Statics.strHeat1;
				if (modREASValuations.Statics.OccCode2 != 0)
					txtHeating2.Text = modCommercial.Statics.strHeat2;
				txtBuilt1.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("c1built"));
				if (modREASValuations.Statics.OccCode2 != 0)
					txtBuilt2.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("c2built"));
				txtRemodeled1.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("c1remodel"));
				if (modREASValuations.Statics.OccCode2 != 0)
					txtRemodeled2.Text = FCConvert.ToString(modDataTypes.Statics.CMR.Get_Fields_Int32("c2remodel"));
				txtBase1.Text = Strings.Format(modCommercial.Statics.dblBase1, "##0.00");
				if (modREASValuations.Statics.OccCode2 != 0)
					txtBase2.Text = Strings.Format(modCommercial.Statics.dblBase2, "##0.00");
				txtHeatCool1.Text = Strings.Format(modCommercial.Statics.dblHeat1, "##0.00");
				if (modREASValuations.Statics.OccCode2 != 0)
					txtHeatCool2.Text = Strings.Format(modCommercial.Statics.dblHeat2, "##0.00");
				txtTotal1.Text = Strings.Format(modCommercial.Statics.dblTotal1, "##0.00");
				if (modREASValuations.Statics.OccCode2 != 0)
					txtTotal2.Text = Strings.Format(modCommercial.Statics.dblTotal2, "##0.00");
				txtSize1.Text = Strings.Format(modCommercial.Statics.dblSize1, "0.000");
				if (modREASValuations.Statics.OccCode2 != 0)
					txtSize2.Text = Strings.Format(modCommercial.Statics.dblSize2, "0.000");
				txtAdjusted1.Text = Strings.Format(modCommercial.Statics.dblAdjustment1, "##0.00");
				if (modREASValuations.Statics.OccCode2 != 0)
					txtAdjusted2.Text = Strings.Format(modCommercial.Statics.dblAdjustment2, "##0.00");
				txtTotSqft1.Text = Strings.Format(modCommercial.Statics.intSQFT1, "#,##0");
				if (modREASValuations.Statics.OccCode2 != 0)
					txtTotalSqft2.Text = Strings.Format(modCommercial.Statics.intSQFT2, "#,##0");
				txtReplacement1.Text = Strings.Format(modCommercial.Statics.dblReplacement1, "#,###,##0");
				if (modREASValuations.Statics.OccCode2 != 0)
					txtReplacement2.Text = Strings.Format(modCommercial.Statics.dblReplacement2, "#,###,##0");
				modProperty.Statics.WKey = FCConvert.ToInt32(350 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c1condition")));
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
				modProperty.Statics.WORKA1 = modGlobalVariables.Statics.ComCostRec.ClDesc;
				modREASValuations.Statics.WORKA2 = "";
				if (modREASValuations.Statics.OccCode2 != 0)
				{
					modProperty.Statics.WKey = FCConvert.ToInt32(350 + Conversion.Val(modDataTypes.Statics.CMR.Get_Fields_Int32("c2condition")));
					modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey, "COMMERCIAL", "COMMERCIAL");
					modREASValuations.Statics.WORKA2 = modGlobalVariables.Statics.ComCostRec.ClDesc;
				}
				txtCondition1.Text = Strings.Trim(modProperty.Statics.WORKA1);
				txtCondition2.Text = Strings.Trim(modREASValuations.Statics.WORKA2);
				txtPhys1.Text = Strings.Format(modCommercial.Statics.dblPhysical1, ".00");
				if (modREASValuations.Statics.OccCode2 != 0)
					txtPhys2.Text = Strings.Format(modCommercial.Statics.dblPhysical2, ".00");
				txtFunctional1.Text = Strings.Format(modCommercial.Statics.dblFunctional1, "0.00");
				if (modREASValuations.Statics.OccCode2 != 0)
					txtFunctional2.Text = Strings.Format(modCommercial.Statics.dblFunctional2, "0.00");
				txtSub1.Text = Strings.Format(modCommercial.Statics.lngSubTotal1, "#,###,##0");
				if (modREASValuations.Statics.OccCode2 != 0)
					txtSub2.Text = Strings.Format(modCommercial.Statics.lngSubTotal2, "#,###,##0");
				txtEcon.Text = Strings.Format(modCommercial.Statics.dblEconomic, "0.00");
				txtTotVal.Text = Strings.Format(modCommercial.Statics.lngCTotal, "#,###,##0");
			}
			else
			{
				// With frmNewValuationReport.CommercialGrid(intCCard)
				// txtOcc1.Text = .TextMatrix(0, 2)
				// txtOcc2.Text = .TextMatrix(0, 3)
				// txtCQ1.Text = .TextMatrix(1, 2)
				// txtCQ2.Text = .TextMatrix(1, 3)
				// txtDwell1.Text = .TextMatrix(2, 2)
				// txtDwell2.Text = .TextMatrix(2, 3)
				// txtExt1.Text = .TextMatrix(3, 2)
				// txtExt2.Text = .TextMatrix(3, 3)
				// txtStories1.Text = .TextMatrix(4, 2)
				// txtStories2.Text = .TextMatrix(4, 3)
				// txtHeating1.Text = .TextMatrix(5, 2)
				// txtHeating2.Text = .TextMatrix(5, 3)
				txtOcc1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].OccupancyType[1];
				txtCQ1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].ClassQuality[1];
				txtDwell1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].DwellUnits[1];
				txtExt1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Exterior[1];
				txtStories1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Stories[1];
				txtHeating1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].HeatingCool[1];
				if (modSpeedCalc.Statics.CalcCommList[intCCard + 1].OccupancyType[2] != string.Empty)
				{
					txtOcc2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].OccupancyType[2];
					txtCQ2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].ClassQuality[2];
					txtDwell2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].DwellUnits[2];
					txtExt2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Exterior[2];
					txtStories2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Stories[2];
					txtHeating2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].HeatingCool[2];
					txtBuilt2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Built[2];
					txtRemodeled2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Remodeled[2];
					txtBase2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].BaseCost[2];
					txtHeatCool2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].HeatCoolSqft[2];
					txtTotal2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Total[2];
					txtSize2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].SizeFactor[2];
					txtAdjusted2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].AdjustedCost[2];
					txtTotalSqft2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].TotalSqft[2];
					txtReplacement2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].ReplacementCost[2];
					txtCondition2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Condition[2];
					txtPhys2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Physical[2];
					txtFunctional2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Functional[2];
					txtSub2.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].subtotal[2];
					if (modSpeedCalc.Statics.CalcOutList[intCCard + 1].OutbuildingType[1] > 0)
					{
						lblTotSQFT.Visible = false;
						txtTotTotSqft.Visible = false;
					}
					else
					{
						lblTotSQFT.Visible = true;
						txtTotTotSqft.Visible = true;
						txtTotTotSqft.Text = Strings.Format(FCConvert.ToInt32(FCConvert.ToDouble(modSpeedCalc.Statics.CalcCommList[intCCard + 1].TotalSqft[1])) + FCConvert.ToInt32(FCConvert.ToDouble(modSpeedCalc.Statics.CalcCommList[intCCard + 1].TotalSqft[2])), "#,###,##0");
					}
				}
				else
				{
					lblTotSQFT.Visible = false;
					txtTotTotSqft.Visible = false;
					txtOcc2.Text = "";
					txtCQ2.Text = "";
					txtDwell2.Text = "";
					txtExt2.Text = "";
					txtStories2.Text = "";
					txtHeating2.Text = "";
					txtBuilt2.Text = "";
					txtRemodeled2.Text = "";
					txtBase2.Text = "";
					txtHeatCool2.Text = "";
					txtTotal2.Text = "";
					txtSize2.Text = "";
					txtAdjusted2.Text = "";
					txtTotalSqft2.Text = "";
					txtReplacement2.Text = "";
					txtCondition2.Text = "";
					txtPhys2.Text = "";
					txtFunctional2.Text = "";
					txtSub2.Text = "";
				}
				// txtBuilt1.Text = .TextMatrix(6, 2)
				// txtBuilt2.Text = .TextMatrix(6, 3)
				// txtRemodeled1.Text = .TextMatrix(7, 2)
				// txtRemodeled2.Text = .TextMatrix(7, 3)
				// txtBase1.Text = .TextMatrix(8, 2)
				// txtBase2.Text = .TextMatrix(8, 3)
				// txtHeatCool1.Text = .TextMatrix(9, 2)
				// txtHeatCool2.Text = .TextMatrix(9, 3)
				// txtTotal1.Text = .TextMatrix(10, 2)
				// txtTotal2.Text = .TextMatrix(10, 3)
				// txtSize1.Text = .TextMatrix(11, 2)
				// txtSize2.Text = .TextMatrix(11, 3)
				// txtAdjusted1.Text = .TextMatrix(12, 2)
				// txtAdjusted2.Text = .TextMatrix(12, 3)
				txtBuilt1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Built[1];
				txtRemodeled1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Remodeled[1];
				txtBase1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].BaseCost[1];
				txtHeatCool1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].HeatCoolSqft[1];
				txtTotal1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Total[1];
				txtSize1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].SizeFactor[1];
				txtAdjusted1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].AdjustedCost[1];
				// txtTotSqft1.Text = .TextMatrix(13, 2)
				// txtTotalSqft2.Text = .TextMatrix(13, 3)
				// txtReplacement1.Text = .TextMatrix(14, 2)
				// txtReplacement2.Text = .TextMatrix(14, 3)
				// txtCondition1.Text = .TextMatrix(15, 2)
				// txtCondition2.Text = .TextMatrix(15, 3)
				// txtPhys1.Text = .TextMatrix(16, 2)
				// txtPhys2.Text = .TextMatrix(16, 3)
				// txtFunctional1.Text = .TextMatrix(17, 2)
				// txtFunctional2.Text = .TextMatrix(17, 3)
				// txtSub1.Text = .TextMatrix(18, 2)
				// txtSub2.Text = .TextMatrix(18, 3)
				// txtEcon.Text = .TextMatrix(19, 2)
				// txtTotVal.Text = .TextMatrix(20, 2)
				txtTotSqft1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].TotalSqft[1];
				txtReplacement1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].ReplacementCost[1];
				txtCondition1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Condition[1];
				txtPhys1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Physical[1];
				txtFunctional1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].Functional[1];
				txtSub1.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].subtotal[1];
				txtEcon.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].EconFactor;
				txtTotVal.Text = modSpeedCalc.Statics.CalcCommList[intCCard + 1].TotalValue;
			}
		}

		
	}
}
