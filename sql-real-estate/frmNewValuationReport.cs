﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmNewValuationReport.
	/// </summary>
	public partial class frmNewValuationReport : BaseForm
	{
		public frmNewValuationReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            //FC:FINAL:AM:#3294 - allow only numbers
            this.txtCardNumber.AllowOnlyNumericInput();
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmNewValuationReport InstancePtr
		{
			get
			{
				return (frmNewValuationReport)Sys.GetInstance(typeof(frmNewValuationReport));
			}
		}

		protected frmNewValuationReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intNumCards;
		public double dblRatioAssess;

		private void CorrelationGrid_KeyDownEvent(object sender, KeyEventArgs e)
		{
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
			if (e.KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
			{
                modHelpFiles.Statics.Helping = true;
				if (CorrelationGrid.Col == 6)
				{
					if (CorrelationGrid.Row == 2)
					{
						modHelpFiles.HelpFiles_2("VALUATIONSCREEN", 88);
					}
					else if (CorrelationGrid.Row == 3)
					{
						modHelpFiles.HelpFiles_2("VALUATIONSCREEN", 91);
					}
				}
				else if (CorrelationGrid.Col == 3)
				{
					modHelpFiles.HelpFiles_2("VALUATIONSCREEN", 94);
				}
				else if (CorrelationGrid.Col == 4)
				{
					modHelpFiles.HelpFiles_2("VALUATIONSCREEN", 96);
				}
			}
		}

		private void CorrelationGrid_RowColChange(object sender, System.EventArgs e)
		{
			int Index;
			Index = modGlobalVariables.Statics.intCurrentCard;
			switch (CorrelationGrid.Col)
			{
				case 3:
				case 4:
				case 6:
					{
						if ((CorrelationGrid.Col == 3) || (CorrelationGrid.Col == 4))
						{
							if ((CorrelationGrid.Col == Conversion.Val(txtLandFrom.Text)) || (CorrelationGrid.Col == Conversion.Val(txtBldgFrom.Text)))
							{
								if (CorrelationGrid.Row == 4 && CorrelationGrid.Col != 4)
								{
									CorrelationGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								}
								else
								{
									CorrelationGrid.Editable = FCGrid.EditableSettings.flexEDNone;
								}
							}
							else
							{
								CorrelationGrid.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						if (CorrelationGrid.Col == 6)
						{
							if ((CorrelationGrid.Col == Conversion.Val(txtLandFrom.Text)) || (CorrelationGrid.Col == Conversion.Val(txtBldgFrom.Text)))
							{
								switch (CorrelationGrid.Row)
								{
									case 2:
									case 3:
										{
											CorrelationGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
											break;
										}
									default:
										{
											CorrelationGrid.Editable = FCGrid.EditableSettings.flexEDNone;
											break;
										}
								}
								//end switch
							}
							else
							{
								CorrelationGrid.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						break;
					}
				default:
					{
						CorrelationGrid.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void CorrelationGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            int Index;
			Index = modGlobalVariables.Statics.intCurrentCard;
			CorrelationGrid.EditText = Strings.Format(CorrelationGrid.EditText, "#,###,###,##0");
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = CorrelationGrid.GetFlexRowIndex(e.RowIndex);
			int col = CorrelationGrid.GetFlexColIndex(e.ColumnIndex);
			switch (col)
			{
				case 3:
					{
						if (row == 4)
						{
							modProperty.Statics.lngMarketLand[Index] = FCConvert.ToInt32(FCConvert.ToDouble(CorrelationGrid.TextMatrix(2, 5)));
							if (Conversion.Val(CorrelationGrid.EditText) > 0)
							{
								modProperty.Statics.lngMarketBldg[Index] = FCConvert.ToInt32(FCConvert.ToDouble(CorrelationGrid.EditText)) - modProperty.Statics.lngMarketLand[Index];
							}
							else
							{
								modProperty.Statics.lngMarketBldg[Index] = 0;
							}
							if (modProperty.Statics.lngMarketBldg[Index] < 0)
							{
								modProperty.Statics.lngMarketBldg[Index] = 0;
								CorrelationGrid.EditText = FCConvert.ToString(modProperty.Statics.lngMarketLand[Index]);
								// Cancel = True
								return;
							}
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
						}
						break;
					}
				case 4:
					{
						break;
					}
				case 6:
					{
						switch (row)
						{
							case 2:
								{
									if (CorrelationGrid.EditText != "")
									{
										modProperty.Statics.lngOverRideLand[Index] = FCConvert.ToInt32(FCConvert.ToDouble(CorrelationGrid.EditText));
									}
									else
									{
										modProperty.Statics.lngOverRideLand[Index] = 0;
									}
									RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
									break;
								}
							case 3:
								{
									if (Conversion.Val(CorrelationGrid.EditText) > 0)
									{
										modProperty.Statics.lngOverRideBldg[Index] = FCConvert.ToInt32(FCConvert.ToDouble(CorrelationGrid.EditText));
									}
									else
									{
										modProperty.Statics.lngOverRideBldg[Index] = 0;
									}
									RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
									break;
								}
						}
						//end switch
						break;
					}
			}
			//end switch
		}

		private void frmNewValuationReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuQuit_Click();
			}
		}

		private void frmNewValuationReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGrids();
		}

		private void FillAccountLabels()
		{
			lblAccount.Text = modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + "";
			lblCard.Text = modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + "";
			lblMapLot.Text = modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + "";
			txtName.Text = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("DeedName1"));
			lblLocation.Text = Strings.Trim(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + " " + modDataTypes.Statics.MR.Get_Fields_String("rslocapt")) + " " + Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocstreet") + ""));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			frmNewREProperty.InstancePtr.cmdCalculate.Enabled = true;
		}

		private void mnuCurrentAccountCalc_Click(object sender, System.EventArgs e)
		{
			rptValuation.InstancePtr.Init(0, FCConvert.ToInt16(intNumCards - 1), ref intNumCards, this.Modal, false);
		}

		private void mnuPrintAccount_Click(object sender, System.EventArgs e)
		{
			rptValuation.InstancePtr.Init(0, FCConvert.ToInt16(intNumCards - 1), ref intNumCards, this.Modal);
		}

		private void mnuPrintCard_Click(object sender, System.EventArgs e)
		{
			rptValuation.InstancePtr.Init(FCConvert.ToInt16(modGlobalVariables.Statics.intCurrentCard - 1), FCConvert.ToInt16(modGlobalVariables.Statics.intCurrentCard - 1), ref intNumCards, this.Modal);
		}

		private void mnuPrintCardCalc_Click(object sender, System.EventArgs e)
		{
			rptValuation.InstancePtr.Init(FCConvert.ToInt16(modGlobalVariables.Statics.intCurrentCard - 1), FCConvert.ToInt16(modGlobalVariables.Statics.intCurrentCard - 1), ref intNumCards, this.Modal, false);
		}

		private void mnuPrintForm_Click(object sender, System.EventArgs e)
		{
            //double dblRatio = 0;
            //int lngOWidth = 0;
            //int lngOHeight = 0;
            //        //- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
            //        /*? On Error Resume Next  */
            //        //FC:FINAL:DSE:#667 Upon cancelling the print dialog, there should be no errors thrown.
            //        try
            //        {
            ////FC:FINAL:DDU:#i1563 - initialize CommonDialog1
            //MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
            //MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
            //        } catch
            //        {
            //        }
            //if (Information.Err().Number == 0)
            //{
            //	FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
            //	dblRatio = FCConvert.ToDouble(this.Height) / this.Width;
            //	lngOWidth = this.Width;
            //	lngOHeight = this.Height;
            //	this.Width = FCConvert.ToInt32(7.5 * 1440);
            //	// fit on a piece of paper
            //	this.Height = FCConvert.ToInt32(dblRatio * this.Width);
            //	//Application.DoEvents();
            //	//FCGlobal.Printer.Print();
            //	this.PrintForm();
            //	FCGlobal.Printer.EndDoc();
            //	FCGlobal.Screen.MousePointer = 0;
            //	this.Width = lngOWidth;
            //	this.Height = lngOHeight;
            //	//Application.DoEvents();
            //}
            //else
            //{
            //	MessageBox.Show("Print was cancelled by you.", "Print Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
            //Information.Err().Clear();
            /*? On Error GoTo 0 */
            //FCGlobal.Screen.MousePointer = 0;
            this.PrintForm();
        }

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			modREASValuations.Statics.Account = modGlobalVariables.Statics.gintLastAccountNumber;
			clsDRWrapper temp = modDataTypes.Statics.MR;
			modREMain.OpenMasterTable(ref temp, modREASValuations.Statics.Account, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.MR = temp;
			temp = modDataTypes.Statics.CMR;
			modREMain.OpenCOMMERCIALTable(ref temp, modREASValuations.Statics.Account, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.CMR = temp;
			temp = modDataTypes.Statics.DWL;
			modREMain.OpenDwellingTable(ref temp, modREASValuations.Statics.Account, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.DWL = temp;
			temp = modDataTypes.Statics.OUT;
			modREMain.OpenOutBuildingTable(ref temp, modREASValuations.Statics.Account, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.OUT = temp;
			modGlobalVariables.Statics.gintLastAccountNumber = modREASValuations.Statics.Account;
			this.Unload();
		}

		public void mnuQuit_Click()
		{
			//mnuQuit_Click(mnuQuit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			int intTemp;
			int x;
			clsDRWrapper clsSave = new clsDRWrapper();
			string strTemp = "";
			string strTemp2 = "";
			string strTemp3 = "";
			string strTemp4 = "";
			double dblRatio = 0;
			CorrelationGrid.Row = 0;
			//Application.DoEvents();
			clsSave.Execute("update status set calculated = '" + FCConvert.ToString(DateTime.Now) + "'", modGlobalVariables.strREDatabase);
			modSpeedCalc.SaveSummary();
			for (x = 0; x <= intNumCards - 1; x++)
			{
				// save info for each card
				clsDRWrapper temp1 = modDataTypes.Statics.MR;
				modREMain.OpenMasterTable(ref temp1, modGlobalVariables.Statics.gintLastAccountNumber, x + 1);
				modDataTypes.Statics.MR = temp1;
				strTemp = "";
				strTemp2 = "";
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode")) != FCConvert.ToInt16(modProperty.Statics.intOverRideCodeBldg[x + 1]))
				{
					strTemp2 = "Building OV Changed from " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode"))) + " To " + FCConvert.ToString(FCConvert.ToInt16(modProperty.Statics.intOverRideCodeBldg[x + 1]));
				}
				else
				{
					strTemp2 = "Building OV is " + FCConvert.ToString(FCConvert.ToInt16(modProperty.Statics.intOverRideCodeBldg[x + 1]));
				}
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("Hivallandcode")) != FCConvert.ToInt16(modProperty.Statics.intOverRideCodeLand[x + 1]))
				{
					strTemp = "Land OV Changed from " + FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivallandcode"))) + " To " + FCConvert.ToString(FCConvert.ToInt16(modProperty.Statics.intOverRideCodeLand[x + 1]));
				}
				else
				{
					strTemp = "Land OV is " + FCConvert.ToString(FCConvert.ToInt16(modProperty.Statics.intOverRideCodeLand[x + 1]));
				}
				if (FCConvert.ToInt16(modProperty.Statics.intOverRideCodeLand[x + 1]) > 1)
				{
					strTemp3 = "Land OV value is " + FCConvert.ToString(modProperty.Statics.lngOverRideLand[x + 1]);
				}
				else
				{
					strTemp3 = "";
				}
				if (FCConvert.ToInt16(modProperty.Statics.intOverRideCodeBldg[x + 1]) > 1)
				{
					strTemp4 = "Bldg OV value is " + FCConvert.ToString(modProperty.Statics.lngOverRideBldg[x + 1]);
				}
				else
				{
					strTemp4 = "";
				}
				modGlobalFunctions.AddCYAEntry_8("RE", "Valuation Screen Saved " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(x + 1), strTemp, strTemp2, strTemp3, strTemp4);
				modDataTypes.Statics.MR.Set_Fields("rssoft", modGlobalVariables.Statics.SoftAcresArray[x]);
				modDataTypes.Statics.MR.Set_Fields("rshard", modGlobalVariables.Statics.HardAcresArray[x]);
				modDataTypes.Statics.MR.Set_Fields("rsmixed", modGlobalVariables.Statics.MixedAcresArray[x]);
				modDataTypes.Statics.MR.Set_Fields("rssoftvalue", modGlobalVariables.Statics.SoftValueArray[x]);
				modDataTypes.Statics.MR.Set_Fields("rshardvalue", modGlobalVariables.Statics.HardValueArray[x]);
				modDataTypes.Statics.MR.Set_Fields("rsmixedvalue", modGlobalVariables.Statics.MixedValueArray[x]);
				modDataTypes.Statics.MR.Set_Fields("rsother", modDataTypes.Statics.MR.Get_Fields_Double("piacres") - modGlobalVariables.Statics.SoftAcresArray[x] - modGlobalVariables.Statics.HardAcresArray[x] - modGlobalVariables.Statics.MixedAcresArray[x]);
				modDataTypes.Statics.MR.Set_Fields("RSothervalue", modProperty.Statics.lngCurrentLand[x + 1] - modGlobalVariables.Statics.SoftValueArray[x] - modGlobalVariables.Statics.HardValueArray[x] - modGlobalVariables.Statics.MixedValueArray[x]);
				modDataTypes.Statics.MR.Set_Fields("hivalbldgcode", FCConvert.ToInt16(modProperty.Statics.intOverRideCodeBldg[x + 1]));
				modDataTypes.Statics.MR.Set_Fields("hivallandcode", FCConvert.ToInt16(modProperty.Statics.intOverRideCodeLand[x + 1]));
				if (modProperty.Statics.lngOverRideBldg[x + 1] > 0)
				{
					modDataTypes.Statics.MR.Set_Fields("hlvalbldg", modProperty.Statics.lngOverRideBldg[x + 1]);
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("HLVALBLDG", 0);
				}
				if (modProperty.Statics.lngOverRideLand[x + 1] > 0)
				{
					modDataTypes.Statics.MR.Set_Fields("hlvalland", modProperty.Statics.lngOverRideLand[x + 1]);
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("HLVALLAND", 0);
				}
				modDataTypes.Statics.MR.Set_Fields("HLALT1BLDG", modProperty.Statics.lngMarketBldg[x + 1]);
				modDataTypes.Statics.MR.Set_Fields("HLALT2BLDG", modProperty.Statics.lngIncomeBldg[x + 1]);
				modDataTypes.Statics.MR.Set_Fields("hlcompvalland", modProperty.Statics.lngCurrentLand[x + 1]);
				modDataTypes.Statics.MR.Set_Fields("hlcompvalbldg", modProperty.Statics.lngCurrentBldg[x + 1]);
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode") + ""))));
				switch (intTemp)
				{
					case 1:
						{
							modDataTypes.Statics.MR.Set_Fields("rlbldgval", modProperty.Statics.lngCurrentBldg[x + 1]);
							break;
						}
					case 2:
						{
							modDataTypes.Statics.MR.Set_Fields("rlbldgval", modProperty.Statics.lngPreviousBldg[x + 1]);
							break;
						}
					case 3:
						{
							modDataTypes.Statics.MR.Set_Fields("rlbldgval", modProperty.Statics.lngMarketBldg[x + 1]);
							break;
						}
					case 4:
						{
							modDataTypes.Statics.MR.Set_Fields("rlbldgval", modProperty.Statics.lngIncomeBldg[x + 1]);
							break;
						}
					case 6:
						{
							modDataTypes.Statics.MR.Set_Fields("rlbldgval", FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("HLVALBLDG") + "")));
							break;
						}
					default:
						{
							modDataTypes.Statics.MR.Set_Fields("rlbldgval", modProperty.Statics.lngCurrentBldg[x + 1]);
							break;
						}
				}
				//end switch
				intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("hivallandcode") + ""))));
				switch (intTemp)
				{
					case 1:
						{
							modDataTypes.Statics.MR.Set_Fields("rllandval", modProperty.Statics.lngCurrentLand[x + 1]);
							break;
						}
					case 2:
						{
							modDataTypes.Statics.MR.Set_Fields("rllandval", modProperty.Statics.lngPreviousLand[x + 1]);
							break;
						}
					case 3:
						{
							modDataTypes.Statics.MR.Set_Fields("rllandval", modProperty.Statics.lngMarketLand[x + 1]);
							break;
						}
					case 4:
						{
							modDataTypes.Statics.MR.Set_Fields("rllandval", modProperty.Statics.lngIncomeLand[x + 1]);
							break;
						}
					case 6:
						{
							modDataTypes.Statics.MR.Set_Fields("rllandval", FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("HLVALLAND") + "")));
							break;
						}
					default:
						{
							modDataTypes.Statics.MR.Set_Fields("rllandval", modProperty.Statics.lngCurrentLand[x + 1]);
							break;
						}
				}
				//end switch
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ritrancode"))));
				}
				else
				{
					modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = 0;
				}
				modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1452, "", "", modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
				dblRatio = (Conversion.Val(modGlobalVariables.Statics.CostRec.CBase) / 100);
				dblRatioAssess = dblRatio;
				modProperty.Statics.lngAcceptedLand[x + 1] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rllandval") + "")));
				modProperty.Statics.lngAcceptedBldg[x + 1] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rlbldgval") + "")));
				modProperty.Statics.LandSummaryTotal[x + 1] = modProperty.Statics.lngAcceptedLand[x + 1];
				modProperty.Statics.LandTotal[x + 1] = modProperty.Statics.lngAcceptedLand[x + 1];
				modProperty.Statics.BuildingTotal[x + 1] = modProperty.Statics.lngAcceptedBldg[x + 1];
				modProperty.Statics.lngCorrelatedLand[x + 1] = modGlobalRoutines.RERound_2(FCConvert.ToInt32(modProperty.Statics.lngAcceptedLand[x + 1] * dblRatio));
				modProperty.Statics.lngCorrelatedBldg[x + 1] = modGlobalRoutines.RERound_2(FCConvert.ToInt32(modProperty.Statics.lngAcceptedBldg[x + 1] * dblRatio));
				modProperty.Statics.lngPreviousBldg[x + 1] = modProperty.Statics.lngCurrentBldg[x + 1];
				modProperty.Statics.lngPreviousLand[x + 1] = modProperty.Statics.lngCurrentLand[x + 1];
				modDataTypes.Statics.MR.Set_Fields("rllandval", modProperty.Statics.lngCorrelatedLand[x + 1]);
				modDataTypes.Statics.MR.Set_Fields("rlbldgval", modProperty.Statics.lngCorrelatedBldg[x + 1]);
				clsDRWrapper temp2 = modDataTypes.Statics.MR;
				modREMain.SaveMasterTable(ref temp2, modREASValuations.Statics.Account, x + 1);
				modDataTypes.Statics.MR = temp2;
			}
			// x
			FillCorrelationGrid(ref modGlobalVariables.Statics.intCurrentCard);
			FillTotalGrid(ref intNumCards);
			clsDRWrapper temp = modDataTypes.Statics.MR;
			modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGlobalVariables.Statics.intCurrentCard);
			modDataTypes.Statics.MR = temp;
			MessageBox.Show("Save Complete.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(cmdSave, new System.EventArgs());
		}

		public void MakeGrids()
		{
		}

		public void AddGrids(ref short cardnum)
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorHandler = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				int x;
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
				/* On Error GoTo ErrorHandler */
				if (cardnum == 0)
				{
					SetupGrids(cardnum);
					return;
				}
				/*? On Error Resume Next  */
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
				/* On Error GoTo ErrorHandler */
				SSTab1.Tab = 5;
				SetupGrids(cardnum);
				for (x = 0; x <= cardnum; x++)
				{
					// add a line to the totals grid
					TotalGrid.Rows += 1;
				}
				// x
				return;
			}
			catch (Exception ex)
			{
				//ErrorHandler: ;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In AddGrids", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmNewValuationReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmNewValuationReport properties;
			//frmNewValuationReport.ScaleWidth	= 9570;
			//frmNewValuationReport.ScaleHeight	= 8400;
			//frmNewValuationReport.LinkTopic	= "Form1";
			//frmNewValuationReport.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			if (modGlobalVariables.Statics.boolRegRecords)
			{
				lblSalesRecord.Visible = false;
			}
			else
			{
				lblSalesRecord.Visible = true;
			}
			dblRatioAssess = 1;
			lblSalesRecord.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			Label12.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			Label13.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			Label14.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			Label15.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			Label16.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			Label17.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			lblNumCards.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			txtCardNumber.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			lblNumCards.Font = new Font(lblNumCards.Font, FontStyle.Bold);
			txtCardNumber.Font = new Font(txtCardNumber.Font, FontStyle.Bold);
		}

		private void SetupGrids(short intGNum = 0)
		{
			SetupPropertyGrid();
			SetupCommercialGrid();
			SetupDwellingGrid();
			SetupDwellDescGrid();
			SetupDwellMiscGrid();
			SetupDwellCondGrid();
			SetupDwellPercGrid();
			SetupOutbuildingGrid();
			SetupCorrelationGrid();
			SetupTotalGrid();
			SetupSaleGrid();
			SetupMiscGrid();
		}

		private void ResizeGrids()
		{
			int x;
			ResizeTotalGrid();
			// For x = 0 To intNumCards - 1
			ResizePropertyGrid();
			ResizeDwellingGrid();
			ResizeDwellDescGrid();
			ResizeDwellMiscGrid();
			ResizeDwellCondGrid();
			ResizeDwellPercGrid();
			ResizeOutbuildingGrid();
			ResizeCorrelationGrid();
			ResizeCommercialGrid();
			ResizeSaleGrid();
			ResizeMiscGrid();
			// Next x
		}

		private void SetupSaleGrid()
		{
			SaleGrid.Cols = 2;
			SaleGrid.Rows = 6;
			SaleGrid.FixedRows = 0;
			SaleGrid.FixedCols = 1;
			SaleGrid.TextMatrix(0, 0, "Sale Date");
			SaleGrid.TextMatrix(1, 0, "Sale Price");
			SaleGrid.TextMatrix(2, 0, "Sale Type");
			SaleGrid.TextMatrix(3, 0, "Financing");
			SaleGrid.TextMatrix(4, 0, "Verified");
			SaleGrid.TextMatrix(5, 0, "Validity");
			ResizeSaleGrid();
		}

		private void ResizeSaleGrid()
		{
			int GridWidth;
			GridWidth = SaleGrid.WidthOriginal;
			SaleGrid.ColWidth(0, FCConvert.ToInt32(0.3 * GridWidth));
			SaleGrid.ColWidth(1, FCConvert.ToInt32(0.66 * GridWidth));
			SaleGrid.HeightOriginal = SaleGrid.RowHeight(1) * SaleGrid.Rows + 60;
		}

		private void SetupMiscGrid()
		{
			MiscGrid.Cols = 2;
			MiscGrid.Rows = 15;
			MiscGrid.FixedRows = 0;
			MiscGrid.FixedCols = 1;
			MiscGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			MiscGrid.TextMatrix(1, 0, "");
			MiscGrid.TextMatrix(2, 0, "Zoning/Use");
			MiscGrid.TextMatrix(3, 0, "Topography");
			MiscGrid.TextMatrix(4, 0, "Utilities");
			MiscGrid.TextMatrix(5, 0, "Street");
			MiscGrid.TextMatrix(6, 0, modSpeedCalc.Statics.CalcPropertyList[1].Open1Title);
			MiscGrid.TextMatrix(7, 0, modSpeedCalc.Statics.CalcPropertyList[1].Open2Title);
			MiscGrid.TextMatrix(8, 0, "Ref 1");
			MiscGrid.TextMatrix(9, 0, "Ref 2");
			MiscGrid.TextMatrix(10, 0, modSpeedCalc.Statics.CalcPropertyList[1].TranLandBldgDesc);
			MiscGrid.TextMatrix(11, 0, modSpeedCalc.Statics.CalcPropertyList[1].XCoordTitle);
			MiscGrid.TextMatrix(12, 0, modSpeedCalc.Statics.CalcPropertyList[1].YCoordTitle);
			MiscGrid.TextMatrix(13, 0, "Exemption Codes");
			MiscGrid.TextMatrix(14, 0, "Land Schedule");
			ResizeMiscGrid();
		}

		private void ResizeMiscGrid()
		{
			int GridWidth;
			// Dim GridHeight As Long
			// Dim RowHeight As Long
			// Dim X As Integer
			GridWidth = MiscGrid.WidthOriginal;
			MiscGrid.ColWidth(0, FCConvert.ToInt32(0.42 * GridWidth));
			MiscGrid.ColWidth(1, FCConvert.ToInt32(0.55 * GridWidth));
			// GridHeight = MiscGrid(intgnum).Height
			// RowHeight = GridHeight / 15
			// 
			// For X = 0 To MiscGrid(intgnum).Rows - 1
			// MiscGrid(intgnum).RowHeight(X) = RowHeight
			// Next X
			// 
		}

		private void SetupPropertyGrid()
		{
			PropertyGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
			PropertyGrid.Rows = 1;
			PropertyGrid.Cols = 7;
			PropertyGrid.FixedRows = 1;
			PropertyGrid.FixedCols = 0;
			PropertyGrid.TextMatrix(0, 0, "Units");
			PropertyGrid.TextMatrix(0, 1, "Method/Desc.");
			PropertyGrid.TextMatrix(0, 2, "Price/Unit");
			PropertyGrid.TextMatrix(0, 3, "Total");
			PropertyGrid.TextMatrix(0, 4, "FCTR");
			PropertyGrid.TextMatrix(0, 5, "Influence");
			PropertyGrid.TextMatrix(0, 6, "Value");
			//FC:FINAL:CHN - issue #1631: Fix grid alignment.
			PropertyGrid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			PropertyGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			PropertyGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 6, FCGrid.AlignmentSettings.flexAlignRightCenter);
            ResizePropertyGrid();
		}

		private void ResizePropertyGrid()
		{
			int GridWidth;
			GridWidth = PropertyGrid.WidthOriginal;
			PropertyGrid.ColWidth(0, FCConvert.ToInt32(0.14 * GridWidth));
			PropertyGrid.ColWidth(1, FCConvert.ToInt32(0.24 * GridWidth));
			PropertyGrid.ColWidth(2, FCConvert.ToInt32(0.1 * GridWidth));
			PropertyGrid.ColWidth(3, FCConvert.ToInt32(0.13 * GridWidth));
			PropertyGrid.ColWidth(4, FCConvert.ToInt32(0.1 * GridWidth));
			PropertyGrid.ColWidth(5, FCConvert.ToInt32(0.16 * GridWidth));
			PropertyGrid.ColWidth(6, FCConvert.ToInt32(0.125 * GridWidth));
			PropertyGrid.HeightOriginal = PropertyGrid.RowHeight(1) * PropertyGrid.Rows + 60;
		}

		private void SetupCommercialGrid()
		{
			CommercialGrid.Cols = 4;
			CommercialGrid.Rows = 22;
			CommercialGrid.FixedRows = 0;
			CommercialGrid.FixedCols = 1;
			CommercialGrid.TextMatrix(0, 0, "Occupancy Type");
			CommercialGrid.TextMatrix(1, 0, "Class & Quality");
			CommercialGrid.TextMatrix(2, 0, "# Dwelling Units");
			CommercialGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 2, 2, 2, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			CommercialGrid.TextMatrix(3, 0, "Exterior");
			CommercialGrid.TextMatrix(4, 0, "Stories & Height");
			CommercialGrid.TextMatrix(5, 0, "Heating/Cooling");
			CommercialGrid.TextMatrix(6, 0, "Built");
			CommercialGrid.TextMatrix(7, 0, "Remodeled");
			CommercialGrid.TextMatrix(8, 0, "Base Cost/Sqft");
			CommercialGrid.TextMatrix(9, 0, "Heat-Cool/Sqft");
			CommercialGrid.TextMatrix(10, 0, "      Total");
			CommercialGrid.TextMatrix(11, 0, "Size Factor");
			CommercialGrid.TextMatrix(12, 0, "Adjusted Cost/Sqft");
			CommercialGrid.TextMatrix(13, 0, "Total Square Feet");
			CommercialGrid.TextMatrix(14, 0, "Replacement Cost");
			CommercialGrid.TextMatrix(15, 0, "Condition");
			CommercialGrid.TextMatrix(16, 0, "Physical % Good");
			CommercialGrid.TextMatrix(17, 0, "Functional % Good");
			CommercialGrid.TextMatrix(18, 0, "Subtotal");
			CommercialGrid.TextMatrix(19, 0, "Economic Factor");
			CommercialGrid.TextMatrix(20, 0, "Total Value");
			CommercialGrid.TextMatrix(9, 1, "+");
			CommercialGrid.TextMatrix(11, 1, "X");
			CommercialGrid.TextMatrix(13, 1, "X");
			CommercialGrid.TextMatrix(16, 1, "X");
			CommercialGrid.TextMatrix(17, 1, "X");
			CommercialGrid.TextMatrix(19, 1, "X");
			CommercialGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 6, 2, 7, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			CommercialGrid.Row = CommercialGrid.Rows - 1;
			CommercialGrid.RowSel = CommercialGrid.Row;
			CommercialGrid.Col = 0;
			CommercialGrid.ColSel = 3;
			CommercialGrid.CellBorder(Color.FromArgb(1, 1, 1), -1, 2, -1, -1, -1, -1);
			// lblCopyright.Caption = lblCopyright.Caption & " " & Year(Date) & ", Marshall && Swift."
			ResizeCommercialGrid();
            //FC:FINAL:AM:#1640 - decrease the height of rows
            this.CommercialGrid.RowHeight(-1, 400);
        }

		private void SetupDwellDescGrid()
		{
			DwellDescGrid.Cols = 2;
			DwellDescGrid.Rows = 15;
			DwellDescGrid.FixedRows = 0;
			DwellDescGrid.FixedCols = 1;
			DwellDescGrid.TextMatrix(1, 0, "Exterior");
			DwellDescGrid.TextMatrix(2, 0, "Dwelling Units");
			DwellDescGrid.TextMatrix(3, 0, "Open 3");
			DwellDescGrid.TextMatrix(4, 0, "Open 4");
			modProperty.Statics.WKey = 1520;
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (modGlobalVariables.Statics.CostRec.ClDesc.Length > 1)
			{
				if (Strings.Left(modGlobalVariables.Statics.CostRec.ClDesc, 2) != "..")
				{
					DwellDescGrid.TextMatrix(3, 0, modGlobalVariables.Statics.CostRec.ClDesc);
				}
			}
			modProperty.Statics.WKey = 1530;
			modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(modProperty.Statics.WKey);
			if (modGlobalVariables.Statics.CostRec.ClDesc.Length > 1)
			{
				if (Strings.Left(modGlobalVariables.Statics.CostRec.ClDesc, 2) != "..")
				{
					DwellDescGrid.TextMatrix(4, 0, modGlobalVariables.Statics.CostRec.ClDesc);
				}
			}
			DwellDescGrid.TextMatrix(5, 0, "Foundation");
			DwellDescGrid.TextMatrix(6, 0, "Fin. Basement Area");
			DwellDescGrid.TextMatrix(7, 0, "Heating");
			DwellDescGrid.TextMatrix(8, 0, "Rooms");
			DwellDescGrid.TextMatrix(9, 0, "Bedrooms");
			DwellDescGrid.TextMatrix(10, 0, "Baths");
			DwellDescGrid.TextMatrix(11, 0, "Attic");
			DwellDescGrid.TextMatrix(12, 0, "Fireplaces");
			DwellDescGrid.TextMatrix(13, 0, "Insulation");
			DwellDescGrid.TextMatrix(14, 0, "Unfin. Living Area");
			ResizeDwellDescGrid();
		}

		private void SetupDwellMiscGrid()
		{
			DwellMiscGrid.Cols = 2;
			DwellMiscGrid.Rows = 11;
			DwellMiscGrid.FixedRows = 0;
			DwellMiscGrid.FixedCols = 1;
			DwellMiscGrid.TextMatrix(1, 0, "Masonry Trim");
			DwellMiscGrid.TextMatrix(2, 0, "Roof Cover");
			DwellMiscGrid.TextMatrix(5, 0, "Basement");
			DwellMiscGrid.TextMatrix(6, 0, "Bsmt Garage");
			DwellMiscGrid.TextMatrix(7, 0, "Cooling");
			DwellMiscGrid.TextMatrix(10, 0, "Half Baths");
			DwellMiscGrid.TextMatrix(9, 0, "Add Fixtures");
			ResizeDwellMiscGrid();
		}

		private void SetupDwellCondGrid()
		{
			DwellCondGrid.Cols = 7;
			DwellCondGrid.Rows = 2;
			DwellCondGrid.FixedRows = 1;
			DwellCondGrid.FixedCols = 0;
			DwellCondGrid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			DwellCondGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			DwellCondGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			DwellCondGrid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			DwellCondGrid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			DwellCondGrid.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			DwellCondGrid.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			DwellCondGrid.TextMatrix(0, 0, "Built");
			DwellCondGrid.TextMatrix(0, 1, "Renovated");
			DwellCondGrid.TextMatrix(0, 2, "Kitchens");
			DwellCondGrid.TextMatrix(0, 3, "Baths");
			DwellCondGrid.TextMatrix(0, 4, "Condition");
			DwellCondGrid.TextMatrix(0, 5, "Layout");
			DwellCondGrid.TextMatrix(0, 6, "Total");
			ResizeDwellCondGrid();
		}

		private void SetupDwellPercGrid()
		{
			DwellPercGrid.Cols = 6;
			DwellPercGrid.Rows = 2;
			DwellPercGrid.FixedRows = 1;
			DwellPercGrid.FixedCols = 0;
			DwellPercGrid.TextMatrix(0, 0, "Functional Obsolescence");
			DwellPercGrid.TextMatrix(0, 1, "Economic Obsolescence");
			DwellPercGrid.TextMatrix(0, 2, "Physical %");
			DwellPercGrid.TextMatrix(0, 3, "Functional %");
			DwellPercGrid.TextMatrix(0, 4, "Economic %");
			DwellPercGrid.TextMatrix(0, 5, "Value (Rcnld)");
			ResizeDwellPercGrid();
		}

		private void SetupDwellingGrid()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			DwellingGrid.Cols = 2;
			DwellingGrid.Rows = 15;
			DwellingGrid.FixedRows = 0;
			DwellingGrid.FixedCols = 1;
			DwellingGrid.TextMatrix(0, 0, "Base");
			DwellingGrid.TextMatrix(1, 0, "Trim");
			DwellingGrid.TextMatrix(2, 0, "Roof");
			clsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1520", modGlobalVariables.strREDatabase);
			DwellingGrid.TextMatrix(3, 0, FCConvert.ToString(clsTemp.Get_Fields_String("csdesc")));
			clsTemp.OpenRecordset("select * from costrecord where crecordnumber = 1530", modGlobalVariables.strREDatabase);
			DwellingGrid.TextMatrix(4, 0, FCConvert.ToString(clsTemp.Get_Fields_String("csdesc")));
			DwellingGrid.TextMatrix(5, 0, "Basement");
			DwellingGrid.TextMatrix(6, 0, "Fin Bsmt");
			DwellingGrid.TextMatrix(7, 0, "Heat");
			DwellingGrid.TextMatrix(10, 0, "Plumbing");
			DwellingGrid.TextMatrix(11, 0, "Attic");
			DwellingGrid.TextMatrix(12, 0, "Fireplace");
			DwellingGrid.TextMatrix(13, 0, "Insulation");
			DwellingGrid.TextMatrix(14, 0, "Unfinished");
			ResizeDwellingGrid();
		}

		private void ResizeDwellingGrid()
		{
			int GridWidth;
			GridWidth = DwellingGrid.WidthOriginal;
			DwellingGrid.ColWidth(0, FCConvert.ToInt32(0.47 * GridWidth));
			//FC:FINAL:MSH - issue #1638: increase column width to hide empty area
			//DwellingGrid.ColWidth(1, FCConvert.ToInt32(0.49 * GridWidth));
			DwellingGrid.ColWidth(1, FCConvert.ToInt32(0.47 * GridWidth));
			//DwellingGrid.HeightOriginal = DwellingGrid.RowHeight(1) * DwellingGrid.Rows + 50;
		}

		private void ResizeCommercialGrid()
		{
			int GridWidth;
			GridWidth = CommercialGrid.WidthOriginal;
			CommercialGrid.ColWidth(0, FCConvert.ToInt32(0.3 * GridWidth));
			CommercialGrid.ColWidth(1, FCConvert.ToInt32(0.055 * GridWidth));
			CommercialGrid.ColWidth(2, FCConvert.ToInt32(0.31 * GridWidth));
			CommercialGrid.ColWidth(3, FCConvert.ToInt32(0.31 * GridWidth));
			CommercialGrid.Height = CommercialGrid.RowHeight(1) * CommercialGrid.Rows + 60;
		}

		private void ResizeDwellCondGrid()
		{
			int GridWidth;
			GridWidth = DwellCondGrid.WidthOriginal;
			DwellCondGrid.ColWidth(0, FCConvert.ToInt32(0.06 * GridWidth));
			DwellCondGrid.ColWidth(1, FCConvert.ToInt32(0.12 * GridWidth));
			DwellCondGrid.ColWidth(2, FCConvert.ToInt32(0.17 * GridWidth));
			DwellCondGrid.ColWidth(3, FCConvert.ToInt32(0.17 * GridWidth));
			DwellCondGrid.ColWidth(4, FCConvert.ToInt32(0.17 * GridWidth));
			DwellCondGrid.ColWidth(5, FCConvert.ToInt32(0.17 * GridWidth));
			DwellCondGrid.ColWidth(6, FCConvert.ToInt32(0.13 * GridWidth));
			//DwellCondGrid.HeightOriginal = DwellCondGrid.RowHeight(1) * DwellCondGrid.Rows + 50;
		}

		private void ResizeDwellPercGrid()
		{
			int GridWidth;
			GridWidth = DwellPercGrid.WidthOriginal;
			DwellPercGrid.ColWidth(0, FCConvert.ToInt32(0.27 * GridWidth));
			DwellPercGrid.ColWidth(1, FCConvert.ToInt32(0.27 * GridWidth));
			DwellPercGrid.ColWidth(2, FCConvert.ToInt32(0.1 * GridWidth));
			DwellPercGrid.ColWidth(3, FCConvert.ToInt32(0.11 * GridWidth));
			DwellPercGrid.ColWidth(4, FCConvert.ToInt32(0.11 * GridWidth));
			DwellPercGrid.ColWidth(5, FCConvert.ToInt32(0.135 * GridWidth));
			//DwellPercGrid.HeightOriginal = DwellPercGrid.RowHeight(1) * DwellPercGrid.Rows + 50;
		}

		private void ResizeDwellDescGrid()
		{
			int GridWidth;
			GridWidth = DwellDescGrid.WidthOriginal;
			DwellDescGrid.ColWidth(0, FCConvert.ToInt32(0.47 * GridWidth));
			//FC:FINAL:MSH - issue #1638: decrease column width for displaying hidden data
			//DwellDescGrid.ColWidth(1, FCConvert.ToInt32(0.5 * GridWidth));
			DwellDescGrid.ColWidth(1, FCConvert.ToInt32(0.47 * GridWidth));
			//DwellDescGrid.HeightOriginal = DwellDescGrid.RowHeight(1) * DwellDescGrid.Rows + 50;
		}

		private void ResizeDwellMiscGrid()
		{
			int GridWidth;
			GridWidth = DwellMiscGrid.WidthOriginal;
			DwellMiscGrid.ColWidth(0, FCConvert.ToInt32(0.47 * GridWidth));
			//FC:FINAL:MSH - issue #1638: increase column width to hide empty area
			//DwellMiscGrid.ColWidth(1, FCConvert.ToInt32(0.49 * GridWidth));
			DwellMiscGrid.ColWidth(1, FCConvert.ToInt32(0.47 * GridWidth));
			//DwellMiscGrid.HeightOriginal = DwellMiscGrid.RowHeight(1) * DwellMiscGrid.Rows + 50;
		}

		private void SetupOutbuildingGrid()
		{
			OutBuildingGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
			OutBuildingGrid.Cols = 10;
			OutBuildingGrid.FixedRows = 1;
			OutBuildingGrid.Rows = 1;
			OutBuildingGrid.FixedCols = 0;
			OutBuildingGrid.TextMatrix(0, 0, "Description");
			OutBuildingGrid.TextMatrix(0, 1, "Year");
			OutBuildingGrid.TextMatrix(0, 2, "Units");
			OutBuildingGrid.TextMatrix(0, 3, "Grade");
			OutBuildingGrid.TextMatrix(0, 4, "RCN");
			OutBuildingGrid.TextMatrix(0, 5, "Cond");
			OutBuildingGrid.TextMatrix(0, 6, "Phy");
			OutBuildingGrid.TextMatrix(0, 7, "Func");
			OutBuildingGrid.TextMatrix(0, 8, "Econ");
			OutBuildingGrid.TextMatrix(0, 9, "(Rcnld)");
			//FC:FINAL:CHN - issue #1643: Form design fixes.
			//OutBuildingGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, 9, FCGrid.AlignmentSettings.flexAlignRightCenter);
			ResizeOutbuildingGrid();
		}

		private void ResizeOutbuildingGrid()
		{
			int GridWidth;
			GridWidth = OutBuildingGrid.WidthOriginal;
			//OutBuildingGrid.Height = OutBuildingGrid.Height;
			OutBuildingGrid.ColWidth(0, FCConvert.ToInt32(0.205 * GridWidth));
			OutBuildingGrid.ColWidth(1, FCConvert.ToInt32(0.08 * GridWidth));
			OutBuildingGrid.ColWidth(2, FCConvert.ToInt32(0.09 * GridWidth));
			OutBuildingGrid.ColWidth(3, FCConvert.ToInt32(0.1 * GridWidth));
			OutBuildingGrid.ColWidth(4, FCConvert.ToInt32(0.1 * GridWidth));
			OutBuildingGrid.ColWidth(5, FCConvert.ToInt32(0.08 * GridWidth));
			OutBuildingGrid.ColWidth(6, FCConvert.ToInt32(0.08 * GridWidth));
			OutBuildingGrid.ColWidth(7, FCConvert.ToInt32(0.08 * GridWidth));
			OutBuildingGrid.ColWidth(8, FCConvert.ToInt32(0.08 * GridWidth));
			OutBuildingGrid.ColWidth(9, FCConvert.ToInt32(0.1 * GridWidth));
			OutBuildingGrid.HeightOriginal = OutBuildingGrid.RowHeight(1) * OutBuildingGrid.Rows + 60;
		}
		// Private Sub GetCardNumbers()
		// If Val(optCardNumber(1).Caption) > gintMaxCards Then optCardNumber(1).Enabled = False
		// If Val(optCardNumber(2).Caption) > gintMaxCards Then optCardNumber(2).Enabled = False
		// If Val(optCardNumber(3).Caption) > gintMaxCards Then optCardNumber(3).Enabled = False
		// If Val(optCardNumber(4).Caption) > gintMaxCards Then optCardNumber(4).Enabled = False
		// If Val(optCardNumber(5).Caption) > gintMaxCards Then optCardNumber(5).Enabled = False
		// If Val(optCardNumber(6).Caption) > gintMaxCards Then optCardNumber(6).Enabled = False
		// If Val(optCardNumber(7).Caption) > gintMaxCards Then optCardNumber(7).Enabled = False
		// If Val(optCardNumber(8).Caption) > gintMaxCards Then optCardNumber(8).Enabled = False
		// Put Black Dot in Correct Option Button
		// If Val(optCardNumber(0).Caption) = intCurrentCard + 1 Then optCardNumber(0).Enabled = True
		// If Val(optCardNumber(1).Caption) = intCurrentCard + 1 Then optCardNumber(1).Enabled = True
		// If Val(optCardNumber(2).Caption) = intCurrentCard + 1 Then optCardNumber(2).Enabled = True
		// If Val(optCardNumber(3).Caption) = intCurrentCard + 1 Then optCardNumber(3).Enabled = True
		// If Val(optCardNumber(4).Caption) = intCurrentCard + 1 Then optCardNumber(4).Enabled = True
		// If Val(optCardNumber(5).Caption) = intCurrentCard + 1 Then optCardNumber(5).Enabled = True
		// If Val(optCardNumber(6).Caption) = intCurrentCard + 1 Then optCardNumber(6).Enabled = True
		// If Val(optCardNumber(7).Caption) = intCurrentCard + 1 Then optCardNumber(7).Enabled = True
		// If Val(optCardNumber(8).Caption) = intCurrentCard + 1 Then optCardNumber(8).Enabled = True
		// End Sub
		private void SetupCorrelationGrid()
		{
			CorrelationGrid.Cols = 8;
			CorrelationGrid.Rows = 5;
			CorrelationGrid.FixedRows = 2;
			CorrelationGrid.FixedCols = 1;
			CorrelationGrid.TextMatrix(0, 0, "");
			//FC:FINAL:CHN - issue #1629: Fix grid alignment.
			// CorrelationGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, 7, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			CorrelationGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, 1, 7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			CorrelationGrid.MergeRow(0, true);
			CorrelationGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeRestrictColumns;
			CorrelationGrid.TextMatrix(0, 1, "Cost Approach");
			CorrelationGrid.TextMatrix(0, 2, "Cost Approach");
			CorrelationGrid.TextMatrix(1, 1, "Current");
			CorrelationGrid.TextMatrix(1, 2, "Previous");
			CorrelationGrid.TextMatrix(0, 3, "Market");
			CorrelationGrid.TextMatrix(1, 3, "Approach");
			CorrelationGrid.TextMatrix(0, 4, "Income");
			CorrelationGrid.TextMatrix(1, 4, "Approach");
			CorrelationGrid.TextMatrix(0, 5, "Accepted");
			CorrelationGrid.TextMatrix(1, 5, "Assessment");
			CorrelationGrid.TextMatrix(0, 6, "");
			CorrelationGrid.TextMatrix(1, 6, "Override");
			CorrelationGrid.TextMatrix(0, 7, "Correlated");
			CorrelationGrid.TextMatrix(1, 7, "Assessment");
			CorrelationGrid.TextMatrix(2, 0, "Land");
			CorrelationGrid.TextMatrix(3, 0, "Building");
			CorrelationGrid.TextMatrix(4, 0, "Total");
			// .Row = 4
			// .RowSel = 4
			// .Col = 0
			// .ColSel = 7
			CorrelationGrid.Select(4, 0, 4, 7);
			CorrelationGrid.CellBorder(Color.FromArgb(1, 1, 1), -1, 2, -1, -1, -1, -1);
            CorrelationGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            CorrelationGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            CorrelationGrid.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
            CorrelationGrid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
            CorrelationGrid.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
            CorrelationGrid.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
            CorrelationGrid.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
        }

		private void ResizeCorrelationGrid()
		{
			int GridWidth;
			GridWidth = CorrelationGrid.WidthOriginal;
			CorrelationGrid.ColWidth(0, FCConvert.ToInt32(0.12 * GridWidth));
			CorrelationGrid.ColWidth(1, FCConvert.ToInt32(0.125 * GridWidth));
			CorrelationGrid.ColWidth(2, FCConvert.ToInt32(0.125 * GridWidth));
			CorrelationGrid.ColWidth(3, FCConvert.ToInt32(0.125 * GridWidth));
			CorrelationGrid.ColWidth(4, FCConvert.ToInt32(0.125 * GridWidth));
			CorrelationGrid.ColWidth(5, FCConvert.ToInt32(0.125 * GridWidth));
			CorrelationGrid.ColWidth(6, FCConvert.ToInt32(0.125 * GridWidth));
			CorrelationGrid.ColWidth(7, FCConvert.ToInt32(0.125 * GridWidth));
			CorrelationGrid.HeightOriginal = CorrelationGrid.RowHeight(2) * CorrelationGrid.Rows + 60;
            //FC:FINAL:AM:#3282 - align labels with columns
            int left = CorrelationGrid.LeftOriginal + CorrelationGrid.ColWidth(0);
            Label12.LeftOriginal = left + CorrelationGrid.ColWidth(1) / 2;
            left += CorrelationGrid.ColWidth(1);
            Label13.LeftOriginal = left + CorrelationGrid.ColWidth(2) / 2;
            left += CorrelationGrid.ColWidth(2);
            Label14.LeftOriginal = left + CorrelationGrid.ColWidth(3) / 2;
            left += CorrelationGrid.ColWidth(3);
            Label15.LeftOriginal = left + CorrelationGrid.ColWidth(4) / 2;
            left += CorrelationGrid.ColWidth(4);
            Label16.LeftOriginal = left + CorrelationGrid.ColWidth(5) / 2;
            left += CorrelationGrid.ColWidth(5);
            Label17.LeftOriginal = left + CorrelationGrid.ColWidth(6) / 2;
        }

		private void SetupTotalGrid()
		{
			TotalGrid.Cols = 7;
			TotalGrid.Rows = intNumCards + 1;
			TotalGrid.FixedRows = 1;
			TotalGrid.FixedCols = 0;
			TotalGrid.TextMatrix(0, 0, "CARD");
			TotalGrid.TextMatrix(0, 1, "Calculated Land");
			TotalGrid.TextMatrix(0, 2, "Calculated Building");
			TotalGrid.TextMatrix(0, 3, "Calculated Total");
			TotalGrid.TextMatrix(0, 4, "Land");
			TotalGrid.TextMatrix(0, 5, "Building");
			TotalGrid.TextMatrix(0, 6, "Total");
			//FC:FINAL:CHN - issue #1630: Fix grid alignment.
			TotalGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			TotalGrid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void ResizeTotalGrid()
		{
			int GridWidth;
			int lngHeight;
			GridWidth = TotalGrid.WidthOriginal;
			// TotalGrid.ColWidth(0) = 0.1 * GridWidth
			// TotalGrid.ColWidth(1) = 0.295 * GridWidth
			// TotalGrid.ColWidth(2) = 0.3 * GridWidth
			// TotalGrid.ColWidth(3) = 0.3 * GridWidth
			TotalGrid.ColWidth(0, FCConvert.ToInt32(0.095 * GridWidth));
			TotalGrid.ColWidth(1, FCConvert.ToInt32(0.15 * GridWidth));
			TotalGrid.ColWidth(2, FCConvert.ToInt32(0.15 * GridWidth));
			TotalGrid.ColWidth(3, FCConvert.ToInt32(0.15 * GridWidth));
			TotalGrid.ColWidth(4, FCConvert.ToInt32(0.15 * GridWidth));
			TotalGrid.ColWidth(5, FCConvert.ToInt32(0.15 * GridWidth));
			// TotalGrid.ColWidth(6) = 0.15 * GridWidth
			TotalGrid.ExtendLastCol = true;
            TotalGrid.HeightOriginal = TotalGrid.RowHeight(1) * TotalGrid.Rows + 60;
            lngHeight = SSTab1.HeightOriginal;
            if (TotalGrid.HeightOriginal + TotalGrid.TopOriginal >= lngHeight)
            {
                TotalGrid.HeightOriginal = lngHeight - TotalGrid.TopOriginal - TotalGrid.RowHeight(1);
            }
            //Application.DoEvents();
            this.Refresh();
			//Application.DoEvents();
		}
		// vbPorter upgrade warning: intNumberCards As short	OnWriteFCConvert.ToInt32(
		public void Init(ref int intNumberCards)
		{
			int intCounter;
			// Dim clsTemp As New clsDRWrapper
			try
			{
				// On Error GoTo ErrorHandler
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				intNumCards = intNumberCards;
				// Call clsTemp.OpenRecordset("select count(*) as thecount from master where not rsdeleted = 1 and rsaccount = " & gintLastAccountNumber, strredatabase)
				// gintMaxCards = clsTemp.Fields("thecount")
				// For intcounter = 0 To MAXCARDS - 1
				// optCardNumber(intcounter).Enabled = intcounter < intNumCards
				// If intcounter = intNumCards Then Exit For
				// Next
				// vsElasticLight1.Enabled = True
				// VScrollCard.Max = intNumCards
				//UpDownCard.Maximum = FCConvert.ToInt16(intNumCards);
				lblNumCards.Text = FCConvert.ToString(intNumCards);
				txtCardNumber.Text = FCConvert.ToString(1);
				if (intNumCards == 1)
					TotalGrid.Rows = 2;
				SetupGrids();
				FillTotalGrid(ref intNumCards);
				FillCorrelationGrid_2(1);
				FillGrids(1);
				BorderOutbuilding(intNumCards);
				borderProperty(ref intNumCards);
				// Call optCardNumber_MouseDown(0, 1, 0, 1, 1)
				// Call txtCardNumber_Validate(False)
				// Call VScrollCard_Validate(False)
				//FC:FINAL:MSH - i.issue #1059: trigger validating for initializing labels
				//UpDownCard_Validate(false);
				UpDownCard_Validating_2(false);
				if (!modGlobalVariables.Statics.boolRegRecords)
				{
					lblSalesRecord.Visible = true;
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				//Application.DoEvents();
				// 
				// frmValuationReport.Refresh
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Valuation Report_INIT", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		//FC:FINAL:MSH - issue #1643: resize SSTab1 depend on selected tab
		private void SSTab1_SelectedIndexChanged(object sender, EventArgs e) 
		{
			if(SSTab1.SelectedTab == SSTab1_Page1) 
			{
				SSTab1.Height = 650;
			}
			else if (SSTab1.SelectedTab == SSTab1_Page2)
			{
				SSTab1.Height = 650;
			}
			else if (SSTab1.SelectedTab == SSTab1_Page3)
			{
				SSTab1.Height = 650;
			}
			else if (SSTab1.SelectedTab == SSTab1_Page4)
			{
				SSTab1.Height = 340;
			}
			else if (SSTab1.SelectedTab == SSTab1_Page5)
			{
				SSTab1.Height = 650;
			}
			else if (SSTab1.SelectedTab == SSTab1_Page6)
			{
				SSTab1.Height = 650;
			}
		}

		// vbPorter upgrade warning: intcnum As short	OnWriteFCConvert.ToInt32(
		private void FillGrids(int intcnum)
		{
			FillSaleGrid(intcnum);
			FillMiscGrid(intcnum);
			FillPropertyGrid(intcnum);
			string vbPorterVar = modSpeedCalc.Statics.CalcPropertyList[intcnum].DwellingOrCommercial;
			if (vbPorterVar == "Y")
			{
				SSTab1.TabPages[1].Visible = true;
				SSTab1.TabPages[2].Visible = false;
				FillDwellDescGrid(intcnum);
				FillDwellMiscGrid(intcnum);
				FillDwellingGrid(intcnum);
				FillDwellCondGrid(intcnum);
				FillDwellPercGrid(intcnum);
			}
			else if (vbPorterVar == "C")
			{
				SSTab1.TabPages[1].Visible = false;
				SSTab1.TabPages[2].Visible = true;
				FillCommercialGrid(intcnum);
			}
			else if (vbPorterVar == "N")
			{
				SSTab1.TabPages[1].Visible = false;
				SSTab1.TabPages[2].Visible = false;
			}
			else
			{
				SSTab1.TabPages[1].Visible = false;
				SSTab1.TabPages[2].Visible = false;
			}
			FillOutbuildingGrid(intcnum);
			RefillCorrelationGrid(intcnum);
		}

		private void FillDwellingGrid(int intcnum)
		{
			DwellingGrid.TextMatrix(0, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Base);
			DwellingGrid.TextMatrix(1, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Trim);
			DwellingGrid.TextMatrix(2, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].RoofVal);
			DwellingGrid.TextMatrix(3, 0, DwellMiscGrid.TextMatrix(3, 0));
			DwellingGrid.TextMatrix(3, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Open3Val);
			DwellingGrid.TextMatrix(4, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Open4Val);
			DwellingGrid.TextMatrix(4, 0, DwellMiscGrid.TextMatrix(4, 0));
			DwellingGrid.TextMatrix(5, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].BasementVal);
			DwellingGrid.TextMatrix(6, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].FinBasementVal);
			DwellingGrid.TextMatrix(7, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].HeatVal);
			DwellingGrid.TextMatrix(8, 0, DwellMiscGrid.TextMatrix(8, 0));
			DwellingGrid.TextMatrix(8, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Open5Val);
			DwellingGrid.TextMatrix(10, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].PlumbingVal);
			DwellingGrid.TextMatrix(11, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].AtticVal);
			DwellingGrid.TextMatrix(12, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].FireplaceVal);
			DwellingGrid.TextMatrix(13, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].InsulationVal);
			DwellingGrid.TextMatrix(14, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].UnfinishedVal);
		}
		// vbPorter upgrade warning: intcnum As object	OnWriteFCConvert.ToInt16(
		private void FillDwellDescGrid(int intcnum)
		{
			DwellDescGrid.TextMatrix(0, 0, modSpeedCalc.Statics.CalcDwellList[intcnum].BuildingType);
			DwellDescGrid.TextMatrix(0, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Stories);
			DwellDescGrid.TextMatrix(1, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Exterior);
			DwellDescGrid.TextMatrix(2, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].DwellUnits);
			DwellDescGrid.TextMatrix(3, 0, modSpeedCalc.Statics.CalcDwellList[intcnum].Open3Title);
			DwellDescGrid.TextMatrix(3, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Open3);
			DwellDescGrid.TextMatrix(4, 0, modSpeedCalc.Statics.CalcDwellList[intcnum].Open4Title);
			DwellDescGrid.TextMatrix(4, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Open4);
			DwellDescGrid.TextMatrix(5, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Foundation);
			DwellDescGrid.TextMatrix(6, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].FinBasementArea);
			DwellDescGrid.TextMatrix(7, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Heating);
			DwellDescGrid.TextMatrix(8, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Rooms);
			DwellDescGrid.TextMatrix(9, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Bedrooms);
			DwellDescGrid.TextMatrix(10, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Bath);
			DwellDescGrid.TextMatrix(11, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Attic);
			DwellDescGrid.TextMatrix(12, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Fireplaces);
			DwellDescGrid.TextMatrix(13, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Insulation);
			DwellDescGrid.TextMatrix(14, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].UnfinLivingArea);
		}
		// vbPorter upgrade warning: intcnum As object	OnWriteFCConvert.ToInt16(
		private void FillDwellMiscGrid(int intcnum)
		{
			DwellMiscGrid.TextMatrix(0, 0, modSpeedCalc.Statics.CalcDwellList[intcnum].Sqft);
			DwellMiscGrid.TextMatrix(0, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Grade);
			DwellMiscGrid.TextMatrix(1, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].MasonryTrim);
			DwellMiscGrid.TextMatrix(2, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Roof);
			if (Conversion.Val(modSpeedCalc.Statics.CalcDwellList[intcnum].Open3Val) > 0)
			{
				DwellMiscGrid.TextMatrix(3, 0, modSpeedCalc.Statics.CalcDwellList[intcnum].Open3Title);
			}
			else
			{
				DwellMiscGrid.TextMatrix(3, 0, "");
			}
			DwellMiscGrid.TextMatrix(3, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Open3Val);
			if (Conversion.Val(modSpeedCalc.Statics.CalcDwellList[intcnum].Open4Val) > 0)
			{
				DwellMiscGrid.TextMatrix(4, 0, modSpeedCalc.Statics.CalcDwellList[intcnum].Open4Title);
			}
			else
			{
				DwellMiscGrid.TextMatrix(4, 0, "");
			}
			DwellMiscGrid.TextMatrix(4, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Open4Val);
			DwellMiscGrid.TextMatrix(5, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Basement);
			DwellMiscGrid.TextMatrix(6, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].BsmtGarage);
			DwellMiscGrid.TextMatrix(7, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Cooling);
			DwellMiscGrid.TextMatrix(8, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Open5);
			DwellMiscGrid.TextMatrix(8, 0, modSpeedCalc.Statics.CalcDwellList[intcnum].Open5Title);
			DwellMiscGrid.TextMatrix(9, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].AddnFixtures);
			DwellMiscGrid.TextMatrix(10, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].HalfBaths);
		}
		// vbPorter upgrade warning: intcnum As object	OnWriteFCConvert.ToInt16(
		private void FillDwellCondGrid(int intcnum)
		{
			DwellCondGrid.TextMatrix(1, 0, modSpeedCalc.Statics.CalcDwellList[intcnum].YearBuilt);
			DwellCondGrid.TextMatrix(1, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].Renovated);
			DwellCondGrid.TextMatrix(1, 2, modSpeedCalc.Statics.CalcDwellList[intcnum].Kitchens);
			DwellCondGrid.TextMatrix(1, 3, modSpeedCalc.Statics.CalcDwellList[intcnum].Baths);
			DwellCondGrid.TextMatrix(1, 4, modSpeedCalc.Statics.CalcDwellList[intcnum].Condition);
			DwellCondGrid.TextMatrix(1, 5, modSpeedCalc.Statics.CalcDwellList[intcnum].Layout);
			DwellCondGrid.TextMatrix(1, 6, modSpeedCalc.Statics.CalcDwellList[intcnum].Total);
		}

		private void FillOutbuildingGrid(int intcnum)
		{
			int x;
			int intHighestBuilding = 0;
			SetupOutbuildingGrid();
			intHighestBuilding = 0;
			for (x = 1; x <= 10; x++)
			{
				if (Strings.Trim(modSpeedCalc.Statics.CalcOutList[intcnum].Description[x]) != string.Empty)
				{
					intHighestBuilding = x;
				}
			}
			// x
			if (intHighestBuilding == 0)
			{
				// hide or disable tab
				SSTab1.TabPages[3].Visible = false;
			}
			else
			{
				SSTab1.TabPages[3].Visible = true;
				for (x = 1; x <= intHighestBuilding; x++)
				{
					if (modSpeedCalc.Statics.CalcOutList[intcnum].Units[x] == "    - - - - S O U N D  V A L U E - - - -    ")
					{
						// OutBuildingGrid.Rows = OutBuildingGrid.Rows + 1
						// OutBuildingGrid.MergeCol(2) = True
						// OutBuildingGrid.MergeCol(3) = True
						// OutBuildingGrid.MergeCol(4) = True
						// OutBuildingGrid.MergeCol(5) = True
						// OutBuildingGrid.MergeCol(6) = True
						// OutBuildingGrid.MergeCol(7) = True
						// OutBuildingGrid.MergeCol(8) = True
						OutBuildingGrid.AddItem(modSpeedCalc.Statics.CalcOutList[intcnum].Description[x] + "\t" + FCConvert.ToString(modSpeedCalc.Statics.CalcOutList[intcnum].Year[x]) + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Units[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Units[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Units[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Units[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Units[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Units[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Units[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].OutbuildingValue[x]);
						//FC:FINAL:MSH - i.issue #1328: don't merge all cells to avoid missing data
						//OutBuildingGrid.MergeRow(x, true);
						OutBuildingGrid.MergeRow(x, true, 2, 8);
						OutBuildingGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, x, 2, x, 8, FCGrid.AlignmentSettings.flexAlignCenterCenter);
						// OutBuildingGrid.TextMatrix(x, 0) = .Description(x)
						// OutBuildingGrid.TextMatrix(x, 1) = .Year(x)
						// OutBuildingGrid.TextMatrix(x, 2) = .Units(x)
						// OutBuildingGrid.TextMatrix(x, 3) = .Units(x)
						// OutBuildingGrid.TextMatrix(x, 4) = .Units(x)
						// OutBuildingGrid.TextMatrix(x, 4) = .Units(x)
						// OutBuildingGrid.TextMatrix(x, 5) = .Units(x)
						// OutBuildingGrid.TextMatrix(x, 6) = .Units(x)
						// OutBuildingGrid.TextMatrix(x, 7) = .Units(x)
						// OutBuildingGrid.TextMatrix(x, 8) = .Units(x)
						// OutBuildingGrid.TextMatrix(x, 9) = .OutbuildingValue(x)
						// OutBuildingGrid.MergeRow(x) = False
					}
					else
					{
						OutBuildingGrid.AddItem(modSpeedCalc.Statics.CalcOutList[intcnum].Description[x] + "\t" + FCConvert.ToString(modSpeedCalc.Statics.CalcOutList[intcnum].Year[x]) + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Units[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Grade[x] + "\t" + FCConvert.ToString(modSpeedCalc.Statics.CalcOutList[intcnum].Recon[x]) + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Condition[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Physcial[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Functional[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].Economic[x] + "\t" + modSpeedCalc.Statics.CalcOutList[intcnum].OutbuildingValue[x]);
						//FC:FINAL:MSH - i.issue #1328: don't need to merge cells (different implementation between web and VB6)
						//OutBuildingGrid.MergeRow(x, false);
						OutBuildingGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, x, 2, x, 8, FCGrid.AlignmentSettings.flexAlignRightCenter);
					}
				}
				// x
				OutBuildingGrid.Rows += 3;
				//FC:FINAL:MSH - i.issue #1328: merge cells to display the text in full
				OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 2, 6, "OUTBUILDING TOTAL");
				//OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 2, 7, "OUTBUILDING TOTAL");
				//OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 2, 8, "OUTBUILDING TOTAL");
				OutBuildingGrid.MergeRow(OutBuildingGrid.Rows - 2, true, 6, 8);
				//FC:FINAL:MSH - i.issue #1328: merge cells to display the text in full
				OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 1, 6, "BUILDING TOTAL");
				//OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 1, 7, "BUILDING TOTAL");
				//OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 1, 8, "BUILDING TOTAL");
				OutBuildingGrid.MergeRow(OutBuildingGrid.Rows - 1, true, 6, 8);
				OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 2, 9, modSpeedCalc.Statics.CalcOutList[intcnum].TotalOutbuilding);
				OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 1, 9, modSpeedCalc.Statics.CalcOutList[intcnum].TotalBuilding);
				OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 2, 0, modSpeedCalc.Statics.CalcOutList[intcnum].SFLA);
				OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 2, 1, modSpeedCalc.Statics.CalcOutList[intcnum].SFLA);
				OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 2, 2, modSpeedCalc.Statics.CalcOutList[intcnum].SFLAPerVal);
				OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 2, 3, modSpeedCalc.Statics.CalcOutList[intcnum].SFLAPerVal);
				OutBuildingGrid.TextMatrix(OutBuildingGrid.Rows - 2, 4, modSpeedCalc.Statics.CalcOutList[intcnum].SFLAPerVal);
                //FC:FINAL:MSH - i.issue #1328: don't merge cells (to avoid missing data)
                //OutBuildingGrid.MergeRow(OutBuildingGrid.Rows - 2, true);
                //OutBuildingGrid.MergeRow(OutBuildingGrid.Rows - 1, true);
                //FC:FINAL:MSH - issue #1643: merge part of cells to avoid displaying duplicate data (as in original)
                OutBuildingGrid.MergeRow(OutBuildingGrid.Rows - 2, true, 0, 1);
                OutBuildingGrid.MergeRow(OutBuildingGrid.Rows - 2, true, 2, 4);
			}
			//OutBuildingGrid.MergeCompare = FCGrid.MergeCompareSettings.flexMCExact;
			OutBuildingGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
			ResizeOutbuildingGrid();
			BorderOutbuilding(intcnum);
		}

		private void FillCommercialGrid(int intcnum)
		{
			CommercialGrid.TextMatrix(0, 2, modSpeedCalc.Statics.CalcCommList[intcnum].OccupancyType[1]);
			CommercialGrid.TextMatrix(1, 2, modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[1]);
			CommercialGrid.TextMatrix(2, 2, modSpeedCalc.Statics.CalcCommList[intcnum].DwellUnits[1]);
			CommercialGrid.TextMatrix(3, 2, modSpeedCalc.Statics.CalcCommList[intcnum].Exterior[1]);
			CommercialGrid.TextMatrix(4, 2, modSpeedCalc.Statics.CalcCommList[intcnum].Stories[1]);
			CommercialGrid.TextMatrix(5, 2, modSpeedCalc.Statics.CalcCommList[intcnum].HeatingCool[1]);
			CommercialGrid.TextMatrix(6, 2, modSpeedCalc.Statics.CalcCommList[intcnum].Built[1]);
			CommercialGrid.TextMatrix(7, 2, modSpeedCalc.Statics.CalcCommList[intcnum].Remodeled[1]);
			CommercialGrid.TextMatrix(8, 2, modSpeedCalc.Statics.CalcCommList[intcnum].BaseCost[1]);
			CommercialGrid.TextMatrix(9, 2, modSpeedCalc.Statics.CalcCommList[intcnum].HeatCoolSqft[1]);
			CommercialGrid.TextMatrix(10, 2, modSpeedCalc.Statics.CalcCommList[intcnum].Total[1]);
			CommercialGrid.TextMatrix(11, 2, modSpeedCalc.Statics.CalcCommList[intcnum].SizeFactor[1]);
			CommercialGrid.TextMatrix(12, 2, modSpeedCalc.Statics.CalcCommList[intcnum].AdjustedCost[1]);
			CommercialGrid.TextMatrix(13, 2, modSpeedCalc.Statics.CalcCommList[intcnum].TotalSqft[1]);
			CommercialGrid.TextMatrix(14, 2, modSpeedCalc.Statics.CalcCommList[intcnum].ReplacementCost[1]);
			CommercialGrid.TextMatrix(15, 2, modSpeedCalc.Statics.CalcCommList[intcnum].Condition[1]);
			CommercialGrid.TextMatrix(16, 2, modSpeedCalc.Statics.CalcCommList[intcnum].Physical[1]);
			CommercialGrid.TextMatrix(17, 2, modSpeedCalc.Statics.CalcCommList[intcnum].Functional[1]);
			CommercialGrid.TextMatrix(18, 2, modSpeedCalc.Statics.CalcCommList[intcnum].subtotal[1]);
			CommercialGrid.TextMatrix(19, 2, modSpeedCalc.Statics.CalcCommList[intcnum].EconFactor);
			CommercialGrid.TextMatrix(20, 2, modSpeedCalc.Statics.CalcCommList[intcnum].TotalValue);
			if (modSpeedCalc.Statics.CalcCommList[intcnum].OccupancyType[2] != string.Empty)
			{
				CommercialGrid.TextMatrix(0, 3, modSpeedCalc.Statics.CalcCommList[intcnum].OccupancyType[2]);
				CommercialGrid.TextMatrix(1, 3, modSpeedCalc.Statics.CalcCommList[intcnum].ClassQuality[2]);
				CommercialGrid.TextMatrix(2, 3, modSpeedCalc.Statics.CalcCommList[intcnum].DwellUnits[2]);
				CommercialGrid.TextMatrix(3, 3, modSpeedCalc.Statics.CalcCommList[intcnum].Exterior[2]);
				CommercialGrid.TextMatrix(4, 3, modSpeedCalc.Statics.CalcCommList[intcnum].Stories[2]);
				CommercialGrid.TextMatrix(5, 3, modSpeedCalc.Statics.CalcCommList[intcnum].HeatingCool[2]);
				CommercialGrid.TextMatrix(6, 3, modSpeedCalc.Statics.CalcCommList[intcnum].Built[2]);
				CommercialGrid.TextMatrix(7, 3, modSpeedCalc.Statics.CalcCommList[intcnum].Remodeled[2]);
				CommercialGrid.TextMatrix(8, 3, modSpeedCalc.Statics.CalcCommList[intcnum].BaseCost[2]);
				CommercialGrid.TextMatrix(9, 3, modSpeedCalc.Statics.CalcCommList[intcnum].HeatCoolSqft[2]);
				CommercialGrid.TextMatrix(10, 3, modSpeedCalc.Statics.CalcCommList[intcnum].Total[2]);
				CommercialGrid.TextMatrix(11, 3, modSpeedCalc.Statics.CalcCommList[intcnum].SizeFactor[2]);
				CommercialGrid.TextMatrix(12, 3, modSpeedCalc.Statics.CalcCommList[intcnum].AdjustedCost[2]);
				CommercialGrid.TextMatrix(13, 3, modSpeedCalc.Statics.CalcCommList[intcnum].TotalSqft[2]);
				CommercialGrid.TextMatrix(14, 3, modSpeedCalc.Statics.CalcCommList[intcnum].ReplacementCost[2]);
				CommercialGrid.TextMatrix(15, 3, modSpeedCalc.Statics.CalcCommList[intcnum].Condition[2]);
				CommercialGrid.TextMatrix(16, 3, modSpeedCalc.Statics.CalcCommList[intcnum].Physical[2]);
				CommercialGrid.TextMatrix(17, 3, modSpeedCalc.Statics.CalcCommList[intcnum].Functional[2]);
				CommercialGrid.TextMatrix(18, 3, modSpeedCalc.Statics.CalcCommList[intcnum].subtotal[2]);
				CommercialGrid.TextMatrix(19, 3, modSpeedCalc.Statics.CalcCommList[intcnum].EconFactor);
				// CommercialGrid.TextMatrix(20, 3) = .TotalValue
				CommercialGrid.TextMatrix(20, 3, CommercialGrid.TextMatrix(20, 2));
				CommercialGrid.MergeRow(20, true, 2, 3);
				CommercialGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				CommercialGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 20, 2, 20, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				CommercialGrid.TextMatrix(20, 3, CommercialGrid.TextMatrix(20, 2));
			}
			else
			{
				CommercialGrid.TextMatrix(0, 3, "");
				CommercialGrid.TextMatrix(1, 3, "");
				CommercialGrid.TextMatrix(2, 3, "");
				CommercialGrid.TextMatrix(3, 3, "");
				CommercialGrid.TextMatrix(4, 3, "");
				CommercialGrid.TextMatrix(5, 3, "");
				CommercialGrid.TextMatrix(6, 3, "");
				CommercialGrid.TextMatrix(7, 3, "");
				CommercialGrid.TextMatrix(8, 3, "");
				CommercialGrid.TextMatrix(9, 3, "");
				CommercialGrid.TextMatrix(10, 3, "");
				CommercialGrid.TextMatrix(11, 3, "");
				CommercialGrid.TextMatrix(12, 3, "");
				CommercialGrid.TextMatrix(13, 3, "");
				CommercialGrid.TextMatrix(14, 3, "");
				CommercialGrid.TextMatrix(15, 3, "");
				CommercialGrid.TextMatrix(16, 3, "");
				CommercialGrid.TextMatrix(17, 3, "");
				CommercialGrid.TextMatrix(18, 3, "");
				CommercialGrid.TextMatrix(19, 3, "");
				CommercialGrid.TextMatrix(20, 3, "");
				CommercialGrid.MergeRow(2, false);
				CommercialGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 20, 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
				CommercialGrid.TextMatrix(20, 3, "");
			}
		}

		private void FillDwellPercGrid(int intcnum)
		{
			DwellPercGrid.TextMatrix(1, 0, modSpeedCalc.Statics.CalcDwellList[intcnum].FuncObs);
			DwellPercGrid.TextMatrix(1, 1, modSpeedCalc.Statics.CalcDwellList[intcnum].EconObs);
			DwellPercGrid.TextMatrix(1, 2, modSpeedCalc.Statics.CalcDwellList[intcnum].PhysPerc);
			DwellPercGrid.TextMatrix(1, 3, modSpeedCalc.Statics.CalcDwellList[intcnum].FuncPerc);
			DwellPercGrid.TextMatrix(1, 4, modSpeedCalc.Statics.CalcDwellList[intcnum].EconPerc);
			DwellPercGrid.TextMatrix(1, 5, modSpeedCalc.Statics.CalcDwellList[intcnum].DwellValue);
		}

		private void FillSaleGrid(int intcnum)
		{
			SaleGrid.TextMatrix(0, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].SaleDate);
			SaleGrid.TextMatrix(1, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].SalePrice);
			SaleGrid.TextMatrix(2, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].SaleType);
			SaleGrid.TextMatrix(3, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Financing);
			SaleGrid.TextMatrix(4, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Verified);
			SaleGrid.TextMatrix(5, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Validity);
		}

		private void FillMiscGrid(int intcnum)
		{
			MiscGrid.TextMatrix(0, 0, "Neighborhood " + FCConvert.ToString(modSpeedCalc.Statics.CalcPropertyList[intcnum].NeighborhoodCode));
			MiscGrid.TextMatrix(0, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].NeighborhoodDesc);
			MiscGrid.TextMatrix(1, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].TreeGrowth);
			if (Strings.Trim(modSpeedCalc.Statics.CalcPropertyList[intcnum].TreeGrowth) != string.Empty)
			{
				MiscGrid.TextMatrix(1, 0, "Tree Growth (Year)");
			}
			MiscGrid.TextMatrix(2, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Zone);
			MiscGrid.TextMatrix(3, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Topography);
			MiscGrid.TextMatrix(4, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Utilities);
			MiscGrid.TextMatrix(5, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Street);
			MiscGrid.TextMatrix(6, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Open1);
			MiscGrid.TextMatrix(6, 0, modSpeedCalc.Statics.CalcPropertyList[intcnum].Open1Title);
			MiscGrid.TextMatrix(7, 0, modSpeedCalc.Statics.CalcPropertyList[intcnum].Open2Title);
			MiscGrid.TextMatrix(7, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Open2);
			MiscGrid.TextMatrix(8, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Ref1);
			MiscGrid.TextMatrix(9, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].Ref2);
			MiscGrid.TextMatrix(10, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].TranLandBldgCode);
			MiscGrid.TextMatrix(10, 0, modSpeedCalc.Statics.CalcPropertyList[intcnum].TranLandBldgDesc);
			MiscGrid.TextMatrix(11, 0, modSpeedCalc.Statics.CalcPropertyList[intcnum].XCoordTitle);
			MiscGrid.TextMatrix(11, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].XCoord);
			MiscGrid.TextMatrix(12, 0, modSpeedCalc.Statics.CalcPropertyList[intcnum].YCoordTitle);
			MiscGrid.TextMatrix(12, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].YCoord);
			MiscGrid.TextMatrix(13, 1, modSpeedCalc.Statics.CalcPropertyList[intcnum].ExemptCodes);
			if (Strings.Trim(modSpeedCalc.Statics.CalcPropertyList[intcnum].ExemptCodes) != string.Empty)
			{
				MiscGrid.TextMatrix(13, 0, "Exempt Codes");
			}
			MiscGrid.TextMatrix(14, 1, FCConvert.ToString(modSpeedCalc.Statics.CalcPropertyList[intcnum].LandSchedule));
		}

		private void FillPropertyGrid(int intcnum)
		{
			int x;
			object dblPercent;
			SetupPropertyGrid();
			for (x = 1; x <= 7; x++)
			{
				if (modSpeedCalc.Statics.CalcPropertyList[intcnum].LandType[x] > 0)
				{
					// If Trim(.LandDesc(x)) <> vbNullString Then
					if (modSpeedCalc.Statics.CalcPropertyList[intcnum].LandType[x] != 99)
					{
						PropertyGrid.AddItem(modSpeedCalc.Statics.CalcPropertyList[intcnum].LandUnits[x] + "\t" + modSpeedCalc.Statics.CalcPropertyList[intcnum].LandDesc[x] + "\t" + modSpeedCalc.Statics.CalcPropertyList[intcnum].LandPricePerUnit[x] + "\t" + modSpeedCalc.Statics.CalcPropertyList[intcnum].LandTotal[x] + "\t" + modSpeedCalc.Statics.CalcPropertyList[intcnum].LandFctr[x] + "\t" + modSpeedCalc.Statics.CalcPropertyList[intcnum].LandInfluence[x] + "\t" + modSpeedCalc.Statics.CalcPropertyList[intcnum].LandValue[x]);
					}
					else
					{
						// If x > 1 Then
						// if not then there should be no 99 type here
						// PropertyGrid.TextMatrix(PropertyGrid.Rows - 1, 6) = .LandValue(x)
						// PropertyGrid.TextMatrix(PropertyGrid.Rows - 1, 5) = .LandInfluence(x)
						// dblPercent = ((Val(.LandFctr(x)) / 100) * (Val(.LandFctr(x - 1)) / 100)) * 100
						// If dblPercent = CInt(dblPercent) Then
						// PropertyGrid.TextMatrix(PropertyGrid.Rows - 1, 4) = Format(dblPercent, "0") & "%"
						// Else
						// PropertyGrid.TextMatrix(PropertyGrid.Rows - 1, 4) = Format(dblPercent, "0.#") & "%"
						// End If
						// End If
						PropertyGrid.AddItem("\t" + "\t" + "\t" + "\t" + modSpeedCalc.Statics.CalcPropertyList[intcnum].LandFctr[x] + "\t" + modSpeedCalc.Statics.CalcPropertyList[intcnum].LandInfluence[x] + "\t" + modSpeedCalc.Statics.CalcPropertyList[intcnum].LandValue[x]);
					}
				}
			}
			// x
			PropertyGrid.AddItem(modSpeedCalc.Statics.CalcPropertyList[intcnum].Acres);
			PropertyGrid.TextMatrix(PropertyGrid.Rows - 1, 5, "Land Total");
			PropertyGrid.TextMatrix(PropertyGrid.Rows - 1, 6, modSpeedCalc.Statics.CalcPropertyList[intcnum].TotalLandValue);
			PropertyGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
            //FC:FINAL:AM:#1681 - merge the first 2 columns
            PropertyGrid.MergeRow(PropertyGrid.Rows - 1, true, 0, 1);
            borderProperty(ref intcnum);
			ResizePropertyGrid();
		}
		// Private Sub FillMiscGrid(intCNum As Integer)
		// With CalcPropertyList(intCNum)
		// MiscGrid.TextMatrix(1, 0) = ""
		// If Trim(.TreeGrowth) <> vbNullString Then
		// MiscGrid.TextMatrix(1, 0) = "Tree Growth (Year)"
		// End If
		// End With
		// End Sub
		private void FillTotalGrid(ref int intcnum)
		{
			int x;
			int lngLand;
			int lngbldg;
			int lngTot;
			int lngCurLand;
			int lngCurBldg;
			int lngTotCur;
			lngLand = 0;
			lngbldg = 0;
			lngTot = 0;
			lngCurLand = 0;
			lngCurBldg = 0;
			lngTotCur = 0;
			for (x = 1; x <= intcnum; x++)
			{
				TotalGrid.TextMatrix(x, 0, FCConvert.ToString(x));
				TotalGrid.TextMatrix(x, 1, Strings.Format(modProperty.Statics.lngCurrentLand[x], "#,###,##0"));
				TotalGrid.TextMatrix(x, 2, Strings.Format(modProperty.Statics.lngCurrentBldg[x], "#,###,###,##0"));
				TotalGrid.TextMatrix(x, 4, Strings.Format(modProperty.Statics.lngCorrelatedLand[x], "#,###,##0"));
				TotalGrid.TextMatrix(x, 5, Strings.Format(modProperty.Statics.lngCorrelatedBldg[x], "#,###,###,##0"));
				lngLand += modProperty.Statics.lngCorrelatedLand[x];
				lngbldg += modProperty.Statics.lngCorrelatedBldg[x];
				lngTot = lngLand + lngbldg;
				lngCurLand += modProperty.Statics.lngCurrentLand[x];
				lngCurBldg += modProperty.Statics.lngCurrentBldg[x];
				lngTotCur = lngCurBldg + lngCurLand;
				TotalGrid.TextMatrix(x, 3, Strings.Format(modProperty.Statics.lngCurrentLand[x] + modProperty.Statics.lngCurrentBldg[x], "#,###,###,##0"));
				TotalGrid.TextMatrix(x, 6, Strings.Format(modProperty.Statics.lngCorrelatedLand[x] + modProperty.Statics.lngCorrelatedBldg[x], "#,###,###,##0"));
			}
			// x
			if (TotalGrid.TextMatrix(TotalGrid.Rows - 1, 0) == "TOTAL")
			{
				TotalGrid.TextMatrix(TotalGrid.Rows - 1, 4, Strings.Format(lngLand, "#,###,###,##0"));
				TotalGrid.TextMatrix(TotalGrid.Rows - 1, 5, Strings.Format(lngbldg, "#,###,###,##0"));
				TotalGrid.TextMatrix(TotalGrid.Rows - 1, 6, Strings.Format(lngTot, "#,###,###,##0"));
				TotalGrid.Row = 0;
				TotalGrid.Col = 3;
				TotalGrid.RowSel = TotalGrid.Rows - 1;
				TotalGrid.ColSel = 3;
				TotalGrid.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, 2, -1, -1, -1);
				TotalGrid.Row = 0;
				TotalGrid.Col = 0;
				TotalGrid.RowSel = TotalGrid.Rows - 1;
				TotalGrid.ColSel = 0;
				TotalGrid.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, 2, -1, -1, -1);
			}
			else
			{
				TotalGrid.AddItem("TOTAL" + "\t" + Strings.Format(lngCurLand, "#,###,###,##0") + "\t" + Strings.Format(lngCurBldg, "#,###,###,##0") + "\t" + Strings.Format(lngTotCur, "#,###,###,##0") + "\t" + Strings.Format(lngLand, "#,###,###,##0") + "\t" + Strings.Format(lngbldg, "#,###,###,##0") + "\t" + Strings.Format(lngTot, "#,###,###,##0"));
				TotalGrid.Row = TotalGrid.Rows - 1;
				TotalGrid.RowSel = TotalGrid.Row;
				TotalGrid.Col = 0;
				TotalGrid.ColSel = 6;
				TotalGrid.CellBorder(Color.FromArgb(1, 1, 1), -1, 2, -1, -1, -1, -1);
				TotalGrid.Row = 0;
				TotalGrid.Col = 3;
				TotalGrid.RowSel = TotalGrid.Rows - 1;
				TotalGrid.ColSel = 3;
				TotalGrid.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, 2, -1, -1, -1);
				TotalGrid.Row = 0;
				TotalGrid.Col = 0;
				TotalGrid.RowSel = TotalGrid.Rows - 1;
				TotalGrid.ColSel = 0;
				TotalGrid.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, 2, -1, -1, -1);
			}
		}
		// vbPorter upgrade warning: intcnum As short	OnWriteFCConvert.ToInt32(
		private void FillCorrelationGrid_2(int intcnum)
		{
			FillCorrelationGrid(ref intcnum);
		}

		private void FillCorrelationGrid(ref int intcnum)
		{
			int x;
			clsDRWrapper clsLoadIncomeData = new clsDRWrapper();
			x = intcnum;
			CorrelationGrid.TextMatrix(2, 1, Strings.Format(modProperty.Statics.lngCurrentLand[x], "#,###,##0"));
			CorrelationGrid.TextMatrix(3, 1, Strings.Format(modProperty.Statics.lngCurrentBldg[x], "#,###,##0"));
			CorrelationGrid.TextMatrix(4, 1, Strings.Format(modProperty.Statics.lngCurrentLand[x] + modProperty.Statics.lngCurrentBldg[x], "#,###,##0"));
			CorrelationGrid.TextMatrix(2, 2, Strings.Format(modProperty.Statics.lngPreviousLand[x], "#,###,##0"));
			CorrelationGrid.TextMatrix(3, 2, Strings.Format(modProperty.Statics.lngPreviousBldg[x], "#,###,##0"));
			CorrelationGrid.TextMatrix(4, 2, Strings.Format(modProperty.Statics.lngPreviousLand[x] + modProperty.Statics.lngPreviousBldg[x], "#,###,##0"));
			clsLoadIncomeData.OpenRecordset("select indicatedvalue from incomevalue where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
			if (!clsLoadIncomeData.EndOfFile())
			{
				modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(Conversion.Val(clsLoadIncomeData.Get_Fields_Int32("indicatedvalue")) - modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard]);
			}
			else
			{
				modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard] = 0;
			}
			if (modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard] <= 0 && modProperty.Statics.intOverRideCodeBldg[x] != 4)
			{
				CorrelationGrid.TextMatrix(2, 4, "");
				CorrelationGrid.TextMatrix(3, 4, "");
				CorrelationGrid.TextMatrix(4, 4, "");
			}
			else
			{
				CorrelationGrid.TextMatrix(2, 4, Strings.Format(modProperty.Statics.lngIncomeLand[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
				CorrelationGrid.TextMatrix(3, 4, Strings.Format(modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
				CorrelationGrid.TextMatrix(4, 4, Strings.Format(modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard] + modProperty.Statics.lngIncomeLand[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
			}
			CorrelationGrid.TextMatrix(2, 5, Strings.Format(modProperty.Statics.lngAcceptedLand[x], "#,###,##0"));
			CorrelationGrid.TextMatrix(3, 5, Strings.Format(modProperty.Statics.lngAcceptedBldg[x], "#,###,##0"));
			if (modProperty.Statics.intOverRideCodeLand[x] == 6)
			{
				CorrelationGrid.TextMatrix(2, 6, Strings.Format(modProperty.Statics.lngOverRideLand[x], "#,###,##0"));
			}
			if (modProperty.Statics.intOverRideCodeBldg[x] == 6)
			{
				CorrelationGrid.TextMatrix(3, 6, Strings.Format(modProperty.Statics.lngOverRideBldg[x], "#,###,##0"));
			}
			CorrelationGrid.TextMatrix(4, 5, Strings.Format(modProperty.Statics.lngAcceptedLand[x] + modProperty.Statics.lngAcceptedBldg[x], "#,###,##0"));
			CorrelationGrid.TextMatrix(2, 7, Strings.Format(modProperty.Statics.lngCorrelatedLand[x], "#,###,##0"));
			CorrelationGrid.TextMatrix(3, 7, Strings.Format(modProperty.Statics.lngCorrelatedBldg[x], "#,###,##0"));
			CorrelationGrid.TextMatrix(4, 7, Strings.Format(modProperty.Statics.lngCorrelatedLand[x] + modProperty.Statics.lngCorrelatedBldg[x], "#,###,##0"));
			txtBldgFrom.Text = FCConvert.ToString(FCConvert.ToInt16(modProperty.Statics.intOverRideCodeBldg[x]));
			txtLandFrom.Text = FCConvert.ToString(FCConvert.ToInt16(modProperty.Statics.intOverRideCodeLand[x]));
		}
		// vbPorter upgrade warning: intcnum As object	OnWriteFCConvert.ToInt32(
		private void BorderOutbuilding(int intcnum)
		{
			int x;
			// For x = 0 To intcnum - 1
			if (OutBuildingGrid.Rows > 1)
			{
				OutBuildingGrid.Row = OutBuildingGrid.Rows - 2;
				OutBuildingGrid.Col = OutBuildingGrid.Cols - 4;
				OutBuildingGrid.RowSel = OutBuildingGrid.Rows - 1;
				OutBuildingGrid.ColSel = OutBuildingGrid.Cols - 1;
				OutBuildingGrid.CellBorder(Color.FromArgb(1, 1, 1), 2, 2, 2, 2, -1, -1);
			}
			// Next x
		}
		// vbPorter upgrade warning: intcnum As object	OnWriteFCConvert.ToInt32(
		private void borderProperty(ref int intcnum)
		{
			int x;
			// For x = 0 To intcnum - 1
			if (PropertyGrid.Rows > 1)
			{
				PropertyGrid.Row = PropertyGrid.Rows - 1;
				PropertyGrid.Col = 0;
				PropertyGrid.ColSel = PropertyGrid.Cols - 1;
				PropertyGrid.CellBorder(Color.FromArgb(1, 1, 1), -1, 2, -1, -1, -1, -1);
			}
			// Next x
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			mnuQuit_Click();
		}

		private void txtBldgFrom_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (((KeyCode >= Keys.D1) && (KeyCode <= Keys.D6)) || ((KeyCode >= Keys.NumPad1) && (KeyCode <= Keys.NumPad6)))
			{
				switch (KeyCode)
				{
					case Keys.D1:
					case Keys.NumPad1:
						{
							txtBldgFrom.Text = "1";
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
							break;
						}
					case Keys.D2:
					case Keys.NumPad2:
						{
							txtBldgFrom.Text = "2";
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
							break;
						}
					case Keys.D3:
					case Keys.NumPad3:
						{
							txtBldgFrom.Text = "3";
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
							break;
						}
					case Keys.D4:
					case Keys.NumPad4:
						{
							txtBldgFrom.Text = "4";
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
							break;
						}
					case Keys.D5:
					case Keys.NumPad5:
						{
							// KeyCode = 0
							break;
						}
					case Keys.D6:
					case Keys.NumPad6:
						{
							txtBldgFrom.Text = "6";
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
							break;
						}
				}
				//end switch
				KeyCode = (Keys)0;
			}
            //FC:FINAL:AM:#3284 - reset the value to 1
			else if(KeyCode != Keys.Tab && KeyCode != Keys.Enter)
			{
                //KeyCode = (Keys)0;
                txtBldgFrom.Text = "1";
			}
		}

		private void txtBldgFrom_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtBldgFrom_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			KeyCode = (Keys)0;
		}

		private void txtCardNumber_TextChanged(object sender, System.EventArgs e)
		{
            // DFK
            //FC:FINAL:AM:#3291 - updowncard removed; validate the card here
            Validate_Card();
        }

		private void UpDownCard_ValueChanged(object sender, System.EventArgs e)
		{
			//txtCardNumber.Text = FCConvert.ToString(FCConvert.ToInt32(UpDownCard.Value);
			Validate_Card();
		}

		private void txtLandFrom_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (((KeyCode >= Keys.D1) && (KeyCode <= Keys.D6)) || ((KeyCode >= Keys.NumPad1) && (KeyCode <= Keys.NumPad6)))
			{
				switch (KeyCode)
				{
					case Keys.D1:
					case Keys.NumPad1:
						{
							txtLandFrom.Text = "1";
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
							break;
						}
					case Keys.D2:
					case Keys.NumPad2:
						{
							txtLandFrom.Text = "2";
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
							break;
						}
					case Keys.D3:
					case Keys.NumPad3:
						{
							txtLandFrom.Text = "3";
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
							break;
						}
					case Keys.D4:
					case Keys.NumPad4:
						{
							txtLandFrom.Text = "1";
							KeyCode = (Keys)0;
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
							break;
						}
					case Keys.D5:
					case Keys.NumPad5:
						{
							// KeyCode = 0
							break;
						}
					case Keys.D6:
					case Keys.NumPad6:
						{
							txtLandFrom.Text = "6";
							RefillCorrelationGrid(modGlobalVariables.Statics.intCurrentCard);
							break;
						}
				}
				//end switch
				// If Not KeyCode = vbKey5 Then
				// Call RefillCorrelationGrid
				// End If
				KeyCode = (Keys)0;
			}
            else if (KeyCode != Keys.Tab && KeyCode != Keys.Enter)
            {
                //KeyCode = (Keys)0;
                txtLandFrom.Text = "1";
			}
		}

		private void txtLandFrom_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLandFrom_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			KeyCode = (Keys)0;
		}
		// vbPorter upgrade warning: intcnum As short	OnWriteFCConvert.ToInt32(
		private void RefillCorrelationGrid(int intcnum)
		{
			clsDRWrapper clsLoadIncomeData = new clsDRWrapper();
			modProperty.Statics.intOverRideCodeBldg[intcnum] = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBldgFrom.Text)));
			modProperty.Statics.intOverRideCodeLand[intcnum] = FCConvert.ToInt32(Math.Round(Conversion.Val(txtLandFrom.Text)));
			CorrelationGrid.TextMatrix(2, 1, Strings.Format(modProperty.Statics.lngCurrentLand[intcnum], "#,###,##0"));
			CorrelationGrid.TextMatrix(3, 1, Strings.Format(modProperty.Statics.lngCurrentBldg[intcnum], "#,###,##0"));
			CorrelationGrid.TextMatrix(4, 1, Strings.Format(modProperty.Statics.lngCurrentLand[intcnum] + modProperty.Statics.lngCurrentBldg[intcnum], "#,###,##0"));
			CorrelationGrid.TextMatrix(2, 2, Strings.Format(modProperty.Statics.lngPreviousLand[intcnum], "#,###,##0"));
			CorrelationGrid.TextMatrix(3, 2, Strings.Format(modProperty.Statics.lngPreviousBldg[intcnum], "#,###,##0"));
			CorrelationGrid.TextMatrix(4, 2, Strings.Format(modProperty.Statics.lngPreviousLand[intcnum] + modProperty.Statics.lngPreviousBldg[intcnum], "#,###,##0"));
			clsLoadIncomeData.OpenRecordset("select indicatedvalue from incomevalue where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = " + FCConvert.ToString(intcnum), modGlobalVariables.strREDatabase);
			if (!clsLoadIncomeData.EndOfFile())
			{
				modProperty.Statics.lngIncomeBldg[intcnum] = FCConvert.ToInt32(Conversion.Val(clsLoadIncomeData.Get_Fields_Int32("indicatedvalue")) - modProperty.Statics.lngAcceptedLand[intcnum]);
			}
			else
			{
				modProperty.Statics.lngIncomeBldg[intcnum] = 0;
			}
			if (modProperty.Statics.lngIncomeBldg[intcnum] <= 0 && modProperty.Statics.intOverRideCodeBldg[intcnum] != 4)
			{
				CorrelationGrid.TextMatrix(2, 4, "");
				CorrelationGrid.TextMatrix(3, 4, "");
				CorrelationGrid.TextMatrix(4, 4, "");
			}
			else
			{
				CorrelationGrid.TextMatrix(2, 4, Strings.Format(modProperty.Statics.lngIncomeLand[intcnum], "#,###,###,##0"));
				CorrelationGrid.TextMatrix(3, 4, Strings.Format(modProperty.Statics.lngIncomeBldg[intcnum], "#,###,###,##0"));
				CorrelationGrid.TextMatrix(4, 4, Strings.Format(modProperty.Statics.lngIncomeBldg[intcnum] + modProperty.Statics.lngIncomeLand[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
			}
			CorrelationGrid.TextMatrix(2, 4, Strings.Format(modProperty.Statics.lngIncomeLand[intcnum], "#,###,###,##0"));
			if (Conversion.Val(txtLandFrom.Text) == 1)
			{
				modProperty.Statics.lngAcceptedLand[intcnum] = modProperty.Statics.lngCurrentLand[intcnum];
				CorrelationGrid.TextMatrix(2, 3, "");
				// .TextMatrix(2, 4) = ""
				CorrelationGrid.TextMatrix(2, 6, "");
			}
			else if (Conversion.Val(txtLandFrom.Text) == 2)
			{
				modProperty.Statics.lngAcceptedLand[intcnum] = modProperty.Statics.lngPreviousLand[intcnum];
				CorrelationGrid.TextMatrix(2, 3, "");
				// .TextMatrix(2, 4) = ""
				CorrelationGrid.TextMatrix(2, 6, "");
			}
			else if (Conversion.Val(txtLandFrom.Text) == 3)
			{
				modProperty.Statics.lngAcceptedLand[intcnum] = modProperty.Statics.lngMarketLand[intcnum];
				CorrelationGrid.TextMatrix(2, 3, Strings.Format(modProperty.Statics.lngMarketLand[intcnum], "#,###,###,##0"));
				// .TextMatrix(2, 4) = ""
				CorrelationGrid.TextMatrix(2, 6, "");
			}
			else if (Conversion.Val(txtLandFrom.Text) == 4)
			{
				modProperty.Statics.lngAcceptedLand[intcnum] = modProperty.Statics.lngIncomeLand[intcnum];
				CorrelationGrid.TextMatrix(2, 3, "");
				CorrelationGrid.TextMatrix(2, 6, "");
			}
			else if (Conversion.Val(txtLandFrom.Text) == 6)
			{
				modProperty.Statics.lngAcceptedLand[intcnum] = modProperty.Statics.lngOverRideLand[intcnum];
				CorrelationGrid.TextMatrix(2, 3, "");
				// .TextMatrix(2, 4) = ""
				CorrelationGrid.TextMatrix(2, 6, Strings.Format(modProperty.Statics.lngOverRideLand[intcnum], "#,###,###,##0"));
			}
			clsLoadIncomeData.OpenRecordset("select indicatedvalue from incomevalue where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
			if (!clsLoadIncomeData.EndOfFile())
			{
				modProperty.Statics.lngIncomeBldg[intcnum] = FCConvert.ToInt32(Conversion.Val(clsLoadIncomeData.Get_Fields_Int32("indicatedvalue")) - modProperty.Statics.lngAcceptedLand[intcnum]);
			}
			else
			{
				modProperty.Statics.lngIncomeBldg[intcnum] = 0;
			}
			CorrelationGrid.TextMatrix(3, 4, Strings.Format(modProperty.Statics.lngIncomeBldg[intcnum], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(4, 4, Strings.Format(modProperty.Statics.lngIncomeBldg[intcnum] + modProperty.Statics.lngIncomeLand[intcnum], "#,###,###,##0"));
			if (Conversion.Val(txtBldgFrom.Text) == 1)
			{
				modProperty.Statics.lngAcceptedBldg[intcnum] = modProperty.Statics.lngCurrentBldg[intcnum];
				CorrelationGrid.TextMatrix(3, 3, "");
				// .TextMatrix(3, 4) = ""
				CorrelationGrid.TextMatrix(3, 6, "");
			}
			else if (Conversion.Val(txtBldgFrom.Text) == 2)
			{
				modProperty.Statics.lngAcceptedBldg[intcnum] = modProperty.Statics.lngPreviousBldg[intcnum];
				CorrelationGrid.TextMatrix(3, 3, "");
				// .TextMatrix(3, 4) = ""
				CorrelationGrid.TextMatrix(3, 6, "");
			}
			else if (Conversion.Val(txtBldgFrom.Text) == 3)
			{
				modProperty.Statics.lngAcceptedBldg[intcnum] = modProperty.Statics.lngMarketBldg[intcnum];
				CorrelationGrid.TextMatrix(3, 3, Strings.Format(modProperty.Statics.lngMarketBldg[intcnum], "#,###,###,##0"));
				// .TextMatrix(3, 4) = ""
				CorrelationGrid.TextMatrix(3, 6, "");
			}
			else if (Conversion.Val(txtBldgFrom.Text) == 4)
			{
				modProperty.Statics.lngAcceptedBldg[intcnum] = modProperty.Statics.lngIncomeBldg[intcnum];
				CorrelationGrid.TextMatrix(3, 3, "");
				CorrelationGrid.TextMatrix(3, 6, "");
				modProperty.Statics.lngIncomeLand[intcnum] = modProperty.Statics.lngAcceptedLand[intcnum];
				CorrelationGrid.TextMatrix(2, 4, Strings.Format(modProperty.Statics.lngIncomeLand[intcnum], "#,###,###,##0"));
			}
			else if (Conversion.Val(txtBldgFrom.Text) == 6)
			{
				modProperty.Statics.lngAcceptedBldg[intcnum] = modProperty.Statics.lngOverRideBldg[intcnum];
				CorrelationGrid.TextMatrix(3, 3, "");
				// .TextMatrix(3, 4) = ""
				CorrelationGrid.TextMatrix(3, 6, Strings.Format(modProperty.Statics.lngOverRideBldg[intcnum], "#,###,###,##0"));
			}
			if (Conversion.Val(txtBldgFrom.Text) != 4)
			{
				CorrelationGrid.TextMatrix(3, 4, "");
				CorrelationGrid.TextMatrix(4, 4, "");
				CorrelationGrid.TextMatrix(2, 4, "");
			}
			// correlated figures are the accepted figures * a percent and rounded
			clsDRWrapper temp = modDataTypes.Statics.CR;
			modREMain.OpenCRTable(ref temp, 1452);
			modDataTypes.Statics.CR = temp;
			if (Conversion.Val(modDataTypes.Statics.CR.Get_Fields_String("cbase")) == 0)
			{
				modProperty.Statics.lngCorrelatedLand[intcnum] = 0;
				dblRatioAssess = 0;
			}
			else
			{
				modProperty.Statics.lngCorrelatedLand[intcnum] = modGlobalRoutines.RERound_2(FCConvert.ToInt32(modProperty.Statics.lngAcceptedLand[intcnum] * (FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.CR.Get_Fields_String("CBase")))) / 100.0)));
				dblRatioAssess = Conversion.Val(modDataTypes.Statics.CR.Get_Fields_String("cbase")) / 100;
			}
			if (Conversion.Val(modDataTypes.Statics.CR.Get_Fields_String("CBase")) == 0)
			{
				modProperty.Statics.lngCorrelatedBldg[intcnum] = 0;
			}
			else
			{
				modProperty.Statics.lngCorrelatedBldg[intcnum] = modGlobalRoutines.RERound_2(FCConvert.ToInt32(modProperty.Statics.lngAcceptedBldg[intcnum] * (FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.CR.Get_Fields_String("CBase")))) / 100.0)));
			}
			CorrelationGrid.TextMatrix(2, 5, Strings.Format(modProperty.Statics.lngAcceptedLand[intcnum], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(3, 5, Strings.Format(modProperty.Statics.lngAcceptedBldg[intcnum], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(4, 5, Strings.Format(modProperty.Statics.lngAcceptedLand[intcnum] + modProperty.Statics.lngAcceptedBldg[intcnum], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(2, 7, Strings.Format(modProperty.Statics.lngCorrelatedLand[intcnum], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(3, 7, Strings.Format(modProperty.Statics.lngCorrelatedBldg[intcnum], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(4, 7, Strings.Format(modProperty.Statics.lngCorrelatedBldg[intcnum] + modProperty.Statics.lngCorrelatedLand[intcnum], "#,###,###,##0"));
			FillTotalGrid(ref intNumCards);
		}

		private bool Validate_Card()
		{
			bool Validate_Card = false;
			int intTemp;
            // If Val(txtCardNumber.Text) < 0 Or Val(txtCardNumber.Text) > VScrollCard.Max Then
            if (Conversion.Val(txtCardNumber.Text) < 0 || Conversion.Val(txtCardNumber.Text) > modGNBas.Statics.gintMaxCards)
            {
                Validate_Card = false;
                return Validate_Card;
            }
            modGlobalVariables.Statics.intCurrentCard = FCConvert.ToInt32(Math.Round(Conversion.Val(txtCardNumber.Text)));
			modGNBas.Statics.gintCardNumber = modGlobalVariables.Statics.intCurrentCard;
			modREASValuations.Statics.Account = modGlobalVariables.Statics.gintLastAccountNumber;
			clsDRWrapper temp = modDataTypes.Statics.MR;
			modREMain.OpenMasterTable(ref temp, modREASValuations.Statics.Account, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.MR = temp;
			temp = modDataTypes.Statics.CMR;
			modREMain.OpenCOMMERCIALTable(ref temp, modREASValuations.Statics.Account, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.CMR = temp;
			temp = modDataTypes.Statics.DWL;
			modREMain.OpenDwellingTable(ref temp, modREASValuations.Statics.Account, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.DWL = temp;
			temp = modDataTypes.Statics.OUT;
			modREMain.OpenOutBuildingTable(ref temp, modREASValuations.Statics.Account, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.OUT = temp;
			// PropertyGrid(intCurrentCard - 1).ZOrder
			// DwellingGrid(intCurrentCard - 1).ZOrder
			// CommercialGrid(intCurrentCard - 1).ZOrder
			// OutBuildingGrid(intCurrentCard - 1).ZOrder
			// CorrelationGrid(intCurrentCard - 1).ZOrder
			// For intTemp = 0 To intNumCards - 1
			// OutBuildingGrid(intTemp).Visible = False
			// PropertyGrid(intTemp).Visible = False
			// CommercialGrid(intTemp).Visible = False
			// CorrelationGrid(intTemp).Visible = False
			// DwellingGrid(intTemp).Visible = False
			// DwellMiscGrid(intTemp).Visible = False
			// DwellDescGrid(intTemp).Visible = False
			// DwellPercGrid(intTemp).Visible = False
			// DwellCondGrid(intTemp).Visible = False
			// MiscGrid(intTemp).Visible = False
			// SaleGrid(intTemp).Visible = False
			// txtLandFrom.Visible = False
			// txtBldgFrom.Visible = False
			// Next intTemp
			// 
			if (FCConvert.ToInt16(modProperty.Statics.intOverRideCodeLand[modGlobalVariables.Statics.intCurrentCard]) > 0)
			{
				txtLandFrom.Text = FCConvert.ToString(FCConvert.ToInt16(modProperty.Statics.intOverRideCodeLand[modGlobalVariables.Statics.intCurrentCard]));
			}
			else
			{
				// End If
				// If txtLandFrom.Text = "" Then
				modProperty.Statics.lngOverRideLand[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hLvalland") + "")));
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivallandcode") + "") == 0)
				{
					modDataTypes.Statics.MR.Set_Fields("hivallandcode", 1);
				}
				else if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivallandcode") + "") == 3)
				{
					CorrelationGrid.TextMatrix(2, 3, Strings.Format(modProperty.Statics.lngMarketLand[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
				}
				else if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivallandcode") + "") == 4)
				{
					CorrelationGrid.TextMatrix(2, 4, Strings.Format(modProperty.Statics.lngIncomeLand[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
				}
				else if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivallandcode") + "") == 6)
				{
					modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hlvalland"))));
					CorrelationGrid.TextMatrix(2, 6, Strings.Format(modProperty.Statics.lngOverRideLand[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
					// CorrelationGrid(intCurrentCard - 1).TextMatrix(2, 7) = Format(lngOverRideLand(intCurrentCard), "#,###,###,##0")
				}
				txtLandFrom.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivallandcode") + ""));
			}
			if (FCConvert.ToInt16(modProperty.Statics.intOverRideCodeBldg[modGlobalVariables.Statics.intCurrentCard]) > 0)
			{
				txtBldgFrom.Text = FCConvert.ToString(FCConvert.ToInt16(modProperty.Statics.intOverRideCodeBldg[modGlobalVariables.Statics.intCurrentCard]));
			}
			else
			{
				// If txtBldgFrom.Text = "" Then
				modProperty.Statics.lngOverRideBldg[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("hlvalbldg"));
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode") + "") == 0)
				{
					modDataTypes.Statics.MR.Set_Fields("hivalbldgcode", 1);
				}
				else if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode") + "") == 3)
				{
					CorrelationGrid.TextMatrix(3, 3, Strings.Format(modProperty.Statics.lngMarketBldg[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
				}
				else if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode") + "") == 4)
				{
					CorrelationGrid.TextMatrix(3, 4, Strings.Format(modProperty.Statics.lngIncomeBldg[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
				}
				else if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode") + "") == 6)
				{
					modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hlvalbldg"))));
					CorrelationGrid.TextMatrix(3, 6, Strings.Format(modProperty.Statics.lngOverRideBldg[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
					// CorrelationGrid(intCurrentCard - 1).TextMatrix(3, 7) = Format(lngOverRideBldg(intCurrentCard), "#,###,###,##0")
				}
				txtBldgFrom.Text = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode"));
			}
			// (intCurrentCard - 1)
			CorrelationGrid.TextMatrix(2, 5, Strings.Format(modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(3, 5, Strings.Format(modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(4, 5, Strings.Format(modProperty.Statics.lngAcceptedLand[modGlobalVariables.Statics.intCurrentCard] + modProperty.Statics.lngAcceptedBldg[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(2, 7, Strings.Format(modProperty.Statics.lngCorrelatedLand[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(3, 7, Strings.Format(modProperty.Statics.lngCorrelatedBldg[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
			CorrelationGrid.TextMatrix(4, 7, Strings.Format(modProperty.Statics.lngCorrelatedBldg[modGlobalVariables.Statics.intCurrentCard] + modProperty.Statics.lngCorrelatedLand[modGlobalVariables.Statics.intCurrentCard], "#,###,###,##0"));
			FillGrids(FCConvert.ToInt16(modGlobalVariables.Statics.intCurrentCard));
			FillAccountLabels();
			// Call FillTotalGrid(intNumCards)
			this.Refresh();
			// DoEvents
			return Validate_Card;
		}
		// Private Sub VScrollCard_Validate(Cancel As Boolean)
		// If Not Validate_Card Then Cancel = True
		// End Sub
		private void UpDownCard_Validating_2(bool Cancel)
		{
			UpDownCard_Validating(new object(), new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void UpDownCard_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Validate_Card())
				e.Cancel = true;
		}

		public void UpDownCard_Validate(bool Cancel)
		{
			//UpDownCard_Validating(UpDownCard, new System.ComponentModel.CancelEventArgs(Cancel));
		}
	}
}
