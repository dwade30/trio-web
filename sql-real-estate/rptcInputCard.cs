﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptInputCard.
	/// </summary>
	public partial class rptInputCard : BaseSectionReport
	{
		public rptInputCard()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Input Card Report";
		}

		public static rptInputCard InstancePtr
		{
			get
			{
				return (rptInputCard)Sys.GetInstance(typeof(rptInputCard));
			}
		}

		protected rptInputCard _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMst?.Dispose();
				rsTemp?.Dispose();
				RSOther?.Dispose();
                rsMst = null;
                rsTemp = null;
                RSOther = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptInputCard	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsTemp = new clsDRWrapper();
		// Dim dbTemp As DAO.Database
		clsDRWrapper RSOther = new clsDRWrapper();
		object varAccount;
		int intPage;
		int intMaxCards;
		clsDRWrapper rsMst = new clsDRWrapper();
		bool boolSales;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			nextaccount:
			;
			if (rsTemp.EndOfFile())
				return;
			if (intPage > 0)
			{
				// this forces a new page for each record in the database
				if (!rsTemp.EndOfFile())
					rsTemp.MoveNext();
				if (!rsTemp.EndOfFile())
				{
					if (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("rsdeleted")))
						goto nextaccount;
					// Exit Sub
				}
			}
			eArgs.EOF = rsTemp.EndOfFile();
			// EOF = False
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			modGlobalVariables.Statics.LandTypes.UnloadLandTypes();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strTemp;
                modGlobalVariables.Statics.LandTypes.Init();
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                clsLoad.OpenRecordset(
                    "select * from costrecord where crecordnumber between 1091 and 1092 order by crecordnumber",
                    modGlobalVariables.strREDatabase);
                strTemp = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                strTemp = Strings.Replace(strTemp, ".", "", 1, -1, CompareConstants.vbTextCompare);
                strTemp = Strings.Trim(strTemp);
                if (strTemp != string.Empty)
                {
                    Label22.Text = strTemp;
                }

                clsLoad.MoveNext();
                strTemp = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                strTemp = Strings.Replace(strTemp, ".", "", 1, -1, CompareConstants.vbTextCompare);
                if (Strings.Trim(strTemp) != string.Empty)
                {
                    Label25.Text = strTemp;
                }

                clsLoad.OpenRecordset(
                    "select * from costrecord where crecordnumber = 1330 or crecordnumber = 1340 order by crecordnumber",
                    modGlobalVariables.strREDatabase);
                if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"))) == string.Empty)
                {
                    Label28.Text = "Open 1";
                }
                else
                {
                    Label28.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                }

                clsLoad.MoveNext();
                if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"))) == string.Empty)
                {
                    Label29.Text = "Open 2";
                }
                else
                {
                    Label29.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("cldesc")));
                }

                txtCaption.Text = "Real Estate Assessment System";
                txtMuni.Text = modGlobalConstants.Statics.MuniName;
                txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
                txtDate.Text = "Date " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp;
			int x;
            using (clsDRWrapper rsLookUp = new clsDRWrapper())
            {
                int[] intExemptCodes = new int[3 + 1];
                makedwellinginvisible();
                makecommercialinvisible();
                if (rsTemp.EndOfFile())
                    return;
                intPage += 1;
                // lblPage.Caption = "Page " & intPage
                intMaxCards = modREMain.GetMaxCards("Master", "RSAccount", rsTemp.Get_Fields_Int32("RSAccount"));
                txtAccount.Text = FCConvert.ToString(rsTemp.Get_Fields_Int32("RSAccount"));
                txtCard.Text = rsTemp.Get_Fields_Int32("RSCard") + " OF " + FCConvert.ToString(intMaxCards);
                if (!boolSales)
                {
                    txtName.Text = rsTemp.Get_Fields_String("DeedName1").Trim();
                    txtSecondOwner.Text = rsTemp.Get_Fields_String("DeedName2").Trim();
                }
                else
                {
                    txtName.Text = Strings.Trim(rsTemp.Get_Fields_String("RSName") + " ");
                    txtSecondOwner.Text = Strings.Trim(rsTemp.Get_Fields_String("rssecowner") + "");
                }

                if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("rslocnumalph"))).Length < 5)
                {
                    txtLocation.Text = Strings.Trim(rsTemp.Get_Fields_String("RSLOCNUMALPH") + " " +
                                                    Strings.StrDup(
                                                        5 - Strings.Trim(rsTemp.Get_Fields_String("RSLOCNUMALPH") + " ")
                                                            .Length, " ") + rsTemp.Get_Fields_String("RSLOCStreet") +
                                                    " ");
                }
                else
                {
                    txtLocation.Text = Strings.Trim(rsTemp.Get_Fields_String("RSLOCNUMALPH") + " " +
                                                    rsTemp.Get_Fields_String("RSLOCStreet") + " ");
                }

                txtMap.Text = Strings.Trim(rsTemp.Get_Fields_String("RSMAPLOT") + " ");
                txtAddress.Text = Strings.Trim(rsTemp.Get_Fields_String("RSAddr1") + " ");
                txtAddress2.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("rsaddr2")));
                txtReference.Text = Strings.Trim(rsTemp.Get_Fields_String("RSRef1") + " ");
                txtReference2.Text = Strings.Trim(rsTemp.Get_Fields_String("rsref2") + "");
                strTemp = modGlobalFunctions.GetCurrentBookPageString(rsTemp.Get_Fields_Int32("rsaccount"));
                txtBookPage.Text = Strings.Trim(strTemp);
                txtState.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("rsstate")));
                Field1.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("rsaddr3")));
                txtZip.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("rszip")));
                // & "  " & Trim(rsTemp.Fields("rszip4"))
                if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 0)
                {
                    strTemp = "None";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 101)
                {
                    strTemp = "Rural- Vacant Land";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 102)
                {
                    strTemp = "Urban - Vacant Land";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 103)
                {
                    strTemp = "Oceanfront - Vacant Land";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 104)
                {
                    strTemp = "Lake/Pond front - Vacant Land";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 105)
                {
                    strTemp = "Stream/Riverfront - Vacant Land";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 106)
                {
                    strTemp = "Agricultural - Vacant Land";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 107)
                {
                    strTemp = "Commercial Zone - Vacant Land";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 108)
                {
                    strTemp = "Other - Vacant Land";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 201)
                {
                    strTemp = "Rural - Single Family";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 202)
                {
                    strTemp = "Urban - Single Family";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 203)
                {
                    strTemp = "Oceanfront - Single Family";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 204)
                {
                    strTemp = "Lake/Pond front - Single Family";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 205)
                {
                    strTemp = "Stream/Riverfront - Single Family";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 206)
                {
                    strTemp = "Mobile Home - Single Family";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 220)
                {
                    strTemp = "Other - Single Family";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 301)
                {
                    strTemp = "Mixed Use - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 302)
                {
                    strTemp = "Apt/2-3 Unit - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 303)
                {
                    strTemp = "Apt/4+ - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 304)
                {
                    strTemp = "Bank - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 305)
                {
                    strTemp = "Restaurant - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 306)
                {
                    strTemp = "Medical - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 307)
                {
                    strTemp = "Office - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 308)
                {
                    strTemp = "Retail - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 309)
                {
                    strTemp = "Automotive - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 310)
                {
                    strTemp = "Marina - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 311)
                {
                    strTemp = "Warehouse - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 312)
                {
                    strTemp = "Hotel/Motel/Inn - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 313)
                {
                    strTemp = "Nursing Home - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 314)
                {
                    strTemp = "Shopping Mall - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 320)
                {
                    strTemp = "Other - Commercial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 401)
                {
                    strTemp = "Gas and Oil - Industrial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 402)
                {
                    strTemp = "Utility - Industrial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 403)
                {
                    strTemp = "Gravel Pit - Industrial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 404)
                {
                    strTemp = "Lumber/Saw Mill - Industrial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 405)
                {
                    strTemp = "Pulp/Paper Mill - Industrial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 406)
                {
                    strTemp = "Light Manufacture - Industrial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 407)
                {
                    strTemp = "Heavy Manufacture - Industrial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 420)
                {
                    strTemp = "Manufacture Other - Industrial";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 501)
                {
                    strTemp = "Government - Misc Codes";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 502)
                {
                    strTemp = "Condominium - Misc Codes";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 503)
                {
                    strTemp = "Timeshare Unit - Misc Codes";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 504)
                {
                    strTemp = "Non-Profit - Misc Codes";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 505)
                {
                    strTemp = "Mobile Home Park - Misc Codes";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 506)
                {
                    strTemp = "Airport - Misc Codes";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 507)
                {
                    strTemp = "Conservation - Misc Codes";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 508)
                {
                    strTemp = "Current Use Classification";
                }
                else if (Conversion.Val(rsTemp.Get_Fields_Int32("propertycode") + "") == 520)
                {
                    strTemp = "Other - Misc Codes";
                }
                else
                {
                    strTemp = "None";
                }

                txtPropertyCode.Text = strTemp;
                txtLand.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("lastlandval") + ""),
                    "##,###,###");
                txtBuilding.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("lastbldgval") + ""),
                    "##,###,###");
                txtExemption.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("RLEXEMPTION") + ""),
                    "##,###,###");
                rsLookUp.OpenRecordset("select * from exemptcode order by code", modGlobalVariables.strREDatabase);
                if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("rscard")) == 1)
                {
                    intExemptCodes[1] =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields_Int32("riexemptcd1") + "")));
                    intExemptCodes[2] =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields_Int32("riexemptcd2") + "")));
                    intExemptCodes[3] =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields_Int32("riexemptcd3") + "")));
                }
                else
                {
                    rsMst.OpenRecordset(
                        "select riexemptcd1,riexemptcd2,riexemptcd3 from master where rsaccount = " +
                        rsTemp.Get_Fields_Int32("rsaccount") + " and rscard = 1", modGlobalVariables.strREDatabase);
                    if (!rsMst.EndOfFile())
                    {
                        intExemptCodes[1] =
                            FCConvert.ToInt32(Math.Round(Conversion.Val(rsMst.Get_Fields_Int32("riexemptcd1") + "")));
                        intExemptCodes[2] =
                            FCConvert.ToInt32(Math.Round(Conversion.Val(rsMst.Get_Fields_Int32("riexemptcd2") + "")));
                        intExemptCodes[3] =
                            FCConvert.ToInt32(Math.Round(Conversion.Val(rsMst.Get_Fields_Int32("riexemptcd3") + "")));
                    }
                    else
                    {
                        intExemptCodes[1] = 0;
                        intExemptCodes[2] = 0;
                        intExemptCodes[3] = 0;
                    }
                }

                if (intExemptCodes[1] == 0 && intExemptCodes[2] == 0 && intExemptCodes[3] == 0)
                {
                    txtExemptCode1.Text = "None";
                    txtExemptCode2.Text = "";
                    txtExemptCode3.Text = "";
                }
                else
                {
                    for (x = 1; x <= 3; x++)
                    {
                        // If rsLookUp.FindFirstRecord("code", intExemptCodes(x)) Then
                        if (rsLookUp.FindFirst("code = " + FCConvert.ToString(intExemptCodes[x])))
                        {
                            (Detail.Controls["txtExemptCode" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                                .Text = rsLookUp.Get_Fields_String("description");
                        }
                        else
                        {
                            (Detail.Controls["txtExemptCode" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                                .Text = "";
                        }
                    }

                    // x
                }

                rsLookUp.OpenRecordset(
                    "select * from tbltrancode where code = " +
                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ritrancode") + "")),
                    modGlobalVariables.strREDatabase);
                if (!rsLookUp.EndOfFile())
                {
                    txtTranCode.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("description"));
                }
                else
                {
                    txtTranCode.Text = "None";
                }

                rsLookUp.OpenRecordset(
                    "select * from tbllandcode where code = " +
                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("rilandcode") + "")),
                    modGlobalVariables.strREDatabase);
                if (!rsLookUp.EndOfFile())
                {
                    txtLandCode.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("description"));
                }
                else
                {
                    txtLandCode.Text = "None";
                }

                rsLookUp.OpenRecordset(
                    "select * from tblbldgcode where code = " +
                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ribldgcode") + "")),
                    modGlobalVariables.strREDatabase);
                if (!rsLookUp.EndOfFile())
                {
                    txtBuildingCode.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("description"));
                }
                else
                {
                    txtBuildingCode.Text = "None";
                }

                txtTreeGrowthYear.Text = FCConvert.ToString(rsTemp.Get_Fields_Int32("pistreetcode") + "");
                // set property data
                if (Conversion.Val(rsTemp.Get_Fields_Int32("pineighborhood") + "") > 0)
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        rsLookUp.OpenRecordset(
                            "select * from neighborhood where code = " +
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("pineighborhood") + "")),
                            modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        rsLookUp.OpenRecordset(
                            "select * from neighborhood where code = " +
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("pineighborhood") + "")) +
                            " and townnumber = " +
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ritrancode") + "")),
                            modGlobalVariables.strREDatabase);
                    }

                    if (!rsLookUp.EndOfFile())
                    {
                        txtNeighborhood.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("Description"));
                    }
                    else
                    {
                        txtNeighborhood.Text = "None";
                    }
                }
                else
                {
                    txtNeighborhood.Text = "None";
                }

                // TODO Get_Fields: Check the table for the column [PIXCOORD] and replace with corresponding Get_Field method
                txtXCoordinate.Text = Strings.Trim(rsTemp.Get_Fields("PIXCOORD") + "");
                // TODO Get_Fields: Check the table for the column [PIYCOORD] and replace with corresponding Get_Field method
                txtYCoordinate.Text = Strings.Trim(rsTemp.Get_Fields("PIYCOORD") + "");
                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    rsLookUp.OpenRecordset(
                        "select description from zones where CODE = " +
                        FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("pizone") + "")),
                        modGlobalVariables.strREDatabase);
                }
                else
                {
                    rsLookUp.OpenRecordset(
                        "select description from zones where CODE = " +
                        FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("pizone") + "")) +
                        " and townnumber = " +
                        FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ritrancode") + "")),
                        modGlobalVariables.strREDatabase);
                }

                if (!rsLookUp.EndOfFile())
                {
                    txtZone.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("description"));
                }
                else
                {
                    txtZone.Text = "None";
                }

                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    rsLookUp.OpenRecordset(
                        "select description,secondarydescription from zones where CODE = " +
                        FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("piseczone") + "")),
                        modGlobalVariables.strREDatabase);
                }
                else
                {
                    rsLookUp.OpenRecordset(
                        "select description,secondarydescription from zones where CODE = " +
                        FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("piseczone") + "")) +
                        " and townnumber = " +
                        FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ritrancode") + "")),
                        modGlobalVariables.strREDatabase);
                }

                if (!rsLookUp.EndOfFile())
                {
                    if (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("zoneoverride")))
                    {
                        txtZone2.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("description"));
                    }
                    else
                    {
                        txtZone2.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("secondarydescription"));
                    }
                }
                else
                {
                    txtZone2.Text = "None";
                }

                if (Conversion.Val(rsTemp.Get_Fields_Int32("pitopography1") + "") > 0)
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        rsLookUp.OpenRecordset(
                            "select cldesc from costrecord where crecordnumber = " +
                            FCConvert.ToString(1300 + Conversion.Val(rsTemp.Get_Fields_Int32("pitopography1") + "")),
                            modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        rsLookUp.OpenRecordset(
                            "select cldesc from costrecord where townnumber = " +
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ritrancode") + "")) +
                            " and crecordnumber = " +
                            FCConvert.ToString(1300 + Conversion.Val(rsTemp.Get_Fields_Int32("pitopography1") + "")),
                            modGlobalVariables.strREDatabase);
                    }

                    if (!rsLookUp.EndOfFile())
                    {
                        txtTopography.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("cldesc"));
                    }
                    else
                    {
                        txtTopography.Text = "None";
                    }
                }
                else
                {
                    txtTopography.Text = "None";
                }

                if (Conversion.Val(rsTemp.Get_Fields_Int32("pitopography2") + "") > 0)
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        rsLookUp.OpenRecordset(
                            "select cldesc from costrecord where crecordnumber = " +
                            FCConvert.ToString(1300 + Conversion.Val(rsTemp.Get_Fields_Int32("pitopography2") + "")),
                            modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        rsLookUp.OpenRecordset(
                            "select cldesc from costrecord where townnumber = " +
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ritrancode") + "")) +
                            " and crecordnumber = " +
                            FCConvert.ToString(1300 + Conversion.Val(rsTemp.Get_Fields_Int32("pitopography2") + "")),
                            modGlobalVariables.strREDatabase);
                    }

                    if (!rsLookUp.EndOfFile())
                    {
                        txtTopography2.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("cldesc"));
                    }
                    else
                    {
                        txtTopography2.Text = "None";
                    }
                }
                else
                {
                    txtTopography2.Text = "None";
                }

                if (Conversion.Val(rsTemp.Get_Fields_Int32("piutilities1") + "") > 0)
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        rsLookUp.OpenRecordset(
                            "select cldesc from costrecord where crecordnumber = " +
                            FCConvert.ToString(1310 + Conversion.Val(rsTemp.Get_Fields_Int32("piutilities1") + "")),
                            modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        rsLookUp.OpenRecordset(
                            "select cldesc from costrecord where townnumber = " +
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ritrancode") + "")) +
                            " and crecordnumber = " +
                            FCConvert.ToString(1310 + Conversion.Val(rsTemp.Get_Fields_Int32("piutilities1") + "")),
                            modGlobalVariables.strREDatabase);
                    }

                    if (!rsLookUp.EndOfFile())
                    {
                        txtUtilities.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("cldesc"));
                    }
                    else
                    {
                        txtUtilities.Text = "None";
                    }
                }
                else
                {
                    txtUtilities.Text = "None";
                }

                if (Conversion.Val(rsTemp.Get_Fields_Int32("piutilities2") + "") > 0)
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        rsLookUp.OpenRecordset(
                            "select cldesc from costrecord where crecordnumber = " +
                            FCConvert.ToString(1310 + Conversion.Val(rsTemp.Get_Fields_Int32("piutilities2") + "")),
                            modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        rsLookUp.OpenRecordset(
                            "select cldesc from costrecord where townnumber = " +
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ritrancode") + "")) +
                            " and crecordnumber = " +
                            FCConvert.ToString(1310 + Conversion.Val(rsTemp.Get_Fields_Int32("piutilities2") + "")),
                            modGlobalVariables.strREDatabase);
                    }

                    if (!rsLookUp.EndOfFile())
                    {
                        txtUtility2.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("cldesc"));
                    }
                    else
                    {
                        txtUtility2.Text = "None";
                    }
                }
                else
                {
                    txtUtility2.Text = "None";
                }

                if (Conversion.Val(rsTemp.Get_Fields_Int32("pistreet") + "") > 0)
                {
                    if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        rsLookUp.OpenRecordset(
                            "select cldesc from costrecord where crecordnumber = " +
                            FCConvert.ToString(1320 + Conversion.Val(rsTemp.Get_Fields_Int32("pistreet") + "")),
                            modGlobalVariables.strREDatabase);
                    }
                    else
                    {
                        rsLookUp.OpenRecordset(
                            "select cldesc from costrecord where townnumber = " +
                            FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ritrancode") + "")) +
                            " and crecordnumber = " +
                            FCConvert.ToString(1320 + Conversion.Val(rsTemp.Get_Fields_Int32("pistreet") + "")),
                            modGlobalVariables.strREDatabase);
                    }

                    if (!rsLookUp.EndOfFile())
                    {
                        txtStreet.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("cldesc"));
                    }
                    else
                    {
                        txtStreet.Text = "None";
                    }
                }
                else
                {
                    txtStreet.Text = "None";
                }

                txtForestYear.Text = Strings.Trim(rsTemp.Get_Fields_Int32("PIOPEN1") + "");
                txtAffidavit.Text = Strings.Trim(rsTemp.Get_Fields_Int32("PIOPEN2") + "");
                if (Information.IsDate(rsTemp.Get_Fields("saledate") + ""))
                {
                    if (rsTemp.Get_Fields_DateTime("saledate") == DateTime.FromOADate(0))
                    {
                        txtSaleDate.Text = "";
                    }
                    else
                    {
                        txtSaleDate.Text = Strings.Format(rsTemp.Get_Fields_DateTime("saledate"), "MM/dd/yyyy");
                    }
                }

                txtSalePrice.Text = Strings.Format(Strings.Trim(rsTemp.Get_Fields_Int32("PISALEPRICE") + ""),
                    "#,###,###,###");
                if (Conversion.Val(rsTemp.Get_Fields_Int32("pisaletype") + "") > 0)
                {
                    rsLookUp.OpenRecordset(
                        "select * from costrecord where crecordnumber  = " +
                        FCConvert.ToString(1350 + Conversion.Val(rsTemp.Get_Fields_Int32("pisaletype") + "")),
                        modGlobalVariables.strREDatabase);
                    if (!rsLookUp.EndOfFile())
                    {
                        txtSaleType.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("cldesc") + "");
                    }
                    else
                    {
                        txtSaleType.Text = "";
                    }
                }
                else
                {
                    txtSaleType.Text = "";
                }

                if (Conversion.Val(rsTemp.Get_Fields_Int32("pisalefinancing") + "") > 0)
                {
                    rsLookUp.OpenRecordset(
                        "select * from costrecord where crecordnumber = " +
                        FCConvert.ToString(1360 + Conversion.Val(rsTemp.Get_Fields_Int32("pisalefinancing") + "")),
                        modGlobalVariables.strREDatabase);
                    if (!rsLookUp.EndOfFile())
                    {
                        txtSaleFinancing.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("cldesc") + "");
                    }
                    else
                    {
                        txtSaleFinancing.Text = "";
                    }
                }
                else
                {
                    txtSaleFinancing.Text = "";
                }

                if (Conversion.Val(rsTemp.Get_Fields_Int32("pisaleverified") + "") > 0)
                {
                    rsLookUp.OpenRecordset(
                        "select * from costrecord where crecordnumber = " +
                        FCConvert.ToString(1370 + Conversion.Val(rsTemp.Get_Fields_Int32("pisaleverified") + "")),
                        modGlobalVariables.strREDatabase);
                    if (!rsLookUp.EndOfFile())
                    {
                        txtSaleVerified.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("cldesc") + "");
                    }
                    else
                    {
                        txtSaleVerified.Text = "";
                    }
                }
                else
                {
                    txtSaleVerified.Text = "";
                }

                if (Conversion.Val(rsTemp.Get_Fields_Int32("pisalevalidity") + "") > 0)
                {
                    rsLookUp.OpenRecordset(
                        "select * from costrecord where crecordnumber = " +
                        FCConvert.ToString(1380 + Conversion.Val(rsTemp.Get_Fields_Int32("pisalevalidity") + "")),
                        modGlobalVariables.strREDatabase);
                    if (!rsLookUp.EndOfFile())
                    {
                        txtSaleValidity.Text = FCConvert.ToString(rsLookUp.Get_Fields_String("cldesc") + "");
                    }
                    else
                    {
                        txtSaleValidity.Text = "";
                    }
                }
                else
                {
                    txtSaleValidity.Text = "";
                }

                for (x = 1; x <= 7; x++)
                {
                    // TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
                    if (Conversion.Val(rsTemp.Get_Fields("piland" + FCConvert.ToString(x) + "type") + "") > 0)
                    {
                        // TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
                        if (modGlobalVariables.Statics.LandTypes.FindCode(FCConvert.ToInt32(
                            Conversion.Val(rsTemp.Get_Fields("piland" + FCConvert.ToString(x) + "type") + ""))))
                        {
                            (Detail.Controls["txtLandType" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                                .Text = modGlobalVariables.Statics.LandTypes.Description;
                        }
                        else
                        {
                            (Detail.Controls["txtLandType" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                                .Text = "";
                        }

                        // TODO Get_Fields: Field [PILAND] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [PILAND] not found!! (maybe it is an alias?)
                        // TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
                        (Detail.Controls["txtLandUnits" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text
                            = FormatLandUnits_26(rsTemp.Get_Fields("PILAND" + FCConvert.ToString(x) + "UNITSA") + "",
                                rsTemp.Get_Fields("PILAND" + FCConvert.ToString(x) + "UNITSB") + "",
                                FCConvert.ToInt16(
                                    Conversion.Val(rsTemp.Get_Fields("piland" + FCConvert.ToString(x) + "type") + "")));
                        // TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
                        (Detail.Controls["txtInf" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                            Strings.Trim(rsTemp.Get_Fields("piland" + FCConvert.ToString(x) + "inf") + "");
                        // TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
                        if (Conversion.Val(rsTemp.Get_Fields("piland" + FCConvert.ToString(x) + "infcode") + "") > 0)
                        {
                            if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                            {
                                // TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
                                rsLookUp.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(
                                        1440 + Conversion.Val(
                                            rsTemp.Get_Fields("piland" + FCConvert.ToString(x) + "infcode") + "")),
                                    modGlobalVariables.strREDatabase);
                            }
                            else
                            {
                                // TODO Get_Fields: Field [piland] not found!! (maybe it is an alias?)
                                rsLookUp.OpenRecordset(
                                    "select * from costrecord where crecordnumber = " +
                                    FCConvert.ToString(
                                        1440 + Conversion.Val(
                                            rsTemp.Get_Fields("piland" + FCConvert.ToString(x) + "infcode") + "")) +
                                    " and townnumber = " +
                                    FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("ritrancode") + "")),
                                    modGlobalVariables.strREDatabase);
                            }

                            if (!rsLookUp.EndOfFile())
                            {
                                (Detail.Controls["txtCd" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                                    .Text = rsLookUp.Get_Fields_String("cldesc");
                            }
                            else
                            {
                                (Detail.Controls["txtCd" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                                    .Text = "";
                            }
                        }
                        else
                        {
                            (Detail.Controls["txtCd" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                                "";
                        }
                    }
                    else
                    {
                        (Detail.Controls["txtLandType" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = "";
                        (Detail.Controls["txtLandUnits" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = "";
                        (Detail.Controls["txtInf" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                        (Detail.Controls["txtCd" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
                    }
                }

                // x
                // 
                // TODO Get_Fields: Check the table for the column [ENTRANCECODE] and replace with corresponding Get_Field method
                txtEntranceCode.Text = Strings.Trim(rsTemp.Get_Fields("ENTRANCECODE") + "");
                // TODO Get_Fields: Check the table for the column [INFORMATIONcode] and replace with corresponding Get_Field method
                txtInfoCode.Text = Strings.Trim(rsTemp.Get_Fields("INFORMATIONcode") + "");
                // txtDateInspected = fixthedate(Trim(RSOther.Fields("OIDATEINSPECTED") & ""))
                txtDateInspected.Text = Information.IsDate(rsTemp.Get_Fields("DATEINSPECTED"))
                    ? FCConvert.ToString(rsTemp.Get_Fields_DateTime("DATEINSPECTED"))
                    : "";
                if (FCConvert.ToString(rsTemp.Get_Fields_String("rsdwellingcode")) == "Y")
                {
                    if (!boolSales)
                    {
                        filldwellingboxes();
                    }
                    else
                    {
                        filldwellingboxes(FCConvert.ToString(rsTemp.Get_Fields_DateTime("saledate")));
                    }
                }
                else if (rsTemp.Get_Fields_String("rsdwellingcode") == "C")
                {
                    if (!boolSales)
                    {
                        fillcommercialboxes();
                    }
                    else
                    {
                        fillcommercialboxes(FCConvert.ToString(rsTemp.Get_Fields_DateTime("saledate")));
                    }
                }

                // set outbuilding data
                if (!boolSales)
                {
                    RSOther.OpenRecordset(
                        "SELECT distinct * FROM OutBuilding where RSAccount = " + rsTemp.Get_Fields_Int32("RSAccount") +
                        " and RSCard = " + rsTemp.Get_Fields_Int32("RSCard"), modGlobalVariables.strREDatabase);
                }
                else
                {
                    RSOther.OpenRecordset(
                        "SELECT distinct * FROM OutBuilding where RSAccount = " + rsTemp.Get_Fields_Int32("RSAccount") +
                        " and RSCard = " + rsTemp.Get_Fields_Int32("RSCard") + " and saledate = '" +
                        rsTemp.Get_Fields_DateTime("saledate") + "'", modGlobalVariables.strREDatabase);
                }

                if (!RSOther.EndOfFile())
                {
                    // txtEntranceCode = Trim(RSOther.Fields("OIENTRANCECODE") & "")
                    // txtInfoCode = Trim(RSOther.Fields("OIINFORMATION") & "")
                    // txtDateInspected = fixthedate(Trim(RSOther.Fields("OIDATEINSPECTED") & ""))
                    // 
                    // txtDateInspected = Trim(RSOther.Fields("OIDATEINSPECTED") & "")
                    txtType1.Text = Strings.Trim(RSOther.Get_Fields_Int32("OITYPE1") + "");
                    txtYear1.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIYEAR1") + "");
                    txtUnits1.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIUNITS1") + "");
                    txtOIGrade1.Text = Strings.Trim(RSOther.Get_Fields_Int32("oigradecd1") + "");
                    txtGrade1.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIGRADEPCT1") + "");
                    txtCondition1.Text = Strings.Trim(RSOther.Get_Fields_Int32("OICOND1") + "");
                    txtPhysical1.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTPHYS1") + "");
                    txtFunctional1.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTFUNCT1") + "");
                    if (FCConvert.ToBoolean(RSOther.Get_Fields_Boolean("oiusesound1")))
                    {
                        txtUseSV1.Text = "Yes";
                    }
                    else
                    {
                        txtUseSV1.Text = "No";
                    }

                    txtSV1.Text = Strings.Format(Conversion.Val(RSOther.Get_Fields_Int32("oisoundvalue1")),
                        "#,###,###,##0");
                    txtType2.Text = Strings.Trim(RSOther.Get_Fields_Int32("OITYPE2") + "");
                    txtYear2.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIYEAR2") + "");
                    txtUnits2.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIUNITS2") + "");
                    txtOIGrade2.Text = Strings.Trim(RSOther.Get_Fields_Int32("oigradecd2") + "");
                    txtGrade2.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIGRADEPCT2") + "");
                    txtCondition2.Text = Strings.Trim(RSOther.Get_Fields_Int32("OICOND2") + "");
                    txtPhysical2.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTPHYS2") + "");
                    txtFunctional2.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTFUNCT2") + "");
                    if (FCConvert.ToBoolean(RSOther.Get_Fields_Boolean("oiusesound2")))
                    {
                        txtUseSV2.Text = "Yes";
                    }
                    else
                    {
                        txtUseSV2.Text = "No";
                    }

                    txtSV2.Text = Strings.Format(Conversion.Val(RSOther.Get_Fields_Int32("oisoundvalue2")),
                        "#,###,###,##0");
                    txtType3.Text = Strings.Trim(RSOther.Get_Fields_Int32("OITYPE3") + "");
                    txtYear3.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIYEAR3") + "");
                    txtUnits3.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIUNITS3") + "");
                    txtOIGrade3.Text = Strings.Trim(RSOther.Get_Fields_Int32("oigradecd3") + "");
                    txtGrade3.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIGRADEPCT3") + "");
                    txtCondition3.Text = Strings.Trim(RSOther.Get_Fields_Int32("OICOND3") + "");
                    txtPhysical3.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTPHYS3") + "");
                    txtFunctional3.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTFUNCT3") + "");
                    if (FCConvert.ToBoolean(RSOther.Get_Fields_Boolean("oiusesound3")))
                    {
                        txtUseSV3.Text = "Yes";
                    }
                    else
                    {
                        txtUseSV3.Text = "No";
                    }

                    txtSV3.Text = Strings.Format(Conversion.Val(RSOther.Get_Fields_Int32("oisoundvalue3")),
                        "#,###,###,##0");
                    txtType4.Text = Strings.Trim(RSOther.Get_Fields_Int32("OITYPE4") + "");
                    txtYear4.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIYEAR4") + "");
                    txtUnits4.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIUNITS4") + "");
                    txtOIGrade4.Text = Strings.Trim(RSOther.Get_Fields_Int32("oigradecd4") + "");
                    txtGrade4.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIGRADEPCT4") + "");
                    txtCondition4.Text = Strings.Trim(RSOther.Get_Fields_Int32("OICOND4") + "");
                    txtPhysical4.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTPHYS4") + "");
                    txtFunctional4.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTFUNCT4") + "");
                    if (FCConvert.ToBoolean(RSOther.Get_Fields_Boolean("oiusesound4")))
                    {
                        txtUseSV4.Text = "Yes";
                    }
                    else
                    {
                        txtUseSV4.Text = "No";
                    }

                    txtSV4.Text = Strings.Format(Conversion.Val(RSOther.Get_Fields_Int32("oisoundvalue4")),
                        "#,###,###,##0");
                    txtType5.Text = Strings.Trim(RSOther.Get_Fields_Int32("OITYPE5") + "");
                    txtYear5.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIYEAR5") + "");
                    txtUnits5.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIUNITS5") + "");
                    txtOIGrade5.Text = Strings.Trim(RSOther.Get_Fields_Int32("oigradecd5") + "");
                    txtGrade5.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIGRADEPCT5") + "");
                    txtCondition5.Text = Strings.Trim(RSOther.Get_Fields_Int32("OICOND5") + "");
                    txtPhysical5.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTPHYS5") + "");
                    txtFunctional5.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTFUNCT5") + "");
                    if (FCConvert.ToBoolean(RSOther.Get_Fields_Boolean("oiusesound5")))
                    {
                        txtUseSV5.Text = "Yes";
                    }
                    else
                    {
                        txtUseSV5.Text = "No";
                    }

                    txtSV5.Text = Strings.Format(Conversion.Val(RSOther.Get_Fields_Int32("oisoundvalue5")),
                        "#,###,###,##0");
                    txtType6.Text = Strings.Trim(RSOther.Get_Fields_Int32("OITYPE6") + "");
                    txtYear6.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIYEAR6") + "");
                    txtUnits6.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIUNITS6") + "");
                    txtOIGrade6.Text = Strings.Trim(RSOther.Get_Fields_Int32("oigradecd6") + "");
                    txtGrade6.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIGRADEPCT6") + "");
                    txtCondition6.Text = Strings.Trim(RSOther.Get_Fields_Int32("OICOND6") + "");
                    txtPhysical6.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTPHYS6") + "");
                    txtFunctional6.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTFUNCT6") + "");
                    if (FCConvert.ToBoolean(RSOther.Get_Fields_Boolean("oiusesound6")))
                    {
                        txtUseSV6.Text = "Yes";
                    }
                    else
                    {
                        txtUseSV6.Text = "No";
                    }

                    txtSV6.Text = Strings.Format(Conversion.Val(RSOther.Get_Fields_Int32("oisoundvalue6")),
                        "#,###,###,##0");
                    txtType7.Text = Strings.Trim(RSOther.Get_Fields_Int32("OITYPE7") + "");
                    txtYear7.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIYEAR7") + "");
                    txtUnits7.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIUNITS7") + "");
                    txtOIGrade7.Text = Strings.Trim(RSOther.Get_Fields_Int32("oigradecd7") + "");
                    txtGrade7.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIGRADEPCT7") + "");
                    txtCondition7.Text = Strings.Trim(RSOther.Get_Fields_Int32("OICOND7") + "");
                    txtPhysical7.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTPHYS7") + "");
                    txtFunctional7.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTFUNCT7") + "");
                    if (FCConvert.ToBoolean(RSOther.Get_Fields_Boolean("oiusesound7")))
                    {
                        txtUseSV7.Text = "Yes";
                    }
                    else
                    {
                        txtUseSV7.Text = "No";
                    }

                    txtSV7.Text = Strings.Format(Conversion.Val(RSOther.Get_Fields_Int32("oisoundvalue7")),
                        "#,###,###,##0");
                    if (FCConvert.ToBoolean(RSOther.Get_Fields_Boolean("oiusesound8")))
                    {
                        txtUseSV8.Text = "Yes";
                    }
                    else
                    {
                        txtUseSV8.Text = "No";
                    }

                    txtSV8.Text = Strings.Format(Conversion.Val(RSOther.Get_Fields_Int32("oisoundvalue8")),
                        "#,###,###,##0");
                    txtType8.Text = Strings.Trim(RSOther.Get_Fields_Int32("OITYPE8") + "");
                    txtYear8.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIYEAR8") + "");
                    txtUnits8.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIUNITS8") + "");
                    txtOIGrade8.Text = Strings.Trim(RSOther.Get_Fields_Int32("oigradecd8") + "");
                    txtGrade8.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIGRADEPCT8") + "");
                    txtCondition8.Text = Strings.Trim(RSOther.Get_Fields_Int32("OICOND8") + "");
                    txtPhysical8.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTPHYS8") + "");
                    txtFunctional8.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTFUNCT8") + "");
                    txtType9.Text = Strings.Trim(RSOther.Get_Fields_Int32("OITYPE9") + "");
                    txtYear9.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIYEAR9") + "");
                    txtUnits9.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIUNITS9") + "");
                    txtOIGrade9.Text = Strings.Trim(RSOther.Get_Fields_Int32("oigradecd9") + "");
                    txtGrade9.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIGRADEPCT9") + "");
                    txtCondition9.Text = Strings.Trim(RSOther.Get_Fields_Int32("OICOND9") + "");
                    txtPhysical9.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTPHYS9") + "");
                    txtFunctional9.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTFUNCT9") + "");
                    if (FCConvert.ToBoolean(RSOther.Get_Fields_Boolean("oiusesound9")))
                    {
                        txtUseSV9.Text = "Yes";
                    }
                    else
                    {
                        txtUseSV9.Text = "No";
                    }

                    txtSV9.Text = Strings.Format(Conversion.Val(RSOther.Get_Fields_Int32("oisoundvalue9")),
                        "#,###,###,##0");
                    txtType10.Text = Strings.Trim(RSOther.Get_Fields_Int32("OITYPE10") + "");
                    txtYear10.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIYEAR10") + "");
                    txtUnits10.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIUNITS10") + "");
                    txtOIGrade10.Text = Strings.Trim(RSOther.Get_Fields_Int32("oigradecd10") + "");
                    txtGrade10.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIGRADEPCT10") + "");
                    txtCondition10.Text = Strings.Trim(RSOther.Get_Fields_Int32("OICOND10") + "");
                    txtPhysical10.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTPHYS10") + "");
                    txtFunctional10.Text = Strings.Trim(RSOther.Get_Fields_Int32("OIPCTFUNCT10") + "");
                    if (FCConvert.ToBoolean(RSOther.Get_Fields_Boolean("oiusesound10")))
                    {
                        txtUseSV10.Text = "Yes";
                    }
                    else
                    {
                        txtUseSV10.Text = "No";
                    }

                    txtSV10.Text = Strings.Format(Conversion.Val(RSOther.Get_Fields_Int32("oisoundvalue10")),
                        "#,###,###,##0");
                }
            }
        }

		public void filldwellingboxes(string strSaleDate = "")
		{
			// set dwelling data and enable the boxes
			if (!Information.IsDate(strSaleDate))
			{
				RSOther.OpenRecordset("SELECT distinct * FROM Dwelling where RSAccount = " + rsTemp.Get_Fields_Int32("RSAccount") + " and RSCard = " + rsTemp.Get_Fields_Int32("RSCard"), modGlobalVariables.strREDatabase);
			}
			else
			{
				RSOther.OpenRecordset("SELECT distinct * FROM srDwelling where RSAccount = " + rsTemp.Get_Fields_Int32("RSAccount") + " and RSCard = " + rsTemp.Get_Fields_Int32("RSCard") + " and saledate = '" + strSaleDate + "' ", modGlobalVariables.strREDatabase);
			}
			if (!RSOther.EndOfFile())
			{
				Field20.Visible = true;
				txtBuildingStyle.Text = Strings.Trim(RSOther.Get_Fields_Int32("DISTYLE") + "");
				txtBuildingStyle.Visible = true;
				txtDwellingUnits.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIUNITSDWELLING") + "");
				txtDwellingUnits.Visible = true;
				txtOtherUnits.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIUNITSOTHER") + "");
				txtOtherUnits.Visible = true;
				txtStories.Text = Strings.Trim(RSOther.Get_Fields_Int32("DISTORIES") + "");
				txtStories.Visible = true;
				txtExteriorWalls.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIEXTWALLS") + "");
				txtExteriorWalls.Visible = true;
				txtRoofSurface.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIROOF") + "");
				txtRoofSurface.Visible = true;
				txtMasonryTrim.Text = Strings.Trim(RSOther.Get_Fields_Int32("DISFMASONRY") + "");
				txtMasonryTrim.Visible = true;
				txtOpen3.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIOPEN3") + "");
				txtOpen3.Visible = true;
				txtOpen4.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIOPEN4") + "");
				txtOpen4.Visible = true;
				txtYearBuilt.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIYEARBUILT") + "");
				txtYearBuilt.Visible = true;
				txtYearRemodeled.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIYEARREMODEL") + "");
				txtYearRemodeled.Visible = true;
				txtFoundation.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIFOUNDATION") + "");
				txtFoundation.Visible = true;
				txtBasement.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIBSMT") + "");
				txtBasement.Visible = true;
				txtBSMTGar.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIBSMTGAR") + "");
				txtBSMTGar.Visible = true;
				txtWetBasement.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIWETBSMT") + "");
				txtWetBasement.Visible = true;
				txtBSMTLiving.Text = Strings.Trim(RSOther.Get_Fields_Int32("DISFBSMTLIVING") + "");
				txtBSMTLiving.Visible = true;
				txtBSMTGrade.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIBSMTFINGRADE1") + "") + "" + Strings.Trim(RSOther.Get_Fields_Int32("DIBSMTFINGRADE2") + "");
				txtBSMTGrade.Visible = true;
				txtOpen5.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIOPEN5") + "");
				txtOpen5.Visible = true;
				txtHeatType.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIHEAT") + "");
				txtHeatType.Visible = true;
				txtPercentHeated.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIPCTHEAT") + "");
				txtPercentHeated.Visible = true;
				txtCoolType.Text = Strings.Trim(RSOther.Get_Fields_Int32("DICOOL") + "");
				txtCoolType.Visible = true;
				txtPercentCooled.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIPCTCOOL") + "");
				txtPercentCooled.Visible = true;
				txtKitchenStyle.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIKITCHENS") + "");
				txtKitchenStyle.Visible = true;
				txtBathStyle.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIBATHS") + "");
				txtBathStyle.Visible = true;
				txtRooms.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIROOMS") + "");
				txtRooms.Visible = true;
				txtBedrooms.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIBEDROOMS") + "");
				txtBedrooms.Visible = true;
				txtFullBaths.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIFULLBATHS") + "");
				txtFullBaths.Visible = true;
				txtHalfBaths.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIHALFBATHS") + "");
				txtHalfBaths.Visible = true;
				txtFixtures.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIADDNFIXTURES") + "");
				txtFixtures.Visible = true;
				txtfireplaces.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIFIREPLACES") + "");
				txtfireplaces.Visible = true;
				txtLayout.Text = Strings.Trim(RSOther.Get_Fields_Int32("DILAYOUT") + "");
				txtLayout.Visible = true;
				txtAttic.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIATTIC") + "");
				txtAttic.Visible = true;
				txtInsulation.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIINSULATION") + "");
				txtInsulation.Visible = true;
				txtUnfinished.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIPCTUNFINISHED") + "");
				txtUnfinished.Visible = true;
				txtGrade.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIGRADE1") + "");
				txtGrade.Visible = true;
				txtFactor.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIGRADE2") + "");
				txtFactor.Visible = true;
				txtSQFootage.Text = Strings.Trim(RSOther.Get_Fields_Int32("DISQFT") + "");
				txtSQFootage.Visible = true;
				txtCondition.Text = Strings.Trim(RSOther.Get_Fields_Int32("DICONDITION") + "");
				txtCondition.Visible = true;
				txtPhysicalGood.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIPCTPHYS") + "");
				txtPhysicalGood.Visible = true;
				txtFunctGood.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIPCTFUNCT") + "");
				txtFunctGood.Visible = true;
				txtFunctCode.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIFUNCTCODE") + "");
				txtFunctCode.Visible = true;
				txtEconGood.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIPCTECON") + "");
				txtEconGood.Visible = true;
				txtEconCode.Text = Strings.Trim(RSOther.Get_Fields_Int32("DIECONCODE") + "");
				txtEconCode.Visible = true;
				// txtEntranceCode = Trim(RSOther.Fields("DIENTRANCECODE") & " ")
				txtEntranceCode.Visible = true;
				// txtInfoCode = Trim(RSOther.Fields("DIINFORMATION") & " ")
				txtInfoCode.Visible = true;
				// txtDateInspected = fixthedate(Trim(RSOther.Fields("DIDATEINSPECTED") & ""))
				txtDateInspected.Visible = true;
				Label41.Visible = true;
				Label42.Visible = true;
				Label43.Visible = true;
				Label44.Visible = true;
				Label45.Visible = true;
				Label46.Visible = true;
				Label47.Visible = true;
				Label48.Visible = true;
				Label49.Visible = true;
				Label50.Visible = true;
				Label51.Visible = true;
				Label52.Visible = true;
				Label53.Visible = true;
				Label54.Visible = true;
				Label55.Visible = true;
				Label56.Visible = true;
				Label57.Visible = true;
				Label58.Visible = true;
				Label59.Visible = true;
				Label60.Visible = true;
				Label61.Visible = true;
				Label62.Visible = true;
				Label63.Visible = true;
				Label64.Visible = true;
				Label65.Visible = true;
				Label66.Visible = true;
				Label67.Visible = true;
				Label68.Visible = true;
				Label69.Visible = true;
				Label70.Visible = true;
				Label71.Visible = true;
				Label72.Visible = true;
				Label73.Visible = true;
				Label74.Visible = true;
				Label75.Visible = true;
				Label76.Visible = true;
				Label77.Visible = true;
				Label78.Visible = true;
				Label79.Visible = true;
				Label80.Visible = true;
				Label81.Visible = true;
				Label82.Visible = true;
				Label83.Visible = true;
				Label84.Visible = true;
				Label85.Visible = true;
				Label86.Visible = true;
			}
		}

		public void fillcommercialboxes(string strSaleDate = "")
		{
			if (!Information.IsDate(strSaleDate))
			{
				RSOther.OpenRecordset("SELECT * FROM Commercial where RSAccount = " + rsTemp.Get_Fields_Int32("RSAccount") + " and RSCard = " + rsTemp.Get_Fields_Int32("RSCard"), modGlobalVariables.strREDatabase);
			}
			else
			{
				RSOther.OpenRecordset("SELECT * FROM Commercial where RSAccount = " + rsTemp.Get_Fields_Int32("RSAccount") + " and RSCard = " + rsTemp.Get_Fields_Int32("RSCard") + " and saledate = '" + strSaleDate + "'", modGlobalVariables.strREDatabase);
			}
			if (!RSOther.EndOfFile())
			{
				Field68.Visible = true;
				txtOCC1.Visible = true;
				txtOCC1.Text = Strings.Trim(RSOther.Get_Fields_Int32("OCC1") + "");
				txtOCC2.Visible = true;
				txtOCC2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("OCC2")));
				txtDwellingUnits1.Visible = true;
				txtDwellingUnits1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("DWEL1")));
				txtDwellingUnits2.Visible = true;
				txtDwellingUnits2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("DWEL2")));
				txtclass1.Visible = true;
				txtclass1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1class")));
				txtclass2.Visible = true;
				txtclass2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2class")));
				txtqual1.Visible = true;
				txtqual1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1quality")));
				txtqual2.Visible = true;
				txtqual2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2quality")));
				txtgradefactor1.Visible = true;
				txtgradefactor1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1grade")));
				txtgradefactor2.Visible = true;
				txtgradefactor2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2grade")));
				txtExteriorWalls1.Visible = true;
				txtExteriorWalls1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1extwalls")));
				txtExteriorWalls2.Visible = true;
				txtExteriorWalls2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2extwalls")));
				txtstories1.Visible = true;
				txtstories1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1stories")));
				txtstories2.Visible = true;
				txtstories2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2stories")));
				txtheight1.Visible = true;
				txtheight1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1height")));
				txtHeight2.Visible = true;
				txtHeight2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2height")));
				txtbasefloor1.Visible = true;
				txtbasefloor1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1floor")));
				txtBaseFloor2.Visible = true;
				txtBaseFloor2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2floor")));
				txtperimeter1.Visible = true;
				txtperimeter1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1perimeter")));
				txtPerimeter2.Visible = true;
				txtPerimeter2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2perimeter")));
				txtheatcool1.Visible = true;
				txtheatcool1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1heat")));
				txtHeatCool2.Visible = true;
				txtHeatCool2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2heat")));
				txtYearBuilt1.Visible = true;
				txtYearBuilt1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1built")));
				txtYearBuilt2.Visible = true;
				txtYearBuilt2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2built")));
				txtYearRemodelled1.Visible = true;
				txtYearRemodelled1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1remodel")));
				txtYearRemodelled2.Visible = true;
				txtYearRemodelled2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2remodel")));
				txtcond1.Visible = true;
				txtcond1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1condition")));
				txtcond2.Visible = true;
				txtcond2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2condition")));
				txtphys1.Visible = true;
				txtphys1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1phys")));
				txtphys2.Visible = true;
				txtphys2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2phys")));
				txtfunc1.Visible = true;
				txtfunc1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c1funct")));
				txtfunc2.Visible = true;
				txtfunc2.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("c2funct")));
				txteconomic1.Visible = true;
				txteconomic1.Text = Strings.Trim(FCConvert.ToString(RSOther.Get_Fields_Int32("cmecon")));
				txtEntranceCode.Visible = true;
				txtInfoCode.Visible = true;
				txtDateInspected.Visible = true;
				Label42.Visible = true;
				Label47.Visible = true;
				Label49.Visible = true;
				Label51.Visible = true;
				Label83.Visible = true;
				Label84.Visible = true;
				Label86.Visible = true;
				Label93.Visible = true;
				Label94.Visible = true;
				Label95.Visible = true;
				Label96.Visible = true;
				Label97.Visible = true;
				Label98.Visible = true;
				Label99.Visible = true;
				Label100.Visible = true;
				Label101.Visible = true;
				Label102.Visible = true;
				Label103.Visible = true;
				Label104.Visible = true;
				Label105.Visible = true;
				Label106.Visible = true;
				Label107.Visible = true;
				Label108.Visible = true;
				Label109.Visible = true;
				Label110.Visible = true;
				Label111.Visible = true;
				Label112.Visible = true;
				Label113.Visible = true;
				Label114.Visible = true;
				Label115.Visible = true;
				Label116.Visible = true;
				Label117.Visible = true;
			}
		}

		public void makedwellinginvisible()
		{
			Field20.Visible = false;
			txtBuildingStyle.Visible = false;
			txtDwellingUnits.Visible = false;
			txtOtherUnits.Visible = false;
			txtStories.Visible = false;
			txtExteriorWalls.Visible = false;
			txtRoofSurface.Visible = false;
			txtMasonryTrim.Visible = false;
			txtOpen3.Visible = false;
			txtOpen4.Visible = false;
			txtYearBuilt.Visible = false;
			txtYearRemodeled.Visible = false;
			txtFoundation.Visible = false;
			txtBasement.Visible = false;
			txtBSMTGar.Visible = false;
			txtWetBasement.Visible = false;
			txtBSMTLiving.Visible = false;
			txtBSMTGrade.Visible = false;
			txtOpen5.Visible = false;
			txtHeatType.Visible = false;
			txtPercentHeated.Visible = false;
			txtCoolType.Visible = false;
			txtPercentCooled.Visible = false;
			txtKitchenStyle.Visible = false;
			txtBathStyle.Visible = false;
			txtRooms.Visible = false;
			txtBedrooms.Visible = false;
			txtFullBaths.Visible = false;
			txtHalfBaths.Visible = false;
			txtFixtures.Visible = false;
			txtfireplaces.Visible = false;
			txtLayout.Visible = false;
			txtAttic.Visible = false;
			txtInsulation.Visible = false;
			txtUnfinished.Visible = false;
			txtGrade.Visible = false;
			txtFactor.Visible = false;
			txtSQFootage.Visible = false;
			txtCondition.Visible = false;
			txtPhysicalGood.Visible = false;
			txtFunctGood.Visible = false;
			txtFunctCode.Visible = false;
			txtEconGood.Visible = false;
			txtEconCode.Visible = false;
			txtEntranceCode.Visible = false;
			txtInfoCode.Visible = false;
			txtDateInspected.Visible = false;
			Label41.Visible = false;
			Label42.Visible = false;
			Label43.Visible = false;
			Label44.Visible = false;
			Label45.Visible = false;
			Label46.Visible = false;
			Label47.Visible = false;
			Label48.Visible = false;
			Label49.Visible = false;
			Label50.Visible = false;
			Label51.Visible = false;
			Label52.Visible = false;
			Label53.Visible = false;
			Label54.Visible = false;
			Label55.Visible = false;
			Label56.Visible = false;
			Label57.Visible = false;
			Label58.Visible = false;
			Label59.Visible = false;
			Label60.Visible = false;
			Label61.Visible = false;
			Label62.Visible = false;
			Label63.Visible = false;
			Label64.Visible = false;
			Label65.Visible = false;
			Label66.Visible = false;
			Label67.Visible = false;
			Label68.Visible = false;
			Label69.Visible = false;
			Label70.Visible = false;
			Label71.Visible = false;
			Label72.Visible = false;
			Label73.Visible = false;
			Label74.Visible = false;
			Label75.Visible = false;
			Label76.Visible = false;
			Label77.Visible = false;
			Label78.Visible = false;
			Label79.Visible = false;
			Label80.Visible = false;
			Label81.Visible = false;
			Label82.Visible = false;
			Label83.Visible = false;
			Label84.Visible = false;
			Label85.Visible = false;
			Label86.Visible = false;
		}

		public void makecommercialinvisible()
		{
			int forindex;
			Field68.Visible = false;
			txtOCC1.Visible = false;
			txtOCC2.Visible = false;
			txtDwellingUnits1.Visible = false;
			txtDwellingUnits2.Visible = false;
			txtclass1.Visible = false;
			txtclass2.Visible = false;
			txtqual1.Visible = false;
			txtqual2.Visible = false;
			txtgradefactor1.Visible = false;
			txtgradefactor2.Visible = false;
			txtExteriorWalls1.Visible = false;
			txtExteriorWalls2.Visible = false;
			txtstories1.Visible = false;
			txtstories2.Visible = false;
			txtheight1.Visible = false;
			txtHeight2.Visible = false;
			txtbasefloor1.Visible = false;
			txtBaseFloor2.Visible = false;
			txtperimeter1.Visible = false;
			txtPerimeter2.Visible = false;
			txtheatcool1.Visible = false;
			txtHeatCool2.Visible = false;
			txtYearBuilt1.Visible = false;
			txtYearBuilt2.Visible = false;
			txtYearRemodelled1.Visible = false;
			txtYearRemodelled2.Visible = false;
			txtcond1.Visible = false;
			txtcond2.Visible = false;
			txtphys1.Visible = false;
			txtphys2.Visible = false;
			txtfunc1.Visible = false;
			txtfunc2.Visible = false;
			txteconomic1.Visible = false;
			txtEntranceCode.Visible = false;
			txtInfoCode.Visible = false;
			txtDateInspected.Visible = false;
			Label93.Visible = false;
			Label94.Visible = false;
			Label95.Visible = false;
			Label96.Visible = false;
			Label97.Visible = false;
			Label98.Visible = false;
			Label99.Visible = false;
			Label100.Visible = false;
			Label101.Visible = false;
			Label102.Visible = false;
			Label103.Visible = false;
			Label104.Visible = false;
			Label105.Visible = false;
			Label106.Visible = false;
			Label107.Visible = false;
			Label108.Visible = false;
			Label109.Visible = false;
			Label110.Visible = false;
			Label111.Visible = false;
			Label112.Visible = false;
			Label113.Visible = false;
			Label114.Visible = false;
			Label115.Visible = false;
			Label116.Visible = false;
			Label117.Visible = false;
			Label83.Visible = false;
			Label84.Visible = false;
			Label86.Visible = false;
		}
		// vbPorter upgrade warning: lngSaleID As int	OnWriteFCConvert.ToDouble(
		public void Start(bool boolByExtract, int intAcctNum = 0, int intCardNum = 0, int lngSaleID = 0)
		{
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin;
			strREFullDBName = rsTemp.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			strMasterJoinJoin = modREMain.GetMasterJoinForJoin();
			string strSQL = "";
			int lngUID;
			boolSales = false;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (intAcctNum > 0)
			{
				if (lngSaleID < 1)
				{
					if (!(intCardNum > 0))
					{
						rsTemp.OpenRecordset(strMasterJoin + " where not rsdeleted = 1 and rsaccount = " + FCConvert.ToString(intAcctNum) + " order by rscard", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsTemp.OpenRecordset(strMasterJoin + " where not rsdeleted = 1 and rsaccount = " + FCConvert.ToString(intAcctNum) + " and rscard = " + FCConvert.ToString(FCConvert.ToInt16(intCardNum)), modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					boolSales = true;
					if (!(intCardNum > 0))
					{
						rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount = " + FCConvert.ToString(intAcctNum) + " and saleid = " + FCConvert.ToString(lngSaleID) + " order by rscard", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsTemp.OpenRecordset("select * from srmaster where not rsdeleted = 1 and rsaccount = " + FCConvert.ToString(intAcctNum) + " and saleid = " + FCConvert.ToString(lngSaleID) + " and rscard = " + FCConvert.ToString(intCardNum), modGlobalVariables.strREDatabase);
					}
				}
			}
			else
			{
				if (boolByExtract)
				{
					// strSQL = "select master.* from (master inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and master.rsdeleted = 0"
					strSQL = "select * from (" + strMasterJoinQuery + " inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid)) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and mj.rsdeleted = 0";
					rsTemp.OpenRecordset("select TITLE FROM EXTRACT WHERe reportnumber = 0 and userid = " + FCConvert.ToString(lngUID), modGlobalVariables.strREDatabase);
					if (!rsTemp.EndOfFile())
					{
						if (FCConvert.ToString(rsTemp.Get_Fields_String("title")) != string.Empty)
						{
							lblTitle2.Text = FCConvert.ToString(rsTemp.Get_Fields_String("title"));
						}
					}
				}
				else
				{
					strSQL = strMasterJoin + " where rsdeleted = 0";
				}
				if (modGlobalVariables.Statics.boolRange)
				{
					if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
					{
						rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " + modPrintRoutines.Statics.gstrFieldName + " <= " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + " order by " + modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
					}
					else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
					{
						rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' order by " + modPrintRoutines.Statics.gstrFieldName + ",CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float)  ,rsaccount,RSCard", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' order by " + modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
					}
				}
				else
				{
					if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
					{
						rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float)  ", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",rscard", modGlobalVariables.strREDatabase);
					}
				}
			}
			this.PageSettings.Margins.Top = 300 / 1440f;
			this.PageSettings.Margins.Bottom = 300 / 1440f;
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "InputCard");
		}
		// vbPorter upgrade warning: intLandType As short	OnWriteFCConvert.ToDouble(
		private string FormatLandUnits_26(string strUnitsA, string strUnitsB, short intLandType)
		{
			return FormatLandUnits(ref strUnitsA, ref strUnitsB, ref intLandType);
		}

		private string FormatLandUnits(ref string strUnitsA, ref string strUnitsB, ref short intLandType)
		{
			string FormatLandUnits = "";
			// based on the land type, format the units string
			FormatLandUnits = "";
			switch (intLandType)
			{
				case 0:
					{
						FormatLandUnits = "";
						break;
					}
				case 99:
					{
						FormatLandUnits = "";
						break;
					}
				default:
					{
						modGlobalVariables.Statics.LandTypes.FindCode(intLandType);
						switch (modGlobalVariables.Statics.LandTypes.UnitsType)
						{
						// Case 11 To 15
							case modREConstants.CNSTLANDTYPEFRONTFOOT:
							case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
								{
									FormatLandUnits = Strings.Format(Conversion.Val(strUnitsA), "000") + " X " + Strings.Format(Conversion.Val(strUnitsB), "000");
									// Case 16 To 20
									break;
								}
							case modREConstants.CNSTLANDTYPESQUAREFEET:
								{
									FormatLandUnits = Strings.Format(FCConvert.ToString(Conversion.Val(strUnitsA + strUnitsB)), "#,###,###,##0");
									break;
								}
							default:
								{
									FormatLandUnits = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strUnitsA + strUnitsB))) / 100, "#,###,##0.00");
									break;
								}
						}
						//end switch
						break;
					}
			}
			//end switch
			return FormatLandUnits;
		}

		
	}
}
