﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewSharonImport.
	/// </summary>
	partial class rptNewSharonImport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewSharonImport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNumNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAcct,
				this.txtName,
				this.txtMapLot,
				this.txtLand,
				this.txtBldg,
				this.txtNew
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTotLand,
				this.txtTotBuilding,
				this.Label8,
				this.Label9,
				this.txtNumUpdated,
				this.txtNumNew,
				this.Label11,
				this.Label12
			});
			this.ReportFooter.Height = 0.6354167F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.lblTitle,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label7
			});
			this.PageHeader.Height = 0.875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Imported Accounts";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.0625F;
			this.txtMuni.Width = 2.875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.04166667F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Text = "Account";
			this.Label1.Top = 0.6875F;
			this.Label1.Width = 0.6458333F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.75F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Name";
			this.Label2.Top = 0.6875F;
			this.Label2.Width = 2.395833F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 4.916667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Text = "Land";
			this.Label3.Top = 0.6875F;
			this.Label3.Width = 0.9375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 5.916667F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: right";
			this.Label4.Text = "Building";
			this.Label4.Top = 0.6875F;
			this.Label4.Width = 0.9375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 3.6875F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Map / Lot";
			this.Label7.Top = 0.6875F;
			this.Label7.Width = 1.020833F;
			// 
			// txtAcct
			// 
			this.txtAcct.Height = 0.1770833F;
			this.txtAcct.Left = 0.04166667F;
			this.txtAcct.Name = "txtAcct";
			this.txtAcct.Style = "text-align: right";
			this.txtAcct.Text = null;
			this.txtAcct.Top = 0F;
			this.txtAcct.Width = 0.6458333F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1770833F;
			this.txtName.Left = 0.75F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "text-align: left";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 2.895833F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.Height = 0.1770833F;
			this.txtMapLot.Left = 3.6875F;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Style = "text-align: left";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0F;
			this.txtMapLot.Width = 1.208333F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.1770833F;
			this.txtLand.Left = 4.916667F;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Text = null;
			this.txtLand.Top = 0F;
			this.txtLand.Width = 0.9375F;
			// 
			// txtBldg
			// 
			this.txtBldg.Height = 0.1770833F;
			this.txtBldg.Left = 5.916667F;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "text-align: right";
			this.txtBldg.Text = null;
			this.txtBldg.Top = 0F;
			this.txtBldg.Width = 0.9375F;
			// 
			// txtNew
			// 
			this.txtNew.Height = 0.1770833F;
			this.txtNew.Left = 6.90625F;
			this.txtNew.Name = "txtNew";
			this.txtNew.Style = "font-weight: bold; text-align: left";
			this.txtNew.Text = null;
			this.txtNew.Top = 0F;
			this.txtNew.Width = 0.3645833F;
			// 
			// txtTotLand
			// 
			this.txtTotLand.Height = 0.1770833F;
			this.txtTotLand.Left = 5.3125F;
			this.txtTotLand.Name = "txtTotLand";
			this.txtTotLand.Style = "text-align: right";
			this.txtTotLand.Text = null;
			this.txtTotLand.Top = 0.1875F;
			this.txtTotLand.Width = 1.25F;
			// 
			// txtTotBuilding
			// 
			this.txtTotBuilding.Height = 0.1770833F;
			this.txtTotBuilding.Left = 5.3125F;
			this.txtTotBuilding.Name = "txtTotBuilding";
			this.txtTotBuilding.Style = "text-align: right";
			this.txtTotBuilding.Text = null;
			this.txtTotBuilding.Top = 0.375F;
			this.txtTotBuilding.Width = 1.25F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.25F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: right";
			this.Label8.Text = "Land";
			this.Label8.Top = 0.1875F;
			this.Label8.Width = 0.9375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.25F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Text = "Building";
			this.Label9.Top = 0.375F;
			this.Label9.Width = 0.9375F;
			// 
			// txtNumUpdated
			// 
			this.txtNumUpdated.Height = 0.1770833F;
			this.txtNumUpdated.Left = 1.4375F;
			this.txtNumUpdated.Name = "txtNumUpdated";
			this.txtNumUpdated.Style = "text-align: left";
			this.txtNumUpdated.Text = null;
			this.txtNumUpdated.Top = 0.1875F;
			this.txtNumUpdated.Width = 1.25F;
			// 
			// txtNumNew
			// 
			this.txtNumNew.Height = 0.1770833F;
			this.txtNumNew.Left = 1.4375F;
			this.txtNumNew.Name = "txtNumNew";
			this.txtNumNew.Style = "text-align: left";
			this.txtNumNew.Text = null;
			this.txtNumNew.Top = 0.375F;
			this.txtNumNew.Width = 1.25F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: left";
			this.Label11.Text = "Updated";
			this.Label11.Top = 0.1875F;
			this.Label11.Width = 0.75F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: left";
			this.Label12.Text = "New";
			this.Label12.Top = 0.375F;
			this.Label12.Width = 0.75F;
			// 
			// rptNewSharonImport
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNew;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumNew;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
