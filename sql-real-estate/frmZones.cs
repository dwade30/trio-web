﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmZones.
	/// </summary>
	public partial class frmZones : BaseForm
	{
		public frmZones()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmZones InstancePtr
		{
			get
			{
				return (frmZones)Sys.GetInstance(typeof(frmZones));
			}
		}

		protected frmZones _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDZONECOLAUTOID = 0;
		const int CNSTGRIDZONECOLCODE = 1;
		const int CNSTGRIDZONECOLDESCRIPTION = 2;
		const int CNSTGRIDZONECOLSHORTDESCRIPTION = 3;
		const int CNSTGRIDZONECOLSECDESCRIPTION = 4;
		const int CNSTGRIDZONECOLSECSHORT = 5;
		const int CNSTGRIDZONECOLTOWNCODE = 6;
		const int CNSTGRIDNEIGHCOLAUTOID = 0;
		const int CNSTGRIDNEIGHCOLCODE = 2;
		const int CNSTGRIDNEIGHCOLDESC = 1;
		const int CNSTGRIDNEIGHCOLLANDFACTOR = 3;
		const int CNSTGRIDNEIGHCOLBLDGFACTOR = 4;
		const int CNSTGRIDNEIGHCOLSCHEDULE = 5;
		const int CNSTGRIDNEIGHCOLZONE = 6;
		const int CNSTGRIDNEIGHCOLTOWNCODE = 7;
		const int CNSTGRIDNEIGHCOLPARENT = 8;
		private string[] strComboList = null;

		public void Init()
		{
			this.Show(App.MainForm);
		}

		private void frmZones_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmZones_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmZones properties;
			//frmZones.FillStyle	= 0;
			//frmZones.ScaleWidth	= 9300;
			//frmZones.ScaleHeight	= 7590;
			//frmZones.LinkTopic	= "Form2";
			//frmZones.LockControls	= true;
			//frmZones.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridTownCode();
			SetupGridZone();
			SetupGridNeigh();
			// LoadGridNeigh
			LoadGridZone();
		}

		private void frmZones_Resize(object sender, System.EventArgs e)
		{
			ResizeGridZone();
			ResizeGridTownCode();
			ResizeGridNeigh();
		}

		private void GridNeigh_ComboCloseUp(object sender, EventArgs e)
		{
			if (GridNeigh.Col == CNSTGRIDNEIGHCOLCODE)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from neighborhood where code = " + FCConvert.ToString(Conversion.Val(GridNeigh.EditText)) + " and townnumber = " + GridNeigh.TextMatrix(GridNeigh.Row, CNSTGRIDNEIGHCOLTOWNCODE), modGlobalVariables.strREDatabase);
				if (!rsLoad.EndOfFile())
				{
					GridNeigh.TextMatrix(GridNeigh.Row, CNSTGRIDNEIGHCOLDESC, FCConvert.ToString(rsLoad.Get_Fields_String("description")));
				}
				GridNeigh.RowData(GridNeigh.Row, true);
			}
		}

		private void GridNeigh_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (GridNeigh.Row < 1)
				return;
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteNeigh(GridNeigh.Row);
						break;
					}
				case Keys.Insert:
					{
						KeyCode = 0;
						AddNeigh();
						break;
					}
			}
			//end switch
		}

		private void AddNeigh()
		{
			int lngZoneRow;
			if (GridZone.Row < 1)
				return;
			if (GridZone.RowHidden(GridZone.Row))
				return;
			lngZoneRow = GridZone.Row;
			// vbPorter upgrade warning: lngZone As int	OnWrite(string)
			int lngZone;
			int lngRow;
			lngZone = FCConvert.ToInt32(GridZone.TextMatrix(lngZoneRow, CNSTGRIDZONECOLCODE));
			GridNeigh.Rows += 1;
			lngRow = GridNeigh.Rows - 1;
			GridNeigh.TopRow = lngRow;
			GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLPARENT, lngZoneRow);
			GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLAUTOID, 0);
			GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLBLDGFACTOR, 1);
			GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLLANDFACTOR, 1);
			GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLSCHEDULE, 1);
			GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLCODE, 0);
			GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLTOWNCODE, GridZone.TextMatrix(lngZoneRow, CNSTGRIDZONECOLTOWNCODE));
			GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLZONE, lngZone);
			GridNeigh.RowData(lngRow, true);
		}

		private void DeleteNeigh(int lngRow)
		{
			if (lngRow < 1)
				return;
			if (Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLAUTOID)) > 0)
			{
				GridDeleteNeigh.Rows += 1;
				GridDeleteNeigh.TextMatrix(GridDeleteNeigh.Rows - 1, 0, GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLAUTOID));
			}
			GridNeigh.RemoveItem(lngRow);
		}

		private void GridNeigh_RowColChange(object sender, System.EventArgs e)
		{
			switch (GridNeigh.Col)
			{
				case CNSTGRIDNEIGHCOLCODE:
					{
						if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
						{
							GridNeigh.ComboList = strComboList[FCConvert.ToInt32(GridNeigh.TextMatrix(GridNeigh.Row, CNSTGRIDNEIGHCOLTOWNCODE))];
						}
						break;
					}
				default:
					{
						if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
						{
							GridNeigh.ComboList = "";
						}
						break;
					}
			}
			//end switch
		}

		private void GridNeigh_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = GridNeigh.GetFlexRowIndex(e.RowIndex);
			int col = GridNeigh.GetFlexColIndex(e.ColumnIndex);
			if (row > 0)
			{
				GridNeigh.RowData(row, true);
			}
		}

		private void gridTownCode_ComboCloseUp(object sender, EventArgs e)
		{
			gridTownCode.EndEdit();
			//Application.DoEvents();
			GridZone.Focus();
			//Application.DoEvents();
			ReShowGridZone();
		}

		private void DeleteZone(int lngRow)
		{
			if (lngRow < 1)
				return;
			int lngLoop;
			for (lngLoop = GridNeigh.Rows - 1; lngLoop >= 1; lngLoop--)
			{
				if (FCConvert.ToDouble(GridNeigh.TextMatrix(lngLoop, CNSTGRIDNEIGHCOLPARENT)) == lngRow)
				{
					DeleteNeigh(lngLoop);
				}
			}
			// lngLoop
			GridDeleteZone.Rows += 1;
			GridDeleteZone.TextMatrix(GridDeleteZone.Rows - 1, 0, FCConvert.ToString(Conversion.Val(GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLAUTOID))));
			GridZone.RemoveItem(lngRow);
			for (lngLoop = 1; lngLoop <= GridNeigh.Rows - 1; lngLoop++)
			{
				if (FCConvert.ToDouble(GridNeigh.TextMatrix(lngLoop, CNSTGRIDNEIGHCOLPARENT)) > lngRow)
				{
					GridNeigh.TextMatrix(lngLoop, CNSTGRIDNEIGHCOLPARENT, FCConvert.ToDouble(GridNeigh.TextMatrix(lngLoop, CNSTGRIDNEIGHCOLPARENT)) - 1);
				}
			}
			// lngLoop
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				SaveData = false;
				int lngRow;
				GridZone.Col = CNSTGRIDZONECOLAUTOID;
				GridNeigh.Col = CNSTGRIDNEIGHCOLAUTOID;
				for (lngRow = 0; lngRow <= GridDeleteNeigh.Rows - 1; lngRow++)
				{
					rsSave.Execute("delete from zoneschedule where id = " + FCConvert.ToString(Conversion.Val(GridDeleteNeigh.TextMatrix(lngRow, 0))), modGlobalVariables.strREDatabase);
				}
				// lngRow
				GridDeleteNeigh.Rows = 0;
				for (lngRow = 0; lngRow <= GridDeleteZone.Rows - 1; lngRow++)
				{
					rsSave.Execute("delete from zones where id = " + FCConvert.ToString(Conversion.Val(GridDeleteZone.TextMatrix(lngRow, 0))), modGlobalVariables.strREDatabase);
				}
				// lngRow
				GridDeleteZone.Rows = 0;
                //FC:FINAL:AM:#3326 - sort on multiple columns
				//GridNeigh.Col = CNSTGRIDNEIGHCOLCODE;
				//GridNeigh.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				//GridNeigh.Col = CNSTGRIDNEIGHCOLTOWNCODE;
				//GridNeigh.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				//GridNeigh.Col = CNSTGRIDNEIGHCOLZONE;
				//GridNeigh.Sort = FCGrid.SortSettings.flexSortNumericAscending;
                GridNeigh.SortColumns(FCGrid.SortSettings.flexSortNumericAscending, CNSTGRIDNEIGHCOLCODE, CNSTGRIDNEIGHCOLTOWNCODE, CNSTGRIDNEIGHCOLZONE);
				int lngLastCode;
				int lngLastZone;
				int lngLastTown;
				lngLastCode = -1;
				lngLastTown = -1;
				lngLastZone = -1;
				for (lngRow = 1; lngRow <= GridNeigh.Rows - 1; lngRow++)
				{
					if (lngLastZone != Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLZONE)))
					{
						lngLastZone = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLZONE))));
						lngLastCode = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLCODE))));
						lngLastTown = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLTOWNCODE))));
					}
					else if (lngLastTown != Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLTOWNCODE)))
					{
						lngLastCode = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLCODE))));
						lngLastTown = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLTOWNCODE))));
					}
					else if (lngLastCode != Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLCODE)))
					{
						lngLastCode = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLCODE))));
					}
					else
					{
						// same as last one
						MessageBox.Show("You cannot define a Zone / Neighborhood pair twice" + "\r\n" + "Zone " + FCConvert.ToString(lngLastZone) + "  Neighborhood " + FCConvert.ToString(lngLastCode), "Repeat Pair", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveData;
					}
				}
				// lngRow
				for (lngRow = 1; lngRow <= GridZone.Rows - 1; lngRow++)
				{
					if (FCConvert.ToBoolean(GridZone.RowData(lngRow)))
					{
						rsSave.OpenRecordset("select * from zones where id = " + FCConvert.ToString(Conversion.Val(GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLAUTOID))), modGlobalVariables.strREDatabase);
						if (!rsSave.EndOfFile())
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
							GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLAUTOID, FCConvert.ToString(rsSave.Get_Fields_Int32("id")));
						}
						rsSave.Set_Fields("code", FCConvert.ToString(Conversion.Val(GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLCODE))));
						rsSave.Set_Fields("description", Strings.Trim(GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLDESCRIPTION)));
						rsSave.Set_Fields("shortdescription", Strings.Trim(GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLSHORTDESCRIPTION)));
						rsSave.Set_Fields("secondarydescription", Strings.Trim(GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLSECDESCRIPTION)));
						rsSave.Set_Fields("secondaryshort", Strings.Trim(GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLSECSHORT)));
						rsSave.Set_Fields("townnumber", FCConvert.ToString(Conversion.Val(GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLTOWNCODE))));
						rsSave.Update();
						GridZone.RowData(lngRow, false);
					}
				}
				// lngRow
				for (lngRow = 1; lngRow <= GridNeigh.Rows - 1; lngRow++)
				{
					if (FCConvert.ToBoolean(GridNeigh.RowData(lngRow)))
					{
						rsSave.OpenRecordset("select * from zoneschedule where id = " + FCConvert.ToString(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDZONECOLAUTOID))), modGlobalVariables.strREDatabase);
						if (!rsSave.EndOfFile())
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
							GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLAUTOID, FCConvert.ToString(rsSave.Get_Fields_Int32("id")));
						}
						rsSave.Set_Fields("neighborhood", FCConvert.ToString(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLCODE))));
						rsSave.Set_Fields("zone", FCConvert.ToString(Conversion.Val(GridZone.TextMatrix(FCConvert.ToInt32(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLPARENT)), CNSTGRIDZONECOLCODE))));
						rsSave.Set_Fields("schedule", FCConvert.ToString(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLSCHEDULE))));
						rsSave.Set_Fields("landfactor", FCConvert.ToString(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLLANDFACTOR))));
						rsSave.Set_Fields("bldgfactor", FCConvert.ToString(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLBLDGFACTOR))));
						rsSave.Set_Fields("townnumber", FCConvert.ToString(Conversion.Val(GridNeigh.TextMatrix(lngRow, CNSTGRIDNEIGHCOLTOWNCODE))));
						rsSave.Update();
						GridNeigh.RowData(lngRow, false);
					}
				}
				// lngRow
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void GridZone_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (GridZone.Row < 1)
				return;
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						DeleteZone(GridZone.Row);
						break;
					}
				case Keys.Insert:
					{
						AddZone();
						break;
					}
			}
			//end switch
		}

		private void AddZone()
		{
			int lngRow;
			GridZone.Rows += 1;
			lngRow = GridZone.Rows - 1;
			GridZone.TopRow = lngRow;
			GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLAUTOID, FCConvert.ToString(0));
			GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLCODE, FCConvert.ToString(0));
			GridZone.RowData(lngRow, true);
			GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLTOWNCODE, FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
		}

		private void GridZone_RowColChange(object sender, System.EventArgs e)
		{
			if (GridZone.Row == 0)
			{
				ShowNeighs_2(0);
				return;
			}
			ShowNeighs(FCConvert.ToInt16(GridZone.Row));
		}

		private void GridZone_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = GridZone.GetFlexRowIndex(e.RowIndex);
			int col = GridZone.GetFlexColIndex(e.ColumnIndex);
			if (row > 0)
			{
				GridZone.RowData(row, true);
			}
		}

		private void mnuAddNeighborhood_Click(object sender, System.EventArgs e)
		{
			AddNeigh();
		}

		private void mnuAddZone_Click(object sender, System.EventArgs e)
		{
			AddZone();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGridNeigh()
		{
			GridNeigh.ColHidden(CNSTGRIDNEIGHCOLAUTOID, true);
			GridNeigh.ColHidden(CNSTGRIDNEIGHCOLTOWNCODE, true);
			GridNeigh.ColHidden(CNSTGRIDNEIGHCOLZONE, true);
			GridNeigh.ColHidden(CNSTGRIDNEIGHCOLPARENT, true);
			GridNeigh.TextMatrix(0, CNSTGRIDNEIGHCOLLANDFACTOR, "Land Factor");
			GridNeigh.TextMatrix(0, CNSTGRIDNEIGHCOLBLDGFACTOR, "Bldg Factor");
			GridNeigh.TextMatrix(0, CNSTGRIDNEIGHCOLSCHEDULE, "Schedule");
			GridNeigh.TextMatrix(0, CNSTGRIDNEIGHCOLCODE, "Code");
			GridNeigh.TextMatrix(0, CNSTGRIDNEIGHCOLDESC, "Neighborhood");
			GridNeigh.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDNEIGHCOLLANDFACTOR, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridNeigh.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDNEIGHCOLBLDGFACTOR, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridNeigh.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDNEIGHCOLSCHEDULE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridNeigh.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDNEIGHCOLCODE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void SetupGridZone()
		{
            //FC:FINAL:AM:#3327 - don't edit grid on enter
            GridZone.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2;
            GridZone.TextMatrix(0, CNSTGRIDZONECOLCODE, "Zone");
			GridZone.TextMatrix(0, CNSTGRIDZONECOLDESCRIPTION, "Description");
			GridZone.TextMatrix(0, CNSTGRIDZONECOLSHORTDESCRIPTION, "Short Desc.");
			GridZone.TextMatrix(0, CNSTGRIDZONECOLSECDESCRIPTION, "Secondary Description");
			GridZone.TextMatrix(0, CNSTGRIDZONECOLSECSHORT, "Sec. Short");
			GridZone.ColHidden(CNSTGRIDZONECOLAUTOID, true);
			GridZone.ColHidden(CNSTGRIDZONECOLTOWNCODE, true);
			//FC:FINAL:CHN - issue #1614: Zone Form redesign.
			GridZone.ColAlignment(CNSTGRIDZONECOLCODE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGridZone()
		{
			int GridWidth = 0;
			GridWidth = GridZone.WidthOriginal;
			GridZone.ColWidth(CNSTGRIDZONECOLCODE, FCConvert.ToInt32(0.08 * GridWidth));
			GridZone.ColWidth(CNSTGRIDZONECOLDESCRIPTION, FCConvert.ToInt32(0.3 * GridWidth));
			GridZone.ColWidth(CNSTGRIDZONECOLSECDESCRIPTION, FCConvert.ToInt32(0.3 * GridWidth));
			GridZone.ColWidth(CNSTGRIDZONECOLSHORTDESCRIPTION, FCConvert.ToInt32(0.15 * GridWidth));
		}

		private void ResizeGridNeigh()
		{
			int GridWidth = 0;
			GridWidth = GridNeigh.WidthOriginal;
			GridNeigh.ColWidth(CNSTGRIDNEIGHCOLDESC, FCConvert.ToInt32(0.3 * GridWidth));
			GridNeigh.ColWidth(CNSTGRIDNEIGHCOLCODE, FCConvert.ToInt32(0.16 * GridWidth));
			GridNeigh.ColWidth(CNSTGRIDNEIGHCOLBLDGFACTOR, FCConvert.ToInt32(0.17 * GridWidth));
			GridNeigh.ColWidth(CNSTGRIDNEIGHCOLLANDFACTOR, FCConvert.ToInt32(0.17 * GridWidth));
			GridNeigh.ColWidth(CNSTGRIDNEIGHCOLSCHEDULE, FCConvert.ToInt32(0.16 * GridWidth));
		}

		private void LoadGridZone()
		{
			clsDRWrapper rsZone = new clsDRWrapper();
			int lngRow = 0;
			int lngFirst;
			int lngNeighRow;
			clsDRWrapper rsNeigh = new clsDRWrapper();
			clsDRWrapper rsLoad = new clsDRWrapper();
			int intLast;
			intLast = 0;
			lngFirst = 0;
			strComboList = new string[1 + 1];
			rsNeigh.OpenRecordset("select * from neighborhood order by townnumber,code", modGlobalVariables.Statics.strRECostFileDatabase);
			while (!rsNeigh.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsNeigh.Get_Fields("townnumber")) != intLast)
				{
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					Array.Resize(ref strComboList, FCConvert.ToInt32(rsNeigh.Get_Fields("townnumber")) + 1);
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					intLast = FCConvert.ToInt32(rsNeigh.Get_Fields("townnumber"));
				}
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				strComboList[intLast] += rsNeigh.Get_Fields("code") + "\t" + rsNeigh.Get_Fields_String("description") + "|";
				rsNeigh.MoveNext();
			}
			int x;
			//FC:FINAL:AAKV- exception clearing
			//for (x = 0; x < Information.UBound(strComboList, 1); x++)
			for (x = 0; x < Information.UBound(strComboList, 1); x++)
			{
				if (strComboList[x].Length > 0)
				{
					strComboList[x] = Strings.Mid(strComboList[x], 1, strComboList[x].Length - 1);
				}
			}
			// x
			if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				GridNeigh.ColComboList(CNSTGRIDNEIGHCOLCODE, strComboList[0]);
			}
			else
			{
				// GridNeigh.ComboList(CNSTGRIDNEIGHCOLCODE) = strComboList(1)
			}
			lngNeighRow = 0;
			rsZone.OpenRecordset("select * from zones order by code,townnumber", modGlobalVariables.Statics.strRECostFileDatabase);
			rsLoad.OpenRecordset("select * from zoneschedule order by zone,townnumber,neighborhood", modGlobalVariables.Statics.strRECostFileDatabase);
			// vbPorter upgrade warning: aryFields As Variant --> As string
			string[] aryFields = new string[1 + 1];
			aryFields[0] = "Zone";
			aryFields[1] = "TownNumber";
			object[] aryValues = new object[1 + 1];
			while (!rsZone.EndOfFile())
			{
				GridZone.Rows += 1;
				lngRow = GridZone.Rows - 1;
				GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLAUTOID, FCConvert.ToString(rsZone.Get_Fields_Int32("id")));
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLCODE, FCConvert.ToString(rsZone.Get_Fields("code")));
				GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLDESCRIPTION, FCConvert.ToString(rsZone.Get_Fields_String("description")));
				GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLSHORTDESCRIPTION, FCConvert.ToString(rsZone.Get_Fields_String("shortdescription")));
				GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLSECDESCRIPTION, FCConvert.ToString(rsZone.Get_Fields_String("secondarydescription")));
				GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLSECSHORT, FCConvert.ToString(rsZone.Get_Fields_String("secondaryshort")));
				// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
				GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLTOWNCODE, FCConvert.ToString(rsZone.Get_Fields("townnumber")));
				GridZone.RowData(lngRow, false);
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					if (rsZone.Get_Fields("townnumber") != Conversion.Val(gridTownCode.TextMatrix(0, 0)))
					{
						GridZone.RowHidden(lngRow, true);
					}
					if (lngFirst == 0)
						lngFirst = lngRow;
				}
				else
				{
					if (lngFirst == 0)
						lngFirst = lngRow;
				}
				// If rsLoad.FindFirstRecord(, , "zone = " & rsZone.Fields("code") & " and townnumber = " & rsZone.Fields("townnumber")) Then
				// If rsLoad.FindFirstRecord2("zone,townnumber", rsZone.Fields("code") & "," & rsZone.Fields("townnumber"), ",") Then
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
				if (rsLoad.FindFirst("zone = " + rsZone.Get_Fields("code") + " and townnumber = " + rsZone.Get_Fields("townnumber")))
				{
					GridNeigh.Rows += 1;
					lngNeighRow = GridNeigh.Rows - 1;
					GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
					GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLCODE, FCConvert.ToString(rsLoad.Get_Fields_Int32("neighborhood")));
					GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLZONE, FCConvert.ToString(rsLoad.Get_Fields_Int32("zone")));
					GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLSCHEDULE, FCConvert.ToString(rsLoad.Get_Fields_Int32("schedule")));
					GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLLANDFACTOR, FCConvert.ToString(rsLoad.Get_Fields_Double("landfactor")));
					GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLBLDGFACTOR, FCConvert.ToString(rsLoad.Get_Fields_Double("bldgfactor")));
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLTOWNCODE, FCConvert.ToString(rsLoad.Get_Fields("townnumber")));
					// If rsNeigh.FindFirstRecord(, , "code = " & rsLoad.Fields("neighborhood") & " and townnumber = " & rsLoad.Fields("townnumber")) Then
					// If rsNeigh.FindFirstRecord2("code,townnumber", rsLoad.Fields("neighborhood") & "," & rsLoad.Fields("townnumber"), ",") Then
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					if (rsNeigh.FindFirst("code = " + rsLoad.Get_Fields_Int32("neighborhood") + " and townnumber = " + rsLoad.Get_Fields("townnumber")))
					{
						GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLDESC, FCConvert.ToString(rsNeigh.Get_Fields_String("description")));
					}
					GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLPARENT, FCConvert.ToString(lngRow));
					GridNeigh.RowHidden(lngNeighRow, true);
					// Do While rsLoad.FindNextRecord(, , "zone = " & rsZone.Fields("code") & " and townnumber = " & rsZone.Fields("townnumber"))
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					aryValues[0] = rsZone.Get_Fields("Code");
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					aryValues[1] = rsZone.Get_Fields("townnumber");
					while (rsLoad.FindNextRecord2(aryFields, aryValues))
					{
						GridNeigh.Rows += 1;
						lngNeighRow = GridNeigh.Rows - 1;
						GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
						GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLCODE, FCConvert.ToString(rsLoad.Get_Fields_Int32("neighborhood")));
						GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLZONE, FCConvert.ToString(rsLoad.Get_Fields_Int32("zone")));
						GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLSCHEDULE, FCConvert.ToString(rsLoad.Get_Fields_Int32("schedule")));
						GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLLANDFACTOR, FCConvert.ToString(rsLoad.Get_Fields_Double("landfactor")));
						GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLBLDGFACTOR, FCConvert.ToString(rsLoad.Get_Fields_Double("bldgfactor")));
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLTOWNCODE, FCConvert.ToString(rsLoad.Get_Fields("townnumber")));
						// If rsNeigh.FindFirstRecord(, , "code = " & rsLoad.Fields("neighborhood") & " and townnumber = " & rsLoad.Fields("townnumber")) Then
						// If rsNeigh.FindFirstRecord2("code,townnumber", rsLoad.Fields("neighborhood") & "," & rsLoad.Fields("townnumber"), ",") Then
						// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
						if (rsNeigh.FindFirst("code = " + rsLoad.Get_Fields_Int32("neighborhood") + " and townnumber = " + rsLoad.Get_Fields("townnumber")))
						{
							GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLDESC, FCConvert.ToString(rsNeigh.Get_Fields_String("description")));
						}
						GridNeigh.TextMatrix(lngNeighRow, CNSTGRIDNEIGHCOLPARENT, FCConvert.ToString(lngRow));
						GridNeigh.RowHidden(lngNeighRow, true);
					}
				}
				rsZone.MoveNext();
			}
			GridZone.Row = lngFirst;
			if (lngFirst > 0)
			{
				ShowNeighs(FCConvert.ToInt16(lngFirst));
			}
			else
			{
				ShowNeighs_2(0);
			}
		}

		private void ReShowGridZone()
		{
			int x;
			int intFirst;
			intFirst = 0;
			if (GridZone.Rows < 2)
			{
				return;
			}
			for (x = 1; x <= GridZone.Rows - 1; x++)
			{
				if (FCConvert.ToDouble(GridZone.TextMatrix(x, CNSTGRIDZONECOLTOWNCODE)) == Conversion.Val(gridTownCode.TextMatrix(0, 0)))
				{
					GridZone.RowHidden(x, false);
					if (intFirst == 0)
						intFirst = x;
				}
				else
				{
					GridZone.RowHidden(x, true);
				}
			}
			// x
			GridZone.Row = intFirst;
			GridZone.TopRow = GridZone.Row;
		}
		// vbPorter upgrade warning: intZoneRow As short	OnWriteFCConvert.ToInt32(
		private void ShowNeighs_2(short intZoneRow)
		{
			ShowNeighs(intZoneRow);
		}

		private void ShowNeighs(short intZoneRow)
		{
			int x;
			if (GridNeigh.Rows < 2)
				return;
			for (x = 1; x <= GridNeigh.Rows - 1; x++)
			{
				if (FCConvert.ToDouble(GridNeigh.TextMatrix(x, CNSTGRIDNEIGHCOLPARENT)) != intZoneRow)
				{
					GridNeigh.RowHidden(x, true);
				}
				else
				{
					GridNeigh.RowHidden(x, false);
				}
			}
			// x
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from tblTranCode where code > 0 order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
				gridTownCode.Visible = true;
			}
			else
			{
				gridTownCode.Rows = 1;
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = false;
			}
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}

		private void mnuPrintZoneS_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			rptZones.InstancePtr.Init(true);
		}

		private void mnuPrintZoneSchedule_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
            //FC:FINAL:AM:#3326 - sort on multiple columns
            //GridNeigh.Col = CNSTGRIDNEIGHCOLTOWNCODE;
            //GridNeigh.Sort = FCGrid.SortSettings.flexSortNumericAscending;
            //GridNeigh.Col = CNSTGRIDNEIGHCOLZONE;
            //GridNeigh.Sort = FCGrid.SortSettings.flexSortNumericAscending;
            GridNeigh.SortColumns(FCGrid.SortSettings.flexSortNumericAscending, CNSTGRIDNEIGHCOLTOWNCODE, CNSTGRIDNEIGHCOLZONE);
            rptZoneSchedule.InstancePtr.Init(true);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				this.Unload();
			}
		}
	}
}
