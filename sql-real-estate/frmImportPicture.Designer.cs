﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportPicture.
	/// </summary>
	partial class frmImportPicture : BaseForm
	{
		public fecherFoundation.FCCheckBox chkDelete;
		public fecherFoundation.FCComboBox cmbDirectory;
		public fecherFoundation.FCCheckBox chkCreate;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImportPicture));
            this.chkDelete = new fecherFoundation.FCCheckBox();
            this.cmbDirectory = new fecherFoundation.FCComboBox();
            this.chkCreate = new fecherFoundation.FCCheckBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdSave = new Wisej.Web.Button();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 183);
            this.BottomPanel.Size = new System.Drawing.Size(510, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkDelete);
            this.ClientArea.Controls.Add(this.cmbDirectory);
            this.ClientArea.Controls.Add(this.chkCreate);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(530, 338);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkCreate, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbDirectory, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkDelete, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(530, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(153, 28);
            this.HeaderText.Text = "Import Picture";
            // 
            // chkDelete
            // 
            this.chkDelete.Location = new System.Drawing.Point(30, 67);
            this.chkDelete.Name = "chkDelete";
            this.chkDelete.Size = new System.Drawing.Size(242, 23);
            this.chkDelete.TabIndex = 3;
            this.chkDelete.Text = "Delete source pictures when done";
            // 
            // cmbDirectory
            // 
            this.cmbDirectory.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDirectory.Location = new System.Drawing.Point(30, 143);
            this.cmbDirectory.Name = "cmbDirectory";
            this.cmbDirectory.Size = new System.Drawing.Size(242, 40);
            this.cmbDirectory.TabIndex = 1;
            // 
            // chkCreate
            // 
            this.chkCreate.Checked = true;
            this.chkCreate.CheckState = Wisej.Web.CheckState.Checked;
            this.chkCreate.Location = new System.Drawing.Point(30, 30);
            this.chkCreate.Name = "chkCreate";
            this.chkCreate.Size = new System.Drawing.Size(255, 23);
            this.chkCreate.TabIndex = 0;
            this.chkCreate.Text = "Automatically create a new filename";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 104);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(236, 19);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "DIRECTORY TO COPY PICTURES INTO";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(196, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(150, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmImportPicture
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(530, 398);
            this.FillColor = 0;
            this.Name = "frmImportPicture";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Import Picture";
            this.Load += new System.EventHandler(this.frmImportPicture_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmImportPicture_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreate)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdSave;
	}
}
