﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Wisej.Web;
using Global;
using fecherFoundation;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptZones.
	/// </summary>
	public partial class rptZones : BaseSectionReport
	{
		public static rptZones InstancePtr
		{
			get
			{
				return (rptZones)Sys.GetInstance(typeof(rptZones));
			}
		}

		protected rptZones _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport?.Dispose();
                rsReport = null;
            }
			base.Dispose(disposing);
		}

		public rptZones()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Zones";
		}
		// nObj = 1
		//   0	rptZones	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolFromForm;
		private clsDRWrapper rsReport = new clsDRWrapper();
		private int lngMax;
		private int lngRow;
		const int CNSTGRIDZONECOLAUTOID = 0;
		const int CNSTGRIDZONECOLCODE = 1;
		const int CNSTGRIDZONECOLDESCRIPTION = 2;
		const int CNSTGRIDZONECOLSHORTDESCRIPTION = 3;
		const int CNSTGRIDZONECOLSECDESCRIPTION = 4;
		const int CNSTGRIDZONECOLSECSHORT = 5;
		const int CNSTGRIDZONECOLTOWNCODE = 6;

		public void Init(bool boolFromScreen = false)
		{
			boolFromForm = boolFromScreen;
			string strSQL = "";
			if (!boolFromScreen)
			{
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					strSQL = "select * from zones where townnumber = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " order by code";
				}
				else
				{
					strSQL = "Select * from ZONES order by code";
				}
				rsReport.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (rsReport.EndOfFile())
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				lngMax = frmZones.InstancePtr.GridZone.Rows - 1;
				if (lngMax == 0)
				{
					MessageBox.Show("No zones", "No Zones", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				lngRow = 1;
			}
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "Zones");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolFromForm)
			{
				eArgs.EOF = rsReport.EndOfFile();
			}
			else
			{
				TRYAGAIN:
				;
				eArgs.EOF = lngRow > lngMax;
				if (!eArgs.EOF)
				{
					if (frmZones.InstancePtr.GridZone.RowHidden(lngRow))
					{
						lngRow += 1;
						goto TRYAGAIN;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				txtMuni.Text = modRegionalTown.GetTownKeyName_2(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
			}
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!boolFromForm)
			{
				if (!rsReport.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					txtZone.Text = rsReport.Get_Fields_String("code");
					txtDescription.Text = rsReport.Get_Fields_String("description");
					txtShort.Text = rsReport.Get_Fields_String("shortdescription");
					txtSecondary.Text = rsReport.Get_Fields_String("secondarydescription");
					txtShortSecondary.Text = rsReport.Get_Fields_String("secondaryshort");
					rsReport.MoveNext();
				}
			}
			else
			{
				if (lngRow <= lngMax)
				{
					txtZone.Text = frmZones.InstancePtr.GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLCODE);
					txtDescription.Text = frmZones.InstancePtr.GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLDESCRIPTION);
					txtShort.Text = frmZones.InstancePtr.GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLSHORTDESCRIPTION);
					txtSecondary.Text = frmZones.InstancePtr.GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLSECDESCRIPTION);
					txtShortSecondary.Text = frmZones.InstancePtr.GridZone.TextMatrix(lngRow, CNSTGRIDZONECOLSECSHORT);
					lngRow += 1;
				}
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
