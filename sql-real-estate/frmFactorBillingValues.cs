﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmFactorBillingValues.
	/// </summary>
	public partial class frmFactorBillingValues : BaseForm
	{
		public frmFactorBillingValues()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmFactorBillingValues InstancePtr
		{
			get
			{
				return (frmFactorBillingValues)Sys.GetInstance(typeof(frmFactorBillingValues));
			}
		}

		protected frmFactorBillingValues _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmFactorBillingValues_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmFactorBillingValues_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFactorBillingValues properties;
			//frmFactorBillingValues.ScaleWidth	= 5880;
			//frmFactorBillingValues.ScaleHeight	= 4140;
			//frmFactorBillingValues.LinkTopic	= "Form1";
			//frmFactorBillingValues.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			if (modREMain.Statics.boolShortRealEstate)
			{
				chkOnlyOverrides.Visible = false;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			bool boolLand = false;
			bool boolRange = false;
			int intRange;
			if (cmbFactor.Text == "Land Values")
			{
				boolLand = true;
			}
			else
			{
				boolLand = false;
			}
			if (cmbOverride.Text == "Range")
			{
				boolRange = true;
				if ((Strings.Trim(txtStart.Text) == string.Empty) || (Strings.Trim(txtEnd.Text) == string.Empty))
				{
					MessageBox.Show("You must specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (Conversion.Val(txtFactor.Text) < 0)
			{
				MessageBox.Show("You must enter a valid factor", "Invalid Factor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			intRange = 0;
			if (boolRange)
			{
				if (cmbRange.Text == "Account")
				{
					intRange = 0;
				}
				else if (cmbRange.Text == "Map/Lot")
				{
					intRange = 1;
				}
				else if (cmbRange.Text == "Location")
				{
					intRange = 2;
				}
				else if (cmbRange.Text == "Tran Code")
				{
					intRange = 3;
				}
				else if (cmbRange.Text == "Land Code")
				{
					intRange = 4;
				}
				else if (cmbRange.Text == "Building Code")
				{
					intRange = 5;
				}
				else if (cmbRange.Text == "Neighborhood")
				{
					intRange = 6;
				}
				else if (cmbRange.Text == "Zone")
				{
					intRange = 7;
				}
			}
			if (cmbFactor.Text != "Outbuilding Sound Values")
			{
				FactorValues_702(boolLand, boolRange, intRange, Conversion.Val(txtFactor.Text), txtStart.Text, txtEnd.Text);
			}
			else
			{
				FactorSoundValues_234(boolRange, intRange, Conversion.Val(txtFactor.Text), txtStart.Text, txtEnd.Text);
			}
		}
		// vbPorter upgrade warning: intRange As short	OnWriteFCConvert.ToInt32(
		private void FactorSoundValues_234(bool boolRange, int intRange, double dblFactor, string strStart, string strEnd)
		{
			FactorSoundValues(ref boolRange, ref intRange, ref dblFactor, ref strStart, ref strEnd);
		}

		private void FactorSoundValues(ref bool boolRange, ref int intRange, ref double dblFactor, ref string strStart, ref string strEnd)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			string strStartRange;
			string strEndRange;
			string strWhere;
			string strRangeField = "";
			try
			{
				// On Error GoTo ErrorHandler
				strStartRange = strStart;
				strEndRange = strEnd;
				strWhere = "";
				if (boolRange)
				{
					switch (intRange)
					{
						case 0:
							{
								// account
								strRangeField = "outbuilding.rsaccount";
								break;
							}
						case 1:
							{
								// maplot
								strRangeField = "rsmaplot";
								strStartRange = "'" + strStart + "'";
								strEndRange = "'" + strEnd + "'";
								break;
							}
						case 2:
							{
								// location
								strRangeField = "rslocstreet";
								strStartRange = "'" + strStart + "'";
								strEndRange = "'" + strEnd + "'";
								break;
							}
						case 3:
							{
								// trancode
								strRangeField = "ritrancode";
								break;
							}
						case 4:
							{
								// landcode
								strRangeField = "rilandcode";
								break;
							}
						case 5:
							{
								// bldgcode
								strRangeField = "ribldgcode";
								break;
							}
						case 6:
							{
								// neighborhood
								strRangeField = "pineighborhood";
								break;
							}
						case 7:
							{
								// zone
								strRangeField = "pizone";
								break;
							}
					}
					//end switch
					strWhere = " where " + strRangeField + " between " + strStartRange + " and " + strEndRange;
				}
				modGlobalFunctions.AddCYAEntry_26("RE", "Factored Outbuilding Sound Values", "Factored by " + FCConvert.ToString(dblFactor), strWhere);
				strSQL = "update (select outbuilding.* from outbuilding inner join master on (master.rsaccount = outbuilding.rsaccount) and (master.rscard = outbuilding.rsCARD)) set OISOUNDVALUE1 = oisoundvalue1 * " + FCConvert.ToString(dblFactor) + ",oisoundvalue2 = oisoundvalue2 * " + FCConvert.ToString(dblFactor) + ",oisoundvalue3 = oisoundvalue3 * " + FCConvert.ToString(dblFactor) + ",oisoundvalue4 = oisoundvalue4 * " + FCConvert.ToString(dblFactor) + ",oisoundvalue5 = oisoundvalue5 * " + FCConvert.ToString(dblFactor);
				strSQL += ",oisoundvalue6 = oisoundvalue6 * " + FCConvert.ToString(dblFactor) + ",oisoundvalue7 = oisoundvalue7 * " + FCConvert.ToString(dblFactor) + ",oisoundvalue8 = oisoundvalue8 * " + FCConvert.ToString(dblFactor) + ",oisoundvalue9 = oisoundvalue9 * " + FCConvert.ToString(dblFactor) + ",oisoundvalue10 = oisoundvalue10 * " + FCConvert.ToString(dblFactor) + " " + strWhere;
				rsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
				MessageBox.Show("Sound Values Factored", "Factored", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FactorSoundValues", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intRange As short	OnWriteFCConvert.ToInt32(
		private void FactorValues_702(bool boolLand, bool boolRange, int intRange, double dblFactor, string strStart, string strEnd)
		{
			FactorValues(ref boolLand, ref boolRange, ref intRange, ref dblFactor, ref strStart, ref strEnd);
		}

		private void FactorValues(ref bool boolLand, ref bool boolRange, ref int intRange, ref double dblFactor, ref string strStart, ref string strEnd)
		{
			string strBillingField = "";
			string strOverrideField = "";
			string strOverrideCode = "";
			string strRangeField = "";
			string strStartRange;
			string strEndRange;
			string strWhere;
			string strSQL = "";
			clsDRWrapper clsSave = new clsDRWrapper();
			string strCYA1 = "";
			string strCYA2 = "";
			string strCYA3;
			bool boolOnlyOverrides = false;
			try
			{
				// On Error GoTo ErrorHandler
				if (chkOnlyOverrides.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolOnlyOverrides = true;
				}
				else
				{
					boolOnlyOverrides = false;
				}
				if (boolLand)
				{
					if (!modREMain.Statics.boolShortRealEstate)
					{
						strBillingField = "lastlandval";
						strOverrideField = "hlvalland";
						strOverrideCode = "hivallandcode";
						strCYA1 = "Overrode Land Values";
						strCYA2 = "Factored by " + FCConvert.ToString(dblFactor);
					}
					else
					{
						strBillingField = "lastlandval";
						strOverrideField = "lastlandval";
						strCYA1 = "Factored Land Billing Values";
						strCYA2 = "Factored by " + FCConvert.ToString(dblFactor);
					}
				}
				else
				{
					if (!modREMain.Statics.boolShortRealEstate)
					{
						strBillingField = "lastbldgval";
						strOverrideField = "Hlvalbldg";
						strOverrideCode = "hivalbldgcode";
						strCYA1 = "Overrode Bldg Values";
						strCYA2 = "Factored by " + FCConvert.ToString(dblFactor);
					}
					else
					{
						strBillingField = "lastbldgval";
						strOverrideField = "lastbldgval";
						strCYA1 = "Factored Bldg Billing Values";
						strCYA2 = "Factored by " + FCConvert.ToString(dblFactor);
					}
				}
				strStartRange = strStart;
				strEndRange = strEnd;
				strWhere = "";
				if (boolRange)
				{
					switch (intRange)
					{
						case 0:
							{
								// account
								strRangeField = "rsaccount";
								break;
							}
						case 1:
							{
								// maplot
								strRangeField = "rsmaplot";
								strStartRange = "'" + strStart + "'";
								strEndRange = "'" + strEnd + "'";
								break;
							}
						case 2:
							{
								// location
								strRangeField = "rslocstreet";
								strStartRange = "'" + strStart + "'";
								strEndRange = "'" + strEnd + "'";
								break;
							}
						case 3:
							{
								// trancode
								strRangeField = "ritrancode";
								break;
							}
						case 4:
							{
								// landcode
								strRangeField = "rilandcode";
								break;
							}
						case 5:
							{
								// bldgcode
								strRangeField = "ribldgcode";
								break;
							}
						case 6:
							{
								// neighborhood
								strRangeField = "pineighborhood";
								break;
							}
						case 7:
							{
								// zone
								strRangeField = "pizone";
								break;
							}
					}
					//end switch
					strWhere = " where " + strRangeField + " between " + strStartRange + " and " + strEndRange;
					if (boolOnlyOverrides)
					{
						strWhere += " and " + strOverrideCode + " = 6 ";
					}
				}
				else
				{
					if (boolOnlyOverrides)
					{
						strWhere = " where " + strOverrideCode + " = 6 ";
					}
				}
				strCYA3 = strWhere;
				int intPlaces = 0;
				switch (modGlobalVariables.Statics.CustomizedInfo.Round)
				{
					case 1:
						{
							// 1000
							intPlaces = 1000;
							break;
						}
					case 2:
						{
							// 100
							intPlaces = 100;
							break;
						}
					case 3:
						{
							// 10
							intPlaces = 10;
							break;
						}
					case 4:
						{
							// 1
							intPlaces = 1;
							break;
						}
				}
				//end switch
				modGlobalFunctions.AddCYAEntry_240("RE", strCYA1, strCYA2, strCYA3);
				if (!modREMain.Statics.boolShortRealEstate)
				{
					strSQL = "update master set " + strOverrideField + " = ROUND((" + FCConvert.ToString(dblFactor) + " * " + strBillingField + " ) / " + FCConvert.ToString(intPlaces) + ",0) * " + FCConvert.ToString(intPlaces) + "," + strOverrideCode + " = 6 " + strWhere;
				}
				else
				{
					if (!boolLand)
					{
						strSQL = "update master set " + strOverrideField + " = ROUND((" + FCConvert.ToString(dblFactor) + " * " + strBillingField + " ) / " + FCConvert.ToString(intPlaces) + ",0) * " + FCConvert.ToString(intPlaces) + " " + strWhere;
					}
					else
					{
						// must update rsothervalue only.  Not mess with tree growth
						// strSQL = "update master set " & strOverrideField & " = ROUND((" & dblFactor & " * val(" & strBillingField & " & '')) / " & intPlaces & ",0) * " & intPlaces & " , rsothervalue = ROUND((" & dblFactor & " * val(" & strBillingField & " & '')) / " & intPlaces & ",0) * " & intPlaces & " " & strWhere
						strSQL = "update master set rsothervalue  = ROUND((" + FCConvert.ToString(dblFactor) + " * rsothervalue) / " + FCConvert.ToString(intPlaces) + ",0) * " + FCConvert.ToString(intPlaces) + " " + strWhere;
					}
				}
				clsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
				if (boolLand)
				{
					if (!modREMain.Statics.boolShortRealEstate)
					{
						MessageBox.Show("Land overrides have been created that are " + FCConvert.ToString(dblFactor) + " times the billing value.", "Overrides created", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						strSQL = "update master set lastlandval = rsothervalue  + rssoftvalue  + rshardvalue  + rsmixedvalue ";
						clsSave.Execute(strSQL, modGlobalVariables.strREDatabase);
						MessageBox.Show("Land values have been factored by " + FCConvert.ToString(dblFactor) + " times the original value.", "Land Factored", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					if (!modREMain.Statics.boolShortRealEstate)
					{
						MessageBox.Show("Building overrides have been created that are " + FCConvert.ToString(dblFactor) + " times the billing value.", "Overrides created", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("Building values have been factored by " + FCConvert.ToString(dblFactor) + " times the original value.", "Building Factored", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FactorValues", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void optFactor_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 2)
			{
				//Frame2.Text = "Records to Factor";
				chkOnlyOverrides.Visible = false;
			}
			else
			{
				//Frame2.Text = "Records to Override";
				chkOnlyOverrides.Visible = !modREMain.Statics.boolShortRealEstate;
			}
		}

		private void optFactor_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbFactor.SelectedIndex;
			optFactor_CheckedChanged(index, sender, e);
		}

		private void optOverride_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						framRange.Visible = false;
						break;
					}
				case 1:
					{
						framRange.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optOverride_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbOverride.SelectedIndex;
			optOverride_CheckedChanged(index, sender, e);
		}
	}
}
