﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cCommercialExteriorWall
	{
		//=========================================================
		private int lngRecordID;
		private string strFullDescription = string.Empty;
		private string strShortDescription = string.Empty;
		private int lngCodeNumber;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int CodeNumber
		{
			set
			{
				lngCodeNumber = value;
				IsUpdated = true;
			}
			get
			{
				int CodeNumber = 0;
				CodeNumber = lngCodeNumber;
				return CodeNumber;
			}
		}

		public string FullDescription
		{
			set
			{
				strFullDescription = value;
				IsUpdated = true;
			}
			get
			{
				string FullDescription = "";
				FullDescription = strFullDescription;
				return FullDescription;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
				IsUpdated = true;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}
	}
}
