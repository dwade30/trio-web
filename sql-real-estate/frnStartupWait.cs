﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmStartupWait.
	/// </summary>
	public partial class frmStartupWait : BaseForm
	{
		public frmStartupWait()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmStartupWait InstancePtr
		{
			get
			{
				return (frmStartupWait)Sys.GetInstance(typeof(frmStartupWait));
			}
		}

		protected frmStartupWait _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmStartupWait_Activated(object sender, System.EventArgs e)
		{
			this.BringToFront();
			this.Height = FCConvert.ToInt32(FCGlobal.Screen.Height / 4);
			this.Width = FCConvert.ToInt32(FCGlobal.Screen.Width / 4);
		}

		private void frmStartupWait_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmStartupWait properties;
			//frmStartupWait.ScaleWidth	= 4680;
			//frmStartupWait.ScaleHeight	= 3585;
			//frmStartupWait.LinkTopic	= "Form1";
			//End Unmaped Properties
		}
	}
}
