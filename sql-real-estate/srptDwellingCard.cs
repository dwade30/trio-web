﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptDwellingCard.
	/// </summary>
	public partial class srptDwellingCard : FCSectionReport
	{
		public static srptDwellingCard InstancePtr
		{
			get
			{
				return (srptDwellingCard)Sys.GetInstance(typeof(srptDwellingCard));
			}
		}

		protected srptDwellingCard _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsDwell?.Dispose();
                clsCost = null;
                clsDwell = null;
            }
			base.Dispose(disposing);
		}

		public srptDwellingCard()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptDwellingCard	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolEmpty;
		clsDRWrapper clsDwell = new clsDRWrapper();
		int lngAcct;
		int intCard;
        //FC:FINAL:MSH - rptpdfPropertyCard.InstancePtr will be equal null only after first launching
        //clsDRWrapper clsCost = rptpdfPropertyCard.InstancePtr == null ? rptPropertyCard.InstancePtr.clsCost : rptpdfPropertyCard.InstancePtr.clsCost;
        clsDRWrapper clsCost;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			boolEmpty = false;
			//FC:FINAL:RPU:#i1029 - Check the Parent type
			if (this.ParentReport as srptpdfPropertyCard2 != null)
			{
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields_Int32("rsaccount"))));
				intCard = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields_Int32("rscard"))));
			}
			else
			{
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields_Int32("rsaccount"))));
				intCard = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields_Int32("rscard"))));
			}
			Image1.Image = MDIParent.InstancePtr.ImageList1.Images[9 - 1];
			clsDwell.OpenRecordset("select * from dwelling where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
            //FC:FINAL:MSH - get object from the report(as in original)
            clsCost = this.ParentReport.ParentReport.GetType().GetProperty("clsCost").GetValue(this.ParentReport.ParentReport) as clsDRWrapper;
            if (clsDwell.EndOfFile())
			{
				boolEmpty = true;
			}
			FillFromCostFiles();
		}

		private void FillFromCostFiles()
		{
			string strTemp = "";
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                int x;
                int Y;
                // vbPorter upgrade warning: intTemp As short --> As int	OnWriteFCConvert.ToDouble(
                int intTemp = 0;
                // building style
                clsTemp.OpenRecordset("select * from dwellingstyle ORDER by code", modGlobalVariables.strREDatabase);
                x = 1;
                while (!clsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    strTemp = clsTemp.Get_Fields("code") + "." + clsTemp.Get_Fields_String("shortdescription");
                    if (strTemp.Length > 10)
                    {
                        strTemp = Strings.Mid(strTemp, 1, 10);
                    }

                    (Detail.Controls["txtStyleCost" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        strTemp;
                    x += 1;
                    if (x > 12)
                        break;
                    clsTemp.MoveNext();
                }

                for (Y = x; Y <= 12; Y++)
                {
                    (Detail.Controls["txtStyleCost" + Y] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        FCConvert.ToString(Y) + ".";
                }

                // Y
                clsTemp.OpenRecordset("select * from dwellingexterior order by code", modGlobalVariables.strREDatabase);
                x = 1;
                while (!clsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    strTemp = clsTemp.Get_Fields("code") + "." + clsTemp.Get_Fields_String("shortdescription");
                    if (strTemp.Length > 10)
                    {
                        strTemp = Strings.Mid(strTemp, 1, 10);
                    }

                    (Detail.Controls["txtExteriorCost" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                        .Text = strTemp;
                    x += 1;
                    if (x > 12)
                        break;
                    clsTemp.MoveNext();
                }

                for (Y = x; Y <= 12; Y++)
                {
                    (Detail.Controls["txtExteriorCost" + Y] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text
                        = FCConvert.ToString(Y) + ".";
                }

                // Y
                clsTemp.OpenRecordset("select * from dwellingheat order by code", modGlobalVariables.strREDatabase);
                x = 1;
                while (!clsTemp.EndOfFile())
                {
                    // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                    strTemp = clsTemp.Get_Fields("code") + "." + clsTemp.Get_Fields_String("shortdescription");
                    if (strTemp.Length > 10)
                    {
                        strTemp = Strings.Mid(strTemp, 1, 10);
                    }

                    (Detail.Controls["txtHeatCost" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        strTemp;
                    x += 1;
                    if (x > 12)
                        break;
                    clsTemp.MoveNext();
                }

                for (Y = x; Y <= 12; Y++)
                {
                    (Detail.Controls["txtHeatCost" + Y] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text =
                        FCConvert.ToString(Y) + ".";
                }

                // Y
                clsCost.MoveFirst();
                if (clsCost.FindNextRecord("crecordnumber", 1461))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1470)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1460);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtConditionCost" + intTemp] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1481))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1490)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        if (Strings.InStr(1, strTemp, "..", CompareConstants.vbTextCompare) > 0)
                        {
                            // dont want to get rid of decimal places
                            strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        }

                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1480);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtStoriesCost" + intTemp] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1511))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1520)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1510);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtRoofCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                strTemp = clsCost.Get_Fields_String("cldesc");
                strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                if (strTemp.Length > 17)
                {
                    strTemp = Strings.Mid(strTemp, 1, 17);
                }

                lblOpen3.Text = strTemp;
                clsCost.FindNextRecord("crecordnumber", 1530);
                strTemp = clsCost.Get_Fields_String("cldesc");
                strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                if (strTemp.Length > 17)
                {
                    strTemp = Strings.Mid(strTemp, 1, 17);
                }

                lblOpen4.Text = strTemp;
                if (clsCost.FindNextRecord("crecordnumber", 1541))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1550)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1540);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtFoundationCost" + intTemp] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1551))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1560)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1550);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtBasementCost" + intTemp] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1561))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1570)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1560);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtWetCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                // open5
                strTemp = clsCost.Get_Fields_String("cldesc");
                strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                if (strTemp.Length > 17)
                {
                    strTemp = Strings.Mid(strTemp, 1, 17);
                }

                lblOpen5.Text = strTemp;
                if (clsCost.FindNextRecord("crecordnumber", 1601))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1610)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1600);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtCoolCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1611))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1620)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1610);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtKitchenCost" + intTemp] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1621))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1630)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1620);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtBathCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1631))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1640)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1630);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtLayoutCost" + intTemp] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1641))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1650)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1640);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtAtticCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox
                            ).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1651))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1660)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1650);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtInsulationCost" + intTemp] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1671))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1680)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1670);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtGradeCost" + intTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox
                            ).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1681))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1690)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1680);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtFunctionalCost" + intTemp] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    rsLoad.OpenRecordset(
                        "select * from economiccode where townnumber = " +
                        FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) +
                        " order by code",
                        modGlobalVariables.Statics.strRECostFileDatabase);
                    x = 1;
                    while (!rsLoad.EndOfFile() && x < 10)
                    {
                        strTemp = FCConvert.ToString(rsLoad.Get_Fields_String("shortdescription"));
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        // TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
                        strTemp = rsLoad.Get_Fields("code") + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtEconomicCost" + x] as GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            .Text = strTemp;
                        x += 1;
                        rsLoad.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1701))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1710)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1700);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtEntranceCost" + intTemp] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }

                if (clsCost.FindNextRecord("crecordnumber", 1711))
                {
                    while (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) < 1720)
                    {
                        strTemp = clsCost.Get_Fields_String("csdesc");
                        strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
                        intTemp = FCConvert.ToInt32(Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) - 1710);
                        strTemp = FCConvert.ToString(intTemp) + "." + Strings.Trim(strTemp);
                        if (strTemp.Length > 10)
                        {
                            strTemp = Strings.Mid(strTemp, 1, 10);
                        }

                        (Detail.Controls["txtInformationCost" + intTemp] as
                            GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
                        clsCost.MoveNext();
                    }
                }
            }
        }
		// vbPorter upgrade warning: intCode As short	OnWrite(string, double)
		private string GetDesc_26(int lngCostNumber, short intCode, bool boolLong, short intLimit = 50)
		{
			return GetDesc(ref lngCostNumber, ref intCode, ref boolLong, intLimit);
		}

		private string GetDesc(ref int lngCostNumber, ref short intCode, ref bool boolLong, short intLimit = 50)
		{
			string GetDesc = "";
			string strTemp = "";
			if (intCode == 0)
			{
				GetDesc = "0";
				return GetDesc;
			}
			if (Conversion.Val(clsCost.Get_Fields_Int32("crecordnumber")) != lngCostNumber + intCode)
			{
				if (clsCost.FindNextRecord("crecordnumber", lngCostNumber + intCode))
				{
					if (boolLong)
					{
						strTemp = Strings.Trim(clsCost.Get_Fields_String("cldesc"));
					}
					else
					{
						strTemp = Strings.Trim(clsCost.Get_Fields_String("csdesc"));
					}
					strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
				}
				else
				{
					strTemp = "";
				}
			}
			else
			{
				if (boolLong)
				{
					strTemp = Strings.Trim(clsCost.Get_Fields_String("cldesc"));
				}
				else
				{
					strTemp = Strings.Trim(clsCost.Get_Fields_String("csdesc"));
				}
				strTemp = Strings.Replace(strTemp, ".", " ", 1, -1, CompareConstants.vbTextCompare);
			}
			strTemp = Strings.Trim(strTemp);
			strTemp = FCConvert.ToString(intCode) + " " + strTemp;
			if (strTemp.Length > intLimit)
			{
				strTemp = Strings.Mid(strTemp, 1, intLimit);
			}
			GetDesc = strTemp;
			return GetDesc;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsCost.MoveFirst();
			int intTemp;
			int lngTemp;
			string strTemp = "";
			using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                if (!boolEmpty)
                {
                    // must do this in order so getdesc will work since in uses findnext not find first
                    // it is faster searching this way
                    clsCost.MoveFirst();
                    strTemp = "";
                    clsTemp.OpenRecordset(
                        "select * from dwellingstyle where code = " +
                        FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("distyle"))),
                        modGlobalVariables.strREDatabase);
                    if (!clsTemp.EndOfFile())
                    {
                        strTemp = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("Description")));
                    }

                    txtStyle.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("distyle"))) + " " +
                                    strTemp;
                    strTemp = "";
                    clsTemp.OpenRecordset(
                        "select * from dwellingexterior where code = " +
                        FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diextwalls"))),
                        modGlobalVariables.strREDatabase);
                    if (!clsTemp.EndOfFile())
                    {
                        strTemp = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
                    }

                    txtExterior.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diextwalls"))) +
                                       " " + strTemp;
                    strTemp = "";
                    clsTemp.OpenRecordset(
                        "select * from dwellingheat where code = " +
                        FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diheat"))),
                        modGlobalVariables.strREDatabase);
                    if (!clsTemp.EndOfFile())
                    {
                        strTemp = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("description")));
                    }

                    txtHeat.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diheat"))) + " " +
                                   strTemp;
                    txtHeatPct.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dipctheat"))) + "%";
                    txtUnits.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diunitsdwelling")));
                    txtOtherUnits.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diunitsother")));
                    // condition
                    strTemp = GetDesc_26(1460,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dicondITION")))),
                        true);
                    txtCondition.Text = strTemp;
                    strTemp = GetDesc_26(1480,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("distories")))),
                        true);
                    txtStories.Text = strTemp;
                    strTemp = GetDesc_26(1510,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diroof")))),
                        true);
                    txtRoof.Text = strTemp;
                    txtMasonry.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("disfmasonry")));
                    txtOpen3.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diopen3")));
                    txtOpen4.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diopen4")));
                    txtYear.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diyearbuilt")));
                    txtRemodel.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diyearremodel")));
                    strTemp = GetDesc_26(1540,
                        FCConvert.ToInt16(
                            FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("difoundation")))), true);
                    txtFoundation.Text = strTemp;
                    strTemp = GetDesc_26(1550,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dibsmt")))),
                        true);
                    txtBasement.Text = strTemp;
                    txtBsmtGar.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dibsmtgar")));
                    strTemp = GetDesc_26(1560,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diwetbsmt")))),
                        true);
                    txtWet.Text = strTemp;
                    txtSFBsmtLiv.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("disfbsmtliving")));
                    txtFinBsmtGrade.Text =
                        FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dibsmtfingrade1"))) + "  " +
                        FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dibsmtfingrade2")));
                    txtOpen5.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diopen5")));
                    strTemp = GetDesc_26(1600,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dicool")))),
                        true);
                    txtCool.Text = strTemp;
                    txtCoolPct.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dipctcool"))) + "%";
                    strTemp = GetDesc_26(1610,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dikitchens")))),
                        true);
                    txtKitchen.Text = strTemp;
                    strTemp = GetDesc_26(1620,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dibaths")))),
                        true);
                    txtBath.Text = strTemp;
                    txtRooms.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dirooms")));
                    txtBedrooms.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dibedrooms")));
                    txtBaths.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("difullbaths")));
                    txtHalfBaths.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dihalfbaths")));
                    txtFixtures.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diaddnfixtures")));
                    txtFirePlaces.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("difireplaces")));
                    strTemp = GetDesc_26(1630,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dilayout")))),
                        true);
                    txtLayout.Text = strTemp;
                    strTemp = GetDesc_26(1640,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diattic")))),
                        true);
                    txtAttic.Text = strTemp;
                    strTemp = GetDesc_26(1650,
                        FCConvert.ToInt16(
                            FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("diinsulation")))), true);
                    txtInsulation.Text = strTemp;
                    txtUnfinished.Text =
                        FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dipctunfinished"))) + "%";
                    strTemp = GetDesc_26(1670, FCConvert.ToInt16(Conversion.Val(clsDwell.Get_Fields_Int32("digrade1"))),
                                  true) + " " +
                              FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("digrade2"))) + "%";
                    txtGradeFactor.Text = strTemp;
                    txtSqft.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("disqft")));
                    txtPhysPct.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dipctphys"))) + "%";
                    txtFuncPct.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dipctfunct"))) + "%";
                    strTemp = GetDesc_26(1680,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("difunctcode")))),
                        true);
                    txtFuncCode.Text = strTemp;
                    txtEconPct.Text = FCConvert.ToString(Conversion.Val(clsDwell.Get_Fields_Int32("dipctecon"))) + "%";
                    if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(
                        FCConvert.ToInt16(Conversion.Val(clsDwell.Get_Fields_Int32("dieconcode"))), "DWELLING",
                        "ECONOMIC"))
                    {
                        strTemp = Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc);
                    }
                    else
                    {
                        strTemp = "";
                    }

                    txtEconCode.Text = strTemp;
                    //FC:FINAL:RPU: #i1029 - Check which is the ParentReport and use it
                    if (this.ParentReport as srptpdfPropertyCard2 != null)
                    {
                        // TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
                        strTemp = GetDesc_26(1700,
                            FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(
                                (this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields("entrancecode")))),
                            true);
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
                        strTemp = GetDesc_26(1700,
                            FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(
                                (this.ParentReport as srptPropertyCard2).clsProp.Get_Fields("entrancecode")))), true);
                    }

                    // txtEntrance.Text = strTemp
                    // 
                    // strTemp = GetDesc(1710, Val(Me.ParentReport.clsProp.Fields("Informationcode")), True)
                    // txtInformation.Text = strTemp
                    // 
                    // strTemp = ""
                    // If IsDate(Me.ParentReport.clsProp.Fields("Dateinspected")) Then
                    // If Me.ParentReport.clsProp.Fields("dateinspected") <> 0 Then
                    // strTemp = Format(Me.ParentReport.clsProp.Fields("dateinspected"), "m/dd/yyyy")
                    // End If
                    // End If
                    // txtDateInspected.Text = strTemp
                }
                else
                {
                    // make all blanks
                    txtCondition.Text = "";
                    txtStyle.Text = "";
                    txtUnits.Text = "";
                    txtOtherUnits.Text = "";
                    txtStories.Text = "";
                    txtExterior.Text = "";
                    txtRoof.Text = "";
                    txtMasonry.Text = "";
                    txtOpen3.Text = "";
                    txtOpen4.Text = "";
                    txtYear.Text = "";
                    txtRemodel.Text = "";
                    txtFoundation.Text = "";
                    txtBasement.Text = "";
                    txtBsmtGar.Text = "";
                    txtWet.Text = "";
                    txtSFBsmtLiv.Text = "";
                    txtFinBsmtGrade.Text = "";
                    txtOpen5.Text = "";
                    txtHeat.Text = "";
                    txtCool.Text = "";
                    txtKitchen.Text = "";
                    txtBath.Text = "";
                    txtRooms.Text = "";
                    txtBedrooms.Text = "";
                    txtBaths.Text = "";
                    txtHalfBaths.Text = "";
                    txtFixtures.Text = "";
                    txtFirePlaces.Text = "";
                    txtLayout.Text = "";
                    txtAttic.Text = "";
                    txtInsulation.Text = "";
                    txtUnfinished.Text = "";
                    txtGradeFactor.Text = "";
                    txtSqft.Text = "";
                    txtPhysPct.Text = "";
                    txtFuncPct.Text = "";
                    txtFuncCode.Text = "";
                    txtEconCode.Text = "";
                    txtEconPct.Text = "";
                    txtEntrance.Text = "";
                    txtInformation.Text = "";
                }

                //FC:FINAL:RPU:#i1029 - Chech which is the parent
                if (this.ParentReport as srptpdfPropertyCard2 != null)
                {
                    // TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
                    strTemp = GetDesc_26(1700,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(
                            (this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields("entrancecode")))), true);
                    txtEntrance.Text = strTemp;
                    // TODO Get_Fields: Check the table for the column [Informationcode] and replace with corresponding Get_Field method
                    strTemp = GetDesc_26(1710,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(
                            (this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields("Informationcode")))), true);
                    txtInformation.Text = strTemp;
                    strTemp = "";
                    if (Information.IsDate(
                        (this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields("Dateinspected")))
                    {
                        if ((this.ParentReport as srptpdfPropertyCard2).clsProp.Get_Fields_DateTime("dateinspected") !=
                            DateTime.FromOADate(0))
                        {
                            strTemp = Strings.Format(
                                (this.ParentReport as srptpdfPropertyCard2).clsProp
                                .Get_Fields_DateTime("dateinspected"), "M/dd/yyyy");
                        }
                    }
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
                    strTemp = GetDesc_26(1700,
                        FCConvert.ToInt16(FCConvert.ToString(
                            Conversion.Val(
                                (this.ParentReport as srptPropertyCard2).clsProp.Get_Fields("entrancecode")))), true);
                    txtEntrance.Text = strTemp;
                    // TODO Get_Fields: Check the table for the column [Informationcode] and replace with corresponding Get_Field method
                    strTemp = GetDesc_26(1710,
                        FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(
                            (this.ParentReport as srptPropertyCard2).clsProp.Get_Fields("Informationcode")))), true);
                    txtInformation.Text = strTemp;
                    strTemp = "";
                    if (Information.IsDate((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields("Dateinspected"))
                    )
                    {
                        if ((this.ParentReport as srptPropertyCard2).clsProp.Get_Fields_DateTime("dateinspected") !=
                            DateTime.FromOADate(0))
                        {
                            strTemp = Strings.Format(
                                (this.ParentReport as srptPropertyCard2).clsProp.Get_Fields_DateTime("dateinspected"),
                                "M/dd/yyyy");
                        }
                    }
                }

                txtDateInspected.Text = strTemp;
            }
        }

		
	}
}
