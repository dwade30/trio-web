﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptDwellAge.
	/// </summary>
	public partial class srptDwellAge : FCSectionReport
	{
		public static srptDwellAge InstancePtr
		{
			get
			{
				return (srptDwellAge)Sys.GetInstance(typeof(srptDwellAge));
			}
		}

		protected srptDwellAge _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsAge?.Dispose();
                clsAge = null;
            }
			base.Dispose(disposing);
		}

		public srptDwellAge()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptDwellAge	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intGroup;
		string strSQL = "";
		clsDRWrapper clsAge = new clsDRWrapper();
		int lngCount;
		int lngTotal;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			intGroup = 0;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = false;
			if (intGroup > 7)
			{
				eArgs.EOF = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// strSQL = "select count(*) as thecount, sum(master.rlbldgval) as thetotal from (master inner join dwelling on (master.rsaccount = dwelling.rsaccount) and (master.rscard = dwelling.rscard)) where master.rsdwellingcode = 'Y' and  (" & Year(Date) & " - dwelling.diyearbuilt) "
			if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
			{
				strSQL = "select count(*) as thecount,sum(cast(sdwellrcnld as bigint)) as thetotal from (summrecord inner join (master inner join dwelling on (master.rsaccount = dwelling.rsaccount) and (master.rscard = dwelling.rscard)) on (summrecord.srecordnumber = master.rsaccount) and (summrecord.cardnumber = master.rscard) )where not master.rsdeleted = 1 and master.rsdwellingcode = 'Y' and  (" + FCConvert.ToString(DateTime.Today.Year) + " - dwelling.diyearbuilt )";
			}
			else
			{
				strSQL = "select count(*) as thecount,sum(cast(sdwellrcnld as bigint)) as thetotal from (summrecord inner join (master inner join dwelling on (master.rsaccount = dwelling.rsaccount) and (master.rscard = dwelling.rscard)) on (summrecord.srecordnumber = master.rsaccount) and (summrecord.cardnumber = master.rscard) )where not master.rsdeleted = 1 and  ritrancode  = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " and master.rsdwellingcode = 'Y' and  (" + FCConvert.ToString(DateTime.Today.Year) + " - dwelling.diyearbuilt )";
			}
			switch (intGroup)
			{
				case 0:
					{
						txtAge.Text = "0  -  1 YEARS";
						strSQL += "between 0 and 1";
						break;
					}
				case 1:
					{
						txtAge.Text = "2  -  5 YEARS";
						strSQL += "between 2 and 5";
						break;
					}
				case 2:
					{
						txtAge.Text = "6  - 10 YEARS";
						strSQL += "between 6 and 10";
						break;
					}
				case 3:
					{
						txtAge.Text = "11 - 20 YEARS";
						strSQL += "between 11 and 20";
						break;
					}
				case 4:
					{
						txtAge.Text = "21 - 30 YEARS";
						strSQL += "between 21 and 30";
						break;
					}
				case 5:
					{
						txtAge.Text = "31 - 40 YEARS";
						strSQL += "between 31 and 40";
						break;
					}
				case 6:
					{
						txtAge.Text = "41 - 50 YEARS";
						strSQL += "between 41 and 50";
						break;
					}
				case 7:
					{
						txtAge.Text = "OVER 50 YEARS";
						strSQL += " > 50";
						break;
					}
			}
			//end switch
			clsAge.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (clsAge.EndOfFile())
			{
				txtCount.Text = "0";
				txtAssess.Text = "0";
				txtAve.Text = "0";
			}
			else
			{
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				lngCount = FCConvert.ToInt32(clsAge.Get_Fields("thecount"));
				// TODO Get_Fields: Field [thetotal] not found!! (maybe it is an alias?)
				lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsAge.Get_Fields("thetotal"))));
				txtCount.Text = lngCount.ToString();
				txtAssess.Text = Strings.Format(lngTotal, "###,###,###,##0");
				if (lngCount > 0)
				{
					txtAve.Text = Strings.Format(FCConvert.ToDouble(lngTotal) / lngCount, "#,###,###,##0");
				}
				else
				{
					txtAve.Text = "0";
				}
			}
			intGroup += 1;
		}

		
	}
}
