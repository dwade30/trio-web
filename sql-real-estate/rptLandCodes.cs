﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptLandCodes.
	/// </summary>
	public partial class rptLandCodes : BaseSectionReport
	{
		public static rptLandCodes InstancePtr
		{
			get
			{
				return (rptLandCodes)Sys.GetInstance(typeof(rptLandCodes));
			}
		}

		protected rptLandCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsFigures?.Dispose();
				clsLandCodes?.Dispose();
                clsFigures = null;
                clsLandCodes = null;
            }
			base.Dispose(disposing);
		}

		public rptLandCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptLandCodes	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLandCodes = new clsDRWrapper();
		clsDRWrapper clsFigures = new clsDRWrapper();
		string strland = "";
		string strbldg = "";
		int intLandCode;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!clsFigures.EndOfFile())
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			// Call clsLandCodes.OpenRecordset("select * from costrecord where crecordnumber between 1000 and 1009", strredatabase)
			clsLandCodes.OpenRecordset("select * from tblLandCode order by code", modGlobalVariables.strREDatabase);
			if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
			{
				strSQL = "select rilandcode,count(rsaccount) as NumProps,sum(piacres) as sumAcres,sum(rllandval) as TotAssess from master where not rsdeleted = 1 group by rilandcode";
			}
			else
			{
				strSQL = "select rilandcode,count(rsaccount) as NumProps,sum(piacres) as sumAcres,sum(rllandval) as TotAssess from master where not rsdeleted = 1 and val(ritrancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " group by rilandcode";
			}
			clsFigures.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (clsFigures.EndOfFile())
			{
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			txtCount.Text = FCConvert.ToString(clsFigures.GetData("numprops"));
			txtAcres.Text = Strings.Format(clsFigures.GetData("sumacres"), "#,###,###,##0.00");
			txtAsessment.Text = Strings.Format(clsFigures.GetData("totassess"), "##,###,###,##0");
			intLandCode = FCConvert.ToInt32(clsFigures.GetData("rilandcode"));
			if (intLandCode == 0)
			{
				txtLandCode.Text = "0 Uncoded";
			}
			else
			{
				txtLandCode.Text = FCConvert.ToString(intLandCode) + " ";
				// Call clsLandCodes.FindFirstRecord("crecordnumber", 1000 + intLandCode)
				// Call clsLandCodes.FindFirstRecord("Code", intLandCode)
				clsLandCodes.FindFirst("code = " + FCConvert.ToString(intLandCode));
				if (!clsLandCodes.NoMatch)
				{
					// txtLandCode.Text = txtLandCode.Text & clsLandCodes.GetData("cldesc")
					txtLandCode.Text = txtLandCode.Text + clsLandCodes.Get_Fields_String("Description");
				}
				else
				{
					txtLandCode.Text = "Unknown Land Code";
				}
			}
			clsFigures.MoveNext();
		}

		
	}
}
