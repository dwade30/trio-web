﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class clsLandSummary
	{
		//=========================================================
		private string strDescription = "";
		private double dblValue;
		private double dblFactor;
		private string strUnits = "";
		private string strPricePerUnit = "";
		private string strTotal = "";
		private string strMethod = "";
		private string strAcreage = "";
		private int lngLandType;
		private string strValue = "";
		private string strFactor = "";

		public string Factor
		{
			get
			{
				string Factor = "";
				Factor = strFactor;
				return Factor;
			}
			set
			{
				strFactor = value;
			}
		}

		public string LandValue
		{
			get
			{
				string LandValue = "";
				LandValue = strValue;
				return LandValue;
			}
			set
			{
				strValue = value;
			}
		}

		public int LandType
		{
			get
			{
				int LandType = 0;
				LandType = lngLandType;
				return LandType;
			}
			set
			{
				lngLandType = value;
			}
		}

		public string Total
		{
			get
			{
				string Total = "";
				Total = strTotal;
				return Total;
			}
			set
			{
				strTotal = value;
			}
		}

		public string Method
		{
			get
			{
				string Method = "";
				Method = strMethod;
				return Method;
			}
			set
			{
				strMethod = value;
			}
		}

		public string Units
		{
			get
			{
				string Units = "";
				Units = strUnits;
				return Units;
			}
			set
			{
				strUnits = value;
			}
		}

		public string Acreage
		{
			get
			{
				string Acreage = "";
				Acreage = strAcreage;
				return Acreage;
			}
			set
			{
				strAcreage = value;
			}
		}

		public string Description
		{
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
			set
			{
				strDescription = value;
			}
		}
	}
}
