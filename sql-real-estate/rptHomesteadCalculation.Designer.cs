﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptHomesteadCalculation.
	/// </summary>
	partial class rptHomesteadCalculation
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptHomesteadCalculation));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCertifiedRatio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine1Ratio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.TXTLine1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine2Ratio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine3Amount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine3NumHomesteads = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine4HomesteadVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine4NumHomesteads = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine4Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine5Ratio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine6Ratio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine7GreaterThan = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine7NumHomesteads = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine7LessThan = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine8HomesteadVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine8NumHomesteads = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine8Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine9Ratio = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine10LessThan = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine10NumHomesteads = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine11ExemptVal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtLine11NumHomesteads = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine11Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbl12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbl13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLine13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCertifiedRatio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1Ratio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TXTLine1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2Ratio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3Amount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3NumHomesteads)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4HomesteadVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4NumHomesteads)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5Ratio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6Ratio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7GreaterThan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7NumHomesteads)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7LessThan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8HomesteadVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8NumHomesteads)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9Ratio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine10LessThan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine10NumHomesteads)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11ExemptVal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11NumHomesteads)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11Total)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.txtCertifiedRatio,
				this.Shape1,
				this.Label1,
				this.Label2,
				this.Line1,
				this.Field1,
				this.Label3,
				this.Label4,
				this.txtLine1Ratio,
				this.Shape2,
				this.Label5,
				this.Line2,
				this.TXTLine1,
				this.Label6,
				this.Field3,
				this.Label7,
				this.txtLine2Ratio,
				this.Shape3,
				this.Label8,
				this.Line3,
				this.txtLine2,
				this.Label9,
				this.Label10,
				this.Line4,
				this.txtLine3Amount,
				this.Label11,
				this.Line5,
				this.txtLine3NumHomesteads,
				this.Label12,
				this.Label13,
				this.Line6,
				this.txtLine4HomesteadVal,
				this.Line7,
				this.txtLine4NumHomesteads,
				this.Label14,
				this.Label15,
				this.txtLine4Total,
				this.Line8,
				this.Label16,
				this.Label17,
				this.Field11,
				this.Label18,
				this.txtLine5Ratio,
				this.Shape4,
				this.Label19,
				this.Line9,
				this.txtLine5,
				this.Field14,
				this.Label20,
				this.txtLine6Ratio,
				this.Shape5,
				this.Label21,
				this.Line10,
				this.txtLine6,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Line11,
				this.txtLine7GreaterThan,
				this.Label25,
				this.Line12,
				this.txtLine7NumHomesteads,
				this.Label26,
				this.Line13,
				this.txtLine7LessThan,
				this.Label27,
				this.Label28,
				this.Line14,
				this.txtLine8HomesteadVal,
				this.Line15,
				this.txtLine8NumHomesteads,
				this.Label29,
				this.Label30,
				this.txtLine8Total,
				this.Line16,
				this.Label31,
				this.Label32,
				this.Field23,
				this.Label33,
				this.txtLine9Ratio,
				this.Shape6,
				this.Label34,
				this.Line17,
				this.txtLine9,
				this.Label35,
				this.Label36,
				this.Line18,
				this.txtLine10LessThan,
				this.Label37,
				this.Line19,
				this.txtLine10NumHomesteads,
				this.Label38,
				this.Label39,
				this.Line20,
				this.txtLine11ExemptVal,
				this.Line21,
				this.txtLine11NumHomesteads,
				this.Label40,
				this.Label41,
				this.txtLine11Total,
				this.Line22,
				this.Label42,
				this.lbl12,
				this.Label44,
				this.lbl13,
				this.Label46,
				this.txtLine12,
				this.Label47,
				this.txtLine13,
				this.Shape7,
				this.Shape8,
				this.Shape9,
				this.Line23,
				this.Line24
			});
			this.Detail.Height = 8.583333F;
			this.Detail.Name = "Detail";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.21875F;
			this.txtTitle.Left = 1F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "HOMESTEAD EXEMPTION CALCULATION FORM";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 5.4375F;
			// 
			// txtCertifiedRatio
			// 
			this.txtCertifiedRatio.CanGrow = false;
			this.txtCertifiedRatio.Height = 0.21875F;
			this.txtCertifiedRatio.Left = 2F;
			this.txtCertifiedRatio.Name = "txtCertifiedRatio";
			this.txtCertifiedRatio.Style = "text-align: right";
			this.txtCertifiedRatio.Text = "%";
			this.txtCertifiedRatio.Top = 0.65625F;
			this.txtCertifiedRatio.Width = 0.9375F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 0.21875F;
			this.Shape1.Left = 2F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0.65625F;
			this.Shape1.Width = 0.9375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "";
			this.Label1.Text = "1.";
			this.Label1.Top = 1.375F;
			this.Label1.Width = 0.1875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.21875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.5625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "CERTIFIED RATIO:";
			this.Label2.Top = 0.65625F;
			this.Label2.Width = 1.375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 7.4375F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.4375F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.25F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "text-align: right";
			this.Field1.Text = "$125,000";
			this.Field1.Top = 1.375F;
			this.Field1.Width = 1.0625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: left";
			this.Label3.Text = "HOMESTEADS WITH A JUST VALUE OF LESS THAN $125,000";
			this.Label3.Top = 1.0625F;
			this.Label3.Width = 4.6875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.3125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: center";
			this.Label4.Text = "x";
			this.Label4.Top = 1.375F;
			this.Label4.Width = 0.1875F;
			// 
			// txtLine1Ratio
			// 
			this.txtLine1Ratio.CanGrow = false;
			this.txtLine1Ratio.Height = 0.1875F;
			this.txtLine1Ratio.Left = 1.5F;
			this.txtLine1Ratio.Name = "txtLine1Ratio";
			this.txtLine1Ratio.Style = "text-align: right";
			this.txtLine1Ratio.Text = "%";
			this.txtLine1Ratio.Top = 1.375F;
			this.txtLine1Ratio.Width = 0.6875F;
			// 
			// Shape2
			// 
			this.Shape2.Height = 0.1875F;
			this.Shape2.Left = 1.5F;
			this.Shape2.Name = "Shape2";
			this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape2.Top = 1.375F;
			this.Shape2.Width = 0.6875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.25F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold";
			this.Label5.Text = "=";
			this.Label5.Top = 1.375F;
			this.Label5.Width = 0.1875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.5F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.53125F;
			this.Line2.Width = 1.4375F;
			this.Line2.X1 = 2.5F;
			this.Line2.X2 = 3.9375F;
			this.Line2.Y1 = 1.53125F;
			this.Line2.Y2 = 1.53125F;
			// 
			// TXTLine1
			// 
			this.TXTLine1.Height = 0.1875F;
			this.TXTLine1.Left = 2.5F;
			this.TXTLine1.Name = "TXTLine1";
			this.TXTLine1.Style = "text-align: right";
			this.TXTLine1.Text = null;
			this.TXTLine1.Top = 1.375F;
			this.TXTLine1.Width = 1.4375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.0625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "";
			this.Label6.Text = "2.";
			this.Label6.Top = 1.78125F;
			this.Label6.Width = 0.1875F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.25F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "text-align: right";
			this.Field3.Text = "$7,000";
			this.Field3.Top = 1.78125F;
			this.Field3.Width = 1.0625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 1.3125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: center";
			this.Label7.Text = "x";
			this.Label7.Top = 1.78125F;
			this.Label7.Width = 0.1875F;
			// 
			// txtLine2Ratio
			// 
			this.txtLine2Ratio.CanGrow = false;
			this.txtLine2Ratio.Height = 0.1875F;
			this.txtLine2Ratio.Left = 1.5F;
			this.txtLine2Ratio.Name = "txtLine2Ratio";
			this.txtLine2Ratio.Style = "text-align: right";
			this.txtLine2Ratio.Text = "%";
			this.txtLine2Ratio.Top = 1.78125F;
			this.txtLine2Ratio.Width = 0.6875F;
			// 
			// Shape3
			// 
			this.Shape3.Height = 0.1875F;
			this.Shape3.Left = 1.5F;
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 1.78125F;
			this.Shape3.Width = 0.6875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.25F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "=";
			this.Label8.Top = 1.78125F;
			this.Label8.Width = 0.1875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 2.5F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.9375F;
			this.Line3.Width = 1.4375F;
			this.Line3.X1 = 2.5F;
			this.Line3.X2 = 3.9375F;
			this.Line3.Y1 = 1.9375F;
			this.Line3.Y2 = 1.9375F;
			// 
			// txtLine2
			// 
			this.txtLine2.Height = 0.1875F;
			this.txtLine2.Left = 2.5F;
			this.txtLine2.Name = "txtLine2";
			this.txtLine2.Style = "text-align: right";
			this.txtLine2.Text = null;
			this.txtLine2.Top = 1.78125F;
			this.txtLine2.Width = 1.4375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "";
			this.Label9.Text = "3.";
			this.Label9.Top = 2.1875F;
			this.Label9.Width = 0.1875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.15625F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.3125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-size: 8.5pt";
			this.Label10.Text = "Number of homesteads with an assessed value of less than";
			this.Label10.Top = 2.21875F;
			this.Label10.Width = 3.375F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 3.8125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 2.375F;
			this.Line4.Width = 1.25F;
			this.Line4.X1 = 3.8125F;
			this.Line4.X2 = 5.0625F;
			this.Line4.Y1 = 2.375F;
			this.Line4.Y2 = 2.375F;
			// 
			// txtLine3Amount
			// 
			this.txtLine3Amount.Height = 0.1875F;
			this.txtLine3Amount.Left = 3.8125F;
			this.txtLine3Amount.Name = "txtLine3Amount";
			this.txtLine3Amount.Style = "text-align: right";
			this.txtLine3Amount.Text = null;
			this.txtLine3Amount.Top = 2.21875F;
			this.txtLine3Amount.Width = 1.25F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold";
			this.Label11.Text = "=";
			this.Label11.Top = 2.21875F;
			this.Label11.Width = 0.1875F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 5.375F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.375F;
			this.Line5.Width = 1.1875F;
			this.Line5.X1 = 5.375F;
			this.Line5.X2 = 6.5625F;
			this.Line5.Y1 = 2.375F;
			this.Line5.Y2 = 2.375F;
			// 
			// txtLine3NumHomesteads
			// 
			this.txtLine3NumHomesteads.Height = 0.1875F;
			this.txtLine3NumHomesteads.Left = 5.375F;
			this.txtLine3NumHomesteads.Name = "txtLine3NumHomesteads";
			this.txtLine3NumHomesteads.Style = "text-align: right";
			this.txtLine3NumHomesteads.Text = null;
			this.txtLine3NumHomesteads.Top = 2.21875F;
			this.txtLine3NumHomesteads.Width = 1.1875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.0625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "";
			this.Label12.Text = "4.";
			this.Label12.Top = 2.5625F;
			this.Label12.Width = 0.1875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.15625F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 8.5pt";
			this.Label13.Text = "TOTAL ESTIMATED EXEMPT VALUE:";
			this.Label13.Top = 2.59375F;
			this.Label13.Width = 2.125F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 2.625F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 2.75F;
			this.Line6.Width = 1.0625F;
			this.Line6.X1 = 2.625F;
			this.Line6.X2 = 3.6875F;
			this.Line6.Y1 = 2.75F;
			this.Line6.Y2 = 2.75F;
			// 
			// txtLine4HomesteadVal
			// 
			this.txtLine4HomesteadVal.Height = 0.1875F;
			this.txtLine4HomesteadVal.Left = 2.625F;
			this.txtLine4HomesteadVal.Name = "txtLine4HomesteadVal";
			this.txtLine4HomesteadVal.Style = "text-align: right";
			this.txtLine4HomesteadVal.Text = null;
			this.txtLine4HomesteadVal.Top = 2.59375F;
			this.txtLine4HomesteadVal.Width = 1.0625F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 4F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 2.75F;
			this.Line7.Width = 1.0625F;
			this.Line7.X1 = 4F;
			this.Line7.X2 = 5.0625F;
			this.Line7.Y1 = 2.75F;
			this.Line7.Y2 = 2.75F;
			// 
			// txtLine4NumHomesteads
			// 
			this.txtLine4NumHomesteads.Height = 0.1875F;
			this.txtLine4NumHomesteads.Left = 4F;
			this.txtLine4NumHomesteads.Name = "txtLine4NumHomesteads";
			this.txtLine4NumHomesteads.Style = "text-align: right";
			this.txtLine4NumHomesteads.Text = null;
			this.txtLine4NumHomesteads.Top = 2.59375F;
			this.txtLine4NumHomesteads.Width = 1.0625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.75F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: center";
			this.Label14.Text = "x";
			this.Label14.Top = 2.59375F;
			this.Label14.Width = 0.1875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5.125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold";
			this.Label15.Text = "=";
			this.Label15.Top = 2.59375F;
			this.Label15.Width = 0.1875F;
			// 
			// txtLine4Total
			// 
			this.txtLine4Total.Height = 0.1875F;
			this.txtLine4Total.Left = 5.375F;
			this.txtLine4Total.Name = "txtLine4Total";
			this.txtLine4Total.Style = "text-align: right";
			this.txtLine4Total.Text = null;
			this.txtLine4Total.Top = 2.59375F;
			this.txtLine4Total.Width = 1.25F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 0F;
			this.Line8.LineWeight = 3F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 3F;
			this.Line8.Width = 7.4375F;
			this.Line8.X1 = 0F;
			this.Line8.X2 = 7.4375F;
			this.Line8.Y1 = 3F;
			this.Line8.Y2 = 3F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold; text-align: left";
			this.Label16.Text = "HOMESTEADS WITH A JUST VALUE OF AT LEAST $125,000 AND LESS THAN $250,000";
			this.Label16.Top = 3.0625F;
			this.Label16.Width = 6.1875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.0625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "";
			this.Label17.Text = "5.";
			this.Label17.Top = 3.375F;
			this.Label17.Width = 0.1875F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 0.25F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "text-align: right";
			this.Field11.Text = "$250,000";
			this.Field11.Top = 3.375F;
			this.Field11.Width = 1.0625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 1.3125F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold; text-align: center";
			this.Label18.Text = "x";
			this.Label18.Top = 3.375F;
			this.Label18.Width = 0.1875F;
			// 
			// txtLine5Ratio
			// 
			this.txtLine5Ratio.CanGrow = false;
			this.txtLine5Ratio.Height = 0.1875F;
			this.txtLine5Ratio.Left = 1.5F;
			this.txtLine5Ratio.Name = "txtLine5Ratio";
			this.txtLine5Ratio.Style = "text-align: right";
			this.txtLine5Ratio.Text = "%";
			this.txtLine5Ratio.Top = 3.375F;
			this.txtLine5Ratio.Width = 0.6875F;
			// 
			// Shape4
			// 
			this.Shape4.Height = 0.1875F;
			this.Shape4.Left = 1.5F;
			this.Shape4.Name = "Shape4";
			this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape4.Top = 3.375F;
			this.Shape4.Width = 0.6875F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 2.25F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold";
			this.Label19.Text = "=";
			this.Label19.Top = 3.375F;
			this.Label19.Width = 0.1875F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 2.5F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 3.53125F;
			this.Line9.Width = 1.4375F;
			this.Line9.X1 = 2.5F;
			this.Line9.X2 = 3.9375F;
			this.Line9.Y1 = 3.53125F;
			this.Line9.Y2 = 3.53125F;
			// 
			// txtLine5
			// 
			this.txtLine5.Height = 0.1875F;
			this.txtLine5.Left = 2.5F;
			this.txtLine5.Name = "txtLine5";
			this.txtLine5.Style = "text-align: right";
			this.txtLine5.Text = null;
			this.txtLine5.Top = 3.375F;
			this.txtLine5.Width = 1.4375F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1875F;
			this.Field14.Left = 0.25F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "text-align: right";
			this.Field14.Text = "$5,000";
			this.Field14.Top = 3.78125F;
			this.Field14.Width = 1.0625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 1.3125F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold; text-align: center";
			this.Label20.Text = "x";
			this.Label20.Top = 3.78125F;
			this.Label20.Width = 0.1875F;
			// 
			// txtLine6Ratio
			// 
			this.txtLine6Ratio.CanGrow = false;
			this.txtLine6Ratio.Height = 0.1875F;
			this.txtLine6Ratio.Left = 1.5F;
			this.txtLine6Ratio.Name = "txtLine6Ratio";
			this.txtLine6Ratio.Style = "text-align: right";
			this.txtLine6Ratio.Text = "%";
			this.txtLine6Ratio.Top = 3.78125F;
			this.txtLine6Ratio.Width = 0.6875F;
			// 
			// Shape5
			// 
			this.Shape5.Height = 0.1875F;
			this.Shape5.Left = 1.5F;
			this.Shape5.Name = "Shape5";
			this.Shape5.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape5.Top = 3.78125F;
			this.Shape5.Width = 0.6875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 2.25F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-weight: bold";
			this.Label21.Text = "=";
			this.Label21.Top = 3.78125F;
			this.Label21.Width = 0.1875F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 2.5F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 3.9375F;
			this.Line10.Width = 1.4375F;
			this.Line10.X1 = 2.5F;
			this.Line10.X2 = 3.9375F;
			this.Line10.Y1 = 3.9375F;
			this.Line10.Y2 = 3.9375F;
			// 
			// txtLine6
			// 
			this.txtLine6.Height = 0.1875F;
			this.txtLine6.Left = 2.5F;
			this.txtLine6.Name = "txtLine6";
			this.txtLine6.Style = "text-align: right";
			this.txtLine6.Text = null;
			this.txtLine6.Top = 3.78125F;
			this.txtLine6.Width = 1.4375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.0625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "";
			this.Label22.Text = "6.";
			this.Label22.Top = 3.78125F;
			this.Label22.Width = 0.1875F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.0625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "";
			this.Label23.Text = "7.";
			this.Label23.Top = 4.15625F;
			this.Label23.Width = 0.1875F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.15625F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.3125F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 8.5pt";
			this.Label24.Text = "Number of homesteads with an assessed value equal to or greater than";
			this.Label24.Top = 4.1875F;
			this.Label24.Width = 3.875F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 4.1875F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 4.34375F;
			this.Line11.Width = 1.25F;
			this.Line11.X1 = 4.1875F;
			this.Line11.X2 = 5.4375F;
			this.Line11.Y1 = 4.34375F;
			this.Line11.Y2 = 4.34375F;
			// 
			// txtLine7GreaterThan
			// 
			this.txtLine7GreaterThan.Height = 0.1875F;
			this.txtLine7GreaterThan.Left = 4.1875F;
			this.txtLine7GreaterThan.Name = "txtLine7GreaterThan";
			this.txtLine7GreaterThan.Style = "text-align: right";
			this.txtLine7GreaterThan.Text = null;
			this.txtLine7GreaterThan.Top = 4.1875F;
			this.txtLine7GreaterThan.Width = 1.25F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 1.1875F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-weight: bold";
			this.Label25.Text = "=";
			this.Label25.Top = 4.5F;
			this.Label25.Width = 0.1875F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 1.4375F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 4.65625F;
			this.Line12.Width = 1.1875F;
			this.Line12.X1 = 1.4375F;
			this.Line12.X2 = 2.625F;
			this.Line12.Y1 = 4.65625F;
			this.Line12.Y2 = 4.65625F;
			// 
			// txtLine7NumHomesteads
			// 
			this.txtLine7NumHomesteads.Height = 0.1875F;
			this.txtLine7NumHomesteads.Left = 1.4375F;
			this.txtLine7NumHomesteads.Name = "txtLine7NumHomesteads";
			this.txtLine7NumHomesteads.Style = "text-align: right";
			this.txtLine7NumHomesteads.Text = null;
			this.txtLine7NumHomesteads.Top = 4.5F;
			this.txtLine7NumHomesteads.Width = 1.1875F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.15625F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 5.4375F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-size: 8.5pt; text-align: center";
			this.Label26.Text = "and less than";
			this.Label26.Top = 4.21875F;
			this.Label26.Width = 0.8125F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 6.25F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 4.34375F;
			this.Line13.Width = 1.25F;
			this.Line13.X1 = 6.25F;
			this.Line13.X2 = 7.5F;
			this.Line13.Y1 = 4.34375F;
			this.Line13.Y2 = 4.34375F;
			// 
			// txtLine7LessThan
			// 
			this.txtLine7LessThan.Height = 0.1875F;
			this.txtLine7LessThan.Left = 6.25F;
			this.txtLine7LessThan.Name = "txtLine7LessThan";
			this.txtLine7LessThan.Style = "text-align: right";
			this.txtLine7LessThan.Text = null;
			this.txtLine7LessThan.Top = 4.1875F;
			this.txtLine7LessThan.Width = 1.25F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.0625F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "";
			this.Label27.Text = "8.";
			this.Label27.Top = 4.875F;
			this.Label27.Width = 0.1875F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.15625F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.375F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-size: 8.5pt";
			this.Label28.Text = "TOTAL ESTIMATED EXEMPT VALUE:";
			this.Label28.Top = 4.90625F;
			this.Label28.Width = 2.125F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 2.625F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 5.0625F;
			this.Line14.Width = 1.0625F;
			this.Line14.X1 = 2.625F;
			this.Line14.X2 = 3.6875F;
			this.Line14.Y1 = 5.0625F;
			this.Line14.Y2 = 5.0625F;
			// 
			// txtLine8HomesteadVal
			// 
			this.txtLine8HomesteadVal.Height = 0.1875F;
			this.txtLine8HomesteadVal.Left = 2.625F;
			this.txtLine8HomesteadVal.Name = "txtLine8HomesteadVal";
			this.txtLine8HomesteadVal.Style = "text-align: right";
			this.txtLine8HomesteadVal.Text = null;
			this.txtLine8HomesteadVal.Top = 4.90625F;
			this.txtLine8HomesteadVal.Width = 1.0625F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 4F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 5.0625F;
			this.Line15.Width = 1.0625F;
			this.Line15.X1 = 4F;
			this.Line15.X2 = 5.0625F;
			this.Line15.Y1 = 5.0625F;
			this.Line15.Y2 = 5.0625F;
			// 
			// txtLine8NumHomesteads
			// 
			this.txtLine8NumHomesteads.Height = 0.1875F;
			this.txtLine8NumHomesteads.Left = 4F;
			this.txtLine8NumHomesteads.Name = "txtLine8NumHomesteads";
			this.txtLine8NumHomesteads.Style = "text-align: right";
			this.txtLine8NumHomesteads.Text = null;
			this.txtLine8NumHomesteads.Top = 4.90625F;
			this.txtLine8NumHomesteads.Width = 1.0625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 3.75F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-weight: bold; text-align: center";
			this.Label29.Text = "x";
			this.Label29.Top = 4.90625F;
			this.Label29.Width = 0.1875F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 5.125F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-weight: bold";
			this.Label30.Text = "=";
			this.Label30.Top = 4.90625F;
			this.Label30.Width = 0.1875F;
			// 
			// txtLine8Total
			// 
			this.txtLine8Total.Height = 0.1875F;
			this.txtLine8Total.Left = 5.375F;
			this.txtLine8Total.Name = "txtLine8Total";
			this.txtLine8Total.Style = "text-align: right";
			this.txtLine8Total.Text = null;
			this.txtLine8Total.Top = 4.90625F;
			this.txtLine8Total.Width = 1.25F;
			// 
			// Line16
			// 
			this.Line16.Height = 0F;
			this.Line16.Left = 0F;
			this.Line16.LineWeight = 3F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 5.34375F;
			this.Line16.Width = 7.4375F;
			this.Line16.X1 = 0F;
			this.Line16.X2 = 7.4375F;
			this.Line16.Y1 = 5.34375F;
			this.Line16.Y2 = 5.34375F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0.625F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-weight: bold; text-align: left";
			this.Label31.Text = "HOMESTEADS WITH A JUST VALUE GREATER THAN $250,000";
			this.Label31.Top = 5.40625F;
			this.Label31.Width = 6.1875F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0.0625F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "";
			this.Label32.Text = "9.";
			this.Label32.Top = 5.71875F;
			this.Label32.Width = 0.1875F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.1875F;
			this.Field23.Left = 0.25F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "text-align: right";
			this.Field23.Text = "$2,500";
			this.Field23.Top = 5.71875F;
			this.Field23.Width = 1.0625F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 1.3125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-weight: bold; text-align: center";
			this.Label33.Text = "x";
			this.Label33.Top = 5.71875F;
			this.Label33.Width = 0.1875F;
			// 
			// txtLine9Ratio
			// 
			this.txtLine9Ratio.CanGrow = false;
			this.txtLine9Ratio.Height = 0.1875F;
			this.txtLine9Ratio.Left = 1.5F;
			this.txtLine9Ratio.Name = "txtLine9Ratio";
			this.txtLine9Ratio.Style = "text-align: right";
			this.txtLine9Ratio.Text = "%";
			this.txtLine9Ratio.Top = 5.71875F;
			this.txtLine9Ratio.Width = 0.6875F;
			// 
			// Shape6
			// 
			this.Shape6.Height = 0.1875F;
			this.Shape6.Left = 1.5F;
			this.Shape6.Name = "Shape6";
			this.Shape6.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape6.Top = 5.71875F;
			this.Shape6.Width = 0.6875F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 2.25F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-weight: bold";
			this.Label34.Text = "=";
			this.Label34.Top = 5.71875F;
			this.Label34.Width = 0.1875F;
			// 
			// Line17
			// 
			this.Line17.Height = 0F;
			this.Line17.Left = 2.5F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 5.875F;
			this.Line17.Width = 1.4375F;
			this.Line17.X1 = 2.5F;
			this.Line17.X2 = 3.9375F;
			this.Line17.Y1 = 5.875F;
			this.Line17.Y2 = 5.875F;
			// 
			// txtLine9
			// 
			this.txtLine9.Height = 0.1875F;
			this.txtLine9.Left = 2.5F;
			this.txtLine9.Name = "txtLine9";
			this.txtLine9.Style = "text-align: right";
			this.txtLine9.Text = null;
			this.txtLine9.Top = 5.71875F;
			this.txtLine9.Width = 1.4375F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "";
			this.Label35.Text = "10.";
			this.Label35.Top = 6.125F;
			this.Label35.Width = 0.25F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.15625F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 0.3125F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-size: 8.5pt";
			this.Label36.Text = "Number of homesteads with an assessed value greater than";
			this.Label36.Top = 6.15625F;
			this.Label36.Width = 3.375F;
			// 
			// Line18
			// 
			this.Line18.Height = 0F;
			this.Line18.Left = 3.8125F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 6.3125F;
			this.Line18.Width = 1.25F;
			this.Line18.X1 = 3.8125F;
			this.Line18.X2 = 5.0625F;
			this.Line18.Y1 = 6.3125F;
			this.Line18.Y2 = 6.3125F;
			// 
			// txtLine10LessThan
			// 
			this.txtLine10LessThan.Height = 0.1875F;
			this.txtLine10LessThan.Left = 3.8125F;
			this.txtLine10LessThan.Name = "txtLine10LessThan";
			this.txtLine10LessThan.Style = "text-align: right";
			this.txtLine10LessThan.Text = null;
			this.txtLine10LessThan.Top = 6.15625F;
			this.txtLine10LessThan.Width = 1.25F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1875F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 5.125F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-weight: bold";
			this.Label37.Text = "=";
			this.Label37.Top = 6.15625F;
			this.Label37.Width = 0.1875F;
			// 
			// Line19
			// 
			this.Line19.Height = 0F;
			this.Line19.Left = 5.375F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 6.3125F;
			this.Line19.Width = 1.1875F;
			this.Line19.X1 = 5.375F;
			this.Line19.X2 = 6.5625F;
			this.Line19.Y1 = 6.3125F;
			this.Line19.Y2 = 6.3125F;
			// 
			// txtLine10NumHomesteads
			// 
			this.txtLine10NumHomesteads.Height = 0.1875F;
			this.txtLine10NumHomesteads.Left = 5.375F;
			this.txtLine10NumHomesteads.Name = "txtLine10NumHomesteads";
			this.txtLine10NumHomesteads.Style = "text-align: right";
			this.txtLine10NumHomesteads.Text = null;
			this.txtLine10NumHomesteads.Top = 6.15625F;
			this.txtLine10NumHomesteads.Width = 1.1875F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1875F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 0F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "";
			this.Label38.Text = "11.";
			this.Label38.Top = 6.53125F;
			this.Label38.Width = 0.25F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.15625F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 0.375F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-size: 8.5pt";
			this.Label39.Text = "TOTAL ESTIMATED EXEMPT VALUE:";
			this.Label39.Top = 6.5625F;
			this.Label39.Width = 2.125F;
			// 
			// Line20
			// 
			this.Line20.Height = 0F;
			this.Line20.Left = 2.625F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 6.71875F;
			this.Line20.Width = 1.0625F;
			this.Line20.X1 = 2.625F;
			this.Line20.X2 = 3.6875F;
			this.Line20.Y1 = 6.71875F;
			this.Line20.Y2 = 6.71875F;
			// 
			// txtLine11ExemptVal
			// 
			this.txtLine11ExemptVal.Height = 0.1875F;
			this.txtLine11ExemptVal.Left = 2.625F;
			this.txtLine11ExemptVal.Name = "txtLine11ExemptVal";
			this.txtLine11ExemptVal.Style = "text-align: right";
			this.txtLine11ExemptVal.Text = null;
			this.txtLine11ExemptVal.Top = 6.5625F;
			this.txtLine11ExemptVal.Width = 1.0625F;
			// 
			// Line21
			// 
			this.Line21.Height = 0F;
			this.Line21.Left = 4F;
			this.Line21.LineWeight = 1F;
			this.Line21.Name = "Line21";
			this.Line21.Top = 6.71875F;
			this.Line21.Width = 1.0625F;
			this.Line21.X1 = 4F;
			this.Line21.X2 = 5.0625F;
			this.Line21.Y1 = 6.71875F;
			this.Line21.Y2 = 6.71875F;
			// 
			// txtLine11NumHomesteads
			// 
			this.txtLine11NumHomesteads.Height = 0.1875F;
			this.txtLine11NumHomesteads.Left = 4F;
			this.txtLine11NumHomesteads.Name = "txtLine11NumHomesteads";
			this.txtLine11NumHomesteads.Style = "text-align: right";
			this.txtLine11NumHomesteads.Text = null;
			this.txtLine11NumHomesteads.Top = 6.5625F;
			this.txtLine11NumHomesteads.Width = 1.0625F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1875F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 3.75F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-weight: bold; text-align: center";
			this.Label40.Text = "x";
			this.Label40.Top = 6.5625F;
			this.Label40.Width = 0.1875F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1875F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 5.125F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-weight: bold";
			this.Label41.Text = "=";
			this.Label41.Top = 6.5625F;
			this.Label41.Width = 0.1875F;
			// 
			// txtLine11Total
			// 
			this.txtLine11Total.Height = 0.1875F;
			this.txtLine11Total.Left = 5.375F;
			this.txtLine11Total.Name = "txtLine11Total";
			this.txtLine11Total.Style = "text-align: right";
			this.txtLine11Total.Text = null;
			this.txtLine11Total.Top = 6.5625F;
			this.txtLine11Total.Width = 1.25F;
			// 
			// Line22
			// 
			this.Line22.Height = 0F;
			this.Line22.Left = 0F;
			this.Line22.LineWeight = 3F;
			this.Line22.Name = "Line22";
			this.Line22.Top = 7.03125F;
			this.Line22.Width = 7.4375F;
			this.Line22.X1 = 0F;
			this.Line22.X2 = 7.4375F;
			this.Line22.Y1 = 7.03125F;
			this.Line22.Y2 = 7.03125F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1875F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 0F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "";
			this.Label42.Text = "12.";
			this.Label42.Top = 7.15625F;
			this.Label42.Width = 0.25F;
			// 
			// lbl12
			// 
			this.lbl12.Height = 0.15625F;
			this.lbl12.HyperLink = null;
			this.lbl12.Left = 0.375F;
			this.lbl12.Name = "lbl12";
			this.lbl12.Style = "font-size: 8.5pt";
			this.lbl12.Text = "TOTAL NUMBER OF HOMESTEAD EXEMPTIONS GRANTED (SUM OF LINES 3,7,10)";
			this.lbl12.Top = 7.1875F;
			this.lbl12.Width = 4.4375F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1875F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 0F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "";
			this.Label44.Text = "13.";
			this.Label44.Top = 7.5625F;
			this.Label44.Width = 0.25F;
			// 
			// lbl13
			// 
			this.lbl13.Height = 0.15625F;
			this.lbl13.HyperLink = null;
			this.lbl13.Left = 0.375F;
			this.lbl13.Name = "lbl13";
			this.lbl13.Style = "font-size: 8.5pt";
			this.lbl13.Text = "TOTAL AMOUNT OF 2003 HOMESTEAD VALUE EXEMPTED (SUM OF LINES 4,8,11)";
			this.lbl13.Top = 7.59375F;
			this.lbl13.Width = 4.4375F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1875F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 5.125F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-weight: bold";
			this.Label46.Text = "=";
			this.Label46.Top = 7.15625F;
			this.Label46.Width = 0.1875F;
			// 
			// txtLine12
			// 
			this.txtLine12.Height = 0.1875F;
			this.txtLine12.Left = 5.375F;
			this.txtLine12.Name = "txtLine12";
			this.txtLine12.Style = "text-align: right";
			this.txtLine12.Text = null;
			this.txtLine12.Top = 7.15625F;
			this.txtLine12.Width = 1.1875F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1875F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 5.125F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-weight: bold";
			this.Label47.Text = "=";
			this.Label47.Top = 7.5625F;
			this.Label47.Width = 0.1875F;
			// 
			// txtLine13
			// 
			this.txtLine13.Height = 0.1875F;
			this.txtLine13.Left = 5.375F;
			this.txtLine13.Name = "txtLine13";
			this.txtLine13.Style = "text-align: right";
			this.txtLine13.Text = null;
			this.txtLine13.Top = 7.5625F;
			this.txtLine13.Width = 1.1875F;
			// 
			// Shape7
			// 
			this.Shape7.Height = 0.1875F;
			this.Shape7.Left = 5.375F;
			this.Shape7.Name = "Shape7";
			this.Shape7.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape7.Top = 6.5625F;
			this.Shape7.Width = 1.25F;
			// 
			// Shape8
			// 
			this.Shape8.Height = 0.1875F;
			this.Shape8.Left = 5.375F;
			this.Shape8.Name = "Shape8";
			this.Shape8.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape8.Top = 4.90625F;
			this.Shape8.Width = 1.25F;
			// 
			// Shape9
			// 
			this.Shape9.Height = 0.1875F;
			this.Shape9.Left = 5.375F;
			this.Shape9.Name = "Shape9";
			this.Shape9.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape9.Top = 2.59375F;
			this.Shape9.Width = 1.25F;
			// 
			// Line23
			// 
			this.Line23.Height = 0F;
			this.Line23.Left = 5.375F;
			this.Line23.LineWeight = 1F;
			this.Line23.Name = "Line23";
			this.Line23.Top = 7.3125F;
			this.Line23.Width = 1.1875F;
			this.Line23.X1 = 5.375F;
			this.Line23.X2 = 6.5625F;
			this.Line23.Y1 = 7.3125F;
			this.Line23.Y2 = 7.3125F;
			// 
			// Line24
			// 
			this.Line24.Height = 0F;
			this.Line24.Left = 5.375F;
			this.Line24.LineWeight = 1F;
			this.Line24.Name = "Line24";
			this.Line24.Top = 7.71875F;
			this.Line24.Width = 1.1875F;
			this.Line24.X1 = 5.375F;
			this.Line24.X2 = 6.5625F;
			this.Line24.Y1 = 7.71875F;
			this.Line24.Y2 = 7.71875F;
			// 
			// rptHomesteadCalculation
			//
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCertifiedRatio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine1Ratio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TXTLine1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2Ratio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3Amount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine3NumHomesteads)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4HomesteadVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4NumHomesteads)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine4Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5Ratio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6Ratio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7GreaterThan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7NumHomesteads)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine7LessThan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8HomesteadVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8NumHomesteads)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine8Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9Ratio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine10LessThan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine10NumHomesteads)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11ExemptVal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11NumHomesteads)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine11Total)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLine13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCertifiedRatio;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine1Ratio;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox TXTLine1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2Ratio;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3Amount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine3NumHomesteads;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4HomesteadVal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4NumHomesteads;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine4Total;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5Ratio;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6Ratio;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine7GreaterThan;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine7NumHomesteads;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine7LessThan;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine8HomesteadVal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine8NumHomesteads;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine8Total;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine9Ratio;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine10LessThan;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine10NumHomesteads;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine11ExemptVal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine11NumHomesteads;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine11Total;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLine13;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape7;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape8;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line24;
	}
}
