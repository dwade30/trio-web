﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWRE0000
{
	public class modREFileFormatsXF
	{
		//=========================================================
		public struct ConvertedData
		{
			public string MapLot;
			public int StreetNumber;
			public string StreetName;
			public string CompetitionAccount;
			public int TrioAccount;
			public string Name;
			public string SecOwner;
			public string Address1;
			public string Address2;
			public string City;
			public string State;
			public string Zip;
			public string Zip4;
			public string PhoneNumber;
			public string BookPage;
			public string Book;
			public string Page;
			public string ExemptCode1;
			public string ExemptCode2;
			public string ExemptCode3;
			public int ExemptAmount1;
			public int ExemptAmount2;
			public int ExemptAmount3;
			public double Acreage;
			public int LandValue;
			public int BldgValue;
			public int OtherValue;
			public bool BadExempt;
			public bool TooManyExempt;
			public bool BadExemptAmount;
			public bool BadMapLot;
			public bool UpdatedTooMany;
			public int PercentageOwned;
			public string MultiOwner;
			public string GUID;

			public ConvertedData(int unusedParam)
			{
				this.MapLot = "";
				this.StreetNumber = 0;
				this.StreetName = "";
				this.CompetitionAccount = "";
				this.TrioAccount = 0;
				this.Name = "";
				this.SecOwner = "";
				this.Address1 = "";
				this.Address2 = "";
				this.City = "";
				this.State = "";
				this.Zip = "";
				this.Zip4 = "";
				this.PhoneNumber = "";
				this.BookPage = "";
				this.Book = "";
				this.Page = "";
				this.ExemptCode1 = "";
				this.ExemptCode2 = "";
				this.ExemptCode3 = "";
				this.ExemptAmount1 = 0;
				this.ExemptAmount2 = 0;
				this.ExemptAmount3 = 0;
				this.Acreage = 0;
				this.LandValue = 0;
				this.BldgValue = 0;
				this.OtherValue = 0;
				this.BadExempt = false;
				this.TooManyExempt = false;
				this.BadExemptAmount = false;
				this.BadMapLot = false;
				this.UpdatedTooMany = false;
				this.PercentageOwned = 0;
				this.MultiOwner = "";
				this.GUID = "";
			}
		};

		public struct VisionAccountInfo
		{
			// occurs once
			public FCFixedString Map;
			public FCFixedString MapCut;
			public FCFixedString Block;
			public FCFixedString BlockCut;
			public FCFixedString Lot;
			public FCFixedString LotCut;
			public FCFixedString AltParcelID;
			public FCFixedString LocStreet;
			public FCFixedString LocNumber;
			public FCFixedString AccountNum;
			public FCFixedString District;
			public VisionAccountInfo(int unusedParam)
			{
				this.Map = new FCFixedString(4);
				this.MapCut = new FCFixedString(1);
				this.Block = new FCFixedString(4);
				this.BlockCut = new FCFixedString(1);
				this.Lot = new FCFixedString(4);
				this.LotCut = new FCFixedString(1);
				this.AltParcelID = new FCFixedString(21);
				this.LocStreet = new FCFixedString(25);
				this.LocNumber = new FCFixedString(10);
				this.AccountNum = new FCFixedString(20);
				this.District = new FCFixedString(2);
			}
		};

		public struct VisionOwnerRecord
		{
			// occurs once
			public FCFixedString owner;
			public FCFixedString SecOwner;
			public FCFixedString Address1;
			public FCFixedString Address2;
			public FCFixedString CityStZip;
			public FCFixedString City;
			public FCFixedString State;
			public FCFixedString Zip;
			public FCFixedString Country;
			public VisionOwnerRecord(int unusedParam)
			{
				this.owner = new FCFixedString(40);
				this.SecOwner = new FCFixedString(40);
				this.Address1 = new FCFixedString(30);
				this.Address2 = new FCFixedString(30);
				this.CityStZip = new FCFixedString(40);
				this.City = new FCFixedString(20);
				this.State = new FCFixedString(2);
				this.Zip = new FCFixedString(20);
				this.Country = new FCFixedString(18);
			}
		};

		public struct VisionSupplemental
		{
			// occurs once
			public FCFixedString User1;
			public FCFixedString User2;
			public FCFixedString User3;
			public FCFixedString User4;
			public FCFixedString User5;
			public FCFixedString User6;
			public FCFixedString User7;
			public FCFixedString User8;
			public FCFixedString User9;
			public FCFixedString User10;
			public VisionSupplemental(int unusedParam)
			{
				this.User1 = new FCFixedString(30);
				this.User2 = new FCFixedString(30);
				this.User3 = new FCFixedString(30);
				this.User4 = new FCFixedString(30);
				this.User5 = new FCFixedString(30);
				this.User6 = new FCFixedString(30);
				this.User7 = new FCFixedString(30);
				this.User8 = new FCFixedString(30);
				this.User9 = new FCFixedString(30);
				this.User10 = new FCFixedString(30);
			}
		};

		public struct VisionSaleHistory
		{
			// occurs 6 times
			public FCFixedString BookPage;
			public FCFixedString SaleDate;
			public FCFixedString owner;
			public FCFixedString Validity;
			public FCFixedString Qualified;
			public FCFixedString Vacant;
			public FCFixedString SalePrice;
			public VisionSaleHistory(int unusedParam)
			{
				this.BookPage = new FCFixedString(10);
				this.SaleDate = new FCFixedString(8);
				this.owner = new FCFixedString(40);
				this.Validity = new FCFixedString(2);
				this.Qualified = new FCFixedString(1);
				this.Vacant = new FCFixedString(1);
				this.SalePrice = new FCFixedString(9);
			}
		};

		public struct VisionExemptions
		{
			// occurs 8 times
			// vbPorter upgrade warning: Year As FixedString	OnWrite(string)
			public string Year;
			// vbPorter upgrade warning: Code As FixedString	OnWrite(string, int, double)
			public string Code;
			// vbPorter upgrade warning: Amount As FixedString	OnWrite(int, string)
			public string Amount;
			public VisionExemptions(int unusedParam)
			{
				this.Year = new string(' ', 4);
				this.Code = new string(' ', 4);
				this.Amount = new string(' ', 11);
			}
		};

		public struct VisionOtherAssessments
		{
			// occurs 4 times
			public FCFixedString Number;
			public FCFixedString Code;
			public FCFixedString Year;
			public FCFixedString Amount;
			public FCFixedString Interest;
			public VisionOtherAssessments(int unusedParam)
			{
				this.Number = new FCFixedString(7);
				this.Code = new FCFixedString(2);
				this.Year = new FCFixedString(4);
				this.Amount = new FCFixedString(11);
				this.Interest = new FCFixedString(11);
			}
		};

		public struct VisionAssessedValues
		{
			// occurs 12 times
			public FCFixedString Code;
			public FCFixedString LandType;
			public FCFixedString Description;
			public FCFixedString LandArea;
			public FCFixedString LandValue;
			public FCFixedString BldgValue;
			public FCFixedString TotalAppraised;
			public FCFixedString TotalAssessed;
			public VisionAssessedValues(int unusedParam)
			{
				this.Code = new FCFixedString(4);
				this.LandType = new FCFixedString(1);
				this.Description = new FCFixedString(8);
				this.LandArea = new FCFixedString(9);
				this.LandValue = new FCFixedString(9);
				this.BldgValue = new FCFixedString(9);
				this.TotalAppraised = new FCFixedString(9);
				this.TotalAssessed = new FCFixedString(9);
			}
		};

		public struct VisionAdditionalParcelInfo
		{
			// occurs once
			public FCFixedString Map;
			public FCFixedString MapCut;
			public FCFixedString Block;
			public FCFixedString BlockCut;
			public FCFixedString Lot;
			public FCFixedString LotCut;
			public FCFixedString Unit;
			public FCFixedString UnitCut;
			public FCFixedString Modified;
			public FCFixedString TotalAppraised;
			public FCFixedString TotalAssessed;
			public VisionAdditionalParcelInfo(int unusedParam)
			{
				this.Map = new FCFixedString(7);
				this.MapCut = new FCFixedString(3);
				this.Block = new FCFixedString(7);
				this.BlockCut = new FCFixedString(3);
				this.Lot = new FCFixedString(7);
				this.LotCut = new FCFixedString(3);
				this.Unit = new FCFixedString(7);
				this.UnitCut = new FCFixedString(3);
				this.Modified = new FCFixedString(8);
				this.TotalAppraised = new FCFixedString(9);
				this.TotalAssessed = new FCFixedString(9);
			}
		};

		public struct VisionCurrentOwner
		{
			// occurs once
			public FCFixedString owner;
			public FCFixedString SecOwner;
			public FCFixedString Address1;
			public FCFixedString Address2;
			public FCFixedString CityStZip;
			public FCFixedString City;
			public FCFixedString State;
			public FCFixedString Zip;
			public FCFixedString Country;
			public VisionCurrentOwner(int unusedParam)
			{
				this.owner = new FCFixedString(40);
				this.SecOwner = new FCFixedString(40);
				this.Address1 = new FCFixedString(30);
				this.Address2 = new FCFixedString(30);
				this.CityStZip = new FCFixedString(40);
				this.City = new FCFixedString(20);
				this.State = new FCFixedString(2);
				this.Zip = new FCFixedString(20);
				this.Country = new FCFixedString(18);
			}
		};

		public struct VisionCurrentMultiOwner
		{
			// occurs once
			public FCFixedString owner;
			public FCFixedString SecOwner;
			public FCFixedString Address1;
			public FCFixedString Address2;
			public FCFixedString CityStZip;
			public FCFixedString City;
			public FCFixedString State;
			public FCFixedString Zip;
			public FCFixedString Country;
			public FCFixedString PercentOwned;
			public VisionCurrentMultiOwner(int unusedParam)
			{
				this.owner = new FCFixedString(40);
				this.SecOwner = new FCFixedString(40);
				this.Address1 = new FCFixedString(30);
				this.Address2 = new FCFixedString(30);
				this.CityStZip = new FCFixedString(40);
				this.City = new FCFixedString(20);
				this.State = new FCFixedString(2);
				this.Zip = new FCFixedString(20);
				this.Country = new FCFixedString(18);
				this.PercentOwned = new FCFixedString(3);
			}
		};
	}
}
