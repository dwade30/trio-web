﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmOwnershipChangeReason.
	/// </summary>
	partial class frmOwnershipChangeReason : BaseForm
	{
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCComboBox cmbReason;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOwnershipChangeReason));
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmbReason = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 241);
			this.BottomPanel.Size = new System.Drawing.Size(488, 98);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.cmbReason);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(488, 181);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(488, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(225, 30);
			this.HeaderText.Text = "Reason for Change";
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(30, 122);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(73, 40);
			this.cmdCancel.TabIndex = 3;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmbReason
			// 
			this.cmbReason.AutoSize = false;
			this.cmbReason.BackColor = System.Drawing.SystemColors.Window;
			this.cmbReason.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbReason.FormattingEnabled = true;
			this.cmbReason.Location = new System.Drawing.Point(30, 66);
			this.cmbReason.Name = "cmbReason";
			this.cmbReason.Size = new System.Drawing.Size(363, 40);
			this.cmbReason.TabIndex = 1;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(401, 34);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "PLEASE SPECIFY THE REASON FOR CHANGING THE OWNER PARTY";
			// 
			// cmdSave
			// 
			this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(172, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(150, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save & Continue";
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmOwnershipChangeReason
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(488, 339);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmOwnershipChangeReason";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Reason for Change";
			this.Load += new System.EventHandler(this.frmOwnershipChangeReason_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmOwnershipChangeReason_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
