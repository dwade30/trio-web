﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetValuationComparisonInfo.
	/// </summary>
	public partial class frmGetValuationComparisonInfo : BaseForm
	{
		public frmGetValuationComparisonInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label2 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.txtRange = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.chkRecalc = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.cmbYear = new System.Collections.Generic.List<fecherFoundation.FCComboBox>();
			this.Label2.AddControlArrayElement(Label2_2, 2);
			this.Label2.AddControlArrayElement(Label2_1, 1);
			this.Label2.AddControlArrayElement(Label2_0, 0);
			this.txtRange.AddControlArrayElement(txtRange_0, 0);
			this.txtRange.AddControlArrayElement(txtRange_1, 1);
			this.chkRecalc.AddControlArrayElement(chkRecalc_1, 1);
			this.chkRecalc.AddControlArrayElement(chkRecalc_0, 0);
			this.cmbYear.AddControlArrayElement(cmbYear_1, 1);
			this.cmbYear.AddControlArrayElement(cmbYear_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetValuationComparisonInfo InstancePtr
		{
			get
			{
				return (frmGetValuationComparisonInfo)Sys.GetInstance(typeof(frmGetValuationComparisonInfo));
			}
		}

		protected frmGetValuationComparisonInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 05/09/2005
		// ********************************************************
		public void Init()
		{
			//FileSystemObject fso = new FileSystemObject();
			clsDRWrapper rsLoad = new clsDRWrapper();
			if (!rsLoad.DBExists("RECommit"))
			{
				// If Not File.Exists(strCommitDB) Then
				MessageBox.Show("You will need to create and fill the commitment database before you can run the analysis", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				frmCommit.InstancePtr.Init();
				if (!File.Exists(modGlobalVariables.Statics.strCommitDB))
					return;
				rsLoad.OpenRecordset("select * FROM CUSTOMIZE order by commitmentyear desc", modGlobalVariables.Statics.strCommitDB);
				if (rsLoad.EndOfFile())
				{
					return;
				}
			}
			else
			{
				rsLoad.OpenRecordset("select * FROM CUSTOMIZE order by commitmentyear desc", modGlobalVariables.Statics.strCommitDB);
				if (rsLoad.EndOfFile())
				{
					MessageBox.Show("You will need to fill the commitment database before you can run the analysis", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					frmCommit.InstancePtr.Init();
					rsLoad.OpenRecordset("select * from customize order by commitmentyear desc", modGlobalVariables.Statics.strCommitDB);
					if (rsLoad.EndOfFile())
					{
						return;
					}
				}
			}
			if (!Loadinfo())
			{
				this.Unload();
				return;
			}
			this.Show(App.MainForm);
		}

		private void frmGetValuationComparisonInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGetValuationComparisonInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetValuationComparisonInfo properties;
			//frmGetValuationComparisonInfo.FillStyle	= 0;
			//frmGetValuationComparisonInfo.ScaleWidth	= 5880;
			//frmGetValuationComparisonInfo.ScaleHeight	= 4065;
			//frmGetValuationComparisonInfo.LinkTopic	= "Form2";
			//frmGetValuationComparisonInfo.LockControls	= true;
			//frmGetValuationComparisonInfo.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			if (modREMain.Statics.boolShortRealEstate)
			{
				chkRecalc[0].CheckState = Wisej.Web.CheckState.Unchecked;
				chkRecalc[1].CheckState = Wisej.Web.CheckState.Unchecked;
				chkRecalc[0].Enabled = false;
				chkRecalc[1].Enabled = false;
				txtDepreciation.Enabled = false;
				txtDepreciationComm.Enabled = false;
				txtDepreciationMH.Enabled = false;
				cmbCriteria.Clear();
				cmbCriteria.Items.Add("Any Increased Valuation");
				cmbCriteria.Text = "Any Increased Valuation";
				chkShowUnexplained.Visible = false;
			}
		}

		private bool Loadinfo()
		{
			bool Loadinfo = false;
			// fill the commitment years and depreciation info
			// exit with an error if there is no data to load
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				Loadinfo = false;
				clsLoad.OpenRecordset("select * FROM CUSTOMIZE order by commitmentyear desc", modGlobalVariables.Statics.strCommitDB);
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("There is no data saved for any commitment years" + "\r\n" + "Cannot Continue", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Loadinfo = false;
					return Loadinfo;
				}
				cmbYear[0].Clear();
				cmbYear[1].Clear();
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [commitmentyear] not found!! (maybe it is an alias?)
					cmbYear[0].AddItem(Strings.Format(clsLoad.Get_Fields("commitmentyear"), "MM/dd/yyyy"));
					// TODO Get_Fields: Field [commitmentyear] not found!! (maybe it is an alias?)
					cmbYear[1].AddItem(Strings.Format(clsLoad.Get_Fields("commitmentyear"), "MM/dd/yyyy"));
					clsLoad.MoveNext();
				}
				// initialize to the last two years in the database
				cmbYear[0].SelectedIndex = 0;
				if (cmbYear[1].Items.Count > 1)
				{
					cmbYear[1].SelectedIndex = 1;
				}
				txtDepreciation.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.DepreciationYear);
				txtDepreciationMH.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.MHDepreciationYear);
				txtDepreciationComm.Text = FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CommDepreciationYear);
				Loadinfo = true;
				return Loadinfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Loadinfo;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuImport_Click(object sender, System.EventArgs e)
		{
			frmCommit.InstancePtr.Init();
			Loadinfo();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			int[] lngYear = new int[2 + 1];
			bool[] boolReCalc = new bool[2 + 1];
			int intRangeOption = 0;
			int intRangeType = 0;
			string strMin;
			string strMax;
			bool boolAutoDetermine = false;
			// check if there is a valid range
			if (Conversion.Val(cmbYear[0].Text) == Conversion.Val(cmbYear[1].Text))
			{
				MessageBox.Show("You must compare two different years", "Same Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// they can choose in any order, but sort so the most recent year is sent as first parameter
			if (Conversion.Val(cmbYear[0].Text) > Conversion.Val(cmbYear[1].Text))
			{
				lngYear[1] = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbYear[0].Text)));
				lngYear[2] = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbYear[1].Text)));
				boolReCalc[1] = chkRecalc[0].CheckState == Wisej.Web.CheckState.Checked;
				boolReCalc[2] = chkRecalc[1].CheckState == Wisej.Web.CheckState.Checked;
			}
			else
			{
				lngYear[1] = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbYear[1].Text)));
				lngYear[2] = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbYear[0].Text)));
				boolReCalc[1] = chkRecalc[1].CheckState == Wisej.Web.CheckState.Unchecked;
				boolReCalc[2] = chkRecalc[0].CheckState == Wisej.Web.CheckState.Unchecked;
			}
			strMin = "";
			strMax = "";
			if (cmbRange.Text == "Account")
			{
				intRangeType = 0;
			}
			else if (cmbRange.Text == "Map Lot")
			{
				intRangeType = 1;
			}
			else
			{
				intRangeType = 2;
			}
			if (cmbtFrom.Text == "All")
			{
				intRangeOption = 0;
			}
			else if (cmbtFrom.Text == "Range")
			{
				intRangeOption = 1;
				if (Strings.Trim(txtRange[0].Text) == string.Empty || Strings.Trim(txtRange[1].Text) == string.Empty)
				{
					MessageBox.Show("You must specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (intRangeType == 0)
				{
					// account
					if (Conversion.Val(txtRange[1].Text) < Conversion.Val(txtRange[0].Text))
					{
						MessageBox.Show("You must specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else if (Conversion.Val(txtRange[1].Text) <= 0 || Conversion.Val(txtRange[0].Text) <= 0)
					{
						MessageBox.Show("You must specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					strMin = FCConvert.ToString(Conversion.Val(txtRange[0].Text));
					strMax = FCConvert.ToString(Conversion.Val(txtRange[1].Text));
				}
				else
				{
					if (Strings.Trim(txtRange[0].Text) == string.Empty || Strings.Trim(txtRange[1].Text) == string.Empty)
					{
						MessageBox.Show("You must specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else if (Strings.CompareString(Strings.Trim(txtRange[1].Text), "<", Strings.Trim(txtRange[0].Text)))
					{
						MessageBox.Show("You must specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					strMin = Strings.Trim(txtRange[0].Text);
					strMax = Strings.Trim(txtRange[1].Text);
				}
			}
			else
			{
				intRangeOption = 2;
			}
			if (cmbCriteria.Text == "Auto determine eligibility")
			{
				boolAutoDetermine = true;
			}
			else
			{
				boolAutoDetermine = false;
			}
			// open the analysis screen
			frmNewValuationAnalysis.InstancePtr.Init(ref lngYear[1], ref lngYear[2], ref boolReCalc[1], ref boolReCalc[2], FCConvert.ToInt32(Conversion.Val(txtDepreciation.Text)), FCConvert.ToInt32(Conversion.Val(txtDepreciationMH.Text)), FCConvert.ToInt32(Conversion.Val(txtDepreciationComm.Text)), ref intRangeOption, ref intRangeType, ref strMin, ref strMax, ref boolAutoDetermine, chkShowUnexplained.CheckState == Wisej.Web.CheckState.Checked);
			mnuExit_Click();
		}

		private void optCriteria_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						chkShowUnexplained.Visible = true;
						break;
					}
				case 1:
					{
						chkShowUnexplained.Visible = false;
						break;
					}
			}
			//end switch
		}

		private void optCriteria_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbCriteria.SelectedIndex;
			optCriteria_CheckedChanged(index, sender, e);
		}

		private void OptFrom_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// all
						framRange.Visible = false;
						break;
					}
				case 1:
					{
						// range
						framRange.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void OptFrom_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtFrom.SelectedIndex;
			OptFrom_CheckedChanged(index, sender, e);
		}
	}
}
