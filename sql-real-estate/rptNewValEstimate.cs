﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewValEstimate.
	/// </summary>
	public partial class rptNewValEstimate : BaseSectionReport
	{
		public static rptNewValEstimate InstancePtr
		{
			get
			{
				return (rptNewValEstimate)Sys.GetInstance(typeof(rptNewValEstimate));
			}
		}

		protected rptNewValEstimate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptNewValEstimate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "New Valuation Analysis";
		}
		// nObj = 1
		//   0	rptNewValEstimate	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngCurRow;
		double dblTotalPrevious;
		double dblTotalCurrent;
		double dblTotalNew;
		int lngCount;
		const int CNSTGRIDCOLACCOUNT = 0;
		const int CNSTGRIDCOLMAPLOT = 1;
		const int CNSTGRIDCOLPREVLAND = 2;
		const int CNSTGRIDCOLPREVBLDG = 3;
		const int CNSTGRIDCOLPREVEXEMPTION = 4;
		const int CNSTGRIDCOLCURLAND = 5;
		const int CNSTGRIDCOLCURBLDG = 6;
		const int CNSTGRIDCOLCUREXEMPTION = 7;
		const int CNSTGRIDCOLNEWVALUATION = 8;
		const int CNSTGRIDCOLNAME = 9;

		public void Init(ref int lngCurYear, ref int lngPrevYear, bool modalDialog)
		{
			if (frmNewValuationAnalysis.InstancePtr.Grid.Rows < 2)
			{
				MessageBox.Show("There are no entries to put in the report", "No Entries", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			txtRange.Text = FCConvert.ToString(lngPrevYear) + " to " + FCConvert.ToString(lngCurYear);
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngCurRow >= frmNewValuationAnalysis.InstancePtr.Grid.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtmuniname.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtdate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngCurRow = 1;
			lngCount = 0;
			dblTotalPrevious = 0;
			dblTotalCurrent = 0;
			dblTotalNew = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngTemp = 0;
			if (lngCurRow < frmNewValuationAnalysis.InstancePtr.Grid.Rows)
			{
				lngCount += 1;
				txtAccount.Text = frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLACCOUNT);
				txtName.Text = frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLNAME);
				txtMapLot.Text = frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLMAPLOT);
				txtNewValuation.Text = frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLNEWVALUATION);
				if (Conversion.Val(txtNewValuation.Text) > 0)
				{
					lngTemp = FCConvert.ToInt32(txtNewValuation.Text);
					dblTotalNew += lngTemp;
				}
				else if (Conversion.Val(txtNewValuation.Text) < 0)
				{
					lngTemp = lngTemp;
				}
				// will have to add up the values
				lngTemp = 0;
				if (Conversion.Val(frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLPREVLAND)) > 0)
				{
					lngTemp += FCConvert.ToInt32(FCConvert.ToDouble(frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLPREVLAND)));
				}
				if (Conversion.Val(frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLPREVBLDG)) > 0)
				{
					lngTemp += FCConvert.ToInt32(FCConvert.ToDouble(frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLPREVBLDG)));
				}
				txtPrevValuation.Text = Strings.Format(lngTemp, "#,###,###,###,##0");
				dblTotalPrevious += lngTemp;
				lngTemp = 0;
				if (Conversion.Val(frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLCURLAND)) > 0)
				{
					lngTemp += FCConvert.ToInt32(FCConvert.ToDouble(frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLCURLAND)));
				}
				if (Conversion.Val(frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLCURBLDG)) > 0)
				{
					lngTemp += FCConvert.ToInt32(FCConvert.ToDouble(frmNewValuationAnalysis.InstancePtr.Grid.TextMatrix(lngCurRow, CNSTGRIDCOLCURBLDG)));
				}
				txtCurrValuation.Text = Strings.Format(lngTemp, "#,###,###,###,##0");
				dblTotalCurrent += lngTemp;
				lngCurRow += 1;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtpage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalCurr.Text = Strings.Format(dblTotalCurrent, "#,###,###,###,##0");
			txtTotalPrev.Text = Strings.Format(dblTotalPrevious, "#,###,###,###,##0");
			txtTotalNew.Text = Strings.Format(dblTotalNew, "#,###,###,###,##0");
			txtCount.Text = Strings.Format(lngCount, "#,###,###,###,##0");
		}

		
	}
}
