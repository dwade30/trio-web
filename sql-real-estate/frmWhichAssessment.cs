﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation.Extensions;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmWhichAssessment.
	/// </summary>
	public partial class frmWhichAssessment : BaseForm
	{
		public frmWhichAssessment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmWhichAssessment InstancePtr
		{
			get
			{
				return (frmWhichAssessment)Sys.GetInstance(typeof(frmWhichAssessment));
			}
		}

		protected frmWhichAssessment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdOkay_Click(object sender, System.EventArgs e)
		{
			if (cmbWhich.Text == "Billing")
			{
				modGlobalVariables.Statics.boolBillingorCorrelated = true;
			}
			else
			{
				modGlobalVariables.Statics.boolBillingorCorrelated = false;
			}
			this.Unload();
		}

		private void frmWhichAssessment_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmWhichAssessment properties;
			//frmWhichAssessment.ScaleWidth	= 5880;
			//frmWhichAssessment.ScaleHeight	= 4155;
			//frmWhichAssessment.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
		}
	}
}
