﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class modMDIParent
	{
		
		public static bool FormExist(Form FormName)
		{
			bool FormExist = false;
			//Form frm = null;
			foreach (Form frm in Application.OpenForms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}

		public static void EnableDisableMenuOption(bool boolState, int intRow)
		{
			//// If Not boolState Then
			//// MDIParent.Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow * 2, 1, intRow * 2, 1) = TRIOCOLORDISABLEDOPTION
			//// Else
			//// MDIParent.Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow * 2, 1, intRow * 2, 1) = TRIOCOLORBLACK
			//// End If
			//if (!boolState)
			//{
			//	MDIParent.InstancePtr.GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, 1, intRow, 1, modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//	MDIParent.InstancePtr.GRID.RowData(intRow, false);
			//}
			//else
			//{
			//	MDIParent.InstancePtr.GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, 1, intRow, 1, modGlobalConstants.Statics.TRIOCOLORBLACK);
			//}
		}
	}
}
