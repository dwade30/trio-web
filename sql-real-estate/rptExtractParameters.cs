﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptExtractParameters.
	/// </summary>
	public partial class rptExtractParameters : BaseSectionReport
	{
		public static rptExtractParameters InstancePtr
		{
			get
			{
				return (rptExtractParameters)Sys.GetInstance(typeof(rptExtractParameters));
			}
		}

		protected rptExtractParameters _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsReport?.Dispose();
				clsCodes?.Dispose();
                clsReport = null;
                clsCodes = null;
            }
			base.Dispose(disposing);
		}

		public rptExtractParameters()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}
		// nObj = 1
		//   0	rptExtractParameters	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsReport = new clsDRWrapper();
		clsDRWrapper clsCodes = new clsDRWrapper();

		public void Init(ref int lngReportNumber, bool modalDialog)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                // get the title first
                lblTitle.Text = "Extract Report " + FCConvert.ToString(lngReportNumber);
                clsLoad.OpenRecordset(
                    "select * from reporttitles where reportnumber = " + FCConvert.ToString(lngReportNumber),
                    "twre0000.vb1");
                if (!clsLoad.EndOfFile())
                {
                    txtTitle.Text = FCConvert.ToString(clsLoad.Get_Fields_String("title"));
                }
                else
                {
                    txtTitle.Text = "";
                }

                // now get the whole report to print
                clsReport.OpenRecordset(
                    "select * from reporttable where reportnumber = " + FCConvert.ToString(lngReportNumber) +
                    " order by linenumber", "twre0000.vb1");
                if (clsReport.EndOfFile())
                {
                    MessageBox.Show(
                        "No report data found" + "\r\n" +
                        "If you have just created the report, try saving and running the report again", "No Data",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                    return;
                }

                clsCodes.OpenRecordset("select * from reportcodes order by code", "twre0000.vb1");
            }

            frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsReport.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				txtCode.Text = FCConvert.ToString(clsReport.Get_Fields("code"));
				if (Strings.UCase(FCConvert.ToString(clsReport.Get_Fields_String("TorP"))) == "T")
				{
					txtTotalPrint.Text = "Total";
				}
				else if (Strings.UCase(FCConvert.ToString(clsReport.Get_Fields_String("TorP"))) == "P")
				{
					txtTotalPrint.Text = "Print";
				}
				else
				{
					txtTotalPrint.Text = "Neither";
				}
				if (Strings.UCase(FCConvert.ToString(clsReport.Get_Fields_String("IncOrExc"))) == "I")
				{
					txtIncludeExclude.Text = "Include";
				}
				else if (Strings.UCase(FCConvert.ToString(clsReport.Get_Fields_String("IncOrExc"))) == "E")
				{
					txtIncludeExclude.Text = "Exclude";
				}
				else
				{
					txtIncludeExclude.Text = "Neither";
				}
				txtMin.Text = "";
				txtMax.Text = "";
				if (Strings.UCase(FCConvert.ToString(clsReport.Get_Fields_String("IncOrExc"))) == "I" || Strings.UCase(FCConvert.ToString(clsReport.Get_Fields_String("IncOrExc"))) == "E")
				{
					txtMin.Text = FCConvert.ToString(clsReport.Get_Fields_String("Min"));
					txtMax.Text = FCConvert.ToString(clsReport.Get_Fields_String("Max"));
				}
				// If clsCodes.FindFirstRecord("Code", clsReport.Fields("code")) Then
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				if (clsCodes.FindFirst("code = " + clsReport.Get_Fields("code")))
				{
					txtDescription.Text = FCConvert.ToString(clsCodes.Get_Fields_String("Description"));
				}
				else
				{
					txtDescription.Text = "";
				}
				clsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
