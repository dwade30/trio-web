﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class clsBuilding
	{
		//=========================================================
		public int ClientID;
		public int Account;
		public string Description = "";
		public int Value;
		public string strType = "";
		public int YearBuilt;
		public int Width;
		public double Stories;
		public int Length;
		public int Area;
		public int Rooms;
		public int Bedrooms;
		public int FullBaths;
		public int HalfBaths;
		public int Fireplaces;
		public int YearRemodeled;
		public int NumCars;
	}
}
