﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLandSchedule.
	/// </summary>
	partial class frmLandSchedule : BaseForm
	{
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCTextBox txtSchedule;
		public FCGrid GridStandard;
		public FCGrid Grid;
		public FCGrid gridTownCode;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblSchedule;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLandSchedule));
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.txtSchedule = new fecherFoundation.FCTextBox();
            this.GridStandard = new fecherFoundation.FCGrid();
            this.Grid = new fecherFoundation.FCGrid();
            this.gridTownCode = new fecherFoundation.FCGrid();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblSchedule = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAddSchedule = new fecherFoundation.FCButton();
            this.cmdDeleteSchedule = new fecherFoundation.FCButton();
            this.cmdPrevious = new fecherFoundation.FCButton();
            this.cmdNext = new fecherFoundation.FCButton();
            this.cmdPrintPreviewCurrent = new fecherFoundation.FCButton();
            this.cmdPrintPreviewAll = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridStandard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrevious)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreviewCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreviewAll)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 694);
            this.BottomPanel.Size = new System.Drawing.Size(898, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.cmdSelect);
            this.ClientArea.Controls.Add(this.txtSchedule);
            this.ClientArea.Controls.Add(this.GridStandard);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.gridTownCode);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblSchedule);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(918, 644);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSchedule, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.gridTownCode, 0);
            this.ClientArea.Controls.SetChildIndex(this.Grid, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridStandard, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtSchedule, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdSelect, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddSchedule);
            this.TopPanel.Controls.Add(this.cmdDeleteSchedule);
            this.TopPanel.Controls.Add(this.cmdPrevious);
            this.TopPanel.Controls.Add(this.cmdNext);
            this.TopPanel.Controls.Add(this.cmdPrintPreviewCurrent);
            this.TopPanel.Controls.Add(this.cmdPrintPreviewAll);
            this.TopPanel.Size = new System.Drawing.Size(918, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreviewAll, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreviewCurrent, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNext, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrevious, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteSchedule, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddSchedule, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(189, 30);
            this.HeaderText.Text = "Land Schedules";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(154, 90);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(372, 40);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // cmdSelect
            // 
            this.cmdSelect.AppearanceKey = "actionButton";
            this.cmdSelect.Location = new System.Drawing.Point(232, 30);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Size = new System.Drawing.Size(74, 40);
            this.cmdSelect.TabIndex = 1;
            this.cmdSelect.Text = "Select";
            this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
            // 
            // txtSchedule
            // 
            this.txtSchedule.BackColor = System.Drawing.SystemColors.Window;
            this.txtSchedule.Location = new System.Drawing.Point(154, 30);
            this.txtSchedule.Name = "txtSchedule";
            this.txtSchedule.Size = new System.Drawing.Size(58, 40);
            // 
            // GridStandard
            // 
            this.GridStandard.Cols = 3;
            this.GridStandard.ColumnHeadersVisible = false;
            this.GridStandard.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridStandard.ExtendLastCol = true;
            this.GridStandard.FixedRows = 0;
            this.GridStandard.Location = new System.Drawing.Point(30, 570);
            this.GridStandard.Name = "GridStandard";
            this.GridStandard.ReadOnly = false;
            this.GridStandard.Rows = 3;
            this.GridStandard.Size = new System.Drawing.Size(600, 124);
            this.GridStandard.TabIndex = 5;
            this.GridStandard.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridStandard_CellChanged);
            this.GridStandard.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridStandard_ValidateEdit);
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 9;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 3;
            this.Grid.Location = new System.Drawing.Point(30, 150);
            this.Grid.Name = "Grid";
            this.Grid.ReadOnly = false;
            this.Grid.Rows = 1;
            this.Grid.Size = new System.Drawing.Size(868, 400);
            this.Grid.TabIndex = 4;
            this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
            // 
            // gridTownCode
            // 
            this.gridTownCode.Cols = 1;
            this.gridTownCode.ColumnHeadersVisible = false;
            this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTownCode.ExtendLastCol = true;
            this.gridTownCode.FixedCols = 0;
            this.gridTownCode.FixedRows = 0;
            this.gridTownCode.Location = new System.Drawing.Point(326, 30);
            this.gridTownCode.Name = "gridTownCode";
            this.gridTownCode.ReadOnly = false;
            this.gridTownCode.RowHeadersVisible = false;
            this.gridTownCode.Rows = 1;
            this.gridTownCode.Size = new System.Drawing.Size(200, 40);
            this.gridTownCode.TabIndex = 2;
            this.gridTownCode.Visible = false;
            this.gridTownCode.ComboCloseUp += new System.EventHandler(this.gridTownCode_ComboCloseUp);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(80, 17);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "DESCRIPTION";
            // 
            // lblSchedule
            // 
            this.lblSchedule.Location = new System.Drawing.Point(103, 44);
            this.lblSchedule.Name = "lblSchedule";
            this.lblSchedule.Size = new System.Drawing.Size(33, 18);
            this.lblSchedule.TabIndex = 7;
            this.lblSchedule.Text = "1";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(66, 18);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "SCHEDULE";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(422, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAddSchedule
            // 
            this.cmdAddSchedule.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddSchedule.Location = new System.Drawing.Point(788, 29);
            this.cmdAddSchedule.Name = "cmdAddSchedule";
            this.cmdAddSchedule.Size = new System.Drawing.Size(102, 24);
            this.cmdAddSchedule.TabIndex = 1;
            this.cmdAddSchedule.Text = "Add Schedule";
            this.cmdAddSchedule.Click += new System.EventHandler(this.mnuAddSchedule_Click);
            // 
            // cmdDeleteSchedule
            // 
            this.cmdDeleteSchedule.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteSchedule.Location = new System.Drawing.Point(664, 29);
            this.cmdDeleteSchedule.Name = "cmdDeleteSchedule";
            this.cmdDeleteSchedule.Size = new System.Drawing.Size(118, 24);
            this.cmdDeleteSchedule.TabIndex = 2;
            this.cmdDeleteSchedule.Text = "Delete Schedule";
            this.cmdDeleteSchedule.Click += new System.EventHandler(this.mnuDeleteSchedule_Click);
            // 
            // cmdPrevious
            // 
            this.cmdPrevious.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrevious.Location = new System.Drawing.Point(589, 29);
            this.cmdPrevious.Name = "cmdPrevious";
            this.cmdPrevious.Shortcut = Wisej.Web.Shortcut.F7;
            this.cmdPrevious.Size = new System.Drawing.Size(69, 24);
            this.cmdPrevious.TabIndex = 3;
            this.cmdPrevious.Text = "Previous";
            this.cmdPrevious.Click += new System.EventHandler(this.mnuPrevious_Click);
            // 
            // cmdNext
            // 
            this.cmdNext.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNext.Location = new System.Drawing.Point(537, 29);
            this.cmdNext.Name = "cmdNext";
            this.cmdNext.Shortcut = Wisej.Web.Shortcut.F8;
            this.cmdNext.Size = new System.Drawing.Size(46, 24);
            this.cmdNext.TabIndex = 4;
            this.cmdNext.Text = "Next";
            this.cmdNext.Click += new System.EventHandler(this.mnuNext_Click);
            // 
            // cmdPrintPreviewCurrent
            // 
            this.cmdPrintPreviewCurrent.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreviewCurrent.Location = new System.Drawing.Point(379, 29);
            this.cmdPrintPreviewCurrent.Name = "cmdPrintPreviewCurrent";
            this.cmdPrintPreviewCurrent.Size = new System.Drawing.Size(152, 24);
            this.cmdPrintPreviewCurrent.TabIndex = 5;
            this.cmdPrintPreviewCurrent.Text = "Print Preview (Current)";
            this.cmdPrintPreviewCurrent.Click += new System.EventHandler(this.mnuPrintPreviewCurrent_Click);
            // 
            // cmdPrintPreviewAll
            // 
            this.cmdPrintPreviewAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreviewAll.Location = new System.Drawing.Point(249, 29);
            this.cmdPrintPreviewAll.Name = "cmdPrintPreviewAll";
            this.cmdPrintPreviewAll.Size = new System.Drawing.Size(124, 24);
            this.cmdPrintPreviewAll.TabIndex = 6;
            this.cmdPrintPreviewAll.Text = "Print Preview (All)";
            this.cmdPrintPreviewAll.Click += new System.EventHandler(this.mnuPrintPreviewAll_Click);
            // 
            // frmLandSchedule
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(918, 704);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmLandSchedule";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Land Schedules";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmLandSchedule_Load);
            this.Resize += new System.EventHandler(this.frmLandSchedule_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLandSchedule_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridStandard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrevious)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreviewCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreviewAll)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdAddSchedule;
		private FCButton cmdDeleteSchedule;
		private FCButton cmdPrevious;
		private FCButton cmdNext;
		private FCButton cmdPrintPreviewCurrent;
		private FCButton cmdPrintPreviewAll;
	}
}
