﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptExtractReport.
	/// </summary>
	partial class rptExtractReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptExtractReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupTitles = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDatePrinted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtlocnum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaddr3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtstate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtlocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtmaplot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaddr1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtaddr2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtzip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGroup = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupTitles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePrinted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtlocnum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaddr3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtstate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtlocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmaplot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaddr1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaddr2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtzip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.ColumnDirection = GrapeCity.ActiveReports.SectionReportModel.ColumnDirection.AcrossDown;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAmount,
				this.txtDesc
			});
			this.Detail.Height = 0.19F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1
			});
			this.ReportFooter.Height = 0.125F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.Label1,
				this.Label2,
				this.Label3,
				this.txtDate,
				this.txtGroupTitles,
				this.txtMuniName,
				this.txtTime,
				this.txtDatePrinted,
				this.txtPage
			});
			this.PageHeader.Height = 0.75F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtlocnum,
				this.txtaddr3,
				this.txtstate,
				this.txtAccount,
				this.txtName,
				this.txtCard,
				this.txtlocation,
				this.txtmaplot,
				this.txtaddr1,
				this.txtaddr2,
				this.txtzip,
				this.Label7,
				this.txtGroup,
				this.Field7,
				this.txtAmount1,
				this.txtDesc1,
				this.txtAmount2,
				this.txtDesc2,
				this.txtAmount3,
				this.txtDesc3,
				this.txtAmount4,
				this.txtDesc4,
				this.txtAmount5,
				this.txtDesc5
			});
			this.GroupHeader1.DataField = "thebinder";
			this.GroupHeader1.Height = 1.25F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.21875F;
			this.txtTitle.Left = 1.5625F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.txtTitle.Text = "Extract Report";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 4.1875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.MultiLine = false;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.Label1.Text = "Account";
			this.Label1.Top = 0.5625F;
			this.Label1.Width = 0.6875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.6458333F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.Label2.Text = "Card";
			this.Label2.Top = 0.5625F;
			this.Label2.Width = 0.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.041667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.Label3.Text = "Name/Loc/MapLot/Address";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 2.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 2F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 0";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0.21875F;
			this.txtDate.Width = 3F;
			// 
			// txtGroupTitles
			// 
			this.txtGroupTitles.CanShrink = true;
			this.txtGroupTitles.Height = 0.19F;
			this.txtGroupTitles.Left = 1.5625F;
			this.txtGroupTitles.Name = "txtGroupTitles";
			this.txtGroupTitles.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtGroupTitles.Text = null;
			this.txtGroupTitles.Top = 0.40625F;
			this.txtGroupTitles.Width = 4.1875F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.19F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.txtMuniName.Text = "Field1";
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.5F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.19F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.txtTime.Text = "Field1";
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.5F;
			// 
			// txtDatePrinted
			// 
			this.txtDatePrinted.Height = 0.19F;
			this.txtDatePrinted.Left = 6F;
			this.txtDatePrinted.Name = "txtDatePrinted";
			this.txtDatePrinted.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtDatePrinted.Text = "Field1";
			this.txtDatePrinted.Top = 0F;
			this.txtDatePrinted.Width = 1.5F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.19F;
			this.txtPage.Left = 6F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtPage.Text = "Field1";
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 1.5F;
			// 
			// txtlocnum
			// 
			this.txtlocnum.CanGrow = false;
			this.txtlocnum.Height = 0.19F;
			this.txtlocnum.Left = 1F;
			this.txtlocnum.MultiLine = false;
			this.txtlocnum.Name = "txtlocnum";
			this.txtlocnum.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtlocnum.Text = null;
			this.txtlocnum.Top = 0.25F;
			this.txtlocnum.Width = 0.5625F;
			// 
			// txtaddr3
			// 
			this.txtaddr3.CanGrow = false;
			this.txtaddr3.Height = 0.19F;
			this.txtaddr3.Left = 1F;
			this.txtaddr3.MultiLine = false;
			this.txtaddr3.Name = "txtaddr3";
			this.txtaddr3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtaddr3.Text = "Field3";
			this.txtaddr3.Top = 1.083333F;
			this.txtaddr3.Width = 1.3125F;
			// 
			// txtstate
			// 
			this.txtstate.CanGrow = false;
			this.txtstate.Height = 0.19F;
			this.txtstate.Left = 2.375F;
			this.txtstate.MultiLine = false;
			this.txtstate.Name = "txtstate";
			this.txtstate.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtstate.Text = "Field3";
			this.txtstate.Top = 1.083333F;
			this.txtstate.Width = 0.4375F;
			// 
			// txtAccount
			// 
			this.txtAccount.CanGrow = false;
			this.txtAccount.Height = 0.19F;
			this.txtAccount.Left = 0F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtAccount.Text = "Field1";
			this.txtAccount.Top = 0.08333334F;
			this.txtAccount.Width = 0.625F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.19F;
			this.txtName.Left = 1F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0.08333334F;
			this.txtName.Width = 2.416667F;
			// 
			// txtCard
			// 
			this.txtCard.CanGrow = false;
			this.txtCard.Height = 0.19F;
			this.txtCard.Left = 0.6458333F;
			this.txtCard.MultiLine = false;
			this.txtCard.Name = "txtCard";
			this.txtCard.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtCard.Text = "Field1";
			this.txtCard.Top = 0.08333334F;
			this.txtCard.Width = 0.25F;
			// 
			// txtlocation
			// 
			this.txtlocation.CanGrow = false;
			this.txtlocation.Height = 0.19F;
			this.txtlocation.Left = 1.625F;
			this.txtlocation.MultiLine = false;
			this.txtlocation.Name = "txtlocation";
			this.txtlocation.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtlocation.Text = "Field3";
			this.txtlocation.Top = 0.25F;
			this.txtlocation.Width = 1.791667F;
			// 
			// txtmaplot
			// 
			this.txtmaplot.CanGrow = false;
			this.txtmaplot.Height = 0.19F;
			this.txtmaplot.Left = 1F;
			this.txtmaplot.MultiLine = false;
			this.txtmaplot.Name = "txtmaplot";
			this.txtmaplot.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtmaplot.Text = "Field3";
			this.txtmaplot.Top = 0.4166667F;
			this.txtmaplot.Width = 2.416667F;
			// 
			// txtaddr1
			// 
			this.txtaddr1.CanGrow = false;
			this.txtaddr1.Height = 0.19F;
			this.txtaddr1.Left = 1F;
			this.txtaddr1.MultiLine = false;
			this.txtaddr1.Name = "txtaddr1";
			this.txtaddr1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtaddr1.Text = "Field3";
			this.txtaddr1.Top = 0.75F;
			this.txtaddr1.Width = 2.416667F;
			// 
			// txtaddr2
			// 
			this.txtaddr2.CanGrow = false;
			this.txtaddr2.Height = 0.19F;
			this.txtaddr2.Left = 1F;
			this.txtaddr2.MultiLine = false;
			this.txtaddr2.Name = "txtaddr2";
			this.txtaddr2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtaddr2.Text = "Field3";
			this.txtaddr2.Top = 0.9166667F;
			this.txtaddr2.Width = 2.416667F;
			// 
			// txtzip
			// 
			this.txtzip.CanGrow = false;
			this.txtzip.Height = 0.19F;
			this.txtzip.Left = 2.8125F;
			this.txtzip.MultiLine = false;
			this.txtzip.Name = "txtzip";
			this.txtzip.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtzip.Text = "Field3";
			this.txtzip.Top = 1.083333F;
			this.txtzip.Width = 0.6041667F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.416667F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 0";
			this.Label7.Text = "---------- Group";
			this.Label7.Top = 0.08333334F;
			this.Label7.Width = 1.416667F;
			// 
			// txtGroup
			// 
			this.txtGroup.CanGrow = false;
			this.txtGroup.Height = 0.19F;
			this.txtGroup.Left = 5.916667F;
			this.txtGroup.MultiLine = false;
			this.txtGroup.Name = "txtGroup";
			this.txtGroup.Style = "font-family: \'Courier New\'; text-align: center; ddo-char-set: 0";
			this.txtGroup.Text = "Field3";
			this.txtGroup.Top = 0.08333334F;
			this.txtGroup.Width = 0.3333333F;
			// 
			// Field7
			// 
			this.Field7.CanGrow = false;
			this.Field7.Height = 0.19F;
			this.Field7.Left = 6.333333F;
			this.Field7.MultiLine = false;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Courier New\'; ddo-char-set: 0";
			this.Field7.Text = "--------";
			this.Field7.Top = 0.08333334F;
			this.Field7.Width = 1.083333F;
			// 
			// txtAmount1
			// 
			this.txtAmount1.Height = 0.19F;
			this.txtAmount1.Left = 3.5F;
			this.txtAmount1.Name = "txtAmount1";
			this.txtAmount1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAmount1.Text = null;
			this.txtAmount1.Top = 0.4166667F;
			this.txtAmount1.Width = 1.791667F;
			// 
			// txtDesc1
			// 
			this.txtDesc1.Height = 0.19F;
			this.txtDesc1.Left = 5.333333F;
			this.txtDesc1.Name = "txtDesc1";
			this.txtDesc1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtDesc1.Text = null;
			this.txtDesc1.Top = 0.4166667F;
			this.txtDesc1.Width = 2.083333F;
			// 
			// txtAmount2
			// 
			this.txtAmount2.Height = 0.19F;
			this.txtAmount2.Left = 3.5F;
			this.txtAmount2.Name = "txtAmount2";
			this.txtAmount2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAmount2.Text = null;
			this.txtAmount2.Top = 0.5833333F;
			this.txtAmount2.Width = 1.791667F;
			// 
			// txtDesc2
			// 
			this.txtDesc2.Height = 0.19F;
			this.txtDesc2.Left = 5.333333F;
			this.txtDesc2.Name = "txtDesc2";
			this.txtDesc2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtDesc2.Text = null;
			this.txtDesc2.Top = 0.5833333F;
			this.txtDesc2.Width = 2.083333F;
			// 
			// txtAmount3
			// 
			this.txtAmount3.Height = 0.19F;
			this.txtAmount3.Left = 3.5F;
			this.txtAmount3.Name = "txtAmount3";
			this.txtAmount3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAmount3.Text = null;
			this.txtAmount3.Top = 0.75F;
			this.txtAmount3.Width = 1.791667F;
			// 
			// txtDesc3
			// 
			this.txtDesc3.Height = 0.19F;
			this.txtDesc3.Left = 5.333333F;
			this.txtDesc3.Name = "txtDesc3";
			this.txtDesc3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtDesc3.Text = null;
			this.txtDesc3.Top = 0.75F;
			this.txtDesc3.Width = 2.083333F;
			// 
			// txtAmount4
			// 
			this.txtAmount4.Height = 0.19F;
			this.txtAmount4.Left = 3.5F;
			this.txtAmount4.Name = "txtAmount4";
			this.txtAmount4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAmount4.Text = null;
			this.txtAmount4.Top = 0.9166667F;
			this.txtAmount4.Width = 1.791667F;
			// 
			// txtDesc4
			// 
			this.txtDesc4.Height = 0.19F;
			this.txtDesc4.Left = 5.333333F;
			this.txtDesc4.Name = "txtDesc4";
			this.txtDesc4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtDesc4.Text = null;
			this.txtDesc4.Top = 0.9166667F;
			this.txtDesc4.Width = 2.083333F;
			// 
			// txtAmount5
			// 
			this.txtAmount5.Height = 0.19F;
			this.txtAmount5.Left = 3.5F;
			this.txtAmount5.Name = "txtAmount5";
			this.txtAmount5.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAmount5.Text = null;
			this.txtAmount5.Top = 1.083333F;
			this.txtAmount5.Width = 1.791667F;
			// 
			// txtDesc5
			// 
			this.txtDesc5.Height = 0.19F;
			this.txtDesc5.Left = 5.333333F;
			this.txtDesc5.Name = "txtDesc5";
			this.txtDesc5.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtDesc5.Text = null;
			this.txtDesc5.Top = 1.083333F;
			this.txtDesc5.Width = 2.083333F;
			// 
			// txtAmount
			// 
			this.txtAmount.CanShrink = true;
			this.txtAmount.Height = 0.19F;
			this.txtAmount.Left = 3.5F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtAmount.Text = "thebinder";
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 1.791667F;
			// 
			// txtDesc
			// 
			this.txtDesc.CanGrow = false;
			this.txtDesc.Height = 0.19F;
			this.txtDesc.Left = 5.333333F;
			this.txtDesc.MultiLine = false;
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtDesc.Text = "Field2";
			this.txtDesc.Top = 0F;
			this.txtDesc.Width = 2.083333F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.0625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.03125F;
			this.SubReport1.Width = 7.5F;
			// 
			// rptExtractReport
			//
			// 
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupTitles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDatePrinted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtlocnum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaddr3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtstate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtlocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmaplot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaddr1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtaddr2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtzip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupTitles;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDatePrinted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtlocnum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaddr3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtstate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtlocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmaplot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaddr1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaddr2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtzip;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroup;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc5;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
