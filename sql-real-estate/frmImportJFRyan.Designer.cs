﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmImportJFRyan.
	/// </summary>
	partial class frmImportJFRyan : BaseForm
	{
		public fecherFoundation.FCPanel framProgress;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblProgress;
		public fecherFoundation.FCFrame framFile;
		public fecherFoundation.FCButton btnBrowse;
		public fecherFoundation.FCTextBox txtFilename;
		public fecherFoundation.FCButton btnImport;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImportJFRyan));
			this.framProgress = new fecherFoundation.FCPanel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.lblProgress = new fecherFoundation.FCLabel();
			this.framFile = new fecherFoundation.FCFrame();
			this.btnBrowse = new fecherFoundation.FCButton();
			this.txtFilename = new fecherFoundation.FCTextBox();
			this.btnImport = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framProgress)).BeginInit();
			this.framProgress.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framFile)).BeginInit();
			this.framFile.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnImport)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 442);
			this.BottomPanel.Size = new System.Drawing.Size(755, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.framProgress);
			this.ClientArea.Controls.Add(this.framFile);
			this.ClientArea.Controls.Add(this.btnImport);
			this.ClientArea.Size = new System.Drawing.Size(755, 382);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(755, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(223, 30);
			this.HeaderText.Text = "Importing Accounts";
			// 
			// framProgress
			// 
			this.framProgress.Controls.Add(this.lblAccount);
			this.framProgress.Controls.Add(this.lblProgress);
			this.framProgress.Location = new System.Drawing.Point(30, 150);
			this.framProgress.Name = "framProgress";
			this.framProgress.Size = new System.Drawing.Size(380, 70);
			this.framProgress.TabIndex = 5;
			this.framProgress.Visible = false;
			// 
			// lblAccount
			// 
			this.lblAccount.Location = new System.Drawing.Point(161, 7);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(81, 21);
			this.lblAccount.TabIndex = 7;
			// 
			// lblProgress
			// 
			this.lblProgress.AutoSize = true;
			this.lblProgress.Location = new System.Drawing.Point(163, 34);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(78, 15);
			this.lblProgress.TabIndex = 6;
			this.lblProgress.Text = "IMPORTING";
			// 
			// framFile
			// 
			this.framFile.Controls.Add(this.btnBrowse);
			this.framFile.Controls.Add(this.txtFilename);
			this.framFile.Location = new System.Drawing.Point(30, 30);
			this.framFile.Name = "framFile";
			this.framFile.Size = new System.Drawing.Size(450, 90);
			this.framFile.TabIndex = 2;
			this.framFile.Text = "Import File";
			// 
			// btnBrowse
			// 
			this.btnBrowse.AppearanceKey = "actionButton";
			this.btnBrowse.Location = new System.Drawing.Point(360, 30);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.Size = new System.Drawing.Size(70, 40);
			this.btnBrowse.TabIndex = 4;
			this.btnBrowse.Text = "Browse";
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			// 
			// txtFilename
			// 
			this.txtFilename.AutoSize = false;
			this.txtFilename.BackColor = System.Drawing.SystemColors.Window;
			this.txtFilename.LinkItem = null;
			this.txtFilename.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFilename.LinkTopic = null;
			this.txtFilename.Location = new System.Drawing.Point(20, 30);
			this.txtFilename.Name = "txtFilename";
			this.txtFilename.Size = new System.Drawing.Size(320, 40);
			this.txtFilename.TabIndex = 3;
			// 
			// btnImport
			// 
			this.btnImport.AppearanceKey = "acceptButton";
			this.btnImport.Enabled = false;
			this.btnImport.Location = new System.Drawing.Point(30, 240);
			this.btnImport.Name = "btnImport";
			this.btnImport.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnImport.Size = new System.Drawing.Size(100, 48);
			this.btnImport.TabIndex = 0;
			this.btnImport.Text = "Import";
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			// 
			// frmImportJFRyan
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(755, 550);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmImportJFRyan";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Import Accounts";
			this.Load += new System.EventHandler(this.frmImportJFRyan_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmImportJFRyan_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framProgress)).EndInit();
			this.framProgress.ResumeLayout(false);
			this.framProgress.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framFile)).EndInit();
			this.framFile.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnImport)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
