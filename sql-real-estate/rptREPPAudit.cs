﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptREPPAudit.
	/// </summary>
	public partial class rptREPPAudit : BaseSectionReport
	{
		public static rptREPPAudit InstancePtr
		{
			get
			{
				return (rptREPPAudit)Sys.GetInstance(typeof(rptREPPAudit));
			}
		}

		protected rptREPPAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptREPPAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:MSH - i.issue #1074: restore missed report name
			this.Name = "Real Estate & Personal Property Audit Summary";
		}
		// nObj = 1
		//   0	rptREPPAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intPage;
		int intTownNumber;

		public void Init(int intTownCode = 0)
		{
			intTownNumber = intTownCode;
			if (intTownNumber > 0)
			{
				txtTownName.Text = "For " + modRegionalTown.GetTownKeyName_2(intTownCode);
			}
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuniname.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			intPage = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper clsLoad = new clsDRWrapper())
            {
                string strSQL = "";
                double lngTotBillable;
                double lngHomestead;
                int lngTotCount;
                double lngREBillable;
                double lngPPBillable;
                // vbPorter upgrade warning: lngTETotCount As int	OnWriteFCConvert.ToDouble(
                int lngTETotCount;
                string strHomestead = "";
                string strTemp = "";
                double lngHalfHomestead;

                lngPPBillable = 0;
                lngHomestead = 0;
                if (intTownNumber == 0)
                {
                    strSQL =
                        "select count(rsaccount) as RECount, sum(sumland) as landsum, sum(sumbldg) as bldgsum, sum (sumexempt) as exemptsum from (select rsaccount , sum(lastlandval) as sumland,sum(lastbldgval) as sumbldg,sum(rlexemption) as SUMexempt from master where not rsdeleted = 1 group by rsaccount having (sum(lastlandval) + sum(lastbldgval)) = sum(rlexemption) and sum(rlexemption) > 0 ) as tbl";
                }
                else
                {
                    strSQL =
                        "select count(rsaccount) as RECount, sum(sumland) as landsum, sum(sumbldg) as bldgsum, sum (sumexempt) as exemptsum from (select rsaccount , sum(lastlandval) as sumland,sum(lastbldgval) as sumbldg,sum(rlexemption) as SUMexempt from master where not rsdeleted = 1 and ritrancode = " +
                        FCConvert.ToString(intTownNumber) +
                        " group by rsaccount having (sum(lastlandval) + sum(lastbldgval)) = sum(rlexemption) and sum(rlexemption) > 0 ) as tbl";
                }

                // strSQL = "select count(rsaccount) as RECount, sum (lastlandval) as landsum,sum(lastbldgval) as bldgsum, sum(rlexemption) as exemptsum from master where not rsdeleted = 1  and (lastlandval + lastbldgval) = rlexemption and rlexemption > 0"
                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                // TODO Get_Fields: Field [recount] not found!! (maybe it is an alias?)
                txtTERECount.Text = Strings.Format(clsLoad.Get_Fields("recount"), "#,###,##0");
                // TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
                txtTELand.Text = Strings.Format(clsLoad.Get_Fields("landsum"), "#,###,###,##0");
                // TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
                txtTEBuilding.Text = Strings.Format(clsLoad.Get_Fields("bldgsum"), "#,###,###,##0");
                // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
                txtTEREExemption.Text = Strings.Format(clsLoad.Get_Fields("exemptsum"), "#,###,###,##0");
                // TODO Get_Fields: Field [recount] not found!! (maybe it is an alias?)
                lngTETotCount = FCConvert.ToInt32(clsLoad.Get_Fields("recount"));
                if (intTownNumber == 0)
                {
                    strSQL =
                        "select sum(sumhome) as homesum, count(rsaccount) as thecount from (select SUM(homesteadvalue) as sumhome,rsaccount from master where not rsdeleted = 1 group by rsaccount having sum(rlexemption) > 0 and sum(rlexemption) = (sum(lastlandval) + sum(lastbldgval)) and sum(cast(homesteadvalue as bigint)) > 0) as tbl";
                }
                else
                {
                    strSQL =
                        "select sum(sumhome) as homesum, count(rsaccount) as thecount from (select SUM(homesteadvalue) as sumhome,rsaccount from master where not rsdeleted = 1  and ritrancode = " +
                        FCConvert.ToString(intTownNumber) +
                        " group by rsaccount having sum(rlexemption) > 0 and sum(rlexemption) = (sum(lastlandval) + sum(lastbldgval)) and sum(val(homesteadvalue & '')) > 0) as tbl";
                }

                // strSQL = "select SUM(homesteadvalue) as homesum,count(rsaccount) as thecount from master where not rsdeleted = 1 and rlexemption > 0 and rlexemption = (lastlandval + lastbldgval) and homesteadvalue > 0"
                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                if (!clsLoad.EndOfFile())
                {
                    // TODO Get_Fields: Field [homesum] not found!! (maybe it is an alias?)
                    txtTEHomestead.Text =
                        Strings.Format(Conversion.Val(clsLoad.Get_Fields("homesum")), "#,###,###,##0");
                    // TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
                    txtTEHomeCount.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("thecount")), "#,###,##0");
                    // TODO Get_Fields: Field [homesum] not found!! (maybe it is an alias?)
                    lngHomestead = Conversion.Val(clsLoad.Get_Fields("homesum"));
                }

                if (intTownNumber == 0)
                {
                    strSQL =
                        "select sum(sumhome) as homesum,count(rsaccount) as thecount from (select SUM(homesteadvalue) as sumhome,rsaccount  from master where not rsdeleted = 1 group by rsaccount having  sum(cast(homesteadvalue as bigint)) > 0 and (sum(cast(lastlandval as bigint)) + sum(cast(lastbldgval as bigint))) - sum(cast(rlexemption as bigint)) > 0) as tbl";
                }
                else
                {
                    strSQL =
                        "select sum(sumhome) as homesum,count(rsaccount) as thecount from (select SUM(homesteadvalue) as sumhome,rsaccount  from master where not rsdeleted = 1 and ritrancode = " +
                        FCConvert.ToString(intTownNumber) +
                        "  group by rsaccount having  sum(cast(homesteadvalue as bigint)) > 0 and (sum(cast(lastlandval as bigint)) + sum(cast(lastbldgval as bigint))) - sum(cast(rlexemption as bigint)) > 0) as tbl";
                }

                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                if (!clsLoad.EndOfFile())
                {
                    // TODO Get_Fields: Field [homesum] not found!! (maybe it is an alias?)
                    txtBHomestead.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("homesum")), "#,###,###,##0");
                    // TODO Get_Fields: Field [homesum] not found!! (maybe it is an alias?)
                    lngHomestead += Conversion.Val(clsLoad.Get_Fields("homesum"));
                    // TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
                    txtBHomeCount.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("thecount")), "#,###,##0");
                }

                if (intTownNumber == 0)
                {
                    strSQL =
                        "select count(rsaccount) as recount,sum(sumland) as landsum,sum(sumbldg) as bldgsum, sum(sumexempt) as exemptsum from (select rsaccount,sum(cast(lastlandval as bigint)) as sumland,sum(cast(lastbldgval as bigint)) as sumbldg,sum(cast(rlexemption as bigint)) as sumexempt from master where not rsdeleted = 1 group by rsaccount having sum(cast(rlexemption as bigint)) < (sum(cast(lastlandval as bigint)) + sum(cast(lastbldgval as bigint)))) as tbl";
                }
                else
                {
                    strSQL =
                        "select count(rsaccount) as recount,sum(sumland) as landsum,sum(sumbldg) as bldgsum, sum(sumexempt) as exemptsum from (select rsaccount,sum(cast(lastlandval as bigint)) as sumland,sum(cast(lastbldgval as bigint)) as sumbldg,sum(cast(rlexemption as bigint)) as sumexempt from master where not rsdeleted = 1 and ritrancode = " +
                        FCConvert.ToString(intTownNumber) +
                        " group by rsaccount having sum(cast(rlexemption as bigint)) < (sum(cast(lastlandval as bigint)) + sum(cast(lastbldgval as bigint)))) as tbl";
                }

                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                // TODO Get_Fields: Field [recount] not found!! (maybe it is an alias?)
                txtBRECount.Text = Strings.Format(clsLoad.Get_Fields("recount"), "#,###,##0");
                // TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
                txtBLand.Text = Strings.Format(clsLoad.Get_Fields("landsum"), "#,###,###,##0");
                // TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
                txtBBuilding.Text = Strings.Format(clsLoad.Get_Fields("bldgsum"), "#,###,###,##0");
                // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
                txtBREExemption.Text = Strings.Format(clsLoad.Get_Fields("exemptsum"), "#,###,###,##0");
                // TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
                // TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
                // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
                lngTotBillable = Conversion.Val(clsLoad.Get_Fields("landsum")) +
                                 Conversion.Val(clsLoad.Get_Fields("bldgsum")) -
                                 Conversion.Val(clsLoad.Get_Fields("exemptsum"));
                // TODO Get_Fields: Field [recount] not found!! (maybe it is an alias?)
                lngTotCount = FCConvert.ToInt32(clsLoad.Get_Fields("recount"));
                lngREBillable = lngTotBillable;
                if (intTownNumber == 0)
                {
                    strSQL =
                        "select count(rsaccount) as thecount from (select rsaccount from master where not rsdeleted = 1  group by rsaccount having  (sum(cast(rlexemption as bigint)) = 0 and sum(cast(lastlandval as bigint)) + sum(cast(lastbldgval as bigint)) = 0)) as tbl";
                }
                else
                {
                    strSQL =
                        "select count(rsaccount) as thecount from (select rsaccount from master where not rsdeleted = 1 and ritrancode = " +
                        FCConvert.ToString(intTownNumber) +
                        " group by rsaccount having  (sum(cast(rlexemption as bigint)) = 0 and sum(cast(lastlandval as bigint)) + sum(cast(lastbldgval as bigint)) = 0)) as tbl";
                }

                clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                if (!clsLoad.EndOfFile())
                {
                    // TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
                    txtBRENoValuation.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("thecount")), "#,##0");
                }
                else
                {
                    txtBRENoValuation.Text = "0";
                }

                if (clsLoad.DBExists("PersonalProperty"))
                {
                    // If File.Exists(strPPDatabase) Then
                    if (intTownNumber == 0)
                    {
                        strSQL =
                            "select count(account) as PPCount,sum([value]) as valsum,sum(cast(exemption as bigint)) as exemptsum from ppmaster where not deleted = 1 and exemption  > 0 and exemption  = [value]";
                    }
                    else
                    {
                        strSQL =
                            "select count(account) as PPCount,sum([value]) as valsum,sum(cast(exemption as bigint)) as exemptsum from ppmaster where not deleted = 1 and exemption  > 0 and exemption  = [value] and trancode = " +
                            FCConvert.ToString(intTownNumber);
                    }

                    clsLoad.OpenRecordset(strSQL, modGlobalVariables.strPPDatabase);
                    // TODO Get_Fields: Field [ppcount] not found!! (maybe it is an alias?)
                    txtTEPPCount.Text = Strings.Format(clsLoad.Get_Fields("ppcount"), "#,###,##0");
                    // TODO Get_Fields: Field [valsum] not found!! (maybe it is an alias?)
                    txtTEValue.Text = Strings.Format(clsLoad.Get_Fields("valsum"), "#,###,###,##0");
                    // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
                    txtTEPPExemption.Text = Strings.Format(clsLoad.Get_Fields("exemptsum"), "#,###,###,##0");
                    // TODO Get_Fields: Field [ppcount] not found!! (maybe it is an alias?)
                    lngTETotCount += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("ppcount")));
                    txtTETotCount.Text = Strings.Format(lngTETotCount, "#,###,###,##0");
                    if (intTownNumber == 0)
                    {
                        strSQL =
                            "select count(account) as ppcount,sum([value]) as valsum,sum(cast(exemption as bigint)) as exemptsum from ppmaster where not deleted = 1 and [value] > exemption ";
                    }
                    else
                    {
                        strSQL =
                            "select count(account) as ppcount,sum([value]) as valsum,sum(cast(exemption as bigint)) as exemptsum from ppmaster where not deleted = 1 and [value] > exemption  and trancode = " +
                            FCConvert.ToString(intTownNumber);
                    }

                    clsLoad.OpenRecordset(strSQL, modGlobalVariables.strPPDatabase);
                    // TODO Get_Fields: Field [ppcount] not found!! (maybe it is an alias?)
                    txtBPPCount.Text = Strings.Format(clsLoad.Get_Fields("ppcount"), "#,###,##0");
                    // TODO Get_Fields: Field [valsum] not found!! (maybe it is an alias?)
                    txtBValuation.Text = Strings.Format(clsLoad.Get_Fields("valsum"), "#,###,###,##0");
                    // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
                    txtBPPExemption.Text = Strings.Format(clsLoad.Get_Fields("exemptsum"), "#,###,###,##0");
                    // TODO Get_Fields: Field [valsum] not found!! (maybe it is an alias?)
                    // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
                    lngPPBillable = Conversion.Val(clsLoad.Get_Fields("valsum")) -
                                    Conversion.Val(clsLoad.Get_Fields("exemptsum"));
                    lngTotBillable += lngPPBillable;
                    // TODO Get_Fields: Field [ppcount] not found!! (maybe it is an alias?)
                    lngTotCount += clsLoad.Get_Fields("ppcount");
                    if (intTownNumber == 0)
                    {
                        strSQL =
                            "select count(account) as ppcount from ppmaster where not deleted = 1 and [value] = 0 and exemption  = 0";
                    }
                    else
                    {
                        strSQL =
                            "select count(account) as ppcount from ppmaster where not deleted = 1 and [value] = 0 and exemption  = 0 and trancode = " +
                            FCConvert.ToString(intTownNumber);
                    }

                    clsLoad.OpenRecordset(strSQL, modGlobalVariables.strPPDatabase);
                    if (!clsLoad.EndOfFile())
                    {
                        // TODO Get_Fields: Field [ppcount] not found!! (maybe it is an alias?)
                        txtBPPNoValuation.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("ppcount")), "#,##0");
                    }
                    else
                    {
                        txtBPPNoValuation.Text = "0";
                    }
                }

                lngHalfHomestead = modGlobalRoutines.Round(lngHomestead * .7, 0);
                txtTotBillable.Text = Strings.Format(lngTotBillable, "#,###,###,##0");
                txtTotCount.Text = Strings.Format(lngTotCount, "#,###,###,##0");
                txtTotREBillable.Text = Strings.Format(lngREBillable, "#,###,###,##0");
                txtTotPPBillable.Text = Strings.Format(lngPPBillable, "#,###,###,##0");
                txtTotHomestead.Text = Strings.Format(lngHomestead, "#,###,###,##0");
                txtHalfHomestead.Text = Strings.Format(lngHalfHomestead, "#,###,###,##0");
                txtTotTot.Text = Strings.Format(lngTotBillable + lngHalfHomestead, "#,###,###,##0");
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page  " + FCConvert.ToString(intPage);
			intPage += 1;
		}

		
	}
}
