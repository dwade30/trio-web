﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptCommercial.
	/// </summary>
	public partial class srptCommercial : FCSectionReport
	{
		public static srptCommercial InstancePtr
		{
			get
			{
				return (srptCommercial)Sys.GetInstance(typeof(srptCommercial));
			}
		}

		protected srptCommercial _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsComm?.Dispose();
				clsDesc?.Dispose();
                clsComm = null;
                clsDesc = null;
            }
			base.Dispose(disposing);
		}

		public srptCommercial()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptCommercial	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsDesc = new clsDRWrapper();
		clsDRWrapper clsComm = new clsDRWrapper();
		int lngTotal;
		int lngCount;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!clsComm.EndOfFile())
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// check to see if the query exists. if not, then create it
			
			string strSQL = "";
			// If Not QueryExists("comquery", strredatabase) Then
			if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
			{
				// strSQL = "    (select commercial.occ1 as occcode,commercial.rsaccount,commercial.rscard, summrecord.comval1 as comval from (select * from summrecord inner join commercial on (commercial.rsaccount = summrecord.srecordnumber) and (commercial.rscard = summrecord.cardnumber))) union (select commercial.occ2 as occcode,commercial.rsaccount,commercial.rscard,summrecord.comval2 as comval from (select * from summrecord inner join commercial on (commercial.rsaccount = summrecord.srecordnumber) and (commercial.rscard = summrecord.cardnumber)))"
				strSQL = "    (select occ1 as occcode,rsaccount,rscard, comval1 as comval from (select occ1,commercial.rsaccount,commercial.rscard, summrecord.comval1 from summrecord inner join commercial on (commercial.rsaccount = summrecord.srecordnumber) and (commercial.rscard = summrecord.cardnumber)) tbl1) union (select occ2 as occcode,rsaccount,rscard,comval2 as comval from (select occ2,commercial.rsaccount,commercial.rscard, summrecord.comval2 from summrecord inner join commercial on (commercial.rsaccount = summrecord.srecordnumber) and (commercial.rscard = summrecord.cardnumber)) tbl2)";
			}
			else
			{
				// strSQL = "   select * from (select * from  (select commercial.occ1 as occcode,commercial.rsaccount,commercial.rscard, summrecord.comval1 as comval from (select * from summrecord inner join commercial on (commercial.rsaccount = summrecord.srecordnumber) and (commercial.rscard = summrecord.cardnumber))) union (select commercial.occ2 as occcode,commercial.rsaccount,commercial.rscard,summrecord.comval2 as comval from (select * from summrecord inner join commercial on (commercial.rsaccount = summrecord.srecordnumber) and (commercial.rscard = summrecord.cardnumber)))) as qry1 inner join master on (qry1.rsaccount = master.rsaccount) and (qry1.rscard = master.rscard) where ritrancode = " & CustomizedInfo.CurrentTownNumber
				strSQL = "   select occcode,comval from (select * from  (select occ1 as occcode,rsaccount,rscard, comval1 as comval from (select occ1 ,rsaccount,rscard, comval1 from summrecord inner join commercial on (commercial.rsaccount = summrecord.srecordnumber) and (commercial.rscard = summrecord.cardnumber)) tbl1) tbl3 union (select occ2 as occcode,rsaccount,rscard,comval2 as comval from (select occ2 ,rsaccount,rscard,comval2 from summrecord inner join commercial on (commercial.rsaccount = summrecord.srecordnumber) and (commercial.rscard = summrecord.cardnumber)) tbl2)) as qry1 inner join master on (qry1.rsaccount = master.rsaccount) and (qry1.rscard = master.rscard) where ritrancode = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
			}
			// Call clsTemp.CreateStoredProcedure("comquery", strSQL, strREDatabase)
			// End If
			clsDesc.OpenRecordset("select * from commercialcost where crecordnumber < 159", modGlobalVariables.strREDatabase);
			clsComm.OpenRecordset("select occcode,count(occcode) as thecount, sum(cast(comval as bigint)) as thesum  from (" + strSQL + ") comquery group by occcode having occcode >= 0  order by occcode ", modGlobalVariables.strREDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsComm.EndOfFile())
			{
				// TODO Get_Fields: Field [occcode] not found!! (maybe it is an alias?)
				if (Conversion.Val(clsComm.Get_Fields("occcode")) == 0)
				{
					txtOccupancy.Text = "Uncoded";
				}
				else
				{
					// Call clsDesc.FindFirstRecord("crecordnumber", clsComm.GetData("occcode"))
					// TODO Get_Fields: Field [occcode] not found!! (maybe it is an alias?)
					clsDesc.FindFirst("crecordnumber = " + clsComm.Get_Fields("occcode"));
					txtOccupancy.Text = clsDesc.GetData("crecordnumber") + " " + clsDesc.GetData("cldesc");
				}
				lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsComm.GetData("thecount"))));
				txtCount.Text = lngCount.ToString();
				lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsComm.GetData("thesum"))));
				txtAssess.Text = Strings.Format(lngTotal, "###,###,###,##0");
				if (lngCount < 1)
				{
					txtAve.Text = "0";
				}
				else
				{
					txtAve.Text = Strings.Format(FCConvert.ToDouble(lngTotal) / lngCount, "##,###,###,##0");
				}
				clsComm.MoveNext();
			}
		}

		
	}
}
