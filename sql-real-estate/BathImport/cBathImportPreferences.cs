//Fecher vbPorter - Version 1.0.0.90

namespace TWRE0000
{
	public class cBathImportPreferences
	{

		//=========================================================


		private bool boolNameAddress;
		private bool boolValues;
		private bool boolMarkAsDeleted;
		private bool boolClearValues;
		private bool boolCreateNew;

		public bool CreateNew
		{
			set
			{
				boolCreateNew = value;
			}

			get
			{
					bool CreateNew = false;
				CreateNew = boolCreateNew;
				return CreateNew;
			}
		}



		public bool ConvertNameAddress
		{
			set
			{
				boolNameAddress = value;
			}

			get
			{
					bool ConvertNameAddress = false;
				ConvertNameAddress = boolNameAddress;
				return ConvertNameAddress;
			}
		}



		public bool ConvertValues
		{
			set
			{
				boolValues = value;
			}

			get
			{
					bool ConvertValues = false;
				ConvertValues = boolValues;
				return ConvertValues;
			}
		}



		public bool MarkAsDeleted
		{
			set
			{
				boolMarkAsDeleted = value;
			}

			get
			{
					bool MarkAsDeleted = false;
				MarkAsDeleted = boolMarkAsDeleted;
				return MarkAsDeleted;
			}
		}



		public bool ClearValues
		{
			set
			{
				boolClearValues = value;
			}

			get
			{
					bool ClearValues = false;
				ClearValues = boolClearValues;
				return ClearValues;
			}
		}



	}
}
