﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExemptCodes.
	/// </summary>
	partial class frmBathImport : BaseForm
	{

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBathImport));
            this.cmdDeleteCode = new Wisej.Web.Button();
            this.cmdSave = new Wisej.Web.Button();
            this.chkMarkDeleted = new fecherFoundation.FCCheckBox();
            this.lblUpdate = new fecherFoundation.FCLabel();
            this.fcPictureBox1 = new fecherFoundation.FCPictureBox();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMarkDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fcPictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 182);
            this.BottomPanel.Size = new System.Drawing.Size(737, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fcPictureBox1);
            this.ClientArea.Controls.Add(this.lblUpdate);
            this.ClientArea.Controls.Add(this.chkMarkDeleted);
            this.ClientArea.Size = new System.Drawing.Size(757, 436);
            this.ClientArea.Controls.SetChildIndex(this.chkMarkDeleted, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblUpdate, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcPictureBox1, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDeleteCode);
            this.TopPanel.Size = new System.Drawing.Size(757, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteCode, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(201, 30);
            this.HeaderText.Text = "Import Properties";
            // 
            // cmdDeleteCode
            // 
            this.cmdDeleteCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteCode.AppearanceKey = "toolbarButton";
            this.cmdDeleteCode.Location = new System.Drawing.Point(644, 29);
            this.cmdDeleteCode.Name = "cmdDeleteCode";
            this.cmdDeleteCode.Size = new System.Drawing.Size(90, 24);
            this.cmdDeleteCode.TabIndex = 2;
            this.cmdDeleteCode.Text = "Delete Code";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(357, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(90, 48);
            this.cmdSave.TabIndex = 3;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // chkMarkDeleted
            // 
            this.chkMarkDeleted.Location = new System.Drawing.Point(30, 31);
            this.chkMarkDeleted.Name = "chkMarkDeleted";
            this.chkMarkDeleted.Size = new System.Drawing.Size(214, 24);
            this.chkMarkDeleted.TabIndex = 2;
            this.chkMarkDeleted.Text = "Mark all properties as deleted";
            // 
            // lblUpdate
            // 
            this.lblUpdate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblUpdate.Location = new System.Drawing.Point(21, 151);
            this.lblUpdate.Name = "lblUpdate";
            this.lblUpdate.Size = new System.Drawing.Size(713, 31);
            this.lblUpdate.TabIndex = 1;
            // 
            // fcPictureBox1
            // 
            this.fcPictureBox1.Location = new System.Drawing.Point(507, 34);
            this.fcPictureBox1.Name = "fcPictureBox1";
            this.fcPictureBox1.Size = new System.Drawing.Size(181, 117);
            // 
            // frmBathImport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(757, 496);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmBathImport";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Import Properties";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmBathImport_Load);
            this.Activated += new System.EventHandler(this.frmBathImport_Activated);
            this.Resize += new System.EventHandler(this.frmBathImport_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBathImport_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMarkDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fcPictureBox1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdDeleteCode;
		private Button cmdSave;
        private FCCheckBox chkMarkDeleted;
        private FCLabel lblUpdate;
        private FCPictureBox fcPictureBox1;
    }
}
