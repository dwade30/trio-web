//Fecher vbPorter - Version 1.0.0.90
using System;
using System.Collections.ObjectModel;
using System.IO;
using fecherFoundation;
using Wisej.Web;
using Microsoft.VisualBasic;

namespace TWRE0000
{
	public class cBathImportFileController
	{

		//=========================================================

		private string strFileName;
		private bool boolStripTrailingZeroes;

		public bool StripTrailingZeroes
		{
			set
			{
				boolStripTrailingZeroes = value;
			}

			get
			{
					bool StripTrailingZeroes = false;
				StripTrailingZeroes = boolStripTrailingZeroes;
				return StripTrailingZeroes;
			}
		}




		public string FileName
		{
			set
			{
				strFileName = value;
			}

			get
			{
					string FileName = "";
				FileName = strFileName;
				return FileName;
			}
		}




		public Collection<cBathImportMaster> LoadFromFile(string strFName)
		{
			strFileName = strFName;
			return LoadFile();
		}

		public bool SaveToFile(ref FCCollection tCollection, string strFName)
		{
			bool SaveToFile = false;
			strFileName = strFName;
			SaveToFile = SaveFile(ref tCollection);
			return SaveToFile;
		}

		public bool SaveFile(ref FCCollection tCollection)
		{
			bool SaveFile = false;
			try
			{	// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				return SaveFile;
			}
			catch
			{	// ErrorHandler:
			}
			return SaveFile;
		}

		public Collection<cBathImportMaster> LoadFile()
		{
//            FCCollection tReturn = new/*AsNew*/ FCCollection();
            var tReturn = new Collection<cBathImportMaster>();
            try
			{	// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
								
				StreamReader ts;
				cBathImportMaster tMast;
				
				if (fecherFoundation.Strings.Trim(strFileName)!="") {
					if (File.Exists(strFileName))
                    {
                        ts = File.OpenText(strFileName);						
						string strLine = "";
						// strLine = ts.ReadLine ' skip header line
						while (!ts.EndOfStream) {
							strLine = ts.ReadLine();
							if (strLine.Length>=366) {
								tMast = ParseRecord(strLine);
								if (!(tMast==null)) {
									tReturn.Add(tMast);
								}
							}
						}
						ts.Close();
					} else {
						MessageBox.Show("File "+strFileName+" not found", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				} else {
					MessageBox.Show("No file name specified", "No File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}				
				return tReturn;
			}
			catch
			{	// ErrorHandler:
				MessageBox.Show("Error "+FCConvert.ToString(fecherFoundation.Information.Err().Number)+" "+fecherFoundation.Information.Err().Description+"\n"+"In LoadFile", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}

            return tReturn;
        }


		private cBathImportMaster ParseRecord(string strLine)
		{
			cBathImportMaster ParseRecord = null;
			try
			{	// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strTemp;
				cBathImportMaster tMast = new/*AsNew*/ cBathImportMaster();
				string[] strAry = null;
				strTemp = fecherFoundation.Strings.Mid(strLine, 1, 20);
				if (boolStripTrailingZeroes) {
					if (fecherFoundation.Strings.Right("    "+strTemp, 4)=="-000") {
						strTemp = fecherFoundation.Strings.Mid(strTemp, 1, strTemp.Length-4);
					}
				}
				tMast.MapLot = fecherFoundation.Strings.Trim(strTemp);
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 21, 40));
				tMast.Name = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 61, 40));
				tMast.Name2 = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 101, 40));
				tMast.Address1 = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 141, 40));
				tMast.Address2 = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 181, 25));
				tMast.City = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 206, 5));
				tMast.State = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 211, 10));
				tMast.Zip = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 221, 5));
				tMast.StreetNumber = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 226, 30));
				tMast.Street = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 256, 8));
				tMast.Book = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 264, 8));
				tMast.PAGE = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 272, 10));
				tMast.OldMapLot = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 282, 8));
				tMast.Acres = fecherFoundation.Conversion.Val(strTemp)/1000;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 290, 3));
				tMast.StateClass = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 293, 2));
				tMast.ClassType = strTemp;
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 295, 11));
				tMast.Land = fecherFoundation.Conversion.Val(strTemp);
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 306, 11));
				tMast.Building = fecherFoundation.Conversion.Val(strTemp);
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 317, 11));
				tMast.Total = fecherFoundation.Conversion.Val(strTemp);
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 328, 11));
				tMast.Set_ExemptVal(1, Math.Round(fecherFoundation.Conversion.Val(strTemp)));
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 339, 11));
				tMast.Set_ExemptVal(2, Math.Round(fecherFoundation.Conversion.Val(strTemp)));
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 350, 11));
				tMast.Set_ExemptVal(3, Math.Round(fecherFoundation.Conversion.Val(strTemp)));
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 361, 2));
				tMast.Set_ExemptCode(1, (short)Math.Round(fecherFoundation.Conversion.Val(strTemp)));
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 363, 2));
				tMast.Set_ExemptCode(2, (short)Math.Round(fecherFoundation.Conversion.Val(strTemp)));
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Mid(strLine, 365, 2));
				tMast.Set_ExemptCode(3, (short)Math.Round(fecherFoundation.Conversion.Val(strTemp)));

				ParseRecord = tMast;
				return ParseRecord;
			}
			catch
			{	// ErrorHandler:
				MessageBox.Show("Error "+FCConvert.ToString(fecherFoundation.Information.Err().Number)+"  "+fecherFoundation.Information.Err().Description+"\n"+"In ParseRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ParseRecord;
		}

	}
}
