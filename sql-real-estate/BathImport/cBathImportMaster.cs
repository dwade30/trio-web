//Fecher vbPorter - Version 1.0.0.90

namespace TWRE0000
{
	public class cBathImportMaster
	{

		//=========================================================


		private string strMapLot;
		private string strName;
		private string strName2;
		private string strAddr1;
		private string strAddr2;
		private string strCity;
		private string strState;
		private string strZip;
		private string strLocNum;
		private string strLocStreet;
		private string strBook;
		private string strPage;
		private string strOldMapLot;
		private double dblAcres;
		private string strStateClass;
		private string strClass;
		private double dblLand;
		private double dblBuilding;
		private double dblTotal;
		private double []dblExemptVal = new double[3 + 1];
		private short []intExemptCode = new short[3 + 1];

		public string MapLot
		{
			set
			{
				strMapLot = value;
			}

			get
			{
					string MapLot = "";
				MapLot = strMapLot;
				return MapLot;
			}
		}



		public string Name
		{
			set
			{
				strName = value;
			}

			get
			{
					string Name = "";
				Name = strName;
				return Name;
			}
		}



		public string Name2
		{
			set
			{
				strName2 = value;
			}

			get
			{
					string Name2 = "";
				Name2 = strName2;
				return Name2;
			}
		}



		public string Address1
		{
			set
			{
				strAddr1 = value;
			}

			get
			{
					string Address1 = "";
				Address1 = strAddr1;
				return Address1;
			}
		}



		public string Address2
		{
			set
			{
				strAddr2 = value;
			}

			get
			{
					string Address2 = "";
				Address2 = strAddr2;
				return Address2;
			}
		}



		public string City
		{
			set
			{
				strCity = value;
			}

			get
			{
					string City = "";
				City = strCity;
				return City;
			}
		}



		public string State
		{
			set
			{
				strState = value;
			}

			get
			{
					string State = "";
				State = strState;
				return State;
			}
		}



		public string Zip
		{
			set
			{
				strZip = value;
			}

			get
			{
					string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}



		public string StreetNumber
		{
			set
			{
				strLocNum = value;
			}

			get
			{
					string StreetNumber = "";
				StreetNumber = strLocNum;
				return StreetNumber;
			}
		}



		public string Street
		{
			set
			{
				strLocStreet = value;
			}

			get
			{
					string Street = "";
				Street = strLocStreet;
				return Street;
			}
		}



		public string Book
		{
			set
			{
				strBook = value;
			}

			get
			{
					string Book = "";
				Book = strBook;
				return Book;
			}
		}



		public string PAGE
		{
			set
			{
				strPage = value;
			}

			get
			{
					string PAGE = "";
				PAGE = strPage;
				return PAGE;
			}
		}



		public string OldMapLot
		{
			set
			{
				strOldMapLot = value;
			}

			get
			{
					string OldMapLot = "";
				OldMapLot = strOldMapLot;
				return OldMapLot;
			}
		}



		public double Acres
		{
			set
			{
				dblAcres = value;
			}

			get
			{
					double Acres = 0;
				Acres = dblAcres;
				return Acres;
			}
		}



		public string StateClass
		{
			set
			{
				strStateClass = value;
			}

			get
			{
					string StateClass = "";
				StateClass = strStateClass;
				return StateClass;
			}
		}




		public string ClassType
		{
			set
			{
				strClass = value;
			}

			get
			{
					string ClassType = "";
				ClassType = strClass;
				return ClassType;
			}
		}



		public double Land
		{
			set
			{
				dblLand = value;
			}

			get
			{
					double Land = 0;
				Land = dblLand;
				return Land;
			}
		}



		public double Building
		{
			set
			{
				dblBuilding = value;
			}

			get
			{
					double Building = 0;
				Building = dblBuilding;
				return Building;
			}
		}



		public double Total
		{
			set
			{
				dblTotal = value;
			}

			get
			{
					double Total = 0;
				Total = dblTotal;
				return Total;
			}
		}



		public void Set_ExemptVal(short intExemptNum,  double dblVal)
		{
			if (intExemptNum<4) {
				dblExemptVal[intExemptNum] = dblVal;
			}
		}

		public double Get_ExemptVal(short intExemptNum)
		{
				double ExemptVal = 0;
			if (intExemptNum<4) {
				ExemptVal = dblExemptVal[intExemptNum];
			}
			return ExemptVal;
		}

		public void Set_ExemptCode(short intExemptNum, short intCode)
		{
			if (intExemptNum<4) {
				intExemptCode[intExemptNum] = intCode;
			}
		}

		public short Get_ExemptCode(short intExemptNum)
		{
				short ExemptCode = 0;
			if (intExemptNum<4) {
				ExemptCode = intExemptCode[intExemptNum];
			}
			return ExemptCode;
		}

	}
}
