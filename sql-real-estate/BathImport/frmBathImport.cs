﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Linq.Expressions;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExemptCodes.
	/// </summary>
	public partial class frmBathImport : BaseForm
    {
        //private cBathImportFileController flImport;
		public frmBathImport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();

		}

		private void InitializeComponentEx()
		{

		}


		private void frmBathImport_Activated(object sender, System.EventArgs e)
		{
			
		}

		private void frmBathImport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBathImport_Load(object sender, System.EventArgs e)
		{

			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
		}



		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}


		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			Import();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			
		}

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
           
        }

        private void frmBathImport_Resize(object sender, EventArgs e)
        {
           
        }

        private void Import()
        {
            var strFileName = GetImportFile();
            if (String.IsNullOrWhiteSpace(strFileName))
            {
                return;
            }
            var dbImport = new cBathImportDBController();
            var flImport = new cBathImportFileController();
            dbImport.ProcessedRecord += DbImportOnProcessedRecord;
            dbImport.CreatedAccount += DbImportOnCreatedAccount;
            flImport.FileName = strFileName;
            var tColl = flImport.LoadFile();
            var tPrefs = new cBathImportPreferences();
            tPrefs.CreateNew = true;
            tPrefs.ConvertNameAddress = true;
            tPrefs.ConvertValues = true;
            tPrefs.ClearValues = false;
            if(chkMarkDeleted.Value == FCCheckBox.ValueSettings.Checked)
            {
                tPrefs.MarkAsDeleted = true;
            }
            else
            {
                tPrefs.MarkAsDeleted = false;
            }

            if (!dbImport.SaveCollection(ref tColl, ref tPrefs))
            {
                MessageBox.Show( "Error Importing");
            }
            else
            {
                MessageBox.Show("Done");
            }

            dbImport.ProcessedRecord -= DbImportOnProcessedRecord;
            dbImport.CreatedAccount -= DbImportOnCreatedAccount;
        }

        private string GetImportFile()
        {
            FCCommonDialog ofd = new FCCommonDialog();
            string strFileName = "";
            ofd.Text = "Import File";
            if (ofd.ShowOpen())
            {
                return ofd.FileName;
            }
            else
            {
                return "";
            }
        }
        private void DbImportOnCreatedAccount(ref cPropertyValues tprop)
        {
            if (tprop != null)
            {
                lblUpdate.Text = "Created " + tprop.Account;
            }
        }

        private void DbImportOnProcessedRecord(ref cPropertyValues tprop)
        {
            if (tprop != null)
            {
                lblUpdate.Text = "Account " + tprop.MapLot;
                lblUpdate.Refresh();
            }
        }
    }
}
