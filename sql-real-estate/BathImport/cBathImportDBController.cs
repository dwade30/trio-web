//Fecher vbPorter - Version 1.0.0.90
using System;
using System.Collections.ObjectModel;
using System.Data.Common;
using fecherFoundation;
using Global;
using Wisej.Web;
using Microsoft.VisualBasic;
using SharedApplication.Extensions;

namespace TWRE0000
{
	public class cBathImportDBController
	{

		//=========================================================

		public delegate void ProcessedRecordEventHandler(ref cPropertyValues tProp);
		public event ProcessedRecordEventHandler ProcessedRecord;
		public delegate void CreatedAccountEventHandler(ref cPropertyValues tProp);
		public event CreatedAccountEventHandler CreatedAccount;
		public delegate void UnmatchedAccountEventHandler(ref cPropertyValues tProp);
		public event UnmatchedAccountEventHandler UnmatchedAccount;
		cPartyUtil tPu = new/*AsNew*/ cPartyUtil();
		private object[] arParties = null;

		public Collection<cBathImportMaster> LoadCollection()
		{
			try
			{	// On Error GoTo ErrorHandler
				//FCCollection tReturn = new/*AsNew*/ FCCollection();
                var records = new Collection<cBathImportMaster>();
				string strSQL;
				clsDRWrapper rsLoad = new/*AsNew*/ clsDRWrapper();

				strSQL = "Select * from master where not rsdeleted order by rsaccount,rscard";
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				while (!rsLoad.EndOfFile()) {

					rsLoad.MoveNext();
				}
				return records;
			}
			catch
			{	// ErrorHandler:
				MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err().Number)+" "+fecherFoundation.Information.Err().Description+"\n"+"In LoadCollection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}

            return new Collection<cBathImportMaster>();
        }

		private cBathImportMaster FillRecord(ref clsDRWrapper tMast)
		{
			cBathImportMaster FillRecord = null;
			try
			{	// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cBathImportMaster tReturn = new/*AsNew*/ cBathImportMaster();

				FillRecord = tReturn;
				return FillRecord;
			}
			catch
			{	// ErrorHandler:
				MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err().Number)+" "+fecherFoundation.Information.Err().Description+"\n"+"In FillRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillRecord;
		}


		public bool SaveCollection(ref Collection<cBathImportMaster> tCollection, ref cBathImportPreferences tPrefs)
		{
			bool SaveCollection = false;
			try
			{	// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveCollection = false;
				bool boolReturn;
				boolReturn = true;
				if (!(tCollection==null) && !(tPrefs==null)) {
					clsDRWrapper rsSave = new/*AsNew*/ clsDRWrapper();
					if (tPrefs.MarkAsDeleted) {
						rsSave.Execute("update master set rsdeleted = 'true'", modGlobalVariables.strREDatabase);
					}
					if (tPrefs.ClearValues) {
						rsSave.Execute("update master set lastlandval = 0, lastbldgval = 0,rllandval = 0,rlbldgval = 0,rlexemption = 0,EXEMPTVAL1 = 0,exemptval2 = 0,exemptval3 = 0", modGlobalVariables.strREDatabase);
					}

					//cBathImportMaster tMast;
					foreach (cBathImportMaster tMast in tCollection)
                    {
                        var copyMast = tMast;
						if (!SaveRecord(ref copyMast, ref tPrefs)) {
							boolReturn = false;
						}
					} // tMast
				}
				SaveCollection = boolReturn;
				return SaveCollection;
			}
			catch
			{	// ErrorHandler:
				MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err().Number)+" "+fecherFoundation.Information.Err().Description+"\n"+"In SaveCollection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCollection;
		}

		private bool SaveRecord(ref cBathImportMaster tMast, ref cBathImportPreferences tPrefs)
		{
			bool SaveRecord = false;
			try
			{	// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveRecord = false;
				bool boolReturn;
				int lngAccount = 0;
				bool boolNew = false;
				double dblTemp;
				// vbPorter upgrade warning: intTemp As short	OnWrite(double)
				short intTemp = 0;
				cPropertyValues tValues = new/*AsNew*/ cPropertyValues();
				cPartyController tPartyCont = new/*AsNew*/ cPartyController();
				cParty tParty = new cParty();
				cParty stParty = new/*AsNew*/ cParty();
				cParty tempParty = new cParty();
				cPartyAddress tAddr;
				bool boolAddAddress;
				clsDRWrapper rsMastOwner = new/*AsNew*/ clsDRWrapper();

				cCreateGUID tGUID = new/*AsNew*/ cCreateGUID();
				// Dim oParty As New CParty
				// Dim uParty As UtilParty
				// Dim tVar As Variant
				string strOwner1 = "";
				string strOwner2 = "";
				string strAddress1 = "";
				string strAddress2 = "";
				string strCity = "";
				string strState = "";
				string strZip = "";
				string strZip4 = "";
				string strZipCode = "";
				FCCollection vArray = new FCCollection();
				string[] tempArray = null;
				// vbPorter upgrade warning: intIndex As short	OnWrite(int)
				short intIndex = 0;
				// vbPorter upgrade warning: testParty As CParty	OnWrite(cParty)
				cParty testParty = new cParty();
				Array.Resize(ref arParties, 0 + 1);
				short x;
				bool blnSkipParty = false;


				boolReturn = true;
				if (!(tMast==null)) {
					clsDRWrapper rsSave = new/*AsNew*/ clsDRWrapper();
					string[] strAry = null;
					boolNew = false;
					tValues.Exemption = 0;
					tValues.Building = 0;
					tValues.Account = 0;
					tValues.Land = 0;
					tValues.MapLot = tMast.MapLot;
					tValues.Name = "";
					rsSave.OpenRecordset("select * from master where rsmaplot = '"+tMast.MapLot+"'", modGlobalVariables.strREDatabase);

					if (!rsSave.EndOfFile()) {
						rsSave.Edit();
						lngAccount = Convert.ToInt32(rsSave.Get_Fields("rsaccount"));
						if (this.ProcessedRecord != null) this.ProcessedRecord(ref tValues);
					} else {
						if (tPrefs.CreateNew) {
							rsSave.AddNew();
							boolNew = true;
							lngAccount = modGlobalRoutines.GetNextAccountToCreate();
							rsSave.Set_Fields("rscard", 1);
							rsSave.Set_Fields("rsaccount", lngAccount);
							// Dim tGuid As New cCreateGUID
							string strTemp = "";
							strTemp = tGUID.CreateGUID();
							rsSave.Set_Fields("accountid", strTemp);
							rsSave.Set_Fields("cardid", strTemp);
							rsSave.Set_Fields("datecreated", fecherFoundation.Strings.Format(DateTime.Now, "mm/dd/yyyy"));
							rsSave.Set_Fields("exemptpct1", 100);
							rsSave.Set_Fields("exemptpct2", 100);
							rsSave.Set_Fields("exemptpct3", 100);
							rsSave.Set_Fields("rsmaplot", tMast.MapLot);
						} else {
							tValues.Name = tMast.Name;
							tValues.Building = tMast.Building;
							tValues.Land = tMast.Land;
							if (this.UnmatchedAccount != null) this.UnmatchedAccount(ref tValues);
							return SaveRecord;
						}
					}

					if (boolNew || tPrefs.ConvertNameAddress) {

						strOwner1 = fecherFoundation.Strings.Trim(tMast.Name);
						strOwner2 = fecherFoundation.Strings.Trim(tMast.Name2);
						strAddress1 = fecherFoundation.Strings.Trim(tMast.Address1);
						strAddress2 = fecherFoundation.Strings.Trim(tMast.Address2);
						strCity = fecherFoundation.Strings.Trim(tMast.City);
						strState = fecherFoundation.Strings.Trim(tMast.State);
						strZip = fecherFoundation.Strings.Trim(tMast.Zip);



						if (fecherFoundation.Strings.LCase(strOwner1)!="") {

							blnSkipParty = false;

							vArray = tPu.SplitName(strOwner1, true); // 211 was false
							if (vArray.Count>=1) {
								// If UBound(vArray) >= 0 Then
								for(x=1; x<=vArray.Count; x++) {
									tempParty = vArray[x];
									tParty = new cParty();
									if (fecherFoundation.Strings.LCase(modGlobalConstants.Statics.MuniName)=="gardiner") {
										if (tempParty.PartyType==0) {
											if (tempParty.LastName=="") {
												if (fecherFoundation.Strings.InStr(1, tempParty.FirstName, ",", fecherFoundation.CompareConstants.vbBinaryCompare)<=0) {
													intIndex = (short)fecherFoundation.Strings.InStr(1, tempParty.FirstName, " ", fecherFoundation.CompareConstants.vbBinaryCompare);
													if (intIndex>0) {
														if (intIndex+1<tempParty.FirstName.Length) {
															vArray = tPu.SplitName(fecherFoundation.Strings.Mid(tempParty.FirstName, 1, intIndex)+","+fecherFoundation.Strings.Mid(tempParty.FirstName, intIndex+1), false);
															tempParty = vArray[x];
														}
													}
												}
											}
										}
									}
									tParty.DateCreated = fecherFoundation.Strings.Format(DateTime.Now, "mm/dd/yyyy").ToDate();
									tParty.CreatedBy = modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID();
									tParty.Designation = tempParty.Designation;
									tParty.FirstName = tempParty.FirstName;
									tParty.LastName = tempParty.LastName;
									tParty.MiddleName = tempParty.MiddleName;
									tParty.PartyGUID = tempParty.PartyGUID;
									tParty.PartyType = tempParty.PartyType;
									tAddr = new cPartyAddress();
									tAddr.AddressType = "Primary";
									tAddr.Address1 = strAddress1;
									tAddr.Address2 = strAddress2;
									tAddr.Address3 = "";
									tAddr.City = strCity;
									tAddr.State = strState;
									tAddr.Zip = strZip;

									tParty.Addresses.Add(tAddr);

									testParty = tPartyCont.FindFirstMatchingParty(ref tParty);
									if (!(testParty==null)) {
										if (!tPartyCont.PartiesHaveSameAddress(ref tParty, ref testParty)) {
											tPartyCont.SaveParty(ref tParty);
										} else {
											tParty = testParty;
										}
									} else {
										tPartyCont.SaveParty(ref tParty);
									}
									if (x==1) { // trores-211 9.4.18
										rsSave.Set_Fields("OwnerPartyID", tParty.ID);
										Array.Resize(ref arParties, fecherFoundation.Information.UBound(arParties, 1)+1 + 1);
										arParties[fecherFoundation.Information.UBound(arParties)] = tParty.ID;
									} else {
										rsSave.Set_Fields("SecOwnerPartyID", tParty.ID);
										Array.Resize(ref arParties, fecherFoundation.Information.UBound(arParties, 1)+1 + 1);
										arParties[fecherFoundation.Information.UBound(arParties)] = tParty.ID;
										blnSkipParty = true;
									}
								}
							} else {
								rsSave.Set_Fields("OwnerPartyID", 0);
							}
						} else {
							rsSave.Set_Fields("OwnerPartyID", 0);
						}

						if (!blnSkipParty) { // trores-211 9.4.18
							if (strOwner2!="") {
								vArray = tPu.SplitName(strOwner2, false);
								// If UBound(vArray) >= 0 Then
								if (vArray.Count>=1) {
									tempParty = vArray[1];
									tParty = new cParty();
									// If LCase(MuniName) = "gardiner" Then
									if (tempParty.PartyType==0) {
										if (tempParty.LastName=="") {
											if (fecherFoundation.Strings.InStr(1, tempParty.FirstName, ",", fecherFoundation.CompareConstants.vbBinaryCompare)<=0) {
												intIndex = (short)fecherFoundation.Strings.InStr(1, tempParty.FirstName, " ", fecherFoundation.CompareConstants.vbBinaryCompare);
												if (intIndex>0) {
													if (intIndex+1<tempParty.FirstName.Length) {
														vArray = tPu.SplitName(fecherFoundation.Strings.Mid(tempParty.FirstName, 1, intIndex)+","+fecherFoundation.Strings.Mid(tempParty.FirstName, intIndex+1), false);
														tempParty = vArray[1];
													}
												}
											}
										}
									}
									// End If
									tParty.PartyGUID = tempParty.PartyGUID;
									tParty.PartyType = tempParty.PartyType;
									tParty.FirstName = tempParty.FirstName;
									tParty.MiddleName = tempParty.MiddleName;
									tParty.LastName = tempParty.LastName;
									tParty.Designation = tempParty.Designation;
									tParty.DateCreated = fecherFoundation.Strings.Format(DateTime.Now, "mm/dd/yyyy").ToDate();
									tParty.CreatedBy = modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID();
									tPartyCont.SaveParty(ref tParty);
									rsSave.Set_Fields("SecOwnerPartyID", tParty.ID);
									Array.Resize(ref arParties, fecherFoundation.Information.UBound(arParties, 1)+1 + 1);
									arParties[fecherFoundation.Information.UBound(arParties)] = tParty.ID;
								} else {
									rsSave.Set_Fields("SecOwnerPartyID", 0);
								}
							} else {
								rsSave.Set_Fields("SecOwnerPartyID", 0);
							}
						}
					}


					if (boolNew || tPrefs.ConvertValues) {
						rsSave.Set_Fields("piacres", tMast.Acres);
						rsSave.Set_Fields("rsother", tMast.Acres);
						rsSave.Set_Fields("rssoft", 0);
						rsSave.Set_Fields("rshard", 0);
						rsSave.Set_Fields("rsmixed", 0);
						rsSave.Set_Fields("rssoftvalue", 0);
						rsSave.Set_Fields("rshardvalue", 0);
						rsSave.Set_Fields("rsmixedvalue", 0);
						rsSave.Set_Fields("rsothervalue", tMast.Land);
						rsSave.Set_Fields("lastlandval", tMast.Land);
						rsSave.Set_Fields("lastbldgval", tMast.Building);
						rsSave.Set_Fields("rslocnumalph", tMast.StreetNumber);
						rsSave.Set_Fields("rslocstreet", tMast.Street);
						rsSave.Set_Fields("exemptpct1", 100);
						rsSave.Set_Fields("exemptpct2", 100);
						rsSave.Set_Fields("exemptpct3", 100);
						rsSave.Set_Fields("exemptval1", 0);
						rsSave.Set_Fields("exemptval2", 0);
						rsSave.Set_Fields("exemptval3", 0);
						rsSave.Set_Fields("riexemptcd1", 0);
						rsSave.Set_Fields("riexemptcd2", 0);
						rsSave.Set_Fields("riexemptcd3", 0);
						rsSave.Set_Fields("piopen1", FCConvert.ToString(fecherFoundation.Conversion.Val(tMast.StateClass)));

						if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Right(" "+tMast.ClassType, 1))!="E") {
							rsSave.Set_Fields("rlexemption", tMast.Get_ExemptVal(1)+tMast.Get_ExemptVal(2)+tMast.Get_ExemptVal(3));
							tValues.Exemption = rsSave.Get_Fields("rlexemption");
							if (fecherFoundation.Conversion.Val(tMast.Get_ExemptCode(1))>0) {
								rsSave.Set_Fields("riexemptcd1", tMast.Get_ExemptCode(1));
								rsSave.Set_Fields("exemptval1", tMast.Get_ExemptVal(1));
								if (fecherFoundation.Conversion.Val(tMast.Get_ExemptCode(1))==50) {
									rsSave.Set_Fields("exemptpct1", 50);
								}
							}
							if (fecherFoundation.Conversion.Val(tMast.Get_ExemptCode(2))>0) {
								rsSave.Set_Fields("riexemptcd2", tMast.Get_ExemptCode(2));
								rsSave.Set_Fields("exemptval2", tMast.Get_ExemptVal(2));
								if (fecherFoundation.Conversion.Val(tMast.Get_ExemptCode(2))==50) {
									rsSave.Set_Fields("exemptpct2", 50);
								}
							}
							if (fecherFoundation.Conversion.Val(tMast.Get_ExemptCode(3))>0) {
								rsSave.Set_Fields("riexemptcd3", tMast.Get_ExemptCode(3));
								rsSave.Set_Fields("exemptval3", tMast.Get_ExemptVal(3));
								if (fecherFoundation.Conversion.Val(tMast.Get_ExemptCode(3))==50) {
									rsSave.Set_Fields("exemptpct3", 50);
								}
							}
						} else {
							if (fecherFoundation.Conversion.Val(tMast.StateClass)>=900) {
								intTemp = Convert.ToInt16(70+(fecherFoundation.Conversion.Val(tMast.StateClass)-900));
								rsSave.Set_Fields("riexemptcd1", intTemp);
								rsSave.Set_Fields("exemptval1", tMast.Land+tMast.Building);
								rsSave.Set_Fields("rlexemption", tMast.Land+tMast.Building);
								tValues.Exemption = tMast.Land+tMast.Building;
							}
						}
						if (tMast.Book!="" && tMast.PAGE!="") {
							AddBookPage(tMast.Book,  tMast.PAGE,  lngAccount);
						}
					}

					tValues.Account = lngAccount;
					tValues.Building = tMast.Building;
					tValues.Land = tMast.Land;
					tValues.MapLot = tMast.MapLot;
					tValues.Name = tMast.Name;
					if (boolNew) {
						if (this.CreatedAccount != null) this.CreatedAccount(ref tValues);
					}
					if (this.ProcessedRecord != null) this.ProcessedRecord(ref tValues);
					rsSave.Set_Fields("rsdeleted", false);
					rsSave.Update();
				}
				SaveRecord = boolReturn;
				return SaveRecord;
			}
			catch
			{	// ErrorHandler:
				MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err().Number)+" "+fecherFoundation.Information.Err().Description+"\n"+"In SaveRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveRecord;
		}


		private void AddBookPage( string strBook,  string strPage, int lngAcct)
		{
			clsDRWrapper rsMisc = new/*AsNew*/ clsDRWrapper();
			bool boolFound = false;
			short intLine;
			if (strBook=="" || strPage=="") {
				return;
			}
			intLine = 1;
			rsMisc.OpenRecordset("select * from bookpage where account = "+FCConvert.ToString(lngAcct)+" order by line", modGlobalVariables.strREDatabase);
			while (!rsMisc.EndOfFile()) {
				if (FCConvert.ToString(rsMisc.Get_Fields("book"))==strBook && FCConvert.ToString(rsMisc.Get_Fields("page"))==strPage) {
					boolFound = true;
				}
				intLine = Convert.ToInt16(rsMisc.Get_Fields("line"));
				rsMisc.MoveNext();
			}
			if (!boolFound && strBook!="" && strPage!="") {
				rsMisc.AddNew();
				rsMisc.Set_Fields("account", lngAcct);
				rsMisc.Set_Fields("card", 1);
				rsMisc.Set_Fields("line", intLine);
				rsMisc.Set_Fields("book", strBook);
				rsMisc.Set_Fields("page", strPage);
				rsMisc.Set_Fields("current", true);
				rsMisc.Set_Fields("saledate", 0);

				rsMisc.Update();
			}
		}

	}
}
