//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPreviousOwner.
	/// </summary>
	partial class frmPreviousOwner : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label8;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCTextBox txtSecOwner;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtAddress2;
		public Global.T2KDateBox T2KSaleDate;
		public fecherFoundation.FCLabel Label8_0;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel Label9;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPreviousOwner));
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtName = new fecherFoundation.FCTextBox();
			this.txtSecOwner = new fecherFoundation.FCTextBox();
			this.txtAddress1 = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtZip4 = new fecherFoundation.FCTextBox();
			this.txtAddress2 = new fecherFoundation.FCTextBox();
			this.T2KSaleDate = new Global.T2KDateBox();
			this.Label8_0 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdInterestedParties = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.T2KSaleDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdInterestedParties)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 568);
			this.BottomPanel.Size = new System.Drawing.Size(698, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.lblAccount);
			this.ClientArea.Controls.Add(this.Label9);
			this.ClientArea.Size = new System.Drawing.Size(698, 508);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdInterestedParties);
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(698, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdInterestedParties, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(187, 30);
			this.HeaderText.Text = "Previous Owner";
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.txtName);
			this.Frame3.Controls.Add(this.txtSecOwner);
			this.Frame3.Controls.Add(this.txtAddress1);
			this.Frame3.Controls.Add(this.txtCity);
			this.Frame3.Controls.Add(this.txtState);
			this.Frame3.Controls.Add(this.txtZip);
			this.Frame3.Controls.Add(this.txtZip4);
			this.Frame3.Controls.Add(this.txtAddress2);
			this.Frame3.Controls.Add(this.T2KSaleDate);
			this.Frame3.Controls.Add(this.Label8_0);
			this.Frame3.Controls.Add(this.Label1);
			this.Frame3.Controls.Add(this.Label2);
			this.Frame3.Controls.Add(this.Label3);
			this.Frame3.Controls.Add(this.Label4);
			this.Frame3.Controls.Add(this.Label5);
			this.Frame3.Controls.Add(this.Label6);
			this.Frame3.Controls.Add(this.Label7);
			this.Frame3.Location = new System.Drawing.Point(30, 70);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(652, 382);
			this.Frame3.TabIndex = 2;
			this.Frame3.Text = "Previous Owner Information";
			// 
			// txtName
			// 
			this.txtName.AutoSize = false;
			this.txtName.BackColor = System.Drawing.SystemColors.Window;
			this.txtName.LinkItem = null;
			this.txtName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName.LinkTopic = null;
			this.txtName.Location = new System.Drawing.Point(162, 30);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(470, 40);
			this.txtName.TabIndex = 1;
			// 
			// txtSecOwner
			// 
			this.txtSecOwner.AutoSize = false;
			this.txtSecOwner.BackColor = System.Drawing.SystemColors.Window;
			this.txtSecOwner.LinkItem = null;
			this.txtSecOwner.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSecOwner.LinkTopic = null;
			this.txtSecOwner.Location = new System.Drawing.Point(162, 90);
			this.txtSecOwner.Name = "txtSecOwner";
			this.txtSecOwner.Size = new System.Drawing.Size(470, 40);
			this.txtSecOwner.TabIndex = 3;
			// 
			// txtAddress1
			// 
			this.txtAddress1.AutoSize = false;
			this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress1.LinkItem = null;
			this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress1.LinkTopic = null;
			this.txtAddress1.Location = new System.Drawing.Point(162, 150);
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Size = new System.Drawing.Size(470, 40);
			this.txtAddress1.TabIndex = 5;
			// 
			// txtCity
			// 
			this.txtCity.AutoSize = false;
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCity.LinkItem = null;
			this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCity.LinkTopic = null;
			this.txtCity.Location = new System.Drawing.Point(162, 270);
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(113, 40);
			this.txtCity.TabIndex = 9;
			// 
			// txtState
			// 
			this.txtState.MaxLength = 2;
			this.txtState.AutoSize = false;
			this.txtState.BackColor = System.Drawing.SystemColors.Window;
			this.txtState.LinkItem = null;
			this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtState.LinkTopic = null;
			this.txtState.Location = new System.Drawing.Point(364, 270);
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(61, 40);
			this.txtState.TabIndex = 11;
			// 
			// txtZip
			// 
			this.txtZip.MaxLength = 5;
			this.txtZip.AutoSize = false;
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip.LinkItem = null;
			this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip.LinkTopic = null;
			this.txtZip.Location = new System.Drawing.Point(479, 270);
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(78, 40);
			this.txtZip.TabIndex = 13;
			// 
			// txtZip4
			// 
			this.txtZip4.MaxLength = 4;
			this.txtZip4.AutoSize = false;
			this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip4.LinkItem = null;
			this.txtZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip4.LinkTopic = null;
			this.txtZip4.Location = new System.Drawing.Point(560, 270);
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Size = new System.Drawing.Size(72, 40);
			this.txtZip4.TabIndex = 14;
			// 
			// txtAddress2
			// 
			this.txtAddress2.AutoSize = false;
			this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress2.LinkItem = null;
			this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress2.LinkTopic = null;
			this.txtAddress2.Location = new System.Drawing.Point(162, 210);
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Size = new System.Drawing.Size(470, 40);
			this.txtAddress2.TabIndex = 7;
			// 
			// T2KSaleDate
			// 
			this.T2KSaleDate.Location = new System.Drawing.Point(162, 330);
			this.T2KSaleDate.Mask = "##/##/####";
			this.T2KSaleDate.MaxLength = 10;
			this.T2KSaleDate.Name = "T2KSaleDate";
			this.T2KSaleDate.Size = new System.Drawing.Size(115, 40);
			this.T2KSaleDate.TabIndex = 16;
			this.T2KSaleDate.Text = "  /  /";
			// 
			// Label8_0
			// 
			this.Label8_0.Location = new System.Drawing.Point(20, 344);
			this.Label8_0.Name = "Label8_0";
			this.Label8_0.Size = new System.Drawing.Size(76, 17);
			this.Label8_0.TabIndex = 15;
			this.Label8_0.Text = "SALE DATE";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(76, 17);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "NAME";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(76, 17);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "2ND OWNER";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 164);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(76, 17);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "ADDRESS 1";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 224);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(76, 17);
			this.Label4.TabIndex = 6;
			this.Label4.Text = "ADDRESS 2";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 284);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(76, 17);
			this.Label5.TabIndex = 8;
			this.Label5.Text = "CITY";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(298, 284);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(37, 17);
			this.Label6.TabIndex = 10;
			this.Label6.Text = "STATE";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(440, 284);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(23, 17);
			this.Label7.TabIndex = 12;
			this.Label7.Text = "ZIP";
			// 
			// lblAccount
			// 
			this.lblAccount.Location = new System.Drawing.Point(138, 30);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(116, 21);
			this.lblAccount.TabIndex = 1;
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(30, 30);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(67, 21);
			this.Label9.TabIndex = 0;
			this.Label9.Text = "ACCOUNT";
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(354, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(186, 24);
			this.cmdDelete.TabIndex = 1;
			this.cmdDelete.Text = "Delete this Previous Owner";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(302, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(46, 24);
			this.cmdNew.TabIndex = 2;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(309, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 3;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// cmdInterestedParties
			// 
			this.cmdInterestedParties.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdInterestedParties.AppearanceKey = "toolbarButton";
			this.cmdInterestedParties.Location = new System.Drawing.Point(546, 29);
			this.cmdInterestedParties.Name = "cmdInterestedParties";
			this.cmdInterestedParties.Size = new System.Drawing.Size(124, 24);
			this.cmdInterestedParties.TabIndex = 3;
			this.cmdInterestedParties.Text = "Interested Parties";
			this.cmdInterestedParties.Click += new System.EventHandler(this.mnuInterestedParties_Click);
			// 
			// frmPreviousOwner
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(698, 676);
			this.FillColor = 0;
			this.Name = "frmPreviousOwner";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Previous Owner";
			this.Load += new System.EventHandler(this.frmPreviousOwner_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPreviousOwner_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.T2KSaleDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdInterestedParties)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdDelete;
		private FCButton cmdNew;
		private FCButton cmdSave;
		private FCButton cmdInterestedParties;
	}
}