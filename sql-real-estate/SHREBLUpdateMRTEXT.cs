﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class modSHREBLUpdateMRTEXT
	{
		public static void TextToREMR()
		{
			/*? On Error Resume Next  */
			try
			{
				bool boolBillValsChanged;
				clsDRWrapper clsSave = new clsDRWrapper();
				bool boolExemptionChanged;
				string strTemp = "";
				string strPhone = "";
				int x;
				boolBillValsChanged = false;
				boolExemptionChanged = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				modDataTypes.Statics.MR.Set_Fields("rssoft", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(1, 1) + "")));
				modDataTypes.Statics.MR.Set_Fields("rshard", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(1, 3) + "")));
				modDataTypes.Statics.MR.Set_Fields("rsmixed", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(1, 2) + "")));
				modDataTypes.Statics.MR.Set_Fields("rsother", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(1, 4))));
				modDataTypes.Statics.MR.Set_Fields("rssoftvalue", FCConvert.ToInt32(FCConvert.ToDouble(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(2, 1) + "")));
				modDataTypes.Statics.MR.Set_Fields("rshardvalue", FCConvert.ToInt32(FCConvert.ToDouble(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(2, 3) + "")));
				modDataTypes.Statics.MR.Set_Fields("rsmixedvalue", FCConvert.ToInt32(FCConvert.ToDouble(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(2, 2) + "")));
				modDataTypes.Statics.MR.Set_Fields("rsothervalue", FCConvert.ToInt32(FCConvert.ToDouble(frmSHREBLUpdate.InstancePtr.TreeGrid.TextMatrix(2, 4))));
				modDataTypes.Statics.MR.Set_Fields("taxacquired", frmSHREBLUpdate.InstancePtr.chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked);
				modDataTypes.Statics.MR.Set_Fields("inbankruptcy", frmSHREBLUpdate.InstancePtr.chkBankruptcy.CheckState == Wisej.Web.CheckState.Checked);
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					modDataTypes.Statics.MR.Set_Fields("revocabletrust", frmSHREBLUpdate.InstancePtr.chkRLivingTrust.CheckState == Wisej.Web.CheckState.Checked);
				}
				modDataTypes.Statics.MR.Set_Fields("hlupdate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				modDataTypes.Statics.MR.Set_Fields("rsMapLot", frmSHREBLUpdate.InstancePtr.txtMRRSMAPLOT.Text);
				modDataTypes.Statics.MR.Set_Fields("RSPREVIOUSMASTER", Strings.Trim(frmSHREBLUpdate.InstancePtr.txtPreviousOwner.Text));
				modDataTypes.Statics.MR.Set_Fields("rslocnumalph", frmSHREBLUpdate.InstancePtr.txtMRRSLOCNUMALPH.Text);
				modDataTypes.Statics.MR.Set_Fields("rslocapt", frmSHREBLUpdate.InstancePtr.txtMRRSLOCAPT.Text);
				modDataTypes.Statics.MR.Set_Fields("rslocstreet", frmSHREBLUpdate.InstancePtr.txtMRRSLOCSTREET.Text);
				modDataTypes.Statics.MR.Set_Fields("rsref1", frmSHREBLUpdate.InstancePtr.txtMRRSREF1.Text);
				modDataTypes.Statics.MR.Set_Fields("rsref2", frmSHREBLUpdate.InstancePtr.txtMRRSREF2.Text);
				// these lines are for the backfill numeric fields
				if (frmSHREBLUpdate.InstancePtr.txtMRRLLANDVAL.Text != "")
				{
					if (FCConvert.ToDecimal(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval")) != FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLLANDVAL.Text + ""))
					{
						boolBillValsChanged = true;
					}
					modDataTypes.Statics.MR.Set_Fields("lastlandval", FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLLANDVAL.Text + ""));
				}
				else
				{
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval")) != 0)
						boolBillValsChanged = true;
				}
				if (frmSHREBLUpdate.InstancePtr.txtMRRLBLDGVAL.Text != "")
				{
					if (FCConvert.ToDecimal(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval")) != FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLBLDGVAL.Text + ""))
					{
						boolBillValsChanged = true;
					}
					modDataTypes.Statics.MR.Set_Fields("lastbldgval", FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLBLDGVAL.Text + ""));
				}
				else
				{
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval")) != 0)
						boolBillValsChanged = true;
				}
				for (x = 1; x <= 3; x++)
				{
					if (Conversion.Val(frmSHREBLUpdate.InstancePtr.GridExempts.TextMatrix(x, 0)) > -1)
					{
						modDataTypes.Statics.MR.Set_Fields("riexemptcd" + x, FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.GridExempts.TextMatrix(x, 0))));
						modDataTypes.Statics.MR.Set_Fields("exemptpct" + x, FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.GridExempts.TextMatrix(x, 1))));
					}
					else
					{
						modDataTypes.Statics.MR.Set_Fields("riexemptcd" + x, 0);
						modDataTypes.Statics.MR.Set_Fields("exemptpct" + x, 100);
					}
				}
				// x
				if (frmSHREBLUpdate.InstancePtr.txtMRRLEXEMPTION.Text != "")
				{
					if (FCConvert.ToDecimal(modDataTypes.Statics.MR.Get_Fields_Int32("rlexemption")) != FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLEXEMPTION.Text + ""))
					{
						boolBillValsChanged = true;
						boolExemptionChanged = true;
					}
					modDataTypes.Statics.MR.Set_Fields("rlexemption", FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLEXEMPTION.Text + ""));
				}
				else
				{
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rlexemption")) != 0)
					{
						boolBillValsChanged = true;
						boolExemptionChanged = true;
						modDataTypes.Statics.MR.Set_Fields("rlexemption", 0);
					}
				}
				if (boolBillValsChanged)
				{
					clsSave.Execute("update status set accountschange = '" + FCConvert.ToString(DateTime.Now) + "'", modGlobalVariables.strREDatabase);
				}
				// must calc exemptions if the exemption changed and we have legitimate codes entered
				// end of backfill numeric fields
				modDataTypes.Statics.MR.Set_Fields("rilandcode", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.gridLandCode.TextMatrix(0, 0))));
				modDataTypes.Statics.MR.Set_Fields("ribldgcode", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.gridBldgCode.TextMatrix(0, 0))));
				modDataTypes.Statics.MR.Set_Fields("propertycode", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.gridPropertyCode.TextMatrix(0, 0))));
				modDataTypes.Statics.MR.Set_Fields("piopen1", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRPIOPEN1.Text + "")));
				modDataTypes.Statics.MR.Set_Fields("piopen2", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRPIOPEN2.Text + "")));
				if (Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRPIACRES.Text + "") != 0)
				{
					modDataTypes.Statics.MR.Set_Fields("piacres", FCConvert.ToDouble(frmSHREBLUpdate.InstancePtr.txtMRPIACRES.Text));
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("piacres", 0);
				}
				modDataTypes.Statics.MR.Set_Fields("entrancecode", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRDIENTRANCECODE.Text + "")));
				modDataTypes.Statics.MR.Set_Fields("informationcode", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRDIENTRANCECODE.Text + "")));
				if (Information.IsDate(frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text))
				{
					if (Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text) > 0)
					{
						modDataTypes.Statics.MR.Set_Fields("dateinspected", Strings.Format(frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text, "MM/dd/yy"));
					}
					else
					{
						modDataTypes.Statics.MR.Set_Fields("dateinspected", 0);
					}
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("dateinspected", 0);
				}
				modDataTypes.Statics.MR.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.MR.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
				modDataTypes.Statics.DWL.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.DWL.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
				modDataTypes.Statics.CMR.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.CMR.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
				modDataTypes.Statics.OUT.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.OUT.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
				if (!modGlobalVariables.Statics.boolInPendingMode)
				{
					clsDRWrapper temp = modDataTypes.Statics.MR;
					modREMain.SaveMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
					modDataTypes.Statics.MR = temp;
					temp = modDataTypes.Statics.DWL;
					modREMain.SaveDwellingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
					modDataTypes.Statics.DWL = temp;
					temp = modDataTypes.Statics.CMR;
					modREMain.SaveCommercialTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
					modDataTypes.Statics.CMR = temp;
					temp = modDataTypes.Statics.OUT;
					modREMain.SaveOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
					modDataTypes.Statics.OUT = temp;
					if (boolExemptionChanged)
					{
						modGlobalRoutines.SplitExemption(ref modGlobalVariables.Statics.gintLastAccountNumber);
						temp = modDataTypes.Statics.MR;
						modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
						modDataTypes.Statics.MR = temp;
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			catch
			{
			}
		}

		public class StaticVariables
		{
			// vbPorter upgrade warning: BillingRate As double	OnWrite(string)
			//=========================================================
			public double BillingRate;
			// vbPorter upgrade warning: BillingYear As short --> As int	OnWrite(int, string)
			public int BillingYear;
			public bool Searching;
			public int Crd;
			public int CrdCnt;
			public string AddUpdDelCode = string.Empty;
			public bool Adding;
			public bool Deleting;
			public bool SearchNext;
			public bool NextCard;
			public bool PreviousCard;
			public bool SearchPrevious;
			public bool gboolAddAccount;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
