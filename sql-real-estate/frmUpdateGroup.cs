﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmUpdateGroup.
	/// </summary>
	public partial class frmUpdateGroup : BaseForm
	{
		public frmUpdateGroup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUpdateGroup InstancePtr
		{
			get
			{
				return (frmUpdateGroup)Sys.GetInstance(typeof(frmUpdateGroup));
			}
		}

		protected frmUpdateGroup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const int AccountCol = 1;
		const int TYPECOL = 0;
		const int NameCol = 2;
		const int GROUPCOL = 3;
		const int PRIMARYCOL = 4;
		const int RECOL = 5;
		bool boolHaveRe;
		bool boolHaveUT;
		bool boolHavePP;
		bool boolFilling;
		int lngGroupID;
		int lngREAccount;

		private void frmUpdateGroup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuSave_Click();
			}
		}

		private void frmUpdateGroup_Load(object sender, System.EventArgs e)
		{

			modGlobalFunctions.SetFixedSize(this);
		}

		private void frmUpdateGroup_Resize(object sender, System.EventArgs e)
		{
			ResizeGrids();
		}

		private void GridDest_OLEDragDrop(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.Copy;
			GridDest.AddItem(FCConvert.ToString(e.Data.GetData(FCConvert.ToString(TextDataFormat.Text))));
		}

		private void GridDest_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = GridDest.GetFlexRowIndex(e.RowIndex);
			int col = GridDest.GetFlexColIndex(e.ColumnIndex);
			clsDRWrapper clsTemp = new clsDRWrapper();
			if (row < 2)
				return;
			if (row > GridDest.Rows - 1)
				return;
			if (col == RECOL)
			{
				// make sure this isn't a RE account
				if (Conversion.Val(GridDest.EditText) < 1)
				{
					GridDest.TextMatrix(row, PRIMARYCOL, FCConvert.ToString(false));
					return;
				}
				if (GridDest.TextMatrix(row, TYPECOL) == "RE")
				{
					e.Cancel = true;
					MessageBox.Show("Can't associate real estate accounts with real estate accounts.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// see if it exists
				clsTemp.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(Conversion.Val(GridDest.EditText)) + " and rscard = 1 and not rsdeleted = 1", modGlobalVariables.strREDatabase);
				if (clsTemp.EndOfFile())
				{
					MessageBox.Show("This account either doesn't exist or is deleted.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					return;
				}
				// make sure it's not in another group
				clsTemp.OpenRecordset("select * from moduleassociation where account = " + GridDest.EditText + " and module = 'RE' and GROUPNUMBER <> " + FCConvert.ToString(lngGroupID), "CentralData");
				if (!clsTemp.EndOfFile())
				{
					e.Cancel = true;
					MessageBox.Show("This RE account is already in group number " + clsTemp.Get_Fields_Int32("groupnumber"), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
		}

		private void gridSource_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int intloop;
			gridSource.Editable = FCGrid.EditableSettings.flexEDNone;
			if (gridSource.MouseRow > 1 && gridSource.MouseRow < gridSource.Rows)
			{
				gridSource.Row = gridSource.MouseRow;
				gridSource.Col = gridSource.MouseCol;
				if (e.Button == MouseButtons.Left)
				{
					switch (gridSource.Col)
					{
						case GROUPCOL:
							{
								gridSource.Editable = FCGrid.EditableSettings.flexEDNone;
								if (Strings.Trim(gridSource.TextMatrix(gridSource.Row, GROUPCOL)) != string.Empty)
								{
									if (FCConvert.CBool(gridSource.TextMatrix(gridSource.Row, GROUPCOL)) == true)
									{
										for (intloop = 2; intloop <= gridSource.Rows - 1; intloop++)
										{
											if (intloop != gridSource.Row)
												gridSource.TextMatrix(intloop, GROUPCOL, FCConvert.ToString(false));
										}
										// intloop
										MessageBox.Show("You must have a group primary account.  Make another account the primary to de-select this one.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
									else
									{
										// make this one true then set the original primary to false
										if (Conversion.Val(gridSource.TextMatrix(gridSource.Row, AccountCol)) < 1)
										{
											// nothing to make primary
											return;
										}
										gridSource.TextMatrix(gridSource.Row, GROUPCOL, FCConvert.ToString(true));
										for (intloop = 2; intloop <= gridSource.Rows - 1; intloop++)
										{
											if (intloop != gridSource.Row)
												gridSource.TextMatrix(intloop, GROUPCOL, FCConvert.ToString(false));
										}
										// intloop
										return;
									}
								}
								else
								{
									// make this one true then set the original primary to false
									gridSource.TextMatrix(gridSource.Row, GROUPCOL, FCConvert.ToString(true));
									for (intloop = 2; intloop <= gridSource.Rows - 1; intloop++)
									{
										if (intloop != gridSource.Row)
											gridSource.TextMatrix(intloop, GROUPCOL, FCConvert.ToString(false));
									}
									// intloop
									return;
								}
								break;
							}
						case PRIMARYCOL:
							{
								gridSource.Editable = FCGrid.EditableSettings.flexEDNone;
								// don't worry if they have no primary
								if (Conversion.Val(gridSource.TextMatrix(gridSource.Row, AccountCol)) < 1)
								{
									return;
								}
								// can't be primary if there is no association to be primary of
								if (Conversion.Val(gridSource.TextMatrix(gridSource.Row, RECOL)) < 1)
								{
									return;
								}
								if (gridSource.TextMatrix(gridSource.Row, TYPECOL) == "RE")
								{
									// can't associate a RE account with an RE account
									return;
								}
								if (gridSource.TextMatrix(gridSource.Row, PRIMARYCOL) == string.Empty)
								{
									for (intloop = 2; intloop <= gridSource.Rows - 1; intloop++)
									{
										if ((intloop != gridSource.Row) && (Conversion.Val(gridSource.TextMatrix(intloop, RECOL)) == Conversion.Val(gridSource.TextMatrix(gridSource.Row, RECOL))))
										{
											if (Strings.Trim(gridSource.TextMatrix(intloop, PRIMARYCOL)) != string.Empty)
											{
												if (FCConvert.CBool(gridSource.TextMatrix(intloop, PRIMARYCOL)) == true)
												{
													gridSource.TextMatrix(intloop, PRIMARYCOL, FCConvert.ToString(false));
													break;
												}
											}
										}
									}
									// intloop
									gridSource.TextMatrix(gridSource.Row, PRIMARYCOL, FCConvert.ToString(true));
								}
								else
								{
									if (FCConvert.CBool(gridSource.TextMatrix(gridSource.Row, PRIMARYCOL)) == false)
									{
										// check to see if there is another that is already primary
										for (intloop = 2; intloop <= gridSource.Rows - 1; intloop++)
										{
											if ((intloop != gridSource.Row) && (Conversion.Val(gridSource.TextMatrix(intloop, RECOL)) == Conversion.Val(gridSource.TextMatrix(gridSource.Row, RECOL))))
											{
												if (Strings.Trim(gridSource.TextMatrix(intloop, PRIMARYCOL)) != string.Empty)
												{
													if (FCConvert.CBool(gridSource.TextMatrix(intloop, PRIMARYCOL)) == true)
													{
														gridSource.TextMatrix(intloop, PRIMARYCOL, FCConvert.ToString(false));
														break;
													}
												}
											}
										}
										// intloop
										gridSource.TextMatrix(gridSource.Row, PRIMARYCOL, FCConvert.ToString(true));
									}
									else
									{
										gridSource.TextMatrix(gridSource.Row, PRIMARYCOL, FCConvert.ToString(false));
									}
								}
								break;
							}
						case RECOL:
							{
								gridSource.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								break;
							}
					}
					//end switch
				}
			}
			else
			{
				gridSource.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void GridDest_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int intloop;
			GridDest.Editable = FCGrid.EditableSettings.flexEDNone;
			if (GridDest.MouseRow > 1 && GridDest.MouseRow < GridDest.Rows)
			{
				GridDest.Row = GridDest.MouseRow;
				GridDest.Col = GridDest.MouseCol;
				if (e.Button == MouseButtons.Left)
				{
					switch (GridDest.Col)
					{
						case GROUPCOL:
							{
								GridDest.Editable = FCGrid.EditableSettings.flexEDNone;
								if (Strings.Trim(GridDest.TextMatrix(GridDest.Row, GROUPCOL)) != string.Empty)
								{
									if (FCConvert.CBool(GridDest.TextMatrix(GridDest.Row, GROUPCOL)) == true)
									{
										for (intloop = 2; intloop <= GridDest.Rows - 1; intloop++)
										{
											if (intloop != GridDest.Row)
												GridDest.TextMatrix(intloop, GROUPCOL, FCConvert.ToString(false));
										}
										// intloop
										MessageBox.Show("You must have a group primary account.  Make another account the primary to de-select this one.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
									else
									{
										// make this one true then set the original primary to false
										if (Conversion.Val(GridDest.TextMatrix(GridDest.Row, AccountCol)) < 1)
										{
											// nothing to make primary
											return;
										}
										GridDest.TextMatrix(GridDest.Row, GROUPCOL, FCConvert.ToString(true));
										for (intloop = 2; intloop <= GridDest.Rows - 1; intloop++)
										{
											if (intloop != GridDest.Row)
												GridDest.TextMatrix(intloop, GROUPCOL, FCConvert.ToString(false));
										}
										// intloop
										return;
									}
								}
								else
								{
									// make this one true then set the original primary to false
									GridDest.TextMatrix(GridDest.Row, GROUPCOL, FCConvert.ToString(true));
									for (intloop = 2; intloop <= GridDest.Rows - 1; intloop++)
									{
										if (intloop != GridDest.Row)
											GridDest.TextMatrix(intloop, GROUPCOL, FCConvert.ToString(false));
									}
									// intloop
									return;
								}
								break;
							}
						case PRIMARYCOL:
							{
								GridDest.Editable = FCGrid.EditableSettings.flexEDNone;
								// don't worry if they have no primary
								if (Conversion.Val(GridDest.TextMatrix(GridDest.Row, AccountCol)) < 1)
								{
									return;
								}
								// can't be primary if there is no association to be primary of
								if (Conversion.Val(GridDest.TextMatrix(GridDest.Row, RECOL)) < 1)
								{
									return;
								}
								if (GridDest.TextMatrix(GridDest.Row, TYPECOL) == "RE")
								{
									// can't associate a RE account with an RE account
									return;
								}
								if (GridDest.TextMatrix(GridDest.Row, PRIMARYCOL) == string.Empty)
								{
									for (intloop = 2; intloop <= GridDest.Rows - 1; intloop++)
									{
										if ((intloop != GridDest.Row) && (Conversion.Val(GridDest.TextMatrix(intloop, RECOL)) == Conversion.Val(GridDest.TextMatrix(GridDest.Row, RECOL))))
										{
											if (Strings.Trim(GridDest.TextMatrix(intloop, PRIMARYCOL)) != string.Empty)
											{
												if (FCConvert.CBool(GridDest.TextMatrix(intloop, PRIMARYCOL)) == true)
												{
													GridDest.TextMatrix(intloop, PRIMARYCOL, FCConvert.ToString(false));
													break;
												}
											}
										}
									}
									// intloop
									GridDest.TextMatrix(GridDest.Row, PRIMARYCOL, FCConvert.ToString(true));
								}
								else
								{
									if (FCConvert.CBool(GridDest.TextMatrix(GridDest.Row, PRIMARYCOL)) == false)
									{
										// check to see if there is another that is already primary
										for (intloop = 2; intloop <= GridDest.Rows - 1; intloop++)
										{
											if ((intloop != GridDest.Row) && (Conversion.Val(GridDest.TextMatrix(intloop, RECOL)) == Conversion.Val(GridDest.TextMatrix(GridDest.Row, RECOL))))
											{
												if (Strings.Trim(GridDest.TextMatrix(intloop, PRIMARYCOL)) != string.Empty)
												{
													if (FCConvert.CBool(GridDest.TextMatrix(intloop, PRIMARYCOL)) == true)
													{
														GridDest.TextMatrix(intloop, PRIMARYCOL, FCConvert.ToString(false));
														break;
													}
												}
											}
										}
										// intloop
										GridDest.TextMatrix(GridDest.Row, PRIMARYCOL, FCConvert.ToString(true));
									}
									else
									{
										GridDest.TextMatrix(GridDest.Row, PRIMARYCOL, FCConvert.ToString(false));
									}
								}
								break;
							}
						case RECOL:
							{
								GridDest.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								break;
							}
					}
					//end switch
				}
			}
			else
			{
				GridDest.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}
		//private void gridSource_OLECompleteDrag(object sender, EventArgs e)
		//{
		//	if (e.effect==DragDropEffects.Copy) {
		//		gridSource.RemoveItem(gridSource.Row);
		//	}
		//}
		// vbPorter upgrade warning: Effect As int	OnWrite(DragDropEffects)
		private void gridSource_OLEDragDrop(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.Copy;
			gridSource.AddItem(e.Data.GetData(TextDataFormat.Text.ToString()).ToString());
		}
		// vbPorter upgrade warning: AllowedEffects As int	OnWrite(DragDropEffects)
		//private void gridSource_OLEStartDrag(object sender, EventArgs e)
		//{
		//	e.allowedEffects = FCConvert.ToInt32(DragDropEffects.Copy;
		//	e.Data.SetData(gridSource.TextMatrix(gridSource.Row, 0)+"\t"+gridSource.TextMatrix(gridSource.Row, 1)+"\t"+gridSource.TextMatrix(gridSource.Row, 2)+"\t"+gridSource.TextMatrix(gridSource.Row, 3)+"\t"+gridSource.TextMatrix(gridSource.Row, 4)+"\t"+gridSource.TextMatrix(gridSource.Row, 5), TextDataFormat.Text);
		//}
		private void gridSource_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = gridSource.GetFlexRowIndex(e.RowIndex);
			int col = gridSource.GetFlexColIndex(e.ColumnIndex);
			clsDRWrapper clsTemp = new clsDRWrapper();
			if (row < 2)
				return;
			if (row > gridSource.Rows - 1)
				return;
			if (col == RECOL)
			{
				// make sure this isn't a RE account
				if (Conversion.Val(gridSource.EditText) < 1)
				{
					gridSource.TextMatrix(row, PRIMARYCOL, FCConvert.ToString(false));
					return;
				}
				if (gridSource.TextMatrix(row, TYPECOL) == "RE")
				{
					e.Cancel = true;
					MessageBox.Show("Can't associate real estate accounts with real estate accounts.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// see if it exists
				clsTemp.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(Conversion.Val(gridSource.EditText)) + " and rscard = 1 and not rsdeleted = 1", modGlobalVariables.strREDatabase);
				if (clsTemp.EndOfFile())
				{
					MessageBox.Show("This account either doesn't exist or is deleted.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					return;
				}
				// make sure it's not in another group
				clsTemp.OpenRecordset("select * from moduleassociation where account = " + gridSource.EditText + " and module = 'RE' and GROUPNUMBER <> " + FCConvert.ToString(lngGroupID), "CentralData");
				if (!clsTemp.EndOfFile())
				{
					e.Cancel = true;
					MessageBox.Show("This RE account is already in group number " + clsTemp.Get_Fields_Int32("groupnumber"), null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
		}

		private void mnuExit_Click()
		{
			this.Unload();
		}

		private void SetupGrids()
		{
			gridSource.Rows = 2;
			gridSource.FixedRows = 2;
			gridSource.ExtendLastCol = true;
			gridSource.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridSource.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridSource.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridSource.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridSource.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridSource.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridSource.ColDataType(3, FCGrid.DataTypeSettings.flexDTBoolean);
			gridSource.ColDataType(4, FCGrid.DataTypeSettings.flexDTBoolean);
			gridSource.TextMatrix(0, 3, "Group");
			gridSource.TextMatrix(0, 4, "Primary");
			gridSource.TextMatrix(0, 5, "Associated");
			gridSource.TextMatrix(1, 0, "Type");
			gridSource.TextMatrix(1, 1, "Acct");
			gridSource.TextMatrix(1, 2, "Name");
			gridSource.TextMatrix(1, 3, "Primary");
			gridSource.TextMatrix(1, 4, "Associate");
			gridSource.TextMatrix(1, 5, "RE Account");
			GridDest.Rows = 2;
			GridDest.FixedRows = 2;
			GridDest.ExtendLastCol = true;
			GridDest.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDest.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDest.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDest.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDest.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDest.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridDest.ColDataType(3, FCGrid.DataTypeSettings.flexDTBoolean);
			GridDest.ColDataType(4, FCGrid.DataTypeSettings.flexDTBoolean);
			GridDest.TextMatrix(0, 3, "Group");
			GridDest.TextMatrix(0, 4, "Primary");
			GridDest.TextMatrix(0, 5, "Associated");
			GridDest.TextMatrix(1, 0, "Type");
			GridDest.TextMatrix(1, 1, "Acct");
			GridDest.TextMatrix(1, 2, "Name");
			GridDest.TextMatrix(1, 3, "Primary");
			GridDest.TextMatrix(1, 4, "Associate");
			GridDest.TextMatrix(1, 5, "RE Account");
		}

		private void ResizeGrids()
		{
			int GridWidth = 0;
			GridWidth = gridSource.WidthOriginal;
			gridSource.ColWidth(AccountCol, FCConvert.ToInt32(0.1 * GridWidth));
			gridSource.ColWidth(TYPECOL, FCConvert.ToInt32(0.08 * GridWidth));
			gridSource.ColWidth(NameCol, FCConvert.ToInt32(0.46 * GridWidth));
			gridSource.ColWidth(GROUPCOL, FCConvert.ToInt32(0.1 * GridWidth));
			gridSource.ColWidth(PRIMARYCOL, FCConvert.ToInt32(0.12 * GridWidth));
			gridSource.ColWidth(RECOL, FCConvert.ToInt32(0.1 * GridWidth));
			GridWidth = GridDest.WidthOriginal;
			GridDest.ColWidth(AccountCol, FCConvert.ToInt32(0.1 * GridWidth));
			GridDest.ColWidth(TYPECOL, FCConvert.ToInt32(0.08 * GridWidth));
			GridDest.ColWidth(NameCol, FCConvert.ToInt32(0.46 * GridWidth));
			GridDest.ColWidth(GROUPCOL, FCConvert.ToInt32(0.1 * GridWidth));
			GridDest.ColWidth(PRIMARYCOL, FCConvert.ToInt32(0.12 * GridWidth));
			GridDest.ColWidth(RECOL, FCConvert.ToInt32(0.1 * GridWidth));
		}

		private void FillGrid()
		{
			clsDRWrapper clsGroup = new clsDRWrapper();
			clsDRWrapper clsModule = new clsDRWrapper();
			bool boolUseName = false;
			int intGroupPrimaryRow;
			bool boolUseDestGrid = false;
			string strModule = "";
			int lngCurAcct = 0;
			// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
			cParty tParty = new cParty();
			cPartyController tCont = new cPartyController();
			try
			{
				// On Error GoTo ErrorHandler
				boolFilling = true;
				// get every account in group and put them in grid in order of module type
				intGroupPrimaryRow = -1;
				clsGroup.OpenRecordset("select * from moduleassociation where groupnumber = " + FCConvert.ToString(lngGroupID) + " order by GROUPPRIMARY,primaryassociate,module,account", "CentralData");
				while (!clsGroup.EndOfFile())
				{
					boolUseDestGrid = false;
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					lngCurAcct = FCConvert.ToInt32(clsGroup.Get_Fields("account"));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					if (FCConvert.ToString(clsGroup.Get_Fields_String("module")) == "RE" && FCConvert.ToInt32(clsGroup.Get_Fields("account")) == lngREAccount)
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						GridDest.AddItem(clsGroup.Get_Fields_String("module") + "\t" + clsGroup.Get_Fields("account") + "\t" + "\t" + FCConvert.ToString(FCConvert.CBool(clsGroup.Get_Fields_Boolean("groupprimary"))) + "\t" + FCConvert.ToString(FCConvert.CBool(clsGroup.Get_Fields_Boolean("PrimaryAssociate"))) + "\t" + clsGroup.Get_Fields_Int32("remasteracct"));
						GridDest.TextMatrix(GridDest.Rows - 1, GROUPCOL, FCConvert.ToString(true));
						boolUseDestGrid = true;
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						gridSource.AddItem(clsGroup.Get_Fields_String("module") + "\t" + clsGroup.Get_Fields("account") + "\t" + "\t" + FCConvert.ToString(FCConvert.CBool(clsGroup.Get_Fields_Boolean("groupprimary"))) + "\t" + FCConvert.ToString(FCConvert.CBool(clsGroup.Get_Fields_Boolean("PrimaryAssociate"))) + "\t" + clsGroup.Get_Fields_Int32("remasteracct"));
						if (FCConvert.CBool(gridSource.TextMatrix(gridSource.Rows - 1, GROUPCOL)) == true)
						{
							intGroupPrimaryRow = gridSource.Rows - 1;
						}
					}
					// now fill in the name
					boolUseName = true;
					if (boolUseDestGrid)
					{
						strModule = GridDest.TextMatrix(GridDest.Rows - 1, TYPECOL);
					}
					else
					{
						strModule = gridSource.TextMatrix(gridSource.Rows - 1, TYPECOL);
					}
					tParty = null;
					if (Strings.UCase(strModule) == "RE")
					{
						if (boolHaveRe)
						{
							clsModule.OpenRecordset("select ownerpartyid as thepartyid from master where rsaccount = " + FCConvert.ToString(lngCurAcct) + " and rscard = 1", modGlobalVariables.strREDatabase);
						}
						else
						{
							boolUseName = false;
						}
					}
					else if (Strings.UCase(strModule) == "PP")
					{
						if (boolHavePP)
						{
							clsModule.OpenRecordset("select partyid as thepartyid from ppmaster where account = " + FCConvert.ToString(lngCurAcct), modGlobalVariables.strPPDatabase);
						}
						else
						{
							boolUseName = false;
						}
					}
					else if (Strings.UCase(strModule) == "UT")
					{
						if (boolHaveUT)
						{
							clsModule.OpenRecordset("select ownerpartyid as thepartyid from master where accountnumber = " + FCConvert.ToString(lngCurAcct), modGlobalVariables.strUTDatabase);
						}
						else
						{
							boolUseName = false;
						}
					}
					if (boolUseName && !clsModule.EndOfFile())
					{
						// TODO Get_Fields: Field [thepartyid] not found!! (maybe it is an alias?)
						tParty = tCont.GetParty(clsModule.Get_Fields("thepartyid"), true);
					}
					if (boolUseName && !(tParty == null))
					{
						if (boolUseDestGrid)
						{
							GridDest.TextMatrix(GridDest.Rows - 1, NameCol, tParty.FullNameLastFirst);
						}
						else
						{
							gridSource.TextMatrix(gridSource.Rows - 1, NameCol, tParty.FullNameLastFirst);
						}
					}
					else
					{
						if (boolUseDestGrid)
						{
							GridDest.TextMatrix(GridDest.Rows - 1, NameCol, "Record or Database Missing");
						}
						else
						{
							gridSource.TextMatrix(gridSource.Rows - 1, NameCol, "Record or Database Missing");
						}
					}
					clsGroup.MoveNext();
				}
				if (intGroupPrimaryRow == -1)
				{
					// no primary for some reason so assign one
					intGroupPrimaryRow = 2;
					gridSource.TextMatrix(2, GROUPCOL, FCConvert.ToString(true));
				}
				boolFilling = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				boolFilling = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Fill Accounts Grid", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void Init(int lngGroup, ref int lngREAcct)
		{
			clsDRWrapper tDR = new clsDRWrapper();
			lngGroupID = lngGroup;
			lngREAccount = lngREAcct;
			if (!tDR.DBExists("RealEstate"))
			{
				boolHaveRe = false;
			}
			else
			{
				boolHaveRe = true;
			}
			if (!tDR.DBExists("PersonalProperty"))
			{
				boolHavePP = false;
			}
			else
			{
				boolHavePP = true;
			}
			if (!tDR.DBExists("UtilityBilling"))
			{
				boolHaveUT = false;
			}
			else
			{
				boolHaveUT = true;
			}
			SetupGrids();
			FillGrid();
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsCheck = new clsDRWrapper();
			int x;
			// loop counters
			int y;
			int intNumGPrimaries = 0;
			// count the number of group primaries
			string strSQL = "";
			// for insert statement
			try
			{
				// On Error GoTo ErrorHandler
				if (!CheckREAssociations())
				{
					return;
				}
				// check for group primaries
				// check for valid accounts as well
				intNumGPrimaries = 0;
				for (x = 2; x <= gridSource.Rows - 1; x++)
				{
					if (gridSource.TextMatrix(x, GROUPCOL) != string.Empty)
					{
						if (FCConvert.CBool(gridSource.TextMatrix(x, GROUPCOL)) == true)
							intNumGPrimaries += 1;
					}
					if (gridSource.TextMatrix(x, TYPECOL) == "RE")
					{
						// force correctness
						gridSource.TextMatrix(x, PRIMARYCOL, FCConvert.ToString(false));
						gridSource.TextMatrix(x, RECOL, "");
					}
					if (gridSource.TextMatrix(x, TYPECOL) == "")
					{
						MessageBox.Show("You have at least one row that has invalid data.  The account type is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (Conversion.Val(gridSource.TextMatrix(x, AccountCol)) < 1)
					{
						MessageBox.Show("You have at least one row that has invalid data. The account number is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					// check to see if this account is listed in another group
					clsCheck.OpenRecordset("select * from moduleassociation where account = " + FCConvert.ToString(Conversion.Val(gridSource.TextMatrix(x, AccountCol))) + " and module = '" + gridSource.TextMatrix(x, TYPECOL) + "' and groupnumber <> " + FCConvert.ToString(lngGroupID), "CentralData");
					if (!clsCheck.EndOfFile())
					{
						MessageBox.Show(gridSource.TextMatrix(x, TYPECOL) + " account " + gridSource.TextMatrix(x, AccountCol) + " is already in another group.  Cannot continue saving.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (FCConvert.CBool(gridSource.TextMatrix(x, PRIMARYCOL)) == true)
					{
						// check all other entries to see if there is a conflict in primary accounts
						for (y = x + 1; y <= gridSource.Rows - 1; y++)
						{
							if (gridSource.TextMatrix(y, PRIMARYCOL) != string.Empty)
							{
								if (FCConvert.CBool(gridSource.TextMatrix(y, PRIMARYCOL)) == true)
								{
									if (Conversion.Val(gridSource.TextMatrix(y, RECOL)) == Conversion.Val(gridSource.TextMatrix(x, RECOL)))
									{
										if (gridSource.TextMatrix(y, TYPECOL) == gridSource.TextMatrix(x, TYPECOL))
										{
											// Two primary accounts for the same re association with the same module type
											MessageBox.Show("You have two primary accounts of type " + gridSource.TextMatrix(x, TYPECOL) + " for RE account " + FCConvert.ToString(Conversion.Val(gridSource.TextMatrix(x, RECOL))) + ". Unable to continue saving.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
											return;
										}
									}
								}
							}
						}
						// y
					}
				}
				// x
				if (intNumGPrimaries > 1)
				{
					MessageBox.Show("You can only have one group primary. Cannot continue saving.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				
				clsCheck.Execute("delete * from moduleassociation where groupnumber = " + FCConvert.ToString(lngGroupID), "CentralData");
				for (x = 2; x <= gridSource.Rows - 1; x++)
				{
					strSQL = "(";
					strSQL += FCConvert.ToString(lngGroupID);
					strSQL += "," + FCConvert.ToString(Conversion.Val(gridSource.TextMatrix(x, AccountCol)));
					strSQL += ",'" + gridSource.TextMatrix(x, TYPECOL) + "'";
					if (gridSource.TextMatrix(x, GROUPCOL) != string.Empty)
					{
						strSQL += "," + FCConvert.ToString(FCConvert.CBool(gridSource.TextMatrix(x, GROUPCOL)));
					}
					else
					{
						strSQL += "," + FCConvert.ToString(FCConvert.CBool(false));
					}
					if (gridSource.TextMatrix(x, PRIMARYCOL) != string.Empty)
					{
						strSQL += "," + FCConvert.ToString(FCConvert.CBool(gridSource.TextMatrix(x, PRIMARYCOL)));
					}
					else
					{
						strSQL += "," + FCConvert.ToString(FCConvert.CBool(false));
					}
					strSQL += "," + FCConvert.ToString(Conversion.Val(gridSource.TextMatrix(x, RECOL)));
					strSQL += ")";
					clsCheck.Execute("Insert into moduleassociation (groupnumber,account,module,groupprimary,primaryassociate,remasteracct) values " + strSQL, "CentralData");
				}
				// x
				lngGroupID = 0;
				// check for group primaries
				// check for valid accounts as well
				intNumGPrimaries = 0;
				for (x = 2; x <= GridDest.Rows - 1; x++)
				{
					if (GridDest.TextMatrix(x, GROUPCOL) != string.Empty)
					{
						if (FCConvert.CBool(GridDest.TextMatrix(x, GROUPCOL)) == true)
							intNumGPrimaries += 1;
					}
					if (GridDest.TextMatrix(x, TYPECOL) == "RE")
					{
						// force correctness
						GridDest.TextMatrix(x, PRIMARYCOL, FCConvert.ToString(false));
						GridDest.TextMatrix(x, RECOL, "");
					}
					if (GridDest.TextMatrix(x, TYPECOL) == "")
					{
						MessageBox.Show("You have at least one row that has invalid data.  The account type is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (Conversion.Val(GridDest.TextMatrix(x, AccountCol)) < 1)
					{
						MessageBox.Show("You have at least one row that has invalid data. The account number is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					// check to see if this account is listed in another group
					clsCheck.OpenRecordset("select * from moduleassociation where account = " + FCConvert.ToString(Conversion.Val(GridDest.TextMatrix(x, AccountCol))) + " and module = '" + GridDest.TextMatrix(x, TYPECOL) + "' and groupnumber <> " + FCConvert.ToString(lngGroupID), "CentralData");
					if (!clsCheck.EndOfFile())
					{
						MessageBox.Show(GridDest.TextMatrix(x, TYPECOL) + " account " + GridDest.TextMatrix(x, AccountCol) + " is already in another group.  Cannot continue saving.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (FCConvert.CBool(GridDest.TextMatrix(x, PRIMARYCOL)) == true)
					{
						// check all other entries to see if there is a conflict in primary accounts
						for (y = x + 1; y <= GridDest.Rows - 1; y++)
						{
							if (GridDest.TextMatrix(y, PRIMARYCOL) != string.Empty)
							{
								if (FCConvert.CBool(GridDest.TextMatrix(y, PRIMARYCOL)) == true)
								{
									if (Conversion.Val(GridDest.TextMatrix(y, RECOL)) == Conversion.Val(GridDest.TextMatrix(x, RECOL)))
									{
										if (GridDest.TextMatrix(y, TYPECOL) == GridDest.TextMatrix(x, TYPECOL))
										{
											// Two primary accounts for the same re association with the same module type
											MessageBox.Show("You have two primary accounts of type " + GridDest.TextMatrix(x, TYPECOL) + " for RE account " + FCConvert.ToString(Conversion.Val(GridDest.TextMatrix(x, RECOL))) + ". Unable to continue saving.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
											return;
										}
									}
								}
							}
						}
						// y
					}
				}
				// x
				if (intNumGPrimaries > 1)
				{
					MessageBox.Show("You can only have one group primary. Cannot continue saving.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// should be safe to save now
				if (lngGroupID == 0)
				{
					clsCheck.OpenRecordset("select * from groupmaster where id = -1", "CentralData");
					clsCheck.AddNew();
					// If Val(txtGroupNumber.Text) > 0 Then
					// clsCheck.Fields("groupnumber") = Val(txtGroupNumber.Text)
					// Else
					clsCheck.Set_Fields("groupnumber", clsCheck.Get_Fields_Int32("id"));
					// txtGroupNumber.Text = clsCheck.Fields("id")
					// End If
					lngGroupID = FCConvert.ToInt32(clsCheck.Get_Fields_Int32("id"));
					clsCheck.Update();
				}
				// save comment if any
				// Call clsCheck.OpenRecordset("select * from groupmaster where id = " & lngGroupID, strgndatabase)
				// clsCheck.Edit
				// clsCheck.Fields("comment") = Trim(rtbComment.Text)
				// If Val(txtGroupNumber.Text) > 0 Then
				// clsCheck.Fields("groupnumber") = Val(txtGroupNumber.Text)
				// End If
				// clsCheck.Update
				// Clear all previous info for this group and replace it with new stuff
				clsCheck.Execute("delete * from moduleassociation where groupnumber = " + FCConvert.ToString(lngGroupID), "CentralData");
				for (x = 2; x <= GridDest.Rows - 1; x++)
				{
					strSQL = "(";
					strSQL += FCConvert.ToString(lngGroupID);
					strSQL += "," + FCConvert.ToString(Conversion.Val(GridDest.TextMatrix(x, AccountCol)));
					strSQL += ",'" + GridDest.TextMatrix(x, TYPECOL) + "'";
					if (GridDest.TextMatrix(x, GROUPCOL) != string.Empty)
					{
						strSQL += "," + FCConvert.ToString(FCConvert.CBool(GridDest.TextMatrix(x, GROUPCOL)));
					}
					else
					{
						strSQL += "," + FCConvert.ToString(FCConvert.CBool(false));
					}
					if (GridDest.TextMatrix(x, PRIMARYCOL) != string.Empty)
					{
						strSQL += "," + FCConvert.ToString(FCConvert.CBool(GridDest.TextMatrix(x, PRIMARYCOL)));
					}
					else
					{
						strSQL += "," + FCConvert.ToString(FCConvert.CBool(false));
					}
					strSQL += "," + FCConvert.ToString(Conversion.Val(GridDest.TextMatrix(x, RECOL)));
					strSQL += ")";
					clsCheck.Execute("Insert into moduleassociation (groupnumber,account,module,groupprimary,primaryassociate,remasteracct) values " + strSQL, "CentralData");
				}
				// x
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In save", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(cmdSave, new System.EventArgs());
		}

		private bool CheckREAssociations()
		{
			bool CheckREAssociations = false;
			int x;
			int y;
			CheckREAssociations = false;
			for (x = 2; x <= gridSource.Rows - 1; x++)
			{
				for (y = 2; y <= GridDest.Rows - 1; y++)
				{
					if (Strings.UCase(gridSource.TextMatrix(x, TYPECOL)) == "RE")
					{
						if (Conversion.Val(GridDest.TextMatrix(y, RECOL)) == Conversion.Val(gridSource.TextMatrix(x, AccountCol)))
						{
							MessageBox.Show("An account in one group is associated with an RE account in the other group." + "\r\n" + "Cannot continue saving.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return CheckREAssociations;
						}
					}
					else if ((Strings.UCase(gridSource.TextMatrix(x, TYPECOL)) == "PP") || (Strings.UCase(gridSource.TextMatrix(x, TYPECOL)) == "UT"))
					{
						if (Conversion.Val(gridSource.TextMatrix(x, RECOL)) > 0)
						{
							if (Strings.UCase(GridDest.TextMatrix(y, TYPECOL)) == "RE")
							{
								if (Conversion.Val(GridDest.TextMatrix(y, AccountCol)) == Conversion.Val(gridSource.TextMatrix(x, RECOL)))
								{
									MessageBox.Show("An account in one group is associated with an RE account in the other group." + "\r\n" + "Cannot continue saving.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return CheckREAssociations;
								}
							}
							else if ((Strings.UCase(GridDest.TextMatrix(y, TYPECOL)) == "PP") || (Strings.UCase(GridDest.TextMatrix(y, TYPECOL)) == "UT"))
							{
								if (Conversion.Val(GridDest.TextMatrix(y, RECOL)) == Conversion.Val(gridSource.TextMatrix(x, RECOL)))
								{
									MessageBox.Show("Accounts in both groups are trying to associate with the same account." + "\r\n" + "Cannot continue saving.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return CheckREAssociations;
								}
							}
						}
					}
				}
				// y
			}
			// x
			CheckREAssociations = true;
			return CheckREAssociations;
		}
	}
}
