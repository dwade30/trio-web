﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmExportREWeb.
	/// </summary>
	public partial class frmExportREWeb : BaseForm
	{
		public frmExportREWeb()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExportREWeb InstancePtr
		{
			get
			{
				return (frmExportREWeb)Sys.GetInstance(typeof(frmExportREWeb));
			}
		}

		protected frmExportREWeb _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		string strExportDataPath = "";
		clsProperty PropertyRec = new clsProperty();
		int lngCurrentPropID;
		int lngCurrentOwnerID;
		int intTaxType;
		private bool boolLoading;
		const int CNSTGRIDCOLAUTOID = 0;
		const int CNSTGRIDCOLACCOUNT = 1;
		const int CNSTGRIDCOLMAPLOT = 2;
		const int CNSTGRIDCOLOWNER = 3;
		const int CNSTGRIDCOLALL = 4;
		const int CNSTGRIDCOLPICTURES = 5;
		const int CNSTGRIDCOLADDRESS = 6;
		const int CNSTGRIDCOLLOCATION = 7;
		const int CNSTGRIDCOLNAME = 8;

		private void frmExportREWeb_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmExportREWeb_Load(object sender, System.EventArgs e)
		{

			try
			{
				// On Error GoTo ErrorHandler
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				SetupGridRecords();
				SetupGridTownCode();
				SetupGridType();
				SetupGrid();
				LoadGrid();
				if (modGlobalConstants.Statics.clsSecurityClass.Get_UserID() == -1)
				{
					// trio super user
					cmdSettings.Visible = true;
					//mnuSepar2.Visible = true;
				}
				if (modGlobalConstants.Statics.gboolBL)
				{
					cmbFrom.Text = "From Bills";
				}
				if (modGlobalConstants.Statics.gboolCL)
				{
					chkOutstanding.Visible = true;
					chkOutstanding.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkOutstanding.CheckState = Wisej.Web.CheckState.Unchecked;
					chkOutstanding.Visible = false;
				}
				if (modGlobalVariables.Statics.CustomizedInfo.ExportRef1ToWeb)
				{
					chkIncludeRef1.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkIncludeRef1.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (modGlobalVariables.Statics.CustomizedInfo.ExportRef2ToWeb)
				{
					chkIncludeRef2.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkIncludeRef2.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				txtRef1Caption.Text = modGlobalVariables.Statics.CustomizedInfo.Ref1Description;
				txtRef2Caption.Text = modGlobalVariables.Statics.CustomizedInfo.Ref2Description;
				FillPictureCombo();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Form_Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmExportREWeb_Resize(object sender, System.EventArgs e)
		{
			ResizeGridTownCode();
			ResizeGrid();
		}

		private void Grid_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (!boolLoading)
			{
				Grid.RowData(Grid.Row, true);
			}
		}

		private void GridRecords_ComboCloseUp(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWrite(string)
			int lngTemp;
			lngTemp = FCConvert.ToInt32(GridRecords.ComboData(GridRecords.ComboIndex));
			switch (lngTemp)
			{
				case 0:
					{
						// all
						framRange.Visible = false;
						break;
					}
				case 1:
					{
						// range
						framRange.Visible = true;
						// Case 2
						// from extract
						// framRange.Visible = False
						break;
					}
			}
			//end switch
		}

		private void GridType_ComboCloseUp(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWrite(string)
			int lngTemp;
			lngTemp = FCConvert.ToInt32(GridType.ComboData(GridType.ComboIndex));
			intTaxType = lngTemp;
			switch (lngTemp)
			{
				case 0:
					{
						// current
						lblTaxYear.Visible = false;
						txtTaxYear.Visible = false;
						// chkIncludePictures.Visible = True
						cmbIncludePictures.Visible = true;
						lblpictures.Visible = true;
						if (!modREMain.Statics.boolShortRealEstate)
						{
							cmbCurrent.Visible = true;
							//FC:FINAL:MSH - i.issue #1215: change visibility of label
							lblCurrent.Visible = true;
						}
						cmbFrom.Visible = false;
						//FC:FINAL:MSH - i.issue #1149: change the visibility of the label along with the combobox
						lblFrom.Visible = false;
						break;
					}
				case 1:
					{
						// tax commitment
						lblTaxYear.Visible = true;
						txtTaxYear.Visible = true;
						// chkIncludePictures.Visible = False
						cmbIncludePictures.Visible = false;
						lblpictures.Visible = false;
						cmbCurrent.Visible = false;
						//FC:FINAL:MSH - i.issue #1215: change visibility of label
						lblCurrent.Visible = false;
						if (modGlobalConstants.Statics.gboolBL)
						{
							cmbFrom.Visible = true;
							//FC:FINAL:MSH - i.issue #1149: change the visibility of the label along with the combobox
							lblFrom.Visible = true;
						}
						break;
					}
				case 2:
					{
						// both
						// tax commitment
						lblTaxYear.Visible = true;
						txtTaxYear.Visible = true;
						// chkIncludePictures.Visible = True
						cmbIncludePictures.Visible = true;
						lblpictures.Visible = true;
						if (!modREMain.Statics.boolShortRealEstate)
						{
							cmbCurrent.Visible = true;
							//FC:FINAL:MSH - i.issue #1215: change visibility of label
							lblCurrent.Visible = true;
						}
						if (modGlobalConstants.Statics.gboolBL)
						{
							cmbFrom.Visible = true;
							//FC:FINAL:MSH - i.issue #1149: change the visibility of the label along with the combobox
							lblFrom.Visible = true;
						}
						break;
					}
			}
			//end switch
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGridRecords()
		{
			//FC:FINAL:MSH - i.issue #1215: assign handler for correct work with combobox
			GridRecords.EditingControlShowing -= GridRecords_EditingControlShowing;
			GridRecords.EditingControlShowing += GridRecords_EditingControlShowing;
			GridRecords.Rows = 1;
			string strTemp = "";
			strTemp = "#0;All|#1;Range";
			GridRecords.ColComboList(0, strTemp);
			GridRecords.TextMatrix(0, 0, FCConvert.ToString(0));
		}

		private void GridRecords_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				var combobox = e.Control as ComboBox;
				if (combobox != null)
				{
					//FC:FINAL:MSH - i.issue #1215: assign handler to SelectedIndexChanged event for getting correct SelectedIndex
					combobox.SelectedIndexChanged -= GridRecords_ComboCloseUp;
					combobox.SelectedIndexChanged += GridRecords_ComboCloseUp;
				}
			}
		}

		private void SetupGridType()
		{
			//FC:FINAL:MSH - i.issue #1215: assign handler for correct work with combobox
			GridType.EditingControlShowing -= GridType_EditingControlShowing;
			GridType.EditingControlShowing += GridType_EditingControlShowing;
			GridType.Rows = 1;
			string strTemp = "";
			strTemp = "#0;Current|#1;Tax Commitment|#2;Both";
			GridType.ColComboList(0, strTemp);
			GridType.TextMatrix(0, 0, FCConvert.ToString(0));
		}

		private void GridType_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				var combobox = e.Control as ComboBox;
				if (combobox != null)
				{
					//FC:FINAL:MSH - i.issue #1215: assign handler to SelectedIndexChanged event for getting correct SelectedIndex
					combobox.SelectedIndexChanged -= GridType_ComboCloseUp;
					combobox.SelectedIndexChanged += GridType_ComboCloseUp;
				}
			}
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from tblTranCode where code > 0 order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = true;
			}
			else
			{
				gridTownCode.Rows = 1;
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = false;
			}
		}

		private void ResizeGridTownCode()
		{
			//FC:FINAL:MSH - i.issue #1149: using HeightOriginal property for correct resizing
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
			//gridTownCode.HeightOriginal = gridTownCode.RowHeight(0) + 60;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			if (SaveGrid())
			{
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveGrid())
			{
				if (CreateFiles())
				{
					this.Unload();
				}
			}
		}

		private void FillPictureCombo()
		{
			cmbIncludePictures.Clear();
			cmbIncludePictures.AddItem("None");
			cmbIncludePictures.AddItem("All");
			cmbIncludePictures.AddItem("1");
			cmbIncludePictures.AddItem("2");
			cmbIncludePictures.AddItem("3");
			cmbIncludePictures.AddItem("4");
			cmbIncludePictures.SelectedIndex = 0;
		}

		private bool CreateFiles()
		{
			bool CreateFiles = false;
			try
			{
				// On Error GoTo ErrorHandler
				frmWait.InstancePtr.Unload();
                
				string strEXE = "";
				// vbPorter upgrade warning: intWhat As short --> As int	OnWrite(string)
				int intWhat;
				// vbPorter upgrade warning: strStart As string	OnWrite(string, short)
				string strStart = "";
				string strEnd = "";
                // strEXE = "REOnlineUpdater.exe "


				if (modGlobalConstants.Statics.gboolCL && chkOutstanding.CheckState == Wisej.Web.CheckState.Checked)
				{
					strEXE += " -o ";
				}
				intWhat = FCConvert.ToInt32(GridRecords.TextMatrix(0, 0));
				if (intWhat == 0)
				{
					strEXE += " -a";
				}
				else if (intWhat == 1)
				{
					if (Strings.Trim(txtStart.Text) == "" && Strings.Trim(txtEnd.Text) == "")
					{
						MessageBox.Show("You must specify a range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return CreateFiles;
					}
					if (cmbtRange.Text == "Account")
					{
						strStart = FCConvert.ToString(Conversion.Val(txtStart.Text));
						strEnd = FCConvert.ToString(Conversion.Val(txtEnd.Text));
						if ((strStart == "0" && strEnd == "0") || (Conversion.Val(strStart) < 0 || Conversion.Val(strEnd) < 0))
						{
							MessageBox.Show("You must specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return CreateFiles;
						}
						if (Conversion.Val(strStart) == 0)
						{
							strStart = FCConvert.ToString(1);
						}
						else if (Conversion.Val(strEnd) == 0)
						{
						}
						else
						{
							if (Conversion.Val(strEnd) < Conversion.Val(strStart))
							{
								MessageBox.Show("You must specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return CreateFiles;
							}
						}
						strEXE += " -ra " + strStart;
						if (Conversion.Val(strEnd) > 0)
						{
							strEXE += " " + strEnd;
						}
					}
					else
					{
						strStart = Strings.Trim(txtStart.Text);
						strEnd = Strings.Trim(txtEnd.Text);
						if ((Strings.CompareString(strEnd, "<", strStart) && strEnd != "") || strStart == "")
						{
							MessageBox.Show("You must specify a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return CreateFiles;
						}
						if (cmbtRange.Text == "Name")
						{
							// name
							strEXE += " -rn " + FCConvert.ToString(Convert.ToChar(34)) + strStart + FCConvert.ToString(Convert.ToChar(34));
							if (strEnd != "")
							{
								strEXE += " " + FCConvert.ToString(Convert.ToChar(34)) + strEnd + FCConvert.ToString(Convert.ToChar(34));
							}
						}
						else if (cmbtRange.Text == "Map / Lot")
						{
							// maplot
							strEXE += " -rm " + FCConvert.ToString(Convert.ToChar(34)) + strStart + FCConvert.ToString(Convert.ToChar(34));
							if (strEnd != "")
							{
								strEXE += " " + FCConvert.ToString(Convert.ToChar(34)) + strEnd + FCConvert.ToString(Convert.ToChar(34));
							}
						}
						else
						{
							// location
							strEXE += " -rl " + FCConvert.ToString(Convert.ToChar(34)) + strStart + FCConvert.ToString(Convert.ToChar(34));
							if (strEnd != "")
							{
								strEXE += " " + FCConvert.ToString(Convert.ToChar(34)) + strEnd + FCConvert.ToString(Convert.ToChar(34));
							}
						}
					}
				}
				// vbPorter upgrade warning: intType As short --> As int	OnWrite(string)
				int intMaxPics = 0;
				intTaxType = FCConvert.ToInt32(GridType.TextMatrix(0, 0));
				if (intTaxType == 0 || intTaxType == 2)
				{
					strEXE += " -c";
					if (cmbIncludePictures.Visible == true)
					{
						if (cmbIncludePictures.SelectedIndex > 0)
						{
							if (cmbIncludePictures.SelectedIndex == 1)
							{
								intMaxPics = -1;
							}
							else
							{
								intMaxPics = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbIncludePictures.Text)));
							}
							strEXE += " -p " + FCConvert.ToString(intMaxPics);
							if (chkIncludeSketch.CheckState == Wisej.Web.CheckState.Checked)
							{
								strEXE += " -s";
							}
						}
					}
				}

				int intYear;
				if (!int.TryParse(txtTaxYear.Text, out intYear))
				{
					intYear = 0;
				}
				if (intTaxType == 1 || intTaxType == 2 || txtTaxYear.Visible)
				{
					if (intYear == 0 || txtTaxYear.Text.Length != 4)
					{
						MessageBox.Show("You must enter a valid Tax Year","Invalid Tax Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return false;
					}

					strEXE += " -b";
					strEXE += " -year " + FCConvert.ToString(Conversion.Val(txtTaxYear.Text));
				}

                //FC:FINAL:AM:#1867 - pass the current group too (in VB6 it was read from the registry)
                strEXE += " -de " + StaticSettings.gGlobalSettings.DataEnvironment;
                strEXE += " -client " + StaticSettings.gGlobalSettings.ClientEnvironment;

                string strWorkDir = "";
				string strFile;
				string strParameters;
				strParameters = Strings.Trim(strEXE);
				//strFile = FCFileSystem.Statics.UserDataFolder + "\\REOnlineWebUpdater.exe";
                strFile = System.IO.Path.Combine(Application.MapPath("\\"), "REOnlineWebUpdater.exe");
                int lngReturn;
				//FC:FINAL:DSE Path should be relative to deployment
				//lngReturn = modAPIsConst.ShellExecute(this.Handle.ToInt32(), "open", strFile, strParameters, Environment.CurrentDirectory, 1);
				lngReturn = modAPIsConst.ShellExecute(this.Handle.ToInt32(), "open", strFile, strParameters, Application.MapPath("\\"), 1);
				if (lngReturn > 31)
				{
					MessageBox.Show("Update program started" + "\r\n" + "");
				}
				else
				{
					//Information.Err().Raise(Information.Err().LastDLLError, null, null, null, null);
                    throw new Exception("Error running update program. Method returned value " + lngReturn);
					// Select Case lngReturn
					// Case 0
					// MsgBox "Unable to run update program" & vbNewLine & "The operating system is out of memory or resources", vbCritical, "Error"
					// Case 2
					// MsgBox "Unable to run update program" & vbNewLine & "The specified file was not found", vbCritical, "Error"
					// Case 3
					// MsgBox "Unable to run update program" & vbNewLine & "The specified path was not found", vbCritical, "Error"
					// Case 5
					// MsgBox "Unable to run update program" & vbNewLine & "The operating system denied access to the specified file", vbCritical, "Error"
					// Case 10
					// MsgBox "Unable to run update program" & vbNewLine & "Wrong Windows version", vbCritical, "Error"
					// Case 11
					// MsgBox "Unable to run update program" & vbNewLine & "The .exe file is invalid", vbCritical, "Error"
					// Case 12
					// MsgBox "Unable to run update program" & vbNewLine & "Application was designed for a different operating system", vbCritical, "Error"
					// Case 20
					// MsgBox "Unable to run update program" & vbNewLine & "Dynamic link library (DLL) file failure", vbCritical, "Error"
					// Case 26
					// MsgBox "Unable to run update program" & vbNewLine & "A sharing violation occured", vbCritical, "Error"
					// Case Else
					// MsgBox "Unable to run update program" & vbNewLine & "Method returned value " & lngReturn, vbCritical, "Error"
					// End Select
				}
				CreateFiles = true;
				return CreateFiles;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateFiles;
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.ColHidden(CNSTGRIDCOLAUTOID, true);
			Grid.ColDataType(CNSTGRIDCOLADDRESS, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.ColDataType(CNSTGRIDCOLALL, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.ColDataType(CNSTGRIDCOLLOCATION, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.ColDataType(CNSTGRIDCOLNAME, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.ColDataType(CNSTGRIDCOLPICTURES, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.TextMatrix(0, CNSTGRIDCOLADDRESS, "Address");
			Grid.TextMatrix(0, CNSTGRIDCOLALL, "All");
			Grid.TextMatrix(0, CNSTGRIDCOLLOCATION, "Location");
			Grid.TextMatrix(0, CNSTGRIDCOLMAPLOT, "Map Lot");
			Grid.TextMatrix(0, CNSTGRIDCOLNAME, "Name");
			Grid.TextMatrix(0, CNSTGRIDCOLOWNER, "Owner");
			Grid.TextMatrix(0, CNSTGRIDCOLPICTURES, "Pictures");
			Grid.TextMatrix(0, CNSTGRIDCOLACCOUNT, "Acct");
			Grid.ColAlignment(CNSTGRIDCOLACCOUNT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTGRIDCOLOWNER, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTGRIDCOLMAPLOT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTGRIDCOLADDRESS, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTGRIDCOLALL, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTGRIDCOLLOCATION, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTGRIDCOLNAME, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTGRIDCOLPICTURES, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGrid()
		{
			//FC:FINAL:MSH - i.issue #1149: using ...Original properties for correct resizing
			int GridWidth = 0;
			double dblTemp;
			//dblTemp = this.Width - 940;
			//dblTemp = this.WidthOriginal - 940;
			//if (dblTemp > 0)
			//{
			//	//Grid.Width = this.Width - 940;
			//	// FC:FINAL:VGE - #i1250 Grid resize is based on anchors now.
			//	//Grid.WidthOriginal = this.WidthOriginal - 940;
			//}
			////dblTemp = this.Width - 330;
			//dblTemp = this.WidthOriginal - 1000;
			//if (dblTemp > 0)
			//{
			//	//Frame2.Width = this.Width - 330;
			//	Frame2.WidthOriginal = this.WidthOriginal - 1000;
			//}
			////dblTemp = this.Height - Frame2.Top - 1200;
			//dblTemp = this.HeightOriginal - Frame2.TopOriginal - 200;
			//if (dblTemp > 0)
			//{
			//	//Frame2.Height = this.Height - Frame2.Top - 1200;
			//	Frame2.HeightOriginal = this.HeightOriginal - Frame2.TopOriginal - 200;
			//}
			////dblTemp = Frame2.Height - 300;
			//dblTemp = Frame2.HeightOriginal - 300;
			//if (dblTemp > 0)
			//{
			//	//Grid.Height = Frame2.Height - 300;
			//	// FC:FINAL:VGE - #i1250 Grid resize is based on anchors now.
			//	//Grid.Height = Frame2.HeightOriginal - 300;
			//}
			//GridWidth = Grid.Width;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLACCOUNT, FCConvert.ToInt32(0.06 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLMAPLOT, FCConvert.ToInt32(0.15 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLOWNER, FCConvert.ToInt32(0.35 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLADDRESS, FCConvert.ToInt32(0.09 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLALL, FCConvert.ToInt32(0.06 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLLOCATION, FCConvert.ToInt32(0.09 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLPICTURES, FCConvert.ToInt32(0.09 * GridWidth));
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper RSMast = new clsDRWrapper();
				int lngRow;
				string strMasterJoin;
				strMasterJoin = modREMain.GetMasterJoinForJoin();
				boolLoading = true;
				Grid.Rows = 1;
				Grid.Visible = false;
				rsLoad.OpenRecordset("select * from webexclude order by account", modGlobalVariables.strREDatabase);
				// Call RSMast.OpenRecordset("select rsaccount,rsmaplot,rsname from master where not rsdeleted = 1 and rscard = 1 order by rsaccount", strREDatabase)
				RSMast.OpenRecordset("select rsaccount,rsmaplot,rsname from " + strMasterJoin + " where not rsdeleted = 1 and rscard = 1 order by rsaccount", modGlobalVariables.strREDatabase);
				while (!RSMast.EndOfFile())
				{
					//Application.DoEvents();
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(RSMast.Get_Fields_Int32("rsaccount")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(RSMast.Get_Fields_String("rsmaplot")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLOWNER, FCConvert.ToString(RSMast.Get_Fields_String("rsname")));
					Grid.RowData(lngRow, false);
					if (rsLoad.FindFirstRecord("account", RSMast.Get_Fields_Int32("rsaccount")))
					{
						Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLALL, FCConvert.ToString(rsLoad.Get_Fields_Boolean("DontInclude")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLPICTURES, FCConvert.ToString(rsLoad.Get_Fields_Boolean("ExcludePictures")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS, FCConvert.ToString(rsLoad.Get_Fields_Boolean("ExcludeAddress")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCATION, FCConvert.ToString(rsLoad.Get_Fields_Boolean("ExcludeLocation")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(rsLoad.Get_Fields_Boolean("ExcludeName")));
					}
					else
					{
						Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(0));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLALL, FCConvert.ToString(false));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLPICTURES, FCConvert.ToString(false));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS, FCConvert.ToString(false));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCATION, FCConvert.ToString(false));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(false));
					}
					RSMast.MoveNext();
				}
				Grid.Visible = true;
				boolLoading = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				boolLoading = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveGrid()
		{
			bool SaveGrid = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngRow = 0;
				int lngID;
				clsDRWrapper rsSave = new clsDRWrapper();
				SaveGrid = false;
				modGlobalVariables.Statics.CustomizedInfo.ExportRef1ToWeb = chkIncludeRef1.CheckState == Wisej.Web.CheckState.Checked;
				modGlobalVariables.Statics.CustomizedInfo.ExportRef2ToWeb = chkIncludeRef2.CheckState == Wisej.Web.CheckState.Checked;
				modGlobalVariables.Statics.CustomizedInfo.Ref1Description = Strings.Trim(txtRef1Caption.Text);
				modGlobalVariables.Statics.CustomizedInfo.Ref2Description = Strings.Trim(txtRef2Caption.Text);
				modGlobalVariables.Statics.CustomizedInfo.SaveCustomInfo();
				if (Grid.Rows > 1)
				{
					rsSave.OpenRecordset("select * from webexclude order by account", modGlobalVariables.strREDatabase);
					lngRow = Grid.FindRow(true, 1);
					while (lngRow > 0)
					{
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID)) > 0)
						{
							// If rsSave.FindFirstRecord("Account", Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT)) Then
							if (rsSave.FindFirst("account = " + Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT)))
							{
								rsSave.Edit();
							}
							else
							{
								rsSave.AddNew();
							}
						}
						else
						{
							rsSave.AddNew();
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(rsSave.Get_Fields_Int32("id")));
						rsSave.Set_Fields("ACCOUNT", Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT));
						rsSave.Set_Fields("dontinclude", FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLALL)));
						rsSave.Set_Fields("excludepictures", FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLPICTURES)));
						rsSave.Set_Fields("excludeaddress", FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS)));
						rsSave.Set_Fields("excludelocation", FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCATION)));
						rsSave.Set_Fields("excludename", FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME)));
						rsSave.Update();
						if (lngRow < Grid.Rows - 1)
						{
							lngRow = Grid.FindRow(true, lngRow + 1);
						}
						else
						{
							lngRow = -1;
						}
					}
				}
				SaveGrid = true;
				return SaveGrid;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGrid;
		}

		private void mnuSettings_Click(object sender, System.EventArgs e)
		{
			frmREOnlineSettings reoSet = new frmREOnlineSettings();
			//FC:FINAL:CHN - issue 1706: Change showing window on modal.
			// reoSet.Show(App.MainForm);
			reoSet.Show(FormShowEnum.Modal);
		}
	}
}
