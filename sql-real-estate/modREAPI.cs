﻿//Fecher vbPorter - Version 1.0.0.40
using Global;
using System.Drawing;
using System.Runtime.InteropServices;

namespace TWRE0000
{
	public class modREAPI
	{
		//=========================================================
		

		[DllImport("gdi32")]
		public static extern int PlayMetaFile(int hdc, int hMF);

		public static int PlayMetaFileWrp(int hdc, int hMF)
		{
			int ret = PlayMetaFile(hdc, hMF);
			return ret;
		}

		

		public const uint SRCCOPY = 0xCC0020;
		// (DWORD) dest = source
		//public struct BITMAP
		//{
		//	public int bmType;
		//	public int bmWidth;
		//	public int bmHeight;
		//	public int bmWidthBytes;
		//	public short bmPlanes;
		//	public short bmBitsPixel;
		//	public int BMBits;
		//	//FC:FINAL:RPU - Use custom constructor to initialize fields
		//	public BITMAP(int unusedPram)
		//	{
		//		this.bmType = 0;
		//		this.bmWidth = 0;
		//		this.bmHeight = 0;
		//		this.bmWidthBytes = 0;
		//		this.bmPlanes = 0;
		//		this.bmBitsPixel = 0;
		//		this.BMBits = 0;
		//	}
		//};
		
		[StructLayout(LayoutKind.Sequential)]
		public struct BITMAPFILEHEADER
		{
			public short bfType;
			public int bfSize;
			public short bfReserved1;
			public short bfReserved2;
			public int bfOffBits;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public BITMAPFILEHEADER(int unusedParam)
			{
				this.bfType = 0;
				this.bfSize = 0;
				this.bfReserved1 = 0;
				this.bfReserved2 = 0;
				this.bfOffBits = 0;
			}
		};

		[StructLayout(LayoutKind.Sequential)]
		public struct BITMAPINFOHEADER
		{
			// 40 bytes
			public int biSize;
			public int biWidth;
			public int biHeight;
			public short biPlanes;
			public short biBitCount;
			public int biCompression;
			public int biSizeImage;
			public int biXPelsPerMeter;
			public int biYPelsPerMeter;
			public int biClrUsed;
			public int biClrImportant;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public BITMAPINFOHEADER(int unusedParam)
			{
				this.biSize = 0;
				this.biWidth = 0;
				this.biHeight = 0;
				this.biPlanes = 0;
				this.biBitCount = 0;
				this.biCompression = 0;
				this.biSizeImage = 0;
				this.biXPelsPerMeter = 0;
				this.biYPelsPerMeter = 0;
				this.biClrUsed = 0;
				this.biClrImportant = 0;
			}
		};

		

		[StructLayout(LayoutKind.Sequential)]
		public struct RGBQUAD
		{
			// vbPorter upgrade warning: rgbBlue As byte	OnWriteFCConvert.ToInt16(
			public byte rgbBlue;
			// vbPorter upgrade warning: rgbGreen As byte	OnWriteFCConvert.ToInt16(
			public byte rgbGreen;
			// vbPorter upgrade warning: rgbRed As byte	OnWriteFCConvert.ToInt16(
			public byte rgbRed;
			public byte rgbReserved;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public RGBQUAD(int unusedPram)
			{
				this.rgbBlue = 0;
				this.rgbGreen = 0;
				this.rgbRed = 0;
				this.rgbReserved = 0;
			}
		};
		// This is for a monochrome map
		[StructLayout(LayoutKind.Sequential)]
		public struct BITMAPINFO
		{
			public BITMAPINFOHEADER bmiHeader;
			public RGBQUAD[] bmiColors/*?  = new RGBQUAD[1 + 1] */;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public BITMAPINFO(int unusedParam)
			{
				this.bmiHeader = new BITMAPINFOHEADER(0);
				this.bmiColors = new RGBQUAD[2];
			}
		};

		[StructLayout(LayoutKind.Sequential)]
		public struct BITMAPINFO256
		{
			public BITMAPINFOHEADER bmiHeader;
			public RGBQUAD[] bmiColors/*?  = new RGBQUAD[255 + 1] */;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public BITMAPINFO256(int unusedParam)
			{
				this.bmiHeader = new BITMAPINFOHEADER(0);
				this.bmiColors = new RGBQUAD[256];
			}
		};
	}
}
