﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	public class modMDIParentXF
	{
		//=========================================================
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public static object FormExist(ref FCForm FormName)
		{
			object FormExist = null;
			foreach (FCForm frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}

		
	}
}
