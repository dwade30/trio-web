﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for sprtExemptAnal.
	/// </summary>
	public partial class srptExemptAnal : FCSectionReport
	{
		public srptExemptAnal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static srptExemptAnal InstancePtr
		{
			get
			{
				return (srptExemptAnal)Sys.GetInstance(typeof(srptExemptAnal));
			}
		}

		protected srptExemptAnal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsCode?.Dispose();
				clsExemption?.Dispose();
                clsCode = null;
                clsExemption = null;
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptExemptAnal	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsExemption = new clsDRWrapper();
		clsDRWrapper clsCode = new clsDRWrapper();
		
		int lngCount;
		int lngTotal;
		int lngAve;
		int[,] ExemptCode = new int[100 + 1, 3 + 1];
		int ExemptIndex;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// EOF = GetExemptInfo
			eArgs.EOF = clsExemption.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			// Call clsCode.OpenRecordset("select * from costrecord where crecordnumber between 1901 and 1999 order by crecordnumber", strREDatabase)
			clsCode.OpenRecordset("Select * from exemptcode order by code", modGlobalVariables.strREDatabase);
			// Call clsExemption.OpenRecordset("select rsaccount,riexemptcd1,riexemptcd2,riexemptcd3,rlexemption from master where not rsdeleted = 1 and rscard = 1 and ((riexemptcd1 > 0) or (riexemptcd2 > 0) or (riexemptcd3 > 0)) order by rsaccount", strREDatabase)
			if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
			{
				strSQL = "(select riexemptcd1 as exempt,exemptval1 as exemptval from master where not rsdeleted = 1 and rscard = 1 and exemptval1 > 0) union ALL (select  riexemptcd2 as exempt,exemptval2 as exemptval from master where not rsdeleted = 1 and rscard = 1 and exemptval2 > 0) union ALL (select riexemptcd3 as exempt,exemptval3 as exemptval from master where not rsdeleted = 1 and rscard = 1 and exemptval3 > 0)";
			}
			else
			{
				strSQL = "(select riexemptcd1 as exempt,exemptval1 as exemptval from master where not rsdeleted = 1 and rscard = 1 and ritrancode  = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " and exemptval1 > 0) union ALL (select  riexemptcd2 as exempt,exemptval2 as exemptval from master where not rsdeleted = 1 and rscard = 1 and ritrancode  = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + "  and exemptval2 > 0) union ALL (select riexemptcd3 as exempt,exemptval3 as exemptval from master where not rsdeleted = 1 and rscard = 1 and ritrancode  = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " and exemptval3 > 0)";
			}
			// Call clsExemption.CreateStoredProcedure("ExemptAnalysis", strSQL, strREDatabase)
			// strSQL = "select exempt,count(exempt) as thecount,sum(exemptval) as totval, avg(exemptval) as theaverage from exemptanalysis group by exempt order by exempt"
			strSQL = "select exempt,count(exempt) as thecount,sum(exemptval) as totval, avg(exemptval) as theaverage from (" + strSQL + ") exemptanalysis group by exempt order by exempt";
			clsExemption.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			// Call clsExemptAccounts.OpenRecordset("Select rsaccount,sum(rlexemption) as exempttot,sum(lastbldgval + lastlandval) as tottot from master where not rsdeleted = 1 group by rsaccount order by rsaccount", strredatabase)
			// Call SetupExemptInfo
			// ExemptIndex = 1
		}
		
		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!clsExemption.EndOfFile())
				{
					// Call clsCode.FindFirstRecord("crecordnumber", 1900 + clsExemption.Fields("exempt"))
					// Call clsCode.FindFirstRecord("code", clsExemption.Fields("exempt"))
					clsCode.FindFirst("code = " + clsExemption.Get_Fields_Int32("exempt"));
					txtExemption.Text = clsExemption.Get_Fields_Int32("exempt") + " " + clsCode.Get_Fields_String("description");
					// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
					lngCount = FCConvert.ToInt32(clsExemption.Get_Fields("thecount"));
					// TODO Get_Fields: Field [totval] not found!! (maybe it is an alias?)
					lngTotal = FCConvert.ToInt32(clsExemption.Get_Fields("totval"));
					// TODO Get_Fields: Field [theaverage] not found!! (maybe it is an alias?)
					lngAve = FCConvert.ToInt32(clsExemption.Get_Fields("theaverage"));
					txtCount.Text = lngCount.ToString();
					txtAssess.Text = Strings.Format(lngTotal, "###,###,###,##0");
					txtAve.Text = Strings.Format(lngAve, "#,###,###,##0");
					clsExemption.MoveNext();
				}
				// If ExemptIndex < 99 Then
				// Call clsCode.FindFirstRecord("crecordnumber", 1900 + ExemptIndex)
				// txtExemption.Text = ExemptIndex & " " & clsCode.GetData("cldesc")
				// lngCount = ExemptCode(ExemptIndex, 2)
				// lngTotal = ExemptCode(ExemptIndex, 1)
				// 
				// If lngCount > 0 Then
				// lngAve = lngTotal / lngCount
				// Else
				// lngAve = 0
				// End If
				// 
				// txtCount.Text = lngCount
				// txtAssess.Text = Format(lngTotal, "###,###,###,##0")
				// txtave.Text = Format(lngAve, "###,###,###,##0")
				// 
				// ExemptIndex = ExemptIndex + 1
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In subreport Exempt Analysis", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
