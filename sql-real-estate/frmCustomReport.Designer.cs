//Fecher vbPorter - Version 1.0.0.40
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	partial class frmCustomReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbTotal;
		public fecherFoundation.FCLabel lblTotal;
		public fecherFoundation.FCComboBox cmbJust;
		public fecherFoundation.FCLabel lblJust;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCDraggableListBox lstOrderBy;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCGrid Grid1;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCGrid grid2;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCTextBox txtCode;
		public fecherFoundation.FCTextBox txtTitle;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel lblCode;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCButton cmdClear;
		// public fecherFoundation.FCButton cmdDelete;
		// public fecherFoundation.FCButton cmdImport;
		// public fecherFoundation.FCButton cmdExport;
		public fecherFoundation.FCToolStripMenuItem mnuImport;
		public fecherFoundation.FCToolStripMenuItem mnuExport;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuPrintCodes;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow2;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow2;
		public fecherFoundation.FCToolStripMenuItem mnuAddCol2;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn2;
		public fecherFoundation.FCToolStripMenuItem mnuSep2;
		//FC:FINAL:MSH - i.issue #1225: replace menu item by toolbat button
		//public fecherFoundation.FCToolStripMenuItem mnuLoad;
		public fecherFoundation.FCButton cmdLoad;
		public fecherFoundation.FCButton cmdRunReport;
		//FC:FINAL:MSH - i.issue #1172: add 'Save' btn to toolbar
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCToolStripMenuItem mnuOptions;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuInsertRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem sep1;
		public fecherFoundation.FCToolStripMenuItem mnuAddCol;
		public fecherFoundation.FCToolStripMenuItem mnuInsertColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomReport));
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
			this.cmbTotal = new fecherFoundation.FCComboBox();
			this.lblTotal = new fecherFoundation.FCLabel();
			this.cmbJust = new fecherFoundation.FCComboBox();
			this.lblJust = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.lstOrderBy = new fecherFoundation.FCDraggableListBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.Grid1 = new fecherFoundation.FCGrid();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.grid2 = new fecherFoundation.FCGrid();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.txtCode = new fecherFoundation.FCTextBox();
			this.txtTitle = new fecherFoundation.FCTextBox();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.lblCode = new fecherFoundation.FCLabel();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuImport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintCodes = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOptions = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuInsertRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
			this.sep1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddCol = new fecherFoundation.FCToolStripMenuItem();
			this.mnuInsertColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSep2 = new fecherFoundation.FCToolStripMenuItem();
			this.cmdLoad = new fecherFoundation.FCButton();
			this.mnuAddRow2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteRow2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddCol2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteColumn2 = new fecherFoundation.FCToolStripMenuItem();
			this.cmdClear = new fecherFoundation.FCButton();
			this.cmdRunReport = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.grid2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoad)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRunReport)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdRunReport);
			this.BottomPanel.Location = new System.Drawing.Point(0, 583);
			this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.txtCode);
			this.ClientArea.Controls.Add(this.cmbTotal);
			this.ClientArea.Controls.Add(this.lblTotal);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cmbJust);
			this.ClientArea.Controls.Add(this.lblJust);
			this.ClientArea.Controls.Add(this.grid2);
			this.ClientArea.Controls.Add(this.txtTitle);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Controls.Add(this.lblCode);
			this.ClientArea.Size = new System.Drawing.Size(1078, 606);
			this.ClientArea.Controls.SetChildIndex(this.lblCode, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblDescription, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtTitle, 0);
			this.ClientArea.Controls.SetChildIndex(this.grid2, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblJust, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbJust, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
			this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblTotal, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbTotal, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtCode, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtDescription, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdLoad);
			this.TopPanel.Controls.Add(this.cmdSave);
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSave, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdLoad, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(177, 30);
			this.HeaderText.Text = "Custom Report";
			// 
			// cmbTotal
			// 
			this.cmbTotal.Items.AddRange(new object[] {
            "None",
            "Total",
            "Count"});
			this.cmbTotal.Location = new System.Drawing.Point(501, 429);
			this.cmbTotal.Name = "cmbTotal";
			this.cmbTotal.Size = new System.Drawing.Size(150, 40);
			this.cmbTotal.TabIndex = 1001;
			this.cmbTotal.Text = "None";
			this.cmbTotal.SelectedIndexChanged += new System.EventHandler(this.optTotal_CheckedChanged);
			// 
			// lblTotal
			// 
			this.lblTotal.AutoSize = true;
			this.lblTotal.Location = new System.Drawing.Point(365, 444);
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Size = new System.Drawing.Size(96, 16);
			this.lblTotal.TabIndex = 1;
			this.lblTotal.Text = "SHOW TOTAL";
			this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbJust
			// 
			this.cmbJust.Items.AddRange(new object[] {
            "Left",
            "Right"});
			this.cmbJust.Location = new System.Drawing.Point(501, 330);
			this.cmbJust.Name = "cmbJust";
			this.cmbJust.Size = new System.Drawing.Size(150, 40);
			this.cmbJust.TabIndex = 15;
			this.cmbJust.Text = "Left";
			this.cmbJust.SelectedIndexChanged += new System.EventHandler(this.optJust_CheckedChanged);
			// 
			// lblJust
			// 
			this.lblJust.AutoSize = true;
			this.lblJust.Location = new System.Drawing.Point(365, 344);
			this.lblJust.Name = "lblJust";
			this.lblJust.Size = new System.Drawing.Size(108, 16);
			this.lblJust.TabIndex = 16;
			this.lblJust.Text = "JUSTIFICATION";
			this.lblJust.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.lstOrderBy);
			this.Frame3.Location = new System.Drawing.Point(731, 327);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(310, 201);
			this.Frame3.TabIndex = 14;
			this.Frame3.Text = "Order By";
			// 
			// lstOrderBy
			// 
			this.lstOrderBy.BackColor = System.Drawing.SystemColors.Window;
			this.lstOrderBy.CheckBoxes = true;
			this.lstOrderBy.Location = new System.Drawing.Point(20, 30);
			this.lstOrderBy.Name = "lstOrderBy";
			this.lstOrderBy.Size = new System.Drawing.Size(274, 153);
			this.lstOrderBy.Style = 1;
			this.lstOrderBy.TabIndex = 15;
			// 
			// Frame1
			// 
			this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.Grid1);
			this.Frame1.Controls.Add(this.Image1);
			this.Frame1.Location = new System.Drawing.Point(0, 110);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(1058, 211);
			this.Frame1.TabIndex = 11;
			// 
			// Grid1
			// 
			this.Grid1.AllowUserToOrderColumns = true;
			this.Grid1.AllowUserToResizeColumns = true;
			this.Grid1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Grid1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Both;
			this.Grid1.Cols = 1;
			this.Grid1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.Grid1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
			this.Grid1.FixedCols = 0;
			this.Grid1.Location = new System.Drawing.Point(30, 49);
			this.Grid1.Name = "Grid1";
			this.Grid1.ReadOnly = false;
			this.Grid1.RowHeadersVisible = false;
			this.Grid1.Rows = 1;
			this.Grid1.Size = new System.Drawing.Size(1011, 149);
			this.Grid1.TabIndex = 13;
			this.Grid1.CurrentCellChanged += new System.EventHandler(this.Grid1_RowColChange);
			this.Grid1.ColumnWidthChanged += new Wisej.Web.DataGridViewColumnEventHandler(this.Grid1_ColumnWidthChanged);
			this.Grid1.RowHeightChanged += new Wisej.Web.DataGridViewRowEventHandler(this.Grid1_RowHeightChanged);
			this.Grid1.MouseDown += new Wisej.Web.MouseEventHandler(this.Grid1_MouseDownEvent);
			// 
			// Image1
			// 
			this.Image1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
			this.Image1.Location = new System.Drawing.Point(30, 7);
			this.Image1.Name = "Image1";
			this.Image1.Size = new System.Drawing.Size(1011, 22);
			// 
			// grid2
			// 
			this.grid2.Cols = 10;
			this.grid2.ExtendLastCol = true;
			this.grid2.FixedCols = 0;
			this.grid2.Location = new System.Drawing.Point(30, 327);
			this.grid2.Name = "grid2";
			this.grid2.RowHeadersVisible = false;
			this.grid2.Rows = 50;
			this.grid2.Size = new System.Drawing.Size(311, 256);
			this.grid2.TabIndex = 5;
			this.grid2.DoubleClick += new System.EventHandler(this.grid2_DblClick);
			// 
			// txtDescription
			// 
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.Enabled = false;
			this.txtDescription.Location = new System.Drawing.Point(501, 479);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(210, 40);
			this.txtDescription.TabIndex = 4;
			// 
			// txtCode
			// 
			this.txtCode.BackColor = System.Drawing.SystemColors.Window;
			this.txtCode.Enabled = false;
			this.txtCode.Location = new System.Drawing.Point(501, 380);
			this.txtCode.Name = "txtCode";
			this.txtCode.Size = new System.Drawing.Size(150, 40);
			this.txtCode.TabIndex = 1;
			// 
			// txtTitle
			// 
			this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
			this.txtTitle.Location = new System.Drawing.Point(129, 30);
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Size = new System.Drawing.Size(441, 40);
			this.txtTitle.TabIndex = 1002;
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(29, 86);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(602, 18);
			this.Label2.TabIndex = 10;
			this.Label2.Text = "SELECT A CELL (IF APPLICABLE) THEN RIGHT-CLICK INSIDE THE GRID TO POP-UP THE EDIT" +
    " MENU";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(40, 16);
			this.Label1.TabIndex = 6;
			this.Label1.Text = "TITLE";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(365, 493);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(96, 16);
			this.lblDescription.TabIndex = 3;
			this.lblDescription.Text = "DESCRIPTION";
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCode
			// 
			this.lblCode.Location = new System.Drawing.Point(365, 394);
			this.lblCode.Name = "lblCode";
			this.lblCode.Size = new System.Drawing.Size(50, 16);
			this.lblCode.TabIndex = 2;
			this.lblCode.Text = "CODE";
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuDelete,
            this.mnuImport,
            this.mnuExport,
            this.mnuPrintCodes,
            this.mnuOptions});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 0;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete a Report";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuImport
			// 
			this.mnuImport.Index = 1;
			this.mnuImport.Name = "mnuImport";
			this.mnuImport.Text = "Import";
			this.mnuImport.Click += new System.EventHandler(this.mnuImport_Click);
			// 
			// mnuExport
			// 
			this.mnuExport.Index = 2;
			this.mnuExport.Name = "mnuExport";
			this.mnuExport.Text = "Export";
			this.mnuExport.Click += new System.EventHandler(this.mnuExport_Click);
			// 
			// mnuPrintCodes
			// 
			this.mnuPrintCodes.Index = 3;
			this.mnuPrintCodes.Name = "mnuPrintCodes";
			this.mnuPrintCodes.Text = "Print Codes List";
			this.mnuPrintCodes.Click += new System.EventHandler(this.mnuPrintCodes_Click);
			// 
			// mnuOptions
			// 
			this.mnuOptions.Index = 4;
			this.mnuOptions.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuInsertRow,
            this.mnuDeleteRow,
            this.sep1,
            this.mnuAddCol,
            this.mnuInsertColumn,
            this.mnuDeleteColumn});
			this.mnuOptions.Name = "mnuOptions";
			this.mnuOptions.Text = "Layout";
			// 
			// mnuAddRow
			// 
			this.mnuAddRow.Index = 0;
			this.mnuAddRow.Name = "mnuAddRow";
			this.mnuAddRow.Text = "Add Row (After Last)";
			this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
			// 
			// mnuInsertRow
			// 
			this.mnuInsertRow.Index = 1;
			this.mnuInsertRow.Name = "mnuInsertRow";
			this.mnuInsertRow.Text = "Insert Row (Before Current)";
			this.mnuInsertRow.Click += new System.EventHandler(this.mnuInsertRow_Click);
			// 
			// mnuDeleteRow
			// 
			this.mnuDeleteRow.Index = 2;
			this.mnuDeleteRow.Name = "mnuDeleteRow";
			this.mnuDeleteRow.Text = "Delete Row";
			this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
			// 
			// sep1
			// 
			this.sep1.Index = 3;
			this.sep1.Name = "sep1";
			this.sep1.Text = "-";
			// 
			// mnuAddCol
			// 
			this.mnuAddCol.Index = 4;
			this.mnuAddCol.Name = "mnuAddCol";
			this.mnuAddCol.Text = "Add Column (After Last)";
			this.mnuAddCol.Click += new System.EventHandler(this.mnuAddCol_Click);
			// 
			// mnuInsertColumn
			// 
			this.mnuInsertColumn.Index = 5;
			this.mnuInsertColumn.Name = "mnuInsertColumn";
			this.mnuInsertColumn.Text = "Insert Column (Before Current)";
			this.mnuInsertColumn.Click += new System.EventHandler(this.mnuInsertColumn_Click);
			// 
			// mnuDeleteColumn
			// 
			this.mnuDeleteColumn.Index = 6;
			this.mnuDeleteColumn.Name = "mnuDeleteColumn";
			this.mnuDeleteColumn.Text = "Delete Column";
			this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
			// 
			// mnuSep2
			// 
			this.mnuSep2.Index = -1;
			this.mnuSep2.Name = "mnuSep2";
			this.mnuSep2.Text = "-";
			// 
			// cmdLoad
			// 
			this.cmdLoad.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdLoad.Location = new System.Drawing.Point(898, 29);
			this.cmdLoad.Name = "cmdLoad";
			this.cmdLoad.Size = new System.Drawing.Size(56, 24);
			this.cmdLoad.TabIndex = 10;
			this.cmdLoad.Text = "Load";
			this.cmdLoad.Click += new System.EventHandler(this.mnuLoad_Click);
			// 
			// mnuAddRow2
			// 
			this.mnuAddRow2.Index = -1;
			this.mnuAddRow2.Name = "mnuAddRow2";
			this.mnuAddRow2.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuAddRow2.Text = "Add Row (After Last)";
			this.mnuAddRow2.Click += new System.EventHandler(this.mnuAddRow2_Click);
			// 
			// mnuDeleteRow2
			// 
			this.mnuDeleteRow2.Index = -1;
			this.mnuDeleteRow2.Name = "mnuDeleteRow2";
			this.mnuDeleteRow2.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuDeleteRow2.Text = "Delete Row";
			this.mnuDeleteRow2.Click += new System.EventHandler(this.mnuDeleteRow2_Click);
			// 
			// mnuAddCol2
			// 
			this.mnuAddCol2.Index = -1;
			this.mnuAddCol2.Name = "mnuAddCol2";
			this.mnuAddCol2.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuAddCol2.Text = "Add Column (After Last)";
			this.mnuAddCol2.Click += new System.EventHandler(this.mnuAddCol2_Click);
			// 
			// mnuDeleteColumn2
			// 
			this.mnuDeleteColumn2.Index = -1;
			this.mnuDeleteColumn2.Name = "mnuDeleteColumn2";
			this.mnuDeleteColumn2.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuDeleteColumn2.Text = "Delete Column";
			this.mnuDeleteColumn2.Click += new System.EventHandler(this.mnuDeleteColumn2_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.Location = new System.Drawing.Point(1025, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(45, 24);
			this.cmdClear.TabIndex = 1;
			this.cmdClear.Text = "New";
			this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// cmdRunReport
			// 
			this.cmdRunReport.AppearanceKey = "acceptButton";
			this.cmdRunReport.Location = new System.Drawing.Point(413, 30);
			this.cmdRunReport.Name = "cmdRunReport";
			this.cmdRunReport.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdRunReport.Size = new System.Drawing.Size(190, 48);
			this.cmdRunReport.Text = "Save & Run Report";
			this.cmdRunReport.Click += new System.EventHandler(this.mnuRunReport_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSave.Location = new System.Drawing.Point(960, 29);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdSave.Size = new System.Drawing.Size(56, 24);
			this.cmdSave.TabIndex = 3;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmCustomReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmCustomReport";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Custom Report";
			this.Load += new System.EventHandler(this.frmCustomReport_Load);
			this.Resize += new System.EventHandler(this.frmCustomReport_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomReport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.grid2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoad)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRunReport)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.ComponentModel.IContainer components;
	}
}