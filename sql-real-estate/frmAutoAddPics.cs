﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmAutoAddPics.
	/// </summary>
	public partial class frmAutoAddPics : BaseForm
	{
		public frmAutoAddPics()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAutoAddPics InstancePtr
		{
			get
			{
				return (frmAutoAddPics)Sys.GetInstance(typeof(frmAutoAddPics));
			}
		}

		protected frmAutoAddPics _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		private bool cmbNameLenFromCode = false;
		//=========================================================
		private void chkImport_CheckedChanged(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			if (chkImport.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (cmbDestDir.Items.Count < 1)
				{
					MessageBox.Show("You can only copy pictures if you have at least one default picture directory." + "\r\n" + "You can add default directories in File Maintenance/Customize.", "No Default Directory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					chkImport.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			EnableOptions();
		}

		private void chkIncludeNumber_CheckedChanged(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			EnableOptions();
		}

		private void chkMiscData_CheckedChanged(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			EnableOptions();
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			string strLocation = "";
			// strLocation = CurDir
			//! Load frmGetDirectory;
			frmGetDirectory.InstancePtr.Text = "Source Directory";
			frmGetDirectory.InstancePtr.Label1.Text = "Select the Drive";
			frmGetDirectory.InstancePtr.Label2.Text = "Select the Picture Source Directory";
			// If Trim(strLocation) <> vbNullString Then
			// frmGetDirectory.Dir1.Path = strLocation
			// frmGetDirectory.Drive1.Drive = Left(strLocation, 1)
			// End If
			//frmGetDirectory.InstancePtr.cmdCancel.Visible = true;
			frmGetDirectory.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (modGlobalVariables.Statics.strGetPath != string.Empty)
			{
				txtSourcedir.Text = modGlobalVariables.Statics.strGetPath;
			}
		}

		private void frmAutoAddPics_Load(object sender, System.EventArgs e)
		{

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			FillDirCombo();
			cmbSeparator.SelectedIndex = 0;
			SetupGridPosition();
			SetupGridPictures();
			// FC:FINAL:MSH - i.issue #1133: force triggering method for correct initializing of elements
			EnableOptions();
		}

		private void frmAutoAddPics_Resize(object sender, System.EventArgs e)
		{
			ResizeGridPosition();
			ResizeGridPictures();
		}

		private void GridPictures_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridPictures.Row > 0)
						{
							GridPictures.RemoveItem(GridPictures.Row);
						}
						break;
					}
			}
			//end switch
		}

		private void mnuDelRow_Click(object sender, System.EventArgs e)
		{
			if (GridPictures.Row > 0)
			{
				GridPictures.RemoveItem(GridPictures.Row);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void EnableOptions()
		{
			int x;
			int intRows;
			string prevCmbNameLenText = cmbNameLen.Text;
			if (!(cmbFormat.Text == "Map Lot & Card" || cmbFormat.Text == "Account & Card" || chkIncludeNumber.CheckState == Wisej.Web.CheckState.Checked || chkMiscData.CheckState == Wisej.Web.CheckState.Checked))
			{
				//FC:FINAL:MSH - i.issue #1133: remove extra functionality
				//cmbNameLen.Clear();
				//cmbNameLen.Items.Add("Variable Length");
				//cmbNameLen.Items.Add("Use Separators");
				//cmbNameLen.Items.Add("Fixed Length");
				//if (cmbNameLen.Text == "Use Separators" || cmbNameLen.Text == "Fixed Length")
				//{
				//    cmbNameLen.Text = "Variable Length";
				//}
				cmbNameLen.Clear();
				cmbNameLen.Items.Add("Variable Length");
				//FC:FINAL:MSH - i.issue #1133: check selection before updating and select appropriate item
				if (prevCmbNameLenText == "Use Separators" || prevCmbNameLenText == "Fixed Length")
				{
					cmbNameLenFromCode = true;
					cmbNameLen.Text = "Variable Length";
				}
				else if (prevCmbNameLenText == "Variable Length")
				{
					cmbNameLenFromCode = true;
					cmbNameLen.Text = prevCmbNameLenText;
				}
				framPosition.Enabled = false;
				GridPosition.Enabled = false;
			}
			else
			{
				framPosition.Enabled = true;
				GridPosition.Enabled = true;
				//FC:FINAL:MSH - i.issue #1133: remove extra functionality
				//cmbNameLen.Clear();
				//cmbNameLen.Items.Add("Variable Length");
				//cmbNameLen.Items.Add("Use Separators");
				//cmbNameLen.Items.Add("Fixed Length");
				//if (cmbNameLen.Text == "Variable Length")
				//{
				//    cmbNameLen.Text = "Use Separators";
				//}
				cmbNameLen.Clear();
				cmbNameLen.Items.Add("Use Separators");
				cmbNameLen.Items.Add("Fixed Length");
				//FC:FINAL:MSH - i.issue #1133: check selection before updating and select appropriate item
				if (prevCmbNameLenText == "Variable Length")
				{
					cmbNameLenFromCode = true;
					cmbNameLen.Text = "Use Separators";
				}
				else if (prevCmbNameLenText == "Use Separators" || prevCmbNameLenText == "Fixed Length")
				{
					cmbNameLenFromCode = true;
					cmbNameLen.Text = prevCmbNameLenText;
				}
			}
			intRows = 2;
			if (cmbFormat.Text == "Map Lot & Card")
			{
				GridPosition.RowHidden(2, false);
				intRows += 1;
			}
			if (cmbFormat.Text == "Account & Card")
			{
				GridPosition.RowHidden(2, false);
				intRows += 1;
			}
			if (!(cmbFormat.Text == "Map Lot & Card" || cmbFormat.Text == "Account & Card"))
			{
				GridPosition.RowHidden(2, true);
			}
			if (cmbFormat.Text == "Map Lot" || cmbFormat.Text == "Map Lot & Card")
			{
				GridPosition.TextMatrix(1, 0, "Map Lot");
			}
			else
			{
				GridPosition.TextMatrix(1, 0, "Account");
			}
			if (chkIncludeNumber.CheckState == Wisej.Web.CheckState.Checked)
			{
				GridPosition.RowHidden(3, false);
				intRows += 1;
			}
			else
			{
				GridPosition.RowHidden(3, true);
			}
			if (chkMiscData.CheckState == Wisej.Web.CheckState.Checked)
			{
				GridPosition.RowHidden(4, false);
				intRows += 1;
			}
			else
			{
				GridPosition.RowHidden(4, true);
			}
			if (intRows == 5)
			{
				GridPosition.ColComboList(1, "First|Second|Third|Fourth");
			}
			else if (intRows == 4)
			{
				GridPosition.ColComboList(1, "First|Second|Third");
				for (x = 1; x <= 4; x++)
				{
					if (GridPosition.TextMatrix(x, 1) == "Fourth")
						GridPosition.TextMatrix(x, 1, "Third");
					if (GridPosition.TextMatrix(x, 1) == "First")
						GridPosition.TextMatrix(x, 2, FCConvert.ToString(1));
				}
				// x
			}
			else if (intRows < 4)
			{
				for (x = 1; x <= 3; x++)
				{
					if (GridPosition.TextMatrix(x, 1) == "Third" || GridPosition.TextMatrix(x, 1) == "Fourth")
						GridPosition.TextMatrix(x, 1, "Second");
					if (GridPosition.TextMatrix(x, 1) == "First")
						GridPosition.TextMatrix(x, 2, FCConvert.ToString(1));
				}
				// x
				GridPosition.ColComboList(1, "First|Second");
			}
			if (cmbNameLen.Text == "Fixed Length")
			{
				GridPosition.ColHidden(2, false);
			}
			else
			{
				GridPosition.ColHidden(2, true);
			}
			if (cmbNameLen.Text == "Fixed Length")
			{
				lblFilenameLen.Visible = true;
				txtFileNameLen.Visible = true;
			}
			else
			{
				lblFilenameLen.Visible = false;
				txtFileNameLen.Visible = false;
			}
			if (cmbNameLen.Text == "Use Separators")
			{
				lblSeparator.Visible = true;
				cmbSeparator.Visible = true;
			}
			else
			{
				lblSeparator.Visible = false;
				cmbSeparator.Visible = false;
			}
			if (chkImport.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblDestDir.Visible = true;
				cmbDestDir.Visible = true;
				chkRename.Enabled = true;
				chkDeleteSource.Enabled = true;
			}
			else
			{
				lblDestDir.Visible = false;
				cmbDestDir.Visible = false;
				chkRename.CheckState = Wisej.Web.CheckState.Unchecked;
				chkRename.Enabled = false;
				chkDeleteSource.CheckState = Wisej.Web.CheckState.Unchecked;
				chkDeleteSource.Enabled = false;
			}
		}

		private bool CheckOptions()
		{
			bool CheckOptions = false;
			// does error checking
			int x;
			int intFirstPos = 0;
			int intSecondPos = 0;
			int intLastPos;
			int intThirdPos = 0;
			int intFourthPos = 0;
			int intFirsts;
			int intSeconds;
			int intLasts;
			int intThirds;
			int intFourths;
			int intFilenameLen;
			int intParts;
			CheckOptions = false;
			intFilenameLen = FCConvert.ToInt32(Math.Round(Conversion.Val(txtFileNameLen.Text)));
			intParts = 0;
			if (cmbFormat.Text == "Map Lot & Card" || cmbFormat.Text == "Account & Card")
			{
				intParts += 1;
			}
			if (chkIncludeNumber.CheckState == Wisej.Web.CheckState.Checked)
			{
				intParts += 1;
			}
			if (chkMiscData.CheckState == Wisej.Web.CheckState.Checked)
			{
				intParts += 1;
			}
			// check to make sure you dont have two things in same position
			intFirsts = 0;
			intSeconds = 0;
			intLasts = 0;
			intThirds = 0;
			intFourths = 0;
			for (x = 1; x <= 4; x++)
			{
				if (!GridPosition.RowHidden(x))
				{
					if (GridPosition.TextMatrix(x, 1) == "First")
					{
						intFirsts += 1;
						intFirstPos = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPosition.TextMatrix(x, 2))));
					}
					else if (GridPosition.TextMatrix(x, 1) == "Second")
					{
						intSeconds += 1;
						intSecondPos = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPosition.TextMatrix(x, 2))));
						// ElseIf GridPosition.TextMatrix(x, 1) = "Last" Then
						// intLasts = intLasts + 1
						// intLastPos = Val(GridPosition.TextMatrix(x, 2))
					}
					else if (GridPosition.TextMatrix(x, 1) == "Third")
					{
						intThirds += 1;
						intThirdPos = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPosition.TextMatrix(x, 2))));
					}
					else if (GridPosition.TextMatrix(x, 1) == "Fourth")
					{
						intFourths += 1;
						intFourthPos = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPosition.TextMatrix(x, 2))));
					}
				}
			}
			// x
			if (intFirsts > 1)
			{
				MessageBox.Show("You have more than one item marked as the first part of the filename", "Invalid Position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckOptions;
			}
			if (intSeconds > 1)
			{
				MessageBox.Show("You have more than one item marked as the second part of the filename", "Invalid Position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckOptions;
			}
			// If intLasts > 1 Then
			// MsgBox "You have more than one item marked as the last part of the filename", vbExclamation, "Invalid Position"
			// Exit Function
			// End If
			if (intThirds > 1)
			{
				MessageBox.Show("You have more than one item marked as the third part of the filename", "Invalid Position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckOptions;
			}
			if (intFourths > 1)
			{
				MessageBox.Show("You have more than one item marked as the fourth part of the filename", "Invalid Position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			if (intParts > 0)
			{
				if (intSeconds < 1)
				{
					MessageBox.Show("You have no item marked as the second part of the filename", "Invalid Position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckOptions;
				}
			}
			// If intParts > 1 Then
			// If intLasts < 1 Then
			// MsgBox "You have no item marked as the last part of the filename", vbExclamation, "Invalid Position"
			// Exit Function
			// End If
			// End If
			if (intParts > 1)
			{
				if (intThirds < 1)
				{
					MessageBox.Show("You have no item marked as the third part of the filename", "Invalid Position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckOptions;
				}
			}
			if (intParts > 2)
			{
				if (intFourths < 1)
				{
					MessageBox.Show("You have no item marked as the fourth part of the filename", "Invalid Position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			if (cmbNameLen.Text == "Use Separators" || cmbNameLen.Text == "Fixed Length")
			{
				if (intFirsts < 1)
				{
					MessageBox.Show("You have no item marked as the first part of the filename", "Invalid Position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckOptions;
				}
			}
			if (cmbNameLen.Text == "Fixed Length")
			{
				if (Conversion.Val(txtFileNameLen.Text) <= 0)
				{
					MessageBox.Show("You must enter a valid length for the filename", "Invalid Filename length", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckOptions;
				}
				if (intFirstPos != 1)
				{
					MessageBox.Show("The first item must start at position 1", "Invalid start position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckOptions;
				}
				if (intSecondPos <= 1 || intSecondPos > intFilenameLen)
				{
					MessageBox.Show("The second position must be greater than the first position but not greater than the filename length", "Invalid second position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckOptions;
				}
				if (intParts > 1)
				{
					if ((intThirdPos <= intSecondPos) || (intThirdPos <= 1) || (intThirdPos > intFilenameLen))
					{
						MessageBox.Show("The third position must be greater than the second position but not greater than the filename length", "Invalid third position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return CheckOptions;
					}
				}
				if (intParts > 2)
				{
					// already tested third pos above
					if ((intFourthPos <= intThirdPos) || (intFourthPos <= 1) || (intFourthPos > intFilenameLen))
					{
						MessageBox.Show("The fourth position must be greater than the third position but not greater than the filename length", "Invalid fourth position", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
			if (Strings.Trim(txtSourcedir.Text) == string.Empty)
			{
				MessageBox.Show("You must specify a valid source directory", "Invalid Source Directory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckOptions;
			}
			CheckOptions = true;
			return CheckOptions;
		}

		private void mnuLoadFiles_Click(object sender, System.EventArgs e)
		{
			// load grid with filenames based on choices user made
			string strFileName = "";
			string strFile;
			int intFileLenType = 0;
			int intFileFormat = 0;
			bool boolIncludePicNum = false;
			int intFilenameLen;
			string strSeparatorChar;
			string strSrcDir;
			bool boolValidFile = false;
			bool boolIncMiscData = false;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			GridPictures.Rows = 1;
			GridPosition.Row = 0;
			//Application.DoEvents();
			if (!CheckOptions())
			{
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			strSeparatorChar = cmbSeparator.Text;
			intFilenameLen = FCConvert.ToInt32(Math.Round(Conversion.Val(txtFileNameLen.Text)));
			strSrcDir = Strings.Trim(txtSourcedir.Text);
			if (cmbFormat.Text == "Map Lot")
			{
				intFileFormat = 0;
			}
			else if (cmbFormat.Text == "Map Lot & Card")
			{
				intFileFormat = 1;
			}
			else if (cmbFormat.Text == "Account")
			{
				intFileFormat = 2;
			}
			else if (cmbFormat.Text == "Account & Card")
			{
				intFileFormat = 3;
			}
			if (cmbNameLen.Text == "Variable Length")
			{
				intFileLenType = 0;
			}
			else if (cmbNameLen.Text == "Use Separators")
			{
				intFileLenType = 1;
			}
			else if (cmbNameLen.Text == "Fixed Length")
			{
				intFileLenType = 2;
			}
			if (chkIncludeNumber.CheckState == Wisej.Web.CheckState.Checked)
			{
				boolIncludePicNum = true;
			}
			else
			{
				boolIncludePicNum = false;
			}
			if (chkMiscData.CheckState == Wisej.Web.CheckState.Checked)
			{
				boolIncMiscData = true;
			}
			else
			{
				boolIncMiscData = false;
			}
			strFile = FCFileSystem.Dir(strSrcDir + "\\*.*", (fecherFoundation.VisualBasicLayer.FileAttribute)(FCConvert.ToInt32(fecherFoundation.VisualBasicLayer.FileAttribute.Normal) + FCConvert.ToInt32(fecherFoundation.VisualBasicLayer.FileAttribute.Archive) + FCConvert.ToInt32(fecherFoundation.VisualBasicLayer.FileAttribute.ReadOnly)));
			while (!(strFile == string.Empty))
			{
				// we have a file but it might not be valid
				boolValidFile = true;
				if (strFile.Length > 4)
				{
					if ((Strings.UCase(Strings.Right(strFile, 4)) == ".JPG") || (Strings.UCase(Strings.Right(strFile, 4)) == ".BMP") || (Strings.UCase(Strings.Right(strFile, 4)) == ".GIF"))
					{
						boolValidFile = true;
					}
					else
					{
						boolValidFile = false;
					}
				}
				else
				{
					boolValidFile = false;
				}
				if (boolValidFile)
				{
					// still valid so far
					// strFilename = Mid(strFile, 1, Len(strFile) - 4)
					CheckFilenameValidity(ref strFile, ref intFileFormat, ref intFileLenType, ref boolIncludePicNum, ref boolIncMiscData, ref strSeparatorChar, ref intFilenameLen);
				}
				strFile = FCFileSystem.Dir();
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}

		private void mnuProcessList_Click(object sender, System.EventArgs e)
		{
			// go through the list and associate pictures
			// may also have to move pictures and/or rename them
			// vbPorter upgrade warning: x As int --> As DialogResult
			DialogResult x;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			if (chkImport.CheckState == Wisej.Web.CheckState.Checked)
			{
				x = MessageBox.Show("The options you have chosen will delete the original files." + "\r\n" + "Do you wish to continue?", "Delete Files?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (x == DialogResult.No)
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					return;
				}
			}
			for (int y = 1; y <= GridPictures.Rows - 1; y++)
			{
				if (chkImport.CheckState == Wisej.Web.CheckState.Checked)
				{
					RenameAndAssign_24(y, chkDeleteSource.CheckState == Wisej.Web.CheckState.Checked, cmbDestDir.Text);
				}
				else
				{
					AssignPics(ref y);
				}
			}
			// x
			GridPictures.Rows = 1;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			MessageBox.Show("List Processing Complete", "List Processed", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void optFormat_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			EnableOptions();
		}

		private void optFormat_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbFormat.SelectedIndex;
			optFormat_CheckedChanged(index, sender, e);
		}

		private void SetupGridPosition()
		{
			GridPosition.TextMatrix(0, 1, "Position");
			GridPosition.TextMatrix(0, 2, "Start");
			GridPosition.TextMatrix(1, 0, "Map Lot");
			GridPosition.TextMatrix(2, 0, "Card");
			GridPosition.TextMatrix(3, 0, "Picture Number");
			GridPosition.TextMatrix(4, 0, "Misc. Data");
			GridPosition.ColComboList(1, "First|Second|Third|Fourth");
			GridPosition.RowHidden(2, true);
			GridPosition.RowHidden(3, true);
			GridPosition.RowHidden(4, true);
		}

		private void optNameLen_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			EnableOptions();
		}

		private void optNameLen_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbNameLen.SelectedIndex;
			optNameLen_CheckedChanged(index, sender, e);
		}

		private void ResizeGridPosition()
		{
			int GridWidth = 0;
			GridWidth = GridPosition.WidthOriginal;
			GridPosition.ColWidth(0, FCConvert.ToInt32(0.4 * GridWidth));
			GridPosition.ColWidth(1, FCConvert.ToInt32(0.3 * GridWidth));
			GridPosition.ColWidth(2, FCConvert.ToInt32(0.3 * GridWidth));
		}

		private void FillDirCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			cmbDestDir.Clear();
			cmbDestDir.SelectedIndex = -1;
			clsLoad.OpenRecordset("Select * from DEFAULTpicturelocations", modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				if (Directory.Exists(FCConvert.ToString(clsLoad.Get_Fields_String("location"))))
				{
					cmbDestDir.AddItem(clsLoad.Get_Fields_String("location"));
				}
				clsLoad.MoveNext();
			}
			if (cmbDestDir.Items.Count > 0)
			{
				cmbDestDir.SelectedIndex = 0;
			}
		}

		private void ResizeGridPictures()
		{
			int GridWidth = 0;
			GridWidth = GridPictures.WidthOriginal;
			GridPictures.ColWidth(0, FCConvert.ToInt32(0.3 * GridWidth));
			GridPictures.ColWidth(1, FCConvert.ToInt32(0.2 * GridWidth));
			GridPictures.ColWidth(2, FCConvert.ToInt32(0.1 * GridWidth));
			GridPictures.ColWidth(3, FCConvert.ToInt32(0.1 * GridWidth));
			// .ColHidden(4) = True
			GridPictures.ColHidden(5, true);
		}

		private void SetupGridPictures()
		{
			GridPictures.TextMatrix(0, 0, "Filename");
			GridPictures.TextMatrix(0, 1, "Map Lot");
			GridPictures.TextMatrix(0, 2, "Account");
			GridPictures.TextMatrix(0, 3, "Card");
			GridPictures.TextMatrix(0, 4, "Name");
			// .ColHidden(4) = True
			GridPictures.ColHidden(5, true);
			// id
		}
		// vbPorter upgrade warning: intFileFormat As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intFileLenType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: strSeparatorChar As object	OnWrite(string)
		// vbPorter upgrade warning: intFilenameLen As short	OnWriteFCConvert.ToInt32(
		private void CheckFilenameValidity(ref string strFileName, ref int intFileFormat, ref int intFileLenType, ref bool boolIncludePicNum, ref bool boolIncMiscData, ref string strSeparatorChar, ref int intFilenameLen)
		{
			// accepts a filename and checks to see if it can match it to a RE master record
			// if so it adds it to the grid
			int[] intStart = new int[4 + 1];
			// holds the starting position of each part of the filename
			int intParts;
			// how many parts to the filename?
			string[] strPart = new string[4 + 1];
			// holds the filename split in parts
			string[] strAry = null;
			string strMapAccount = "";
			int intCard;
			int intPicNum;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strWhere;
			string strFullFileName;
			int x;
			string strMiscData;
			strFullFileName = strFileName;
			strFileName = Strings.Mid(strFileName, 1, strFileName.Length - 4);
			intParts = 1;
			if (boolIncludePicNum)
			{
				intParts += 1;
			}
			if (boolIncMiscData)
			{
				intParts += 1;
			}
			if (intFileFormat == 1 || intFileFormat == 3)
			{
				intParts += 1;
			}
			for (x = 1; x <= 4; x++)
			{
				if (!GridPosition.RowHidden(x))
				{
					string vbPorterVar = GridPosition.TextMatrix(x, 1);
					if (vbPorterVar == "First")
					{
						intStart[1] = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPosition.TextMatrix(x, 2))));
					}
					else if (vbPorterVar == "Second")
					{
						intStart[2] = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPosition.TextMatrix(x, 2))));
					}
					else if (vbPorterVar == "Third")
					{
						intStart[3] = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPosition.TextMatrix(x, 2))));
					}
					else if (vbPorterVar == "Fourth")
					{
						intStart[4] = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPosition.TextMatrix(x, 2))));
					}
				}
			}
			// x
			FCUtils.EraseSafe(strPart);
			if (intParts == 1)
			{
				strPart[1] = strFileName;
			}
			else if (intFileLenType == 2)
			{
				// split by fixed position
				if (strFileName.Length == intFilenameLen)
				{
					strPart[1] = Strings.Mid(strFileName, intStart[1], intStart[2] - intStart[1]);
					switch (intParts)
					{
						case 2:
							{
								strPart[2] = Strings.Mid(strFileName, intStart[2]);
								break;
							}
						case 3:
							{
								strPart[2] = Strings.Mid(strFileName, intStart[2], intStart[3] - intStart[2]);
								strPart[3] = Strings.Mid(strFileName, intStart[3]);
								break;
							}
						case 4:
							{
								strPart[2] = Strings.Mid(strFileName, intStart[2], intStart[3] - intStart[2]);
								strPart[3] = Strings.Mid(strFileName, intStart[3], intStart[4] - intStart[3]);
								strPart[4] = Strings.Mid(strFileName, intStart[4]);
								break;
							}
					}
					//end switch
				}
				else
				{
					// doesn't match format
					return;
				}
			}
			else if (intFileLenType == 1)
			{
				// split by separator char - easy
				strAry = Strings.Split(strFileName, FCConvert.ToString(strSeparatorChar), -1, CompareConstants.vbTextCompare);
				if (Information.UBound(strAry, 1) + 1 != intParts)
				{
					// +1 since it is zero based
					// filename doesn't work with current format
					return;
				}
				for (x = 1; x <= intParts; x++)
				{
					strPart[x] = strAry[x - 1];
				}
				// x
			}
			intCard = 0;
			if (intFileFormat == 0 || intFileFormat == 2)
			{
				intCard = 1;
				// if card isn't included in filename then it goes to card 1
			}
			else
			{
				string vbPorterVar1 = GridPosition.TextMatrix(2, 1);
				if (vbPorterVar1 == "First")
				{
					intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(strPart[1])));
				}
				else if (vbPorterVar1 == "Second")
				{
					intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(strPart[2])));
				}
				else if (vbPorterVar1 == "Third")
				{
					intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(strPart[3])));
				}
				else if (vbPorterVar1 == "Fourth")
				{
					intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(strPart[4])));
				}
			}
			if (intParts > 1)
			{
				string vbPorterVar2 = GridPosition.TextMatrix(1, 1);
				if (vbPorterVar2 == "First")
				{
					strMapAccount = strPart[1];
				}
				else if (vbPorterVar2 == "Second")
				{
					strMapAccount = strPart[2];
				}
				else if (vbPorterVar2 == "Third")
				{
					strMapAccount = strPart[3];
				}
				else if (vbPorterVar2 == "Fourth")
				{
					strMapAccount = strPart[4];
				}
			}
			else
			{
				strMapAccount = strPart[1];
			}
			intPicNum = 0;
			if (boolIncludePicNum)
			{
				string vbPorterVar3 = GridPosition.TextMatrix(3, 1);
				if (vbPorterVar3 == "First")
				{
					intPicNum = FCConvert.ToInt32(Math.Round(Conversion.Val(strPart[1])));
				}
				else if (vbPorterVar3 == "Second")
				{
					intPicNum = FCConvert.ToInt32(Math.Round(Conversion.Val(strPart[2])));
				}
				else if (vbPorterVar3 == "Third")
				{
					intPicNum = FCConvert.ToInt32(Math.Round(Conversion.Val(strPart[3])));
				}
				else if (vbPorterVar3 == "Fourth")
				{
					intPicNum = FCConvert.ToInt32(Math.Round(Conversion.Val(strPart[4])));
				}
			}
			else
			{
				intPicNum = 1;
				// doesn't really matter what the picnum is. Just need to know if it is in the right format
			}
			strMiscData = "";
			if (boolIncMiscData)
			{
				string vbPorterVar4 = GridPosition.TextMatrix(4, 1);
				if (vbPorterVar4 == "First")
				{
					strMiscData = strPart[1];
				}
				else if (vbPorterVar4 == "Second")
				{
					strMiscData = strPart[2];
				}
				else if (vbPorterVar4 == "Third")
				{
					strMiscData = strPart[3];
				}
				else if (vbPorterVar4 == "Fourth")
				{
					strMiscData = strPart[4];
				}
				if (strMiscData == string.Empty)
					return;
				// not valid since misc data isn't here
			}
			if (intPicNum <= 0)
				return;
			if (intCard <= 0)
				return;
			strWhere = " where not rsdeleted = 1 ";
			switch (intFileFormat)
			{
				case 0:
				case 1:
					{
						strWhere += " and rsmaplot = '" + strMapAccount + "'";
						break;
					}
				case 2:
				case 3:
					{
						if (Information.IsNumeric(strMapAccount))
						{
							strWhere += " and rsaccount = " + FCConvert.ToString(Conversion.Val(strMapAccount));
						}
						else
						{
							return;
						}
						break;
					}
			}
			//end switch
			strWhere += " and rscard = " + FCConvert.ToString(intCard);
			// valid format now see if there is a match
			clsLoad.OpenRecordset("select id,ownerpartyid,rsaccount,rscard,rsmaplot from master " + strWhere, modGlobalVariables.strREDatabase);
			if (!clsLoad.EndOfFile())
			{
				// Call clsLoad.InsertName("OwnerPartyID", "Owner", False, True, False)
				string strName = "";
				strName = "";
				cPartyController tPC = new cPartyController();
				// vbPorter upgrade warning: tP As cParty	OnWrite(cParty)
				cParty tP = new cParty();
				if (Conversion.Val(clsLoad.Get_Fields_Int32("ownerpartyid")) > 0)
				{
					tP = tPC.GetParty(clsLoad.Get_Fields_Int32("OwnerPartyID"), true);
					if (!(tP == null))
					{
						strName = tP.FullNameLastFirst;
					}
				}
				GridPictures.AddItem(strFullFileName + "\t" + clsLoad.Get_Fields_String("rsmaplot") + "\t" + clsLoad.Get_Fields_Int32("rsaccount") + "\t" + clsLoad.Get_Fields_Int32("rscard") + "\t" + strName + "\t" + clsLoad.Get_Fields_Int32("id"));
			}
		}
		// vbPorter upgrade warning: lngRowNum As int	OnWrite(DialogResult)
		private void RenameAndAssign_24(int lngRowNum, bool boolDeleteSource, string strDestdir)
		{
			RenameAndAssign(ref lngRowNum, ref boolDeleteSource, ref strDestdir);
		}

		private void RenameAndAssign(ref int lngRowNum, ref bool boolDeleteSource, ref string strDestdir)
		{
			// Assigns the pictures but must first copy them and possibly rename them
			bool boolAssign;
			int lngMaxPic = 0;
			bool boolPicsExist = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngAcct;
			int intCard;
			bool boolRename;
			bool boolDeleteOrigs;
			string strNewName = "";
			int lngAutoNum;
			int lngPicNum;
			string strPicNum = "";
			string strExtension;
			bool boolCopy = false;
			string strFileToSave;
			string strSrcDir;
			// vbPorter upgrade warning: intReturn As short, int --> As DialogResult
			DialogResult intReturn;
			//FileSystemObject fso = new FileSystemObject();
			boolDeleteOrigs = boolDeleteSource;
			boolRename = chkRename.CheckState == Wisej.Web.CheckState.Checked;
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPictures.TextMatrix(lngRowNum, 2))));
			intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPictures.TextMatrix(lngRowNum, 3))));
			clsLoad.OpenRecordset("select * from PICTURERECORD where mraccountnumber = " + FCConvert.ToString(lngAcct) + " and rscard = " + FCConvert.ToString(intCard) + " order by picnum", modGlobalVariables.strREDatabase);
			if (!clsLoad.EndOfFile())
			{
				boolPicsExist = true;
				clsLoad.MoveLast();
				lngMaxPic = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("picnum"))));
				clsLoad.MoveFirst();
			}
			else
			{
				boolPicsExist = false;
				lngMaxPic = 0;
			}
			strSrcDir = txtSourcedir.Text + "\\";
			lngAutoNum = 1;
			// start as the first picture
			strFileToSave = "";
			strExtension = Strings.Right(GridPictures.TextMatrix(lngRowNum, 0), 4);
			boolAssign = true;
			if (boolRename)
			{
				// figure out the auto name and then check if the name exists. If so increment the autoname
				// format of filename is AAAAAAPP. 6 hex digits for the account and 2 for the picture number
				// 4 chars will only give 65536 accounts.  Portland would already be more than half way there.
				// So must have at least 5 chars for account.  This leaves 2 for card (256 cards) and one for pics (16)
				// 16 should be enough, but it is possible to have more so must go with 6 for account and disregard card number
				// Topsham has up to 38 cards.  If each one had a picture that would be more than 16 pictures so must use AAAAAAPP
				strNewName = Convert.ToString(FCConvert.ToInt32(lngAcct), 16).ToUpper();
				strNewName = modGlobalFunctions.PadStringWithSpaces(strNewName, 5);
				strNewName = Strings.Replace(strNewName, " ", "0", 1, -1, CompareConstants.vbTextCompare);
				for (lngPicNum = lngAutoNum; lngPicNum <= 255; lngPicNum++)
				{
					strPicNum = Convert.ToString(FCConvert.ToInt32(lngPicNum), 16).ToUpper();
					strPicNum = modGlobalFunctions.PadStringWithSpaces(strPicNum, 2);
					strPicNum = Strings.Replace(strPicNum, " ", "0", 1, -1, CompareConstants.vbTextCompare);
					if (!File.Exists(strDestdir + "\\" + strNewName + strPicNum + strExtension))
					{
						// exit the for loop so we use this file name
						strFileToSave = strDestdir + "\\" + strNewName + strPicNum + strExtension;
						break;
					}
				}
				// lngPicNum
			}
			else
			{
				boolCopy = true;
				if (File.Exists(strDestdir + "\\" + GridPictures.TextMatrix(lngRowNum, 0)))
				{
					//Application.DoEvents();
					intReturn = MessageBox.Show("There is already a file of the name " + strDestdir + "\\" + GridPictures.TextMatrix(lngRowNum, 0) + "." + "\r\n" + "Do you wish to overwrite it?", "File Exists", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
					if (intReturn == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_26("RE", "Overwrote Picture File", strDestdir + "\\" + GridPictures.TextMatrix(lngRowNum, 0));
					}
					else
					{
						boolCopy = false;
					}
				}
				if (boolCopy)
				{
					strFileToSave = strDestdir + "\\" + GridPictures.TextMatrix(lngRowNum, 0);
					if (boolPicsExist)
					{
						clsLoad.MoveFirst();
						while (!clsLoad.EndOfFile())
						{
							if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("PICTURELOCATION"))) == Strings.UCase(strFileToSave))
							{
								// already assigned this picture (only happens when you are overwriting)
								boolAssign = false;
							}
							clsLoad.MoveNext();
						}
					}
				}
			}
			if (strFileToSave != string.Empty)
			{
				File.Copy(strSrcDir + GridPictures.TextMatrix(lngRowNum, 0), strFileToSave, true);
				if (boolDeleteOrigs)
				{
					modGlobalFunctions.AddCYAEntry_80("RE", "Deleted Src Pic", strSrcDir + GridPictures.TextMatrix(lngRowNum, 0), "Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + "  Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber));
					File.Delete(strSrcDir + GridPictures.TextMatrix(lngRowNum, 0));
				}
				if (boolAssign)
				{
					// make entries into the database for this
					lngMaxPic += 1;
					// increment the picture number
					clsLoad.AddNew();
					clsLoad.Set_Fields("mraccountnumber", lngAcct);
					clsLoad.Set_Fields("picturelocation", strFileToSave);
					clsLoad.Set_Fields("rscard", intCard);
					clsLoad.Set_Fields("description", "Enter Comment or Description Here");
					clsLoad.Set_Fields("picnum", lngMaxPic);
					clsLoad.Update();
				}
			}
		}
		// vbPorter upgrade warning: lngRowNum As int	OnWrite(DialogResult)
		private void AssignPics(ref int lngRowNum)
		{
			// accepts the row to gridpictures and then assigns the picture to the account
			bool boolAssign;
			clsDRWrapper clsLoad = new clsDRWrapper();
			int intTemp = 0;
			string strPicName = "";
			string strFileName;
			string strPicFileName = "";
			int lngMaxPic;
			boolAssign = true;
			strFileName = GridPictures.TextMatrix(lngRowNum, 0);
			lngMaxPic = 0;
			// check to see if it is already assigned and see what the max pic num is
			clsLoad.OpenRecordset("select * from picturerecord where mraccountnumber = " + FCConvert.ToString(Conversion.Val(GridPictures.TextMatrix(lngRowNum, 2))) + " AND rscard = " + FCConvert.ToString(Conversion.Val(GridPictures.TextMatrix(lngRowNum, 3))), modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				strPicName = FCConvert.ToString(clsLoad.Get_Fields_String("picturelocation"));
				intTemp = -1;
				while ((FCConvert.ToInt16(intTemp) < strPicName.Length - 1) && intTemp != 0)
				{
					if (intTemp == -1)
						intTemp = 0;
					intTemp = Strings.InStr(intTemp + 1, strPicName, "\\", CompareConstants.vbTextCompare);
				}
				if (intTemp > 0)
				{
					strPicFileName = Strings.Mid(strPicName, intTemp + 1);
				}
				else
				{
					strPicFileName = strPicName;
				}
				if (Conversion.Val(clsLoad.Get_Fields_Int32("PICNUM")) > lngMaxPic)
				{
					lngMaxPic = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("picnum"))));
				}
				if (Strings.UCase(strPicFileName) == Strings.UCase(strFileName))
				{
					boolAssign = false;
					break;
				}
				clsLoad.MoveNext();
			}
			if (boolAssign)
			{
				// make entries into the database for this
				lngMaxPic += 1;
				// increment the picture number
				clsLoad.AddNew();
				clsLoad.Set_Fields("mraccountnumber", FCConvert.ToString(Conversion.Val(GridPictures.TextMatrix(lngRowNum, 2))));
				clsLoad.Set_Fields("picturelocation", txtSourcedir.Text + "\\" + strFileName);
				clsLoad.Set_Fields("rscard", FCConvert.ToString(Conversion.Val(GridPictures.TextMatrix(lngRowNum, 3))));
				clsLoad.Set_Fields("description", "Enter Comment or Description Here");
				clsLoad.Set_Fields("picnum", lngMaxPic);
				clsLoad.Update();
			}
		}

		private void cmbFormat_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFormat.SelectedIndex == 0)
			{
				optFormat_CheckedChanged(sender, e);
			}
			else if (cmbFormat.SelectedIndex == 1)
			{
				optFormat_CheckedChanged(sender, e);
			}
				//FC:FINAL:MSH - i.issue #1133: add comparing to handle selecting of each item from combobox
				else if (cmbFormat.SelectedIndex == 2)
			{
				optFormat_CheckedChanged(sender, e);
			}
			else if (cmbFormat.SelectedIndex == 3)
			{
				optFormat_CheckedChanged(sender, e);
			}
		}

		private void cmbNameLen_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1133: leave the function if selection changed from code
			if (cmbNameLenFromCode)
			{
				cmbNameLenFromCode = false;
				return;
			}
			if (cmbNameLen.SelectedIndex == 0)
			{
				optNameLen_CheckedChanged(sender, e);
			}
			else if (cmbNameLen.SelectedIndex == 1)
			{
				optNameLen_CheckedChanged(sender, e);
			}
			else if (cmbNameLen.SelectedIndex == 2)
			{
				optNameLen_CheckedChanged(sender, e);
			}
		}
	}
}
