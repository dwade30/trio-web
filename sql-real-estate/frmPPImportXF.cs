﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWRE0000
{
	public partial class frmPPImportXF : BaseForm
	{
		public frmPPImportXF()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPPImportXF InstancePtr
		{
			get
			{
				return (frmPPImportXF)Sys.GetInstance(typeof(frmPPImportXF));
			}
		}

		protected frmPPImportXF _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDGOOD = 0;
		const int CNSTGRIDNEW = 1;
		const int CNSTGRIDUNTOUCHED = 2;
		const int CNSTGRIDBAD = 3;
		const int CNSTGRIDNONE = -1;
		string[] strCat = new string[9 + 1];
		string strOpen1 = "";
		string strOpen2 = "";
		bool boolXferAmounts;
		bool boolXferInfoOnly;
		// vbPorter upgrade warning: arParties As object()	OnWriteFCConvert.ToInt32(
		private object[] arParties = null;
		cPartyUtil tPU;

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 1; x <= (GridNew.Rows - 1); x++)
			{
				GridNew.TextMatrix(x, modGridConstantsXF.CNSTUSEROW, FCConvert.ToString(true));
				GridNew.RowHidden(x, false);
			}
			// x
		}

		private void cmdUnselect_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 1; x <= (GridNew.Rows - 1); x++)
			{
				GridNew.TextMatrix(x, modGridConstantsXF.CNSTUSEROW, FCConvert.ToString(false));
				if (chkShowAddedOnly.CheckState == Wisej.Web.CheckState.Checked)
				{
					GridNew.RowHidden(x, true);
				}
			}
			// x
		}

		private void chkFlagChanges_CheckedChanged(object sender, System.EventArgs e)
		{
			UpdatePercChanges();
		}

		private void chkShowAddedOnly_CheckedChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			if (chkShowAddedOnly.CheckState == Wisej.Web.CheckState.Checked)
			{
				for (x = 1; x <= (GridNew.Rows - 1); x++)
				{
					if (FCConvert.CBool(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPUSEROW)))
					{
						GridNew.RowHidden(x, false);
					}
					else
					{
						GridNew.RowHidden(x, true);
					}
				}
				// x
			}
			else
			{
				for (x = 1; x <= (GridNew.Rows - 1); x++)
				{
					GridNew.RowHidden(x, false);
				}
				// x
			}
		}

		private void chkShowChangesOnly_CheckedChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngValue = 0;
			int lngOldValue = 0;
			if (chkShowChangesOnly.CheckState == Wisej.Web.CheckState.Checked)
			{
				for (x = 1; x <= (GridGood.Rows - 1); x++)
				{
					lngValue = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE))));
					lngOldValue = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPORIGINALVALUE))));
					if (lngValue != lngOldValue)
					{
						GridGood.RowHidden(x, false);
					}
					else
					{
						GridGood.RowHidden(x, true);
					}
				}
				// x
			}
			else
			{
				for (x = 1; x <= (GridGood.Rows - 1); x++)
				{
					GridGood.RowHidden(x, false);
				}
				// x
			}
		}

		private void frmPPImport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void GridNew_RowColChange(object sender, System.EventArgs e)
		{
			switch (GridNew.Col)
			{
				case modGridConstantsXF.CNSTPPUSEROW:
					{
						GridNew.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						GridNew.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void GridNew_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (GridNew.Col == modGridConstantsXF.CNSTPPUSEROW)
			{
				if (chkShowAddedOnly.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (FCConvert.CBool(GridNew.TextMatrix(GridNew.Row, modGridConstantsXF.CNSTPPUSEROW)))
					{
						// use the opposite since the textmatrix is what it used to be
						// edit text is messed up
						GridNew.RowHidden(GridNew.Row, true);
					}
				}
			}
		}

		private void mnuPrintAccountsToUpdate_Click(object sender, System.EventArgs e)
		{
			if (GridGood.Rows < 2)
			{
				MessageBox.Show("There are no accounts to update", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked)
			{
				UpdatePercChanges();
			}
			frmReportViewer.InstancePtr.Init(rptPPUpdatedAccounts.InstancePtr, boolAllowEmail: true, strAttachmentName: "PPAccountsToUpdate");
		}

		private void mnuPrintBadAccount_Click(object sender, System.EventArgs e)
		{
			if (GridBadAccounts.Rows < 2)
			{
				MessageBox.Show("There are no bad accounts", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(rptPPBadAccounts.InstancePtr, boolAllowEmail: true, strAttachmentName: "PPBadAccounts");
		}

		private void mnuPrintNewAccount_Click(object sender, System.EventArgs e)
		{
			if (GridNew.Rows < 2)
			{
				MessageBox.Show("There are no new accounts", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rptPPNewAccounts.InstancePtr.Init(true);
		}

		private void mnuPrintNotUpdated_Click(object sender, System.EventArgs e)
		{
			if (GridNotUpdated.Rows < 2)
			{
				MessageBox.Show("There are no accounts that weren't updated", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(rptPPNotUpdatedAccounts.InstancePtr, boolAllowEmail: true, strAttachmentName: "PPNotUpdated");
		}

		private void mnuPrintSummary_Click(object sender, System.EventArgs e)
		{
			if (chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked)
			{
				UpdatePercChanges();
			}
			rptPPSummary.InstancePtr.Init(chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked);
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			// perform the actual transfer
			if (cmbUpdateType.ItemData(cmbUpdateType.SelectedIndex) == modGlobalVariablesXF.CNSTUPDATEAMOUNT)
			{
				boolXferAmounts = true;
				boolXferInfoOnly = false;
			}
			else if (cmbUpdateType.ItemData(cmbUpdateType.SelectedIndex) == modGlobalVariablesXF.CNSTUPDATEINFOONLY)
			{
				boolXferAmounts = false;
				boolXferInfoOnly = true;
			}
			if (TransferToPP())
			{
				//mnuExit_Click();
				return;
			}
		}

		private void txtPercentChg_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked)
			{
				UpdatePercChanges();
			}
		}

		private void frmPPImport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPPImport properties;
			//frmPPImport.FillStyle	= 0;
			//frmPPImport.ScaleWidth	= 9300;
			//frmPPImport.ScaleHeight	= 7845;
			//frmPPImport.LinkTopic	= "Form2";
			//frmPPImport.LockControls	= -1  'True;
			//frmPPImport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper clsLoad = new clsDRWrapper();
			int x = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				tPU = new cPartyUtil();
				// tPU.ConnectionString = clsLoad.ConnectionInformation("CentralParties")
				SetupcmbUpdateType();
				cmbUpdateType.SelectedIndex = 0;
				Shape1.FillColor(ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT));
				clsLoad.OpenRecordset("select sum([value]) as valsum from ppmaster where not deleted = 1", "twpp0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					lblCurAssess.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields("valsum")), "#,###,###,##0");
				}
				else
				{
					lblCurAssess.Text = FCConvert.ToString(0);
				}
				// get category descriptions
				clsLoad.OpenRecordset("select * from ratioTRENDS order by type", "twpp0000.vb1");
				while (!clsLoad.EndOfFile())
				{
					x = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("type"))));
					strCat[x] = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("description")));
					if (strCat[x] == string.Empty)
					{
						strCat[x] = "Category " + FCConvert.ToString(x);
					}
					clsLoad.MoveNext();
				}
				clsLoad.OpenRecordset("select * from PPRATIOOPENS", "twpp0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					strOpen1 = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("OpenField1")));
					strOpen2 = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("OpenField2")));
					if (strOpen1 == string.Empty)
					{
						strOpen1 = "Open 1";
					}
					if (strOpen2 == string.Empty)
					{
						strOpen2 = "Open 2";
					}
				}
				else
				{
					strOpen1 = "Open 1";
					strOpen2 = "Open 2";
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Form_Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupcmbUpdateType()
		{
			cmbUpdateType.Clear();
			cmbUpdateType.AddItem("Update Amounts");
			cmbUpdateType.ItemData(cmbUpdateType.NewIndex, modGlobalVariablesXF.CNSTUPDATEAMOUNT);
			cmbUpdateType.AddItem("Update Information Only");
			cmbUpdateType.ItemData(cmbUpdateType.NewIndex, modGlobalVariablesXF.CNSTUPDATEINFOONLY);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void frmPPImport_Resize(object sender, System.EventArgs e)
		{
			GridBadAccounts.AutoSize(0, GridBadAccounts.Cols - 1);
			GridGood.AutoSize(0, GridGood.Cols - 1);
			GridNew.AutoSize(0, GridNew.Cols - 1);
			GridNotUpdated.AutoSize(0, GridNotUpdated.Cols - 1);
		}

		private void mnuLoad_Click(object sender, System.EventArgs e)
		{
			// initialize and clear grids
			SetupGridLoad();
			SetupGridBadAccounts();
			SetupGridGood();
			SetupGridNew();
			SetupGridNotUpdated();
			FCUtils.StartTask(this, () =>
			{
				ImportVision();
				// now sort into the correct grids (good,bad,new,notupdated)
				ProcessGridLoad();
				UpdatePercChanges();
				GridBadAccounts.AutoSize(0, GridBadAccounts.Cols - 1);
				GridGood.AutoSize(0, GridGood.Cols - 1);
				GridNew.AutoSize(0, GridNew.Cols - 1);
				GridNotUpdated.AutoSize(0, GridNotUpdated.Cols - 1);
				this.EndWait();
			});
		}

		private void SetupGridLoad()
		{
			GridLoad.Rows = 0;
			GridLoad.Cols = modGridConstantsXF.CNSTPPNUMCOLS;
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPACCOUNT, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPBUSINESSCODE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT1, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT2, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT3, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT4, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT5, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT6, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT7, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT8, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT9, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCITY, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPOTHERACCOUNT, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPSTREETNUMBER, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPUSEROW, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPZIP, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPDELETED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPUPDATED, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPNEVERUPDATED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPOPEN1, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPOPEN2, FCGrid.DataTypeSettings.flexDTString);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT1BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT2BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT3BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT4BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT5BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT6BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT7BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT8BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridLoad.ColDataType(modGridConstantsXF.CNSTPPCAT9BETE, FCGrid.DataTypeSettings.flexDTLong);
		}

		private void SetupGridBadAccounts()
		{
			GridBadAccounts.Rows = 1;
			GridBadAccounts.Cols = modGridConstantsXF.CNSTPPNUMCOLS;
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPACCOUNT, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPBUSINESSCODE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT1, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT2, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT3, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT4, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT5, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT6, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT7, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT8, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT9, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCITY, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPOTHERACCOUNT, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPSTREETNUMBER, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPUSEROW, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPZIP, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPDELETED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPUPDATED, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPNEVERUPDATED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPOPEN1, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPOPEN2, FCGrid.DataTypeSettings.flexDTString);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT1BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT2BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT3BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT4BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT5BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT6BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT7BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT8BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColDataType(modGridConstantsXF.CNSTPPCAT9BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTPPUSEROW, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTPPNEVERUPDATED, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTPPORIGINALVALUE, true);
			GridBadAccounts.ColHidden(modGridConstantsXF.CNSTPPEXCEED, true);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPACCOUNT, "TRIO Account");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPADDRESS1, "Address 1");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPADDRESS2, "Address 2");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPBUSINESSCODE, "Bus Code");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT1, strCat[1]);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT2, strCat[2]);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT3, strCat[3]);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT4, strCat[4]);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT5, strCat[5]);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT6, strCat[6]);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT7, strCat[7]);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT8, strCat[8]);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT9, strCat[9]);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCITY, "City");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPOTHERACCOUNT, "Other Account");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPOWNER, "Name");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPSTATE, "ST");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPSTREET, "Street Name");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPSTREETNUMBER, "ST #");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPUSEROW, "Import");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPVALUE, "Total Assessed");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPZIP, "Zip");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPZIP4, "Zip4");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPDELETED, "Deleted");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPUPDATED, "Times Updated");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPOPEN1, strOpen1);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPOPEN2, strOpen2);
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT1BETE, strCat[1] + " BETE");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT2BETE, strCat[2] + " BETE");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT3BETE, strCat[3] + " BETE");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT4BETE, strCat[4] + " BETE");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT5BETE, strCat[5] + " BETE");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT6BETE, strCat[6] + " BETE");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT7BETE, strCat[7] + " BETE");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT8BETE, strCat[8] + " BETE");
			GridBadAccounts.TextMatrix(0, modGridConstantsXF.CNSTPPCAT9BETE, strCat[9] + " BETE");
		}

		private void SetupGridGood()
		{
			GridGood.Rows = 1;
			GridGood.Cols = modGridConstantsXF.CNSTPPNUMCOLS;
			GridGood.ColDataType(modGridConstantsXF.CNSTPPACCOUNT, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPBUSINESSCODE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT1, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT2, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT3, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT4, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT5, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT6, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT7, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT8, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT9, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCITY, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPOTHERACCOUNT, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPSTREETNUMBER, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPUSEROW, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPZIP, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPDELETED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPUPDATED, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPNEVERUPDATED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPEXCEED, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPOPEN1, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPOPEN2, FCGrid.DataTypeSettings.flexDTString);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT1BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT2BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT3BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT4BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT5BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT6BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT7BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT8BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColDataType(modGridConstantsXF.CNSTPPCAT9BETE, FCGrid.DataTypeSettings.flexDTLong);
			GridGood.ColHidden(modGridConstantsXF.CNSTPPUSEROW, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTPPNEVERUPDATED, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTPPORIGINALVALUE, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTPPDELETED, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTPPUPDATED, true);
			GridGood.ColHidden(modGridConstantsXF.CNSTPPEXCEED, true);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPACCOUNT, "TRIO Account");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPADDRESS1, "Address 1");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPADDRESS2, "Address 2");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPBUSINESSCODE, "Bus Code");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT1, strCat[1]);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT2, strCat[2]);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT3, strCat[3]);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT4, strCat[4]);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT5, strCat[5]);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT6, strCat[6]);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT7, strCat[7]);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT8, strCat[8]);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT9, strCat[9]);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCITY, "City");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPOTHERACCOUNT, "Other Account");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPOWNER, "Name");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPSTATE, "ST");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPSTREET, "Street Name");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPSTREETNUMBER, "ST #");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPUSEROW, "Import");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPVALUE, "Total Assessed");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPZIP, "Zip");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPZIP4, "Zip4");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPDELETED, "Deleted");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPUPDATED, "Times Updated");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPOPEN1, strOpen1);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPOPEN2, strOpen2);
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT1BETE, strCat[1] + " BETE");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT2BETE, strCat[2] + " BETE");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT3BETE, strCat[3] + " BETE");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT4BETE, strCat[4] + " BETE");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT5BETE, strCat[5] + " BETE");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT6BETE, strCat[6] + " BETE");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT7BETE, strCat[7] + " BETE");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT8BETE, strCat[8] + " BETE");
			GridGood.TextMatrix(0, modGridConstantsXF.CNSTPPCAT9BETE, strCat[9] + " BETE");
		}

		private void SetupGridNew()
		{
			GridNew.Rows = 1;
			GridNew.Cols = modGridConstantsXF.CNSTPPNUMCOLS;
			GridNew.ColDataType(modGridConstantsXF.CNSTPPACCOUNT, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPBUSINESSCODE, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPCAT1, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPCAT2, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPCAT3, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPCAT4, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPCAT5, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPCAT6, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPCAT7, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPCAT8, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPCAT9, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPCITY, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPOTHERACCOUNT, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPSTREETNUMBER, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPUSEROW, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPZIP, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPDELETED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPUPDATED, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPNEVERUPDATED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPEXCEED, FCGrid.DataTypeSettings.flexDTLong);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPOPEN1, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColDataType(modGridConstantsXF.CNSTPPOPEN2, FCGrid.DataTypeSettings.flexDTString);
			GridNew.ColHidden(modGridConstantsXF.CNSTPPNEVERUPDATED, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTPPORIGINALVALUE, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTPPDELETED, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTPPUPDATED, true);
			GridNew.ColHidden(modGridConstantsXF.CNSTPPEXCEED, true);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPUSEROW, "Import");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPACCOUNT, "TRIO Account");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPADDRESS1, "Address 1");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPADDRESS2, "Address 2");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPBUSINESSCODE, "Bus Code");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPCAT1, strCat[1]);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPCAT2, strCat[2]);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPCAT3, strCat[3]);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPCAT4, strCat[4]);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPCAT5, strCat[5]);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPCAT6, strCat[6]);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPCAT7, strCat[7]);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPCAT8, strCat[8]);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPCAT9, strCat[9]);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPCITY, "City");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPOTHERACCOUNT, "Other Account");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPOWNER, "Name");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPSTATE, "ST");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPSTREET, "Street Name");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPSTREETNUMBER, "ST #");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPUSEROW, "Import");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPVALUE, "Total Assessed");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPZIP, "Zip");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPZIP4, "Zip4");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPDELETED, "Deleted");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPUPDATED, "Times Updated");
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPOPEN1, strOpen1);
			GridNew.TextMatrix(0, modGridConstantsXF.CNSTPPOPEN2, strOpen2);
			GridNew.Row = 0;
			GridNew.Col = 1;
		}

		private void SetupGridNotUpdated()
		{
			GridNotUpdated.Rows = 1;
			GridNotUpdated.Cols = modGridConstantsXF.CNSTPPNUMCOLS;
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPACCOUNT, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPADDRESS1, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPADDRESS2, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPBUSINESSCODE, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPCAT1, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPCAT2, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPCAT3, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPCAT4, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPCAT5, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPCAT6, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPCAT7, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPCAT8, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPCAT9, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPCITY, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPOTHERACCOUNT, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPOWNER, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPSTATE, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPSTREET, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPSTREETNUMBER, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPUSEROW, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPVALUE, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPZIP, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPZIP4, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPDELETED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPUPDATED, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPNEVERUPDATED, FCGrid.DataTypeSettings.flexDTBoolean);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPEXCEED, FCGrid.DataTypeSettings.flexDTLong);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPOPEN1, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColDataType(modGridConstantsXF.CNSTPPOPEN2, FCGrid.DataTypeSettings.flexDTString);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTPPUSEROW, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTPPNEVERUPDATED, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTPPORIGINALVALUE, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTPPDELETED, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTPPUPDATED, true);
			GridNotUpdated.ColHidden(modGridConstantsXF.CNSTPPEXCEED, true);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPACCOUNT, "TRIO Account");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPADDRESS1, "Address 1");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPADDRESS2, "Address 2");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPBUSINESSCODE, "Bus Code");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPCAT1, strCat[1]);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPCAT2, strCat[2]);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPCAT3, strCat[3]);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPCAT4, strCat[4]);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPCAT5, strCat[5]);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPCAT6, strCat[6]);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPCAT7, strCat[7]);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPCAT8, strCat[8]);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPCAT9, strCat[9]);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPCITY, "City");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPOTHERACCOUNT, "Other Account");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPOWNER, "Name");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPSTATE, "ST");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPSTREET, "Street Name");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPSTREETNUMBER, "ST #");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPUSEROW, "Import");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPVALUE, "Total Assessed");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPZIP, "Zip");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPZIP4, "Zip4");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPDELETED, "Deleted");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPUPDATED, "Times Updated");
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPOPEN1, strOpen1);
			GridNotUpdated.TextMatrix(0, modGridConstantsXF.CNSTPPOPEN2, strOpen2);
		}

		private void ImportVision()
		{
            // load the vision pp file into gridload then put each row in the correct grid
            FCFileSystem fso = new FCFileSystem();
			string strSrcFile;
			StreamReader ts = null;
			string strReturn;
			// holds string returned by read routine
			int lngCRecord;
			int VisionRecordSize;
			modPPFileFormatsXF.PPConvertedData PPInfo = new modPPFileFormatsXF.PPConvertedData(1);
			modPPFileFormatsXF.PPVisionAccountInfo AccountInfo = new modPPFileFormatsXF.PPVisionAccountInfo(1);
			modPPFileFormatsXF.PPVisionPropertyItems AssessInfo = new modPPFileFormatsXF.PPVisionPropertyItems(1);
			int x;
			bool boolNewVersion;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// lets find the file first
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNExplorer	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strSrcFile = MDIParent.InstancePtr.CommonDialog1.FileName;
				this.ShowWait();
				if (!FCFileSystem.FileExists(strSrcFile))
				{
					MessageBox.Show("File " + strSrcFile + " cannot be found", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.EndWait();
					return;
				}
				this.Refresh();
				//App.DoEvents();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//frmWait.InstancePtr.Init("Please Wait", boolShowCounter: true);
				lngCRecord = 0;
				FillGridLoad();
				// vision record size is 1003 (including crlf)
				VisionRecordSize = 1003;
                // in bytes
                ts = FCFileSystem.OpenText(strSrcFile);//, IOMode.ForReading, false, Tristate.TristateUseDefault);
				// personal property doesn't seem to have the leading line like the RE file does
				boolNewVersion = false;
				strReturn = ts.ReadLine();
				if (strReturn.Length == 1801)
				{
					boolNewVersion = true;
				}
				ts.Close();
                ts = FCFileSystem.OpenText(strSrcFile);//, IOMode.ForReading, false, Tristate.TristateUseDefault);
                char[] buffer = null;
                while (!ts.EndOfStream)
				{
					//App.DoEvents();
					lngCRecord += 1;
					//frmWait.InstancePtr.lblMessage.Text = "Loading Record " + FCConvert.ToString(lngCRecord);
					this.UpdateWait("Loading Record " + FCConvert.ToString(lngCRecord));
					//App.DoEvents();
					ClearRecord(ref PPInfo);
					// get account info
					//strReturn = ts.Read(Marshal.SizeOf(AccountInfo));
					buffer = new char[Strings.Len(AccountInfo)];
					ts.Read(buffer, 0, Strings.Len(AccountInfo));
                    strReturn = new string(buffer);
                    GetAccountInfo(ref strReturn, ref PPInfo);
					// get assessment info
					// there are 20 of these
					// currently only the first one has data so skip the other 19
					//strReturn = ts.Read(Marshal.SizeOf(AssessInfo));
					buffer = new char[Strings.Len(AssessInfo)];
					ts.Read(buffer, 0, Strings.Len(AssessInfo));
                    strReturn = new string(buffer);
                    GetAssessInfo(ref strReturn, ref PPInfo);
					for (x = 1; x <= 19; x++)
					{
						//App.DoEvents();
						//strReturn = ts.Read(Marshal.SizeOf(AssessInfo));
						buffer = new char[Strings.Len(AssessInfo)];
						ts.Read(buffer, 0, Strings.Len(AssessInfo));
                        strReturn = new string(buffer);
                    }
					// x
					if (boolNewVersion)
					{
						for (x = 1; x <= 10; x++)
						{
							//App.DoEvents();
							//strReturn = ts.Read(Marshal.SizeOf(AssessInfo));
							buffer = new char[Strings.Len(AssessInfo)];
							ts.Read(buffer, 0, Strings.Len(AssessInfo));
                            strReturn = new string(buffer);
                            GetPenaltyInfo(ref strReturn, ref PPInfo);
						}
						// x
						for (x = 1; x <= 10; x++)
						{
							//App.DoEvents();
							//strReturn = ts.Read(Marshal.SizeOf(AssessInfo));
							buffer = new char[Strings.Len(AssessInfo)];
							ts.Read(buffer, 0, Strings.Len(AssessInfo));
                            strReturn = new string(buffer);
                            GetExemptInfo(ref strReturn, ref PPInfo);
						}
						// x
					}
					strReturn = ts.ReadLine();
					// Get rid of CRLF, could also have read two chars
					if (modGlobalVariablesXF.Statics.CustomizedInfo.intPPExtraCharacters > 0)
					{
						//strReturn = ts.Read(modGlobalVariables.Statics.CustomizedInfo.intPPExtraCharacters);
						buffer = new char[modGlobalVariablesXF.Statics.CustomizedInfo.intPPExtraCharacters];
						ts.Read(buffer, 0, modGlobalVariablesXF.Statics.CustomizedInfo.intPPExtraCharacters);
                        strReturn = new string(buffer);
                    }
					PutInList(ref PPInfo);
				}
				ts.Close();
				//frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Load Complete", "Loaded", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (fecherFoundation.Information.Err(ex).Number == 62)
				{
					// input past end of file
					MessageBox.Show("Error 62 Input past end of File encountered" + "\r\n" + "If this is due to extra characters at the end of the file, then all of the data has loaded" + "\r\n" + "Please verify that all records seem to have loaded", "Unexpected End Of File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					if (ts != null)
					{
						ts.Close();
					}
				}
				else
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ImportVision", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void FillGridLoad()
		{
			// load the current pp records
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow = 0;
			string strMasterJoin;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsLoad.OpenRecordset("select * from ppmaster left join ppvaluations on (ppmaster.account = ppvaluations.valuekey) order by account", "twpp0000.vb1");
				// strMasterJoin = GetPPMasterJoin
				strMasterJoin = modMainXF.GetPPMasterJoinForJoin();
				clsLoad.OpenRecordset("select * from " + strMasterJoin + " left join ppvaluations on (mj.account = ppvaluations.valuekey) order by account", "PersonalProperty");
				GridLoad.Rows = 0;
				while (!clsLoad.EndOfFile())
				{
					//App.DoEvents();
					GridLoad.Rows += 1;
					lngRow = GridLoad.Rows - 1;
					InitializeGridLoadRow(ref lngRow);
					// fill in some data in case it isn't updated.
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPACCOUNT, FCConvert.ToString(clsLoad.Get_Fields("account")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOWNER, FCConvert.ToString(clsLoad.Get_Fields("name")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPADDRESS1, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("address1"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPADDRESS2, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("address2"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPBUSINESSCODE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("BUSINESSCODE"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT1, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("category1"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT2, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("category2"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT3, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("category3"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT4, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("category4"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT5, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("category5"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT6, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("category6"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT7, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("category7"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT8, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("category8"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT9, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("category9"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCITY, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("city"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPDELETED, FCConvert.ToString(clsLoad.Get_Fields("deleted")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPORIGINALVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("value"))));
					if (modGlobalVariablesXF.Statics.CustomizedInfo.boolPPMatchByOpen1)
					{
						GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOTHERACCOUNT, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("open1"))));
					}
					else
					{
						GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOTHERACCOUNT, FCConvert.ToString(clsLoad.Get_Fields("account")));
					}
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOWNERID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("partyid"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOLDOWNERID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("partyid"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPSTATE, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("state"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPSTREET, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("street"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPSTREETNUMBER, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("streetnumber"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("value"))));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPZIP, FCConvert.ToString(clsLoad.Get_Fields("zip")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPZIP4, FCConvert.ToString(clsLoad.Get_Fields("zip4")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOPEN1, FCConvert.ToString(clsLoad.Get_Fields("open1")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOPEN2, FCConvert.ToString(clsLoad.Get_Fields("open2")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT1BETE, FCConvert.ToString(clsLoad.Get_Fields("cat1exempt")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT2BETE, FCConvert.ToString(clsLoad.Get_Fields("cat2exempt")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT3BETE, FCConvert.ToString(clsLoad.Get_Fields("cat3exempt")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT4BETE, FCConvert.ToString(clsLoad.Get_Fields("cat4exempt")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT5BETE, FCConvert.ToString(clsLoad.Get_Fields("cat5exempt")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT6BETE, FCConvert.ToString(clsLoad.Get_Fields("cat6exempt")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT7BETE, FCConvert.ToString(clsLoad.Get_Fields("cat7exempt")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT8BETE, FCConvert.ToString(clsLoad.Get_Fields("cat8exempt")));
					GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT9BETE, FCConvert.ToString(clsLoad.Get_Fields("cat9exempt")));
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void InitializeGridLoadRow(ref int lngRow)
		{
			// just put initial values in row
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPACCOUNT, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPADDRESS1, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPADDRESS2, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPBUSINESSCODE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT1, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT2, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT3, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT4, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT5, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT6, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT7, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT8, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT9, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCITY, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPDELETED, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPNEVERUPDATED, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPORIGINALVALUE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOTHERACCOUNT, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOWNER, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPSTATE, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPSTREET, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPSTREETNUMBER, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPUPDATED, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPUSEROW, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPVALUE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPZIP, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPZIP4, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPEXCEED, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOPEN1, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOPEN2, "");
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT1BETE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT2BETE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT3BETE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT4BETE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT5BETE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT6BETE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT7BETE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT8BETE, FCConvert.ToString(0));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT9BETE, FCConvert.ToString(0));
		}

		private void ClearRecord(ref modPPFileFormatsXF.PPConvertedData PPRecord)
		{
			PPRecord.Account = 0;
			PPRecord.Address1 = "";
			PPRecord.Address2 = "";
			PPRecord.BusinessCode = 0;
			PPRecord.BusinessName = "";
			PPRecord.Category1 = 0;
			PPRecord.Category2 = 0;
			PPRecord.Category3 = 0;
			PPRecord.Category4 = 0;
			PPRecord.Category5 = 0;
			PPRecord.Category6 = 0;
			PPRecord.Category7 = 0;
			PPRecord.Category8 = 0;
			PPRecord.Category9 = 0;
			PPRecord.City = "";
			PPRecord.Name = "";
			PPRecord.Open1 = "";
			PPRecord.Open2 = "";
			PPRecord.OtherAccount = "";
			PPRecord.State = "";
			PPRecord.Street = "";
			PPRecord.StreetNumber = 0;
			PPRecord.UpdatedTooMany = false;
			PPRecord.Value = 0;
			PPRecord.Zip = "";
			PPRecord.Zip4 = "";
			PPRecord.Category1Bete = 0;
			PPRecord.Category2Bete = 0;
			PPRecord.Category3Bete = 0;
			PPRecord.Category4Bete = 0;
			PPRecord.Category5Bete = 0;
			PPRecord.Category6Bete = 0;
			PPRecord.Category7Bete = 0;
			PPRecord.Category8Bete = 0;
			PPRecord.Category9Bete = 0;
		}

		private bool GetAccountInfo(ref string strDataRead, ref modPPFileFormatsXF.PPConvertedData PPRecord)
		{
			bool GetAccountInfo = false;
			// vbPorter upgrade warning: strTemp As object	OnWrite(string)
			object strTemp = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetAccountInfo = false;
				strTemp = Strings.Mid(strDataRead, 1, 15);
				PPRecord.OtherAccount = fecherFoundation.Strings.Trim(FCConvert.ToString(strTemp));
				strTemp = Strings.Mid(strDataRead, 16, 5);
				PPRecord.StreetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				PPRecord.Street = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 21, 25));
				PPRecord.BusinessName = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 46, 30));
				PPRecord.Open2 = PPRecord.BusinessName;
				PPRecord.Open1 = PPRecord.OtherAccount;
				PPRecord.Name = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 76, 30));
				// Kristen in Arundel call #71023
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "ARUNDEL")
				{
					PPRecord.Open2 = PPRecord.Name;
					PPRecord.Name = PPRecord.BusinessName;
				}
				PPRecord.Address1 = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 106, 30));
				PPRecord.Address2 = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 136, 30));
				PPRecord.City = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 166, 18));
				PPRecord.State = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 184, 2));
				strTemp = fecherFoundation.Strings.RTrim(Strings.Mid(strDataRead, 186, 10));
				PPRecord.Zip4 = "";
				if (FCConvert.ToString(strTemp).Length >= 9)
				{
					PPRecord.Zip = Strings.Mid(FCConvert.ToString(strTemp), 1, 5);
					PPRecord.Zip4 = Strings.Mid(FCConvert.ToString(strTemp), 7, 4);
				}
				else
				{
					PPRecord.Zip = fecherFoundation.Strings.Trim(Strings.Left(strTemp + "     ", 5));
					if (FCConvert.ToString(strTemp).Length > 5)
					{
						PPRecord.Zip4 = Strings.Mid(FCConvert.ToString(strTemp), 6);
					}
				}
				PPRecord.BusinessCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strDataRead, 199, 3))));
				GetAccountInfo = true;
				return GetAccountInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetAccountInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetAccountInfo;
		}

		private bool GetAssessInfo(ref string strDataRead, ref modPPFileFormatsXF.PPConvertedData PPRecord)
		{
			bool GetAssessInfo = false;
			string strCode;
			int lngValue;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetAssessInfo = false;
				strCode = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 5));
				lngValue = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strDataRead, 32, 9))));
				// currently they put all value into the first record so it only goes into category 1
				PPRecord.Category1 = lngValue;
				PPRecord.Value += lngValue;
				GetAssessInfo = true;
				return GetAssessInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetAssessInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetAssessInfo;
		}

		private bool GetPenaltyInfo(ref string strDataRead, ref modPPFileFormatsXF.PPConvertedData PPRecord)
		{
			bool GetPenaltyInfo = false;
			string strCode;
			int lngValue;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetPenaltyInfo = false;
				strCode = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 5));
				lngValue = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strDataRead, 32, 9))));
				if (lngValue > 0)
				{
					lngValue = lngValue;
				}
				PPRecord.Category1 += lngValue;
				PPRecord.Value += lngValue;
				GetPenaltyInfo = true;
				return GetPenaltyInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetPenaltyInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetPenaltyInfo;
		}

		private bool GetExemptInfo(ref string strDataRead, ref modPPFileFormatsXF.PPConvertedData PPRecord)
		{
			bool GetExemptInfo = false;
			string strCode;
			int lngValue;
			int lngAppraisedValue;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetExemptInfo = false;
				strCode = fecherFoundation.Strings.Trim(Strings.Mid(strDataRead, 1, 5));
				lngValue = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strDataRead, 32, 9))));
				lngAppraisedValue = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strDataRead, 23, 9))));
				if (lngValue > 0)
				{
					lngValue = lngValue;
				}
				if (lngAppraisedValue > 0)
				{
					lngAppraisedValue = lngAppraisedValue;
				}
				PPRecord.Category1 -= lngValue;
				PPRecord.Value -= lngValue;
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "GORHAM")
				{
					PPRecord.Category1Bete += lngValue;
				}
				if (PPRecord.Value < 0)
				{
					PPRecord.Value = 0;
				}
				if (PPRecord.Category1 < 0)
				{
					PPRecord.Category1 = 0;
				}
				GetExemptInfo = true;
				return GetExemptInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetExemptInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetExemptInfo;
		}

		private void PutInList(ref modPPFileFormatsXF.PPConvertedData PPRecord)
		{
			// puts record in list.  Searches to see if it is already in the list
			int lngRow;
			if (modGlobalVariablesXF.Statics.CustomizedInfo.boolPPMatchByTRIOaccount)
			{
				// if this isn't numeric and isn't blank then don't process it
				if (fecherFoundation.Strings.Trim(PPRecord.OtherAccount) != "")
				{
					if (!modMainXF.MyIsNumeric(PPRecord.OtherAccount))
					{
						return;
					}
				}
			}
			lngRow = FindInList(ref PPRecord.OtherAccount);
			if (lngRow < 0)
			{
				GridLoad.Rows += 1;
				lngRow = GridLoad.Rows - 1;
				GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPACCOUNT, FCConvert.ToString(0));
				// If CustomizedInfo.boolPPMatchByTRIOAccount Then
				// GridLoad.TextMatrix(lngRow, CNSTPPACCOUNT) = Val(PPRecord.OtherAccount)
				// End If
			}
			else
			{
			}
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPADDRESS1, PPRecord.Address1);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPADDRESS2, PPRecord.Address2);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPBUSINESSCODE, FCConvert.ToString(PPRecord.BusinessCode));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT1, FCConvert.ToString(PPRecord.Category1));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT2, FCConvert.ToString(PPRecord.Category2));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT3, FCConvert.ToString(PPRecord.Category3));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT4, FCConvert.ToString(PPRecord.Category4));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT5, FCConvert.ToString(PPRecord.Category5));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT6, FCConvert.ToString(PPRecord.Category6));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT7, FCConvert.ToString(PPRecord.Category7));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT8, FCConvert.ToString(PPRecord.Category8));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT9, FCConvert.ToString(PPRecord.Category9));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCITY, PPRecord.City);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPNEVERUPDATED, FCConvert.ToString(false));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOTHERACCOUNT, PPRecord.OtherAccount);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOWNER, PPRecord.Name);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPSTATE, PPRecord.State);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPSTREET, PPRecord.Street);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPSTREETNUMBER, FCConvert.ToString(PPRecord.StreetNumber));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPUPDATED, FCConvert.ToString(Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPUPDATED)) + 1));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPVALUE, FCConvert.ToString(PPRecord.Value));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPZIP, PPRecord.Zip);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPZIP4, PPRecord.Zip4);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOPEN1, PPRecord.Open1);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOPEN2, PPRecord.Open2);
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT1BETE, FCConvert.ToString(PPRecord.Category1Bete));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT2BETE, FCConvert.ToString(PPRecord.Category2Bete));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT3BETE, FCConvert.ToString(PPRecord.Category3Bete));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT4BETE, FCConvert.ToString(PPRecord.Category4Bete));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT5BETE, FCConvert.ToString(PPRecord.Category5Bete));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT6BETE, FCConvert.ToString(PPRecord.Category6Bete));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT7BETE, FCConvert.ToString(PPRecord.Category7Bete));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT8BETE, FCConvert.ToString(PPRecord.Category8Bete));
			GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPCAT9BETE, FCConvert.ToString(PPRecord.Category9Bete));
		}

		private int FindInList(ref string strOtherAccount)
		{
			int FindInList = 0;
			// searches through the grid fo find the account
			// returns -1 if not found
			int x;
			FindInList = -1;
			for (x = 0; x <= GridLoad.Rows - 1; x++)
			{
				//App.DoEvents();
				if (modGlobalVariablesXF.Statics.CustomizedInfo.boolPPMatchByOpen1)
				{
					if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(GridLoad.TextMatrix(x, modGridConstantsXF.CNSTPPOTHERACCOUNT))) == fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(strOtherAccount)))
					{
						FindInList = x;
						return FindInList;
					}
				}
				else
				{
					if (Conversion.Val(GridLoad.TextMatrix(x, modGridConstantsXF.CNSTPPACCOUNT)) == Conversion.Val(strOtherAccount))
					{
						FindInList = x;
						return FindInList;
					}
					if (Conversion.Val(GridLoad.TextMatrix(x, modGridConstantsXF.CNSTPPOTHERACCOUNT)) == Conversion.Val(strOtherAccount))
					{
						FindInList = x;
						return FindInList;
					}
				}
			}
			// x
			return FindInList;
		}

		private void ProcessGridLoad()
		{
			int lngRow;
			int intWhichGrid;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngCurRow;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				for (lngRow = 0; lngRow <= GridLoad.Rows - 1; lngRow++)
				{
					//App.DoEvents();
					intWhichGrid = CNSTGRIDGOOD;
					if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPACCOUNT)) < 1)
					{
						// new account
						intWhichGrid = CNSTGRIDNEW;
					}
					else if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPUPDATED)) < 1)
					{
						intWhichGrid = CNSTGRIDUNTOUCHED;
					}
					if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPUPDATED)) > 1)
					{
						intWhichGrid = CNSTGRIDBAD;
					}
					if (fecherFoundation.Strings.Trim(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPDELETED)) != string.Empty)
					{
						if (FCConvert.CBool(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPDELETED)))
						{
							if (Conversion.Val(GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPUPDATED)) < 1)
							{
								intWhichGrid = CNSTGRIDNONE;
								// just drop it. This is a deleted account that wasn't updated
							}
							else
							{
								intWhichGrid = CNSTGRIDBAD;
							}
						}
					}
					if (intWhichGrid == CNSTGRIDGOOD)
					{
						GridGood.Rows += 1;
						lngCurRow = GridGood.Rows - 1;
						// copy over to another table
						for (x = 0; x <= (GridLoad.Cols - 1); x++)
						{
							GridGood.TextMatrix(lngCurRow, x, GridLoad.TextMatrix(lngRow, x));
						}
						// x
						GridGood.TextMatrix(lngCurRow, modGridConstantsXF.CNSTPPUSEROW, FCConvert.ToString(true));
					}
					else if (intWhichGrid == CNSTGRIDNEW)
					{
						GridNew.Rows += 1;
						lngCurRow = GridNew.Rows - 1;
						for (x = 0; x <= (GridLoad.Cols - 1); x++)
						{
							GridNew.TextMatrix(lngCurRow, x, GridLoad.TextMatrix(lngRow, x));
						}
						// x
						GridNew.TextMatrix(lngCurRow, modGridConstantsXF.CNSTPPUSEROW, FCConvert.ToString(false));
						if (modGlobalVariablesXF.Statics.CustomizedInfo.boolPPMatchByTRIOaccount)
						{
							GridNew.TextMatrix(lngCurRow, modGridConstantsXF.CNSTPPACCOUNT, GridLoad.TextMatrix(lngRow, modGridConstantsXF.CNSTPPOTHERACCOUNT));
						}
					}
					else if (intWhichGrid == CNSTGRIDUNTOUCHED)
					{
						GridNotUpdated.Rows += 1;
						lngCurRow = GridNotUpdated.Rows - 1;
						for (x = 0; x <= (GridLoad.Cols - 1); x++)
						{
							GridNotUpdated.TextMatrix(lngCurRow, x, GridLoad.TextMatrix(lngRow, x));
						}
						// x
					}
					else if (intWhichGrid == CNSTGRIDBAD)
					{
						GridBadAccounts.Rows += 1;
						lngCurRow = GridBadAccounts.Rows - 1;
						for (x = 0; x <= (GridLoad.Cols - 1); x++)
						{
							GridBadAccounts.TextMatrix(lngCurRow, x, GridLoad.TextMatrix(lngRow, x));
						}
						// x
					}
					else if (intWhichGrid == CNSTGRIDNONE)
					{
						// don't do anything
					}
				}
				// lngRow
				// clear the grid
				GridLoad.Rows = 0;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ProcessGridLoad", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void UpdatePercChanges()
		{
			int lngRow;
			// vbPorter upgrade warning: dblPerc As double	OnWrite(string, double)
			double dblPerc = 0;
			int intExceed = 0;
			if (chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked)
			{
				// go through and highlight rows that fall within the limits
				if (GridGood.Rows > 1)
				{
					GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, GridGood.Rows - 1, GridGood.Cols - 1, Color.White);
					for (lngRow = 1; lngRow <= GridGood.Rows - 1; lngRow++)
					{
						//App.DoEvents();
						if (Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPVALUE)) != Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPORIGINALVALUE)))
						{
							if (Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPORIGINALVALUE)) == 0)
							{
								GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridGood.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPEXCEED, FCConvert.ToString(1));
							}
							else if (Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPVALUE)) == 0)
							{
								GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridGood.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPEXCEED, FCConvert.ToString(-1));
							}
							else
							{
								dblPerc = FCConvert.ToDouble(Strings.Format(Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPVALUE)) / Conversion.Val(GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPORIGINALVALUE)), "0.00"));
								if (dblPerc > 1)
								{
									dblPerc -= 1;
									intExceed = 1;
								}
								else
								{
									dblPerc = 1 - dblPerc;
									intExceed = -1;
								}
								dblPerc *= 100;
								if (dblPerc > Conversion.Val(txtPercentChg.Text))
								{
									GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPEXCEED, FCConvert.ToString(intExceed));
									GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridGood.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								}
								else
								{
									GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPEXCEED, FCConvert.ToString(0));
								}
							}
						}
						else
						{
							GridGood.TextMatrix(lngRow, modGridConstantsXF.CNSTPPEXCEED, FCConvert.ToString(0));
						}
					}
					// lngRow
				}
			}
			else
			{
				if (GridGood.Rows > 1)
				{
					GridGood.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, GridGood.Rows - 1, GridGood.Cols - 1, Color.White);
				}
			}
		}

		private bool TransferToPP()
		{
			bool TransferToPP = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// vbPorter upgrade warning: lngAcct As int	OnWrite(int, double)
			int lngAcct = 0;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strSQL = "";
			UtilParty uParty = new UtilParty();
			cParty tParty;
			// Dim tempParty As UtilParty
			cParty tempParty;
			cPartyController tPartyCont = new cPartyController();
			FCCollection vArray = new FCCollection();
			cPartyAddress tAddr;
			bool boolAddAddress = false;
			int intIndex;
			cParty testParty;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				TransferToPP = false;
				tPartyCont.ConnectionName = "CentralParties";
				Array.Resize(ref arParties, 0 + 1);
				if (boolXferAmounts)
				{
					modGlobalFunctions.AddCYAEntry_6("PP", "Transferred info and amounts to PP from other assessment prog");
				}
				else if (boolXferInfoOnly)
				{
					modGlobalFunctions.AddCYAEntry_6("PP", "Transferred info only to PP from other assessment prog");
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmWait.InstancePtr.Init("Please Wait", boolShowCounter: true);
				// sort the grid so that we can do a find next
				GridGood.Col = modGridConstantsXF.CNSTPPACCOUNT;
				GridGood.RowSel = GridGood.Row;
				GridGood.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				GridNew.Col = modGridConstantsXF.CNSTPPOWNER;
				GridNew.RowSel = GridNew.Row;
				GridNew.Sort = FCGrid.SortSettings.flexSortStringAscending;
				clsSave.OpenRecordset("select * from ppmaster order by account", "twpp0000.vb1");
				// updates first
				for (x = 1; x <= (GridGood.Rows - 1); x++)
				{
					//App.DoEvents();
					lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPACCOUNT))));
					frmWait.InstancePtr.lblMessage.Text = "Updating " + FCConvert.ToString(lngAcct);
					frmWait.InstancePtr.lblMessage.Refresh();
					if (x > 1)
					{
						if (clsSave.FindNextRecord("account", lngAcct))
						{
							// should never fail since these are in the good grid
							clsSave.Edit();
						}
					}
					else
					{
						if (clsSave.FindFirstRecord("account", lngAcct))
						{
							// should never fail since these are in the good grid
							clsSave.Edit();
						}
					}
					if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPOLDOWNER))) != fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPOWNER))))
					{
						if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPOWNER))) != "")
						{
							vArray = tPU.SplitName(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPOWNER)), false);
							if (vArray.Count >= 1)
							{
								// If UBound(vArray) >= 0 Then
								tempParty = vArray[1];
								tParty = new cParty();
								// If LCase(MuniName) = "gardiner" Then
								// If tempParty.PartyType = 0 Then
								// If tempParty.LastName = "" Then
								// If InStr(1, tempParty.FirstName, ",") <= 0 Then
								// intIndex = InStr(1, tempParty.FirstName, " ")
								// If intIndex > 0 Then
								// If intIndex + 1 < Len(tempParty.FirstName) Then
								// vArray = tPU.SplitName(Mid(tempParty.FirstName, 1, intIndex) & "," & Mid(tempParty.FirstName, intIndex + 1), False)
								// Set tempParty = vArray(0)
								// End If
								// End If
								// End If
								// End If
								// End If
								// End If
								tParty.DateCreated = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
								tParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
								tParty.Designation = tempParty.Designation;
								tParty.FirstName = tempParty.FirstName;
								tParty.LastName = tempParty.LastName;
								tParty.MiddleName = tempParty.MiddleName;
								tParty.PartyGUID = tempParty.PartyGUID;
								tParty.PartyType = tempParty.PartyType;
								tAddr = new cPartyAddress();
								tAddr.Address1 = fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPADDRESS1));
								tAddr.Address2 = fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPADDRESS2));
								tAddr.Address3 = "";
								tAddr.City = fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCITY));
								tAddr.State = fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPSTATE));
								tAddr.Zip = fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPZIP));
								if (fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPZIP4)) != "")
								{
									tAddr.Zip = tAddr.Zip + " " + fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPZIP4));
								}
								tAddr.AddressType = "Primary";
								tParty.Addresses.Add(tAddr);
								testParty = tPartyCont.FindFirstMatchingParty(ref tParty);
								if (!(testParty == null))
								{
									if (!tPartyCont.PartiesHaveSameAddress(ref tParty, ref testParty))
									{
										tPartyCont.SaveParty(ref tParty);
									}
									else
									{
										tParty = testParty;
									}
								}
								else
								{
									tPartyCont.SaveParty(ref tParty);
								}
								clsSave.Set_Fields("PartyID", tParty.ID);
								Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
								arParties[Information.UBound(arParties)] = tParty.ID;
							}
							else
							{
								clsSave.Set_Fields("PartyID", 0);
							}
						}
						else
						{
							clsSave.Set_Fields("PartyID", 0);
						}
					}
					else
					{
						// same person
						if (!modGlobalVariablesXF.Statics.CustomizedInfo.UsePartyAddressFromSystem)
						{
							if (Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPOWNERID)) > 0)
							{
								tParty = tPartyCont.GetParty(FCConvert.ToInt32(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPOWNERID)));
								if (!(tParty == null))
								{
									if (tParty.Addresses.Count > 0)
									{
										boolAddAddress = true;
										foreach (cPartyAddress tAddr_foreach in tParty.Addresses)
										{
											tAddr = tAddr_foreach;
											if (fecherFoundation.Strings.Trim(tAddr.Address1) != "" && fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(tAddr.Address1)) == fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPADDRESS1))))
											{
												boolAddAddress = false;
												break;
											}
											tAddr = null;
										}
									}
									else
									{
										boolAddAddress = true;
									}
									if (boolAddAddress)
									{
										foreach (cPartyAddress tAddr_foreach in tParty.Addresses)
										{
											tAddr = tAddr_foreach;
											if (tAddr.AddressType == "Primary")
											{
												tAddr.Address1 = GridGood.TextMatrix(x, modGridConstantsXF.CNSTADDRESS1);
												tAddr.Address2 = GridGood.TextMatrix(x, modGridConstantsXF.CNSTADDRESS2);
												tAddr.Address3 = "";
												tAddr.City = GridGood.TextMatrix(x, modGridConstantsXF.CNSTCITY);
												tAddr.State = GridGood.TextMatrix(x, modGridConstantsXF.CNSTSTATE);
												tAddr.Zip = GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP);
												if (fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP4)) != "")
												{
													tAddr.Zip = tAddr.Zip + "-" + GridGood.TextMatrix(x, modGridConstantsXF.CNSTZIP4);
												}
												break;
											}
											tAddr = null;
										}
										tPartyCont.SaveParty(ref tParty);
									}
								}
							}
						}
					}
					clsSave.Set_Fields("streetnumber", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPSTREETNUMBER))));
					clsSave.Set_Fields("street", fecherFoundation.Strings.Trim(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPSTREET)));
					clsSave.Set_Fields("businesscode", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPBUSINESSCODE))));
					clsSave.Set_Fields("open1", GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPOPEN1));
					clsSave.Set_Fields("open2", GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPOPEN2));
					clsSave.Set_Fields("EXEMPTION", 0);
					clsSave.Set_Fields("orcode", "N");
					if (boolXferAmounts)
					{
						clsSave.Set_Fields("value", FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE))));
						strSQL = "Update ppvaluations set ";
						strSQL += "Category1 = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT1)));
						strSQL += ",Category2 = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT2)));
						strSQL += ",Category3 = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT3)));
						strSQL += ",Category4 = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT4)));
						strSQL += ",category5 = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT5)));
						strSQL += ",category6 = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT6)));
						strSQL += ",category7 = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT7)));
						strSQL += ",category8 = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT8)));
						strSQL += ",category9 = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT9)));
						strSQL += ",overrideamount = 0";
						strSQL += ",totalitemized = 0";
						strSQL += ",totalleased = 0";
						strSQL += ",Cat1Exempt = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT1BETE)));
						strSQL += ",Cat2Exempt = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT2BETE)));
						strSQL += ",Cat3Exempt = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT3BETE)));
						strSQL += ",Cat4Exempt = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT4BETE)));
						strSQL += ",Cat5Exempt = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT5BETE)));
						strSQL += ",Cat6Exempt = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT6BETE)));
						strSQL += ",Cat7Exempt = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT7BETE)));
						strSQL += ",Cat8Exempt = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT8BETE)));
						strSQL += ",Cat9Exempt = " + FCConvert.ToString(Conversion.Val(GridGood.TextMatrix(x, modGridConstantsXF.CNSTPPCAT9BETE)));
						strSQL += " where valuekey = " + FCConvert.ToString(lngAcct);
						clsTemp.Execute(strSQL, "twpp0000.vb1");
					}
					clsSave.Update();
				}
				// x
				// new accounts next
				// .ColSort(CNSTPPACCOUNT) = flexSortNumericDescending
				GridNew.Col = modGridConstantsXF.CNSTPPACCOUNT;
				GridNew.Sort = FCGrid.SortSettings.flexSortNumericDescending;
				// there is no matching, only adding
				clsSave.OpenRecordset("select max(account) as maxacct from ppmaster", "twpp0000.vb1");
				if (!clsSave.EndOfFile())
				{
					lngAcct = FCConvert.ToInt32(Conversion.Val(clsSave.Get_Fields("maxacct")) + 1);
				}
				else
				{
					lngAcct = 1;
				}
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsSave.OpenRecordset("select * from ppmaster where account = -99", "twpp0000.vb1");
				for (x = 1; x <= (GridNew.Rows - 1); x++)
				{
					//App.DoEvents();
					if (FCConvert.CBool(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPUSEROW)))
					{
						if (Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPACCOUNT)) > 0)
						{
							lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPACCOUNT))));
						}
						else
						{
							// If CustomizedInfo.boolPPMatchByTRIOAccount Then
							rsTemp.OpenRecordset("select max(account) as maxacct from ppmaster", "twpp0000.vb1");
							if (!rsTemp.EndOfFile())
							{
								lngAcct = FCConvert.ToInt32(Conversion.Val(rsTemp.Get_Fields("maxacct")) + 1);
							}
							else
							{
								lngAcct = 1;
							}
							// End If
						}
						frmWait.InstancePtr.lblMessage.Text = "Updating " + FCConvert.ToString(lngAcct);
						frmWait.InstancePtr.lblMessage.Refresh();
						clsSave.AddNew();
						// extra stuff to do since this is a new account
						clsSave.Set_Fields("account", lngAcct);
						GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPACCOUNT, FCConvert.ToString(lngAcct));
						clsSave.Set_Fields("deleted", false);
						// clsSave.Fields("phonenumber") = "0000000000"
						clsSave.Set_Fields("realassoc", 0);
						clsSave.Set_Fields("exemption", 0);
						clsSave.Set_Fields("exemptcode1", 0);
						clsSave.Set_Fields("exemptcode2", 0);
						clsSave.Set_Fields("trancode", 0);
						clsSave.Set_Fields("streetcode", 0);
						clsSave.Set_Fields("compvalue", 0);
						clsSave.Set_Fields("orcode", "_");
						clsSave.Set_Fields("updcode", "_");
						clsSave.Set_Fields("rbcode", "N");
						clsSave.Set_Fields("squarefootage", 0);
						clsSave.Set_Fields("PartyID", 0);
						clsSave.Set_Fields("Open1", GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPOTHERACCOUNT));
						if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPOWNER))) != "")
						{
							vArray = tPU.SplitName(fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPOWNER)), false);
							if (vArray.Count >= 1)
							{
								// If UBound(vArray) >= 0 Then
								tempParty = vArray[1];
								tParty = new cParty();
								// If LCase(MuniName) = "gardiner" Then
								// If tempParty.PartyType = 0 Then
								// If tempParty.LastName = "" Then
								// If InStr(1, tempParty.FirstName, ",") <= 0 Then
								// intIndex = InStr(1, tempParty.FirstName, " ")
								// If intIndex > 0 Then
								// If intIndex + 1 < Len(tempParty.FirstName) Then
								// vArray = tPU.SplitName(Mid(tempParty.FirstName, 1, intIndex) & "," & Mid(tempParty.FirstName, intIndex + 1), False)
								// Set tempParty = vArray(0)
								// End If
								// End If
								// End If
								// End If
								// End If
								// End If
								tParty.FirstName = tempParty.FirstName;
								tParty.LastName = tempParty.LastName;
								tParty.MiddleName = tempParty.MiddleName;
								tParty.Designation = tempParty.Designation;
								tParty.PartyGUID = tempParty.PartyGUID;
								tParty.PartyType = tempParty.PartyType;
								tParty.DateCreated = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
								tParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
								tAddr = new cPartyAddress();
								tAddr.Address1 = fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPADDRESS1));
								tAddr.Address2 = fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPADDRESS2));
								tAddr.Address3 = "";
								tAddr.City = fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCITY));
								tAddr.State = fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPSTATE));
								tAddr.Zip = fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPZIP));
								if (fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPZIP4)) != "")
								{
									tAddr.Zip = tAddr.Zip + " " + fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPZIP4));
								}
								tAddr.AddressType = "Primary";
								tParty.Addresses.Add(tAddr);
								tPartyCont.SaveParty(ref tParty);
								clsSave.Set_Fields("PartyID", tParty.ID);
								Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
								arParties[Information.UBound(arParties)] = tParty.ID;
							}
						}
						clsSave.Set_Fields("streetnumber", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPSTREETNUMBER))));
						clsSave.Set_Fields("street", fecherFoundation.Strings.Trim(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPSTREET)));
						clsSave.Set_Fields("businesscode", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPBUSINESSCODE))));
						clsSave.Set_Fields("open1", GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPOPEN1));
						clsSave.Set_Fields("open2", GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPOPEN2));
						if (boolXferAmounts)
						{
							clsSave.Set_Fields("value", FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE))));
							strSQL = "Insert into ppvaluations (ValueKey,Category1,category2,category3,category4,category5";
							strSQL += ",category6,category7,category8,category9,overrideamount,totalleased,totalitemized,cat1exempt,cat2exempt,cat3exempt,cat4exempt,cat5exempt,cat6exempt,cat7exempt,cat8exempt,cat9exempt)";
							strSQL += " values (";
							strSQL += FCConvert.ToString(lngAcct);
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT1)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT2)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT3)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT4)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT5)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT6)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT7)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT8)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT9)));
							strSQL += ",0,0,0";
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT1BETE)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT2BETE)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT3BETE)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT4BETE)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT5BETE)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT6BETE)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT7BETE)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT8BETE)));
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPCAT9BETE)));
							strSQL += ")";
							clsTemp.Execute(strSQL, "twpp0000.vb1");
						}
						clsSave.Update();
						GridNew.TextMatrix(x, modGridConstantsXF.CNSTPPUSEROW, FCConvert.ToString(false));
						lngAcct += 1;
					}
				}
				// x
				if (chkMarkDeleted.CheckState == Wisej.Web.CheckState.Checked)
				{
					frmWait.InstancePtr.Unload();
					if (MessageBox.Show("Are you sure you want to mark all not updated accounts as deleted?" + "\r\n" + "They can be undeleted individually at any time.", "Delete Not Updated Accounts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_6("PP", "TWXF deleted accounts that were not updated");
						frmWait.InstancePtr.Init("Deleting", false, 100, true);
						for (x = 1; x <= (GridNotUpdated.Rows - 1); x++)
						{
							//App.DoEvents();
							lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTPPACCOUNT))));
							frmWait.InstancePtr.lblMessage.Text = "Deleting " + FCConvert.ToString(lngAcct);
							frmWait.InstancePtr.lblMessage.Refresh();
							clsSave.Execute("Update ppmaster set deleted = 1 where account = " + FCConvert.ToString(lngAcct), "TWpp0000.vb1", false);
							// Call clsSave.Execute("delete from ppvaluations where valuekey = " & lngAcct, "twre0000.vb1", False)
						}
						// x
					}
				}
				if (chkUndeleteAccounts.CheckState == Wisej.Web.CheckState.Checked)
				{
					frmWait.InstancePtr.Unload();
					if (MessageBox.Show("Are you sure you want to un-delete accounts marked deleted in TRIO but not in the import file?", "Un-Delete Accounts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_6("PP", "TWXF Undeleted Accounts that were not deleted in import file");
						clsSave.OpenRecordset("select * from ppmaster order by account", "twpp0000.vb1");
						frmWait.InstancePtr.Init("Un-Deleting", false, 100, true);
						for (x = 1; x <= (GridBadAccounts.Rows - 1); x++)
						{
							//App.DoEvents();
							if (FCConvert.CBool(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPDELETED)) && Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPUPDATED)) == 1)
							{
								lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPACCOUNT))));
								frmWait.InstancePtr.lblMessage.Text = "Undeleting " + FCConvert.ToString(lngAcct);
								frmWait.InstancePtr.lblMessage.Refresh();
								if (x > 1)
								{
									if (clsSave.FindFirstRecord("account", lngAcct))
									{
										clsSave.Edit();
									}
								}
								else
								{
									if (clsSave.FindFirstRecord("account", lngAcct))
									{
										clsSave.Edit();
									}
								}
								if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPOLDOWNER))) != fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPOWNER))))
								{
									if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPOWNER))) != "")
									{
										vArray = tPU.SplitName(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPOWNER)), false);
										if (vArray.Count >= 1)
										{
											// If UBound(vArray) >= 0 Then
											tempParty = vArray[1];
											tParty = new cParty();
											// If LCase(MuniName) = "gardiner" Then
											// If tempParty.PartyType = 0 Then
											// If tempParty.LastName = "" Then
											// If InStr(1, tempParty.FirstName, ",") <= 0 Then
											// intIndex = InStr(1, tempParty.FirstName, " ")
											// If intIndex > 0 Then
											// If intIndex + 1 < Len(tempParty.FirstName) Then
											// vArray = tPU.SplitName(Mid(tempParty.FirstName, 1, intIndex) & "," & Mid(tempParty.FirstName, intIndex + 1), False)
											// Set tempParty = vArray(0)
											// End If
											// End If
											// End If
											// End If
											// End If
											// End If
											tParty.DateCreated = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
											tParty.CreatedBy = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
											tParty.Designation = tempParty.Designation;
											tParty.FirstName = tempParty.FirstName;
											tParty.LastName = tempParty.LastName;
											tParty.MiddleName = tempParty.MiddleName;
											tParty.PartyGUID = tempParty.PartyGUID;
											tParty.PartyType = tempParty.PartyType;
											tAddr = new cPartyAddress();
											tAddr.Address1 = fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPADDRESS1));
											tAddr.Address2 = fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPADDRESS2));
											tAddr.Address3 = "";
											tAddr.City = fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCITY));
											tAddr.State = fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPSTATE));
											tAddr.Zip = fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPZIP));
											if (fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPZIP4)) != "")
											{
												tAddr.Zip = tAddr.Zip + " " + fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPZIP4));
											}
											tAddr.AddressType = "Primary";
											tParty.Addresses.Add(tAddr);
											tPartyCont.SaveParty(ref tParty);
											clsSave.Set_Fields("PartyID", tParty.ID);
											Array.Resize(ref arParties, Information.UBound(arParties, 1) + 1 + 1);
											arParties[Information.UBound(arParties)] = tParty.ID;
										}
										else
										{
											clsSave.Set_Fields("PartyID", 0);
										}
									}
									else
									{
										clsSave.Set_Fields("PartyID", 0);
									}
								}
								else
								{
									// same person
									if (Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPOWNERID)) > 0)
									{
										tParty = tPartyCont.GetParty(FCConvert.ToInt32(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPOWNERID)));
										if (!(tParty == null))
										{
											if (tParty.Addresses.Count > 0)
											{
												boolAddAddress = true;
												foreach (cPartyAddress tAddr_foreach in tParty.Addresses)
												{
													tAddr = tAddr_foreach;
													if (fecherFoundation.Strings.Trim(tAddr.Address1) != "" && fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(tAddr.Address1)) == fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPADDRESS1))))
													{
														boolAddAddress = false;
														break;
													}
													tAddr = null;
												}
											}
											else
											{
												boolAddAddress = true;
											}
											if (boolAddAddress)
											{
												tAddr = new cPartyAddress();
												tAddr.Address1 = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPADDRESS1);
												tAddr.Address2 = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPADDRESS2);
												tAddr.Address3 = "";
												tAddr.City = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCITY);
												tAddr.State = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPSTATE);
												tAddr.Zip = GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPZIP);
												if (fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPZIP4)) != "")
												{
													tAddr.Zip = tAddr.Zip + "-" + GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPZIP4);
												}
												if (tParty.Addresses.Count > 0)
												{
													tAddr.ProgModule = "PP";
													tAddr.ModAccountID = lngAcct;
													tAddr.AddressType = "Override";
												}
											}
										}
									}
								}
								clsSave.Set_Fields("streetnumber", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPSTREETNUMBER))));
								clsSave.Set_Fields("street", fecherFoundation.Strings.Trim(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPSTREET)));
								clsSave.Set_Fields("businesscode", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPBUSINESSCODE))));
								clsSave.Set_Fields("open1", GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPOPEN1));
								clsSave.Set_Fields("open2", GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPOPEN2));
								clsSave.Set_Fields("exemption", 0);
								clsSave.Set_Fields("deleted", false);
								clsSave.Set_Fields("orcode", "N");
								if (boolXferAmounts)
								{
									clsSave.Set_Fields("value", FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPVALUE))));
									strSQL = "Update ppvaluations set ";
									strSQL += "Category1 = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT1)));
									strSQL += ",Category2 = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT2)));
									strSQL += ",Category3 = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT3)));
									strSQL += ",Category4 = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT4)));
									strSQL += ",category5 = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT5)));
									strSQL += ",category6 = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT6)));
									strSQL += ",category7 = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT7)));
									strSQL += ",category8 = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT8)));
									strSQL += ",category9 = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT9)));
									strSQL += ",overrideamount = 0";
									strSQL += ",totalitemized = 0";
									strSQL += ",totalleased = 0";
									strSQL += ",Cat1Exempt = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT1BETE)));
									strSQL += ",Cat2Exempt = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT2BETE)));
									strSQL += ",Cat3Exempt = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT3BETE)));
									strSQL += ",Cat4Exempt = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT4BETE)));
									strSQL += ",Cat5Exempt = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT5BETE)));
									strSQL += ",Cat6Exempt = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT6BETE)));
									strSQL += ",Cat7Exempt = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT7BETE)));
									strSQL += ",Cat8Exempt = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT8BETE)));
									strSQL += ",Cat9Exempt = " + FCConvert.ToString(Conversion.Val(GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTPPCAT9BETE)));
									strSQL += " where valuekey = " + FCConvert.ToString(lngAcct);
									clsTemp.Execute(strSQL, "twpp0000.vb1");
								}
								clsSave.Update();
							}
						}
						// x
					}
				}
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Transfer Complete", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				TransferToPP = true;
				return TransferToPP;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In TransferToPP", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return TransferToPP;
		}
	}
}
