﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptInputCard.
	/// </summary>
	partial class rptInputCard
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptInputCard));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Field68 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCaption2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReference = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExemptCode1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTranCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBuildingCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNeighborhood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopography = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtXCoordinate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtilities = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYCoordinate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtZone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtZone2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtForestYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAffidavit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleVerified = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSaleValidity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSalePrice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSaleFinancing = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLandType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandUnits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInf1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCd1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInf2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCd2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandType3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandUnits3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInf3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCd3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandType4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandUnits4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInf4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCd4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandType5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandUnits5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInf5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCd5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandType6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandUnits6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInf6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCd6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandType7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandUnits7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInf7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCd7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBuildingStyle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoofSurface = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDwellingUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMasonryTrim = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOtherUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStories = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExteriorWalls = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearBuilt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOpen3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearRemodeled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBSMTGar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtWetBasement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBasement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBSMTLiving = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCoolType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBSMTGrade = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPercentCooled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOpen5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHeatType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPercentHeated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBathStyle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRooms = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtKitchenStyle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBedrooms = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFullBaths = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFixtures = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtfireplaces = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtHalfBaths = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLayout = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFactor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAttic = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSQFootage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtInsulation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUnfinished = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGrade = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysicalGood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctGood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCondition = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEconGood = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label82 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEntranceCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInfoCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label83 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label84 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label85 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEconCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateInspected = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label86 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field67 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label87 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label88 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label89 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGrade2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label90 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCondition2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label91 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label92 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPhysical1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhysical10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunctional8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label93 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOCC1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtstories1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtbasefloor1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label94 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label95 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label96 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtclass1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label97 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtgradefactor1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExteriorWalls1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtheatcool1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearBuilt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label98 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label99 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtperimeter1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearRemodelled1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtcond1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label100 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtfunc1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txteconomic1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label101 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label102 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label103 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtphys1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label104 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label105 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOCC2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtstories2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDwellingUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBaseFloor2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label106 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label107 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label108 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtclass2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label109 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtgradefactor2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label110 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtExteriorWalls2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCool2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearBuilt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label111 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label112 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label113 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPerimeter2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYearRemodelled2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtcond2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label114 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label115 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtfunc2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label116 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label117 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtphys2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtqual1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtqual2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeight2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtheight1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDwellingUnits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label118 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label119 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label120 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMap = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOIGrade1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOIGrade10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOIGrade9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOIGrade8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOIGrade7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOIGrade6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOIGrade5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOIGrade4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOIGrade3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOIGrade2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label121 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtReference2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label122 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label123 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label124 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSecondOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label125 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSV1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSV2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSV3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSV4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSV5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSV6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSV7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSV9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSV10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSV8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label126 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUseSV1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUseSV2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUseSV3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUseSV4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUseSV5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUseSV6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUseSV7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUseSV9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUseSV10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUseSV8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label127 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTreeGrowthYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTopography2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUtility2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptCode2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptCode3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPropertyCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label128 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtCaption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.Field68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptCode1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTranCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuildingCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeighborhood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopography)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtXCoordinate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilities)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYCoordinate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtForestYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAffidavit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleVerified)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleValidity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalePrice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleFinancing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuildingStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofSurface)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDwellingUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMasonryTrim)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorWalls)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearBuilt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearRemodeled)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBSMTGar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetBasement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBSMTLiving)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBSMTGrade)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercentCooled)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercentHeated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRooms)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBedrooms)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFullBaths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFixtures)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtfireplaces)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHalfBaths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFactor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttic)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSQFootage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnfinished)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysicalGood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctGood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconGood)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfoCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateInspected)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOCC1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtstories1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbasefloor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtclass1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtgradefactor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorWalls1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtheatcool1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearBuilt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtperimeter1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearRemodelled1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtcond1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtfunc1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txteconomic1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtphys1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOCC2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtstories2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDwellingUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaseFloor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtclass2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtgradefactor2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorWalls2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCool2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearBuilt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPerimeter2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearRemodelled2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtcond2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtfunc2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtphys2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtqual1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtqual2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeight2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtheight1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDwellingUnits1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMap)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReference2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTreeGrowthYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopography2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtility2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptCode2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptCode3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPropertyCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field68,
				this.Field20,
				this.txtCaption2,
				this.txtLand,
				this.txtBuilding,
				this.txtExemption,
				this.Label2,
				this.Label4,
				this.Label6,
				this.Label7,
				this.txtName,
				this.txtLocation,
				this.txtAddress,
				this.txtReference,
				this.Label8,
				this.Label10,
				this.Label11,
				this.txtAddress2,
				this.Label12,
				this.Field1,
				this.Label13,
				this.txtZip,
				this.Label14,
				this.txtState,
				this.Label15,
				this.txtExemptCode1,
				this.txtTranCode,
				this.txtLandCode,
				this.txtBuildingCode,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.txtType1,
				this.Label20,
				this.txtType2,
				this.txtType3,
				this.Label21,
				this.Label22,
				this.txtNeighborhood,
				this.txtTopography,
				this.txtXCoordinate,
				this.txtUtilities,
				this.Label23,
				this.Label24,
				this.Label25,
				this.txtYCoordinate,
				this.Label26,
				this.txtZone,
				this.Label27,
				this.txtZone2,
				this.txtForestYear,
				this.txtAffidavit,
				this.Label28,
				this.Label29,
				this.Label30,
				this.txtStreet,
				this.Field12,
				this.Label31,
				this.txtSaleVerified,
				this.txtSaleDate,
				this.txtSaleValidity,
				this.Label32,
				this.Label33,
				this.Label34,
				this.txtSalePrice,
				this.Label35,
				this.txtSaleType,
				this.Label36,
				this.txtSaleFinancing,
				this.Field19,
				this.Label37,
				this.Label38,
				this.Label39,
				this.Label40,
				this.txtLandType1,
				this.txtLandUnits1,
				this.txtInf1,
				this.txtCd1,
				this.txtLandType2,
				this.txtLandUnits2,
				this.txtInf2,
				this.txtCd2,
				this.txtLandType3,
				this.txtLandUnits3,
				this.txtInf3,
				this.txtCd3,
				this.txtLandType4,
				this.txtLandUnits4,
				this.txtInf4,
				this.txtCd4,
				this.txtLandType5,
				this.txtLandUnits5,
				this.txtInf5,
				this.txtCd5,
				this.txtLandType6,
				this.txtLandUnits6,
				this.txtInf6,
				this.txtCd6,
				this.txtLandType7,
				this.txtLandUnits7,
				this.txtInf7,
				this.txtCd7,
				this.Label41,
				this.Label42,
				this.txtBuildingStyle,
				this.txtRoofSurface,
				this.txtDwellingUnits,
				this.txtMasonryTrim,
				this.Label43,
				this.Label44,
				this.Label45,
				this.txtOtherUnits,
				this.Label46,
				this.txtStories,
				this.Label47,
				this.txtExteriorWalls,
				this.txtOpen4,
				this.txtYearBuilt,
				this.Label48,
				this.Label49,
				this.Label50,
				this.txtOpen3,
				this.txtYearRemodeled,
				this.txtFoundation,
				this.Label51,
				this.Label52,
				this.txtBSMTGar,
				this.txtWetBasement,
				this.Label53,
				this.Label54,
				this.Label55,
				this.txtBasement,
				this.Label56,
				this.Label57,
				this.txtBSMTLiving,
				this.txtCoolType,
				this.txtBSMTGrade,
				this.txtPercentCooled,
				this.Label58,
				this.Label59,
				this.Label60,
				this.txtOpen5,
				this.Label61,
				this.txtHeatType,
				this.Label62,
				this.txtPercentHeated,
				this.txtBathStyle,
				this.txtRooms,
				this.Label63,
				this.Label64,
				this.Label65,
				this.txtKitchenStyle,
				this.txtBedrooms,
				this.txtFullBaths,
				this.Label66,
				this.Label67,
				this.txtFixtures,
				this.txtfireplaces,
				this.Label68,
				this.Label69,
				this.Label70,
				this.txtHalfBaths,
				this.Label71,
				this.Label72,
				this.txtLayout,
				this.txtFactor,
				this.txtAttic,
				this.txtSQFootage,
				this.Label73,
				this.Label74,
				this.Label75,
				this.txtInsulation,
				this.Label76,
				this.txtUnfinished,
				this.Label77,
				this.txtGrade,
				this.txtPhysicalGood,
				this.txtFunctGood,
				this.Label78,
				this.Label79,
				this.Label80,
				this.txtCondition,
				this.txtFunctCode,
				this.txtEconGood,
				this.Label81,
				this.Label82,
				this.txtEntranceCode,
				this.txtInfoCode,
				this.Label83,
				this.Label84,
				this.Label85,
				this.txtEconCode,
				this.txtDateInspected,
				this.Label86,
				this.Field67,
				this.txtType4,
				this.txtType5,
				this.txtType6,
				this.txtType7,
				this.txtType8,
				this.txtType9,
				this.txtType10,
				this.txtYear1,
				this.Label87,
				this.txtYear2,
				this.txtYear3,
				this.txtYear4,
				this.txtYear5,
				this.txtYear6,
				this.txtYear7,
				this.txtYear8,
				this.txtYear9,
				this.txtYear10,
				this.txtUnits1,
				this.Label88,
				this.txtUnits2,
				this.txtUnits3,
				this.txtUnits4,
				this.txtUnits5,
				this.txtUnits6,
				this.txtUnits7,
				this.txtUnits8,
				this.txtUnits9,
				this.txtUnits10,
				this.txtGrade1,
				this.Label89,
				this.txtGrade2,
				this.txtGrade3,
				this.txtGrade4,
				this.txtGrade5,
				this.txtGrade6,
				this.txtGrade7,
				this.txtGrade8,
				this.txtGrade9,
				this.txtGrade10,
				this.txtCondition1,
				this.Label90,
				this.txtCondition2,
				this.txtCondition6,
				this.txtCondition3,
				this.txtCondition4,
				this.txtCondition5,
				this.txtCondition7,
				this.txtCondition8,
				this.txtCondition9,
				this.txtCondition10,
				this.Label91,
				this.Label92,
				this.txtPhysical1,
				this.txtPhysical2,
				this.txtPhysical3,
				this.txtPhysical4,
				this.txtPhysical5,
				this.txtPhysical6,
				this.txtPhysical7,
				this.txtPhysical8,
				this.txtPhysical9,
				this.txtPhysical10,
				this.txtFunctional1,
				this.txtFunctional2,
				this.txtFunctional3,
				this.txtFunctional4,
				this.txtFunctional5,
				this.txtFunctional6,
				this.txtFunctional7,
				this.txtFunctional9,
				this.txtFunctional10,
				this.txtFunctional8,
				this.Label93,
				this.txtOCC1,
				this.txtstories1,
				this.txtbasefloor1,
				this.Label94,
				this.Label95,
				this.Label96,
				this.txtclass1,
				this.Label97,
				this.txtgradefactor1,
				this.txtExteriorWalls1,
				this.txtheatcool1,
				this.txtYearBuilt1,
				this.Label98,
				this.Label99,
				this.txtperimeter1,
				this.txtYearRemodelled1,
				this.txtcond1,
				this.Label100,
				this.txtfunc1,
				this.txteconomic1,
				this.Label101,
				this.Label102,
				this.Label103,
				this.txtphys1,
				this.Label104,
				this.Label105,
				this.txtOCC2,
				this.txtstories2,
				this.txtDwellingUnits2,
				this.txtBaseFloor2,
				this.Label106,
				this.Label107,
				this.Label108,
				this.txtclass2,
				this.Label109,
				this.txtgradefactor2,
				this.Label110,
				this.txtExteriorWalls2,
				this.txtHeatCool2,
				this.txtYearBuilt2,
				this.Label111,
				this.Label112,
				this.Label113,
				this.txtPerimeter2,
				this.txtYearRemodelled2,
				this.txtcond2,
				this.Label114,
				this.Label115,
				this.txtfunc2,
				this.Label116,
				this.Label117,
				this.txtphys2,
				this.txtqual1,
				this.txtqual2,
				this.txtHeight2,
				this.txtheight1,
				this.txtDwellingUnits1,
				this.Label118,
				this.txtAccount,
				this.txtCard,
				this.Label119,
				this.Label120,
				this.txtMap,
				this.txtOIGrade1,
				this.txtOIGrade10,
				this.txtOIGrade9,
				this.txtOIGrade8,
				this.txtOIGrade7,
				this.txtOIGrade6,
				this.txtOIGrade5,
				this.txtOIGrade4,
				this.txtOIGrade3,
				this.txtOIGrade2,
				this.Label121,
				this.txtReference2,
				this.Label122,
				this.txtBookPage,
				this.Label123,
				this.Label124,
				this.txtSecondOwner,
				this.Label125,
				this.txtSV1,
				this.txtSV2,
				this.txtSV3,
				this.txtSV4,
				this.txtSV5,
				this.txtSV6,
				this.txtSV7,
				this.txtSV9,
				this.txtSV10,
				this.txtSV8,
				this.Label126,
				this.txtUseSV1,
				this.txtUseSV2,
				this.txtUseSV3,
				this.txtUseSV4,
				this.txtUseSV5,
				this.txtUseSV6,
				this.txtUseSV7,
				this.txtUseSV9,
				this.txtUseSV10,
				this.txtUseSV8,
				this.Label127,
				this.txtTreeGrowthYear,
				this.txtTopography2,
				this.txtUtility2,
				this.txtExemptCode2,
				this.txtExemptCode3,
				this.txtPropertyCode,
				this.Label128
			});
			this.Detail.Height = 10F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Field68
			// 
			this.Field68.CanGrow = false;
			this.Field68.Height = 0.1875F;
			this.Field68.Left = 0.125F;
			this.Field68.Name = "Field68";
			this.Field68.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center; ddo-char-set: 0";
			this.Field68.Text = "C O M M E R C I A L   D A T A";
			this.Field68.Top = 4.239583F;
			this.Field68.Width = 7.25F;
			// 
			// Field20
			// 
			this.Field20.CanGrow = false;
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 0.125F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center; ddo-char-set: 0";
			this.Field20.Text = "D W E L L I N G   D A T A";
			this.Field20.Top = 4.239583F;
			this.Field20.Width = 7.25F;
			// 
			// txtCaption2
			// 
			this.txtCaption2.Height = 0.1875F;
			this.txtCaption2.Left = 0.125F;
			this.txtCaption2.Name = "txtCaption2";
			this.txtCaption2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.txtCaption2.Text = "P R O P E R T Y  D A T A";
			this.txtCaption2.Top = 1.864583F;
			this.txtCaption2.Width = 7.25F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.1666667F;
			this.txtLand.Left = 5.375F;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtLand.Text = null;
			this.txtLand.Top = 0.1666667F;
			this.txtLand.Width = 0.875F;
			// 
			// txtBuilding
			// 
			this.txtBuilding.Height = 0.1666667F;
			this.txtBuilding.Left = 5.375F;
			this.txtBuilding.Name = "txtBuilding";
			this.txtBuilding.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtBuilding.Text = null;
			this.txtBuilding.Top = 0.3333333F;
			this.txtBuilding.Width = 0.875F;
			// 
			// txtExemption
			// 
			this.txtExemption.Height = 0.1666667F;
			this.txtExemption.Left = 5.375F;
			this.txtExemption.Name = "txtExemption";
			this.txtExemption.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtExemption.Text = null;
			this.txtExemption.Top = 0.5F;
			this.txtExemption.Width = 0.875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1666667F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.25F;
			this.Label2.MultiLine = false;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label2.Text = "Name ";
			this.Label2.Top = 0.1666667F;
			this.Label2.Width = 0.375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1666667F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.25F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label4.Text = "Address 1";
			this.Label4.Top = 0.5F;
			this.Label4.Width = 0.6875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1666667F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.625F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label6.Text = "Building";
			this.Label6.Top = 0.3333333F;
			this.Label6.Width = 0.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1666667F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.4375F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label7.Text = "Exemption";
			this.Label7.Top = 0.5F;
			this.Label7.Width = 0.875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1666667F;
			this.txtName.Left = 1F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtName.Text = null;
			this.txtName.Top = 0.1666667F;
			this.txtName.Width = 2.770833F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1666667F;
			this.txtLocation.Left = 1F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 1F;
			this.txtLocation.Width = 2.1875F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1666667F;
			this.txtAddress.Left = 1F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 0.5F;
			this.txtAddress.Width = 2.1875F;
			// 
			// txtReference
			// 
			this.txtReference.Height = 0.1666667F;
			this.txtReference.Left = 1F;
			this.txtReference.Name = "txtReference";
			this.txtReference.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtReference.Text = null;
			this.txtReference.Top = 1.166667F;
			this.txtReference.Width = 2.1875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1666667F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.25F;
			this.Label8.MultiLine = false;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label8.Text = "Location";
			this.Label8.Top = 1F;
			this.Label8.Width = 0.6875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1666667F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.25F;
			this.Label10.MultiLine = false;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label10.Text = "Reference 1";
			this.Label10.Top = 1.166667F;
			this.Label10.Width = 0.6875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1666667F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.25F;
			this.Label11.MultiLine = false;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label11.Text = "Address 2";
			this.Label11.Top = 0.6666667F;
			this.Label11.Width = 0.6875F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.1666667F;
			this.txtAddress2.Left = 1F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 0.6666667F;
			this.txtAddress2.Width = 2.1875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1666667F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.25F;
			this.Label12.MultiLine = false;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label12.Text = "City";
			this.Label12.Top = 0.8333333F;
			this.Label12.Width = 0.6875F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1666667F;
			this.Field1.Left = 1F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Field1.Text = null;
			this.Field1.Top = 0.8333333F;
			this.Field1.Width = 1.375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1666667F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.0625F;
			this.Label13.MultiLine = false;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label13.Text = "Zip";
			this.Label13.Top = 0.8333333F;
			this.Label13.Width = 0.2708333F;
			// 
			// txtZip
			// 
			this.txtZip.Height = 0.1666667F;
			this.txtZip.Left = 3.333333F;
			this.txtZip.Name = "txtZip";
			this.txtZip.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtZip.Text = null;
			this.txtZip.Top = 0.8333333F;
			this.txtZip.Width = 0.7708333F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1666667F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.4375F;
			this.Label14.MultiLine = false;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label14.Text = "ST";
			this.Label14.Top = 0.8333333F;
			this.Label14.Width = 0.2708333F;
			// 
			// txtState
			// 
			this.txtState.Height = 0.1666667F;
			this.txtState.Left = 2.708333F;
			this.txtState.Name = "txtState";
			this.txtState.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtState.Text = null;
			this.txtState.Top = 0.8333333F;
			this.txtState.Width = 0.3125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1666667F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 4.625F;
			this.Label15.MultiLine = false;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label15.Text = "Land";
			this.Label15.Top = 0.1666667F;
			this.Label15.Width = 0.6875F;
			// 
			// txtExemptCode1
			// 
			this.txtExemptCode1.Height = 0.1666667F;
			this.txtExemptCode1.Left = 5.375F;
			this.txtExemptCode1.Name = "txtExemptCode1";
			this.txtExemptCode1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtExemptCode1.Text = null;
			this.txtExemptCode1.Top = 0.6666667F;
			this.txtExemptCode1.Width = 1.875F;
			// 
			// txtTranCode
			// 
			this.txtTranCode.Height = 0.1666667F;
			this.txtTranCode.Left = 5.375F;
			this.txtTranCode.Name = "txtTranCode";
			this.txtTranCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtTranCode.Text = null;
			this.txtTranCode.Top = 1.166667F;
			this.txtTranCode.Width = 1.875F;
			// 
			// txtLandCode
			// 
			this.txtLandCode.Height = 0.1666667F;
			this.txtLandCode.Left = 5.375F;
			this.txtLandCode.Name = "txtLandCode";
			this.txtLandCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtLandCode.Text = null;
			this.txtLandCode.Top = 1.333333F;
			this.txtLandCode.Width = 1.875F;
			// 
			// txtBuildingCode
			// 
			this.txtBuildingCode.Height = 0.1666667F;
			this.txtBuildingCode.Left = 5.375F;
			this.txtBuildingCode.Name = "txtBuildingCode";
			this.txtBuildingCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtBuildingCode.Text = null;
			this.txtBuildingCode.Top = 1.5F;
			this.txtBuildingCode.Width = 1.875F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1666667F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 4.4375F;
			this.Label16.MultiLine = false;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label16.Text = "Building Code";
			this.Label16.Top = 1.5F;
			this.Label16.Width = 0.875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1666667F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4.625F;
			this.Label17.MultiLine = false;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label17.Text = "Tran Code";
			this.Label17.Top = 1.166667F;
			this.Label17.Width = 0.6875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1666667F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 4.625F;
			this.Label18.MultiLine = false;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label18.Text = "Land Code";
			this.Label18.Top = 1.333333F;
			this.Label18.Width = 0.6875F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1666667F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 4F;
			this.Label19.MultiLine = false;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label19.Text = "Exempt Code (s)";
			this.Label19.Top = 0.6666667F;
			this.Label19.Width = 1.3125F;
			// 
			// txtType1
			// 
			this.txtType1.Height = 0.1666667F;
			this.txtType1.Left = 0.3125F;
			this.txtType1.Name = "txtType1";
			this.txtType1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtType1.Text = null;
			this.txtType1.Top = 7.541667F;
			this.txtType1.Width = 0.4375F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.3125F;
			this.Label20.MultiLine = false;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Label20.Text = "Type";
			this.Label20.Top = 7.354167F;
			this.Label20.Width = 0.4375F;
			// 
			// txtType2
			// 
			this.txtType2.Height = 0.1666667F;
			this.txtType2.Left = 0.3125F;
			this.txtType2.Name = "txtType2";
			this.txtType2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtType2.Text = null;
			this.txtType2.Top = 7.708333F;
			this.txtType2.Width = 0.4375F;
			// 
			// txtType3
			// 
			this.txtType3.Height = 0.1666667F;
			this.txtType3.Left = 0.3125F;
			this.txtType3.Name = "txtType3";
			this.txtType3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtType3.Text = null;
			this.txtType3.Top = 7.885417F;
			this.txtType3.Width = 0.4375F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1666667F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.0625F;
			this.Label21.MultiLine = false;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label21.Text = "Neighborhood";
			this.Label21.Top = 2.052083F;
			this.Label21.Width = 1.0625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1666667F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.0625F;
			this.Label22.MultiLine = false;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label22.Text = "X-Coordinate";
			this.Label22.Top = 2.385417F;
			this.Label22.Width = 1.0625F;
			// 
			// txtNeighborhood
			// 
			this.txtNeighborhood.Height = 0.1666667F;
			this.txtNeighborhood.Left = 1.125F;
			this.txtNeighborhood.Name = "txtNeighborhood";
			this.txtNeighborhood.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtNeighborhood.Text = null;
			this.txtNeighborhood.Top = 2.052083F;
			this.txtNeighborhood.Width = 1.0625F;
			// 
			// txtTopography
			// 
			this.txtTopography.Height = 0.1666667F;
			this.txtTopography.Left = 1.125F;
			this.txtTopography.Name = "txtTopography";
			this.txtTopography.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTopography.Text = null;
			this.txtTopography.Top = 3.052083F;
			this.txtTopography.Width = 1.0625F;
			// 
			// txtXCoordinate
			// 
			this.txtXCoordinate.Height = 0.1666667F;
			this.txtXCoordinate.Left = 1.125F;
			this.txtXCoordinate.Name = "txtXCoordinate";
			this.txtXCoordinate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtXCoordinate.Text = null;
			this.txtXCoordinate.Top = 2.385417F;
			this.txtXCoordinate.Width = 1.0625F;
			// 
			// txtUtilities
			// 
			this.txtUtilities.Height = 0.1666667F;
			this.txtUtilities.Left = 1.125F;
			this.txtUtilities.Name = "txtUtilities";
			this.txtUtilities.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtUtilities.Text = null;
			this.txtUtilities.Top = 3.385417F;
			this.txtUtilities.Width = 1.0625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1666667F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.0625F;
			this.Label23.MultiLine = false;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label23.Text = "Topography";
			this.Label23.Top = 3.052083F;
			this.Label23.Width = 1.0625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1666667F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.0625F;
			this.Label24.MultiLine = false;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label24.Text = "Utilities";
			this.Label24.Top = 3.385417F;
			this.Label24.Width = 1.0625F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1666667F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.0625F;
			this.Label25.MultiLine = false;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label25.Text = "Y-Coordinate";
			this.Label25.Top = 2.552083F;
			this.Label25.Width = 1.0625F;
			// 
			// txtYCoordinate
			// 
			this.txtYCoordinate.Height = 0.1666667F;
			this.txtYCoordinate.Left = 1.125F;
			this.txtYCoordinate.Name = "txtYCoordinate";
			this.txtYCoordinate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtYCoordinate.Text = null;
			this.txtYCoordinate.Top = 2.552083F;
			this.txtYCoordinate.Width = 1.0625F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1666667F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.0625F;
			this.Label26.MultiLine = false;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label26.Text = "Zoning / Use";
			this.Label26.Top = 2.71875F;
			this.Label26.Width = 1.0625F;
			// 
			// txtZone
			// 
			this.txtZone.Height = 0.1666667F;
			this.txtZone.Left = 1.125F;
			this.txtZone.Name = "txtZone";
			this.txtZone.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtZone.Text = null;
			this.txtZone.Top = 2.71875F;
			this.txtZone.Width = 1.0625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1666667F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.0625F;
			this.Label27.MultiLine = false;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label27.Text = "Secondary Zone";
			this.Label27.Top = 2.885417F;
			this.Label27.Width = 1.0625F;
			// 
			// txtZone2
			// 
			this.txtZone2.Height = 0.1666667F;
			this.txtZone2.Left = 1.125F;
			this.txtZone2.Name = "txtZone2";
			this.txtZone2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtZone2.Text = null;
			this.txtZone2.Top = 2.885417F;
			this.txtZone2.Width = 1.0625F;
			// 
			// txtForestYear
			// 
			this.txtForestYear.Height = 0.1666667F;
			this.txtForestYear.Left = 1.125F;
			this.txtForestYear.Name = "txtForestYear";
			this.txtForestYear.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtForestYear.Text = null;
			this.txtForestYear.Top = 3.885417F;
			this.txtForestYear.Width = 1.0625F;
			// 
			// txtAffidavit
			// 
			this.txtAffidavit.Height = 0.1666667F;
			this.txtAffidavit.Left = 1.125F;
			this.txtAffidavit.Name = "txtAffidavit";
			this.txtAffidavit.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAffidavit.Text = null;
			this.txtAffidavit.Top = 4.052083F;
			this.txtAffidavit.Width = 1.0625F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1666667F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.0625F;
			this.Label28.MultiLine = false;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label28.Text = "Forest Plan Year";
			this.Label28.Top = 3.885417F;
			this.Label28.Width = 1.0625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1666667F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0.0625F;
			this.Label29.MultiLine = false;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label29.Text = "Tree Affidavit";
			this.Label29.Top = 4.052083F;
			this.Label29.Width = 1.0625F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1666667F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0.0625F;
			this.Label30.MultiLine = false;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label30.Text = "Street";
			this.Label30.Top = 3.71875F;
			this.Label30.Width = 1.0625F;
			// 
			// txtStreet
			// 
			this.txtStreet.Height = 0.1666667F;
			this.txtStreet.Left = 1.125F;
			this.txtStreet.Name = "txtStreet";
			this.txtStreet.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtStreet.Text = null;
			this.txtStreet.Top = 3.71875F;
			this.txtStreet.Width = 1.0625F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1666667F;
			this.Field12.Left = 2.3125F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field12.Text = "- S A L E   D A T A -";
			this.Field12.Top = 2.052083F;
			this.Field12.Width = 1.9375F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1666667F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 2.25F;
			this.Label31.MultiLine = false;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label31.Text = "Date";
			this.Label31.Top = 2.21875F;
			this.Label31.Width = 0.75F;
			// 
			// txtSaleVerified
			// 
			this.txtSaleVerified.Height = 0.1666667F;
			this.txtSaleVerified.Left = 3F;
			this.txtSaleVerified.Name = "txtSaleVerified";
			this.txtSaleVerified.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSaleVerified.Text = null;
			this.txtSaleVerified.Top = 2.885417F;
			this.txtSaleVerified.Width = 1.3125F;
			// 
			// txtSaleDate
			// 
			this.txtSaleDate.Height = 0.1666667F;
			this.txtSaleDate.Left = 3F;
			this.txtSaleDate.Name = "txtSaleDate";
			this.txtSaleDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSaleDate.Text = null;
			this.txtSaleDate.Top = 2.21875F;
			this.txtSaleDate.Width = 1.3125F;
			// 
			// txtSaleValidity
			// 
			this.txtSaleValidity.Height = 0.1666667F;
			this.txtSaleValidity.Left = 3F;
			this.txtSaleValidity.Name = "txtSaleValidity";
			this.txtSaleValidity.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSaleValidity.Text = null;
			this.txtSaleValidity.Top = 3.052083F;
			this.txtSaleValidity.Width = 1.3125F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1666667F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 2.25F;
			this.Label32.MultiLine = false;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label32.Text = "Verified";
			this.Label32.Top = 2.885417F;
			this.Label32.Width = 0.75F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1666667F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 2.25F;
			this.Label33.MultiLine = false;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label33.Text = "Validity";
			this.Label33.Top = 3.052083F;
			this.Label33.Width = 0.75F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1666667F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 2.25F;
			this.Label34.MultiLine = false;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label34.Text = "Price";
			this.Label34.Top = 2.385417F;
			this.Label34.Width = 0.75F;
			// 
			// txtSalePrice
			// 
			this.txtSalePrice.Height = 0.1666667F;
			this.txtSalePrice.Left = 3F;
			this.txtSalePrice.Name = "txtSalePrice";
			this.txtSalePrice.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSalePrice.Text = null;
			this.txtSalePrice.Top = 2.385417F;
			this.txtSalePrice.Width = 1.3125F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1666667F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 2.25F;
			this.Label35.MultiLine = false;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label35.Text = "Sale Type";
			this.Label35.Top = 2.552083F;
			this.Label35.Width = 0.75F;
			// 
			// txtSaleType
			// 
			this.txtSaleType.Height = 0.1666667F;
			this.txtSaleType.Left = 3F;
			this.txtSaleType.Name = "txtSaleType";
			this.txtSaleType.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSaleType.Text = null;
			this.txtSaleType.Top = 2.552083F;
			this.txtSaleType.Width = 1.3125F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1666667F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 2.25F;
			this.Label36.MultiLine = false;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label36.Text = "Financing";
			this.Label36.Top = 2.71875F;
			this.Label36.Width = 0.75F;
			// 
			// txtSaleFinancing
			// 
			this.txtSaleFinancing.Height = 0.1666667F;
			this.txtSaleFinancing.Left = 3F;
			this.txtSaleFinancing.Name = "txtSaleFinancing";
			this.txtSaleFinancing.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSaleFinancing.Text = null;
			this.txtSaleFinancing.Top = 2.71875F;
			this.txtSaleFinancing.Width = 1.3125F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.1666667F;
			this.Field19.Left = 4.5625F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field19.Text = " - L A N D   D A T A -";
			this.Field19.Top = 2.052083F;
			this.Field19.Width = 2F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1666667F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 4.375F;
			this.Label37.MultiLine = false;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label37.Text = "Type";
			this.Label37.Top = 2.21875F;
			this.Label37.Width = 0.4375F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1666667F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 5.5F;
			this.Label38.MultiLine = false;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label38.Text = "Units";
			this.Label38.Top = 2.21875F;
			this.Label38.Width = 0.6875F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.1666667F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 6.25F;
			this.Label39.MultiLine = false;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label39.Text = "Inf";
			this.Label39.Top = 2.21875F;
			this.Label39.Width = 0.3125F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1666667F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 6.625F;
			this.Label40.MultiLine = false;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.Label40.Text = "Cd";
			this.Label40.Top = 2.21875F;
			this.Label40.Width = 0.3125F;
			// 
			// txtLandType1
			// 
			this.txtLandType1.Height = 0.1666667F;
			this.txtLandType1.Left = 4.375F;
			this.txtLandType1.Name = "txtLandType1";
			this.txtLandType1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtLandType1.Text = null;
			this.txtLandType1.Top = 2.385417F;
			this.txtLandType1.Width = 1.0625F;
			// 
			// txtLandUnits1
			// 
			this.txtLandUnits1.Height = 0.1666667F;
			this.txtLandUnits1.Left = 5.5F;
			this.txtLandUnits1.Name = "txtLandUnits1";
			this.txtLandUnits1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLandUnits1.Text = null;
			this.txtLandUnits1.Top = 2.385417F;
			this.txtLandUnits1.Width = 0.6875F;
			// 
			// txtInf1
			// 
			this.txtInf1.Height = 0.1666667F;
			this.txtInf1.Left = 6.25F;
			this.txtInf1.Name = "txtInf1";
			this.txtInf1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtInf1.Text = null;
			this.txtInf1.Top = 2.385417F;
			this.txtInf1.Width = 0.3125F;
			// 
			// txtCd1
			// 
			this.txtCd1.Height = 0.1666667F;
			this.txtCd1.Left = 6.625F;
			this.txtCd1.Name = "txtCd1";
			this.txtCd1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtCd1.Text = null;
			this.txtCd1.Top = 2.385417F;
			this.txtCd1.Width = 0.8125F;
			// 
			// txtLandType2
			// 
			this.txtLandType2.Height = 0.1666667F;
			this.txtLandType2.Left = 4.375F;
			this.txtLandType2.Name = "txtLandType2";
			this.txtLandType2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtLandType2.Text = null;
			this.txtLandType2.Top = 2.552083F;
			this.txtLandType2.Width = 1.0625F;
			// 
			// txtLandUnits2
			// 
			this.txtLandUnits2.Height = 0.1666667F;
			this.txtLandUnits2.Left = 5.5F;
			this.txtLandUnits2.Name = "txtLandUnits2";
			this.txtLandUnits2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLandUnits2.Text = null;
			this.txtLandUnits2.Top = 2.552083F;
			this.txtLandUnits2.Width = 0.6875F;
			// 
			// txtInf2
			// 
			this.txtInf2.Height = 0.1666667F;
			this.txtInf2.Left = 6.25F;
			this.txtInf2.Name = "txtInf2";
			this.txtInf2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtInf2.Text = null;
			this.txtInf2.Top = 2.552083F;
			this.txtInf2.Width = 0.3125F;
			// 
			// txtCd2
			// 
			this.txtCd2.Height = 0.1666667F;
			this.txtCd2.Left = 6.625F;
			this.txtCd2.Name = "txtCd2";
			this.txtCd2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtCd2.Text = null;
			this.txtCd2.Top = 2.552083F;
			this.txtCd2.Width = 0.8125F;
			// 
			// txtLandType3
			// 
			this.txtLandType3.Height = 0.1666667F;
			this.txtLandType3.Left = 4.375F;
			this.txtLandType3.Name = "txtLandType3";
			this.txtLandType3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtLandType3.Text = null;
			this.txtLandType3.Top = 2.71875F;
			this.txtLandType3.Width = 1.0625F;
			// 
			// txtLandUnits3
			// 
			this.txtLandUnits3.Height = 0.1666667F;
			this.txtLandUnits3.Left = 5.5F;
			this.txtLandUnits3.Name = "txtLandUnits3";
			this.txtLandUnits3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLandUnits3.Text = null;
			this.txtLandUnits3.Top = 2.71875F;
			this.txtLandUnits3.Width = 0.6875F;
			// 
			// txtInf3
			// 
			this.txtInf3.Height = 0.1666667F;
			this.txtInf3.Left = 6.25F;
			this.txtInf3.Name = "txtInf3";
			this.txtInf3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtInf3.Text = null;
			this.txtInf3.Top = 2.71875F;
			this.txtInf3.Width = 0.3125F;
			// 
			// txtCd3
			// 
			this.txtCd3.Height = 0.1666667F;
			this.txtCd3.Left = 6.625F;
			this.txtCd3.Name = "txtCd3";
			this.txtCd3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtCd3.Text = null;
			this.txtCd3.Top = 2.71875F;
			this.txtCd3.Width = 0.8125F;
			// 
			// txtLandType4
			// 
			this.txtLandType4.Height = 0.1666667F;
			this.txtLandType4.Left = 4.375F;
			this.txtLandType4.Name = "txtLandType4";
			this.txtLandType4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtLandType4.Text = null;
			this.txtLandType4.Top = 2.885417F;
			this.txtLandType4.Width = 1.0625F;
			// 
			// txtLandUnits4
			// 
			this.txtLandUnits4.Height = 0.1666667F;
			this.txtLandUnits4.Left = 5.5F;
			this.txtLandUnits4.Name = "txtLandUnits4";
			this.txtLandUnits4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLandUnits4.Text = null;
			this.txtLandUnits4.Top = 2.885417F;
			this.txtLandUnits4.Width = 0.6875F;
			// 
			// txtInf4
			// 
			this.txtInf4.Height = 0.1666667F;
			this.txtInf4.Left = 6.25F;
			this.txtInf4.Name = "txtInf4";
			this.txtInf4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtInf4.Text = null;
			this.txtInf4.Top = 2.885417F;
			this.txtInf4.Width = 0.3125F;
			// 
			// txtCd4
			// 
			this.txtCd4.Height = 0.1666667F;
			this.txtCd4.Left = 6.625F;
			this.txtCd4.Name = "txtCd4";
			this.txtCd4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtCd4.Text = null;
			this.txtCd4.Top = 2.885417F;
			this.txtCd4.Width = 0.8125F;
			// 
			// txtLandType5
			// 
			this.txtLandType5.Height = 0.1666667F;
			this.txtLandType5.Left = 4.375F;
			this.txtLandType5.Name = "txtLandType5";
			this.txtLandType5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtLandType5.Text = null;
			this.txtLandType5.Top = 3.052083F;
			this.txtLandType5.Width = 1.0625F;
			// 
			// txtLandUnits5
			// 
			this.txtLandUnits5.Height = 0.1666667F;
			this.txtLandUnits5.Left = 5.5F;
			this.txtLandUnits5.Name = "txtLandUnits5";
			this.txtLandUnits5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLandUnits5.Text = null;
			this.txtLandUnits5.Top = 3.052083F;
			this.txtLandUnits5.Width = 0.6875F;
			// 
			// txtInf5
			// 
			this.txtInf5.Height = 0.1666667F;
			this.txtInf5.Left = 6.25F;
			this.txtInf5.Name = "txtInf5";
			this.txtInf5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtInf5.Text = null;
			this.txtInf5.Top = 3.052083F;
			this.txtInf5.Width = 0.3125F;
			// 
			// txtCd5
			// 
			this.txtCd5.Height = 0.1666667F;
			this.txtCd5.Left = 6.625F;
			this.txtCd5.Name = "txtCd5";
			this.txtCd5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtCd5.Text = null;
			this.txtCd5.Top = 3.052083F;
			this.txtCd5.Width = 0.8125F;
			// 
			// txtLandType6
			// 
			this.txtLandType6.Height = 0.1666667F;
			this.txtLandType6.Left = 4.375F;
			this.txtLandType6.Name = "txtLandType6";
			this.txtLandType6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtLandType6.Text = null;
			this.txtLandType6.Top = 3.21875F;
			this.txtLandType6.Width = 1.0625F;
			// 
			// txtLandUnits6
			// 
			this.txtLandUnits6.Height = 0.1666667F;
			this.txtLandUnits6.Left = 5.5F;
			this.txtLandUnits6.Name = "txtLandUnits6";
			this.txtLandUnits6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLandUnits6.Text = null;
			this.txtLandUnits6.Top = 3.21875F;
			this.txtLandUnits6.Width = 0.6875F;
			// 
			// txtInf6
			// 
			this.txtInf6.Height = 0.1666667F;
			this.txtInf6.Left = 6.25F;
			this.txtInf6.Name = "txtInf6";
			this.txtInf6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtInf6.Text = null;
			this.txtInf6.Top = 3.21875F;
			this.txtInf6.Width = 0.3125F;
			// 
			// txtCd6
			// 
			this.txtCd6.Height = 0.1666667F;
			this.txtCd6.Left = 6.625F;
			this.txtCd6.Name = "txtCd6";
			this.txtCd6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtCd6.Text = null;
			this.txtCd6.Top = 3.21875F;
			this.txtCd6.Width = 0.8125F;
			// 
			// txtLandType7
			// 
			this.txtLandType7.Height = 0.1666667F;
			this.txtLandType7.Left = 4.375F;
			this.txtLandType7.Name = "txtLandType7";
			this.txtLandType7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtLandType7.Text = null;
			this.txtLandType7.Top = 3.385417F;
			this.txtLandType7.Width = 1.0625F;
			// 
			// txtLandUnits7
			// 
			this.txtLandUnits7.Height = 0.1666667F;
			this.txtLandUnits7.Left = 5.5F;
			this.txtLandUnits7.Name = "txtLandUnits7";
			this.txtLandUnits7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtLandUnits7.Text = null;
			this.txtLandUnits7.Top = 3.385417F;
			this.txtLandUnits7.Width = 0.6875F;
			// 
			// txtInf7
			// 
			this.txtInf7.Height = 0.1666667F;
			this.txtInf7.Left = 6.25F;
			this.txtInf7.Name = "txtInf7";
			this.txtInf7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtInf7.Text = null;
			this.txtInf7.Top = 3.385417F;
			this.txtInf7.Width = 0.3125F;
			// 
			// txtCd7
			// 
			this.txtCd7.Height = 0.1666667F;
			this.txtCd7.Left = 6.625F;
			this.txtCd7.Name = "txtCd7";
			this.txtCd7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtCd7.Text = null;
			this.txtCd7.Top = 3.385417F;
			this.txtCd7.Width = 0.8125F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1666667F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 0.125F;
			this.Label41.MultiLine = false;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label41.Text = "Building Style";
			this.Label41.Top = 4.427083F;
			this.Label41.Width = 1.0625F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1666667F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 0.125F;
			this.Label42.MultiLine = false;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label42.Text = "Dwelling Units";
			this.Label42.Top = 4.59375F;
			this.Label42.Width = 1.0625F;
			// 
			// txtBuildingStyle
			// 
			this.txtBuildingStyle.Height = 0.1666667F;
			this.txtBuildingStyle.Left = 1.1875F;
			this.txtBuildingStyle.Name = "txtBuildingStyle";
			this.txtBuildingStyle.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtBuildingStyle.Text = null;
			this.txtBuildingStyle.Top = 4.427083F;
			this.txtBuildingStyle.Width = 0.4375F;
			// 
			// txtRoofSurface
			// 
			this.txtRoofSurface.Height = 0.1666667F;
			this.txtRoofSurface.Left = 1.1875F;
			this.txtRoofSurface.Name = "txtRoofSurface";
			this.txtRoofSurface.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtRoofSurface.Text = null;
			this.txtRoofSurface.Top = 5.260417F;
			this.txtRoofSurface.Width = 0.4375F;
			// 
			// txtDwellingUnits
			// 
			this.txtDwellingUnits.Height = 0.1666667F;
			this.txtDwellingUnits.Left = 1.1875F;
			this.txtDwellingUnits.Name = "txtDwellingUnits";
			this.txtDwellingUnits.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtDwellingUnits.Text = null;
			this.txtDwellingUnits.Top = 4.59375F;
			this.txtDwellingUnits.Width = 0.4375F;
			// 
			// txtMasonryTrim
			// 
			this.txtMasonryTrim.Height = 0.1666667F;
			this.txtMasonryTrim.Left = 1.1875F;
			this.txtMasonryTrim.Name = "txtMasonryTrim";
			this.txtMasonryTrim.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtMasonryTrim.Text = null;
			this.txtMasonryTrim.Top = 5.427083F;
			this.txtMasonryTrim.Width = 0.4375F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1666667F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 0.125F;
			this.Label43.MultiLine = false;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label43.Text = "Roof Surface";
			this.Label43.Top = 5.260417F;
			this.Label43.Width = 1.0625F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1666667F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 0.125F;
			this.Label44.MultiLine = false;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label44.Text = "S/F Masonry Trim";
			this.Label44.Top = 5.427083F;
			this.Label44.Width = 1.0625F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1666667F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 0.125F;
			this.Label45.MultiLine = false;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label45.Text = "Other Units";
			this.Label45.Top = 4.760417F;
			this.Label45.Width = 1.0625F;
			// 
			// txtOtherUnits
			// 
			this.txtOtherUnits.Height = 0.1666667F;
			this.txtOtherUnits.Left = 1.1875F;
			this.txtOtherUnits.Name = "txtOtherUnits";
			this.txtOtherUnits.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtOtherUnits.Text = null;
			this.txtOtherUnits.Top = 4.760417F;
			this.txtOtherUnits.Width = 0.4375F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.1666667F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 0.125F;
			this.Label46.MultiLine = false;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label46.Text = "Stories";
			this.Label46.Top = 4.927083F;
			this.Label46.Width = 1.0625F;
			// 
			// txtStories
			// 
			this.txtStories.Height = 0.1666667F;
			this.txtStories.Left = 1.1875F;
			this.txtStories.Name = "txtStories";
			this.txtStories.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtStories.Text = null;
			this.txtStories.Top = 4.927083F;
			this.txtStories.Width = 0.4375F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.1666667F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 0.125F;
			this.Label47.MultiLine = false;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label47.Text = "Exterior Walls";
			this.Label47.Top = 5.09375F;
			this.Label47.Width = 1.0625F;
			// 
			// txtExteriorWalls
			// 
			this.txtExteriorWalls.Height = 0.1666667F;
			this.txtExteriorWalls.Left = 1.1875F;
			this.txtExteriorWalls.Name = "txtExteriorWalls";
			this.txtExteriorWalls.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtExteriorWalls.Text = null;
			this.txtExteriorWalls.Top = 5.09375F;
			this.txtExteriorWalls.Width = 0.4375F;
			// 
			// txtOpen4
			// 
			this.txtOpen4.Height = 0.1666667F;
			this.txtOpen4.Left = 1.1875F;
			this.txtOpen4.Name = "txtOpen4";
			this.txtOpen4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtOpen4.Text = null;
			this.txtOpen4.Top = 5.760417F;
			this.txtOpen4.Width = 0.4375F;
			// 
			// txtYearBuilt
			// 
			this.txtYearBuilt.Height = 0.1666667F;
			this.txtYearBuilt.Left = 1.1875F;
			this.txtYearBuilt.Name = "txtYearBuilt";
			this.txtYearBuilt.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtYearBuilt.Text = null;
			this.txtYearBuilt.Top = 5.916667F;
			this.txtYearBuilt.Width = 0.4375F;
			// 
			// Label48
			// 
			this.Label48.Height = 0.1666667F;
			this.Label48.HyperLink = null;
			this.Label48.Left = 0.125F;
			this.Label48.MultiLine = false;
			this.Label48.Name = "Label48";
			this.Label48.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label48.Text = "Open -4- Customize";
			this.Label48.Top = 5.760417F;
			this.Label48.Width = 1.0625F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.1666667F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 0.125F;
			this.Label49.MultiLine = false;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label49.Text = "Year Built";
			this.Label49.Top = 5.916667F;
			this.Label49.Width = 1.0625F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.1666667F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 0.125F;
			this.Label50.MultiLine = false;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label50.Text = "Open -3- Customize";
			this.Label50.Top = 5.59375F;
			this.Label50.Width = 1.0625F;
			// 
			// txtOpen3
			// 
			this.txtOpen3.Height = 0.1666667F;
			this.txtOpen3.Left = 1.1875F;
			this.txtOpen3.Name = "txtOpen3";
			this.txtOpen3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtOpen3.Text = null;
			this.txtOpen3.Top = 5.59375F;
			this.txtOpen3.Width = 0.4375F;
			// 
			// txtYearRemodeled
			// 
			this.txtYearRemodeled.Height = 0.1666667F;
			this.txtYearRemodeled.Left = 1.1875F;
			this.txtYearRemodeled.Name = "txtYearRemodeled";
			this.txtYearRemodeled.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtYearRemodeled.Text = null;
			this.txtYearRemodeled.Top = 6.083333F;
			this.txtYearRemodeled.Width = 0.4375F;
			// 
			// txtFoundation
			// 
			this.txtFoundation.Height = 0.1666667F;
			this.txtFoundation.Left = 1.1875F;
			this.txtFoundation.Name = "txtFoundation";
			this.txtFoundation.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtFoundation.Text = null;
			this.txtFoundation.Top = 6.25F;
			this.txtFoundation.Width = 0.4375F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1666667F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 0.125F;
			this.Label51.MultiLine = false;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label51.Text = "Year Remodeled";
			this.Label51.Top = 6.083333F;
			this.Label51.Width = 1.0625F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1666667F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 0.125F;
			this.Label52.MultiLine = false;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label52.Text = "Foundation";
			this.Label52.Top = 6.25F;
			this.Label52.Width = 1.0625F;
			// 
			// txtBSMTGar
			// 
			this.txtBSMTGar.Height = 0.1666667F;
			this.txtBSMTGar.Left = 1.1875F;
			this.txtBSMTGar.Name = "txtBSMTGar";
			this.txtBSMTGar.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtBSMTGar.Text = null;
			this.txtBSMTGar.Top = 6.583333F;
			this.txtBSMTGar.Width = 0.4375F;
			// 
			// txtWetBasement
			// 
			this.txtWetBasement.Height = 0.1666667F;
			this.txtWetBasement.Left = 1.1875F;
			this.txtWetBasement.Name = "txtWetBasement";
			this.txtWetBasement.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtWetBasement.Text = null;
			this.txtWetBasement.Top = 6.75F;
			this.txtWetBasement.Width = 0.4375F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1666667F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 0.125F;
			this.Label53.MultiLine = false;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label53.Text = "BSMT Gar # Cars";
			this.Label53.Top = 6.583333F;
			this.Label53.Width = 1.0625F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.1666667F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 0.125F;
			this.Label54.MultiLine = false;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label54.Text = "Wet Basement";
			this.Label54.Top = 6.75F;
			this.Label54.Width = 1.0625F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.1666667F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 0.125F;
			this.Label55.MultiLine = false;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label55.Text = "Basement";
			this.Label55.Top = 6.416667F;
			this.Label55.Width = 1.0625F;
			// 
			// txtBasement
			// 
			this.txtBasement.Height = 0.1666667F;
			this.txtBasement.Left = 1.1875F;
			this.txtBasement.Name = "txtBasement";
			this.txtBasement.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtBasement.Text = null;
			this.txtBasement.Top = 6.416667F;
			this.txtBasement.Width = 0.4375F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.1666667F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 2.1875F;
			this.Label56.MultiLine = false;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label56.Text = "S/F BSMT Living";
			this.Label56.Top = 4.427083F;
			this.Label56.Width = 1.0625F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1666667F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 2.1875F;
			this.Label57.MultiLine = false;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label57.Text = "Fin BSMT Grade";
			this.Label57.Top = 4.59375F;
			this.Label57.Width = 1.0625F;
			// 
			// txtBSMTLiving
			// 
			this.txtBSMTLiving.Height = 0.1666667F;
			this.txtBSMTLiving.Left = 3.25F;
			this.txtBSMTLiving.Name = "txtBSMTLiving";
			this.txtBSMTLiving.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtBSMTLiving.Text = null;
			this.txtBSMTLiving.Top = 4.427083F;
			this.txtBSMTLiving.Width = 0.4375F;
			// 
			// txtCoolType
			// 
			this.txtCoolType.Height = 0.1666667F;
			this.txtCoolType.Left = 3.25F;
			this.txtCoolType.Name = "txtCoolType";
			this.txtCoolType.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtCoolType.Text = null;
			this.txtCoolType.Top = 5.260417F;
			this.txtCoolType.Width = 0.4375F;
			// 
			// txtBSMTGrade
			// 
			this.txtBSMTGrade.Height = 0.1666667F;
			this.txtBSMTGrade.Left = 3.25F;
			this.txtBSMTGrade.Name = "txtBSMTGrade";
			this.txtBSMTGrade.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtBSMTGrade.Text = null;
			this.txtBSMTGrade.Top = 4.59375F;
			this.txtBSMTGrade.Width = 0.4375F;
			// 
			// txtPercentCooled
			// 
			this.txtPercentCooled.Height = 0.1666667F;
			this.txtPercentCooled.Left = 3.25F;
			this.txtPercentCooled.Name = "txtPercentCooled";
			this.txtPercentCooled.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtPercentCooled.Text = null;
			this.txtPercentCooled.Top = 5.427083F;
			this.txtPercentCooled.Width = 0.4375F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1666667F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 2.1875F;
			this.Label58.MultiLine = false;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label58.Text = "Cool Type";
			this.Label58.Top = 5.260417F;
			this.Label58.Width = 1.0625F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.1666667F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 2.1875F;
			this.Label59.MultiLine = false;
			this.Label59.Name = "Label59";
			this.Label59.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label59.Text = "Percent Cooled";
			this.Label59.Top = 5.427083F;
			this.Label59.Width = 1.0625F;
			// 
			// Label60
			// 
			this.Label60.Height = 0.1666667F;
			this.Label60.HyperLink = null;
			this.Label60.Left = 2.1875F;
			this.Label60.MultiLine = false;
			this.Label60.Name = "Label60";
			this.Label60.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label60.Text = "Open -5- Customize";
			this.Label60.Top = 4.760417F;
			this.Label60.Width = 1.0625F;
			// 
			// txtOpen5
			// 
			this.txtOpen5.Height = 0.1666667F;
			this.txtOpen5.Left = 3.25F;
			this.txtOpen5.Name = "txtOpen5";
			this.txtOpen5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtOpen5.Text = null;
			this.txtOpen5.Top = 4.760417F;
			this.txtOpen5.Width = 0.4375F;
			// 
			// Label61
			// 
			this.Label61.Height = 0.1666667F;
			this.Label61.HyperLink = null;
			this.Label61.Left = 2.1875F;
			this.Label61.MultiLine = false;
			this.Label61.Name = "Label61";
			this.Label61.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label61.Text = "Heat Type";
			this.Label61.Top = 4.927083F;
			this.Label61.Width = 1.0625F;
			// 
			// txtHeatType
			// 
			this.txtHeatType.Height = 0.1666667F;
			this.txtHeatType.Left = 3.25F;
			this.txtHeatType.Name = "txtHeatType";
			this.txtHeatType.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtHeatType.Text = null;
			this.txtHeatType.Top = 4.927083F;
			this.txtHeatType.Width = 0.4375F;
			// 
			// Label62
			// 
			this.Label62.Height = 0.1666667F;
			this.Label62.HyperLink = null;
			this.Label62.Left = 2.1875F;
			this.Label62.MultiLine = false;
			this.Label62.Name = "Label62";
			this.Label62.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label62.Text = "Percent Heated";
			this.Label62.Top = 5.09375F;
			this.Label62.Width = 1.0625F;
			// 
			// txtPercentHeated
			// 
			this.txtPercentHeated.Height = 0.1666667F;
			this.txtPercentHeated.Left = 3.25F;
			this.txtPercentHeated.Name = "txtPercentHeated";
			this.txtPercentHeated.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtPercentHeated.Text = null;
			this.txtPercentHeated.Top = 5.09375F;
			this.txtPercentHeated.Width = 0.4375F;
			// 
			// txtBathStyle
			// 
			this.txtBathStyle.Height = 0.1666667F;
			this.txtBathStyle.Left = 3.25F;
			this.txtBathStyle.Name = "txtBathStyle";
			this.txtBathStyle.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtBathStyle.Text = null;
			this.txtBathStyle.Top = 5.760417F;
			this.txtBathStyle.Width = 0.4375F;
			// 
			// txtRooms
			// 
			this.txtRooms.Height = 0.1666667F;
			this.txtRooms.Left = 3.25F;
			this.txtRooms.Name = "txtRooms";
			this.txtRooms.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtRooms.Text = null;
			this.txtRooms.Top = 5.916667F;
			this.txtRooms.Width = 0.4375F;
			// 
			// Label63
			// 
			this.Label63.Height = 0.1666667F;
			this.Label63.HyperLink = null;
			this.Label63.Left = 2.1875F;
			this.Label63.MultiLine = false;
			this.Label63.Name = "Label63";
			this.Label63.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label63.Text = "Bath(s) Style";
			this.Label63.Top = 5.760417F;
			this.Label63.Width = 1.0625F;
			// 
			// Label64
			// 
			this.Label64.Height = 0.1666667F;
			this.Label64.HyperLink = null;
			this.Label64.Left = 2.1875F;
			this.Label64.MultiLine = false;
			this.Label64.Name = "Label64";
			this.Label64.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label64.Text = "# Rooms";
			this.Label64.Top = 5.916667F;
			this.Label64.Width = 1.0625F;
			// 
			// Label65
			// 
			this.Label65.Height = 0.1666667F;
			this.Label65.HyperLink = null;
			this.Label65.Left = 2.1875F;
			this.Label65.MultiLine = false;
			this.Label65.Name = "Label65";
			this.Label65.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label65.Text = "Kitchen Style";
			this.Label65.Top = 5.59375F;
			this.Label65.Width = 1.0625F;
			// 
			// txtKitchenStyle
			// 
			this.txtKitchenStyle.Height = 0.1666667F;
			this.txtKitchenStyle.Left = 3.25F;
			this.txtKitchenStyle.Name = "txtKitchenStyle";
			this.txtKitchenStyle.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtKitchenStyle.Text = null;
			this.txtKitchenStyle.Top = 5.59375F;
			this.txtKitchenStyle.Width = 0.4375F;
			// 
			// txtBedrooms
			// 
			this.txtBedrooms.Height = 0.1666667F;
			this.txtBedrooms.Left = 3.25F;
			this.txtBedrooms.Name = "txtBedrooms";
			this.txtBedrooms.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtBedrooms.Text = null;
			this.txtBedrooms.Top = 6.083333F;
			this.txtBedrooms.Width = 0.4375F;
			// 
			// txtFullBaths
			// 
			this.txtFullBaths.Height = 0.1666667F;
			this.txtFullBaths.Left = 3.25F;
			this.txtFullBaths.Name = "txtFullBaths";
			this.txtFullBaths.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtFullBaths.Text = null;
			this.txtFullBaths.Top = 6.25F;
			this.txtFullBaths.Width = 0.4375F;
			// 
			// Label66
			// 
			this.Label66.Height = 0.1666667F;
			this.Label66.HyperLink = null;
			this.Label66.Left = 2.1875F;
			this.Label66.MultiLine = false;
			this.Label66.Name = "Label66";
			this.Label66.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label66.Text = "# Bedrooms";
			this.Label66.Top = 6.083333F;
			this.Label66.Width = 1.0625F;
			// 
			// Label67
			// 
			this.Label67.Height = 0.1666667F;
			this.Label67.HyperLink = null;
			this.Label67.Left = 2.1875F;
			this.Label67.MultiLine = false;
			this.Label67.Name = "Label67";
			this.Label67.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label67.Text = "# Full Baths";
			this.Label67.Top = 6.25F;
			this.Label67.Width = 1.0625F;
			// 
			// txtFixtures
			// 
			this.txtFixtures.Height = 0.1666667F;
			this.txtFixtures.Left = 3.25F;
			this.txtFixtures.Name = "txtFixtures";
			this.txtFixtures.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtFixtures.Text = null;
			this.txtFixtures.Top = 6.583333F;
			this.txtFixtures.Width = 0.4375F;
			// 
			// txtfireplaces
			// 
			this.txtfireplaces.Height = 0.1666667F;
			this.txtfireplaces.Left = 3.25F;
			this.txtfireplaces.Name = "txtfireplaces";
			this.txtfireplaces.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtfireplaces.Text = null;
			this.txtfireplaces.Top = 6.75F;
			this.txtfireplaces.Width = 0.4375F;
			// 
			// Label68
			// 
			this.Label68.Height = 0.1666667F;
			this.Label68.HyperLink = null;
			this.Label68.Left = 2.1875F;
			this.Label68.MultiLine = false;
			this.Label68.Name = "Label68";
			this.Label68.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label68.Text = "# ADDN Fixtures";
			this.Label68.Top = 6.583333F;
			this.Label68.Width = 1.0625F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1666667F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 2.1875F;
			this.Label69.MultiLine = false;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label69.Text = "# Fireplaces";
			this.Label69.Top = 6.75F;
			this.Label69.Width = 1.0625F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.1666667F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 2.1875F;
			this.Label70.MultiLine = false;
			this.Label70.Name = "Label70";
			this.Label70.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label70.Text = "# Half Baths";
			this.Label70.Top = 6.416667F;
			this.Label70.Width = 1.0625F;
			// 
			// txtHalfBaths
			// 
			this.txtHalfBaths.Height = 0.1666667F;
			this.txtHalfBaths.Left = 3.25F;
			this.txtHalfBaths.Name = "txtHalfBaths";
			this.txtHalfBaths.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.txtHalfBaths.Text = null;
			this.txtHalfBaths.Top = 6.416667F;
			this.txtHalfBaths.Width = 0.4375F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.1666667F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 4.5625F;
			this.Label71.MultiLine = false;
			this.Label71.Name = "Label71";
			this.Label71.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label71.Text = "Layout";
			this.Label71.Top = 4.427083F;
			this.Label71.Width = 1.0625F;
			// 
			// Label72
			// 
			this.Label72.Height = 0.1666667F;
			this.Label72.HyperLink = null;
			this.Label72.Left = 4.5625F;
			this.Label72.MultiLine = false;
			this.Label72.Name = "Label72";
			this.Label72.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label72.Text = "Attic";
			this.Label72.Top = 4.59375F;
			this.Label72.Width = 1.0625F;
			// 
			// txtLayout
			// 
			this.txtLayout.Height = 0.1666667F;
			this.txtLayout.Left = 5.625F;
			this.txtLayout.Name = "txtLayout";
			this.txtLayout.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtLayout.Text = null;
			this.txtLayout.Top = 4.427083F;
			this.txtLayout.Width = 0.4375F;
			// 
			// txtFactor
			// 
			this.txtFactor.Height = 0.1666667F;
			this.txtFactor.Left = 5.625F;
			this.txtFactor.Name = "txtFactor";
			this.txtFactor.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtFactor.Text = null;
			this.txtFactor.Top = 5.260417F;
			this.txtFactor.Width = 0.4375F;
			// 
			// txtAttic
			// 
			this.txtAttic.Height = 0.1666667F;
			this.txtAttic.Left = 5.625F;
			this.txtAttic.Name = "txtAttic";
			this.txtAttic.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtAttic.Text = null;
			this.txtAttic.Top = 4.59375F;
			this.txtAttic.Width = 0.4375F;
			// 
			// txtSQFootage
			// 
			this.txtSQFootage.Height = 0.1666667F;
			this.txtSQFootage.Left = 5.625F;
			this.txtSQFootage.Name = "txtSQFootage";
			this.txtSQFootage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSQFootage.Text = null;
			this.txtSQFootage.Top = 5.427083F;
			this.txtSQFootage.Width = 0.5625F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.1666667F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 4.5625F;
			this.Label73.MultiLine = false;
			this.Label73.Name = "Label73";
			this.Label73.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label73.Text = "Factor";
			this.Label73.Top = 5.260417F;
			this.Label73.Width = 1.0625F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1666667F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 4.5625F;
			this.Label74.MultiLine = false;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label74.Text = "SQ. Footage";
			this.Label74.Top = 5.427083F;
			this.Label74.Width = 1.0625F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.1666667F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 4.5625F;
			this.Label75.MultiLine = false;
			this.Label75.Name = "Label75";
			this.Label75.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label75.Text = "Insulation";
			this.Label75.Top = 4.760417F;
			this.Label75.Width = 1.0625F;
			// 
			// txtInsulation
			// 
			this.txtInsulation.Height = 0.1666667F;
			this.txtInsulation.Left = 5.625F;
			this.txtInsulation.Name = "txtInsulation";
			this.txtInsulation.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtInsulation.Text = null;
			this.txtInsulation.Top = 4.760417F;
			this.txtInsulation.Width = 0.4375F;
			// 
			// Label76
			// 
			this.Label76.Height = 0.1666667F;
			this.Label76.HyperLink = null;
			this.Label76.Left = 4.5625F;
			this.Label76.MultiLine = false;
			this.Label76.Name = "Label76";
			this.Label76.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label76.Text = "Unfinished %";
			this.Label76.Top = 4.927083F;
			this.Label76.Width = 1.0625F;
			// 
			// txtUnfinished
			// 
			this.txtUnfinished.Height = 0.1666667F;
			this.txtUnfinished.Left = 5.625F;
			this.txtUnfinished.Name = "txtUnfinished";
			this.txtUnfinished.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtUnfinished.Text = null;
			this.txtUnfinished.Top = 4.927083F;
			this.txtUnfinished.Width = 0.4375F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.1666667F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 4.5625F;
			this.Label77.MultiLine = false;
			this.Label77.Name = "Label77";
			this.Label77.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label77.Text = "Grade";
			this.Label77.Top = 5.09375F;
			this.Label77.Width = 1.0625F;
			// 
			// txtGrade
			// 
			this.txtGrade.Height = 0.1666667F;
			this.txtGrade.Left = 5.625F;
			this.txtGrade.Name = "txtGrade";
			this.txtGrade.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtGrade.Text = null;
			this.txtGrade.Top = 5.09375F;
			this.txtGrade.Width = 0.4375F;
			// 
			// txtPhysicalGood
			// 
			this.txtPhysicalGood.Height = 0.1666667F;
			this.txtPhysicalGood.Left = 5.625F;
			this.txtPhysicalGood.Name = "txtPhysicalGood";
			this.txtPhysicalGood.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtPhysicalGood.Text = null;
			this.txtPhysicalGood.Top = 5.760417F;
			this.txtPhysicalGood.Width = 0.4375F;
			// 
			// txtFunctGood
			// 
			this.txtFunctGood.Height = 0.1666667F;
			this.txtFunctGood.Left = 5.625F;
			this.txtFunctGood.Name = "txtFunctGood";
			this.txtFunctGood.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtFunctGood.Text = null;
			this.txtFunctGood.Top = 5.916667F;
			this.txtFunctGood.Width = 0.4375F;
			// 
			// Label78
			// 
			this.Label78.Height = 0.1666667F;
			this.Label78.HyperLink = null;
			this.Label78.Left = 4.5625F;
			this.Label78.MultiLine = false;
			this.Label78.Name = "Label78";
			this.Label78.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label78.Text = "Phys. % Good";
			this.Label78.Top = 5.760417F;
			this.Label78.Width = 1.0625F;
			// 
			// Label79
			// 
			this.Label79.Height = 0.1666667F;
			this.Label79.HyperLink = null;
			this.Label79.Left = 4.5625F;
			this.Label79.MultiLine = false;
			this.Label79.Name = "Label79";
			this.Label79.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label79.Text = "Funct. % Good";
			this.Label79.Top = 5.916667F;
			this.Label79.Width = 1.0625F;
			// 
			// Label80
			// 
			this.Label80.Height = 0.1666667F;
			this.Label80.HyperLink = null;
			this.Label80.Left = 4.5625F;
			this.Label80.MultiLine = false;
			this.Label80.Name = "Label80";
			this.Label80.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label80.Text = "Condition";
			this.Label80.Top = 5.59375F;
			this.Label80.Width = 1.0625F;
			// 
			// txtCondition
			// 
			this.txtCondition.Height = 0.1666667F;
			this.txtCondition.Left = 5.625F;
			this.txtCondition.Name = "txtCondition";
			this.txtCondition.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtCondition.Text = null;
			this.txtCondition.Top = 5.59375F;
			this.txtCondition.Width = 0.4375F;
			// 
			// txtFunctCode
			// 
			this.txtFunctCode.Height = 0.1666667F;
			this.txtFunctCode.Left = 5.625F;
			this.txtFunctCode.Name = "txtFunctCode";
			this.txtFunctCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtFunctCode.Text = null;
			this.txtFunctCode.Top = 6.083333F;
			this.txtFunctCode.Width = 0.4375F;
			// 
			// txtEconGood
			// 
			this.txtEconGood.Height = 0.1666667F;
			this.txtEconGood.Left = 5.625F;
			this.txtEconGood.Name = "txtEconGood";
			this.txtEconGood.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtEconGood.Text = null;
			this.txtEconGood.Top = 6.25F;
			this.txtEconGood.Width = 0.4375F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.1666667F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 4.5625F;
			this.Label81.MultiLine = false;
			this.Label81.Name = "Label81";
			this.Label81.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label81.Text = "Funct. Code";
			this.Label81.Top = 6.083333F;
			this.Label81.Width = 1.0625F;
			// 
			// Label82
			// 
			this.Label82.Height = 0.1666667F;
			this.Label82.HyperLink = null;
			this.Label82.Left = 4.5625F;
			this.Label82.MultiLine = false;
			this.Label82.Name = "Label82";
			this.Label82.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label82.Text = "Econ % Good";
			this.Label82.Top = 6.25F;
			this.Label82.Width = 1.0625F;
			// 
			// txtEntranceCode
			// 
			this.txtEntranceCode.Height = 0.1666667F;
			this.txtEntranceCode.Left = 5.625F;
			this.txtEntranceCode.Name = "txtEntranceCode";
			this.txtEntranceCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtEntranceCode.Text = null;
			this.txtEntranceCode.Top = 6.583333F;
			this.txtEntranceCode.Width = 0.4375F;
			// 
			// txtInfoCode
			// 
			this.txtInfoCode.Height = 0.1666667F;
			this.txtInfoCode.Left = 5.625F;
			this.txtInfoCode.Name = "txtInfoCode";
			this.txtInfoCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtInfoCode.Text = null;
			this.txtInfoCode.Top = 6.75F;
			this.txtInfoCode.Width = 0.4375F;
			// 
			// Label83
			// 
			this.Label83.Height = 0.1666667F;
			this.Label83.HyperLink = null;
			this.Label83.Left = 4.5625F;
			this.Label83.MultiLine = false;
			this.Label83.Name = "Label83";
			this.Label83.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label83.Text = "Entrance Code";
			this.Label83.Top = 6.583333F;
			this.Label83.Width = 1.0625F;
			// 
			// Label84
			// 
			this.Label84.Height = 0.1666667F;
			this.Label84.HyperLink = null;
			this.Label84.Left = 4.5625F;
			this.Label84.MultiLine = false;
			this.Label84.Name = "Label84";
			this.Label84.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label84.Text = "Info.Code";
			this.Label84.Top = 6.75F;
			this.Label84.Width = 1.0625F;
			// 
			// Label85
			// 
			this.Label85.Height = 0.1666667F;
			this.Label85.HyperLink = null;
			this.Label85.Left = 4.5625F;
			this.Label85.MultiLine = false;
			this.Label85.Name = "Label85";
			this.Label85.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label85.Text = "Econ. Code";
			this.Label85.Top = 6.416667F;
			this.Label85.Width = 1.0625F;
			// 
			// txtEconCode
			// 
			this.txtEconCode.Height = 0.1666667F;
			this.txtEconCode.Left = 5.625F;
			this.txtEconCode.Name = "txtEconCode";
			this.txtEconCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtEconCode.Text = null;
			this.txtEconCode.Top = 6.416667F;
			this.txtEconCode.Width = 0.4375F;
			// 
			// txtDateInspected
			// 
			this.txtDateInspected.Height = 0.1666667F;
			this.txtDateInspected.Left = 5.625F;
			this.txtDateInspected.Name = "txtDateInspected";
			this.txtDateInspected.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDateInspected.Text = null;
			this.txtDateInspected.Top = 6.916667F;
			this.txtDateInspected.Width = 1.1875F;
			// 
			// Label86
			// 
			this.Label86.Height = 0.1666667F;
			this.Label86.HyperLink = null;
			this.Label86.Left = 4.5625F;
			this.Label86.MultiLine = false;
			this.Label86.Name = "Label86";
			this.Label86.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label86.Text = "Date Inspected";
			this.Label86.Top = 6.916667F;
			this.Label86.Width = 1.0625F;
			// 
			// Field67
			// 
			this.Field67.Height = 0.1875F;
			this.Field67.Left = 0.125F;
			this.Field67.Name = "Field67";
			this.Field67.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
			this.Field67.Text = "O U T B U I L D I N G   D A T A";
			this.Field67.Top = 7.166667F;
			this.Field67.Width = 7.25F;
			// 
			// txtType4
			// 
			this.txtType4.Height = 0.1666667F;
			this.txtType4.Left = 0.3125F;
			this.txtType4.Name = "txtType4";
			this.txtType4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtType4.Text = null;
			this.txtType4.Top = 8.052083F;
			this.txtType4.Width = 0.4375F;
			// 
			// txtType5
			// 
			this.txtType5.Height = 0.1666667F;
			this.txtType5.Left = 0.3125F;
			this.txtType5.Name = "txtType5";
			this.txtType5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtType5.Text = null;
			this.txtType5.Top = 8.21875F;
			this.txtType5.Width = 0.4375F;
			// 
			// txtType6
			// 
			this.txtType6.Height = 0.1666667F;
			this.txtType6.Left = 0.3125F;
			this.txtType6.Name = "txtType6";
			this.txtType6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtType6.Text = null;
			this.txtType6.Top = 8.375F;
			this.txtType6.Width = 0.4375F;
			// 
			// txtType7
			// 
			this.txtType7.Height = 0.1666667F;
			this.txtType7.Left = 0.3125F;
			this.txtType7.Name = "txtType7";
			this.txtType7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtType7.Text = null;
			this.txtType7.Top = 8.541667F;
			this.txtType7.Width = 0.4375F;
			// 
			// txtType8
			// 
			this.txtType8.Height = 0.1666667F;
			this.txtType8.Left = 0.3125F;
			this.txtType8.Name = "txtType8";
			this.txtType8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtType8.Text = null;
			this.txtType8.Top = 8.708333F;
			this.txtType8.Width = 0.4375F;
			// 
			// txtType9
			// 
			this.txtType9.Height = 0.1666667F;
			this.txtType9.Left = 0.3125F;
			this.txtType9.Name = "txtType9";
			this.txtType9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtType9.Text = null;
			this.txtType9.Top = 8.875F;
			this.txtType9.Width = 0.4375F;
			// 
			// txtType10
			// 
			this.txtType10.Height = 0.1666667F;
			this.txtType10.Left = 0.3125F;
			this.txtType10.Name = "txtType10";
			this.txtType10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtType10.Text = null;
			this.txtType10.Top = 9.041667F;
			this.txtType10.Width = 0.4375F;
			// 
			// txtYear1
			// 
			this.txtYear1.Height = 0.1666667F;
			this.txtYear1.Left = 1F;
			this.txtYear1.Name = "txtYear1";
			this.txtYear1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtYear1.Text = null;
			this.txtYear1.Top = 7.541667F;
			this.txtYear1.Width = 0.4375F;
			// 
			// Label87
			// 
			this.Label87.Height = 0.1875F;
			this.Label87.HyperLink = null;
			this.Label87.Left = 1F;
			this.Label87.MultiLine = false;
			this.Label87.Name = "Label87";
			this.Label87.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Label87.Text = "Year";
			this.Label87.Top = 7.354167F;
			this.Label87.Width = 0.4375F;
			// 
			// txtYear2
			// 
			this.txtYear2.Height = 0.1666667F;
			this.txtYear2.Left = 1F;
			this.txtYear2.Name = "txtYear2";
			this.txtYear2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtYear2.Text = null;
			this.txtYear2.Top = 7.708333F;
			this.txtYear2.Width = 0.4375F;
			// 
			// txtYear3
			// 
			this.txtYear3.Height = 0.1666667F;
			this.txtYear3.Left = 1F;
			this.txtYear3.Name = "txtYear3";
			this.txtYear3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtYear3.Text = null;
			this.txtYear3.Top = 7.885417F;
			this.txtYear3.Width = 0.4375F;
			// 
			// txtYear4
			// 
			this.txtYear4.Height = 0.1666667F;
			this.txtYear4.Left = 1F;
			this.txtYear4.Name = "txtYear4";
			this.txtYear4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtYear4.Text = null;
			this.txtYear4.Top = 8.052083F;
			this.txtYear4.Width = 0.4375F;
			// 
			// txtYear5
			// 
			this.txtYear5.Height = 0.1666667F;
			this.txtYear5.Left = 1F;
			this.txtYear5.Name = "txtYear5";
			this.txtYear5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtYear5.Text = null;
			this.txtYear5.Top = 8.21875F;
			this.txtYear5.Width = 0.4375F;
			// 
			// txtYear6
			// 
			this.txtYear6.Height = 0.1666667F;
			this.txtYear6.Left = 1F;
			this.txtYear6.Name = "txtYear6";
			this.txtYear6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtYear6.Text = null;
			this.txtYear6.Top = 8.375F;
			this.txtYear6.Width = 0.4375F;
			// 
			// txtYear7
			// 
			this.txtYear7.Height = 0.1666667F;
			this.txtYear7.Left = 1F;
			this.txtYear7.Name = "txtYear7";
			this.txtYear7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtYear7.Text = null;
			this.txtYear7.Top = 8.541667F;
			this.txtYear7.Width = 0.4375F;
			// 
			// txtYear8
			// 
			this.txtYear8.Height = 0.1666667F;
			this.txtYear8.Left = 1F;
			this.txtYear8.Name = "txtYear8";
			this.txtYear8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtYear8.Text = null;
			this.txtYear8.Top = 8.708333F;
			this.txtYear8.Width = 0.4375F;
			// 
			// txtYear9
			// 
			this.txtYear9.Height = 0.1666667F;
			this.txtYear9.Left = 1F;
			this.txtYear9.Name = "txtYear9";
			this.txtYear9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtYear9.Text = null;
			this.txtYear9.Top = 8.875F;
			this.txtYear9.Width = 0.4375F;
			// 
			// txtYear10
			// 
			this.txtYear10.Height = 0.1666667F;
			this.txtYear10.Left = 1F;
			this.txtYear10.Name = "txtYear10";
			this.txtYear10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtYear10.Text = null;
			this.txtYear10.Top = 9.041667F;
			this.txtYear10.Width = 0.4375F;
			// 
			// txtUnits1
			// 
			this.txtUnits1.Height = 0.1666667F;
			this.txtUnits1.Left = 1.6875F;
			this.txtUnits1.Name = "txtUnits1";
			this.txtUnits1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtUnits1.Text = null;
			this.txtUnits1.Top = 7.541667F;
			this.txtUnits1.Width = 0.4375F;
			// 
			// Label88
			// 
			this.Label88.Height = 0.1875F;
			this.Label88.HyperLink = null;
			this.Label88.Left = 1.6875F;
			this.Label88.MultiLine = false;
			this.Label88.Name = "Label88";
			this.Label88.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Label88.Text = "Units";
			this.Label88.Top = 7.354167F;
			this.Label88.Width = 0.4375F;
			// 
			// txtUnits2
			// 
			this.txtUnits2.Height = 0.1666667F;
			this.txtUnits2.Left = 1.6875F;
			this.txtUnits2.Name = "txtUnits2";
			this.txtUnits2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtUnits2.Text = null;
			this.txtUnits2.Top = 7.708333F;
			this.txtUnits2.Width = 0.4375F;
			// 
			// txtUnits3
			// 
			this.txtUnits3.Height = 0.1666667F;
			this.txtUnits3.Left = 1.6875F;
			this.txtUnits3.Name = "txtUnits3";
			this.txtUnits3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtUnits3.Text = null;
			this.txtUnits3.Top = 7.885417F;
			this.txtUnits3.Width = 0.4375F;
			// 
			// txtUnits4
			// 
			this.txtUnits4.Height = 0.1666667F;
			this.txtUnits4.Left = 1.6875F;
			this.txtUnits4.Name = "txtUnits4";
			this.txtUnits4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtUnits4.Text = null;
			this.txtUnits4.Top = 8.052083F;
			this.txtUnits4.Width = 0.4375F;
			// 
			// txtUnits5
			// 
			this.txtUnits5.Height = 0.1666667F;
			this.txtUnits5.Left = 1.6875F;
			this.txtUnits5.Name = "txtUnits5";
			this.txtUnits5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtUnits5.Text = null;
			this.txtUnits5.Top = 8.21875F;
			this.txtUnits5.Width = 0.4375F;
			// 
			// txtUnits6
			// 
			this.txtUnits6.Height = 0.1666667F;
			this.txtUnits6.Left = 1.6875F;
			this.txtUnits6.Name = "txtUnits6";
			this.txtUnits6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtUnits6.Text = null;
			this.txtUnits6.Top = 8.375F;
			this.txtUnits6.Width = 0.4375F;
			// 
			// txtUnits7
			// 
			this.txtUnits7.Height = 0.1666667F;
			this.txtUnits7.Left = 1.6875F;
			this.txtUnits7.Name = "txtUnits7";
			this.txtUnits7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtUnits7.Text = null;
			this.txtUnits7.Top = 8.541667F;
			this.txtUnits7.Width = 0.4375F;
			// 
			// txtUnits8
			// 
			this.txtUnits8.Height = 0.1666667F;
			this.txtUnits8.Left = 1.6875F;
			this.txtUnits8.Name = "txtUnits8";
			this.txtUnits8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtUnits8.Text = null;
			this.txtUnits8.Top = 8.708333F;
			this.txtUnits8.Width = 0.4375F;
			// 
			// txtUnits9
			// 
			this.txtUnits9.Height = 0.1666667F;
			this.txtUnits9.Left = 1.6875F;
			this.txtUnits9.Name = "txtUnits9";
			this.txtUnits9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtUnits9.Text = null;
			this.txtUnits9.Top = 8.875F;
			this.txtUnits9.Width = 0.4375F;
			// 
			// txtUnits10
			// 
			this.txtUnits10.Height = 0.1666667F;
			this.txtUnits10.Left = 1.6875F;
			this.txtUnits10.Name = "txtUnits10";
			this.txtUnits10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtUnits10.Text = null;
			this.txtUnits10.Top = 9.041667F;
			this.txtUnits10.Width = 0.4375F;
			// 
			// txtGrade1
			// 
			this.txtGrade1.Height = 0.1666667F;
			this.txtGrade1.Left = 3.0625F;
			this.txtGrade1.Name = "txtGrade1";
			this.txtGrade1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtGrade1.Text = null;
			this.txtGrade1.Top = 7.541667F;
			this.txtGrade1.Width = 0.4375F;
			// 
			// Label89
			// 
			this.Label89.Height = 0.1875F;
			this.Label89.HyperLink = null;
			this.Label89.Left = 2.375F;
			this.Label89.MultiLine = false;
			this.Label89.Name = "Label89";
			this.Label89.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Label89.Text = "Grade";
			this.Label89.Top = 7.354167F;
			this.Label89.Width = 0.4375F;
			// 
			// txtGrade2
			// 
			this.txtGrade2.Height = 0.1666667F;
			this.txtGrade2.Left = 3.0625F;
			this.txtGrade2.Name = "txtGrade2";
			this.txtGrade2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtGrade2.Text = null;
			this.txtGrade2.Top = 7.708333F;
			this.txtGrade2.Width = 0.4375F;
			// 
			// txtGrade3
			// 
			this.txtGrade3.Height = 0.1666667F;
			this.txtGrade3.Left = 3.0625F;
			this.txtGrade3.Name = "txtGrade3";
			this.txtGrade3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtGrade3.Text = null;
			this.txtGrade3.Top = 7.885417F;
			this.txtGrade3.Width = 0.4375F;
			// 
			// txtGrade4
			// 
			this.txtGrade4.Height = 0.1666667F;
			this.txtGrade4.Left = 3.0625F;
			this.txtGrade4.Name = "txtGrade4";
			this.txtGrade4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtGrade4.Text = null;
			this.txtGrade4.Top = 8.052083F;
			this.txtGrade4.Width = 0.4375F;
			// 
			// txtGrade5
			// 
			this.txtGrade5.Height = 0.1666667F;
			this.txtGrade5.Left = 3.0625F;
			this.txtGrade5.Name = "txtGrade5";
			this.txtGrade5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtGrade5.Text = null;
			this.txtGrade5.Top = 8.21875F;
			this.txtGrade5.Width = 0.4375F;
			// 
			// txtGrade6
			// 
			this.txtGrade6.Height = 0.1666667F;
			this.txtGrade6.Left = 3.0625F;
			this.txtGrade6.Name = "txtGrade6";
			this.txtGrade6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtGrade6.Text = null;
			this.txtGrade6.Top = 8.375F;
			this.txtGrade6.Width = 0.4375F;
			// 
			// txtGrade7
			// 
			this.txtGrade7.Height = 0.1666667F;
			this.txtGrade7.Left = 3.0625F;
			this.txtGrade7.Name = "txtGrade7";
			this.txtGrade7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtGrade7.Text = null;
			this.txtGrade7.Top = 8.541667F;
			this.txtGrade7.Width = 0.4375F;
			// 
			// txtGrade8
			// 
			this.txtGrade8.Height = 0.1666667F;
			this.txtGrade8.Left = 3.0625F;
			this.txtGrade8.Name = "txtGrade8";
			this.txtGrade8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtGrade8.Text = null;
			this.txtGrade8.Top = 8.708333F;
			this.txtGrade8.Width = 0.4375F;
			// 
			// txtGrade9
			// 
			this.txtGrade9.Height = 0.1666667F;
			this.txtGrade9.Left = 3.0625F;
			this.txtGrade9.Name = "txtGrade9";
			this.txtGrade9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtGrade9.Text = null;
			this.txtGrade9.Top = 8.875F;
			this.txtGrade9.Width = 0.4375F;
			// 
			// txtGrade10
			// 
			this.txtGrade10.Height = 0.1666667F;
			this.txtGrade10.Left = 3.0625F;
			this.txtGrade10.Name = "txtGrade10";
			this.txtGrade10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtGrade10.Text = null;
			this.txtGrade10.Top = 9.041667F;
			this.txtGrade10.Width = 0.4375F;
			// 
			// txtCondition1
			// 
			this.txtCondition1.Height = 0.1666667F;
			this.txtCondition1.Left = 3.75F;
			this.txtCondition1.Name = "txtCondition1";
			this.txtCondition1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtCondition1.Text = null;
			this.txtCondition1.Top = 7.541667F;
			this.txtCondition1.Width = 0.4375F;
			// 
			// Label90
			// 
			this.Label90.Height = 0.1875F;
			this.Label90.HyperLink = null;
			this.Label90.Left = 3.75F;
			this.Label90.MultiLine = false;
			this.Label90.Name = "Label90";
			this.Label90.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label90.Text = "Condition";
			this.Label90.Top = 7.354167F;
			this.Label90.Width = 0.5625F;
			// 
			// txtCondition2
			// 
			this.txtCondition2.Height = 0.1666667F;
			this.txtCondition2.Left = 3.75F;
			this.txtCondition2.Name = "txtCondition2";
			this.txtCondition2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtCondition2.Text = null;
			this.txtCondition2.Top = 7.708333F;
			this.txtCondition2.Width = 0.4375F;
			// 
			// txtCondition6
			// 
			this.txtCondition6.Height = 0.1666667F;
			this.txtCondition6.Left = 3.75F;
			this.txtCondition6.Name = "txtCondition6";
			this.txtCondition6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtCondition6.Text = null;
			this.txtCondition6.Top = 8.375F;
			this.txtCondition6.Width = 0.4375F;
			// 
			// txtCondition3
			// 
			this.txtCondition3.Height = 0.1666667F;
			this.txtCondition3.Left = 3.75F;
			this.txtCondition3.Name = "txtCondition3";
			this.txtCondition3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtCondition3.Text = null;
			this.txtCondition3.Top = 7.885417F;
			this.txtCondition3.Width = 0.4375F;
			// 
			// txtCondition4
			// 
			this.txtCondition4.Height = 0.1666667F;
			this.txtCondition4.Left = 3.75F;
			this.txtCondition4.Name = "txtCondition4";
			this.txtCondition4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtCondition4.Text = null;
			this.txtCondition4.Top = 8.052083F;
			this.txtCondition4.Width = 0.4375F;
			// 
			// txtCondition5
			// 
			this.txtCondition5.Height = 0.1666667F;
			this.txtCondition5.Left = 3.75F;
			this.txtCondition5.Name = "txtCondition5";
			this.txtCondition5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtCondition5.Text = null;
			this.txtCondition5.Top = 8.21875F;
			this.txtCondition5.Width = 0.4375F;
			// 
			// txtCondition7
			// 
			this.txtCondition7.Height = 0.1666667F;
			this.txtCondition7.Left = 3.75F;
			this.txtCondition7.Name = "txtCondition7";
			this.txtCondition7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtCondition7.Text = null;
			this.txtCondition7.Top = 8.541667F;
			this.txtCondition7.Width = 0.4375F;
			// 
			// txtCondition8
			// 
			this.txtCondition8.Height = 0.1666667F;
			this.txtCondition8.Left = 3.75F;
			this.txtCondition8.Name = "txtCondition8";
			this.txtCondition8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtCondition8.Text = null;
			this.txtCondition8.Top = 8.708333F;
			this.txtCondition8.Width = 0.4375F;
			// 
			// txtCondition9
			// 
			this.txtCondition9.Height = 0.1666667F;
			this.txtCondition9.Left = 3.75F;
			this.txtCondition9.Name = "txtCondition9";
			this.txtCondition9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtCondition9.Text = null;
			this.txtCondition9.Top = 8.875F;
			this.txtCondition9.Width = 0.4375F;
			// 
			// txtCondition10
			// 
			this.txtCondition10.Height = 0.1666667F;
			this.txtCondition10.Left = 3.75F;
			this.txtCondition10.Name = "txtCondition10";
			this.txtCondition10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtCondition10.Text = null;
			this.txtCondition10.Top = 9.041667F;
			this.txtCondition10.Width = 0.4375F;
			// 
			// Label91
			// 
			this.Label91.Height = 0.1875F;
			this.Label91.HyperLink = null;
			this.Label91.Left = 4.375F;
			this.Label91.MultiLine = false;
			this.Label91.Name = "Label91";
			this.Label91.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label91.Text = "Physical %";
			this.Label91.Top = 7.354167F;
			this.Label91.Width = 0.625F;
			// 
			// Label92
			// 
			this.Label92.Height = 0.1875F;
			this.Label92.HyperLink = null;
			this.Label92.Left = 5.0625F;
			this.Label92.MultiLine = false;
			this.Label92.Name = "Label92";
			this.Label92.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label92.Text = "Functional %";
			this.Label92.Top = 7.354167F;
			this.Label92.Width = 0.75F;
			// 
			// txtPhysical1
			// 
			this.txtPhysical1.Height = 0.1666667F;
			this.txtPhysical1.Left = 4.4375F;
			this.txtPhysical1.Name = "txtPhysical1";
			this.txtPhysical1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtPhysical1.Text = null;
			this.txtPhysical1.Top = 7.541667F;
			this.txtPhysical1.Width = 0.4375F;
			// 
			// txtPhysical2
			// 
			this.txtPhysical2.Height = 0.1666667F;
			this.txtPhysical2.Left = 4.4375F;
			this.txtPhysical2.Name = "txtPhysical2";
			this.txtPhysical2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtPhysical2.Text = null;
			this.txtPhysical2.Top = 7.708333F;
			this.txtPhysical2.Width = 0.4375F;
			// 
			// txtPhysical3
			// 
			this.txtPhysical3.Height = 0.1666667F;
			this.txtPhysical3.Left = 4.4375F;
			this.txtPhysical3.Name = "txtPhysical3";
			this.txtPhysical3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtPhysical3.Text = null;
			this.txtPhysical3.Top = 7.885417F;
			this.txtPhysical3.Width = 0.4375F;
			// 
			// txtPhysical4
			// 
			this.txtPhysical4.Height = 0.1666667F;
			this.txtPhysical4.Left = 4.4375F;
			this.txtPhysical4.Name = "txtPhysical4";
			this.txtPhysical4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtPhysical4.Text = null;
			this.txtPhysical4.Top = 8.052083F;
			this.txtPhysical4.Width = 0.4375F;
			// 
			// txtPhysical5
			// 
			this.txtPhysical5.Height = 0.1666667F;
			this.txtPhysical5.Left = 4.4375F;
			this.txtPhysical5.Name = "txtPhysical5";
			this.txtPhysical5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtPhysical5.Text = null;
			this.txtPhysical5.Top = 8.21875F;
			this.txtPhysical5.Width = 0.4375F;
			// 
			// txtPhysical6
			// 
			this.txtPhysical6.Height = 0.1666667F;
			this.txtPhysical6.Left = 4.4375F;
			this.txtPhysical6.Name = "txtPhysical6";
			this.txtPhysical6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtPhysical6.Text = null;
			this.txtPhysical6.Top = 8.375F;
			this.txtPhysical6.Width = 0.4375F;
			// 
			// txtPhysical7
			// 
			this.txtPhysical7.Height = 0.1666667F;
			this.txtPhysical7.Left = 4.4375F;
			this.txtPhysical7.Name = "txtPhysical7";
			this.txtPhysical7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtPhysical7.Text = null;
			this.txtPhysical7.Top = 8.541667F;
			this.txtPhysical7.Width = 0.4375F;
			// 
			// txtPhysical8
			// 
			this.txtPhysical8.Height = 0.1666667F;
			this.txtPhysical8.Left = 4.4375F;
			this.txtPhysical8.Name = "txtPhysical8";
			this.txtPhysical8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtPhysical8.Text = null;
			this.txtPhysical8.Top = 8.708333F;
			this.txtPhysical8.Width = 0.4375F;
			// 
			// txtPhysical9
			// 
			this.txtPhysical9.Height = 0.1666667F;
			this.txtPhysical9.Left = 4.4375F;
			this.txtPhysical9.Name = "txtPhysical9";
			this.txtPhysical9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtPhysical9.Text = null;
			this.txtPhysical9.Top = 8.875F;
			this.txtPhysical9.Width = 0.4375F;
			// 
			// txtPhysical10
			// 
			this.txtPhysical10.Height = 0.1666667F;
			this.txtPhysical10.Left = 4.4375F;
			this.txtPhysical10.Name = "txtPhysical10";
			this.txtPhysical10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtPhysical10.Text = null;
			this.txtPhysical10.Top = 9.041667F;
			this.txtPhysical10.Width = 0.4375F;
			// 
			// txtFunctional1
			// 
			this.txtFunctional1.Height = 0.1666667F;
			this.txtFunctional1.Left = 5.125F;
			this.txtFunctional1.Name = "txtFunctional1";
			this.txtFunctional1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtFunctional1.Text = null;
			this.txtFunctional1.Top = 7.541667F;
			this.txtFunctional1.Width = 0.4375F;
			// 
			// txtFunctional2
			// 
			this.txtFunctional2.Height = 0.1666667F;
			this.txtFunctional2.Left = 5.125F;
			this.txtFunctional2.Name = "txtFunctional2";
			this.txtFunctional2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtFunctional2.Text = null;
			this.txtFunctional2.Top = 7.708333F;
			this.txtFunctional2.Width = 0.4375F;
			// 
			// txtFunctional3
			// 
			this.txtFunctional3.Height = 0.1666667F;
			this.txtFunctional3.Left = 5.125F;
			this.txtFunctional3.Name = "txtFunctional3";
			this.txtFunctional3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtFunctional3.Text = null;
			this.txtFunctional3.Top = 7.885417F;
			this.txtFunctional3.Width = 0.4375F;
			// 
			// txtFunctional4
			// 
			this.txtFunctional4.Height = 0.1666667F;
			this.txtFunctional4.Left = 5.125F;
			this.txtFunctional4.Name = "txtFunctional4";
			this.txtFunctional4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtFunctional4.Text = null;
			this.txtFunctional4.Top = 8.052083F;
			this.txtFunctional4.Width = 0.4375F;
			// 
			// txtFunctional5
			// 
			this.txtFunctional5.Height = 0.1666667F;
			this.txtFunctional5.Left = 5.125F;
			this.txtFunctional5.Name = "txtFunctional5";
			this.txtFunctional5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtFunctional5.Text = null;
			this.txtFunctional5.Top = 8.21875F;
			this.txtFunctional5.Width = 0.4375F;
			// 
			// txtFunctional6
			// 
			this.txtFunctional6.Height = 0.1666667F;
			this.txtFunctional6.Left = 5.125F;
			this.txtFunctional6.Name = "txtFunctional6";
			this.txtFunctional6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtFunctional6.Text = null;
			this.txtFunctional6.Top = 8.375F;
			this.txtFunctional6.Width = 0.4375F;
			// 
			// txtFunctional7
			// 
			this.txtFunctional7.Height = 0.1666667F;
			this.txtFunctional7.Left = 5.125F;
			this.txtFunctional7.Name = "txtFunctional7";
			this.txtFunctional7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtFunctional7.Text = null;
			this.txtFunctional7.Top = 8.541667F;
			this.txtFunctional7.Width = 0.4375F;
			// 
			// txtFunctional9
			// 
			this.txtFunctional9.Height = 0.1666667F;
			this.txtFunctional9.Left = 5.125F;
			this.txtFunctional9.Name = "txtFunctional9";
			this.txtFunctional9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtFunctional9.Text = null;
			this.txtFunctional9.Top = 8.875F;
			this.txtFunctional9.Width = 0.4375F;
			// 
			// txtFunctional10
			// 
			this.txtFunctional10.Height = 0.1666667F;
			this.txtFunctional10.Left = 5.125F;
			this.txtFunctional10.Name = "txtFunctional10";
			this.txtFunctional10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtFunctional10.Text = null;
			this.txtFunctional10.Top = 9.041667F;
			this.txtFunctional10.Width = 0.4375F;
			// 
			// txtFunctional8
			// 
			this.txtFunctional8.Height = 0.1666667F;
			this.txtFunctional8.Left = 5.125F;
			this.txtFunctional8.Name = "txtFunctional8";
			this.txtFunctional8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtFunctional8.Text = null;
			this.txtFunctional8.Top = 8.708333F;
			this.txtFunctional8.Width = 0.4375F;
			// 
			// Label93
			// 
			this.Label93.Height = 0.1666667F;
			this.Label93.HyperLink = null;
			this.Label93.Left = 0.125F;
			this.Label93.MultiLine = false;
			this.Label93.Name = "Label93";
			this.Label93.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label93.Text = "Occupancy Code";
			this.Label93.Top = 4.427083F;
			this.Label93.Width = 1.0625F;
			// 
			// txtOCC1
			// 
			this.txtOCC1.Height = 0.1666667F;
			this.txtOCC1.Left = 1.1875F;
			this.txtOCC1.Name = "txtOCC1";
			this.txtOCC1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtOCC1.Text = null;
			this.txtOCC1.Top = 4.427083F;
			this.txtOCC1.Width = 0.4375F;
			// 
			// txtstories1
			// 
			this.txtstories1.Height = 0.1666667F;
			this.txtstories1.Left = 1.1875F;
			this.txtstories1.Name = "txtstories1";
			this.txtstories1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtstories1.Text = null;
			this.txtstories1.Top = 5.260417F;
			this.txtstories1.Width = 0.25F;
			// 
			// txtbasefloor1
			// 
			this.txtbasefloor1.Height = 0.1666667F;
			this.txtbasefloor1.Left = 1.1875F;
			this.txtbasefloor1.Name = "txtbasefloor1";
			this.txtbasefloor1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtbasefloor1.Text = null;
			this.txtbasefloor1.Top = 5.427083F;
			this.txtbasefloor1.Width = 0.625F;
			// 
			// Label94
			// 
			this.Label94.Height = 0.1666667F;
			this.Label94.HyperLink = null;
			this.Label94.Left = 0.125F;
			this.Label94.MultiLine = false;
			this.Label94.Name = "Label94";
			this.Label94.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label94.Text = "Stories/Height";
			this.Label94.Top = 5.260417F;
			this.Label94.Width = 1.0625F;
			// 
			// Label95
			// 
			this.Label95.Height = 0.1666667F;
			this.Label95.HyperLink = null;
			this.Label95.Left = 0.125F;
			this.Label95.MultiLine = false;
			this.Label95.Name = "Label95";
			this.Label95.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label95.Text = "Base Floor Area";
			this.Label95.Top = 5.427083F;
			this.Label95.Width = 1.0625F;
			// 
			// Label96
			// 
			this.Label96.Height = 0.1666667F;
			this.Label96.HyperLink = null;
			this.Label96.Left = 0.125F;
			this.Label96.MultiLine = false;
			this.Label96.Name = "Label96";
			this.Label96.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label96.Text = "Class & Quality";
			this.Label96.Top = 4.760417F;
			this.Label96.Width = 1.0625F;
			// 
			// txtclass1
			// 
			this.txtclass1.Height = 0.1666667F;
			this.txtclass1.Left = 1.1875F;
			this.txtclass1.Name = "txtclass1";
			this.txtclass1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtclass1.Text = null;
			this.txtclass1.Top = 4.760417F;
			this.txtclass1.Width = 0.25F;
			// 
			// Label97
			// 
			this.Label97.Height = 0.1666667F;
			this.Label97.HyperLink = null;
			this.Label97.Left = 0.125F;
			this.Label97.MultiLine = false;
			this.Label97.Name = "Label97";
			this.Label97.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label97.Text = "grade factor";
			this.Label97.Top = 4.927083F;
			this.Label97.Width = 1.0625F;
			// 
			// txtgradefactor1
			// 
			this.txtgradefactor1.Height = 0.1666667F;
			this.txtgradefactor1.Left = 1.1875F;
			this.txtgradefactor1.Name = "txtgradefactor1";
			this.txtgradefactor1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtgradefactor1.Text = null;
			this.txtgradefactor1.Top = 4.927083F;
			this.txtgradefactor1.Width = 0.4375F;
			// 
			// txtExteriorWalls1
			// 
			this.txtExteriorWalls1.Height = 0.1666667F;
			this.txtExteriorWalls1.Left = 1.1875F;
			this.txtExteriorWalls1.Name = "txtExteriorWalls1";
			this.txtExteriorWalls1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtExteriorWalls1.Text = null;
			this.txtExteriorWalls1.Top = 5.09375F;
			this.txtExteriorWalls1.Width = 0.4375F;
			// 
			// txtheatcool1
			// 
			this.txtheatcool1.Height = 0.1666667F;
			this.txtheatcool1.Left = 1.1875F;
			this.txtheatcool1.Name = "txtheatcool1";
			this.txtheatcool1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtheatcool1.Text = null;
			this.txtheatcool1.Top = 5.760417F;
			this.txtheatcool1.Width = 0.4375F;
			// 
			// txtYearBuilt1
			// 
			this.txtYearBuilt1.Height = 0.1666667F;
			this.txtYearBuilt1.Left = 1.1875F;
			this.txtYearBuilt1.Name = "txtYearBuilt1";
			this.txtYearBuilt1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtYearBuilt1.Text = null;
			this.txtYearBuilt1.Top = 5.916667F;
			this.txtYearBuilt1.Width = 0.4375F;
			// 
			// Label98
			// 
			this.Label98.Height = 0.1666667F;
			this.Label98.HyperLink = null;
			this.Label98.Left = 0.125F;
			this.Label98.MultiLine = false;
			this.Label98.Name = "Label98";
			this.Label98.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label98.Text = "Heating/Cooling";
			this.Label98.Top = 5.760417F;
			this.Label98.Width = 1.0625F;
			// 
			// Label99
			// 
			this.Label99.Height = 0.1666667F;
			this.Label99.HyperLink = null;
			this.Label99.Left = 0.125F;
			this.Label99.MultiLine = false;
			this.Label99.Name = "Label99";
			this.Label99.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label99.Text = "Perimeter/Units";
			this.Label99.Top = 5.59375F;
			this.Label99.Width = 1.0625F;
			// 
			// txtperimeter1
			// 
			this.txtperimeter1.Height = 0.1666667F;
			this.txtperimeter1.Left = 1.1875F;
			this.txtperimeter1.Name = "txtperimeter1";
			this.txtperimeter1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtperimeter1.Text = null;
			this.txtperimeter1.Top = 5.59375F;
			this.txtperimeter1.Width = 0.625F;
			// 
			// txtYearRemodelled1
			// 
			this.txtYearRemodelled1.Height = 0.1666667F;
			this.txtYearRemodelled1.Left = 1.1875F;
			this.txtYearRemodelled1.Name = "txtYearRemodelled1";
			this.txtYearRemodelled1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtYearRemodelled1.Text = null;
			this.txtYearRemodelled1.Top = 6.083333F;
			this.txtYearRemodelled1.Width = 0.4375F;
			// 
			// txtcond1
			// 
			this.txtcond1.Height = 0.1666667F;
			this.txtcond1.Left = 1.1875F;
			this.txtcond1.Name = "txtcond1";
			this.txtcond1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtcond1.Text = null;
			this.txtcond1.Top = 6.25F;
			this.txtcond1.Width = 0.4375F;
			// 
			// Label100
			// 
			this.Label100.Height = 0.1666667F;
			this.Label100.HyperLink = null;
			this.Label100.Left = 0.125F;
			this.Label100.MultiLine = false;
			this.Label100.Name = "Label100";
			this.Label100.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label100.Text = "Condition";
			this.Label100.Top = 6.25F;
			this.Label100.Width = 1.0625F;
			// 
			// txtfunc1
			// 
			this.txtfunc1.Height = 0.1666667F;
			this.txtfunc1.Left = 1.1875F;
			this.txtfunc1.Name = "txtfunc1";
			this.txtfunc1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtfunc1.Text = null;
			this.txtfunc1.Top = 6.583333F;
			this.txtfunc1.Width = 0.4375F;
			// 
			// txteconomic1
			// 
			this.txteconomic1.Height = 0.1666667F;
			this.txteconomic1.Left = 1.1875F;
			this.txteconomic1.Name = "txteconomic1";
			this.txteconomic1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txteconomic1.Text = null;
			this.txteconomic1.Top = 6.75F;
			this.txteconomic1.Width = 0.4375F;
			// 
			// Label101
			// 
			this.Label101.Height = 0.1666667F;
			this.Label101.HyperLink = null;
			this.Label101.Left = 0.125F;
			this.Label101.MultiLine = false;
			this.Label101.Name = "Label101";
			this.Label101.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label101.Text = "Functional";
			this.Label101.Top = 6.583333F;
			this.Label101.Width = 1.0625F;
			// 
			// Label102
			// 
			this.Label102.Height = 0.1666667F;
			this.Label102.HyperLink = null;
			this.Label102.Left = 0.125F;
			this.Label102.MultiLine = false;
			this.Label102.Name = "Label102";
			this.Label102.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label102.Text = "Economic";
			this.Label102.Top = 6.75F;
			this.Label102.Width = 1.0625F;
			// 
			// Label103
			// 
			this.Label103.Height = 0.1666667F;
			this.Label103.HyperLink = null;
			this.Label103.Left = 0.125F;
			this.Label103.MultiLine = false;
			this.Label103.Name = "Label103";
			this.Label103.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label103.Text = "% Good Physical";
			this.Label103.Top = 6.416667F;
			this.Label103.Width = 1.0625F;
			// 
			// txtphys1
			// 
			this.txtphys1.Height = 0.1666667F;
			this.txtphys1.Left = 1.1875F;
			this.txtphys1.Name = "txtphys1";
			this.txtphys1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtphys1.Text = null;
			this.txtphys1.Top = 6.416667F;
			this.txtphys1.Width = 0.4375F;
			// 
			// Label104
			// 
			this.Label104.Height = 0.1666667F;
			this.Label104.HyperLink = null;
			this.Label104.Left = 2.1875F;
			this.Label104.MultiLine = false;
			this.Label104.Name = "Label104";
			this.Label104.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label104.Text = "Occupancy Code";
			this.Label104.Top = 4.427083F;
			this.Label104.Width = 1.0625F;
			// 
			// Label105
			// 
			this.Label105.Height = 0.1666667F;
			this.Label105.HyperLink = null;
			this.Label105.Left = 2.1875F;
			this.Label105.MultiLine = false;
			this.Label105.Name = "Label105";
			this.Label105.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label105.Text = "Dwelling Units";
			this.Label105.Top = 4.59375F;
			this.Label105.Width = 1.0625F;
			// 
			// txtOCC2
			// 
			this.txtOCC2.Height = 0.1666667F;
			this.txtOCC2.Left = 3.25F;
			this.txtOCC2.Name = "txtOCC2";
			this.txtOCC2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtOCC2.Text = null;
			this.txtOCC2.Top = 4.427083F;
			this.txtOCC2.Width = 0.6875F;
			// 
			// txtstories2
			// 
			this.txtstories2.Height = 0.1666667F;
			this.txtstories2.Left = 3.25F;
			this.txtstories2.Name = "txtstories2";
			this.txtstories2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtstories2.Text = null;
			this.txtstories2.Top = 5.260417F;
			this.txtstories2.Width = 0.25F;
			// 
			// txtDwellingUnits2
			// 
			this.txtDwellingUnits2.Height = 0.1666667F;
			this.txtDwellingUnits2.Left = 3.25F;
			this.txtDwellingUnits2.Name = "txtDwellingUnits2";
			this.txtDwellingUnits2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDwellingUnits2.Text = null;
			this.txtDwellingUnits2.Top = 4.59375F;
			this.txtDwellingUnits2.Width = 0.4375F;
			// 
			// txtBaseFloor2
			// 
			this.txtBaseFloor2.Height = 0.1666667F;
			this.txtBaseFloor2.Left = 3.25F;
			this.txtBaseFloor2.Name = "txtBaseFloor2";
			this.txtBaseFloor2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtBaseFloor2.Text = null;
			this.txtBaseFloor2.Top = 5.427083F;
			this.txtBaseFloor2.Width = 0.625F;
			// 
			// Label106
			// 
			this.Label106.Height = 0.1666667F;
			this.Label106.HyperLink = null;
			this.Label106.Left = 2.1875F;
			this.Label106.MultiLine = false;
			this.Label106.Name = "Label106";
			this.Label106.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label106.Text = "Stories/Height";
			this.Label106.Top = 5.260417F;
			this.Label106.Width = 1.0625F;
			// 
			// Label107
			// 
			this.Label107.Height = 0.1666667F;
			this.Label107.HyperLink = null;
			this.Label107.Left = 2.1875F;
			this.Label107.MultiLine = false;
			this.Label107.Name = "Label107";
			this.Label107.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label107.Text = "Base Floor Area";
			this.Label107.Top = 5.427083F;
			this.Label107.Width = 1.0625F;
			// 
			// Label108
			// 
			this.Label108.Height = 0.1666667F;
			this.Label108.HyperLink = null;
			this.Label108.Left = 2.1875F;
			this.Label108.MultiLine = false;
			this.Label108.Name = "Label108";
			this.Label108.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label108.Text = "Class & Quality";
			this.Label108.Top = 4.760417F;
			this.Label108.Width = 1.0625F;
			// 
			// txtclass2
			// 
			this.txtclass2.Height = 0.1666667F;
			this.txtclass2.Left = 3.25F;
			this.txtclass2.Name = "txtclass2";
			this.txtclass2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtclass2.Text = null;
			this.txtclass2.Top = 4.760417F;
			this.txtclass2.Width = 0.25F;
			// 
			// Label109
			// 
			this.Label109.Height = 0.1666667F;
			this.Label109.HyperLink = null;
			this.Label109.Left = 2.1875F;
			this.Label109.MultiLine = false;
			this.Label109.Name = "Label109";
			this.Label109.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label109.Text = "Grade Factor";
			this.Label109.Top = 4.927083F;
			this.Label109.Width = 1.0625F;
			// 
			// txtgradefactor2
			// 
			this.txtgradefactor2.Height = 0.1666667F;
			this.txtgradefactor2.Left = 3.25F;
			this.txtgradefactor2.Name = "txtgradefactor2";
			this.txtgradefactor2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtgradefactor2.Text = null;
			this.txtgradefactor2.Top = 4.927083F;
			this.txtgradefactor2.Width = 0.4375F;
			// 
			// Label110
			// 
			this.Label110.Height = 0.1666667F;
			this.Label110.HyperLink = null;
			this.Label110.Left = 2.1875F;
			this.Label110.MultiLine = false;
			this.Label110.Name = "Label110";
			this.Label110.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label110.Text = "Exterior Walls";
			this.Label110.Top = 5.09375F;
			this.Label110.Width = 1.0625F;
			// 
			// txtExteriorWalls2
			// 
			this.txtExteriorWalls2.Height = 0.1666667F;
			this.txtExteriorWalls2.Left = 3.25F;
			this.txtExteriorWalls2.Name = "txtExteriorWalls2";
			this.txtExteriorWalls2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtExteriorWalls2.Text = null;
			this.txtExteriorWalls2.Top = 5.09375F;
			this.txtExteriorWalls2.Width = 0.4375F;
			// 
			// txtHeatCool2
			// 
			this.txtHeatCool2.Height = 0.1666667F;
			this.txtHeatCool2.Left = 3.25F;
			this.txtHeatCool2.Name = "txtHeatCool2";
			this.txtHeatCool2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtHeatCool2.Text = null;
			this.txtHeatCool2.Top = 5.760417F;
			this.txtHeatCool2.Width = 0.4375F;
			// 
			// txtYearBuilt2
			// 
			this.txtYearBuilt2.Height = 0.1666667F;
			this.txtYearBuilt2.Left = 3.25F;
			this.txtYearBuilt2.Name = "txtYearBuilt2";
			this.txtYearBuilt2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtYearBuilt2.Text = null;
			this.txtYearBuilt2.Top = 5.916667F;
			this.txtYearBuilt2.Width = 0.4375F;
			// 
			// Label111
			// 
			this.Label111.Height = 0.1666667F;
			this.Label111.HyperLink = null;
			this.Label111.Left = 2.1875F;
			this.Label111.MultiLine = false;
			this.Label111.Name = "Label111";
			this.Label111.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label111.Text = "Heating/Cooling";
			this.Label111.Top = 5.760417F;
			this.Label111.Width = 1.0625F;
			// 
			// Label112
			// 
			this.Label112.Height = 0.1666667F;
			this.Label112.HyperLink = null;
			this.Label112.Left = 2.1875F;
			this.Label112.MultiLine = false;
			this.Label112.Name = "Label112";
			this.Label112.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label112.Text = "Year Built";
			this.Label112.Top = 5.916667F;
			this.Label112.Width = 1.0625F;
			// 
			// Label113
			// 
			this.Label113.Height = 0.1666667F;
			this.Label113.HyperLink = null;
			this.Label113.Left = 2.1875F;
			this.Label113.MultiLine = false;
			this.Label113.Name = "Label113";
			this.Label113.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label113.Text = "Perimeter/Units";
			this.Label113.Top = 5.59375F;
			this.Label113.Width = 1.0625F;
			// 
			// txtPerimeter2
			// 
			this.txtPerimeter2.Height = 0.1666667F;
			this.txtPerimeter2.Left = 3.25F;
			this.txtPerimeter2.Name = "txtPerimeter2";
			this.txtPerimeter2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtPerimeter2.Text = null;
			this.txtPerimeter2.Top = 5.59375F;
			this.txtPerimeter2.Width = 0.625F;
			// 
			// txtYearRemodelled2
			// 
			this.txtYearRemodelled2.Height = 0.1666667F;
			this.txtYearRemodelled2.Left = 3.25F;
			this.txtYearRemodelled2.Name = "txtYearRemodelled2";
			this.txtYearRemodelled2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtYearRemodelled2.Text = null;
			this.txtYearRemodelled2.Top = 6.083333F;
			this.txtYearRemodelled2.Width = 0.4375F;
			// 
			// txtcond2
			// 
			this.txtcond2.Height = 0.1666667F;
			this.txtcond2.Left = 3.25F;
			this.txtcond2.Name = "txtcond2";
			this.txtcond2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtcond2.Text = null;
			this.txtcond2.Top = 6.25F;
			this.txtcond2.Width = 0.4375F;
			// 
			// Label114
			// 
			this.Label114.Height = 0.1666667F;
			this.Label114.HyperLink = null;
			this.Label114.Left = 2.1875F;
			this.Label114.MultiLine = false;
			this.Label114.Name = "Label114";
			this.Label114.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label114.Text = "Year Remodeled";
			this.Label114.Top = 6.083333F;
			this.Label114.Width = 1.0625F;
			// 
			// Label115
			// 
			this.Label115.Height = 0.1666667F;
			this.Label115.HyperLink = null;
			this.Label115.Left = 2.1875F;
			this.Label115.MultiLine = false;
			this.Label115.Name = "Label115";
			this.Label115.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label115.Text = "Condition";
			this.Label115.Top = 6.25F;
			this.Label115.Width = 1.0625F;
			// 
			// txtfunc2
			// 
			this.txtfunc2.Height = 0.1666667F;
			this.txtfunc2.Left = 3.25F;
			this.txtfunc2.Name = "txtfunc2";
			this.txtfunc2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtfunc2.Text = null;
			this.txtfunc2.Top = 6.583333F;
			this.txtfunc2.Width = 0.4375F;
			// 
			// Label116
			// 
			this.Label116.Height = 0.1666667F;
			this.Label116.HyperLink = null;
			this.Label116.Left = 2.1875F;
			this.Label116.MultiLine = false;
			this.Label116.Name = "Label116";
			this.Label116.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label116.Text = "Functional";
			this.Label116.Top = 6.583333F;
			this.Label116.Width = 1.0625F;
			// 
			// Label117
			// 
			this.Label117.Height = 0.1666667F;
			this.Label117.HyperLink = null;
			this.Label117.Left = 2.1875F;
			this.Label117.MultiLine = false;
			this.Label117.Name = "Label117";
			this.Label117.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label117.Text = "% Good Physical";
			this.Label117.Top = 6.416667F;
			this.Label117.Width = 1.0625F;
			// 
			// txtphys2
			// 
			this.txtphys2.Height = 0.1666667F;
			this.txtphys2.Left = 3.25F;
			this.txtphys2.Name = "txtphys2";
			this.txtphys2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtphys2.Text = null;
			this.txtphys2.Top = 6.416667F;
			this.txtphys2.Width = 0.4375F;
			// 
			// txtqual1
			// 
			this.txtqual1.Height = 0.1666667F;
			this.txtqual1.Left = 1.4375F;
			this.txtqual1.Name = "txtqual1";
			this.txtqual1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtqual1.Text = null;
			this.txtqual1.Top = 4.760417F;
			this.txtqual1.Width = 0.4375F;
			// 
			// txtqual2
			// 
			this.txtqual2.Height = 0.1666667F;
			this.txtqual2.Left = 3.5F;
			this.txtqual2.Name = "txtqual2";
			this.txtqual2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtqual2.Text = null;
			this.txtqual2.Top = 4.760417F;
			this.txtqual2.Width = 0.4375F;
			// 
			// txtHeight2
			// 
			this.txtHeight2.Height = 0.1666667F;
			this.txtHeight2.Left = 3.5F;
			this.txtHeight2.Name = "txtHeight2";
			this.txtHeight2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtHeight2.Text = null;
			this.txtHeight2.Top = 5.260417F;
			this.txtHeight2.Width = 0.4375F;
			// 
			// txtheight1
			// 
			this.txtheight1.Height = 0.1666667F;
			this.txtheight1.Left = 1.4375F;
			this.txtheight1.Name = "txtheight1";
			this.txtheight1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtheight1.Text = null;
			this.txtheight1.Top = 5.260417F;
			this.txtheight1.Width = 0.4375F;
			// 
			// txtDwellingUnits1
			// 
			this.txtDwellingUnits1.Height = 0.1666667F;
			this.txtDwellingUnits1.Left = 1.1875F;
			this.txtDwellingUnits1.Name = "txtDwellingUnits1";
			this.txtDwellingUnits1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtDwellingUnits1.Text = null;
			this.txtDwellingUnits1.Top = 4.59375F;
			this.txtDwellingUnits1.Width = 0.4375F;
			// 
			// Label118
			// 
			this.Label118.Height = 0.1666667F;
			this.Label118.HyperLink = null;
			this.Label118.Left = 0.25F;
			this.Label118.MultiLine = false;
			this.Label118.Name = "Label118";
			this.Label118.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label118.Text = "Account";
			this.Label118.Top = 0F;
			this.Label118.Width = 0.5625F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1666667F;
			this.txtAccount.Left = 1F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.5625F;
			// 
			// txtCard
			// 
			this.txtCard.Height = 0.1666667F;
			this.txtCard.Left = 2F;
			this.txtCard.Name = "txtCard";
			this.txtCard.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtCard.Text = null;
			this.txtCard.Top = 0F;
			this.txtCard.Width = 1.75F;
			// 
			// Label119
			// 
			this.Label119.Height = 0.1666667F;
			this.Label119.HyperLink = null;
			this.Label119.Left = 1.625F;
			this.Label119.MultiLine = false;
			this.Label119.Name = "Label119";
			this.Label119.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label119.Text = "Card ";
			this.Label119.Top = 0F;
			this.Label119.Width = 0.375F;
			// 
			// Label120
			// 
			this.Label120.Height = 0.1666667F;
			this.Label120.HyperLink = null;
			this.Label120.Left = 4F;
			this.Label120.MultiLine = false;
			this.Label120.Name = "Label120";
			this.Label120.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label120.Text = "Map/Lot";
			this.Label120.Top = 0F;
			this.Label120.Width = 0.625F;
			// 
			// txtMap
			// 
			this.txtMap.Height = 0.1666667F;
			this.txtMap.Left = 4.625F;
			this.txtMap.Name = "txtMap";
			this.txtMap.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtMap.Text = null;
			this.txtMap.Top = 0F;
			this.txtMap.Width = 2.75F;
			// 
			// txtOIGrade1
			// 
			this.txtOIGrade1.Height = 0.1666667F;
			this.txtOIGrade1.Left = 2.375F;
			this.txtOIGrade1.Name = "txtOIGrade1";
			this.txtOIGrade1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtOIGrade1.Text = null;
			this.txtOIGrade1.Top = 7.541667F;
			this.txtOIGrade1.Width = 0.4375F;
			// 
			// txtOIGrade10
			// 
			this.txtOIGrade10.Height = 0.1666667F;
			this.txtOIGrade10.Left = 2.375F;
			this.txtOIGrade10.Name = "txtOIGrade10";
			this.txtOIGrade10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtOIGrade10.Text = null;
			this.txtOIGrade10.Top = 9.041667F;
			this.txtOIGrade10.Width = 0.4375F;
			// 
			// txtOIGrade9
			// 
			this.txtOIGrade9.Height = 0.1666667F;
			this.txtOIGrade9.Left = 2.375F;
			this.txtOIGrade9.Name = "txtOIGrade9";
			this.txtOIGrade9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtOIGrade9.Text = null;
			this.txtOIGrade9.Top = 8.875F;
			this.txtOIGrade9.Width = 0.4375F;
			// 
			// txtOIGrade8
			// 
			this.txtOIGrade8.Height = 0.1666667F;
			this.txtOIGrade8.Left = 2.375F;
			this.txtOIGrade8.Name = "txtOIGrade8";
			this.txtOIGrade8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtOIGrade8.Text = null;
			this.txtOIGrade8.Top = 8.708333F;
			this.txtOIGrade8.Width = 0.4375F;
			// 
			// txtOIGrade7
			// 
			this.txtOIGrade7.Height = 0.1666667F;
			this.txtOIGrade7.Left = 2.375F;
			this.txtOIGrade7.Name = "txtOIGrade7";
			this.txtOIGrade7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtOIGrade7.Text = null;
			this.txtOIGrade7.Top = 8.541667F;
			this.txtOIGrade7.Width = 0.4375F;
			// 
			// txtOIGrade6
			// 
			this.txtOIGrade6.Height = 0.1666667F;
			this.txtOIGrade6.Left = 2.375F;
			this.txtOIGrade6.Name = "txtOIGrade6";
			this.txtOIGrade6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtOIGrade6.Text = null;
			this.txtOIGrade6.Top = 8.375F;
			this.txtOIGrade6.Width = 0.4375F;
			// 
			// txtOIGrade5
			// 
			this.txtOIGrade5.Height = 0.1666667F;
			this.txtOIGrade5.Left = 2.375F;
			this.txtOIGrade5.Name = "txtOIGrade5";
			this.txtOIGrade5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtOIGrade5.Text = null;
			this.txtOIGrade5.Top = 8.21875F;
			this.txtOIGrade5.Width = 0.4375F;
			// 
			// txtOIGrade4
			// 
			this.txtOIGrade4.Height = 0.1666667F;
			this.txtOIGrade4.Left = 2.375F;
			this.txtOIGrade4.Name = "txtOIGrade4";
			this.txtOIGrade4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtOIGrade4.Text = null;
			this.txtOIGrade4.Top = 8.052083F;
			this.txtOIGrade4.Width = 0.4375F;
			// 
			// txtOIGrade3
			// 
			this.txtOIGrade3.Height = 0.1666667F;
			this.txtOIGrade3.Left = 2.375F;
			this.txtOIGrade3.Name = "txtOIGrade3";
			this.txtOIGrade3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtOIGrade3.Text = null;
			this.txtOIGrade3.Top = 7.885417F;
			this.txtOIGrade3.Width = 0.4375F;
			// 
			// txtOIGrade2
			// 
			this.txtOIGrade2.Height = 0.1666667F;
			this.txtOIGrade2.Left = 2.375F;
			this.txtOIGrade2.Name = "txtOIGrade2";
			this.txtOIGrade2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtOIGrade2.Text = null;
			this.txtOIGrade2.Top = 7.708333F;
			this.txtOIGrade2.Width = 0.4375F;
			// 
			// Label121
			// 
			this.Label121.Height = 0.1875F;
			this.Label121.HyperLink = null;
			this.Label121.Left = 2.875F;
			this.Label121.MultiLine = false;
			this.Label121.Name = "Label121";
			this.Label121.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.Label121.Text = "Factor";
			this.Label121.Top = 7.354167F;
			this.Label121.Width = 0.625F;
			// 
			// txtReference2
			// 
			this.txtReference2.Height = 0.1666667F;
			this.txtReference2.Left = 1F;
			this.txtReference2.Name = "txtReference2";
			this.txtReference2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtReference2.Text = null;
			this.txtReference2.Top = 1.333333F;
			this.txtReference2.Width = 2.125F;
			// 
			// Label122
			// 
			this.Label122.Height = 0.1666667F;
			this.Label122.HyperLink = null;
			this.Label122.Left = 0.25F;
			this.Label122.MultiLine = false;
			this.Label122.Name = "Label122";
			this.Label122.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label122.Text = "Reference 2";
			this.Label122.Top = 1.333333F;
			this.Label122.Width = 0.6875F;
			// 
			// txtBookPage
			// 
			this.txtBookPage.Height = 0.1666667F;
			this.txtBookPage.Left = 1F;
			this.txtBookPage.Name = "txtBookPage";
			this.txtBookPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtBookPage.Text = null;
			this.txtBookPage.Top = 1.5F;
			this.txtBookPage.Width = 3.3125F;
			// 
			// Label123
			// 
			this.Label123.Height = 0.1666667F;
			this.Label123.HyperLink = null;
			this.Label123.Left = 0.25F;
			this.Label123.MultiLine = false;
			this.Label123.Name = "Label123";
			this.Label123.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label123.Text = "Book & Page";
			this.Label123.Top = 1.5F;
			this.Label123.Width = 0.75F;
			// 
			// Label124
			// 
			this.Label124.Height = 0.1666667F;
			this.Label124.HyperLink = null;
			this.Label124.Left = 0.25F;
			this.Label124.MultiLine = false;
			this.Label124.Name = "Label124";
			this.Label124.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label124.Text = "2nd Owner";
			this.Label124.Top = 0.3333333F;
			this.Label124.Width = 0.7083333F;
			// 
			// txtSecondOwner
			// 
			this.txtSecondOwner.Height = 0.1666667F;
			this.txtSecondOwner.Left = 1F;
			this.txtSecondOwner.Name = "txtSecondOwner";
			this.txtSecondOwner.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtSecondOwner.Text = null;
			this.txtSecondOwner.Top = 0.3333333F;
			this.txtSecondOwner.Width = 2.770833F;
			// 
			// Label125
			// 
			this.Label125.Height = 0.1875F;
			this.Label125.HyperLink = null;
			this.Label125.Left = 6.5625F;
			this.Label125.MultiLine = false;
			this.Label125.Name = "Label125";
			this.Label125.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label125.Text = "Sound Value";
			this.Label125.Top = 7.354167F;
			this.Label125.Width = 0.75F;
			// 
			// txtSV1
			// 
			this.txtSV1.Height = 0.1666667F;
			this.txtSV1.Left = 6.5625F;
			this.txtSV1.Name = "txtSV1";
			this.txtSV1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtSV1.Text = null;
			this.txtSV1.Top = 7.541667F;
			this.txtSV1.Width = 0.6875F;
			// 
			// txtSV2
			// 
			this.txtSV2.Height = 0.1666667F;
			this.txtSV2.Left = 6.5625F;
			this.txtSV2.Name = "txtSV2";
			this.txtSV2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtSV2.Text = null;
			this.txtSV2.Top = 7.708333F;
			this.txtSV2.Width = 0.6875F;
			// 
			// txtSV3
			// 
			this.txtSV3.Height = 0.1666667F;
			this.txtSV3.Left = 6.5625F;
			this.txtSV3.Name = "txtSV3";
			this.txtSV3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtSV3.Text = null;
			this.txtSV3.Top = 7.885417F;
			this.txtSV3.Width = 0.6875F;
			// 
			// txtSV4
			// 
			this.txtSV4.Height = 0.1666667F;
			this.txtSV4.Left = 6.5625F;
			this.txtSV4.Name = "txtSV4";
			this.txtSV4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtSV4.Text = null;
			this.txtSV4.Top = 8.052083F;
			this.txtSV4.Width = 0.6875F;
			// 
			// txtSV5
			// 
			this.txtSV5.Height = 0.1666667F;
			this.txtSV5.Left = 6.5625F;
			this.txtSV5.Name = "txtSV5";
			this.txtSV5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtSV5.Text = null;
			this.txtSV5.Top = 8.21875F;
			this.txtSV5.Width = 0.6875F;
			// 
			// txtSV6
			// 
			this.txtSV6.Height = 0.1666667F;
			this.txtSV6.Left = 6.5625F;
			this.txtSV6.Name = "txtSV6";
			this.txtSV6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtSV6.Text = null;
			this.txtSV6.Top = 8.375F;
			this.txtSV6.Width = 0.6875F;
			// 
			// txtSV7
			// 
			this.txtSV7.Height = 0.1666667F;
			this.txtSV7.Left = 6.5625F;
			this.txtSV7.Name = "txtSV7";
			this.txtSV7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtSV7.Text = null;
			this.txtSV7.Top = 8.541667F;
			this.txtSV7.Width = 0.6875F;
			// 
			// txtSV9
			// 
			this.txtSV9.Height = 0.1666667F;
			this.txtSV9.Left = 6.5625F;
			this.txtSV9.Name = "txtSV9";
			this.txtSV9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtSV9.Text = null;
			this.txtSV9.Top = 8.875F;
			this.txtSV9.Width = 0.6875F;
			// 
			// txtSV10
			// 
			this.txtSV10.Height = 0.1666667F;
			this.txtSV10.Left = 6.5625F;
			this.txtSV10.Name = "txtSV10";
			this.txtSV10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtSV10.Text = null;
			this.txtSV10.Top = 9.041667F;
			this.txtSV10.Width = 0.6875F;
			// 
			// txtSV8
			// 
			this.txtSV8.Height = 0.1666667F;
			this.txtSV8.Left = 6.5625F;
			this.txtSV8.Name = "txtSV8";
			this.txtSV8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.txtSV8.Text = null;
			this.txtSV8.Top = 8.708333F;
			this.txtSV8.Width = 0.6875F;
			// 
			// Label126
			// 
			this.Label126.Height = 0.1875F;
			this.Label126.HyperLink = null;
			this.Label126.Left = 5.9375F;
			this.Label126.MultiLine = false;
			this.Label126.Name = "Label126";
			this.Label126.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.Label126.Text = "Use SV";
			this.Label126.Top = 7.354167F;
			this.Label126.Width = 0.4375F;
			// 
			// txtUseSV1
			// 
			this.txtUseSV1.Height = 0.1666667F;
			this.txtUseSV1.Left = 5.9375F;
			this.txtUseSV1.Name = "txtUseSV1";
			this.txtUseSV1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtUseSV1.Text = null;
			this.txtUseSV1.Top = 7.541667F;
			this.txtUseSV1.Width = 0.4375F;
			// 
			// txtUseSV2
			// 
			this.txtUseSV2.Height = 0.1666667F;
			this.txtUseSV2.Left = 5.9375F;
			this.txtUseSV2.Name = "txtUseSV2";
			this.txtUseSV2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtUseSV2.Text = null;
			this.txtUseSV2.Top = 7.708333F;
			this.txtUseSV2.Width = 0.4375F;
			// 
			// txtUseSV3
			// 
			this.txtUseSV3.Height = 0.1666667F;
			this.txtUseSV3.Left = 5.9375F;
			this.txtUseSV3.Name = "txtUseSV3";
			this.txtUseSV3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtUseSV3.Text = null;
			this.txtUseSV3.Top = 7.885417F;
			this.txtUseSV3.Width = 0.4375F;
			// 
			// txtUseSV4
			// 
			this.txtUseSV4.Height = 0.1666667F;
			this.txtUseSV4.Left = 5.9375F;
			this.txtUseSV4.Name = "txtUseSV4";
			this.txtUseSV4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtUseSV4.Text = null;
			this.txtUseSV4.Top = 8.052083F;
			this.txtUseSV4.Width = 0.4375F;
			// 
			// txtUseSV5
			// 
			this.txtUseSV5.Height = 0.1666667F;
			this.txtUseSV5.Left = 5.9375F;
			this.txtUseSV5.Name = "txtUseSV5";
			this.txtUseSV5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtUseSV5.Text = null;
			this.txtUseSV5.Top = 8.21875F;
			this.txtUseSV5.Width = 0.4375F;
			// 
			// txtUseSV6
			// 
			this.txtUseSV6.Height = 0.1666667F;
			this.txtUseSV6.Left = 5.9375F;
			this.txtUseSV6.Name = "txtUseSV6";
			this.txtUseSV6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtUseSV6.Text = null;
			this.txtUseSV6.Top = 8.375F;
			this.txtUseSV6.Width = 0.4375F;
			// 
			// txtUseSV7
			// 
			this.txtUseSV7.Height = 0.1666667F;
			this.txtUseSV7.Left = 5.9375F;
			this.txtUseSV7.Name = "txtUseSV7";
			this.txtUseSV7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtUseSV7.Text = null;
			this.txtUseSV7.Top = 8.541667F;
			this.txtUseSV7.Width = 0.4375F;
			// 
			// txtUseSV9
			// 
			this.txtUseSV9.Height = 0.1666667F;
			this.txtUseSV9.Left = 5.9375F;
			this.txtUseSV9.Name = "txtUseSV9";
			this.txtUseSV9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtUseSV9.Text = null;
			this.txtUseSV9.Top = 8.875F;
			this.txtUseSV9.Width = 0.4375F;
			// 
			// txtUseSV10
			// 
			this.txtUseSV10.Height = 0.1666667F;
			this.txtUseSV10.Left = 5.9375F;
			this.txtUseSV10.Name = "txtUseSV10";
			this.txtUseSV10.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtUseSV10.Text = null;
			this.txtUseSV10.Top = 9.041667F;
			this.txtUseSV10.Width = 0.4375F;
			// 
			// txtUseSV8
			// 
			this.txtUseSV8.Height = 0.1666667F;
			this.txtUseSV8.Left = 5.9375F;
			this.txtUseSV8.Name = "txtUseSV8";
			this.txtUseSV8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.txtUseSV8.Text = null;
			this.txtUseSV8.Top = 8.708333F;
			this.txtUseSV8.Width = 0.4375F;
			// 
			// Label127
			// 
			this.Label127.Height = 0.1666667F;
			this.Label127.HyperLink = null;
			this.Label127.Left = 0.0625F;
			this.Label127.MultiLine = false;
			this.Label127.Name = "Label127";
			this.Label127.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.Label127.Text = "Tree Growth Year";
			this.Label127.Top = 2.21875F;
			this.Label127.Width = 1.0625F;
			// 
			// txtTreeGrowthYear
			// 
			this.txtTreeGrowthYear.Height = 0.1666667F;
			this.txtTreeGrowthYear.Left = 1.125F;
			this.txtTreeGrowthYear.Name = "txtTreeGrowthYear";
			this.txtTreeGrowthYear.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTreeGrowthYear.Text = null;
			this.txtTreeGrowthYear.Top = 2.21875F;
			this.txtTreeGrowthYear.Width = 1.0625F;
			// 
			// txtTopography2
			// 
			this.txtTopography2.Height = 0.1666667F;
			this.txtTopography2.Left = 1.125F;
			this.txtTopography2.Name = "txtTopography2";
			this.txtTopography2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtTopography2.Text = null;
			this.txtTopography2.Top = 3.21875F;
			this.txtTopography2.Width = 1.0625F;
			// 
			// txtUtility2
			// 
			this.txtUtility2.Height = 0.1666667F;
			this.txtUtility2.Left = 1.125F;
			this.txtUtility2.Name = "txtUtility2";
			this.txtUtility2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.txtUtility2.Text = null;
			this.txtUtility2.Top = 3.552083F;
			this.txtUtility2.Width = 1.0625F;
			// 
			// txtExemptCode2
			// 
			this.txtExemptCode2.Height = 0.1666667F;
			this.txtExemptCode2.Left = 5.375F;
			this.txtExemptCode2.Name = "txtExemptCode2";
			this.txtExemptCode2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtExemptCode2.Text = null;
			this.txtExemptCode2.Top = 0.8333333F;
			this.txtExemptCode2.Width = 1.875F;
			// 
			// txtExemptCode3
			// 
			this.txtExemptCode3.Height = 0.1666667F;
			this.txtExemptCode3.Left = 5.375F;
			this.txtExemptCode3.Name = "txtExemptCode3";
			this.txtExemptCode3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtExemptCode3.Text = null;
			this.txtExemptCode3.Top = 1F;
			this.txtExemptCode3.Width = 1.875F;
			// 
			// txtPropertyCode
			// 
			this.txtPropertyCode.Height = 0.1666667F;
			this.txtPropertyCode.Left = 5.375F;
			this.txtPropertyCode.Name = "txtPropertyCode";
			this.txtPropertyCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.txtPropertyCode.Text = null;
			this.txtPropertyCode.Top = 1.666667F;
			this.txtPropertyCode.Width = 1.875F;
			// 
			// Label128
			// 
			this.Label128.Height = 0.1666667F;
			this.Label128.HyperLink = null;
			this.Label128.Left = 4.4375F;
			this.Label128.MultiLine = false;
			this.Label128.Name = "Label128";
			this.Label128.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.Label128.Text = "Property Code";
			this.Label128.Top = 1.666667F;
			this.Label128.Width = 0.875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCaption,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.Field5,
				this.Field6,
				this.lblTitle2
			});
			this.PageHeader.Height = 0.4166667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// txtCaption
			// 
			this.txtCaption.Height = 0.1875F;
			this.txtCaption.Left = 1.875F;
			this.txtCaption.Name = "txtCaption";
			this.txtCaption.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtCaption.Text = "Account";
			this.txtCaption.Top = 0.03125F;
			this.txtCaption.Width = 3.6875F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.15625F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.15625F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.17625F;
			this.txtDate.Left = 5.875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.375F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 7F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field5.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.Field5.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.Field5.Text = null;
			this.Field5.Top = 0.1875F;
			this.Field5.Visible = false;
			this.Field5.Width = 0.25F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 6.375F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field6.Text = "Page";
			this.Field6.Top = 0.1875F;
			this.Field6.Visible = false;
			this.Field6.Width = 0.625F;
			// 
			// lblTitle2
			// 
			this.lblTitle2.Height = 0.1666667F;
			this.lblTitle2.HyperLink = null;
			this.lblTitle2.Left = 1.666667F;
			this.lblTitle2.Name = "lblTitle2";
			this.lblTitle2.Style = "";
			this.lblTitle2.Text = null;
			this.lblTitle2.Top = 0.2083333F;
			this.lblTitle2.Width = 4.166667F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptInputCard
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.PageEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			((System.ComponentModel.ISupportInitialize)(this.Field68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptCode1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTranCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuildingCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNeighborhood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopography)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtXCoordinate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtilities)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYCoordinate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtForestYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAffidavit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStreet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleVerified)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleValidity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSalePrice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSaleFinancing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandType7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandUnits7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInf7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCd7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuildingStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofSurface)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDwellingUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMasonryTrim)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStories)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorWalls)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearBuilt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearRemodeled)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBSMTGar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtWetBasement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBSMTLiving)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCoolType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBSMTGrade)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercentCooled)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPercentHeated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRooms)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchenStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBedrooms)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFullBaths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFixtures)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtfireplaces)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHalfBaths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFactor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttic)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSQFootage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnfinished)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysicalGood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctGood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconGood)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label82)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEntranceCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInfoCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label83)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label84)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label85)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEconCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateInspected)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label86)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label87)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label88)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label89)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label90)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label91)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label92)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhysical10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunctional8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label93)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOCC1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtstories1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbasefloor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label94)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label95)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label96)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtclass1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label97)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtgradefactor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorWalls1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtheatcool1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearBuilt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label98)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label99)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtperimeter1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearRemodelled1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtcond1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label100)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtfunc1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txteconomic1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label101)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label102)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label103)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtphys1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label104)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label105)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOCC2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtstories2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDwellingUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaseFloor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label106)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label107)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label108)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtclass2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label109)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtgradefactor2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExteriorWalls2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCool2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearBuilt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPerimeter2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYearRemodelled2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtcond2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtfunc2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtphys2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtqual1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtqual2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeight2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtheight1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDwellingUnits1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMap)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOIGrade2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReference2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label122)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label123)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label124)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label125)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSV8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label126)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUseSV8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label127)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTreeGrowthYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTopography2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUtility2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptCode2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptCode3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPropertyCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label128)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field68;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemption;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReference;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptCode1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTranCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBuildingCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNeighborhood;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopography;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtXCoordinate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtilities;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYCoordinate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZone;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZone2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtForestYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAffidavit;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStreet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleVerified;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleValidity;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSalePrice;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleType;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaleFinancing;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandType1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandUnits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInf1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInf2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandType3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandUnits3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInf3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandType4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandUnits4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInf4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandType5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandUnits5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInf5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandType6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandUnits6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInf6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandType7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandUnits7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInf7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCd7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBuildingStyle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofSurface;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDwellingUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMasonryTrim;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherUnits;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStories;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorWalls;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearBuilt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearRemodeled;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBSMTGar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtWetBasement;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasement;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBSMTLiving;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCoolType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBSMTGrade;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPercentCooled;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatType;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPercentHeated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathStyle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRooms;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label65;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchenStyle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBedrooms;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFullBaths;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFixtures;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtfireplaces;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHalfBaths;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayout;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFactor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAttic;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSQFootage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnfinished;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysicalGood;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctGood;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label79;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconGood;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label82;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEntranceCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInfoCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label83;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label84;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label85;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEconCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateInspected;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label86;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field67;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label87;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label88;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label89;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label90;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label91;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label92;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhysical10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunctional8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label93;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOCC1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtstories1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtbasefloor1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label94;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label95;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label96;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtclass1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label97;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtgradefactor1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorWalls1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtheatcool1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearBuilt1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label98;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label99;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtperimeter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearRemodelled1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtcond1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label100;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtfunc1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txteconomic1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label101;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label102;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label103;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtphys1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label104;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label105;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOCC2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtstories2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDwellingUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBaseFloor2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label106;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label107;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label108;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtclass2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label109;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtgradefactor2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label110;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExteriorWalls2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCool2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearBuilt2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label111;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label112;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label113;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPerimeter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYearRemodelled2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtcond2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label114;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label115;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtfunc2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label116;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label117;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtphys2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtqual1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtqual2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeight2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtheight1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDwellingUnits1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label118;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCard;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label119;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label120;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMap;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOIGrade1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOIGrade10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOIGrade9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOIGrade8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOIGrade7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOIGrade6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOIGrade5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOIGrade4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOIGrade3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOIGrade2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label121;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReference2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label122;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label123;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label124;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondOwner;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label125;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSV1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSV2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSV3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSV4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSV5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSV6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSV7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSV9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSV10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSV8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label126;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUseSV1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUseSV2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUseSV3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUseSV4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUseSV5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUseSV6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUseSV7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUseSV9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUseSV10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUseSV8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label127;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTreeGrowthYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTopography2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUtility2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptCode2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptCode3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPropertyCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label128;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
