﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmShowResults.
	/// </summary>
	partial class frmShowResults : BaseForm
	{
		public fecherFoundation.FCCommonDialog CommonDialog1;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCRichTextBox rtbText;
		public fecherFoundation.FCLabel lblScreenTitle;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmShowResults));
            this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
            this.cmdOK = new fecherFoundation.FCButton();
            this.rtbText = new fecherFoundation.FCRichTextBox();
            this.lblScreenTitle = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 503);
            this.BottomPanel.Size = new System.Drawing.Size(693, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdOK);
            this.ClientArea.Controls.Add(this.rtbText);
            this.ClientArea.Controls.Add(this.lblScreenTitle);
            this.ClientArea.Size = new System.Drawing.Size(693, 443);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(693, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(162, 30);
            this.HeaderText.Text = "Show Results";
            // 
            // CommonDialog1
            // 
            this.CommonDialog1.Color = System.Drawing.Color.Black;
            this.CommonDialog1.DefaultExt = null;
            this.CommonDialog1.FilterIndex = ((short)(0));
            this.CommonDialog1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CommonDialog1.FontName = "Microsoft Sans Serif";
            this.CommonDialog1.FontSize = 8.25F;
            this.CommonDialog1.ForeColor = System.Drawing.Color.Black;
            this.CommonDialog1.FromPage = 0;
            this.CommonDialog1.Location = new System.Drawing.Point(0, 0);
            this.CommonDialog1.Name = "CommonDialog1";
            this.CommonDialog1.PrinterSettings = null;
            this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
            this.CommonDialog1.TabIndex = 0;
            this.CommonDialog1.ToPage = 0;
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(539, 18);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(80, 40);
            this.cmdOK.TabIndex = 3;
            this.cmdOK.Text = "OK";
            this.cmdOK.Visible = false;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // rtbText
            // 
            this.rtbText.Location = new System.Drawing.Point(30, 75);
            this.rtbText.Multiline = true;
            this.rtbText.ReadOnly = true;
            this.rtbText.Name = "rtbText";
            this.rtbText.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
            this.rtbText.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
            this.rtbText.SelTabCount = null;
            this.rtbText.Size = new System.Drawing.Size(603, 330);
            this.rtbText.TabIndex = 2;
            this.rtbText.TabStop = false;
            // 
            // lblScreenTitle
            // 
            this.lblScreenTitle.Location = new System.Drawing.Point(30, 30);
            this.lblScreenTitle.Name = "lblScreenTitle";
            this.lblScreenTitle.Size = new System.Drawing.Size(300, 15);
            this.lblScreenTitle.TabIndex = 0;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 0;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(302, 33);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(88, 48);
            this.cmdPrint.TabIndex = 0;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // frmShowResults
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(693, 611);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmShowResults";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Show Results";
            this.Load += new System.EventHandler(this.frmShowResults_Load);
            this.Activated += new System.EventHandler(this.frmShowResults_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmShowResults_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmShowResults_KeyPress);
            this.Resize += new System.EventHandler(this.frmShowResults_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdPrint;
	}
}
