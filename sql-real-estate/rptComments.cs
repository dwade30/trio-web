﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptComments.
	/// </summary>
	public partial class rptComments : BaseSectionReport
	{
		public static rptComments InstancePtr
		{
			get
			{
				return (rptComments)Sys.GetInstance(typeof(rptComments));
			}
		}

		protected rptComments _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptComments()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptComments	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strComment;
		bool boolPrinted;
		int intPage;
		// vbPorter upgrade warning: intCommNumber As short	OnWriteFCConvert.ToInt32(
		public void Init(string strComm, ref int intCommNumber, ref int intAcctNum)
		{
			strComment = strComm;
			// Label1.Caption = "Comment #" & intCommNumber & " for Account #" & intAcctNum
			Label1.Text = "Comment for Account " + FCConvert.ToString(intAcctNum) + "  Card " + FCConvert.ToString(intCommNumber);
			// Me.Show
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			boolPrinted = false;
			intPage = 1;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			boolPrinted = true;
			txtComment.Text = strComment;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// lblPage.Caption = "Page " & intPage
			// intPage = intPage + 1
		}

		
	}
}
