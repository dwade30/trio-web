﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLandSchedule.
	/// </summary>
	public partial class frmLandSchedule : BaseForm
	{
		public frmLandSchedule()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLandSchedule InstancePtr
		{
			get
			{
				return (frmLandSchedule)Sys.GetInstance(typeof(frmLandSchedule));
			}
		}

		protected frmLandSchedule _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		bool boolDataChanged;
		const int CNSTCOLAUTOID = 8;
		const int CNSTCOLCODE = 0;
		const int CNSTCOLTYPE = 1;
		const int CNSTCOLDESCRIPTION = 2;
		const int CNSTCOLAMOUNT = 3;
		const int CNSTCOLEXP1 = 4;
		const int CNSTCOLEXP2 = 5;
		const int CNSTCOLWidthEXP1 = 6;
		const int CNSTCOLWidthEXP2 = 7;
		const int CNSTCOLSTDCODE = 2;
		const int CNSTCOLSTDAMOUNT = 1;
		const int CNSTCOLSTDDESC = 0;
		const int CNSTROWSTDDEPTH = 0;
		const int CNSTROWSTDLOT = 1;
		const int CNSTROWSTDWIDTH = 2;
		// Private lngCurrentScheduleNum As Long
		private cLandScheduleController contLandSchedules = new cLandScheduleController();
		private cGenericCollection collSchedules = new cGenericCollection();
		private bool boolIsUpdating;

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			// FillGrid (Val(txtSchedule.Text))
			ShowScheduleByNumber(FCConvert.ToInt32(Conversion.Val(txtSchedule.Text)));
			txtSchedule.Text = "";
		}

		private void frmLandSchedule_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmLandSchedule_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLandSchedule properties;
			//frmLandSchedule.FillStyle	= 0;
			//frmLandSchedule.ScaleWidth	= 9300;
			//frmLandSchedule.ScaleHeight	= 7770;
			//frmLandSchedule.LinkTopic	= "Form2";
			//frmLandSchedule.LockControls	= true;
			//frmLandSchedule.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			boolIsUpdating = true;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			// lngCurrentScheduleNum = 1
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				gridTownCode.Visible = true;
			}
			else
			{
				gridTownCode.Visible = false;
			}
			SetupGridTownCode();
			LoadSchedules();
			collSchedules.MoveFirst();
			ShowSchedule();
			boolIsUpdating = false;
		}

		private void LoadSchedules()
		{
			collSchedules.ClearList();
			collSchedules = null;
			collSchedules = contLandSchedules.GetLandSchedules();
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from tblTranCode where code > 0 order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
			else
			{
				gridTownCode.Rows = 1;
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
			}
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				if (MessageBox.Show("Data has changed. Do you wish to save first?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveData())
						e.Cancel = true;
				}
			}
		}

		private void frmLandSchedule_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridTownCode();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// VB6 Bad Scope Dim:
			int lngRow = 0;
			if (Grid.Row == 0)
			{
				return;
			}
			if (!boolIsUpdating)
			{
				int lngCol;
				lngRow = Grid.Row;
				lngCol = Grid.Col;
				Grid.RowData(lngRow, true);
				boolDataChanged = true;
				if (collSchedules.IsCurrent())
				{
					cLandSchedule lSched;
					int lngLandCode = 0;
					lSched = (cLandSchedule)collSchedules.GetCurrentItem();
					lSched.IsUpdated = true;
					lngLandCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLCODE))));
					lSched.Details.MoveFirst();
					bool boolFound = false;
					cLandScheduleRecord schedDetail = null;
					boolFound = false;
					while (lSched.Details.IsCurrent() && !boolFound)
					{
						schedDetail = (cLandScheduleRecord)lSched.Details.GetCurrentItem();
						if (schedDetail.LandTypeCode == lngLandCode)
						{
							boolFound = true;
							break;
						}
						lSched.Details.MoveNext();
					}
					if (!boolFound)
					{
						schedDetail = new cLandScheduleRecord();
						schedDetail.LandTypeCode = lngLandCode;
						schedDetail.ScheduleNumber = lSched.ScheduleNumber;
						schedDetail.TownNumber = lSched.TownCode;
						lSched.Details.AddItem(schedDetail);
					}
					schedDetail.Amount = Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLAMOUNT));
					schedDetail.Exponent1 = Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLEXP1));
					schedDetail.Exponent2 = Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLEXP2));
					schedDetail.WidthExponent1 = Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLWidthEXP1));
					schedDetail.WidthExponent2 = Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLWidthEXP2));
				}
			}
		}

		private void GridStandard_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			int lngRow;
			int lngCol;
			lngRow = GridStandard.Row;
			lngCol = GridStandard.Col;
			if (!boolIsUpdating)
			{
				if (collSchedules.IsCurrent())
				{
					cLandSchedule lSched = new cLandSchedule();
					lSched = (cLandSchedule)collSchedules.GetCurrentItem();
					if (lngCol == CNSTCOLSTDAMOUNT)
					{
						if (Conversion.Val(GridStandard.TextMatrix(lngRow, CNSTCOLSTDCODE)) == modREConstants.CNSTLANDSCHEDULESTANDARDDEPTH)
						{
							lSched.StandardDepth = Conversion.Val(GridStandard.TextMatrix(lngRow, CNSTCOLSTDAMOUNT));
						}
						else if (Conversion.Val(GridStandard.TextMatrix(lngRow, CNSTCOLSTDCODE)) == modREConstants.CNSTLANDSCHEDULESTANDARDLOT)
						{
							lSched.StandardLot = Conversion.Val(GridStandard.TextMatrix(lngRow, CNSTCOLSTDAMOUNT));
						}
						else if (Conversion.Val(GridStandard.TextMatrix(lngRow, CNSTCOLSTDCODE)) == modREConstants.CNSTLANDSCHEDULESTANDARDWIDTH)
						{
							lSched.StandardWidth = Conversion.Val(GridStandard.TextMatrix(lngRow, CNSTCOLSTDAMOUNT));
						}
					}
					boolDataChanged = true;
				}
			}
		}

		private void GridStandard_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            if (!boolIsUpdating)
			{
				boolDataChanged = true;
			}
		}

		private void gridTownCode_ComboCloseUp(object sender, EventArgs e)
		{
			if (!boolIsUpdating)
			{
				cLandSchedule lSched;
				if (collSchedules.IsCurrent())
				{
					lSched = (cLandSchedule)collSchedules.GetCurrentItem();
					lSched.TownCode = FCConvert.ToInt32(gridTownCode.ComboData());
					boolDataChanged = true;
				}
			}
		}

		private void mnuAddSchedule_Click(object sender, System.EventArgs e)
		{
			AddSchedule();
		}

		private void AddSchedule()
		{
			cLandSchedule lSched = new cLandSchedule();
			lSched.Description = "Schedule (New)";
			lSched.StandardLot = 1;
			lSched.StandardDepth = 100;
			lSched.StandardWidth = 100;
			lSched.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			collSchedules.AddItem(lSched);
			collSchedules.MoveLast();
			boolDataChanged = true;
			ShowSchedule();
			lSched.IsUpdated = true;
		}

		private void mnuDeleteSchedule_Click(object sender, System.EventArgs e)
		{
			DeleteCurrentSchedule();
		}

		private void DeleteCurrentSchedule()
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (collSchedules.IsCurrent())
				{
					cLandSchedule lSched;
					lSched = (cLandSchedule)collSchedules.GetCurrentItem();
					if (lSched.ID > 0)
					{
						if (MessageBox.Show("Are you sure you want to delete this schedule?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
						{
							modGlobalFunctions.AddCYAEntry_8("RE", "Deleted Schedule " + FCConvert.ToString(lSched.ScheduleNumber));
						}
						else
						{
							return;
						}
					}
					contLandSchedules.DeleteLandSchedule(lSched.ID);
					if (contLandSchedules.HadError)
					{
						Information.Err().Raise(contLandSchedules.LastErrorNumber, null, contLandSchedules.LastErrorMessage, null, null);
					}
					collSchedules.RemoveCurrent();
					ShowSchedule();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In DeleteCurrentSchedule", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuNext_Click(object sender, System.EventArgs e)
		{
			NextSchedule();
		}

		private void NextSchedule()
		{
			Grid.Row = 0;
			Grid.Col = 0;
			GridStandard.Row = 0;
			Grid.Col = 0;
			gridTownCode.Row = -1;
			//Application.DoEvents();
			collSchedules.MoveNext();
			if (!collSchedules.IsCurrent())
			{
				collSchedules.MoveLast();
			}
			ShowSchedule();
		}

		private void mnuPrevious_Click(object sender, System.EventArgs e)
		{
			PreviousSchedule();
		}

		private void PreviousSchedule()
		{
			Grid.Row = 0;
			Grid.Col = 0;
			GridStandard.Row = 0;
			Grid.Col = 0;
			gridTownCode.Row = -1;
			//Application.DoEvents();
			collSchedules.MovePrevious();
			if (!collSchedules.IsCurrent())
			{
				collSchedules.MoveFirst();
			}
			ShowSchedule();
		}

		private void mnuPrintPreviewAll_Click(object sender, System.EventArgs e)
		{
			rptLandSchedule.InstancePtr.Init(0);
		}

		private void mnuPrintPreviewCurrent_Click(object sender, System.EventArgs e)
		{
			if (collSchedules.IsCurrent())
			{
				cLandSchedule lSched;
				lSched = (cLandSchedule)collSchedules.GetCurrentItem();
				if (lSched.ScheduleNumber > 0)
				{
					rptLandSchedule.InstancePtr.Init(lSched.ScheduleNumber);
				}
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			Grid.Col = 0;
			//Application.DoEvents();
			int lngCurrentIndex;
			lngCurrentIndex = collSchedules.GetCurrentIndex();
			SaveData();
			cLandSchedule lSched;
			lSched = (cLandSchedule)collSchedules.GetItemByIndex(lngCurrentIndex);
			if (!collSchedules.IsCurrent())
			{
				collSchedules.MoveFirst();
			}
			ShowSchedule();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			Grid.Col = 0;
			//Application.DoEvents();
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.TextMatrix(0, CNSTCOLCODE, "Code");
			Grid.TextMatrix(0, CNSTCOLTYPE, "Type");
			Grid.TextMatrix(0, CNSTCOLDESCRIPTION, "Description");
			Grid.TextMatrix(0, CNSTCOLAMOUNT, "Amount");
			Grid.TextMatrix(0, CNSTCOLEXP1, "<= Std Lot/Depth");
			Grid.TextMatrix(0, CNSTCOLEXP2, "> Std Lot/Depth");
			Grid.TextMatrix(0, CNSTCOLWidthEXP1, "<= Std Width");
			Grid.TextMatrix(0, CNSTCOLWidthEXP2, "> Std Width");
			Grid.ColHidden(CNSTCOLAUTOID, true);
			GridStandard.ColHidden(CNSTCOLSTDCODE, true);
			GridStandard.TextMatrix(CNSTROWSTDDEPTH, CNSTCOLSTDDESC, "Standard Depth");
			GridStandard.TextMatrix(CNSTROWSTDLOT, CNSTCOLSTDDESC, "Standard Lot");
			GridStandard.TextMatrix(CNSTROWSTDWIDTH, CNSTCOLSTDDESC, "Standard Width");
			GridStandard.TextMatrix(CNSTROWSTDDEPTH, CNSTCOLSTDCODE, FCConvert.ToString(modREConstants.CNSTLANDSCHEDULESTANDARDDEPTH));
			GridStandard.TextMatrix(CNSTROWSTDLOT, CNSTCOLSTDCODE, FCConvert.ToString(modREConstants.CNSTLANDSCHEDULESTANDARDLOT));
			GridStandard.TextMatrix(CNSTROWSTDWIDTH, CNSTCOLSTDCODE, FCConvert.ToString(modREConstants.CNSTLANDSCHEDULESTANDARDWIDTH));

			//FC:FINAL:CHN - issue 1669: Change Column alignment.
			Grid.ColAlignment(CNSTCOLAMOUNT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTCOLEXP1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTCOLEXP2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTCOLWidthEXP1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTCOLWidthEXP2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ClearSchedule()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				Grid.Rows = 1;
				txtDescription.Text = "";
				rsLoad.OpenRecordset("select * from landtype order by code", modGlobalVariables.strREDatabase);
				while (!rsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTCOLAUTOID, FCConvert.ToString(0));
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTCOLCODE, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("code"))));
					Grid.TextMatrix(lngRow, CNSTCOLAMOUNT, "0.00");
					Grid.TextMatrix(lngRow, CNSTCOLEXP1, FCConvert.ToString(0.5));
					Grid.TextMatrix(lngRow, CNSTCOLEXP2, FCConvert.ToString(0.5));
					Grid.TextMatrix(lngRow, CNSTCOLWidthEXP1, FCConvert.ToString(0.5));
					Grid.TextMatrix(lngRow, CNSTCOLWidthEXP2, FCConvert.ToString(0.5));
					Grid.TextMatrix(lngRow, CNSTCOLDESCRIPTION, FCConvert.ToString(rsLoad.Get_Fields_String("description")));
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					if (Conversion.Val(rsLoad.Get_Fields("type")) == modREConstants.CNSTLANDTYPEACRES)
					{
						Grid.TextMatrix(lngRow, CNSTCOLTYPE, "Acres");
					}
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					else if (Conversion.Val(rsLoad.Get_Fields("type")) == modREConstants.CNSTLANDTYPEFRACTIONALACREAGE)
					{
						Grid.TextMatrix(lngRow, CNSTCOLTYPE, "Fractional Acres");
					}
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					else if (Conversion.Val(rsLoad.Get_Fields("type")) == modREConstants.CNSTLANDTYPEFRONTFOOT)
					{
						Grid.TextMatrix(lngRow, CNSTCOLTYPE, "Front Foot");
					}
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					else if (Conversion.Val(rsLoad.Get_Fields("type")) == modREConstants.CNSTLANDTYPESITE)
					{
						Grid.TextMatrix(lngRow, CNSTCOLTYPE, "Site");
					}
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					else if (Conversion.Val(rsLoad.Get_Fields("type")) == modREConstants.CNSTLANDTYPEIMPROVEMENTS)
					{
						Grid.TextMatrix(lngRow, CNSTCOLTYPE, "Improvements");
					}
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					else if (Conversion.Val(rsLoad.Get_Fields("type")) == modREConstants.CNSTLANDTYPELINEARFEET)
					{
						Grid.TextMatrix(lngRow, CNSTCOLTYPE, "Linear Feet");
					}
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					else if (Conversion.Val(rsLoad.Get_Fields("type")) == modREConstants.CNSTLANDTYPESQUAREFEET)
					{
						Grid.TextMatrix(lngRow, CNSTCOLTYPE, "Square Feet");
					}
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					else if (Conversion.Val(rsLoad.Get_Fields("type")) == modREConstants.CNSTLANDTYPEFRONTFOOTSCALED)
					{
						Grid.TextMatrix(lngRow, CNSTCOLTYPE, "Front Foot - Width");
					}
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					else if (Conversion.Val(rsLoad.Get_Fields("type")) == modREConstants.CNSTLANDTYPELINEARFEETSCALED)
					{
						Grid.TextMatrix(lngRow, CNSTCOLTYPE, "Linear Feet - Width");
					}
					Grid.RowData(lngRow, true);
					rsLoad.MoveNext();
				}
				GridStandard.TextMatrix(CNSTROWSTDDEPTH, CNSTCOLSTDDESC, "Standard Depth");
				GridStandard.TextMatrix(CNSTROWSTDDEPTH, CNSTCOLSTDAMOUNT, "1.00");
				GridStandard.TextMatrix(CNSTROWSTDDEPTH, CNSTCOLSTDCODE, FCConvert.ToString(modREConstants.CNSTLANDSCHEDULESTANDARDDEPTH));
				GridStandard.TextMatrix(CNSTROWSTDLOT, CNSTCOLSTDDESC, "Standard Lot");
				GridStandard.TextMatrix(CNSTROWSTDLOT, CNSTCOLSTDAMOUNT, "1.00");
				GridStandard.TextMatrix(CNSTROWSTDLOT, CNSTCOLSTDCODE, FCConvert.ToString(modREConstants.CNSTLANDSCHEDULESTANDARDLOT));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ClearSchedule", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowSchedule()
		{
			try
			{
				// On Error GoTo ErrorHandler
				boolIsUpdating = true;
				ClearSchedule();
				cLandSchedule lSched;
				int lngRow;
				if (collSchedules.IsCurrent())
				{
					lSched = (cLandSchedule)collSchedules.GetCurrentItem();
					txtDescription.Text = lSched.Description;
					if (lSched.ScheduleNumber > 0)
					{
						lblSchedule.Text = FCConvert.ToString(lSched.ScheduleNumber);
					}
					else
					{
						lblSchedule.Text = "";
					}
					gridTownCode.TextMatrix(0, 0, FCConvert.ToString(lSched.TownCode));
					GridStandard.TextMatrix(CNSTROWSTDDEPTH, CNSTCOLSTDAMOUNT, Strings.Format(lSched.StandardDepth, "0.00"));
					GridStandard.TextMatrix(CNSTROWSTDLOT, CNSTCOLSTDAMOUNT, Strings.Format(lSched.StandardLot, "0.00"));
					GridStandard.TextMatrix(CNSTROWSTDWIDTH, CNSTCOLSTDAMOUNT, Strings.Format(lSched.StandardWidth, "0.00"));
					cLandScheduleRecord schedDetail;
					lSched.Details.MoveFirst();
					while (lSched.Details.IsCurrent())
					{
						schedDetail = (cLandScheduleRecord)lSched.Details.GetCurrentItem();
						for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
						{
							if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTCOLCODE)) == schedDetail.LandTypeCode)
							{
								Grid.TextMatrix(lngRow, CNSTCOLAUTOID, FCConvert.ToString(schedDetail.ID));
								Grid.TextMatrix(lngRow, CNSTCOLCODE, FCConvert.ToString(schedDetail.LandTypeCode));
								Grid.TextMatrix(lngRow, CNSTCOLEXP1, Strings.Format(schedDetail.Exponent1, "0.00"));
								Grid.TextMatrix(lngRow, CNSTCOLEXP2, Strings.Format(schedDetail.Exponent2, "0.00"));
								Grid.TextMatrix(lngRow, CNSTCOLWidthEXP1, Strings.Format(schedDetail.WidthExponent1, "0.00"));
								Grid.TextMatrix(lngRow, CNSTCOLWidthEXP2, Strings.Format(schedDetail.WidthExponent2, "0.00"));
								Grid.TextMatrix(lngRow, CNSTCOLAMOUNT, Strings.Format(schedDetail.Amount, "0.00"));
								break;
							}
						}
						// lngRow
						lSched.Details.MoveNext();
					}
				}
				boolIsUpdating = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				boolIsUpdating = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ShowSchedule", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngSchedule As int	OnWriteFCConvert.ToDouble(
		private void ShowScheduleByNumber(int lngSchedule)
		{
			int lngCurrentIndex = 0;
			if (lngSchedule < 1)
				return;
			if (collSchedules.IsCurrent())
			{
				cLandSchedule lSched;
				lngCurrentIndex = collSchedules.GetCurrentIndex();
				collSchedules.MoveFirst();
				while (collSchedules.IsCurrent())
				{
					lSched = (cLandSchedule)collSchedules.GetCurrentItem();
					if (lSched.ScheduleNumber == lngSchedule)
					{
						ShowSchedule();
						return;
					}
					collSchedules.MoveNext();
				}
				collSchedules.GetItemByIndex(FCConvert.ToInt16(lngCurrentIndex));
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTCOLCODE, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTCOLDESCRIPTION, FCConvert.ToInt32(0.2 * GridWidth));
			Grid.ColWidth(CNSTCOLTYPE, FCConvert.ToInt32(0.16 * GridWidth));
			Grid.ColWidth(CNSTCOLAMOUNT, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(CNSTCOLEXP1, FCConvert.ToInt32(0.12 * GridWidth));
			Grid.ColWidth(CNSTCOLEXP2, FCConvert.ToInt32(0.12 * GridWidth));
			Grid.ColWidth(CNSTCOLWidthEXP1, FCConvert.ToInt32(0.1 * GridWidth));
			GridWidth = GridStandard.WidthOriginal;
			GridStandard.ColWidth(CNSTCOLSTDDESC, FCConvert.ToInt32(0.8 * GridWidth));
			//FC:FINAL:AAKV anchored in design
			//GridStandard.Height = 3 * GridStandard.RowHeight(0) + 60;
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				SaveData = false;
				modGlobalVariables.Statics.boolLandSChanged = true;
				Grid.Row = 0;
				Grid.Col = 0;
				GridStandard.Row = 0;
				Grid.Col = 0;
				gridTownCode.Row = -1;
				//Application.DoEvents();
				contLandSchedules.SaveLandSchedules(ref collSchedules);
				if (contLandSchedules.HadError)
				{
					Information.Err().Raise(contLandSchedules.LastErrorNumber, null, contLandSchedules.LastErrorMessage, null, null);
				}
				boolDataChanged = false;
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveData = true;
				modGlobalRoutines.LoadCustomizedInfo();
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}

		private void txtDescription_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolIsUpdating)
			{
				if (collSchedules.IsCurrent())
				{
					cLandSchedule lSched;
					lSched = (cLandSchedule)collSchedules.GetCurrentItem();
					lSched.Description = txtDescription.Text;
					boolDataChanged = true;
				}
			}
		}
	}
}
