﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for dlgSalesAnalysis.
	/// </summary>
	partial class dlgSalesAnalysis : BaseForm
	{
		public fecherFoundation.FCComboBox cmbval;
		public fecherFoundation.FCLabel lblval;
		public fecherFoundation.FCComboBox cmbmean;
		public fecherFoundation.FCLabel lblmean;
		public fecherFoundation.FCComboBox cmbValidity;
		public fecherFoundation.FCTextBox txtPercent;
		public fecherFoundation.FCButton cmdOkay;
		public fecherFoundation.FCFrame frame1;
		public fecherFoundation.FCTextBox txtBldgFactor;
		public fecherFoundation.FCTextBox txtLandFactor;
		public fecherFoundation.FCTextBox txtSaleFactor;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label4;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dlgSalesAnalysis));
			this.cmbval = new fecherFoundation.FCComboBox();
			this.lblval = new fecherFoundation.FCLabel();
			this.cmbmean = new fecherFoundation.FCComboBox();
			this.lblmean = new fecherFoundation.FCLabel();
			this.cmbValidity = new fecherFoundation.FCComboBox();
			this.txtPercent = new fecherFoundation.FCTextBox();
			this.cmdOkay = new fecherFoundation.FCButton();
			this.frame1 = new fecherFoundation.FCFrame();
			this.txtBldgFactor = new fecherFoundation.FCTextBox();
			this.txtLandFactor = new fecherFoundation.FCTextBox();
			this.txtSaleFactor = new fecherFoundation.FCTextBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.lblPercent = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOkay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frame1)).BeginInit();
			this.frame1.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdOkay);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblPercent);
			this.ClientArea.Controls.Add(this.txtPercent);
			this.ClientArea.Controls.Add(this.cmbValidity);
			this.ClientArea.Controls.Add(this.cmbval);
			this.ClientArea.Controls.Add(this.lblval);
			this.ClientArea.Controls.Add(this.cmbmean);
			this.ClientArea.Controls.Add(this.lblmean);
			this.ClientArea.Controls.Add(this.frame1);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(172, 30);
			this.HeaderText.Text = "Sales Analysis";
			// 
			// cmbval
			// 
			this.cmbval.AutoSize = false;
			this.cmbval.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbval.FormattingEnabled = true;
			this.cmbval.Items.AddRange(new object[] {
				"Billing",
				"Correlated"
			});
			this.cmbval.Location = new System.Drawing.Point(299, 420);
			this.cmbval.Name = "cmbval";
			this.cmbval.Size = new System.Drawing.Size(197, 40);
			this.cmbval.TabIndex = 25;
			this.cmbval.Text = "Correlated";
			// 
			// lblval
			// 
			this.lblval.AutoSize = true;
			this.lblval.Location = new System.Drawing.Point(30, 434);
			this.lblval.Name = "lblval";
			this.lblval.Size = new System.Drawing.Size(117, 15);
			this.lblval.TabIndex = 24;
			this.lblval.Text = "VALUATION FROM";
			// 
			// cmbmean
			// 
			this.cmbmean.AutoSize = false;
			this.cmbmean.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbmean.FormattingEnabled = true;
			this.cmbmean.Items.AddRange(new object[] {
				"Mean",
				"Median",
				"Mid-Quartile Mean",
				"Weighted Mean"
			});
			this.cmbmean.Location = new System.Drawing.Point(299, 360);
			this.cmbmean.Name = "cmbmean";
			this.cmbmean.Size = new System.Drawing.Size(197, 40);
			this.cmbmean.TabIndex = 23;
			this.cmbmean.Text = "Mean";
			// 
			// lblmean
			// 
			this.lblmean.AutoSize = true;
			this.lblmean.Location = new System.Drawing.Point(30, 374);
			this.lblmean.Name = "lblmean";
			this.lblmean.Size = new System.Drawing.Size(33, 15);
			this.lblmean.TabIndex = 26;
			this.lblmean.Text = "USE";
			this.lblmean.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbValidity
			// 
			this.cmbValidity.AutoSize = false;
			this.cmbValidity.BackColor = System.Drawing.SystemColors.Window;
			this.cmbValidity.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbValidity.FormattingEnabled = true;
			this.cmbValidity.Items.AddRange(new object[] {
				"All",
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"7",
				"8",
				"9"
			});
			this.cmbValidity.Location = new System.Drawing.Point(299, 300);
			this.cmbValidity.Name = "cmbValidity";
			this.cmbValidity.Size = new System.Drawing.Size(197, 40);
			this.cmbValidity.TabIndex = 22;
			// 
			// txtPercent
			// 
			this.txtPercent.AutoSize = false;
			this.txtPercent.BackColor = System.Drawing.SystemColors.Window;
			this.txtPercent.LinkItem = null;
			this.txtPercent.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPercent.LinkTopic = null;
			this.txtPercent.Location = new System.Drawing.Point(299, 240);
			this.txtPercent.Name = "txtPercent";
			this.txtPercent.Size = new System.Drawing.Size(61, 40);
			this.txtPercent.TabIndex = 17;
			this.txtPercent.Text = "15";
			this.txtPercent.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPercent_KeyPress);
			// 
			// cmdOkay
			// 
			this.cmdOkay.AppearanceKey = "acceptButton";
			this.cmdOkay.Location = new System.Drawing.Point(460, 30);
			this.cmdOkay.Name = "cmdOkay";
			this.cmdOkay.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdOkay.Size = new System.Drawing.Size(100, 48);
			this.cmdOkay.TabIndex = 15;
			this.cmdOkay.Text = "Continue";
			this.cmdOkay.Click += new System.EventHandler(this.cmdOkay_Click);
			// 
			// frame1
			// 
			this.frame1.Controls.Add(this.txtBldgFactor);
			this.frame1.Controls.Add(this.txtLandFactor);
			this.frame1.Controls.Add(this.txtSaleFactor);
			this.frame1.Controls.Add(this.Label3);
			this.frame1.Controls.Add(this.Label2);
			this.frame1.Controls.Add(this.Label1);
			this.frame1.Location = new System.Drawing.Point(30, 30);
			this.frame1.Name = "frame1";
			this.frame1.Size = new System.Drawing.Size(249, 190);
			this.frame1.TabIndex = 0;
			this.frame1.Text = "Trends";
			// 
			// txtBldgFactor
			// 
			this.txtBldgFactor.AutoSize = false;
			this.txtBldgFactor.BackColor = System.Drawing.SystemColors.Window;
			this.txtBldgFactor.LinkItem = null;
			this.txtBldgFactor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtBldgFactor.LinkTopic = null;
			this.txtBldgFactor.Location = new System.Drawing.Point(166, 130);
			this.txtBldgFactor.Name = "txtBldgFactor";
			this.txtBldgFactor.Size = new System.Drawing.Size(63, 40);
			this.txtBldgFactor.TabIndex = 3;
			this.txtBldgFactor.Text = "100";
			this.txtBldgFactor.Validating += new System.ComponentModel.CancelEventHandler(this.txtBldgFactor_Validating);
			// 
			// txtLandFactor
			// 
			this.txtLandFactor.AutoSize = false;
			this.txtLandFactor.BackColor = System.Drawing.SystemColors.Window;
			this.txtLandFactor.LinkItem = null;
			this.txtLandFactor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLandFactor.LinkTopic = null;
			this.txtLandFactor.Location = new System.Drawing.Point(166, 80);
			this.txtLandFactor.Name = "txtLandFactor";
			this.txtLandFactor.Size = new System.Drawing.Size(63, 40);
			this.txtLandFactor.TabIndex = 2;
			this.txtLandFactor.Text = "100";
			this.txtLandFactor.Validating += new System.ComponentModel.CancelEventHandler(this.txtLandFactor_Validating);
			// 
			// txtSaleFactor
			// 
			this.txtSaleFactor.AutoSize = false;
			this.txtSaleFactor.BackColor = System.Drawing.SystemColors.Window;
			this.txtSaleFactor.LinkItem = null;
			this.txtSaleFactor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSaleFactor.LinkTopic = null;
			this.txtSaleFactor.Location = new System.Drawing.Point(166, 30);
			this.txtSaleFactor.Name = "txtSaleFactor";
			this.txtSaleFactor.Size = new System.Drawing.Size(63, 40);
			this.txtSaleFactor.TabIndex = 1;
			this.txtSaleFactor.Text = "0";
			this.txtSaleFactor.Validating += new System.ComponentModel.CancelEventHandler(this.txtSaleFactor_Validating);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 144);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(102, 22);
			this.Label3.TabIndex = 9;
			this.Label3.Text = "BLDG FACTOR";
			// 
			// Label2
			// 
			this.Label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label2.Location = new System.Drawing.Point(20, 94);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(102, 24);
			this.Label2.TabIndex = 8;
			this.Label2.Text = "LAND FACTOR";
			// 
			// Label1
			// 
			this.Label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(100, 24);
			this.Label1.TabIndex = 7;
			this.Label1.Text = "SALE FACTOR";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 314);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(107, 16);
			this.Label4.TabIndex = 23;
			this.Label4.Text = "VALIDITY CODES";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuContinue
			// 
			this.mnuContinue.Index = 0;
			this.mnuContinue.Name = "mnuContinue";
			this.mnuContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuContinue.Text = "Continue";
			this.mnuContinue.Click += new System.EventHandler(this.mnuContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// lblPercent
			// 
			this.lblPercent.Location = new System.Drawing.Point(30, 254);
			this.lblPercent.Name = "lblPercent";
			this.lblPercent.Size = new System.Drawing.Size(218, 16);
			this.lblPercent.TabIndex = 27;
			this.lblPercent.Text = "PERCENT FOR MID-QUARTILE MEAN";
			this.lblPercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// dlgSalesAnalysis
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "dlgSalesAnalysis";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Sales Analysis";
			this.Load += new System.EventHandler(this.dlgSalesAnalysis_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.dlgSalesAnalysis_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOkay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frame1)).EndInit();
			this.frame1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCLabel lblPercent;
	}
}
