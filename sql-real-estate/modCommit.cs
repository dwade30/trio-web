﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class modCommit
	{
		//=========================================================
		public static bool CreateCommitDatabase()
		{
			bool CreateCommitDatabase = false;
			try
			{
				// Dim clsSave As New clsDRWrapper
				// Dim DB As DAO.Database
				// Dim fso As New FCFileSystemObject
				// 
				// On Error GoTo ErrorHandler
				// 
				// CreateCommitDatabase = False
				// If Not File.Exists(strCommitDB) Then
				// Call DBEngine.CreateDatabase(strCommitDB, dbLangGeneral, dbVersion40)
				// Set DB = DBEngine.OpenDatabase(strCommitDB, True, False)
				// Call DB.NewPassword("", DATABASEPASSWORD)
				CreateCommitDatabase = true;
				// End If
				return CreateCommitDatabase;
			}
			catch (Exception ex)
			{
				//ErrorHandler:;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CreateCommitDatabase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateCommitDatabase;
		}
		// Public Sub CreateCommitTables()
		// adds the correct tables to the commit database
		// Dim clsSave As New clsDRWrapper
		// Dim rsLoad As clsDRWrapper
		// Dim DB As DAO.Database
		// Dim fld As DAO.Field
		// Dim tbl As DAO.TableDef
		//
		// On Error GoTo ErrorHandler
		//
		// Set DB = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
		//
		//
		// If Not clsSave.UpdateDatabaseTable("Master", strCommitDB) Then
		// If clsSave.CreateNewDatabaseTable("Master", strCommitDB) Then
		// With clsSave
		// Call .AddTableField("CommitmentYear", dbLong)
		//
		// For Each fld In DB.TableDefs("Master").Fields
		// Call .AddTableField(fld.Name, fld.Type, fld.Size)
		// Call .SetFieldDefaultValue(fld.Name, fld.DefaultValue)
		// Next fld
		//
		// End With
		// clsSave.UpdateTableCreation
		// Call clsSave.SetAllTextFieldsToAllowNull("Master", strCommitDB)
		// End If
		// End If
		//
		//
		//
		// If Not clsSave.UpdateDatabaseTable("Outbuilding", strCommitDB) Then
		// If clsSave.CreateNewDatabaseTable("Outbuilding", strCommitDB) Then
		// With clsSave
		// Call .AddTableField("id", dbLong)
		// Call .SetFieldAttribute("id", dbAutoIncrField)
		// Call .AddTableField("CommitmentYear", dbLong)
		// For Each fld In DB.TableDefs("Outbuilding").Fields
		// Call .AddTableField(fld.Name, fld.Type, fld.Size)
		// Call .SetFieldDefaultValue(fld.Name, fld.DefaultValue)
		// Next fld
		// End With
		// clsSave.UpdateTableCreation
		// Call clsSave.SetAllTextFieldsToAllowNull("Outbuilding", strCommitDB)
		// End If
		// End If
		//
		// If Not clsSave.UpdateDatabaseTable("Commercial", strCommitDB) Then
		// If clsSave.CreateNewDatabaseTable("Commercial", strCommitDB) Then
		// With clsSave
		// Call .AddTableField("id", dbLong)
		// Call .SetFieldAttribute("id", dbAutoIncrField)
		// Call .AddTableField("CommitmentYear", dbLong)
		// For Each fld In DB.TableDefs("Commercial").Fields
		// Call .AddTableField(fld.Name, fld.Type, fld.Size)
		// Call .SetFieldDefaultValue(fld.Name, fld.DefaultValue)
		// Next fld
		// End With
		// clsSave.UpdateTableCreation
		// Call clsSave.SetAllTextFieldsToAllowNull("Commercial", strCommitDB)
		// End If
		// End If
		//
		// If Not clsSave.UpdateDatabaseTable("Dwelling", strCommitDB) Then
		// If clsSave.CreateNewDatabaseTable("Dwelling", strCommitDB) Then
		// With clsSave
		// Call .AddTableField("id", dbLong)
		// Call .SetFieldAttribute("id", dbAutoIncrField)
		// Call .AddTableField("CommitmentYear", dbLong)
		// For Each fld In DB.TableDefs("Dwelling").Fields
		// Call .AddTableField(fld.Name, fld.Type, fld.Size)
		// Call .SetFieldDefaultValue(fld.Name, fld.DefaultValue)
		// Next fld
		// End With
		// clsSave.UpdateTableCreation
		// Call clsSave.SetAllTextFieldsToAllowNull("Dwelling", strCommitDB)
		// End If
		// End If
		//
		// If Not clsSave.UpdateDatabaseTable("SummRecord", strCommitDB) Then
		// If clsSave.CreateNewDatabaseTable("SummRecord", strCommitDB) Then
		// With clsSave
		// Call .AddTableField("CommitmentYear", dbLong)
		// Call .AddTableField("id", dbLong)
		// Call .SetFieldAttribute("id", dbAutoIncrField)
		// For Each fld In DB.TableDefs("SummRecord").Fields
		// Call .AddTableField(fld.Name, fld.Type, fld.Size)
		// Call .SetFieldDefaultValue(fld.Name, fld.DefaultValue)
		// Next fld
		// End With
		// clsSave.UpdateTableCreation
		// Call clsSave.SetAllTextFieldsToAllowNull("SummRecord", strCommitDB)
		// End If
		// End If
		//
		// If Not clsSave.UpdateDatabaseTable("Customize", strCommitDB) Then
		// If clsSave.CreateNewDatabaseTable("Customize", strCommitDB) Then
		// With clsSave
		// Call .AddTableField("CommitmentYear", dbLong)
		// Call .AddTableField("CertifiedRatio", dbDouble)
		// End With
		// clsSave.UpdateTableCreation
		// End If
		// End If
		// Exit Sub
		// ErrorHandler:
		// Screen.MousePointer = vbDefault
		// MsgBox "Error Number " & Err.Number & "  " & Err.Description & vbNewLine & "In CreateCommitTables", vbCritical, "Error"
		// End Sub
		// vbPorter upgrade warning: lngYear As int	OnWriteFCConvert.ToDouble(
		public static void FillCommitmentRecords_8(int lngYear, string strDBFullPath)
		{
			FillCommitmentRecords(ref lngYear, ref strDBFullPath);
		}

		public static void FillCommitmentRecords(ref int lngYear, ref string strDBFullPath)
		{
			// takes info from strDBFullPath and creates commitment entries for the year lngyear
			string strPath = "";
			string strDB = "";
			int intPlace = 0;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//Application.DoEvents();
				// Call frmWait.Init("Please Wait. Copying Data", True)
				if (Strings.InStr(1, strDBFullPath, "\\", CompareConstants.vbTextCompare) > 0)
				{
					// split the path from the db
					intPlace = Strings.InStrRev(strDBFullPath, "\\", -1, CompareConstants.vbTextCompare);
					strDB = Strings.Mid(strDBFullPath, intPlace + 1);
					strPath = Strings.Mid(strDBFullPath, 1, intPlace);
				}
				else
				{
					strPath = "";
					strDB = strDBFullPath;
				}
				clsSave.Execute("delete from master where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				clsSave.Execute("delete from dwelling where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				clsSave.Execute("delete from Outbuilding where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				clsSave.Execute("delete from commercial where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				clsSave.Execute("delete from customize where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				clsSave.Execute("DELETE FROM SUMMRECORD where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				// frmWait.prgProgress.Value = 1
				// frmWait.lblMessage = "Copying Account Information"
				// frmWait.prgProgress.Refresh
				// frmWait.lblMessage.Refresh
				if (!FillCommitMasterAcct(ref strPath, ref strDB, ref lngYear))
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("There was an error trying to convert Master Account information" + "\r\n" + "Cannot continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				// frmWait.IncrementProgress (16)
				// frmWait.lblMessage = "Copying Outbuilding Information"
				// frmWait.lblMessage.Refresh
				if (!FillCommitOutbuilding(ref strPath, ref strDB, ref lngYear))
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("There was an error trying to convert outbuilding information" + "\r\n" + "Cannot continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				// frmWait.IncrementProgress (17)
				// frmWait.lblMessage = "Copying Dwelling Information"
				// frmWait.lblMessage.Refresh
				if (!FillCommitDwell(ref strPath, ref strDB, ref lngYear))
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("There was an error trying to convert Dwelling information" + "\r\n" + "Cannot Continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				// frmWait.IncrementProgress (17)
				// frmWait.lblMessage = "Copying Commercial Information"
				// frmWait.lblMessage.Refresh
				if (!FillCommitCommercial(ref strPath, ref strDB, ref lngYear))
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("There was an error trying to convert Commercial information" + "\r\n" + "Cannot Continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				// frmWait.IncrementProgress (17)
				// frmWait.lblMessage = "Copying Customize Information"
				// frmWait.lblMessage.Refresh
				if (!FillCommitCustomize(ref strPath, ref strDB, ref lngYear))
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("There was an error trying to convert Customize information" + "\r\n" + "Cannot Continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				// frmWait.IncrementProgress (17)
				// frmWait.lblMessage = "Copying Summary Information"
				// frmWait.lblMessage.Refresh
				if (!FillCommitSummary(ref strPath, ref strDB, ref lngYear))
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("There was an error trying to convert Summary information" + "\r\n" + "Cannot Continue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				// Unload frmWait
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Data Imported", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				// Unload frmWait
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillCommitmentRecords", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static bool FillCommitMasterAcct(ref string strPath, ref string strDB, ref int lngYear)
		{
			bool FillCommitMasterAcct = false;
			//const int curOnErrorGoToLabel_Default = 0;
			//const int curOnErrorGoToLabel_ErrorHandler = 1;
			//const int curOnErrorGoToLabel_ErrorHandler = 2;
			//int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsSave = new clsDRWrapper();
				int x;
				//vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
				FillCommitMasterAcct = false;
				// If strPath <> vbNullString Then
				// clsLoad.Path = strPath
				// End If
				// 
				clsLoad.OpenRecordset("select * from master  where not rsdeleted = 1 order by rsaccount,rscard", "TWRE0000.vb1");
				clsSave.OpenRecordset("select * from master where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				while (!clsLoad.EndOfFile())
				{
					clsSave.AddNew();
					clsSave.Set_Fields("CommitmentYear", lngYear);
					for (x = 0; x <= clsLoad.FieldsCount - 1; x++)
					{
						if (Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "ID" && Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "CARDID" && Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "ACCOUNTID")
						{
							/*? On Error Resume Next  */
							clsSave.Set_Fields(clsLoad.Get_FieldsIndexName(x), clsLoad.Get_FieldsIndexValue(x));
							//vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
						}
					}
					// x
					clsSave.Update();
					clsLoad.MoveNext();
				}
				FillCommitMasterAcct = true;
				return FillCommitMasterAcct;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillCommitMasterAcct", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillCommitMasterAcct;
		}

		private static bool FillCommitOutbuilding(ref string strPath, ref string strDB, ref int lngYear)
		{
			bool FillCommitOutbuilding = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				FillCommitOutbuilding = false;
				// If strPath <> vbNullString Then
				// clsLoad.Path = strPath
				// End If
				clsLoad.OpenRecordset("select * from outbuilding order by rsaccount,rscard", "twre0000.vb1");
				clsSave.OpenRecordset("select * from outbuilding where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				while (!clsLoad.EndOfFile())
				{
					clsSave.AddNew();
					clsSave.Set_Fields("commitmentyear", lngYear);
					for (x = 0; x <= clsLoad.FieldsCount - 1; x++)
					{
						if (Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "ID" && Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "CARDID" && Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "ACCOUNTID")
						{
							clsSave.Set_Fields(clsLoad.Get_FieldsIndexName(x), clsLoad.Get_FieldsIndexValue(x));
						}
					}
					// x
					clsSave.Update();
					clsLoad.MoveNext();
				}
				FillCommitOutbuilding = true;
				return FillCommitOutbuilding;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillCommitOutbuilding", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillCommitOutbuilding;
		}

		private static bool FillCommitDwell(ref string strPath, ref string strDB, ref int lngYear)
		{
			bool FillCommitDwell = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsSave = new clsDRWrapper();
				clsDRWrapper clsTemp = new clsDRWrapper();
				int x;
				FillCommitDwell = false;
				// If strPath <> vbNullString Then
				// clsLoad.Path = strPath
				// End If
				clsLoad.OpenRecordset("select * from dwelling order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				clsSave.OpenRecordset("select * from dwelling where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				while (!clsLoad.EndOfFile())
				{
					clsSave.AddNew();
					clsSave.Set_Fields("CommitmentYear", lngYear);
					for (x = 0; x <= clsLoad.FieldsCount - 1; x++)
					{
						if (Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "ID" && Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "CARDID" && Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "ACCOUNTID")
						{
							clsSave.Set_Fields(clsLoad.Get_FieldsIndexName(x), clsLoad.Get_FieldsIndexValue(x));
						}
					}
					// x
					clsSave.Update();
					clsLoad.MoveNext();
				}
				FillCommitDwell = true;
				return FillCommitDwell;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillCommitDwell", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillCommitDwell;
		}

		private static bool FillCommitCommercial(ref string strPath, ref string strDB, ref int lngYear)
		{
			bool FillCommitCommercial = false;
			//const int curOnErrorGoToLabel_Default = 0;
			//const int curOnErrorGoToLabel_ErrorHandler = 1;
			//const int curOnErrorGoToLabel_ErrorHandler = 2;
			//int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				//vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsSave = new clsDRWrapper();
				int x;
				clsDRWrapper clsTemp = new clsDRWrapper();
				FillCommitCommercial = false;
				// If strPath <> vbNullString Then
				// clsLoad.Path = strPath
				// End If
				clsLoad.OpenRecordset("select * from commercial order by rsaccount,rscard", modGlobalVariables.strREDatabase);
				clsSave.OpenRecordset("select * from commercial where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				/*? On Error Resume Next  */
				while (!clsLoad.EndOfFile())
				{
					clsSave.AddNew();
					clsSave.Set_Fields("CommitmentYear", lngYear);
					for (x = 0; x <= clsLoad.FieldsCount - 1; x++)
					{
						if (Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "ID" && Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "CARDID" && Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "ACCOUNTID")
						{
							clsSave.Set_Fields(clsLoad.Get_FieldsIndexName(x), clsLoad.Get_FieldsIndexValue(x));
						}
					}
					// x
					clsSave.Update();
					clsLoad.MoveNext();
				}
				//vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
				FillCommitCommercial = true;
				return FillCommitCommercial;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillCommitCommercial", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillCommitCommercial;
		}

		private static bool FillCommitCustomize(ref string strPath, ref string strDB, ref int lngYear)
		{
			bool FillCommitCustomize = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsSave = new clsDRWrapper();
				FillCommitCustomize = false;
				// If strPath <> vbNullString Then
				// clsLoad.Path = strPath
				// End If
				clsLoad.OpenRecordset("select * from customize", modGlobalVariables.strREDatabase);
				clsSave.OpenRecordset("select * from customize where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				clsSave.AddNew();
				clsSave.Set_Fields("CertifiedRatio", clsLoad.Get_Fields_Double("CertifiedRatio"));
				clsSave.Set_Fields("CommitmentYear", lngYear);
				clsSave.Update();
				FillCommitCustomize = true;
				return FillCommitCustomize;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillCommitCustomize", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillCommitCustomize;
		}

		private static bool FillCommitSummary(ref string strPath, ref string strDB, ref int lngYear)
		{
			bool FillCommitSummary = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsSave = new clsDRWrapper();
				int x;
				FillCommitSummary = false;
				// If strPath <> vbNullString Then
				// clsLoad.Path = strPath
				// End If
				clsLoad.OpenRecordset("select * from summrecORD", modGlobalVariables.strREDatabase);
				clsSave.OpenRecordset("select * from summrecord where commitmentyear = " + FCConvert.ToString(lngYear), modGlobalVariables.Statics.strCommitDB);
				while (!clsLoad.EndOfFile())
				{
					clsSave.AddNew();
					clsSave.Set_Fields("CommitmentYear", lngYear);
					for (x = 0; x <= clsLoad.FieldsCount - 1; x++)
					{
						if (Strings.UCase(clsLoad.Get_FieldsIndexName(x)) != "ID")
						{
							clsSave.Set_Fields(clsLoad.Get_FieldsIndexName(x), clsLoad.Get_FieldsIndexValue(x));
						}
					}
					// x
					clsSave.Update();
					clsLoad.MoveNext();
				}
				FillCommitSummary = true;
				return FillCommitSummary;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillCommitSummary", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillCommitSummary;
		}
	}
}
