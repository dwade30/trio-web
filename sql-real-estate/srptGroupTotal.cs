﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptGroupTotal.
	/// </summary>
	public partial class srptGroupTotal : FCSectionReport
	{
		public static srptGroupTotal InstancePtr
		{
			get
			{
				return (srptGroupTotal)Sys.GetInstance(typeof(srptGroupTotal));
			}
		}

		protected srptGroupTotal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsGroup?.Dispose();
				clsExtract?.Dispose();
                clsExtract = null;
                clsGroup = null;
                clCodeList = null;
            }
			base.Dispose(disposing);
		}

		public srptGroupTotal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptGroupTotal	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intGroupNumber;
		clsDRWrapper clsGroup = new clsDRWrapper();
		clsDRWrapper clsExtract = new clsDRWrapper();
		int intCurLine;
		int intRepNumber;
		// Dim clsDesc As New clsDRWrapper
		int intCode;
		clsReportCodes clCodeList = new clsReportCodes();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsGroup.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int lngUID;
			clCodeList.LoadCodes();
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			intRepNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData + "")));
			// Call clsGroup.OpenRecordset("select * from reporttable where reportnumber = " & intRepNumber & " and torp = 'T' order by linenumber", strREDatabase)
			clsGroup.OpenRecordset("select * from reporttable where reportnumber = " + FCConvert.ToString(intRepNumber) + "  order by linenumber", modGlobalVariables.strREDatabase);
			clsExtract.OpenRecordset("select * from extract where userid = " + FCConvert.ToString(lngUID) + " and reportnumber = " + FCConvert.ToString(intRepNumber), modGlobalVariables.strREDatabase);
			// Call clsDesc.OpenRecordset("select * from reportcodes order by code", strREDatabase)
			intGroupNumber = FCConvert.ToInt32(clsExtract.Get_Fields_Int32("groupnumber"));
			intCurLine = 1;
			intCurLine = FCConvert.ToInt32(clsGroup.Get_Fields_Int32("linenumber"));
			txtTotalRecords.Text = FCConvert.ToString(modGlobalVariables.Statics.reporttotals[intGroupNumber, 0]);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// On Error GoTo ErrorHandler
			if (FCConvert.ToString(clsGroup.Get_Fields_String("torp")) == "T")
			{
				Detail.Visible = true;
				intCurLine = FCConvert.ToInt32(clsGroup.Get_Fields_Int32("LINENUMBER"));
				if (modGlobalVariables.Statics.reporttotals[intGroupNumber, intCurLine] == modGlobalConstants.EleventyBillion)
				{
					txtTotal.Text = "OverFlow";
				}
				else
				{
					txtTotal.Text = Strings.Format(modGlobalVariables.Statics.reporttotals[intGroupNumber, intCurLine], "#,###,###,###.##");
					if (txtTotal.Text.Length > 0)
					{
						if (Strings.Right(txtTotal.Text, 1) == ".")
						{
							txtTotal.Text = Strings.Mid(txtTotal.Text, 1, txtTotal.Text.Length - 1);
						}
					}
					if (txtTotal.Text == "")
						txtTotal.Text = "0";
				}
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				intCode = FCConvert.ToInt32(clsGroup.Get_Fields("code"));
				clsReportParameter tCode;
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				tCode = clCodeList.GetCodeByCode(FCConvert.ToInt32(Conversion.Val(clsGroup.Get_Fields("code"))), FCConvert.ToInt32(Conversion.Val(clsGroup.Get_Fields_Int32("codeid"))));
				if (!(tCode == null))
				{
					// Call clsDesc.FindFirstRecord("code", intCode)
					// Call clsDesc.OpenRecordset("Select * from costrecord where crecordnumber = " & (1280 + intCode), strredatabase)
					if (intCode == 1)
					{
						txtCodeTitle.Text = "Records";
					}
					else
					{
						// txtCodeTitle.Text = clsDesc.Fields("description")
						txtCodeTitle.Text = tCode.Description;
					}
					clsGroup.MoveNext();
					if (!clsGroup.EndOfFile())
						intCurLine = FCConvert.ToInt32(clsGroup.Get_Fields_Int32("linenumber"));
				}
				else
				{
					Detail.Visible = false;
					clsGroup.MoveNext();
				}
				// intCurLine = intCurLine + 1
				// ErrorHandler:
			}
			else
			{
				Detail.Visible = false;
				clsGroup.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtTitle.Text = "Group " + FCConvert.ToString(intGroupNumber) + " " + clsExtract.Get_Fields_String("title");
		}

		
	}
}
