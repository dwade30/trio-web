﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewAccounts.
	/// </summary>
	public partial class rptNewAccounts : BaseSectionReport
	{
		public rptNewAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "New Accounts";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptNewAccounts InstancePtr
		{
			get
			{
				return (rptNewAccounts)Sys.GetInstance(typeof(rptNewAccounts));
			}
		}

		protected rptNewAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTCMBUPDATEAMOUNTS = 0;
		const int CNSTCMBUPDATEINFOONLY = 1;
		// goes through each row in gridbadaccounts and puts it in the report
		int lngCRow;
		double lngTotLand;
		double lngTotBldg;
		double lngTotExempt;
		double lngTotAssess;
		bool boolImportsOnly;
		int lngTotAccounts;
		bool boolInfoOnly;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool boolHidden = false;
			if (boolImportsOnly)
			{
				if (lngCRow < frmImportCompetitorXF.InstancePtr.GridNew.Rows)
				{
					if (frmImportCompetitorXF.InstancePtr.GridNew.RowHidden(lngCRow))
					{
						boolHidden = true;
					}
					else
					{
						boolHidden = false;
					}
					while (boolHidden && lngCRow < frmImportCompetitorXF.InstancePtr.GridNew.Rows)
					{
						if (frmImportCompetitorXF.InstancePtr.GridNew.RowHidden(lngCRow))
						{
							boolHidden = true;
						}
						else
						{
							boolHidden = false;
						}
						if (boolHidden)
						{
							lngCRow += 1;
						}
					}
				}
			}
			eArgs.EOF = lngCRow >= frmImportCompetitorXF.InstancePtr.GridNew.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngCRow = 1;
			lngTotAccounts = 0;
			if (frmImportCompetitorXF.InstancePtr.chkShowAddedOnly.CheckState == Wisej.Web.CheckState.Checked)
			{
				boolImportsOnly = true;
			}
			else
			{
				boolImportsOnly = false;
			}
			if (frmImportCompetitorXF.InstancePtr.cmbUpdateType.ItemData(frmImportCompetitorXF.InstancePtr.cmbUpdateType.SelectedIndex) == CNSTCMBUPDATEINFOONLY)
			{
				boolInfoOnly = true;
				Label6.Visible = false;
				Label8.Visible = false;
				Label9.Visible = false;
				Label10.Visible = false;
				Label11.Visible = false;
			}
			else
			{
				boolInfoOnly = false;
			}
			lngTotLand = 0;
			lngTotBldg = 0;
			lngTotExempt = 0;
			lngTotAssess = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngLand = 0;
			int lngBldg = 0;
			// vbPorter upgrade warning: lngExempt As int	OnWriteFCConvert.ToDouble(
			int lngExempt = 0;
			int lngAssess = 0;
			if (lngCRow < frmImportCompetitorXF.InstancePtr.GridNew.Rows)
			{
				txtMapLot.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTMAPLOT));
				txtName.Text = fecherFoundation.Strings.Trim(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTOWNER));
				lngLand = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTLANDVALUE))));
				lngBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTBLDGVALUE))));
				// If UCase(MuniName) = "SKOWHEGAN" Then
				lngExempt = FCConvert.ToInt32(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT1)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT2)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(lngCRow, modGridConstantsXF.CNSTEXEMPTAMOUNT3)));
				// Else
				// lngExempt = Val(.TextMatrix(lngCRow, CNSTORIGINALEXEMPTION))
				// End If
				lngAssess = lngLand + lngBldg - lngExempt;
				lngTotLand += lngLand;
				lngTotBldg += lngBldg;
				lngTotExempt += lngExempt;
				lngTotAssess += lngAssess;
				if (!boolInfoOnly)
				{
					txtExempt.Text = Strings.Format(lngExempt, "#,###,###,##0");
					txtLand.Text = Strings.Format(lngLand, "#,###,###,##0");
					txtBuilding.Text = Strings.Format(lngBldg, "#,###,###,##0");
					txtAssessment.Text = Strings.Format(lngAssess, "#,###,###,##0");
				}
				lngTotAccounts += 1;
			}
			else
			{
				txtMapLot.Text = "";
				txtName.Text = "";
				txtLand.Text = "";
				txtExempt.Text = "";
				txtBuilding.Text = "";
				txtAssessment.Text = "";
			}
			lngCRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (!boolInfoOnly)
			{
				txtTotalAssessment.Text = Strings.Format(lngTotAssess, "#,###,###,##0");
				txtTotalBuilding.Text = Strings.Format(lngTotBldg, "#,###,###,##0");
				txtTotalExempt.Text = Strings.Format(lngTotExempt, "#,###,###,##0");
				txtTotalLand.Text = Strings.Format(lngTotLand, "#,###,###,##0");
			}
			txtTotal.Text = Strings.Format(lngTotAccounts, "#,###,###,##0");
		}

	
	}
}
