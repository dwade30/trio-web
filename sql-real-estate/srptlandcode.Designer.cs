﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptlandcode.
	/// </summary>
	partial class srptlandcode
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptlandcode));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtland = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtbldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtexempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtexempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCode,
				this.txtCount,
				this.txtland,
				this.txtbldg,
				this.txtexempt,
				this.txttotal
			});
			this.Detail.Height = 0.21875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtCode
			// 
			this.txtCode.Height = 0.19F;
			this.txtCode.Left = 0F;
			this.txtCode.Name = "txtCode";
			this.txtCode.Style = "font-family: \'Tahoma\'";
			this.txtCode.Text = "Field1";
			this.txtCode.Top = 0.03125F;
			this.txtCode.Width = 1F;
			// 
			// txtCount
			// 
			this.txtCount.CanGrow = false;
			this.txtCount.Height = 0.19F;
			this.txtCount.Left = 1.0625F;
			this.txtCount.MultiLine = false;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCount.Text = "Field1";
			this.txtCount.Top = 0.03125F;
			this.txtCount.Width = 0.5F;
			// 
			// txtland
			// 
			this.txtland.CanGrow = false;
			this.txtland.Height = 0.19F;
			this.txtland.Left = 1.6875F;
			this.txtland.MultiLine = false;
			this.txtland.Name = "txtland";
			this.txtland.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtland.Text = "Field1";
			this.txtland.Top = 0.03125F;
			this.txtland.Width = 1.25F;
			// 
			// txtbldg
			// 
			this.txtbldg.CanGrow = false;
			this.txtbldg.Height = 0.19F;
			this.txtbldg.Left = 3F;
			this.txtbldg.MultiLine = false;
			this.txtbldg.Name = "txtbldg";
			this.txtbldg.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtbldg.Text = "Field1";
			this.txtbldg.Top = 0.03125F;
			this.txtbldg.Width = 1.4375F;
			// 
			// txtexempt
			// 
			this.txtexempt.CanGrow = false;
			this.txtexempt.Height = 0.19F;
			this.txtexempt.Left = 4.5F;
			this.txtexempt.MultiLine = false;
			this.txtexempt.Name = "txtexempt";
			this.txtexempt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtexempt.Text = "Field1";
			this.txtexempt.Top = 0.03125F;
			this.txtexempt.Width = 1.375F;
			// 
			// txttotal
			// 
			this.txttotal.CanGrow = false;
			this.txttotal.Height = 0.1875F;
			this.txttotal.Left = 5.9375F;
			this.txttotal.MultiLine = false;
			this.txttotal.Name = "txttotal";
			this.txttotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txttotal.Text = "Field1";
			this.txttotal.Top = 0.03125F;
			this.txttotal.Width = 1.5F;
			// 
			// srptlandcode
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.465278F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtexempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtland;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtbldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtexempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
