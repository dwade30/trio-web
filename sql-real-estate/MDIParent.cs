﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using System;
using System.Drawing;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.MVR;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
    /// <summary>
    /// Summary description for MDIParent.
    /// </summary>
    public class MDIParent : FCForm
    //: BaseForm
    {
        public fecherFoundation.FCCommonDialog CommonDialog1;
        public Wisej.Web.ImageList ImageList1;
        public fecherFoundation.FCTextBox txtCommSource;
        public MDIParent()
        {
            //
            // Required for Windows Form Designer support
            //
            //InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            CommonDialog1 = new fecherFoundation.FCCommonDialog();
            ImageList1 = new Wisej.Web.ImageList();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParent));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))), "Clear");
            Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images1"))), "Left");
            Wisej.Web.ImageListEntry imageListEntry3 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images2"))), "Right");
            Wisej.Web.ImageListEntry imageListEntry4 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images3"))));
            Wisej.Web.ImageListEntry imageListEntry5 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images4"))));
            Wisej.Web.ImageListEntry imageListEntry6 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images5"))));
            Wisej.Web.ImageListEntry imageListEntry7 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images6"))));
            Wisej.Web.ImageListEntry imageListEntry8 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images7"))));
            Wisej.Web.ImageListEntry imageListEntry9 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images8"))));
            ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
                imageListEntry1,
                imageListEntry2,
                imageListEntry3,
                imageListEntry4,
                imageListEntry5,
                imageListEntry6,
                imageListEntry7,
                imageListEntry8,
                imageListEntry9
            });
            ImageList1.ImageSize = new System.Drawing.Size(17, 19);
            ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            txtCommSource = new fecherFoundation.FCTextBox();
            LinkExecute += MDIParent_LinkExecute;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static MDIParent InstancePtr
        {
            get
            {
                return (MDIParent)Sys.GetInstance(typeof(MDIParent));
            }
        }

        protected MDIParent _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        private int[] LabelNumber = new int[200 + 1];
        public bool boolLeavetoBilling;
        bool boolCommChangeNotLegit;
        bool boolDontShellToGE;
        private bool boolCheckForExtract;
        private int intExit;
        private bool mnuImportBritesideVisible;
        private bool mnuNewSharonImportVisible;
        private bool mnuImportJFRyanVisible;
        private bool mnuImportODonnellVisible;
        private bool mnuExportODonnellVisible;
        private bool mnuCustomFunctionsVisible;
        const string NEWLINE = "\r\n";

        public void LeavetoBilling()
        {
            //this.Unload();
        }

        private void mnuFileCentralParties_Click(object sender, System.EventArgs e)
        {
            frmCentralPartySearch.InstancePtr.Show(App.MainForm);
        }

        public void SetMenuOptions(string strMenu)
        {
            var menuName = Strings.UCase(strMenu);

            if (!modREMain.Statics.boolShortRealEstate)
            {
                switch (menuName)
                {
                    case "DATAMANIPULATION":

                        //DataManipulationCaptions();
                        break;
                    case "ACCOUNTFUNCTIONS":

                        //AccountCaptions();
                        break;
                    case "PROPERTYCOST":

                        //PropertyCostCaptions();
                        break;
                    case "MAIN":

                        // Call LoadEntryMenuOptions(9)
                        MainCaptions();

                        break;
                    case "ASSOCIATION":

                        //AssociationCaptions();
                        break;
                    case "IMPORTEXPORT":

                        //ImportExportCaptions();
                        break;
                    case "SYSTEM":

                        // Call LoadEntryMenuOptions(10)
                        //CalculateCaptions();
                        break;
                    case "FILE":

                        // Call LoadEntryMenuOptions(12)
                        //FileMaintCaptions();
                        break;
                    case "COSTFILEMAIN":

                        // Call LoadEntryMenuOptions(4)
                        //CostFileMainCaptions();
                        break;
                    case "COSTFILE":

                        // Call LoadEntryMenuOptions(12)
                        //CostFileCaptions();
                        break;
                    case "DWELLINGCOST":

                        //DwellingCostCaptions();
                        break;
                    case "INCOMEUPDATE":

                        //IncomeUpdateCaptions();
                        // Case "LISTINGS"
                        // Call LoadEntryMenuOptions(13)
                        // Call PrintListingsCaptions(13)
                        // Case "LISTINGTYPE"
                        // Call LoadEntryMenuOptions(9)
                        // Call PrintListing2Captions(9)
                        // Case "LABELS2"
                        // Call LoadEntryMenuOptions(6)
                        // Call PrintLabels2Captions(6)
                        break;
                    case "PRINTMENU1":

                        // Call LoadEntryMenuOptions(4)
                        //PrintMenu1Captions();
                        break;
                    case "SALESMENU":

                        //SalesCaptions();
                        break;
                    case "PICTURE":

                        //PictureCaptions();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (menuName)
                {
                    case "MAIN":
                        ShortMainCaptions();

                        break;
                    case "DATAMANIPULATION":

                        //DataManipulationCaptions();
                        break;
                    case "IMPORTEXPORT":

                        //                    ShortImportExportCaptions();
                        break;
                    case "SYSTEM":

                        //CalculateCaptions();
                        break;
                    case "ASSOCIATION":

                        //AssociationCaptions();
                        break;
                    case "FILE":

                        //FileMaintCaptions();
                        break;
                    case "COSTFILE":

                        //ShortCostFileCaptions();
                        break;
                    case "PRINTMENU1":

                        //PrintMenu1Captions();
                        break;
                    case "ACCOUNTFUNCTIONS":

                        //AccountCaptions();
                        break;
                    default:
                        break;
                }
            }
            // AutoSizeMenu (40)
        }

        public void CloseChildForms()
        {
            FCUtils.CloseFormsOnProject();
        }

        public void MenuActions(int intMenuChoice)
        {
            string menu = App.MainForm.NavigationMenu.CurrentItem.Menu;
            var menuName = Strings.UCase(menu);

            switch (menuName)
            {
                case "MAIN":
                    MainActions(intMenuChoice);

                    break;
                case "ASSOCIATION":
                    AssociationActions(intMenuChoice);

                    break;
                case "SALESMENU":
                    SalesMenuActions(intMenuChoice);

                    break;
                case "IMPORTEXPORT":
                    ImportExportActions(intMenuChoice);

                    break;
                case "INCOMEUPDATE":
                    IncomeUpdateActions(intMenuChoice);

                    break;
                case "DWELLINGCOST":
                    DwellingCostActions(intMenuChoice);

                    break;
                case "PROPERTYCOST":
                    PropertyCostActions(intMenuChoice);

                    break;
                case "SYSTEM":
                    CalculateActions(intMenuChoice);

                    break;
                case "FILE":
                    FileMaintActions(intMenuChoice);

                    break;
                case "COSTFILEMAIN":
                    CostFileMainActions(intMenuChoice);

                    break;
                case "COSTFILE":
                    CostFileActions(intMenuChoice);

                    break;
                case "PRINTMENU1":
                    PrintActions(intMenuChoice);

                    break;
                case "ACCOUNTFUNCTIONS":
                    AccountActions(intMenuChoice);

                    break;
                case "PICTURE":
                    PictureActions(intMenuChoice);

                    break;
                case "DATAMANIPULATION":
                    DataManipulationActions(intMenuChoice);

                    break;
                case "CUSTOM":
                    CustomActions(intMenuChoice);

                    break;
                case "RETRANSFER":
                    RETransferActions(intMenuChoice);

                    break;
                default:
                    break;
            }
        }

        private void CustomActions(int menu)
        {

            switch (menu)
            {
                case 1:
                    mnuImportODonnell_Click(null, null);
                    break;
                case 2:
                    mnuExportODonnell_Click(null, null);
                    break;
                case 3:
                    BathImport();
                    break;
                case 4:
                    mnuImportBriteside_Click(null, null);
                    break;
                case 5:
                    mnuNewSharonImport_Click(null, null);
                    break;
                case 6:
                    mnuImportJFRyan_Click(null, null);
                    break;
            }
        }

        private void BathImport()
        {
            var bathImport = new frmBathImport();
            bathImport.Show();
        }

        private void MDIParent_Activated(object sender, System.EventArgs e)
        {
            try
            {
                if (!modGlobalVariables.Statics.boolFromBilling || boolLeavetoBilling) return;

                modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(FCUtils.GetCommandLine())));
                clsDRWrapper temp = modDataTypes.Statics.MR;
                modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, 1);
                modDataTypes.Statics.MR = temp;
                temp = modDataTypes.Statics.CMR;
                modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, 1);
                modDataTypes.Statics.CMR = temp;
                temp = modDataTypes.Statics.DWL;
                modREMain.OpenDwellingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, 1);
                modDataTypes.Statics.DWL = temp;
                temp = modDataTypes.Statics.OUT;
                modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, 1);
                modDataTypes.Statics.OUT = temp;
                // Call OpenWWKTable(WWK)
                modGNBas.Statics.gintCardNumber = 1;
                if (!modREMain.Statics.boolShortRealEstate)
                {
                    frmNewREProperty.InstancePtr.Show();
                }
                else
                {
                    frmSHREBLUpdate.InstancePtr.Show();
                }
                modGNBas.Statics.gboolLoadingDone = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error Number {FCConvert.ToString(Information.Err(ex).Number)}  {Information.Err(ex).Description}{NEWLINE}In MDIForm_Activate in line {Information.Erl()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void MDIParent_LinkExecute(object sender, FCLinkExecuteEventArgs e)
        {
            string cmd = e.CmdStr;
            short cancel = e.Cancel;
            Form_LinkExecute(ref cmd, ref cancel);
            e.Cancel = cancel;
        }

        private void Form_LinkExecute(ref string CmdStr, ref short Cancel)
        {
            //Application.DoEvents();
            if (Strings.UCase(Strings.Left(CmdStr, 2)) == "BL")
            {
                modGlobalVariables.Statics.boolFromBilling = true;
                // must close out all forms and then go to account maintenance
                //SetMenuOptions("MAIN");
                modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(CmdStr, 3))));
                clsDRWrapper temp = modDataTypes.Statics.MR;

                try
                {
                    var lastAcctNumber = modGlobalVariables.Statics.gintLastAccountNumber;

                    modREMain.OpenMasterTable(ref temp, lastAcctNumber, 1);
                    modDataTypes.Statics.MR = temp;
                    temp = modDataTypes.Statics.CMR;
                    modREMain.OpenCOMMERCIALTable(ref temp, lastAcctNumber, 1);
                    modDataTypes.Statics.CMR = temp;
                    temp = modDataTypes.Statics.DWL;
                    modREMain.OpenDwellingTable(ref temp, lastAcctNumber, 1);
                    modDataTypes.Statics.DWL = temp;
                    temp = modDataTypes.Statics.OUT;
                    modREMain.OpenOutBuildingTable(ref temp, lastAcctNumber, 1);
                    modDataTypes.Statics.OUT = temp;
                    // Call OpenWWKTable(WWK)
                    modGNBas.Statics.gintCardNumber = 1;
                    if (!modREMain.Statics.boolShortRealEstate)
                    {
                        frmREProperty.InstancePtr.Show();
                    }
                    else
                    {
                        frmSHREBLUpdate.InstancePtr.Show();
                    }
                    modGNBas.Statics.gboolLoadingDone = true;
                }
                finally
                {
                    temp.DisposeOf();
                }
            }
            Cancel = 0;
            // tell calling app that its okay
        }
        //private void MDIParent_Load(object sender, System.EventArgs e)
        public void Init()
        {

            string strReturn = "";
            int intUserID;

            try
            {
                // On Error GoTo ErrorHandler
                boolCheckForExtract = true;
                modGlobalFunctions.LoadTRIOColors();
                // TRIOCOLORBLUE = RGB(0, 0, 200)
                //modGlobalFunctions.SetTRIOColors(this);
                App.MainForm.menuTree.ImageList = CreateImageList();
                //this.Text = strTrio + " - Real Estate";
                App.MainForm.Caption = "REAL ESTATE";
                App.MainForm.ApplicationIcon = "icon-real-estate";
                string assemblyName = GetType().Assembly.GetName().Name;
                if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
                {
                    App.MainForm.ApplicationIcons.Add(assemblyName, "icon-real-estate");
                }
                App.MainForm.NavigationMenu.Owner = this;
                App.MainForm.NavigationMenu.OriginName = "Real Estate";
                boolDontShellToGE = false;
                if (strReturn == string.Empty)
                    strReturn = "False";
                modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
                //mnuOMaxForms.Checked = modGlobalConstants.Statics.boolMaxForms;
                //SetMenuOptions("Main");
                modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\", "Maximize Forms", ref strReturn);

                modGlobalVariables.Statics.boolCostRecChanged = true;
                modGlobalVariables.Statics.boolComCostChanged = true;
                modGlobalVariables.Statics.boolLandTChanged = true;
                modGlobalVariables.Statics.boolLandSChanged = true;
                modGlobalVariables.Statics.boolRSZChanged = true;
                modRegistry.GetKeyValues(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "securityid", ref strReturn);
                intUserID = FCConvert.ToInt32(Math.Round(Conversion.Val(strReturn + "")));
                // secRS.FindFirst ("id = " & intUserID)
                cLogin tSec = new cLogin();
                cSecurityUser tUser;
                tUser = tSec.GetUserByID(intUserID);
                if (!(tUser == null))
                {
                    modGlobalConstants.Statics.gstrUserID = tUser.User;
                }
                // 22 Call secRS.FindFirstRecord("id", intUserID)
                // 23 gstrUserID = secRS.Fields("UserID")
                modGlobalVariables.Statics.CostRec = new clsCostRecord();
                modGlobalVariables.Statics.LandTRec = new clsLandTableRecord();
                modGlobalVariables.Statics.LandSRec = new clsLandScheduleRecord();
                modGlobalVariables.Statics.ComCostRec = new clsCostRecord();
                // Call RegCreateKey(HKEY_LOCAL_MACHINE, REGISTRYKEY, 0)
                modRegistry.GetKeyValues(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "Maximize Forms", ref strReturn);
                if (strReturn == string.Empty)
                    strReturn = "False";
                modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
                //mnuOMaxForms.Checked = modGlobalConstants.Statics.boolMaxForms;
                //StatusBar1.Panels[0].Text = modGlobalConstants.MuniName;
                App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
                SetMenuOptions("MAIN");
                //FC:FINAL:DDU: initialize TWXF module properties
                modMainXF.Main();
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show($"Error Number {FCConvert.ToString(Information.Err(ex).Number)}  {Information.Err(ex).Description}{NEWLINE}In MdiForm_Load. Line {Information.Erl()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public ImageList CreateImageList()
        {
            ImageList imageList = new ImageList();
            imageList.ImageSize = new System.Drawing.Size(18, 18);
            imageList.Images.AddRange(new ImageListEntry[] {
                new ImageListEntry("menutree-account-maintenance", "account-maintenance"),
                new ImageListEntry("menutree-short-maintenance", "short-maintenance"),
                new ImageListEntry("menutree-cost-files", "cost-files"),
                new ImageListEntry("menutree-printing", "printing"),
                new ImageListEntry("menutree-sale-record", "sale-record"),
                new ImageListEntry("menutree-compute", "compute"),
                new ImageListEntry("menutree-import-export", "import-export"),
                new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
                new ImageListEntry("menutree-account-payable-processing", "account-payable-processing"),
                new ImageListEntry("menutree-check-reconciliation", "check-reconciliation"),
                new ImageListEntry("menutree-audit", "audit"),
                new ImageListEntry("menutree-ap-research", "ap-research"),
                new ImageListEntry("menutree-tax-rate-calculator", "tax-rate-calculator"),
                new ImageListEntry("menutree-account-maintenance-active", "account-maintenance"),
                new ImageListEntry("menutree-short-maintenance-active", "short-maintenance-active"),
                new ImageListEntry("menutree-cost-files-active", "cost-files-active"),
                new ImageListEntry("menutree-printing-active", "printing-active"),
                new ImageListEntry("menutree-sale-record-active", "sale-record-active"),
                new ImageListEntry("menutree-compute-active", "compute-active"),
                new ImageListEntry("menutree-import-export-active", "import-export-active"),
                new ImageListEntry("menutree-file-maintenance-active", "file-maintenance-active"),
                new ImageListEntry("menutree-account-payable-processing-active", "account-payable-processing-active"),
                new ImageListEntry("menutree-check-reconciliation-active", "check-reconciliation-active"),
                new ImageListEntry("menutree-audit-active", "audit-active"),
                new ImageListEntry("menutree-ap-research-active", "ap-research-active"),
                new ImageListEntry("menutree-tax-rate-calculator-active", "tax-rate-calculator-active"),
                new ImageListEntry("menutree-street-maintenance", "street-maintenance")
            });
            return imageList;
        }

        private void PrintMenu1Captions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Printing]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Reports", "PRINTMENU1", parentItem, true, modGlobalVariables.CNSTLISTINGS);
            AddMenuItem(ref CurRow, 2, "Labels", "PRINTMENU1", parentItem, true, modGlobalVariables.CNSTLABELS);
            AddMenuItem(ref CurRow, 3, "Extract for Reports / Labels", "PRINTMENU1", parentItem, true, modGlobalVariables.CNSTEXTRACTFORLISTINGSANDLABELS);
            //AddMenuItem(ref CurRow, 4, "Return to Main", "PRINTMENU1", 0, "X");
            AutoSizeMenu();
        }

        private void PrintActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmListing.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        rptLabels.InstancePtr.Unload();
                        frmLabelPrinting.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 3:
                    {
                        frmAssessExtract.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 4:
                    {
                        //SetMenuOptions("MAIN");
                        break;
                    }
            }
            //end switch
        }

        public void MainActions(int intMenu)
        {
            if (modREMain.Statics.boolShortRealEstate)
            {
                ShortMainActions(intMenu);
                return;
            }
            switch (intMenu)
            {
                case 1:
                    {

                        modGlobalVariables.Statics.wmainresponse = 1;
                        // Call SaveWWKTable(WWK)
                        frmREProperty.InstancePtr.Unload();
                        frmNewREProperty.InstancePtr.Unload();
                        frmSHREBLUpdate.InstancePtr.Unload();
                        frmGetAccount.InstancePtr.Show(App.MainForm);
                        //FC:FINAL:CHN - issue #1645: Fix not changed header text.
                        frmGetAccount.InstancePtr.Text = "Account Maintenance";
                        break;
                    }
                case 2:
                    {
                        modGlobalVariables.Statics.wmainresponse = 2;
                        // Call SaveWWKTable(WWK)
                        frmREProperty.InstancePtr.Unload();
                        frmNewREProperty.InstancePtr.Unload();
                        frmSHREBLUpdate.InstancePtr.Unload();
                        frmGetAccount.InstancePtr.Show(App.MainForm);
                        //FC:FINAL:CHN - issue #1645: Fix not changed header text.
                        frmGetAccount.InstancePtr.Text = "Short Maintenance";
                        break;
                    }
                case 3:
                    {
                        //SetMenuOptions("COSTFILEMAIN");
                        break;
                    }
                case 4:
                    {
                        //SetMenuOptions("PRINTMENU1");
                        break;
                    }
                case 5:
                    {
                        //SetMenuOptions("SALESMENU");
                        break;
                    }
                case 6:
                    {
                        //SetMenuOptions("SYSTEM");
                        break;
                    }
                case 7:
                    {
                        //SetMenuOptions("IMPORTEXPORT");
                        break;
                    }
                case 8:
                    {
                        //SetMenuOptions("FILE");
                        break;
                    }

            }
            //end switch
        }

        private void ClearMenuItems()
        {
            //int CurRow;
            //for (CurRow = 0; CurRow <= 20; CurRow++)
            //{
            //	GRID.RowData(CurRow, false);
            //	GRID.TextMatrix(CurRow, 1, "");
            //	GRID.TextMatrix(CurRow, 2, FCConvert.ToString(0));
            //	GRID.TextMatrix(CurRow, 3, "");
            //}
            //// CurRow
        }

        private object MainCaptions()
        {
            object MainCaptions = null;
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //FC:FINAL:MSH - i.issue #1076: clear the menu to prevent multi adding menu items
            FCMainForm.InstancePtr.NavigationMenu.Clear();
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Main]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            FCMenuItem newItem = null;
            AddMenuItem(ref CurRow, 1, "Account Maintenance", "MAIN", null, true, modGlobalVariables.CNSTLONGSCREEN, "", imageKey: "account-maintenance");
            AddMenuItem(ref CurRow, 2, "Short Maintenance", "MAIN", null, true, modGlobalVariables.CNSTSHORTSCREEN, imageKey: "short-maintenance");
            newItem = AddMenuItem(ref CurRow, 3, "Cost File Update", "MAIN", null, false, modGlobalVariables.CNSTMNUCOSTUPDATE, imageKey: "cost-files");
            CostFileMainCaptions(newItem);
            newItem = AddMenuItem(ref CurRow, 4, "Printing", "MAIN", null, false, modGlobalVariables.CNSTPRINTROUTINES, imageKey: "printing");
            PrintMenu1Captions(newItem);
            newItem = AddMenuItem(ref CurRow, 5, "Sale Records", "MAIN", null, false, modGlobalVariables.CNSTSALESRECORDS, imageKey: "sale-record");
            SalesCaptions(newItem);
            newItem = AddMenuItem(ref CurRow, 6, "Compute", "MAIN", null, false, modGlobalVariables.CNSTMNUCALCULATE, imageKey: "compute");
            CalculateCaptions(newItem);
            newItem = AddMenuItem(ref CurRow, 7, "Import / Export", "MAIN", null, false, modGlobalVariables.CNSTIMPORTEXPORT, imageKey: "import-export");
            ImportExportCaptions(newItem);
            newItem = AddMenuItem(ref CurRow, 8, "File Maintenance", "MAIN", null, false, modGlobalVariables.CNSTFILEMAINTENANCE, "M", imageKey: "file-maintenance");
            FileMaintCaptions(newItem);
            CustomFunctionsCaptions(ref CurRow, ref newItem);
            AutoSizeMenu();
            return MainCaptions;
        }

        private void CustomFunctionsCaptions(ref int CurRow, ref FCMenuItem newItem)
        {
            if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "ACTON")
            {
                mnuCustomFunctionsVisible = true;
            }
            if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "NEW SHARON")
            {
                mnuCustomFunctionsVisible = true;
            }
            if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "OXFORD")
            {
                mnuCustomFunctionsVisible = true;
            }

            if (modGlobalConstants.Statics.MuniName.ToLower() == "bath")
            {
                mnuCustomFunctionsVisible = true;
            }
            if (modREMain.Statics.boolAllowXF)
            {
                if (modREMain.Statics.intXFLevel == 1)
                {
                    mnuCustomFunctionsVisible = true;
                }
            }
            if (mnuCustomFunctionsVisible)
            {
                newItem = AddMenuItem(ref CurRow, 9, "Custom Programs", "MAIN", null, false, imageKey: "street-maintenance");
                CustomProgramsCaptions(newItem);
            }
        }

        private void RETransferCaptions(FCMenuItem parentItem)
        {
            int CurRow = 1;
            ClearLabels();
            AddMenuItem(ref CurRow, 1, "Import Real Estate", "RETransfer", parentItem, true);
            AddMenuItem(ref CurRow, 2, "Import Personal Property", "RETransfer", parentItem, true);
            AddMenuItem(ref CurRow, 3, "Customize", "RETransfer", parentItem, true);
            AutoSizeMenu();
        }

        private void RETransferActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmImportCompetitorXF.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        frmPPImportXF.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 3:
                    {
                        frmCustomizeXF.InstancePtr.Show(App.MainForm);
                        break;
                    }
            }
        }

        private void CustomProgramsCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate [Custom]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "ACTON")
            {
                mnuImportBritesideVisible = true;
                AddMenuItem(ref CurRow, 4, "Import Briteside File", "CUSTOM", parentItem, true);
            }
            if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "NEW SHARON")
            {
                AddMenuItem(ref CurRow, 5, "Import From Excel", "CUSTOM", parentItem, true);
                mnuNewSharonImportVisible = true;
            }
            if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "OXFORD")
            {
                AddMenuItem(ref CurRow, 6, "Import from JF Ryan", "CUSTOM", parentItem, true);
                mnuImportJFRyanVisible = true;
            }

            if (modGlobalConstants.Statics.MuniName.ToLower() == "bath")
            {
                AddMenuItem(ref CurRow, 3, "Import Bath File", "CUSTOM", parentItem, true);
            }
            if (modREMain.Statics.boolAllowXF)
            {
                if (modREMain.Statics.intXFLevel == 1)
                {
                    AddMenuItem(ref CurRow, 1, "Import ODonnell File", "CUSTOM", parentItem, true);
                    AddMenuItem(ref CurRow, 2, "Export ODonnell File", "CUSTOM", parentItem, true);
                    mnuImportODonnellVisible = true;
                    mnuExportODonnellVisible = true;
                }
            }
            AutoSizeMenu();
        }

        private void ShortMainCaptions()
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //FC:FINAL:MSH - i.issue #1076: clear the menu to prevent multi adding menu items
            FCMainForm.InstancePtr.NavigationMenu.Clear();
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Main]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Short Maintenance", "MAIN", null, true, modGlobalVariables.CNSTSHORTSCREEN, imageKey: "short-maintenance");
            FCMenuItem newItem = AddMenuItem(ref CurRow, 2, "Cost File Update", "MAIN", null, false, modGlobalVariables.CNSTMNUCOSTUPDATE, imageKey: "cost-files");
            ShortCostFileCaptions(newItem);
            newItem = AddMenuItem(ref CurRow, 3, "Printing", "MAIN", null, false, modGlobalVariables.CNSTPRINTROUTINES, imageKey: "printing");
            PrintMenu1Captions(newItem);
            newItem = AddMenuItem(ref CurRow, 4, "Import/Export", "MAIN", null, false, modGlobalVariables.CNSTIMPORTEXPORT, imageKey: "import-export");
            ShortImportExportCaptions(newItem);
            AddMenuItem(ref CurRow, 5, "Calculate Exemptions", "MAIN", null, true, modGlobalVariables.CNSTCALCEXEMPTION, imageKey: "account-payable-processing");
            AddMenuItem(ref CurRow, 6, "Calculate Tree Growth", "MAIN", null, true, modGlobalVariables.CNSTMNUCALCULATE, imageKey: "check-reconciliation");
            AddMenuItem(ref CurRow, 7, "Audit of Billing Amounts", "MAIN", null, true, modGlobalVariables.CNSTAUDITBILLING, imageKey: "audit");
            AddMenuItem(ref CurRow, 8, "RE/PP Audit Summary", "MAIN", null, true, modGlobalVariables.CNSTAUDITBILLING, imageKey: "ap-research");
            AddMenuItem(ref CurRow, 9, "Tax Rate Calculator", "MAIN", null, true, modGlobalVariables.CNSTTAXRATECALCULATOR, imageKey: "tax-rate-calculator");
            newItem = AddMenuItem(ref CurRow, 10, "File Maintenance", "MAIN", null, false, modGlobalVariables.CNSTFILEMAINTENANCE, "M", imageKey: "file-maintenance");
            FileMaintCaptions(newItem);
            CustomFunctionsCaptions(ref CurRow, ref newItem);
            //AddMenuItem(ref CurRow, 11, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        public void ShortMainActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        modGlobalVariables.Statics.wmainresponse = 2;
                        // Call SaveWWKTable(WWK)
                        frmSHREBLUpdate.InstancePtr.Unload();
                        frmGetAccount.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        //SetMenuOptions("COSTFILE");
                        break;
                    }
                case 3:
                    {
                        //SetMenuOptions("PRINTMENU1");
                        break;
                    }
                case 4:
                    {
                        //SetMenuOptions("IMPORTEXPORT");
                        break;
                    }
                case 5:
                    {
                        // Call modcalcexemptions.calculateexemptions
                        //FC:FINAL:MSH - i.issue #1070: call form as tab
                        //modcalcexemptions.NewCalcExemptions();
                        frmExemptList.InstancePtr.Init();
                        break;
                    }
                case 6:
                    {
                        modSpeedCalc.CalcTreeGrowth();
                        break;
                    }
                case 7:
                    {
                        frmaudit.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 9:
                    {
                        frmTaxRateCalc.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 8:
                    {
                        frmReportViewer.InstancePtr.Init(rptREPPAudit.InstancePtr);
                        // Case 10
                        // rptHomesteadCalculation.Show , MDIParent
                        break;
                    }
                case 10:
                    {
                        SetMenuOptions("FILE");
                        break;
                    }
                case 11:
                    {
                        modReplaceWorkFiles.EntryFlagFile(true, "RS", true);
                        // If bolFromDosTrio = True Then
                        // Do Nothing
                        // Else
                        // On Error GoTo BWShellError
                        // Shell "TWGNENTY.EXE", vbNormalFocus
                        // End If
                        //MDIParent.InstancePtr.Unload();
                        Application.Exit();
                        break;
                    }
                    //case 6:
                    //{
                    //	break;
                    //}
            }
            //end switch
        }

        public object ShortCostFileMainCaptions()
        {
            object ShortCostFileMainCaptions = null;
            return ShortCostFileMainCaptions;
        }

        private void CostFileMainCaptions(FCMenuItem parentMenu)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Cost File Update]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            FCMenuItem newItem = AddMenuItem(ref CurRow, 1, "Update", "COSTFILEMAIN", parentMenu, false, modGlobalVariables.CNSTMNUUPDATE);
            CostFileCaptions(newItem);
            newItem = AddMenuItem(ref CurRow, 2, "Income Approach", "COSTFILEMAIN", parentMenu, false, modGlobalVariables.CNSTMNUINCAPPROACH);
            IncomeUpdateCaptions(newItem);
            //AddMenuItem(ref CurRow, 3, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        private void ShortCostFileCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Update Cost Files]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Property", "COSTFILE", parentItem, true, modGlobalVariables.CNSTCOSTFILES);
            AddMenuItem(ref CurRow, 2, "Exemption Codes", "COSTFILE", parentItem, true, modGlobalVariables.CNSTEXEMPTIONCODES);
            AddMenuItem(ref CurRow, 3, "Tran/Land/Bldg Codes", "COSTFILE", parentItem, true);
            AddMenuItem(ref CurRow, 4, "Entrance and Information", "COSTFILE", parentItem, true);
            //AddMenuItem(ref CurRow, 5, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        public void ShortCostFileActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmCostFiles.InstancePtr.Init(1000, 1449, false, "Property");
                        break;
                    }
                case 2:
                    {
                        // Call frmCostFiles.Init(1900, 1999, False, "Exemption Codes")
                        frmExemptCodes.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 3:
                    {
                        frmTranLandBldgFiles.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 4:
                    {
                        frmCostFiles.InstancePtr.Init(1700, 1719, false, "Entrance and Information Codes");
                        break;
                    }
                case 5:
                    {
                        //SetMenuOptions("MAIN");
                        break;
                    }
            }
            //end switch
        }

        public void DwellingCostActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmNewCostFiles.InstancePtr.Init("Dwelling", "Style");
                        break;
                    }
                case 2:
                    {
                        frmNewCostFiles.InstancePtr.Init("Dwelling", "Heat");
                        break;
                    }
                case 3:
                    {
                        frmNewCostFiles.InstancePtr.Init("Dwelling", "Exterior");
                        break;
                    }
                case 4:
                    {
                        frmNewCostFiles.InstancePtr.Init("Dwelling", "Economic Code");
                        break;
                    }
                case 5:
                    {
                        frmCostFiles.InstancePtr.Init(1450, 1899, false, "Dwelling");
                        break;
                    }
                case 6:
                    {
                        //SetMenuOptions("COSTFILE");
                        break;
                    }
            }
            //end switch
        }

        private void PropertyCostActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmNewCostFiles.InstancePtr.Init("Property", "Neighborhood");
                        break;
                    }
                case 2:
                    {
                        frmZones.InstancePtr.Init();
                        break;
                    }
                case 3:
                    {
                        frmCostFiles.InstancePtr.Init(1000, 1449, false, "Property");
                        break;
                    }
                default:
                    {
                        //SetMenuOptions("COSTFILE");
                        break;
                    }
            }
            //end switch
        }

        private void CostFileMainActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        //SetMenuOptions("COSTFILE");
                        break;
                    }
                case 2:
                    {
                        //SetMenuOptions("INCOMEUPDATE");
                        break;
                    }
                case 3:
                    {
                        //SetMenuOptions("MAIN");
                        break;
                    }
            }
            //end switch
        }

        public void CostFileActions(int intMenu)
        {
            if (modREMain.Statics.boolShortRealEstate)
            {
                ShortCostFileActions(intMenu);
                return;
            }
            switch (intMenu)
            {
                case 1:
                    {
                        //SetMenuOptions("PROPERTYCOST");
                        break;
                    }
                case 2:
                    {
                        // Unload frmCostSchedule
                        // Call frmCostSchedule.Init(31, False, "Land Acreage Factor", "Factor")
                        frmLandType.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 3:
                    {
                        frmLandSchedule.InstancePtr.Show(App.MainForm);
                        // Unload frmCostSchedule
                        // Call frmCostSchedule.Init(ue, "Land Schedule 1", "Amount")
                        // Case 4
                        // frmCategoryTable.Show , MDIParent
                        break;
                    }
                case 4:
                    {
                        // Call frmCostFiles.Init(1450, 1899, False, "Dwelling")
                        //SetMenuOptions("DWELLINGCOST");
                        break;
                    }
                case 5:
                    {
                        frmCostFiles.InstancePtr.Init(1, 999, false, "Outbuildings");
                        break;
                    }
                case 6:
                    {
                        frmCostFiles.InstancePtr.Init(1, 3500, true, "Commercial");
                        break;
                    }
                case 7:
                    {
                        frmCostSchedule.InstancePtr.Unload();
                        modGNBas.Statics.boolOutbuildingSQFT = true;
                        frmCostSchedule.InstancePtr.Init("Outbuilding Square Foot Factor");
                        break;
                    }
                case 8:
                    {
                        frmRECResSizeFactor.InstancePtr.Show(App.MainForm);
                        // Case 10
                        // frmFAE.Show , MDIParent
                        break;
                    }
                case 9:
                    {
                        // Call frmCostFiles.Init(1900, 1999, False, "Exemption Codes")
                        frmExemptCodes.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 10:
                    {
                        frmTranLandBldgFiles.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 11:
                    {
                        //SetMenuOptions("MAIN");
                        break;
                    }
            }
            //end switch
        }

        private void PropertyCostCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Property Cost]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Neighborhood", "PROPERTYCOST", parentItem, true);
            AddMenuItem(ref CurRow, 2, "Zone", "PROPERTYCOST", parentItem, true);
            AddMenuItem(ref CurRow, 3, "Other", "PROPERTYCOST", parentItem, true);
            //AddMenuItem(ref CurRow, 4, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        private void DwellingCostCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Dwelling Cost]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Building Style", "DWELLINGCOST", parentItem, true, modGlobalVariables.CNSTCOSTFILES);
            AddMenuItem(ref CurRow, 2, "Heat", "DWELLINGCOST", parentItem, true, modGlobalVariables.CNSTCOSTFILES);
            AddMenuItem(ref CurRow, 3, "Exterior", "DWELLINGCOST", parentItem, true, modGlobalVariables.CNSTCOSTFILES);
            AddMenuItem(ref CurRow, 4, "Economic Code", "DWELLINGCOST", parentItem, true, modGlobalVariables.CNSTCOSTFILES);
            AddMenuItem(ref CurRow, 5, "Other", "DWELLINGCOST", parentItem, true, modGlobalVariables.CNSTCOSTFILES);
            //AddMenuItem(ref CurRow, 6, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        private void CostFileCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Update]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            FCMenuItem newItem = AddMenuItem(ref CurRow, 1, "Property", "COSTFILE", parentItem, false, modGlobalVariables.CNSTCOSTFILES);
            PropertyCostCaptions(newItem);
            AddMenuItem(ref CurRow, 2, "Land Types", "COSTFILE", parentItem, true);
            AddMenuItem(ref CurRow, 3, "Land Schedule", "COSTFILE", parentItem, true, modGlobalVariables.CNSTLANDSCHEDTABLE);
            newItem = AddMenuItem(ref CurRow, 4, "Dwelling", "COSTFILE", parentItem, false, modGlobalVariables.CNSTCOSTFILES);
            DwellingCostCaptions(newItem);
            AddMenuItem(ref CurRow, 5, "Outbuilding", "COSTFILE", parentItem, true, modGlobalVariables.CNSTCOSTFILES);
            AddMenuItem(ref CurRow, 6, "Commercial", "COSTFILE", parentItem, true, modGlobalVariables.CNSTCOSTFILES);
            AddMenuItem(ref CurRow, 7, "Outbuilding Sqft Factors", "COSTFILE", parentItem, true, modGlobalVariables.CNSTOUTSQFTTABLE);
            AddMenuItem(ref CurRow, 8, "Residential Size Factors", "COSTFILE", parentItem, true, modGlobalVariables.CNSTRESSIZEFACT);
            AddMenuItem(ref CurRow, 9, "Exemption Codes", "COSTFILE", parentItem, true, modGlobalVariables.CNSTEXEMPTIONCODES);
            AddMenuItem(ref CurRow, 10, "Tran/Land/Bldg Codes", "COSTFILE", parentItem, true);
            //AddMenuItem(ref CurRow, 11, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        private object CalculateActions(int intMenu)
        {
            object CalculateActions = null;
            switch (intMenu)
            {
                case 1:
                    {
                        FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                        modProperty.Statics.RUNTYPE = "P";
                        modGlobalVariables.Statics.gboolAllAccounts = true;
                        clsDRWrapper rsTemp = new clsDRWrapper();
                        // Dim dbTemp As DAO.Database
                        modGlobalVariables.Statics.boolUseArrays = true;
                        if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                        {
                            modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = 1;
                        }
                        modGlobalRoutines.Check_Arrays();
                        // Set dbTemp = OpenDatabase(strREDatabase, False, False, ";PWD=" & DATABASEPASSWORD)
                        rsTemp.OpenRecordset("SELECT MIN(RSAccount) as MinAccount FROM Master", modGlobalVariables.strREDatabase);
                        // TODO Get_Fields: Field [minaccount] not found!! (maybe it is an alias?)
                        modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(rsTemp.Get_Fields("minaccount"));
                        modGlobalVariables.Statics.gintMaxAccounts = FCConvert.ToInt32(modGlobalRoutines.GetMaxAccounts());
                        // gintMaxCards = GetRecordCount("Master", "RSAccount")
                        modGlobalVariables.Statics.CancelledIt = false;
                        Frmcalcshowassessment.InstancePtr.Show();
                        FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                        break;
                    }
                case 2:
                    {
                        if (MessageBox.Show("Committing values will replace last years billing figures.", "Commit to Billing Values", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                        {
                            modGlobalRoutines.transfervaluestobillingamounts();
                        }
                        else
                        {
                            //this.GRID.Focus();
                        }
                        break;
                    }
                case 3:
                    {
                        if (modGlobalVariables.Statics.boolRegRecords)
                        {
                            //FC:FINAL:MSH - i.issue #1070: call form as tab
                            //modcalcexemptions.NewCalcExemptions();
                            frmExemptList.InstancePtr.Init();
                        }
                        else
                        {
                            MessageBox.Show("This option is not available for sales records.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        break;
                    }
                case 4:
                    {
                        frmaudit.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 5:
                    {
                        int intTNum = 0;
                        if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                        {
                            intTNum = frmPickRegionalTown.InstancePtr.Init(1, false, "Choose the town to show the summary for");
                            if (intTNum < 0)
                                intTNum = 0;
                        }
                        else
                        {
                            intTNum = 0;
                        }
                        rptREPPAudit.InstancePtr.Init(intTNum);
                        // Call frmReportViewer.Init(rptREPPAudit)
                        break;
                    }
                case 6:
                    {
                        if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                        {
                            modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = 1;
                        }
                        frmTaxRateCalc.InstancePtr.Show(App.MainForm);
                        break;
                    }
                //case 7:
                //    {
                        
                //        clsDRWrapper clsLoad = new clsDRWrapper();
                //        bool boolTemp = false;
                //        int lngTown = 0;
                //        boolTemp = false;
                //        if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                //        {
                //            lngTown = frmPickRegionalTown.InstancePtr.Init(1);
                //            modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = lngTown;
                //        }
                //        clsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0)  = " + FCConvert.ToString(lngTown), modGlobalVariables.strREDatabase);
                //        if (!clsLoad.EndOfFile())
                //        {
                //            if (Conversion.Val(clsLoad.Get_Fields_Int32("taxyear")) < DateTime.Today.Year)
                //            {
                //                boolTemp = true;
                //            }
                //        }
                //        else
                //        {
                //            boolTemp = true;
                //        }
                //        if (!boolTemp)
                //        {
                //            frmMVRViewer.InstancePtr.Show(App.MainForm);
                //        }
                //        else
                //        {
                //            MessageBox.Show("The tax rate calculation form has not been filled out and saved for this year yet" + NEWLINE + "You must complete this form first before filling out the rest of the MVR", "Tax Rate Calculator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //            frmTaxRateCalc.InstancePtr.Show(App.MainForm);
                //        }
                //        break;
                //    }
                case 8:
                    {
                        frmStatus.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 9:
                    {
                        rptNewValuationReport.InstancePtr.Init();
                        break;
                    }
                case 10:
                    {
                        frmGetValuationComparisonInfo.InstancePtr.Init();
                        break;
                    }
                case 11:
                    {
                        //SetMenuOptions("MAIN");
                        break;
                    }
                case 7:
                    int townId = 0;
                    var taxYear = 2020;
                    if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        townId = frmPickRegionalTown.InstancePtr.Init(1);
                        modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = townId;
                    }

                    var rsLoad = new clsDRWrapper();
                    var needTaxRateCalc = false;
                    rsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0)  = " + FCConvert.ToString(townId), modGlobalVariables.strREDatabase);
                    if (!rsLoad.EndOfFile())
                    {
                        if (Conversion.Val(rsLoad.Get_Fields_Int32("taxyear")) < DateTime.Today.Year)
                        {
                            needTaxRateCalc = true;
                        }
                    }
                    else
                    {
                        needTaxRateCalc = true;
                    }
                    if (!needTaxRateCalc)
                    {
                        StaticSettings.GlobalCommandDispatcher.Send(new ShowMunicipalValuationReturn(townId, taxYear));
                    }
                    else
                    {
                        MessageBox.Show("The tax rate calculation form has not been filled out and saved for this year yet\nYou must complete this form first before filling out the rest of the MVR", "Tax Rate Calculator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        frmTaxRateCalc.InstancePtr.Show(App.MainForm);
                    }


                    
                    break;
                case 13:
                    frmAltMunicipalValuationReturn.InstancePtr.Show();
                    break;
            }
            //end switch
            return CalculateActions;
        }

        private void CalculateCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Compute]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Batch Calculation", "SYSTEM", parentItem, true, modGlobalVariables.CNSTSUMMARY);
            AddMenuItem(ref CurRow, 2, "Commit to Billing Values", "SYSTEM", parentItem, true, modGlobalVariables.CNSTTRANSFERTOBILLING);
            AddMenuItem(ref CurRow, 3, "Calculate Exemptions", "SYSTEM", parentItem, true, modGlobalVariables.CNSTCALCEXEMPTION);
            AddMenuItem(ref CurRow, 4, "Audit of Billing Amounts", "SYSTEM", parentItem, true, modGlobalVariables.CNSTAUDITBILLING);
            AddMenuItem(ref CurRow, 5, "RE / PP Audit Summary", "SYSTEM", parentItem, true, modGlobalVariables.CNSTAUDITBILLING);
            AddMenuItem(ref CurRow, 6, "Tax Rate Calculator", "SYSTEM", parentItem, true, modGlobalVariables.CNSTTAXRATECALCULATOR);
            AddMenuItem(ref CurRow, 7, "Municipal Valuation Return", "SYSTEM", parentItem, true);
            AddMenuItem(ref CurRow, 8, "Status", "SYSTEM", parentItem, true);
            AddMenuItem(ref CurRow, 9, "New Valuation Estimate", "SYSTEM", parentItem, true, 0, "", "Estimate the amount of new valuation by comparing calculated values with billing values");
            //if (StaticSettings.gGlobalSettings.IsHarrisStaffComputer)
            //{
               
            //    AddMenuItem(ref CurRow, 13, "Alternate Municipal Valuation Return", "SYSTEM", parentItem, true, 0, "",
            //        "");
            //}

            // Call AddMenuItem(CurRow, 10, "New Valuation Analysis", 0, "", "Estimate the amount of new valuation by comparing values from two different commitment years")
            //AddMenuItem(ref CurRow, 11, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        private void DataManipulationActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmGlobalChanges.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        frmFactorBillingValues.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 3:
                    {
                        if (MessageBox.Show("Warning: This should only be performed on new conversions or when upgrading to full assessing" + NEWLINE + "Do you want to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmLandTypesFromAcres.InstancePtr.Show(App.MainForm);
                        }
                        break;
                    }
                case 4:
                    {
                        ReRoundBillingValues();
                        break;
                    }
                case 5:
                    {
                        //SetMenuOptions("FILE");
                        break;
                    }
            }
            //end switch
        }

        private void ReRoundBillingValues()
        {
            modGlobalFunctions.AddCYAEntry_8("RE", "Re-rounded billing values");
            clsDRWrapper rsLoad = new clsDRWrapper();
            rsLoad.OpenRecordset("select lastlandval , lastbldgval , rssoftvalue , rshardvalue , rsmixedvalue , rsothervalue from master where not rsdeleted = 1 order by rsaccount,rscard", modGlobalVariables.strREDatabase);
            if (!modREMain.Statics.boolShortRealEstate)
            {
                //Application.DoEvents();
                while (!rsLoad.EndOfFile())
                {
                    rsLoad.Edit();
                    rsLoad.Set_Fields("lastlandval", modGlobalRoutines.RERound_2(rsLoad.Get_Fields_Int32("lastlandval")));
                    rsLoad.Set_Fields("lastbldgval", modGlobalRoutines.RERound_2(rsLoad.Get_Fields_Int32("lastbldgval")));
                    rsLoad.Update();
                    rsLoad.MoveNext();
                }
            }
            else
            {
                // add up
                while (!rsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    rsLoad.Edit();
                    rsLoad.Set_Fields("lastlandval", modGlobalRoutines.RERound_2(rsLoad.Get_Fields_Int32("rssoftvalue") + rsLoad.Get_Fields_Int32("rshardvalue") + rsLoad.Get_Fields_Int32("rsmixedvalue") + rsLoad.Get_Fields_Int32("rsothervalue")));
                    rsLoad.Set_Fields("lastbldgval", modGlobalRoutines.RERound_2(rsLoad.Get_Fields_Int32("lastbldgval")));
                    rsLoad.Update();
                    rsLoad.MoveNext();
                }
            }
            MessageBox.Show("Billing values were rounded." + NEWLINE + "Exempt values may need to be recalculated.", "Billing Values Rounded", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void AccountActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        modGlobalRoutines.UndeleteMasterRecord();
                        break;
                    }
                case 2:
                    {
                        string STRResp = "";
                        STRResp = Interaction.InputBox("Enter the account to fix", "Fix Account", null/*, -1, -1*/);
                        if (Conversion.Val(STRResp) > 0)
                        {
                            modGlobalRoutines.RenumberCards_2(FCConvert.ToInt32(Conversion.Val(STRResp)));
                            MessageBox.Show("Fix complete", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        //this.GRID.Focus();
                        break;
                    }
                //case 3:
                //    {
                //        frmLockUnlock.InstancePtr.Show(App.MainForm);
                //        break;
                //    }
                case 4:
                    {
                        DeleteAcctPermanently();
                        break;
                    }
                case 5:
                    {
                        DeleteDwellingsPermanently();
                        break;
                    }
                case 6:
                    {
                        DeleteOutbuildingsPermanently();
                        break;
                    }
                case 7:
                    {
                        DeleteCommercialsPermanently();
                        break;
                    }
                case 8:
                    {
                        //SetMenuOptions("FILE");
                        break;
                    }
            }
            //end switch
        }

        private void DeleteDwellingsPermanently()
        {
            try
            {
                // On Error GoTo ErrorHandler
                object lngReturn = 0;
                if (MessageBox.Show("This will permanently delete all dwelling information" + NEWLINE + "Do you want to continue?", "Delete Dwelling Information?", MessageBoxButtons.YesNo, MessageBoxIcon.Hand) == DialogResult.Yes)
                {
                    if (frmInput.InstancePtr.Init(ref lngReturn, "Delete Dwelling Info", "Please contact TRIO for today's security code", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber))
                    {
                        // test this code with RE one day code
                        if (modModDayCode.CheckWinModCode(FCConvert.ToInt32(lngReturn), "RE"))
                        {
                            modGlobalFunctions.AddCYAEntry_8("RE", "Deleted all dwelling information");
                            clsDRWrapper rsSave = new clsDRWrapper();
                            rsSave.Execute("delete from dwelling", modGlobalVariables.strREDatabase);
                            rsSave.Execute("update master set RSDWELLINGCODE = 'N' where rsdwellingcode = 'Y'", modGlobalVariables.strREDatabase);
                            MessageBox.Show("Dwelling information deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            //Application.DoEvents();
                            MessageBox.Show("Invalid Code", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + NEWLINE + "In DeleteDwellingsPermanently", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void DeleteOutbuildingsPermanently()
        {
            try
            {
                // On Error GoTo ErrorHandler
                object lngReturn = 0;
                if (MessageBox.Show("This will permanently delete all outbuilding information" + NEWLINE + "Do you want to continue?", "Delete Outbuilding/Additions Information?", MessageBoxButtons.YesNo, MessageBoxIcon.Hand) == DialogResult.Yes)
                {
                    if (frmInput.InstancePtr.Init(ref lngReturn, "Delete Outbuilding Info", "Please contact TRIO for today's security code", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber))
                    {
                        if (modModDayCode.CheckWinModCode(FCConvert.ToInt32(lngReturn), "RE"))
                        {
                            modGlobalFunctions.AddCYAEntry_8("RE", "Deleted all outbuilding/additions information");
                            clsDRWrapper rsSave = new clsDRWrapper();
                            rsSave.Execute("delete from outbuilding", modGlobalVariables.strREDatabase);
                            rsSave.Execute("update master set RSoutbuildingCODE = 'N'", modGlobalVariables.strREDatabase);
                            MessageBox.Show("Outbuilding/Additions information deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            //Application.DoEvents();
                            MessageBox.Show("Invalid Code", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + NEWLINE + "In DeleteOutbuildingsPermanently", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void DeleteCommercialsPermanently()
        {
            try
            {
                // On Error GoTo ErrorHandler
                object lngReturn = 0;
                if (MessageBox.Show("This will permanently delete all commercial information" + NEWLINE + "Do you want to continue?", "Delete Commercial Data?", MessageBoxButtons.YesNo, MessageBoxIcon.Hand) == DialogResult.Yes)
                {
                    if (frmInput.InstancePtr.Init(ref lngReturn, "Delete Commercial Info", "Please contact TRIO for today's security code", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber))
                    {
                        if (modModDayCode.CheckWinModCode(FCConvert.ToInt32(lngReturn), "RE"))
                        {
                            modGlobalFunctions.AddCYAEntry_8("RE", "Deleted all commercial information");
                            clsDRWrapper rsSave = new clsDRWrapper();
                            rsSave.Execute("Delete From commercial", modGlobalVariables.strREDatabase);
                            rsSave.Execute("update master set rsdwellingcode = 'N' where rsdwellingcode = 'C'", modGlobalVariables.strREDatabase);
                            MessageBox.Show("Commercial information deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            //Application.DoEvents();
                            MessageBox.Show("Invalid Code", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + NEWLINE + "In DeleteCommercialsPermanently", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void DeleteAcctPermanently()
        {
            object lngAcct = 0;
            clsDRWrapper clsTemp = new clsDRWrapper();
            object lngCode = 0;
            try
            {
                // On Error GoTo ErrorHandler
                if (frmInput.InstancePtr.Init(ref lngAcct, "Delete Account", "Enter Account to Delete", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber, "", true))
                {
                    clsTemp.OpenRecordset("select rsaccount from master where rsaccount = " + FCConvert.ToString(lngAcct) + " and rscard = 1", modGlobalVariables.strREDatabase);
                    if (!clsTemp.EndOfFile())
                    {
                        if (MessageBox.Show("This will permanently delete the data for this account" + NEWLINE + "You will not be able to get the data back" + NEWLINE + "If any other programs use real estate accounts, deleting this account might cause problems" + NEWLINE + "Are you sure you want to delete this account?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            if (frmInput.InstancePtr.Init(ref lngCode, "RE One Day Code", "Call TRIO for today's code", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber, "", true))
                            {
                                if (modModDayCode.CheckWinModCode(FCConvert.ToInt32(lngCode), "RE"))
                                {
                                    modGlobalFunctions.AddCYAEntry_26("RE", "Permanently Deleted", "Account " + FCConvert.ToString(lngAcct));
                                    clsTemp.Execute("delete from master where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
                                    clsTemp.Execute("delete from dwelling where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
                                    clsTemp.Execute("delete from outbuilding where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
                                    clsTemp.Execute("delete from commercial where rsaccount = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
                                    clsTemp.Execute("delete from summrecORD where srecordnumber = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
                                    clsTemp.Execute("delete from bookpage where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
                                    clsTemp.Execute("delete from maplot where account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strREDatabase);
                                    MessageBox.Show("Account Deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                                else
                                {
                                    MessageBox.Show("Invalid Code", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Account " + FCConvert.ToString(lngAcct) + " could not be found", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + NEWLINE + "In DeleteAcctPermanently", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void DataManipulationCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Data Manipulation]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Global Changes", "DATAMANIPULATION", parentItem, true, 0, "", "Make global changes to all or a subset of the real estate files");
            AddMenuItem(ref CurRow, 2, "Factor Values", "DATAMANIPULATION", parentItem, true, 0, "", "Create overrides that are a given factor times the billing values");
            AddMenuItem(ref CurRow, 3, "Create Land Entries", "DATAMANIPULATION", parentItem, true, 0, "", "Create land entries from acreage values");
            if (modREMain.Statics.boolShortRealEstate)
            {
                AddMenuItem(ref CurRow, 4, "Round Billing Values", "DATAMANIPULATION", parentItem, true);
            }
            //AddMenuItem(ref CurRow, 5, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        private object AccountCaptions(FCMenuItem parentItem)
        {
            object AccountCaptions = null;
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Account Functions]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Undelete an Account", "ACCOUNTFUNCTIONS", parentItem, true, modGlobalVariables.CNSTUNDELETEACCOUNTS, "", "Re-activate an account marked as deleted");
            AddMenuItem(ref CurRow, 2, "Fix an Account", "ACCOUNTFUNCTIONS", parentItem, true, 0, "", "Fixes some errors in accounts");
            //AddMenuItem(ref CurRow, 3, "Locked Accounts", "ACCOUNTFUNCTIONS", parentItem, true, modGlobalVariables.CNSTCALCEXEMPTION, "", "Lock / unlock accounts. Locking prevents accounts from being undeleted");
            AddMenuItem(ref CurRow, 4, "Permanently Delete Account", "ACCOUNTFUNCTIONS", parentItem, true, modGlobalVariables.CNSTDELCARDS, "", "Permanently delete an account");
            AddMenuItem(ref CurRow, 5, "Delete all Dwelling Records", "ACCOUNTFUNCTIONS", parentItem, true, modGlobalVariables.CNSTDELCARDS);
            AddMenuItem(ref CurRow, 6, "Delete all Outbuilding Records", "ACCOUNTFUNCTIONS", parentItem, true, modGlobalVariables.CNSTDELCARDS);
            AddMenuItem(ref CurRow, 7, "Delete all Commercial Records", "ACCOUNTFUNCTIONS", parentItem, true, modGlobalVariables.CNSTDELCARDS);
            //AddMenuItem(ref CurRow, 8, "Return to File Maintenance", 0, "X");
            AutoSizeMenu();
            return AccountCaptions;
        }
       
        private void PictureCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Picture Maintenance]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            //AddMenuItem(ref CurRow, 1, "Auto Add Pictures", "PICTURE", parentItem, true, modGlobalVariables.CNSTADDPICSKETCH, "", "Automatically add pictures to multiple accounts based on filename");          
            AddMenuItem(ref CurRow, 3, "Delete Duplicate Entries", "PICTURE", parentItem, true, modGlobalVariables.CNSTADDPICSKETCH);
            AddMenuItem(ref CurRow,5,"Get Upload Utility","PICTURE",parentItem,true,modGlobalVariables.CNSTADDPICSKETCH,"","Download and install the real estate picture upload utility");
            AutoSizeMenu();
        }

        public void FileMaintCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [File Maintenance]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Customize", "FILE", parentItem, true, modGlobalVariables.CNSTREALESTATESETTINGS);
            AddMenuItem(ref CurRow, 2, "Check Database Structure", "FILE", parentItem, true, 0, "", "Run a diagnostic to ensure that the database has the most current structure");
            FCMenuItem newItem = AddMenuItem(ref CurRow, 3, "Account Functions", "FILE", parentItem, false);
            AccountCaptions(newItem);
            newItem = AddMenuItem(ref CurRow, 4, "Account Associations", "FILE", parentItem, false, 0, "", "Mortgage holders and account groups");
            AssociationCaptions(newItem);
            // Call AddMenuItem(CurRow, 5, "Convert to DOS for Billing", 0, "", "Convert Master,mortgage and some cost files to DOS for users with TRIO DOS billing")
            AddMenuItem(ref CurRow, 6, "Process Pending Activity", "FILE", parentItem, true, 0, "", "Process or edit pending changes and pending transfers");
            newItem = AddMenuItem(ref CurRow, 7, "Data Functions", "FILE", parentItem, false);
            DataManipulationCaptions(newItem);
            if (modREMain.Statics.boolHandHeld)
            {
                AddMenuItem(ref CurRow, 8, "Update Pocket PC Install", "FILE", parentItem, true, 0, "", "Install Field Assessor on a Pocket PC");
            }
            // Call AddMenuItem(CurRow, 9, "Convert Ref1 to Book Page", 0, "", "Convert book and page references entered in the reference 1 field into entries in the book and page table")
            //FC:FINAL:SBE - #1824 -  allow user to enter in archive mode, in order to go back to current year
            //if (!modGlobalVariables.Statics.boolInArchiveMode)
            //{
            AddMenuItem(ref CurRow, 10, "Archives", "FILE", parentItem, true, modGlobalVariables.CNSTRUNARCHIVES, "", "Create or run archived data");
            //}
            newItem = AddMenuItem(ref CurRow, 11, "Picture Maintenance", "FILE", parentItem, false);
            PictureCaptions(newItem);
            AddMenuItem(ref CurRow, 12, "Previous Assessments", "FILE", parentItem, true, 0, "", "View historical assessment data for accounts");
            AddMenuItem(ref CurRow, 13, "Create Database Extract", "FILE", parentItem, true, 0, "", "Create a comma delimited file with user chosen data fields");
            if (modGlobalConstants.Statics.gboolCL)
            {
                AddMenuItem(ref CurRow, 14, "Edit Tax Acquired", "FILE", parentItem, true, 0, "", "Manually remove accounts from tax acquired status");
            }
            AddMenuItem(ref CurRow, 16, "Purge Audit Archive", "FILE", parentItem, true);
            //AddMenuItem(ref CurRow, 17, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        private void MDIForm_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            // If UnloadMode = vbAppTaskManager Then
            // Cancel = True
            // End If
        }

        private void MDIParent_Resize(object sender, System.EventArgs e)
        {
            AutoSizeMenu(20);
        }

        private void MDIForm_Unload(object sender, FCFormClosingEventArgs e)
        {
            modGNBas.Statics.gboolExiting = true;
            modReplaceWorkFiles.EntryFlagFile(true, "RE", true);
            if (!(modGlobalVariables.Statics.boolFromBilling || boolDontShellToGE))
            {
                if (modGlobalVariables.Statics.boolInArchiveMode && modGlobalVariables.Statics.gstrArchiveParentDir != string.Empty)
                {
                    //Environment.CurrentDirectory = modGlobalVariables.Statics.gstrArchiveParentDir;
                }
                //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
                //Interaction.Shell(FCFileSystem.Statics.UserDataFolder + "\\Twgnenty.exe", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
                App.MainForm.OpenModule("TWGNENTY");
            }
            else
            {
                // For Each fm In Forms
                // Unload fm
                // Next fm
            }
            Application.Exit();
        }

        private void mnuBudgetaryHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twbd0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuCashReceiptsHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twcr0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuClerkHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twck0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuCodeEnforcementHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twce0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuEnhanced911Help_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twe90000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuExportODonnell_Click(object sender, System.EventArgs e)
        {
            frmExportODonnell.InstancePtr.Init();
        }

        private void mnuFExit_Click(object sender, System.EventArgs e)
        {
            //this.Unload();
        }

        private void mnuFixedAssetsHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twfa0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuGeneralEntryHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twgn0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuHAbout_Click(object sender, System.EventArgs e)
        {
            frmAbout.InstancePtr.Show();
        }

        private void mnuImportBriteside_Click(object sender, System.EventArgs e)
        {
            frmBritesideImport.InstancePtr.Init();
        }

        private void mnuImportJFRyan_Click(object sender, System.EventArgs e)
        {
            frmImportJFRyan.InstancePtr.Show(App.MainForm);
        }

        private void mnuImportODonnell_Click(object sender, System.EventArgs e)
        {
            frmImportODonnell.InstancePtr.Init();
        }

        private void mnuMenucolor_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: red As Variant, short --> As int	OnWriteFCConvert.ToInt16(
            // vbPorter upgrade warning: blue As Variant, short --> As int	OnWriteFCConvert.ToInt16(
            int red, blue, green;
            clsDRWrapper rsColorInfo = new clsDRWrapper();
            //- CommonDialog1.CancelError = true;
            try
            {
                // On Error GoTo ErrorHandler
                // Set the Flags property.
                // CommonDialog1.Flags = vbPorterConverter.cdlCCRGBInit	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                // Display the Color dialog box.
                CommonDialog1.Show();
                red = RedFromRGB(CommonDialog1.Color);
                green = GreenFromRGB(CommonDialog1.Color);
                blue = BlueFromRGB(CommonDialog1.Color);
                rsColorInfo.OpenRecordset("SELECT * FROM GlobalVariables", modGlobalVariables.Statics.strGNDatabase);
                if (rsColorInfo.EndOfFile() != true && rsColorInfo.BeginningOfFile() != true)
                {
                    rsColorInfo.Edit();
                }
                else
                {
                    rsColorInfo.AddNew();
                }
                rsColorInfo.Set_Fields("MenuBackColor", Information.RGB(FCConvert.ToInt16(red), FCConvert.ToInt16(green), FCConvert.ToInt16(blue)));
                rsColorInfo.Update();
                //GRID.BackColor = ColorTranslator.FromOle(Information.RGB(FCConvert.ToInt16(red), FCConvert.ToInt16(green), FCConvert.ToInt16(blue)));
                //GRID.BackColorAlternate = ColorTranslator.FromOle(Information.RGB(FCConvert.ToInt16(red), FCConvert.ToInt16(green), FCConvert.ToInt16(blue)));
                return;
            }
            catch
            {
                // ErrorHandler:
            }
        }

        private void mnuMotorVehicleRegistrationHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twmv0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuNewSharonImport_Click(object sender, System.EventArgs e)
        {
            frmImportNewSharon.InstancePtr.Init();
        }

        //private void mnuOMaxForms_Click(object sender, System.EventArgs e)
        //{
        //	Registry.RegCreateKeyWrp(FCConvert.ToInt32(Registry.HKEY_LOCAL_MACHINE), modGlobalConstants.REGISTRYKEY, 0);
        //	if (mnuOMaxForms.Checked)
        //	{
        //		modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "Maximize Forms", "False");
        //	}
        //	else
        //	{
        //		modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "Maximize Forms", "True");
        //	}
        //	modGlobalConstants.Statics.boolMaxForms = !modGlobalConstants.Statics.boolMaxForms;
        //	mnuOMaxForms.Checked = !mnuOMaxForms.Checked;
        //}
        //
        // Private Sub AutoSizeMenu(intLabels As Integer)
        // Dim intCounter As Integer
        // Dim FullRowHeight As Long 'this will store the height of the labels with text
        //
        // intLabels = 20
        //
        // GRID.RowHeight(0) = 0       'the first row in the grid is height = 0
        // If intLabels <= 10 Then         'if there are less than 10 labels,
        // FullRowHeight = Grid.Height / 19  'then adjust the labels, the 15 is
        // calculated by 10 labels at full height and 30 labels at 1/3 the height
        // Else
        // this will adjust the full row height by using the amount of rows that have text
        // FullRowHeight = Grid.Height / (intLabels + (((40 - intLabels) / 3) / 2))
        // FullRowHeight = Grid.Height / (intLabels + (((20 - intLabels) / 3) / 2))
        // End If
        // FullRowHeight = GRID.Height / 20
        //
        // For intCounter = 1 To 40    'this just assigns the row height
        // If Trim(GRID.TextMatrix(intCounter, 1)) = vbNullString And intCounter < (LabelNumber(Asc("X")) * 2) Then
        // Grid.RowHeight(intcounter) = (FullRowHeight / 3) - 55
        // -50 is just a small adjustment
        // GRID.RowHeight(intCounter) = 1
        // Else
        // Grid.RowHeight(intcounter) = FullRowHeight + 15
        // GRID.RowHeight(intCounter) = FullRowHeight
        // +20 minor adjustment to help with two row labels
        // End If
        // Next
        // End Sub
        private void AutoSizeMenu(int intRows = 0)
        {
            //int intCounter;
            //// vbPorter upgrade warning: FullRowHeight As int	OnWriteFCConvert.ToDouble(
            //int FullRowHeight;
            //// this will store the height of the labels with text
            //int intLabels;
            //intLabels = 20;
            //// Grid.RowHeight(0) = 0       'the first row in the grid is height = 0
            //FullRowHeight = FCConvert.ToInt32(GRID.Height / (intLabels + (((21 - intLabels) / 3.0) / 2)));
            //for (intCounter = 1; intCounter <= 20; intCounter++)
            //{
            //	// this just assigns the row height
            //	// If Grid.TextMatrix(intCounter, 1) = vbNullString Then
            //	// Grid.RowHeight(intCounter) = (FullRowHeight / 3) - 55
            //	// -50 is just a small adjustment
            //	// Else
            //	GRID.RowHeight(intCounter, FullRowHeight + 15);
            //	// +20 minor adjustment to help with two row labels
            //	// End If
            //}
        }

        private void mnuPayrollHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twpy0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuPersonalPropertyHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twpp0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuPrintForms_Click(object sender, System.EventArgs e)
        {
            frmScreenPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal);
            //Support.ZOrder(this, 1);
        }

        private void mnuRealEstateHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twre0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuRedbookHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twrb0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuTaxBillingHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twbl0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuTaxCollectionsHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twcl0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuUtilityBillingHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twut0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        private void mnuVoterRegistrationHelp_Click(object sender, System.EventArgs e)
        {
            modGlobalFunctions.HelpShell("twvr0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
        }

        //private void GRID_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	double i;
        //	// avoid extra work
        //	if (GRID.MouseRow < 1)
        //		return;
        //	ToolTip1.SetToolTip(GRID, GRID.TextMatrix(GRID.MouseRow, 3));
        //	if (GRID.MouseRow == Statics.R)
        //		return;
        //	Statics.R = GRID.MouseRow;
        //	if (Statics.R < 1)
        //		return;
        //	// move selection (even with no click)
        //	GRID.Select(Statics.R, 1);
        //	// Grid.Select R, 0
        //	// remove cursor image
        //	// Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1, 0) = ImageList1.ListImages("Clear").Picture
        //	GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, GRID.Rows - 1, 0, null);
        //	// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, Grid.Rows - 1, 1) = TRIOCOLORGRAYBACKGROUND
        //	GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, GRID.Rows - 1, 1, GRID.BackColor);
        //	// show cursor image if there is some text in column 5
        //	if (Statics.R == 0)
        //		return;
        //	if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Statics.R, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION || GRID.TextMatrix(Statics.R, 1) == string.Empty)
        //	{
        //		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
        //		return;
        //	}
        //	else
        //	{
        //		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
        //	}
        //	// Call Grid.Select(R, 0, R, 1)
        //	GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, Statics.R, 0, ImageList1.Images["Left"]);
        //	GRID.Select(Statics.R, 1);
        //}
        // vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(	OnRead(string)
        //private void AddMenuItem(ref int intRow, short intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "") { AddMenuItem(ref intRow, ref intCode, ref strCaption, lngFCode, strForceOption, strToolTip); }
        //private void AddMenuItem(ref int intRow, short intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "") { AddMenuItem(ref intRow, ref intCode, ref strCaption, lngFCode, strForceOption, strToolTip); }
        //private void AddMenuItem(ref int intRow, short intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "") { AddMenuItem(ref intRow, ref intCode, ref strCaption, lngFCode, strForceOption, strToolTip); }
        //private void AddMenuItem(ref int intRow, short intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "") { AddMenuItem(ref intRow, ref intCode, ref strCaption, lngFCode, strForceOption, strToolTip); }
        private FCMenuItem AddMenuItem(ref int intRow, short intCode, string strCaption, string strMenu, FCMenuItem parentMenu = null, bool addMenuAction = false, int lngFCode = 0, string strForceOption = "", string strToolTip = "", string imageKey = "")
        {
            // vbPorter upgrade warning: strOption As string	OnWrite(string, short)
            string strOption;
            strOption = strForceOption;
            if (strOption == string.Empty)
            {
                if (intRow < 10)
                {
                    strOption = FCConvert.ToString(intRow);
                }
                else
                {
                    switch (intRow)
                    {
                        case 10:
                            {
                                strOption = "A";
                                break;
                            }
                        case 11:
                            {
                                strOption = "B";
                                break;
                            }
                        case 12:
                            {
                                strOption = "C";
                                break;
                            }
                        case 13:
                            {
                                strOption = "D";
                                break;
                            }
                        case 14:
                            {
                                strOption = "E";
                                break;
                            }
                        case 15:
                            {
                                strOption = "F";
                                break;
                            }
                        case 16:
                            {
                                strOption = "G";
                                break;
                            }
                        case 17:
                            {
                                strOption = "H";
                                break;
                            }
                        case 18:
                            {
                                strOption = "I";
                                break;
                            }
                        case 19:
                            {
                                strOption = "J";
                                break;
                            }
                        case 20:
                            {
                                strOption = "K";
                                break;
                            }
                        case 21:
                            {
                                strOption = "L";
                                break;
                            }
                        case 22:
                            {
                                strOption = "M";
                                break;
                            }
                    }
                    //end switch
                }
            }
            strOption += ". " + strCaption;
            //GRID.TextMatrix(intRow, 1, strOption);
            //GRID.TextMatrix(intRow, 2, FCConvert.ToString(intCode));
            //GRID.TextMatrix(intRow, 3, strToolTip);
            //GRID.RowData(intRow, true);
            bool boolDisable = false;
            if (lngFCode != 0)
            {
                boolDisable = !modSecurity.ValidPermissions(this, lngFCode, false);
                modMDIParent.EnableDisableMenuOption(!boolDisable, intRow);
            }
            //string imageKey = "";
            FCMenuItem newItem = null;
            string strAction = string.Empty;
            if (addMenuAction)
            {
                //FC:FINAL:MSH - i.issue #1121: for correct handling of menu item in FileMaintActions need to add 'intCode' instead of 'intRow'
                //strAction = "MenuActions:" + intRow;
                strAction = "MenuActions:" + intCode;
            }
            if (parentMenu == null)
            {
                newItem = App.MainForm.NavigationMenu.Add(strCaption, strAction, strMenu, !boolDisable, 1, imageKey);
            }
            else
            {
                newItem = parentMenu.SubItems.Add(strCaption, strAction, strMenu, !boolDisable, parentMenu.Level + 1, imageKey);
            }
            intRow += 1;
            return newItem;
        }

        //private void GRID_KeyDownEvent(object sender, KeyEventArgs e)
        //{
        //	Keys KeyCode = e.KeyCode;
        //	int intCounter;
        //	int intPlaceHolder = 0;
        //	if (KeyCode >= Keys.NumPad0 && KeyCode <= Keys.NumPad9)
        //	{
        //		KeyCode -= 48;
        //	}
        //	if ((KeyCode == Keys.Down) || (KeyCode == Keys.Up))
        //	{
        //		intPlaceHolder = GRID.Row;
        //		if (KeyCode == Keys.Down)
        //		{
        //			KeepGoing:
        //			if ((GRID.Row + 1) <= (GRID.Rows - 1))
        //			{
        //				GRID.Row += 1;
        //				if (GRID.TextMatrix(GRID.Row, 1) == string.Empty)
        //				{
        //					GRID.Row -= 1;
        //				}
        //				else if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
        //				{
        //					goto KeepGoing;
        //				}
        //			}
        //		}
        //		else
        //		{
        //			KeepGoingUP:
        //			if ((GRID.Row - 1) >= 1)
        //			{
        //				GRID.Row -= 1;
        //				if (GRID.Row < 1)
        //					GRID.Row = 1;
        //				if (GRID.TextMatrix(GRID.Row, 1) == string.Empty)
        //				{
        //				}
        //				else if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
        //				{
        //					goto KeepGoingUP;
        //				}
        //			}
        //			else
        //			{
        //				GRID.Row = intPlaceHolder;
        //			}
        //		}
        //		// Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1) = ImageList1.ListImages("Clear").Picture
        //		GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, GRID.Rows - 1, null);
        //		GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, GRID.Row, 0, ImageList1.Images["Left"]);
        //		KeyCode = 0;
        //		return;
        //	}
        //	for (intCounter = 1; intCounter <= GRID.Rows - 1; intCounter++)
        //	{
        //		if (Strings.Left(FCConvert.ToString(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)), 1) == FCConvert.ToString(Convert.ToChar(KeyCode)))
        //		{
        //			if (!FCConvert.ToBoolean(GRID.RowData(intCounter)))
        //				return;
        //			// MenuActions (intCounter)
        //			MenuActions(FCConvert.ToInt32(Conversion.Val(GRID.TextMatrix(intCounter, 2))));
        //			KeyCode = 0;
        //			break;
        //		}
        //	}
        //}
        // Private Sub MenuActions(lngMenuRow As Long)
        // accepts the menu row and calls the appropriate routine
        // Dim lngRow As Integer
        // lngRow = lngMenuRow
        // If Not boolShortRealEstate Then
        // Select Case UCase(Grid.Tag)
        // Case "DATAMANIPULATION"
        // Call DataManipulationActions(lngRow)
        // Case "ACCOUNTFUNCTIONS"
        // Call AccountActions(lngRow)
        // Case "PROPERTYCOST"
        // Call PropertyCostActions(lngRow)
        // Case "MAIN"
        // Call LoadEntryMenuOptions(9)
        // Call MainActions(lngRow)
        // Case "ASSOCIATION"
        // Call AssociationActions(lngRow)
        // Case "IMPORTEXPORT"
        // Call ImportExportActions(lngRow)
        // Case "SYSTEM"
        // Call LoadEntryMenuOptions(10)
        // Call CalculateActions(lngRow)
        // Case "FILE"
        // Call LoadEntryMenuOptions(12)
        // FileMaintActions (lngRow)
        // Case "COSTFILEMAIN"
        // Call LoadEntryMenuOptions(4)
        // Call CostFileMainActions(lngRow)
        // Case "COSTFILE"
        // Call LoadEntryMenuOptions(12)
        // Call CostFileActions(lngRow)
        // Case "DWELLINGCOST"
        // Call DwellingCostActions(lngRow)
        // Case "INCOMEUPDATE"
        // Call IncomeUpdateActions(lngRow)
        // Case "PRINTMENU1"
        // Call PrintActions(lngRow)
        // Case "SALESMENU"
        // Call SalesMenuActions(lngRow)
        // Case "PICTURE"
        // PictureActions (lngRow)
        // End Select
        // Else
        // Select Case UCase(Grid.Tag)
        // Case "MAIN"
        // Call ShortMainActions(lngRow)
        // Case "DATAMANIPULATION"
        // Call DataManipulationActions(lngRow)
        // Case "IMPORTEXPORT"
        // Call ShortImportExportActions(lngRow)
        // Case "SYSTEM"
        // Call CalculateActions(lngRow)
        // Case "ASSOCIATION"
        // Call AssociationActions(lngRow)
        // Case "FILE"
        // FileMaintActions (lngRow)
        // Case "COSTFILE"
        // Call ShortCostFileActions(lngRow)
        // Case "PRINTMENU1"
        // Call PrintActions(lngRow)
        // Case "ACCOUNTFUNCTIONS"
        // Call AccountActions(lngRow)
        // End Select
        // End If
        // End Sub
        //private void GRID_KeyPressEvent(object sender, KeyPressEventArgs e)
        //{
        //	if (e.KeyChar != 13)
        //		return;
        //	if (!FCConvert.ToBoolean(GRID.RowData(GRID.Row)))
        //		return;
        //	// MenuActions (Grid.Row)
        //	MenuActions(FCConvert.ToInt32(Conversion.Val(GRID.TextMatrix(GRID.Row, 2))));
        //}

        //private void GRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	int lngRow;
        //	lngRow = GRID.MouseRow;
        //	if (lngRow < 0)
        //		return;
        //	if (!FCConvert.ToBoolean(GRID.RowData(lngRow)))
        //		return;
        //	// MenuActions lngRow
        //	MenuActions(FCConvert.ToInt32(Conversion.Val(GRID.TextMatrix(lngRow, 2))));
        //}

        private void ClearLabels()
        {
            //// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, Grid.Rows - 1, 1) = TRIOCOLORGRAYBACKGROUND
            ////GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, GRID.Rows - 1, GRID.Cols - 1, GRID.BackColor);
            //GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 40, 1, modGlobalConstants.Statics.TRIOCOLORBLACK);
            //GRID.ColData(1, string.Empty);
        }

        public void HandlePartialPermission(ref int lngFuncID)
        {
            // takes a function number as a parameter
            // Then gets all the child functions that belong to it and
            // disables the appropriate controls
            // mdiparent shouldnt need this function.  mdi functions should always send false
            // to validpermissions
            // Dim clsChildList As clsDRWrapper
            // Dim dcTemp As New clsDRWrapper
            // Dim strPerm As String
            // Dim lngChild As Long
            // 
            // 
            // Set clsChildList = New clsDRWrapper
            // 
            // Call clsSecurityClass.Get_Children(clsChildList, lngFuncID)
            // 
            // Do While Not clsChildList.EndOfFile
            // lngChild = clsChildList.GetData("childid")
            // strPerm = clsSecurityClass.Check_Permissions(lngChild)
            // 
            // Select Case lngFuncID
            // Case COSTUPDATE
            // Select Case lngChild
            // 
            // End Select
            // Case UPDATEMENU
            // End Select
            // 
            // Call clsChildList.MoveNext
            // Loop
        }

        private void SalesCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Sale Records]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Account Maintenance", "SALESMENU", parentItem, true, modGlobalVariables.CNSTLONGSCREEN);
            AddMenuItem(ref CurRow, 2, "Short Maintenance", "SALESMENU", parentItem, true, modGlobalVariables.CNSTSHORTSCREEN);
            AddMenuItem(ref CurRow, 3, "Calculate Sale Records", "SALESMENU", parentItem, true, modGlobalVariables.CNSTSALESRECORDS);
            AddMenuItem(ref CurRow, 4, "Extract for Analysis", "SALESMENU", parentItem, true, modGlobalVariables.CNSTEXTRACTFORLISTINGSANDLABELS);
            AddMenuItem(ref CurRow, 5, "Sales Analysis", "SALESMENU", parentItem, true, modGlobalVariables.CNSTSALESANALYSIS);
            AddMenuItem(ref CurRow, 6, "Purge Sale Records", "SALESMENU", parentItem, true, modGlobalVariables.CNSTPURGESALESREC);
            //AddMenuItem(ref CurRow, 7, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        public void SalesMenuActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        modGlobalVariables.Statics.wmainresponse = 1;
                        // Call SaveWWKTable(WWK)
                        frmGetAccount.InstancePtr.Show(App.MainForm);
                        frmGetAccount.InstancePtr.cmbtrecordtype.Text = "Sales Records";
                        modGlobalVariables.Statics.boolRegRecords = false;
                        //FC:FINAL:CHN - issue #1645: Fix not changed header text.
                        frmGetAccount.InstancePtr.Text = "Account Maintenance";
                        break;
                    }
                case 2:
                    {
                        modGlobalVariables.Statics.wmainresponse = 2;
                        // Call SaveWWKTable(WWK)
                        frmGetAccount.InstancePtr.Show(App.MainForm);
                        frmGetAccount.InstancePtr.cmbtrecordtype.Text = "Sales Records";
                        modGlobalVariables.Statics.boolRegRecords = false;
                        //FC:FINAL:CHN - issue #1645: Fix not changed header text.
                        frmGetAccount.InstancePtr.Text = "Short Maintenance";
                        break;
                    }
                case 3:
                    {
                        modGlobalVariables.Statics.boolUseArrays = true;
                        modProperty.Statics.RUNTYPE = "P";
                        modGlobalRoutines.Check_Arrays();
                        modSpeedCalc.Statics.boolCalcErrors = false;
                        modSpeedCalc.Statics.CalcLog = "";
                        modSpeedCalc.REAS03_1075_GET_PARAM_RECORD();
                        Array.Resize(ref modSpeedCalc.Statics.SummaryList, 0 + 1);
                        modSpeedCalc.Statics.SummaryListIndex = 0;
                        frmCalcSales.InstancePtr.Show();
                        break;
                    }
                case 4:
                    {
                        frmAssessExtract.InstancePtr.Show(App.MainForm);
                        frmAssessExtract.InstancePtr.cmbRorS.Text = "From Sales Records";
                        break;
                    }
                case 5:
                    {
                        dlgSalesAnalysis.InstancePtr.Show();
                        break;
                    }
                case 6:
                    {
                        frmPurgeSaleFile.InstancePtr.Show();
                        break;
                    }
                case 7:
                    {
                        modGlobalVariables.Statics.boolRegRecords = true;
                        //SetMenuOptions("MAIN");
                        break;
                    }
            }
            //end switch
        }

        public void ImportExportActions(int intMenu)
        {
            int intResponse;
            if (modREMain.Statics.boolShortRealEstate)
            {
                ShortImportExportActions(intMenu);
                return;
            }
            int intWidth;
            int intHeight;
            int intTop;
            int intLeft;
            switch (intMenu)
            {
                case 1:
                    {
                        frmDBExtract.InstancePtr.Show();
                        // Case 2
                        // Call ExtractToDisk
                        // Call NewExtractToDisk
                        break;
                    }
                case 2:
                    {
                        modGNBas.Statics.Resp1 = "FIRST";
                        // frmConvertGemini.Show 1
                        // Close
                        // Unload frmConvertGemini
                        break;
                    }
                case 3:
                    {
                        // If Not boolOldHH Then
                        // intWidth = MDIParent.Width - MDIParent.Grid.Width - 250
                        // intHeight = MDIParent.Grid.Height - 100
                        // intLeft = MDIParent.Grid.Left + MDIParent.Grid.Width + 50
                        // intTop = MDIParent.Grid.Top + 600
                        // intWidth = intWidth / Screen.TwipsPerPixelX
                        // intLeft = intLeft / Screen.TwipsPerPixelX
                        // intHeight = intHeight / Screen.TwipsPerPixelY
                        // intTop = intTop / Screen.TwipsPerPixelY
                        // Dim tFrm As New TRIOInteropLibrary.frmChooseMobileConnection
                        // Call tFrm.Init(CurDir & "\Twre0000.vb1", True, intLeft, intTop, intWidth, intHeight, CLng(clsSecurityClass.Get_UserID))
                        // tFrm.Show
                        // Else
                        // frmExportToHH.Show , Me
                        // End If
                        break;
                    }
                case 4:
                    {
                        // intResponse = MsgBox("This will import data from a mobile device.  Do you wish to continue?", vbQuestion + vbYesNo, "Continue?")
                        // If intResponse = vbYes Then
                        // If Not boolOldHH Then
                        // intWidth = MDIParent.Width - MDIParent.Grid.Width - 250
                        // intHeight = MDIParent.Grid.Height - 100
                        // intLeft = MDIParent.Grid.Left + MDIParent.Grid.Width + 50
                        // intTop = MDIParent.Grid.Top + 600
                        // intWidth = intWidth / Screen.TwipsPerPixelX
                        // intLeft = intLeft / Screen.TwipsPerPixelX
                        // intHeight = intHeight / Screen.TwipsPerPixelY
                        // intTop = intTop / Screen.TwipsPerPixelY
                        // Dim tIFrm As New TRIOInteropLibrary.frmChooseMobileConnection
                        // Call tIFrm.Init(CurDir & "\TWRE0000.vb1", False, intLeft, intTop, intWidth, intHeight, CLng(clsSecurityClass.Get_UserID))
                        // tIFrm.Show
                        // Else
                        // frmImportFromHH.Show , Me
                        // End If
                        // End If
                        break;
                    }
                case 5:
                    {
                        frmExportMasterInfo.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 6:
                    {
                        frmImportMasterInfo.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 7:
                    {
                        frmImportExportCostFiles.InstancePtr.Init(true);
                        break;
                    }
                case 8:
                    {
                        frmImportExportCostFiles.InstancePtr.Init(false);
                        modGlobalVariables.Statics.boolCostRecChanged = true;
                        modGlobalVariables.Statics.boolComCostChanged = true;
                        modGlobalVariables.Statics.boolLandTChanged = true;
                        modGlobalVariables.Statics.boolLandSChanged = true;
                        modGlobalVariables.Statics.boolRSZChanged = true;
                        // Case 9
                        // frmInfoExtract.Show , MDIParent
                        // ShowInfoExtract
                        break;
                    }
                case 10:
                    {
                        frmExportREWeb.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 11:
                    {
                        frmLandExtract.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 12:
                    {
                        //SetMenuOptions("MAIN");
                        break;
                    }
            }
            //end switch
        }

        public void ShortImportExportActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmDBExtract.InstancePtr.Show();
                        // Case 2
                        // Call ExtractToDisk
                        break;
                    }
                case 2:
                    {
                        //if (Interaction.Shell("twxf0000.exe", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1) > 0)
                        //{
                        //	boolDontShellToGE = true;
                        //	//this.Unload();
                        //	Application.Exit();
                        //}
                        //else
                        //{
                        //	MessageBox.Show("Unable to start transfer program");
                        //}
                        break;
                    }
                case 3:
                    {
                        frmImportExportCostFiles.InstancePtr.Init(true);
                        break;
                    }
                case 4:
                    {
                        frmImportExportCostFiles.InstancePtr.Init(false);
                        // Case 5
                        // frmInfoExtract.Show , MDIParent
                        // ShowInfoExtract
                        break;
                    }
                case 6:
                    {
                        frmExportREWeb.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 7:
                    {
                        //SetMenuOptions("MAIN");
                        break;
                    }
            }
            //end switch
        }

        private void ShortImportExportCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Import / Export]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Create Database Extract", "IMPORTEXPORT", parentItem, true, modGlobalVariables.CNSTDATABASEEXTRACT);
            if (modREMain.Statics.boolAllowXF)
            {
                FCMenuItem newItem = AddMenuItem(ref CurRow, 2, "Transfer from other vendor", "IMPORTEXPORT", parentItem, true);
                RETransferCaptions(newItem);
            }
            AddMenuItem(ref CurRow, 3, "Export Cost Files", "IMPORTEXPORT", parentItem, true);
            AddMenuItem(ref CurRow, 4, "Import Cost Files", "IMPORTEXPORT", parentItem, true);
            AddMenuItem(ref CurRow, 6, "Web Export", "IMPORTEXPORT", parentItem, true, 0, "", "Export files for Harris hosted web site");
            //AddMenuItem(ref CurRow, 7, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        public void AssociationActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmGetMortgage.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 2:
                    {
                        frmGetGroup.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 3:
                    {
                        //SetMenuOptions("MAIN");
                        break;
                    }
            }
            //end switch
        }

        private void IncomeUpdateActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmIncomeCostFiles.InstancePtr.Unload();
                        //Application.DoEvents();
                        frmIncomeCostFiles.InstancePtr.Init(1);
                        break;
                    }
                case 2:
                    {
                        frmIncomeCostFiles.InstancePtr.Unload();
                        //Application.DoEvents();
                        frmIncomeCostFiles.InstancePtr.Init(2);
                        break;
                    }
                case 3:
                    {
                        frmIncomeCostFiles.InstancePtr.Unload();
                        //Application.DoEvents();
                        frmIncomeCostFiles.InstancePtr.Init(3);
                        break;
                    }
                case 4:
                    {
                        //SetMenuOptions("MAIN");
                        break;
                    }
            }
            //end switch
        }

        private void IncomeUpdateCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Income Approach]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Occupancy Codes", "INCOMEUPDATE", parentItem, true);
            AddMenuItem(ref CurRow, 2, "Expense Codes", "INCOMEUPDATE", parentItem, true);
            AddMenuItem(ref CurRow, 3, "Standard Overall Rate", "INCOMEUPDATE", parentItem, true);
            //AddMenuItem(ref CurRow, 4, "Return to Main", 0, "X");
            AutoSizeMenu();
        }

        private void AssociationCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Account Associations]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "MortgageMaintenance", "ASSOCIATION", parentItem, true, modGlobalVariables.CNSTMORTGAGEMAINT);
            AddMenuItem(ref CurRow, 2, "Group Maintenance", "ASSOCIATION", parentItem, true, modGlobalVariables.CNSTGROUPMAINT);
            //AddMenuItem(ref CurRow, 3, "Return to Main", 0, "X");
            AutoSizeMenu();
        }
        // Public Sub AssociationCaptions()
        // Dim CurRow As Integer
        // Dim strTemp As String
        // Dim lngFCode As Long
        //
        // MDIParent.Caption = "TRIO Software - Real Estate        [Account Associations]"
        // ClearLabels
        // With GRID
        // For CurRow = 0 To 20
        // lngFCode = 0
        //
        // Select Case CurRow
        // Case 1
        // strTemp = "1.  Mortgage Maintenance"
        // LabelNumber(Asc("1")) = 1
        // lngFCode = CNSTMORTGAGEMAINT
        // Case 2
        // strTemp = "2.  Group Maintenance"
        // LabelNumber(Asc("2")) = 2
        // lngFCode = CNSTGROUPMAINT
        // Case 3
        // strTemp = "X.  Return to Main"
        // LabelNumber(Asc("X")) = 3
        // Case Else
        // strTemp = ""
        // End Select
        //
        // .TextMatrix((CurRow * 2), 1) = strTemp
        // If lngFCode <> 0 Then
        // Call EnableDisableMenuOption(ValidPermissions(Me, lngFCode, False), CurRow)
        // End If
        // Next CurRow
        // End With
        //
        // Call AutoSizeMenu(20)
        // End Sub
        // vbPorter upgrade warning: 'Return' As string	OnWrite(short, string)
        private string GetCurCaption(ref short intChoice)
        {
            string GetCurCaption = "";
            if (intChoice >= 1 && intChoice <= 9)
            {
                GetCurCaption = FCConvert.ToString(intChoice);
            }
            else
            {
                GetCurCaption = FCConvert.ToString(Convert.ToChar(intChoice + 64));
            }
            return GetCurCaption;
        }

        private void ImportExportCaptions(FCMenuItem parentItem)
        {
            int CurRow;
            string strTemp = "";
            int lngFCode;
            //MDIParent.InstancePtr.Text = "TRIO Software - Real Estate       [Import / Export]";
            ClearLabels();
            ClearMenuItems();
            CurRow = 1;
            AddMenuItem(ref CurRow, 1, "Create Database Extract", "IMPORTEXPORT", parentItem, true, modGlobalVariables.CNSTDATABASEEXTRACT);
            // Call AddMenuItem(CurRow, 2, "Create User Extract", CNSTUSEREXTRACT)
            // If boolHandHeld Then
            // Call AddMenuItem(CurRow, 3, "Export to Pocket PC", CNSTIMEXPOCKETPC)
            // Call AddMenuItem(CurRow, 4, "Import from Pocket PC", CNSTIMEXPOCKETPC)
            // End If
            if (modREMain.Statics.boolAllowXF)
            {
                FCMenuItem newItem = AddMenuItem(ref CurRow, 2, "Transfer from other vendor", "IMPORTEXPORT", parentItem, false);
                RETransferCaptions(newItem);
            }
            AddMenuItem(ref CurRow, 5, "Export Properties", "IMPORTEXPORT", parentItem, true, modGlobalVariables.CNSTIMEXNAMEETC, "", "Export XML files to transfer to another TRIO database");
            AddMenuItem(ref CurRow, 6, "Import Properties", "IMPORTEXPORT", parentItem, true, modGlobalVariables.CNSTIMEXNAMEETC, "", "Import XML files from another TRIO database");
            AddMenuItem(ref CurRow, 7, "Export Cost Files", "IMPORTEXPORT", parentItem, true);
            AddMenuItem(ref CurRow, 8, "Import Cost Files", "IMPORTEXPORT", parentItem, true, 0);
            AddMenuItem(ref CurRow, 10, "Web Export", "IMPORTEXPORT", parentItem, true, 0, "", "Export files for Harris hosted web site");
            AddMenuItem(ref CurRow, 11, "Land Extract", "IMPORTEXPORT", parentItem, true);
            //AddMenuItem(ref CurRow, 12, "Return to Main", "IMPORTEXPORT", parentItem, 0, "X");
            AutoSizeMenu();
        }

        public void PictureActions(int intMenu)
        {
            switch (intMenu)
            {
                case 1:
                    {
                        frmAutoAddPics.InstancePtr.Show(App.MainForm);
                        break;
                    }

                case 3:
                    {
                        frmDeleteDuplicatePictures.InstancePtr.Show(App.MainForm);
                        break;
                    }
                case 5:
                    {
                        Application.Navigate("http://trioassistant.trio-web.com/realestatepictureimport/AutoImportREPicturesSetup.msi", "_blank");
                        break;
                    }
            }
            //end switch
        }

        public void FileMaintActions(int intMenu)
        {
            const int curOnErrorGoToLabel_Default = 0;
            const int curOnErrorGoToLabel_ErrorHandler = 1;
            int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
            try
            {
                // vbPorter upgrade warning: intResp As short, int --> As DialogResult
                DialogResult intResp;
                clsDRWrapper clsSave = new clsDRWrapper();
                switch (intMenu)
                {
                    case 1:
                        {
                            frmREOptions.InstancePtr.Show(App.MainForm);
                            break;
                        }
                    case 3:
                        {
                            //SetMenuOptions("ACCOUNTFUNCTIONS");
                            // Case 4
                            // Resp1 = "FIRST"
                            // frmMoveSecondOwner.Show 1
                            // Case 4
                            // Call UndeleteMasterRecord
                            break;
                        }
                    case 4:
                        {
                            //SetMenuOptions("ASSOCIATION");
                            break;
                        }
                    case 5:
                        {
                            int dblID = 0;
                            string strDOSPath = "";
                            intResp = MessageBox.Show("This will overwrite the TSREAS50 (DOS Real Estate Master) file." + NEWLINE + "Do you wish to continue?", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                            if (intResp == DialogResult.OK)
                            {
                                // Call clsSave.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_OpID & "',#" & Now & "#,'Convert to DOS','Converted to DOS')", strredatabase)
                                modGlobalFunctions.AddCYAEntry_26("RE", "Convert to DOS", "Converted to DOS");
                                MessageBox.Show("This will exit you from TRIO." + NEWLINE + "Make sure everyone else is also out of Windows Real Estate *AND* DOS Real Estate.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                // CommonDialog1.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                                CommonDialog1.FileName = "tsreas50.td1";
                                CommonDialog1.InitDir = FCFileSystem.Statics.UserDataFolder;
                                CommonDialog1.DialogTitle = "Choose the directory to save the DOS master file to";
                                //- CommonDialog1.CancelError = true;
                                /*? On Error Resume Next  */
                                try
                                {
                                    CommonDialog1.ShowOpen();
                                }
                                catch
                                {
                                }
                                if (Information.Err().Number != 0)
                                {
                                    return;
                                }
                                vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
                                /* On Error GoTo ErrorHandler */
                                strDOSPath = CommonDialog1.FileName;
                                dblID = Interaction.Shell("REWn2DOS.exe " + strDOSPath, System.Diagnostics.ProcessWindowStyle.Minimized, false, -1);
                                if (dblID == 0)
                                {
                                    MessageBox.Show("Error. Could not run conversion.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                }
                                else
                                {
                                    Application.Exit();
                                }
                            }
                            else
                            {
                                //this.GRID.Focus();
                            }
                            break;
                        }
                    case 6:
                        {
                            // transfer stuff
                            frmPendingTransfers.InstancePtr.Show(App.MainForm);
                            break;
                        }
                    case 7:
                        {
                            //SetMenuOptions("DATAMANIPULATION");
                            break;
                        }
                    case 8:
                        {
                            // intResp = MsgBox("Make sure that the Pocket PC is on and connected before proceeding.", vbOKCancel, "Update Pocket PC")
                            // 
                            // If intResp = vbOK Then
                            // intResp = frmPocketPCType.Init
                            // Select Case intResp
                            // Case 1
                            // Shell CurDir & "\fasu\Dell\setup.exe", vbNormalFocus
                            // Case 2
                            // Shell CurDir & "\fasu\Dell03\setup.exe", vbNormalFocus
                            // Case 3
                            // Shell CurDir & "\fasu\Ipaq\setup.exe", vbNormalFocus
                            // Shell CurDir & "\fasu\evb2003\setup.exe", vbNormalFocus
                            // End Select
                            // End If
                            break;
                        }
                    case 9:
                        {
                            // If MsgBox("Are you sure you want to convert the Reference 1 field to the Book Page table?", vbQuestion + vbYesNo, "Continue?") = vbNo Then
                            // Exit Sub
                            // End If
                            // If MsgBox("Do you wish to convert regular records?" & vbNewLine & "Answer no to convert the sales records", vbQuestion + vbYesNo, "Convert Regular Records?") = vbNo Then
                            // ConvertRef1ToBookPage (True)
                            // Else
                            // ConvertRef1ToBookPage
                            // End If
                            break;
                        }
                    case 10:
                        {
                            if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTCREATEARCHIVES)) == "F")
                            {
                                frmArchive.InstancePtr.Init("RealEstate", true);
                            }
                            else
                            {
                                frmArchive.InstancePtr.Init("RealEstate", false);
                            }
                            // Case 13
                            // frmAutoAddPics.Show , MDIParent
                            // Case 14
                            // frmMovePictures.Show , MDIParent
                            break;
                        }
                    case 11:
                        {
                            //SetMenuOptions("PICTURE");
                            break;
                        }
                    case 12:
                        {
                            frmPreviousAssessment.InstancePtr.Show(App.MainForm);
                            break;
                        }
                    case 13:
                        {
                            frmDBExtract.InstancePtr.Show(App.MainForm);
                            break;
                        }
                    case 2:
                        {
                            if (modGlobalRoutines.CheckDatabaseStructure())
                            {
                                MessageBox.Show("Database Structure Check completed successfully.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("There was an error trying to check/update database structure.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            }
                            //this.GRID.Focus();
                            break;
                        }
                    case 14:
                        {
                            frmTaxAcquiredRemoval.InstancePtr.Init();
                            // Case 15
                            // frmInfoExtract.Show , MDIParent
                            // ShowInfoExtract
                            break;
                        }
                    case 16:
                        {
                            frmPurgeArchive.InstancePtr.Show(App.MainForm);
                            break;
                        }
                    case 17:
                        {
                            //SetMenuOptions("MAIN");
                            break;
                        }
                }
                //end switch
                return;
            ErrorHandler:
                ;
            }
            catch
            {
                switch (vOnErrorGoToLabel)
                {
                    default:
                    case curOnErrorGoToLabel_Default:
                        // ...
                        break;
                    case curOnErrorGoToLabel_ErrorHandler:
                        //? goto ErrorHandler;
                        break;
                }
            }
        }
        // vbPorter upgrade warning: rgb As int --> As Color
        // vbPorter upgrade warning: 'Return' As short	OnWrite(int, bool)
        private short RedFromRGB(Color rgb)
        {
            short RedFromRGB = 0;
            // The ampersand after &HFF coerces the number as a
            // long, preventing Visual Basic from evaluating the
            // number as a negative value. The logical And is
            // used to return bit values.
            RedFromRGB = FCConvert.ToInt16(0xFF & ColorTranslator.ToOle(rgb));
            return RedFromRGB;
        }
        // vbPorter upgrade warning: rgb As int --> As Color
        private short GreenFromRGB(Color rgb)
        {
            short GreenFromRGB = 0;
            // The result of the And operation is divided by
            // 256, to return the value of the middle bytes.
            // Note the use of the Integer divisor.
            GreenFromRGB = FCConvert.ToInt16(fecherFoundation.FCUtils.iDiv((0xFF00 & ColorTranslator.ToOle(rgb)), 256));
            return GreenFromRGB;
        }
        // vbPorter upgrade warning: rgb As int --> As Color
        // vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
        private short BlueFromRGB(Color rgb)
        {
            short BlueFromRGB = 0;
            // This function works like the GreenFromRGB above,
            // except you don't need the ampersand. The
            // number is already a long. The result divided by
            // 65536 to obtain the highest bytes.
            BlueFromRGB = FCConvert.ToInt16(fecherFoundation.FCUtils.iDiv((0xFF0000 & ColorTranslator.ToOle(rgb)), 65536));
            return BlueFromRGB;
        }

        public class StaticVariables
        {
            // End Sub
            // Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, R, 0) = ImageList1.ListImages("Left").Picture
            //
            // End If
            // Grid.HighLight = flexHighlightAlways
            // Else
            // Exit Sub
            // Grid.HighLight = flexHighlightNever
            // If Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, R, 1) = TRIOCOLORDISABLEDOPTION Or Grid.TextMatrix(R, 1) = vbNullString Then
            //
            // If R = 0 Then Exit Sub
            // show cursor image if there is some text in column 5
            // Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, Grid.Rows - 1, 1) = Grid.BackColor
            // Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, Grid.Rows - 1, 1) = TRIOCOLORGRAYBACKGROUND
            // Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1) = Nothing
            // Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1) = ImageList1.ListImages("Clear").Picture
            // remove cursor image
            // Grid.ToolTipText = GetGridToolTipText(UCase(Grid.Tag), R / 2)
            //
            // Grid.Select R, 1
            // move selection (even with no click)
            // If R < 1 Then Exit Sub
            // R = Grid.MouseRow
            // If Grid.MouseRow = R Then Exit Sub
            // avoid extra work
            //
            //
            // Static R&, C&
            // Dim i#
            // Private Sub GRID_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
            //
            // End Sub
            // Call MenuClick(LabelNumber(Asc(UCase(Left(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, (Grid.Row), 1), 1)))))
            // CallByName MDIParent, "Menu" & LabelNumber(Asc(UCase(Left(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, (GRID.Row), 1), 1)))) & "_click", VbMethod
            // If Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.MouseRow, 1) = TRIOCOLORDISABLEDOPTION Or Grid.TextMatrix(Grid.MouseRow, 1) = vbNullString Then Exit Sub
            //
            // Private Sub GRID_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
            //
            // End Sub
            // Call MenuClick(LabelNumber(Asc(UCase(Left(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, (Grid.Row), 1), 1)))))
            // CallByName MDIParent, "Menu" & LabelNumber(Asc(UCase(Left(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, (GRID.Row), 1), 1)))) & "_click", VbMethod
            // If Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Row, 1) = MDIParent.BackColor Or Grid.TextMatrix(Grid.Row, 1) = vbNullString Then Exit Sub
            //
            // If KeyAscii <> 13 Then Exit Sub
            // Private Sub GRID_KeyPress(KeyAscii As Integer)
            //
            // End Sub
            //
            // Next
            // End If
            // Exit For
            // KeyCode = 0
            // Call MenuClick(intCounter / 2)
            // CallByName MDIParent, "Menu" & intCounter / 2 & "_click", VbMethod
            // If Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intCounter, 1) = TRIOCOLORDISABLEDOPTION Then Exit Sub
            // If Left(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1), 1) = Chr(KeyCode) Then
            // For intCounter = 2 To Grid.Rows - 1 Step 2
            //
            // End If
            // Exit Sub
            // KeyCode = 0
            // Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, Grid.Row, 0) = ImageList1.ListImages("Left").Picture
            // Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1) = Nothing
            // Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1) = ImageList1.ListImages("Clear").Picture
            // End If
            // End If
            // Grid.Row = intPlaceHolder
            // Else
            //
            // End If
            // GoTo KeepGoingUP
            // ElseIf Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Row, 1) = TRIOCOLORDISABLEDOPTION Then
            // If Grid.TextMatrix(Grid.Row, 1) = vbNullString Then
            // If Grid.Row < 2 Then Grid.Row = 2
            // Grid.Row = Grid.Row - 2
            // KeepGoingUP:    If (Grid.Row - 2) >= 2 Then
            // Else
            //
            // End If
            // End If
            // GoTo KeepGoing
            // ElseIf Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.Row, 1) = TRIOCOLORDISABLEDOPTION Then
            // Grid.Row = Grid.Row - 2
            // If Grid.TextMatrix(Grid.Row, 1) = vbNullString Then
            // Grid.Row = Grid.Row + 2
            // KeepGoing:      If (Grid.Row + 2) <= (Grid.Rows - 1) Then
            // If KeyCode = vbKeyDown Then
            //
            // intPlaceHolder = Grid.Row
            //
            // If (KeyCode = vbKeyDown) Or (KeyCode = vbKeyUp) Then
            //
            // End If
            // KeyCode = KeyCode - 48
            // If KeyCode >= vbKeyNumpad0 And KeyCode <= vbKeyNumpad9 Then
            //
            //
            // Dim intPlaceHolder As Integer
            // Dim intCounter As Integer
            //
            // Private Sub grid_KeyDown(KeyCode As Integer, Shift As Integer)
            public int R = 0, c;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
