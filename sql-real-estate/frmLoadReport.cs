﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLoadReport.
	/// </summary>
	public partial class frmLoadReport : BaseForm
	{
		public frmLoadReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLoadReport InstancePtr
		{
			get
			{
				return (frmLoadReport)Sys.GetInstance(typeof(frmLoadReport));
			}
		}

		protected frmLoadReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const int CNSTGRIDCOLNUMBER = 0;
		const int CNSTGRIDCOLTITLE = 1;
		const int CNSTGRIDCOLDESCRIPTION = 2;
		const int CNSTGRIDCOLUSERNAME = 3;
		const int CNSTGRIDCOLUSERID = 4;
		// vbPorter upgrade warning: lngReportToLoad As int	OnWrite(short, string)
		int lngReportToLoad;

		public int Init()
		{
			int Init = 0;
			lngReportToLoad = 0;
			Init = lngReportToLoad;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngReportToLoad;
			return Init;
		}

		private void frmLoadReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmLoadReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLoadReport properties;
			//frmLoadReport.ScaleWidth	= 5880;
			//frmLoadReport.ScaleHeight	= 4215;
			//frmLoadReport.LinkTopic	= "Form1";
			//frmLoadReport.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
		}

		private void LoadGrid()
		{
			clsDRWrapper clsIDs = new clsDRWrapper();
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				clsIDs.OpenRecordset("select * from Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' order by id", "ClientSettings");
				Grid.Rows = 1;
				clsLoad.OpenRecordset("select * from  reporttitles order by reportnumber", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLNUMBER, FCConvert.ToString(clsLoad.Get_Fields_Int32("reportnumber")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLTITLE, FCConvert.ToString(clsLoad.Get_Fields_String("Title")));
					// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("userid"))));
					// If clsIDs.FindFirstRecord("id", Val(clsLoad.Fields("userid"))) Then
					// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
					if (clsIDs.FindFirst("id = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("userid")))))
					{
						// TODO Get_Fields: Check the table for the column [userid] and replace with corresponding Get_Field method
						Grid.TextMatrix(lngRow, CNSTGRIDCOLUSERNAME, FCConvert.ToString(clsIDs.Get_Fields("userid")));
					}
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.Cols = 5;
			Grid.ExtendLastCol = true;
			Grid.ColHidden(CNSTGRIDCOLUSERID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Description");
			Grid.TextMatrix(0, CNSTGRIDCOLNUMBER, "#");
			Grid.TextMatrix(0, CNSTGRIDCOLTITLE, "Title");
			Grid.TextMatrix(0, CNSTGRIDCOLUSERNAME, "User Name");

            Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLNUMBER, FCConvert.ToInt32(0.09 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLTITLE, FCConvert.ToInt32(0.35 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLDESCRIPTION, FCConvert.ToInt32(0.35 * GridWidth));
		}

		private void frmLoadReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow < 1)
				return;
			mnuContinue_Click();
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
			{
				MessageBox.Show("No report was selected", "No selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			lngReportToLoad = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLNUMBER));
			this.Unload();
		}

		public void mnuContinue_Click()
		{
			mnuContinue_Click(cmdContinue, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			lngReportToLoad = 0;
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
