﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWRE0000
{
	public class cREValuationItem
	{
		//=========================================================
		private double dblTotalLand;
		private double dblTotalBuilding;
		private double dblTotalExemption;

		public double Exemption
		{
			set
			{
				dblTotalExemption = value;
			}
			get
			{
				double Exemption = 0;
				Exemption = dblTotalExemption;
				return Exemption;
			}
		}

		public double Building
		{
			set
			{
				dblTotalBuilding = value;
			}
			get
			{
				double Building = 0;
				Building = dblTotalBuilding;
				return Building;
			}
		}

		public double Land
		{
			set
			{
				dblTotalLand = value;
			}
			get
			{
				double Land = 0;
				Land = dblTotalLand;
				return Land;
			}
		}
	}
}
