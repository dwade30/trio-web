﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWRE0000
{
	public class modBitMap
	{
		//=========================================================
		//public static void ConvertBMP256ToMonochrome_2(string strSrcFile, string strFileName)
		//{
		//	ConvertBMP256ToMonochrome(ref strSrcFile, ref strFileName);
		//}

		//public static void ConvertBMP256ToMonochrome(ref string strSrcFile, ref string strFileName)
		//{
		//	// opens an 8 bit srcfile and saves to a monochrome.  Both are bmp files
		//	//FC:FINAL:RPU - Use custom constructor to initialize fields
		//	modREAPI.BITMAPINFOHEADER bhead = new modREAPI.BITMAPINFOHEADER(0);
		//	modREAPI.BITMAPINFO256 binfo256 = new modREAPI.BITMAPINFO256(0);
		//	modREAPI.BITMAPINFO binfo = new modREAPI.BITMAPINFO(0);
		//	modREAPI.BITMAPFILEHEADER bfhead = new modREAPI.BITMAPFILEHEADER(0);
		//	//FileSystemObject fso = new FileSystemObject();
		//	int x;
		//	int Y;
		//	byte[] srcBuf = null;
		//	byte[] destBuf = null;
		//	// vbPorter upgrade warning: bt As byte	OnWriteFCConvert.ToInt16(
		//	byte bt = 0;
		//	int lngtemp;
		//	int lngWidthPixels;
		//	int lngHeightPixels;
		//	int lngCroppedWidth;
		//	int lngCroppedHeight;
		//	int lngNumBytes;
		//	int lngWidthBytes;
		//	int intNumPadBytes;
		//	int lngCurrentSrcByte;
		//	int lngCurrentDestByte;
		//	int intNumSrcPadBytes;
		//	int intCurrentBit;
		//	int lngMinX;
		//	int lngMinY;
		//	int lngMaxX;
		//	int lngMaxY;
		//	int lngLeftOffset;
		//	int lngLineWidth;
		//	int intBackGround;
		//	try
		//	{
		//		// On Error GoTo ErrorHandler
		//		FCFileSystem.FileClose();
		//		FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
		//		FCFileSystem.FileOpen(42, strSrcFile, OpenMode.Binary, (OpenAccess)(-1), (OpenShare)(-1), -1);
		//		FCFileSystem.FileGet(42, ref bfhead, -1);
		//		FCFileSystem.FileGet(42, ref bhead, -1);
		//		// this is different than 24 bit image
		//		// the 24 bit image doesn't have the binfo structure.
		//		FCFileSystem.FileGet(42, ref binfo256, -1);
		//		srcBuf = new byte[bhead.biSizeImage - 1 + 1];
		//		// the image size
		//		//FC:TODO
		//		//FCFileSystem.FileGet(42, ref srcBuf, -1);
		//		FCFileSystem.FileClose();
		//		intBackGround = FCConvert.ToInt32(Math.Round(Conversion.Val(srcBuf[0])));
		//		// assume first color is the background
		//		lngWidthPixels = bhead.biWidth;
		//		lngHeightPixels = bhead.biHeight;
		//		lngtemp = lngWidthPixels;
		//		// 1 byte per pixel
		//		intNumSrcPadBytes = 4 - (lngtemp % 4);
		//		if (intNumSrcPadBytes == 4)
		//			intNumSrcPadBytes = 0;
		//		binfo.bmiHeader.biBitCount = 1;
		//		binfo.bmiHeader.biPlanes = 1;
		//		binfo.bmiHeader.biCompression = 0;
		//		binfo.bmiHeader.biClrImportant = 0;
		//		binfo.bmiHeader.biClrUsed = 0;
		//		binfo.bmiHeader.biSize = 40;
		//		// size of this record
		//		binfo.bmiHeader.biXPelsPerMeter = 0;
		//		// ? copied from a file created in mspaint
		//		binfo.bmiHeader.biYPelsPerMeter = 0;
		//		bfhead.bfOffBits = 62;
		//		binfo.bmiColors[0].rgbBlue = 255;
		//		binfo.bmiColors[0].rgbGreen = 255;
		//		binfo.bmiColors[0].rgbRed = 255;
		//		binfo.bmiColors[1].rgbBlue = 0;
		//		binfo.bmiColors[1].rgbGreen = 0;
		//		binfo.bmiColors[1].rgbRed = 0;
		//		lngCurrentSrcByte = 0;
		//		lngMinY = modGlobalConstants.EleventyBillion;
		//		lngMinX = modGlobalConstants.EleventyBillion;
		//		lngMaxX = 0;
		//		lngMaxY = 0;
		//		// skip first line since we get a ghost line in the upper right corner
		//		// for some reason
		//		for (Y = 1; Y <= lngHeightPixels - 1; Y++)
		//		{
		//			for (x = 0; x <= lngWidthPixels - 1; x++)
		//			{
		//				if (Conversion.Val(srcBuf[lngCurrentSrcByte]) != intBackGround)
		//				{
		//					if (x < lngMinX)
		//					{
		//						lngMinX = x;
		//					}
		//					if (x > lngMaxX)
		//					{
		//						lngMaxX = x;
		//					}
		//					if (Y < lngMinY)
		//					{
		//						lngMinY = Y;
		//					}
		//					if (Y > lngMaxY)
		//					{
		//						lngMaxY = Y;
		//					}
		//				}
		//				lngCurrentSrcByte += 1;
		//			}
		//			// x
		//			lngCurrentSrcByte += intNumSrcPadBytes;
		//		}
		//		// Y
		//		// leave a 5 pixel margin around it as a border
		//		int intMargin;
		//		intMargin = 5;
		//		lngMinX -= 5;
		//		if (lngMinX < 0)
		//			lngMinX = 0;
		//		lngMinY -= 5;
		//		if (lngMinY < 0)
		//			lngMinY = 0;
		//		if (lngMaxX < lngWidthPixels - 5)
		//		{
		//			lngMaxX += 5;
		//		}
		//		if (lngMaxY < lngHeightPixels - 5)
		//		{
		//			lngMaxY += 5;
		//		}
		//		// now calculate the cropped image size
		//		lngCroppedWidth = lngMaxX - lngMinX + 1;
		//		lngCroppedHeight = lngMaxY - lngMinY + 1;
		//		binfo.bmiHeader.biHeight = lngCroppedHeight;
		//		binfo.bmiHeader.biWidth = lngCroppedWidth;
		//		intNumSrcPadBytes += (lngWidthPixels) - ((lngMaxX + 1));
		//		// skip the padding and the cropped width together
		//		lngWidthBytes = FCConvert.ToInt32(Conversion.Int(lngCroppedWidth / 8.0));
		//		// if it doesn't go even number of times we must add another byte
		//		// using intNumPadBytes as a temp variable here
		//		intNumPadBytes = 8 - (lngCroppedWidth % 8);
		//		if (intNumPadBytes > 0 && intNumPadBytes < 8)
		//		{
		//			lngWidthBytes += 1;
		//		}
		//		// must pad the buffer to dwords so find out how many bytes to add to each scanline to be even
		//		intNumPadBytes = 4 - (lngWidthBytes % 4);
		//		// must make it an even dword
		//		if (intNumPadBytes == 4)
		//			intNumPadBytes = 0;
		//		// for each scanline we will actually write lngWidthBytes plus intNumPadBytes
		//		// the imagesize written will be (lngwidthbytes + intnumpadbytes) * lngheightpixels
		//		binfo.bmiHeader.biSizeImage = (lngWidthBytes + intNumPadBytes) * lngCroppedHeight;
		//		bfhead.bfSize = binfo.bmiHeader.biSizeImage + 62;
		//		// buffer + header info
		//		destBuf = new byte[binfo.bmiHeader.biSizeImage - 1 + 1];
		//		lngLeftOffset = (lngMinX);
		//		lngLineWidth = ((lngMaxX + 1)) + intNumSrcPadBytes;
		//		lngCurrentSrcByte = lngMinY * lngLineWidth;
		//		lngCurrentDestByte = 0;
		//		intCurrentBit = 8;
		//		// we will process from srcBuf to destBuf and then write the buffer all at once
		//		for (Y = lngMinY; Y <= lngMaxY; Y++)
		//		{
		//			// y represents the current scanline
		//			bt = 255;
		//			// reset
		//			intCurrentBit = 8;
		//			lngCurrentSrcByte += lngLeftOffset;
		//			for (x = lngMinX; x <= lngMaxX; x++)
		//			{
		//				// x represents the column
		//				if (Conversion.Val(srcBuf[lngCurrentSrcByte]) != intBackGround)
		//				{
		//					// leave it set
		//					intCurrentBit = intCurrentBit;
		//				}
		//				else
		//				{
		//					// zero out this bit
		//					switch (intCurrentBit)
		//					{
		//						case 8:
		//							{
		//								bt = Convert.ToByte(bt & 127);
		//								break;
		//							}
		//						case 7:
		//							{
		//								bt = Convert.ToByte(bt & 191);
		//								break;
		//							}
		//						case 6:
		//							{
		//								bt = Convert.ToByte(bt & 223);
		//								break;
		//							}
		//						case 5:
		//							{
		//								bt = Convert.ToByte(bt & 239);
		//								break;
		//							}
		//						case 4:
		//							{
		//								bt = Convert.ToByte(bt & 247);
		//								break;
		//							}
		//						case 3:
		//							{
		//								bt = Convert.ToByte(bt & 251);
		//								break;
		//							}
		//						case 2:
		//							{
		//								bt = Convert.ToByte(bt & 253);
		//								break;
		//							}
		//						case 1:
		//							{
		//								bt = Convert.ToByte(bt & 254);
		//								break;
		//							}
		//					}
		//					//end switch
		//				}
		//				intCurrentBit -= 1;
		//				if (intCurrentBit == 0)
		//				{
		//					// have processed 8 pixels so write the byte to the buffer
		//					intCurrentBit = 8;
		//					destBuf[lngCurrentDestByte] = bt;
		//					bt = 255;
		//					// reset it
		//					lngCurrentDestByte += 1;
		//				}
		//				lngCurrentSrcByte += 1;
		//				// skip to next pixel
		//			}
		//			// x
		//			if (intCurrentBit != 8)
		//			{
		//				// didn't write the last byte yet
		//				destBuf[lngCurrentDestByte] = bt;
		//				lngCurrentDestByte += 1;
		//			}
		//			// skip the padding
		//			lngCurrentSrcByte += intNumSrcPadBytes;
		//			lngCurrentDestByte += intNumPadBytes;
		//		}
		//		// Y
		//		FCFileSystem.FileClose();
		//		if (File.Exists(strFileName))
		//		{
		//			File.Delete(strFileName);
		//		}
		//		FCFileSystem.FileOpen(42, strFileName, OpenMode.Binary, (OpenAccess)(-1), (OpenShare)(-1), -1);
		//		FCFileSystem.FilePut(42, bfhead, -1);
		//		FCFileSystem.FilePut(42, binfo, -1);
		//		//FC:TODO
		//		//FCFileSystem.FilePut(42, destBuf, -1);
		//		FCFileSystem.FileClose();
		//		FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ErrorHandler:
		//		MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ConvertBMP256ToMonochrome", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//}

		public static void ConvertBMPToMonochrome_2(string strSrcFile, string strFileName)
		{
			ConvertBMPToMonochrome(ref strSrcFile, ref strFileName);
		}

		public static void ConvertBMPToMonochrome(ref string strSrcFile, ref string strFileName)
		{
			// opens a 24 bit srcfile and saves to a monochrome both are bmp files
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			modREAPI.BITMAPINFOHEADER bhead = new modREAPI.BITMAPINFOHEADER(0);
			modREAPI.BITMAPINFO binfo = new modREAPI.BITMAPINFO(0);
			modREAPI.BITMAPFILEHEADER bfhead = new modREAPI.BITMAPFILEHEADER(0);
			modREAPI.RGBQUAD quadColor1 = new modREAPI.RGBQUAD(0);
			modREAPI.RGBQUAD quadColor2 = new modREAPI.RGBQUAD(0);
			StreamWriter ts;
			//FileSystemObject fso = new FileSystemObject();
			int x;
			int Y;
			byte[] srcBuf = null;
			byte[] destBuf = null;
			// vbPorter upgrade warning: bt As byte	OnWriteFCConvert.ToInt16(
			byte bt = 0;
			int lngtemp;
			int lngWidthPixels;
			int lngHeightPixels;
			int lngCroppedWidth;
			int lngCroppedHeight;
			int lngNumBytes;
			int lngWidthBytes;
			int intNumPadBytes;
			int lngCurrentSrcByte;
			int lngCurrentDestByte;
			int intNumSrcPadBytes;
			int intCurrentBit;
			int lngMinX;
			int lngMinY;
			int lngMaxX;
			int lngMaxY;
			int lngLeftOffset;
			int lngLineWidth;
			try
			{
				// On Error GoTo ErrorHandler
				FCFileSystem.FileClose();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				FCFileSystem.FileOpen(42, strSrcFile, OpenMode.Binary, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.FileGet(42, ref bfhead, -1);
				FCFileSystem.FileGet(42, ref bhead, -1);
				srcBuf = new byte[bhead.biSizeImage - 1 + 1];
				// the image size
				//FC:TODO
				//FCFileSystem.FileGet(42, ref srcBuf, -1);
				FCFileSystem.FileClose();
				lngWidthPixels = bhead.biWidth;
				lngHeightPixels = bhead.biHeight;
				lngtemp = lngWidthPixels * 3;
				// 3 bytes per pixel
				intNumSrcPadBytes = 4 - (lngtemp % 4);
				if (intNumSrcPadBytes == 4)
					intNumSrcPadBytes = 0;
				// don't need the headers anymore so we can use them for the output file
				binfo.bmiHeader.biBitCount = 1;
				// binfo.bmiHeader.biHeight = lngHeightPixels
				// binfo.bmiHeader.biWidth = lngWidthPixels
				binfo.bmiHeader.biPlanes = 1;
				binfo.bmiHeader.biCompression = 0;
				binfo.bmiHeader.biClrImportant = 0;
				binfo.bmiHeader.biClrUsed = 0;
				binfo.bmiHeader.biSize = 40;
				// size of this record
				binfo.bmiHeader.biXPelsPerMeter = 0;
				// ? copied from a file created in mspaint
				binfo.bmiHeader.biYPelsPerMeter = 0;
				bfhead.bfOffBits = 62;
				// lngWidthBytes = Int(lngWidthPixels / 8)
				// if it doesn't go even number of times we must add another byte
				// using intNumPadBytes as a temp variable here
				// intNumPadBytes = 8 - (lngWidthPixels Mod 8)
				// If intNumPadBytes > 0 And intNumPadBytes < 8 Then
				// lngWidthBytes = lngWidthBytes + 1
				// End If
				// 
				// must pad the buffer to dwords so find out how many bytes to add to each scanline to be even
				// intNumPadBytes = 4 - (lngWidthBytes Mod 4)   'must make it an even dword
				// If intNumPadBytes = 4 Then intNumPadBytes = 0
				// for each scanline we will actually write lngWidthBytes plus intNumPadBytes
				// the imagesize written will be (lngwidthbytes + intnumpadbytes) * lngheightpixels
				// binfo.bmiHeader.biSizeImage = (lngWidthBytes + intNumPadBytes) * lngHeightPixels
				// bfhead.bfSize = binfo.bmiHeader.biSizeImage + 62  'buffer + header info
				binfo.bmiColors[0].rgbBlue = 255;
				binfo.bmiColors[0].rgbGreen = 255;
				binfo.bmiColors[0].rgbRed = 255;
				binfo.bmiColors[1].rgbBlue = 0;
				binfo.bmiColors[1].rgbGreen = 0;
				binfo.bmiColors[1].rgbRed = 0;
				lngCurrentSrcByte = 0;
				lngMinY = modGlobalConstants.EleventyBillion;
				lngMinX = modGlobalConstants.EleventyBillion;
				lngMaxX = 0;
				lngMaxY = 0;
				for (Y = 0; Y <= lngHeightPixels - 1; Y++)
				{
					for (x = 0; x <= lngWidthPixels - 1; x++)
					{
						if (Color.FromArgb(srcBuf[lngCurrentSrcByte + 2], srcBuf[lngCurrentSrcByte + 1], srcBuf[lngCurrentSrcByte]) == Color.Black)
						{
							if (x < lngMinX)
							{
								lngMinX = x;
							}
							if (x > lngMaxX)
							{
								lngMaxX = x;
							}
							if (Y < lngMinY)
							{
								lngMinY = Y;
							}
							if (Y > lngMaxY)
							{
								lngMaxY = Y;
							}
						}
						lngCurrentSrcByte += 3;
					}
					// x
					lngCurrentSrcByte += intNumSrcPadBytes;
				}
				// Y
				// place a 5 pixel margin around it
				lngMinX -= 5;
				if (lngMinX < 0)
					lngMinX = 0;
				lngMinY -= 5;
				if (lngMinY < 0)
					lngMinY = 0;
				if (lngMaxX < lngWidthPixels - 5)
				{
					lngMaxX += 5;
				}
				if (lngMaxY < lngHeightPixels - 5)
				{
					lngMaxY += 5;
				}
				// now calculate the cropped image size
				lngCroppedWidth = lngMaxX - lngMinX + 1;
				lngCroppedHeight = lngMaxY - lngMinY + 1;
				binfo.bmiHeader.biHeight = lngCroppedHeight;
				binfo.bmiHeader.biWidth = lngCroppedWidth;
				intNumSrcPadBytes += (lngWidthPixels * 3) - ((lngMaxX + 1) * 3);
				// skip the padding and the cropped width together
				lngWidthBytes = FCConvert.ToInt32(Conversion.Int(lngCroppedWidth / 8.0));
				// if it doesn't go even number of times we must add another byte
				// using intNumPadBytes as a temp variable here
				intNumPadBytes = 8 - (lngCroppedWidth % 8);
				if (intNumPadBytes > 0 && intNumPadBytes < 8)
				{
					lngWidthBytes += 1;
				}
				// must pad the buffer to dwords so find out how many bytes to add to each scanline to be even
				intNumPadBytes = 4 - (lngWidthBytes % 4);
				// must make it an even dword
				if (intNumPadBytes == 4)
					intNumPadBytes = 0;
				// for each scanline we will actually write lngWidthBytes plus intNumPadBytes
				// the imagesize written will be (lngwidthbytes + intnumpadbytes) * lngheightpixels
				binfo.bmiHeader.biSizeImage = (lngWidthBytes + intNumPadBytes) * lngCroppedHeight;
				bfhead.bfSize = binfo.bmiHeader.biSizeImage + 62;
				// buffer + header info
				destBuf = new byte[binfo.bmiHeader.biSizeImage - 1 + 1];
				lngLeftOffset = (lngMinX) * 3;
				lngLineWidth = (3 * (lngMaxX + 1)) + intNumSrcPadBytes;
				lngCurrentSrcByte = lngMinY * lngLineWidth;
				lngCurrentDestByte = 0;
				intCurrentBit = 8;
				// we will process from srcBuf to destBuf and then write the buffer all at once
				for (Y = lngMinY; Y <= lngMaxY; Y++)
				{
					// y represents the current scanline
					bt = 255;
					// reset
					intCurrentBit = 8;
					lngCurrentSrcByte += lngLeftOffset;
					for (x = lngMinX; x <= lngMaxX; x++)
					{
						// x represents the column
						if (Color.FromArgb(srcBuf[lngCurrentSrcByte + 2], srcBuf[lngCurrentSrcByte + 1], srcBuf[lngCurrentSrcByte]) == Color.Black)
						{
							// leave it set
							intCurrentBit = intCurrentBit;
						}
						else
						{
							// zero out this bit
							switch (intCurrentBit)
							{
								case 8:
									{
										bt = Convert.ToByte(bt & 127);
										break;
									}
								case 7:
									{
										bt = Convert.ToByte(bt & 191);
										break;
									}
								case 6:
									{
										bt = Convert.ToByte(bt & 223);
										break;
									}
								case 5:
									{
										bt = Convert.ToByte(bt & 239);
										break;
									}
								case 4:
									{
										bt = Convert.ToByte(bt & 247);
										break;
									}
								case 3:
									{
										bt = Convert.ToByte(bt & 251);
										break;
									}
								case 2:
									{
										bt = Convert.ToByte(bt & 253);
										break;
									}
								case 1:
									{
										bt = Convert.ToByte(bt & 254);
										break;
									}
							}
							//end switch
						}
						intCurrentBit -= 1;
						if (intCurrentBit == 0)
						{
							// have processed 8 pixels so write the byte to the buffer
							intCurrentBit = 8;
							destBuf[lngCurrentDestByte] = bt;
							bt = 255;
							// reset it
							lngCurrentDestByte += 1;
						}
						lngCurrentSrcByte += 3;
						// skip to next pixel
					}
					// x
					if (intCurrentBit != 8)
					{
						// didn't write the last byte yet
						destBuf[lngCurrentDestByte] = bt;
						lngCurrentDestByte += 1;
					}
					// skip the padding
					lngCurrentSrcByte += intNumSrcPadBytes;
					lngCurrentDestByte += intNumPadBytes;
				}
				// Y
				FCFileSystem.FileClose();
				if (File.Exists(strFileName))
				{
					File.Delete(strFileName);
				}
				FCFileSystem.FileOpen(42, strFileName, OpenMode.Binary, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.FilePut(42, bfhead, -1);
				FCFileSystem.FilePut(42, binfo, -1);
				//FC:TODO
				//FCFileSystem.FilePut(42, destBuf, -1);
				FCFileSystem.FileClose();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ConvertBMPToMonochrome", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
