﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using fecherFoundation.VisualBasicLayer;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmNewREProperty.
	/// </summary>
	partial class frmNewREProperty : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label8;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblMRRLEXEMPTION;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblMRRLBLDGVAL;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lbl2;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lbl1;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblMRRLLANDVAL;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLocation;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblMapLotCopy;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label3;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label6;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label4;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROISoundValue;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkSound;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIPctFunct1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIUnits1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIGradePct1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIGradeCd1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROICond1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIYear1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIType1;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMROIPctPhys1;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label65;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcessProperty;
		public fecherFoundation.FCToolStripMenuItem mnuAddNewAccount;
		public fecherFoundation.FCToolStripMenuItem mnuAddACard;
		public fecherFoundation.FCToolStripMenuItem mnuSep;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteAccount;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteCard;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteDwellingData;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteOutbuilding;
		public fecherFoundation.FCToolStripMenuItem mnuSeparateCard;
		public fecherFoundation.FCToolStripMenuItem Separator1;
		public fecherFoundation.FCToolStripMenuItem mnuCalculate;
		public fecherFoundation.FCToolStripMenuItem mnuValuationReport;
		public fecherFoundation.FCToolStripMenuItem mnuViewDocs;
		public fecherFoundation.FCToolStripMenuItem mnuOptions;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuRECLComment;
		public fecherFoundation.FCToolStripMenuItem mnuCollectionsNote;
		public fecherFoundation.FCToolStripMenuItem mnuinterestedparties;
		public fecherFoundation.FCToolStripMenuItem mnuCopyProperty;
		public fecherFoundation.FCToolStripMenuItem mnuPendingTransfer;
		public fecherFoundation.FCToolStripMenuItem mnuSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSearchName;
		public fecherFoundation.FCToolStripMenuItem mnuSearchLocation;
		public fecherFoundation.FCToolStripMenuItem mnuSearchMapLot;
		public fecherFoundation.FCToolStripMenuItem mnuSeparator4;
		public fecherFoundation.FCToolStripMenuItem mnuPreviousAccountFromSearch;
		public fecherFoundation.FCToolStripMenuItem mnuNextAccountFromSearch;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAccountInformationSheet;
		public fecherFoundation.FCToolStripMenuItem mnuPrintCard;
		public fecherFoundation.FCToolStripMenuItem mnuPrintCards;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPropertyCard;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAllPropertyCards;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPropertyCardsPDF;
		public fecherFoundation.FCToolStripMenuItem mnuPrint1pagepropertycard;
		public fecherFoundation.FCToolStripMenuItem mnuPrintLabel;
		public fecherFoundation.FCToolStripMenuItem mnuPrintMailing;
		public fecherFoundation.FCToolStripMenuItem mnuPrintIncomeApproach;
		public fecherFoundation.FCToolStripMenuItem mnuPrintScreen;
		public fecherFoundation.FCToolStripMenuItem mnuPrintInterestedParties;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddOcc;
		public fecherFoundation.FCToolStripMenuItem mnuDelOcc;
		public fecherFoundation.FCToolStripMenuItem mnuIncomeSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuAddExpense;
		public fecherFoundation.FCToolStripMenuItem mnuDelExpense;
		public fecherFoundation.FCToolStripMenuItem mnuSketch;
		public fecherFoundation.FCToolStripMenuItem mnuEditSketch;
		public fecherFoundation.FCToolStripMenuItem mnuNewSketch;
		public fecherFoundation.FCToolStripMenuItem mnuEditConfig;
		public fecherFoundation.FCToolStripMenuItem mnuReassignSketch;
		public fecherFoundation.FCToolStripMenuItem mnuSketchSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuImportSQFT;
		public fecherFoundation.FCToolStripMenuItem mnuAll;
		public fecherFoundation.FCToolStripMenuItem mnuDwellingSqft;
		public fecherFoundation.FCToolStripMenuItem mnuOutbuildingSQFT;
		public fecherFoundation.FCToolStripMenuItem mnuSketchSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteSketch;
		public fecherFoundation.FCToolStripMenuItem mnuPictures;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPicture;
		public fecherFoundation.FCToolStripMenuItem mnuPicSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuImportPicture;
		public fecherFoundation.FCToolStripMenuItem mnuAddPicture;
		public fecherFoundation.FCToolStripMenuItem mnuDeletePicture;
		public fecherFoundation.FCToolStripMenuItem mnuSepar7;
		public fecherFoundation.FCToolStripMenuItem mnuPicMakePrimary;
		public fecherFoundation.FCToolStripMenuItem mnuPicMakeSecondary;
		public fecherFoundation.FCToolStripMenuItem mnuSeparator2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveQuit;
		public fecherFoundation.FCToolStripMenuItem mnuSavePending;
		public fecherFoundation.FCToolStripMenuItem mnuSeparator3;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		public fecherFoundation.FCTextBox MaskEdbox3;
		public fecherFoundation.FCTextBox MaskEdBox1;
		public fecherFoundation.FCTextBox txtMRRSMAPLOT;
		public fecherFoundation.FCTextBox txtMRRSLocnumalph;
		public fecherFoundation.FCTextBox txtMRRSLocapt;
		public fecherFoundation.FCTextBox txtMRRSLocStreet;
		public ToolTipCtl ToolTip;
		public FCGrid GridToolTip;
		public Global.T2KDateBox T2KDateInspected;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtMRRSRef2;
		public fecherFoundation.FCTextBox txtMRRSRef1;
		public fecherFoundation.FCTextBox txtMRRSPrevMaster;
		public fecherFoundation.FCCheckBox chkRLivingTrust;
		public fecherFoundation.FCButton cmdLogSale;
		public fecherFoundation.FCCheckBox chkBankruptcy;
		public fecherFoundation.FCCheckBox chkTaxAcquired;
		public fecherFoundation.FCPanel framRegOwner;
		public fecherFoundation.FCButton cmdChangeOwnership;
		public fecherFoundation.FCButton cmdRemoveOwner;
		public fecherFoundation.FCButton cmdRemoveSecOwner;
		public fecherFoundation.FCTextBox txt2ndOwnerID;
		public fecherFoundation.FCButton cmdEditSecOwner;
		public fecherFoundation.FCButton cmdSearchSecOwner;
		public fecherFoundation.FCTextBox txtOwnerID;
		public fecherFoundation.FCButton cmdEditOwner;
		public fecherFoundation.FCButton cmdSearchOwner;
		public fecherFoundation.FCLabel lblAddress;
		public fecherFoundation.FCLabel lblOwner1;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label43;
		public fecherFoundation.FCLabel Label2_9;
		public fecherFoundation.FCLabel lbl2ndOwner;
		public fecherFoundation.FCCommonDialog CommonDialog1;
		public FCGrid GridExempts;
		public FCGrid SaleGrid;
		public fecherFoundation.FCGrid BPGrid;
		public FCGrid gridTranCode;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8_8;
		public fecherFoundation.FCLabel Label8_7;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel lblDate_5;
		public fecherFoundation.FCLabel lblDate_4;
		public fecherFoundation.FCLabel lbl2_3;
		public fecherFoundation.FCLabel lbl1_2;
		public fecherFoundation.FCLabel lblDate_7;
		public fecherFoundation.FCLabel lblDate_0;
		public fecherFoundation.FCPictureBox imgDocuments;
		public fecherFoundation.FCLabel lblMRRLEXEMPTION_30;
		public fecherFoundation.FCLabel lblMRRLBLDGVAL_29;
		public fecherFoundation.FCLabel lblMRRLLANDVAL_28;
		public fecherFoundation.FCLabel lblTaxable;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtMRPIAcres;
		public fecherFoundation.FCTextBox txtMRPIStreetCode;
		public fecherFoundation.FCTextBox txtMRPIOpen1;
		public fecherFoundation.FCTextBox txtMRPIXCoord;
		public fecherFoundation.FCTextBox txtMRPIYCoord;
		public fecherFoundation.FCTextBox txtMRPIOpen2;
		public fecherFoundation.FCCheckBox chkZoneOverride;
		public FCGrid gridLandCode;
		public FCGrid gridBldgCode;
		public FCGrid GridLand;
		public FCGrid gridPropertyCode;
		public FCGrid GridNeighborhood;
		public FCGrid GridZone;
		public FCGrid GridSecZone;
		public FCGrid GridTopography;
		public FCGrid GridUtilities;
		public FCGrid GridStreet;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label8_36;
		public fecherFoundation.FCLabel Label8_10;
		public fecherFoundation.FCLabel Label8_11;
		public fecherFoundation.FCLabel Label8_12;
		public fecherFoundation.FCLabel Label8_13;
		public fecherFoundation.FCLabel Label8_14;
		public fecherFoundation.FCLabel Label8_15;
		public fecherFoundation.FCLabel Label8_16;
		public fecherFoundation.FCLabel Label8_17;
		public fecherFoundation.FCLabel Label8_18;
		public fecherFoundation.FCLabel Label8_19;
		public fecherFoundation.FCLabel Label8_20;
		public fecherFoundation.FCLabel lblMapLot;
		public fecherFoundation.FCLabel lblLocation_0;
		public fecherFoundation.FCLabel lblMapLotCopy_0;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCGrid GridDwelling1;
		public fecherFoundation.FCGrid GridDwelling2;
		public fecherFoundation.FCGrid GridDwelling3;
		public fecherFoundation.FCLabel lblMapLotCopy_1;
		public fecherFoundation.FCLabel lblDwel;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox mebC1Heat;
		public fecherFoundation.FCTextBox mebc2grade;
		public fecherFoundation.FCTextBox mebC1Grade;
		public fecherFoundation.FCTextBox mebOcc1;
		public fecherFoundation.FCTextBox mebC1Height;
		public fecherFoundation.FCTextBox mebDwel1;
		public fecherFoundation.FCTextBox mebC1Class;
		public fecherFoundation.FCTextBox mebC1Quality;
		public fecherFoundation.FCTextBox mebC1ExteriorWalls;
		public fecherFoundation.FCTextBox mebC1Stories;
		public fecherFoundation.FCTextBox mebC1Floor;
		public fecherFoundation.FCTextBox mebC1Perimeter;
		public fecherFoundation.FCTextBox mebC1Built;
		public fecherFoundation.FCTextBox mebC1Remodel;
		public fecherFoundation.FCTextBox mebC1Condition;
		public fecherFoundation.FCTextBox mebC1Phys;
		public fecherFoundation.FCTextBox mebC1Funct;
		public fecherFoundation.FCTextBox mebCMEcon;
		public fecherFoundation.FCTextBox mebOcc2;
		public fecherFoundation.FCTextBox mebDwel2;
		public fecherFoundation.FCTextBox mebC2Class;
		public fecherFoundation.FCTextBox mebC2Quality;
		public fecherFoundation.FCTextBox mebC2ExtWalls;
		public fecherFoundation.FCTextBox mebC2Stories;
		public fecherFoundation.FCTextBox mebC2Floor;
		public fecherFoundation.FCTextBox mebC2Perimeter;
		public fecherFoundation.FCTextBox mebC2Heat;
		public fecherFoundation.FCTextBox mebC2Built;
		public fecherFoundation.FCTextBox mebC2Remodel;
		public fecherFoundation.FCTextBox mebC2Condition;
		public fecherFoundation.FCTextBox mebC2Phys;
		public fecherFoundation.FCTextBox mebC2Funct;
		public fecherFoundation.FCTextBox mebC2Height;
		public fecherFoundation.FCLabel Label3_31;
		public fecherFoundation.FCLabel Label3_30;
		public fecherFoundation.FCLabel Label3_29;
		public fecherFoundation.FCLabel Label3_25;
		public fecherFoundation.FCLabel Label3_24;
		public fecherFoundation.FCLabel Label3_23;
		public fecherFoundation.FCLabel Label3_22;
		public fecherFoundation.FCLabel Label3_21;
		public fecherFoundation.FCLabel Label3_20;
		public fecherFoundation.FCLabel Label3_19;
		public fecherFoundation.FCLabel Label3_18;
		public fecherFoundation.FCLabel Label3_17;
		public fecherFoundation.FCLabel Label3_16;
		public fecherFoundation.FCLabel Label3_15;
		public fecherFoundation.FCLabel Label3_14;
		public fecherFoundation.FCLabel Label3_13;
		public fecherFoundation.FCLabel Label3_12;
		public fecherFoundation.FCLabel Label3_11;
		public fecherFoundation.FCLabel Label3_10;
		public fecherFoundation.FCLabel Label3_9;
		public fecherFoundation.FCLabel Label3_8;
		public fecherFoundation.FCLabel Label3_7;
		public fecherFoundation.FCLabel Label3_6;
		public fecherFoundation.FCLabel Label3_5;
		public fecherFoundation.FCLabel Label3_4;
		public fecherFoundation.FCLabel Label3_3;
		public fecherFoundation.FCLabel Label3_2;
		public fecherFoundation.FCLabel Label3_1;
		public fecherFoundation.FCLabel Label3_32;
		public fecherFoundation.FCLabel lblcomm;
		public fecherFoundation.FCLabel Label6_0;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4_0;
		public fecherFoundation.FCTabPage SSTab1_Page5;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtMROISoundValue_9;
		public fecherFoundation.FCCheckBox chkSound_9;
		public fecherFoundation.FCTextBox txtMROISoundValue_8;
		public fecherFoundation.FCCheckBox chkSound_8;
		public fecherFoundation.FCTextBox txtMROISoundValue_7;
		public fecherFoundation.FCCheckBox chkSound_7;
		public fecherFoundation.FCTextBox txtMROISoundValue_6;
		public fecherFoundation.FCCheckBox chkSound_6;
		public fecherFoundation.FCTextBox txtMROISoundValue_5;
		public fecherFoundation.FCCheckBox chkSound_5;
		public fecherFoundation.FCTextBox txtMROISoundValue_4;
		public fecherFoundation.FCCheckBox chkSound_4;
		public fecherFoundation.FCTextBox txtMROISoundValue_3;
		public fecherFoundation.FCCheckBox chkSound_3;
		public fecherFoundation.FCTextBox txtMROISoundValue_2;
		public fecherFoundation.FCCheckBox chkSound_2;
		public fecherFoundation.FCTextBox txtMROISoundValue_1;
		public fecherFoundation.FCCheckBox chkSound_1;
		public fecherFoundation.FCTextBox txtMROISoundValue_0;
		public fecherFoundation.FCCheckBox chkSound_0;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_3;
		public fecherFoundation.FCTextBox txtMROIUnits1_3;
		public fecherFoundation.FCTextBox txtMROIGradePct1_8;
		public fecherFoundation.FCTextBox txtMROIUnits1_6;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_9;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_1;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_2;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_4;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_5;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_6;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_7;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_8;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_9;
		public fecherFoundation.FCTextBox txtMROICond1_1;
		public fecherFoundation.FCTextBox txtMROICond1_2;
		public fecherFoundation.FCTextBox txtMROICond1_3;
		public fecherFoundation.FCTextBox txtMROICond1_4;
		public fecherFoundation.FCTextBox txtMROICond1_5;
		public fecherFoundation.FCTextBox txtMROICond1_6;
		public fecherFoundation.FCTextBox txtMROICond1_7;
		public fecherFoundation.FCTextBox txtMROICond1_8;
		public fecherFoundation.FCTextBox txtMROICond1_9;
		public fecherFoundation.FCTextBox txtMROIGradePct1_1;
		public fecherFoundation.FCTextBox txtMROIGradePct1_2;
		public fecherFoundation.FCTextBox txtMROIGradePct1_3;
		public fecherFoundation.FCTextBox txtMROIGradePct1_4;
		public fecherFoundation.FCTextBox txtMROIGradePct1_5;
		public fecherFoundation.FCTextBox txtMROIGradePct1_6;
		public fecherFoundation.FCTextBox txtMROIGradePct1_7;
		public fecherFoundation.FCTextBox txtMROIGradePct1_9;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_0;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_1;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_2;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_3;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_4;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_5;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_6;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_7;
		public fecherFoundation.FCTextBox txtMROIGradeCd1_8;
		public fecherFoundation.FCTextBox txtMROIUnits1_1;
		public fecherFoundation.FCTextBox txtMROIUnits1_2;
		public fecherFoundation.FCTextBox txtMROIUnits1_4;
		public fecherFoundation.FCTextBox txtMROIUnits1_5;
		public fecherFoundation.FCTextBox txtMROIUnits1_7;
		public fecherFoundation.FCTextBox txtMROIUnits1_8;
		public fecherFoundation.FCTextBox txtMROIUnits1_9;
		public fecherFoundation.FCTextBox txtMROIYear1_1;
		public fecherFoundation.FCTextBox txtMROIYear1_2;
		public fecherFoundation.FCTextBox txtMROIYear1_3;
		public fecherFoundation.FCTextBox txtMROIYear1_4;
		public fecherFoundation.FCTextBox txtMROIYear1_5;
		public fecherFoundation.FCTextBox txtMROIYear1_6;
		public fecherFoundation.FCTextBox txtMROIYear1_7;
		public fecherFoundation.FCTextBox txtMROIYear1_8;
		public fecherFoundation.FCTextBox txtMROIYear1_9;
		public fecherFoundation.FCTextBox txtMROIType1_1;
		public fecherFoundation.FCTextBox txtMROIType1_2;
		public fecherFoundation.FCTextBox txtMROIType1_3;
		public fecherFoundation.FCTextBox txtMROIType1_4;
		public fecherFoundation.FCTextBox txtMROIType1_5;
		public fecherFoundation.FCTextBox txtMROIType1_6;
		public fecherFoundation.FCTextBox txtMROIType1_7;
		public fecherFoundation.FCTextBox txtMROIType1_8;
		public fecherFoundation.FCTextBox txtMROIType1_9;
		public fecherFoundation.FCTextBox txtMROIUnits1_0;
		public fecherFoundation.FCTextBox txtMROIYear1_0;
		public fecherFoundation.FCTextBox txtMROIType1_0;
		public fecherFoundation.FCTextBox txtMROIGradePct1_0;
		public fecherFoundation.FCTextBox txtMROICond1_0;
		public fecherFoundation.FCTextBox txtMROIPctFunct1_0;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_0;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_9;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_8;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_7;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_6;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_5;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_4;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_3;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_2;
		public fecherFoundation.FCTextBox txtMROIPctPhys1_1;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel lblMapLotCopy_2;
		public fecherFoundation.FCLabel Label2_8;
		public fecherFoundation.FCLabel Label2_7;
		public fecherFoundation.FCLabel Label2_6;
		public fecherFoundation.FCLabel Label2_5;
		public fecherFoundation.FCLabel Label2_4;
		public fecherFoundation.FCLabel Label2_3;
		public fecherFoundation.FCLabel Label2_2;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel lblOut;
		public fecherFoundation.FCTabPage SSTab1_Page6;
		public fecherFoundation.FCTextBox txtTotalSF;
		public fecherFoundation.FCTextBox txtTotalExpense;
		public fecherFoundation.FCGrid GridValues;
		public fecherFoundation.FCGrid GridExpenses;
		public fecherFoundation.FCGrid Grid1;
		public fecherFoundation.FCLabel lblTotalSF;
		public fecherFoundation.FCLabel lblTotalExpense;
		public fecherFoundation.FCTabPage SSTab1_Page7;
		public fecherFoundation.FCToolBar Toolbar1;
		private FCToolBarButton BTNPICADDPIC;
		private FCToolBarButton BTNPICDELETEPIC;
		private FCToolBarButton BTNPRINTPICTURE;
		public fecherFoundation.FCPictureBox Picture1;
		public fecherFoundation.FCTextBox txtPicDesc;
		public fecherFoundation.FCButton cmdNextPicture;
		public fecherFoundation.FCButton cmdPrevPicture;
		public fecherFoundation.FCFrame framPicture;
		public fecherFoundation.FCPictureBox imgPicture;
		public fecherFoundation.FCFrame framPicDimensions;
		public fecherFoundation.FCComboBox cmbPrintSize;
		public fecherFoundation.FCLabel lblPicName;
		public fecherFoundation.FCLabel lblSketchNumber;
		public fecherFoundation.FCLabel lblName2;
		public fecherFoundation.FCTabPage SSTab1_Page8;
		public fecherFoundation.FCPictureBox Picture2;
		public fecherFoundation.FCPanel framSketch;
		public fecherFoundation.FCPictureBox imSketch;
		public fecherFoundation.FCButton cmdNextSketch;
		public fecherFoundation.FCButton cmdPrevSketch;
		public fecherFoundation.FCLabel lblInterestedPartiesPresent;
		public fecherFoundation.FCLabel lblMRHLAcctNum;
		public fecherFoundation.FCLabel lblComment;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel lblNumCards;
		public fecherFoundation.FCLabel Label65_18;
		public fecherFoundation.FCLabel Label65_8;
		public fecherFoundation.FCLabel Label65_9;
		public fecherFoundation.FCLabel lblPendingFlag;
		public fecherFoundation.FCLabel lblPicturePresent;
		public fecherFoundation.FCLabel lblSketchPresent;
		public fecherFoundation.FCLabel lblDate_2;
		public fecherFoundation.FCLabel Label8_6;
		private Wisej.Web.ToolTip ToolTip1;
        private Wisej.Web.Timer winSketchImageTimer;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewREProperty));
            this.MaskEdbox3 = new fecherFoundation.FCTextBox();
            this.MaskEdBox1 = new fecherFoundation.FCTextBox();
            this.txtMRRSMAPLOT = new fecherFoundation.FCTextBox();
            this.txtMRRSLocnumalph = new fecherFoundation.FCTextBox();
            this.txtMRRSLocapt = new fecherFoundation.FCTextBox();
            this.txtMRRSLocStreet = new fecherFoundation.FCTextBox();
            this.ToolTip = new TWRE0000.ToolTipCtl();
            this.GridToolTip = new fecherFoundation.FCGrid();
            this.T2KDateInspected = new Global.T2KDateBox();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.SaleGrid = new fecherFoundation.FCGrid();
            this.txtMRRSRef2 = new fecherFoundation.FCTextBox();
            this.txtMRRSRef1 = new fecherFoundation.FCTextBox();
            this.txtMRRSPrevMaster = new fecherFoundation.FCTextBox();
            this.chkRLivingTrust = new fecherFoundation.FCCheckBox();
            this.cmdLogSale = new fecherFoundation.FCButton();
            this.chkBankruptcy = new fecherFoundation.FCCheckBox();
            this.chkTaxAcquired = new fecherFoundation.FCCheckBox();
            this.framRegOwner = new fecherFoundation.FCPanel();
            this.txtDeedName2 = new fecherFoundation.FCTextBox();
            this.txtDeedName1 = new fecherFoundation.FCTextBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.cmdChangeOwnership = new fecherFoundation.FCButton();
            this.cmdRemoveOwner = new fecherFoundation.FCButton();
            this.cmdRemoveSecOwner = new fecherFoundation.FCButton();
            this.txt2ndOwnerID = new fecherFoundation.FCTextBox();
            this.cmdEditSecOwner = new fecherFoundation.FCButton();
            this.cmdSearchSecOwner = new fecherFoundation.FCButton();
            this.txtOwnerID = new fecherFoundation.FCTextBox();
            this.cmdEditOwner = new fecherFoundation.FCButton();
            this.cmdSearchOwner = new fecherFoundation.FCButton();
            this.lblAddress = new fecherFoundation.FCLabel();
            this.lblOwner1 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label43 = new fecherFoundation.FCLabel();
            this.Label2_9 = new fecherFoundation.FCLabel();
            this.lbl2ndOwner = new fecherFoundation.FCLabel();
            this.GridExempts = new fecherFoundation.FCGrid();
            this.BPGrid = new fecherFoundation.FCGrid();
            this.gridTranCode = new fecherFoundation.FCGrid();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label8_8 = new fecherFoundation.FCLabel();
            this.Label8_7 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.lblDate_5 = new fecherFoundation.FCLabel();
            this.lblDate_4 = new fecherFoundation.FCLabel();
            this.lbl2_3 = new fecherFoundation.FCLabel();
            this.lbl1_2 = new fecherFoundation.FCLabel();
            this.lblDate_7 = new fecherFoundation.FCLabel();
            this.lblDate_0 = new fecherFoundation.FCLabel();
            this.imgDocuments = new fecherFoundation.FCPictureBox();
            this.lblMRRLEXEMPTION_30 = new fecherFoundation.FCLabel();
            this.lblMRRLBLDGVAL_29 = new fecherFoundation.FCLabel();
            this.lblMRRLLANDVAL_28 = new fecherFoundation.FCLabel();
            this.lblTaxable = new fecherFoundation.FCLabel();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.lblMapLot = new fecherFoundation.FCLabel();
            this.lblLocation_0 = new fecherFoundation.FCLabel();
            this.lblMapLotCopy_0 = new fecherFoundation.FCLabel();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtMRPIAcres = new fecherFoundation.FCTextBox();
            this.txtMRPIStreetCode = new fecherFoundation.FCTextBox();
            this.txtMRPIOpen1 = new fecherFoundation.FCTextBox();
            this.txtMRPIXCoord = new fecherFoundation.FCTextBox();
            this.txtMRPIYCoord = new fecherFoundation.FCTextBox();
            this.txtMRPIOpen2 = new fecherFoundation.FCTextBox();
            this.chkZoneOverride = new fecherFoundation.FCCheckBox();
            this.GridLand = new fecherFoundation.FCGrid();
            this.gridLandCode = new fecherFoundation.FCGrid();
            this.gridBldgCode = new fecherFoundation.FCGrid();
            this.gridPropertyCode = new fecherFoundation.FCGrid();
            this.GridNeighborhood = new fecherFoundation.FCGrid();
            this.GridZone = new fecherFoundation.FCGrid();
            this.GridSecZone = new fecherFoundation.FCGrid();
            this.GridTopography = new fecherFoundation.FCGrid();
            this.GridUtilities = new fecherFoundation.FCGrid();
            this.GridStreet = new fecherFoundation.FCGrid();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label8_36 = new fecherFoundation.FCLabel();
            this.Label8_10 = new fecherFoundation.FCLabel();
            this.Label8_11 = new fecherFoundation.FCLabel();
            this.Label8_12 = new fecherFoundation.FCLabel();
            this.Label8_13 = new fecherFoundation.FCLabel();
            this.Label8_14 = new fecherFoundation.FCLabel();
            this.Label8_15 = new fecherFoundation.FCLabel();
            this.Label8_16 = new fecherFoundation.FCLabel();
            this.Label8_17 = new fecherFoundation.FCLabel();
            this.Label8_18 = new fecherFoundation.FCLabel();
            this.Label8_19 = new fecherFoundation.FCLabel();
            this.Label8_20 = new fecherFoundation.FCLabel();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.GridDwelling1 = new fecherFoundation.FCGrid();
            this.GridDwelling2 = new fecherFoundation.FCGrid();
            this.GridDwelling3 = new fecherFoundation.FCGrid();
            this.lblMapLotCopy_1 = new fecherFoundation.FCLabel();
            this.lblDwel = new fecherFoundation.FCLabel();
            this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.mebC1Heat = new fecherFoundation.FCTextBox();
            this.mebc2grade = new fecherFoundation.FCTextBox();
            this.mebC1Grade = new fecherFoundation.FCTextBox();
            this.mebOcc1 = new fecherFoundation.FCTextBox();
            this.mebC1Height = new fecherFoundation.FCTextBox();
            this.mebDwel1 = new fecherFoundation.FCTextBox();
            this.mebC1Class = new fecherFoundation.FCTextBox();
            this.mebC1Quality = new fecherFoundation.FCTextBox();
            this.mebC1ExteriorWalls = new fecherFoundation.FCTextBox();
            this.mebC1Stories = new fecherFoundation.FCTextBox();
            this.mebC1Floor = new fecherFoundation.FCTextBox();
            this.mebC1Perimeter = new fecherFoundation.FCTextBox();
            this.mebC1Built = new fecherFoundation.FCTextBox();
            this.mebC1Remodel = new fecherFoundation.FCTextBox();
            this.mebC1Condition = new fecherFoundation.FCTextBox();
            this.mebC1Phys = new fecherFoundation.FCTextBox();
            this.mebC1Funct = new fecherFoundation.FCTextBox();
            this.mebCMEcon = new fecherFoundation.FCTextBox();
            this.mebOcc2 = new fecherFoundation.FCTextBox();
            this.mebDwel2 = new fecherFoundation.FCTextBox();
            this.mebC2Class = new fecherFoundation.FCTextBox();
            this.mebC2Quality = new fecherFoundation.FCTextBox();
            this.mebC2ExtWalls = new fecherFoundation.FCTextBox();
            this.mebC2Stories = new fecherFoundation.FCTextBox();
            this.mebC2Floor = new fecherFoundation.FCTextBox();
            this.mebC2Perimeter = new fecherFoundation.FCTextBox();
            this.mebC2Heat = new fecherFoundation.FCTextBox();
            this.mebC2Built = new fecherFoundation.FCTextBox();
            this.mebC2Remodel = new fecherFoundation.FCTextBox();
            this.mebC2Condition = new fecherFoundation.FCTextBox();
            this.mebC2Phys = new fecherFoundation.FCTextBox();
            this.mebC2Funct = new fecherFoundation.FCTextBox();
            this.mebC2Height = new fecherFoundation.FCTextBox();
            this.Label3_31 = new fecherFoundation.FCLabel();
            this.Label3_30 = new fecherFoundation.FCLabel();
            this.Label3_29 = new fecherFoundation.FCLabel();
            this.Label3_25 = new fecherFoundation.FCLabel();
            this.Label3_24 = new fecherFoundation.FCLabel();
            this.Label3_23 = new fecherFoundation.FCLabel();
            this.Label3_22 = new fecherFoundation.FCLabel();
            this.Label3_21 = new fecherFoundation.FCLabel();
            this.Label3_20 = new fecherFoundation.FCLabel();
            this.Label3_19 = new fecherFoundation.FCLabel();
            this.Label3_18 = new fecherFoundation.FCLabel();
            this.Label3_17 = new fecherFoundation.FCLabel();
            this.Label3_16 = new fecherFoundation.FCLabel();
            this.Label3_15 = new fecherFoundation.FCLabel();
            this.Label3_14 = new fecherFoundation.FCLabel();
            this.Label3_13 = new fecherFoundation.FCLabel();
            this.Label3_12 = new fecherFoundation.FCLabel();
            this.Label3_11 = new fecherFoundation.FCLabel();
            this.Label3_10 = new fecherFoundation.FCLabel();
            this.Label3_9 = new fecherFoundation.FCLabel();
            this.Label3_8 = new fecherFoundation.FCLabel();
            this.Label3_7 = new fecherFoundation.FCLabel();
            this.Label3_6 = new fecherFoundation.FCLabel();
            this.Label3_5 = new fecherFoundation.FCLabel();
            this.Label3_4 = new fecherFoundation.FCLabel();
            this.Label3_3 = new fecherFoundation.FCLabel();
            this.Label3_2 = new fecherFoundation.FCLabel();
            this.Label3_1 = new fecherFoundation.FCLabel();
            this.Label3_32 = new fecherFoundation.FCLabel();
            this.lblcomm = new fecherFoundation.FCLabel();
            this.Label6_0 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4_0 = new fecherFoundation.FCLabel();
            this.SSTab1_Page5 = new fecherFoundation.FCTabPage();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtMROISoundValue_9 = new fecherFoundation.FCTextBox();
            this.chkSound_9 = new fecherFoundation.FCCheckBox();
            this.txtMROISoundValue_8 = new fecherFoundation.FCTextBox();
            this.chkSound_8 = new fecherFoundation.FCCheckBox();
            this.txtMROISoundValue_7 = new fecherFoundation.FCTextBox();
            this.chkSound_7 = new fecherFoundation.FCCheckBox();
            this.txtMROISoundValue_6 = new fecherFoundation.FCTextBox();
            this.chkSound_6 = new fecherFoundation.FCCheckBox();
            this.txtMROISoundValue_5 = new fecherFoundation.FCTextBox();
            this.chkSound_5 = new fecherFoundation.FCCheckBox();
            this.txtMROISoundValue_4 = new fecherFoundation.FCTextBox();
            this.chkSound_4 = new fecherFoundation.FCCheckBox();
            this.txtMROISoundValue_3 = new fecherFoundation.FCTextBox();
            this.chkSound_3 = new fecherFoundation.FCCheckBox();
            this.txtMROISoundValue_2 = new fecherFoundation.FCTextBox();
            this.chkSound_2 = new fecherFoundation.FCCheckBox();
            this.txtMROISoundValue_1 = new fecherFoundation.FCTextBox();
            this.chkSound_1 = new fecherFoundation.FCCheckBox();
            this.txtMROISoundValue_0 = new fecherFoundation.FCTextBox();
            this.chkSound_0 = new fecherFoundation.FCCheckBox();
            this.txtMROIPctFunct1_3 = new fecherFoundation.FCTextBox();
            this.txtMROIUnits1_3 = new fecherFoundation.FCTextBox();
            this.txtMROIGradePct1_8 = new fecherFoundation.FCTextBox();
            this.txtMROIUnits1_6 = new fecherFoundation.FCTextBox();
            this.txtMROIGradeCd1_9 = new fecherFoundation.FCTextBox();
            this.txtMROIPctFunct1_1 = new fecherFoundation.FCTextBox();
            this.txtMROIPctFunct1_2 = new fecherFoundation.FCTextBox();
            this.txtMROIPctFunct1_4 = new fecherFoundation.FCTextBox();
            this.txtMROIPctFunct1_5 = new fecherFoundation.FCTextBox();
            this.txtMROIPctFunct1_6 = new fecherFoundation.FCTextBox();
            this.txtMROIPctFunct1_7 = new fecherFoundation.FCTextBox();
            this.txtMROIPctFunct1_8 = new fecherFoundation.FCTextBox();
            this.txtMROIPctFunct1_9 = new fecherFoundation.FCTextBox();
            this.txtMROICond1_1 = new fecherFoundation.FCTextBox();
            this.txtMROICond1_2 = new fecherFoundation.FCTextBox();
            this.txtMROICond1_3 = new fecherFoundation.FCTextBox();
            this.txtMROICond1_4 = new fecherFoundation.FCTextBox();
            this.txtMROICond1_5 = new fecherFoundation.FCTextBox();
            this.txtMROICond1_6 = new fecherFoundation.FCTextBox();
            this.txtMROICond1_7 = new fecherFoundation.FCTextBox();
            this.txtMROICond1_8 = new fecherFoundation.FCTextBox();
            this.txtMROICond1_9 = new fecherFoundation.FCTextBox();
            this.txtMROIGradePct1_1 = new fecherFoundation.FCTextBox();
            this.txtMROIGradePct1_2 = new fecherFoundation.FCTextBox();
            this.txtMROIGradePct1_3 = new fecherFoundation.FCTextBox();
            this.txtMROIGradePct1_4 = new fecherFoundation.FCTextBox();
            this.txtMROIGradePct1_5 = new fecherFoundation.FCTextBox();
            this.txtMROIGradePct1_6 = new fecherFoundation.FCTextBox();
            this.txtMROIGradePct1_7 = new fecherFoundation.FCTextBox();
            this.txtMROIGradePct1_9 = new fecherFoundation.FCTextBox();
            this.txtMROIGradeCd1_0 = new fecherFoundation.FCTextBox();
            this.txtMROIGradeCd1_1 = new fecherFoundation.FCTextBox();
            this.txtMROIGradeCd1_2 = new fecherFoundation.FCTextBox();
            this.txtMROIGradeCd1_3 = new fecherFoundation.FCTextBox();
            this.txtMROIGradeCd1_4 = new fecherFoundation.FCTextBox();
            this.txtMROIGradeCd1_5 = new fecherFoundation.FCTextBox();
            this.txtMROIGradeCd1_6 = new fecherFoundation.FCTextBox();
            this.txtMROIGradeCd1_7 = new fecherFoundation.FCTextBox();
            this.txtMROIGradeCd1_8 = new fecherFoundation.FCTextBox();
            this.txtMROIUnits1_1 = new fecherFoundation.FCTextBox();
            this.txtMROIUnits1_2 = new fecherFoundation.FCTextBox();
            this.txtMROIUnits1_4 = new fecherFoundation.FCTextBox();
            this.txtMROIUnits1_5 = new fecherFoundation.FCTextBox();
            this.txtMROIUnits1_7 = new fecherFoundation.FCTextBox();
            this.txtMROIUnits1_8 = new fecherFoundation.FCTextBox();
            this.txtMROIUnits1_9 = new fecherFoundation.FCTextBox();
            this.txtMROIYear1_1 = new fecherFoundation.FCTextBox();
            this.txtMROIYear1_2 = new fecherFoundation.FCTextBox();
            this.txtMROIYear1_3 = new fecherFoundation.FCTextBox();
            this.txtMROIYear1_4 = new fecherFoundation.FCTextBox();
            this.txtMROIYear1_5 = new fecherFoundation.FCTextBox();
            this.txtMROIYear1_6 = new fecherFoundation.FCTextBox();
            this.txtMROIYear1_7 = new fecherFoundation.FCTextBox();
            this.txtMROIYear1_8 = new fecherFoundation.FCTextBox();
            this.txtMROIYear1_9 = new fecherFoundation.FCTextBox();
            this.txtMROIType1_1 = new fecherFoundation.FCTextBox();
            this.txtMROIType1_2 = new fecherFoundation.FCTextBox();
            this.txtMROIType1_3 = new fecherFoundation.FCTextBox();
            this.txtMROIType1_4 = new fecherFoundation.FCTextBox();
            this.txtMROIType1_5 = new fecherFoundation.FCTextBox();
            this.txtMROIType1_6 = new fecherFoundation.FCTextBox();
            this.txtMROIType1_7 = new fecherFoundation.FCTextBox();
            this.txtMROIType1_8 = new fecherFoundation.FCTextBox();
            this.txtMROIType1_9 = new fecherFoundation.FCTextBox();
            this.txtMROIUnits1_0 = new fecherFoundation.FCTextBox();
            this.txtMROIYear1_0 = new fecherFoundation.FCTextBox();
            this.txtMROIType1_0 = new fecherFoundation.FCTextBox();
            this.txtMROIGradePct1_0 = new fecherFoundation.FCTextBox();
            this.txtMROICond1_0 = new fecherFoundation.FCTextBox();
            this.txtMROIPctFunct1_0 = new fecherFoundation.FCTextBox();
            this.txtMROIPctPhys1_0 = new fecherFoundation.FCTextBox();
            this.txtMROIPctPhys1_9 = new fecherFoundation.FCTextBox();
            this.txtMROIPctPhys1_8 = new fecherFoundation.FCTextBox();
            this.txtMROIPctPhys1_7 = new fecherFoundation.FCTextBox();
            this.txtMROIPctPhys1_6 = new fecherFoundation.FCTextBox();
            this.txtMROIPctPhys1_5 = new fecherFoundation.FCTextBox();
            this.txtMROIPctPhys1_4 = new fecherFoundation.FCTextBox();
            this.txtMROIPctPhys1_3 = new fecherFoundation.FCTextBox();
            this.txtMROIPctPhys1_2 = new fecherFoundation.FCTextBox();
            this.txtMROIPctPhys1_1 = new fecherFoundation.FCTextBox();
            this.Label2_1 = new fecherFoundation.FCLabel();
            this.lblMapLotCopy_2 = new fecherFoundation.FCLabel();
            this.Label2_8 = new fecherFoundation.FCLabel();
            this.Label2_7 = new fecherFoundation.FCLabel();
            this.Label2_6 = new fecherFoundation.FCLabel();
            this.Label2_5 = new fecherFoundation.FCLabel();
            this.Label2_4 = new fecherFoundation.FCLabel();
            this.Label2_3 = new fecherFoundation.FCLabel();
            this.Label2_2 = new fecherFoundation.FCLabel();
            this.Label2_0 = new fecherFoundation.FCLabel();
            this.lblOut = new fecherFoundation.FCLabel();
            this.SSTab1_Page6 = new fecherFoundation.FCTabPage();
            this.txtTotalSF = new fecherFoundation.FCTextBox();
            this.txtTotalExpense = new fecherFoundation.FCTextBox();
            this.GridValues = new fecherFoundation.FCGrid();
            this.GridExpenses = new fecherFoundation.FCGrid();
            this.Grid1 = new fecherFoundation.FCGrid();
            this.lblTotalSF = new fecherFoundation.FCLabel();
            this.lblTotalExpense = new fecherFoundation.FCLabel();
            this.SSTab1_Page7 = new fecherFoundation.FCTabPage();
            this.Toolbar1 = new fecherFoundation.FCToolBar();
            this.BTNPICADDPIC = new fecherFoundation.FCToolBarButton();
            this.BTNPICDELETEPIC = new fecherFoundation.FCToolBarButton();
            this.BTNPRINTPICTURE = new fecherFoundation.FCToolBarButton();
            this.Picture1 = new fecherFoundation.FCPictureBox();
            this.txtPicDesc = new fecherFoundation.FCTextBox();
            this.cmdNextPicture = new fecherFoundation.FCButton();
            this.cmdPrevPicture = new fecherFoundation.FCButton();
            this.framPicture = new fecherFoundation.FCFrame();
            this.imgPicture = new fecherFoundation.FCPictureBox();
            this.framPicDimensions = new fecherFoundation.FCFrame();
            this.cmbPrintSize = new fecherFoundation.FCComboBox();
            this.lblPicName = new fecherFoundation.FCLabel();
            this.lblSketchNumber = new fecherFoundation.FCLabel();
            this.lblName2 = new fecherFoundation.FCLabel();
            this.SSTab1_Page8 = new fecherFoundation.FCTabPage();
            this.framSketch = new fecherFoundation.FCPanel();
            this.imSketch = new fecherFoundation.FCPictureBox();
            this.Picture2 = new fecherFoundation.FCPictureBox();
            this.cmdNextSketch = new fecherFoundation.FCButton();
            this.cmdPrevSketch = new fecherFoundation.FCButton();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuAddNewAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddACard = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteCard = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteDwellingData = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteOutbuilding = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeparateCard = new fecherFoundation.FCToolStripMenuItem();
            this.mnuValuationReport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewDocs = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewGroupInformation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewMortgageInformation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuOptions = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRECLComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCollectionsNote = new fecherFoundation.FCToolStripMenuItem();
            this.mnuinterestedparties = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCopyProperty = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPendingTransfer = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintAccountInformationSheet = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintCard = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintCards = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPropertyCard = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintAllPropertyCards = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPropertyCardsPDF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint1pagepropertycard = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintLabel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintMailing = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintIncomeApproach = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintScreen = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintInterestedParties = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSketch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditSketch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNewSketch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEditConfig = new fecherFoundation.FCToolStripMenuItem();
            this.mnuReassignSketch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuImportSQFT = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAll = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDwellingSqft = new fecherFoundation.FCToolStripMenuItem();
            this.mnuOutbuildingSQFT = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteSketch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPictures = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPicture = new fecherFoundation.FCToolStripMenuItem();
            this.mnuImportPicture = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddPicture = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeletePicture = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPicMakePrimary = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPicMakeSecondary = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddOcc = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelOcc = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddExpense = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelExpense = new fecherFoundation.FCToolStripMenuItem();
            this.mnuIncomeSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearchName = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearchLocation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearchMapLot = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPreviousAccountFromSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNextAccountFromSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeparator4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessProperty = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSep = new fecherFoundation.FCToolStripMenuItem();
            this.Separator1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCalculate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSketchSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSketchSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPicSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar7 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeparator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveQuit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSavePending = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeparator3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
            this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
            this.lblInterestedPartiesPresent = new fecherFoundation.FCLabel();
            this.lblMRHLAcctNum = new fecherFoundation.FCLabel();
            this.lblComment = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.lblNumCards = new fecherFoundation.FCLabel();
            this.Label65_18 = new fecherFoundation.FCLabel();
            this.Label65_8 = new fecherFoundation.FCLabel();
            this.Label65_9 = new fecherFoundation.FCLabel();
            this.lblPendingFlag = new fecherFoundation.FCLabel();
            this.lblPicturePresent = new fecherFoundation.FCLabel();
            this.lblSketchPresent = new fecherFoundation.FCLabel();
            this.lblDate_2 = new fecherFoundation.FCLabel();
            this.Label8_6 = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdComment = new fecherFoundation.FCButton();
            this.cmdCalculate = new fecherFoundation.FCButton();
            this.cmdPreviousAccountFromSearch = new fecherFoundation.FCButton();
            this.cmdNextAccountFromSearch = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmbCardNumber = new fecherFoundation.FCComboBox();
            this.winSketchImageTimer = new Wisej.Web.Timer(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.ToolTip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridToolTip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDateInspected)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SaleGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRLivingTrust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLogSale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBankruptcy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTaxAcquired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framRegOwner)).BeginInit();
            this.framRegOwner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChangeOwnership)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveSecOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditSecOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchSecOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridExempts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BPGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTranCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).BeginInit();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkZoneOverride)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLandCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBldgCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPropertyCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridNeighborhood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridSecZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridTopography)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridUtilities)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridStreet)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDwelling1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDwelling2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDwelling3)).BeginInit();
            this.SSTab1_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            this.SSTab1_Page5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_0)).BeginInit();
            this.SSTab1_Page6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridExpenses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
            this.SSTab1_Page7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNextPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrevPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPicture)).BeginInit();
            this.framPicture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPicDimensions)).BeginInit();
            this.framPicDimensions.SuspendLayout();
            this.SSTab1_Page8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framSketch)).BeginInit();
            this.framSketch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imSketch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNextSketch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrevSketch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreviousAccountFromSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNextAccountFromSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 1050);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbCardNumber);
            this.ClientArea.Controls.Add(this.MaskEdbox3);
            this.ClientArea.Controls.Add(this.MaskEdBox1);
            this.ClientArea.Controls.Add(this.txtMRRSMAPLOT);
            this.ClientArea.Controls.Add(this.txtMRRSLocnumalph);
            this.ClientArea.Controls.Add(this.txtMRRSLocapt);
            this.ClientArea.Controls.Add(this.txtMRRSLocStreet);
            this.ClientArea.Controls.Add(this.T2KDateInspected);
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Controls.Add(this.lblInterestedPartiesPresent);
            this.ClientArea.Controls.Add(this.lblMRHLAcctNum);
            this.ClientArea.Controls.Add(this.lblComment);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.Label11);
            this.ClientArea.Controls.Add(this.lblNumCards);
            this.ClientArea.Controls.Add(this.Label65_18);
            this.ClientArea.Controls.Add(this.Label65_8);
            this.ClientArea.Controls.Add(this.Label65_9);
            this.ClientArea.Controls.Add(this.lblPendingFlag);
            this.ClientArea.Controls.Add(this.lblPicturePresent);
            this.ClientArea.Controls.Add(this.lblSketchPresent);
            this.ClientArea.Controls.Add(this.lblDate_2);
            this.ClientArea.Controls.Add(this.Label8_6);
            this.ClientArea.Controls.Add(this.ToolTip);
            this.ClientArea.Size = new System.Drawing.Size(1078, 606);
            this.ClientArea.Controls.SetChildIndex(this.ToolTip, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label8_6, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblDate_2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSketchPresent, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPicturePresent, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPendingFlag, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label65_9, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label65_8, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label65_18, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblNumCards, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label11, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label10, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblMRHLAcctNum, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblInterestedPartiesPresent, 0);
            this.ClientArea.Controls.SetChildIndex(this.SSTab1, 0);
            this.ClientArea.Controls.SetChildIndex(this.T2KDateInspected, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtMRRSLocStreet, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtMRRSLocapt, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtMRRSLocnumalph, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtMRRSMAPLOT, 0);
            this.ClientArea.Controls.SetChildIndex(this.MaskEdBox1, 0);
            this.ClientArea.Controls.SetChildIndex(this.MaskEdbox3, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbCardNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdCalculate);
            this.TopPanel.Controls.Add(this.cmdComment);
            this.TopPanel.Controls.Add(this.cmdPreviousAccountFromSearch);
            this.TopPanel.Controls.Add(this.cmdNextAccountFromSearch);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNextAccountFromSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPreviousAccountFromSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCalculate, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(235, 28);
            this.HeaderText.Text = "Account Maintenance";
            // 
            // MaskEdbox3
            // 
            this.MaskEdbox3.Cursor = Wisej.Web.Cursors.Default;
            this.MaskEdbox3.Location = new System.Drawing.Point(468, 68);
            this.MaskEdbox3.Name = "MaskEdbox3";
            this.MaskEdbox3.Size = new System.Drawing.Size(50, 16);
            this.MaskEdbox3.TabIndex = 18;
            this.MaskEdbox3.Tag = "land";
            this.MaskEdbox3.Enter += new System.EventHandler(this.MaskEdbox3_Enter);
            this.MaskEdbox3.KeyUp += new Wisej.Web.KeyEventHandler(this.MaskEdbox3_KeyUp);
            // 
            // MaskEdBox1
            // 
            this.MaskEdBox1.Cursor = Wisej.Web.Cursors.Default;
            this.MaskEdBox1.Location = new System.Drawing.Point(580, 68);
            this.MaskEdBox1.Name = "MaskEdBox1";
            this.MaskEdBox1.Size = new System.Drawing.Size(50, 16);
            this.MaskEdBox1.TabIndex = 17;
            this.MaskEdBox1.Tag = "land";
            this.MaskEdBox1.Enter += new System.EventHandler(this.MaskEdBox1_Enter);
            this.MaskEdBox1.KeyUp += new Wisej.Web.KeyEventHandler(this.MaskEdBox1_KeyUp);
            // 
            // txtMRRSMAPLOT
            // 
            this.txtMRRSMAPLOT.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRRSMAPLOT.Location = new System.Drawing.Point(110, 104);
            this.txtMRRSMAPLOT.MaxLength = 17;
            this.txtMRRSMAPLOT.Name = "txtMRRSMAPLOT";
            this.txtMRRSMAPLOT.Size = new System.Drawing.Size(170, 40);
            this.txtMRRSMAPLOT.TabIndex = 1;
            this.txtMRRSMAPLOT.Tag = "address";
            this.txtMRRSMAPLOT.Enter += new System.EventHandler(this.txtMRRSMAPLOT_Enter);
            this.txtMRRSMAPLOT.DoubleClick += new System.EventHandler(this.txtMRRSMAPLOT_DoubleClick);
            this.txtMRRSMAPLOT.TextChanged += new System.EventHandler(this.txtMRRSMAPLOT_TextChanged);
            this.txtMRRSMAPLOT.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSMAPLOT_KeyDown);
            // 
            // txtMRRSLocnumalph
            // 
            this.txtMRRSLocnumalph.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRRSLocnumalph.Location = new System.Drawing.Point(383, 104);
            this.txtMRRSLocnumalph.MaxLength = 6;
            this.txtMRRSLocnumalph.Name = "txtMRRSLocnumalph";
            this.txtMRRSLocnumalph.Size = new System.Drawing.Size(85, 40);
            this.txtMRRSLocnumalph.TabIndex = 2;
            this.txtMRRSLocnumalph.Tag = "address";
            this.txtMRRSLocnumalph.Enter += new System.EventHandler(this.txtMRRSLocnumalph_Enter);
            this.txtMRRSLocnumalph.DoubleClick += new System.EventHandler(this.txtMRRSLocnumalph_DoubleClick);
            this.txtMRRSLocnumalph.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSLocnumalph_KeyUp);
            // 
            // txtMRRSLocapt
            // 
            this.txtMRRSLocapt.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRRSLocapt.Location = new System.Drawing.Point(483, 104);
            this.txtMRRSLocapt.MaxLength = 1;
            this.txtMRRSLocapt.Name = "txtMRRSLocapt";
            this.txtMRRSLocapt.Size = new System.Drawing.Size(35, 40);
            this.txtMRRSLocapt.TabIndex = 3;
            this.txtMRRSLocapt.Tag = "address";
            this.txtMRRSLocapt.Enter += new System.EventHandler(this.txtMRRSLocapt_Enter);
            this.txtMRRSLocapt.DoubleClick += new System.EventHandler(this.txtMRRSLocapt_DoubleClick);
            // 
            // txtMRRSLocStreet
            // 
            this.txtMRRSLocStreet.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRRSLocStreet.Location = new System.Drawing.Point(533, 104);
            this.txtMRRSLocStreet.MaxLength = 26;
            this.txtMRRSLocStreet.Name = "txtMRRSLocStreet";
            this.txtMRRSLocStreet.Size = new System.Drawing.Size(296, 40);
            this.txtMRRSLocStreet.TabIndex = 4;
            this.txtMRRSLocStreet.Tag = "address";
            this.txtMRRSLocStreet.Enter += new System.EventHandler(this.txtMRRSLocStreet_Enter);
            this.txtMRRSLocStreet.DoubleClick += new System.EventHandler(this.txtMRRSLocStreet_DoubleClick);
            this.txtMRRSLocStreet.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMRRSLocStreet_KeyUp);
            // 
            // ToolTip
            // 
            this.ToolTip.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToolTip.BackgroundImage")));
            this.ToolTip.Controls.Add(this.GridToolTip);
            this.ToolTip.CurrentTip = null;
            this.ToolTip.ForeColor = System.Drawing.Color.FromName("@windowText");
            this.ToolTip.Name = "ToolTip";
            this.ToolTip.Size = new System.Drawing.Size(600, 96);
            this.ToolTip.TabIndex = 15;
            this.ToolTip.UserControlForeColor = System.Drawing.Color.FromName("@windowText");
            this.ToolTip.UserControlHeight = 1440;
            this.ToolTip.UserControlWidth = 9000;
            this.ToolTip.Visible = false;
            // 
            // GridToolTip
            // 
            this.GridToolTip.Cols = 10;
            this.GridToolTip.Location = new System.Drawing.Point(13, 20);
            this.GridToolTip.Name = "GridToolTip";
            this.GridToolTip.Rows = 50;
            this.GridToolTip.ShowFocusCell = false;
            this.GridToolTip.Size = new System.Drawing.Size(289, 32);
            this.GridToolTip.TabIndex = 16;
            this.GridToolTip.Visible = false;
            this.GridToolTip.DoubleClick += new System.EventHandler(this.GridToolTip_DblClick);
            // 
            // T2KDateInspected
            // 
            this.T2KDateInspected.Location = new System.Drawing.Point(657, 54);
            this.T2KDateInspected.Mask = "##/##/####";
            this.T2KDateInspected.MaxLength = 10;
            this.T2KDateInspected.Name = "T2KDateInspected";
            this.T2KDateInspected.Size = new System.Drawing.Size(130, 22);
            this.T2KDateInspected.TabIndex = 20;
            this.T2KDateInspected.Tag = "land";
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Controls.Add(this.SSTab1_Page4);
            this.SSTab1.Controls.Add(this.SSTab1_Page5);
            this.SSTab1.Controls.Add(this.SSTab1_Page6);
            this.SSTab1.Controls.Add(this.SSTab1_Page7);
            this.SSTab1.Controls.Add(this.SSTab1_Page8);
            this.SSTab1.Location = new System.Drawing.Point(30, 164);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
            this.SSTab1.Size = new System.Drawing.Size(950, 886);
            this.SSTab1.TabIndex = 49;
            this.SSTab1.Text = "Account";
            this.SSTab1.SelectedIndexChanged += new System.EventHandler(this.SSTab1_SelectedIndexChanged);
            this.SSTab1.Enter += new System.EventHandler(this.SSTab1_Enter);
            this.SSTab1.KeyDown += new Wisej.Web.KeyEventHandler(this.SSTab1_KeyDown);
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.Label1);
            this.SSTab1_Page1.Controls.Add(this.Frame3);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Size = new System.Drawing.Size(948, 850);
            this.SSTab1_Page1.Text = "Account";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(279, 389);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(8, 13);
            this.Label1.TabIndex = 308;
            this.Label1.Visible = false;
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.SaleGrid);
            this.Frame3.Controls.Add(this.txtMRRSRef2);
            this.Frame3.Controls.Add(this.txtMRRSRef1);
            this.Frame3.Controls.Add(this.txtMRRSPrevMaster);
            this.Frame3.Controls.Add(this.chkRLivingTrust);
            this.Frame3.Controls.Add(this.cmdLogSale);
            this.Frame3.Controls.Add(this.chkBankruptcy);
            this.Frame3.Controls.Add(this.chkTaxAcquired);
            this.Frame3.Controls.Add(this.framRegOwner);
            this.Frame3.Controls.Add(this.GridExempts);
            this.Frame3.Controls.Add(this.BPGrid);
            this.Frame3.Controls.Add(this.gridTranCode);
            this.Frame3.Controls.Add(this.Label7);
            this.Frame3.Controls.Add(this.Label8_8);
            this.Frame3.Controls.Add(this.Label8_7);
            this.Frame3.Controls.Add(this.Label9);
            this.Frame3.Controls.Add(this.lblDate_5);
            this.Frame3.Controls.Add(this.lblDate_4);
            this.Frame3.Controls.Add(this.lbl2_3);
            this.Frame3.Controls.Add(this.lbl1_2);
            this.Frame3.Controls.Add(this.lblDate_7);
            this.Frame3.Controls.Add(this.lblDate_0);
            this.Frame3.Controls.Add(this.imgDocuments);
            this.Frame3.Controls.Add(this.lblMRRLEXEMPTION_30);
            this.Frame3.Controls.Add(this.lblMRRLBLDGVAL_29);
            this.Frame3.Controls.Add(this.lblMRRLLANDVAL_28);
            this.Frame3.Controls.Add(this.lblTaxable);
            this.Frame3.Dock = Wisej.Web.DockStyle.Fill;
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(948, 850);
            this.Frame3.TabIndex = 243;
            // 
            // SaleGrid
            // 
            this.SaleGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.SaleGrid.Cols = 6;
            this.SaleGrid.ColumnHeadersHeight = 60;
            this.SaleGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.SaleGrid.ExtendLastCol = true;
            this.SaleGrid.FixedCols = 0;
            this.SaleGrid.FixedRows = 2;
            this.SaleGrid.Location = new System.Drawing.Point(20, 577);
            this.SaleGrid.Name = "SaleGrid";
            this.SaleGrid.ReadOnly = false;
            this.SaleGrid.RowHeadersVisible = false;
            this.SaleGrid.Rows = 3;
            this.SaleGrid.ShowFocusCell = false;
            this.SaleGrid.Size = new System.Drawing.Size(908, 183);
            this.SaleGrid.StandardTab = false;
            this.SaleGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.SaleGrid.TabIndex = 16;
            this.SaleGrid.Tag = "sale";
            this.SaleGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.SaleGrid_AfterEdit);
            this.SaleGrid.CurrentCellChanged += new System.EventHandler(this.SaleGrid_RowColChange);
            // 
            // txtMRRSRef2
            // 
            this.txtMRRSRef2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRRSRef2.Location = new System.Drawing.Point(123, 475);
            this.txtMRRSRef2.MaxLength = 44;
            this.txtMRRSRef2.Name = "txtMRRSRef2";
            this.txtMRRSRef2.Size = new System.Drawing.Size(382, 40);
            this.txtMRRSRef2.TabIndex = 7;
            this.txtMRRSRef2.Tag = "address";
            this.txtMRRSRef2.Enter += new System.EventHandler(this.txtMRRSRef2_Enter);
            this.txtMRRSRef2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSRef2_KeyDown);
            // 
            // txtMRRSRef1
            // 
            this.txtMRRSRef1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRRSRef1.Location = new System.Drawing.Point(123, 425);
            this.txtMRRSRef1.MaxLength = 44;
            this.txtMRRSRef1.Name = "txtMRRSRef1";
            this.txtMRRSRef1.Size = new System.Drawing.Size(382, 40);
            this.txtMRRSRef1.TabIndex = 6;
            this.txtMRRSRef1.Tag = "address";
            this.txtMRRSRef1.Enter += new System.EventHandler(this.txtMRRSRef1_Enter);
            this.txtMRRSRef1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRRSRef1_KeyDown);
            // 
            // txtMRRSPrevMaster
            // 
            this.txtMRRSPrevMaster.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRRSPrevMaster.Location = new System.Drawing.Point(123, 375);
            this.txtMRRSPrevMaster.MaxLength = 50;
            this.txtMRRSPrevMaster.Name = "txtMRRSPrevMaster";
            this.txtMRRSPrevMaster.Size = new System.Drawing.Size(382, 40);
            this.txtMRRSPrevMaster.TabIndex = 5;
            this.txtMRRSPrevMaster.Tag = "address";
            this.txtMRRSPrevMaster.Enter += new System.EventHandler(this.txtMRRSPrevMaster_Enter);
            // 
            // chkRLivingTrust
            // 
            this.chkRLivingTrust.Location = new System.Drawing.Point(521, 326);
            this.chkRLivingTrust.Name = "chkRLivingTrust";
            this.chkRLivingTrust.Size = new System.Drawing.Size(164, 22);
            this.chkRLivingTrust.TabIndex = 12;
            this.chkRLivingTrust.Tag = "land";
            this.chkRLivingTrust.Text = "Revocable Living Trust";
            this.chkRLivingTrust.KeyDown += new Wisej.Web.KeyEventHandler(this.chkRLivingTrust_KeyDown);
            // 
            // cmdLogSale
            // 
            this.cmdLogSale.AppearanceKey = "actionButton";
            this.cmdLogSale.Cursor = Wisej.Web.Cursors.Default;
            this.cmdLogSale.Location = new System.Drawing.Point(20, 786);
            this.cmdLogSale.Name = "cmdLogSale";
            this.cmdLogSale.Size = new System.Drawing.Size(180, 40);
            this.cmdLogSale.TabIndex = 257;
            this.cmdLogSale.Tag = "sale";
            this.cmdLogSale.Text = "Save as Sale Record";
            this.ToolTip1.SetToolTip(this.cmdLogSale, "Used when validity code isn\'t 1 to force a save in the sale record.");
            this.cmdLogSale.Click += new System.EventHandler(this.cmdLogSale_Click);
            // 
            // chkBankruptcy
            // 
            this.chkBankruptcy.Location = new System.Drawing.Point(521, 385);
            this.chkBankruptcy.Name = "chkBankruptcy";
            this.chkBankruptcy.Size = new System.Drawing.Size(97, 22);
            this.chkBankruptcy.TabIndex = 14;
            this.chkBankruptcy.Text = "Bankruptcy";
            this.chkBankruptcy.CheckedChanged += new System.EventHandler(this.chkBankruptcy_CheckedChanged);
            // 
            // chkTaxAcquired
            // 
            this.chkTaxAcquired.Location = new System.Drawing.Point(521, 356);
            this.chkTaxAcquired.Name = "chkTaxAcquired";
            this.chkTaxAcquired.Size = new System.Drawing.Size(109, 22);
            this.chkTaxAcquired.TabIndex = 13;
            this.chkTaxAcquired.Text = "Tax Acquired";
            this.chkTaxAcquired.CheckedChanged += new System.EventHandler(this.chkTaxAcquired_CheckedChanged);
            // 
            // framRegOwner
            // 
            this.framRegOwner.AppearanceKey = "groupBoxNoBorders";
            this.framRegOwner.Controls.Add(this.txtDeedName2);
            this.framRegOwner.Controls.Add(this.txtDeedName1);
            this.framRegOwner.Controls.Add(this.fcLabel1);
            this.framRegOwner.Controls.Add(this.fcLabel2);
            this.framRegOwner.Controls.Add(this.cmdChangeOwnership);
            this.framRegOwner.Controls.Add(this.cmdRemoveOwner);
            this.framRegOwner.Controls.Add(this.cmdRemoveSecOwner);
            this.framRegOwner.Controls.Add(this.txt2ndOwnerID);
            this.framRegOwner.Controls.Add(this.cmdEditSecOwner);
            this.framRegOwner.Controls.Add(this.cmdSearchSecOwner);
            this.framRegOwner.Controls.Add(this.txtOwnerID);
            this.framRegOwner.Controls.Add(this.cmdEditOwner);
            this.framRegOwner.Controls.Add(this.cmdSearchOwner);
            this.framRegOwner.Controls.Add(this.lblAddress);
            this.framRegOwner.Controls.Add(this.lblOwner1);
            this.framRegOwner.Controls.Add(this.Label14);
            this.framRegOwner.Controls.Add(this.Label43);
            this.framRegOwner.Controls.Add(this.Label2_9);
            this.framRegOwner.Controls.Add(this.lbl2ndOwner);
            this.framRegOwner.Location = new System.Drawing.Point(10, 20);
            this.framRegOwner.Name = "framRegOwner";
            this.framRegOwner.Size = new System.Drawing.Size(505, 341);
            this.framRegOwner.TabIndex = 244;
            // 
            // txtDeedName2
            // 
            this.txtDeedName2.Cursor = Wisej.Web.Cursors.Default;
            this.txtDeedName2.Location = new System.Drawing.Point(113, 213);
            this.txtDeedName2.MaxLength = 255;
            this.txtDeedName2.Name = "txtDeedName2";
            this.txtDeedName2.Size = new System.Drawing.Size(382, 40);
            this.txtDeedName2.TabIndex = 2;
            this.txtDeedName2.Tag = "address";
            // 
            // txtDeedName1
            // 
            this.txtDeedName1.Cursor = Wisej.Web.Cursors.Default;
            this.txtDeedName1.Location = new System.Drawing.Point(113, 163);
            this.txtDeedName1.MaxLength = 255;
            this.txtDeedName1.Name = "txtDeedName1";
            this.txtDeedName1.Size = new System.Drawing.Size(382, 40);
            this.txtDeedName1.TabIndex = 1;
            this.txtDeedName1.Tag = "address";
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(10, 226);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(83, 15);
            this.fcLabel1.TabIndex = 316;
            this.fcLabel1.Text = "DEED NAME 2";
            // 
            // fcLabel2
            // 
            this.fcLabel2.Location = new System.Drawing.Point(10, 176);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(83, 15);
            this.fcLabel2.TabIndex = 315;
            this.fcLabel2.Text = "DEED NAME 1";
            // 
            // cmdChangeOwnership
            // 
            this.cmdChangeOwnership.Cursor = Wisej.Web.Cursors.Default;
            this.cmdChangeOwnership.Image = ((System.Drawing.Image)(resources.GetObject("cmdChangeOwnership.Image")));
            this.cmdChangeOwnership.Location = new System.Drawing.Point(353, 10);
            this.cmdChangeOwnership.Name = "cmdChangeOwnership";
            this.cmdChangeOwnership.Size = new System.Drawing.Size(40, 40);
            this.cmdChangeOwnership.TabIndex = 312;
            this.ToolTip1.SetToolTip(this.cmdChangeOwnership, "Change of Ownership");
            this.cmdChangeOwnership.Visible = false;
            this.cmdChangeOwnership.Click += new System.EventHandler(this.cmdChangeOwnership_Click);
            // 
            // cmdRemoveOwner
            // 
            this.cmdRemoveOwner.AppearanceKey = "actionButton";
            this.cmdRemoveOwner.Cursor = Wisej.Web.Cursors.Default;
            this.cmdRemoveOwner.ImageSource = "icon-close-menu";
            this.cmdRemoveOwner.Location = new System.Drawing.Point(307, 10);
            this.cmdRemoveOwner.Name = "cmdRemoveOwner";
            this.cmdRemoveOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdRemoveOwner.TabIndex = 311;
            this.cmdRemoveOwner.Click += new System.EventHandler(this.cmdRemoveOwner_Click);
            // 
            // cmdRemoveSecOwner
            // 
            this.cmdRemoveSecOwner.AppearanceKey = "actionButton";
            this.cmdRemoveSecOwner.Cursor = Wisej.Web.Cursors.Default;
            this.cmdRemoveSecOwner.ImageSource = "icon-close-menu";
            this.cmdRemoveSecOwner.Location = new System.Drawing.Point(307, 86);
            this.cmdRemoveSecOwner.Name = "cmdRemoveSecOwner";
            this.cmdRemoveSecOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdRemoveSecOwner.TabIndex = 310;
            this.cmdRemoveSecOwner.Click += new System.EventHandler(this.cmdRemoveSecOwner_Click);
            // 
            // txt2ndOwnerID
            // 
            this.txt2ndOwnerID.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
            this.txt2ndOwnerID.Cursor = Wisej.Web.Cursors.Default;
            this.txt2ndOwnerID.Location = new System.Drawing.Point(110, 86);
            this.txt2ndOwnerID.Name = "txt2ndOwnerID";
            this.txt2ndOwnerID.Size = new System.Drawing.Size(94, 40);
            this.txt2ndOwnerID.TabIndex = 250;
            this.txt2ndOwnerID.Tag = "address";
            this.txt2ndOwnerID.Text = "0";
            this.txt2ndOwnerID.Validating += new System.ComponentModel.CancelEventHandler(this.txt2ndOwnerID_Validating);
            // 
            // cmdEditSecOwner
            // 
            this.cmdEditSecOwner.AppearanceKey = "actionButton";
            this.cmdEditSecOwner.Cursor = Wisej.Web.Cursors.Default;
            this.cmdEditSecOwner.ImageSource = "icon - edit";
            this.cmdEditSecOwner.Location = new System.Drawing.Point(262, 86);
            this.cmdEditSecOwner.Name = "cmdEditSecOwner";
            this.cmdEditSecOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdEditSecOwner.TabIndex = 249;
            this.cmdEditSecOwner.Click += new System.EventHandler(this.cmdEditSecOwner_Click);
            // 
            // cmdSearchSecOwner
            // 
            this.cmdSearchSecOwner.AppearanceKey = "actionButton";
            this.cmdSearchSecOwner.Cursor = Wisej.Web.Cursors.Default;
            this.cmdSearchSecOwner.ImageSource = "icon - search";
            this.cmdSearchSecOwner.Location = new System.Drawing.Point(217, 86);
            this.cmdSearchSecOwner.Name = "cmdSearchSecOwner";
            this.cmdSearchSecOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdSearchSecOwner.TabIndex = 248;
            this.cmdSearchSecOwner.Click += new System.EventHandler(this.cmdSearchSecOwner_Click);
            // 
            // txtOwnerID
            // 
            this.txtOwnerID.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
            this.txtOwnerID.Cursor = Wisej.Web.Cursors.Default;
            this.txtOwnerID.Location = new System.Drawing.Point(110, 10);
            this.txtOwnerID.Name = "txtOwnerID";
            this.txtOwnerID.Size = new System.Drawing.Size(94, 40);
            this.txtOwnerID.TabIndex = 247;
            this.txtOwnerID.Tag = "address";
            this.txtOwnerID.Text = "0";
            this.txtOwnerID.Validating += new System.ComponentModel.CancelEventHandler(this.txtOwnerID_Validating);
            // 
            // cmdEditOwner
            // 
            this.cmdEditOwner.AppearanceKey = "actionButton";
            this.cmdEditOwner.Cursor = Wisej.Web.Cursors.Default;
            this.cmdEditOwner.ImageSource = "icon - edit";
            this.cmdEditOwner.Location = new System.Drawing.Point(262, 10);
            this.cmdEditOwner.Name = "cmdEditOwner";
            this.cmdEditOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdEditOwner.TabIndex = 246;
            this.cmdEditOwner.Click += new System.EventHandler(this.cmdEditOwner_Click);
            // 
            // cmdSearchOwner
            // 
            this.cmdSearchOwner.AppearanceKey = "actionButton";
            this.cmdSearchOwner.Cursor = Wisej.Web.Cursors.Default;
            this.cmdSearchOwner.ImageSource = "icon - search";
            this.cmdSearchOwner.Location = new System.Drawing.Point(217, 10);
            this.cmdSearchOwner.Name = "cmdSearchOwner";
            this.cmdSearchOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdSearchOwner.TabIndex = 245;
            this.cmdSearchOwner.Click += new System.EventHandler(this.cmdSearchOwner_Click);
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(110, 266);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(356, 64);
            this.lblAddress.TabIndex = 256;
            // 
            // lblOwner1
            // 
            this.lblOwner1.Location = new System.Drawing.Point(110, 60);
            this.lblOwner1.Name = "lblOwner1";
            this.lblOwner1.Size = new System.Drawing.Size(356, 16);
            this.lblOwner1.TabIndex = 255;
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(10, 24);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(50, 16);
            this.Label14.TabIndex = 254;
            this.Label14.Text = "OWNER";
            // 
            // Label43
            // 
            this.Label43.Location = new System.Drawing.Point(10, 100);
            this.Label43.Name = "Label43";
            this.Label43.Size = new System.Drawing.Size(80, 16);
            this.Label43.TabIndex = 253;
            this.Label43.Text = "2ND OWNER";
            // 
            // Label2_9
            // 
            this.Label2_9.Location = new System.Drawing.Point(10, 271);
            this.Label2_9.Name = "Label2_9";
            this.Label2_9.Size = new System.Drawing.Size(60, 16);
            this.Label2_9.TabIndex = 252;
            this.Label2_9.Text = "ADDRESS";
            // 
            // lbl2ndOwner
            // 
            this.lbl2ndOwner.Location = new System.Drawing.Point(110, 132);
            this.lbl2ndOwner.Name = "lbl2ndOwner";
            this.lbl2ndOwner.Size = new System.Drawing.Size(356, 16);
            this.lbl2ndOwner.TabIndex = 251;
            // 
            // GridExempts
            // 
            this.GridExempts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridExempts.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridExempts.ExtendLastCol = true;
            this.GridExempts.FixedCols = 0;
            this.GridExempts.Location = new System.Drawing.Point(521, 165);
            this.GridExempts.Name = "GridExempts";
            this.GridExempts.ReadOnly = false;
            this.GridExempts.RowHeadersVisible = false;
            this.GridExempts.Rows = 4;
            this.GridExempts.ShowFocusCell = false;
            this.GridExempts.Size = new System.Drawing.Size(407, 152);
            this.GridExempts.TabIndex = 11;
            this.GridExempts.Tag = "land";
            this.GridExempts.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridExempts_AfterEdit);
            this.GridExempts.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridExempts_MouseMoveEvent);
            // 
            // BPGrid
            // 
            this.BPGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.BPGrid.Cols = 4;
            this.BPGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.BPGrid.ExtendLastCol = true;
            this.BPGrid.FixedCols = 0;
            this.BPGrid.Location = new System.Drawing.Point(521, 419);
            this.BPGrid.Name = "BPGrid";
            this.BPGrid.ReadOnly = false;
            this.BPGrid.RowHeadersVisible = false;
            this.BPGrid.ShowFocusCell = false;
            this.BPGrid.Size = new System.Drawing.Size(407, 152);
            this.BPGrid.TabIndex = 15;
            this.BPGrid.Tag = "land";
            this.BPGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.BPGrid_AfterEdit);
            this.BPGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.BPGrid_ValidateEdit);
            this.BPGrid.CurrentCellChanged += new System.EventHandler(this.BPGrid_RowColChange);
            this.BPGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.BPGrid_KeyDownEvent);
            // 
            // gridTranCode
            // 
            this.gridTranCode.Cols = 1;
            this.gridTranCode.ColumnHeadersVisible = false;
            this.gridTranCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridTranCode.ExtendLastCol = true;
            this.gridTranCode.FixedCols = 0;
            this.gridTranCode.FixedRows = 0;
            this.gridTranCode.Location = new System.Drawing.Point(123, 525);
            this.gridTranCode.Name = "gridTranCode";
            this.gridTranCode.ReadOnly = false;
            this.gridTranCode.RowHeadersVisible = false;
            this.gridTranCode.Rows = 1;
            this.gridTranCode.ShowFocusCell = false;
            this.gridTranCode.Size = new System.Drawing.Size(382, 42);
            this.gridTranCode.TabIndex = 8;
            this.gridTranCode.Tag = "land";
            this.gridTranCode.ComboCloseUp += new System.EventHandler(this.gridTranCode_ComboCloseUp);
            this.gridTranCode.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.gridTranCode_BeforeEdit);
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(232, 786);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(63, 15);
            this.Label7.TabIndex = 309;
            this.Label7.Text = "PENDING";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Label7.Visible = false;
            // 
            // Label8_8
            // 
            this.Label8_8.Location = new System.Drawing.Point(20, 489);
            this.Label8_8.Name = "Label8_8";
            this.Label8_8.Size = new System.Drawing.Size(83, 15);
            this.Label8_8.TabIndex = 270;
            this.Label8_8.Text = "REFERENCE 2";
            // 
            // Label8_7
            // 
            this.Label8_7.Location = new System.Drawing.Point(20, 439);
            this.Label8_7.Name = "Label8_7";
            this.Label8_7.Size = new System.Drawing.Size(83, 15);
            this.Label8_7.TabIndex = 269;
            this.Label8_7.Text = "REFERENCE 1";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 389);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(83, 15);
            this.Label9.TabIndex = 268;
            this.Label9.Text = "PREV OWNER";
            // 
            // lblDate_5
            // 
            this.lblDate_5.Location = new System.Drawing.Point(612, 105);
            this.lblDate_5.Name = "lblDate_5";
            this.lblDate_5.Size = new System.Drawing.Size(60, 15);
            this.lblDate_5.TabIndex = 265;
            this.lblDate_5.Text = "EXEMPT";
            // 
            // lblDate_4
            // 
            this.lblDate_4.Location = new System.Drawing.Point(612, 80);
            this.lblDate_4.Name = "lblDate_4";
            this.lblDate_4.Size = new System.Drawing.Size(60, 15);
            this.lblDate_4.TabIndex = 264;
            this.lblDate_4.Text = "BUILDING";
            // 
            // lbl2_3
            // 
            this.lbl2_3.Location = new System.Drawing.Point(612, 55);
            this.lbl2_3.Name = "lbl2_3";
            this.lbl2_3.Size = new System.Drawing.Size(60, 15);
            this.lbl2_3.TabIndex = 263;
            this.lbl2_3.Text = "LAND";
            // 
            // lbl1_2
            // 
            this.lbl1_2.Location = new System.Drawing.Point(672, 30);
            this.lbl1_2.Name = "lbl1_2";
            this.lbl1_2.Size = new System.Drawing.Size(80, 15);
            this.lbl1_2.TabIndex = 262;
            this.lbl1_2.Text = "ASSESSMENT";
            this.lbl1_2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblDate_7
            // 
            this.lblDate_7.Location = new System.Drawing.Point(20, 539);
            this.lblDate_7.Name = "lblDate_7";
            this.lblDate_7.Size = new System.Drawing.Size(83, 15);
            this.lblDate_7.TabIndex = 260;
            this.lblDate_7.Text = "TRAN CODE";
            // 
            // lblDate_0
            // 
            this.lblDate_0.Location = new System.Drawing.Point(612, 130);
            this.lblDate_0.Name = "lblDate_0";
            this.lblDate_0.Size = new System.Drawing.Size(60, 15);
            this.lblDate_0.TabIndex = 259;
            this.lblDate_0.Text = "TAXABLE";
            // 
            // imgDocuments
            // 
            this.imgDocuments.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgDocuments.Cursor = Wisej.Web.Cursors.Default;
            this.imgDocuments.FillColor = 16777215;
            this.imgDocuments.Image = ((System.Drawing.Image)(resources.GetObject("imgDocuments.Image")));
            this.imgDocuments.Location = new System.Drawing.Point(526, 30);
            this.imgDocuments.Name = "imgDocuments";
            this.imgDocuments.Size = new System.Drawing.Size(29, 31);
            this.imgDocuments.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgDocuments, "One or more documents are attached to this record");
            this.imgDocuments.Visible = false;
            // 
            // lblMRRLEXEMPTION_30
            // 
            this.lblMRRLEXEMPTION_30.BackColor = System.Drawing.Color.Transparent;
            this.lblMRRLEXEMPTION_30.Location = new System.Drawing.Point(728, 105);
            this.lblMRRLEXEMPTION_30.Name = "lblMRRLEXEMPTION_30";
            this.lblMRRLEXEMPTION_30.Size = new System.Drawing.Size(86, 15);
            this.lblMRRLEXEMPTION_30.TabIndex = 267;
            this.lblMRRLEXEMPTION_30.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblMRRLBLDGVAL_29
            // 
            this.lblMRRLBLDGVAL_29.BackColor = System.Drawing.Color.Transparent;
            this.lblMRRLBLDGVAL_29.Location = new System.Drawing.Point(728, 80);
            this.lblMRRLBLDGVAL_29.Name = "lblMRRLBLDGVAL_29";
            this.lblMRRLBLDGVAL_29.Size = new System.Drawing.Size(86, 15);
            this.lblMRRLBLDGVAL_29.TabIndex = 266;
            this.lblMRRLBLDGVAL_29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblMRRLLANDVAL_28
            // 
            this.lblMRRLLANDVAL_28.BackColor = System.Drawing.Color.Transparent;
            this.lblMRRLLANDVAL_28.Location = new System.Drawing.Point(728, 55);
            this.lblMRRLLANDVAL_28.Name = "lblMRRLLANDVAL_28";
            this.lblMRRLLANDVAL_28.Size = new System.Drawing.Size(86, 15);
            this.lblMRRLLANDVAL_28.TabIndex = 261;
            this.lblMRRLLANDVAL_28.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTaxable
            // 
            this.lblTaxable.BackColor = System.Drawing.Color.Transparent;
            this.lblTaxable.Location = new System.Drawing.Point(728, 130);
            this.lblTaxable.Name = "lblTaxable";
            this.lblTaxable.Size = new System.Drawing.Size(86, 15);
            this.lblTaxable.TabIndex = 258;
            this.lblTaxable.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.lblMapLot);
            this.SSTab1_Page2.Controls.Add(this.lblLocation_0);
            this.SSTab1_Page2.Controls.Add(this.lblMapLotCopy_0);
            this.SSTab1_Page2.Controls.Add(this.Frame5);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Size = new System.Drawing.Size(948, 850);
            this.SSTab1_Page2.Text = "Property";
            // 
            // lblMapLot
            // 
            this.lblMapLot.Location = new System.Drawing.Point(434, 48);
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Size = new System.Drawing.Size(104, 14);
            this.lblMapLot.TabIndex = 304;
            this.lblMapLot.Visible = false;
            // 
            // lblLocation_0
            // 
            this.lblLocation_0.Location = new System.Drawing.Point(222, 48);
            this.lblLocation_0.Name = "lblLocation_0";
            this.lblLocation_0.Size = new System.Drawing.Size(194, 14);
            this.lblLocation_0.TabIndex = 303;
            this.lblLocation_0.Visible = false;
            // 
            // lblMapLotCopy_0
            // 
            this.lblMapLotCopy_0.Location = new System.Drawing.Point(20, 715);
            this.lblMapLotCopy_0.Name = "lblMapLotCopy_0";
            this.lblMapLotCopy_0.Size = new System.Drawing.Size(203, 16);
            this.lblMapLotCopy_0.TabIndex = 301;
            this.lblMapLotCopy_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame5
            // 
            this.Frame5.AutoSize = true;
            this.Frame5.Controls.Add(this.txtMRPIAcres);
            this.Frame5.Controls.Add(this.txtMRPIStreetCode);
            this.Frame5.Controls.Add(this.txtMRPIOpen1);
            this.Frame5.Controls.Add(this.txtMRPIXCoord);
            this.Frame5.Controls.Add(this.txtMRPIYCoord);
            this.Frame5.Controls.Add(this.txtMRPIOpen2);
            this.Frame5.Controls.Add(this.chkZoneOverride);
            this.Frame5.Controls.Add(this.GridLand);
            this.Frame5.Controls.Add(this.gridLandCode);
            this.Frame5.Controls.Add(this.gridBldgCode);
            this.Frame5.Controls.Add(this.gridPropertyCode);
            this.Frame5.Controls.Add(this.GridNeighborhood);
            this.Frame5.Controls.Add(this.GridZone);
            this.Frame5.Controls.Add(this.GridSecZone);
            this.Frame5.Controls.Add(this.GridTopography);
            this.Frame5.Controls.Add(this.GridUtilities);
            this.Frame5.Controls.Add(this.GridStreet);
            this.Frame5.Controls.Add(this.Label13);
            this.Frame5.Controls.Add(this.Label12);
            this.Frame5.Controls.Add(this.Label22);
            this.Frame5.Controls.Add(this.Label8_36);
            this.Frame5.Controls.Add(this.Label8_10);
            this.Frame5.Controls.Add(this.Label8_11);
            this.Frame5.Controls.Add(this.Label8_12);
            this.Frame5.Controls.Add(this.Label8_13);
            this.Frame5.Controls.Add(this.Label8_14);
            this.Frame5.Controls.Add(this.Label8_15);
            this.Frame5.Controls.Add(this.Label8_16);
            this.Frame5.Controls.Add(this.Label8_17);
            this.Frame5.Controls.Add(this.Label8_18);
            this.Frame5.Controls.Add(this.Label8_19);
            this.Frame5.Controls.Add(this.Label8_20);
            this.Frame5.Dock = Wisej.Web.DockStyle.Fill;
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(948, 850);
            this.Frame5.TabIndex = 277;
            // 
            // txtMRPIAcres
            // 
            this.txtMRPIAcres.BackColor = System.Drawing.SystemColors.Window;
            this.txtMRPIAcres.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRPIAcres.Location = new System.Drawing.Point(693, 793);
            this.txtMRPIAcres.LockedOriginal = true;
            this.txtMRPIAcres.Name = "txtMRPIAcres";
            this.txtMRPIAcres.ReadOnly = true;
            this.txtMRPIAcres.Size = new System.Drawing.Size(68, 40);
            this.txtMRPIAcres.TabIndex = 278;
            this.txtMRPIAcres.TabStop = false;
            this.txtMRPIAcres.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtMRPIStreetCode
            // 
            this.txtMRPIStreetCode.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRPIStreetCode.Location = new System.Drawing.Point(615, 75);
            this.txtMRPIStreetCode.MaxLength = 4;
            this.txtMRPIStreetCode.Name = "txtMRPIStreetCode";
            this.txtMRPIStreetCode.Size = new System.Drawing.Size(313, 40);
            this.txtMRPIStreetCode.TabIndex = 25;
            this.txtMRPIStreetCode.Tag = "land";
            this.txtMRPIStreetCode.Enter += new System.EventHandler(this.txtMRPIStreetCode_Enter);
            this.txtMRPIStreetCode.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRPIStreetCode_KeyDown);
            // 
            // txtMRPIOpen1
            // 
            this.txtMRPIOpen1.Appearance = 0;
            this.txtMRPIOpen1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtMRPIOpen1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRPIOpen1.Location = new System.Drawing.Point(615, 615);
            this.txtMRPIOpen1.MaxLength = 4;
            this.txtMRPIOpen1.Name = "txtMRPIOpen1";
            this.txtMRPIOpen1.Size = new System.Drawing.Size(77, 40);
            this.txtMRPIOpen1.TabIndex = 33;
            this.txtMRPIOpen1.Tag = "land";
            this.txtMRPIOpen1.Enter += new System.EventHandler(this.txtMRPIOpen1_Enter);
            this.txtMRPIOpen1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRPIOpen1_Validating);
            this.txtMRPIOpen1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRPIOpen1_KeyDown);
            // 
            // txtMRPIXCoord
            // 
            this.txtMRPIXCoord.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRPIXCoord.Location = new System.Drawing.Point(615, 125);
            this.txtMRPIXCoord.MaxLength = 4;
            this.txtMRPIXCoord.Name = "txtMRPIXCoord";
            this.txtMRPIXCoord.Size = new System.Drawing.Size(313, 40);
            this.txtMRPIXCoord.TabIndex = 26;
            this.txtMRPIXCoord.Tag = "land";
            this.txtMRPIXCoord.Enter += new System.EventHandler(this.txtMRPIXCoord_Enter);
            this.txtMRPIXCoord.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRPIXCoord_KeyDown);
            // 
            // txtMRPIYCoord
            // 
            this.txtMRPIYCoord.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRPIYCoord.Location = new System.Drawing.Point(615, 175);
            this.txtMRPIYCoord.MaxLength = 4;
            this.txtMRPIYCoord.Name = "txtMRPIYCoord";
            this.txtMRPIYCoord.Size = new System.Drawing.Size(313, 40);
            this.txtMRPIYCoord.TabIndex = 27;
            this.txtMRPIYCoord.Tag = "land";
            this.txtMRPIYCoord.Enter += new System.EventHandler(this.txtMRPIYCoord_Enter);
            this.txtMRPIYCoord.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRPIYCoord_KeyDown);
            // 
            // txtMRPIOpen2
            // 
            this.txtMRPIOpen2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMRPIOpen2.Location = new System.Drawing.Point(615, 665);
            this.txtMRPIOpen2.MaxLength = 4;
            this.txtMRPIOpen2.Name = "txtMRPIOpen2";
            this.txtMRPIOpen2.Size = new System.Drawing.Size(77, 40);
            this.txtMRPIOpen2.TabIndex = 34;
            this.txtMRPIOpen2.Tag = "land";
            this.txtMRPIOpen2.Enter += new System.EventHandler(this.txtMRPIOpen2_Enter);
            this.txtMRPIOpen2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMRPIOpen2_Validating);
            this.txtMRPIOpen2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMRPIOpen2_KeyDown);
            // 
            // chkZoneOverride
            // 
            this.chkZoneOverride.Location = new System.Drawing.Point(425, 335);
            this.chkZoneOverride.Name = "chkZoneOverride";
            this.chkZoneOverride.Size = new System.Drawing.Size(174, 22);
            this.chkZoneOverride.TabIndex = 30;
            this.chkZoneOverride.Text = "Price as secondary zone";
            // 
            // GridLand
            // 
            this.GridLand.Cols = 4;
            this.GridLand.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridLand.FixedCols = 0;
            this.GridLand.Location = new System.Drawing.Point(20, 715);
            this.GridLand.Name = "GridLand";
            this.GridLand.ReadOnly = false;
            this.GridLand.RowHeadersVisible = false;
            this.GridLand.Rows = 8;
            this.GridLand.ShowFocusCell = false;
            this.GridLand.Size = new System.Drawing.Size(570, 214);
            this.GridLand.StandardTab = false;
            this.GridLand.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridLand.TabIndex = 35;
            this.GridLand.Tag = "land";
            this.GridLand.ComboDropDown += new System.EventHandler(this.GridLand_ComboDropDown);
            this.GridLand.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridLand_AfterEdit);
            this.GridLand.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridLand_BeforeEdit);
            this.GridLand.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridLand_ValidateEdit);
            this.GridLand.CurrentCellChanged += new System.EventHandler(this.GridLand_RowColChange);
            this.GridLand.KeyDown += new Wisej.Web.KeyEventHandler(this.GridLand_KeyDownEvent);
            // 
            // gridLandCode
            // 
            this.gridLandCode.Cols = 1;
            this.gridLandCode.ColumnHeadersVisible = false;
            this.gridLandCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridLandCode.ExtendLastCol = true;
            this.gridLandCode.FixedCols = 0;
            this.gridLandCode.FixedRows = 0;
            this.gridLandCode.Location = new System.Drawing.Point(110, 20);
            this.gridLandCode.Name = "gridLandCode";
            this.gridLandCode.ReadOnly = false;
            this.gridLandCode.RowHeadersVisible = false;
            this.gridLandCode.Rows = 1;
            this.gridLandCode.ShowFocusCell = false;
            this.gridLandCode.Size = new System.Drawing.Size(280, 45);
            this.gridLandCode.TabIndex = 21;
            this.gridLandCode.Tag = "land";
            this.gridLandCode.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.gridLandCode_BeforeEdit);
            this.gridLandCode.Enter += new System.EventHandler(this.gridLandCode_Enter);
            // 
            // gridBldgCode
            // 
            this.gridBldgCode.Cols = 1;
            this.gridBldgCode.ColumnHeadersVisible = false;
            this.gridBldgCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridBldgCode.ExtendLastCol = true;
            this.gridBldgCode.FixedCols = 0;
            this.gridBldgCode.FixedRows = 0;
            this.gridBldgCode.Location = new System.Drawing.Point(110, 75);
            this.gridBldgCode.Name = "gridBldgCode";
            this.gridBldgCode.ReadOnly = false;
            this.gridBldgCode.RowHeadersVisible = false;
            this.gridBldgCode.Rows = 1;
            this.gridBldgCode.ShowFocusCell = false;
            this.gridBldgCode.Size = new System.Drawing.Size(280, 45);
            this.gridBldgCode.TabIndex = 22;
            this.gridBldgCode.Tag = "land";
            this.gridBldgCode.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.gridBldgCode_BeforeEdit);
            // 
            // gridPropertyCode
            // 
            this.gridPropertyCode.Cols = 1;
            this.gridPropertyCode.ColumnHeadersVisible = false;
            this.gridPropertyCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridPropertyCode.ExtendLastCol = true;
            this.gridPropertyCode.FixedCols = 0;
            this.gridPropertyCode.FixedRows = 0;
            this.gridPropertyCode.Location = new System.Drawing.Point(110, 130);
            this.gridPropertyCode.Name = "gridPropertyCode";
            this.gridPropertyCode.ReadOnly = false;
            this.gridPropertyCode.RowHeadersVisible = false;
            this.gridPropertyCode.Rows = 1;
            this.gridPropertyCode.ShowFocusCell = false;
            this.gridPropertyCode.Size = new System.Drawing.Size(280, 45);
            this.gridPropertyCode.TabIndex = 23;
            this.gridPropertyCode.Tag = "land";
            this.gridPropertyCode.ComboDropDown += new System.EventHandler(this.gridPropertyCode_ComboDropDown);
            this.gridPropertyCode.ComboCloseUp += new System.EventHandler(this.gridPropertyCode_ComboCloseUp);
            this.gridPropertyCode.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.gridPropertyCode_BeforeEdit);
            this.gridPropertyCode.KeyDown += new Wisej.Web.KeyEventHandler(this.gridPropertyCode_KeyDownEvent);
            // 
            // GridNeighborhood
            // 
            this.GridNeighborhood.Cols = 1;
            this.GridNeighborhood.ColumnHeadersVisible = false;
            this.GridNeighborhood.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridNeighborhood.ExtendLastCol = true;
            this.GridNeighborhood.FixedCols = 0;
            this.GridNeighborhood.FixedRows = 0;
            this.GridNeighborhood.Location = new System.Drawing.Point(615, 20);
            this.GridNeighborhood.Name = "GridNeighborhood";
            this.GridNeighborhood.ReadOnly = false;
            this.GridNeighborhood.RowHeadersVisible = false;
            this.GridNeighborhood.Rows = 1;
            this.GridNeighborhood.ShowFocusCell = false;
            this.GridNeighborhood.Size = new System.Drawing.Size(313, 45);
            this.GridNeighborhood.TabIndex = 24;
            this.GridNeighborhood.Tag = "land";
            this.GridNeighborhood.ComboDropDown += new System.EventHandler(this.GridNeighborhood_ComboDropDown);
            this.GridNeighborhood.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridNeighborhood_BeforeEdit);
            this.GridNeighborhood.KeyDown += new Wisej.Web.KeyEventHandler(this.GridNeighborhood_KeyDownEvent);
            // 
            // GridZone
            // 
            this.GridZone.Cols = 1;
            this.GridZone.ColumnHeadersVisible = false;
            this.GridZone.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridZone.ExtendLastCol = true;
            this.GridZone.FixedCols = 0;
            this.GridZone.FixedRows = 0;
            this.GridZone.Location = new System.Drawing.Point(615, 225);
            this.GridZone.Name = "GridZone";
            this.GridZone.ReadOnly = false;
            this.GridZone.RowHeadersVisible = false;
            this.GridZone.Rows = 1;
            this.GridZone.ShowFocusCell = false;
            this.GridZone.Size = new System.Drawing.Size(313, 45);
            this.GridZone.TabIndex = 28;
            this.GridZone.Tag = "land";
            this.GridZone.ComboDropDown += new System.EventHandler(this.GridZone_ComboDropDown);
            this.GridZone.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridZone_BeforeEdit);
            this.GridZone.KeyDown += new Wisej.Web.KeyEventHandler(this.GridZone_KeyDownEvent);
            // 
            // GridSecZone
            // 
            this.GridSecZone.Cols = 1;
            this.GridSecZone.ColumnHeadersVisible = false;
            this.GridSecZone.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridSecZone.ExtendLastCol = true;
            this.GridSecZone.FixedCols = 0;
            this.GridSecZone.FixedRows = 0;
            this.GridSecZone.Location = new System.Drawing.Point(615, 280);
            this.GridSecZone.Name = "GridSecZone";
            this.GridSecZone.ReadOnly = false;
            this.GridSecZone.RowHeadersVisible = false;
            this.GridSecZone.Rows = 1;
            this.GridSecZone.ShowFocusCell = false;
            this.GridSecZone.Size = new System.Drawing.Size(313, 45);
            this.GridSecZone.TabIndex = 29;
            this.GridSecZone.Tag = "land";
            this.GridSecZone.ComboDropDown += new System.EventHandler(this.GridSecZone_ComboDropDown);
            this.GridSecZone.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridSecZone_BeforeEdit);
            this.GridSecZone.KeyDown += new Wisej.Web.KeyEventHandler(this.GridSecZone_KeyDownEvent);
            // 
            // GridTopography
            // 
            this.GridTopography.Cols = 1;
            this.GridTopography.ColumnHeadersVisible = false;
            this.GridTopography.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridTopography.ExtendLastCol = true;
            this.GridTopography.FixedCols = 0;
            this.GridTopography.FixedRows = 0;
            this.GridTopography.Location = new System.Drawing.Point(615, 372);
            this.GridTopography.Name = "GridTopography";
            this.GridTopography.ReadOnly = false;
            this.GridTopography.RowHeadersVisible = false;
            this.GridTopography.ShowFocusCell = false;
            this.GridTopography.Size = new System.Drawing.Size(313, 84);
            this.GridTopography.StandardTab = false;
            this.GridTopography.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridTopography.TabIndex = 31;
            this.GridTopography.Tag = "land";
            this.GridTopography.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridTopography_BeforeRowColChange);
            this.GridTopography.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridTopography_BeforeEdit);
            this.GridTopography.CurrentCellChanged += new System.EventHandler(this.GridTopography_RowColChange);
            this.GridTopography.KeyDown += new Wisej.Web.KeyEventHandler(this.GridTopography_KeyDownEvent);
            // 
            // GridUtilities
            // 
            this.GridUtilities.Cols = 1;
            this.GridUtilities.ColumnHeadersVisible = false;
            this.GridUtilities.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridUtilities.ExtendLastCol = true;
            this.GridUtilities.FixedCols = 0;
            this.GridUtilities.FixedRows = 0;
            this.GridUtilities.Location = new System.Drawing.Point(615, 466);
            this.GridUtilities.Name = "GridUtilities";
            this.GridUtilities.ReadOnly = false;
            this.GridUtilities.RowHeadersVisible = false;
            this.GridUtilities.ShowFocusCell = false;
            this.GridUtilities.Size = new System.Drawing.Size(313, 84);
            this.GridUtilities.StandardTab = false;
            this.GridUtilities.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridUtilities.TabIndex = 279;
            this.GridUtilities.Tag = "land";
            this.GridUtilities.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.GridUtilities_BeforeRowColChange);
            this.GridUtilities.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridUtilities_BeforeEdit);
            this.GridUtilities.CurrentCellChanged += new System.EventHandler(this.GridUtilities_RowColChange);
            this.GridUtilities.KeyDown += new Wisej.Web.KeyEventHandler(this.GridUtilities_KeyDownEvent);
            // 
            // GridStreet
            // 
            this.GridStreet.Cols = 1;
            this.GridStreet.ColumnHeadersVisible = false;
            this.GridStreet.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridStreet.ExtendLastCol = true;
            this.GridStreet.FixedCols = 0;
            this.GridStreet.FixedRows = 0;
            this.GridStreet.Location = new System.Drawing.Point(615, 560);
            this.GridStreet.Name = "GridStreet";
            this.GridStreet.ReadOnly = false;
            this.GridStreet.RowHeadersVisible = false;
            this.GridStreet.Rows = 1;
            this.GridStreet.ShowFocusCell = false;
            this.GridStreet.Size = new System.Drawing.Size(313, 45);
            this.GridStreet.TabIndex = 32;
            this.GridStreet.Tag = "land";
            this.GridStreet.Click += new System.EventHandler(this.GridStreet_ClickEvent);
            this.GridStreet.KeyDown += new Wisej.Web.KeyEventHandler(this.GridStreet_KeyDownEvent);
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(20, 89);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(60, 16);
            this.Label13.TabIndex = 294;
            this.Label13.Text = "BUILDING";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 34);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(48, 16);
            this.Label12.TabIndex = 293;
            this.Label12.Text = "LAND";
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(20, 144);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(65, 16);
            this.Label22.TabIndex = 292;
            this.Label22.Text = "PROPERTY";
            this.ToolTip1.SetToolTip(this.Label22, "State Property Type Code");
            // 
            // Label8_36
            // 
            this.Label8_36.Location = new System.Drawing.Point(615, 807);
            this.Label8_36.Name = "Label8_36";
            this.Label8_36.Size = new System.Drawing.Size(60, 18);
            this.Label8_36.TabIndex = 291;
            this.Label8_36.Text = "ACREAGE";
            // 
            // Label8_10
            // 
            this.Label8_10.Location = new System.Drawing.Point(425, 34);
            this.Label8_10.Name = "Label8_10";
            this.Label8_10.Size = new System.Drawing.Size(118, 16);
            this.Label8_10.TabIndex = 290;
            this.Label8_10.Text = "NEIGHBORHOOD";
            // 
            // Label8_11
            // 
            this.Label8_11.Location = new System.Drawing.Point(425, 89);
            this.Label8_11.Name = "Label8_11";
            this.Label8_11.Size = new System.Drawing.Size(130, 16);
            this.Label8_11.TabIndex = 289;
            this.Label8_11.Text = "TREE GROWTH (YEAR)";
            // 
            // Label8_12
            // 
            this.Label8_12.Location = new System.Drawing.Point(425, 139);
            this.Label8_12.Name = "Label8_12";
            this.Label8_12.Size = new System.Drawing.Size(120, 16);
            this.Label8_12.TabIndex = 288;
            this.Label8_12.Text = "NEW DESCRIPTION 1";
            // 
            // Label8_13
            // 
            this.Label8_13.Location = new System.Drawing.Point(425, 189);
            this.Label8_13.Name = "Label8_13";
            this.Label8_13.Size = new System.Drawing.Size(120, 16);
            this.Label8_13.TabIndex = 287;
            this.Label8_13.Text = "NEW DESCRIPTION 2";
            // 
            // Label8_14
            // 
            this.Label8_14.Location = new System.Drawing.Point(425, 239);
            this.Label8_14.Name = "Label8_14";
            this.Label8_14.Size = new System.Drawing.Size(80, 16);
            this.Label8_14.TabIndex = 286;
            this.Label8_14.Text = "ZONING USE";
            // 
            // Label8_15
            // 
            this.Label8_15.Location = new System.Drawing.Point(425, 294);
            this.Label8_15.Name = "Label8_15";
            this.Label8_15.Size = new System.Drawing.Size(113, 16);
            this.Label8_15.TabIndex = 285;
            this.Label8_15.Text = "SECONDARY ZONE";
            // 
            // Label8_16
            // 
            this.Label8_16.Location = new System.Drawing.Point(425, 386);
            this.Label8_16.Name = "Label8_16";
            this.Label8_16.Size = new System.Drawing.Size(90, 16);
            this.Label8_16.TabIndex = 284;
            this.Label8_16.Text = "TOPOGRAPHY";
            // 
            // Label8_17
            // 
            this.Label8_17.Location = new System.Drawing.Point(425, 480);
            this.Label8_17.Name = "Label8_17";
            this.Label8_17.Size = new System.Drawing.Size(70, 16);
            this.Label8_17.TabIndex = 283;
            this.Label8_17.Text = "UTILITIES";
            // 
            // Label8_18
            // 
            this.Label8_18.Location = new System.Drawing.Point(425, 574);
            this.Label8_18.Name = "Label8_18";
            this.Label8_18.Size = new System.Drawing.Size(60, 16);
            this.Label8_18.TabIndex = 282;
            this.Label8_18.Text = "STREET";
            // 
            // Label8_19
            // 
            this.Label8_19.Location = new System.Drawing.Point(425, 629);
            this.Label8_19.Name = "Label8_19";
            this.Label8_19.Size = new System.Drawing.Size(184, 16);
            this.Label8_19.TabIndex = 281;
            this.Label8_19.Text = "INSPECTION CODE";
            // 
            // Label8_20
            // 
            this.Label8_20.Location = new System.Drawing.Point(425, 679);
            this.Label8_20.Name = "Label8_20";
            this.Label8_20.Size = new System.Drawing.Size(184, 16);
            this.Label8_20.TabIndex = 280;
            this.Label8_20.Text = "WARD CODE";
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.GridDwelling1);
            this.SSTab1_Page3.Controls.Add(this.GridDwelling2);
            this.SSTab1_Page3.Controls.Add(this.GridDwelling3);
            this.SSTab1_Page3.Controls.Add(this.lblMapLotCopy_1);
            this.SSTab1_Page3.Controls.Add(this.lblDwel);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Size = new System.Drawing.Size(948, 850);
            this.SSTab1_Page3.Text = "Dwelling";
            // 
            // GridDwelling1
            // 
            this.GridDwelling1.ColumnHeadersVisible = false;
            this.GridDwelling1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridDwelling1.ExtendLastCol = true;
            this.GridDwelling1.FixedRows = 0;
            this.GridDwelling1.Location = new System.Drawing.Point(20, 55);
            this.GridDwelling1.Name = "GridDwelling1";
            this.GridDwelling1.ReadOnly = false;
            this.GridDwelling1.Rows = 15;
            this.GridDwelling1.ShowFocusCell = false;
            this.GridDwelling1.Size = new System.Drawing.Size(286, 386);
            this.GridDwelling1.StandardTab = false;
            this.GridDwelling1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridDwelling1.TabIndex = 274;
            this.GridDwelling1.Tag = "dwelling";
            this.GridDwelling1.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridDwelling1_BeforeEdit);
            this.GridDwelling1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridDwelling1_ValidateEdit);
            this.GridDwelling1.CurrentCellChanged += new System.EventHandler(this.GridDwelling1_RowColChange);
            this.GridDwelling1.Enter += new System.EventHandler(this.GridDwelling1_Enter);
            this.GridDwelling1.KeyDown += new Wisej.Web.KeyEventHandler(this.GridDwelling1_KeyDownEvent);
            // 
            // GridDwelling2
            // 
            this.GridDwelling2.ColumnHeadersVisible = false;
            this.GridDwelling2.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridDwelling2.ExtendLastCol = true;
            this.GridDwelling2.FixedRows = 0;
            this.GridDwelling2.Location = new System.Drawing.Point(326, 55);
            this.GridDwelling2.Name = "GridDwelling2";
            this.GridDwelling2.ReadOnly = false;
            this.GridDwelling2.Rows = 16;
            this.GridDwelling2.ShowFocusCell = false;
            this.GridDwelling2.Size = new System.Drawing.Size(290, 386);
            this.GridDwelling2.StandardTab = false;
            this.GridDwelling2.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridDwelling2.TabIndex = 275;
            this.GridDwelling2.Tag = "dwelling";
            this.GridDwelling2.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridDwelling2_BeforeEdit);
            this.GridDwelling2.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridDwelling2_ValidateEdit);
            this.GridDwelling2.CurrentCellChanged += new System.EventHandler(this.GridDwelling2_RowColChange);
            this.GridDwelling2.KeyDown += new Wisej.Web.KeyEventHandler(this.GridDwelling2_KeyDownEvent);
            // 
            // GridDwelling3
            // 
            this.GridDwelling3.ColumnHeadersVisible = false;
            this.GridDwelling3.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridDwelling3.ExtendLastCol = true;
            this.GridDwelling3.FixedRows = 0;
            this.GridDwelling3.Location = new System.Drawing.Point(635, 55);
            this.GridDwelling3.Name = "GridDwelling3";
            this.GridDwelling3.ReadOnly = false;
            this.GridDwelling3.Rows = 13;
            this.GridDwelling3.ShowFocusCell = false;
            this.GridDwelling3.Size = new System.Drawing.Size(290, 386);
            this.GridDwelling3.StandardTab = false;
            this.GridDwelling3.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridDwelling3.TabIndex = 276;
            this.GridDwelling3.Tag = "dwelling";
            this.GridDwelling3.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridDwelling3_BeforeEdit);
            this.GridDwelling3.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridDwelling3_ValidateEdit);
            this.GridDwelling3.CurrentCellChanged += new System.EventHandler(this.GridDwelling3_RowColChange);
            this.GridDwelling3.KeyDown += new Wisej.Web.KeyEventHandler(this.GridDwelling3_KeyDownEvent);
            // 
            // lblMapLotCopy_1
            // 
            this.lblMapLotCopy_1.Location = new System.Drawing.Point(20, 451);
            this.lblMapLotCopy_1.Name = "lblMapLotCopy_1";
            this.lblMapLotCopy_1.Size = new System.Drawing.Size(318, 15);
            this.lblMapLotCopy_1.TabIndex = 300;
            this.lblMapLotCopy_1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblDwel
            // 
            this.lblDwel.Location = new System.Drawing.Point(20, 30);
            this.lblDwel.Name = "lblDwel";
            this.lblDwel.Size = new System.Drawing.Size(131, 15);
            this.lblDwel.TabIndex = 295;
            this.lblDwel.Text = "VIEW ONLY";
            this.lblDwel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDwel.Visible = false;
            // 
            // SSTab1_Page4
            // 
            this.SSTab1_Page4.Controls.Add(this.Frame4);
            this.SSTab1_Page4.Controls.Add(this.Label6_0);
            this.SSTab1_Page4.Controls.Add(this.Label5);
            this.SSTab1_Page4.Controls.Add(this.Label4_0);
            this.SSTab1_Page4.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page4.Name = "SSTab1_Page4";
            this.SSTab1_Page4.Size = new System.Drawing.Size(948, 850);
            this.SSTab1_Page4.Text = "Commercial";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.mebC1Heat);
            this.Frame4.Controls.Add(this.mebc2grade);
            this.Frame4.Controls.Add(this.mebC1Grade);
            this.Frame4.Controls.Add(this.mebOcc1);
            this.Frame4.Controls.Add(this.mebC1Height);
            this.Frame4.Controls.Add(this.mebDwel1);
            this.Frame4.Controls.Add(this.mebC1Class);
            this.Frame4.Controls.Add(this.mebC1Quality);
            this.Frame4.Controls.Add(this.mebC1ExteriorWalls);
            this.Frame4.Controls.Add(this.mebC1Stories);
            this.Frame4.Controls.Add(this.mebC1Floor);
            this.Frame4.Controls.Add(this.mebC1Perimeter);
            this.Frame4.Controls.Add(this.mebC1Built);
            this.Frame4.Controls.Add(this.mebC1Remodel);
            this.Frame4.Controls.Add(this.mebC1Condition);
            this.Frame4.Controls.Add(this.mebC1Phys);
            this.Frame4.Controls.Add(this.mebC1Funct);
            this.Frame4.Controls.Add(this.mebCMEcon);
            this.Frame4.Controls.Add(this.mebOcc2);
            this.Frame4.Controls.Add(this.mebDwel2);
            this.Frame4.Controls.Add(this.mebC2Class);
            this.Frame4.Controls.Add(this.mebC2Quality);
            this.Frame4.Controls.Add(this.mebC2ExtWalls);
            this.Frame4.Controls.Add(this.mebC2Stories);
            this.Frame4.Controls.Add(this.mebC2Floor);
            this.Frame4.Controls.Add(this.mebC2Perimeter);
            this.Frame4.Controls.Add(this.mebC2Heat);
            this.Frame4.Controls.Add(this.mebC2Built);
            this.Frame4.Controls.Add(this.mebC2Remodel);
            this.Frame4.Controls.Add(this.mebC2Condition);
            this.Frame4.Controls.Add(this.mebC2Phys);
            this.Frame4.Controls.Add(this.mebC2Funct);
            this.Frame4.Controls.Add(this.mebC2Height);
            this.Frame4.Controls.Add(this.Label3_31);
            this.Frame4.Controls.Add(this.Label3_30);
            this.Frame4.Controls.Add(this.Label3_29);
            this.Frame4.Controls.Add(this.Label3_25);
            this.Frame4.Controls.Add(this.Label3_24);
            this.Frame4.Controls.Add(this.Label3_23);
            this.Frame4.Controls.Add(this.Label3_22);
            this.Frame4.Controls.Add(this.Label3_21);
            this.Frame4.Controls.Add(this.Label3_20);
            this.Frame4.Controls.Add(this.Label3_19);
            this.Frame4.Controls.Add(this.Label3_18);
            this.Frame4.Controls.Add(this.Label3_17);
            this.Frame4.Controls.Add(this.Label3_16);
            this.Frame4.Controls.Add(this.Label3_15);
            this.Frame4.Controls.Add(this.Label3_14);
            this.Frame4.Controls.Add(this.Label3_13);
            this.Frame4.Controls.Add(this.Label3_12);
            this.Frame4.Controls.Add(this.Label3_11);
            this.Frame4.Controls.Add(this.Label3_10);
            this.Frame4.Controls.Add(this.Label3_9);
            this.Frame4.Controls.Add(this.Label3_8);
            this.Frame4.Controls.Add(this.Label3_7);
            this.Frame4.Controls.Add(this.Label3_6);
            this.Frame4.Controls.Add(this.Label3_5);
            this.Frame4.Controls.Add(this.Label3_4);
            this.Frame4.Controls.Add(this.Label3_3);
            this.Frame4.Controls.Add(this.Label3_2);
            this.Frame4.Controls.Add(this.Label3_1);
            this.Frame4.Controls.Add(this.Label3_32);
            this.Frame4.Controls.Add(this.lblcomm);
            this.Frame4.Dock = Wisej.Web.DockStyle.Fill;
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(948, 850);
            this.Frame4.TabIndex = 51;
            // 
            // mebC1Heat
            // 
            this.mebC1Heat.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Heat.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Heat.Location = new System.Drawing.Point(183, 272);
            this.mebC1Heat.MaxLength = 2;
            this.mebC1Heat.Name = "mebC1Heat";
            this.mebC1Heat.Size = new System.Drawing.Size(90, 20);
            this.mebC1Heat.TabIndex = 62;
            this.mebC1Heat.Tag = "commercial";
            this.mebC1Heat.Enter += new System.EventHandler(this.mebC1Heat_Enter);
            this.mebC1Heat.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Heat_Validating);
            this.mebC1Heat.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Heat_KeyUp);
            // 
            // mebc2grade
            // 
            this.mebc2grade.BackColor = System.Drawing.SystemColors.Window;
            this.mebc2grade.Cursor = Wisej.Web.Cursors.Default;
            this.mebc2grade.Location = new System.Drawing.Point(596, 133);
            this.mebc2grade.MaxLength = 4;
            this.mebc2grade.Name = "mebc2grade";
            this.mebc2grade.Size = new System.Drawing.Size(90, 20);
            this.mebc2grade.TabIndex = 73;
            this.mebc2grade.Tag = "commercial";
            this.mebc2grade.Enter += new System.EventHandler(this.mebc2grade_Enter);
            this.mebc2grade.Validating += new System.ComponentModel.CancelEventHandler(this.mebc2grade_Validating);
            this.mebc2grade.KeyUp += new Wisej.Web.KeyEventHandler(this.mebc2grade_KeyUp);
            // 
            // mebC1Grade
            // 
            this.mebC1Grade.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Grade.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Grade.Location = new System.Drawing.Point(183, 133);
            this.mebC1Grade.MaxLength = 4;
            this.mebC1Grade.Name = "mebC1Grade";
            this.mebC1Grade.Size = new System.Drawing.Size(90, 20);
            this.mebC1Grade.TabIndex = 56;
            this.mebC1Grade.Tag = "commercial";
            this.mebC1Grade.Enter += new System.EventHandler(this.mebC1Grade_Enter);
            this.mebC1Grade.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Grade_Validating);
            this.mebC1Grade.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Grade_KeyUp);
            // 
            // mebOcc1
            // 
            this.mebOcc1.BackColor = System.Drawing.SystemColors.Window;
            this.mebOcc1.Cursor = Wisej.Web.Cursors.Default;
            this.mebOcc1.Location = new System.Drawing.Point(183, 50);
            this.mebOcc1.MaxLength = 3;
            this.mebOcc1.Name = "mebOcc1";
            this.mebOcc1.Size = new System.Drawing.Size(90, 20);
            this.mebOcc1.TabIndex = 52;
            this.mebOcc1.Tag = "commercial";
            this.mebOcc1.Enter += new System.EventHandler(this.mebOcc1_Enter);
            this.mebOcc1.Validating += new System.ComponentModel.CancelEventHandler(this.mebOcc1_Validating);
            this.mebOcc1.KeyUp += new Wisej.Web.KeyEventHandler(this.mebOcc1_KeyUp);
            // 
            // mebC1Height
            // 
            this.mebC1Height.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Height.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Height.Location = new System.Drawing.Point(297, 187);
            this.mebC1Height.MaxLength = 2;
            this.mebC1Height.Name = "mebC1Height";
            this.mebC1Height.Size = new System.Drawing.Size(90, 20);
            this.mebC1Height.TabIndex = 59;
            this.mebC1Height.Tag = "commercial";
            this.mebC1Height.Enter += new System.EventHandler(this.mebC1Height_Enter);
            this.mebC1Height.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Height_Validating);
            this.mebC1Height.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Height_KeyUp);
            // 
            // mebDwel1
            // 
            this.mebDwel1.BackColor = System.Drawing.SystemColors.Window;
            this.mebDwel1.Cursor = Wisej.Web.Cursors.Default;
            this.mebDwel1.Location = new System.Drawing.Point(183, 78);
            this.mebDwel1.MaxLength = 2;
            this.mebDwel1.Name = "mebDwel1";
            this.mebDwel1.Size = new System.Drawing.Size(90, 20);
            this.mebDwel1.TabIndex = 53;
            this.mebDwel1.Tag = "commercial";
            this.mebDwel1.Enter += new System.EventHandler(this.mebDwel1_Enter);
            this.mebDwel1.Validating += new System.ComponentModel.CancelEventHandler(this.mebDwel1_Validating);
            this.mebDwel1.KeyUp += new Wisej.Web.KeyEventHandler(this.mebDwel1_KeyUp);
            // 
            // mebC1Class
            // 
            this.mebC1Class.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Class.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Class.Location = new System.Drawing.Point(183, 105);
            this.mebC1Class.MaxLength = 1;
            this.mebC1Class.Name = "mebC1Class";
            this.mebC1Class.Size = new System.Drawing.Size(90, 20);
            this.mebC1Class.TabIndex = 54;
            this.mebC1Class.Tag = "commercial";
            this.mebC1Class.Enter += new System.EventHandler(this.mebC1Class_Enter);
            this.mebC1Class.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Class_Validating);
            this.mebC1Class.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Class_KeyUp);
            // 
            // mebC1Quality
            // 
            this.mebC1Quality.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Quality.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Quality.Location = new System.Drawing.Point(297, 105);
            this.mebC1Quality.MaxLength = 1;
            this.mebC1Quality.Name = "mebC1Quality";
            this.mebC1Quality.Size = new System.Drawing.Size(90, 20);
            this.mebC1Quality.TabIndex = 55;
            this.mebC1Quality.Tag = "commercial";
            this.mebC1Quality.Enter += new System.EventHandler(this.mebC1Quality_Enter);
            this.mebC1Quality.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Quality_Validating);
            this.mebC1Quality.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Quality_KeyUp);
            // 
            // mebC1ExteriorWalls
            // 
            this.mebC1ExteriorWalls.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1ExteriorWalls.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1ExteriorWalls.Location = new System.Drawing.Point(183, 160);
            this.mebC1ExteriorWalls.MaxLength = 1;
            this.mebC1ExteriorWalls.Name = "mebC1ExteriorWalls";
            this.mebC1ExteriorWalls.Size = new System.Drawing.Size(90, 20);
            this.mebC1ExteriorWalls.TabIndex = 57;
            this.mebC1ExteriorWalls.Tag = "commercial";
            this.mebC1ExteriorWalls.Enter += new System.EventHandler(this.mebC1ExteriorWalls_Enter);
            this.mebC1ExteriorWalls.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1ExteriorWalls_Validating);
            this.mebC1ExteriorWalls.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1ExteriorWalls_KeyUp);
            // 
            // mebC1Stories
            // 
            this.mebC1Stories.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Stories.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Stories.Location = new System.Drawing.Point(183, 187);
            this.mebC1Stories.MaxLength = 2;
            this.mebC1Stories.Name = "mebC1Stories";
            this.mebC1Stories.Size = new System.Drawing.Size(90, 20);
            this.mebC1Stories.TabIndex = 58;
            this.mebC1Stories.Tag = "commercial";
            this.mebC1Stories.Enter += new System.EventHandler(this.mebC1Stories_Enter);
            this.mebC1Stories.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Stories_Validating);
            this.mebC1Stories.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Stories_KeyUp);
            // 
            // mebC1Floor
            // 
            this.mebC1Floor.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Floor.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Floor.Location = new System.Drawing.Point(183, 216);
            this.mebC1Floor.MaxLength = 6;
            this.mebC1Floor.Name = "mebC1Floor";
            this.mebC1Floor.Size = new System.Drawing.Size(90, 20);
            this.mebC1Floor.TabIndex = 60;
            this.mebC1Floor.Tag = "commercial";
            this.mebC1Floor.Enter += new System.EventHandler(this.mebC1Floor_Enter);
            this.mebC1Floor.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Floor_Validating);
            this.mebC1Floor.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Floor_KeyUp);
            // 
            // mebC1Perimeter
            // 
            this.mebC1Perimeter.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Perimeter.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Perimeter.Location = new System.Drawing.Point(183, 244);
            this.mebC1Perimeter.MaxLength = 5;
            this.mebC1Perimeter.Name = "mebC1Perimeter";
            this.mebC1Perimeter.Size = new System.Drawing.Size(90, 20);
            this.mebC1Perimeter.TabIndex = 61;
            this.mebC1Perimeter.Tag = "commercial";
            this.mebC1Perimeter.Enter += new System.EventHandler(this.mebC1Perimeter_Enter);
            this.mebC1Perimeter.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Perimeter_Validating);
            this.mebC1Perimeter.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Perimeter_KeyUp);
            // 
            // mebC1Built
            // 
            this.mebC1Built.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Built.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Built.Location = new System.Drawing.Point(183, 302);
            this.mebC1Built.MaxLength = 4;
            this.mebC1Built.Name = "mebC1Built";
            this.mebC1Built.Size = new System.Drawing.Size(90, 20);
            this.mebC1Built.TabIndex = 63;
            this.mebC1Built.Tag = "commercial";
            this.mebC1Built.Enter += new System.EventHandler(this.mebC1Built_Enter);
            this.mebC1Built.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Built_Validating);
            this.mebC1Built.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Built_KeyUp);
            // 
            // mebC1Remodel
            // 
            this.mebC1Remodel.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Remodel.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Remodel.Location = new System.Drawing.Point(183, 331);
            this.mebC1Remodel.MaxLength = 4;
            this.mebC1Remodel.Name = "mebC1Remodel";
            this.mebC1Remodel.Size = new System.Drawing.Size(90, 20);
            this.mebC1Remodel.TabIndex = 64;
            this.mebC1Remodel.Tag = "commercial";
            this.mebC1Remodel.Enter += new System.EventHandler(this.mebC1Remodel_Enter);
            this.mebC1Remodel.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Remodel_Validating);
            this.mebC1Remodel.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Remodel_KeyUp);
            // 
            // mebC1Condition
            // 
            this.mebC1Condition.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Condition.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Condition.Location = new System.Drawing.Point(183, 357);
            this.mebC1Condition.MaxLength = 1;
            this.mebC1Condition.Name = "mebC1Condition";
            this.mebC1Condition.Size = new System.Drawing.Size(90, 20);
            this.mebC1Condition.TabIndex = 65;
            this.mebC1Condition.Tag = "commercial";
            this.mebC1Condition.Enter += new System.EventHandler(this.mebC1Condition_Enter);
            this.mebC1Condition.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Condition_Validating);
            this.mebC1Condition.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Condition_KeyUp);
            // 
            // mebC1Phys
            // 
            this.mebC1Phys.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Phys.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Phys.Location = new System.Drawing.Point(183, 384);
            this.mebC1Phys.MaxLength = 2;
            this.mebC1Phys.Name = "mebC1Phys";
            this.mebC1Phys.Size = new System.Drawing.Size(90, 20);
            this.mebC1Phys.TabIndex = 66;
            this.mebC1Phys.Tag = "commercial";
            this.mebC1Phys.Enter += new System.EventHandler(this.mebC1Phys_Enter);
            this.mebC1Phys.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Phys_Validating);
            this.mebC1Phys.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Phys_KeyUp);
            // 
            // mebC1Funct
            // 
            this.mebC1Funct.BackColor = System.Drawing.SystemColors.Window;
            this.mebC1Funct.Cursor = Wisej.Web.Cursors.Default;
            this.mebC1Funct.Location = new System.Drawing.Point(183, 411);
            this.mebC1Funct.MaxLength = 3;
            this.mebC1Funct.Name = "mebC1Funct";
            this.mebC1Funct.Size = new System.Drawing.Size(90, 20);
            this.mebC1Funct.TabIndex = 67;
            this.mebC1Funct.Tag = "commercial";
            this.mebC1Funct.Enter += new System.EventHandler(this.mebC1Funct_Enter);
            this.mebC1Funct.Validating += new System.ComponentModel.CancelEventHandler(this.mebC1Funct_Validating);
            this.mebC1Funct.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC1Funct_KeyUp);
            // 
            // mebCMEcon
            // 
            this.mebCMEcon.BackColor = System.Drawing.SystemColors.Window;
            this.mebCMEcon.Cursor = Wisej.Web.Cursors.Default;
            this.mebCMEcon.Location = new System.Drawing.Point(183, 439);
            this.mebCMEcon.MaxLength = 3;
            this.mebCMEcon.Name = "mebCMEcon";
            this.mebCMEcon.Size = new System.Drawing.Size(90, 20);
            this.mebCMEcon.TabIndex = 68;
            this.mebCMEcon.Tag = "commercial";
            this.mebCMEcon.Enter += new System.EventHandler(this.mebCMEcon_Enter);
            this.mebCMEcon.Validating += new System.ComponentModel.CancelEventHandler(this.mebCMEcon_Validating);
            this.mebCMEcon.KeyUp += new Wisej.Web.KeyEventHandler(this.mebCMEcon_KeyUp);
            // 
            // mebOcc2
            // 
            this.mebOcc2.BackColor = System.Drawing.SystemColors.Window;
            this.mebOcc2.Cursor = Wisej.Web.Cursors.Default;
            this.mebOcc2.Location = new System.Drawing.Point(596, 50);
            this.mebOcc2.MaxLength = 3;
            this.mebOcc2.Name = "mebOcc2";
            this.mebOcc2.Size = new System.Drawing.Size(90, 20);
            this.mebOcc2.TabIndex = 69;
            this.mebOcc2.Tag = "commercial";
            this.mebOcc2.Enter += new System.EventHandler(this.mebOcc2_Enter);
            this.mebOcc2.Validating += new System.ComponentModel.CancelEventHandler(this.mebOcc2_Validating);
            this.mebOcc2.KeyUp += new Wisej.Web.KeyEventHandler(this.mebOcc2_KeyUp);
            // 
            // mebDwel2
            // 
            this.mebDwel2.BackColor = System.Drawing.SystemColors.Window;
            this.mebDwel2.Cursor = Wisej.Web.Cursors.Default;
            this.mebDwel2.Location = new System.Drawing.Point(596, 78);
            this.mebDwel2.MaxLength = 2;
            this.mebDwel2.Name = "mebDwel2";
            this.mebDwel2.Size = new System.Drawing.Size(90, 20);
            this.mebDwel2.TabIndex = 70;
            this.mebDwel2.Tag = "commercial";
            this.mebDwel2.Enter += new System.EventHandler(this.mebDwel2_Enter);
            this.mebDwel2.Validating += new System.ComponentModel.CancelEventHandler(this.mebDwel2_Validating);
            this.mebDwel2.KeyUp += new Wisej.Web.KeyEventHandler(this.mebDwel2_KeyUp);
            // 
            // mebC2Class
            // 
            this.mebC2Class.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Class.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Class.Location = new System.Drawing.Point(596, 105);
            this.mebC2Class.MaxLength = 1;
            this.mebC2Class.Name = "mebC2Class";
            this.mebC2Class.Size = new System.Drawing.Size(90, 20);
            this.mebC2Class.TabIndex = 71;
            this.mebC2Class.Tag = "commercial";
            this.mebC2Class.Enter += new System.EventHandler(this.mebC2Class_Enter);
            this.mebC2Class.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Class_Validating);
            this.mebC2Class.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Class_KeyUp);
            // 
            // mebC2Quality
            // 
            this.mebC2Quality.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Quality.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Quality.Location = new System.Drawing.Point(726, 105);
            this.mebC2Quality.MaxLength = 1;
            this.mebC2Quality.Name = "mebC2Quality";
            this.mebC2Quality.Size = new System.Drawing.Size(90, 20);
            this.mebC2Quality.TabIndex = 72;
            this.mebC2Quality.Tag = "commercial";
            this.mebC2Quality.Enter += new System.EventHandler(this.mebC2Quality_Enter);
            this.mebC2Quality.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Quality_Validating);
            this.mebC2Quality.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Quality_KeyUp);
            // 
            // mebC2ExtWalls
            // 
            this.mebC2ExtWalls.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2ExtWalls.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2ExtWalls.Location = new System.Drawing.Point(596, 160);
            this.mebC2ExtWalls.MaxLength = 1;
            this.mebC2ExtWalls.Name = "mebC2ExtWalls";
            this.mebC2ExtWalls.Size = new System.Drawing.Size(90, 20);
            this.mebC2ExtWalls.TabIndex = 74;
            this.mebC2ExtWalls.Tag = "commercial";
            this.mebC2ExtWalls.Enter += new System.EventHandler(this.mebC2ExtWalls_Enter);
            this.mebC2ExtWalls.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2ExtWalls_Validating);
            this.mebC2ExtWalls.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2ExtWalls_KeyUp);
            // 
            // mebC2Stories
            // 
            this.mebC2Stories.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Stories.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Stories.Location = new System.Drawing.Point(596, 187);
            this.mebC2Stories.MaxLength = 2;
            this.mebC2Stories.Name = "mebC2Stories";
            this.mebC2Stories.Size = new System.Drawing.Size(90, 20);
            this.mebC2Stories.TabIndex = 75;
            this.mebC2Stories.Tag = "commercial";
            this.mebC2Stories.Enter += new System.EventHandler(this.mebC2Stories_Enter);
            this.mebC2Stories.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Stories_Validating);
            this.mebC2Stories.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Stories_KeyUp);
            // 
            // mebC2Floor
            // 
            this.mebC2Floor.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Floor.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Floor.Location = new System.Drawing.Point(596, 216);
            this.mebC2Floor.MaxLength = 6;
            this.mebC2Floor.Name = "mebC2Floor";
            this.mebC2Floor.Size = new System.Drawing.Size(90, 20);
            this.mebC2Floor.TabIndex = 77;
            this.mebC2Floor.Tag = "commercial";
            this.mebC2Floor.Enter += new System.EventHandler(this.mebC2Floor_Enter);
            this.mebC2Floor.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Floor_Validating);
            this.mebC2Floor.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Floor_KeyUp);
            // 
            // mebC2Perimeter
            // 
            this.mebC2Perimeter.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Perimeter.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Perimeter.Location = new System.Drawing.Point(596, 244);
            this.mebC2Perimeter.MaxLength = 4;
            this.mebC2Perimeter.Name = "mebC2Perimeter";
            this.mebC2Perimeter.Size = new System.Drawing.Size(90, 20);
            this.mebC2Perimeter.TabIndex = 78;
            this.mebC2Perimeter.Tag = "commercial";
            this.mebC2Perimeter.Enter += new System.EventHandler(this.mebC2Perimeter_Enter);
            this.mebC2Perimeter.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Perimeter_Validating);
            this.mebC2Perimeter.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Perimeter_KeyUp);
            // 
            // mebC2Heat
            // 
            this.mebC2Heat.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Heat.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Heat.Location = new System.Drawing.Point(596, 272);
            this.mebC2Heat.MaxLength = 2;
            this.mebC2Heat.Name = "mebC2Heat";
            this.mebC2Heat.Size = new System.Drawing.Size(90, 20);
            this.mebC2Heat.TabIndex = 79;
            this.mebC2Heat.Tag = "commercial";
            this.mebC2Heat.Enter += new System.EventHandler(this.mebC2Heat_Enter);
            this.mebC2Heat.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Heat_Validating);
            this.mebC2Heat.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Heat_KeyUp);
            // 
            // mebC2Built
            // 
            this.mebC2Built.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Built.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Built.Location = new System.Drawing.Point(596, 302);
            this.mebC2Built.MaxLength = 4;
            this.mebC2Built.Name = "mebC2Built";
            this.mebC2Built.Size = new System.Drawing.Size(90, 20);
            this.mebC2Built.TabIndex = 80;
            this.mebC2Built.Tag = "commercial";
            this.mebC2Built.Enter += new System.EventHandler(this.mebC2Built_Enter);
            this.mebC2Built.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Built_Validating);
            this.mebC2Built.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Built_KeyUp);
            // 
            // mebC2Remodel
            // 
            this.mebC2Remodel.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Remodel.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Remodel.Location = new System.Drawing.Point(596, 331);
            this.mebC2Remodel.MaxLength = 4;
            this.mebC2Remodel.Name = "mebC2Remodel";
            this.mebC2Remodel.Size = new System.Drawing.Size(90, 20);
            this.mebC2Remodel.TabIndex = 81;
            this.mebC2Remodel.Tag = "commercial";
            this.mebC2Remodel.Enter += new System.EventHandler(this.mebC2Remodel_Enter);
            this.mebC2Remodel.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Remodel_Validating);
            this.mebC2Remodel.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Remodel_KeyUp);
            // 
            // mebC2Condition
            // 
            this.mebC2Condition.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Condition.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Condition.Location = new System.Drawing.Point(596, 357);
            this.mebC2Condition.MaxLength = 1;
            this.mebC2Condition.Name = "mebC2Condition";
            this.mebC2Condition.Size = new System.Drawing.Size(90, 20);
            this.mebC2Condition.TabIndex = 82;
            this.mebC2Condition.Tag = "commercial";
            this.mebC2Condition.Enter += new System.EventHandler(this.mebC2Condition_Enter);
            this.mebC2Condition.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Condition_Validating);
            this.mebC2Condition.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Condition_KeyUp);
            // 
            // mebC2Phys
            // 
            this.mebC2Phys.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Phys.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Phys.Location = new System.Drawing.Point(596, 384);
            this.mebC2Phys.MaxLength = 2;
            this.mebC2Phys.Name = "mebC2Phys";
            this.mebC2Phys.Size = new System.Drawing.Size(90, 20);
            this.mebC2Phys.TabIndex = 83;
            this.mebC2Phys.Tag = "commercial";
            this.mebC2Phys.Enter += new System.EventHandler(this.mebC2Phys_Enter);
            this.mebC2Phys.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Phys_Validating);
            this.mebC2Phys.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Phys_KeyUp);
            // 
            // mebC2Funct
            // 
            this.mebC2Funct.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Funct.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Funct.Location = new System.Drawing.Point(596, 411);
            this.mebC2Funct.MaxLength = 3;
            this.mebC2Funct.Name = "mebC2Funct";
            this.mebC2Funct.Size = new System.Drawing.Size(90, 20);
            this.mebC2Funct.TabIndex = 85;
            this.mebC2Funct.Tag = "commercial";
            this.mebC2Funct.Enter += new System.EventHandler(this.mebC2Funct_Enter);
            this.mebC2Funct.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Funct_Validating);
            this.mebC2Funct.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Funct_KeyUp);
            // 
            // mebC2Height
            // 
            this.mebC2Height.BackColor = System.Drawing.SystemColors.Window;
            this.mebC2Height.Cursor = Wisej.Web.Cursors.Default;
            this.mebC2Height.Location = new System.Drawing.Point(726, 187);
            this.mebC2Height.MaxLength = 2;
            this.mebC2Height.Name = "mebC2Height";
            this.mebC2Height.Size = new System.Drawing.Size(90, 20);
            this.mebC2Height.TabIndex = 76;
            this.mebC2Height.Tag = "commercial";
            this.mebC2Height.Enter += new System.EventHandler(this.mebC2Height_Enter);
            this.mebC2Height.Validating += new System.ComponentModel.CancelEventHandler(this.mebC2Height_Validating);
            this.mebC2Height.KeyUp += new Wisej.Web.KeyEventHandler(this.mebC2Height_KeyUp);
            // 
            // Label3_31
            // 
            this.Label3_31.Location = new System.Drawing.Point(427, 411);
            this.Label3_31.Name = "Label3_31";
            this.Label3_31.Size = new System.Drawing.Size(80, 14);
            this.Label3_31.TabIndex = 114;
            this.Label3_31.Text = "FUNCTIONAL";
            // 
            // Label3_30
            // 
            this.Label3_30.Location = new System.Drawing.Point(20, 439);
            this.Label3_30.Name = "Label3_30";
            this.Label3_30.Size = new System.Drawing.Size(65, 14);
            this.Label3_30.TabIndex = 113;
            this.Label3_30.Text = "ECONOMIC";
            // 
            // Label3_29
            // 
            this.Label3_29.Location = new System.Drawing.Point(20, 411);
            this.Label3_29.Name = "Label3_29";
            this.Label3_29.Size = new System.Drawing.Size(80, 14);
            this.Label3_29.TabIndex = 112;
            this.Label3_29.Text = "FUNCTIONAL";
            // 
            // Label3_25
            // 
            this.Label3_25.Location = new System.Drawing.Point(427, 384);
            this.Label3_25.Name = "Label3_25";
            this.Label3_25.Size = new System.Drawing.Size(110, 14);
            this.Label3_25.TabIndex = 111;
            this.Label3_25.Text = "% GOOD PHYSICAL";
            // 
            // Label3_24
            // 
            this.Label3_24.Location = new System.Drawing.Point(427, 357);
            this.Label3_24.Name = "Label3_24";
            this.Label3_24.Size = new System.Drawing.Size(97, 14);
            this.Label3_24.TabIndex = 110;
            this.Label3_24.Text = "CONDITION";
            // 
            // Label3_23
            // 
            this.Label3_23.Location = new System.Drawing.Point(427, 331);
            this.Label3_23.Name = "Label3_23";
            this.Label3_23.Size = new System.Drawing.Size(110, 14);
            this.Label3_23.TabIndex = 109;
            this.Label3_23.Text = "YEAR REMODELED";
            // 
            // Label3_22
            // 
            this.Label3_22.Location = new System.Drawing.Point(427, 302);
            this.Label3_22.Name = "Label3_22";
            this.Label3_22.Size = new System.Drawing.Size(97, 14);
            this.Label3_22.TabIndex = 108;
            this.Label3_22.Text = "YEAR BUILT";
            // 
            // Label3_21
            // 
            this.Label3_21.Location = new System.Drawing.Point(427, 272);
            this.Label3_21.Name = "Label3_21";
            this.Label3_21.Size = new System.Drawing.Size(120, 16);
            this.Label3_21.TabIndex = 107;
            this.Label3_21.Text = "HEATING / COOLING";
            // 
            // Label3_20
            // 
            this.Label3_20.Location = new System.Drawing.Point(427, 244);
            this.Label3_20.Name = "Label3_20";
            this.Label3_20.Size = new System.Drawing.Size(110, 14);
            this.Label3_20.TabIndex = 106;
            this.Label3_20.Text = "PERIMETER / UNITS";
            // 
            // Label3_19
            // 
            this.Label3_19.Location = new System.Drawing.Point(427, 216);
            this.Label3_19.Name = "Label3_19";
            this.Label3_19.Size = new System.Drawing.Size(110, 14);
            this.Label3_19.TabIndex = 105;
            this.Label3_19.Text = "BASE FLOOR AREA";
            // 
            // Label3_18
            // 
            this.Label3_18.Location = new System.Drawing.Point(427, 187);
            this.Label3_18.Name = "Label3_18";
            this.Label3_18.Size = new System.Drawing.Size(110, 16);
            this.Label3_18.TabIndex = 104;
            this.Label3_18.Text = "STORIES / HEIGHT";
            // 
            // Label3_17
            // 
            this.Label3_17.Location = new System.Drawing.Point(427, 160);
            this.Label3_17.Name = "Label3_17";
            this.Label3_17.Size = new System.Drawing.Size(110, 14);
            this.Label3_17.TabIndex = 103;
            this.Label3_17.Text = "EXTERIOR WALLS";
            // 
            // Label3_16
            // 
            this.Label3_16.Location = new System.Drawing.Point(427, 133);
            this.Label3_16.Name = "Label3_16";
            this.Label3_16.Size = new System.Drawing.Size(97, 14);
            this.Label3_16.TabIndex = 102;
            this.Label3_16.Text = "GRADE FACTOR";
            // 
            // Label3_15
            // 
            this.Label3_15.Location = new System.Drawing.Point(427, 105);
            this.Label3_15.Name = "Label3_15";
            this.Label3_15.Size = new System.Drawing.Size(110, 16);
            this.Label3_15.TabIndex = 101;
            this.Label3_15.Text = "CLASS & QUALITY";
            // 
            // Label3_14
            // 
            this.Label3_14.Location = new System.Drawing.Point(427, 78);
            this.Label3_14.Name = "Label3_14";
            this.Label3_14.Size = new System.Drawing.Size(130, 16);
            this.Label3_14.TabIndex = 100;
            this.Label3_14.Text = "# OF DWELLING UNITS";
            // 
            // Label3_13
            // 
            this.Label3_13.Location = new System.Drawing.Point(427, 50);
            this.Label3_13.Name = "Label3_13";
            this.Label3_13.Size = new System.Drawing.Size(115, 16);
            this.Label3_13.TabIndex = 99;
            this.Label3_13.Text = "OCCUPANCY CODE";
            // 
            // Label3_12
            // 
            this.Label3_12.Location = new System.Drawing.Point(20, 384);
            this.Label3_12.Name = "Label3_12";
            this.Label3_12.Size = new System.Drawing.Size(110, 14);
            this.Label3_12.TabIndex = 98;
            this.Label3_12.Text = "% GOOD PHYSICAL";
            // 
            // Label3_11
            // 
            this.Label3_11.Location = new System.Drawing.Point(20, 357);
            this.Label3_11.Name = "Label3_11";
            this.Label3_11.Size = new System.Drawing.Size(97, 14);
            this.Label3_11.TabIndex = 97;
            this.Label3_11.Text = "CONDITION";
            // 
            // Label3_10
            // 
            this.Label3_10.Location = new System.Drawing.Point(20, 331);
            this.Label3_10.Name = "Label3_10";
            this.Label3_10.Size = new System.Drawing.Size(110, 14);
            this.Label3_10.TabIndex = 96;
            this.Label3_10.Text = "YEAR REMODELED";
            // 
            // Label3_9
            // 
            this.Label3_9.Location = new System.Drawing.Point(20, 302);
            this.Label3_9.Name = "Label3_9";
            this.Label3_9.Size = new System.Drawing.Size(97, 14);
            this.Label3_9.TabIndex = 95;
            this.Label3_9.Text = "YEAR BUILT";
            // 
            // Label3_8
            // 
            this.Label3_8.Location = new System.Drawing.Point(20, 272);
            this.Label3_8.Name = "Label3_8";
            this.Label3_8.Size = new System.Drawing.Size(120, 16);
            this.Label3_8.TabIndex = 94;
            this.Label3_8.Text = "HEATING / COOLING";
            // 
            // Label3_7
            // 
            this.Label3_7.Location = new System.Drawing.Point(20, 244);
            this.Label3_7.Name = "Label3_7";
            this.Label3_7.Size = new System.Drawing.Size(110, 14);
            this.Label3_7.TabIndex = 93;
            this.Label3_7.Text = "PERIMETER / UNITS";
            // 
            // Label3_6
            // 
            this.Label3_6.Location = new System.Drawing.Point(20, 216);
            this.Label3_6.Name = "Label3_6";
            this.Label3_6.Size = new System.Drawing.Size(110, 14);
            this.Label3_6.TabIndex = 92;
            this.Label3_6.Text = "BASE FLOOR AREA";
            // 
            // Label3_5
            // 
            this.Label3_5.Location = new System.Drawing.Point(20, 187);
            this.Label3_5.Name = "Label3_5";
            this.Label3_5.Size = new System.Drawing.Size(110, 16);
            this.Label3_5.TabIndex = 91;
            this.Label3_5.Text = "STORIES / HEIGHT";
            // 
            // Label3_4
            // 
            this.Label3_4.Location = new System.Drawing.Point(20, 160);
            this.Label3_4.Name = "Label3_4";
            this.Label3_4.Size = new System.Drawing.Size(110, 14);
            this.Label3_4.TabIndex = 90;
            this.Label3_4.Text = "EXTERIOR WALLS";
            // 
            // Label3_3
            // 
            this.Label3_3.Location = new System.Drawing.Point(20, 133);
            this.Label3_3.Name = "Label3_3";
            this.Label3_3.Size = new System.Drawing.Size(97, 14);
            this.Label3_3.TabIndex = 89;
            this.Label3_3.Text = "GRADE FACTOR";
            // 
            // Label3_2
            // 
            this.Label3_2.Location = new System.Drawing.Point(20, 105);
            this.Label3_2.Name = "Label3_2";
            this.Label3_2.Size = new System.Drawing.Size(105, 14);
            this.Label3_2.TabIndex = 88;
            this.Label3_2.Text = "CLASS & QUALITY";
            // 
            // Label3_1
            // 
            this.Label3_1.Location = new System.Drawing.Point(20, 78);
            this.Label3_1.Name = "Label3_1";
            this.Label3_1.Size = new System.Drawing.Size(130, 14);
            this.Label3_1.TabIndex = 87;
            this.Label3_1.Text = "# OF DWELLING UNITS";
            // 
            // Label3_32
            // 
            this.Label3_32.Location = new System.Drawing.Point(20, 50);
            this.Label3_32.Name = "Label3_32";
            this.Label3_32.Size = new System.Drawing.Size(115, 14);
            this.Label3_32.TabIndex = 86;
            this.Label3_32.Text = "OCCUPANCY CODE";
            // 
            // lblcomm
            // 
            this.lblcomm.Location = new System.Drawing.Point(20, 20);
            this.lblcomm.Name = "lblcomm";
            this.lblcomm.Size = new System.Drawing.Size(80, 18);
            this.lblcomm.TabIndex = 84;
            this.lblcomm.Text = "VIEW ONLY";
            this.lblcomm.Visible = false;
            // 
            // Label6_0
            // 
            this.Label6_0.Location = new System.Drawing.Point(15, 41);
            this.Label6_0.Name = "Label6_0";
            this.Label6_0.Size = new System.Drawing.Size(202, 14);
            this.Label6_0.TabIndex = 307;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(224, 41);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(194, 14);
            this.Label5.TabIndex = 306;
            // 
            // Label4_0
            // 
            this.Label4_0.Location = new System.Drawing.Point(436, 41);
            this.Label4_0.Name = "Label4_0";
            this.Label4_0.Size = new System.Drawing.Size(104, 14);
            this.Label4_0.TabIndex = 305;
            // 
            // SSTab1_Page5
            // 
            this.SSTab1_Page5.Controls.Add(this.Frame2);
            this.SSTab1_Page5.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page5.Name = "SSTab1_Page5";
            this.SSTab1_Page5.Size = new System.Drawing.Size(948, 850);
            this.SSTab1_Page5.Text = "Outbuilding";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtMROISoundValue_9);
            this.Frame2.Controls.Add(this.chkSound_9);
            this.Frame2.Controls.Add(this.txtMROISoundValue_8);
            this.Frame2.Controls.Add(this.chkSound_8);
            this.Frame2.Controls.Add(this.txtMROISoundValue_7);
            this.Frame2.Controls.Add(this.chkSound_7);
            this.Frame2.Controls.Add(this.txtMROISoundValue_6);
            this.Frame2.Controls.Add(this.chkSound_6);
            this.Frame2.Controls.Add(this.txtMROISoundValue_5);
            this.Frame2.Controls.Add(this.chkSound_5);
            this.Frame2.Controls.Add(this.txtMROISoundValue_4);
            this.Frame2.Controls.Add(this.chkSound_4);
            this.Frame2.Controls.Add(this.txtMROISoundValue_3);
            this.Frame2.Controls.Add(this.chkSound_3);
            this.Frame2.Controls.Add(this.txtMROISoundValue_2);
            this.Frame2.Controls.Add(this.chkSound_2);
            this.Frame2.Controls.Add(this.txtMROISoundValue_1);
            this.Frame2.Controls.Add(this.chkSound_1);
            this.Frame2.Controls.Add(this.txtMROISoundValue_0);
            this.Frame2.Controls.Add(this.chkSound_0);
            this.Frame2.Controls.Add(this.txtMROIPctFunct1_3);
            this.Frame2.Controls.Add(this.txtMROIUnits1_3);
            this.Frame2.Controls.Add(this.txtMROIGradePct1_8);
            this.Frame2.Controls.Add(this.txtMROIUnits1_6);
            this.Frame2.Controls.Add(this.txtMROIGradeCd1_9);
            this.Frame2.Controls.Add(this.txtMROIPctFunct1_1);
            this.Frame2.Controls.Add(this.txtMROIPctFunct1_2);
            this.Frame2.Controls.Add(this.txtMROIPctFunct1_4);
            this.Frame2.Controls.Add(this.txtMROIPctFunct1_5);
            this.Frame2.Controls.Add(this.txtMROIPctFunct1_6);
            this.Frame2.Controls.Add(this.txtMROIPctFunct1_7);
            this.Frame2.Controls.Add(this.txtMROIPctFunct1_8);
            this.Frame2.Controls.Add(this.txtMROIPctFunct1_9);
            this.Frame2.Controls.Add(this.txtMROICond1_1);
            this.Frame2.Controls.Add(this.txtMROICond1_2);
            this.Frame2.Controls.Add(this.txtMROICond1_3);
            this.Frame2.Controls.Add(this.txtMROICond1_4);
            this.Frame2.Controls.Add(this.txtMROICond1_5);
            this.Frame2.Controls.Add(this.txtMROICond1_6);
            this.Frame2.Controls.Add(this.txtMROICond1_7);
            this.Frame2.Controls.Add(this.txtMROICond1_8);
            this.Frame2.Controls.Add(this.txtMROICond1_9);
            this.Frame2.Controls.Add(this.txtMROIGradePct1_1);
            this.Frame2.Controls.Add(this.txtMROIGradePct1_2);
            this.Frame2.Controls.Add(this.txtMROIGradePct1_3);
            this.Frame2.Controls.Add(this.txtMROIGradePct1_4);
            this.Frame2.Controls.Add(this.txtMROIGradePct1_5);
            this.Frame2.Controls.Add(this.txtMROIGradePct1_6);
            this.Frame2.Controls.Add(this.txtMROIGradePct1_7);
            this.Frame2.Controls.Add(this.txtMROIGradePct1_9);
            this.Frame2.Controls.Add(this.txtMROIGradeCd1_0);
            this.Frame2.Controls.Add(this.txtMROIGradeCd1_1);
            this.Frame2.Controls.Add(this.txtMROIGradeCd1_2);
            this.Frame2.Controls.Add(this.txtMROIGradeCd1_3);
            this.Frame2.Controls.Add(this.txtMROIGradeCd1_4);
            this.Frame2.Controls.Add(this.txtMROIGradeCd1_5);
            this.Frame2.Controls.Add(this.txtMROIGradeCd1_6);
            this.Frame2.Controls.Add(this.txtMROIGradeCd1_7);
            this.Frame2.Controls.Add(this.txtMROIGradeCd1_8);
            this.Frame2.Controls.Add(this.txtMROIUnits1_1);
            this.Frame2.Controls.Add(this.txtMROIUnits1_2);
            this.Frame2.Controls.Add(this.txtMROIUnits1_4);
            this.Frame2.Controls.Add(this.txtMROIUnits1_5);
            this.Frame2.Controls.Add(this.txtMROIUnits1_7);
            this.Frame2.Controls.Add(this.txtMROIUnits1_8);
            this.Frame2.Controls.Add(this.txtMROIUnits1_9);
            this.Frame2.Controls.Add(this.txtMROIYear1_1);
            this.Frame2.Controls.Add(this.txtMROIYear1_2);
            this.Frame2.Controls.Add(this.txtMROIYear1_3);
            this.Frame2.Controls.Add(this.txtMROIYear1_4);
            this.Frame2.Controls.Add(this.txtMROIYear1_5);
            this.Frame2.Controls.Add(this.txtMROIYear1_6);
            this.Frame2.Controls.Add(this.txtMROIYear1_7);
            this.Frame2.Controls.Add(this.txtMROIYear1_8);
            this.Frame2.Controls.Add(this.txtMROIYear1_9);
            this.Frame2.Controls.Add(this.txtMROIType1_1);
            this.Frame2.Controls.Add(this.txtMROIType1_2);
            this.Frame2.Controls.Add(this.txtMROIType1_3);
            this.Frame2.Controls.Add(this.txtMROIType1_4);
            this.Frame2.Controls.Add(this.txtMROIType1_5);
            this.Frame2.Controls.Add(this.txtMROIType1_6);
            this.Frame2.Controls.Add(this.txtMROIType1_7);
            this.Frame2.Controls.Add(this.txtMROIType1_8);
            this.Frame2.Controls.Add(this.txtMROIType1_9);
            this.Frame2.Controls.Add(this.txtMROIUnits1_0);
            this.Frame2.Controls.Add(this.txtMROIYear1_0);
            this.Frame2.Controls.Add(this.txtMROIType1_0);
            this.Frame2.Controls.Add(this.txtMROIGradePct1_0);
            this.Frame2.Controls.Add(this.txtMROICond1_0);
            this.Frame2.Controls.Add(this.txtMROIPctFunct1_0);
            this.Frame2.Controls.Add(this.txtMROIPctPhys1_0);
            this.Frame2.Controls.Add(this.txtMROIPctPhys1_9);
            this.Frame2.Controls.Add(this.txtMROIPctPhys1_8);
            this.Frame2.Controls.Add(this.txtMROIPctPhys1_7);
            this.Frame2.Controls.Add(this.txtMROIPctPhys1_6);
            this.Frame2.Controls.Add(this.txtMROIPctPhys1_5);
            this.Frame2.Controls.Add(this.txtMROIPctPhys1_4);
            this.Frame2.Controls.Add(this.txtMROIPctPhys1_3);
            this.Frame2.Controls.Add(this.txtMROIPctPhys1_2);
            this.Frame2.Controls.Add(this.txtMROIPctPhys1_1);
            this.Frame2.Controls.Add(this.Label2_1);
            this.Frame2.Controls.Add(this.lblMapLotCopy_2);
            this.Frame2.Controls.Add(this.Label2_8);
            this.Frame2.Controls.Add(this.Label2_7);
            this.Frame2.Controls.Add(this.Label2_6);
            this.Frame2.Controls.Add(this.Label2_5);
            this.Frame2.Controls.Add(this.Label2_4);
            this.Frame2.Controls.Add(this.Label2_3);
            this.Frame2.Controls.Add(this.Label2_2);
            this.Frame2.Controls.Add(this.Label2_0);
            this.Frame2.Controls.Add(this.lblOut);
            this.Frame2.Dock = Wisej.Web.DockStyle.Fill;
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(948, 850);
            this.Frame2.TabIndex = 115;
            // 
            // txtMROISoundValue_9
            // 
            this.txtMROISoundValue_9.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROISoundValue_9.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROISoundValue_9.Location = new System.Drawing.Point(632, 341);
            this.txtMROISoundValue_9.MaxLength = 9;
            this.txtMROISoundValue_9.Name = "txtMROISoundValue_9";
            this.txtMROISoundValue_9.Size = new System.Drawing.Size(115, 20);
            this.txtMROISoundValue_9.TabIndex = 215;
            this.txtMROISoundValue_9.Tag = "outbuilding";
            this.txtMROISoundValue_9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROISoundValue_9.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
            this.txtMROISoundValue_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
            this.txtMROISoundValue_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROISoundValue_KeyPress);
            // 
            // chkSound_9
            // 
            this.chkSound_9.Location = new System.Drawing.Point(595, 339);
            this.chkSound_9.Name = "chkSound_9";
            this.chkSound_9.Size = new System.Drawing.Size(32, 22);
            this.chkSound_9.TabIndex = 214;
            // 
            // txtMROISoundValue_8
            // 
            this.txtMROISoundValue_8.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROISoundValue_8.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROISoundValue_8.Location = new System.Drawing.Point(632, 313);
            this.txtMROISoundValue_8.MaxLength = 9;
            this.txtMROISoundValue_8.Name = "txtMROISoundValue_8";
            this.txtMROISoundValue_8.Size = new System.Drawing.Size(115, 20);
            this.txtMROISoundValue_8.TabIndex = 205;
            this.txtMROISoundValue_8.Tag = "outbuilding";
            this.txtMROISoundValue_8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROISoundValue_8.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
            this.txtMROISoundValue_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
            this.txtMROISoundValue_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROISoundValue_KeyPress);
            // 
            // chkSound_8
            // 
            this.chkSound_8.Location = new System.Drawing.Point(595, 311);
            this.chkSound_8.Name = "chkSound_8";
            this.chkSound_8.Size = new System.Drawing.Size(32, 22);
            this.chkSound_8.TabIndex = 204;
            // 
            // txtMROISoundValue_7
            // 
            this.txtMROISoundValue_7.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROISoundValue_7.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROISoundValue_7.Location = new System.Drawing.Point(632, 285);
            this.txtMROISoundValue_7.MaxLength = 9;
            this.txtMROISoundValue_7.Name = "txtMROISoundValue_7";
            this.txtMROISoundValue_7.Size = new System.Drawing.Size(115, 20);
            this.txtMROISoundValue_7.TabIndex = 195;
            this.txtMROISoundValue_7.Tag = "outbuilding";
            this.txtMROISoundValue_7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROISoundValue_7.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
            this.txtMROISoundValue_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
            this.txtMROISoundValue_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROISoundValue_KeyPress);
            // 
            // chkSound_7
            // 
            this.chkSound_7.Location = new System.Drawing.Point(595, 283);
            this.chkSound_7.Name = "chkSound_7";
            this.chkSound_7.Size = new System.Drawing.Size(32, 22);
            this.chkSound_7.TabIndex = 194;
            // 
            // txtMROISoundValue_6
            // 
            this.txtMROISoundValue_6.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROISoundValue_6.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROISoundValue_6.Location = new System.Drawing.Point(632, 257);
            this.txtMROISoundValue_6.MaxLength = 9;
            this.txtMROISoundValue_6.Name = "txtMROISoundValue_6";
            this.txtMROISoundValue_6.Size = new System.Drawing.Size(115, 20);
            this.txtMROISoundValue_6.TabIndex = 185;
            this.txtMROISoundValue_6.Tag = "outbuilding";
            this.txtMROISoundValue_6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROISoundValue_6.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
            this.txtMROISoundValue_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
            this.txtMROISoundValue_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROISoundValue_KeyPress);
            // 
            // chkSound_6
            // 
            this.chkSound_6.Location = new System.Drawing.Point(595, 255);
            this.chkSound_6.Name = "chkSound_6";
            this.chkSound_6.Size = new System.Drawing.Size(32, 22);
            this.chkSound_6.TabIndex = 184;
            // 
            // txtMROISoundValue_5
            // 
            this.txtMROISoundValue_5.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROISoundValue_5.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROISoundValue_5.Location = new System.Drawing.Point(632, 229);
            this.txtMROISoundValue_5.MaxLength = 9;
            this.txtMROISoundValue_5.Name = "txtMROISoundValue_5";
            this.txtMROISoundValue_5.Size = new System.Drawing.Size(115, 20);
            this.txtMROISoundValue_5.TabIndex = 175;
            this.txtMROISoundValue_5.Tag = "outbuilding";
            this.txtMROISoundValue_5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROISoundValue_5.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
            this.txtMROISoundValue_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
            this.txtMROISoundValue_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROISoundValue_KeyPress);
            // 
            // chkSound_5
            // 
            this.chkSound_5.Location = new System.Drawing.Point(595, 227);
            this.chkSound_5.Name = "chkSound_5";
            this.chkSound_5.Size = new System.Drawing.Size(32, 22);
            this.chkSound_5.TabIndex = 174;
            // 
            // txtMROISoundValue_4
            // 
            this.txtMROISoundValue_4.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROISoundValue_4.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROISoundValue_4.Location = new System.Drawing.Point(632, 201);
            this.txtMROISoundValue_4.MaxLength = 9;
            this.txtMROISoundValue_4.Name = "txtMROISoundValue_4";
            this.txtMROISoundValue_4.Size = new System.Drawing.Size(115, 20);
            this.txtMROISoundValue_4.TabIndex = 165;
            this.txtMROISoundValue_4.Tag = "outbuilding";
            this.txtMROISoundValue_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROISoundValue_4.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
            this.txtMROISoundValue_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
            this.txtMROISoundValue_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROISoundValue_KeyPress);
            // 
            // chkSound_4
            // 
            this.chkSound_4.Location = new System.Drawing.Point(595, 199);
            this.chkSound_4.Name = "chkSound_4";
            this.chkSound_4.Size = new System.Drawing.Size(32, 22);
            this.chkSound_4.TabIndex = 164;
            // 
            // txtMROISoundValue_3
            // 
            this.txtMROISoundValue_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROISoundValue_3.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROISoundValue_3.Location = new System.Drawing.Point(632, 173);
            this.txtMROISoundValue_3.MaxLength = 9;
            this.txtMROISoundValue_3.Name = "txtMROISoundValue_3";
            this.txtMROISoundValue_3.Size = new System.Drawing.Size(115, 20);
            this.txtMROISoundValue_3.TabIndex = 155;
            this.txtMROISoundValue_3.Tag = "outbuilding";
            this.txtMROISoundValue_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROISoundValue_3.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
            this.txtMROISoundValue_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
            this.txtMROISoundValue_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROISoundValue_KeyPress);
            // 
            // chkSound_3
            // 
            this.chkSound_3.Location = new System.Drawing.Point(595, 171);
            this.chkSound_3.Name = "chkSound_3";
            this.chkSound_3.Size = new System.Drawing.Size(32, 22);
            this.chkSound_3.TabIndex = 154;
            // 
            // txtMROISoundValue_2
            // 
            this.txtMROISoundValue_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROISoundValue_2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROISoundValue_2.Location = new System.Drawing.Point(632, 145);
            this.txtMROISoundValue_2.MaxLength = 9;
            this.txtMROISoundValue_2.Name = "txtMROISoundValue_2";
            this.txtMROISoundValue_2.Size = new System.Drawing.Size(115, 20);
            this.txtMROISoundValue_2.TabIndex = 145;
            this.txtMROISoundValue_2.Tag = "outbuilding";
            this.txtMROISoundValue_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROISoundValue_2.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
            this.txtMROISoundValue_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
            this.txtMROISoundValue_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROISoundValue_KeyPress);
            // 
            // chkSound_2
            // 
            this.chkSound_2.Location = new System.Drawing.Point(595, 143);
            this.chkSound_2.Name = "chkSound_2";
            this.chkSound_2.Size = new System.Drawing.Size(32, 22);
            this.chkSound_2.TabIndex = 144;
            // 
            // txtMROISoundValue_1
            // 
            this.txtMROISoundValue_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROISoundValue_1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROISoundValue_1.Location = new System.Drawing.Point(632, 117);
            this.txtMROISoundValue_1.MaxLength = 9;
            this.txtMROISoundValue_1.Name = "txtMROISoundValue_1";
            this.txtMROISoundValue_1.Size = new System.Drawing.Size(115, 20);
            this.txtMROISoundValue_1.TabIndex = 135;
            this.txtMROISoundValue_1.Tag = "outbuilding";
            this.txtMROISoundValue_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROISoundValue_1.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
            this.txtMROISoundValue_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
            this.txtMROISoundValue_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROISoundValue_KeyPress);
            // 
            // chkSound_1
            // 
            this.chkSound_1.Location = new System.Drawing.Point(595, 115);
            this.chkSound_1.Name = "chkSound_1";
            this.chkSound_1.Size = new System.Drawing.Size(32, 22);
            this.chkSound_1.TabIndex = 134;
            // 
            // txtMROISoundValue_0
            // 
            this.txtMROISoundValue_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROISoundValue_0.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROISoundValue_0.Location = new System.Drawing.Point(632, 89);
            this.txtMROISoundValue_0.MaxLength = 9;
            this.txtMROISoundValue_0.Name = "txtMROISoundValue_0";
            this.txtMROISoundValue_0.Size = new System.Drawing.Size(115, 20);
            this.txtMROISoundValue_0.TabIndex = 125;
            this.txtMROISoundValue_0.Tag = "outbuilding";
            this.txtMROISoundValue_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROISoundValue_0.Leave += new System.EventHandler(this.txtMROISoundValue_Leave);
            this.txtMROISoundValue_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROISoundValue_Validating);
            this.txtMROISoundValue_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROISoundValue_KeyPress);
            // 
            // chkSound_0
            // 
            this.chkSound_0.Location = new System.Drawing.Point(595, 87);
            this.chkSound_0.Name = "chkSound_0";
            this.chkSound_0.Size = new System.Drawing.Size(32, 22);
            this.chkSound_0.TabIndex = 124;
            // 
            // txtMROIPctFunct1_3
            // 
            this.txtMROIPctFunct1_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctFunct1_3.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctFunct1_3.Location = new System.Drawing.Point(502, 173);
            this.txtMROIPctFunct1_3.MaxLength = 3;
            this.txtMROIPctFunct1_3.Name = "txtMROIPctFunct1_3";
            this.txtMROIPctFunct1_3.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctFunct1_3.TabIndex = 153;
            this.txtMROIPctFunct1_3.Tag = "outbuilding";
            this.txtMROIPctFunct1_3.Text = "000";
            this.txtMROIPctFunct1_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctFunct1_3.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
            this.txtMROIPctFunct1_3.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
            this.txtMROIPctFunct1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
            this.txtMROIPctFunct1_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctFunct1_KeyPress);
            this.txtMROIPctFunct1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
            // 
            // txtMROIUnits1_3
            // 
            this.txtMROIUnits1_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIUnits1_3.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIUnits1_3.Location = new System.Drawing.Point(175, 173);
            this.txtMROIUnits1_3.MaxLength = 7;
            this.txtMROIUnits1_3.Name = "txtMROIUnits1_3";
            this.txtMROIUnits1_3.Size = new System.Drawing.Size(65, 20);
            this.txtMROIUnits1_3.TabIndex = 148;
            this.txtMROIUnits1_3.Tag = "outbuilding";
            this.txtMROIUnits1_3.Text = "0000";
            this.txtMROIUnits1_3.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
            this.txtMROIUnits1_3.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
            this.txtMROIUnits1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
            this.txtMROIUnits1_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIUnits1_KeyPress);
            this.txtMROIUnits1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
            // 
            // txtMROIGradePct1_8
            // 
            this.txtMROIGradePct1_8.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradePct1_8.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradePct1_8.Location = new System.Drawing.Point(293, 313);
            this.txtMROIGradePct1_8.MaxLength = 3;
            this.txtMROIGradePct1_8.Name = "txtMROIGradePct1_8";
            this.txtMROIGradePct1_8.Size = new System.Drawing.Size(60, 20);
            this.txtMROIGradePct1_8.TabIndex = 200;
            this.txtMROIGradePct1_8.Tag = "outbuilding";
            this.txtMROIGradePct1_8.Text = "000";
            this.txtMROIGradePct1_8.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradePct1_8.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
            this.txtMROIGradePct1_8.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
            this.txtMROIGradePct1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
            this.txtMROIGradePct1_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradePct1_KeyPress);
            this.txtMROIGradePct1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
            // 
            // txtMROIUnits1_6
            // 
            this.txtMROIUnits1_6.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIUnits1_6.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIUnits1_6.Location = new System.Drawing.Point(175, 257);
            this.txtMROIUnits1_6.MaxLength = 7;
            this.txtMROIUnits1_6.Name = "txtMROIUnits1_6";
            this.txtMROIUnits1_6.Size = new System.Drawing.Size(65, 20);
            this.txtMROIUnits1_6.TabIndex = 178;
            this.txtMROIUnits1_6.Tag = "outbuilding";
            this.txtMROIUnits1_6.Text = "0000";
            this.txtMROIUnits1_6.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
            this.txtMROIUnits1_6.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
            this.txtMROIUnits1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
            this.txtMROIUnits1_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIUnits1_KeyPress);
            this.txtMROIUnits1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
            // 
            // txtMROIGradeCd1_9
            // 
            this.txtMROIGradeCd1_9.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradeCd1_9.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradeCd1_9.Location = new System.Drawing.Point(243, 341);
            this.txtMROIGradeCd1_9.MaxLength = 1;
            this.txtMROIGradeCd1_9.Name = "txtMROIGradeCd1_9";
            this.txtMROIGradeCd1_9.Size = new System.Drawing.Size(40, 20);
            this.txtMROIGradeCd1_9.TabIndex = 209;
            this.txtMROIGradeCd1_9.Tag = "outbuilding";
            this.txtMROIGradeCd1_9.Text = "0";
            this.txtMROIGradeCd1_9.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradeCd1_9.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
            this.txtMROIGradeCd1_9.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
            this.txtMROIGradeCd1_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradeCd1_KeyPress);
            this.txtMROIGradeCd1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
            // 
            // txtMROIPctFunct1_1
            // 
            this.txtMROIPctFunct1_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctFunct1_1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctFunct1_1.Location = new System.Drawing.Point(502, 117);
            this.txtMROIPctFunct1_1.MaxLength = 3;
            this.txtMROIPctFunct1_1.Name = "txtMROIPctFunct1_1";
            this.txtMROIPctFunct1_1.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctFunct1_1.TabIndex = 133;
            this.txtMROIPctFunct1_1.Tag = "outbuilding";
            this.txtMROIPctFunct1_1.Text = "000";
            this.txtMROIPctFunct1_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctFunct1_1.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
            this.txtMROIPctFunct1_1.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
            this.txtMROIPctFunct1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
            this.txtMROIPctFunct1_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctFunct1_KeyPress);
            this.txtMROIPctFunct1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
            // 
            // txtMROIPctFunct1_2
            // 
            this.txtMROIPctFunct1_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctFunct1_2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctFunct1_2.Location = new System.Drawing.Point(502, 145);
            this.txtMROIPctFunct1_2.MaxLength = 3;
            this.txtMROIPctFunct1_2.Name = "txtMROIPctFunct1_2";
            this.txtMROIPctFunct1_2.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctFunct1_2.TabIndex = 143;
            this.txtMROIPctFunct1_2.Tag = "outbuilding";
            this.txtMROIPctFunct1_2.Text = "000";
            this.txtMROIPctFunct1_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctFunct1_2.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
            this.txtMROIPctFunct1_2.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
            this.txtMROIPctFunct1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
            this.txtMROIPctFunct1_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctFunct1_KeyPress);
            this.txtMROIPctFunct1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
            // 
            // txtMROIPctFunct1_4
            // 
            this.txtMROIPctFunct1_4.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctFunct1_4.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctFunct1_4.Location = new System.Drawing.Point(502, 201);
            this.txtMROIPctFunct1_4.MaxLength = 3;
            this.txtMROIPctFunct1_4.Name = "txtMROIPctFunct1_4";
            this.txtMROIPctFunct1_4.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctFunct1_4.TabIndex = 163;
            this.txtMROIPctFunct1_4.Tag = "outbuilding";
            this.txtMROIPctFunct1_4.Text = "000";
            this.txtMROIPctFunct1_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctFunct1_4.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
            this.txtMROIPctFunct1_4.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
            this.txtMROIPctFunct1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
            this.txtMROIPctFunct1_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctFunct1_KeyPress);
            this.txtMROIPctFunct1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
            // 
            // txtMROIPctFunct1_5
            // 
            this.txtMROIPctFunct1_5.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctFunct1_5.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctFunct1_5.Location = new System.Drawing.Point(502, 229);
            this.txtMROIPctFunct1_5.MaxLength = 3;
            this.txtMROIPctFunct1_5.Name = "txtMROIPctFunct1_5";
            this.txtMROIPctFunct1_5.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctFunct1_5.TabIndex = 173;
            this.txtMROIPctFunct1_5.Tag = "outbuilding";
            this.txtMROIPctFunct1_5.Text = "000";
            this.txtMROIPctFunct1_5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctFunct1_5.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
            this.txtMROIPctFunct1_5.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
            this.txtMROIPctFunct1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
            this.txtMROIPctFunct1_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctFunct1_KeyPress);
            this.txtMROIPctFunct1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
            // 
            // txtMROIPctFunct1_6
            // 
            this.txtMROIPctFunct1_6.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctFunct1_6.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctFunct1_6.Location = new System.Drawing.Point(502, 257);
            this.txtMROIPctFunct1_6.MaxLength = 3;
            this.txtMROIPctFunct1_6.Name = "txtMROIPctFunct1_6";
            this.txtMROIPctFunct1_6.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctFunct1_6.TabIndex = 183;
            this.txtMROIPctFunct1_6.Tag = "outbuilding";
            this.txtMROIPctFunct1_6.Text = "000";
            this.txtMROIPctFunct1_6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctFunct1_6.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
            this.txtMROIPctFunct1_6.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
            this.txtMROIPctFunct1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
            this.txtMROIPctFunct1_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctFunct1_KeyPress);
            this.txtMROIPctFunct1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
            // 
            // txtMROIPctFunct1_7
            // 
            this.txtMROIPctFunct1_7.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctFunct1_7.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctFunct1_7.Location = new System.Drawing.Point(502, 285);
            this.txtMROIPctFunct1_7.MaxLength = 3;
            this.txtMROIPctFunct1_7.Name = "txtMROIPctFunct1_7";
            this.txtMROIPctFunct1_7.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctFunct1_7.TabIndex = 193;
            this.txtMROIPctFunct1_7.Tag = "outbuilding";
            this.txtMROIPctFunct1_7.Text = "000";
            this.txtMROIPctFunct1_7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctFunct1_7.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
            this.txtMROIPctFunct1_7.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
            this.txtMROIPctFunct1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
            this.txtMROIPctFunct1_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctFunct1_KeyPress);
            this.txtMROIPctFunct1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
            // 
            // txtMROIPctFunct1_8
            // 
            this.txtMROIPctFunct1_8.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctFunct1_8.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctFunct1_8.Location = new System.Drawing.Point(502, 313);
            this.txtMROIPctFunct1_8.MaxLength = 3;
            this.txtMROIPctFunct1_8.Name = "txtMROIPctFunct1_8";
            this.txtMROIPctFunct1_8.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctFunct1_8.TabIndex = 203;
            this.txtMROIPctFunct1_8.Tag = "outbuilding";
            this.txtMROIPctFunct1_8.Text = "000";
            this.txtMROIPctFunct1_8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctFunct1_8.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
            this.txtMROIPctFunct1_8.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
            this.txtMROIPctFunct1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
            this.txtMROIPctFunct1_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctFunct1_KeyPress);
            this.txtMROIPctFunct1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
            // 
            // txtMROIPctFunct1_9
            // 
            this.txtMROIPctFunct1_9.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctFunct1_9.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctFunct1_9.Location = new System.Drawing.Point(502, 341);
            this.txtMROIPctFunct1_9.MaxLength = 3;
            this.txtMROIPctFunct1_9.Name = "txtMROIPctFunct1_9";
            this.txtMROIPctFunct1_9.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctFunct1_9.TabIndex = 213;
            this.txtMROIPctFunct1_9.Tag = "outbuilding";
            this.txtMROIPctFunct1_9.Text = "000";
            this.txtMROIPctFunct1_9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctFunct1_9.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
            this.txtMROIPctFunct1_9.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
            this.txtMROIPctFunct1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
            this.txtMROIPctFunct1_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctFunct1_KeyPress);
            this.txtMROIPctFunct1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
            // 
            // txtMROICond1_1
            // 
            this.txtMROICond1_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROICond1_1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROICond1_1.Location = new System.Drawing.Point(356, 117);
            this.txtMROICond1_1.MaxLength = 1;
            this.txtMROICond1_1.Name = "txtMROICond1_1";
            this.txtMROICond1_1.Size = new System.Drawing.Size(40, 20);
            this.txtMROICond1_1.TabIndex = 131;
            this.txtMROICond1_1.Tag = "outbuilding";
            this.txtMROICond1_1.Text = "0";
            this.txtMROICond1_1.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROICond1_1.Enter += new System.EventHandler(this.txtMROICond1_Enter);
            this.txtMROICond1_1.Leave += new System.EventHandler(this.txtMROICond1_Leave);
            this.txtMROICond1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
            this.txtMROICond1_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROICond1_KeyPress);
            this.txtMROICond1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
            // 
            // txtMROICond1_2
            // 
            this.txtMROICond1_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROICond1_2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROICond1_2.Location = new System.Drawing.Point(356, 145);
            this.txtMROICond1_2.MaxLength = 1;
            this.txtMROICond1_2.Name = "txtMROICond1_2";
            this.txtMROICond1_2.Size = new System.Drawing.Size(40, 20);
            this.txtMROICond1_2.TabIndex = 141;
            this.txtMROICond1_2.Tag = "outbuilding";
            this.txtMROICond1_2.Text = "0";
            this.txtMROICond1_2.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROICond1_2.Enter += new System.EventHandler(this.txtMROICond1_Enter);
            this.txtMROICond1_2.Leave += new System.EventHandler(this.txtMROICond1_Leave);
            this.txtMROICond1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
            this.txtMROICond1_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROICond1_KeyPress);
            this.txtMROICond1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
            // 
            // txtMROICond1_3
            // 
            this.txtMROICond1_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROICond1_3.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROICond1_3.Location = new System.Drawing.Point(356, 173);
            this.txtMROICond1_3.MaxLength = 1;
            this.txtMROICond1_3.Name = "txtMROICond1_3";
            this.txtMROICond1_3.Size = new System.Drawing.Size(40, 20);
            this.txtMROICond1_3.TabIndex = 151;
            this.txtMROICond1_3.Tag = "outbuilding";
            this.txtMROICond1_3.Text = "0";
            this.txtMROICond1_3.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROICond1_3.Enter += new System.EventHandler(this.txtMROICond1_Enter);
            this.txtMROICond1_3.Leave += new System.EventHandler(this.txtMROICond1_Leave);
            this.txtMROICond1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
            this.txtMROICond1_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROICond1_KeyPress);
            this.txtMROICond1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
            // 
            // txtMROICond1_4
            // 
            this.txtMROICond1_4.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROICond1_4.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROICond1_4.Location = new System.Drawing.Point(356, 201);
            this.txtMROICond1_4.MaxLength = 1;
            this.txtMROICond1_4.Name = "txtMROICond1_4";
            this.txtMROICond1_4.Size = new System.Drawing.Size(40, 20);
            this.txtMROICond1_4.TabIndex = 161;
            this.txtMROICond1_4.Tag = "outbuilding";
            this.txtMROICond1_4.Text = "0";
            this.txtMROICond1_4.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROICond1_4.Enter += new System.EventHandler(this.txtMROICond1_Enter);
            this.txtMROICond1_4.Leave += new System.EventHandler(this.txtMROICond1_Leave);
            this.txtMROICond1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
            this.txtMROICond1_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROICond1_KeyPress);
            this.txtMROICond1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
            // 
            // txtMROICond1_5
            // 
            this.txtMROICond1_5.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROICond1_5.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROICond1_5.Location = new System.Drawing.Point(356, 229);
            this.txtMROICond1_5.MaxLength = 1;
            this.txtMROICond1_5.Name = "txtMROICond1_5";
            this.txtMROICond1_5.Size = new System.Drawing.Size(40, 20);
            this.txtMROICond1_5.TabIndex = 171;
            this.txtMROICond1_5.Tag = "outbuilding";
            this.txtMROICond1_5.Text = "0";
            this.txtMROICond1_5.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROICond1_5.Enter += new System.EventHandler(this.txtMROICond1_Enter);
            this.txtMROICond1_5.Leave += new System.EventHandler(this.txtMROICond1_Leave);
            this.txtMROICond1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
            this.txtMROICond1_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROICond1_KeyPress);
            this.txtMROICond1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
            // 
            // txtMROICond1_6
            // 
            this.txtMROICond1_6.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROICond1_6.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROICond1_6.Location = new System.Drawing.Point(356, 257);
            this.txtMROICond1_6.MaxLength = 1;
            this.txtMROICond1_6.Name = "txtMROICond1_6";
            this.txtMROICond1_6.Size = new System.Drawing.Size(40, 20);
            this.txtMROICond1_6.TabIndex = 181;
            this.txtMROICond1_6.Tag = "outbuilding";
            this.txtMROICond1_6.Text = "0";
            this.txtMROICond1_6.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROICond1_6.Enter += new System.EventHandler(this.txtMROICond1_Enter);
            this.txtMROICond1_6.Leave += new System.EventHandler(this.txtMROICond1_Leave);
            this.txtMROICond1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
            this.txtMROICond1_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROICond1_KeyPress);
            this.txtMROICond1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
            // 
            // txtMROICond1_7
            // 
            this.txtMROICond1_7.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROICond1_7.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROICond1_7.Location = new System.Drawing.Point(356, 285);
            this.txtMROICond1_7.MaxLength = 1;
            this.txtMROICond1_7.Name = "txtMROICond1_7";
            this.txtMROICond1_7.Size = new System.Drawing.Size(40, 20);
            this.txtMROICond1_7.TabIndex = 191;
            this.txtMROICond1_7.Tag = "outbuilding";
            this.txtMROICond1_7.Text = "0";
            this.txtMROICond1_7.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROICond1_7.Enter += new System.EventHandler(this.txtMROICond1_Enter);
            this.txtMROICond1_7.Leave += new System.EventHandler(this.txtMROICond1_Leave);
            this.txtMROICond1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
            this.txtMROICond1_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROICond1_KeyPress);
            this.txtMROICond1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
            // 
            // txtMROICond1_8
            // 
            this.txtMROICond1_8.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROICond1_8.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROICond1_8.Location = new System.Drawing.Point(356, 313);
            this.txtMROICond1_8.MaxLength = 1;
            this.txtMROICond1_8.Name = "txtMROICond1_8";
            this.txtMROICond1_8.Size = new System.Drawing.Size(40, 20);
            this.txtMROICond1_8.TabIndex = 201;
            this.txtMROICond1_8.Tag = "outbuilding";
            this.txtMROICond1_8.Text = "0";
            this.txtMROICond1_8.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROICond1_8.Enter += new System.EventHandler(this.txtMROICond1_Enter);
            this.txtMROICond1_8.Leave += new System.EventHandler(this.txtMROICond1_Leave);
            this.txtMROICond1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
            this.txtMROICond1_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROICond1_KeyPress);
            this.txtMROICond1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
            // 
            // txtMROICond1_9
            // 
            this.txtMROICond1_9.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROICond1_9.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROICond1_9.Location = new System.Drawing.Point(356, 341);
            this.txtMROICond1_9.MaxLength = 1;
            this.txtMROICond1_9.Name = "txtMROICond1_9";
            this.txtMROICond1_9.Size = new System.Drawing.Size(40, 20);
            this.txtMROICond1_9.TabIndex = 211;
            this.txtMROICond1_9.Tag = "outbuilding";
            this.txtMROICond1_9.Text = "0";
            this.txtMROICond1_9.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROICond1_9.Enter += new System.EventHandler(this.txtMROICond1_Enter);
            this.txtMROICond1_9.Leave += new System.EventHandler(this.txtMROICond1_Leave);
            this.txtMROICond1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
            this.txtMROICond1_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROICond1_KeyPress);
            this.txtMROICond1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
            // 
            // txtMROIGradePct1_1
            // 
            this.txtMROIGradePct1_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradePct1_1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradePct1_1.Location = new System.Drawing.Point(293, 117);
            this.txtMROIGradePct1_1.MaxLength = 3;
            this.txtMROIGradePct1_1.Name = "txtMROIGradePct1_1";
            this.txtMROIGradePct1_1.Size = new System.Drawing.Size(60, 20);
            this.txtMROIGradePct1_1.TabIndex = 130;
            this.txtMROIGradePct1_1.Tag = "outbuilding";
            this.txtMROIGradePct1_1.Text = "000";
            this.txtMROIGradePct1_1.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradePct1_1.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
            this.txtMROIGradePct1_1.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
            this.txtMROIGradePct1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
            this.txtMROIGradePct1_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradePct1_KeyPress);
            this.txtMROIGradePct1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
            // 
            // txtMROIGradePct1_2
            // 
            this.txtMROIGradePct1_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradePct1_2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradePct1_2.Location = new System.Drawing.Point(293, 145);
            this.txtMROIGradePct1_2.MaxLength = 3;
            this.txtMROIGradePct1_2.Name = "txtMROIGradePct1_2";
            this.txtMROIGradePct1_2.Size = new System.Drawing.Size(60, 20);
            this.txtMROIGradePct1_2.TabIndex = 140;
            this.txtMROIGradePct1_2.Tag = "outbuilding";
            this.txtMROIGradePct1_2.Text = "000";
            this.txtMROIGradePct1_2.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradePct1_2.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
            this.txtMROIGradePct1_2.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
            this.txtMROIGradePct1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
            this.txtMROIGradePct1_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradePct1_KeyPress);
            this.txtMROIGradePct1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
            // 
            // txtMROIGradePct1_3
            // 
            this.txtMROIGradePct1_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradePct1_3.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradePct1_3.Location = new System.Drawing.Point(293, 173);
            this.txtMROIGradePct1_3.MaxLength = 3;
            this.txtMROIGradePct1_3.Name = "txtMROIGradePct1_3";
            this.txtMROIGradePct1_3.Size = new System.Drawing.Size(60, 20);
            this.txtMROIGradePct1_3.TabIndex = 150;
            this.txtMROIGradePct1_3.Tag = "outbuilding";
            this.txtMROIGradePct1_3.Text = "000";
            this.txtMROIGradePct1_3.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradePct1_3.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
            this.txtMROIGradePct1_3.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
            this.txtMROIGradePct1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
            this.txtMROIGradePct1_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradePct1_KeyPress);
            this.txtMROIGradePct1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
            // 
            // txtMROIGradePct1_4
            // 
            this.txtMROIGradePct1_4.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradePct1_4.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradePct1_4.Location = new System.Drawing.Point(293, 201);
            this.txtMROIGradePct1_4.MaxLength = 3;
            this.txtMROIGradePct1_4.Name = "txtMROIGradePct1_4";
            this.txtMROIGradePct1_4.Size = new System.Drawing.Size(60, 20);
            this.txtMROIGradePct1_4.TabIndex = 160;
            this.txtMROIGradePct1_4.Tag = "outbuilding";
            this.txtMROIGradePct1_4.Text = "000";
            this.txtMROIGradePct1_4.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradePct1_4.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
            this.txtMROIGradePct1_4.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
            this.txtMROIGradePct1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
            this.txtMROIGradePct1_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradePct1_KeyPress);
            this.txtMROIGradePct1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
            // 
            // txtMROIGradePct1_5
            // 
            this.txtMROIGradePct1_5.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradePct1_5.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradePct1_5.Location = new System.Drawing.Point(293, 229);
            this.txtMROIGradePct1_5.MaxLength = 3;
            this.txtMROIGradePct1_5.Name = "txtMROIGradePct1_5";
            this.txtMROIGradePct1_5.Size = new System.Drawing.Size(60, 20);
            this.txtMROIGradePct1_5.TabIndex = 170;
            this.txtMROIGradePct1_5.Tag = "outbuilding";
            this.txtMROIGradePct1_5.Text = "000";
            this.txtMROIGradePct1_5.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradePct1_5.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
            this.txtMROIGradePct1_5.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
            this.txtMROIGradePct1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
            this.txtMROIGradePct1_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradePct1_KeyPress);
            this.txtMROIGradePct1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
            // 
            // txtMROIGradePct1_6
            // 
            this.txtMROIGradePct1_6.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradePct1_6.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradePct1_6.Location = new System.Drawing.Point(293, 257);
            this.txtMROIGradePct1_6.MaxLength = 3;
            this.txtMROIGradePct1_6.Name = "txtMROIGradePct1_6";
            this.txtMROIGradePct1_6.Size = new System.Drawing.Size(60, 20);
            this.txtMROIGradePct1_6.TabIndex = 180;
            this.txtMROIGradePct1_6.Tag = "outbuilding";
            this.txtMROIGradePct1_6.Text = "000";
            this.txtMROIGradePct1_6.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradePct1_6.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
            this.txtMROIGradePct1_6.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
            this.txtMROIGradePct1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
            this.txtMROIGradePct1_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradePct1_KeyPress);
            this.txtMROIGradePct1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
            // 
            // txtMROIGradePct1_7
            // 
            this.txtMROIGradePct1_7.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradePct1_7.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradePct1_7.Location = new System.Drawing.Point(293, 285);
            this.txtMROIGradePct1_7.MaxLength = 3;
            this.txtMROIGradePct1_7.Name = "txtMROIGradePct1_7";
            this.txtMROIGradePct1_7.Size = new System.Drawing.Size(60, 20);
            this.txtMROIGradePct1_7.TabIndex = 190;
            this.txtMROIGradePct1_7.Tag = "outbuilding";
            this.txtMROIGradePct1_7.Text = "000";
            this.txtMROIGradePct1_7.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradePct1_7.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
            this.txtMROIGradePct1_7.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
            this.txtMROIGradePct1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
            this.txtMROIGradePct1_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradePct1_KeyPress);
            this.txtMROIGradePct1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
            // 
            // txtMROIGradePct1_9
            // 
            this.txtMROIGradePct1_9.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradePct1_9.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradePct1_9.Location = new System.Drawing.Point(293, 341);
            this.txtMROIGradePct1_9.MaxLength = 3;
            this.txtMROIGradePct1_9.Name = "txtMROIGradePct1_9";
            this.txtMROIGradePct1_9.Size = new System.Drawing.Size(60, 20);
            this.txtMROIGradePct1_9.TabIndex = 210;
            this.txtMROIGradePct1_9.Tag = "outbuilding";
            this.txtMROIGradePct1_9.Text = "000";
            this.txtMROIGradePct1_9.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradePct1_9.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
            this.txtMROIGradePct1_9.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
            this.txtMROIGradePct1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
            this.txtMROIGradePct1_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradePct1_KeyPress);
            this.txtMROIGradePct1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
            // 
            // txtMROIGradeCd1_0
            // 
            this.txtMROIGradeCd1_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradeCd1_0.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradeCd1_0.Location = new System.Drawing.Point(243, 89);
            this.txtMROIGradeCd1_0.MaxLength = 1;
            this.txtMROIGradeCd1_0.Name = "txtMROIGradeCd1_0";
            this.txtMROIGradeCd1_0.Size = new System.Drawing.Size(40, 20);
            this.txtMROIGradeCd1_0.TabIndex = 119;
            this.txtMROIGradeCd1_0.Tag = "outbuilding";
            this.txtMROIGradeCd1_0.Text = "0";
            this.txtMROIGradeCd1_0.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradeCd1_0.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
            this.txtMROIGradeCd1_0.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
            this.txtMROIGradeCd1_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradeCd1_KeyPress);
            this.txtMROIGradeCd1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
            // 
            // txtMROIGradeCd1_1
            // 
            this.txtMROIGradeCd1_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradeCd1_1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradeCd1_1.Location = new System.Drawing.Point(243, 117);
            this.txtMROIGradeCd1_1.MaxLength = 1;
            this.txtMROIGradeCd1_1.Name = "txtMROIGradeCd1_1";
            this.txtMROIGradeCd1_1.Size = new System.Drawing.Size(40, 20);
            this.txtMROIGradeCd1_1.TabIndex = 129;
            this.txtMROIGradeCd1_1.Tag = "outbuilding";
            this.txtMROIGradeCd1_1.Text = "0";
            this.txtMROIGradeCd1_1.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradeCd1_1.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
            this.txtMROIGradeCd1_1.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
            this.txtMROIGradeCd1_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradeCd1_KeyPress);
            this.txtMROIGradeCd1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
            // 
            // txtMROIGradeCd1_2
            // 
            this.txtMROIGradeCd1_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradeCd1_2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradeCd1_2.Location = new System.Drawing.Point(243, 145);
            this.txtMROIGradeCd1_2.MaxLength = 1;
            this.txtMROIGradeCd1_2.Name = "txtMROIGradeCd1_2";
            this.txtMROIGradeCd1_2.Size = new System.Drawing.Size(40, 20);
            this.txtMROIGradeCd1_2.TabIndex = 139;
            this.txtMROIGradeCd1_2.Tag = "outbuilding";
            this.txtMROIGradeCd1_2.Text = "0";
            this.txtMROIGradeCd1_2.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradeCd1_2.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
            this.txtMROIGradeCd1_2.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
            this.txtMROIGradeCd1_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradeCd1_KeyPress);
            this.txtMROIGradeCd1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
            // 
            // txtMROIGradeCd1_3
            // 
            this.txtMROIGradeCd1_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradeCd1_3.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradeCd1_3.Location = new System.Drawing.Point(243, 173);
            this.txtMROIGradeCd1_3.MaxLength = 1;
            this.txtMROIGradeCd1_3.Name = "txtMROIGradeCd1_3";
            this.txtMROIGradeCd1_3.Size = new System.Drawing.Size(40, 20);
            this.txtMROIGradeCd1_3.TabIndex = 149;
            this.txtMROIGradeCd1_3.Tag = "outbuilding";
            this.txtMROIGradeCd1_3.Text = "0";
            this.txtMROIGradeCd1_3.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradeCd1_3.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
            this.txtMROIGradeCd1_3.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
            this.txtMROIGradeCd1_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradeCd1_KeyPress);
            this.txtMROIGradeCd1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
            // 
            // txtMROIGradeCd1_4
            // 
            this.txtMROIGradeCd1_4.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradeCd1_4.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradeCd1_4.Location = new System.Drawing.Point(243, 201);
            this.txtMROIGradeCd1_4.MaxLength = 1;
            this.txtMROIGradeCd1_4.Name = "txtMROIGradeCd1_4";
            this.txtMROIGradeCd1_4.Size = new System.Drawing.Size(40, 20);
            this.txtMROIGradeCd1_4.TabIndex = 159;
            this.txtMROIGradeCd1_4.Tag = "outbuilding";
            this.txtMROIGradeCd1_4.Text = "0";
            this.txtMROIGradeCd1_4.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradeCd1_4.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
            this.txtMROIGradeCd1_4.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
            this.txtMROIGradeCd1_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradeCd1_KeyPress);
            this.txtMROIGradeCd1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
            // 
            // txtMROIGradeCd1_5
            // 
            this.txtMROIGradeCd1_5.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradeCd1_5.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradeCd1_5.Location = new System.Drawing.Point(243, 229);
            this.txtMROIGradeCd1_5.MaxLength = 1;
            this.txtMROIGradeCd1_5.Name = "txtMROIGradeCd1_5";
            this.txtMROIGradeCd1_5.Size = new System.Drawing.Size(40, 20);
            this.txtMROIGradeCd1_5.TabIndex = 169;
            this.txtMROIGradeCd1_5.Tag = "outbuilding";
            this.txtMROIGradeCd1_5.Text = "0";
            this.txtMROIGradeCd1_5.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradeCd1_5.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
            this.txtMROIGradeCd1_5.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
            this.txtMROIGradeCd1_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradeCd1_KeyPress);
            this.txtMROIGradeCd1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
            // 
            // txtMROIGradeCd1_6
            // 
            this.txtMROIGradeCd1_6.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradeCd1_6.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradeCd1_6.Location = new System.Drawing.Point(243, 257);
            this.txtMROIGradeCd1_6.MaxLength = 1;
            this.txtMROIGradeCd1_6.Name = "txtMROIGradeCd1_6";
            this.txtMROIGradeCd1_6.Size = new System.Drawing.Size(40, 20);
            this.txtMROIGradeCd1_6.TabIndex = 179;
            this.txtMROIGradeCd1_6.Tag = "outbuilding";
            this.txtMROIGradeCd1_6.Text = "0";
            this.txtMROIGradeCd1_6.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradeCd1_6.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
            this.txtMROIGradeCd1_6.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
            this.txtMROIGradeCd1_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradeCd1_KeyPress);
            this.txtMROIGradeCd1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
            // 
            // txtMROIGradeCd1_7
            // 
            this.txtMROIGradeCd1_7.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradeCd1_7.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradeCd1_7.Location = new System.Drawing.Point(243, 285);
            this.txtMROIGradeCd1_7.MaxLength = 1;
            this.txtMROIGradeCd1_7.Name = "txtMROIGradeCd1_7";
            this.txtMROIGradeCd1_7.Size = new System.Drawing.Size(40, 20);
            this.txtMROIGradeCd1_7.TabIndex = 189;
            this.txtMROIGradeCd1_7.Tag = "outbuilding";
            this.txtMROIGradeCd1_7.Text = "0";
            this.txtMROIGradeCd1_7.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradeCd1_7.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
            this.txtMROIGradeCd1_7.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
            this.txtMROIGradeCd1_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradeCd1_KeyPress);
            this.txtMROIGradeCd1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
            // 
            // txtMROIGradeCd1_8
            // 
            this.txtMROIGradeCd1_8.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradeCd1_8.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradeCd1_8.Location = new System.Drawing.Point(243, 313);
            this.txtMROIGradeCd1_8.MaxLength = 1;
            this.txtMROIGradeCd1_8.Name = "txtMROIGradeCd1_8";
            this.txtMROIGradeCd1_8.Size = new System.Drawing.Size(40, 20);
            this.txtMROIGradeCd1_8.TabIndex = 199;
            this.txtMROIGradeCd1_8.Tag = "outbuilding";
            this.txtMROIGradeCd1_8.Text = "0";
            this.txtMROIGradeCd1_8.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradeCd1_8.Enter += new System.EventHandler(this.txtMROIGradeCd1_Enter);
            this.txtMROIGradeCd1_8.Leave += new System.EventHandler(this.txtMROIGradeCd1_Leave);
            this.txtMROIGradeCd1_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradeCd1_KeyPress);
            this.txtMROIGradeCd1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradeCd1_KeyUp);
            // 
            // txtMROIUnits1_1
            // 
            this.txtMROIUnits1_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIUnits1_1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIUnits1_1.Location = new System.Drawing.Point(175, 117);
            this.txtMROIUnits1_1.MaxLength = 7;
            this.txtMROIUnits1_1.Name = "txtMROIUnits1_1";
            this.txtMROIUnits1_1.Size = new System.Drawing.Size(65, 20);
            this.txtMROIUnits1_1.TabIndex = 128;
            this.txtMROIUnits1_1.Tag = "outbuilding";
            this.txtMROIUnits1_1.Text = "0000";
            this.txtMROIUnits1_1.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
            this.txtMROIUnits1_1.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
            this.txtMROIUnits1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
            this.txtMROIUnits1_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIUnits1_KeyPress);
            this.txtMROIUnits1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
            // 
            // txtMROIUnits1_2
            // 
            this.txtMROIUnits1_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIUnits1_2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIUnits1_2.Location = new System.Drawing.Point(175, 145);
            this.txtMROIUnits1_2.MaxLength = 7;
            this.txtMROIUnits1_2.Name = "txtMROIUnits1_2";
            this.txtMROIUnits1_2.Size = new System.Drawing.Size(65, 20);
            this.txtMROIUnits1_2.TabIndex = 138;
            this.txtMROIUnits1_2.Tag = "outbuilding";
            this.txtMROIUnits1_2.Text = "0000";
            this.txtMROIUnits1_2.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
            this.txtMROIUnits1_2.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
            this.txtMROIUnits1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
            this.txtMROIUnits1_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIUnits1_KeyPress);
            this.txtMROIUnits1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
            // 
            // txtMROIUnits1_4
            // 
            this.txtMROIUnits1_4.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIUnits1_4.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIUnits1_4.Location = new System.Drawing.Point(175, 201);
            this.txtMROIUnits1_4.MaxLength = 7;
            this.txtMROIUnits1_4.Name = "txtMROIUnits1_4";
            this.txtMROIUnits1_4.Size = new System.Drawing.Size(65, 20);
            this.txtMROIUnits1_4.TabIndex = 158;
            this.txtMROIUnits1_4.Tag = "outbuilding";
            this.txtMROIUnits1_4.Text = "0000";
            this.txtMROIUnits1_4.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
            this.txtMROIUnits1_4.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
            this.txtMROIUnits1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
            this.txtMROIUnits1_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIUnits1_KeyPress);
            this.txtMROIUnits1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
            // 
            // txtMROIUnits1_5
            // 
            this.txtMROIUnits1_5.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIUnits1_5.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIUnits1_5.Location = new System.Drawing.Point(175, 229);
            this.txtMROIUnits1_5.MaxLength = 7;
            this.txtMROIUnits1_5.Name = "txtMROIUnits1_5";
            this.txtMROIUnits1_5.Size = new System.Drawing.Size(65, 20);
            this.txtMROIUnits1_5.TabIndex = 168;
            this.txtMROIUnits1_5.Tag = "outbuilding";
            this.txtMROIUnits1_5.Text = "0000";
            this.txtMROIUnits1_5.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
            this.txtMROIUnits1_5.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
            this.txtMROIUnits1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
            this.txtMROIUnits1_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIUnits1_KeyPress);
            this.txtMROIUnits1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
            // 
            // txtMROIUnits1_7
            // 
            this.txtMROIUnits1_7.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIUnits1_7.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIUnits1_7.Location = new System.Drawing.Point(175, 285);
            this.txtMROIUnits1_7.MaxLength = 7;
            this.txtMROIUnits1_7.Name = "txtMROIUnits1_7";
            this.txtMROIUnits1_7.Size = new System.Drawing.Size(65, 20);
            this.txtMROIUnits1_7.TabIndex = 188;
            this.txtMROIUnits1_7.Tag = "outbuilding";
            this.txtMROIUnits1_7.Text = "0000";
            this.txtMROIUnits1_7.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
            this.txtMROIUnits1_7.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
            this.txtMROIUnits1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
            this.txtMROIUnits1_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIUnits1_KeyPress);
            this.txtMROIUnits1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
            // 
            // txtMROIUnits1_8
            // 
            this.txtMROIUnits1_8.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIUnits1_8.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIUnits1_8.Location = new System.Drawing.Point(175, 313);
            this.txtMROIUnits1_8.MaxLength = 7;
            this.txtMROIUnits1_8.Name = "txtMROIUnits1_8";
            this.txtMROIUnits1_8.Size = new System.Drawing.Size(65, 20);
            this.txtMROIUnits1_8.TabIndex = 198;
            this.txtMROIUnits1_8.Tag = "outbuilding";
            this.txtMROIUnits1_8.Text = "0000";
            this.txtMROIUnits1_8.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
            this.txtMROIUnits1_8.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
            this.txtMROIUnits1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
            this.txtMROIUnits1_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIUnits1_KeyPress);
            this.txtMROIUnits1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
            // 
            // txtMROIUnits1_9
            // 
            this.txtMROIUnits1_9.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIUnits1_9.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIUnits1_9.Location = new System.Drawing.Point(175, 341);
            this.txtMROIUnits1_9.MaxLength = 6;
            this.txtMROIUnits1_9.Name = "txtMROIUnits1_9";
            this.txtMROIUnits1_9.Size = new System.Drawing.Size(65, 20);
            this.txtMROIUnits1_9.TabIndex = 208;
            this.txtMROIUnits1_9.Tag = "outbuilding";
            this.txtMROIUnits1_9.Text = "0000";
            this.txtMROIUnits1_9.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
            this.txtMROIUnits1_9.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
            this.txtMROIUnits1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
            this.txtMROIUnits1_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIUnits1_KeyPress);
            this.txtMROIUnits1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
            // 
            // txtMROIYear1_1
            // 
            this.txtMROIYear1_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIYear1_1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIYear1_1.Location = new System.Drawing.Point(100, 117);
            this.txtMROIYear1_1.MaxLength = 4;
            this.txtMROIYear1_1.Name = "txtMROIYear1_1";
            this.txtMROIYear1_1.Size = new System.Drawing.Size(65, 20);
            this.txtMROIYear1_1.TabIndex = 127;
            this.txtMROIYear1_1.Tag = "outbuilding";
            this.txtMROIYear1_1.Text = "0000";
            this.txtMROIYear1_1.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
            this.txtMROIYear1_1.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
            this.txtMROIYear1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
            this.txtMROIYear1_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIYear1_KeyPress);
            this.txtMROIYear1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
            // 
            // txtMROIYear1_2
            // 
            this.txtMROIYear1_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIYear1_2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIYear1_2.Location = new System.Drawing.Point(100, 145);
            this.txtMROIYear1_2.MaxLength = 4;
            this.txtMROIYear1_2.Name = "txtMROIYear1_2";
            this.txtMROIYear1_2.Size = new System.Drawing.Size(65, 20);
            this.txtMROIYear1_2.TabIndex = 137;
            this.txtMROIYear1_2.Tag = "outbuilding";
            this.txtMROIYear1_2.Text = "0000";
            this.txtMROIYear1_2.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
            this.txtMROIYear1_2.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
            this.txtMROIYear1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
            this.txtMROIYear1_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIYear1_KeyPress);
            this.txtMROIYear1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
            // 
            // txtMROIYear1_3
            // 
            this.txtMROIYear1_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIYear1_3.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIYear1_3.Location = new System.Drawing.Point(100, 173);
            this.txtMROIYear1_3.MaxLength = 4;
            this.txtMROIYear1_3.Name = "txtMROIYear1_3";
            this.txtMROIYear1_3.Size = new System.Drawing.Size(65, 20);
            this.txtMROIYear1_3.TabIndex = 147;
            this.txtMROIYear1_3.Tag = "outbuilding";
            this.txtMROIYear1_3.Text = "0000";
            this.txtMROIYear1_3.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
            this.txtMROIYear1_3.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
            this.txtMROIYear1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
            this.txtMROIYear1_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIYear1_KeyPress);
            this.txtMROIYear1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
            // 
            // txtMROIYear1_4
            // 
            this.txtMROIYear1_4.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIYear1_4.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIYear1_4.Location = new System.Drawing.Point(100, 201);
            this.txtMROIYear1_4.MaxLength = 4;
            this.txtMROIYear1_4.Name = "txtMROIYear1_4";
            this.txtMROIYear1_4.Size = new System.Drawing.Size(65, 20);
            this.txtMROIYear1_4.TabIndex = 157;
            this.txtMROIYear1_4.Tag = "outbuilding";
            this.txtMROIYear1_4.Text = "0000";
            this.txtMROIYear1_4.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
            this.txtMROIYear1_4.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
            this.txtMROIYear1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
            this.txtMROIYear1_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIYear1_KeyPress);
            this.txtMROIYear1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
            // 
            // txtMROIYear1_5
            // 
            this.txtMROIYear1_5.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIYear1_5.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIYear1_5.Location = new System.Drawing.Point(100, 229);
            this.txtMROIYear1_5.MaxLength = 4;
            this.txtMROIYear1_5.Name = "txtMROIYear1_5";
            this.txtMROIYear1_5.Size = new System.Drawing.Size(65, 20);
            this.txtMROIYear1_5.TabIndex = 167;
            this.txtMROIYear1_5.Tag = "outbuilding";
            this.txtMROIYear1_5.Text = "0000";
            this.txtMROIYear1_5.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
            this.txtMROIYear1_5.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
            this.txtMROIYear1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
            this.txtMROIYear1_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIYear1_KeyPress);
            this.txtMROIYear1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
            // 
            // txtMROIYear1_6
            // 
            this.txtMROIYear1_6.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIYear1_6.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIYear1_6.Location = new System.Drawing.Point(100, 257);
            this.txtMROIYear1_6.MaxLength = 4;
            this.txtMROIYear1_6.Name = "txtMROIYear1_6";
            this.txtMROIYear1_6.Size = new System.Drawing.Size(65, 20);
            this.txtMROIYear1_6.TabIndex = 177;
            this.txtMROIYear1_6.Tag = "outbuilding";
            this.txtMROIYear1_6.Text = "0000";
            this.txtMROIYear1_6.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
            this.txtMROIYear1_6.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
            this.txtMROIYear1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
            this.txtMROIYear1_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIYear1_KeyPress);
            this.txtMROIYear1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
            // 
            // txtMROIYear1_7
            // 
            this.txtMROIYear1_7.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIYear1_7.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIYear1_7.Location = new System.Drawing.Point(100, 285);
            this.txtMROIYear1_7.MaxLength = 4;
            this.txtMROIYear1_7.Name = "txtMROIYear1_7";
            this.txtMROIYear1_7.Size = new System.Drawing.Size(65, 20);
            this.txtMROIYear1_7.TabIndex = 187;
            this.txtMROIYear1_7.Tag = "outbuilding";
            this.txtMROIYear1_7.Text = "0000";
            this.txtMROIYear1_7.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
            this.txtMROIYear1_7.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
            this.txtMROIYear1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
            this.txtMROIYear1_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIYear1_KeyPress);
            this.txtMROIYear1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
            // 
            // txtMROIYear1_8
            // 
            this.txtMROIYear1_8.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIYear1_8.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIYear1_8.Location = new System.Drawing.Point(100, 313);
            this.txtMROIYear1_8.MaxLength = 4;
            this.txtMROIYear1_8.Name = "txtMROIYear1_8";
            this.txtMROIYear1_8.Size = new System.Drawing.Size(65, 20);
            this.txtMROIYear1_8.TabIndex = 197;
            this.txtMROIYear1_8.Tag = "outbuilding";
            this.txtMROIYear1_8.Text = "0000";
            this.txtMROIYear1_8.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
            this.txtMROIYear1_8.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
            this.txtMROIYear1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
            this.txtMROIYear1_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIYear1_KeyPress);
            this.txtMROIYear1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
            // 
            // txtMROIYear1_9
            // 
            this.txtMROIYear1_9.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIYear1_9.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIYear1_9.Location = new System.Drawing.Point(100, 341);
            this.txtMROIYear1_9.MaxLength = 4;
            this.txtMROIYear1_9.Name = "txtMROIYear1_9";
            this.txtMROIYear1_9.Size = new System.Drawing.Size(65, 20);
            this.txtMROIYear1_9.TabIndex = 207;
            this.txtMROIYear1_9.Tag = "outbuilding";
            this.txtMROIYear1_9.Text = "0000";
            this.txtMROIYear1_9.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
            this.txtMROIYear1_9.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
            this.txtMROIYear1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
            this.txtMROIYear1_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIYear1_KeyPress);
            this.txtMROIYear1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
            // 
            // txtMROIType1_1
            // 
            this.txtMROIType1_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIType1_1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIType1_1.Location = new System.Drawing.Point(20, 117);
            this.txtMROIType1_1.MaxLength = 3;
            this.txtMROIType1_1.Name = "txtMROIType1_1";
            this.txtMROIType1_1.Size = new System.Drawing.Size(60, 20);
            this.txtMROIType1_1.TabIndex = 126;
            this.txtMROIType1_1.Tag = "outbuilding";
            this.txtMROIType1_1.Text = "000";
            this.txtMROIType1_1.Enter += new System.EventHandler(this.txtMROIType1_Enter);
            this.txtMROIType1_1.Leave += new System.EventHandler(this.txtMROIType1_Leave);
            this.txtMROIType1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
            this.txtMROIType1_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
            this.txtMROIType1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
            // 
            // txtMROIType1_2
            // 
            this.txtMROIType1_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIType1_2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIType1_2.Location = new System.Drawing.Point(20, 145);
            this.txtMROIType1_2.MaxLength = 3;
            this.txtMROIType1_2.Name = "txtMROIType1_2";
            this.txtMROIType1_2.Size = new System.Drawing.Size(60, 20);
            this.txtMROIType1_2.TabIndex = 136;
            this.txtMROIType1_2.Tag = "outbuilding";
            this.txtMROIType1_2.Text = "000";
            this.txtMROIType1_2.Enter += new System.EventHandler(this.txtMROIType1_Enter);
            this.txtMROIType1_2.Leave += new System.EventHandler(this.txtMROIType1_Leave);
            this.txtMROIType1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
            this.txtMROIType1_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
            this.txtMROIType1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
            // 
            // txtMROIType1_3
            // 
            this.txtMROIType1_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIType1_3.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIType1_3.Location = new System.Drawing.Point(20, 173);
            this.txtMROIType1_3.MaxLength = 3;
            this.txtMROIType1_3.Name = "txtMROIType1_3";
            this.txtMROIType1_3.Size = new System.Drawing.Size(60, 20);
            this.txtMROIType1_3.TabIndex = 146;
            this.txtMROIType1_3.Tag = "outbuilding";
            this.txtMROIType1_3.Text = "000";
            this.txtMROIType1_3.Enter += new System.EventHandler(this.txtMROIType1_Enter);
            this.txtMROIType1_3.Leave += new System.EventHandler(this.txtMROIType1_Leave);
            this.txtMROIType1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
            this.txtMROIType1_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
            this.txtMROIType1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
            // 
            // txtMROIType1_4
            // 
            this.txtMROIType1_4.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIType1_4.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIType1_4.Location = new System.Drawing.Point(20, 201);
            this.txtMROIType1_4.MaxLength = 3;
            this.txtMROIType1_4.Name = "txtMROIType1_4";
            this.txtMROIType1_4.Size = new System.Drawing.Size(60, 20);
            this.txtMROIType1_4.TabIndex = 156;
            this.txtMROIType1_4.Tag = "outbuilding";
            this.txtMROIType1_4.Text = "000";
            this.txtMROIType1_4.Enter += new System.EventHandler(this.txtMROIType1_Enter);
            this.txtMROIType1_4.Leave += new System.EventHandler(this.txtMROIType1_Leave);
            this.txtMROIType1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
            this.txtMROIType1_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
            this.txtMROIType1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
            // 
            // txtMROIType1_5
            // 
            this.txtMROIType1_5.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIType1_5.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIType1_5.Location = new System.Drawing.Point(20, 229);
            this.txtMROIType1_5.MaxLength = 3;
            this.txtMROIType1_5.Name = "txtMROIType1_5";
            this.txtMROIType1_5.Size = new System.Drawing.Size(60, 20);
            this.txtMROIType1_5.TabIndex = 166;
            this.txtMROIType1_5.Tag = "outbuilding";
            this.txtMROIType1_5.Text = "000";
            this.txtMROIType1_5.Enter += new System.EventHandler(this.txtMROIType1_Enter);
            this.txtMROIType1_5.Leave += new System.EventHandler(this.txtMROIType1_Leave);
            this.txtMROIType1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
            this.txtMROIType1_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
            this.txtMROIType1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
            // 
            // txtMROIType1_6
            // 
            this.txtMROIType1_6.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIType1_6.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIType1_6.Location = new System.Drawing.Point(20, 257);
            this.txtMROIType1_6.MaxLength = 3;
            this.txtMROIType1_6.Name = "txtMROIType1_6";
            this.txtMROIType1_6.Size = new System.Drawing.Size(60, 20);
            this.txtMROIType1_6.TabIndex = 176;
            this.txtMROIType1_6.Tag = "outbuilding";
            this.txtMROIType1_6.Text = "000";
            this.txtMROIType1_6.Enter += new System.EventHandler(this.txtMROIType1_Enter);
            this.txtMROIType1_6.Leave += new System.EventHandler(this.txtMROIType1_Leave);
            this.txtMROIType1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
            this.txtMROIType1_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
            this.txtMROIType1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
            // 
            // txtMROIType1_7
            // 
            this.txtMROIType1_7.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIType1_7.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIType1_7.Location = new System.Drawing.Point(20, 285);
            this.txtMROIType1_7.MaxLength = 3;
            this.txtMROIType1_7.Name = "txtMROIType1_7";
            this.txtMROIType1_7.Size = new System.Drawing.Size(60, 20);
            this.txtMROIType1_7.TabIndex = 186;
            this.txtMROIType1_7.Tag = "outbuilding";
            this.txtMROIType1_7.Text = "000";
            this.txtMROIType1_7.Enter += new System.EventHandler(this.txtMROIType1_Enter);
            this.txtMROIType1_7.Leave += new System.EventHandler(this.txtMROIType1_Leave);
            this.txtMROIType1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
            this.txtMROIType1_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
            this.txtMROIType1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
            // 
            // txtMROIType1_8
            // 
            this.txtMROIType1_8.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIType1_8.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIType1_8.Location = new System.Drawing.Point(20, 313);
            this.txtMROIType1_8.MaxLength = 3;
            this.txtMROIType1_8.Name = "txtMROIType1_8";
            this.txtMROIType1_8.Size = new System.Drawing.Size(60, 20);
            this.txtMROIType1_8.TabIndex = 196;
            this.txtMROIType1_8.Tag = "outbuilding";
            this.txtMROIType1_8.Text = "000";
            this.txtMROIType1_8.Enter += new System.EventHandler(this.txtMROIType1_Enter);
            this.txtMROIType1_8.Leave += new System.EventHandler(this.txtMROIType1_Leave);
            this.txtMROIType1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
            this.txtMROIType1_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
            this.txtMROIType1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
            // 
            // txtMROIType1_9
            // 
            this.txtMROIType1_9.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIType1_9.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIType1_9.Location = new System.Drawing.Point(20, 341);
            this.txtMROIType1_9.MaxLength = 3;
            this.txtMROIType1_9.Name = "txtMROIType1_9";
            this.txtMROIType1_9.Size = new System.Drawing.Size(60, 20);
            this.txtMROIType1_9.TabIndex = 206;
            this.txtMROIType1_9.Tag = "outbuilding";
            this.txtMROIType1_9.Text = "000";
            this.txtMROIType1_9.Enter += new System.EventHandler(this.txtMROIType1_Enter);
            this.txtMROIType1_9.Leave += new System.EventHandler(this.txtMROIType1_Leave);
            this.txtMROIType1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
            this.txtMROIType1_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
            this.txtMROIType1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
            // 
            // txtMROIUnits1_0
            // 
            this.txtMROIUnits1_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIUnits1_0.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIUnits1_0.Location = new System.Drawing.Point(175, 89);
            this.txtMROIUnits1_0.MaxLength = 7;
            this.txtMROIUnits1_0.Name = "txtMROIUnits1_0";
            this.txtMROIUnits1_0.Size = new System.Drawing.Size(65, 20);
            this.txtMROIUnits1_0.TabIndex = 118;
            this.txtMROIUnits1_0.Tag = "outbuilding";
            this.txtMROIUnits1_0.Text = "0000";
            this.txtMROIUnits1_0.Enter += new System.EventHandler(this.txtMROIUnits1_Enter);
            this.txtMROIUnits1_0.Leave += new System.EventHandler(this.txtMROIUnits1_Leave);
            this.txtMROIUnits1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIUnits1_Validating);
            this.txtMROIUnits1_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIUnits1_KeyPress);
            this.txtMROIUnits1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIUnits1_KeyUp);
            // 
            // txtMROIYear1_0
            // 
            this.txtMROIYear1_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIYear1_0.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIYear1_0.Location = new System.Drawing.Point(100, 89);
            this.txtMROIYear1_0.MaxLength = 4;
            this.txtMROIYear1_0.Name = "txtMROIYear1_0";
            this.txtMROIYear1_0.Size = new System.Drawing.Size(65, 20);
            this.txtMROIYear1_0.TabIndex = 117;
            this.txtMROIYear1_0.Tag = "outbuilding";
            this.txtMROIYear1_0.Text = "0000";
            this.txtMROIYear1_0.Enter += new System.EventHandler(this.txtMROIYear1_Enter);
            this.txtMROIYear1_0.Leave += new System.EventHandler(this.txtMROIYear1_Leave);
            this.txtMROIYear1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIYear1_Validating);
            this.txtMROIYear1_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIYear1_KeyPress);
            this.txtMROIYear1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIYear1_KeyUp);
            // 
            // txtMROIType1_0
            // 
            this.txtMROIType1_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIType1_0.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIType1_0.Location = new System.Drawing.Point(20, 89);
            this.txtMROIType1_0.MaxLength = 3;
            this.txtMROIType1_0.Name = "txtMROIType1_0";
            this.txtMROIType1_0.Size = new System.Drawing.Size(60, 20);
            this.txtMROIType1_0.TabIndex = 116;
            this.txtMROIType1_0.Tag = "outbuilding";
            this.txtMROIType1_0.Text = "000";
            this.txtMROIType1_0.Enter += new System.EventHandler(this.txtMROIType1_Enter);
            this.txtMROIType1_0.Leave += new System.EventHandler(this.txtMROIType1_Leave);
            this.txtMROIType1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIType1_Validating);
            this.txtMROIType1_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIType1_KeyPress);
            this.txtMROIType1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIType1_KeyUp);
            // 
            // txtMROIGradePct1_0
            // 
            this.txtMROIGradePct1_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIGradePct1_0.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIGradePct1_0.Location = new System.Drawing.Point(293, 89);
            this.txtMROIGradePct1_0.MaxLength = 3;
            this.txtMROIGradePct1_0.Name = "txtMROIGradePct1_0";
            this.txtMROIGradePct1_0.Size = new System.Drawing.Size(60, 20);
            this.txtMROIGradePct1_0.TabIndex = 120;
            this.txtMROIGradePct1_0.Tag = "outbuilding";
            this.txtMROIGradePct1_0.Text = "000";
            this.txtMROIGradePct1_0.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROIGradePct1_0.Enter += new System.EventHandler(this.txtMROIGradePct1_Enter);
            this.txtMROIGradePct1_0.Leave += new System.EventHandler(this.txtMROIGradePct1_Leave);
            this.txtMROIGradePct1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIGradePct1_Validating);
            this.txtMROIGradePct1_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIGradePct1_KeyPress);
            this.txtMROIGradePct1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIGradePct1_KeyUp);
            // 
            // txtMROICond1_0
            // 
            this.txtMROICond1_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROICond1_0.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROICond1_0.Location = new System.Drawing.Point(356, 89);
            this.txtMROICond1_0.MaxLength = 1;
            this.txtMROICond1_0.Name = "txtMROICond1_0";
            this.txtMROICond1_0.Size = new System.Drawing.Size(40, 20);
            this.txtMROICond1_0.TabIndex = 121;
            this.txtMROICond1_0.Tag = "outbuilding";
            this.txtMROICond1_0.Text = "0";
            this.txtMROICond1_0.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMROICond1_0.Enter += new System.EventHandler(this.txtMROICond1_Enter);
            this.txtMROICond1_0.Leave += new System.EventHandler(this.txtMROICond1_Leave);
            this.txtMROICond1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROICond1_Validating);
            this.txtMROICond1_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROICond1_KeyPress);
            this.txtMROICond1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROICond1_KeyUp);
            // 
            // txtMROIPctFunct1_0
            // 
            this.txtMROIPctFunct1_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctFunct1_0.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctFunct1_0.Location = new System.Drawing.Point(502, 89);
            this.txtMROIPctFunct1_0.MaxLength = 3;
            this.txtMROIPctFunct1_0.Name = "txtMROIPctFunct1_0";
            this.txtMROIPctFunct1_0.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctFunct1_0.TabIndex = 123;
            this.txtMROIPctFunct1_0.Tag = "outbuilding";
            this.txtMROIPctFunct1_0.Text = "000";
            this.txtMROIPctFunct1_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctFunct1_0.Enter += new System.EventHandler(this.txtMROIPctFunct1_Enter);
            this.txtMROIPctFunct1_0.Leave += new System.EventHandler(this.txtMROIPctFunct1_Leave);
            this.txtMROIPctFunct1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctFunct1_Validating);
            this.txtMROIPctFunct1_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctFunct1_KeyPress);
            this.txtMROIPctFunct1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctFunct1_KeyUp);
            // 
            // txtMROIPctPhys1_0
            // 
            this.txtMROIPctPhys1_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctPhys1_0.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctPhys1_0.Location = new System.Drawing.Point(420, 89);
            this.txtMROIPctPhys1_0.MaxLength = 3;
            this.txtMROIPctPhys1_0.Name = "txtMROIPctPhys1_0";
            this.txtMROIPctPhys1_0.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctPhys1_0.TabIndex = 122;
            this.txtMROIPctPhys1_0.Tag = "outbuilding";
            this.txtMROIPctPhys1_0.Text = "00";
            this.txtMROIPctPhys1_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctPhys1_0.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
            this.txtMROIPctPhys1_0.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
            this.txtMROIPctPhys1_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
            this.txtMROIPctPhys1_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctPhys1_KeyPress);
            this.txtMROIPctPhys1_0.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
            // 
            // txtMROIPctPhys1_9
            // 
            this.txtMROIPctPhys1_9.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctPhys1_9.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctPhys1_9.Location = new System.Drawing.Point(420, 341);
            this.txtMROIPctPhys1_9.MaxLength = 3;
            this.txtMROIPctPhys1_9.Name = "txtMROIPctPhys1_9";
            this.txtMROIPctPhys1_9.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctPhys1_9.TabIndex = 212;
            this.txtMROIPctPhys1_9.Tag = "outbuilding";
            this.txtMROIPctPhys1_9.Text = "00";
            this.txtMROIPctPhys1_9.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctPhys1_9.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
            this.txtMROIPctPhys1_9.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
            this.txtMROIPctPhys1_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
            this.txtMROIPctPhys1_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctPhys1_KeyPress);
            this.txtMROIPctPhys1_9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
            // 
            // txtMROIPctPhys1_8
            // 
            this.txtMROIPctPhys1_8.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctPhys1_8.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctPhys1_8.Location = new System.Drawing.Point(420, 313);
            this.txtMROIPctPhys1_8.MaxLength = 3;
            this.txtMROIPctPhys1_8.Name = "txtMROIPctPhys1_8";
            this.txtMROIPctPhys1_8.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctPhys1_8.TabIndex = 202;
            this.txtMROIPctPhys1_8.Tag = "outbuilding";
            this.txtMROIPctPhys1_8.Text = "00";
            this.txtMROIPctPhys1_8.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctPhys1_8.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
            this.txtMROIPctPhys1_8.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
            this.txtMROIPctPhys1_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
            this.txtMROIPctPhys1_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctPhys1_KeyPress);
            this.txtMROIPctPhys1_8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
            // 
            // txtMROIPctPhys1_7
            // 
            this.txtMROIPctPhys1_7.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctPhys1_7.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctPhys1_7.Location = new System.Drawing.Point(420, 285);
            this.txtMROIPctPhys1_7.MaxLength = 3;
            this.txtMROIPctPhys1_7.Name = "txtMROIPctPhys1_7";
            this.txtMROIPctPhys1_7.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctPhys1_7.TabIndex = 192;
            this.txtMROIPctPhys1_7.Tag = "outbuilding";
            this.txtMROIPctPhys1_7.Text = "00";
            this.txtMROIPctPhys1_7.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctPhys1_7.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
            this.txtMROIPctPhys1_7.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
            this.txtMROIPctPhys1_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
            this.txtMROIPctPhys1_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctPhys1_KeyPress);
            this.txtMROIPctPhys1_7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
            // 
            // txtMROIPctPhys1_6
            // 
            this.txtMROIPctPhys1_6.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctPhys1_6.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctPhys1_6.Location = new System.Drawing.Point(420, 257);
            this.txtMROIPctPhys1_6.MaxLength = 3;
            this.txtMROIPctPhys1_6.Name = "txtMROIPctPhys1_6";
            this.txtMROIPctPhys1_6.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctPhys1_6.TabIndex = 182;
            this.txtMROIPctPhys1_6.Tag = "outbuilding";
            this.txtMROIPctPhys1_6.Text = "00";
            this.txtMROIPctPhys1_6.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctPhys1_6.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
            this.txtMROIPctPhys1_6.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
            this.txtMROIPctPhys1_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
            this.txtMROIPctPhys1_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctPhys1_KeyPress);
            this.txtMROIPctPhys1_6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
            // 
            // txtMROIPctPhys1_5
            // 
            this.txtMROIPctPhys1_5.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctPhys1_5.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctPhys1_5.Location = new System.Drawing.Point(420, 229);
            this.txtMROIPctPhys1_5.MaxLength = 3;
            this.txtMROIPctPhys1_5.Name = "txtMROIPctPhys1_5";
            this.txtMROIPctPhys1_5.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctPhys1_5.TabIndex = 172;
            this.txtMROIPctPhys1_5.Tag = "outbuilding";
            this.txtMROIPctPhys1_5.Text = "00";
            this.txtMROIPctPhys1_5.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctPhys1_5.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
            this.txtMROIPctPhys1_5.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
            this.txtMROIPctPhys1_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
            this.txtMROIPctPhys1_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctPhys1_KeyPress);
            this.txtMROIPctPhys1_5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
            // 
            // txtMROIPctPhys1_4
            // 
            this.txtMROIPctPhys1_4.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctPhys1_4.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctPhys1_4.Location = new System.Drawing.Point(420, 201);
            this.txtMROIPctPhys1_4.MaxLength = 3;
            this.txtMROIPctPhys1_4.Name = "txtMROIPctPhys1_4";
            this.txtMROIPctPhys1_4.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctPhys1_4.TabIndex = 162;
            this.txtMROIPctPhys1_4.Tag = "outbuilding";
            this.txtMROIPctPhys1_4.Text = "00";
            this.txtMROIPctPhys1_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctPhys1_4.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
            this.txtMROIPctPhys1_4.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
            this.txtMROIPctPhys1_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
            this.txtMROIPctPhys1_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctPhys1_KeyPress);
            this.txtMROIPctPhys1_4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
            // 
            // txtMROIPctPhys1_3
            // 
            this.txtMROIPctPhys1_3.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctPhys1_3.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctPhys1_3.Location = new System.Drawing.Point(420, 173);
            this.txtMROIPctPhys1_3.MaxLength = 3;
            this.txtMROIPctPhys1_3.Name = "txtMROIPctPhys1_3";
            this.txtMROIPctPhys1_3.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctPhys1_3.TabIndex = 152;
            this.txtMROIPctPhys1_3.Tag = "outbuilding";
            this.txtMROIPctPhys1_3.Text = "00";
            this.txtMROIPctPhys1_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctPhys1_3.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
            this.txtMROIPctPhys1_3.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
            this.txtMROIPctPhys1_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
            this.txtMROIPctPhys1_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctPhys1_KeyPress);
            this.txtMROIPctPhys1_3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
            // 
            // txtMROIPctPhys1_2
            // 
            this.txtMROIPctPhys1_2.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctPhys1_2.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctPhys1_2.Location = new System.Drawing.Point(420, 145);
            this.txtMROIPctPhys1_2.MaxLength = 3;
            this.txtMROIPctPhys1_2.Name = "txtMROIPctPhys1_2";
            this.txtMROIPctPhys1_2.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctPhys1_2.TabIndex = 142;
            this.txtMROIPctPhys1_2.Tag = "outbuilding";
            this.txtMROIPctPhys1_2.Text = "00";
            this.txtMROIPctPhys1_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctPhys1_2.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
            this.txtMROIPctPhys1_2.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
            this.txtMROIPctPhys1_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
            this.txtMROIPctPhys1_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctPhys1_KeyPress);
            this.txtMROIPctPhys1_2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
            // 
            // txtMROIPctPhys1_1
            // 
            this.txtMROIPctPhys1_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtMROIPctPhys1_1.Cursor = Wisej.Web.Cursors.Default;
            this.txtMROIPctPhys1_1.Location = new System.Drawing.Point(420, 117);
            this.txtMROIPctPhys1_1.MaxLength = 3;
            this.txtMROIPctPhys1_1.Name = "txtMROIPctPhys1_1";
            this.txtMROIPctPhys1_1.Size = new System.Drawing.Size(60, 20);
            this.txtMROIPctPhys1_1.TabIndex = 132;
            this.txtMROIPctPhys1_1.Tag = "outbuilding";
            this.txtMROIPctPhys1_1.Text = "00";
            this.txtMROIPctPhys1_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtMROIPctPhys1_1.Enter += new System.EventHandler(this.txtMROIPctPhys1_Enter);
            this.txtMROIPctPhys1_1.Leave += new System.EventHandler(this.txtMROIPctPhys1_Leave);
            this.txtMROIPctPhys1_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtMROIPctPhys1_Validating);
            this.txtMROIPctPhys1_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMROIPctPhys1_KeyPress);
            this.txtMROIPctPhys1_1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtMROIPctPhys1_KeyUp);
            // 
            // Label2_1
            // 
            this.Label2_1.Location = new System.Drawing.Point(586, 65);
            this.Label2_1.Name = "Label2_1";
            this.Label2_1.Size = new System.Drawing.Size(90, 14);
            this.Label2_1.TabIndex = 226;
            this.Label2_1.Text = "SOUND VALUE";
            // 
            // lblMapLotCopy_2
            // 
            this.lblMapLotCopy_2.Location = new System.Drawing.Point(20, 391);
            this.lblMapLotCopy_2.Name = "lblMapLotCopy_2";
            this.lblMapLotCopy_2.Size = new System.Drawing.Size(203, 16);
            this.lblMapLotCopy_2.TabIndex = 225;
            this.lblMapLotCopy_2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label2_8
            // 
            this.Label2_8.Location = new System.Drawing.Point(459, 47);
            this.Label2_8.Name = "Label2_8";
            this.Label2_8.Size = new System.Drawing.Size(70, 14);
            this.Label2_8.TabIndex = 224;
            this.Label2_8.Text = "% GOOD";
            this.Label2_8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label2_7
            // 
            this.Label2_7.Location = new System.Drawing.Point(498, 65);
            this.Label2_7.Name = "Label2_7";
            this.Label2_7.Size = new System.Drawing.Size(78, 14);
            this.Label2_7.TabIndex = 223;
            this.Label2_7.Text = "FUNCTIONAL";
            // 
            // Label2_6
            // 
            this.Label2_6.Location = new System.Drawing.Point(427, 65);
            this.Label2_6.Name = "Label2_6";
            this.Label2_6.Size = new System.Drawing.Size(60, 14);
            this.Label2_6.TabIndex = 222;
            this.Label2_6.Text = "PHYSICAL";
            // 
            // Label2_5
            // 
            this.Label2_5.Location = new System.Drawing.Point(347, 65);
            this.Label2_5.Name = "Label2_5";
            this.Label2_5.Size = new System.Drawing.Size(70, 14);
            this.Label2_5.TabIndex = 221;
            this.Label2_5.Text = "CONDITION";
            // 
            // Label2_4
            // 
            this.Label2_4.Location = new System.Drawing.Point(277, 65);
            this.Label2_4.Name = "Label2_4";
            this.Label2_4.Size = new System.Drawing.Size(42, 14);
            this.Label2_4.TabIndex = 220;
            this.Label2_4.Text = "GRADE";
            // 
            // Label2_3
            // 
            this.Label2_3.Location = new System.Drawing.Point(194, 65);
            this.Label2_3.Name = "Label2_3";
            this.Label2_3.Size = new System.Drawing.Size(40, 14);
            this.Label2_3.TabIndex = 219;
            this.Label2_3.Text = "UNITS";
            // 
            // Label2_2
            // 
            this.Label2_2.Location = new System.Drawing.Point(117, 65);
            this.Label2_2.Name = "Label2_2";
            this.Label2_2.Size = new System.Drawing.Size(40, 14);
            this.Label2_2.TabIndex = 218;
            this.Label2_2.Text = "YEAR";
            // 
            // Label2_0
            // 
            this.Label2_0.Location = new System.Drawing.Point(31, 65);
            this.Label2_0.Name = "Label2_0";
            this.Label2_0.Size = new System.Drawing.Size(37, 14);
            this.Label2_0.TabIndex = 217;
            this.Label2_0.Text = "TYPE";
            // 
            // lblOut
            // 
            this.lblOut.Location = new System.Drawing.Point(20, 30);
            this.lblOut.Name = "lblOut";
            this.lblOut.Size = new System.Drawing.Size(80, 14);
            this.lblOut.TabIndex = 216;
            this.lblOut.Text = "VIEW ONLY";
            this.lblOut.Visible = false;
            // 
            // SSTab1_Page6
            // 
            this.SSTab1_Page6.Controls.Add(this.txtTotalSF);
            this.SSTab1_Page6.Controls.Add(this.txtTotalExpense);
            this.SSTab1_Page6.Controls.Add(this.GridValues);
            this.SSTab1_Page6.Controls.Add(this.GridExpenses);
            this.SSTab1_Page6.Controls.Add(this.Grid1);
            this.SSTab1_Page6.Controls.Add(this.lblTotalSF);
            this.SSTab1_Page6.Controls.Add(this.lblTotalExpense);
            this.SSTab1_Page6.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page6.Name = "SSTab1_Page6";
            this.SSTab1_Page6.Size = new System.Drawing.Size(948, 850);
            this.SSTab1_Page6.Text = "Income App";
            // 
            // txtTotalSF
            // 
            this.txtTotalSF.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalSF.Cursor = Wisej.Web.Cursors.Default;
            this.txtTotalSF.Enabled = false;
            this.txtTotalSF.Location = new System.Drawing.Point(20, 190);
            this.txtTotalSF.Name = "txtTotalSF";
            this.txtTotalSF.Size = new System.Drawing.Size(80, 40);
            this.txtTotalSF.TabIndex = 228;
            this.txtTotalSF.Text = "0";
            this.txtTotalSF.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtTotalExpense
            // 
            this.txtTotalExpense.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalExpense.Cursor = Wisej.Web.Cursors.Default;
            this.txtTotalExpense.Enabled = false;
            this.txtTotalExpense.Location = new System.Drawing.Point(160, 470);
            this.txtTotalExpense.Name = "txtTotalExpense";
            this.txtTotalExpense.Size = new System.Drawing.Size(73, 40);
            this.txtTotalExpense.TabIndex = 227;
            this.txtTotalExpense.Text = "0";
            this.txtTotalExpense.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // GridValues
            // 
            this.GridValues.Cols = 3;
            this.GridValues.ColumnHeadersVisible = false;
            this.GridValues.FixedRows = 0;
            this.GridValues.Location = new System.Drawing.Point(501, 250);
            this.GridValues.Name = "GridValues";
            this.GridValues.Rows = 50;
            this.GridValues.ShowFocusCell = false;
            this.GridValues.Size = new System.Drawing.Size(426, 362);
            this.GridValues.TabIndex = 271;
            this.GridValues.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridValues_AfterEdit);
            this.GridValues.CurrentCellChanged += new System.EventHandler(this.GridValues_RowColChange);
            // 
            // GridExpenses
            // 
            this.GridExpenses.Cols = 4;
            this.GridExpenses.ColumnHeadersHeight = 60;
            this.GridExpenses.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridExpenses.FixedCols = 0;
            this.GridExpenses.FixedRows = 2;
            this.GridExpenses.Location = new System.Drawing.Point(20, 250);
            this.GridExpenses.Name = "GridExpenses";
            this.GridExpenses.ReadOnly = false;
            this.GridExpenses.RowHeadersVisible = false;
            this.GridExpenses.Rows = 50;
            this.GridExpenses.ShowFocusCell = false;
            this.GridExpenses.Size = new System.Drawing.Size(462, 155);
            this.GridExpenses.TabIndex = 272;
            this.GridExpenses.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridExpenses_AfterEdit);
            this.GridExpenses.KeyDown += new Wisej.Web.KeyEventHandler(this.GridExpenses_KeyDownEvent);
            // 
            // Grid1
            // 
            this.Grid1.Cols = 10;
            this.Grid1.ColumnHeadersHeight = 60;
            this.Grid1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid1.FixedCols = 0;
            this.Grid1.FixedRows = 2;
            this.Grid1.Location = new System.Drawing.Point(20, 30);
            this.Grid1.Name = "Grid1";
            this.Grid1.ReadOnly = false;
            this.Grid1.RowHeadersVisible = false;
            this.Grid1.Rows = 50;
            this.Grid1.ShowFocusCell = false;
            this.Grid1.Size = new System.Drawing.Size(907, 138);
            this.Grid1.TabIndex = 273;
            this.Grid1.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid1_AfterEdit);
            this.Grid1.CurrentCellChanged += new System.EventHandler(this.Grid1_RowColChange);
            this.Grid1.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid1_KeyDownEvent);
            // 
            // lblTotalSF
            // 
            this.lblTotalSF.Location = new System.Drawing.Point(135, 204);
            this.lblTotalSF.Name = "lblTotalSF";
            this.lblTotalSF.Size = new System.Drawing.Size(141, 18);
            this.lblTotalSF.TabIndex = 297;
            this.lblTotalSF.Text = "TOTAL SQUARE FEET";
            // 
            // lblTotalExpense
            // 
            this.lblTotalExpense.Location = new System.Drawing.Point(20, 484);
            this.lblTotalExpense.Name = "lblTotalExpense";
            this.lblTotalExpense.Size = new System.Drawing.Size(105, 18);
            this.lblTotalExpense.TabIndex = 296;
            this.lblTotalExpense.Text = "TOTAL EXPENSE";
            // 
            // SSTab1_Page7
            // 
            this.SSTab1_Page7.Controls.Add(this.Toolbar1);
            this.SSTab1_Page7.Controls.Add(this.Picture1);
            this.SSTab1_Page7.Controls.Add(this.txtPicDesc);
            this.SSTab1_Page7.Controls.Add(this.cmdNextPicture);
            this.SSTab1_Page7.Controls.Add(this.cmdPrevPicture);
            this.SSTab1_Page7.Controls.Add(this.framPicture);
            this.SSTab1_Page7.Controls.Add(this.framPicDimensions);
            this.SSTab1_Page7.Controls.Add(this.lblPicName);
            this.SSTab1_Page7.Controls.Add(this.lblSketchNumber);
            this.SSTab1_Page7.Controls.Add(this.lblName2);
            this.SSTab1_Page7.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page7.Name = "SSTab1_Page7";
            this.SSTab1_Page7.Size = new System.Drawing.Size(948, 850);
            this.SSTab1_Page7.Text = "Picture";
            // 
            // Toolbar1
            // 
            this.Toolbar1.BackColor = System.Drawing.Color.White;
            this.Toolbar1.Buttons.Add(this.BTNPICADDPIC);
            this.Toolbar1.Buttons.Add(this.BTNPICDELETEPIC);
            this.Toolbar1.Buttons.Add(this.BTNPRINTPICTURE);
            this.Toolbar1.ButtonSize = new System.Drawing.Size(40, 40);
            this.Toolbar1.Location = new System.Drawing.Point(20, 30);
            this.Toolbar1.Name = "Toolbar1";
            this.Toolbar1.Size = new System.Drawing.Size(200, 32);
            this.Toolbar1.TabIndex = 50;
            this.Toolbar1.ButtonClick += new System.EventHandler<fecherFoundation.FCToolBarButtonClickEventArgs>(this.Toolbar1_ButtonClick);
            // 
            // BTNPICADDPIC
            // 
            this.BTNPICADDPIC.ImageSource = "icon-add";
            this.BTNPICADDPIC.Key = "BTNPICADDPIC";
            this.BTNPICADDPIC.Name = "BTNPICADDPIC";
            this.BTNPICADDPIC.ToolTipText = "Add picture";
            this.BTNPICADDPIC.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
            // 
            // BTNPICDELETEPIC
            // 
            this.BTNPICDELETEPIC.ImageSource = "icon-delete";
            this.BTNPICDELETEPIC.Key = "BTNPICDELETEPIC";
            this.BTNPICDELETEPIC.Name = "BTNPICDELETEPIC";
            this.BTNPICDELETEPIC.ToolTipText = "Delete picture from account";
            this.BTNPICDELETEPIC.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
            // 
            // BTNPRINTPICTURE
            // 
            this.BTNPRINTPICTURE.ImageSource = "icon-print1";
            this.BTNPRINTPICTURE.Key = "BTNPRINTPICTURE";
            this.BTNPRINTPICTURE.Name = "BTNPRINTPICTURE";
            this.BTNPRINTPICTURE.ToolTipText = "Print Preview";
            this.BTNPRINTPICTURE.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
            // 
            // Picture1
            // 
            this.Picture1.Cursor = Wisej.Web.Cursors.Default;
            this.Picture1.FillColor = 16777215;
            this.Picture1.Location = new System.Drawing.Point(20, 601);
            this.Picture1.Name = "Picture1";
            this.Picture1.Size = new System.Drawing.Size(25, 25);
            this.Picture1.Visible = false;
            // 
            // txtPicDesc
            // 
            this.txtPicDesc.BackColor = System.Drawing.SystemColors.Window;
            this.txtPicDesc.Cursor = Wisej.Web.Cursors.Default;
            this.txtPicDesc.Location = new System.Drawing.Point(20, 667);
            this.txtPicDesc.Name = "txtPicDesc";
            this.txtPicDesc.Size = new System.Drawing.Size(367, 40);
            this.txtPicDesc.TabIndex = 234;
            this.txtPicDesc.Text = "Enter Comment or Description Here";
            // 
            // cmdNextPicture
            // 
            this.cmdNextPicture.AppearanceKey = "actionButton";
            this.cmdNextPicture.Cursor = Wisej.Web.Cursors.Default;
            this.cmdNextPicture.Location = new System.Drawing.Point(355, 587);
            this.cmdNextPicture.Name = "cmdNextPicture";
            this.cmdNextPicture.Size = new System.Drawing.Size(47, 40);
            this.cmdNextPicture.TabIndex = 233;
            this.cmdNextPicture.Click += new System.EventHandler(this.cmdNextPicture_Click);
            // 
            // cmdPrevPicture
            // 
            this.cmdPrevPicture.AppearanceKey = "actionButton";
            this.cmdPrevPicture.Cursor = Wisej.Web.Cursors.Default;
            this.cmdPrevPicture.Location = new System.Drawing.Point(286, 587);
            this.cmdPrevPicture.Name = "cmdPrevPicture";
            this.cmdPrevPicture.Size = new System.Drawing.Size(47, 40);
            this.cmdPrevPicture.TabIndex = 232;
            this.cmdPrevPicture.Click += new System.EventHandler(this.cmdPrevPicture_Click);
            // 
            // framPicture
            // 
            this.framPicture.AppearanceKey = "groupBoxNoBorders";
            this.framPicture.Controls.Add(this.imgPicture);
            this.framPicture.Location = new System.Drawing.Point(20, 88);
            this.framPicture.Name = "framPicture";
            this.framPicture.Size = new System.Drawing.Size(907, 448);
            this.framPicture.TabIndex = 231;
            // 
            // imgPicture
            // 
            this.imgPicture.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgPicture.Cursor = Wisej.Web.Cursors.Default;
            this.imgPicture.FillColor = 16777215;
            this.imgPicture.Location = new System.Drawing.Point(20, 20);
            this.imgPicture.Name = "imgPicture";
            this.imgPicture.Size = new System.Drawing.Size(542, 266);
            this.imgPicture.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgPicture.Visible = false;
            // 
            // framPicDimensions
            // 
            this.framPicDimensions.Controls.Add(this.cmbPrintSize);
            this.framPicDimensions.Location = new System.Drawing.Point(421, 557);
            this.framPicDimensions.Name = "framPicDimensions";
            this.framPicDimensions.Size = new System.Drawing.Size(195, 90);
            this.framPicDimensions.TabIndex = 229;
            this.framPicDimensions.Text = "Print Size";
            // 
            // cmbPrintSize
            // 
            this.cmbPrintSize.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPrintSize.Location = new System.Drawing.Point(20, 30);
            this.cmbPrintSize.Name = "cmbPrintSize";
            this.cmbPrintSize.Size = new System.Drawing.Size(155, 40);
            this.cmbPrintSize.TabIndex = 230;
            // 
            // lblPicName
            // 
            this.lblPicName.Location = new System.Drawing.Point(20, 587);
            this.lblPicName.Name = "lblPicName";
            this.lblPicName.Size = new System.Drawing.Size(246, 40);
            this.lblPicName.TabIndex = 299;
            // 
            // lblSketchNumber
            // 
            this.lblSketchNumber.Location = new System.Drawing.Point(45, 585);
            this.lblSketchNumber.Name = "lblSketchNumber";
            this.lblSketchNumber.Size = new System.Drawing.Size(107, 27);
            this.lblSketchNumber.TabIndex = 302;
            // 
            // lblName2
            // 
            this.lblName2.Location = new System.Drawing.Point(20, 553);
            this.lblName2.Name = "lblName2";
            this.lblName2.Size = new System.Drawing.Size(212, 14);
            this.lblName2.TabIndex = 298;
            // 
            // SSTab1_Page8
            // 
            this.SSTab1_Page8.Controls.Add(this.framSketch);
            this.SSTab1_Page8.Controls.Add(this.Picture2);
            this.SSTab1_Page8.Controls.Add(this.cmdNextSketch);
            this.SSTab1_Page8.Controls.Add(this.cmdPrevSketch);
            this.SSTab1_Page8.Location = new System.Drawing.Point(1, 35);
            this.SSTab1_Page8.Name = "SSTab1_Page8";
            this.SSTab1_Page8.Size = new System.Drawing.Size(948, 850);
            this.SSTab1_Page8.Text = "Sketch";
            // 
            // framSketch
            // 
            this.framSketch.AutoScroll = true;
            this.framSketch.Controls.Add(this.imSketch);
            this.framSketch.Location = new System.Drawing.Point(10, 10);
            this.framSketch.Name = "framSketch";
            this.framSketch.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.framSketch.Size = new System.Drawing.Size(930, 320);
            this.framSketch.TabIndex = 241;
            // 
            // imSketch
            // 
            this.imSketch.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imSketch.Cursor = Wisej.Web.Cursors.Default;
            this.imSketch.FillColor = 16777215;
            this.imSketch.Name = "imSketch";
            this.imSketch.Size = new System.Drawing.Size(920, 320);
            this.imSketch.SizeMode = Wisej.Web.PictureBoxSizeMode.Zoom;
            // 
            // Picture2
            // 
            this.Picture2.Cursor = Wisej.Web.Cursors.Default;
            this.Picture2.FillColor = 16777215;
            this.Picture2.Location = new System.Drawing.Point(20, 360);
            this.Picture2.Name = "Picture2";
            this.Picture2.Size = new System.Drawing.Size(40, 40);
            this.Picture2.Visible = false;
            // 
            // cmdNextSketch
            // 
            this.cmdNextSketch.AppearanceKey = "actionButton";
            this.cmdNextSketch.Cursor = Wisej.Web.Cursors.Default;
            this.cmdNextSketch.Enabled = false;
            this.cmdNextSketch.Location = new System.Drawing.Point(150, 360);
            this.cmdNextSketch.Name = "cmdNextSketch";
            this.cmdNextSketch.Size = new System.Drawing.Size(50, 40);
            this.cmdNextSketch.TabIndex = 237;
            this.ToolTip1.SetToolTip(this.cmdNextSketch, "View next sketch");
            this.cmdNextSketch.Click += new System.EventHandler(this.cmdNextSketch_Click);
            // 
            // cmdPrevSketch
            // 
            this.cmdPrevSketch.AppearanceKey = "actionButton";
            this.cmdPrevSketch.Cursor = Wisej.Web.Cursors.Default;
            this.cmdPrevSketch.Enabled = false;
            this.cmdPrevSketch.Location = new System.Drawing.Point(80, 360);
            this.cmdPrevSketch.Name = "cmdPrevSketch";
            this.cmdPrevSketch.Size = new System.Drawing.Size(50, 40);
            this.cmdPrevSketch.TabIndex = 236;
            this.ToolTip1.SetToolTip(this.cmdPrevSketch, "View previous sketch");
            this.cmdPrevSketch.Click += new System.EventHandler(this.cmdPrevSketch_Click);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddNewAccount,
            this.mnuAddACard,
            this.mnuDeleteAccount,
            this.mnuDeleteCard,
            this.mnuDeleteDwellingData,
            this.mnuDeleteOutbuilding,
            this.mnuSeparateCard,
            this.mnuValuationReport,
            this.mnuViewDocs,
            this.mnuViewGroupInformation,
            this.mnuViewMortgageInformation,
            this.mnuOptions,
            this.mnuPrint,
            this.mnuSketch,
            this.mnuPictures,
            this.mnuLayout});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuAddNewAccount
            // 
            this.mnuAddNewAccount.Index = 0;
            this.mnuAddNewAccount.Name = "mnuAddNewAccount";
            this.mnuAddNewAccount.Text = "New Account";
            this.mnuAddNewAccount.Click += new System.EventHandler(this.mnuAddNewAccount_Click);
            // 
            // mnuAddACard
            // 
            this.mnuAddACard.Index = 1;
            this.mnuAddACard.Name = "mnuAddACard";
            this.mnuAddACard.Text = "New Card";
            this.mnuAddACard.Click += new System.EventHandler(this.mnuAddACard_Click);
            // 
            // mnuDeleteAccount
            // 
            this.mnuDeleteAccount.Index = 2;
            this.mnuDeleteAccount.Name = "mnuDeleteAccount";
            this.mnuDeleteAccount.Text = "Delete Account";
            this.mnuDeleteAccount.Click += new System.EventHandler(this.mnuDeleteAccount_Click);
            // 
            // mnuDeleteCard
            // 
            this.mnuDeleteCard.Index = 3;
            this.mnuDeleteCard.Name = "mnuDeleteCard";
            this.mnuDeleteCard.Text = "Delete Card";
            this.mnuDeleteCard.Click += new System.EventHandler(this.mnuDeleteCard_Click);
            // 
            // mnuDeleteDwellingData
            // 
            this.mnuDeleteDwellingData.Index = 4;
            this.mnuDeleteDwellingData.Name = "mnuDeleteDwellingData";
            this.mnuDeleteDwellingData.Text = "Delete Dwelling / Commercial Data";
            this.mnuDeleteDwellingData.Click += new System.EventHandler(this.mnuDeleteDwellingData_Click);
            // 
            // mnuDeleteOutbuilding
            // 
            this.mnuDeleteOutbuilding.Index = 5;
            this.mnuDeleteOutbuilding.Name = "mnuDeleteOutbuilding";
            this.mnuDeleteOutbuilding.Text = "Clear Outbuilding Data";
            this.mnuDeleteOutbuilding.Click += new System.EventHandler(this.mnuDeleteOutbuilding_Click);
            // 
            // mnuSeparateCard
            // 
            this.mnuSeparateCard.Index = 6;
            this.mnuSeparateCard.Name = "mnuSeparateCard";
            this.mnuSeparateCard.Text = "Save Card as New Account";
            this.mnuSeparateCard.Click += new System.EventHandler(this.mnuSeparateCard_Click);
            // 
            // mnuValuationReport
            // 
            this.mnuValuationReport.Index = 7;
            this.mnuValuationReport.Name = "mnuValuationReport";
            this.mnuValuationReport.Text = "Valuation Report (Calculates)";
            this.mnuValuationReport.Click += new System.EventHandler(this.mnuValuationReport_Click);
            // 
            // mnuViewDocs
            // 
            this.mnuViewDocs.Index = 8;
            this.mnuViewDocs.Name = "mnuViewDocs";
            this.mnuViewDocs.Text = "View Attached Documents";
            this.mnuViewDocs.Click += new System.EventHandler(this.mnuViewDocs_Click);
            // 
            // mnuViewGroupInformation
            // 
            this.mnuViewGroupInformation.Index = 9;
            this.mnuViewGroupInformation.Name = "mnuViewGroupInformation";
            this.mnuViewGroupInformation.Text = "View Group Information";
            this.mnuViewGroupInformation.Click += new System.EventHandler(this.mnuViewGroupInformation_Click);
            // 
            // mnuViewMortgageInformation
            // 
            this.mnuViewMortgageInformation.Index = 10;
            this.mnuViewMortgageInformation.Name = "mnuViewMortgageInformation";
            this.mnuViewMortgageInformation.Text = "View Mortgage Information";
            this.mnuViewMortgageInformation.Click += new System.EventHandler(this.mnuViewMortgageInformation_Click);
            // 
            // mnuOptions
            // 
            this.mnuOptions.Index = 11;
            this.mnuOptions.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRECLComment,
            this.mnuCollectionsNote,
            this.mnuinterestedparties,
            this.mnuCopyProperty,
            this.mnuPendingTransfer});
            this.mnuOptions.Name = "mnuOptions";
            this.mnuOptions.Text = "Options";
            // 
            // mnuRECLComment
            // 
            this.mnuRECLComment.Index = 0;
            this.mnuRECLComment.Name = "mnuRECLComment";
            this.mnuRECLComment.Text = "Add RE / CL Comment";
            this.mnuRECLComment.Click += new System.EventHandler(this.mnuRECLComment_Click);
            // 
            // mnuCollectionsNote
            // 
            this.mnuCollectionsNote.Index = 1;
            this.mnuCollectionsNote.Name = "mnuCollectionsNote";
            this.mnuCollectionsNote.Text = "Collections Note";
            this.mnuCollectionsNote.Click += new System.EventHandler(this.mnuCollectionsNote_Click);
            // 
            // mnuinterestedparties
            // 
            this.mnuinterestedparties.Index = 2;
            this.mnuinterestedparties.Name = "mnuinterestedparties";
            this.mnuinterestedparties.Text = "Interested Parties";
            this.mnuinterestedparties.Click += new System.EventHandler(this.mnuinterestedparties_Click);
            // 
            // mnuCopyProperty
            // 
            this.mnuCopyProperty.Index = 3;
            this.mnuCopyProperty.Name = "mnuCopyProperty";
            this.mnuCopyProperty.Text = "Transfer information from another account";
            this.mnuCopyProperty.Click += new System.EventHandler(this.mnuCopyProperty_Click);
            // 
            // mnuPendingTransfer
            // 
            this.mnuPendingTransfer.Index = 4;
            this.mnuPendingTransfer.Name = "mnuPendingTransfer";
            this.mnuPendingTransfer.Text = "Create Pending Transfer";
            this.mnuPendingTransfer.Click += new System.EventHandler(this.mnuPendingTransfer_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 12;
            this.mnuPrint.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintAccountInformationSheet,
            this.mnuPrintCard,
            this.mnuPrintCards,
            this.mnuPrintPropertyCard,
            this.mnuPrintAllPropertyCards,
            this.mnuPrintPropertyCardsPDF,
            this.mnuPrint1pagepropertycard,
            this.mnuPrintLabel,
            this.mnuPrintMailing,
            this.mnuPrintIncomeApproach,
            this.mnuPrintScreen,
            this.mnuPrintInterestedParties});
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            // 
            // mnuPrintAccountInformationSheet
            // 
            this.mnuPrintAccountInformationSheet.Index = 0;
            this.mnuPrintAccountInformationSheet.Name = "mnuPrintAccountInformationSheet";
            this.mnuPrintAccountInformationSheet.Text = "Print Account Information Sheet";
            this.mnuPrintAccountInformationSheet.Click += new System.EventHandler(this.mnuPrintAccountInformationSheet_Click);
            // 
            // mnuPrintCard
            // 
            this.mnuPrintCard.Index = 1;
            this.mnuPrintCard.Name = "mnuPrintCard";
            this.mnuPrintCard.Text = "Print Card Information";
            this.mnuPrintCard.Click += new System.EventHandler(this.mnuPrintCard_Click);
            // 
            // mnuPrintCards
            // 
            this.mnuPrintCards.Index = 2;
            this.mnuPrintCards.Name = "mnuPrintCards";
            this.mnuPrintCards.Text = "Print All Cards";
            this.mnuPrintCards.Click += new System.EventHandler(this.mnuPrintCards_Click);
            // 
            // mnuPrintPropertyCard
            // 
            this.mnuPrintPropertyCard.Index = 3;
            this.mnuPrintPropertyCard.Name = "mnuPrintPropertyCard";
            this.mnuPrintPropertyCard.Text = "Print Property Card";
            this.mnuPrintPropertyCard.Click += new System.EventHandler(this.mnuPrintPropertyCard_Click);
            // 
            // mnuPrintAllPropertyCards
            // 
            this.mnuPrintAllPropertyCards.Index = 4;
            this.mnuPrintAllPropertyCards.Name = "mnuPrintAllPropertyCards";
            this.mnuPrintAllPropertyCards.Text = "Print All Property Cards";
            this.mnuPrintAllPropertyCards.Click += new System.EventHandler(this.mnuPrintAllPropertyCards_Click);
            // 
            // mnuPrintPropertyCardsPDF
            // 
            this.mnuPrintPropertyCardsPDF.Index = 5;
            this.mnuPrintPropertyCardsPDF.Name = "mnuPrintPropertyCardsPDF";
            this.mnuPrintPropertyCardsPDF.Text = "Print Property PDF";
            this.mnuPrintPropertyCardsPDF.Click += new System.EventHandler(this.mnuPrintPropertyCardsPDF_Click);
            // 
            // mnuPrint1pagepropertycard
            // 
            this.mnuPrint1pagepropertycard.Index = 6;
            this.mnuPrint1pagepropertycard.Name = "mnuPrint1pagepropertycard";
            this.mnuPrint1pagepropertycard.Text = "Print One Page Property Card";
            this.mnuPrint1pagepropertycard.Click += new System.EventHandler(this.mnuPrint1pagepropertycard_Click);
            // 
            // mnuPrintLabel
            // 
            this.mnuPrintLabel.Index = 7;
            this.mnuPrintLabel.Name = "mnuPrintLabel";
            this.mnuPrintLabel.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuPrintLabel.Text = "Print Label";
            this.mnuPrintLabel.Click += new System.EventHandler(this.mnuPrintLabel_Click);
            // 
            // mnuPrintMailing
            // 
            this.mnuPrintMailing.Index = 8;
            this.mnuPrintMailing.Name = "mnuPrintMailing";
            this.mnuPrintMailing.Shortcut = Wisej.Web.Shortcut.ShiftF2;
            this.mnuPrintMailing.Text = "Print Mailing Label";
            this.mnuPrintMailing.Click += new System.EventHandler(this.mnuPrintMailing_Click);
            // 
            // mnuPrintIncomeApproach
            // 
            this.mnuPrintIncomeApproach.Index = 9;
            this.mnuPrintIncomeApproach.Name = "mnuPrintIncomeApproach";
            this.mnuPrintIncomeApproach.Text = "Print Income Approach";
            this.mnuPrintIncomeApproach.Click += new System.EventHandler(this.mnuPrintIncomeApproach_Click);
            // 
            // mnuPrintScreen
            // 
            this.mnuPrintScreen.Index = 10;
            this.mnuPrintScreen.Name = "mnuPrintScreen";
            this.mnuPrintScreen.Text = "Print Screen       (Laser/Inkjet)";
            this.mnuPrintScreen.Click += new System.EventHandler(this.mnuPrintScreen_Click);
            // 
            // mnuPrintInterestedParties
            // 
            this.mnuPrintInterestedParties.Index = 11;
            this.mnuPrintInterestedParties.Name = "mnuPrintInterestedParties";
            this.mnuPrintInterestedParties.Text = "Interested Parties";
            this.mnuPrintInterestedParties.Click += new System.EventHandler(this.mnuPrintInterestedParties_Click);
            // 
            // mnuSketch
            // 
            this.mnuSketch.Index = 13;
            this.mnuSketch.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEditSketch,
            this.mnuNewSketch,
            this.mnuEditConfig,
            this.mnuReassignSketch,
            this.mnuImportSQFT,
            this.mnuDeleteSketch});
            this.mnuSketch.Name = "mnuSketch";
            this.mnuSketch.Text = "Sketch";
            this.mnuSketch.Visible = false;
            // 
            // mnuEditSketch
            // 
            this.mnuEditSketch.Index = 0;
            this.mnuEditSketch.Name = "mnuEditSketch";
            this.mnuEditSketch.Text = "Edit Sketch";
            this.mnuEditSketch.Click += new System.EventHandler(this.mnuEditSketch_Click);
            // 
            // mnuNewSketch
            // 
            this.mnuNewSketch.Index = 1;
            this.mnuNewSketch.Name = "mnuNewSketch";
            this.mnuNewSketch.Text = "New Sketch";
            this.mnuNewSketch.Click += new System.EventHandler(this.mnuNewSketch_Click);
            // 
            // mnuEditConfig
            // 
            this.mnuEditConfig.Index = 2;
            this.mnuEditConfig.Name = "mnuEditConfig";
            this.mnuEditConfig.Text = "Edit Config & Phrase Files";
            this.mnuEditConfig.Click += new System.EventHandler(this.mnuEditConfig_Click);
            // 
            // mnuReassignSketch
            // 
            this.mnuReassignSketch.Index = 3;
            this.mnuReassignSketch.Name = "mnuReassignSketch";
            this.mnuReassignSketch.Text = "Re-assign Sketch File";
            this.mnuReassignSketch.Click += new System.EventHandler(this.mnuReassignSketch_Click);
            // 
            // mnuImportSQFT
            // 
            this.mnuImportSQFT.Index = 4;
            this.mnuImportSQFT.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAll,
            this.mnuDwellingSqft,
            this.mnuOutbuildingSQFT});
            this.mnuImportSQFT.Name = "mnuImportSQFT";
            this.mnuImportSQFT.Text = "Import Square Footage from Sketch";
            // 
            // mnuAll
            // 
            this.mnuAll.Index = 0;
            this.mnuAll.Name = "mnuAll";
            this.mnuAll.Text = "All";
            this.mnuAll.Click += new System.EventHandler(this.mnuAll_Click);
            // 
            // mnuDwellingSqft
            // 
            this.mnuDwellingSqft.Index = 1;
            this.mnuDwellingSqft.Name = "mnuDwellingSqft";
            this.mnuDwellingSqft.Text = "Dwelling";
            this.mnuDwellingSqft.Click += new System.EventHandler(this.mnuDwellingSqft_Click);
            // 
            // mnuOutbuildingSQFT
            // 
            this.mnuOutbuildingSQFT.Index = 2;
            this.mnuOutbuildingSQFT.Name = "mnuOutbuildingSQFT";
            this.mnuOutbuildingSQFT.Text = "Outbuilding";
            this.mnuOutbuildingSQFT.Click += new System.EventHandler(this.mnuOutbuildingSQFT_Click);
            // 
            // mnuDeleteSketch
            // 
            this.mnuDeleteSketch.Index = 5;
            this.mnuDeleteSketch.Name = "mnuDeleteSketch";
            this.mnuDeleteSketch.Text = "Delete Sketch";
            this.mnuDeleteSketch.Click += new System.EventHandler(this.mnuDeleteSketch_Click);
            // 
            // mnuPictures
            // 
            this.mnuPictures.Index = 14;
            this.mnuPictures.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintPicture,
            this.mnuImportPicture,
            this.mnuAddPicture,
            this.mnuDeletePicture,
            this.mnuPicMakePrimary,
            this.mnuPicMakeSecondary});
            this.mnuPictures.Name = "mnuPictures";
            this.mnuPictures.Text = "Pictures";
            this.mnuPictures.Visible = false;
            // 
            // mnuPrintPicture
            // 
            this.mnuPrintPicture.Index = 0;
            this.mnuPrintPicture.Name = "mnuPrintPicture";
            this.mnuPrintPicture.Text = "Print Picture";
            this.mnuPrintPicture.Click += new System.EventHandler(this.mnuPrintPicture_Click);
            // 
            // mnuImportPicture
            // 
            this.mnuImportPicture.Index = 1;
            this.mnuImportPicture.Name = "mnuImportPicture";
            this.mnuImportPicture.Text = "";
            // 
            // mnuAddPicture
            // 
            this.mnuAddPicture.Index = 2;
            this.mnuAddPicture.Name = "mnuAddPicture";
            this.mnuAddPicture.Text = "Add Picture";
            this.mnuAddPicture.Click += new System.EventHandler(this.mnuAddPicture_Click);
            // 
            // mnuDeletePicture
            // 
            this.mnuDeletePicture.Index = 3;
            this.mnuDeletePicture.Name = "mnuDeletePicture";
            this.mnuDeletePicture.Text = "Delete Picture";
            this.mnuDeletePicture.Click += new System.EventHandler(this.mnuDeletePicture_Click);
            // 
            // mnuPicMakePrimary
            // 
            this.mnuPicMakePrimary.Index = 4;
            this.mnuPicMakePrimary.Name = "mnuPicMakePrimary";
            this.mnuPicMakePrimary.Text = "Make Current Picture Primary";
            this.mnuPicMakePrimary.Click += new System.EventHandler(this.mnuPicMakePrimary_Click);
            // 
            // mnuPicMakeSecondary
            // 
            this.mnuPicMakeSecondary.Index = 5;
            this.mnuPicMakeSecondary.Name = "mnuPicMakeSecondary";
            this.mnuPicMakeSecondary.Text = "Make Current Picture Secondary";
            this.mnuPicMakeSecondary.Click += new System.EventHandler(this.mnuPicMakeSecondary_Click);
            // 
            // mnuLayout
            // 
            this.mnuLayout.Index = 15;
            this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddOcc,
            this.mnuDelOcc,
            this.mnuAddExpense,
            this.mnuDelExpense});
            this.mnuLayout.Name = "mnuLayout";
            this.mnuLayout.Text = "Income Approach";
            // 
            // mnuAddOcc
            // 
            this.mnuAddOcc.Index = 0;
            this.mnuAddOcc.Name = "mnuAddOcc";
            this.mnuAddOcc.Text = "Add Occupancy Code";
            this.mnuAddOcc.Click += new System.EventHandler(this.mnuAddOcc_Click);
            // 
            // mnuDelOcc
            // 
            this.mnuDelOcc.Index = 1;
            this.mnuDelOcc.Name = "mnuDelOcc";
            this.mnuDelOcc.Text = "Delete Occupancy Code";
            this.mnuDelOcc.Click += new System.EventHandler(this.mnuDelOcc_Click);
            // 
            // mnuAddExpense
            // 
            this.mnuAddExpense.Index = 2;
            this.mnuAddExpense.Name = "mnuAddExpense";
            this.mnuAddExpense.Text = "Add Expense Code";
            this.mnuAddExpense.Click += new System.EventHandler(this.mnuAddExpense_Click);
            // 
            // mnuDelExpense
            // 
            this.mnuDelExpense.Index = 3;
            this.mnuDelExpense.Name = "mnuDelExpense";
            this.mnuDelExpense.Text = "Delete Expense Code";
            this.mnuDelExpense.Click += new System.EventHandler(this.mnuDelExpense_Click);
            // 
            // mnuIncomeSepar1
            // 
            this.mnuIncomeSepar1.Index = -1;
            this.mnuIncomeSepar1.Name = "mnuIncomeSepar1";
            this.mnuIncomeSepar1.Text = "-";
            // 
            // mnuSearch
            // 
            this.mnuSearch.Index = -1;
            this.mnuSearch.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSearchName,
            this.mnuSearchLocation,
            this.mnuSearchMapLot,
            this.mnuPreviousAccountFromSearch,
            this.mnuNextAccountFromSearch});
            this.mnuSearch.Name = "mnuSearch";
            this.mnuSearch.Text = "Search";
            // 
            // mnuSearchName
            // 
            this.mnuSearchName.Enabled = false;
            this.mnuSearchName.Index = 0;
            this.mnuSearchName.Name = "mnuSearchName";
            this.mnuSearchName.Text = "Search by Name";
            this.mnuSearchName.Visible = false;
            // 
            // mnuSearchLocation
            // 
            this.mnuSearchLocation.Enabled = false;
            this.mnuSearchLocation.Index = 1;
            this.mnuSearchLocation.Name = "mnuSearchLocation";
            this.mnuSearchLocation.Text = "Search by Location";
            this.mnuSearchLocation.Visible = false;
            // 
            // mnuSearchMapLot
            // 
            this.mnuSearchMapLot.Enabled = false;
            this.mnuSearchMapLot.Index = 2;
            this.mnuSearchMapLot.Name = "mnuSearchMapLot";
            this.mnuSearchMapLot.Text = "Search by Map/Lot";
            this.mnuSearchMapLot.Visible = false;
            // 
            // mnuPreviousAccountFromSearch
            // 
            this.mnuPreviousAccountFromSearch.Enabled = false;
            this.mnuPreviousAccountFromSearch.Index = 3;
            this.mnuPreviousAccountFromSearch.Name = "mnuPreviousAccountFromSearch";
            this.mnuPreviousAccountFromSearch.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuPreviousAccountFromSearch.Text = "Previous Account from Search";
            this.mnuPreviousAccountFromSearch.Click += new System.EventHandler(this.mnuPreviousAccountFromSearch_Click);
            // 
            // mnuNextAccountFromSearch
            // 
            this.mnuNextAccountFromSearch.Enabled = false;
            this.mnuNextAccountFromSearch.Index = 4;
            this.mnuNextAccountFromSearch.Name = "mnuNextAccountFromSearch";
            this.mnuNextAccountFromSearch.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuNextAccountFromSearch.Text = "Next Account from Search";
            this.mnuNextAccountFromSearch.Click += new System.EventHandler(this.mnuNextAccountFromSearch_Click);
            // 
            // mnuComment
            // 
            this.mnuComment.Index = -1;
            this.mnuComment.Name = "mnuComment";
            this.mnuComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComment.Text = "Comment";
            this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // mnuSeparator4
            // 
            this.mnuSeparator4.Index = -1;
            this.mnuSeparator4.Name = "mnuSeparator4";
            this.mnuSeparator4.Text = "-";
            this.mnuSeparator4.Visible = false;
            // 
            // mnuProcessProperty
            // 
            this.mnuProcessProperty.Index = -1;
            this.mnuProcessProperty.Name = "mnuProcessProperty";
            this.mnuProcessProperty.Text = "";
            // 
            // mnuSep
            // 
            this.mnuSep.Index = -1;
            this.mnuSep.Name = "mnuSep";
            this.mnuSep.Text = "-";
            // 
            // Separator1
            // 
            this.Separator1.Index = -1;
            this.Separator1.Name = "Separator1";
            this.Separator1.Text = "-";
            // 
            // mnuCalculate
            // 
            this.mnuCalculate.Index = -1;
            this.mnuCalculate.Name = "mnuCalculate";
            this.mnuCalculate.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuCalculate.Text = "Calculate this Account";
            this.mnuCalculate.Click += new System.EventHandler(this.mnuCalculate_Click);
            // 
            // mnuSketchSepar2
            // 
            this.mnuSketchSepar2.Index = -1;
            this.mnuSketchSepar2.Name = "mnuSketchSepar2";
            this.mnuSketchSepar2.Text = "-";
            // 
            // mnuSketchSepar1
            // 
            this.mnuSketchSepar1.Index = -1;
            this.mnuSketchSepar1.Name = "mnuSketchSepar1";
            this.mnuSketchSepar1.Text = "-";
            // 
            // mnuPicSepar1
            // 
            this.mnuPicSepar1.Index = -1;
            this.mnuPicSepar1.Name = "mnuPicSepar1";
            this.mnuPicSepar1.Text = "-";
            // 
            // mnuSepar7
            // 
            this.mnuSepar7.Index = -1;
            this.mnuSepar7.Name = "mnuSepar7";
            this.mnuSepar7.Text = "-";
            // 
            // mnuSeparator2
            // 
            this.mnuSeparator2.Index = -1;
            this.mnuSeparator2.Name = "mnuSeparator2";
            this.mnuSeparator2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveQuit
            // 
            this.mnuSaveQuit.Index = -1;
            this.mnuSaveQuit.Name = "mnuSaveQuit";
            this.mnuSaveQuit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveQuit.Text = "Save & Exit";
            this.mnuSaveQuit.Click += new System.EventHandler(this.mnuSaveQuit_Click);
            // 
            // mnuSavePending
            // 
            this.mnuSavePending.Enabled = false;
            this.mnuSavePending.Index = -1;
            this.mnuSavePending.Name = "mnuSavePending";
            this.mnuSavePending.Text = "Save Pending Changes";
            this.mnuSavePending.Visible = false;
            this.mnuSavePending.Click += new System.EventHandler(this.mnuSavePending_Click);
            // 
            // mnuSeparator3
            // 
            this.mnuSeparator3.Index = -1;
            this.mnuSeparator3.Name = "mnuSeparator3";
            this.mnuSeparator3.Text = "-";
            // 
            // mnuQuit
            // 
            this.mnuQuit.Index = -1;
            this.mnuQuit.Name = "mnuQuit";
            this.mnuQuit.Text = "Exit";
            this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
            // 
            // CommonDialog1
            // 
            this.CommonDialog1.Name = "CommonDialog1";
            this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
            // 
            // lblInterestedPartiesPresent
            // 
            this.lblInterestedPartiesPresent.Location = new System.Drawing.Point(405, 30);
            this.lblInterestedPartiesPresent.Name = "lblInterestedPartiesPresent";
            this.lblInterestedPartiesPresent.Size = new System.Drawing.Size(14, 17);
            this.lblInterestedPartiesPresent.TabIndex = 313;
            this.lblInterestedPartiesPresent.Text = "I";
            this.lblInterestedPartiesPresent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblInterestedPartiesPresent, "Indicates that this account has an interested party");
            this.lblInterestedPartiesPresent.Visible = false;
            // 
            // lblMRHLAcctNum
            // 
            this.lblMRHLAcctNum.Location = new System.Drawing.Point(30, 44);
            this.lblMRHLAcctNum.Name = "lblMRHLAcctNum";
            this.lblMRHLAcctNum.Size = new System.Drawing.Size(130, 16);
            this.lblMRHLAcctNum.TabIndex = 48;
            this.lblMRHLAcctNum.Text = "ACCOUNT";
            // 
            // lblComment
            // 
            this.lblComment.Location = new System.Drawing.Point(367, 30);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(14, 16);
            this.lblComment.TabIndex = 47;
            this.lblComment.Text = "C";
            this.lblComment.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblComment, "Indicates that this account has a comment.");
            this.lblComment.Visible = false;
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(165, 44);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(35, 18);
            this.Label10.TabIndex = 46;
            this.Label10.Text = "CARD";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(306, 44);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(25, 19);
            this.Label11.TabIndex = 45;
            this.Label11.Text = "OF";
            // 
            // lblNumCards
            // 
            this.lblNumCards.Location = new System.Drawing.Point(333, 44);
            this.lblNumCards.Name = "lblNumCards";
            this.lblNumCards.Size = new System.Drawing.Size(25, 19);
            this.lblNumCards.TabIndex = 44;
            // 
            // Label65_18
            // 
            this.Label65_18.Location = new System.Drawing.Point(559, 30);
            this.Label65_18.Name = "Label65_18";
            this.Label65_18.Size = new System.Drawing.Size(70, 15);
            this.Label65_18.TabIndex = 43;
            this.Label65_18.Text = "INFO CODE";
            this.Label65_18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label65_8
            // 
            this.Label65_8.Location = new System.Drawing.Point(439, 30);
            this.Label65_8.Name = "Label65_8";
            this.Label65_8.TabIndex = 42;
            this.Label65_8.Text = "ENTRANCE CODE";
            this.Label65_8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label65_9
            // 
            this.Label65_9.Location = new System.Drawing.Point(660, 30);
            this.Label65_9.Name = "Label65_9";
            this.Label65_9.Size = new System.Drawing.Size(110, 14);
            this.Label65_9.TabIndex = 41;
            this.Label65_9.Text = "DATE INSPECTED";
            this.Label65_9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPendingFlag
            // 
            this.lblPendingFlag.Location = new System.Drawing.Point(386, 30);
            this.lblPendingFlag.Name = "lblPendingFlag";
            this.lblPendingFlag.Size = new System.Drawing.Size(14, 16);
            this.lblPendingFlag.TabIndex = 40;
            this.lblPendingFlag.Text = "P";
            this.lblPendingFlag.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblPendingFlag, "Indicates that this account has pending activity or transfers.");
            this.lblPendingFlag.Visible = false;
            // 
            // lblPicturePresent
            // 
            this.lblPicturePresent.Location = new System.Drawing.Point(367, 48);
            this.lblPicturePresent.Name = "lblPicturePresent";
            this.lblPicturePresent.Size = new System.Drawing.Size(14, 16);
            this.lblPicturePresent.TabIndex = 39;
            this.lblPicturePresent.Text = "P";
            this.lblPicturePresent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblPicturePresent, "Indicates that this account has a picture");
            this.lblPicturePresent.Visible = false;
            // 
            // lblSketchPresent
            // 
            this.lblSketchPresent.Location = new System.Drawing.Point(386, 48);
            this.lblSketchPresent.Name = "lblSketchPresent";
            this.lblSketchPresent.Size = new System.Drawing.Size(14, 16);
            this.lblSketchPresent.TabIndex = 38;
            this.lblSketchPresent.Text = "S";
            this.lblSketchPresent.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblSketchPresent, "Indicates that this account has a sketch");
            this.lblSketchPresent.Visible = false;
            // 
            // lblDate_2
            // 
            this.lblDate_2.Location = new System.Drawing.Point(30, 118);
            this.lblDate_2.Name = "lblDate_2";
            this.lblDate_2.Size = new System.Drawing.Size(60, 16);
            this.lblDate_2.TabIndex = 37;
            this.lblDate_2.Text = "MAP  LOT";
            // 
            // Label8_6
            // 
            this.Label8_6.Location = new System.Drawing.Point(307, 118);
            this.Label8_6.Name = "Label8_6";
            this.Label8_6.Size = new System.Drawing.Size(65, 16);
            this.Label8_6.TabIndex = 36;
            this.Label8_6.Text = "LOCATION";
            // 
            // cmdComment
            // 
            this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComment.Cursor = Wisej.Web.Cursors.Default;
            this.cmdComment.Location = new System.Drawing.Point(636, 29);
            this.cmdComment.Name = "cmdComment";
            this.cmdComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComment.Size = new System.Drawing.Size(78, 24);
            this.cmdComment.TabIndex = 5;
            this.cmdComment.Text = "Comment";
            this.cmdComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // cmdCalculate
            // 
            this.cmdCalculate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCalculate.Cursor = Wisej.Web.Cursors.Default;
            this.cmdCalculate.Location = new System.Drawing.Point(515, 29);
            this.cmdCalculate.Name = "cmdCalculate";
            this.cmdCalculate.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdCalculate.Size = new System.Drawing.Size(115, 24);
            this.cmdCalculate.TabIndex = 6;
            this.cmdCalculate.Text = "Calculate";
            this.cmdCalculate.Click += new System.EventHandler(this.mnuCalculate_Click);
            // 
            // cmdPreviousAccountFromSearch
            // 
            this.cmdPreviousAccountFromSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPreviousAccountFromSearch.Cursor = Wisej.Web.Cursors.Default;
            this.cmdPreviousAccountFromSearch.Enabled = false;
            this.cmdPreviousAccountFromSearch.Location = new System.Drawing.Point(729, 29);
            this.cmdPreviousAccountFromSearch.Name = "cmdPreviousAccountFromSearch";
            this.cmdPreviousAccountFromSearch.Shortcut = Wisej.Web.Shortcut.F7;
            this.cmdPreviousAccountFromSearch.Size = new System.Drawing.Size(202, 24);
            this.cmdPreviousAccountFromSearch.TabIndex = 7;
            this.cmdPreviousAccountFromSearch.Text = "Previous Account from Search";
            this.cmdPreviousAccountFromSearch.Click += new System.EventHandler(this.mnuPreviousAccountFromSearch_Click);
            // 
            // cmdNextAccountFromSearch
            // 
            this.cmdNextAccountFromSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNextAccountFromSearch.Cursor = Wisej.Web.Cursors.Default;
            this.cmdNextAccountFromSearch.Enabled = false;
            this.cmdNextAccountFromSearch.Location = new System.Drawing.Point(938, 29);
            this.cmdNextAccountFromSearch.Name = "cmdNextAccountFromSearch";
            this.cmdNextAccountFromSearch.Shortcut = Wisej.Web.Shortcut.F8;
            this.cmdNextAccountFromSearch.Size = new System.Drawing.Size(176, 24);
            this.cmdNextAccountFromSearch.TabIndex = 8;
            this.cmdNextAccountFromSearch.Text = "Next Account from Search";
            this.cmdNextAccountFromSearch.Click += new System.EventHandler(this.mnuNextAccountFromSearch_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Cursor = Wisej.Web.Cursors.Default;
            this.cmdSave.Location = new System.Drawing.Point(407, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(150, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmbCardNumber
            // 
            this.cmbCardNumber.Location = new System.Drawing.Point(208, 30);
            this.cmbCardNumber.Name = "cmbCardNumber";
            this.cmbCardNumber.Size = new System.Drawing.Size(70, 40);
            this.cmbCardNumber.TabIndex = 314;
            this.cmbCardNumber.SelectedIndexChanged += new System.EventHandler(this.cmbCardNumber_SelectedIndexChanged);
            // 
            // winSketchImageTimer
            // 
            this.winSketchImageTimer.Tick += new System.EventHandler(this.winSketchImageTimer_Tick);
            // 
            // frmNewREProperty
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.Cursor = Wisej.Web.Cursors.Default;
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmNewREProperty";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Account Maintenance";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmNewREProperty_Load);
            this.Activated += new System.EventHandler(this.frmNewREProperty_Activated);
            this.Resize += new System.EventHandler(this.frmNewREProperty_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNewREProperty_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmNewREProperty_KeyPress);
            this.KeyUp += new Wisej.Web.KeyEventHandler(this.frmNewREProperty_KeyUp);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ToolTip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridToolTip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDateInspected)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SaleGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRLivingTrust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLogSale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBankruptcy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTaxAcquired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framRegOwner)).EndInit();
            this.framRegOwner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdChangeOwnership)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveSecOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditSecOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchSecOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridExempts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BPGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTranCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDocuments)).EndInit();
            this.SSTab1_Page2.ResumeLayout(false);
            this.SSTab1_Page2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkZoneOverride)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLandCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBldgCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPropertyCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridNeighborhood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridSecZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridTopography)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridUtilities)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridStreet)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridDwelling1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDwelling2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDwelling3)).EndInit();
            this.SSTab1_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.SSTab1_Page5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSound_0)).EndInit();
            this.SSTab1_Page6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridExpenses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
            this.SSTab1_Page7.ResumeLayout(false);
            this.SSTab1_Page7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Toolbar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNextPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrevPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPicture)).EndInit();
            this.framPicture.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPicDimensions)).EndInit();
            this.framPicDimensions.ResumeLayout(false);
            this.SSTab1_Page8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.framSketch)).EndInit();
            this.framSketch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imSketch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNextSketch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrevSketch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalculate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreviousAccountFromSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNextAccountFromSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdComment;
		public FCButton cmdCalculate;
		public FCButton cmdPreviousAccountFromSearch;
		public FCButton cmdNextAccountFromSearch;
		public FCButton cmdSave;
        public FCTextBox txtDeedName2;
        public FCTextBox txtDeedName1;
        public FCLabel fcLabel1;
        public FCLabel fcLabel2;
        public FCComboBox cmbCardNumber;
		private FCToolStripMenuItem mnuViewGroupInformation;
		private FCToolStripMenuItem mnuViewMortgageInformation;
	}
}
