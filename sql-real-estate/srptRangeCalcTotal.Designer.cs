﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptRangeCalcTotal.
	/// </summary>
	partial class srptRangeCalcTotal
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptRangeCalcTotal));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtCard = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCLandT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCBLDGT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCTT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLandT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCLandT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCBLDGT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCTT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCard,
				this.txtCLand,
				this.txtCBldg,
				this.txtCTotal,
				this.txtLand,
				this.txtBldg,
				this.txtTotal
			});
			this.Detail.Height = 0.1458333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Line1,
				this.Line2,
				this.Label9,
				this.Line3,
				this.Line4
			});
			this.ReportHeader.Height = 0.3645833F;
			this.ReportHeader.Name = "ReportHeader";
			this.ReportHeader.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line5,
				this.Label10,
				this.txtCLandT,
				this.txtCBLDGT,
				this.txtCTT,
				this.txtLandT,
				this.txtBldgT,
				this.txtTT
			});
			this.ReportFooter.Height = 0.2708333F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label1.Tag = "bold";
			this.Label1.Text = "Land";
			this.Label1.Top = 0.1875F;
			this.Label1.Width = 0.875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.0625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Tag = "bold";
			this.Label2.Text = "Card";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 0.4375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.4375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label3.Tag = "bold";
			this.Label3.Text = "Building";
			this.Label3.Top = 0.1875F;
			this.Label3.Width = 1.1875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label4.Tag = "bold";
			this.Label4.Text = "Calc. Total";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.1875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label5.Tag = "bold";
			this.Label5.Text = "Land";
			this.Label5.Top = 0.1875F;
			this.Label5.Width = 0.9375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label6.Tag = "bold";
			this.Label6.Text = "Building";
			this.Label6.Top = 0.1875F;
			this.Label6.Width = 1.1875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.25F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label7.Tag = "bold";
			this.Label7.Text = "Total";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.1875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 1.5F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label8.Tag = "bold";
			this.Label8.Text = "Calculated";
			this.Label8.Top = 0.03125F;
			this.Label8.Width = 1.3125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.5F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.09375F;
			this.Line1.Width = 1F;
			this.Line1.X1 = 0.5F;
			this.Line1.X2 = 1.5F;
			this.Line1.Y1 = 0.09375F;
			this.Line1.Y2 = 0.09375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.8125F;
			this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.09375F;
			this.Line2.Width = 1.0625F;
			this.Line2.X1 = 2.8125F;
			this.Line2.X2 = 3.875F;
			this.Line2.Y1 = 0.09375F;
			this.Line2.Y2 = 0.09375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 5F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label9.Tag = "bold";
			this.Label9.Text = "Correlated";
			this.Label9.Top = 0.03125F;
			this.Label9.Width = 1.375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 6.4375F;
			this.Line3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.09375F;
			this.Line3.Width = 1F;
			this.Line3.X1 = 6.4375F;
			this.Line3.X2 = 7.4375F;
			this.Line3.Y1 = 0.09375F;
			this.Line3.Y2 = 0.09375F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 4F;
			this.Line4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.09375F;
			this.Line4.Width = 1F;
			this.Line4.X1 = 4F;
			this.Line4.X2 = 5F;
			this.Line4.Y1 = 0.09375F;
			this.Line4.Y2 = 0.09375F;
			// 
			// txtCard
			// 
			this.txtCard.Height = 0.19F;
			this.txtCard.Left = 0.0625F;
			this.txtCard.Name = "txtCard";
			this.txtCard.Style = "font-family: \'Tahoma\'";
			this.txtCard.Tag = "text";
			this.txtCard.Text = "Field1";
			this.txtCard.Top = 0F;
			this.txtCard.Width = 0.375F;
			// 
			// txtCLand
			// 
			this.txtCLand.Height = 0.19F;
			this.txtCLand.Left = 0.5625F;
			this.txtCLand.Name = "txtCLand";
			this.txtCLand.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCLand.Tag = "text";
			this.txtCLand.Text = "Field2";
			this.txtCLand.Top = 0F;
			this.txtCLand.Width = 0.875F;
			// 
			// txtCBldg
			// 
			this.txtCBldg.Height = 0.19F;
			this.txtCBldg.Left = 1.5F;
			this.txtCBldg.Name = "txtCBldg";
			this.txtCBldg.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCBldg.Tag = "text";
			this.txtCBldg.Text = "Field3";
			this.txtCBldg.Top = 0F;
			this.txtCBldg.Width = 1.1875F;
			// 
			// txtCTotal
			// 
			this.txtCTotal.Height = 0.19F;
			this.txtCTotal.Left = 2.75F;
			this.txtCTotal.Name = "txtCTotal";
			this.txtCTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCTotal.Tag = "text";
			this.txtCTotal.Text = "Field4";
			this.txtCTotal.Top = 0F;
			this.txtCTotal.Width = 1.1875F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.19F;
			this.txtLand.Left = 4F;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLand.Tag = "text";
			this.txtLand.Text = "Field5";
			this.txtLand.Top = 0F;
			this.txtLand.Width = 0.9375F;
			// 
			// txtBldg
			// 
			this.txtBldg.Height = 0.19F;
			this.txtBldg.Left = 5F;
			this.txtBldg.Name = "txtBldg";
			this.txtBldg.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBldg.Tag = "text";
			this.txtBldg.Text = "Field6";
			this.txtBldg.Top = 0F;
			this.txtBldg.Width = 1.1875F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.19F;
			this.txtTotal.Left = 6.25F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotal.Tag = "text";
			this.txtTotal.Text = "Field7";
			this.txtTotal.Top = 0F;
			this.txtTotal.Width = 1.1875F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0.0625F;
			this.Line5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
			this.Line5.LineWeight = 3F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0.03125F;
			this.Line5.Width = 7.375F;
			this.Line5.X1 = 0.0625F;
			this.Line5.X2 = 7.4375F;
			this.Line5.Y1 = 0.03125F;
			this.Line5.Y2 = 0.03125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label10.Tag = "bold";
			this.Label10.Text = "TOTAL";
			this.Label10.Top = 0.0625F;
			this.Label10.Width = 0.5F;
			// 
			// txtCLandT
			// 
			this.txtCLandT.Height = 0.19F;
			this.txtCLandT.Left = 0.5625F;
			this.txtCLandT.Name = "txtCLandT";
			this.txtCLandT.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCLandT.Tag = "text";
			this.txtCLandT.Text = "Field8";
			this.txtCLandT.Top = 0.0625F;
			this.txtCLandT.Width = 0.875F;
			// 
			// txtCBLDGT
			// 
			this.txtCBLDGT.Height = 0.19F;
			this.txtCBLDGT.Left = 1.5F;
			this.txtCBLDGT.Name = "txtCBLDGT";
			this.txtCBLDGT.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCBLDGT.Tag = "text";
			this.txtCBLDGT.Text = "Field9";
			this.txtCBLDGT.Top = 0.0625F;
			this.txtCBLDGT.Width = 1.1875F;
			// 
			// txtCTT
			// 
			this.txtCTT.Height = 0.19F;
			this.txtCTT.Left = 2.75F;
			this.txtCTT.Name = "txtCTT";
			this.txtCTT.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtCTT.Tag = "text";
			this.txtCTT.Text = "Field10";
			this.txtCTT.Top = 0.0625F;
			this.txtCTT.Width = 1.1875F;
			// 
			// txtLandT
			// 
			this.txtLandT.Height = 0.19F;
			this.txtLandT.Left = 4F;
			this.txtLandT.Name = "txtLandT";
			this.txtLandT.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtLandT.Tag = "text";
			this.txtLandT.Text = "Field11";
			this.txtLandT.Top = 0.0625F;
			this.txtLandT.Width = 0.9375F;
			// 
			// txtBldgT
			// 
			this.txtBldgT.Height = 0.19F;
			this.txtBldgT.Left = 5F;
			this.txtBldgT.Name = "txtBldgT";
			this.txtBldgT.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBldgT.Tag = "text";
			this.txtBldgT.Text = "Field12";
			this.txtBldgT.Top = 0.0625F;
			this.txtBldgT.Width = 1.1875F;
			// 
			// txtTT
			// 
			this.txtTT.Height = 0.19F;
			this.txtTT.Left = 6.25F;
			this.txtTT.Name = "txtTT";
			this.txtTT.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTT.Tag = "text";
			this.txtTT.Text = "Field13";
			this.txtTT.Top = 0.0625F;
			this.txtTT.Width = 1.1875F;
			// 
			// srptRangeCalcTotal
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCard)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCLandT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCBLDGT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCTT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCard;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCLandT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCBLDGT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCTT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTT;
	}
}
