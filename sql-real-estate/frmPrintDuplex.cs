﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPrintDuplex.
	/// </summary>
	public partial class frmPrintDuplex : BaseForm
	{
		public frmPrintDuplex()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrintDuplex InstancePtr
		{
			get
			{
				return (frmPrintDuplex)Sys.GetInstance(typeof(frmPrintDuplex));
			}
		}

		protected frmPrintDuplex _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strDuplex;

		private void frmPrintDuplex_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintDuplex properties;
			//frmPrintDuplex.ScaleWidth	= 5880;
			//frmPrintDuplex.ScaleHeight	= 3810;
			//frmPrintDuplex.LinkTopic	= "Form1";
			//End Unmaped Properties
			string strTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			strTemp = modRegistry.GetRegistryKey("PrintDuplex");
			if (Strings.UCase(Strings.Trim(strTemp)) == "NONE")
			{
				cmbDuplex.Text = "None (Print pages on separate pieces of paper)";
			}
			else if (Strings.UCase(Strings.Trim(strTemp)) == "FULL")
			{
				cmbDuplex.Text = "Duplex (Printer can print on both sides of paper)";
			}
			else if (Strings.UCase(Strings.Trim(strTemp)) == "MANUALTOPISLAST")
			{
				cmbDuplex.Text = "After flipping, top page is the last page";
			}
			else if (Strings.UCase(Strings.Trim(strTemp)) == "MANUALTOPISFIRST")
			{
				cmbDuplex.Text = "After flipping, top page is the first page";
			}
			else
			{
				cmbDuplex.Text = "None (Print pages on separate pieces of paper)";
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			strDuplex = "";
			this.Unload();
		}

		public string Init()
		{
			string Init = "";
			Init = "";
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strDuplex;
			return Init;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (cmbDuplex.Text == "None (Print pages on separate pieces of paper)")
			{
				strDuplex = "NONE";
			}
			else if (cmbDuplex.Text == "Duplex (Printer can print on both sides of paper)")
			{
				strDuplex = "FULL";
			}
			else if (cmbDuplex.Text == "After flipping, top page is the last page")
			{
				strDuplex = "MANUALTOPISLAST";
			}
			else if (cmbDuplex.Text == "After flipping, top page is the first page")
			{
				strDuplex = "MANUALTOPISFIRST";
			}
			// save choice as registry setting
			modRegistry.SaveRegistryKey("PrintDuplex", strDuplex);
			this.Unload();
		}
	}
}
