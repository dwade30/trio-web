﻿namespace TWRE0000
{
    public enum REListingField
    {   
        Account = 1,
        Name = 2,
        Map = 3,
        Location = 4
    }
}