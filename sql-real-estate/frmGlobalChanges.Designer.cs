﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGlobalChanges.
	/// </summary>
	partial class frmGlobalChanges : BaseForm
	{
		public fecherFoundation.FCComboBox cmbChoose;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtEndRange;
		public fecherFoundation.FCTextBox txtOriginal;
		public FCGrid gridPropertyCode;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCTextBox txtNew;
		public FCGrid Grid;
		public fecherFoundation.FCFrame framLandSched;
		public fecherFoundation.FCTextBox txtSchedule;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public FCGrid gridNewPropertyCode;
		public fecherFoundation.FCLabel lblCurrent;
		public fecherFoundation.FCLabel Label2;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGlobalChanges));
			this.cmbChoose = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.txtEndRange = new fecherFoundation.FCTextBox();
			this.gridPropertyCode = new fecherFoundation.FCGrid();
			this.txtOriginal = new fecherFoundation.FCTextBox();
			this.txtNew = new fecherFoundation.FCTextBox();
			this.Grid = new fecherFoundation.FCGrid();
			this.framLandSched = new fecherFoundation.FCFrame();
			this.txtSchedule = new fecherFoundation.FCTextBox();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.gridNewPropertyCode = new fecherFoundation.FCGrid();
			this.lblCurrent = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.cmdChange = new Wisej.Web.Button();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridPropertyCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framLandSched)).BeginInit();
			this.framLandSched.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridNewPropertyCode)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdChange);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblCurrent);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Controls.Add(this.framLandSched);
			this.ClientArea.Controls.Add(this.txtNew);
			this.ClientArea.Controls.Add(this.gridNewPropertyCode);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(189, 30);
			this.HeaderText.Text = "Global Changes";
			// 
			// cmbChoose
			// 
			this.cmbChoose.AutoSize = false;
			this.cmbChoose.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbChoose.FormattingEnabled = true;
			this.cmbChoose.Items.AddRange(new object[] {
				"Initial Value",
				"Map / Lot Range"
			});
			this.cmbChoose.Location = new System.Drawing.Point(20, 30);
			this.cmbChoose.Name = "cmbChoose";
			this.cmbChoose.Size = new System.Drawing.Size(367, 40);
			this.cmbChoose.TabIndex = 10;
			this.cmbChoose.Text = "Initial Value";
			this.cmbChoose.SelectedIndexChanged += new System.EventHandler(this.optChoose_CheckedChanged);
			// 
			// Frame1
			// 
			this.Frame1.BackColor = System.Drawing.Color.White;
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Controls.Add(this.txtEndRange);
			this.Frame1.Controls.Add(this.cmbChoose);
			this.Frame1.Controls.Add(this.gridPropertyCode);
			this.Frame1.Controls.Add(this.txtOriginal);
			this.Frame1.Location = new System.Drawing.Point(400, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(407, 285);
			this.Frame1.TabIndex = 4;
			this.Frame1.Text = "Choose Accounts By";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(195, 188);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(33, 17);
			this.Label3.TabIndex = 10;
			this.Label3.Text = "TO";
			this.Label3.Visible = false;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 90);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(110, 18);
			this.Label1.TabIndex = 8;
			this.Label1.Text = "ORIGINAL VALUE";
			// 
			// txtEndRange
			// 
			this.txtEndRange.AutoSize = false;
			this.txtEndRange.BackColor = System.Drawing.SystemColors.Window;
			this.txtEndRange.LinkItem = null;
			this.txtEndRange.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEndRange.LinkTopic = null;
			this.txtEndRange.Location = new System.Drawing.Point(20, 225);
			this.txtEndRange.Name = "txtEndRange";
			this.txtEndRange.Size = new System.Drawing.Size(367, 40);
			this.txtEndRange.TabIndex = 9;
			this.txtEndRange.Visible = false;
			// 
			// gridPropertyCode
			// 
			this.gridPropertyCode.AllowSelection = false;
			this.gridPropertyCode.AllowUserToResizeColumns = false;
			this.gridPropertyCode.AllowUserToResizeRows = false;
			this.gridPropertyCode.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridPropertyCode.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridPropertyCode.BackColorBkg = System.Drawing.Color.Empty;
			this.gridPropertyCode.BackColorFixed = System.Drawing.Color.Empty;
			this.gridPropertyCode.BackColorSel = System.Drawing.Color.Empty;
			this.gridPropertyCode.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.gridPropertyCode.Cols = 1;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridPropertyCode.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.gridPropertyCode.ColumnHeadersHeight = 30;
			this.gridPropertyCode.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridPropertyCode.ColumnHeadersVisible = false;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridPropertyCode.DefaultCellStyle = dataGridViewCellStyle6;
			this.gridPropertyCode.DragIcon = null;
			this.gridPropertyCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridPropertyCode.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridPropertyCode.ExtendLastCol = true;
			this.gridPropertyCode.FixedCols = 0;
			this.gridPropertyCode.FixedRows = 0;
			this.gridPropertyCode.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridPropertyCode.FrozenCols = 0;
			this.gridPropertyCode.GridColor = System.Drawing.Color.Empty;
			this.gridPropertyCode.GridColorFixed = System.Drawing.Color.Empty;
			this.gridPropertyCode.Location = new System.Drawing.Point(20, 128);
			this.gridPropertyCode.Name = "gridPropertyCode";
			this.gridPropertyCode.RowHeadersVisible = false;
			this.gridPropertyCode.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridPropertyCode.RowHeightMin = 0;
			this.gridPropertyCode.Rows = 1;
			this.gridPropertyCode.ScrollTipText = null;
			this.gridPropertyCode.ShowColumnVisibilityMenu = false;
			this.gridPropertyCode.Size = new System.Drawing.Size(367, 42);
			this.gridPropertyCode.StandardTab = true;
			this.gridPropertyCode.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridPropertyCode.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridPropertyCode.TabIndex = 15;
			this.gridPropertyCode.Tag = "land";
			this.gridPropertyCode.Visible = false;
			this.gridPropertyCode.ComboDropDown += new System.EventHandler(this.gridPropertyCode_ComboDropDown);
			this.gridPropertyCode.ComboCloseUp += new System.EventHandler(this.gridPropertyCode_ComboCloseUp);
			// 
			// txtOriginal
			// 
			this.txtOriginal.AutoSize = false;
			this.txtOriginal.BackColor = System.Drawing.SystemColors.Window;
			this.txtOriginal.LinkItem = null;
			this.txtOriginal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOriginal.LinkTopic = null;
			this.txtOriginal.Location = new System.Drawing.Point(20, 128);
			this.txtOriginal.Name = "txtOriginal";
			this.txtOriginal.Size = new System.Drawing.Size(367, 40);
			this.txtOriginal.TabIndex = 7;
			// 
			// txtNew
			// 
			this.txtNew.AutoSize = false;
			this.txtNew.BackColor = System.Drawing.SystemColors.Window;
			this.txtNew.LinkItem = null;
			this.txtNew.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNew.LinkTopic = null;
			this.txtNew.Location = new System.Drawing.Point(400, 380);
			this.txtNew.Name = "txtNew";
			this.txtNew.Size = new System.Drawing.Size(205, 40);
			this.txtNew.TabIndex = 1;
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid.DragIcon = null;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ExtendLastCol = true;
			this.Grid.FixedCols = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 30);
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(350, 390);
			this.Grid.StandardTab = true;
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 0;
			this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
			// 
			// framLandSched
			// 
			this.framLandSched.BackColor = System.Drawing.Color.White;
			this.framLandSched.Controls.Add(this.txtSchedule);
			this.framLandSched.Controls.Add(this.Label5);
			this.framLandSched.Controls.Add(this.Label4);
			this.framLandSched.Location = new System.Drawing.Point(400, 30);
			this.framLandSched.Name = "framLandSched";
			this.framLandSched.Size = new System.Drawing.Size(163, 193);
			this.framLandSched.TabIndex = 11;
			this.framLandSched.Visible = false;
			// 
			// txtSchedule
			// 
			this.txtSchedule.AutoSize = false;
			this.txtSchedule.BackColor = System.Drawing.SystemColors.Window;
			this.txtSchedule.LinkItem = null;
			this.txtSchedule.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSchedule.LinkTopic = null;
			this.txtSchedule.Location = new System.Drawing.Point(20, 131);
			this.txtSchedule.Name = "txtSchedule";
			this.txtSchedule.Size = new System.Drawing.Size(123, 40);
			this.txtSchedule.TabIndex = 13;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 101);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(123, 19);
			this.Label5.TabIndex = 14;
			this.Label5.Text = "CODE";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(12, 26);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(139, 36);
			this.Label4.TabIndex = 12;
			this.Label4.Text = "UPDATE ALL LAND SCHEDULES";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// gridNewPropertyCode
			// 
			this.gridNewPropertyCode.AllowSelection = false;
			this.gridNewPropertyCode.AllowUserToResizeColumns = false;
			this.gridNewPropertyCode.AllowUserToResizeRows = false;
			this.gridNewPropertyCode.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridNewPropertyCode.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridNewPropertyCode.BackColorBkg = System.Drawing.Color.Empty;
			this.gridNewPropertyCode.BackColorFixed = System.Drawing.Color.Empty;
			this.gridNewPropertyCode.BackColorSel = System.Drawing.Color.Empty;
			this.gridNewPropertyCode.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.gridNewPropertyCode.Cols = 1;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridNewPropertyCode.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.gridNewPropertyCode.ColumnHeadersHeight = 30;
			this.gridNewPropertyCode.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridNewPropertyCode.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridNewPropertyCode.DefaultCellStyle = dataGridViewCellStyle4;
			this.gridNewPropertyCode.DragIcon = null;
			this.gridNewPropertyCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridNewPropertyCode.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridNewPropertyCode.ExtendLastCol = true;
			this.gridNewPropertyCode.FixedCols = 0;
			this.gridNewPropertyCode.FixedRows = 0;
			this.gridNewPropertyCode.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridNewPropertyCode.FrozenCols = 0;
			this.gridNewPropertyCode.GridColor = System.Drawing.Color.Empty;
			this.gridNewPropertyCode.GridColorFixed = System.Drawing.Color.Empty;
			this.gridNewPropertyCode.Location = new System.Drawing.Point(400, 382);
			this.gridNewPropertyCode.Name = "gridNewPropertyCode";
			this.gridNewPropertyCode.RowHeadersVisible = false;
			this.gridNewPropertyCode.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridNewPropertyCode.RowHeightMin = 0;
			this.gridNewPropertyCode.Rows = 1;
			this.gridNewPropertyCode.ScrollTipText = null;
			this.gridNewPropertyCode.ShowColumnVisibilityMenu = false;
			this.gridNewPropertyCode.Size = new System.Drawing.Size(387, 42);
			this.gridNewPropertyCode.StandardTab = true;
			this.gridNewPropertyCode.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridNewPropertyCode.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridNewPropertyCode.TabIndex = 16;
			this.gridNewPropertyCode.Tag = "land";
			this.gridNewPropertyCode.Visible = false;
			this.gridNewPropertyCode.ComboDropDown += new System.EventHandler(this.gridNewPropertyCode_ComboDropDown);
			this.gridNewPropertyCode.ComboCloseUp += new System.EventHandler(this.gridNewPropertyCode_ComboCloseUp);
			// 
			// lblCurrent
			// 
			this.lblCurrent.AutoSize = true;
			this.lblCurrent.Location = new System.Drawing.Point(404, 72);
			this.lblCurrent.Name = "lblCurrent";
			this.lblCurrent.Size = new System.Drawing.Size(4, 14);
			this.lblCurrent.TabIndex = 3;
			this.lblCurrent.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(400, 345);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(80, 15);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "CHANGE TO";
			// 
			// cmdChange
			// 
			this.cmdChange.AppearanceKey = "acceptButton";
			this.cmdChange.Location = new System.Drawing.Point(314, 30);
			this.cmdChange.Name = "cmdChange";
			this.cmdChange.Size = new System.Drawing.Size(100, 48);
			this.cmdChange.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdChange.TabIndex = 0;
			this.cmdChange.Text = "Change";
			this.cmdChange.Click += new System.EventHandler(this.mnuChange_Click);
			// 
			// frmGlobalChanges
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(834, 588);
			this.FillColor = 0;
			this.Name = "frmGlobalChanges";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Global Changes";
			this.Load += new System.EventHandler(this.frmGlobalChanges_Load);
			this.Resize += new System.EventHandler(this.frmGlobalChanges_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridPropertyCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framLandSched)).EndInit();
			this.framLandSched.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridNewPropertyCode)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdChange;
	}
}
