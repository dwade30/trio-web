//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCustomLabels.
	/// </summary>
	partial class frmCustomLabels : BaseForm
	{
		public fecherFoundation.FCCheckBox chkChooseLabelStart;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCFrame fraType;
		public fecherFoundation.FCComboBox cmbLabelType;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public Wisej.Web.ImageList ImageList1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomLabels));
            Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.chkChooseLabelStart = new fecherFoundation.FCCheckBox();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.cmdClear = new fecherFoundation.FCButton();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.Line1 = new fecherFoundation.FCLine();
            this.fraType = new fecherFoundation.FCFrame();
            this.cmbLabelType = new fecherFoundation.FCComboBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkChooseLabelStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            this.vsLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).BeginInit();
            this.fraType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 627);
            this.BottomPanel.Size = new System.Drawing.Size(891, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkChooseLabelStart);
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(891, 567);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(891, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(84, 30);
            this.HeaderText.Text = "Labels";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // chkChooseLabelStart
            // 
            this.chkChooseLabelStart.Location = new System.Drawing.Point(570, 250);
            this.chkChooseLabelStart.Name = "chkChooseLabelStart";
            this.chkChooseLabelStart.Size = new System.Drawing.Size(229, 27);
            this.chkChooseLabelStart.TabIndex = 16;
            this.chkChooseLabelStart.Text = "Choose Label to Start From";
            this.ToolTip1.SetToolTip(this.chkChooseLabelStart, "Choose this option if this sheet is already partially printed on");
            this.chkChooseLabelStart.Visible = false;
            // 
            // fraWhere
            // 
            this.fraWhere.AppearanceKey = "groupBoxNoBorders";
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 470);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(520, 250);
            this.fraWhere.TabIndex = 5;
            this.fraWhere.Text = "Select Search Criteria";
            this.ToolTip1.SetToolTip(this.fraWhere, null);
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.Cols = 10;
            this.vsWhere.ColumnHeadersVisible = false;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.ExtendLastCol = true;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.Location = new System.Drawing.Point(0, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.ReadOnly = false;
            this.vsWhere.Rows = 0;
            this.vsWhere.Size = new System.Drawing.Size(520, 215);
            this.vsWhere.StandardTab = false;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsWhere.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.vsWhere, null);
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(713, 26);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(150, 24);
            this.cmdClear.TabIndex = 7;
            this.cmdClear.Text = "Clear Search Criteria";
            this.ToolTip1.SetToolTip(this.cmdClear, null);
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // fraSort
            // 
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Location = new System.Drawing.Point(300, 250);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(250, 200);
            this.fraSort.TabIndex = 3;
            this.fraSort.Text = "Fields To Sort By";
            this.ToolTip1.SetToolTip(this.fraSort, null);
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(210, 150);
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.lstSort, null);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(388, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(130, 48);
            this.cmdPrint.TabIndex = 1;
            this.cmdPrint.Text = "Print Labels";
            this.ToolTip1.SetToolTip(this.cmdPrint, null);
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // fraFields
            // 
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Location = new System.Drawing.Point(30, 250);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(250, 200);
            this.fraFields.TabIndex = 17;
            this.fraFields.Text = "Get Labels From";
            this.ToolTip1.SetToolTip(this.fraFields, null);
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // lstFields
            // 
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.Location = new System.Drawing.Point(20, 30);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(210, 150);
            this.lstFields.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.lstFields, null);
            this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.vsLayout);
            this.Frame1.Controls.Add(this.fraType);
            this.Frame1.Controls.Add(this.fraMessage);
            this.Frame1.Location = new System.Drawing.Point(10, 10);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(862, 220);
            this.Frame1.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // vsLayout
            // 
            this.vsLayout.Cols = 1;
            this.vsLayout.ColumnHeadersVisible = false;
            this.vsLayout.Controls.Add(this.Line1);
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
            this.vsLayout.FixedRows = 0;
            this.vsLayout.Location = new System.Drawing.Point(20, 20);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.RowHeadersVisible = false;
            this.vsLayout.Rows = 4;
            this.vsLayout.Size = new System.Drawing.Size(520, 200);
            this.vsLayout.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.vsLayout, null);
            this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
            this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
            // 
            // Line1
            // 
            this.Line1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Line1.LineWidth = 2;
            this.Line1.Name = "Line1";
            this.Line1.Size = new System.Drawing.Size(2, 360);
            this.ToolTip1.SetToolTip(this.Line1, null);
            this.Line1.Y2 = 360F;
            // 
            // fraType
            // 
            this.fraType.Controls.Add(this.cmbLabelType);
            this.fraType.Controls.Add(this.lblDescription);
            this.fraType.Location = new System.Drawing.Point(560, 20);
            this.fraType.Name = "fraType";
            this.fraType.Size = new System.Drawing.Size(281, 200);
            this.fraType.TabIndex = 12;
            this.fraType.Text = "Label Type";
            this.ToolTip1.SetToolTip(this.fraType, null);
            // 
            // cmbLabelType
            // 
            this.cmbLabelType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLabelType.Location = new System.Drawing.Point(20, 30);
            this.cmbLabelType.Name = "cmbLabelType";
            this.cmbLabelType.Size = new System.Drawing.Size(241, 40);
            this.cmbLabelType.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.cmbLabelType, null);
            this.cmbLabelType.SelectedIndexChanged += new System.EventHandler(this.cmbLabelType_SelectedIndexChanged);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(20, 90);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(241, 90);
            this.lblDescription.TabIndex = 15;
            this.lblDescription.Text = "LABEL1";
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // fraMessage
            // 
            this.fraMessage.AppearanceKey = "groupBoxNoBorders";
            this.fraMessage.Controls.Add(this.Label3);
            this.fraMessage.Location = new System.Drawing.Point(20, 20);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(371, 84);
            this.fraMessage.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.fraMessage, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 20);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(321, 46);
            this.Label3.TabIndex = 11;
            this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry2});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // frmCustomLabels
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(891, 735);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomLabels";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Labels";
            this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCustomLabels_Load);
            this.Resize += new System.EventHandler(this.frmCustomLabels_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomLabels_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkChooseLabelStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            this.vsLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraType)).EndInit();
            this.fraType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}