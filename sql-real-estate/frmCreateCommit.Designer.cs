﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCommit.
	/// </summary>
	partial class frmCommit : BaseForm
	{
		public fecherFoundation.FCTextBox txtDB;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCButton cmdBuildCommitment;
		public fecherFoundation.FCTextBox txtCYear;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCommit));
			this.txtDB = new fecherFoundation.FCTextBox();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.cmdBuildCommitment = new fecherFoundation.FCButton();
			this.txtCYear = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBuildCommitment)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 445);
			this.BottomPanel.Size = new System.Drawing.Size(568, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtDB);
			this.ClientArea.Controls.Add(this.cmdBrowse);
			this.ClientArea.Controls.Add(this.cmdBuildCommitment);
			this.ClientArea.Controls.Add(this.txtCYear);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(568, 385);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(568, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(361, 30);
			this.HeaderText.Text = "Build Commitment Archive Data";
			// 
			// txtDB
			// 
			this.txtDB.AutoSize = false;
			this.txtDB.BackColor = System.Drawing.SystemColors.Window;
			this.txtDB.LinkItem = null;
			this.txtDB.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDB.LinkTopic = null;
			this.txtDB.Location = new System.Drawing.Point(190, 90);
			this.txtDB.Name = "txtDB";
			this.txtDB.Size = new System.Drawing.Size(244, 40);
			this.txtDB.TabIndex = 4;
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "actionButton";
			this.cmdBrowse.Location = new System.Drawing.Point(454, 90);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(82, 40);
			this.cmdBrowse.TabIndex = 3;
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// cmdBuildCommitment
			// 
			this.cmdBuildCommitment.AppearanceKey = "acceptButton";
			this.cmdBuildCommitment.Location = new System.Drawing.Point(30, 150);
			this.cmdBuildCommitment.Name = "cmdBuildCommitment";
			this.cmdBuildCommitment.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdBuildCommitment.Size = new System.Drawing.Size(240, 48);
			this.cmdBuildCommitment.TabIndex = 1;
			this.cmdBuildCommitment.Text = "Build Commitment Records";
			this.cmdBuildCommitment.Click += new System.EventHandler(this.cmdBuildCommitment_Click);
			// 
			// txtCYear
			// 
			this.txtCYear.AutoSize = false;
			this.txtCYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtCYear.LinkItem = null;
			this.txtCYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCYear.LinkTopic = null;
			this.txtCYear.Location = new System.Drawing.Point(190, 30);
			this.txtCYear.Name = "txtCYear";
			this.txtCYear.Size = new System.Drawing.Size(66, 40);
			this.txtCYear.TabIndex = 0;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(125, 18);
			this.Label2.TabIndex = 5;
			this.Label2.Text = "ARCHIVE DATABASE";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(120, 18);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "COMMITMENT YEAR";
			// 
			// frmCommit
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(568, 553);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCommit";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Build Commitment Archive Data";
			this.Load += new System.EventHandler(this.frmCommit_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCommit_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBuildCommitment)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
