﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptExemptAudit.
	/// </summary>
	public partial class rptExemptAudit : BaseSectionReport
	{
		public static rptExemptAudit InstancePtr
		{
			get
			{
				return (rptExemptAudit)Sys.GetInstance(typeof(rptExemptAudit));
			}
		}

		protected rptExemptAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
				clsExemptCodes.Dispose();
                clsExemptCodes = null;
                clsLoad = null;
				Grid?.Dispose();
                Grid = null;
            }
			base.Dispose(disposing);
		}

		public rptExemptAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Exempt Audit";
		}
		// nObj = 1
		//   0	rptExemptAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		clsDRWrapper clsExemptCodes = new clsDRWrapper();
		bool boolByExempt;
		string strGroupSQL;
		int lngRow;
		bool boolNoSubTotals;
		double lngTotLand;
		double lngTotBldg;
		double lngTotExempt;
		double lngTotAssess;
		int lngTotAccts;
		int lngTotTotExempts;
		int intOrderToUse;
		int intTownCode;
		int lngTotalAccounts;
		int lngLastAcct;
		const int CNSTACCOUNTCOL = 0;
		const int CNSTNAMECOL = 1;
		const int CNSTMAPLOTCOL = 2;
		const int CNSTLANDCOL = 3;
		const int CNSTBLDGCOL = 4;
		const int CNSTEXEMPTCOL = 5;
		const int CNSTCODECOL = 6;
		const int CNSTASSESSCOL = 7;
		FCGrid Grid;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngRow >= Grid.Rows;
			if (lngRow <= Grid.Rows - 1)
			{
				Fields["EndData"].Value = Grid.TextMatrix(lngRow, CNSTCODECOL);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngTotalAccounts = 0;
			txtdate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtmuni.Text = modGlobalConstants.Statics.MuniName;
			txtpage.Text = "Page 1";
			lngRow = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngRow <= Grid.Rows - 1)
			{
				txtAccount.Text = FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTACCOUNTCOL)));
				txtName.Text = Grid.TextMatrix(lngRow, CNSTNAMECOL);
				txtMapLot.Text = Grid.TextMatrix(lngRow, CNSTMAPLOTCOL);
				txtLand.Text = Strings.Format(Conversion.Val(Grid.TextMatrix(lngRow, CNSTLANDCOL)), "#,###,###,##0");
				txtBldg.Text = Strings.Format(Conversion.Val(Grid.TextMatrix(lngRow, CNSTBLDGCOL)), "#,###,###,##0");
				txtExempt.Text = Strings.Format(Conversion.Val(Grid.TextMatrix(lngRow, CNSTEXEMPTCOL)), "#,###,###,##0");
				txtAssessment.Text = Strings.Format(Conversion.Val(Grid.TextMatrix(lngRow, CNSTASSESSCOL)), "#,###,###,##0");
				if (intOrderToUse == 2 || intOrderToUse == 4 || intOrderToUse == 6)
				{
					lngTotExempt += Conversion.Val(Grid.TextMatrix(lngRow, CNSTEXEMPTCOL));
				}
				if (!boolNoSubTotals)
					Fields["EndData"].Value = Grid.TextMatrix(lngRow, CNSTCODECOL);
				lngRow += 1;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			int lngCRow;
			int intCode;
			int x;
			// vbPorter upgrade warning: lngLand As int	OnWrite(short, double)
			int lngLand;
			// vbPorter upgrade warning: lngbldg As int	OnWrite(short, double)
			int lngbldg;
			// vbPorter upgrade warning: lngExempt As int	OnWrite(short, double)
			int lngExempt;
			// vbPorter upgrade warning: lngAssess As int	OnWrite(short, double)
			int lngAssess;
			int lngAccounts;
			int lngTotalExempts;
			//intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(GroupHeader1.DataField));
			intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Fields["EndData"].Value)));
			txtGroup.Text = "Group = " + FCConvert.ToString(intCode);
			if (intCode != 0)
			{
				// If clsExemptCodes.FindFirstRecord("code", intCode) Then
				if (clsExemptCodes.FindFirst("code = " + FCConvert.ToString(intCode)))
				{
					txtGroup.Text = FCConvert.ToString(intCode) + " - " + clsExemptCodes.Get_Fields_String("Description");
				}
				else
				{
					txtGroup.Text = FCConvert.ToString(intCode) + " - Undefined Code";
				}
			}
			else
			{
				txtGroup.Text = "No Exempt Code";
			}
			lngCRow = Grid.FindRow(intCode, 0, CNSTCODECOL);
			lngLand = 0;
			lngbldg = 0;
			lngExempt = 0;
			lngAssess = 0;
			lngAccounts = 0;
			lngTotalExempts = 0;
			lngLastAcct = 0;
			if (lngCRow >= 0)
			{
				for (x = lngCRow; x <= Grid.Rows - 1; x++)
				{
					if (Conversion.Val(Grid.TextMatrix(x, CNSTCODECOL)) != intCode)
					{
						// no more of the current code
						break;
					}
					if (lngLastAcct != Conversion.Val(Grid.TextMatrix(x, CNSTACCOUNTCOL)))
					{
						lngLastAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTACCOUNTCOL))));
						lngAccounts += 1;
						lngLand += FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(x, CNSTLANDCOL)));
						lngbldg += FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(x, CNSTBLDGCOL)));
						lngAssess += FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(x, CNSTASSESSCOL)));
						if (Conversion.Val(Grid.TextMatrix(x, CNSTASSESSCOL)) == 0 && Conversion.Val(Grid.TextMatrix(x, CNSTEXEMPTCOL)) > 0)
						{
							lngTotalExempts += 1;
						}
					}
					lngExempt += FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(x, CNSTEXEMPTCOL)));
				}
				// x
			}
			txtSubAssessment.Text = Strings.Format(lngAssess, "#,###,###,##0");
			txtSubBldg.Text = Strings.Format(lngbldg, "#,###,###,##0");
			txtSubExemption.Text = Strings.Format(lngExempt, "#,###,###,##0");
			txtSubLand.Text = Strings.Format(lngLand, "#,###,###,##0");
			txtNumAccts.Text = Strings.Format(lngAccounts, "#,###,###,##0");
			txtTotalExempts.Text = Strings.Format(lngTotalExempts, "#,###,###,##0");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtpage.Text = "Page " + this.PageNumber;
		}
		// vbPorter upgrade warning: intWhatInclude As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intWhatOrder As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: strMin As string	OnWrite(string, short)
		// vbPorter upgrade warning: strMax As string	OnWrite(string, int)
		public void Init(ref int intWhatInclude, ref int intWhatOrder, ref string strMin, ref string strMax, bool boolBilling = true, bool boolRange = false, bool boolSubTotalOnly = false, string strSpecific = "")
		{
			this.Grid = new FCGrid();
			//FC:FINAL:DDU:#i1614 - make it enter with same rows & cols as in Original
			Grid.Rows = 0;
			Grid.Cols = 8;
			string strSQL = "";
			string strLandField = "";
			string strBldgField = "";
			string strExemptField = "";
			string strWhere;
			string strOrderBy = "";
			string strHaving = "";
			// vbPorter upgrade warning: x As short --> As int	OnWrite(double, short)
			int x;
			bool boolAdd = false;
			string strExemptVal = "";
			string strTotalSQL;
			// vbPorter upgrade warning: strAry As string()	OnReadFCConvert.ToInt32(
			string[] strAry = null;
			// vbPorter upgrade warning: intECode As short --> As int	OnWrite(int, string)
			int intECode = 0;
			clsDRWrapper clsECodes = new clsDRWrapper();
			string strMasterJoin;
			string strREFullDBName;
			strREFullDBName = clsECodes.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			if (intWhatInclude == 4)
			{
				strAry = Strings.Split(strSpecific, ",", -1, CompareConstants.vbTextCompare);
			}
			lngTotAccts = 0;
			lngTotAssess = 0;
			lngTotBldg = 0;
			lngTotLand = 0;
			lngTotExempt = 0;
			lngTotTotExempts = 0;
			intOrderToUse = intWhatOrder;
			clsExemptCodes.OpenRecordset("select * from exemptcode order by code", modGlobalVariables.strREDatabase);
			if (boolBilling)
			{
				strLandField = "lastLandVal";
				strBldgField = "lastbldgval";
				strExemptField = "rlexemption";
				strExemptVal = "ExemptVal";
			}
			else
			{
				strLandField = "rllandval";
				strBldgField = "rlbldgval";
				strExemptField = "correxemption";
				strExemptVal = "CurrExemptVal";
			}
			strWhere = "";
			switch (intWhatInclude)
			{
				case 1:
					{
						// partial only
						strHaving = "having (sum(" + strLandField + ") + sum(" + strBldgField + ") - sum(" + strExemptField + ") ) > 0 ";
						break;
					}
				case 2:
					{
						// total only
						strHaving = "having (sum(" + strLandField + ") + sum(" + strBldgField + ") - sum(" + strExemptField + ") ) = 0 and sum(" + strExemptField + ") > 0";
						break;
					}
				case 3:
					{
						// both
						strHaving = "having sum(" + strExemptField + ") > 0 or sum(riexemptcd1) > 0 or sum(riexemptcd2) > 0 or sum(riexemptcd3) > 0 ";
						break;
					}
				case 4:
					{
						// specific
						strHaving = "";
						strWhere = " and ((riexemptcd1 in (" + strSpecific + ")) or (riexemptcd2 in (" + strSpecific + ")) or (riexemptcd3 in (" + strSpecific + "))) ";
						break;
					}
				case 5:
					{
						// range
						strHaving = "";
						strWhere = " and ((riexemptcd1 between " + FCConvert.ToString(Conversion.Val(strMin)) + " and " + FCConvert.ToString(Conversion.Val(strMax)) + ") or (riexemptcd2  between " + FCConvert.ToString(Conversion.Val(strMin)) + " and " + FCConvert.ToString(Conversion.Val(strMax)) + ") or (riexemptcd3 between " + FCConvert.ToString(Conversion.Val(strMin)) + " and " + FCConvert.ToString(Conversion.Val(strMax)) + "))";
						break;
					}
			}
			//end switch
			// strTotalSQL = "select rsaccount,sum(" & strLandField & ") as landsum,sum(" & strBldgField & ") as bldgsum,sum(" & strExemptField & ") as exemptsum from master where not rsdeleted = 1 " & strWhere & " group by rsaccount " & strHaving
			strTotalSQL = "select rsaccount from master where  not rsdeleted = 1 " + strWhere + " group by rsaccount " + strHaving;
			if (!boolRange)
			{
				strMin = FCConvert.ToString(0);
				clsLoad.OpenRecordset("select max(code) as maxcode from exemptcode", modGlobalVariables.strREDatabase);
				// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
				strMax = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("maxcode")));
			}
			boolNoSubTotals = false;
			if (!boolSubTotalOnly)
			{
				switch (intWhatOrder)
				{
					case 1:
					case 3:
					case 5:
						{
							// no subtotals
							GroupHeader1.Visible = false;
							GroupFooter1.Visible = false;
							boolNoSubTotals = true;
							if (intWhatOrder == 1)
							{
								strOrderBy = " order by rsname ";
							}
							else if (intWhatOrder == 3)
							{
								strOrderBy = " order by rsmaplot ";
							}
							else if (intWhatOrder == 5)
							{
								strOrderBy = " order by account";
							}
							boolByExempt = false;
							strSQL = "select rsaccount as account,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum(" + strExemptField + ") as exemptsum from master where  not rsdeleted = 1 group by rsaccount " + strHaving;
							// strSQL = "select tbl1.*,OwnerPartyId,rsmaplot," & strExemptVal & "1," & strExemptVal & "2," & strExemptVal & "3,riexemptcd1,riexemptcd2,riexemptcd3 from master inner join (" & strSQL & ") as tbl1 on (tbl1.account = master.rsaccount) where rscard = 1 and not rsdeleted = 1 " & strWhere & strOrderBy
							strSQL = "select tbl1.*,rsname,master.rsmaplot,master." + strExemptVal + "1,master." + strExemptVal + "2,master." + strExemptVal + "3,master.riexemptcd1,master.riexemptcd2,master.riexemptcd3 from master inner join (" + strSQL + ") as tbl1 on (tbl1.account = master.rsaccount)  inner join " + strMasterJoinQuery + " on (mj.rsaccount = tbl1.account) where mj.rscard = 1 and not master.rsdeleted = 1 " + strWhere + strOrderBy;
							clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
							while (!clsLoad.EndOfFile())
							{
								// load grid
								boolAdd = true;
								if (intWhatInclude == 1)
								{
									// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
									if (Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) == 0)
									{
										boolAdd = false;
									}
								}
								else if (intWhatInclude == 2)
								{
									// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
									if (Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) != 0)
									{
										boolAdd = false;
									}
								}
								if (boolAdd)
								{
									Grid.Rows += 1;
									lngRow = Grid.Rows - 1;
									// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
									Grid.TextMatrix(lngRow, CNSTACCOUNTCOL, clsLoad.Get_Fields("account"));
									Grid.TextMatrix(lngRow, CNSTNAMECOL, clsLoad.Get_Fields_String("rsname"));
									Grid.TextMatrix(lngRow, CNSTMAPLOTCOL, clsLoad.Get_Fields_String("rsmaplot"));
									// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
									Grid.TextMatrix(lngRow, CNSTLANDCOL, clsLoad.Get_Fields("landsum"));
									// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
									Grid.TextMatrix(lngRow, CNSTBLDGCOL, clsLoad.Get_Fields("bldgsum"));
									// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
									Grid.TextMatrix(lngRow, CNSTEXEMPTCOL, clsLoad.Get_Fields("exemptsum"));
									Grid.TextMatrix(lngRow, CNSTCODECOL, clsLoad.Get_Fields_Int32("riexemptcd1"));
									// TODO Get_Fields: Field [Landsum] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
									Grid.TextMatrix(lngRow, CNSTASSESSCOL, Conversion.Val(clsLoad.Get_Fields("Landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")));
									// If Val(clsLoad.Fields("exemptsum")) > 0 And Val(clsLoad.Fields("exemptsum")) >= Val(clsLoad.Fields("landsum")) + Val(clsLoad.Fields("bldgsum")) Then
									// lngTotTotExempts = lngTotTotExempts + 1
									// End If
									// lngTotAccts = lngTotAccts + 1
									// lngTotLand = lngTotLand + Val(clsLoad.Fields("landsum"))
									// lngTotBldg = lngTotBldg + Val(clsLoad.Fields("bldgsum"))
									// lngTotExempt = lngTotExempt + Val(clsLoad.Fields("exemptsum"))
									// lngTotAssess = lngTotLand + lngTotBldg - lngTotExempt
								}
								clsLoad.MoveNext();
							}
							break;
						}
					case 2:
					case 4:
					case 6:
						{
							boolByExempt = true;
							if (intWhatOrder == 2)
							{
								strOrderBy = " order by rsname ";
							}
							else if (intWhatOrder == 4)
							{
								strOrderBy = " order by rsmaplot ";
							}
							else if (intWhatOrder == 6)
							{
								strOrderBy = " order by account";
							}
							if (!boolRange)
							{
								if (intWhatInclude != 4)
								{
									strMin = FCConvert.ToString(0);
									clsLoad.OpenRecordset("select max(code) as maxcode from exemptcode", modGlobalVariables.strREDatabase);
									// TODO Get_Fields: Field [maxcode] not found!! (maybe it is an alias?)
									strMax = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("maxcode")));
								}
								else
								{
									strMin = FCConvert.ToString(0);
									strMax = FCConvert.ToString(Information.UBound(strAry, 1));
								}
							}
							if (intWhatInclude != 4)
							{
								if (intWhatOrder == 2)
								{
									strOrderBy = " order by mj.rsname ";
								}
								else if (intWhatOrder == 4)
								{
									strOrderBy = " order by mj.rsmaplot ";
								}
								else if (intWhatOrder == 6)
								{
									strOrderBy = " order by account";
								}
								clsECodes.OpenRecordset(" select  isnull(RIEXEMPTCD1,0) as code from master where not rsdeleted = 1 and rscard = 1 group by riexemptcd1 union select isnull(riexemptcd2,0) as code from master where not rsdeleted = 1 and rscard = 1 group by riexemptcd2 union select isnull(riexemptcd3,0) as code from master where not rsdeleted = 1 and rscard = 1 group by riexemptcd3 order by code", modGlobalVariables.strREDatabase);
								while (!clsECodes.EndOfFile())
								{
									// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
									intECode = FCConvert.ToInt32(clsECodes.Get_Fields("code"));
									if (intECode > 0)
									{
										// strSQL = "select rsaccount as account,rsname,rsmaplot," & strExemptVal & "1 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd1 = " & intECode & ") union all (select rsaccount,rsname,rsmaplot," & strExemptVal & "2 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd2 = " & intECode & ") union all (select rsaccount,rsname,rsmaplot," & strExemptVal & "3 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd3 = " & intECode & ") " '& strOrderBy
										strSQL = "(select rsaccount as account,rsmaplot," + strExemptVal + "1 as exemptval from " + strREFullDBName + ".dbo.master where not rsdeleted = 1 and rscard = 1 and riexemptcd1 = " + FCConvert.ToString(intECode) + " union all select rsaccount,rsmaplot," + strExemptVal + "2 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd2 = " + FCConvert.ToString(intECode) + " union all select rsaccount,rsmaplot," + strExemptVal + "3 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd3 = " + FCConvert.ToString(intECode) + ")  ";
										// & strOrderBy
									}
									else
									{
										// strSQL = "select rsaccount as account,rsname,rsmaplot," & strExemptVal & "1 + " & strExemptVal & "2 + " & strExemptVal & "3 as exemptval from " & strMasterJoinQuery & " where not rsdeleted = 1 and rscard = 1 and riexemptcd1 = 0 and riexemptcd2 = 0 and riexemptcd3 = 0 and " & strExemptVal & "1 + " & strExemptVal & "2 + " & strExemptVal & "3 > 0"   '& strOrderBy
										strSQL = "select rsaccount as account,rsmaplot," + strExemptVal + "1 + " + strExemptVal + "2 + " + strExemptVal + "3 as exemptval from " + strREFullDBName + ".dbo.master where not rsdeleted = 1 and rscard = 1 and riexemptcd1 = 0 and riexemptcd2 = 0 and riexemptcd3 = 0 and " + strExemptVal + "1 + " + strExemptVal + "2 + " + strExemptVal + "3 > 0";
										// & strOrderBy
									}
									strSQL = "select * from (select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum (" + strExemptField + ") as exemptsum from " + strREFullDBName + ".dbo.master where  not rsdeleted = 1 GROUP BY RSACCOUNT) as tbl2 inner join (" + strSQL + ") as tbl1 on (tbl1.account = tbl2.rsaccount) inner join " + strMasterJoinQuery + " on (mj.rsaccount = tbl2.rsaccount) where mj.rscard = 1 " + strOrderBy;
									clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
									while (!clsLoad.EndOfFile())
									{
										// add to grid
										boolAdd = true;
										if (intWhatInclude == 1)
										{
											// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
											if (Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) == 0)
											{
												boolAdd = false;
											}
										}
										else if (intWhatInclude == 2)
										{
											// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
											if (Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) != 0)
											{
												boolAdd = false;
											}
										}
										if (boolAdd)
										{
											Grid.Rows += 1;
											lngRow = Grid.Rows - 1;
											Grid.TextMatrix(lngRow, CNSTACCOUNTCOL, clsLoad.Get_Fields_Int32("rsaccount"));
											Grid.TextMatrix(lngRow, CNSTNAMECOL, clsLoad.Get_Fields_String("rsname"));
											Grid.TextMatrix(lngRow, CNSTMAPLOTCOL, clsLoad.Get_Fields_String("rsmaplot"));
											// TODO Get_Fields: Field [exemptval] not found!! (maybe it is an alias?)
											Grid.TextMatrix(lngRow, CNSTEXEMPTCOL, clsLoad.Get_Fields("exemptval"));
											// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
											Grid.TextMatrix(lngRow, CNSTLANDCOL, clsLoad.Get_Fields("landsum"));
											// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
											Grid.TextMatrix(lngRow, CNSTBLDGCOL, clsLoad.Get_Fields("bldgsum"));
											// TODO Get_Fields: Field [Landsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
											Grid.TextMatrix(lngRow, CNSTASSESSCOL, Conversion.Val(clsLoad.Get_Fields("Landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")));
											Grid.TextMatrix(lngRow, CNSTCODECOL, intECode);
											// If Val(clsLoad.Fields("exemptval")) >= Val(clsLoad.Fields("landsum")) + Val(clsLoad.Fields("bldgsum")) And Val(clsLoad.Fields("exemptval")) > 0 Then
											// lngTotTotExempts = lngTotTotExempts + 1
											// End If
											// lngTotAccts = lngTotAccts + 1
											// lngTotLand = lngTotLand + Val(clsLoad.Fields("landsum"))
											// lngTotBldg = lngTotBldg + Val(clsLoad.Fields("bldgsum"))
											// lngTotExempt = lngTotExempt + Val(clsLoad.Fields("exemptval"))
											// lngTotAssess = lngTotLand + lngTotBldg - lngTotExempt
										}
										clsLoad.MoveNext();
									}
									clsECodes.MoveNext();
								}
							}
							else
							{
								for (x = FCConvert.ToInt32(Conversion.Val(strMin)); x <= FCConvert.ToInt32(Conversion.Val(strMax)); x++)
								{
									if (intWhatInclude != 4)
									{
										intECode = x;
									}
									else
									{
										intECode = FCConvert.ToInt32(strAry[x]);
									}
									if (intECode > 0)
									{
										strSQL = "select * from (select rsaccount as account,rsmaplot," + strExemptVal + "1 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd1 = " + FCConvert.ToString(intECode) + ") tbl4 union all (select rsaccount,rsmaplot," + strExemptVal + "2 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd2 = " + FCConvert.ToString(intECode) + ") union all (select rsaccount,rsmaplot," + strExemptVal + "3 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd3 = " + FCConvert.ToString(intECode) + ") ";
										// & strOrderBy
									}
									else
									{
										strSQL = "select rsaccount as account,rsmaplot," + strExemptVal + "1 + " + strExemptVal + "2 + " + strExemptVal + "3 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd1 = 0 and riexemptcd2 = 0 and riexemptcd3 = 0 and " + strExemptVal + "1 + " + strExemptVal + "2 + " + strExemptVal + "3 > 0";
										// & strOrderBy
									}
									// strSQL = "select * from (select rsaccount,sum(" & strLandField & ") as landsum,sum(" & strBldgField & ") as bldgsum,sum (" & strExemptField & ") as exemptsum from master where  not rsdeleted = 1 GROUP BY RSACCOUNT) as tbl2 inner join (" & strSQL & ") as tbl1 on (tbl1.account = tbl2.rsaccount) " & strOrderBy
									strSQL = "select * from (select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum (" + strExemptField + ") as exemptsum from master where  not rsdeleted = 1 GROUP BY RSACCOUNT) as tbl2 inner join (" + strSQL + ") as tbl1 on (tbl1.account = tbl2.rsaccount)  inner join " + strMasterJoinQuery + " on (mj.rsaccount = tbl1.account) where mj.rscard = 1 and not mj.rsdeleted = 1 " + strWhere + strOrderBy;
									// inner join " & strMasterJoinQuery & " on (mj.rsaccount = tbl1.account) where mj.rscard = 1 and not master.rsdeleted = 1 " & strWhere & strOrderBy
									clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
									while (!clsLoad.EndOfFile())
									{
										// add to grid
										boolAdd = true;
										if (intWhatInclude == 1)
										{
											// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
											if (Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) == 0)
											{
												boolAdd = false;
											}
										}
										else if (intWhatInclude == 2)
										{
											// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
											if (Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) != 0)
											{
												boolAdd = false;
											}
										}
										if (boolAdd)
										{
											Grid.Rows += 1;
											lngRow = Grid.Rows - 1;
											Grid.TextMatrix(lngRow, CNSTACCOUNTCOL, clsLoad.Get_Fields_Int32("rsaccount"));
											Grid.TextMatrix(lngRow, CNSTNAMECOL, clsLoad.Get_Fields_String("rsname"));
											Grid.TextMatrix(lngRow, CNSTMAPLOTCOL, clsLoad.Get_Fields_String("rsmaplot"));
											// TODO Get_Fields: Field [exemptval] not found!! (maybe it is an alias?)
											Grid.TextMatrix(lngRow, CNSTEXEMPTCOL, clsLoad.Get_Fields("exemptval"));
											// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
											Grid.TextMatrix(lngRow, CNSTLANDCOL, clsLoad.Get_Fields("landsum"));
											// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
											Grid.TextMatrix(lngRow, CNSTBLDGCOL, clsLoad.Get_Fields("bldgsum"));
											// TODO Get_Fields: Field [Landsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
											// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
											Grid.TextMatrix(lngRow, CNSTASSESSCOL, Conversion.Val(clsLoad.Get_Fields("Landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")));
											Grid.TextMatrix(lngRow, CNSTCODECOL, intECode);
											// If Val(clsLoad.Fields("exemptval")) >= Val(clsLoad.Fields("landsum")) + Val(clsLoad.Fields("bldgsum")) And Val(clsLoad.Fields("exemptval")) > 0 Then
											// lngTotTotExempts = lngTotTotExempts + 1
											// End If
											// lngTotAccts = lngTotAccts + 1
											// lngTotLand = lngTotLand + Val(clsLoad.Fields("landsum"))
											// lngTotBldg = lngTotBldg + Val(clsLoad.Fields("bldgsum"))
											// lngTotExempt = lngTotExempt + Val(clsLoad.Fields("exemptval"))
											// lngTotAssess = lngTotLand + lngTotBldg - lngTotExempt
										}
										clsLoad.MoveNext();
									}
								}
								// x
							}
							break;
						}
				}
				//end switch
			}
			else
			{
				boolByExempt = true;
				Detail.Visible = false;
				if (intWhatInclude == 4)
				{
					strMin = FCConvert.ToString(0);
					strMax = FCConvert.ToString(Information.UBound(strAry, 1));
				}
				if (intWhatInclude != 4)
				{
					// Call clsECodes.OpenRecordset(" select * from (select val(RIEXEMPTCD1 & '') as code from master where not rsdeleted = 1 and rscard = 1) union (select val(riexemptcd2 & '') as code from master where not rsdeleted = 1 and rscard = 1) union (select val(riexemptcd3 & '') as code from master where not rsdeleted = 1 and rscard = 1) order by code", strREDatabase)
					clsECodes.OpenRecordset("select distinct code from ((select isnull(riexemptcd1,0) as code from master where not rsdeleted = 1 and rscard = 1) union all ( select isnull(riexemptcd2,0) as code from master where not rsdeleted = 1 and rscard = 1) union all (select isnull(riexemptcd3,0) as code from master where not rsdeleted = 1 and rscard = 1)) tbl1 order by code", modGlobalVariables.strREDatabase);
					while (!clsECodes.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						intECode = FCConvert.ToInt32(clsECodes.Get_Fields("code"));
						if (intECode > 0)
						{
							strSQL = "select * from (select rsaccount as account,OwnerPartyID,rsmaplot," + strExemptVal + "1 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd1 = " + FCConvert.ToString(intECode) + ") tbl3 union all (select rsaccount,OwnerPartyID,rsmaplot," + strExemptVal + "2 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd2 = " + FCConvert.ToString(intECode) + ") union all (select rsaccount,OwnerPartyId,rsmaplot," + strExemptVal + "3 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd3 = " + FCConvert.ToString(intECode) + ") ";
							// & strOrderBy
						}
						else
						{
							strSQL = "select rsaccount as account,OwnerPartyID,rsmaplot," + strExemptVal + "1 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd1 = 0 and " + strExemptVal + "1 > 0";
							// & strOrderBy
						}
						strSQL = "select * from (select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum (" + strExemptField + ") as exemptsum from master where  not rsdeleted = 1 GROUP BY RSACCOUNT) as tbl2 inner join (" + strSQL + ") as tbl1 on (tbl1.account = tbl2.rsaccount) ";
						clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
						while (!clsLoad.EndOfFile())
						{
							// add to grid
							boolAdd = true;
							if (intWhatInclude == 1)
							{
								// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
								if (Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) == 0)
								{
									boolAdd = false;
								}
							}
							else if (intWhatInclude == 2)
							{
								// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
								if (Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) != 0)
								{
									boolAdd = false;
								}
							}
							if (boolAdd)
							{
								Grid.Rows += 1;
								lngRow = Grid.Rows - 1;
								Grid.TextMatrix(lngRow, CNSTACCOUNTCOL, clsLoad.Get_Fields_Int32("rsaccount"));
								// Grid.TextMatrix(lngRow, CNSTNAMECOL) = clsLoad.Fields("rsname")
								Grid.TextMatrix(lngRow, CNSTMAPLOTCOL, clsLoad.Get_Fields_String("rsmaplot"));
								// TODO Get_Fields: Field [exemptval] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTEXEMPTCOL, clsLoad.Get_Fields("exemptval"));
								// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTLANDCOL, clsLoad.Get_Fields("landsum"));
								// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTBLDGCOL, clsLoad.Get_Fields("bldgsum"));
								// TODO Get_Fields: Field [Landsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTASSESSCOL, Conversion.Val(clsLoad.Get_Fields("Landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")));
								Grid.TextMatrix(lngRow, CNSTCODECOL, intECode);
								// If Val(clsLoad.Fields("exemptval")) >= Val(clsLoad.Fields("landsum")) + Val(clsLoad.Fields("bldgsum")) And Val(clsLoad.Fields("exemptval")) > 0 Then
								// lngTotTotExempts = lngTotTotExempts + 1
								// End If
								// lngTotAccts = lngTotAccts + 1
								// lngTotLand = lngTotLand + Val(clsLoad.Fields("landsum"))
								// lngTotBldg = lngTotBldg + Val(clsLoad.Fields("bldgsum"))
								// lngTotExempt = lngTotExempt + Val(clsLoad.Fields("exemptval"))
								// lngTotAssess = lngTotLand + lngTotBldg - lngTotExempt
							}
							clsLoad.MoveNext();
						}
						clsECodes.MoveNext();
					}
				}
				else
				{
					for (x = FCConvert.ToInt32(Conversion.Val(strMin)); x <= FCConvert.ToInt32(Conversion.Val(strMax)); x++)
					{
						if (!(intWhatInclude == 4))
						{
							intECode = x;
						}
						else
						{
							intECode = FCConvert.ToInt32(strAry[x]);
						}
						if (intECode > 0)
						{
							strSQL = "select * from (select rsaccount as account,OwnerPartyId,rsmaplot," + strExemptVal + "1 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd1 = " + FCConvert.ToString(intECode) + ") tbl3 union all (select rsaccount,OwnerPartyId,rsmaplot," + strExemptVal + "2 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd2 = " + FCConvert.ToString(intECode) + ") union all (select rsaccount,OwnerPartyId,rsmaplot," + strExemptVal + "3 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd3 = " + FCConvert.ToString(intECode) + ") ";
							// & strOrderBy
						}
						else
						{
							strSQL = "select rsaccount as account,OwnerPartyId,rsmaplot," + strExemptVal + "1 as exemptval from master where not rsdeleted = 1 and rscard = 1 and riexemptcd1 = 0 and " + strExemptVal + "1 > 0";
							// & strOrderBy
						}
						strSQL = "select * from (select rsaccount,sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum (" + strExemptField + ") as exemptsum from master where  not rsdeleted = 1 GROUP BY RSACCOUNT) as tbl2 inner join (" + strSQL + ") as tbl1 on (tbl1.account = tbl2.rsaccount) ";
						clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
						while (!clsLoad.EndOfFile())
						{
							// add to grid
							boolAdd = true;
							if (intWhatInclude == 1)
							{
								// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
								if (Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) == 0)
								{
									boolAdd = false;
								}
							}
							else if (intWhatInclude == 2)
							{
								// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
								if (Conversion.Val(clsLoad.Get_Fields("landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) != 0)
								{
									boolAdd = false;
								}
							}
							if (boolAdd)
							{
								Grid.Rows += 1;
								lngRow = Grid.Rows - 1;
								Grid.TextMatrix(lngRow, CNSTACCOUNTCOL, clsLoad.Get_Fields_Int32("rsaccount"));
								// Grid.TextMatrix(lngRow, CNSTNAMECOL) = clsLoad.Fields("rsname")
								Grid.TextMatrix(lngRow, CNSTMAPLOTCOL, clsLoad.Get_Fields_String("rsmaplot"));
								// TODO Get_Fields: Field [exemptval] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTEXEMPTCOL, clsLoad.Get_Fields("exemptval"));
								// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTLANDCOL, clsLoad.Get_Fields("landsum"));
								// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTBLDGCOL, clsLoad.Get_Fields("bldgsum"));
								// TODO Get_Fields: Field [Landsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTASSESSCOL, Conversion.Val(clsLoad.Get_Fields("Landsum")) + Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")));
								Grid.TextMatrix(lngRow, CNSTCODECOL, intECode);
								// If Val(clsLoad.Fields("exemptval")) >= Val(clsLoad.Fields("landsum")) + Val(clsLoad.Fields("bldgsum")) And Val(clsLoad.Fields("exemptval")) > 0 Then
								// lngTotTotExempts = lngTotTotExempts + 1
								// End If
								// lngTotAccts = lngTotAccts + 1
								// lngTotLand = lngTotLand + Val(clsLoad.Fields("landsum"))
								// lngTotBldg = lngTotBldg + Val(clsLoad.Fields("bldgsum"))
								// lngTotExempt = lngTotExempt + Val(clsLoad.Fields("exemptval"))
								// lngTotAssess = lngTotLand + lngTotBldg - lngTotExempt
							}
							clsLoad.MoveNext();
						}
					}
					// x
				}
			}
			if (Grid.Rows == 0)
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			// now make totals
			// If intWhatInclude <> 1 Then
			// If intWhatInclude < 4 Then
			// strHaving = "having (sum(" & strLandField & ") + sum(" & strBldgField & ") - sum(" & strExemptField & ") ) = 0 and sum(" & strExemptField & ") > 0"
			// Else
			// strHaving = "having (sum(" & strLandField & ") + sum(" & strBldgField & ") - sum(" & strExemptField & ") ) = 0 "
			// End If
			// strSQL = "(select rsaccount as ACCOUNT,sum(" & strLandField & ") as landsum,sum(" & strBldgField & ") as bldgsum,sum(" & strExemptField & ") as exemptsum from master where not rsdeleted = 1 group by rsaccount " & strHaving & ") as tbl1 "
			// strSQL = "select * from (select rsaccount from master where rscard = 1 and not rsdeleted = 1 " & strWhere & ") as tbl2 inner join " & strSQL & " on (tbl1.account = tbl2.rsaccount)"
			// 
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsLoad.OpenRecordset(strTotalSQL, modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				clsTemp.OpenRecordset("select sum(" + strLandField + ") as landsum,sum(" + strBldgField + ") as bldgsum,sum(" + strExemptField + ") as exemptsum from master where rsaccount = " + clsLoad.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
				// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
				if (FCConvert.ToDouble(clsTemp.Get_Fields("exemptsum")) > 0 && FCConvert.ToDouble(clsTemp.Get_Fields("landsum")) + FCConvert.ToDouble(clsTemp.Get_Fields("bldgsum")) - FCConvert.ToDouble(clsTemp.Get_Fields("exemptsum")) <= 0)
				{
					lngTotTotExempts += 1;
				}
				lngTotAccts += 1;
				// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
				lngTotLand += FCConvert.ToDouble(clsTemp.Get_Fields("landsum"));
				// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
				lngTotBldg += FCConvert.ToDouble(clsTemp.Get_Fields("bldgsum"));
				// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
				lngTotExempt += FCConvert.ToDouble(clsTemp.Get_Fields("exemptsum"));
				lngTotAssess = lngTotLand + lngTotBldg - lngTotExempt;
				clsLoad.MoveNext();
			}
			// End If
			// If intWhatInclude <> 2 Then
			// strHaving = "having (sum(" & strLandField & ") + sum(" & strBldgField & ") - sum(" & strExemptField & ") ) > 0 "
			// strSQL = "(select rsaccount as ACCOUNT,sum(" & strLandField & ") as landsum,sum(" & strBldgField & ") as bldgsum,sum(" & strExemptField & ") as exemptsum from master where not rsdeleted = 1 group by rsaccount " & strHaving & ") as tbl1 "
			// strSQL = "select * from (select rsaccount from master where rscard = 1 and not rsdeleted = 1 " & strWhere & ") as tbl2 inner join " & strSQL & " on (tbl1.account = tbl2.rsaccount)"
			// 
			// Call clsLoad.OpenRecordset(strSQL, strREDatabase)
			// Do While Not clsLoad.EndOfFile
			// lngTotAccts = lngTotAccts + 1
			// lngTotLand = lngTotLand + Val(clsLoad.Fields("landsum"))
			// lngTotBldg = lngTotBldg + Val(clsLoad.Fields("bldgsum"))
			// lngTotExempt = lngTotExempt + Val(clsLoad.Fields("exemptsum"))
			// lngTotAssess = lngTotLand + lngTotBldg - lngTotExempt
			// clsLoad.MoveNext
			// Loop
			// End If
			txtTotAccts.Text = Strings.Format(lngTotAccts, "#,###,###,##0");
			txtTotExempt.Text = Strings.Format(lngTotExempt, "#,###,###,##0");
			txtTotAssessment.Text = Strings.Format(lngTotAssess, "#,###,###,##0");
			txtTotLand.Text = Strings.Format(lngTotLand, "#,###,###,##0");
			txtTotBldg.Text = Strings.Format(lngTotBldg, "#,###,###,##0");
			txtTotTotExempts.Text = Strings.Format(lngTotTotExempts, "#,###,###,##0");
			//GroupHeader1.DataField = Grid.TextMatrix(0, CNSTCODECOL);
			strGroupSQL = "Select sum(exemptval1) as exemptsum1 from master where rscard = 1 and not rsdeleted = 1 and ";
			lngRow = 0;
			switch (intWhatOrder)
			{
				case 2:
				case 4:
				case 6:
					{
						// name,maplot or account within exempt code
						// make the total only the total of those exemptions
						lngTotExempt = 0;
						break;
					}
			}
			//end switch
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (intOrderToUse == 2 || intOrderToUse == 4 || intOrderToUse == 6)
			{
				txtTotExempt.Text = Strings.Format(lngTotExempt, "#,###,###,##0");
			}
		}

		

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			Fields.Add("EndData");
		}
	}
}
