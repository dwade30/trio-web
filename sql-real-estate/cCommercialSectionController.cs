﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cCommercialSectionController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cGenericCollection GetSections()
		{
			cGenericCollection GetSections = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialSection sect;
				rs.OpenRecordset("select * from CommercialSections order by name", "RealEstate");
				while (!rs.EndOfFile())
				{
					sect = new cCommercialSection();
					sect.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					sect.Name = FCConvert.ToString(rs.Get_Fields_String("Name"));
					sect.Description = FCConvert.ToString(rs.Get_Fields_String("Description"));
					sect.IsUpdated = false;
					gColl.AddItem(sect);
					rs.MoveNext();
				}
				GetSections = gColl;
				return GetSections;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetSections;
		}

		public cCommercialSection GetSection(int lngId)
		{
			cCommercialSection GetSection = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				cCommercialSection sect = null;
				rs.OpenRecordset("select * from commercialsections where id = " + FCConvert.ToString(lngId), "RealEstate");
				if (!rs.EndOfFile())
				{
					sect = new cCommercialSection();
					sect.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					sect.Name = FCConvert.ToString(rs.Get_Fields_String("Name"));
					sect.Description = FCConvert.ToString(rs.Get_Fields_String("Description"));
					sect.IsUpdated = false;
				}
				GetSection = sect;
				return GetSection;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetSection;
		}
	}
}
