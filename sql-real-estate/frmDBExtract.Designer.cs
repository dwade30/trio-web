//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmDBExtract.
	/// </summary>
	partial class frmDBExtract : BaseForm
	{
		public fecherFoundation.FCComboBox cmbtRecords;
		public fecherFoundation.FCLabel lbltRecords;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCComboBox cmbtcreate;
		public fecherFoundation.FCLabel lbltcreate;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkInclude;
		public fecherFoundation.FCCheckBox chkBPOption;
		public fecherFoundation.FCCheckBox chkDescription;
		public fecherFoundation.FCCheckBox chkColumnHeaders;
		public fecherFoundation.FCFrame Frame3;
		public Global.T2KDateBox t2kStart;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public Global.T2KDateBox t2kEnd;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCListBox lstSavedCodes;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCListBox lstCodes;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkInclude_18;
		public fecherFoundation.FCCheckBox chkInclude_0;
		public fecherFoundation.FCCheckBox chkInclude_1;
		public fecherFoundation.FCCheckBox chkInclude_2;
		public fecherFoundation.FCCheckBox chkInclude_3;
		public fecherFoundation.FCCheckBox chkInclude_4;
		public fecherFoundation.FCCheckBox chkInclude_5;
		public fecherFoundation.FCCheckBox chkInclude_6;
		public fecherFoundation.FCCheckBox chkInclude_9;
		public fecherFoundation.FCCheckBox chkInclude_10;
		public fecherFoundation.FCCheckBox chkInclude_11;
		public fecherFoundation.FCCheckBox chkInclude_12;
		public fecherFoundation.FCCheckBox chkInclude_13;
		public fecherFoundation.FCCheckBox chkInclude_14;
		public fecherFoundation.FCCheckBox chkInclude_15;
		public fecherFoundation.FCCheckBox chkInclude_16;
		public fecherFoundation.FCCheckBox chkInclude_8;
		public fecherFoundation.FCCheckBox chkInclude_17;
		public fecherFoundation.FCCheckBox chkInclude_7;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label19;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDBExtract));
            this.cmbtRecords = new fecherFoundation.FCComboBox();
            this.lbltRecords = new fecherFoundation.FCLabel();
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.cmbtcreate = new fecherFoundation.FCComboBox();
            this.lbltcreate = new fecherFoundation.FCLabel();
            this.chkBPOption = new fecherFoundation.FCCheckBox();
            this.chkDescription = new fecherFoundation.FCCheckBox();
            this.chkColumnHeaders = new fecherFoundation.FCCheckBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.t2kStart = new Global.T2KDateBox();
            this.txtEnd = new fecherFoundation.FCTextBox();
            this.txtStart = new fecherFoundation.FCTextBox();
            this.t2kEnd = new Global.T2KDateBox();
            this.lblTo = new fecherFoundation.FCLabel();
            this.lstSavedCodes = new fecherFoundation.FCListBox();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.lstCodes = new fecherFoundation.FCListBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkInclude_18 = new fecherFoundation.FCCheckBox();
            this.chkInclude_0 = new fecherFoundation.FCCheckBox();
            this.chkInclude_1 = new fecherFoundation.FCCheckBox();
            this.chkInclude_2 = new fecherFoundation.FCCheckBox();
            this.chkInclude_3 = new fecherFoundation.FCCheckBox();
            this.chkInclude_4 = new fecherFoundation.FCCheckBox();
            this.chkInclude_5 = new fecherFoundation.FCCheckBox();
            this.chkInclude_6 = new fecherFoundation.FCCheckBox();
            this.chkInclude_9 = new fecherFoundation.FCCheckBox();
            this.chkInclude_10 = new fecherFoundation.FCCheckBox();
            this.chkInclude_11 = new fecherFoundation.FCCheckBox();
            this.chkInclude_12 = new fecherFoundation.FCCheckBox();
            this.chkInclude_13 = new fecherFoundation.FCCheckBox();
            this.chkInclude_14 = new fecherFoundation.FCCheckBox();
            this.chkInclude_15 = new fecherFoundation.FCCheckBox();
            this.chkInclude_16 = new fecherFoundation.FCCheckBox();
            this.chkInclude_8 = new fecherFoundation.FCCheckBox();
            this.chkInclude_17 = new fecherFoundation.FCCheckBox();
            this.chkInclude_7 = new fecherFoundation.FCCheckBox();
            this.Label20 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBPOption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkColumnHeaders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 645);
            this.BottomPanel.Size = new System.Drawing.Size(837, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.chkBPOption);
            this.ClientArea.Controls.Add(this.chkDescription);
            this.ClientArea.Controls.Add(this.chkColumnHeaders);
            this.ClientArea.Controls.Add(this.cmbtRecords);
            this.ClientArea.Controls.Add(this.lbltRecords);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.cmbtcreate);
            this.ClientArea.Controls.Add(this.lbltcreate);
            this.ClientArea.Controls.Add(this.lstSavedCodes);
            this.ClientArea.Controls.Add(this.cmdDelete);
            this.ClientArea.Controls.Add(this.cmdAdd);
            this.ClientArea.Controls.Add(this.lstCodes);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Label20);
            this.ClientArea.Controls.Add(this.Label19);
            this.ClientArea.Size = new System.Drawing.Size(857, 702);
            this.ClientArea.Controls.SetChildIndex(this.Label19, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label20, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstCodes, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdAdd, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdDelete, 0);
            this.ClientArea.Controls.SetChildIndex(this.lstSavedCodes, 0);
            this.ClientArea.Controls.SetChildIndex(this.lbltcreate, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbtcreate, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame3, 0);
            this.ClientArea.Controls.SetChildIndex(this.lbltRecords, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbtRecords, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkColumnHeaders, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkBPOption, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(857, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(267, 28);
            this.HeaderText.Text = "Create Database Extract";
            // 
            // cmbtRecords
            // 
            this.cmbtRecords.Items.AddRange(new object[] {
            "Regular",
            "Sales"});
            this.cmbtRecords.Location = new System.Drawing.Point(480, 350);
            this.cmbtRecords.Name = "cmbtRecords";
            this.cmbtRecords.Size = new System.Drawing.Size(120, 40);
            this.cmbtRecords.TabIndex = 47;
            this.cmbtRecords.Text = "Regular";
            this.cmbtRecords.SelectedIndexChanged += new System.EventHandler(this.OptRecords_CheckedChanged);
            // 
            // lbltRecords
            // 
            this.lbltRecords.AutoSize = true;
            this.lbltRecords.Location = new System.Drawing.Point(377, 364);
            this.lbltRecords.Name = "lbltRecords";
            this.lbltRecords.Size = new System.Drawing.Size(69, 15);
            this.lbltRecords.TabIndex = 48;
            this.lbltRecords.Text = "RECORDS";
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "All Accounts",
            "Account Number",
            "Name",
            "Map Lot",
            "From Extract",
            "Sale Date"});
            this.cmbRange.Location = new System.Drawing.Point(20, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(274, 40);
            this.cmbRange.TabIndex = 43;
            this.cmbRange.Text = "All Accounts";
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
            // 
            // cmbtcreate
            // 
            this.cmbtcreate.Items.AddRange(new object[] {
            "1 Record Per Card",
            "1 Record Per Account"});
            this.cmbtcreate.Location = new System.Drawing.Point(126, 350);
            this.cmbtcreate.Name = "cmbtcreate";
            this.cmbtcreate.Size = new System.Drawing.Size(216, 40);
            this.cmbtcreate.TabIndex = 49;
            this.cmbtcreate.Text = "1 Record Per Card";
            // 
            // lbltcreate
            // 
            this.lbltcreate.AutoSize = true;
            this.lbltcreate.Location = new System.Drawing.Point(30, 364);
            this.lbltcreate.Name = "lbltcreate";
            this.lbltcreate.Size = new System.Drawing.Size(57, 15);
            this.lbltcreate.TabIndex = 50;
            this.lbltcreate.Text = "CREATE";
            // 
            // chkBPOption
            // 
            this.chkBPOption.Location = new System.Drawing.Point(666, 411);
            this.chkBPOption.Name = "chkBPOption";
            this.chkBPOption.Size = new System.Drawing.Size(213, 22);
            this.chkBPOption.TabIndex = 46;
            this.chkBPOption.Text = "Limit Book/Page to most recent";
            // 
            // chkDescription
            // 
            this.chkDescription.Location = new System.Drawing.Point(377, 448);
            this.chkDescription.Name = "chkDescription";
            this.chkDescription.Size = new System.Drawing.Size(130, 22);
            this.chkDescription.TabIndex = 45;
            this.chkDescription.Text = "Use Descriptions";
            this.ToolTip1.SetToolTip(this.chkDescription, "For some fields, use the description not the code");
            // 
            // chkColumnHeaders
            // 
            this.chkColumnHeaders.Location = new System.Drawing.Point(377, 411);
            this.chkColumnHeaders.Name = "chkColumnHeaders";
            this.chkColumnHeaders.Size = new System.Drawing.Size(223, 22);
            this.chkColumnHeaders.TabIndex = 44;
            this.chkColumnHeaders.Text = "Make First Row Column Headers";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.t2kStart);
            this.Frame3.Controls.Add(this.cmbRange);
            this.Frame3.Controls.Add(this.txtEnd);
            this.Frame3.Controls.Add(this.txtStart);
            this.Frame3.Controls.Add(this.t2kEnd);
            this.Frame3.Controls.Add(this.lblTo);
            this.Frame3.Location = new System.Drawing.Point(30, 411);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(312, 234);
            this.Frame3.TabIndex = 29;
            this.Frame3.Text = "All / Range Of";
            // 
            // t2kStart
            // 
            this.t2kStart.Location = new System.Drawing.Point(20, 90);
            this.t2kStart.Mask = "##/##/####";
            this.t2kStart.Name = "t2kStart";
            this.t2kStart.Size = new System.Drawing.Size(274, 22);
            this.t2kStart.TabIndex = 42;
            this.t2kStart.Visible = false;
            // 
            // txtEnd
            // 
            this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
            this.txtEnd.Location = new System.Drawing.Point(20, 168);
            this.txtEnd.Name = "txtEnd";
            this.txtEnd.Size = new System.Drawing.Size(274, 40);
            this.txtEnd.TabIndex = 35;
            this.txtEnd.Visible = false;
            // 
            // txtStart
            // 
            this.txtStart.BackColor = System.Drawing.SystemColors.Window;
            this.txtStart.Location = new System.Drawing.Point(20, 90);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(274, 40);
            this.txtStart.TabIndex = 34;
            this.txtStart.Visible = false;
            // 
            // t2kEnd
            // 
            this.t2kEnd.Location = new System.Drawing.Point(20, 168);
            this.t2kEnd.Mask = "##/##/####";
            this.t2kEnd.Name = "t2kEnd";
            this.t2kEnd.Size = new System.Drawing.Size(274, 22);
            this.t2kEnd.TabIndex = 43;
            this.t2kEnd.Visible = false;
            // 
            // lblTo
            // 
            this.lblTo.Location = new System.Drawing.Point(152, 140);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(30, 18);
            this.lblTo.TabIndex = 36;
            this.lblTo.Text = "TO";
            this.lblTo.Visible = false;
            // 
            // lstSavedCodes
            // 
            this.lstSavedCodes.BackColor = System.Drawing.SystemColors.Window;
            this.lstSavedCodes.Location = new System.Drawing.Point(783, 488);
            this.lstSavedCodes.MultiSelect = 1;
            this.lstSavedCodes.Name = "lstSavedCodes";
            this.lstSavedCodes.SelectionMode = Wisej.Web.SelectionMode.MultiExtended;
            this.lstSavedCodes.Size = new System.Drawing.Size(120, 155);
            this.lstSavedCodes.TabIndex = 22;
            // 
            // cmdDelete
            // 
            this.cmdDelete.Location = new System.Drawing.Point(666, 561);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(98, 23);
            this.cmdDelete.TabIndex = 21;
            this.cmdDelete.Text = "<----- REMOVE";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdAdd
            // 
            this.cmdAdd.Location = new System.Drawing.Point(666, 524);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(99, 23);
            this.cmdAdd.TabIndex = 20;
            this.cmdAdd.Text = "ADD ----->";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // lstCodes
            // 
            this.lstCodes.BackColor = System.Drawing.SystemColors.Window;
            this.lstCodes.Location = new System.Drawing.Point(377, 488);
            this.lstCodes.MultiSelect = 1;
            this.lstCodes.Name = "lstCodes";
            this.lstCodes.SelectionMode = Wisej.Web.SelectionMode.MultiExtended;
            this.lstCodes.Size = new System.Drawing.Size(269, 155);
            this.lstCodes.TabIndex = 19;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkInclude_18);
            this.Frame1.Controls.Add(this.chkInclude_0);
            this.Frame1.Controls.Add(this.chkInclude_1);
            this.Frame1.Controls.Add(this.chkInclude_2);
            this.Frame1.Controls.Add(this.chkInclude_3);
            this.Frame1.Controls.Add(this.chkInclude_4);
            this.Frame1.Controls.Add(this.chkInclude_5);
            this.Frame1.Controls.Add(this.chkInclude_6);
            this.Frame1.Controls.Add(this.chkInclude_9);
            this.Frame1.Controls.Add(this.chkInclude_10);
            this.Frame1.Controls.Add(this.chkInclude_11);
            this.Frame1.Controls.Add(this.chkInclude_12);
            this.Frame1.Controls.Add(this.chkInclude_13);
            this.Frame1.Controls.Add(this.chkInclude_14);
            this.Frame1.Controls.Add(this.chkInclude_15);
            this.Frame1.Controls.Add(this.chkInclude_16);
            this.Frame1.Controls.Add(this.chkInclude_8);
            this.Frame1.Controls.Add(this.chkInclude_17);
            this.Frame1.Controls.Add(this.chkInclude_7);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(610, 300);
            this.Frame1.TabIndex = 1001;
            this.Frame1.Text = "Include";
            // 
            // chkInclude_18
            // 
            this.chkInclude_18.Location = new System.Drawing.Point(20, 67);
            this.chkInclude_18.Name = "chkInclude_18";
            this.chkInclude_18.Size = new System.Drawing.Size(110, 22);
            this.chkInclude_18.TabIndex = 28;
            this.chkInclude_18.Text = "Card Number";
            // 
            // chkInclude_0
            // 
            this.chkInclude_0.Location = new System.Drawing.Point(20, 30);
            this.chkInclude_0.Name = "chkInclude_0";
            this.chkInclude_0.Size = new System.Drawing.Size(129, 22);
            this.chkInclude_0.TabIndex = 18;
            this.chkInclude_0.Text = "Account Number";
            // 
            // chkInclude_1
            // 
            this.chkInclude_1.Location = new System.Drawing.Point(20, 104);
            this.chkInclude_1.Name = "chkInclude_1";
            this.chkInclude_1.Size = new System.Drawing.Size(118, 22);
            this.chkInclude_1.TabIndex = 17;
            this.chkInclude_1.Text = "Owner\'s Name";
            // 
            // chkInclude_2
            // 
            this.chkInclude_2.Location = new System.Drawing.Point(20, 141);
            this.chkInclude_2.Name = "chkInclude_2";
            this.chkInclude_2.Size = new System.Drawing.Size(81, 22);
            this.chkInclude_2.TabIndex = 16;
            this.chkInclude_2.Text = "Location";
            // 
            // chkInclude_3
            // 
            this.chkInclude_3.Location = new System.Drawing.Point(20, 178);
            this.chkInclude_3.Name = "chkInclude_3";
            this.chkInclude_3.Size = new System.Drawing.Size(86, 22);
            this.chkInclude_3.TabIndex = 15;
            this.chkInclude_3.Text = "Map / Lot";
            // 
            // chkInclude_4
            // 
            this.chkInclude_4.Location = new System.Drawing.Point(20, 215);
            this.chkInclude_4.Name = "chkInclude_4";
            this.chkInclude_4.Size = new System.Drawing.Size(135, 22);
            this.chkInclude_4.TabIndex = 14;
            this.chkInclude_4.Text = "Reference Field 1";
            // 
            // chkInclude_5
            // 
            this.chkInclude_5.Location = new System.Drawing.Point(20, 252);
            this.chkInclude_5.Name = "chkInclude_5";
            this.chkInclude_5.Size = new System.Drawing.Size(135, 22);
            this.chkInclude_5.TabIndex = 13;
            this.chkInclude_5.Text = "Reference Field 2";
            // 
            // chkInclude_6
            // 
            this.chkInclude_6.Location = new System.Drawing.Point(206, 30);
            this.chkInclude_6.Name = "chkInclude_6";
            this.chkInclude_6.Size = new System.Drawing.Size(124, 22);
            this.chkInclude_6.TabIndex = 12;
            this.chkInclude_6.Text = "Mailing Address";
            // 
            // chkInclude_9
            // 
            this.chkInclude_9.Location = new System.Drawing.Point(206, 141);
            this.chkInclude_9.Name = "chkInclude_9";
            this.chkInclude_9.Size = new System.Drawing.Size(145, 22);
            this.chkInclude_9.TabIndex = 11;
            this.chkInclude_9.Text = "Current Land Value";
            // 
            // chkInclude_10
            // 
            this.chkInclude_10.Location = new System.Drawing.Point(206, 178);
            this.chkInclude_10.Name = "chkInclude_10";
            this.chkInclude_10.Size = new System.Drawing.Size(162, 22);
            this.chkInclude_10.TabIndex = 10;
            this.chkInclude_10.Text = "Current Building Value";
            // 
            // chkInclude_11
            // 
            this.chkInclude_11.Location = new System.Drawing.Point(206, 215);
            this.chkInclude_11.Name = "chkInclude_11";
            this.chkInclude_11.Size = new System.Drawing.Size(145, 22);
            this.chkInclude_11.TabIndex = 9;
            this.chkInclude_11.Text = "Current Total Value";
            // 
            // chkInclude_12
            // 
            this.chkInclude_12.Location = new System.Drawing.Point(415, 30);
            this.chkInclude_12.Name = "chkInclude_12";
            this.chkInclude_12.Size = new System.Drawing.Size(136, 22);
            this.chkInclude_12.TabIndex = 8;
            this.chkInclude_12.Text = "Billing Land Value";
            // 
            // chkInclude_13
            // 
            this.chkInclude_13.Location = new System.Drawing.Point(415, 65);
            this.chkInclude_13.Name = "chkInclude_13";
            this.chkInclude_13.Size = new System.Drawing.Size(153, 22);
            this.chkInclude_13.TabIndex = 7;
            this.chkInclude_13.Text = "Billing Building Value";
            // 
            // chkInclude_14
            // 
            this.chkInclude_14.Location = new System.Drawing.Point(415, 104);
            this.chkInclude_14.Name = "chkInclude_14";
            this.chkInclude_14.Size = new System.Drawing.Size(136, 22);
            this.chkInclude_14.TabIndex = 6;
            this.chkInclude_14.Text = "Billing Total Value";
            // 
            // chkInclude_15
            // 
            this.chkInclude_15.Location = new System.Drawing.Point(415, 141);
            this.chkInclude_15.Name = "chkInclude_15";
            this.chkInclude_15.Size = new System.Drawing.Size(142, 22);
            this.chkInclude_15.TabIndex = 5;
            this.chkInclude_15.Text = "Exemption Amount";
            // 
            // chkInclude_16
            // 
            this.chkInclude_16.Location = new System.Drawing.Point(415, 178);
            this.chkInclude_16.Name = "chkInclude_16";
            this.chkInclude_16.Size = new System.Drawing.Size(127, 22);
            this.chkInclude_16.TabIndex = 4;
            this.chkInclude_16.Text = "Net Assessment";
            // 
            // chkInclude_8
            // 
            this.chkInclude_8.Location = new System.Drawing.Point(206, 104);
            this.chkInclude_8.Name = "chkInclude_8";
            this.chkInclude_8.Size = new System.Drawing.Size(80, 22);
            this.chkInclude_8.TabIndex = 3;
            this.chkInclude_8.Text = "Acreage";
            // 
            // chkInclude_17
            // 
            this.chkInclude_17.Location = new System.Drawing.Point(415, 215);
            this.chkInclude_17.Name = "chkInclude_17";
            this.chkInclude_17.Size = new System.Drawing.Size(65, 22);
            this.chkInclude_17.TabIndex = 2;
            this.chkInclude_17.Text = "SFLA";
            // 
            // chkInclude_7
            // 
            this.chkInclude_7.Location = new System.Drawing.Point(206, 65);
            this.chkInclude_7.Name = "chkInclude_7";
            this.chkInclude_7.Size = new System.Drawing.Size(143, 22);
            this.chkInclude_7.TabIndex = 1;
            this.chkInclude_7.Text = "Telephone Number";
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(795, 458);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(39, 18);
            this.Label20.TabIndex = 24;
            this.Label20.Text = "USE";
            this.Label20.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(65, 447);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(107, 17);
            this.Label19.TabIndex = 23;
            this.Label19.Text = "CODES";
            // 
            // cmdContinue
            // 
            this.cmdContinue.AppearanceKey = "acceptButton";
            this.cmdContinue.Location = new System.Drawing.Point(379, 30);
            this.cmdContinue.Name = "cmdContinue";
            this.cmdContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdContinue.Size = new System.Drawing.Size(140, 48);
            this.cmdContinue.TabIndex = 21;
            this.cmdContinue.Text = "Create Extract";
            this.cmdContinue.Click += new System.EventHandler(this.mnuContinue_Click);
            // 
            // frmDBExtract
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(857, 762);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDBExtract";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Create Database Extract";
            this.Load += new System.EventHandler(this.frmDBExtract_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDBExtract_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBPOption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkColumnHeaders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContinue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdContinue;
	}
}