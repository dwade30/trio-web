﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmStatus.
	/// </summary>
	public partial class frmStatus : BaseForm
	{
		public frmStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmStatus InstancePtr
		{
			get
			{
				return (frmStatus)Sys.GetInstance(typeof(frmStatus));
			}
		}

		protected frmStatus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmStatus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmStatus properties;
			//frmStatus.ScaleWidth	= 5880;
			//frmStatus.ScaleHeight	= 3810;
			//frmStatus.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			FillGrid();
		}

		private void frmStatus_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void FillGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vbPorter upgrade warning: dtDate As DateTime	OnWrite(short, DateTime)
			DateTime dtDate;
			DateTime dtTempDate;
			DateTime dtTempDate2;
			// vbPorter upgrade warning: dtCalcDate As DateTime	OnWriteFCConvert.ToInt16(
			DateTime dtCalcDate;
			// vbPorter upgrade warning: dtValDate As DateTime	OnWriteFCConvert.ToInt16(
			DateTime dtValDate;
			// vbPorter upgrade warning: dtDataDate As DateTime	OnWrite(short, DateTime)
			DateTime dtDataDate;
			//FileSystemObject fso = new FileSystemObject();
			Grid.ColDataType(1, FCGrid.DataTypeSettings.flexDTBoolean);
            Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			clsLoad.OpenRecordset("select * from status", modGlobalVariables.strREDatabase);
			if (clsLoad.EndOfFile())
			{
				clsLoad.AddNew();
				clsLoad.Update();
			}
			Grid.Rows = 1;
			Grid.TextMatrix(0, 0, "Description");
			Grid.TextMatrix(0, 1, "Done");
			Grid.TextMatrix(0, 2, "Last Performed");
			dtDate = DateTime.FromOADate(0);
			if (modREMain.Statics.boolShortRealEstate)
			{
				if (Information.IsDate(clsLoad.Get_Fields("accountschange")))
				{
					if (clsLoad.Get_Fields_DateTime("accountschange").ToOADate() == 0)
					{
						Grid.AddItem("Account(s) Values Updated" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtDate = (DateTime)clsLoad.Get_Fields_DateTime("accountschange");
						Grid.AddItem("Account(s) Values Updated" + "\t" + true + "\t" + Strings.Format(dtDate, "MM/dd/yyyy h:mm tt"));
					}
				}
				else
				{
					Grid.AddItem("Account(s) Values Updated" + "\t" + false + "\t" + "Unknown");
				}
				if (Information.IsDate(clsLoad.Get_Fields("CalcExemptions")))
				{
					// TODO Get_Fields: Field [Caclexemptions] not found!! (maybe it is an alias?)
					if (clsLoad.Get_Fields_DateTime("Caclexemptions").ToOADate() == 0)
					{
						Grid.AddItem("Calculated Exemptions" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtTempDate = (DateTime)clsLoad.Get_Fields_DateTime("calcexemptions");
						if (DateAndTime.DateDiff("n", dtDate, dtTempDate) >= 0 && (dtDate.ToOADate() > 0))
						{
							// if second date is >=
							Grid.AddItem("Calculated Exemptions" + "\t" + true + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
							dtDate = dtTempDate;
						}
						else
						{
							// first date is > or is 0
							dtDate = DateTime.FromOADate(0);
							Grid.AddItem("Calculated Exemptions" + "\t" + false + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
					}
				}
				else
				{
					Grid.AddItem("Calculated Exemptions" + "\t" + false + "\t" + "Unknown");
				}
			}
			else
			{
				// full assessing
				dtDataDate = DateTime.FromOADate(0);
				if (Information.IsDate(clsLoad.Get_Fields("DataChanged")))
				{
					//FC:FINAL:AAKV - exception while opening form
					//if (FCConvert.ToInt32(clsLoad.Get_Fields("Datachanged")) == 0)
					if (clsLoad.Get_Fields_DateTime("Datachanged").ToOADate() == 0)
					{
						Grid.AddItem("Changed Data" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtDate = (DateTime)clsLoad.Get_Fields_DateTime("DataChanged");
						dtDataDate = dtDate;
						Grid.AddItem("Changed Data" + "\t" + true + "\t" + Strings.Format(dtDate, "MM/dd/yyyy h:mm tt"));
					}
				}
				else
				{
					Grid.AddItem("Changed Data" + "\t" + false + "\t" + "Unknown");
				}
				dtCalcDate = DateTime.FromOADate(0);
				if (Information.IsDate(clsLoad.Get_Fields("Calculated")))
				{
					//FC:FINAL:AAKV - exception while opening form
					if (clsLoad.Get_Fields_DateTime("Calculated").ToOADate() == 0)
					{
						Grid.AddItem("Calculated Account(s)" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtCalcDate = (DateTime)clsLoad.Get_Fields_DateTime("Calculated");
						if (DateAndTime.DateDiff("n", dtDate, dtCalcDate) >= 0)
						{
							Grid.AddItem("Calculated Account(s)" + "\t" + true + "\t" + Strings.Format(dtCalcDate, "MM/dd/yyyy h:mm tt"));
						}
						else
						{
							Grid.AddItem("Calculated Account(s)" + "\t" + false + "\t" + Strings.Format(dtCalcDate, "MM/dd/yyyy h:mm tt"));
						}
					}
				}
				else
				{
					Grid.AddItem("Calculated Account(s)" + "\t" + false + "\t" + "Unknown");
				}
				if (Information.IsDate(clsLoad.Get_Fields("batchcalc")))
				{
					//FC:FINAL:AAKV - exception while opening form
					if (clsLoad.Get_Fields_DateTime("batchcalc").ToOADate() == 0)
					{
						dtDate = DateTime.FromOADate(0);
						Grid.AddItem("Batch Calculation" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtTempDate = (DateTime)clsLoad.Get_Fields_DateTime("batchcalc");
						if (DateAndTime.DateDiff("n", dtDate, dtTempDate) >= 0 && (dtDate.ToOADate() > 0))
						{
							// did an or on this one since manually changing billing vals and calculating
							// amount to the same thing
							if (DateAndTime.DateDiff("n", dtDate, dtTempDate) > 0)
							{
								dtDate = dtTempDate;
							}
							Grid.AddItem("Batch Calculation" + "\t" + true + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
						else
						{
							dtDate = DateTime.FromOADate(0);
							Grid.AddItem("Batch Calculation" + "\t" + false + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
					}
				}
				else
				{
					dtDate = DateTime.FromOADate(0);
					Grid.AddItem("Batch Calculation" + "\t" + false + "\t" + "Unknown");
				}
				dtValDate = DateTime.FromOADate(0);
				if (Information.IsDate(clsLoad.Get_Fields("accountschange")))
				{
					//FC:FINAL:AAKV - exception while opening form
					if (clsLoad.Get_Fields_DateTime("accountschange").ToOADate() == 0)
					{
						Grid.AddItem("Edited Billing Values" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtValDate = (DateTime)clsLoad.Get_Fields_DateTime("accountschange");
						if (DateAndTime.DateDiff("n", dtDataDate, dtValDate) >= 0)
						{
							Grid.AddItem("Edited Billing Values" + "\t" + true + "\t" + Strings.Format(dtValDate, "MM/dd/yyyy h:mm tt"));
						}
						else
						{
							Grid.AddItem("Edited Billing Values" + "\t" + false + "\t" + Strings.Format(dtValDate, "MM/dd/yyyy h:mm tt"));
						}
					}
				}
				else
				{
					Grid.AddItem("Edited Billing Values" + "\t" + false + "\t" + "Unknown");
				}
				if (Information.IsDate(clsLoad.Get_Fields("xferbillingvalues")))
				{
					//FC:FINAL:AAKV - exception while opening form
					if (clsLoad.Get_Fields_DateTime("xferbillingvalues").ToOADate() == 0)
					{
						dtDate = DateTime.FromOADate(0);
						Grid.AddItem("Commit to Billing Values" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtTempDate = (DateTime)clsLoad.Get_Fields_DateTime("xferbillingvalues");
						if (DateAndTime.DateDiff("n", dtDate, dtTempDate) >= 0 && (dtDate.ToOADate() > 0) && (DateAndTime.DateDiff("n", dtCalcDate, dtTempDate) >= 0))
						{
							dtDate = dtTempDate;
							Grid.AddItem("Commit to Billing Values" + "\t" + true + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
						else
						{
							dtDate = DateTime.FromOADate(0);
							Grid.AddItem("Commit to Billing Values" + "\t" + false + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
					}
				}
				else
				{
					dtDate = DateTime.FromOADate(0);
					Grid.AddItem("Commit to Billing Values" + "\t" + false + "\t" + "Unknown");
				}
				if (Information.IsDate(clsLoad.Get_Fields("calcexemptions")))
				{
					//FC:FINAL:AAKV - exception while opening form
					if (clsLoad.Get_Fields_DateTime("calcexemptions").ToOADate() == 0)
					{
						dtDate = DateTime.FromOADate(0);
						Grid.AddItem("Calculated Exemptions" + "\t" + false + "\t" + "Unknown");
					}
					else
					{
						dtTempDate = (DateTime)clsLoad.Get_Fields_DateTime("CalcExemptions");
						if (DateAndTime.DateDiff("n", dtDate, dtTempDate) >= 0 && (dtDate.ToOADate() > 0) && (DateAndTime.DateDiff("n", dtValDate, dtTempDate) >= 0))
						{
							dtDate = dtTempDate;
							Grid.AddItem("Calculated Exemptions" + "\t" + true + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
						else
						{
							dtDate = DateTime.FromOADate(0);
							Grid.AddItem("Calculated Exemptions" + "\t" + false + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
						}
					}
				}
				else
				{
					dtDate = DateTime.FromOADate(0);
					Grid.AddItem("Calculated Exemptions" + "\t" + false + "\t" + "Unknown");
				}
			}
			// if they have windows billing, add the xfer date
			if (modGlobalConstants.Statics.gboolBL)
			{
				if (File.Exists(modGlobalVariables.strCLDatabase))
				{
					// check when the last time they did a billing xfer
					clsLoad.OpenRecordset("select transferfrombillingdateFIRST from billingmaster where BILLINGTYPE = 'RE' order by TRANSFERFROMbillingdatefirst desc", modGlobalVariables.strCLDatabase);
					if (!clsLoad.EndOfFile())
					{
						if (Information.IsDate(clsLoad.Get_Fields("transferfrombillingdatefirst")))
						{
							if (clsLoad.Get_Fields_DateTime("transferfrombillingdatefirst").ToOADate() == 0)
							{
								Grid.AddItem("Transferred to Billing Records" + "\t" + false + "\t" + "Unknown");
							}
							else
							{
								dtTempDate = (DateTime)clsLoad.Get_Fields_DateTime("transferfrombillingdatefirst");
								clsLoad.OpenRecordset("select transferfrombillingdatelast from billingmaster where billingtype = 'RE' order by transferfrombillingdatelast desc", modGlobalVariables.strCLDatabase);
								if (!clsLoad.EndOfFile())
								{
									if (!fecherFoundation.FCUtils.IsEmptyDateTime(clsLoad.Get_Fields_DateTime("transferfrombillingdatelast")))
									{
										if (Information.IsDate(clsLoad.Get_Fields("transferfrombillingdatelast")) && clsLoad.Get_Fields_DateTime("transferfrombillingdatelast").ToOADate() != 0)
										{
											// see which is greater
											dtTempDate2 = (DateTime)clsLoad.Get_Fields_DateTime("transferfrombillingdatelast");
											if (DateAndTime.DateDiff("n", dtTempDate, dtTempDate2) > 0)
												dtTempDate = dtTempDate2;
										}
									}
								}
								if (DateAndTime.DateDiff("n", dtDate, dtTempDate) >= 0 && (dtDate.ToOADate() > 0))
								{
									Grid.AddItem("Transferred to Billing Records" + "\t" + true + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
								}
								else
								{
									Grid.AddItem("Transferred to Billing Records" + "\t" + false + "\t" + Strings.Format(dtTempDate, "MM/dd/yyyy h:mm tt"));
								}
							}
						}
						else
						{
							Grid.AddItem("Transferred to Billing Records" + "\t" + false + "\t" + "Unknown");
						}
					}
					else
					{
						Grid.AddItem("Transferred to Billing Records" + "\t" + false + "\t" + "Unknown");
					}
				}
			}
			ResizeGrid();
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.56 * GridWidth));
			Grid.ColWidth(1, FCConvert.ToInt32(0.1 * GridWidth));
			//FC:FINAL:AAKV - anchored in the design
			//Grid.Height = Grid.RowHeight(0) * Grid.Rows + 50;
		}
	}
}
