﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmShowSalesAnalysis.
	/// </summary>
	public partial class frmShowSalesAnalysis : BaseForm
	{
		public frmShowSalesAnalysis()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmShowSalesAnalysis InstancePtr
		{
			get
			{
				return (frmShowSalesAnalysis)Sys.GetInstance(typeof(frmShowSalesAnalysis));
			}
		}

		protected frmShowSalesAnalysis _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		float sngSaleFactor;
		float sngLandFactor;
		float sngBldgFactor;
		float mean;
		float Median;
		float MidQuartMean;
		float WeightedMean;
		float AveDev;
		float CoeffOfDisp;
		float StandDev;
		float CoeffofVar;
		float PriceDiff;
		float MeantoUse;
		int intWhatMean;
		int intMidQuartPercent;
		int TotSale;
		int TotValuation;
		int TotDeviation;
		int sortColumn;
		// vbPorter upgrade warning: sortOrder As short --> As int	OnWrite(FCGrid.SortSettings, int)
		FCGrid.SortSettings sortOrder;
		bool boolCorrelated;
		int intValidityCode;
		int lngUID;
		// vbPorter upgrade warning: intSFact As short --> As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intLFact As short --> As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intBFact As short --> As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intQuartPer As short --> As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intWhat As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intValidity As short	OnWriteFCConvert.ToInt32(
		public object Init(ref bool boolCorr, int intSFact, int intLFact, int intBFact, int intQuartPer, ref int intWhat, int intValidity)
		{
			object Init = null;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsTemp = new clsDRWrapper();
				lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
				boolCorrelated = boolCorr;
				sortColumn = 5;
				sortOrder = FCGrid.SortSettings.flexSortNumericAscending;
				intValidityCode = intValidity;
				sngSaleFactor = intSFact;
				sngSaleFactor /= 100;
				sngLandFactor = intLFact;
				sngLandFactor /= 100;
				sngBldgFactor = intBFact;
				sngBldgFactor /= 100;
				intMidQuartPercent = intQuartPer;
				intWhatMean = intWhat;
				txtSaleFactor.Text = FCConvert.ToString(sngSaleFactor * 100);
				//VScroll1.Value = FCConvert.ToInt32(Conversion.Val(txtSaleFactor.Text));
				txtLandFactor.Text = FCConvert.ToString(sngLandFactor * 100);
				//VScroll2.Value = FCConvert.ToInt32(Conversion.Val(txtLandFactor.Text));
				txtBldgFactor.Text = FCConvert.ToString(sngBldgFactor * 100);
				//VScroll3.Value = FCConvert.ToInt32(Conversion.Val(txtBldgFactor.Text));
				clsTemp.OpenRecordset("select * from extract where userid = " + FCConvert.ToString(lngUID) + " order by reportnumber", modGlobalVariables.strREDatabase);
				lblTitle.Text = FCConvert.ToString(clsTemp.Get_Fields_String("title"));
				if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("title"))) == string.Empty)
				{
					clsTemp.MoveNext();
					if (!clsTemp.EndOfFile())
					{
						lblTitle.Text = FCConvert.ToString(clsTemp.Get_Fields_String("title"));
					}
					// Call clsTemp.FindFirstRecord("reportnumber", 0)
					clsTemp.FindFirst("reportnumber = 0");
				}
				this.Show(App.MainForm);
				return Init;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Show Sales Analysis Init", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Init;
		}

		private void SetupGrid2()
		{
			Grid2.ExtendLastCol = true;
			Grid2.TextMatrix(0, 0, "Median");
			Grid2.TextMatrix(1, 0, "Mean");
			Grid2.TextMatrix(2, 0, "Mid-Quartile Mean");
			Grid2.TextMatrix(3, 0, "Weighted Mean");
			Grid2.TextMatrix(4, 0, "Average Deviation");
			Grid2.TextMatrix(5, 0, "Coefficient of Dispersion");
			Grid2.TextMatrix(6, 0, "Standard Deviation");
			Grid2.TextMatrix(7, 0, "Coefficient of Variation");
			Grid2.TextMatrix(8, 0, "Price-Related Differential");
			Grid2.ColWidth(0, 2160);
			Grid2.ColWidth(1, 720);
		}

		private void SetupGrid1()
		{
			Grid1.Rows = 2;
			Grid1.Cols = 12;
			// Grid1.ColWidth(0) = 720
			// Grid1.ColWidth(1) = 1800
			// Grid1.ColWidth(2) = 1080
			// Grid1.ColWidth(3) = 1440
			// Grid1.ColWidth(4) = 1440
			// Grid1.ColWidth(5) = 720
			// Grid1.ColWidth(6) = 720
			// Grid1.ColWidth(7) = 720
			Grid1.ColHidden(8, true);
			Grid1.ColHidden(9, true);
			Grid1.ColHidden(10, true);
			Grid1.ColHidden(11, true);
			//FC:FINAL:MSH - i.issue #1341: change DateType of the first column for correct sorting
			Grid1.ColDataType(0, FCGrid.DataTypeSettings.flexDTLong);
			Grid1.ColDataType(2, FCGrid.DataTypeSettings.flexDTDate);
			//FC:FINAL:MSH - i.issue #1344: sorting columns with numeric values will be incorrect, because type of column - string
			Grid1.ColDataType(3, FCGrid.DataTypeSettings.flexDTDouble);
			Grid1.ColDataType(4, FCGrid.DataTypeSettings.flexDTDouble);
			Grid1.ColDataType(5, FCGrid.DataTypeSettings.flexDTDouble);
			Grid1.ColDataType(6, FCGrid.DataTypeSettings.flexDTDouble);
			Grid1.ColDataType(7, FCGrid.DataTypeSettings.flexDTDouble);
			Grid1.TextMatrix(0, 0, "ACCT");
			Grid1.TextMatrix(0, 1, "Map/Lot");
			Grid1.TextMatrix(0, 2, "Sale Date");
			Grid1.TextMatrix(0, 3, "Sale Price");
			Grid1.TextMatrix(0, 4, "Valuation");
			Grid1.TextMatrix(0, 5, "Ratio");
			Grid1.TextMatrix(0, 6, "Mean");
			Grid1.TextMatrix(0, 7, "Dev");
            //FC:FINAL:MSH - issue #1701: change alignment of headers and data in the grid
            //Grid1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 7, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            Grid1.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid1.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid1.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid1.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
            Grid1.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
            Grid1.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
            Grid1.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
            Grid1.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
            Grid1.ColFormat(3, "#,###,###,##0");
			Grid1.ColFormat(4, "#,###,###,##0");
			Grid1.ColSort(0, FCGrid.SortSettings.flexSortNumericAscending);
			Grid1.ColSort(1, FCGrid.SortSettings.flexSortStringAscending);
			Grid1.ColSort(2, FCGrid.SortSettings.flexSortNumericAscending);
			Grid1.ColSort(3, FCGrid.SortSettings.flexSortNumericAscending);
			Grid1.ColSort(4, FCGrid.SortSettings.flexSortNumericAscending);
			Grid1.ColSort(5, FCGrid.SortSettings.flexSortNumericAscending);
			Grid1.ColSort(6, FCGrid.SortSettings.flexSortNumericDescending);
			Grid1.ColSort(7, FCGrid.SortSettings.flexSortNumericDescending);
			Grid1.ExtendLastCol = true;
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			object boolAscend;
			if (Grid1.Rows > 1)
			{
				modGlobalVariables.Statics.SalesAnalRec.AveDev = AveDev;
				modGlobalVariables.Statics.SalesAnalRec.BldgFactor = sngBldgFactor;
				modGlobalVariables.Statics.SalesAnalRec.LandFactor = sngLandFactor;
				modGlobalVariables.Statics.SalesAnalRec.mean = mean;
				modGlobalVariables.Statics.SalesAnalRec.Median = Median;
				modGlobalVariables.Statics.SalesAnalRec.MidQuart = MidQuartMean;
				modGlobalVariables.Statics.SalesAnalRec.SaleFactor = sngSaleFactor;
				modGlobalVariables.Statics.SalesAnalRec.StandDev = StandDev;
				modGlobalVariables.Statics.SalesAnalRec.TotValuation = TotValuation;
				modGlobalVariables.Statics.SalesAnalRec.TotSale = TotSale;
				modGlobalVariables.Statics.SalesAnalRec.CoeffOfDisp = FCConvert.ToSingle(Conversion.Val(Grid2.TextMatrix(5, 1)));
				modGlobalVariables.Statics.SalesAnalRec.CoeffofVar = FCConvert.ToSingle(Conversion.Val(Grid2.TextMatrix(7, 1)));
				modGlobalVariables.Statics.SalesAnalRec.PriceDifferential = FCConvert.ToSingle(Conversion.Val(Grid2.TextMatrix(8, 1)));
				modGlobalVariables.Statics.SalesAnalRec.TotDeviation = TotDeviation;
				modGlobalVariables.Statics.SalesAnalRec.Weighted = FCConvert.ToSingle(Conversion.Val(Grid2.TextMatrix(3, 1)));
				modGlobalVariables.Statics.SalesAnalRec.MeantoUse = MeantoUse;
				// Select Case sortOrder
				// Case flexSortGenericAscending, flexSortNumericAscending, flexSortStringAscending
				// boolAscend = True
				// Case flexSortGenericDescending, flexSortNumericDescending, flexSortStringDescending
				// boolAscend = False
				// End Select
				rptSalesAnalysis.InstancePtr.Init(Grid1.Rows - 1);
			}
			else
			{
				MessageBox.Show("There is no data to print", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void cmdRecalculate_Click(object sender, System.EventArgs e)
		{
			Calculate_Cells();
		}

		private void frmShowSalesAnalysis_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmShowSalesAnalysis properties;
			//frmShowSalesAnalysis.ScaleWidth	= 9300;
			//frmShowSalesAnalysis.ScaleHeight	= 8445;
			//frmShowSalesAnalysis.LinkTopic	= "Form1";
			//frmShowSalesAnalysis.LockControls	= true;
			//End Unmaped Properties
			sortColumn = 5;
			sortOrder = FCGrid.SortSettings.flexSortNumericAscending;
			SetupGrid1();
			SetupGrid2();
			FillGrid1();
			if (Grid1.Rows == 2)
			{
				// check if its because there was one record or none
				if (Strings.Trim(Grid1.TextMatrix(1, 1) + "") == string.Empty)
				{
					return;
				}
			}
			Calculate_Cells();
			modGlobalFunctions.SetFixedSize(this);
			//FC:FINAL:MSH - i.issue #1344: assign handler for handling sorting only after loading form
			this.Grid1.Sorted += new System.EventHandler(this.Grid1_AfterSort);
		}

		private void FillGrid1()
		{
			clsDRWrapper clsSale = new clsDRWrapper();
			string strland = "";
			string strbldg = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strWhere = "";
			try
			{
				// On Error GoTo ErrorHandler
				if (boolCorrelated)
				{
					strland = "RLlandval";
					strbldg = "rlbldgval";
				}
				else
				{
					strland = "lastlandval";
					strbldg = "lastbldgval";
				}
				if (intValidityCode > 0)
				{
					strWhere = " and srmaster.piSALEvalidity = " + FCConvert.ToString(intValidityCode) + " ";
				}
				else
				{
					strWhere = "";
				}
				// Call clsTemp.OpenRecordset("select  srmaster.* from (srmaster inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) ) on (srmaster.rsaccount = extracttable.accountnumber) and (srmaster.rscard = extracttable.cardnumber) and (srmaster.saledate = extracttable.saledate)) where srmaster.rsaccount > 0 and rscard = 1 and not rsdeleted = 1 order by srmaster.rsaccount,srmaster.rscard,srmaster.saledate", strredatabase)
				clsTemp.OpenRecordset("select  srmaster.* from srmaster where srmaster.saleid in  (select srmaster.saleid from srmaster inner join extracttable  on (srmaster.rsaccount = extracttable.accountnumber)  and (srmaster.saledate = extracttable.saledate) and (srmaster.saleid = extracttable.saleid) where extracttable.userid = " + FCConvert.ToString(lngUID) + " AND srmaster.rsaccount > 0 and rscard = 1 and not rsdeleted = 1 " + strWhere + ") and srmaster.rscard = 1  order by srmaster.saleid,srmaster.rsaccount,srmaster.rscard", modGlobalVariables.strREDatabase);
				// Call clsSale.OpenRecordset("select srmaster.rsaccount,srmaster.saledate,srmaster.saleid, sum(" & strland & ") as landsum, sum(" & strbldg & ") as bldgsum from (srmaster inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) ) on (srmaster.rsaccount = extracttable.accountnumber) and (srmaster.rscard = extracttable.cardnumber) and (srmaster.saledate = extracttable.saledate)) where srmaster.rsaccount > 0 and not rsdeleted = 1 group by srmaster.rsaccount,srmaster.saledate,srmaster.saleid order by srmaster.rsaccount,srmaster.saledate", strredatabase)
				clsSale.OpenRecordset("select srmaster.rsaccount,srmaster.saledate,srmaster.saleid,sum(" + strland + ") as landsum , sum(" + strbldg + ") as bldgsum from srmaster where srmaster.saleid in (select srmaster.saleid from srmaster inner join extracttable on (srmaster.rsaccount = extracttable.accountnumber) and (srmaster.saledate = extracttable.saledate) and (srmaster.saleid = extracttable.saleid) where extracttable.userid = " + FCConvert.ToString(lngUID) + " AND srmaster.rsaccount > 0 and not rsdeleted = 1 " + strWhere + " ) group by srmaster.rsaccount,srmaster.saledate,srmaster.saleid order by srmaster.rsaccount,srmaster.saledate", modGlobalVariables.strREDatabase);
				if (clsSale.EndOfFile())
				{
					MessageBox.Show("Could not find any sale files that meet the extract criteria", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
					// Unload Me
					return;
				}
				while (!clsSale.EndOfFile())
				{
					if (clsSale.Get_Fields_Int32("saleid") > 0)
					{
						// 4            Call clsTemp.FindFirstRecord("saleid", clsSale.Fields("saleid"))
						clsTemp.FindFirst("saleid = " + clsSale.Get_Fields_Int32("saleid"));
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
						Grid1.AddItem(FCConvert.ToString(Conversion.Val(clsSale.Get_Fields_Int32("rsaccount"))) + "\t" + clsTemp.Get_Fields_String("rsmaplot") + "\t" + Strings.Format(clsSale.Get_Fields_DateTime("saledate"), "MM/dd/yyyy") + "\t" + clsTemp.Get_Fields_Int32("pisaleprice") + "\t" + FCConvert.ToString(Conversion.Val(clsSale.Get_Fields("landsum"))) + Conversion.Val(clsSale.Get_Fields("bldgsum")) + "\t" + "\t" + "\t" + "\t" + FCConvert.ToString(Conversion.Val(clsSale.Get_Fields("landsum"))) + "\t" + FCConvert.ToString(Conversion.Val(clsSale.Get_Fields("bldgsum"))) + "\t" + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_Int32("pisaleprice"))) + "\t" + clsSale.Get_Fields_Int32("saleid"));
					}
					clsSale.MoveNext();
				}
				// delete second row because it is empty.  This is so the formatting of the title row wont affect the others
				Grid1.RemoveItem(1);
				lblSalesTot.Text = "Total Sales:  " + FCConvert.ToString(Grid1.Rows - 1);
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 6, Grid1.Rows - 1, 6, Information.RGB(180, 50, 210));
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 6, Grid1.Rows - 1, 6, Information.RGB(255, 255, 255));
				switch (intWhatMean)
				{
					case 1:
						{
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, Information.RGB(180, 50, 210));
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, Information.RGB(255, 255, 255));
							Grid2.Row = 1;
							break;
						}
					case 2:
						{
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1, Information.RGB(180, 50, 210));
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 1, Information.RGB(255, 255, 255));
							Grid2.Row = 0;
							break;
						}
					case 3:
						{
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 1, Information.RGB(180, 50, 210));
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 2, 1, Information.RGB(255, 255, 255));
							Grid2.Row = 2;
							break;
						}
					case 4:
						{
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Information.RGB(180, 50, 210));
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 3, 1, Information.RGB(255, 255, 255));
							Grid2.Row = 3;
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "In line " + Information.Erl() + " in FillGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmShowSalesAnalysis_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid1();
			ResizeGrid2();
		}

		private void Grid1_AfterSort(object sender, EventArgs e)
		{
			sortColumn = Grid1.GetFlexColIndex(Grid1.SortedColumn.Index);
			sortOrder = (FCGrid.SortSettings)Grid1.SortOrder;
		}

		private void Grid1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				if (Grid1.Row > 0)
				{
					Grid1.RemoveItem(Grid1.Row);
					Calculate_Cells();
				}
			}
		}

		private void Grid1_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				Grid1.Row = Grid1.MouseRow;
				this.PopupMenu(mnuOpt);
			}
		}

		private void Grid2_ClickEvent(object sender, System.EventArgs e)
		{
			if (Grid2.Row < 4)
			{
				Grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1, 3, 1, Information.RGB(255, 255, 255));
				Grid2.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 1, 3, 1, Information.RGB(1, 1, 1));
				switch (Grid2.Row)
				{
					case 0:
						{
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1, Information.RGB(180, 50, 210));
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 1, Information.RGB(255, 255, 255));
							intWhatMean = 2;
							break;
						}
					case 1:
						{
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, Information.RGB(180, 50, 210));
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, Information.RGB(255, 255, 255));
							intWhatMean = 1;
							break;
						}
					case 2:
						{
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 1, Information.RGB(180, 50, 210));
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 2, 1, Information.RGB(255, 255, 255));
							intWhatMean = 3;
							break;
						}
					case 3:
						{
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Information.RGB(180, 50, 210));
							Grid2.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 3, 1, Information.RGB(255, 255, 255));
							intWhatMean = 4;
							break;
						}
				}
				//end switch
			}
			Calculate_Cells();
		}

		private void mnuDel_Click(object sender, System.EventArgs e)
		{
			if (Grid1.Row > 0)
			{
				Grid1.RemoveItem(Grid1.Row);
				Calculate_Cells();
			}
		}

		private void mnuView_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.boolFromSalesAnalysis = true;
			modGlobalVariables.Statics.boolRegRecords = false;
			modGlobalVariables.Statics.boolBillingorCorrelated = false;
			modGlobalVariables.Statics.gcurrsaledate = Grid1.TextMatrix(Grid1.Row, 2);
			modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid1.TextMatrix(Grid1.Row, 0))));
			modGNBas.Statics.gintCardNumber = 1;
			clsDRWrapper temp = modDataTypes.Statics.MR;
			modREMain.OpenMasterTable(ref temp, Conversion.Val(Grid1.TextMatrix(Grid1.Row, 0)), 1, Grid1.TextMatrix(Grid1.Row, 2), Grid1.TextMatrix(Grid1.Row, 11));
			modDataTypes.Statics.MR = temp;
			modGlobalVariables.Statics.glngSaleID = FCConvert.ToInt32(Grid1.TextMatrix(Grid1.Row, 11));
			if (modGlobalRoutines.Formisloaded_2("frmreproperty"))
			{
				frmREProperty.InstancePtr.Unload();
			}
			modGNBas.Statics.gboolLoadingDone = true;
			//! Load frmREProperty;
			frmREProperty.InstancePtr.Show(App.MainForm);
		}

		private void txtBldgFactor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtBldgFactor.Text) <= 0)
			{
				txtBldgFactor.Text = FCConvert.ToString(0);
			}
			//if (Conversion.Val(txtBldgFactor.Text) > VScroll3.Minimum)
			//{
			//    txtBldgFactor.Text = FCConvert.ToString(VScroll3.Minimum);
			//}
			//if (Conversion.Val(txtBldgFactor.Text) != VScroll3.Value)
			//{
			//    sngBldgFactor = FCConvert.ToSingle(FCConvert.ToDouble(txtBldgFactor.Text)) / 100;
			//    VScroll3.Value = FCConvert.ToInt32(Conversion.Val(txtBldgFactor.Text));
			//}
			sngBldgFactor = FCConvert.ToSingle(FCConvert.ToDouble(txtBldgFactor.Text)) / 100;
		}

		private void txtLandFactor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtLandFactor.Text) <= 0)
			{
				txtLandFactor.Text = FCConvert.ToString(0);
			}
			//if (Conversion.Val(txtLandFactor.Text) > VScroll2.Minimum)
			//{
			//    txtLandFactor.Text = FCConvert.ToString(VScroll2.Minimum);
			//}
			//if (Conversion.Val(txtLandFactor.Text) != VScroll2.Value)
			//{
			//    sngLandFactor = FCConvert.ToSingle(FCConvert.ToDouble(txtLandFactor.Text)) / 100;
			//    VScroll2.Value = FCConvert.ToInt32(Conversion.Val(txtLandFactor.Text));
			//}
			sngLandFactor = FCConvert.ToSingle(FCConvert.ToDouble(txtLandFactor.Text)) / 100;
		}

		private void txtSaleFactor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtSaleFactor.Text) == 0)
			{
				txtSaleFactor.Text = FCConvert.ToString(0);
			}
			if (Conversion.Val(txtSaleFactor.Text) > 300)
				txtSaleFactor.Text = FCConvert.ToString(300);
			if (Conversion.Val(txtSaleFactor.Text) < -300)
				txtSaleFactor.Text = FCConvert.ToString(-300);
			//if (Conversion.Val(txtSaleFactor.Text) != VScroll1.Value)
			//{
			//    sngSaleFactor = FCConvert.ToSingle(FCConvert.ToDouble(txtSaleFactor.Text)) / 100;
			//    VScroll1.Value = FCConvert.ToInt32(Conversion.Val(txtSaleFactor.Text));
			//}
			sngSaleFactor = FCConvert.ToSingle(FCConvert.ToDouble(txtSaleFactor.Text)) / 100;
		}

		private void VScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			//txtSaleFactor.Text = FCConvert.ToString(VScroll1.Value);
			//sngSaleFactor = FCConvert.ToSingle(VScroll1.Value) / 100;
		}

		private void VScroll2_ValueChanged(object sender, System.EventArgs e)
		{
			//txtLandFactor.Text = FCConvert.ToString(VScroll2.Value);
			//sngLandFactor = FCConvert.ToSingle(VScroll2.Value) / 100;
		}

		private void VScroll3_ValueChanged(object sender, System.EventArgs e)
		{
			//txtBldgFactor.Text = FCConvert.ToString(VScroll3.Value);
			//sngBldgFactor = FCConvert.ToSingle(VScroll3.Value) / 100;
		}

		public void Calculate_Cells()
		{
			try
			{
				// On Error GoTo ErrorHandler
				int x;
				// vbPorter upgrade warning: SalePrice As int	OnWrite(int, double)
				int SalePrice = 0;
				double totRatio;
				// vbPorter upgrade warning: sngSaleRateMonth As double	OnWriteFCConvert.ToSingle(
				double sngSaleRateMonth = 0;
				// vbPorter upgrade warning: Landval As int	OnWrite(int, float)
				int Landval = 0;
				// vbPorter upgrade warning: Bldgval As int	OnWrite(int, float)
				int Bldgval = 0;
				// vbPorter upgrade warning: intTemp As short, int --> As long
				long intTemp;
				double TotDev;
				double currDev = 0;
				double totSquaredev;
				double CRatio = 0;
				int lngNumRecs;
				int intSortCol;
				int intSortOrder;
				TotSale = 0;
				TotValuation = 0;
				// loop through each cell. trend any values that need to be and calc means, etc.
				totRatio = 0;
				lngNumRecs = 0;
				lblSalesTot.Text = "Total Sales: " + FCConvert.ToString(Grid1.Rows - 1);
				for (x = 1; x <= Grid1.Rows - 1; x++)
				{
					SalePrice = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid1.TextMatrix(x, 10))));
					if (sngSaleFactor != 0)
					{
						// truncates to whole year
						intTemp = DateAndTime.DateDiff("m", DateTime.Today, FCConvert.ToDateTime(Grid1.TextMatrix(x, 2)));
						intTemp = Math.Abs(FCConvert.ToInt16(intTemp));
						if (intTemp > 0)
						{
							//FC:FINAL:MSH - i.issue #1344: convert values to float for preventing differences in calculation between web and original apps
							sngSaleRateMonth = ((sngSaleFactor * (SalePrice * 1.0f)) / (12 * 1.0f));
							SalePrice += FCConvert.ToInt32((sngSaleRateMonth * intTemp));
						}
					}
					Grid1.TextMatrix(x, 3, FCConvert.ToString(SalePrice));
					TotSale += SalePrice;
					Landval = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid1.TextMatrix(x, 8))));
					if (sngLandFactor != 0)
					{
						Landval = FCConvert.ToInt32((sngLandFactor * Landval));
					}
					Bldgval = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid1.TextMatrix(x, 9))));
					if (sngBldgFactor != 0)
					{
						Bldgval = FCConvert.ToInt32(sngBldgFactor * Bldgval);
					}
					Grid1.TextMatrix(x, 4, FCConvert.ToString(Landval + Bldgval));
					TotValuation += Landval + Bldgval;
					if ((TotValuation > 0) && (SalePrice > 0))
					{
						if (SalePrice == 0)
						{
							CRatio = 0;
						}
						else
						{
							// 19            CRatio = CLng(Round(((CDbl(Landval + Bldgval) / SalePrice) * 100), 4))
							CRatio = modGlobalRoutines.Round(((FCConvert.ToDouble(Landval + Bldgval) / SalePrice) * 100), 4);
						}
						Grid1.TextMatrix(x, 5, Strings.Format(CRatio, "0.0000"));
						totRatio += CRatio;
						lngNumRecs += 1;
					}
					else
					{
						Grid1.TextMatrix(x, 5, FCConvert.ToString(0));
					}
				}
				// x
				txtSalePrice.Text = Strings.Format(TotSale, "#,###,###,###,##0");
				if (sngSaleFactor != 0)
				{
					lblTrended.Visible = true;
				}
				else
				{
					lblTrended.Visible = false;
				}
				txtValuation.Text = Strings.Format(TotValuation, "#,###,###,###,##0");
				if ((sngLandFactor != 1) || (sngBldgFactor != 1))
				{
					lblAdjusted.Visible = true;
				}
				else
				{
					lblAdjusted.Visible = false;
				}
				// now calc the mean etc.
				if (lngNumRecs > 0)
				{
					mean = FCConvert.ToSingle(modGlobalRoutines.Round(FCConvert.ToDouble(FCConvert.ToSingle(totRatio) / (lngNumRecs)), 4));
				}
				else
				{
					mean = 0;
				}
				if (TotSale > 0)
				{
					WeightedMean = FCConvert.ToSingle(modGlobalRoutines.Round((FCConvert.ToSingle(TotValuation) / TotSale) * 100, 4));
				}
				else
				{
					WeightedMean = 0;
				}
				intSortCol = sortColumn;
				intSortOrder = FCConvert.ToInt32(sortOrder);
				sortOrder = FCGrid.SortSettings.flexSortNumericAscending;
				sortColumn = 5;
				SortEm();
				Median = FCConvert.ToSingle(GetMedian());
				MidQuartMean = FCConvert.ToSingle(GetMidQuart());
				if (WeightedMean > 0)
				{
					PriceDiff = FCConvert.ToSingle(modGlobalRoutines.Round(FCConvert.ToDouble(mean) / WeightedMean, 4));
				}
				else
				{
					PriceDiff = 0;
				}
				switch (intWhatMean)
				{
					case 1:
						{
							MeantoUse = mean;
							break;
						}
					case 2:
						{
							MeantoUse = Median;
							break;
						}
					case 3:
						{
							MeantoUse = MidQuartMean;
							break;
						}
					case 4:
						{
							MeantoUse = WeightedMean;
							break;
						}
				}
				//end switch
				if (Grid1.Rows > 1)
				{
					// 46    Grid1.Cell(FCGrid.CellPropertySettings.flexcpText, 1, 6, Grid1.Rows - 1, 6) = Round(CDbl(MeantoUse), 0)
					Grid1.Cell(FCGrid.CellPropertySettings.flexcpText, 1, 6, Grid1.Rows - 1, 6, Strings.Format(MeantoUse, "0.0000"));
				}
				totSquaredev = 0;
				TotDev = 0;
				TotDeviation = 0;
				for (x = 1; x <= Grid1.Rows - 1; x++)
				{
					if (Conversion.Val(Grid1.TextMatrix(x, 5)) > 0)
					{
						currDev = Math.Abs(Conversion.Val(Grid1.TextMatrix(x, 5)) - Conversion.Val(Grid1.TextMatrix(x, 6)));
						Grid1.TextMatrix(x, 7, Strings.Format(currDev, "0.0000"));
						TotDev += currDev;
						currDev *= currDev;
						totSquaredev += currDev;
					}
				}
				// x
				if (lngNumRecs > 1)
				{
					AveDev = FCConvert.ToSingle(modGlobalRoutines.Round(TotDev / (lngNumRecs), 4));
				}
				else if (lngNumRecs == 1)
				{
					AveDev = FCConvert.ToSingle(modGlobalRoutines.Round(TotDev, 4));
				}
				else
				{
					AveDev = 0;
				}
				TotDeviation = Math.Abs(TotValuation - TotSale);
				txtDeviation.Text = Strings.Format(TotDeviation, "#,###,###,###");
				if (lngNumRecs > 0)
				{
					StandDev = FCConvert.ToSingle(modGlobalRoutines.Round(Math.Sqrt(totSquaredev / lngNumRecs), 4));
				}
				else
				{
					StandDev = 0;
				}
				Grid2.TextMatrix(0, 1, Strings.Format(Median, "0.0000"));
				Grid2.TextMatrix(1, 1, Strings.Format(mean, "0.0000"));
				Grid2.TextMatrix(2, 1, Strings.Format(MidQuartMean, "0.0000"));
				Grid2.TextMatrix(3, 1, Strings.Format(WeightedMean, "0.0000"));
				if (MeantoUse > 0)
				{
					Grid2.TextMatrix(5, 1, Strings.Format(modGlobalRoutines.Round((AveDev / MeantoUse) * 100, 4), "0.0000"));
				}
				else
				{
					Grid2.TextMatrix(5, 1, FCConvert.ToString(0));
				}
				Grid2.TextMatrix(6, 1, Strings.Format(StandDev, "0.0000"));
				if (MeantoUse > 0)
				{
					Grid2.TextMatrix(7, 1, Strings.Format(modGlobalRoutines.Round((StandDev / MeantoUse) * 100, 4), "0.0000"));
				}
				else
				{
					Grid2.TextMatrix(7, 1, FCConvert.ToString(0));
				}
				Grid2.TextMatrix(4, 1, Strings.Format(AveDev, "0.0000"));
				Grid2.TextMatrix(8, 1, Strings.Format(PriceDiff, "0.0000"));
				sortColumn = intSortCol;
				sortOrder = (FCGrid.SortSettings)intSortOrder;
				Grid1.Refresh();
				SortEm();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In line " + Information.Erl() + " in calculate_cells", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As double	OnWrite(short, double)
		private double GetMedian()
		{
			double GetMedian = 0;
			// vbPorter upgrade warning: intTemp As short --> As int	OnWriteFCConvert.ToDouble(
			int intTemp = 0;
			try
			{
				// On Error GoTo ErrorHandler
				if (Grid1.Rows <= 1)
				{
					GetMedian = 0;
					return GetMedian;
				}
				if (modGlobalRoutines.Even(Grid1.Rows - 1))
				{
					intTemp = FCConvert.ToInt32((Grid1.Rows - 1) / 2.0);
					GetMedian = FCConvert.ToSingle(Conversion.Val(Grid1.TextMatrix(intTemp, 5)) + Conversion.Val(Grid1.TextMatrix(intTemp + 1, 5))) / 2;
				}
				else
				{
					intTemp = FCConvert.ToInt32(modGlobalRoutines.Round((Grid1.Rows - 1) / 2.0, 0));
					GetMedian = Conversion.Val(Grid1.TextMatrix(intTemp, 5));
				}
				GetMedian = modGlobalRoutines.Round(FCConvert.ToDouble(GetMedian), 4);
				return GetMedian;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Get Median", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetMedian;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As double	OnWrite(double, short)
		private double GetMidQuart()
		{
			double GetMidQuart = 0;
			try
			{
				// On Error GoTo ErrorHandler
				float Perc;
				// vbPorter upgrade warning: intNum As short --> As int	OnWriteFCConvert.ToSingle(
				int intNum;
				int intBegin = 0;
				int intEnd = 0;
				int x;
				int TotVal;
				int TotSale;
				double TotPerc;
				Perc = FCConvert.ToSingle(intMidQuartPercent) / 100;
				intNum = FCConvert.ToInt32(Perc * (Grid1.Rows - 1));
				TotPerc = 0;
				if (((intNum * 2) >= (Grid1.Rows - 1)) || (intNum < 1))
				{
					GetMidQuart = Median;
					return GetMidQuart;
				}
				else
				{
					intBegin = intNum + 1;
					intEnd = (Grid1.Rows - 1) - intNum;
					TotVal = 0;
					TotSale = 0;
					for (x = intBegin; x <= intEnd; x++)
					{
						// If (Val(Grid1.TextMatrix(x, 4) & "") > 0) And (Val(Grid1.TextMatrix(x, 3) & "")) Then
						if (Conversion.Val(Grid1.TextMatrix(x, 5)) > 0)
						{
							// TotVal = TotVal + Val(Grid1.TextMatrix(x, 4))
							// TotSale = TotSale + Val(Grid1.TextMatrix(x, 3))
							TotPerc += Conversion.Val(Grid1.TextMatrix(x, 5));
						}
					}
					// x
					// If TotSale > 0 Then
					if (intEnd - intBegin + 1 > 0)
					{
						// GetMidQuart = Round((TotVal / TotSale), 2) * 100
						GetMidQuart = modGlobalRoutines.Round(TotPerc / (intEnd - intBegin + 1), 4);
					}
					else
					{
						GetMidQuart = 0;
					}
				}
				return GetMidQuart;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Get Mid Quartile", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetMidQuart;
		}

		private void SortEm()
		{
			Grid1.Row = 0;
			Grid1.RowSel = 0;
			Grid1.Col = sortColumn;
			Grid1.ColSel = sortColumn;
			Grid1.ColSort(sortColumn, sortOrder);
			Grid1.Sort = FCGrid.SortSettings.flexSortCustom;
			//.flexSortUseColSort;
			Grid1.Refresh();
		}

		private void ResizeGrid1()
		{
			int GridWidth = 0;
			GridWidth = Grid1.WidthOriginal;
			Grid1.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.081));
			Grid1.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.19));
			Grid1.ColWidth(2, FCConvert.ToInt32(GridWidth * 0.125));
			Grid1.ColWidth(3, FCConvert.ToInt32(GridWidth * 0.16));
			Grid1.ColWidth(4, FCConvert.ToInt32(GridWidth * 0.16));
			Grid1.ColWidth(5, FCConvert.ToInt32(GridWidth * 0.081));
			Grid1.ColWidth(6, FCConvert.ToInt32(GridWidth * 0.081));
			Grid1.ColWidth(7, FCConvert.ToInt32(GridWidth * 0.081));
		}

		private void ResizeGrid2()
		{
			int GridWidth = 0;
			GridWidth = Grid2.WidthOriginal;
			Grid2.ColWidth(0, FCConvert.ToInt32(GridWidth * 0.77));
			Grid2.ColWidth(1, FCConvert.ToInt32(GridWidth * 0.19));
		}
	}
}
