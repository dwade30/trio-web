﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptoutbuilding.
	/// </summary>
	public partial class srptoutbuilding : FCSectionReport
	{
		public static srptoutbuilding InstancePtr
		{
			get
			{
				return (srptoutbuilding)Sys.GetInstance(typeof(srptoutbuilding));
			}
		}

		protected srptoutbuilding _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsTemp?.Dispose();
				clsOutB?.Dispose();
                clsOutB = null;
                clsTemp = null;
            }
			base.Dispose(disposing);
		}

		public srptoutbuilding()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptoutbuilding	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intOutCode;
		clsDRWrapper clsTemp = new clsDRWrapper();
		clsDRWrapper clsOutB = new clsDRWrapper();
		int intCount;
		int intAVal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// If intOutCode <= 999 Then
			// EOF = False
			// End If
			eArgs.EOF = clsOutB.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			intOutCode = 1;
			strSQL = " (SELECT srecordnumber,sobc1 as obc,sobv1 as obv from summrecord where sobc1  > 0) union all (select srecordnumber,sobc2 as obc,sobv2 as obv from summrecord where sobc2  > 0) union all (select srecordnumber,sobc3 as obc,sobv3 as obv from summrecord where sobc3 > 0)";
			strSQL += " union all (SELECT srecordnumber,sobc4 as obc,sobv4 as obv from summrecord where sobc4 > 0) union all (select srecordnumber,sobc5 as obc,sobv5 as obv from summrecord where sobc5 > 0) union all (select srecordnumber,sobc6 as obc,sobv6 as obv from summrecord where sobc6 > 0)";
			strSQL += " union all (SELECT srecordnumber,sobc7 as obc,sobv7 as obv from summrecord where sobc7 > 0) union all (select srecordnumber,sobc8 as obc,sobv8 as obv from summrecord where sobc8 > 0) union all (select srecordnumber,sobc9 as obc,sobv9 as obv from summrecord where sobc9 > 0)";
			strSQL += " union all (select srecordnumber,sobc10 as obc,sobv10 as obv from summrecord where sobc10 > 0)";
			// Call clsTemp.CreateStoredProcedure("OutbuildingUnion", strSQL, strREDatabase)
			if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
			{
				// strSQL = "select count(obc) as thecount,obc,sum(obv)as valsum from outbuildingunion group by obc order by obc"
				strSQL = "select count(obc) as thecount,obc,sum(cast(obv as bigint))as valsum from (" + strSQL + ") outbuildingunion group by obc order by obc";
			}
			else
			{
				// strSQL = "select count(obc) as thecount,obc,sum(obv)as valsum from outbuildingunion inner join master on (master.rsaccount = outbuildingunion.srecordnumber) where rscard = 1 and val(ritrancode & '') = " & CustomizedInfo.CurrentTownNumber & " group by obc order by obc"
				strSQL = "select count(obc) as thecount,obc,sum(cast(obv as bigint))as valsum from (" + strSQL + ") outbuildingunion inner join master on (master.rsaccount = outbuildingunion.srecordnumber) where rscard = 1 and ritrancode = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " group by obc order by obc";
			}
			clsOutB.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			clsTemp.OpenRecordset("select * from costrecord where crecordnumber between 1 and 999 order by crecordnumber", modGlobalVariables.strREDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// Dim X As Integer
			if (!clsOutB.EndOfFile())
			{
				Detail.CanGrow = true;
				Detail.Height = 360 / 1440f;
				RepeatIt:
				;
				// TODO Get_Fields: Field [obc] not found!! (maybe it is an alias?)
				intOutCode = FCConvert.ToInt32(clsOutB.Get_Fields("obc"));
				txtout.Text = intOutCode.ToString();
				// strSQL = "select count(srecordnumber) as thecount from summrecord where ((sobc1  = " & intOutCode & ") or (sobc2  = " & intOutCode & ") or (sobc3  = " & intOutCode & ") or (sobc4  = " & intOutCode & ") or (sobc5  = " & intOutCode & ")"
				// strSQL = strSQL & " or (sobc6  = " & intOutCode & ") or (sobc7  = " & intOutCode & ") or (sobc8  = " & intOutCode & ") or (sobc9   = " & intOutCode & ") or (sobc10  = " & intOutCode & "))"
				// Call clsOutB.OpenRecordset(strSQL, strredatabase)
				// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
				intCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsOutB.Get_Fields("thecount"))));
				// If intCount < 1 Then
				// intOutCode = intOutCode + 1
				// If intOutCode < 1000 Then GoTo RepeatIt
				// Detail.Height = 0
				// Detail.CanGrow = False
				// Else
				// strSQL = "select * from costrecord where crecordnumber = " & intOutCode
				// Call clsOutB.OpenRecordset(strSQL, strredatabase)
				// Call clsTemp.FindFirstRecord("crecordnumber", intOutCode)
				clsTemp.FindFirst("crecordnumber = " + FCConvert.ToString(intOutCode));
				txtout.Text = txtout.Text + " " + clsTemp.Get_Fields_String("cldesc");
				// intAVal = 0
				// For X = 1 To 10
				// strSQL = "select sum(val(sobv" & X & " )) as totval from summrecord where sobc" & X & "  = " & intOutCode & ""
				// Call clsOutB.OpenRecordset(strSQL, strredatabase)
				// intAVal = intAVal + Val(clsOutB.GetData("totval"))
				// Next X
				// TODO Get_Fields: Field [valsum] not found!! (maybe it is an alias?)
				intAVal = FCConvert.ToInt32(clsOutB.Get_Fields("valsum"));
				txtCount.Text = intCount.ToString();
				txtAssess.Text = Strings.Format(intAVal, "##,###,###,##0");
				txtaverage.Text = Strings.Format(FCConvert.ToDouble(intAVal) / intCount, "#,###,###,##0");
				// End If
				clsOutB.MoveNext();
			}
			// intOutCode = intOutCode + 1
		}

		
	}
}
