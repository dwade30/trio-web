﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cREXferSettingsController
	{
		//=========================================================
		private cSettingsController setCont = new cSettingsController();

		public cREXferSetting LoadSettings()
		{
			cREXferSetting LoadSettings = null;
			FCCollection setColl = new FCCollection();
			setColl = setCont.GetSettings("VisionTransfer", "", "", "CentralData");
			cREXferSetting custInfo = new cREXferSetting();
			bool boolSave;
			boolSave = false;
			if (setColl.Count < 2)
			{
				custInfo = GetOldSettings();
				boolSave = true;
			}
			foreach (cSetting tSet in setColl)
			{
				if (fecherFoundation.Strings.LCase(tSet.SettingName) == "numextrachars")
				{
					custInfo.intExtraChars = FCConvert.ToInt32(Math.Round(Conversion.Val(tSet.SettingValue)));
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "usescrlf")
				{
					custInfo.boolUsesCRLF = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "usesunits")
				{
					custInfo.boolUsesUnits = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "useslot")
				{
					custInfo.boolUsesLot = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "usesblock")
				{
					custInfo.boolUsesBlock = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "matchbyaccount")
				{
					custInfo.boolMatchByAccount = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "blankrecords")
				{
					custInfo.intBlankRecords = FCConvert.ToInt32(Math.Round(Conversion.Val(tSet.SettingValue)));
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "matchtrioaccount")
				{
					custInfo.boolMatchByTRIOAccount = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "ppmatchbyopen1")
				{
					custInfo.boolPPMatchByOpen1 = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "ppmatchbytrioaccount")
				{
					custInfo.boolPPMatchByTRIOaccount = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "omittrailingzeroes")
				{
					custInfo.boolOmitTrailingZeroes = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "divisionsize")
				{
					custInfo.intDivisionSize = FCConvert.ToInt32(Math.Round(Conversion.Val(tSet.SettingValue)));
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "usemultiowner")
				{
					custInfo.boolUseMultiOwner = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "owneroption")
				{
					custInfo.intOwnerOption = FCConvert.ToInt32(Math.Round(Conversion.Val(tSet.SettingValue)));
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "splitvaluation")
				{
					custInfo.boolSplitValuation = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "importpreviousowner")
				{
					custInfo.boolImportPreviousOwner = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
				else if (fecherFoundation.Strings.LCase(tSet.SettingName) == "usesystemaddress")
				{
					custInfo.UsePartyAddressFromSystem = fecherFoundation.Strings.LCase(tSet.SettingValue) == "true";
				}
			}
			if (boolSave)
			{
				SaveSettings(custInfo);
			}
			LoadSettings = custInfo;
			return LoadSettings;
		}

		public void SaveSettings(cREXferSetting custInfo)
		{
			setCont.SaveSetting(FCConvert.ToString(custInfo.intExtraChars), "NumExtraChars", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolUsesCRLF), "UsesCRLF", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolUsesUnits), "UsesUnits", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolUsesLot), "UsesLot", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolUsesBlock), "UsesBlock", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolMatchByAccount), "MatchByAccount", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.intBlankRecords), "BlankRecords", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolMatchByTRIOAccount), "MatchTrioAccount", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolPPMatchByOpen1), "PPMatchByOpen1", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolPPMatchByTRIOaccount), "PPMatchByTrioAccount", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolOmitTrailingZeroes), "OmitTrailingZeroes", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.intDivisionSize), "DivisionSize", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolUseMultiOwner), "UseMultiOwner", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.intOwnerOption), "OwnerOption", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolSplitValuation), "SplitValuation", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.boolImportPreviousOwner), "ImportPreviousOwner", "VisionTransfer", "", "", "CentralData");
			setCont.SaveSetting(FCConvert.ToString(custInfo.UsePartyAddressFromSystem), "UseSystemAddress", "VisionTransfer", "", "", "CentralData");
		}

		private cREXferSetting GetOldSettings()
		{
			cREXferSetting GetOldSettings = null;
			clsDRWrapper rsLoad = new clsDRWrapper();
			cREXferSetting custInfo = new cREXferSetting();
			custInfo.AssessType = 1;
			custInfo.boolUsesCRLF = true;
			custInfo.boolUsesLot = true;
			custInfo.boolUsesUnits = false;
			custInfo.boolUsesBlock = false;
			custInfo.intExtraChars = 0;
			custInfo.boolMatchByAccount = false;
			custInfo.boolMatchByTRIOAccount = false;
			custInfo.intBlankRecords = 0;
			custInfo.boolPPMatchByOpen1 = true;
			custInfo.boolPPMatchByTRIOaccount = false;
			custInfo.boolOmitTrailingZeroes = false;
			custInfo.intDivisionSize = 3;
			custInfo.intPPExtraCharacters = 0;
			custInfo.boolUseMultiOwner = true;
			custInfo.intOwnerOption = modMainXF.CNSTREOWNERINFOOLD;
			custInfo.boolSplitValuation = true;
			custInfo.boolImportPreviousOwner = false;
			if (rsLoad.DBExists("RealEstateTransfer"))
			{
				rsLoad.OpenRecordset("Select * from customize", "RealEstateTransfer");
				custInfo.UsePartyAddressFromSystem = false;
				if (!rsLoad.EndOfFile())
				{
					custInfo.AssessType = 1;
					// Val(rsload.Fields("Assesstype"))
					custInfo.boolUsesCRLF = FCConvert.ToBoolean(rsLoad.Get_Fields("UsesCRLF"));
					custInfo.boolUsesLot = FCConvert.ToBoolean(rsLoad.Get_Fields("UsesLot"));
					custInfo.boolUsesUnits = FCConvert.ToBoolean(rsLoad.Get_Fields("UsesUnits"));
					custInfo.boolUsesBlock = FCConvert.ToBoolean(rsLoad.Get_Fields("UsesBlock"));
					custInfo.intExtraChars = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("nUMExtraChars"))));
					custInfo.boolMatchByAccount = FCConvert.ToBoolean(rsLoad.Get_Fields("MatchByAccount"));
					custInfo.boolMatchByTRIOAccount = FCConvert.ToBoolean(rsLoad.Get_Fields("MatchTRIOAccount"));
					custInfo.intBlankRecords = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("BlankRecords"))));
					custInfo.boolPPMatchByOpen1 = FCConvert.ToBoolean(rsLoad.Get_Fields("PPMatchByOpen1"));
					custInfo.boolPPMatchByTRIOaccount = FCConvert.ToBoolean(rsLoad.Get_Fields("PPMatchByTRIOAccount"));
					custInfo.boolOmitTrailingZeroes = FCConvert.ToBoolean(rsLoad.Get_Fields("OmitTrailingZeroes"));
					custInfo.intDivisionSize = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("DivisionSize"))));
					if (custInfo.intDivisionSize < 1)
						custInfo.intDivisionSize = 3;
					custInfo.intPPExtraCharacters = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("ppextracharacters"))));
					custInfo.boolUseMultiOwner = FCConvert.ToBoolean(rsLoad.Get_Fields("usemultiowner"));
					custInfo.intOwnerOption = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("OwnerOption"))));
					custInfo.boolSplitValuation = FCConvert.ToBoolean(rsLoad.Get_Fields("splitvaluation"));
					custInfo.boolImportPreviousOwner = FCConvert.ToBoolean(rsLoad.Get_Fields("ImportPreviousOwner"));
				}
			}
			GetOldSettings = custInfo;
			return GetOldSettings;
		}
	}
}
