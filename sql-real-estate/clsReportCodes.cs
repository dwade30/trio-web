﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class clsReportCodes
	{
		//=========================================================
		public enum REReportCodeCategory
		{
			Property = 1,
			PropertyOwner = 2,
			Land = 3,
			LandType = 7,
			Dwelling = 4,
			Commercial = 5,
			Outbuilding = 6,
		}
		// vbPorter upgrade warning: CodeList As Collection	OnRead(clsReportParameter)
		private FCCollection CodeList = new FCCollection();
		private int intCurrentIndex;

		public void LoadCodes()
		{
			try
			{
				// On Error GoTo ErrorHandler
				int x;
                //FC:FINAL:AM:#3367 - reset the list to avoid duplicate items exception when loading twice
                CodeList.Clear();
				AddCode_18629(35, 2, "Owner", "Name", "master", "rsname");
				AddCode_18629(36, 2, "Owner", "Second Owner", "master", "rssecowner");
				AddCode_18629(37, 2, "Owner", "Address 1", "master", "rsaddr1");
				AddCode_18629(38, 2, "Owner", "Address 2", "master", "rsaddr2");
				AddCode_1862(39, 2, "Owner", "City, State, Zip", true);
				AddCode_1862(43, 2, "Owner", "Phone Number", true);
				AddCode_18710(75, 2, "Owner", "E-Mail", false, "master", "Email");
				AddCode_18629(47, 2, "Owner", "City", "master", "rsaddr3");
				AddCode_18629(68, 2, "Owner", "Zip code", "master", "rszip");
				AddCode_18629(73, 2, "Owner", "Previous Owner", "master", "rspreviousmaster");
				if (!modREMain.Statics.boolShortRealEstate)
				{
					AddCode_18710(1, 1, "Property", "Neighborhood", false, "master", "pineighborhood");
					AddCode_18710(2, 1, "Property", "Tree Growth (Year)", false, "master", "pistreetcode");
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1091))
					{
						if (!(Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) == ""))
						{
							AddCode_18575(3, 1, "Property", modGlobalVariables.Statics.CostRec.ClDesc, "master", "pixcoord");
						}
						else
						{
							AddCode_18710(3, 1, "Property", "X Coord", false, "master", "pixcoord");
						}
					}
					else
					{
						AddCode_18710(3, 1, "Property", "X Coord", false, "master", "pixcoord");
					}
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1092))
					{
						if (!(Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) == ""))
						{
							AddCode_18656(4, 1, "Property", modGlobalVariables.Statics.CostRec.ClDesc, false, "master", "piycoord");
						}
						else
						{
							AddCode_18710(4, 1, "Property", "Y Coord", false, "master", "piycoord");
						}
					}
					else
					{
						AddCode_18710(4, 1, "Property", "Y Coord", false, "master", "piycoord");
					}
					AddCode_18629(5, 1, "Property", "Zone", "master", "pizone");
					AddCode_18629(6, 1, "Property", "Secondary Zone", "master", "piseczone");
					AddCode_1943(7, 1, "Property", "Topography", false, true);
					AddCode_1862(8, 1, "Property", "Utilities", true);
					AddCode_18629(9, 1, "Property", "Street Surface", "master", "pistreet");
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1330))
					{
						if (!(Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) == ""))
						{
							AddCode_18575(10, 1, "Property", modGlobalVariables.Statics.CostRec.ClDesc, "master", "piopen1");
						}
						else
						{
							AddCode_18629(10, 1, "Property", "Open 1", "master", "piopen1");
						}
					}
					else
					{
						AddCode_18629(10, 1, "Property", "Open 1", "master", "piopen1");
					}
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1340))
					{
						if (!(Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) == ""))
						{
							AddCode_18575(11, 1, "Property", modGlobalVariables.Statics.CostRec.ClDesc, "master", "piopen2");
						}
						else
						{
							AddCode_18629(11, 1, "Property", "Open 2", "master", "piopen2");
						}
					}
					else
					{
						AddCode_18629(11, 1, "Property", "Open 2", "master", "piopen2");
					}
					AddCode_18629(12, 1, "Property", "Sale Type", "master", "pisaletype");
					AddCode_18710(13, 1, "Property", "Sale Price", true, "master", "pisaleprice");
					AddCode_18629(14, 1, "Property", "Financing", "master", "pisalefinancing");
					AddCode_2105(15, 1, "Property", "Sale Date", "This is the date that the property changed ownership", true);
					AddCode_18629(16, 1, "Property", "Verified", "master", "pisaleverified");
					AddCode_18629(17, 1, "Property", "Validity", "master", "pisalevalidity");
				}
				else
				{
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1330))
					{
						if (!(Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) == ""))
						{
							AddCode_18575(10, 1, "Property", modGlobalVariables.Statics.CostRec.ClDesc, "master", "piopen1");
						}
						else
						{
							AddCode_18629(10, 1, "Property", "Open 1", "master", "piopen1");
						}
					}
					else
					{
						AddCode_18629(10, 1, "Property", "Open 1", "master", "piopen1");
					}
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1340))
					{
						if (!(Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) == ""))
						{
							AddCode_18575(11, 1, "Property", modGlobalVariables.Statics.CostRec.ClDesc, "master", "piopen2");
						}
						else
						{
							AddCode_18629(11, 1, "Property", "Open 2", "master", "piopen2");
						}
					}
					else
					{
						AddCode_18629(11, 1, "Property", "Open 2", "master", "piopen2");
					}
				}
				AddCode_1862(18, 1, "Property", "Exempt Code", true);
				AddCode_18629(19, 1, "Property", "Tran Code", "master", "ritrancode");
				AddCode_18629(20, 1, "Property", "Land Code", "master", "rilandcode");
				AddCode_18629(21, 1, "Property", "Building Code", "master", "ribldgcode");
				if (!modREMain.Statics.boolShortRealEstate)
				{
					AddCode_18872(22, 1, "Property", "Land Override Code", "The land override code found on the calculation screen", "master", "hivallandcode");
					AddCode_18872(23, 1, "Property", "Building Override Code", "The building override code found on the calculation screen", "master", "hivalbldgcode");
					AddCode_1862(24, 1, "Property", "Land Schedule", true);
					AddCode_18710(25, 1, "Property", "Land Value (Current)", true, "master", "rllandval");
					AddCode_18710(26, 1, "Property", "Building Value (Current)", true, "master", "rlbldgval");
					AddCode_1943(27, 1, "Property", "Total Value (Current)", true, true);
				}
				AddCode_18710(28, 1, "Property", "Land Value (Billing)", true, "master", "lastlandval");
				AddCode_18710(29, 1, "Property", "Building Value (Billing)", true, "master", "lastbldgval");
				AddCode_1943(30, 1, "Property", "Total Value (Billing)", true, true);
				AddCode_18710(31, 1, "Property", "Exemption Amount", true, "master", "rlexemption");
				AddCode_18629(32, 1, "Property", "Account Number", "master", "rsaccount");
				AddCode_18629(33, 1, "Property", "Card Number", "master", "rscard");
				AddCode_18629(34, 1, "Property", "Map/Lot", "master", "rsmaplot");
				AddCode_1862(40, 1, "Property", "Location", true);
				AddCode_18629(41, 1, "Property", "Reference 1", "master", "rsref1");
				AddCode_18629(42, 1, "Property", "Reference 2", "master", "rsref2");
				// Call AddCode(44, 1, "Property", "Net Value (Calculated)", True, , True)
				AddCode_1943(45, 1, "Property", "Net Billing Value", true, true);
				if (!modREMain.Statics.boolShortRealEstate)
				{
					AddCode_1943(46, 1, "Property", "Net Value (Current)", true, true);
				}
				AddCode_18629(48, 1, "Property", "Entrance Code", "master", "EntranceCode");
				AddCode_18629(49, 1, "Property", "Information Code", "master", "informationcode");
				AddCode_1862(50, 1, "Property", "Date Inspected", true);
				AddCode_18953(51, 1, "Property", "Homestead Value", true, "Amount of Homestead Exemption", "master", "homesteadvalue");
				AddCode_19358(52, 1, "Property", "Street Number", true, "master", "rslocnumalph");
				AddCode_18710(53, 1, "Property", "Softwood Acreage", true, "master", "rssoft");
				AddCode_18710(54, 1, "Property", "Softwood Value", true, "master", "rssoftvalue");
				AddCode_18710(55, 1, "Property", "Mixed Wood Acreage", true, "master", "rsmixed");
				AddCode_18710(56, 1, "Property", "Mixed Wood Value", true, "master", "rsmixedvalue");
				AddCode_18710(57, 1, "Property", "Hardwood Acreage", true, "master", "rshard");
				AddCode_18710(58, 1, "Property", "Hardwood Value", true, "master", "rshardvalue");
				AddCode_18710(59, 1, "Property", "Other Acreage", true, "master", "rsother");
				AddCode_18710(60, 1, "Property", "Other Value", true, "master", "rsothervalue");
				AddCode_1862(61, 1, "Property", "Book & Page", true);
				AddCode_1862(62, 1, "Property", "Reference 1 (pos 1-23)", true);
				AddCode_1862(63, 1, "Property", "Reference 1 (pos 24-35)", true);
				AddCode_2105(64, 1, "Property", "Assessment Change", "Difference of billing and current values", true);
				AddCode_2105(65, 1, "Property", "Tax Amount", "Tax rate * (land + bldg - exempt)", true);
				AddCode_2105(66, 1, "Property", "Billing Change", "Difference of this years billing and last years billing values", true);
				AddCode_1862(67, 1, "Property", "Date Updated", true);
				AddCode_18629(69, 1, "Property", "Tax Acquired", "master", "taxacquired");
				if (!modREMain.Statics.boolShortRealEstate)
				{
					AddCode_1943(70, 1, "Property", "No. of Pictures (By Account)", true, true);
					AddCode_1943(71, 1, "Property", "No. of Sketches (By Account)", true, true);
				}
				AddCode_18629(72, 1, "Property", "Street Name", "master", "rslocstreet");
				AddCode_1862(74, 1, "Property", "Date Created", true);
				AddCode_18629(76, 1, "Property", "Property Code", "master", "PropertyCode");
				if (!modREMain.Statics.boolShortRealEstate)
				{
					AddCode_1862(98, 3, "Land", "Influence Factor", true);
					AddCode_1862(99, 3, "Land", "Influence Code", true);
					AddCode_1862(100, 3, "Land", "Land Type", true);
					AddCode_1862(101, 3, "Land", "Frontage", true);
					AddCode_1862(102, 3, "Land", "Depth", true);
					AddCode_1862(103, 3, "Land", "Square Feet", true);
					AddCode_1862(104, 3, "Land", "Acreage", true);
					AddCode_1862(105, 3, "Land", "Sites", true);
					AddCode_18710(147, 3, "Land", "Total Acreage", true, "master", "piacres");
					modGlobalVariables.Statics.LandTypes.Init();
					while (!modGlobalVariables.Statics.LandTypes.EndOfLandTypes())
					{
						AddCode_49976(500, 7, "Land Type", modGlobalVariables.Statics.LandTypes.Description, true, modGlobalVariables.Statics.LandTypes.Code);
						modGlobalVariables.Statics.LandTypes.MoveNext();
					}
				}
				if (!modREMain.Statics.boolShortRealEstate)
				{
					AddCode_18629(200, 4, "Dwelling", "Building Style", "dwelling", "distyle");
					AddCode_1862(201, 4, "Dwelling", "Dwelling Units (By Account)", true);
					AddCode_18629(202, 4, "Dwelling", "Dwelling Units (By Card)", "dwelling", "diunitsdwelling");
					AddCode_1862(203, 4, "Dwelling", "Other Units (By Account)", true);
					AddCode_18629(204, 4, "Dwelling", "Other Units (By Account)", "dwelling", "diunitsother");
					AddCode_18629(205, 4, "Dwelling", "Stories", "dwelling", "distories");
					AddCode_18629(206, 4, "Dwelling", "Exterior Walls", "dwelling", "diextwalls");
					AddCode_18629(207, 4, "Dwelling", "Roof Surface", "dwelling", "diroof");
					AddCode_1862(208, 4, "Dwelling", "S/F Masonry Trim (By Account)", true);
					AddCode_1862(209, 4, "Dwelling", "S/F Masonry Trim (By Card)", true);
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1520))
					{
						if (Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) != "")
						{
							AddCode_18575(210, 4, "Dwelling", modGlobalVariables.Statics.CostRec.ClDesc, "dwelling", "Diopen3");
						}
						else
						{
							AddCode_18629(210, 4, "Dwelling", "Open 3", "dwelling", "Diopen3");
						}
					}
					else
					{
						AddCode_18629(210, 4, "Dwelling", "Open 3", "dwelling", "Diopen3");
					}
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1530))
					{
						if (Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) != "")
						{
							AddCode_18575(211, 4, "Dwelling", modGlobalVariables.Statics.CostRec.ClDesc, "dwelling", "diopen4");
						}
						else
						{
							AddCode_18629(211, 4, "Dwelling", "Open 4", "dwelling", "diopen4");
						}
					}
					else
					{
						AddCode_18629(211, 4, "Dwelling", "Open 4", "dwelling", "diopen4");
					}
					AddCode_18629(212, 4, "Dwelling", "Year Built", "dwelling", "diyearbuilt");
					AddCode_18629(213, 4, "Dwelling", "Year Remodeled", "dwelling", "diyearremodel");
					AddCode_18629(214, 4, "Dwelling", "Foundation", "dwelling", "difoundation");
					AddCode_18629(215, 4, "Dwelling", "Basement", "dwelling", "dibsmt");
					AddCode_18629(216, 4, "Dwelling", "Basement Garage, # of Cars", "Dwelling", "dibsmtgar");
					AddCode_18629(217, 4, "Dwelling", "Wet Basement", "dwelling", "diwetbsmt");
					AddCode_1862(218, 4, "Dwelling", "S/F Basment Living Area (By Account)", true);
					AddCode_18629(219, 4, "Dwelling", "S/F Basement Living Area (By Card)", "dwelling", "disfbsmtliving");
					AddCode_1862(220, 4, "Dwelling", "Finished Basement Grade & Factor", true);
					if (modGlobalVariables.Statics.clsCostRecords.Get_Cost_Record(1570))
					{
						if (Strings.Trim(modGlobalVariables.Statics.CostRec.ClDesc) != "")
						{
							AddCode_18575(221, 4, "Dwelling", modGlobalVariables.Statics.CostRec.ClDesc, "dwelling", "diopen5");
						}
						else
						{
							AddCode_18629(221, 4, "Dwelling", "Open 5", "dwelling", "diopen5");
						}
					}
					else
					{
						AddCode_18629(221, 4, "Dwelling", "Open 5", "dwelling", "diopen5");
					}
					AddCode_18629(222, 4, "Dwelling", "Heat Type", "dwelling", "diheat");
					AddCode_18629(223, 4, "Dwelling", "Heat Percent", "dwelling", "dipctheat");
					AddCode_18629(224, 4, "Dwelling", "Cool Type", "dwelling", "dicool");
					AddCode_18629(225, 4, "Dwelling", "Cool Percent", "dwelling", "dipctcool");
					AddCode_18629(226, 4, "Dwelling", "Kitchen Style", "dwelling", "dikitchens");
					AddCode_18629(227, 4, "Dwelling", "Bath Style", "dwelling", "dibaths");
					AddCode_18629(228, 4, "Dwelling", "Number of Rooms", "dwelling", "dirooms");
					AddCode_18629(229, 4, "Dwelling", "Number of Bedrooms", "dwelling", "dibedrooms");
					AddCode_18629(230, 4, "Dwelling", "Number of Full Baths", "dwelling", "difullbaths");
					AddCode_18629(231, 4, "Dwelling", "Number of Half Baths", "dwelling", "dihalfbaths");
					AddCode_18629(232, 4, "Dwelling", "Number of Additional Fixtures", "dwelling", "diaddnfixtures");
					AddCode_18629(233, 4, "Dwelling", "Number of Fireplaces", "dwelling", "difireplaces");
					AddCode_18629(234, 4, "Dwelling", "Layout", "dwelling", "dilayout");
					AddCode_18629(235, 4, "Dwelling", "Attic", "dwelling", "diattic");
					AddCode_18629(236, 4, "Dwelling", "Insulation", "dwelling", "diinsulation");
					AddCode_18629(237, 4, "Dwelling", "Unfinished Percent", "dwelling", "dipctunfinished");
					// Call AddCode(238, 4, "Dwelling", "Grade & Factor", , , True)
					AddCode_19358(248, 4, "Dwelling", "Grade", false, "Dwelling", "DiGrade1");
					AddCode_19358(249, 4, "Dwelling", "Factor", false, "Dwelling", "DIGrade2");
					AddCode_1862(239, 4, "Dwelling", "Square Footage (By Account)", true);
					AddCode_18629(240, 4, "Dwelling", "Square Footage (By Card)", "dwelling", "disqft");
					AddCode_18629(241, 4, "Dwelling", "Condition", "dwelling", "dicondition");
					AddCode_18629(242, 4, "Dwelling", "Physical Percent Good (As Entered)", "dwelling", "dipctphys");
					AddCode_18629(243, 4, "Dwelling", "Functional Percent Good (As Entered)", "dwelling", "dipctfunct");
					AddCode_18629(244, 4, "Dwelling", "Functional Code", "dwelling", "difunctcode");
					AddCode_18629(245, 4, "Dwelling", "Economic Percent Good (As Entered)", "dwelling", "dipctecon");
					AddCode_18629(246, 4, "Dwelling", "Economic Code", "dwelling", "dieconcode");
					AddCode_18710(247, 4, "Dwelling", "Sqft Living Area", true, "dwelling", "disfla");
					AddCode_19439(250, 4, "Dwelling", "Dwelling Value", true, false, "dwelling", "DwellValue");
					AddCode_1862(301, 5, "Commercial", "Occupancy Code", true);
					AddCode_1862(302, 5, "Commercial", "Dwelling Units (By Account)", true);
					AddCode_1862(303, 5, "Commercial", "Dwelling Units (By Card)", true);
					AddCode_1862(304, 5, "Commercial", "Building Class", true);
					AddCode_1862(305, 5, "Commercial", "Building Quality", true);
					AddCode_1862(306, 5, "Commercial", "Grade Factor", true);
					AddCode_1862(307, 5, "Commercial", "Exterior Walls", true);
					AddCode_1862(308, 5, "Commercial", "Stories", true);
					AddCode_1862(309, 5, "Commercial", "Height", true);
					AddCode_1862(310, 5, "Commercial", "Ground Floor Area (By Account)", true);
					AddCode_1862(311, 5, "Commercial", "Ground Floor Area (By Card)", true);
					AddCode_1862(312, 5, "Commercial", "Perimeter / Units", true);
					AddCode_1862(313, 5, "Commercial", "Heating / Cooling", true);
					AddCode_1862(314, 5, "Commercial", "Year Built", true);
					AddCode_1862(315, 5, "Commercial", "Year Remodeled", true);
					AddCode_1862(316, 5, "Commercial", "Condition", true);
					AddCode_1862(317, 5, "Commercial", "Physical Percent good (As Entered", true);
					AddCode_1862(318, 5, "Commercial", "Functional Percent Good (As Entered)", true);
					AddCode_18629(319, 5, "Commercial", "Economic Percent Good (As Entered)", "commercial", "cmecon");
					AddCode_1862(320, 5, "Commercial", "Square Footage (By Account)", true);
					AddCode_1862(321, 5, "Commercial", "Square Footage (By Card)", true);
					AddCode_1943(322, 5, "Commercial", "Total SQFT (By Account)", true, true);
					AddCode_18710(323, 5, "Commercial", "Total SQFT (By Card)", true, "commercial", "sqft");
					AddCode_2105(400, 6, "Outbuildings", "Outbuildings", "Outbuildings as entered", true);
					AddCode_1862(401, 6, "Outbuildings", "Outbuilding Type", true);
					AddCode_1862(402, 6, "Outbuildings", "Year Built", true);
					AddCode_1862(403, 6, "Outbuildings", "Square Footage", true);
					AddCode_1862(404, 6, "Outbuildings", "Grade & Factor", true);
					AddCode_1862(405, 6, "Outbuildings", "Condition", true);
					AddCode_1862(406, 6, "Outbuildings", "Physical Percent", true);
					AddCode_1862(407, 6, "Outbuildings", "Functional Percent", true);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadCodes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AddCode_1862(int lngCode, short intCategory, string strCategory, string strDescription, bool boolSpecialCase = false, string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, false, "", boolSpecialCase, strTableName, strFieldName, lngID);
		}

		private void AddCode_1943(int lngCode, short intCategory, string strCategory, string strDescription, bool boolSummable = false, bool boolSpecialCase = false, string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, boolSummable, "", boolSpecialCase, strTableName, strFieldName, lngID);
		}

		private void AddCode_2105(int lngCode, short intCategory, string strCategory, string strDescription, string strHelp = "", bool boolSpecialCase = false, string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, false, strHelp, boolSpecialCase, strTableName, strFieldName, lngID);
		}

		private void AddCode_18575(int lngCode, short intCategory, string strCategory, string strDescription, string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, false, "", false, strTableName, strFieldName, lngID);
		}

		private void AddCode_18629(int lngCode, short intCategory, string strCategory, string strDescription, string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, false, "", false, strTableName, strFieldName, lngID);
		}

		private void AddCode_18656(int lngCode, short intCategory, string strCategory, string strDescription, bool boolSummable = false, string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, boolSummable, "", false, strTableName, strFieldName, lngID);
		}

		private void AddCode_18710(int lngCode, short intCategory, string strCategory, string strDescription, bool boolSummable = false, string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, boolSummable, "", false, strTableName, strFieldName, lngID);
		}

		private void AddCode_18872(int lngCode, short intCategory, string strCategory, string strDescription, string strHelp = "", string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, false, strHelp, false, strTableName, strFieldName, lngID);
		}

		private void AddCode_18953(int lngCode, short intCategory, string strCategory, string strDescription, bool boolSummable = false, string strHelp = "", string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, boolSummable, strHelp, false, strTableName, strFieldName, lngID);
		}

		private void AddCode_19358(int lngCode, short intCategory, string strCategory, string strDescription, bool boolSpecialCase = false, string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, false, "", boolSpecialCase, strTableName, strFieldName, lngID);
		}

		private void AddCode_19439(int lngCode, short intCategory, string strCategory, string strDescription, bool boolSummable = false, bool boolSpecialCase = false, string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, boolSummable, "", boolSpecialCase, strTableName, strFieldName, lngID);
		}

		private void AddCode_49976(int lngCode, short intCategory, string strCategory, string strDescription, bool boolSpecialCase = false, int lngID = 0)
		{
			AddCode(ref lngCode, ref intCategory, ref strCategory, ref strDescription, false, "", boolSpecialCase, "", "", lngID);
		}

		private void AddCode(ref int lngCode, ref short intCategory, ref string strCategory, ref string strDescription, bool boolSummable = false, string strHelp = "", bool boolSpecialCase = false, string strTableName = "", string strFieldName = "", int lngID = 0)
		{
			clsReportParameter tP = new clsReportParameter();
			tP.CategoryName = strCategory;
			tP.CategoryNo = intCategory;
			tP.Code = lngCode;
			tP.Description = strDescription;
			tP.FieldName = strFieldName;
			tP.Help = strHelp;
			tP.SpecialCase = boolSpecialCase;
			tP.Summable = boolSummable;
			tP.TableName = strTableName;
			tP.ID = lngID;
			InsertCode(ref tP);
		}

		private bool InsertCode(ref clsReportParameter tP)
		{
			bool InsertCode = false;
			try
			{
				// On Error GoTo ErrorHandler
				InsertCode = false;
				CodeList.Add(tP, "Code" + tP.Code + "ID" + tP.ID);
				InsertCode = true;
				return InsertCode;
			}
			catch
			{
				// ErrorHandler:
			}
			return InsertCode;
		}

		public clsReportCodes() : base()
		{
			intCurrentIndex = -1;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short MoveFirst()
		{
			short MoveFirst = 0;
			int intReturn;
			intCurrentIndex = 0;
			intReturn = MoveNext();
			MoveFirst = FCConvert.ToInt16(intReturn);
			return MoveFirst;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short MoveNext()
		{
			short MoveNext = 0;
			int intReturn;
			intReturn = -1;
			if (!fecherFoundation.FCUtils.IsEmpty(CodeList))
			{
				if (CodeList.Count >= intCurrentIndex + 1)
				{
					while (intCurrentIndex <= CodeList.Count)
					{
						intCurrentIndex += 1;
						if (!(CodeList[intCurrentIndex] == null))
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
					if (intCurrentIndex > CodeList.Count)
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			MoveNext = FCConvert.ToInt16(intReturn);
			return MoveNext;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short MovePrevious()
		{
			short MovePrevious = 0;
			int intReturn;
			intReturn = -1;
			if (!fecherFoundation.FCUtils.IsEmpty(CodeList))
			{
				if (CodeList.Count > 0)
				{
					while (intCurrentIndex > 0)
					{
						intCurrentIndex -= 1;
						if (intCurrentIndex > 0)
						{
							if (!(CodeList[intCurrentIndex] == null))
							{
								intReturn = intCurrentIndex;
							}
						}
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			MovePrevious = FCConvert.ToInt16(intReturn);
			return MovePrevious;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short GetCurrentIndex()
		{
			short GetCurrentIndex = 0;
			if (!fecherFoundation.FCUtils.IsEmpty(CodeList))
			{
				if (intCurrentIndex > CodeList.Count)
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
			GetCurrentIndex = FCConvert.ToInt16(intCurrentIndex);
			return GetCurrentIndex;
		}

		public clsReportParameter GetCurrentCode()
		{
			clsReportParameter GetCurrentCode = null;
			// vbPorter upgrade warning: tcp As clsReportParameter	OnWrite(Collection)
			clsReportParameter tcp;
			tcp = null;
			if (!fecherFoundation.FCUtils.IsEmpty(CodeList))
			{
				if (intCurrentIndex > 0 && intCurrentIndex <= CodeList.Count)
				{
					tcp = CodeList[intCurrentIndex];
				}
			}
			GetCurrentCode = tcp;
			return GetCurrentCode;
		}

		public clsReportParameter GetCodeByIndex(ref short intIndex)
		{
			clsReportParameter GetCodeByIndex = null;
			// vbPorter upgrade warning: tcp As clsReportParameter	OnWrite(Collection)
			clsReportParameter tcp;
			tcp = null;
			if (!fecherFoundation.FCUtils.IsEmpty(CodeList))
			{
				if (CodeList.Count >= intIndex)
				{
					tcp = CodeList[intIndex];
				}
			}
			GetCodeByIndex = tcp;
			return GetCodeByIndex;
		}
		// vbPorter upgrade warning: lngCode As int	OnWrite(int, double)
		// vbPorter upgrade warning: lngID As int	OnWrite(int, double)
		public clsReportParameter GetCodeByCode(int lngCode, int lngID = 0)
		{
			clsReportParameter GetCodeByCode = null;
			clsReportParameter tcp;
			try
			{
				// On Error GoTo ErrorHandler
				tcp = null;
				if (!fecherFoundation.FCUtils.IsEmpty(CodeList))
				{
					if (CodeList.Count > 0)
					{
						tcp = (clsReportParameter)CodeList["Code" + lngCode + "ID" + lngID];
					}
				}
				GetCodeByCode = tcp;
				return GetCodeByCode;
			}
			catch
			{
				// ErrorHandler:
				tcp = null;
				GetCodeByCode = tcp;
			}
			return GetCodeByCode;
		}
	}
}
