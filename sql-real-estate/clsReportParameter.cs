﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace TWRE0000
{
	public class clsReportParameter
	{
		//=========================================================
		private bool boolSummable;
		private bool boolSpecialCase;
		private string strTableName = string.Empty;
		private string strCategory = string.Empty;
		private int intCategory;
		private string strFieldName = string.Empty;
		private int lngCode;
		private string strDescription = string.Empty;
		private string strHelp = string.Empty;
		private int lngID;

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public bool Summable
		{
			set
			{
				boolSummable = value;
			}
			get
			{
				bool Summable = false;
				Summable = boolSummable;
				return Summable;
			}
		}

		public bool SpecialCase
		{
			set
			{
				boolSpecialCase = value;
			}
			get
			{
				bool SpecialCase = false;
				SpecialCase = boolSpecialCase;
				return SpecialCase;
			}
		}

		public string TableName
		{
			set
			{
				strTableName = value;
			}
			get
			{
				string TableName = "";
				TableName = strTableName;
				return TableName;
			}
		}

		public string CategoryName
		{
			set
			{
				strCategory = value;
			}
			get
			{
				string CategoryName = "";
				CategoryName = strCategory;
				return CategoryName;
			}
		}

		public short CategoryNo
		{
			set
			{
				intCategory = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short CategoryNo = 0;
				CategoryNo = FCConvert.ToInt16(intCategory);
				return CategoryNo;
			}
		}

		public string FieldName
		{
			set
			{
				strFieldName = value;
			}
			get
			{
				string FieldName = "";
				FieldName = strFieldName;
				return FieldName;
			}
		}

		public int Code
		{
			set
			{
				lngCode = value;
			}
			get
			{
				int Code = 0;
				Code = lngCode;
				return Code;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string Help
		{
			set
			{
				strHelp = Help;
			}
			get
			{
				string Help = "";
				Help = strHelp;
				return Help;
			}
		}
	}
}
