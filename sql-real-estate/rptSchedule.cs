﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptSchedule.
	/// </summary>
	public partial class rptSchedule : BaseSectionReport
	{
		public static rptSchedule InstancePtr
		{
			get
			{
				return (rptSchedule)Sys.GetInstance(typeof(rptSchedule));
			}
		}

		protected rptSchedule _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptSchedule()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Outbuilding SQFT Factors";
		}
		// nObj = 1
		//   0	rptSchedule	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngCRow;
		
		int lngCRec;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngCRow >= frmCostSchedule.InstancePtr.Grid.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int x;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			lngCRow = 1;
			// lngNumScheds = 99
			// If boolPrintAll Then
			// lngCurSched = 1
			// GroupHeader1.Visible = True
			// GroupHeader1.GroupValue = 1
			// Call clsLoad.OpenRecordset("select * from landtype order by code", strREDatabase)
			// x = 1
			// Do While Not clsLoad.EndOfFile
			// arDesc(x) = clsLoad.Fields("description")
			// arSDesc(x) = clsLoad.Fields("shortdescription")
			// clsLoad.MoveNext
			// x = x + 1
			// Loop
			// Call clsLoad.OpenRecordset("select * from costrecord where crecordnumber between 1427 and 1428 order by crecordnumber", strREDatabase)
			// For x = 7 To 38
			// arDesc(x) = clsLoad.Fields("cldesc")
			// arSDesc(x) = clsLoad.Fields("csdesc")
			// clsLoad.MoveNext
			// Next x
			// Call clsLoad.OpenRecordset("select * from landsrecord where lrecordnumber = 1", strREDatabase)
			// strTitle = "Schedule 1"
			// End If
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngCRow < frmCostSchedule.InstancePtr.Grid.Rows)
			{
				txtCode.Text = frmCostSchedule.InstancePtr.Grid.TextMatrix(lngCRow, 0);
				txtType.Text = frmCostSchedule.InstancePtr.Grid.TextMatrix(lngCRow, 1);
				txtDescription.Text = frmCostSchedule.InstancePtr.Grid.TextMatrix(lngCRow, 2);
				txtAmount.Text = frmCostSchedule.InstancePtr.Grid.TextMatrix(lngCRow, 3);
				lngCRow += 1;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
