﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptIncomeSummary.
	/// </summary>
	partial class srptIncomeSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptIncomeSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblVacancy = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPGI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVacancy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEffectiveGrossIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLessExpenses = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEffectiveNetIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverallRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEffectiveTaxRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCapitalizationRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIndicatedMarketValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVacancy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPGI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacancy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveGrossIncome)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLessExpenses)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveNetIncome)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverallRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveTaxRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCapitalizationRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIndicatedMarketValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.lblVacancy,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.txtPGI,
				this.txtVacancy,
				this.txtEffectiveGrossIncome,
				this.txtLessExpenses,
				this.txtEffectiveNetIncome,
				this.txtOverallRate,
				this.txtEffectiveTaxRate,
				this.txtCapitalizationRate,
				this.txtIndicatedMarketValue,
				this.Line1,
				this.Line2,
				this.Line3
			});
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1
			});
			this.ReportHeader.Height = 0.46875F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.05208333F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Field1.Text = "SUMMARY";
			this.Field1.Top = 0F;
			this.Field1.Width = 2.864583F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.01041667F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "";
			this.Label1.Text = "Potential Gross Income";
			this.Label1.Top = 0F;
			this.Label1.Width = 1.770833F;
			// 
			// lblVacancy
			// 
			this.lblVacancy.Height = 0.1875F;
			this.lblVacancy.HyperLink = null;
			this.lblVacancy.Left = 0.01041667F;
			this.lblVacancy.Name = "lblVacancy";
			this.lblVacancy.Style = "";
			this.lblVacancy.Text = "Less Vacancy of";
			this.lblVacancy.Top = 0.1770833F;
			this.lblVacancy.Width = 1.770833F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.01041667F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "";
			this.Label3.Text = "Effective Gross Income";
			this.Label3.Top = 0.4479167F;
			this.Label3.Width = 1.770833F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.01041667F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "";
			this.Label4.Text = "Less Expenses";
			this.Label4.Top = 0.6354167F;
			this.Label4.Width = 1.770833F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.01041667F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "";
			this.Label5.Text = "Effective Net Income";
			this.Label5.Top = 0.90625F;
			this.Label5.Width = 1.770833F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.01041667F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "";
			this.Label6.Text = "Overall Rate";
			this.Label6.Top = 1.104167F;
			this.Label6.Width = 1.770833F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0.01041667F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "";
			this.Label7.Text = "Effective Tax Rate";
			this.Label7.Top = 1.291667F;
			this.Label7.Width = 1.770833F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.01041667F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "";
			this.Label8.Text = "Capitalization Rate";
			this.Label8.Top = 1.479167F;
			this.Label8.Width = 1.770833F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.01041667F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "";
			this.Label9.Text = "Indicated Market Value";
			this.Label9.Top = 1.75F;
			this.Label9.Width = 1.770833F;
			// 
			// txtPGI
			// 
			this.txtPGI.Height = 0.1875F;
			this.txtPGI.Left = 1.760417F;
			this.txtPGI.Name = "txtPGI";
			this.txtPGI.Style = "text-align: right";
			this.txtPGI.Text = "Field2";
			this.txtPGI.Top = 0F;
			this.txtPGI.Width = 1.208333F;
			// 
			// txtVacancy
			// 
			this.txtVacancy.Height = 0.1875F;
			this.txtVacancy.Left = 1.760417F;
			this.txtVacancy.Name = "txtVacancy";
			this.txtVacancy.Style = "text-align: right";
			this.txtVacancy.Text = "Field2";
			this.txtVacancy.Top = 0.1666667F;
			this.txtVacancy.Width = 1.208333F;
			// 
			// txtEffectiveGrossIncome
			// 
			this.txtEffectiveGrossIncome.Height = 0.1875F;
			this.txtEffectiveGrossIncome.Left = 1.760417F;
			this.txtEffectiveGrossIncome.Name = "txtEffectiveGrossIncome";
			this.txtEffectiveGrossIncome.Style = "text-align: right";
			this.txtEffectiveGrossIncome.Text = "Field2";
			this.txtEffectiveGrossIncome.Top = 0.4479167F;
			this.txtEffectiveGrossIncome.Width = 1.208333F;
			// 
			// txtLessExpenses
			// 
			this.txtLessExpenses.Height = 0.1875F;
			this.txtLessExpenses.Left = 1.760417F;
			this.txtLessExpenses.Name = "txtLessExpenses";
			this.txtLessExpenses.Style = "text-align: right";
			this.txtLessExpenses.Text = "Field2";
			this.txtLessExpenses.Top = 0.6354167F;
			this.txtLessExpenses.Width = 1.208333F;
			// 
			// txtEffectiveNetIncome
			// 
			this.txtEffectiveNetIncome.Height = 0.1875F;
			this.txtEffectiveNetIncome.Left = 1.760417F;
			this.txtEffectiveNetIncome.Name = "txtEffectiveNetIncome";
			this.txtEffectiveNetIncome.Style = "text-align: right";
			this.txtEffectiveNetIncome.Text = "Field2";
			this.txtEffectiveNetIncome.Top = 0.90625F;
			this.txtEffectiveNetIncome.Width = 1.208333F;
			// 
			// txtOverallRate
			// 
			this.txtOverallRate.Height = 0.1875F;
			this.txtOverallRate.Left = 1.760417F;
			this.txtOverallRate.Name = "txtOverallRate";
			this.txtOverallRate.Style = "text-align: right";
			this.txtOverallRate.Text = "Field2";
			this.txtOverallRate.Top = 1.104167F;
			this.txtOverallRate.Width = 1.208333F;
			// 
			// txtEffectiveTaxRate
			// 
			this.txtEffectiveTaxRate.Height = 0.1875F;
			this.txtEffectiveTaxRate.Left = 1.760417F;
			this.txtEffectiveTaxRate.Name = "txtEffectiveTaxRate";
			this.txtEffectiveTaxRate.Style = "text-align: right";
			this.txtEffectiveTaxRate.Text = "Field2";
			this.txtEffectiveTaxRate.Top = 1.291667F;
			this.txtEffectiveTaxRate.Width = 1.208333F;
			// 
			// txtCapitalizationRate
			// 
			this.txtCapitalizationRate.Height = 0.1875F;
			this.txtCapitalizationRate.Left = 1.760417F;
			this.txtCapitalizationRate.Name = "txtCapitalizationRate";
			this.txtCapitalizationRate.Style = "text-align: right";
			this.txtCapitalizationRate.Text = "Field2";
			this.txtCapitalizationRate.Top = 1.479167F;
			this.txtCapitalizationRate.Width = 1.208333F;
			// 
			// txtIndicatedMarketValue
			// 
			this.txtIndicatedMarketValue.Height = 0.1875F;
			this.txtIndicatedMarketValue.Left = 1.760417F;
			this.txtIndicatedMarketValue.Name = "txtIndicatedMarketValue";
			this.txtIndicatedMarketValue.Style = "text-align: right";
			this.txtIndicatedMarketValue.Text = "Field2";
			this.txtIndicatedMarketValue.Top = 1.75F;
			this.txtIndicatedMarketValue.Width = 1.208333F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.02083333F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.375F;
			this.Line1.Width = 2.947917F;
			this.Line1.X1 = 0.02083333F;
			this.Line1.X2 = 2.96875F;
			this.Line1.Y1 = 0.375F;
			this.Line1.Y2 = 0.375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.84375F;
			this.Line2.Width = 2.947917F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 2.947917F;
			this.Line2.Y1 = 0.84375F;
			this.Line2.Y2 = 0.84375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.666667F;
			this.Line3.Width = 2.947917F;
			this.Line3.X1 = 0F;
			this.Line3.X2 = 2.947917F;
			this.Line3.Y1 = 1.666667F;
			this.Line3.Y2 = 1.666667F;
			// 
			// srptIncomeSummary
			//
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 2.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVacancy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPGI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVacancy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveGrossIncome)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLessExpenses)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveNetIncome)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverallRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveTaxRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCapitalizationRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIndicatedMarketValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVacancy;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPGI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVacancy;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEffectiveGrossIncome;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLessExpenses;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEffectiveNetIncome;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverallRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEffectiveTaxRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCapitalizationRate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIndicatedMarketValue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
