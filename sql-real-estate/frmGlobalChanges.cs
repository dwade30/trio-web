﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGlobalChanges.
	/// </summary>
	public partial class frmGlobalChanges : BaseForm
	{
		public frmGlobalChanges()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGlobalChanges InstancePtr
		{
			get
			{
				return (frmGlobalChanges)Sys.GetInstance(typeof(frmGlobalChanges));
			}
		}

		protected frmGlobalChanges _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmGlobalChanges_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGlobalChanges properties;
			//frmGlobalChanges.ScaleWidth	= 5880;
			//frmGlobalChanges.ScaleHeight	= 4620;
			//frmGlobalChanges.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			GridSetup();
			SetupGridPropertyCode();
		}

		private void frmGlobalChanges_Resize(object sender, System.EventArgs e)
		{
			ResizeGridPropertyCode();
		}

		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			if (Grid.Row > 0)
			{
				gridNewPropertyCode.Visible = false;
				gridPropertyCode.Visible = false;
				txtNew.Visible = true;
				if (Strings.UCase(Grid.TextMatrix(Grid.Row, 1)) != "LANDSCHEDULE")
				{
					lblCurrent.Text = Grid.TextMatrix(Grid.Row, 0);
					framLandSched.Visible = false;
					if (Strings.LCase(Grid.TextMatrix(Grid.Row, 0)) == "property code")
					{
						gridNewPropertyCode.Visible = true;
						txtNew.Visible = false;
						if (cmbChoose.Text == "Map / Lot Range")
						{
							txtOriginal.Visible = true;
							gridPropertyCode.Visible = false;
							txtEndRange.Visible = true;
						}
						else
						{
							gridPropertyCode.Visible = true;
							txtOriginal.Visible = false;
							txtEndRange.Visible = false;
						}
					}
					else
					{
						txtOriginal.Visible = true;
					}
					Frame1.Visible = true;
					Frame1.BringToFront();
				}
				else
				{
					Frame1.Visible = false;
					framLandSched.Visible = true;
					framLandSched.BringToFront();
				}
			}
		}

		private void mnuChange_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse = 0;
			string strSQL = "";
			string strWhere = "";
			string strTable = "";
			string strField = "";
			string strDataType = "";
			string strSpecialField = "";
			string strQuotes = "";
			int[] intArray = null;
			string[] strArray = null;
			int x;
			string strOr = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strOText = "";
			string strNText = "";
			try
			{
				// On Error GoTo ErrorHandler
				if (Grid.Row < 1)
				{
					MessageBox.Show("You haven't picked a field to change yet.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				gridPropertyCode.Row = -1;
				gridNewPropertyCode.Row = -1;
				//Application.DoEvents();
				if (framLandSched.Visible == true)
				{
					if (Conversion.Val(txtSchedule.Text) <= 0)
					{
						MessageBox.Show("You must enter which code you want changed", "No Code Specified", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (Strings.Trim(txtNew.Text) == string.Empty)
					{
						MessageBox.Show("You must enter a value to change code " + txtSchedule.Text + " to.", "No Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					modGlobalFunctions.AddCYAEntry_80("RE", "Global changes", "Land Schedule Code " + txtSchedule.Text, "Value " + txtNew.Text);
					strSQL = "update landschedule set amount = " + FCConvert.ToString(Conversion.Val(txtNew.Text)) + " where code = " + FCConvert.ToString(Conversion.Val(txtSchedule.Text));
					clsTemp.Execute(strSQL, modGlobalVariables.strREDatabase);
					MessageBox.Show("Global changes complete.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else if (Strings.LCase(Grid.TextMatrix(Grid.Row, 0)) == "property code")
				{
					if (cmbChoose.Text == "Initial Value")
					{
						intResponse = MessageBox.Show("Are you sure you want to change all " + lblCurrent.Text + " from code " + gridPropertyCode.TextMatrix(0, 0) + " to code " + gridNewPropertyCode.TextMatrix(0, 0) + "?", "Change Values?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
						if (intResponse == DialogResult.Cancel)
						{
							return;
						}
						modGlobalFunctions.AddCYAEntry_242("RE", "Global Changes", lblCurrent.Text, "From " + gridPropertyCode.TextMatrix(0, 0), "To " + gridNewPropertyCode.TextMatrix(0, 0));
						strWhere = " where isnull(Propertycode,0) = " + FCConvert.ToString(Conversion.Val(gridPropertyCode.TextMatrix(0, 0)));
					}
					else
					{
						if (Strings.Trim(txtOriginal.Text) == string.Empty || Strings.Trim(txtEndRange.Text) == string.Empty)
						{
							MessageBox.Show("You must enter a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						intResponse = MessageBox.Show("Are you sure you want to change all " + lblCurrent.Text + " to code " + gridNewPropertyCode.TextMatrix(0, 0) + " for map/lots " + txtOriginal.Text + " to " + txtEndRange.Text + "?", "Change Values?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
						if (intResponse == DialogResult.Cancel)
						{
							return;
						}
						modGlobalFunctions.AddCYAEntry_242("RE", "Global Changes", lblCurrent.Text, "From map " + txtOriginal.Text + " to " + txtEndRange.Text, "To " + gridNewPropertyCode.TextMatrix(0, 0));
						strWhere = " where rsmaplot between '" + txtOriginal.Text + "' and '" + txtEndRange.Text + "' ";
					}
					strSQL = "Select rsaccount,rscard from master " + strWhere;
					clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
					if (!clsTemp.EndOfFile())
					{
						intResponse = MessageBox.Show(FCConvert.ToString(clsTemp.RecordCount()) + " cards matched." + "\r\n" + "Do you wish to see a report of cards affected?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
						if (intResponse == DialogResult.Yes)
						{
							if (cmbChoose.Text == "Initial Value")
							{
								rptGlobalChanges.InstancePtr.Init(strSQL, lblCurrent.Text + " from " + gridPropertyCode.TextMatrix(0, 0) + " to " + gridNewPropertyCode.TextMatrix(0, 0), this.Modal);
							}
							else
							{
								rptGlobalChanges.InstancePtr.Init(strSQL, lblCurrent.Text + " to " + txtNew.Text, this.Modal);
							}
						}
						clsTemp.Execute("update master set propertycode = " + gridNewPropertyCode.TextMatrix(0, 0) + " " + strWhere, modGlobalVariables.strREDatabase);
						MessageBox.Show("Global changes complete.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("No records were found", "No Matches", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					if (Strings.Trim(txtNew.Text) == string.Empty)
					{
						MessageBox.Show("You must enter a value to change to", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cmbChoose.Text == "Initial Value")
					{
						if (Strings.Trim(txtOriginal.Text) == string.Empty)
						{
							MessageBox.Show("You must enter the original value to continue", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						intResponse = MessageBox.Show("Are you sure you want to change all " + lblCurrent.Text + " from " + txtOriginal.Text + " to " + txtNew.Text + "?", "Change Values?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
						if (intResponse == DialogResult.Cancel)
						{
							return;
						}
						modGlobalFunctions.AddCYAEntry_242("RE", "Global Changes", lblCurrent.Text, "From " + txtOriginal.Text, "To " + txtNew.Text);
					}
					else
					{
						if (Strings.Trim(txtOriginal.Text) == string.Empty || Strings.Trim(txtEndRange.Text) == string.Empty)
						{
							MessageBox.Show("You must enter a valid range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						if (intResponse == DialogResult.Cancel)
						{
							return;
						}
						intResponse = MessageBox.Show("Are you sure you want to change all " + lblCurrent.Text + " to " + txtNew.Text + " for map/lots " + txtOriginal.Text + " to " + txtEndRange.Text + "?", "Change Values?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
						modGlobalFunctions.AddCYAEntry_242("RE", "Global Changes", lblCurrent.Text, "From map " + txtOriginal.Text + " to " + txtEndRange.Text, "To " + txtNew.Text);
					}
					strField = Strings.UCase(Grid.TextMatrix(Grid.Row, 2));
					strTable = Grid.TextMatrix(Grid.Row, 1);
					strDataType = Strings.UCase(Grid.TextMatrix(Grid.Row, 3));
					strSpecialField = Grid.TextMatrix(Grid.Row, 4);
					strOText = txtOriginal.Text;
					strNText = txtNew.Text;
					if (strDataType == "TEXT")
					{
						strQuotes = "'";
						strOText = modGlobalRoutines.escapequote(ref strOText);
						strNText = modGlobalRoutines.escapequote(ref strNText);
					}
					else
					{
						strQuotes = "";
					}
					if (cmbChoose.Text == "Initial Value")
					{
						if (strField == "SPECIAL")
						{
							strWhere = " where ";
							strOr = "";
							if (strDataType == "TEXT")
							{
								strArray = Strings.Split(strSpecialField, ",", -1, CompareConstants.vbTextCompare);
								for (x = 0; x <= Information.UBound(strArray, 1); x++)
								{
									strWhere += strOr + "(";
									strWhere += strArray[x] + " = ";
									// strWhere = strWhere & "'" & txtOriginal.Text & "'"
									strWhere += "'" + strOText + "'";
									strWhere += ")";
									strOr = " Or ";
								}
								// x
							}
							else
							{
								// intArray = Split(strSpecialField, ",", , vbTextCompare)
								strArray = Strings.Split(strSpecialField, ",", -1, CompareConstants.vbTextCompare);
								for (x = 0; x <= Information.UBound(strArray, 1); x++)
								{
									strWhere += strOr + "(";
									strWhere += strArray[x] + " = ";
									strWhere += strOText;
									strWhere += ")";
									strOr = " Or ";
								}
								// x
							}
						}
						else
						{
							strWhere = " where " + strField + " = " + strQuotes + strOText + strQuotes;
						}
					}
					else
					{
						strWhere = " where rsmaplot between '" + txtOriginal.Text + "' and '" + txtEndRange.Text + "' ";
						if (strField == "SPECIAL")
						{
							strArray = Strings.Split(strSpecialField, ",", -1, CompareConstants.vbTextCompare);
						}
					}
					// first find out how many are changing
					clsTemp.OpenRecordset("Select rsaccount,rscard from " + strTable + " " + strWhere, modGlobalVariables.strREDatabase);
					if (!clsTemp.EndOfFile())
					{
						intResponse = MessageBox.Show(FCConvert.ToString(clsTemp.RecordCount()) + " cards matched." + "\r\n" + "Do you wish to see a report of cards affected?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
						if (intResponse == DialogResult.Yes)
						{
							rptGlobalChanges.InstancePtr.Init("Select rsaccount,rscard from " + strTable + " " + strWhere, lblCurrent.Text + " from " + txtOriginal.Text + " to " + txtNew.Text, this.Modal);
						}
						strSQL = "update " + strTable + " set ";
						if (strField == "SPECIAL")
						{
							if (strDataType == "TEXT")
							{
								for (x = 0; x <= Information.UBound(strArray, 1); x++)
								{
									if (cmbChoose.Text == "Initial Value")
									{
										clsTemp.Execute(strSQL + strArray[x] + " = '" + strNText + "' where " + strArray[x] + " = '" + strOText + "'", modGlobalVariables.strREDatabase);
									}
									else
									{
										clsTemp.Execute(strSQL + strArray[x] + " = '" + strNText + "' " + strWhere, modGlobalVariables.strREDatabase);
									}
								}
								// x
							}
							else
							{
								for (x = 0; x <= Information.UBound(strArray, 1); x++)
								{
									if (cmbChoose.Text == "Initial Value")
									{
										clsTemp.Execute(strSQL + strArray[x] + " = " + txtNew.Text + " where " + strArray[x] + " = " + txtOriginal.Text, modGlobalVariables.strREDatabase);
									}
									else
									{
										clsTemp.Execute(strSQL + strArray[x] + " = " + txtNew.Text + strWhere, modGlobalVariables.strREDatabase);
									}
								}
								// x
							}
						}
						else
						{
							strSQL += strField + " = " + strQuotes + strNText + strQuotes + strWhere;
							clsTemp.Execute(strSQL, modGlobalVariables.strREDatabase);
						}
						MessageBox.Show("Global changes complete.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("No records were found", "No Matches", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuChange", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void GridSetup()
		{
			Grid.Cols = 5;
			Grid.ColHidden(1, true);
			Grid.ColHidden(2, true);
			Grid.ColHidden(3, true);
			Grid.ColHidden(4, true);
			// now fill grid
			Grid.Rows = 1;
			Grid.AddItem("Land Code" + "\t" + "Master" + "\t" + "rilandcode" + "\t" + "number");
			Grid.AddItem("Building Code" + "\t" + "Master" + "\t" + "RiBldgCode" + "\t" + "number");
			Grid.AddItem("Tran Code" + "\t" + "Master" + "\t" + "RITranCode" + "\t" + "number");
			Grid.AddItem("Neighborhood Code" + "\t" + "Master" + "\t" + "pineighborhood" + "\t" + "number");
			Grid.AddItem("Property Code" + "\t" + "Master" + "\t" + "propertycode" + "\t" + "number");
			Grid.AddItem("Tree Growth Year" + "\t" + "Master" + "\t" + "pistreetcode" + "\t" + "number");
			Grid.AddItem("Zone" + "\t" + "Master" + "\t" + "PIZone" + "\t" + "number");
			Grid.AddItem("Secondary Zone" + "\t" + "Master" + "\t" + "PISecZone" + "\t" + "number");
			Grid.AddItem("Topography" + "\t" + "Master" + "\t" + "Special" + "\t" + "number" + "\t" + "pitopography1,pitopography2");
			Grid.AddItem("Utilities" + "\t" + "Master" + "\t" + "SPECIAL" + "\t" + "number" + "\t" + "piutilities1,piutilities2");
			Grid.AddItem("Street (Paved, etc.)" + "\t" + "Master" + "\t" + "pistreet" + "\t" + "number");
			Grid.AddItem("Open1" + "\t" + "Master" + "\t" + "piopen1" + "\t" + "number");
			Grid.AddItem("Open2" + "\t" + "Master" + "\t" + "piopen2" + "\t" + "number");
			Grid.AddItem("Exempt Code" + "\t" + "Master" + "\t" + "SPECIAL" + "\t" + "number" + "\t" + "RIExemptcd1,riexemptcd2,riexemptcd3");
			Grid.AddItem("Location" + "\t" + "Master" + "\t" + "rslocstreet" + "\t" + "text");
			Grid.AddItem("Land Type" + "\t" + "Master" + "\t" + "SPECIAL" + "\t" + "number" + "\t" + "piland1type,piland2type,piland3type,piland4type,piland5type,piland6type,piland7type");
			Grid.AddItem("Land Influence" + "\t" + "Master" + "\t" + "SPECIAL" + "\t" + "number" + "\t" + "piland1inf,piland2inf,piland3inf,piland4inf,piland5inf,piland6inf,piland7inf");
			Grid.AddItem("Land Influence Code" + "\t" + "Master" + "\t" + "SPECIAL" + "\t" + "number" + "\t" + "piland1infcode,piland2infcode,piland3infcode,piland4infcode,piland5infcode,piland6infcode,piland7infcode");
			Grid.AddItem("Land Override Code" + "\t" + "Master" + "\t" + "HiValLandCode" + "\t" + "number");
			Grid.AddItem("Building Override Code" + "\t" + "Master" + "\t" + "HiValBldgCode" + "\t" + "number");
			if (cmbChoose.Text == "Initial Value")
			{
				Grid.AddItem("Dwelling - Building Style" + "\t" + "Dwelling" + "\t" + "distyle" + "\t" + "number");
				Grid.AddItem("Dwelling - Stories" + "\t" + "Dwelling" + "\t" + "distories" + "\t" + "number");
				Grid.AddItem("Dwelling - Exterior Walls" + "\t" + "Dwelling" + "\t" + "diextwalls" + "\t" + "number");
				Grid.AddItem("Dwelling - Roof Surface" + "\t" + "Dwelling" + "\t" + "diroof" + "\t" + "number");
				Grid.AddItem("Dwelling - Open 3" + "\t" + "Dwelling" + "\t" + "diopen3" + "\t" + "number");
				Grid.AddItem("Dwelling - Open 4" + "\t" + "Dwelling" + "\t" + "diopen4" + "\t" + "number");
				Grid.AddItem("Dwelling - Open 5" + "\t" + "Dwelling" + "\t" + "diopen5" + "\t" + "number");
				Grid.AddItem("Dwelling - Foundation" + "\t" + "Dwelling" + "\t" + "Difoundation" + "\t" + "number");
				Grid.AddItem("Dwelling - Basement" + "\t" + "Dwelling" + "\t" + "dibsmt" + "\t" + "number");
				Grid.AddItem("Dwelling - Wet Basment" + "\t" + "Dwelling" + "\t" + "diwetbsmt" + "\t" + "number");
				Grid.AddItem("Dwelling - Fin. Bsmt. Grade" + "\t" + "Dwelling" + "\t" + "dibsmtfingrade1" + "\t" + "number");
				Grid.AddItem("Dwelling - Heat Type" + "\t" + "Dwelling" + "\t" + "diheat" + "\t" + "number");
				Grid.AddItem("Dwelling - Cool Type" + "\t" + "Dwelling" + "\t" + "dicool" + "\t" + "number");
				Grid.AddItem("Dwelling - Kitchen Style" + "\t" + "Dwelling" + "\t" + "dikitchens" + "\t" + "number");
				Grid.AddItem("Dwelling - Bath Style" + "\t" + "Dwelling" + "\t" + "dibaths" + "\t" + "number");
				Grid.AddItem("Dwelling - Layout" + "\t" + "Dwelling" + "\t" + "dilayout" + "\t" + "number");
				Grid.AddItem("Dwelling - Attic" + "\t" + "Dwelling" + "\t" + "diattic" + "\t" + "number");
				Grid.AddItem("Dwelling - Insulation" + "\t" + "Dwelling" + "\t" + "diinsulation" + "\t" + "number");
				Grid.AddItem("Dwelling - Grade Code" + "\t" + "Dwelling" + "\t" + "digrade1" + "\t" + "number");
				Grid.AddItem("Dwelling - Condition" + "\t" + "Dwelling" + "\t" + "dicondition" + "\t" + "number");
				Grid.AddItem("Dwelling - Functional Code" + "\t" + "Dwelling" + "\t" + "difunctcode" + "\t" + "number");
				Grid.AddItem("Dwelling - Economic Code" + "\t" + "Dwelling" + "\t" + "dieconcode" + "\t" + "number");
				Grid.AddItem("Outbuilding - Type" + "\t" + "Outbuilding" + "\t" + "SPECIAL" + "\t" + "number" + "\t" + "oitype1,oitype2,oitype3,oitype4,oitype5,oitype6,oitype7,oitype8,oitype9,oitype10");
				Grid.AddItem("Commercial - Occupancy Code" + "\t" + "Commercial" + "\t" + "SPECIAL" + "\t" + "number" + "\t" + "Occ1,occ2");
				Grid.AddItem("Commercial - Exterior Walls" + "\t" + "Commercial" + "\t" + "SPECIAL" + "\t" + "number" + "\t" + "c1extwalls,c2extwalls");
				Grid.AddItem("Commercial - Heating/Cooling" + "\t" + "Commercial" + "\t" + "SPECIAL" + "\t" + "number" + "\t" + "c1Heat,c2heat");
				Grid.AddItem("Commercial - Condition" + "\t" + "Commercial" + "\t" + "SPECIAL" + "\t" + "number" + "\t" + "c1condition,c2condition");
			}
			Grid.AddItem("Land Schedule - Value" + "\t" + "LandSchedule" + "\t" + "Amount" + "\t" + "number");
		}

		private void optChoose_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// initial
						Label1.Visible = true;
						Label3.Visible = false;
						txtEndRange.Visible = false;
						// GridSetup
						txtOriginal.Text = "";
						if (Strings.LCase(Grid.TextMatrix(Grid.Row, 0)) == "property code")
						{
							gridPropertyCode.Visible = true;
							txtOriginal.Visible = false;
						}
						else
						{
							txtOriginal.Visible = true;
							gridPropertyCode.Visible = false;
						}
						break;
					}
				case 1:
					{
						Label1.Visible = false;
						Label3.Visible = true;
						txtEndRange.Visible = true;
						txtOriginal.Text = "";
						txtOriginal.Visible = true;
						gridPropertyCode.Visible = false;
						// GridSetup
						break;
					}
			}
			//end switch
		}

		private void optChoose_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbChoose.SelectedIndex;
			optChoose_CheckedChanged(index, sender, e);
		}

		private void gridPropertyCode_ComboCloseUp(object sender, EventArgs e)
		{
			gridPropertyCode.Row = -1;
			//Application.DoEvents();
		}

		private void gridNewPropertyCode_ComboCloseUp(object sender, EventArgs e)
		{
			gridNewPropertyCode.Row = -1;
			//Application.DoEvents();
		}

		private void gridPropertyCode_ComboDropDown(object sender, System.EventArgs e)
		{
			int x;
			int lngID;
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(gridPropertyCode.TextMatrix(0, 0))));
			if (lngID > 0)
			{
				for (x = 0; x <= gridPropertyCode.ComboCount - 1; x++)
				{
					if (Conversion.Val(gridPropertyCode.ComboData(x)) == lngID)
					{
						gridPropertyCode.ComboIndex = x;
					}
				}
				// x
			}
		}

		private void gridNewPropertyCode_ComboDropDown(object sender, System.EventArgs e)
		{
			int x;
			int lngID;
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(gridNewPropertyCode.TextMatrix(0, 0))));
			if (lngID > 0)
			{
				for (x = 0; x <= gridNewPropertyCode.ComboCount - 1; x++)
				{
					if (Conversion.Val(gridNewPropertyCode.ComboData(x)) == lngID)
					{
						gridNewPropertyCode.ComboIndex = x;
					}
				}
				// x
			}
		}

		private void ResizeGridPropertyCode()
		{
			//FC:FINAL:RPU:#i1318 - These were fixed in designer
			//gridPropertyCode.Height = gridPropertyCode.RowHeight(0) + 60;
			//gridNewPropertyCode.Height = gridNewPropertyCode.RowHeight(0) + 60;
		}

		private void SetupGridPropertyCode()
		{
			string strTemp;
			strTemp = "#0;0 None" + "\t" + " ";
			strTemp += "|#101;101 Rural" + "\t" + "Vacant Land|";
			strTemp += "#102;102 Urban" + "\t" + "Vacant Land|";
			strTemp += "#103;103 Oceanfront" + "\t" + "Vacant Land|";
			strTemp += "#104;104 Lake/Pond front" + "\t" + "Vacant Land|";
			strTemp += "#105;105 Stream/Riverfront" + "\t" + "Vacant Land|";
			strTemp += "#106;106 Agricultural" + "\t" + "Vacant Land|";
			strTemp += "#107;107 Commercial Zone" + "\t" + "Vacant Land|";
			strTemp += "#120;120 Other" + "\t" + "Vacant Land|";
			strTemp += "#201;201 Rural" + "\t" + "Single Family|";
			strTemp += "#202;202 Urban" + "\t" + "Single Family|";
			strTemp += "#203;203 Oceanfront" + "\t" + "Single Family|";
			strTemp += "#204;204 Lake/Pond front" + "\t" + "Single Family|";
			strTemp += "#205;205 Stream/Riverfront" + "\t" + "Single Family|";
			strTemp += "#206;206 Mobile Home" + "\t" + "Single Family|";
			strTemp += "#220;220 Other" + "\t" + "Single Family|";
			strTemp += "#301;301 Mixed Use" + "\t" + "Commercial|";
			strTemp += "#302;302 Apt/2-3 Unit" + "\t" + "Commercial|";
			strTemp += "#303;303 Apt/4+" + "\t" + "Commercial|";
			strTemp += "#304;304 Bank" + "\t" + "Commercial|";
			strTemp += "#305;305 Restaurant" + "\t" + "Commercial|";
			strTemp += "#306;306 Medical" + "\t" + "Commercial|";
			strTemp += "#307;307 Office" + "\t" + "Commercial|";
			strTemp += "#308;308 Retail" + "\t" + "Commercial|";
			strTemp += "#309;309 Automotive" + "\t" + "Commercial|";
			strTemp += "#310;310 Marina" + "\t" + "Commercial|";
			strTemp += "#311;311 Warehouse" + "\t" + "Commercial|";
			strTemp += "#312;312 Hotel/Motel/Inn" + "\t" + "Commercial|";
			strTemp += "#313;313 Nursing Home" + "\t" + "Commercial|";
			strTemp += "#314;314 Shopping Mall" + "\t" + "Commercial|";
			strTemp += "#320;320 Other" + "\t" + "Commercial|";
			strTemp += "#401;401 Gas and Oil" + "\t" + "Industrial|";
			strTemp += "#402;402 Utility" + "\t" + "Industrial|";
			strTemp += "#403;403 Gravel Pit" + "\t" + "Industrial|";
			strTemp += "#404;404 Lumber/Saw Mill" + "\t" + "Industrial|";
			strTemp += "#405;405 Pulp/Paper Mill" + "\t" + "Industrial|";
			strTemp += "#406;406 Light Manufacture" + "\t" + "Industrial|";
			strTemp += "#407;407 Heavy Manufacture" + "\t" + "Industrial|";
			strTemp += "#420;420 Manufacture Other" + "\t" + "Industrial|";
			strTemp += "#501;501 Government" + "\t" + "Misc Codes|";
			strTemp += "#502;502 Condominium" + "\t" + "Misc Codes|";
			strTemp += "#503;503 Timeshare Unit" + "\t" + "Misc Codes|";
			strTemp += "#504;504 Non-Profit" + "\t" + "Misc Codes|";
			strTemp += "#505;505 Mobile Home Park" + "\t" + "Misc Codes|";
			strTemp += "#506;506 Airport" + "\t" + "Misc Codes|";
			strTemp += "#507;507 Conservation" + "\t" + "Misc Codes|";
			strTemp += "#508;508 Current Use Classification" + "\t" + "Misc Codes|";
			strTemp += "#520;520 Other" + "\t" + "Misc Codes";
			gridPropertyCode.ColComboList(0, strTemp);
			gridPropertyCode.TextMatrix(0, 0, FCConvert.ToString(0));
			gridNewPropertyCode.ColComboList(0, strTemp);
			gridNewPropertyCode.TextMatrix(0, 0, FCConvert.ToString(0));
		}
	}
}
