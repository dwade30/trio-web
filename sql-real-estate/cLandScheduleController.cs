﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cLandScheduleController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

        public cLandSchedule GetLandSchedule(int lngID)
        {
            cLandSchedule GetLandSchedule = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cLandSchedule lSched = null;
                cLandScheduleRecord schedDetail;
                rsLoad.OpenRecordset("select * from LandSchedule where id = " + FCConvert.ToString(lngID), "RealEstate");
                if (!rsLoad.EndOfFile())
                {
                    lSched = new cLandSchedule();
                    lSched.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
                    lSched.ScheduleNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Schedule"));
                    // TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
                    lSched.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("TownNumber"))));
                    lSched.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
                    rsLoad.OpenRecordset("select * from landschedule where schedule = " + FCConvert.ToString(lSched.ScheduleNumber) + " and isnull(TownNumber,0) = " + FCConvert.ToString(lSched.TownCode) + " order by [code]", "RealEstate");
                    while (!rsLoad.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        if (FCConvert.ToInt32(rsLoad.Get_Fields("Code")) == -4)
                        {
                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            lSched.StandardWidth = Conversion.Val(rsLoad.Get_Fields("Amount"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (FCConvert.ToInt32(rsLoad.Get_Fields("Code")) == -3)
                        {
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (FCConvert.ToInt32(rsLoad.Get_Fields("Code")) == -2)
                        {
                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            lSched.StandardLot = Conversion.Val(rsLoad.Get_Fields("Amount"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (FCConvert.ToInt32(rsLoad.Get_Fields("Code")) == -1)
                        {
                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            lSched.StandardDepth = Conversion.Val(rsLoad.Get_Fields("Amount"));
                        }
                        else
                        {
                            schedDetail = new cLandScheduleRecord();
                            // TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
                            schedDetail.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));
                            schedDetail.Exponent1 = rsLoad.Get_Fields_Double("Exponent1");
                            schedDetail.Exponent2 = rsLoad.Get_Fields_Double("Exponent2");
                            schedDetail.WidthExponent1 = rsLoad.Get_Fields_Double("WidthExponent1");
                            schedDetail.WidthExponent2 = rsLoad.Get_Fields_Double("WidthExponent2");
                            schedDetail.ScheduleNumber = lSched.ScheduleNumber;
                            schedDetail.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
                            // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                            schedDetail.LandTypeCode = FCConvert.ToInt32(rsLoad.Get_Fields("Code"));
                            // TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
                            schedDetail.TownNumber = FCConvert.ToInt32(rsLoad.Get_Fields("TownNumber"));
                            schedDetail.IsUpdated = false;
                            lSched.Details.AddItem(schedDetail);
                        }
                        rsLoad.MoveNext();
                    }
                    lSched.IsUpdated = false;
                }
                GetLandSchedule = lSched;
                return GetLandSchedule;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = Information.Err(ex).Description;
            }
            return GetLandSchedule;
        }

		public cLandSchedule GetLandScheduleByNumber(int lngScheduleNumber)
		{
			cLandSchedule GetLandScheduleByNumber = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cLandSchedule lSched = null;
				cLandScheduleRecord schedDetail;
				rsLoad.OpenRecordset("select * from LandSchedule where Schedule = " + FCConvert.ToString(lngScheduleNumber) + " and [Code] = -3 ", "RealEstate");
				if (!rsLoad.EndOfFile())
				{
					lSched = new cLandSchedule();
					lSched.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					lSched.ScheduleNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Schedule"));
					// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
					lSched.TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("TownNumber"))));
					lSched.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					lSched.IsUpdated = false;
					rsLoad.OpenRecordset("select * from landschedule where schedule = " + FCConvert.ToString(lSched.ScheduleNumber) + " and isnull(TownNumber,0) = " + FCConvert.ToString(lSched.TownCode) + " order by [code]", "RealEstate");
					while (!rsLoad.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsLoad.Get_Fields("Code")) == -4)
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							lSched.StandardWidth = Conversion.Val(rsLoad.Get_Fields("Amount"));
						}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						else if (FCConvert.ToInt32(rsLoad.Get_Fields("Code")) == -3)
						{
						}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if (FCConvert.ToInt32(rsLoad.Get_Fields("Code")) == -2)
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							lSched.StandardLot = Conversion.Val(rsLoad.Get_Fields("Amount"));
						}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (FCConvert.ToInt32(rsLoad.Get_Fields("Code")) == -1)
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							lSched.StandardDepth = Conversion.Val(rsLoad.Get_Fields("Amount"));
						}
						else
						{
							schedDetail = new cLandScheduleRecord();
							// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
							schedDetail.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));
							schedDetail.Exponent1 = rsLoad.Get_Fields_Double("Exponent1");
							schedDetail.Exponent2 = rsLoad.Get_Fields_Double("Exponent2");
							schedDetail.WidthExponent1 = rsLoad.Get_Fields_Double("WidthExponent1");
							schedDetail.WidthExponent2 = rsLoad.Get_Fields_Double("WidthExponent2");
							schedDetail.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							schedDetail.LandTypeCode = FCConvert.ToInt32(rsLoad.Get_Fields("Code"));
							// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
							schedDetail.TownNumber = FCConvert.ToInt32(rsLoad.Get_Fields("TownNumber"));
							schedDetail.IsUpdated = false;
							lSched.Details.AddItem(schedDetail);
						}
						rsLoad.MoveNext();
					}
				}
				GetLandScheduleByNumber = lSched;
				return GetLandScheduleByNumber;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetLandScheduleByNumber;
		}

		public cGenericCollection GetLandSchedules()
		{
			cGenericCollection GetLandSchedules = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cLandSchedule lSched;
				rs.OpenRecordset("select id from landschedule where [code] = -3  order by Schedule", "RealEstate");
				while (!rs.EndOfFile())
				{
					lSched = GetLandSchedule(rs.Get_Fields_Int32("ID"));
					gColl.AddItem(lSched);
					rs.MoveNext();
				}
				GetLandSchedules = gColl;
				return GetLandSchedules;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetLandSchedules;
		}

		public void DeleteLandSchedule(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset("select * from landschedule where id = " + FCConvert.ToString(lngID), "RealEstate");
				if (!rs.EndOfFile())
				{
					int lngSchedule = 0;
					int lngTownCode = 0;
					lngSchedule = FCConvert.ToInt32(rs.Get_Fields_Int32("Schedule"));
					// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
					lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("TownNumber"))));
					rs.Execute("Delete from landschedule where schedule = " + FCConvert.ToString(lngSchedule) + " and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode), "RealEstate");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void DeleteLandScheduleDetail(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset("delete from landschedule where id = " + FCConvert.ToString(lngID), "RealEstate");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveLandSchedules(ref cGenericCollection collSchedules)
		{
			try
			{
				ClearErrors();
				if (!(collSchedules == null))
				{
					cLandSchedule lSched;
					collSchedules.MoveFirst();
					while (collSchedules.IsCurrent())
					{
						lSched = (cLandSchedule)collSchedules.GetCurrentItem();
						if (lSched.IsUpdated)
						{
							SaveLandSchedule(ref lSched);
							if (HadError)
							{
								return;
							}
						}
						collSchedules.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		private int GetNextLandScheduleNumber()
		{
			int GetNextLandScheduleNumber = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select max(schedule) as maxschedule from landschedule", "RealEstate");
			int lngReturn;
			// TODO Get_Fields: Field [maxschedule] not found!! (maybe it is an alias?)
			lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("maxschedule"))));
			GetNextLandScheduleNumber = lngReturn + 1;
			return GetNextLandScheduleNumber;
		}

		public void SaveLandSchedule(ref cLandSchedule lSched)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (lSched.IsDeleted)
				{
					DeleteLandSchedule(lSched.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from LandSchedule where id = " + lSched.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (lSched.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Land schedule record not found";
						return;
					}
					rsSave.AddNew();
				}
				if (lSched.ScheduleNumber < 1)
				{
					lSched.ScheduleNumber = GetNextLandScheduleNumber();
				}
				rsSave.Set_Fields("Schedule", lSched.ScheduleNumber);
				rsSave.Set_Fields("Code", -3);
				rsSave.Set_Fields("amount", 0);
				rsSave.Set_Fields("Exponent1", 0);
				rsSave.Set_Fields("Exponent2", 0);
				rsSave.Set_Fields("WidthExponent1", 0);
				rsSave.Set_Fields("WidthExponent2", 0);
				rsSave.Set_Fields("TownNumber", lSched.TownCode);
				rsSave.Set_Fields("Description", lSched.Description);
				rsSave.Update();
				lSched.ID = rsSave.Get_Fields_Int32("ID");
				lSched.IsUpdated = false;
				cLandScheduleRecord tDetail;
				tDetail = new cLandScheduleRecord();
				tDetail.Amount = lSched.StandardWidth;
				tDetail.Exponent1 = 0;
				tDetail.Exponent2 = 0;
				tDetail.WidthExponent1 = 0;
				tDetail.WidthExponent2 = 0;
				tDetail.LandTypeCode = -4;
				tDetail.ScheduleNumber = lSched.ScheduleNumber;
				tDetail.TownNumber = lSched.TownCode;
				UpdateLandScheduleRecord(ref tDetail);
				tDetail = new cLandScheduleRecord();
				tDetail.Amount = lSched.StandardLot;
				tDetail.Exponent1 = 0;
				tDetail.Exponent2 = 0;
				tDetail.WidthExponent1 = 0;
				tDetail.WidthExponent2 = 0;
				tDetail.LandTypeCode = -2;
				tDetail.ScheduleNumber = lSched.ScheduleNumber;
				tDetail.TownNumber = lSched.TownCode;
				UpdateLandScheduleRecord(ref tDetail);
				tDetail = new cLandScheduleRecord();
				tDetail.Amount = lSched.StandardDepth;
				tDetail.LandTypeCode = -1;
				tDetail.ScheduleNumber = lSched.ScheduleNumber;
				tDetail.TownNumber = lSched.TownCode;
				UpdateLandScheduleRecord(ref tDetail);
				lSched.Details.MoveFirst();
				// Dim rsDetails As New clsDRWrapper
				// Call rsDetails.OpenRecordset("Select * from landschedule where schedule = " & lSched.ScheduleNumber, "RealEstate")
				while (lSched.Details.IsCurrent())
				{
					tDetail = (cLandScheduleRecord)lSched.Details.GetCurrentItem();
					tDetail.ScheduleNumber = lSched.ScheduleNumber;
					// Call SaveLandScheduleRecordBulk(tDetail, rsDetails)
					SaveLandScheduleRecord(ref tDetail);
					if (HadError)
					{
						return;
					}
					if (tDetail.IsDeleted)
					{
						lSched.Details.RemoveCurrent();
					}
					lSched.Details.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		private void SaveLandScheduleRecordBulk(ref cLandScheduleRecord lsRec, ref clsDRWrapper rsSave)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (lsRec.IsDeleted)
				{
					DeleteLandScheduleDetail(lsRec.ID);
					return;
				}
				if (rsSave.FindFirst("id = " + lsRec.ID))
				{
					rsSave.Edit();
				}
				else
				{
					if (lsRec.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Land schedule record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("Schedule", lsRec.ScheduleNumber);
				rsSave.Set_Fields("Code", lsRec.LandTypeCode);
				rsSave.Set_Fields("Amount", lsRec.Amount);
				rsSave.Set_Fields("Exponent1", lsRec.Exponent1);
				rsSave.Set_Fields("Exponent2", lsRec.Exponent2);
				rsSave.Set_Fields("WidthExponent1", lsRec.WidthExponent1);
				rsSave.Set_Fields("WidthExponent2", lsRec.WidthExponent2);
				rsSave.Set_Fields("TownNumber", lsRec.TownNumber);
				rsSave.Set_Fields("Description", "");
				rsSave.Update();
				lsRec.ID = rsSave.Get_Fields_Int32("ID");
				lsRec.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void SaveLandScheduleRecord(ref cLandScheduleRecord lsRec)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				if (lsRec.IsDeleted)
				{
					DeleteLandScheduleDetail(lsRec.ID);
					return;
				}
				rsSave.OpenRecordset("Select * from LandSchedule where id = " + lsRec.ID, "RealEstate");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (lsRec.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Land schedule record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("Schedule", lsRec.ScheduleNumber);
				rsSave.Set_Fields("Code", lsRec.LandTypeCode);
				rsSave.Set_Fields("Amount", lsRec.Amount);
				rsSave.Set_Fields("Exponent1", lsRec.Exponent1);
				rsSave.Set_Fields("Exponent2", lsRec.Exponent2);
				rsSave.Set_Fields("WidthExponent1", lsRec.WidthExponent1);
				rsSave.Set_Fields("WidthExponent2", lsRec.WidthExponent2);
				rsSave.Set_Fields("TownNumber", lsRec.TownNumber);
				rsSave.Set_Fields("Description", "");
				rsSave.Update();
				lsRec.ID = rsSave.Get_Fields_Int32("ID");
				lsRec.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		private void UpdateLandScheduleRecord(ref cLandScheduleRecord lsRec)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			if (lsRec.IsDeleted)
			{
				DeleteLandScheduleDetail(lsRec.ID);
				return;
			}
			rsSave.OpenRecordset("select * from LandSchedule where Schedule = " + lsRec.ScheduleNumber + " and TownNumber = " + lsRec.TownNumber + " and [Code] = " + lsRec.LandTypeCode, "RealEstate");
			if (!rsSave.EndOfFile())
			{
				rsSave.Edit();
			}
			else
			{
				if (lsRec.ID > 0)
				{
					lngLastError = 9999;
					strLastError = "Land schedule record not found";
					return;
				}
				rsSave.AddNew();
			}
			rsSave.Set_Fields("Schedule", lsRec.ScheduleNumber);
			rsSave.Set_Fields("Code", lsRec.LandTypeCode);
			rsSave.Set_Fields("Amount", lsRec.Amount);
			rsSave.Set_Fields("Exponent1", lsRec.Exponent1);
			rsSave.Set_Fields("Exponent2", lsRec.Exponent2);
			rsSave.Set_Fields("WidthExponent1", lsRec.WidthExponent1);
			rsSave.Set_Fields("WidthExponent2", lsRec.WidthExponent2);
			rsSave.Set_Fields("TownNumber", lsRec.TownNumber);
			rsSave.Set_Fields("Description", "");
			rsSave.Update();
			lsRec.ID = rsSave.Get_Fields_Int32("ID");
			lsRec.IsUpdated = false;
		}
	}
}
