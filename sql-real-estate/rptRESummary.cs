﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptRESummary.
	/// </summary>
	public partial class rptRESummary : BaseSectionReport
	{
		public rptRESummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Real Estate Summary";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptRESummary InstancePtr
		{
			get
			{
				return (rptRESummary)Sys.GetInstance(typeof(rptRESummary));
			}
		}

		protected rptRESummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRESummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTCMBUPDATEAMOUNTS = 0;
		const int CNSTCMBUPDATEINFOONLY = 1;
		double lngTotLand;
		double lngTotBldg;
		double lngTotExempt;
		double lngTotAssess;
		bool boolUseExceeds;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngNumBadExempts = 0;
			int lngNumBadExemptAmounts = 0;
			int lngNumBadMapLots = 0;
			int lngNumUpdatedTooMany = 0;
			int lngNumDeleted = 0;
			int lngNumNewLand = 0;
			int lngNumNewBldg = 0;
			int lngLandChange = 0;
			int lngBldgChange = 0;
			int lngExemptionChange = 0;
			int lngOldLand = 0;
			int lngNewLand = 0;
			int lngOldBldg = 0;
			int lngNewBldg = 0;
			// vbPorter upgrade warning: lngNewExempt As int	OnWriteFCConvert.ToDouble(
			int lngNewExempt = 0;
			int lngOldExempt = 0;
			int lngExemptUps = 0;
			int lngExemptDowns = 0;
			double lngTotOtherValue;
			int lngLandUps = 0;
			int lngLandDowns = 0;
			int lngBldgUps = 0;
			int lngBldgDowns = 0;
			double lngTotLandChange = 0;
			double lngTotBldgChange = 0;
			double lngTotExemptChange = 0;
			int lngLandExceedsUp = 0;
			int lngLandExceedsDown = 0;
			int lngBldgExceedsUp = 0;
			int lngBldgExceedsDown = 0;
			double lngTotLandNotUpdated = 0;
			double lngTotBldgNotUpdated = 0;
			double lngTotExemptNotUpdated = 0;
			double lngLandNotXfered;
			double lngBldgNotXfered;
			double lngExemptNotXfered;
			double lngNetNotXfered;
			double dblChange = 0;
			if (frmImportCompetitorXF.InstancePtr.cmbUpdateType.ItemData(frmImportCompetitorXF.InstancePtr.cmbUpdateType.SelectedIndex) == CNSTCMBUPDATEINFOONLY)
			{
				Detail.Height = 3300 / 1440F;
				Detail.CanGrow = false;
			}
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngTotLand = 0;
			lngTotBldg = 0;
			lngTotAssess = 0;
			lngTotExempt = 0;
			lngTotOtherValue = 0;
			lngLandNotXfered = 0;
			lngBldgNotXfered = 0;
			lngExemptNotXfered = 0;
			lngNetNotXfered = 0;
			if (boolUseExceeds)
			{
				dblChange = Conversion.Val(frmImportCompetitorXF.InstancePtr.txtPercentChg.Text);
				lblLandExceed.Text = "Land Exceeds " + FCConvert.ToString(dblChange) + "% Change";
				lblBldgExceed.Text = "Building Exceeds " + FCConvert.ToString(dblChange) + "% Change";
			}
			txtNoMatch.Text = Strings.Format(frmImportCompetitorXF.InstancePtr.GridNew.Rows - 1, "#,###,##0");
			txtNotUpdated.Text = Strings.Format(frmImportCompetitorXF.InstancePtr.GridNotUpdated.Rows - 1, "#,###,##0");
			txtUpdated.Text = Strings.Format(frmImportCompetitorXF.InstancePtr.GridGood.Rows - 1, "#,###,##0");
			lngNumBadExempts = 0;
			lngNumBadExemptAmounts = 0;
			lngNumBadMapLots = 0;
			lngNumUpdatedTooMany = 0;
			lngNumNewLand = 0;
			lngNumNewBldg = 0;
			lngTotLandChange = 0;
			lngTotBldgChange = 0;
			lngTotExemptChange = 0;
			lngLandDowns = 0;
			lngLandUps = 0;
			lngBldgDowns = 0;
			lngBldgUps = 0;
			lngTotLandChange = 0;
			lngTotBldgChange = 0;
			lngTotExemptChange = 0;
			lngLandExceedsUp = 0;
			lngBldgExceedsUp = 0;
			lngLandExceedsDown = 0;
			lngBldgExceedsDown = 0;
			for (x = 1; x <= (frmImportCompetitorXF.InstancePtr.GridBadAccounts.Rows - 1); x++)
			{
				if (FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBADEXEMPT)))
				{
					lngNumBadExempts += 1;
				}
				if (FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBADEXEMPTAMOUNT)))
				{
					lngNumBadExemptAmounts += 1;
				}
				if (FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTBADMAPLOT)))
				{
					lngNumBadMapLots += 1;
				}
				if (FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTDELETED)))
				{
					lngNumDeleted += 1;
				}
				if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridBadAccounts.TextMatrix(x, modGridConstantsXF.CNSTUPDATED)) > 1)
				{
					lngNumUpdatedTooMany += 1;
				}
			}
			// x
			// process the unchanged stuff
			// need to account for it in the totals
			for (x = 1; x <= (frmImportCompetitorXF.InstancePtr.GridNotUpdated.Rows - 1); x++)
			{
				if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE)) > 0)
				{
					lngTotLand += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE));
					lngTotLandNotUpdated += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE));
				}
				if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE)) > 0)
				{
					lngTotBldg += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE));
					lngTotBldgNotUpdated += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE));
				}
				if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTORIGINALEXEMPTION)) > 0)
				{
					lngTotExempt += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTORIGINALEXEMPTION));
					lngTotExemptNotUpdated += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNotUpdated.TextMatrix(x, modGridConstantsXF.CNSTORIGINALEXEMPTION));
				}
			}
			// x
			// process the new stuff
			for (x = 1; x <= (frmImportCompetitorXF.InstancePtr.GridNew.Rows - 1); x++)
			{
				// only add values for ones you will import
				if (FCConvert.CBool(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTUSEROW)))
				{
					if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE)) > 0)
					{
						lngNumNewLand += 1;
						lngTotLandChange += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE));
						lngTotLand += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE));
					}
					if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE)) > 0)
					{
						lngNumNewBldg += 1;
						lngTotBldgChange += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE));
						lngTotBldg += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE));
					}
					lngTotExempt += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT1)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT2)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT3));
					lngTotOtherValue += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTOTHERAMOUNT));
				}
				else
				{
					if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE)) > 0)
					{
						lngLandNotXfered += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE));
					}
					if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE)) > 0)
					{
						lngBldgNotXfered += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE));
					}
					lngExemptNotXfered += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT1)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT2)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridNew.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT3));
				}
			}
			// x
			// process the changed stuff
			for (x = 1; x <= (frmImportCompetitorXF.InstancePtr.GridGood.Rows - 1); x++)
			{
				lngNewLand = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDVALUE))));
				lngOldLand = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTORIGINALLAND))));
				lngNewBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTBLDGVALUE))));
				lngOldBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTORIGINALBLDG))));
				lngNewExempt = FCConvert.ToInt32(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT1)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT2)) + Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTEXEMPTAMOUNT3)));
				lngOldExempt = FCConvert.ToInt32(Math.Round(Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTORIGINALEXEMPTION))));
				lngTotOtherValue += Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTOTHERAMOUNT));
				lngTotLand += lngNewLand;
				lngTotBldg += lngNewBldg;
				lngTotExempt += lngNewExempt;
				if (lngNewLand != lngOldLand)
				{
					// land is different
					if (lngNewLand > lngOldLand)
					{
						lngLandChange = lngNewLand - lngOldLand;
						lngTotLandChange += lngLandChange;
						lngLandUps += 1;
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDEXCEED)) != 0 && boolUseExceeds)
						{
							lngLandExceedsUp += 1;
						}
					}
					else
					{
						lngLandChange = lngOldLand - lngNewLand;
						lngTotLandChange -= lngLandChange;
						lngLandDowns += 1;
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTLANDEXCEED)) != 0 && boolUseExceeds)
						{
							lngLandExceedsDown += 1;
						}
					}
				}
				if (lngNewBldg != lngOldBldg)
				{
					// bldg is different
					if (lngNewBldg > lngOldBldg)
					{
						lngBldgChange = lngNewBldg - lngOldBldg;
						lngTotBldgChange += lngBldgChange;
						lngBldgUps += 1;
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTBLDGEXCEED)) != 0 && boolUseExceeds)
						{
							lngBldgExceedsUp += 1;
						}
					}
					else
					{
						lngBldgChange = lngOldBldg - lngNewBldg;
						lngTotBldgChange -= lngBldgChange;
						lngBldgDowns += 1;
						if (Conversion.Val(frmImportCompetitorXF.InstancePtr.GridGood.TextMatrix(x, modGridConstantsXF.CNSTBLDGEXCEED)) != 0 && boolUseExceeds)
						{
							lngBldgExceedsDown += 1;
						}
					}
				}
				if (lngNewExempt != lngOldExempt)
				{
					// exempt is different
					if (lngNewExempt > lngOldExempt)
					{
						lngExemptionChange = lngNewExempt - lngOldExempt;
						lngTotExemptChange += lngExemptionChange;
						lngExemptUps += 1;
					}
					else
					{
						lngExemptionChange = lngOldExempt - lngNewExempt;
						lngTotExemptChange -= lngExemptionChange;
						lngExemptDowns += 1;
					}
				}
			}
			// x
			txtBadCodes.Text = Strings.Format(lngNumBadExempts, "#,###,###,##0");
			txtBadExemptVal.Text = Strings.Format(lngNumBadExemptAmounts, "#,###,###,##0");
			txtBadUpdated.Text = Strings.Format(lngNumUpdatedTooMany, "#,###,###,##0");
			txtBadMaps.Text = Strings.Format(lngNumBadMapLots, "#,###,###,##0");
			txtDeleted.Text = Strings.Format(lngNumDeleted, "#,###,###,##0");
			txtLandChgVal.Text = Strings.Format(lngTotLandChange, "#,###,###,##0");
			txtBldgChgVal.Text = Strings.Format(lngTotBldgChange, "#,###,###,##0");
			txtExemptChgVal.Text = Strings.Format(lngTotExemptChange, "#,###,###,##0");
			txtBldgDown.Text = Strings.Format(lngBldgDowns, "#,###,###,##0");
			txtBldgUp.Text = Strings.Format(lngBldgUps, "#,###,###,##0");
			txtLandDown.Text = Strings.Format(lngLandDowns, "#,###,###,##0");
			txtLandUp.Text = Strings.Format(lngLandUps, "#,###,###,##0");
			txtExemptUp.Text = Strings.Format(lngExemptUps, "#,###,###,##0");
			txtExemptDown.Text = Strings.Format(lngExemptDowns, "#,###,###,##0");
			txtTotalBuilding.Text = Strings.Format(lngTotBldg, "#,###,###,##0");
			txtTotalLand.Text = Strings.Format(lngTotLand, "#,###,###,##0");
			txtTotalExempt.Text = Strings.Format(lngTotExempt, "#,###,###,##0");
			lngTotAssess = lngTotLand + lngTotBldg - lngTotExempt;
			txtTotalNet.Text = Strings.Format(lngTotAssess, "#,###,###,##0");
			txtOtherAmount.Text = Strings.Format(lngTotOtherValue, "#,###,###,##0");
			txtNewLand.Text = Strings.Format(lngNumNewLand, "#,###,###,##0");
			txtNewBldg.Text = Strings.Format(lngNumNewBldg, "#,###,###,##0");
			txtBldgPercUp.Text = Strings.Format(lngBldgExceedsUp, "#,###,###,##0");
			txtBldgPercDown.Text = Strings.Format(lngBldgExceedsDown, "#,###,###,##0");
			txtLandPercUp.Text = Strings.Format(lngLandExceedsUp, "#,###,###,##0");
			txtLandPercDown.Text = Strings.Format(lngLandExceedsDown, "#,###,###,##0");
			txtlandNotUpdated.Text = Strings.Format(lngTotLand - lngTotLandNotUpdated, "#,###,###,##0");
			txtBldgNotUpdated.Text = Strings.Format(lngTotBldg - lngTotBldgNotUpdated, "#,###,###,##0");
			txtExemptNotUpdated.Text = Strings.Format(lngTotExempt - lngTotExemptNotUpdated, "#,###,###,##0");
			txtTotalNetNotUpdated.Text = Strings.Format(lngTotAssess - lngTotLandNotUpdated - lngTotBldgNotUpdated + lngTotExemptNotUpdated, "#,###,###,##0");
			txtLandNotXfered.Text = Strings.Format(lngLandNotXfered, "#,###,###,##0");
			txtBldgNotXfered.Text = Strings.Format(lngBldgNotXfered, "#,###,###,##0");
			txtExemptNotXfered.Text = Strings.Format(lngExemptNotXfered, "#,###,###,##0");
			lngNetNotXfered = lngLandNotXfered + lngBldgNotXfered - lngExemptNotXfered;
			txtNetNotXfered.Text = Strings.Format(lngNetNotXfered, "#,###,###,##0");
		}

		public void Init(bool boolExceeds)
		{
			boolUseExceeds = boolExceeds;
			frmReportViewer.InstancePtr.Init(rptObj: this, boolAllowEmail: true, strAttachmentName: "RESummary");
		}

		
	}
}
