﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptUpdatedAccounts.
	/// </summary>
	partial class rptUpdatedAccounts
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptUpdatedAccounts));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniname = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExceeds = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOldLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOldBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOldExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOldAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLandExceed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldgExceed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalOldLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOldBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOldExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalOldAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExceeds)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandExceed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgExceed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBuilding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOldLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOldBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOldExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOldAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMapLot,
				this.txtName,
				this.txtOldLand,
				this.txtOldBuilding,
				this.txtOldExempt,
				this.txtOldAssessment,
				this.txtLand,
				this.txtBuilding,
				this.txtExempt,
				this.txtAssessment,
				this.Label6,
				this.Label8,
				this.Label9,
				this.Label10,
				this.txtLandExceed,
				this.txtBldgExceed
			});
			this.Detail.Height = 0.875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label7,
				this.txtTotal,
				this.txtTotalLand,
				this.txtTotalBuilding,
				this.txtTotalExempt,
				this.txtTotalAssessment,
				this.Label11,
				this.txtTotalOldLand,
				this.txtTotalOldBldg,
				this.txtTotalOldExempt,
				this.txtTotalOldAssessment,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17
			});
			this.ReportFooter.Height = 1.125F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.lblMuniname,
				this.lblTime,
				this.lblDate,
				this.lblPage,
				this.Label2,
				this.Label3,
				this.Label12,
				this.Label13,
				this.lblExceeds
			});
			this.PageHeader.Height = 0.8125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Updated Accounts";
			this.Label1.Top = 0F;
			this.Label1.Width = 2.5F;
			// 
			// lblMuniname
			// 
			this.lblMuniname.Height = 0.1875F;
			this.lblMuniname.HyperLink = null;
			this.lblMuniname.Left = 0F;
			this.lblMuniname.Name = "lblMuniname";
			this.lblMuniname.Style = "";
			this.lblMuniname.Text = null;
			this.lblMuniname.Top = 0F;
			this.lblMuniname.Width = 2.4375F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 2F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.5625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.875F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.5625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Map Lot";
			this.Label2.Top = 0.5625F;
			this.Label2.Width = 0.6875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Name";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 0.6875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.6875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: right";
			this.Label12.Text = "Original";
			this.Label12.Top = 0.5625F;
			this.Label12.Width = 0.75F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold; text-align: right";
			this.Label13.Text = "New";
			this.Label13.Top = 0.5625F;
			this.Label13.Width = 0.75F;
			// 
			// lblExceeds
			// 
			this.lblExceeds.Height = 0.375F;
			this.lblExceeds.HyperLink = null;
			this.lblExceeds.Left = 6.6875F;
			this.lblExceeds.Name = "lblExceeds";
			this.lblExceeds.Style = "font-weight: bold; text-align: center";
			this.lblExceeds.Text = "Exceeds %";
			this.lblExceeds.Top = 0.4375F;
			this.lblExceeds.Width = 0.75F;
			// 
			// txtMapLot
			// 
			this.txtMapLot.CanGrow = false;
			this.txtMapLot.Height = 0.1875F;
			this.txtMapLot.Left = 0F;
			this.txtMapLot.Name = "txtMapLot";
			this.txtMapLot.Text = null;
			this.txtMapLot.Top = 0F;
			this.txtMapLot.Width = 1.125F;
			// 
			// txtName
			// 
			this.txtName.CanGrow = false;
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 1.1875F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 2.25F;
			// 
			// txtOldLand
			// 
			this.txtOldLand.Height = 0.1875F;
			this.txtOldLand.Left = 4.3125F;
			this.txtOldLand.Name = "txtOldLand";
			this.txtOldLand.Style = "text-align: right";
			this.txtOldLand.Text = null;
			this.txtOldLand.Top = 0F;
			this.txtOldLand.Width = 1.125F;
			// 
			// txtOldBuilding
			// 
			this.txtOldBuilding.Height = 0.1875F;
			this.txtOldBuilding.Left = 4.3125F;
			this.txtOldBuilding.Name = "txtOldBuilding";
			this.txtOldBuilding.Style = "text-align: right";
			this.txtOldBuilding.Text = null;
			this.txtOldBuilding.Top = 0.1875F;
			this.txtOldBuilding.Width = 1.125F;
			// 
			// txtOldExempt
			// 
			this.txtOldExempt.Height = 0.1875F;
			this.txtOldExempt.Left = 4.3125F;
			this.txtOldExempt.Name = "txtOldExempt";
			this.txtOldExempt.Style = "text-align: right";
			this.txtOldExempt.Text = null;
			this.txtOldExempt.Top = 0.375F;
			this.txtOldExempt.Width = 1.125F;
			// 
			// txtOldAssessment
			// 
			this.txtOldAssessment.Height = 0.1875F;
			this.txtOldAssessment.Left = 4.3125F;
			this.txtOldAssessment.Name = "txtOldAssessment";
			this.txtOldAssessment.Style = "text-align: right";
			this.txtOldAssessment.Text = null;
			this.txtOldAssessment.Top = 0.5625F;
			this.txtOldAssessment.Width = 1.125F;
			// 
			// txtLand
			// 
			this.txtLand.Height = 0.1875F;
			this.txtLand.Left = 5.5F;
			this.txtLand.Name = "txtLand";
			this.txtLand.Style = "text-align: right";
			this.txtLand.Text = null;
			this.txtLand.Top = 0F;
			this.txtLand.Width = 1.125F;
			// 
			// txtBuilding
			// 
			this.txtBuilding.Height = 0.1875F;
			this.txtBuilding.Left = 5.5F;
			this.txtBuilding.Name = "txtBuilding";
			this.txtBuilding.Style = "text-align: right";
			this.txtBuilding.Text = null;
			this.txtBuilding.Top = 0.1875F;
			this.txtBuilding.Width = 1.125F;
			// 
			// txtExempt
			// 
			this.txtExempt.Height = 0.1875F;
			this.txtExempt.Left = 5.5F;
			this.txtExempt.Name = "txtExempt";
			this.txtExempt.Style = "text-align: right";
			this.txtExempt.Text = null;
			this.txtExempt.Top = 0.375F;
			this.txtExempt.Width = 1.125F;
			// 
			// txtAssessment
			// 
			this.txtAssessment.Height = 0.1875F;
			this.txtAssessment.Left = 5.5F;
			this.txtAssessment.Name = "txtAssessment";
			this.txtAssessment.Style = "text-align: right";
			this.txtAssessment.Text = null;
			this.txtAssessment.Top = 0.5625F;
			this.txtAssessment.Width = 1.125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 3.5F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: right";
			this.Label6.Text = "Land";
			this.Label6.Top = 0F;
			this.Label6.Width = 0.75F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 3.5F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold; text-align: right";
			this.Label8.Text = "Building";
			this.Label8.Top = 0.1875F;
			this.Label8.Width = 0.75F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.5F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold; text-align: right";
			this.Label9.Text = "Exempt";
			this.Label9.Top = 0.375F;
			this.Label9.Width = 0.75F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.3125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold; text-align: right";
			this.Label10.Text = "Assessment";
			this.Label10.Top = 0.5625F;
			this.Label10.Width = 0.9375F;
			// 
			// txtLandExceed
			// 
			this.txtLandExceed.Height = 0.1875F;
			this.txtLandExceed.Left = 6.8125F;
			this.txtLandExceed.Name = "txtLandExceed";
			this.txtLandExceed.Style = "text-align: center";
			this.txtLandExceed.Text = null;
			this.txtLandExceed.Top = 0F;
			this.txtLandExceed.Width = 0.5F;
			// 
			// txtBldgExceed
			// 
			this.txtBldgExceed.Height = 0.1875F;
			this.txtBldgExceed.Left = 6.8125F;
			this.txtBldgExceed.Name = "txtBldgExceed";
			this.txtBldgExceed.Style = "text-align: center";
			this.txtBldgExceed.Text = null;
			this.txtBldgExceed.Top = 0.1875F;
			this.txtBldgExceed.Width = 0.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Updated Accounts:";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.4375F;
			// 
			// txtTotal
			// 
			this.txtTotal.CanGrow = false;
			this.txtTotal.Height = 0.1875F;
			this.txtTotal.Left = 1.5F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0.1875F;
			this.txtTotal.Width = 1F;
			// 
			// txtTotalLand
			// 
			this.txtTotalLand.Height = 0.1875F;
			this.txtTotalLand.Left = 5.5F;
			this.txtTotalLand.Name = "txtTotalLand";
			this.txtTotalLand.Style = "text-align: right";
			this.txtTotalLand.Text = null;
			this.txtTotalLand.Top = 0.375F;
			this.txtTotalLand.Width = 1.125F;
			// 
			// txtTotalBuilding
			// 
			this.txtTotalBuilding.Height = 0.1875F;
			this.txtTotalBuilding.Left = 5.5F;
			this.txtTotalBuilding.Name = "txtTotalBuilding";
			this.txtTotalBuilding.Style = "text-align: right";
			this.txtTotalBuilding.Text = null;
			this.txtTotalBuilding.Top = 0.5625F;
			this.txtTotalBuilding.Width = 1.125F;
			// 
			// txtTotalExempt
			// 
			this.txtTotalExempt.Height = 0.1875F;
			this.txtTotalExempt.Left = 5.5F;
			this.txtTotalExempt.Name = "txtTotalExempt";
			this.txtTotalExempt.Style = "text-align: right";
			this.txtTotalExempt.Text = null;
			this.txtTotalExempt.Top = 0.75F;
			this.txtTotalExempt.Width = 1.125F;
			// 
			// txtTotalAssessment
			// 
			this.txtTotalAssessment.Height = 0.1875F;
			this.txtTotalAssessment.Left = 5.5F;
			this.txtTotalAssessment.Name = "txtTotalAssessment";
			this.txtTotalAssessment.Style = "text-align: right";
			this.txtTotalAssessment.Text = null;
			this.txtTotalAssessment.Top = 0.9375F;
			this.txtTotalAssessment.Width = 1.125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.5625F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold; text-align: right";
			this.Label11.Text = "Total:";
			this.Label11.Top = 0.1875F;
			this.Label11.Width = 0.75F;
			// 
			// txtTotalOldLand
			// 
			this.txtTotalOldLand.Height = 0.1875F;
			this.txtTotalOldLand.Left = 4.3125F;
			this.txtTotalOldLand.Name = "txtTotalOldLand";
			this.txtTotalOldLand.Style = "text-align: right";
			this.txtTotalOldLand.Text = null;
			this.txtTotalOldLand.Top = 0.375F;
			this.txtTotalOldLand.Width = 1.125F;
			// 
			// txtTotalOldBldg
			// 
			this.txtTotalOldBldg.Height = 0.1875F;
			this.txtTotalOldBldg.Left = 4.3125F;
			this.txtTotalOldBldg.Name = "txtTotalOldBldg";
			this.txtTotalOldBldg.Style = "text-align: right";
			this.txtTotalOldBldg.Text = null;
			this.txtTotalOldBldg.Top = 0.5625F;
			this.txtTotalOldBldg.Width = 1.125F;
			// 
			// txtTotalOldExempt
			// 
			this.txtTotalOldExempt.Height = 0.1875F;
			this.txtTotalOldExempt.Left = 4.3125F;
			this.txtTotalOldExempt.Name = "txtTotalOldExempt";
			this.txtTotalOldExempt.Style = "text-align: right";
			this.txtTotalOldExempt.Text = null;
			this.txtTotalOldExempt.Top = 0.75F;
			this.txtTotalOldExempt.Width = 1.125F;
			// 
			// txtTotalOldAssessment
			// 
			this.txtTotalOldAssessment.Height = 0.1875F;
			this.txtTotalOldAssessment.Left = 4.3125F;
			this.txtTotalOldAssessment.Name = "txtTotalOldAssessment";
			this.txtTotalOldAssessment.Style = "text-align: right";
			this.txtTotalOldAssessment.Text = null;
			this.txtTotalOldAssessment.Top = 0.9375F;
			this.txtTotalOldAssessment.Width = 1.125F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.5F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: right";
			this.Label14.Text = "Land";
			this.Label14.Top = 0.375F;
			this.Label14.Width = 0.75F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.5F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: right";
			this.Label15.Text = "Building";
			this.Label15.Top = 0.5625F;
			this.Label15.Width = 0.75F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.5F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold; text-align: right";
			this.Label16.Text = "Exempt";
			this.Label16.Top = 0.75F;
			this.Label16.Width = 0.75F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.1875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold; text-align: right";
			this.Label17.Text = "Assessment";
			this.Label17.Top = 0.9375F;
			this.Label17.Width = 1.0625F;
			// 
			// rptUpdatedAccounts
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExceeds)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOldAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLandExceed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldgExceed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalBuilding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOldLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOldBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOldExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalOldAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOldLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOldBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOldExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOldAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLandExceed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldgExceed;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalBuilding;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOldLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOldBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOldExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalOldAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExceeds;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
