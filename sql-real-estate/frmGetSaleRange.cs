﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetSaleRange.
	/// </summary>
	public partial class frmGetSaleRange : BaseForm
	{
		public frmGetSaleRange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetSaleRange InstancePtr
		{
			get
			{
				return (frmGetSaleRange)Sys.GetInstance(typeof(frmGetSaleRange));
			}
		}

		protected frmGetSaleRange _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private bool boolFromPreviousOwner;
		private string strReturn = string.Empty;

		public string Init()
		{
			string Init = "";
			strReturn = "";
			Init = "";
			boolFromPreviousOwner = true;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strReturn;
			return Init;
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			int intMonth1;
			int intMonth2 = 0;
			int intYear1;
			int intYear2 = 0;
			string strDate1String;
			string strDate2String;
			if (cmbRange.Text != "Month and Year" && cmbRange.Text != "Year" && cmbRange.Text != "Multi-Year")
			{
				MessageBox.Show("You must choose a search option", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (txtBegin.Text == string.Empty)
			{
				MessageBox.Show("You have not specified a range", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (txtEnd.Text == string.Empty && cmbRange.Text != "Year")
			{
				MessageBox.Show("You have not specified an end range", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if ((Conversion.Val(txtBegin.Text) == 0) || (Conversion.Val(txtEnd.Text) == 0) && cmbRange.Text != "Year")
			{
				MessageBox.Show("Invalid range. Please make sure that the dates are valid", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			intMonth1 = FCConvert.ToDateTime(txtBegin.Text).Month;
			intYear1 = FCConvert.ToDateTime(txtBegin.Text).Year;
			if (cmbRange.Text != "Year")
			{
				intMonth2 = FCConvert.ToDateTime(txtEnd.Text).Month;
				intYear2 = FCConvert.ToDateTime(txtEnd.Text).Year;
			}
			if (intMonth2 == 12)
			{
				intMonth2 = 1;
				intYear2 += 1;
			}
			else
			{
				intMonth2 += 1;
			}
			strDate1String = FCConvert.ToString(intMonth1) + "/1/" + FCConvert.ToString(intYear1);
			strDate2String = FCConvert.ToString(intMonth2) + "/1/" + FCConvert.ToString(intYear2);
			if (!boolFromPreviousOwner)
			{
				if (cmbRange.Text == "Month and Year")
				{
					frmRESearch.InstancePtr.lblSearch.Text = "Sale date between " + strDate1String + " and " + strDate2String;
					modGlobalVariables.Statics.searchRS.OpenRecordset("Select * from srmaster where saledate >= '" + FCConvert.ToString(DateAndTime.DateValue(strDate1String)) + "' and saledate < '" + FCConvert.ToString(DateAndTime.DateValue(strDate2String)) + "' order by saledate", modGlobalVariables.strREDatabase);
					this.Unload();
				}
				else if (cmbRange.Text == "Year")
				{
					strDate1String = "1/1/" + txtBegin.Text;
					strDate2String = "12/31/" + txtBegin.Text;
					frmRESearch.InstancePtr.lblSearch.Text = "Sale date between " + strDate1String + " and " + strDate2String;
					modGlobalVariables.Statics.searchRS.OpenRecordset("Select * from srmaster where saledate between '" + FCConvert.ToString(DateAndTime.DateValue(strDate1String)) + "' and '" + FCConvert.ToString(DateAndTime.DateValue(strDate2String)) + "' order by saledate", modGlobalVariables.strREDatabase);
					this.Unload();
				}
				else if (cmbRange.Text == "Multi-Year")
				{
					strDate1String = "1/1/" + txtBegin.Text;
					strDate2String = "12/31/" + txtEnd.Text;
					frmRESearch.InstancePtr.lblSearch.Text = "Sale date between " + strDate1String + " and " + strDate2String;
					modGlobalVariables.Statics.searchRS.OpenRecordset("Select * from srmaster where saledate between '" + strDate1String + "' and '" + strDate2String + "' order by saledate", modGlobalVariables.strREDatabase);
					this.Unload();
				}
			}
			else
			{
				boolFromPreviousOwner = false;
				if (cmbRange.Text == "Month and Year")
				{
					frmRESearch.InstancePtr.lblSearch.Text = "Sale date between " + strDate1String + " and " + strDate2String;
					strReturn = "select * from previousowner where saledate >= '" + FCConvert.ToString(DateAndTime.DateValue(strDate1String)) + "' and saledate < '" + FCConvert.ToString(DateAndTime.DateValue(strDate2String)) + "' order by saledate,name";
					this.Unload();
				}
				else if (cmbRange.Text == "Year")
				{
					strDate1String = "1/1/" + txtBegin.Text;
					strDate2String = "12/31/" + txtBegin.Text;
					frmRESearch.InstancePtr.lblSearch.Text = "Sale date between " + strDate1String + " and " + strDate2String;
					strReturn = "select * from previousowner where saledate between '" + FCConvert.ToString(DateAndTime.DateValue(strDate1String)) + "' and '" + FCConvert.ToString(DateAndTime.DateValue(strDate2String)) + "' order by saledate,name";
					this.Unload();
				}
				else
				{
					strDate1String = "1/1/" + txtBegin.Text;
					strDate2String = "12/31/" + txtEnd.Text;
					frmRESearch.InstancePtr.lblSearch.Text = "Sale date between " + strDate1String + " and " + strDate2String;
					strReturn = "Select * from Previousowner where saledate between '" + strDate1String + "' and '" + strDate2String + "' order by saledate,name";
					this.Unload();
				}
			}
		}

		private void frmGetSaleRange_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetSaleRange properties;
			//frmGetSaleRange.ScaleWidth	= 3885;
			//frmGetSaleRange.ScaleHeight	= 2505;
			//frmGetSaleRange.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
            //FC:FINAL:BSE #3302 set default value
            cmbRange.SelectedIndex = 0;
		}

		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (cmbRange.Text == "Month and Year")
			{
				txtBegin.Text = "Month/Year";
				txtEnd.Visible = true;
				txtEnd.Text = "Month/Year";
				// lblMonthYear.Visible = True
			}
			else if (cmbRange.Text == "Year")
			{
				// lblMonthYear.Visible = False
				txtBegin.Text = "Year";
				txtEnd.Visible = false;
			}
			else if (cmbRange.Text == "Multi-Year")
			{
				// lblMonthYear.Visible = False
				txtBegin.Text = "Year";
				txtEnd.Visible = true;
				txtEnd.Text = "Year";
			}
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}

		private void txtBegin_Enter(object sender, System.EventArgs e)
		{
			if (txtBegin.Text != string.Empty)
			{
				txtBegin.SelectionStart = 0;
				txtBegin.SelectionLength = txtBegin.Text.Length;
			}
		}

		private void txtEnd_Enter(object sender, System.EventArgs e)
		{
			if (txtEnd.Text != string.Empty)
			{
				txtEnd.SelectionStart = 0;
				txtEnd.SelectionLength = txtEnd.Text.Length;
			}
		}
	}
}
