﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Text;
using System.Drawing;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Extensions;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmSHREBLUpdate.
	/// </summary>
	public partial class frmSHREBLUpdate : BaseForm
	{
		public frmSHREBLUpdate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label8 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label8.AddControlArrayElement(Label8_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;

            this.txtMRRLLANDVAL.AllowOnlyNumericInput();
            this.txtMRRLBLDGVAL.AllowOnlyNumericInput();
            this.txtMRRLEXEMPTION.AllowOnlyNumericInput();
        }
        //FC:FINAL:RPU: #i1364 - Fix design differently depending on framSaleRecords visibility
        private void FixDesign()
		{
			//if (modGlobalVariables.Statics.boolRegRecords)
			//{
			//	//framSaleRecord is not visible
			//	fcPanel2.Location = new Point(fcPanel2.Location.X, lblAddress.Location.Y + lblAddress.Height + 10);
			//	SaleGrid.Location = new Point(SaleGrid.Location.X, MapLotGrid.Location.Y + MapLotGrid.Height + 10);
			//	TreeGrid.Location = new Point(TreeGrid.Location.X, SaleGrid.Location.Y + SaleGrid.Height + 10);
			//	Frame1.Location = new Point(Frame1.Location.X, SaleGrid.Location.Y + SaleGrid.Height + 10);
			//	Label31.Location = new Point(Label31.Location.X, TreeGrid.Location.Y + TreeGrid.Height + 20);
			//	lblMRHLUPDATE.Location = new Point(lblMRHLUPDATE.Location.X, TreeGrid.Location.Y + TreeGrid.Height + 20);
			//}
			//else
			//{
			//	fcPanel2.Location = new Point(fcPanel2.Location.X, framSaleRecord.Location.Y + framSaleRecord.Height + 10);
			//	SaleGrid.Location = new Point(SaleGrid.Location.X, fcPanel2.Location.Y + fcPanel2.Height + 10);
			//	TreeGrid.Location = new Point(TreeGrid.Location.X, SaleGrid.Location.Y + SaleGrid.Height + 10);
			//	Frame1.Location = new Point(Frame1.Location.X, SaleGrid.Location.Y + SaleGrid.Height + 10);
			//	Label31.Location = new Point(Label31.Location.X, TreeGrid.Location.Y + TreeGrid.Height + 20);
			//	lblMRHLUPDATE.Location = new Point(lblMRHLUPDATE.Location.X, TreeGrid.Location.Y + TreeGrid.Height + 20);
			//}
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSHREBLUpdate InstancePtr
		{
			get
			{
				return (frmSHREBLUpdate)Sys.GetInstance(typeof(frmSHREBLUpdate));
			}
		}

		protected frmSHREBLUpdate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// vbPorter upgrade warning: FormLoaded As FixedString	OnWrite(string)
		char FormLoaded;
		string LocStreet;
		string InNameField;
		// vbPorter upgrade warning: ShowPicture As FixedString	OnWrite(string)
		char[] ShowPicture = new char[1];
		private cREAccount theAccount = new cREAccount();
		// Dim strOriginalName As String
		int lngOriginalOwnerID;
		int lngPOOwnerID;
		int lngPOSecOwnerID;
		string strPOName = "";
		string strPOSecOwner = "";
		string strPOAddress1 = "";
		string strPOAddress2 = "";
		string strPOCity = "";
		string strPOState = "";
		string strPOZip = "";
		string strPOzip4 = "";
		string dtPODate = "";
		bool boolShown;
		// vbPorter upgrade warning: dtMinPendingDate As DateTime	OnWrite(string)
		public DateTime dtMinPendingDate;
		int lngOrigExemptions;
		int lngCurrentPendingID;
		bool boolBookPageChanged;
		bool boolNameChanged;
		bool boolMapLotGridChanged;
		bool boolSaleGridChanged;
		bool boolSaveSaleRecord;
		bool boolGridPhonesChanged;
		bool boolInValidate;
		bool booleditland;
		int[] lngExVal = new int[3 + 1];
		string strAccountID = "";
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		const int BPBookCol = 1;
		const int BPPageCol = 2;
		const int BPSaleDateCol = 3;
		const int BPCheckCol = 0;
		const int CNSTMAPLOTCOLMAP = 0;
		const int CNSTMAPLOTCOLMPDATE = 1;
		const int CNSTMAPLOTCOLID = 2;
		const int CNSTMAPLOTCOLLINE = 3;
		const int CNSTNOEXEMPT = -1;
		const int CNSTEXEMPTCOLCODE = 0;
		const int CNSTEXEMPTCOLPERCENT = 1;
		const int CNSTSALECOLPRICE = 0;
		const int CNSTSALECOLDATE = 1;
		const int CNSTSALECOLSALE = 2;
		const int CNSTSALECOLFINANCING = 3;
		const int CNSTSALECOLVERIFIED = 4;
		const int CNSTSALECOLVALIDITY = 5;
		private bool boolCardChangedByProg;
        private bool inCellFormatting = false;

		private void SaveMisc()
		{
			modDataTypes.Statics.MR.Set_Fields("ritrancode", FCConvert.ToString(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
		}

		private void cmdSearchOwner_Click(object sender, System.EventArgs e)
		{
			try
			{
				int lngReturnID;
				lngReturnID = frmCentralPartySearch.InstancePtr.Init();
				if (lngReturnID != theAccount.OwnerID && lngReturnID > 0)
				{
					HandleOwnerChange(lngReturnID);
                }
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SearchOwner", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void HandleOwnerChange(int ownerPartyId)
		{
			cPartyController tCont = new cPartyController();
			int lngPrice = 0;
			int intType = 0;
			int intFinancing = 0;
			int intVerified = 0;
			int intValidity = 0;
			DateTime dtDate;
			clsDRWrapper clsGroup = new clsDRWrapper();
			bool updateDeed = false;

			try
			{
				if (!modGlobalVariables.Statics.boolInPendingMode)
				{
					if (lngOriginalOwnerID == 0)
					{
						updateDeed = true;
					}
					if (lngOriginalOwnerID > 0 && ownerPartyId != lngOriginalOwnerID)
					{
						int intReturn = 0;
						intReturn = frmOwnershipChangeReason.InstancePtr.Init();
						if (intReturn >= 0)
						{
							if (intReturn == 0)
							{
								boolNameChanged = true;
								clsGroup.OpenRecordset("select * from moduleassociation where module = 'RE' and account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "CentralData");
								if (!clsGroup.EndOfFile())
								{
									this.BringToFront();
									//Application.DoEvents();
									frmUpdateGroup.InstancePtr.Init(clsGroup.Get_Fields_Int32("groupnumber"), ref modGlobalVariables.Statics.gintLastAccountNumber);
								}
								//Application.DoEvents();
								string strTemp = "";
								cParty tParty = new cParty();
								tCont.LoadParty(ref tParty, lngPOOwnerID, true);
								strTemp = txtDeedName1.Text.Trim();
								if (txtDeedName2.Text.Trim() != "")
								{
									if (Strings.Trim(strTemp) != "" && lngPOSecOwnerID == theAccount.SecOwnerID)
									{
										strTemp = Strings.Mid(strTemp + ", " + txtDeedName2.Text.Trim(), 1, 50);
									}
								}
								txtPreviousOwner.Text = strTemp;
								theAccount.SecOwnerID = 0;
								theAccount.SecOwnerParty.Clear();
								updateDeed = true;
								theAccount.DeedName1 = "";
								theAccount.DeedName2 = "";
								intType = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLSALE))));
								intFinancing = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING))));
								intValidity = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY))));
								intVerified = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED))));
								if (Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)) > 0)
								{
									lngPrice = FCConvert.ToInt32(FCConvert.ToDouble(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)));
								}
								// 26                    dtDate = Format(Date, "MM/dd/yyyy")
								dtDate = DateTime.FromOADate(0);
								if (frmGetSaleInfo.InstancePtr.Init(true, ref lngPrice, ref dtDate, ref intType, ref intFinancing, ref intVerified, ref intValidity, "Change of Ownership"))
								{
									boolSaveSaleRecord = true;
									boolSaleGridChanged = true;
									SaleGrid.TextMatrix(2, CNSTSALECOLDATE, Strings.Format(dtDate, "MM/dd/yyyy"));
									SaleGrid.TextMatrix(2, CNSTSALECOLPRICE, Strings.Format(lngPrice, "#,###,###,##0"));
									SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY, FCConvert.ToString(intValidity));
									SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED, FCConvert.ToString(intVerified));
									SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING, FCConvert.ToString(intFinancing));
									SaleGrid.TextMatrix(2, CNSTSALECOLSALE, FCConvert.ToString(intType));
									dtPODate = dtDate.FormatAndPadShortDate();
								}
								ShowSecOwner();
							}
						}
						else
						{
							return;
						}
					}
				}

				cParty tParty2 = theAccount.OwnerParty;
				tCont.LoadParty(ref tParty2, ownerPartyId);
				theAccount.OwnerID = ownerPartyId;
				if (updateDeed)
				{
					theAccount.DeedName1 = theAccount.OwnerParty.FullNameLastFirst;
				}
				modDataTypes.Statics.MR.Set_Fields("Ownerpartyid", theAccount.OwnerID);
				ShowOwner();
				ShowAddress();
				ShowDeeds();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SearchOwner", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void gridPropertyCode_ComboCloseUp(object sender, EventArgs e)
		{
			gridPropertyCode.Row = -1;
			//Application.DoEvents();
		}

		private void gridPropertyCode_ComboDropDown(object sender, System.EventArgs e)
		{
			int x;
			int lngID;
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(gridPropertyCode.TextMatrix(0, 0))));
			if (lngID > 0)
			{
				for (x = 0; x <= gridPropertyCode.ComboCount - 1; x++)
				{
					if (Conversion.Val(gridPropertyCode.ComboData(x)) == lngID)
					{
						gridPropertyCode.ComboIndex = x;
					}
				}
				// x
			}
		}

		private void gridTranCode_ComboCloseUp(object sender, EventArgs e)
		{
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.ComboData())));
				CalculateValues();
			}
		}

		private void mnuCollectionsNote_Click(object sender, System.EventArgs e)
		{
			if (modGlobalVariables.Statics.gintLastAccountNumber > 0)
			{
				frmCLNote.InstancePtr.Init(ref modGlobalVariables.Statics.gintLastAccountNumber);
			}
		}

		private void mnuPrintInterestedParties_Click(object sender, System.EventArgs e)
		{
			rptInterestedParty.InstancePtr.Init(true, 1, modGlobalVariables.Statics.gintLastAccountNumber.ToString(), modGlobalVariables.Statics.gintLastAccountNumber.ToString(), false);
		}

		private void mnuValuationReport_Click(object sender, System.EventArgs e)
		{
			try
			{
				modProperty.Statics.RUNTYPE = "P";
				modGlobalVariables.Statics.boolUseArrays = true;
				mnuValuationReport.Enabled = false;

				cmdValuationReport.Enabled = false;
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
				}
				modGlobalRoutines.Check_Arrays();

				modGlobalVariables.Statics.boolUpdateWhenCalculate = false;
				modGlobalVariables.Statics.gintMinAccountRange = modGlobalVariables.Statics.gintLastAccountNumber;
				modGlobalVariables.Statics.gintMaxAccountRange = modGlobalVariables.Statics.gintLastAccountNumber;
				modPrintRoutines.Statics.gstrFieldName = "RSAccount";
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;

                rptRangeValuation.InstancePtr.Init(1, 1, true, -1, true, reportClosed: () => 
                {
                    mnuValuationReport.Enabled = true;
					cmdValuationReport.Enabled = true;
                    modGlobalVariables.Statics.DataChanged = false;
                    modGlobalVariables.Statics.intCurrentCard = 1;
                    modSpeedCalc.Statics.boolCalcErrors = false;
                    modSpeedCalc.Statics.CalcLog = "";
                });
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				mnuValuationReport.Enabled = true;
				cmdValuationReport.Enabled = true;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuValuationReport_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveSaleGrid()
		{
			if (modREMain.Statics.boolShortRealEstate || boolSaveSaleRecord)
			{
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngID = 0;
				int lngSalePrice = 0;
				int intValid = 0;
				int intVerify = 0;
				int intFinance = 0;
				int intSaleType = 0;
				lngSalePrice = 0;
				if (!Information.IsNumeric(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)))
				{
				}
				else
				{
					lngSalePrice = FCConvert.ToInt32(FCConvert.ToDouble(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)));
				}

				theAccount.SalePrice = lngSalePrice;
				//SaleGrid.TextMatrix(2, CNSTSALECOLPRICE, Strings.Format(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleprice") + "")), "#,###,###,##0"));
				if (Information.IsDate(SaleGrid.TextMatrix(2, CNSTSALECOLDATE)))
				{
					modGlobalVariables.Statics.gcurrsaledate = Strings.Format(SaleGrid.TextMatrix(2, CNSTSALECOLDATE), "MM/dd/yyyy");
					theAccount.SaleDate = FCConvert.ToDateTime(modGlobalVariables.Statics.gcurrsaledate);
				}
				else
				{
				}
				intFinance = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING))));
				intSaleType = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLSALE))));
				intValid = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY))));
				intVerify = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED))));
				theAccount.SaleType = intSaleType;
				theAccount.SaleValidity = intValid;
				theAccount.SaleVerified = intVerify;
				theAccount.SaleFinancing = intFinance;
				if (Information.IsDate(modGlobalVariables.Statics.gcurrsaledate))
				{
					theAccount.SaleDate = FCConvert.ToDateTime(modGlobalVariables.Statics.gcurrsaledate);
				}
				// Call SaveMasterTable(MR, MR.Fields("rsaccount"), MR.Fields("rscard"))
				if (boolSaveSaleRecord && !modREMain.Statics.boolShortRealEstate)
				{
					lngID = 0;
					// Call clsSave.OpenRecordset("select saleid from srmaster where rsaccount = " & gintLastAccountNumber & " and rscard = 1 and saledate = #" & gcurrsaledate & "# and ownerpartyid = '" & EscapeQuotes(txtMRRSName.Text) & "' and not rsdeleted = 1", strREDatabase)
					if (!clsSave.EndOfFile())
					{
						lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields_Int32("saleid"))));
					}
					if (lngID == 0)
					{
						modGlobalRoutines.SaveSaleRecord_2(false, modGlobalVariables.Statics.gintLastAccountNumber, lngID, modGlobalVariables.Statics.gcurrsaledate, lngSalePrice, intValid, intVerify, intSaleType, intFinance);
					}
					else
					{
						modGlobalRoutines.SaveSaleRecord_2(true, modGlobalVariables.Statics.gintLastAccountNumber, lngID, modGlobalVariables.Statics.gcurrsaledate, lngSalePrice, intValid, intVerify, intSaleType, intFinance);
					}
				}
			}
			boolSaleGridChanged = false;
			boolSaveSaleRecord = false;
		}

		private void SaveMapLotGrid()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strSQL;
			int x;
			// vbPorter upgrade warning: strSale As string	OnWrite(string, short)
			string strSale = "";
			string strTable = "";
			try
			{
				// On Error GoTo ErrorHandler
				// get rid of bad rows
				for (x = 1; x <= MapLotGrid.Rows - 1; x++)
				{
					if (x >= MapLotGrid.Rows)
						break;
					if (Strings.Trim(MapLotGrid.TextMatrix(x, CNSTMAPLOTCOLMAP)) == string.Empty)
					{
						MapLotGrid.RemoveItem(x);
					}
				}
				// x
				if (MapLotGrid.Rows > 1)
				{
					if (Strings.Trim(MapLotGrid.TextMatrix(MapLotGrid.Rows - 1, CNSTMAPLOTCOLMAP)) == string.Empty)
						MapLotGrid.RemoveItem(MapLotGrid.Rows - 1);
				}
				// now put the blank line back
				MapLotGrid.AddItem("");
				strSQL = "Delete from ";
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					strSQL += "maplot";
					strTable = "MapLot";
				}
				else
				{
					strSQL += "SRMapLot";
					strTable = "SRMapLot";
				}
				strSQL += " where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
				if (!modGlobalVariables.Statics.boolRegRecords)
				{
					strSQL += " and saledate = #" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "#";
				}
				clsTemp.Execute(strSQL, modGlobalVariables.strREDatabase);
				if (!fecherFoundation.FCUtils.IsEmptyDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate")))
				{
					//FC:FINAL:MSH - i.issue #1026: incorrect converting from datetime to int
					//if (FCConvert.ToInt32(modDataTypes.MR.Get_Fields("saledate")) != 0)
					if (FCConvert.ToDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") as object).ToOADate() != 0)
					{
						strSale = modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "";
					}
					else
					{
						strSale = FCConvert.ToString(0);
					}
				}
				else
				{
					strSale = FCConvert.ToString(0);
				}
				for (x = 1; x <= MapLotGrid.Rows - 2; x++)
				{
					strSQL = "";
					strSQL += "'" + MapLotGrid.TextMatrix(x, CNSTMAPLOTCOLMAP) + "'";
					strSQL += ",'" + MapLotGrid.TextMatrix(x, CNSTMAPLOTCOLMPDATE) + "'";
					strSQL += "," + FCConvert.ToString(x);
					strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
					if (strSale == "0")
					{
						strSQL += ",0";
					}
					else
					{
						strSQL += ",'" + strSale + "'";
					}
					clsTemp.Execute("insert into " + strTable + " (maplot,mpdate,line,account,saledate) values (" + strSQL + ")", modGlobalVariables.strREDatabase);
				}
				// x
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveMapLotGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveBPGrid()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strSQL;
			int x;
			// vbPorter upgrade warning: strSale As string	OnWrite(string, short)
			string strSale = "";
			string strTable = "";
			try
			{
				// On Error GoTo ErrorHandler
				for (x = 1; x <= BPGrid.Rows - 1; x++)
				{
					if (x >= BPGrid.Rows)
						break;
					if (Strings.Trim(BPGrid.TextMatrix(x, BPBookCol)) == "" || Strings.Trim(BPGrid.TextMatrix(x, BPPageCol)) == "")
					{
						modAuditReporting.RemoveGridRow_8("BPGrid", intTotalNumberOfControls - 1, x, clsControlInfo);
						BPGrid.RemoveItem(x);
					}
				}
				// x
				if (BPGrid.Rows > 1)
				{
					if (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) == "" || Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) == "")
					{
						modAuditReporting.RemoveGridRow_8("BPGrid", FCConvert.ToInt16(intTotalNumberOfControls - 1), FCConvert.ToInt16(BPGrid.Rows - 1), clsControlInfo);
						BPGrid.RemoveItem(BPGrid.Rows - 1);
					}
				}

				BPGrid.AddItem(FCConvert.ToString(true));
				strSQL = "Delete from ";
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					strSQL += "BookPage";
					strTable = "BookPage";
				}
				else
				{
					strSQL += "SRBookPage";
					strTable = "SRBookPage";
				}
				strSQL += " Where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = 1";
				if (!modGlobalVariables.Statics.boolRegRecords)
				{
					strSQL += " and saledate = '" + modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "'";
				}
				clsTemp.Execute(strSQL, modGlobalVariables.strREDatabase);
				if (!fecherFoundation.FCUtils.IsEmptyDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate")))
				{
					//FC:FINAL:MSH - i.issue #1026: incorrect converting from datetime to int
					//if (FCConvert.ToInt32(modDataTypes.MR.Get_Fields("saledate")) != 0)
					if (FCConvert.ToDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") as object).ToOADate() != 0)
					{
						strSale = modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "";
					}
					else
					{
						strSale = FCConvert.ToString(0);
					}
				}
				else
				{
					strSale = FCConvert.ToString(0);
				}
				for (x = 1; x <= BPGrid.Rows - 2; x++)
				{
					if (BPGrid.TextMatrix(x, BPCheckCol) == string.Empty)
					{
						//strSQL = "FALSE";
                        strSQL = "0";
                    }
					else
					{
						if (FCConvert.CBool(BPGrid.TextMatrix(x, BPCheckCol)) == true)
						{
							strSQL = "1";
						}
						else
						{
							strSQL = "0";
						}
					}
					strSQL += ",'" + BPGrid.TextMatrix(x, BPBookCol) + "'";
					strSQL += ",'" + BPGrid.TextMatrix(x, BPPageCol) + "'";
					strSQL += ",'" + Strings.Trim(BPGrid.TextMatrix(x, BPSaleDateCol)) + "'";
					strSQL += "," + FCConvert.ToString(x);
					strSQL += "," + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
					strSQL += ",1";
					if (strSale == "0")
					{
						strSQL += ",0";
					}
					else
					{
						strSQL += ",'" + strSale + "'";
					}
					clsTemp.Execute("insert into " + strTable + " ([current],book,page,bpdate,line,account,card,saledate) values (" + strSQL + ")", modGlobalVariables.strREDatabase);
				}
				// x
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "Occured in SaveBPGrid", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void chkBankruptcy_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkBankruptcy.CheckState == Wisej.Web.CheckState.Checked && !modGlobalConstants.Statics.gboolCL)
			{
				chkTaxAcquired.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void chkTaxAcquired_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkBankruptcy.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void cmdComment_Click()
		{
			try
			{
				frmFSComment.InstancePtr.Init(modGlobalVariables.CNSTSHORTSCREEN);
				return;
			}
			catch
			{
				// ErrorRoutine:
				modGlobalRoutines.ErrorRoutine();
			}
		}
		private void cmdNext_Click()
		{

			clsDRWrapper temp = modDataTypes.Statics.MR;
			modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.MR = temp;
			if (modGlobalVariables.Statics.boolRegRecords)
			{
				// strAccountID = MR.GUIDToString(MR.Fields("AccountID"))
				strAccountID = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("AccountID"));
			}
			temp = modDataTypes.Statics.DWL;
			modREMain.OpenDwellingTable_27(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strAccountID);
			modDataTypes.Statics.DWL = temp;
			temp = modDataTypes.Statics.CMR;
			modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.CMR = temp;
			temp = modDataTypes.Statics.OUT;
			modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.OUT = temp;
			if (modGlobalVariables.Statics.boolRegRecords)
			{
				REMRToText();
			}
			else
			{
				SaleMRToText();
			}
			FillTreeGrid();
			frmSHREBLUpdate.InstancePtr.Refresh();
			temp = modDataTypes.Statics.CR;
			modREMain.OpenCRTable(ref temp, 1330);
			Open1Label.Text = Strings.StrConv(temp.Get_Fields_String("ClDesc"), VbStrConv.ProperCase);
			modREMain.OpenCRTable(ref temp, 1340);
			modDataTypes.Statics.CR = temp;
			Open2Label.Text = Strings.StrConv(modDataTypes.Statics.CR.Get_Fields_String("ClDesc"), VbStrConv.ProperCase);
            CalculateValues();
			txtMRRSMAPLOT.Focus();
			// End If
		}

		private void ReShowCard()
		{
			clsDRWrapper temp = modDataTypes.Statics.MR;
			modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.MR = temp;
			if (modGlobalVariables.Statics.boolRegRecords)
			{
				strAccountID = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("AccountID"));
			}
			temp = modDataTypes.Statics.DWL;
			modREMain.OpenDwellingTable_27(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strAccountID);
			modDataTypes.Statics.DWL = temp;
			temp = modDataTypes.Statics.CMR;
			modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.CMR = temp;
			temp = modDataTypes.Statics.OUT;
			modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
			modDataTypes.Statics.OUT = temp;
			boolCardChangedByProg = true;
			REMRToText();
			FillTreeGrid();
			frmSHREBLUpdate.InstancePtr.Refresh();
			boolCardChangedByProg = false;
			temp = modDataTypes.Statics.CR;
			modREMain.OpenCRTable(ref temp, 1330);
			Open1Label.Text = Strings.StrConv(temp.Get_Fields_String("cldesc"), VbStrConv.ProperCase);
            modREMain.OpenCRTable(ref temp, 1340);
			modDataTypes.Statics.CR = temp;
			Open2Label.Text = Strings.StrConv(modDataTypes.Statics.CR.Get_Fields_String("cldesc"), VbStrConv.ProperCase);
            CalculateValues();
			txtMRRSMAPLOT.Focus();
		}

		private void cmdPrevious_Click()
		{
			if (modGNBas.Statics.gintCardNumber != 1)
			{
				modGNBas.Statics.gintCardNumber -= 1;
				clsDRWrapper temp = modDataTypes.Statics.MR;
				modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.MR = temp;
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					strAccountID = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("accountid"));
				}
				temp = modDataTypes.Statics.DWL;
				modREMain.OpenDwellingTable_27(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strAccountID);
				modDataTypes.Statics.DWL = temp;
				temp = modDataTypes.Statics.CMR;
				modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.CMR = temp;
				temp = modDataTypes.Statics.OUT;
				modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.OUT = temp;
				REMRToText();
				FillTreeGrid();
				frmSHREBLUpdate.InstancePtr.Refresh();
				temp = modDataTypes.Statics.CR;
				modREMain.OpenCRTable(ref temp, 1330);
				Open1Label.Text = Strings.StrConv(temp.Get_Fields_String("cldesc"), VbStrConv.ProperCase);
                modREMain.OpenCRTable(ref temp, 1340);
				modDataTypes.Statics.CR = temp;
				Open2Label.Text = Strings.StrConv(modDataTypes.Statics.CR.Get_Fields_String("cldesc"), VbStrConv.ProperCase);
                CalculateValues();
				txtMRRSMAPLOT.Focus();
			}
		}

		private void cmdQuit_Click()
		{
			// TODO Get_Fields: Field [hlacctnum] not found!! (maybe it is an alias?)
			if (modSHREBLUpdateMRTEXT.Statics.Adding == true && Conversion.Val(modDataTypes.Statics.MR.Get_Fields("hlacctnum") + " ") == 0)
				modSHREBLUpdateMRTEXT.Statics.Adding = false;
			frmSHREBLUpdate.InstancePtr.Unload();
		}

		private void cmdSave_Click()
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorHandler = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				clsDRWrapper clsTemp = new clsDRWrapper();
				int lngAutoID = 0;
				int x;
				if (!modGlobalVariables.Statics.boolFromBilling && modGlobalVariables.Statics.boolRegRecords && boolNameChanged)
				{
					boolNameChanged = false;
					clsTemp.OpenRecordset("select * from previousowner where id = -1", modGlobalVariables.strREDatabase);
					clsTemp.AddNew();
					clsTemp.Set_Fields("Account", modGlobalVariables.Statics.gintLastAccountNumber);
					clsTemp.Set_Fields("Name", strPOName);
					clsTemp.Set_Fields("secowner", strPOSecOwner);
					clsTemp.Set_Fields("Address1", strPOAddress1);
					clsTemp.Set_Fields("address2", strPOAddress2);
					clsTemp.Set_Fields("city", strPOCity);
					clsTemp.Set_Fields("state", strPOState);
					clsTemp.Set_Fields("zip", strPOZip);
					clsTemp.Set_Fields("Zip4", strPOzip4);
					clsTemp.Set_Fields("SALEDATE", dtPODate);
					clsTemp.Set_Fields("DateCreated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					clsTemp.Set_Fields("PartyID1", lngPOOwnerID);
					clsTemp.Set_Fields("PartyID2", lngPOSecOwnerID);
					clsTemp.Update();
					lngAutoID = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("id"));
					clsTemp.Execute("update owners set associd = " + FCConvert.ToString(lngAutoID) + " where associd = 0 and account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
				}
				TreeGrid.Row = 0;
				BPGrid.Row = 0;
				MapLotGrid.Row = 0;
				GridExempts.Row = -1;

				CheckValues();
				SaveMisc();
				SaveBPGrid();
				SaveMapLotGrid();

				if (modGlobalVariables.Statics.boolRegRecords)
				{
					modSHREBLUpdateMRTEXT.TextToREMR();
					if (modGNBas.Statics.gintCardNumber == 1)
					{
						for (x = 1; x <= 3; x++)
						{
							if (Conversion.Val(GridExempts.TextMatrix(x, 0)) >= 0)
							{
								theAccount.Set_ExemptCode(x, FCConvert.ToInt32(Math.Round(Conversion.Val(GridExempts.TextMatrix(x, 0)))));
								theAccount.Set_ExemptPct(x, FCConvert.ToInt32(Math.Round(Conversion.Val(GridExempts.TextMatrix(x, 1)))));
								theAccount.Set_ExemptVal(x, lngExVal[x]);
							}
							else
							{
								theAccount.Set_ExemptCode(x, 0);
								theAccount.Set_ExemptPct(x, 100);
								theAccount.Set_ExemptVal(x, 0);
							}
						}
						// x
					}
					theAccount.MapLot = txtMRRSMAPLOT.Text;
					theAccount.RevocableTrust = chkRLivingTrust.CheckState == Wisej.Web.CheckState.Checked;
					theAccount.TaxAcquired = chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked;
					theAccount.InBankruptcy = chkBankruptcy.CheckState == Wisej.Web.CheckState.Checked;
					theAccount.TranCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
					theAccount.PreviousOwner = Strings.Trim(frmSHREBLUpdate.InstancePtr.txtPreviousOwner.Text);
					theAccount.SaleVerified = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED))));
					theAccount.SaleValidity = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY))));
					theAccount.SaleType = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLSALE))));
					theAccount.SaleFinancing = FCConvert.ToInt32(Math.Round(Conversion.Val(SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING))));
                    theAccount.DeedName1 = txtDeedName1.Text.Trim();
                    theAccount.DeedName2 = txtDeedName2.Text.Trim();
                    if (Information.IsDate(SaleGrid.TextMatrix(2, CNSTSALECOLDATE)))
					{
						theAccount.SaleDate = FCConvert.ToDateTime(Strings.Format(SaleGrid.TextMatrix(2, CNSTSALECOLDATE), "MM/dd/yyyy"));
					}
					else
					{
					}
					if (!Information.IsNumeric(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)))
					{
						theAccount.SalePrice = 0;
					}
					else
					{
						theAccount.SalePrice = FCConvert.ToInt32(FCConvert.ToDouble(SaleGrid.TextMatrix(2, CNSTSALECOLPRICE)));
					}
					SaveAccount();
				}
				else
				{
					textToSaleMR();
				}
				SaveSaleGrid();

				if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("hlupdate") + ""))
				{
					//FC:FINAL:MSH - i.issue #1071: incorrect converting from date to int
					//if (FCConvert.ToInt32(modDataTypes.MR.Get_Fields("hlupdate")) != 0)
					if (modDataTypes.Statics.MR.Get_Fields_DateTime("hlupdate").ToOADate() != 0)
					{
						lblMRHLUPDATE.Text = Strings.Format(modDataTypes.Statics.MR.Get_Fields_DateTime("hlupdate") + "", "MM/dd/yyyy");
					}
					else
					{
						lblMRHLUPDATE.Text = "";
					}
				}
				else
				{
					lblMRHLUPDATE.Text = "";
				}

				if (modGlobalVariables.Statics.boolFromBilling)
				{
					MDIParent.InstancePtr.boolLeavetoBilling = true;
					MDIParent.InstancePtr.txtCommSource.LinkMode = LinkModeConstants.VbLinkNone;
					MDIParent.InstancePtr.txtCommSource.LinkTopic = "TWBL0000|frmREAudit";
					MDIParent.InstancePtr.txtCommSource.LinkItem = "txtnotify";
					MDIParent.InstancePtr.txtCommSource.LinkMode = LinkModeConstants.VbLinkAutomatic;
					clsTemp.OpenRecordset("select sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum,sum(rlexemption) as exemptsum from master where not rsdeleted = 1 and rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					modGlobalVariables.Statics.boolFromBilling = false;

					// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					MDIParent.InstancePtr.txtCommSource.LinkExecute(FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("landsum"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("bldgsum"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("exemptsum"))));
					vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
					/* On Error GoTo ErrorHandler */
					return;
				}
				// Set New Information so we can compare
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillNewValue(this);
				}
				// Thsi function compares old and new values and creates change records for any differences
				modAuditReporting.ReportChanges_2(intTotalNumberOfControls - 1, clsControlInfo, clsReportChanges);
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Short Maintenance", modGlobalVariables.Statics.gintLastAccountNumber.ToString(), modGNBas.Statics.gintCardNumber.ToString());
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				// Initialize all the control and old data values
				FillControlInformationClass();
				FillControlInformationClassFromGrid();
				for (x = 0; x <= intTotalNumberOfControls - 1; x++)
				{
					clsControlInfo[x].FillOldValue(this);
				}
				//FC:FINAL:DDU:#1857 - added save complete message box
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In cmdSave_Click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			SaveData = false;
			try
			{
				TreeGrid.Row = 0;
				BPGrid.Row = 0;
				MapLotGrid.Row = 0;

				SaveBPGrid();
				SaveMapLotGrid();
				SaveSaleGrid();
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					modSHREBLUpdateMRTEXT.TextToREMR();
				}
				else
				{
					textToSaleMR();
				}
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveData", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void frmSHREBLUpdate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ErrorHandler
				if (frmSHREBLUpdate.InstancePtr.ActiveControl is FCTextBox)
				{
					if (KeyCode == Keys.Insert)
					{
						if (modGNBas.Statics.OverType == 0)
						{
							modGNBas.Statics.OverType = 1;
						}
						else
						{
							modGNBas.Statics.OverType = 0;
						}
					}
					if (Shift == 2)
					{
						if (KeyCode == Keys.Left || KeyCode == Keys.Right)
							KeyCode = (Keys)0;
					}
					else
					{
						if (KeyCode == Keys.Left || KeyCode == Keys.Right)
							(frmSHREBLUpdate.InstancePtr.ActiveControl as FCTextBox).SelectionLength = 0;
					}
					if (KeyCode == Keys.Up)
						Support.SendKeys("+{TAB}", false);
					if (KeyCode == Keys.Down)
						Support.SendKeys("{TAB}", false);
				}
                //FC:FINAL:DSE:#1780 Do not close form on Esc keyboard
				//if (KeyCode == Keys.Escape)
				//{
				//	KeyCode = (Keys)0;
				//	// Unload Me
				//	mnuQuit_Click();
				//}
				if (Shift == 2)
				{
					if (KeyCode == Keys.C || KeyCode == Keys.V || KeyCode == Keys.X)
					{
						KeyCode = (Keys)0;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Form_KeyDown in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmSHREBLUpdate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo ErrorTag
				if (KeyAscii == Keys.Cancel || FCConvert.ToInt32(KeyAscii) == 24 || FCConvert.ToInt32(KeyAscii) == 22)
					return;
				if (KeyAscii == Keys.Return)
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
					/*? 5 */
				}

				// these lines check for the backspace key
				if (FCGlobal.Screen.ActiveControl != null && FCGlobal.Screen.ActiveControl.Name != "txtMRRLLANDVAL" && FCGlobal.Screen.ActiveControl.Name != "txtMRRLBLDGVAL" && FCGlobal.Screen.ActiveControl.Name != "txtMRRLEXEMPTION")
				{
					if (KeyAscii == Keys.Back)
					{
						if (modGNBas.Statics.OverType == 1)
						{
							if (frmSHREBLUpdate.InstancePtr.ActiveControl is FCTextBox)
							{
								KeyAscii = (Keys)0;
								(frmSHREBLUpdate.InstancePtr.ActiveControl as FCTextBox).SelectionLength = 0;
								Support.SendKeys("{LEFT}", false);
								(frmSHREBLUpdate.InstancePtr.ActiveControl as FCTextBox).SelectionLength = 1;
							}
						}
					}
					else
					{
						if (modGNBas.Statics.OverType == 1 && this.ActiveControl is FCTextBox)
						{
							(frmSHREBLUpdate.InstancePtr.ActiveControl as FCTextBox).SelectionLength = modGNBas.Statics.OverType;
						}
					}
				}

				return;
			}
			catch (Exception ex)
			{
				// ErrorTag:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Form_KeyPress in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Information.Err(ex).Clear();
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
		}

		private void frmSHREBLUpdate_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ErrorHandler
				if (frmSHREBLUpdate.InstancePtr.ActiveControl is FCTextBox)
				{
					// If KeyCode = vbKeyInsert Then SetInsertMode (Screen.ActiveControl.SelLength)
					(frmSHREBLUpdate.InstancePtr.ActiveControl as FCTextBox).SelectionLength = modGNBas.Statics.OverType;
				}
				if (txtMRRSMAPLOT.Enabled)
				{
					if (KeyCode == Keys.Home)
						txtMRRSMAPLOT.Focus();
				}
				if (txtMRRLLANDVAL.Enabled)
				{
					if (KeyCode == Keys.F6)
						txtMRRLLANDVAL.Focus();
				}
				// If KeyCode = vbKeyF9 Then cmdCopy_Click
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Form_Keyup in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmSHREBLUpdate_Activated(object sender, System.EventArgs e)
		{
			/*? On Error Resume Next  */
			try
			{
				clsDRWrapper clsTemp = new clsDRWrapper();

				try
				{
					// On Error GoTo ErrorHandler
					booleditland = true;
					if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTSHORTSCREEN))
					{
						MessageBox.Show("Your permission setting for this function is set to none or is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
						this.Unload();
						return;
					}
					if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTGROUPMAINT, false))
					{
						mnuViewGroupInformation.Enabled = false;
					}
					if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTMORTGAGEMAINT, false))
					{
						mnuViewMortgageInformation.Enabled = false;
					}
					if (modGlobalConstants.Statics.gboolCL)
					{
						chkTaxAcquired.Enabled = false;
					}
					if (modREMain.Statics.boolShortRealEstate)
					{
						txtMRRLLANDVAL.ReadOnly = true;
					}
					if (modGlobalRoutines.Formisloaded_2("frmREProperty"))
					{
						frmREProperty.InstancePtr.Unload();
					}
					if (modGlobalRoutines.Formisloaded_2("frmNewREProperty"))
					{
						frmNewREProperty.InstancePtr.Unload();
					}

					SearchingTag:
					;
					modSHREBLUpdateMRTEXT.Statics.Searching = false;
					int costnumber;

					if (modMDIParent.FormExist(this))
					{
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							clsTemp.OpenRecordset("Select * from commrec where crecordnumber > 0 and crecordnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
							if (!clsTemp.EndOfFile())
							{
								if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("ccomment"))) != "")
								{
									lblComment.Visible = true;
								}
								else
								{
									lblComment.Visible = false;
								}
							}
							else
							{
								lblComment.Visible = false;
							}
						}
						else
						{
							lblComment.Visible = false;
						}
						return;
					}
					// 
					if (FormLoaded != 'Y')
					{
						FillTheScreen();
						if (modSHREBLUpdateMRTEXT.Statics.Searching == true)
							goto SearchingTag;
					}
					modRegistry.SaveKeyValue(Registry.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber));
					return;
				}
				catch (Exception ex)
				{
					// ErrorHandler:
					MessageBox.Show("Errror Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Form_Activate line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		private void frmSHREBLUpdate_Load(object sender, System.EventArgs e)
		{

			try
			{
				// On Error GoTo ErrorHandler
				if (!modGlobalConstants.Statics.gboolCL || !modGlobalVariables.Statics.boolRegRecords)
				{
					mnuRECLComment.Visible = false;
					mnuCollectionsNote.Visible = false;
				}
				boolBookPageChanged = false;
				boolMapLotGridChanged = false;
				boolSaleGridChanged = false;
				boolSaveSaleRecord = false;
				boolGridPhonesChanged = false;
				Label15.Text = modGlobalVariables.Statics.strTranCodeTownCode + " Code";
				SetupGridExempts();
				SetupSaleGrid();
				SetupGridTranCode();
				SetupGridLandCode();
				SetupGridBldgCode();
				SetupGridPropertyCode();
                SetupCardNumber();
				boolInValidate = false;
				if (modGlobalRoutines.Formisloaded_2("frmREProperty"))
				{
					frmREProperty.InstancePtr.Unload();
				}
				if (modGlobalRoutines.Formisloaded_2("frmNewREProperty"))
				{
					frmNewREProperty.InstancePtr.Unload();
				}
				if (modGlobalVariables.Statics.boolInPendingMode)
				{
					mnuInterestedParties.Enabled = false;
				}
				if (modREMain.Statics.boolShortRealEstate)
				{
					TreeGrid.Editable = FCGrid.EditableSettings.flexEDNone;
					txtMRPIACRES.Enabled = false;
					if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTDELCARDS)) == "F")
					{
						mnuDeleteCard.Visible = true;
						cmdDeleteCard.Visible = true;
					}
					else
					{
						mnuDeleteCard.Visible = false;
						cmdDeleteCard.Visible = false;
					}
				}
				else
				{
					if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTDELCARDS)) == "F")
					{
						mnuDeleteCard.Visible = true;
						cmdDeleteCard.Visible = true;
					}
					else
					{
						mnuDeleteCard.Visible = false;
						cmdDeleteCard.Visible = false;
					}
				}
				// 24    strOriginalName = ""
				lngOriginalOwnerID = 0;
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					if (!modREMain.Statics.boolShortRealEstate)
					{
						mnuValuationReport.Visible = true;
						//FC:FINAL:MSH - issue #1782: menu item was replaced by toolbar button
						cmdValuationReport.Visible = true;
					}
					if (modGlobalVariables.Statics.gintLastAccountNumber == 0)
					{
						clsDRWrapper clsTemp = new clsDRWrapper();
						modGlobalVariables.Statics.gintLastAccountNumber = modGlobalRoutines.GetNextAccountToCreate();
						lblMRHLCARDACCT1.Text = "Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
						modGlobalVariables.Statics.CurrentAccount = modGlobalVariables.Statics.gintLastAccountNumber;
						modGlobalFunctions.AddCYAEntry_26("RE", "Created New Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "In Short Screen");
						clsDRWrapper temp = modDataTypes.Statics.MR;
						modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
						modDataTypes.Statics.MR = temp;
						modDataTypes.Statics.MR.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
						modDataTypes.Statics.MR.Set_Fields("rscard", 1);
						string strGuid = "";
						cCreateGUID cGuid = new cCreateGUID();
						strGuid = cGuid.CreateGUID();
						strAccountID = strGuid;
						modDataTypes.Statics.MR.Set_Fields("CardID", strGuid);
						modDataTypes.Statics.MR.Set_Fields("AccountID", strGuid);
						clsTemp.OpenRecordset("select * from citystatezipdefaults", modGlobalVariables.strREDatabase);
						temp = modDataTypes.Statics.MR;
						modREMain.SaveMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, 1);
						modDataTypes.Statics.MR = temp;
						temp = modDataTypes.Statics.DWL;
						modREMain.OpenDwellingTable_27(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strGuid);
						modDataTypes.Statics.DWL = temp;
						temp = modDataTypes.Statics.CMR;
						modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
						modDataTypes.Statics.CMR = temp;
						temp = modDataTypes.Statics.OUT;
						modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
						modDataTypes.Statics.OUT = temp;
						modGNBas.Statics.gintMaxCards = 1;
					}
					else
					{
						lngPOOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("OwnerPartyID"))));
						lngPOSecOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("SecOwnerPartyID"))));
						cPartyController tPartyCont = new cPartyController();
						// vbPorter upgrade warning: tParty As cParty	OnWrite(cParty)
						cParty tParty = new cParty();
						cPartyAddress tAddr;
						tParty = tPartyCont.GetParty(lngPOOwnerID);
						strPOName = "";
						strPOSecOwner = "";
						strPOAddress1 = "";
						strPOAddress2 = "";
						strPOCity = "";
						strPOState = "";
						strPOZip = "";
						strPOzip4 = "";
                        strPOName = modDataTypes.Statics.MR.Get_Fields_String("DeedName1");
                        strPOSecOwner = modDataTypes.Statics.MR.Get_Fields_String("DeedName2");
						if (!(tParty == null))
						{
							tAddr = tParty.GetPrimaryAddress();
							if (!(tAddr == null))
							{
								strPOAddress1 = tAddr.Address1;
								strPOAddress2 = tAddr.Address2;
								strPOCity = tAddr.City;
								strPOZip = tAddr.Zip;
							}
						}

						dtPODate = Strings.Format(DateTime.Today, "MM/dd/yyyy");
					}
				}
				if (modGlobalVariables.Statics.boolInPendingMode)
				{
					mnuSaveQuit.Enabled = false;
					mnuSavePending.Enabled = true;
					//FC:FINAL:MSH - issue #1763: show "Pending Activity" button (similar with original app)
					cmdSaveQuit.Visible = false;
					cmdSavePending.Visible = true;
					mnuAddAccount.Enabled = false;
					cmdAddAccount.Enabled = false;
					mnuDelete.Enabled = false;
					cmdDelete.Enabled = false;
					mnuComments.Enabled = false;
					cmdComments.Enabled = false;
					mnuRECLComment.Enabled = false;
					mnuCollectionsNote.Enabled = false;
					mnuEdit.Enabled = false;
					mnuViewGroupInformation.Enabled = false;
					mnuViewGroupInformation.Enabled = false;
					mnuSearch.Enabled = false;
					lblAlertLabel.Text = "Pending";
					lblAlertLabel.Visible = true;
				}
				else if (!modGlobalVariables.Statics.boolRegRecords)
				{
					lblAlertLabel.Text = "Sale Record";
					lblAlertLabel.Visible = true;
				}
				modGlobalFunctions.SetFixedSize(this);
				modGlobalFunctions.SetTRIOColors(this, false);
				Label18.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				Label35.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				Label28.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);

				lblComment.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
				lblAlertLabel.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				if (!(modGlobalVariables.Statics.boolInPendingMode && lngCurrentPendingID > 0))
				{
					SetupBPGrid();
					SetupMapLotGrid();
					/*? 81 */
				}
				cardNumber.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				lblHighCard.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				cardNumber.Font = new Font(cardNumber.Font, FontStyle.Bold);
				lblHighCard.Font = new Font(lblHighCard.Font, FontStyle.Bold);
				lblMRHLCARDACCT1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				lblMRHLCARDACCT1.Font = new Font(lblMRHLCARDACCT1.Font, FontStyle.Bold);
				if (!modGlobalConstants.Statics.gboolCL)
				{
					mnuPrintAccountInformationSheet.Visible = false;
				}
				if (modGlobalConstants.Statics.gboolCL && modGlobalVariables.Statics.boolRegRecords)
				{
					clsDRWrapper rsTemp = new clsDRWrapper();
					rsTemp.OpenRecordset("select * from comments where type = 'RE' and account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strCLDatabase);
					if (!rsTemp.EndOfFile())
					{
						if (rsTemp.Get_Fields_Boolean("showinre") && Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("comment"))) != "")
						{
							MessageBox.Show(FCConvert.ToString(rsTemp.Get_Fields_String("Comment")).Replace("\r\n", "\r\n"), "Collections Comment", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
				boolNameChanged = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Form_Load in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			return;
		}

		private void frmSHREBLUpdate_Resize(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				ResizeTreeGrid();
				ResizeGridExempts();
				ResizeBPGrid();
				ResizeMapLotGrid();
				ResizeSaleGrid();
				ResizeGridTranCode();
				ResizeLandBldgCodes();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In Form_Resize in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			FormLoaded = ' ';
			if (!modGNBas.Statics.gboolExiting)
			{
			}
		}

		private void mnuNewAccountNoSave_Click()
		{
			cmdQuit_Click();
		}

		private void mnuNewAccountSave_Click()
		{
			cmdSave_Click();
		}

		private void GridExempts_RowColChange(object sender, System.EventArgs e)
		{
			if (GridExempts.Row == GridExempts.Rows - 1 && GridExempts.Col == CNSTEXEMPTCOLPERCENT)
			{
				GridExempts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				GridExempts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void MapLotGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (MapLotGrid.Rows < 2)
				{
					MapLotGrid.AddItem("");
					AutoPositionMapLotGrid();
					/*? 4 */
				}
				else if (Strings.Trim(MapLotGrid.TextMatrix(MapLotGrid.Rows - 1, CNSTMAPLOTCOLMAP)) != string.Empty)
				{
					MapLotGrid.AddItem("");
					AutoPositionMapLotGrid();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In MapLotGrid_AfterEdit in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MapLotGrid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			MapLotGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void MapLotGrid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (e.KeyCode == Keys.Delete)
				{
					// delete key
					if (MapLotGrid.Row > 0)
					{
						MapLotGrid.RemoveItem(MapLotGrid.Row);
						if (MapLotGrid.Rows < 2)
						{
							MapLotGrid.AddItem("");
							AutoPositionMapLotGrid();
							/*? 8 */
						}
						else if (Strings.Trim(MapLotGrid.TextMatrix(MapLotGrid.Rows - 1, CNSTMAPLOTCOLMAP)) != string.Empty)
						{
							MapLotGrid.AddItem("");
							AutoPositionMapLotGrid();
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In MapLotGrid_KeyDown in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MapLotGrid_Leave(object sender, System.EventArgs e)
		{
			MapLotGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
		}

		private void MapLotGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				modGlobalVariables.Statics.DataChanged = true;
				boolMapLotGridChanged = true;
				if (MapLotGrid.Rows < 2)
				{
					MapLotGrid.AddItem("");
					AutoPositionMapLotGrid();
					/*? 7 */
				}
				else if (Strings.Trim(MapLotGrid.TextMatrix(MapLotGrid.Rows - 1, CNSTMAPLOTCOLMAP)) != string.Empty)
				{
					MapLotGrid.AddItem("");
					AutoPositionMapLotGrid();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In MapLotGrid_ValidateEdit in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuAddAccount_Click(object sender, System.EventArgs e)
		{
			int yy;
			int lngCards;
			try
			{

				modGNBas.Statics.response = "X";
				modSHREBLUpdateMRTEXT.Statics.gboolAddAccount = true;
				modGNBas.Statics.gintCardNumber = 1;
				modGNBas.Statics.gintMaxCards = 1;

				modGlobalVariables.Statics.gintLastAccountNumber = modGlobalRoutines.GetNextAccountToCreate();
				modGlobalFunctions.AddCYAEntry_26("RE", "Created New Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "In Short Screen");
				clsDRWrapper temp = modDataTypes.Statics.MR;
				modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber);
				modDataTypes.Statics.MR = temp;
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					cCreateGUID tGuid = new cCreateGUID();
					strAccountID = tGuid.CreateGUID();
					modDataTypes.Statics.MR.Set_Fields("AccountID", strAccountID);
					modDataTypes.Statics.MR.Set_Fields("CardID", strAccountID);
					// End If
				}
				temp = modDataTypes.Statics.DWL;
				modREMain.OpenDwellingTable_36(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, strAccountID);
				modDataTypes.Statics.DWL = temp;
				temp = modDataTypes.Statics.CMR;
				modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber);
				modDataTypes.Statics.CMR = temp;
				temp = modDataTypes.Statics.OUT;
				modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber);
				modDataTypes.Statics.OUT = temp;
				// Call OpenIncomeTable(inc, gintLastAccountNumber)
				theAccount.Clear();
				ShowOwner();
				ShowSecOwner();
				ShowAddress();
                ShowDeeds();
				REMRToText();
				// 15    strOriginalName = ""
				lngOriginalOwnerID = 0;
				frmSHREBLUpdate.InstancePtr.Refresh();
				txtMRRSMAPLOT.Focus();
				modDataTypes.Statics.MR.Set_Fields("rsdwellingcode", " ");
				modDataTypes.Statics.MR.Set_Fields("datecreated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuAddAccount_Click in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuAddNewAccount_Click()
		{
			modSHREBLUpdateMRTEXT.Statics.Adding = true;
			modSHREBLUpdateMRTEXT.Statics.AddUpdDelCode = "2";
			cmdQuit_Click();
		}

		private void mnuComments_Click(object sender, System.EventArgs e)
		{
			cmdComment_Click();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper dbTemp = new clsDRWrapper();
			if (MessageBox.Show("This will remove account #" + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + ". Are you sure you wish to continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					if (!modGlobalRoutines.CheckMortgage(ref modGlobalVariables.Statics.gintLastAccountNumber))
					{
						return;
					}
					modGlobalRoutines.checkGroup_6(modGlobalVariables.Statics.gintLastAccountNumber, false);
				}
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					dbTemp.Execute("Update Master Set rsdeleted = 1 where RSAccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
				}
				else
				{
					dbTemp.Execute("Update SRmaster set rsdeleted = 1 where saleid = " + modDataTypes.Statics.MR.Get_Fields_Int32("saleid"), modGlobalVariables.strREDatabase);
				}
				modDataTypes.Statics.MR.Set_Fields("rsdeleted", 1);
				clsTemp.Execute("delete from summrecord where srecordnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
				this.Unload();
			}
		}

		public void mnuDelete_Click()
		{
			mnuDelete_Click(mnuDelete, new System.EventArgs());
		}

		private void mnuDeleteAccount_Click()
		{
			modSHREBLUpdateMRTEXT.Statics.Deleting = true;
			modSHREBLUpdateMRTEXT.Statics.AddUpdDelCode = "3";
			cmdQuit_Click();
		}

		private void mnuDeleteCard_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				if (MessageBox.Show("This will permanently delete this card.  If you continue it cannot be undeleted." + "\r\n" + "Do you wish to continue?", "Delete Card?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (modGNBas.Statics.gintCardNumber == 1/*&& UpDownCard.Maximum < 2*/)
					{
						// last card. This deletes the account
						MessageBox.Show("This is the last card and cannot be permanently deleted." + "\r\n" + "The account will be marked as a deleted account" + "\r\n" + "If you need to permanently delete the account, this must be done in File Maintenance", "Last Card", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						mnuDelete_Click();
						return;
					}
					else
					{
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							modGlobalFunctions.AddCYAEntry_26("RE", "Permanently Deleted Card from short screen", "Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber));
						}
						else
						{
							modGlobalFunctions.AddCYAEntry_26("RE", "Permanently Deleted Card from short screen", "Sales Account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " Card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber));
						}
					}
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						clsTemp.Execute("delete from summrecord where srecordnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and cardnumber = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase, false);
						clsTemp.Execute("delete from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase, false);
					}
					else
					{
						clsTemp.Execute("delete from srmaster where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + " and saledate = #" + modGlobalVariables.Statics.gcurrsaledate + "#", modGlobalVariables.strREDatabase, false);
					}
					modGNBas.Statics.gintMaxCards -= 1;
					int intCounter;

					if (modGNBas.Statics.gintMaxCards < 1)
					{
						mnuQuit_Click();
						return;
					}
					modGNBas.Statics.gintCardNumber = 1;
                    SetupCardNumber();
                    cardNumber.Text = FCConvert.ToString(1);
					modGNBas.Statics.gintCardNumber = 2;
					// just so we can call cmdprevious to fill the screen for us
					cmdPrevious_Click();
					FillTreeGrid();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuDeleteCard_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuGotoFirstCard_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intRes As short, int --> As DialogResult
			DialogResult intRes;
			try
			{
				// On Error GoTo ErrorHandler
				if (modGlobalVariables.Statics.boolInPendingMode)
				{
					intRes = MessageBox.Show("Do you wish to save pending changes for this card first?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intRes == DialogResult.Yes)
						mnuSavePending_Click();
				}
				else
				{
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						modSHREBLUpdateMRTEXT.TextToREMR();
					}
					else
					{
						textToSaleMR();
					}
				}
				modGNBas.Statics.gintCardNumber = 1;
				clsDRWrapper temp = modDataTypes.Statics.MR;
				modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.MR = temp;
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					strAccountID = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("accountid"));
				}
				temp = modDataTypes.Statics.DWL;
				modREMain.OpenDwellingTable_27(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strAccountID);
				modDataTypes.Statics.DWL = temp;
				temp = modDataTypes.Statics.CMR;
				modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.CMR = temp;
				temp = modDataTypes.Statics.OUT;
				modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.OUT = temp;
				REMRToText();
				FillTreeGrid();
				frmSHREBLUpdate.InstancePtr.Refresh();
				temp = modDataTypes.Statics.CR;
				modREMain.OpenCRTable(ref temp, 1330);
				Open1Label.Text = Strings.StrConv(temp.Get_Fields_String("cldesc"), VbStrConv.ProperCase);
                modREMain.OpenCRTable(ref temp, 1340);
				modDataTypes.Statics.CR = temp;
				Open2Label.Text = Strings.StrConv(modDataTypes.Statics.CR.Get_Fields_String("cldesc"), VbStrConv.ProperCase);
                CalculateValues();
				txtMRRSMAPLOT.Focus();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuGotoFirstCard_Click in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuGotoLastCard_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intRes As short, int --> As DialogResult
			DialogResult intRes;
			try
			{
				// On Error GoTo ErrorHandler
				if (modGlobalVariables.Statics.boolInPendingMode)
				{
					intRes = MessageBox.Show("Do you wish to save pending changes for this card first?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intRes == DialogResult.Yes)
						mnuSavePending_Click();
				}
				else
				{
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						modSHREBLUpdateMRTEXT.TextToREMR();
					}
					else
					{
						textToSaleMR();
					}
				}
				modGNBas.Statics.gintCardNumber = modGNBas.Statics.gintMaxCards;
				clsDRWrapper temp = modDataTypes.Statics.MR;
				modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.MR = temp;
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					strAccountID = FCConvert.ToString(modDataTypes.Statics.MR.Get_Fields_String("accountid"));
				}
				temp = modDataTypes.Statics.DWL;
				modREMain.OpenDwellingTable_27(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strAccountID);
				modDataTypes.Statics.DWL = temp;
				temp = modDataTypes.Statics.CMR;
				modREMain.OpenCOMMERCIALTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.CMR = temp;
				temp = modDataTypes.Statics.OUT;
				modREMain.OpenOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.OUT = temp;
				REMRToText();
				FillTreeGrid();
				frmSHREBLUpdate.InstancePtr.Refresh();
				temp = modDataTypes.Statics.CR;
				modREMain.OpenCRTable(ref temp, 1330);
				Open1Label.Text = Strings.StrConv(temp.Get_Fields_String("cldesc"), VbStrConv.ProperCase);
                modREMain.OpenCRTable(ref temp, 1340);
				modDataTypes.Statics.CR = temp;
				Open2Label.Text = Strings.StrConv(modDataTypes.Statics.CR.Get_Fields_String("cldesc"), VbStrConv.ProperCase);
                CalculateValues();
				txtMRRSMAPLOT.Focus();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuGotoLastCard_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intCardToShow As short --> As int	OnWrite(int, double)
		private void ChangeCard(int intCardToShow)
		{
			// vbPorter upgrade warning: intRes As short, int --> As DialogResult
			DialogResult intRes;
			if (modGNBas.Statics.gintCardNumber == intCardToShow)
				return;
			if (modGlobalVariables.Statics.boolInPendingMode)
			{
				intRes = MessageBox.Show("Do you wish to save pending changes for this card first?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intRes == DialogResult.Yes)
					mnuSavePending_Click();
			}
			else
			{
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					modSHREBLUpdateMRTEXT.TextToREMR();
				}
				else
				{
					textToSaleMR();
				}
			}
			modGNBas.Statics.gintCardNumber = intCardToShow;
			//FC:FINAL:DDU:#i961 - no need of UpDown
			//if (FCConvert.ToInt32(UpDownCard.Value != modGNBas.gintCardNumber)
			//{
			//    UpDownCard.Value = modGNBas.gintCardNumber;
			//}
			ReShowCard();
			if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
			{
				if (booleditland)
				{
					GridExempts.ForeColor = Color.Black;
					GridExempts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
			}
			else
			{
				GridExempts.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
				GridExempts.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			boolCardChangedByProg = false;
			// clear the flag
		}

		private void mnuGotoNextCard_Click(object sender, System.EventArgs e)
		{
			if (modGNBas.Statics.gintCardNumber != modGNBas.Statics.gintMaxCards)
			{
				ChangeCard(modGNBas.Statics.gintCardNumber + 1);
			}
			
		}

		private void mnuGotoPreviousCard_Click(object sender, System.EventArgs e)
		{
			if (modGNBas.Statics.gintCardNumber > 1)
			{
				ChangeCard(modGNBas.Statics.gintCardNumber - 1);
			}
			
		}

		private void mnuInterestedParties_Click(object sender, System.EventArgs e)
		{
			frmInterestedParties.InstancePtr.Init(modGlobalVariables.Statics.gintLastAccountNumber, "RE", 0, FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTEDITADDRESSES)) == "F");
		}

		private void mnuNextSave_Click(object sender, System.EventArgs e)
		{

			int lngOldRow;
			try
			{
				// On Error GoTo ErrorHandler
				BPGrid.Row = 0;
				BPGrid.Col = 0;
				GridExempts.Row = 0;
				GridExempts.Col = 0;
				//Application.DoEvents();
				// save the data before moving on
				SaveData();
				if (frmRESearch.InstancePtr.Grid.Rows <= 1)
					return;
				lngOldRow = frmRESearch.InstancePtr.Grid.Row;
				if (lngOldRow < 1)
					return;
				if (lngOldRow == frmRESearch.InstancePtr.Grid.Rows - 1)
					return;
				frmRESearch.InstancePtr.Grid.Row = lngOldRow + 1;
				modGNBas.Statics.response = Strings.Trim(frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 5));
				modGlobalVariables.Statics.gcurrsaledate = Strings.Trim(frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 3));
				// strSellerName = Trim(frmresearch.Grid.TextMatrix(Grid.Row, 0))
				modGlobalVariables.Statics.gintAutoID = FCConvert.ToInt32(frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 6));
				frmRESearch.InstancePtr.GetSearchAccount();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuNextSave_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuPendingTransfer_Click(object sender, System.EventArgs e)
		{
			frmTransferAccount.InstancePtr.Init(0);
		}

		private void mnuPreviousNoSave_Click()
		{

		}

		private void mnuPrintLabel_Click()
		{

		}

		private void mnuPreviousSave_Click(object sender, System.EventArgs e)
		{
			int lngOldRow;
			BPGrid.Row = 0;
			BPGrid.Col = 0;
			GridExempts.Row = 0;
			GridExempts.Col = 0;
			SaveData();
			lngOldRow = frmRESearch.InstancePtr.Grid.Row;
			if (lngOldRow <= 1)
				return;
			frmRESearch.InstancePtr.Grid.Row = lngOldRow - 1;
			modGNBas.Statics.response = Strings.Trim(frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 5));
			modGlobalVariables.Statics.gcurrsaledate = Strings.Trim(frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 3));
			modGlobalVariables.Statics.gintAutoID = FCConvert.ToInt32(frmRESearch.InstancePtr.Grid.TextMatrix(frmRESearch.InstancePtr.Grid.Row, 6));
			frmRESearch.InstancePtr.GetSearchAccount();
		}

		private void mnuPrintAccountInformationSheet_Click(object sender, System.EventArgs e)
		{
			rptAccountInformationSheet.InstancePtr.Init(modGlobalVariables.Statics.gintLastAccountNumber, DateTime.Now);
		}

		private void mnuPrintCard_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptShortCard.InstancePtr);
		}

		private void mnuPrintLabel1_Click(object sender, System.EventArgs e)
		{
			int intLType;
			modGlobalVariables.Statics.gboolPrintAsMailing = false;
			intLType = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("REF2LabelType"))));
			if (intLType != modLabels.CNSTLBLTYPEDYMO30256 && intLType != modLabels.CNSTLBLTYPEDYMO30252 && intLType != modLabels.CNSTLBLTYPEDYMO4150)
			{
				rptLabels.InstancePtr.PrintReport(false);
			}
			else
			{
				rptLaserLabels.InstancePtr.Init(true, 2, 2, 1, intLType, 640, FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "", 0, "", 0, 0, 1, "", false, true);
				//FC:FINAL:DDU:#i1233 - don't close report
				//rptLaserLabels.InstancePtr.Close();
			}
		}

		private void mnuPrintScreen_Click()
		{
		}

		private void BPGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (BPGrid.Rows < 2)
				{
					BPGrid.AddItem(FCConvert.ToString(true));
					AutoPositionBPGrid();
					/*? 4 */
				}
				else if ((Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPSaleDateCol)) != string.Empty))
				{
					BPGrid.AddItem(FCConvert.ToString(true));
					AutoPositionBPGrid();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In BPGrid_AfterEdit in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void BPGrid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			BPGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void BPGrid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (e.KeyCode == Keys.Delete)
				{
					// delete key
					if (BPGrid.Row > 0)
					{
						BPGrid.RemoveItem(BPGrid.Row);
						if (BPGrid.Rows < 2)
						{
							BPGrid.AddItem(FCConvert.ToString(true));
							AutoPositionBPGrid();
							/*? 7 */
						}
						else if ((Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPSaleDateCol)) != string.Empty))
						{
							BPGrid.AddItem(FCConvert.ToString(true));
							AutoPositionBPGrid();
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In BPGrid_KeyDown in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void BPGrid_Leave(object sender, System.EventArgs e)
		{
			BPGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
		}

		private void BPGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				modGlobalVariables.Statics.DataChanged = true;
				boolBookPageChanged = true;
				if (BPGrid.Rows < 2)
				{
					BPGrid.AddItem(FCConvert.ToString(true));
					AutoPositionBPGrid();
					/*? 6 */
				}
				else if ((Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) != string.Empty) || (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPSaleDateCol)) != string.Empty))
				{
					BPGrid.AddItem(FCConvert.ToString(true));
					AutoPositionBPGrid();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In BPGrid_ValidateEdit in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillMapLotGrid()
		{
			clsDRWrapper clsMP = new clsDRWrapper();
			string strWhere;
			string strMP = "";
			try
			{
				// On Error GoTo ErrorHandler
				SetupMapLotGrid();
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					strMP = "MapLot";
				}
				else
				{
					strMP = "SRMapLot";
				}
				strWhere = " order by line";
				if (!modGlobalVariables.Statics.boolRegRecords)
					strWhere = " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "' " + strWhere;
				clsMP.OpenRecordset("select * from " + strMP + " where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " " + strWhere, modGlobalVariables.strREDatabase);
				if (!clsMP.EndOfFile())
				{
					MapLotGrid.Rows = 1;
					while (!clsMP.EndOfFile())
					{
						MapLotGrid.AddItem(clsMP.Get_Fields_String("maplot") + "\t" + clsMP.Get_Fields_String("mpdate"));
						clsMP.MoveNext();
					}
					MapLotGrid.AddItem("");
				}
				else
				{
					MapLotGrid.Rows = 1;
					MapLotGrid.AddItem("");
				}
				AutoPositionMapLotGrid();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillMapLotGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillBPGrid()
		{
			clsDRWrapper clsBP = new clsDRWrapper();
			string strWhere;
			string strBP = "";
			try
			{
				// On Error GoTo ErrorHandler
				SetupBPGrid();
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					strBP = "BookPage";
				}
				else
				{
					strBP = "SRBookPage";
				}
				strWhere = " order by line";
				if (!modGlobalVariables.Statics.boolRegRecords)
					strWhere = " and saledate = '" + modGlobalVariables.Statics.gcurrsaledate + "'" + strWhere;
				clsBP.OpenRecordset("select * from " + strBP + " where account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and card = 1" + strWhere, modGlobalVariables.strREDatabase);
				if (!clsBP.EndOfFile())
				{
					BPGrid.Rows = 1;
					while (!clsBP.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
						BPGrid.AddItem(clsBP.Get_Fields_Boolean("current") + "\t" + clsBP.Get_Fields("book") + "\t" + clsBP.Get_Fields("Page") + "\t" + clsBP.Get_Fields_String("BPDate"));
						clsBP.MoveNext();
					}
					BPGrid.AddItem(FCConvert.ToString(true));
				}
				else
				{
					BPGrid.Rows = 1;
					BPGrid.AddItem(FCConvert.ToString(true));
				}
				AutoPositionBPGrid();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillBPGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AutoPositionBPGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				BPGrid.ShowCell(BPGrid.Rows - 1, 0);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In AutoPositionBPGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AutoPositionMapLotGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				MapLotGrid.ShowCell(MapLotGrid.Rows - 1, 0);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In AutoPositionMapLotGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupBPGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				BPGrid.Rows = 1;
				BPGrid.TextMatrix(0, BPCheckCol, "");
				BPGrid.TextMatrix(0, BPBookCol, "Book");
				BPGrid.TextMatrix(0, BPPageCol, "Page");
				BPGrid.TextMatrix(0, BPSaleDateCol, "Date");
				BPGrid.ColEditMask(BPSaleDateCol, "##/##/####");
				BPGrid.ColDataType(BPCheckCol, FCGrid.DataTypeSettings.flexDTBoolean);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupBPGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupMapLotGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				MapLotGrid.Rows = 1;
				MapLotGrid.Cols = 2;
				MapLotGrid.ExtendLastCol = true;
				MapLotGrid.TextMatrix(0, 0, "Map/Lot");
				MapLotGrid.TextMatrix(0, 1, "Date");
				MapLotGrid.ColEditMask(CNSTMAPLOTCOLMPDATE, "##/##/####");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupMapLotGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupSaleGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				SaleGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeRestrictColumns;
				SaleGrid.MergeRow(0, true);
				SaleGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				//FC:FINAL:AM:#i1378 - set the header text only for one column
				//SaleGrid.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0, 0, 5, "Sale Data");
				SaleGrid.TextMatrix(0, 3, "SALE DATA");
				SaleGrid.TextMatrix(1, CNSTSALECOLPRICE, "Price");
				SaleGrid.TextMatrix(1, CNSTSALECOLDATE, "Date");
				SaleGrid.TextMatrix(1, CNSTSALECOLSALE, "Type");
				SaleGrid.TextMatrix(1, CNSTSALECOLFINANCING, "Financing");
				SaleGrid.TextMatrix(1, CNSTSALECOLVERIFIED, "Verified");
				SaleGrid.TextMatrix(1, CNSTSALECOLVALIDITY, "Validity");
				SaleGrid.ColEditMask(CNSTSALECOLDATE, "##/##/####");
                SaleGrid.ColAlignment(CNSTSALECOLPRICE, FCGrid.AlignmentSettings.flexAlignRightCenter);
				if (!modREMain.Statics.boolShortRealEstate)
				{
					SaleGrid.Editable = FCGrid.EditableSettings.flexEDNone;
					SaleGrid.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
				}
				else
				{
					SaleGrid.Enabled = true;
					//FC:FINAL:DSE:#1778 Grid is not editable even if Enabled is true
					SaleGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupSaleGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridExempts()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp = "";

			try
			{
				// On Error GoTo ErrorHandler
				GridExempts.Cols = 3;
				// third column holds the code
				GridExempts.Rows = 4;
				GridExempts.ColHidden(2, true);
				GridExempts.TextMatrix(0, 0, "Exemption");
				GridExempts.TextMatrix(0, 1, "Percent");
				GridExempts.TextMatrix(1, 1, "100");
				GridExempts.TextMatrix(2, 1, "100");
				GridExempts.TextMatrix(3, 1, "100");
				clsTemp.OpenRecordset("select * from exemptcode order by DESCRIPTION", modGlobalVariables.strREDatabase);
				strTemp = "#-1;No Exempt|";
				while (!clsTemp.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("code") + "")) + ";" + clsTemp.Get_Fields_String("description") + "\t" + clsTemp.Get_Fields("code") + "|";
					clsTemp.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				GridExempts.ColComboList(0, strTemp);
				GridExempts.TextMatrix(1, 0, FCConvert.ToString(CNSTNOEXEMPT));
				GridExempts.TextMatrix(2, 0, FCConvert.ToString(CNSTNOEXEMPT));
				GridExempts.TextMatrix(3, 0, FCConvert.ToString(CNSTNOEXEMPT));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridExempts in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridExempts()
		{
			int GridWidth = 0;
			try
			{
				// On Error GoTo ErrorHandler
				GridWidth = GridExempts.WidthOriginal;
				GridExempts.ColWidth(0, FCConvert.ToInt32(0.75 * GridWidth));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ResizeGridExempts", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeBPGrid()
		{
			int GridWidth = 0;
			try
			{
				// On Error GoTo ErrorHandler
				GridWidth = BPGrid.WidthOriginal;
				BPGrid.ColWidth(BPCheckCol, FCConvert.ToInt32(0.1 * GridWidth));
				BPGrid.ColWidth(BPBookCol, FCConvert.ToInt32(0.26 * GridWidth));
				BPGrid.ColWidth(BPPageCol, FCConvert.ToInt32(0.26 * GridWidth));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ResizeBPGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeMapLotGrid()
		{
			int GridWidth = 0;
			try
			{
				// On Error GoTo ErrorHandler
				GridWidth = MapLotGrid.WidthOriginal;
				MapLotGrid.ColWidth(0, FCConvert.ToInt32(0.6 * GridWidth));
				// .Height = .RowHeight(0) * 3 + 50
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ResizeMapLotGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		//FC:FINAL:AM:#3312 - show tooltip on cell formatting
        //private void GridExempts_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		//{
		//	int lngMrow;
  //          try
  //          {
  //              // On Error GoTo ErrorHandler
  //              lngMrow = GridExempts.MouseRow;
  //              if (lngMrow < 1)
  //              {
  //                  ToolTip2.SetToolTip(GridExempts, "");
  //              }
  //              else
  //              {
  //                  if (Conversion.Val(GridExempts.TextMatrix(lngMrow, CNSTEXEMPTCOLCODE)) == CNSTNOEXEMPT)
  //                  {
  //                      ToolTip2.SetToolTip(GridExempts, "");
  //                  }
  //                  else
  //                  {
  //                      if (modGNBas.Statics.gintCardNumber == 1)
  //                      {
  //                          // TODO Get_Fields: Field [exemptval] not found!! (maybe it is an alias?)
  //                          ToolTip2.SetToolTip(GridExempts, FCConvert.ToString(Conversion.Val(GridExempts.TextMatrix(lngMrow, CNSTEXEMPTCOLCODE))) + "  " + GridExempts.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngMrow, CNSTEXEMPTCOLCODE) + "     Value: " + Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("exemptval" + FCConvert.ToString(lngMrow)) + ""), "#,###,##0"));
  //                          // TODO Get_Fields: Field [exemptval] not found!! (maybe it is an alias?)
  //                          lngExVal[lngMrow] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("exemptval" + FCConvert.ToString(lngMrow)) + "")));
  //                      }
  //                      else
  //                      {
  //                          ToolTip2.SetToolTip(GridExempts, FCConvert.ToString(Conversion.Val(GridExempts.TextMatrix(lngMrow, CNSTEXEMPTCOLCODE))) + "  " + GridExempts.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngMrow, CNSTEXEMPTCOLCODE) + "     Value: " + Strings.Format(lngExVal[lngMrow], "#,###,##0"));
  //                      }
  //                  }
  //              }
                
  //              return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ErrorHandler:
		//		MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GridExempts_MouseMove in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//}

        private void GridExempts_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (!inCellFormatting && GridExempts.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                inCellFormatting = true;
                DataGridViewCell cell = GridExempts[e.ColumnIndex, e.RowIndex];
                int lngMrow = GridExempts.GetFlexRowIndex(e.RowIndex);
                if (Conversion.Val(GridExempts.TextMatrix(lngMrow, CNSTEXEMPTCOLCODE)) == CNSTNOEXEMPT)
                {
                    cell.ToolTipText = "";
                }
                else
                {
                    if (modGNBas.Statics.gintCardNumber == 1)
                    {                        
                        cell.ToolTipText = FCConvert.ToString(Conversion.Val(GridExempts.TextMatrix(lngMrow, CNSTEXEMPTCOLCODE))) + "  " + GridExempts.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngMrow, CNSTEXEMPTCOLCODE) + "     Value: " + Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("exemptval" + FCConvert.ToString(lngMrow)) + ""), "#,###,##0");
                        lngExVal[lngMrow] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("exemptval" + FCConvert.ToString(lngMrow)) + "")));
                    }
                    else
                    {
                        cell.ToolTipText = FCConvert.ToString(Conversion.Val(GridExempts.TextMatrix(lngMrow, CNSTEXEMPTCOLCODE))) + "  " + GridExempts.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngMrow, CNSTEXEMPTCOLCODE) + "     Value: " + Strings.Format(lngExVal[lngMrow], "#,###,##0");
                    }
                }
                inCellFormatting = false;
            }
        }

        private void mnuPrintMailing_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.gboolPrintAsMailing = true;
			int intLType;
			modGlobalVariables.Statics.gboolPrintAsMailing = false;
			intLType = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("REF2LabelType"))));
			if (intLType != modLabels.CNSTLBLTYPEDYMO30256 && intLType != modLabels.CNSTLBLTYPEDYMO30252 && intLType != modLabels.CNSTLBLTYPEDYMO4150)
			{
				rptLabels.InstancePtr.PrintReport(false);
			}
			else
			{
				rptLaserLabels.InstancePtr.Init(true, 2, 1, 1, intLType, 640, FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "", 0, "", 0, 0, 1, "", false, true);
			}
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorHandler = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				if (modGlobalVariables.Statics.boolFromBilling)
				{
					clsDRWrapper clsTemp = new clsDRWrapper();
					MDIParent.InstancePtr.boolLeavetoBilling = true;
					MDIParent.InstancePtr.txtCommSource.LinkMode = LinkModeConstants.VbLinkNone;
					MDIParent.InstancePtr.txtCommSource.LinkTopic = "TWBL0000|frmREAudit";
					MDIParent.InstancePtr.txtCommSource.LinkItem = "txtnotify";
					MDIParent.InstancePtr.txtCommSource.LinkMode = LinkModeConstants.VbLinkAutomatic;
					clsTemp.OpenRecordset("select sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum,sum(rlexemption) as exemptsum from master where not rsdeleted = 1 and rsaccount = " + modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"), modGlobalVariables.strREDatabase);
					modGlobalVariables.Statics.boolFromBilling = false;

					// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					MDIParent.InstancePtr.txtCommSource.LinkExecute(FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("landsum"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("bldgsum"))) + "," + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("exemptsum"))));
					vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
					/* On Error GoTo ErrorHandler */
					return;
				}
				frmSHREBLUpdate.InstancePtr.Unload();
				frmGetAccount.InstancePtr.Show(App.MainForm);
				// DoEvents
				// frmGetAccount.SetFocus
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuQuit_Click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuQuit_Click()
		{
			mnuQuit_Click(mnuQuit, new System.EventArgs());
		}

		private void mnuQuit1_Click()
		{
			frmSHREBLUpdate.InstancePtr.Unload();
		}

		private void mnuSaveExit1_Click()
		{
			cmdSave_Click();
		}

		private void mnuRECLComment_Click(object sender, System.EventArgs e)
		{
			if (modGlobalVariables.Statics.gintLastAccountNumber > 0)
			{
				frmRECLComment.InstancePtr.Init( modGlobalVariables.Statics.gintLastAccountNumber);
			}
		}

		private void mnuSavePending_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsOrig = new clsDRWrapper();
			clsDRWrapper clsExecute = new clsDRWrapper();
			int lngTransactionID = 0;
			bool boolfirstChange;
			CheckValues();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			CheckValues();
			modSHREBLUpdateMRTEXT.TextToREMR();
			// put changes into mr but don't update the recordset
			clsOrig.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard = " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
			boolfirstChange = true;
			int x;
			clsDRWrapper temp = modDataTypes.Statics.MR;
			for (x = 0; x <= temp.FieldsCount - 1; x++)
			{
				if (Strings.LCase(temp.Get_FieldsIndexName(x)) != "id")
				{
					modGlobalRoutines.CompareFieldValues(ref temp, ref clsOrig, temp.Get_FieldsIndexName(x), "Master", dtMinPendingDate, lngTransactionID, boolfirstChange);
				}
				modDataTypes.Statics.MR = temp;
			}
			// x
			SaveBPPending(ref lngTransactionID, ref dtMinPendingDate, ref boolfirstChange);
			// Call clsExecute.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Created Pending Change','Created a pending change from the short screen for account " & gintLastAccountNumber & " card " & gintCardNumber & "')", strredatabase)
			modGlobalFunctions.AddCYAEntry_26("RE", "Created Pending Change", "Created a pending change from the short screen for account " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " card " + FCConvert.ToString(modGNBas.Statics.gintCardNumber));
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			MessageBox.Show("Pending changes saved in pending activity list", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void mnuSavePending_Click()
		{
			mnuSavePending_Click(mnuSavePending, new System.EventArgs());
		}

		private void mnuSaveQuit_Click(object sender, System.EventArgs e)
		{
			if (cardNumber.Enabled)
			{
				cardNumber.Focus();
			}
			gridTranCode.Row = -1;
			gridLandCode.Row = -1;
			gridBldgCode.Row = -1;
			//Application.DoEvents();
			cmdSave_Click();
		}

		private void mnuSearch1_Click()
		{
			frmRESearch.InstancePtr.Unload();
			frmRESearch.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		private void mnuUpdate_Click()
		{
			modSHREBLUpdateMRTEXT.Statics.AddUpdDelCode = "1";
			if (modSHREBLUpdateMRTEXT.Statics.Adding == false)
			{
				cmdSave_Click();
			}
			else
			{
				cmdQuit_Click();
			}
		}

		private void mnuUpdate1_Click()
		{
			modSHREBLUpdateMRTEXT.Statics.AddUpdDelCode = "1";
			if (modSHREBLUpdateMRTEXT.Statics.Adding == false)
			{
				cmdSave_Click();
			}
			else
			{
				cmdQuit_Click();
			}
		}

		private void SaleGrid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			SaleGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void SaleGrid_Leave(object sender, System.EventArgs e)
		{
			MapLotGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
		}

		private void SaleGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			modGlobalVariables.Statics.DataChanged = true;
			boolBookPageChanged = true;
		}

		private void TreeGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				modDataTypes.Statics.MR.Set_Fields("rssoft", FCConvert.ToString(Conversion.Val(TreeGrid.TextMatrix(1, 1) + "")));
				modDataTypes.Statics.MR.Set_Fields("rshard", FCConvert.ToString(Conversion.Val(TreeGrid.TextMatrix(1, 3) + "")));
				modDataTypes.Statics.MR.Set_Fields("rsmixed", FCConvert.ToString(Conversion.Val(TreeGrid.TextMatrix(1, 2) + "")));
				if (Conversion.Val(TreeGrid.TextMatrix(2, 1)) > 0)
				{
					modDataTypes.Statics.MR.Set_Fields("rssoftvalue", FCConvert.ToInt32(FCConvert.ToDouble(TreeGrid.TextMatrix(2, 1) + "")));
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("rssoftvalue", 0);
				}
				if (Conversion.Val(TreeGrid.TextMatrix(2, 3)) > 0)
				{
					modDataTypes.Statics.MR.Set_Fields("rshardvalue", FCConvert.ToInt32(FCConvert.ToDouble(TreeGrid.TextMatrix(2, 3) + "")));
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("rshardvalue", 0);
				}
				if (Conversion.Val(TreeGrid.TextMatrix(2, 2)) > 0)
				{
					modDataTypes.Statics.MR.Set_Fields("rsmixedvalue", FCConvert.ToInt32(FCConvert.ToDouble(TreeGrid.TextMatrix(2, 2) + "")));
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("rsmixedvalue", 0);
				}
				modDataTypes.Statics.MR.Set_Fields("rsother", FCConvert.ToString(Conversion.Val(TreeGrid.TextMatrix(1, 4))));
				if (Conversion.Val(TreeGrid.TextMatrix(2, 4)) > 0)
				{
					modDataTypes.Statics.MR.Set_Fields("rsothervalue", FCConvert.ToInt32(FCConvert.ToDouble(TreeGrid.TextMatrix(2, 4) + "")));
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("rsothervalue", 0);
				}
				FillTreeGrid();
				CalculateValues();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "in TreeGrid_AfterEdit in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void TreeGrid_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (modREMain.Statics.boolShortRealEstate)
				{
					if (TreeGrid.Col >= 1)
					{
						TreeGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else
					{
						TreeGrid.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				else
				{
					TreeGrid.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "in TreeGrid_RowColChange in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txt2ndOwnerID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txt2ndOwnerID.Text) != theAccount.SecOwnerID)
			{
				cParty tParty = new cParty();
				cPartyController tCont = new cPartyController();
				if (!tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txt2ndOwnerID.Text))))
				{
					e.Cancel = true;
					MessageBox.Show("That party doesn't exist.", "Invalid Party", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txt2ndOwnerID.Text = FCConvert.ToString(theAccount.SecOwnerID);
					return;
				}
				theAccount.SecOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(txt2ndOwnerID.Text)));
				tParty = theAccount.SecOwnerParty;
				tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txt2ndOwnerID.Text)));
				ShowSecOwner();
			}
		}
        
        private void txtMRDIDATEINSPECTED_Enter(object sender, System.EventArgs e)
		{
			// OverType = 0
			try
			{
				// On Error GoTo ErrorHandler
				txtMRDIDATEINSPECTED.SelectionStart = 0;
				txtMRDIDATEINSPECTED.SelectionLength = modGNBas.Statics.OverType;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRDIDateInspected_GotFocus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRDIDATEINSPECTED_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
                // On Error GoTo ErrorHandler
                //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
                if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
                {
                    modHelpFiles.Statics.Helping = true;
                    modHelpFiles.HelpFiles_2("Property", 147);
					//Application.DoEvents();
					frmHelp.InstancePtr.Unload();
				}
				if (txtMRDIDATEINSPECTED.SelectionStart == 1)
				{
					if (Strings.UCase(Strings.Mid(txtMRDIDATEINSPECTED.Text, 1, 1)) == "T")
						txtMRDIDATEINSPECTED.Text = Strings.Format(DateTime.Today, "MM/dd/yy");
				}
				if (txtMRDIDATEINSPECTED.SelectionStart == 2)
					txtMRDIDATEINSPECTED.SelectionStart = 3;
				if (txtMRDIDATEINSPECTED.SelectionStart == 3)
				{
					if (KeyCode == Keys.Left)
						txtMRDIDATEINSPECTED.SelectionStart = 1;
				}
				else if (txtMRDIDATEINSPECTED.SelectionStart == 6)
				{
					if (KeyCode == Keys.Left)
						txtMRDIDATEINSPECTED.SelectionStart = 4;
				}
				if (txtMRDIDATEINSPECTED.SelectionStart == 5)
					txtMRDIDATEINSPECTED.SelectionStart = 6;
				if (txtMRDIDATEINSPECTED.SelectionStart == 8)
					Support.SendKeys("{TAB}", false);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRDIDateInspected_KeyUp", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRDIDATEINSPECTED_Leave(object sender, System.EventArgs e)
		{
			string hold = "";
			try
			{
				// On Error GoTo ErrorHandler
				if (Conversion.Val(Strings.Mid(txtMRDIDATEINSPECTED.Text, 1, 2)) > 12)
				{
					hold = txtMRDIDATEINSPECTED.Text;
					fecherFoundation.Strings.MidSet(ref hold, 1, 2, "00");
					txtMRDIDATEINSPECTED.Text = hold;
					txtMRDIDATEINSPECTED.Focus();
				}
				if (Conversion.Val(Strings.Mid(txtMRDIDATEINSPECTED.Text, 4, 2)) > 31)
				{
					hold = txtMRDIDATEINSPECTED.Text;
					fecherFoundation.Strings.MidSet(ref hold, 4, 2, "00");
					txtMRDIDATEINSPECTED.Text = hold;
					txtMRDIDATEINSPECTED.Focus();
				}
				if (Conversion.Val(Strings.Mid(txtMRDIDATEINSPECTED.Text, 7, 2)) > 99)
				{
					hold = txtMRDIDATEINSPECTED.Text;
					fecherFoundation.Strings.MidSet(ref hold, 7, 2, "00");
					txtMRDIDATEINSPECTED.Text = hold;
					txtMRDIDATEINSPECTED.Focus();
				}
				if (Conversion.Val(txtMRDIDATEINSPECTED.Text) == 0)
				{
					// OUT.Fields("oidateinspected") = 0
					modDataTypes.Statics.MR.Set_Fields("dateinspected", 0);
				}
				else
				{
					if (Information.IsDate(txtMRDIDATEINSPECTED.Text))
					{
						modDataTypes.Statics.MR.Set_Fields("dateinspected", txtMRDIDATEINSPECTED.Text);
					}
					else
					{
						// OUT.Fields("oidateinspected") = 0
						modDataTypes.Statics.MR.Set_Fields("dateinspected", 0);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRDIDateInspected_LostFocus in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRDIDATEINSPECTED_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			if (txtMRDIDATEINSPECTED.SelectedText == "/")
				Support.SendKeys("{RIGHT}", false);
		}

		private void txtMRDIENTRANCECODE_Enter(object sender, System.EventArgs e)
		{
			// OverType = 0
			try
			{
				// On Error GoTo ErrorHandler
				txtMRDIENTRANCECODE.SelectionStart = 0;
				txtMRDIENTRANCECODE.SelectionLength = modGNBas.Statics.OverType;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRDIEntranceCode_GotFocus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRDIENTRANCECODE_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtMRDIENTRANCECODE.SelectionStart == 1)
				Support.SendKeys("{TAB}", false);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2("Property", 145);
				if (Conversion.Val(modGNBas.Statics.response) != 0)
				{
					txtMRDIENTRANCECODE.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
					lblMRDIENTRANCE.Text = Strings.Mid(FCConvert.ToString(modGNBas.Statics.Resp1), 32, 16);
				}
				frmHelp.InstancePtr.Unload();
			}
		}

		private void txtMRDIENTRANCECODE_Leave(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (Conversion.Val(txtMRDIENTRANCECODE.Text) != 0)
				{
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, (Conversion.Val(txtMRDIENTRANCECODE.Text) + 1700));
					modDataTypes.Statics.CR = temp;
					lblMRDIENTRANCE.Text = FCConvert.ToString(modDataTypes.Statics.CR.Get_Fields_String("ClDesc"));
				}
				else
				{
					lblMRDIENTRANCE.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRDIEntranceCode_LostFocus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRDIINFORMATION_Enter(object sender, System.EventArgs e)
		{
			// OverType = 0
			try
			{
				// On Error GoTo ErrorHandler
				txtMRDIINFORMATION.SelectionStart = 0;
				txtMRDIINFORMATION.SelectionLength = modGNBas.Statics.OverType;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRDIInformation_GotFocus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRDIINFORMATION_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtMRDIINFORMATION.SelectionStart == 1)
				Support.SendKeys("{TAB}", false);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles_2("Property", 146);
				if (Conversion.Val(modGNBas.Statics.response) != 0)
				{
					txtMRDIINFORMATION.Text = Strings.Trim(FCConvert.ToString(modGNBas.Statics.response));
					lblMRDIINFORMATION.Text = Strings.Mid(FCConvert.ToString(modGNBas.Statics.Resp1), 32, 16);
				}
				frmHelp.InstancePtr.Unload();
			}
		}

		private void txtMRDIINFORMATION_Leave(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (Conversion.Val(txtMRDIINFORMATION.Text) != 0)
				{
					clsDRWrapper temp = modDataTypes.Statics.CR;
					modREMain.OpenCRTable(ref temp, (Conversion.Val(txtMRDIINFORMATION.Text) + 1710));
					modDataTypes.Statics.CR = temp;
					lblMRDIINFORMATION.Text = FCConvert.ToString(modDataTypes.Statics.CR.Get_Fields_String("ClDesc"));
				}
				else
				{
					lblMRDIINFORMATION.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRDIInformation_LostFocus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRPIACRES_Enter(object sender, System.EventArgs e)
		{

		}

		private void txtMRPIACRES_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);

            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
            }
		}

		private void txtMRPIACRES_Leave(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (Conversion.Val(txtMRPIACRES.Text) > 0)
				{
					modDataTypes.Statics.MR.Set_Fields("piacres", FCConvert.ToDouble(txtMRPIACRES.Text + ""));
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("piacres", 0);
				}
				FillTreeGrid();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRPIAcres_LostFocus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRPIACRES_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (Conversion.Val(txtMRPIACRES.Text) > 0)
				{
					modDataTypes.Statics.MR.Set_Fields("piacres", FCConvert.ToDouble(txtMRPIACRES.Text + ""));
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("piacres", 0);
				}
				FillTreeGrid();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRPIAcres_Validate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRPIOPEN1_Enter(object sender, System.EventArgs e)
		{
			// OverType = 0
			try
			{
				// On Error GoTo ErrorHandler
				txtMRPIOPEN1.SelectionStart = 0;
				txtMRPIOPEN1.SelectionLength = modGNBas.Statics.OverType;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRPIOpen1_GotFocus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRPIOPEN1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtMRPIOPEN1.SelectionStart == 4)
				Support.SendKeys("{TAB}", false);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
            }
		}

		private void txtMRPIOPEN2_Enter(object sender, System.EventArgs e)
		{
			// OverType = 0
			try
			{
				// On Error GoTo ErrorHandler
				txtMRPIOPEN2.SelectionStart = 0;
				txtMRPIOPEN2.SelectionLength = modGNBas.Statics.OverType;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRPIOpen2_GotFocus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRPIOPEN2_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtMRPIOPEN2.SelectionStart == 4)
				Support.SendKeys("{TAB}", false);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
            }
		}

		private void txtMRRLBLDGVAL_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (txtMRRLBLDGVAL.Text == string.Empty)
					txtMRRLBLDGVAL.Text = "0";
				txtMRRLBLDGVAL.Text = Strings.Format(txtMRRLBLDGVAL.Text, "###,###,##0");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRRlBldgVal_Validate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRRLEXEMPTION_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (txtMRRLEXEMPTION.Text == string.Empty)
					txtMRRLEXEMPTION.Text = "0";
				txtMRRLEXEMPTION.Text = Strings.Format(txtMRRLEXEMPTION.Text, "###,###,##0");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRRlExemption", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRRLLANDVAL_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (modREMain.Statics.boolShortRealEstate)
			{
				if ((KeyCode >= Keys.D0 && KeyCode <= Keys.D9) || (KeyCode >= Keys.NumPad0 && KeyCode <= Keys.NumPad9))
				{
					KeyCode = (Keys)0;
					MessageBox.Show("Land value cannot be edited directly.  You must enter land values in the grid below under the appropriate categories.", "Invalid Action", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
		}

		private void txtMRRLLANDVAL_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (txtMRRLLANDVAL.Text == string.Empty)
					txtMRRLLANDVAL.Text = "0";
				txtMRRLLANDVAL.Text = Strings.Format(txtMRRLLANDVAL.Text, "###,###,##0");
				modDataTypes.Statics.MR.Set_Fields("lastlandval", Convert.ToInt64(FCConvert.ToDouble(txtMRRLLANDVAL.Text + "")));
				FillTreeGrid();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRRlLandVal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		
		private void txtMRRSLOCAPT_Enter(object sender, System.EventArgs e)
		{
			LocStreet = "N";
			// OverType = 0
			txtMRRSLOCAPT.SelectionStart = 0;
			txtMRRSLOCAPT.SelectionLength = modGNBas.Statics.OverType;
		}

		private void txtMRRSLOCAPT_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtMRRSLOCAPT.SelectionStart == 1)
				Support.SendKeys("{TAB}", false);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                modHelpFiles.HelpFiles(FCGlobal.Screen.ActiveControl.Name);
            }
		}

		private void txtMRRSLOCNUMALPH_Enter(object sender, System.EventArgs e)
		{

			txtMRRSLOCNUMALPH.SelectionStart = 0;
			txtMRRSLOCNUMALPH.SelectionLength = modGNBas.Statics.OverType;
		}

		private void txtMRRSLOCNUMALPH_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtMRRSLOCNUMALPH.SelectionStart == 5)
				Support.SendKeys("{TAB}", false);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                KeyCode = (Keys)0;
				modHelpFiles.HelpFiles_2("Property", 9);
				frmHelp.InstancePtr.Unload();
			}
		}

		private void txtMRRSLOCSTREET_Enter(object sender, System.EventArgs e)
		{
			LocStreet = "Y";
			// OverType = 0
			txtMRRSLOCSTREET.SelectionStart = 0;
			txtMRRSLOCSTREET.SelectionLength = modGNBas.Statics.OverType;
		}

		private void txtMRRSLOCSTREET_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtMRRSLOCSTREET.SelectionStart == 26)
				Support.SendKeys("{TAB}", false);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                KeyCode = (Keys)0;
				modHelpFiles.HelpFiles_2("Property", 10);
				frmHelp.InstancePtr.Unload();
			}
		}

		private void txtMRRSMAPLOT_Enter(object sender, System.EventArgs e)
		{
			// OverType = 0
			txtMRRSMAPLOT.SelectionStart = 0;
			txtMRRSMAPLOT.SelectionLength = modGNBas.Statics.OverType;
		}

		private void txtMRRSMAPLOT_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtMRRSMAPLOT.SelectionStart == 17)
				Support.SendKeys("{TAB}", false);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                KeyCode = (Keys)0;
				modHelpFiles.HelpFiles_2("Property", 1);
			}
		}

		private void txtMRRLLANDVAL_Enter(object sender, System.EventArgs e)
		{
			txtMRRLLANDVAL.SelectionStart = 11;
		}

		private void txtMRRLLANDVAL_Leave(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (txtMRRLLANDVAL.Text == "")
					txtMRRLLANDVAL.Text = "0";
				CalculateValues();
				modDataTypes.Statics.MR.Set_Fields("lastlandval", Convert.ToInt64(FCConvert.ToDouble(txtMRRLLANDVAL.Text + "")));
				FillTreeGrid();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtMRRLLandVal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRRLLANDVAL_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
			{
				if (KeyAscii != Keys.Back && KeyAscii != Keys.Escape && KeyAscii != Keys.F16)
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMRRLLANDVAL_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.C)
			{
				txtMRRLLANDVAL.Text = "0";
			}
		}

		private void txtMRRLBLDGVAL_Enter(object sender, System.EventArgs e)
		{
			txtMRRLBLDGVAL.SelectionStart = 11;
		}

		private void txtMRRLBLDGVAL_Leave(object sender, System.EventArgs e)
		{
			if (txtMRRLBLDGVAL.Text == "")
				txtMRRLBLDGVAL.Text = "0";
			CalculateValues();
		}

		private void txtMRRLBLDGVAL_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
			{
				if (KeyAscii != Keys.Back && KeyAscii != Keys.Escape && KeyAscii != Keys.F16)
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMRRLEXEMPTION_Enter(object sender, System.EventArgs e)
		{
			txtMRRLEXEMPTION.SelectionStart = 11;
		}

		private void txtMRRLEXEMPTION_Leave(object sender, System.EventArgs e)
		{
			if (txtMRRLEXEMPTION.Text == "")
				txtMRRLEXEMPTION.Text = "0";
			CalculateValues();
		}

		private void txtMRRLEXEMPTION_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
			{
				if (KeyAscii != Keys.Back && KeyAscii != Keys.Escape && KeyAscii != Keys.F16)
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		
		private void txtMRRSNAME_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int intResponse;
			DateTime dtDate;
			int lngPrice;
			int intType;
			int intFinancing;
			int intValidity;
			int intVerified;
			try
			{
				// On Error GoTo ErrorHandler
				if (boolInValidate)
					return;
				boolInValidate = true;
				
				boolInValidate = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				boolInValidate = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In name_validate line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtMRRSREF1_Enter(object sender, System.EventArgs e)
		{
			// OverType = 0
			txtMRRSREF1.SelectionStart = 0;
			txtMRRSREF1.SelectionLength = modGNBas.Statics.OverType;
		}

		private void txtMRRSREF1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtMRRSREF1.SelectionStart == 44)
				Support.SendKeys("{TAB}", false);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                KeyCode = (Keys)0;
				modHelpFiles.HelpFiles_2("Property", 11);
				frmHelp.InstancePtr.Unload();
			}
		}
		// Private Sub txtMRRSREF1_Validate(Cancel As Boolean)
		// If Mid$(txtMRRSRef1.Text, 1, 1) = "." Then Call TransferAccount(txtMRRSRef1.Text, "REF1")
		// End Sub
		private void txtMRRSREF2_Enter(object sender, System.EventArgs e)
		{
			// OverType = 0
			txtMRRSREF2.SelectionStart = 0;
			txtMRRSREF2.SelectionLength = modGNBas.Statics.OverType;
		}

		private void txtMRRSREF2_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (txtMRRSREF2.SelectionStart == 44)
				Support.SendKeys("{TAB}", false);
            //FC:FINAL:MSH - issue #1651: add an extra comparing to avoid multi handling (in web multiple requests can be handled)
            if (KeyCode == Keys.F9 && !modHelpFiles.Statics.Helping)
            {
                modHelpFiles.Statics.Helping = true;
                KeyCode = (Keys)0;
				modHelpFiles.HelpFiles_2("Property", 12);
				frmHelp.InstancePtr.Unload();
			}
		}
		
		public void CalculateValues()
		{
			// vbPorter upgrade warning: NewLand As int	OnWrite(int, double)
			long NewLand = 0;
			// vbPorter upgrade warning: newbldg As int	OnWrite(int, double)
			long newbldg = 0;
			// vbPorter upgrade warning: newexem As int	OnWrite(int, double)
			long newexem = 0;
			double newAcreage = 0;
			//FileSystemObject fso = new FileSystemObject();
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				
				if (Conversion.Val(txtMRRLLANDVAL.Text) > 0)
				{
					NewLand = Convert.ToInt64(FCConvert.ToDouble(txtMRRLLANDVAL.Text));
				}
				else
				{
					NewLand = 0;
				}
				// Building Valuation
				if (Conversion.Val(txtMRRLBLDGVAL.Text) > 0)
				{
					newbldg = Convert.ToInt64(FCConvert.ToDouble(txtMRRLBLDGVAL.Text));
				}
				else
				{
					newbldg = 0;
					txtMRRLBLDGVAL.Text = "0";
				}
				// Exempt Valuation
				if (Conversion.Val(txtMRRLEXEMPTION.Text) > 0)
				{
					newexem = Convert.ToInt64(FCConvert.ToDouble(txtMRRLEXEMPTION.Text));
				}
				else
				{
					newexem = 0;
				}
				if (!modREMain.Statics.boolShortRealEstate)
				{
					newAcreage = Conversion.Val(txtMRPIACRES.Text);
				}
				else
				{
					newAcreage = Conversion.Val(TreeGrid.TextMatrix(1, 1)) + Conversion.Val(TreeGrid.TextMatrix(1, 2)) + Conversion.Val(TreeGrid.TextMatrix(1, 3)) + Conversion.Val(TreeGrid.TextMatrix(1, 4));
				}
				clsLoad.OpenRecordset("select sum(piacres) as acresum,sum(lastlandval) as landsum,sum(lastbldgval) as bldgsum,sum(rlexemption) as exemptsum from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + " and rscard <> " + FCConvert.ToString(modGNBas.Statics.gintCardNumber), modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
					NewLand += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("landsum")));
					// TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
					newbldg += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("bldgsum")));
					// TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
					newexem += FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("exemptsum")));
					// TODO Get_Fields: Field [acresum] not found!! (maybe it is an alias?)
					newAcreage += Conversion.Val(clsLoad.Get_Fields("acresum"));
				}
				lblMRRLLANDVAL.Text = Strings.Format(NewLand, "#,##0");
				lblMRRLBLDGVAL.Text = Strings.Format(newbldg, "#,##0");
				lblMRRLEXEMPTION.Text = Strings.Format(newexem, "#,##0");
				// lblTotal.Caption = Format((NewLand + newbldg), "#,##0")
				lblNetAssessment.Text = Strings.Format((NewLand + newbldg - newexem), "#,##0");
				lblTotalAcreage.Text = Strings.Format(newAcreage, "#,###,##0.00");
				lblTaxRate.Text = Strings.Format(modGlobalVariables.Statics.CustomizedInfo.BillRate, "00.000");
				if (!File.Exists(FCFileSystem.Statics.UserDataFolder + "\\twbl0000.exe") && Strings.UCase(modGlobalConstants.Statics.MuniName) == "HERMON")
				{
					lblTaxAmount.Text = Strings.Format(Conversion.Int(((modGlobalVariables.Statics.CustomizedInfo.BillRate / 1000) * (NewLand + newbldg - newexem)) * 100) / 100.0, "#,##0.00");
				}
				else
				{
					lblTaxAmount.Text = Strings.Format((modGlobalVariables.Statics.CustomizedInfo.BillRate / 1000) * (NewLand + newbldg - newexem), "#,##0.00");
				}
				if (FCConvert.ToInt16(modSHREBLUpdateMRTEXT.Statics.BillingYear) == 0)
					modSHREBLUpdateMRTEXT.Statics.BillingYear = DateTime.Today.Year;
				TaxRateLabel.Text = Strings.Format(modSHREBLUpdateMRTEXT.Statics.BillingYear, "0000") + " Tax Rate";
				// TaxAmountLabel.Caption = Format(BillingYear, "0000") & " Tax Amount"
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In CalculateValues in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				// takes a function number as a parameter
				// Then gets all the child functions that belong to it and
				// disables the appropriate controls
				clsDRWrapper clsChildList = new clsDRWrapper();
				// Dim dcTemp As New clsDRWrapper
				string strPerm = "";
				int lngChild = 0;
				/* Control ctltemp = new Control(); */
				FCTextBox txttemp;
				// Set clsChildList = New clsDRWrapper
				modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
				DisableMyControls();
				while (!clsChildList.EndOfFile())
				{
					lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
					strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
					switch (lngChild)
					{
						case modGlobalVariables.CNSTDELCARDS:
							{
								if (strPerm == "F")
								{
									mnuDeleteCard.Enabled = true;
									cmdDeleteCard.Enabled = true;
								}
								else
								{
									mnuDeleteCard.Enabled = false;
									cmdDeleteCard.Enabled = false;
								}
								break;
							}
						case modGlobalVariables.CNSTDELACCOUNTS:
							{
								if (strPerm == "F")
								{
									mnuDelete.Enabled = true;
									cmdDelete.Enabled = true;
								}
								else
								{
									mnuDelete.Enabled = false;
									cmdDelete.Enabled = false;
								}
								break;
							}
						case modGlobalVariables.CNSTADDACCOUNTS:
							{
								if (strPerm == "F")
								{
									mnuAddAccount.Enabled = true;
									cmdAddAccount.Enabled = true;
								}
								else
								{
									mnuAddAccount.Enabled = false;
									cmdAddAccount.Enabled = false;
								}
								break;
							}
						case modGlobalVariables.CNSTSAVEACCOUNTS:
							{
								if (strPerm == "F")
								{
									mnuSaveQuit.Enabled = true;
									if (modGlobalConstants.Statics.gboolCL)
									{
										chkTaxAcquired.Enabled = false;
									}
								}
								else
								{
									mnuPendingTransfer.Enabled = false;
									mnuSaveQuit.Enabled = false;
									BPGrid.Editable = FCGrid.EditableSettings.flexEDNone;
									BPGrid.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
									GridExempts.Editable = FCGrid.EditableSettings.flexEDNone;
									GridExempts.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
									TreeGrid.Enabled = false;
									MapLotGrid.Editable = FCGrid.EditableSettings.flexEDNone;
									MapLotGrid.Editable = (FCGrid.EditableSettings)modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION;
									TreeGrid.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
									chkTaxAcquired.Enabled = false;
									chkBankruptcy.Enabled = false;
								}
								break;
							}
						case modGlobalVariables.CNSTCOMMENTS:
							{
								if (strPerm == "N")
								{
									mnuComments.Enabled = false;
									cmdComments.Enabled = false;
									mnuRECLComment.Enabled = false;
									mnuCollectionsNote.Enabled = false;
								}
								else
								{
									mnuComments.Enabled = true;
									cmdComments.Enabled = true;
									mnuRECLComment.Enabled = true;
									mnuCollectionsNote.Enabled = true;
								}
								break;
							}
						case modGlobalVariables.CNSTVIEWPICSKETCH:
							{

								break;
							}
						case modGlobalVariables.CNSTEDITADDRESSES:
							{
								if (strPerm == "F")
								{
									//FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
									//foreach (Control ctltemp in frmSHREBLUpdate.InstancePtr.Controls)
									var controls = frmSHREBLUpdate.InstancePtr.GetAllControls();
									foreach (Control ctltemp in controls)
									{
										// For Each txttemp In frmSHREBLUpdate
										if (FCConvert.ToString(ctltemp.Tag) == "address")
										{
											ctltemp.Enabled = true;
											ctltemp.BackColor = Color.White;
										}
									}
								}
								else
								{
									mnuPendingTransfer.Enabled = false;
									//FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
									//foreach (Control ctltemp in frmSHREBLUpdate.InstancePtr.Controls)
									var controls = frmSHREBLUpdate.InstancePtr.GetAllControls();
									foreach (Control ctltemp in controls)
									{
										if (FCConvert.ToString(ctltemp.Tag) == "address")
										{
											ctltemp.Enabled = false;
										}
									}
								}
								break;
							}
						case modGlobalVariables.CNSTEDITLANDBLDGEXMT:
							{
								if (strPerm == "F")
								{
									txtMRRLLANDVAL.Enabled = true;
									txtMRRLLANDVAL.BackColor = Color.White;
									txtMRRLBLDGVAL.Enabled = true;
									txtMRRLBLDGVAL.BackColor = Color.White;
									txtMRRLEXEMPTION.Enabled = true;
									txtMRRLEXEMPTION.BackColor = Color.White;
								}
								else
								{
									mnuPendingTransfer.Enabled = false;
								}
								break;
							}
					}
					//end switch
					clsChildList.MoveNext();
				}
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTEDITSALEINFO));
				if (strPerm == "F")
				{
					//FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
					//foreach (Control ctltemp in frmSHREBLUpdate.InstancePtr.Controls)
					var controls = frmSHREBLUpdate.InstancePtr.GetAllControls();
					foreach (Control ctltemp in controls)
					{
						if (FCConvert.ToString(ctltemp.Tag) == "sale")
						{
							if (modREMain.Statics.boolShortRealEstate)
							{
								ctltemp.Enabled = true;
								ctltemp.BackColor = Color.White;
							}
							else
							{
								ctltemp.Enabled = false;
							}
						}
					}
					// ctltemp
				}
				else
				{
					//FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
					//foreach (Control ctltemp in frmSHREBLUpdate.InstancePtr.Controls)
					var controls = frmSHREBLUpdate.InstancePtr.GetAllControls();
					foreach (Control ctltemp in controls)
					{
						if (FCConvert.ToString(ctltemp.Tag) == "sale")
						{
							ctltemp.Enabled = false;
						}
					}
					// ctltemp
				}
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTEDITPROPERTY));
				if (strPerm == "F")
				{
					booleditland = true;
					//FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
					//foreach (Control ctltemp in frmSHREBLUpdate.InstancePtr.Controls)
					var controls = frmSHREBLUpdate.InstancePtr.GetAllControls();
					foreach (Control ctltemp in controls)
					{
						if (FCConvert.ToString(ctltemp.Tag) == "land")
						{
							ctltemp.Enabled = true;
							ctltemp.BackColor = Color.White;
						}
					}
					// ctltemp
				}
				else
				{
					booleditland = false;
					//FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
					//foreach (Control ctltemp in frmSHREBLUpdate.InstancePtr.Controls)
					var controls = frmSHREBLUpdate.InstancePtr.GetAllControls();
					foreach (Control ctltemp in controls)
					{
						if (FCConvert.ToString(ctltemp.Tag) == "land")
						{
							ctltemp.Enabled = false;
							GridExempts.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
							GridExempts.Enabled = true;
							GridExempts.Editable = FCGrid.EditableSettings.flexEDNone;
							BPGrid.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						}
					}
					// ctltemp
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In HandlePartialPermission in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void DisableMyControls()
		{
			/* Control ctltemp = new Control(); */
			try
			{
				// On Error GoTo ErrorHandler
				//FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
				//foreach (Control ctltemp in frmSHREBLUpdate.InstancePtr.Controls)
				var controls = frmSHREBLUpdate.InstancePtr.GetAllControls();
				foreach (Control ctltemp in controls)
				{
					if ((ctltemp is FCTextBox) && Strings.UCase(ctltemp.GetName()) != "TXTCARDNUMBER")
					{
						ctltemp.Enabled = false;
						// ctltemp.BackColor = frmSHREBLUpdate.BackColor
					}
					else if (ctltemp is Global.T2KPhoneNumberBox)
					{
						ctltemp.Enabled = false;
						// ctltemp.BackColor = frmSHREBLUpdate.BackColor
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In DisableMyControls", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupTreeGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				TreeGrid.TextMatrix(0, 1, "Soft");
				TreeGrid.TextMatrix(0, 3, "Hard");
				TreeGrid.TextMatrix(0, 2, "Mixed");
				TreeGrid.TextMatrix(0, 4, "Other");
				TreeGrid.TextMatrix(1, 0, "Acres");
				TreeGrid.TextMatrix(2, 0, "Value");
				//TreeGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 4, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				//TreeGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 1, 2, 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
				if (!modREMain.Statics.boolShortRealEstate)
				{
					TreeGrid.Editable = FCGrid.EditableSettings.flexEDNone;
					TreeGrid.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
				}
				ResizeTreeGrid();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupTreeGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeTreeGrid()
		{
			int GridWidth;
			int BoxWidth;
			try
			{
				// On Error GoTo ErrorHandler
				GridWidth = TreeGrid.WidthOriginal;
				TreeGrid.ColWidth(0, FCConvert.ToInt32(0.2 * GridWidth));
				TreeGrid.ColWidth(1, FCConvert.ToInt32(0.197 * GridWidth));
				TreeGrid.ColWidth(2, FCConvert.ToInt32(0.197 * GridWidth));
				TreeGrid.ColWidth(3, FCConvert.ToInt32(0.197 * GridWidth));
				TreeGrid.ColWidth(4, FCConvert.ToInt32(0.2 * GridWidth));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ResizeTreeGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeSaleGrid()
		{
			int GridWidth = 0;
			try
			{
				// On Error GoTo ErrorHandler
				GridWidth = SaleGrid.WidthOriginal;
				// .ColWidth(0) = 0.3 * GridWidth
				// .Height = .RowHeight(0) * 7 + 50
				SaleGrid.ColWidth(CNSTSALECOLPRICE, FCConvert.ToInt32(0.1 * GridWidth));
				SaleGrid.ColWidth(CNSTSALECOLDATE, FCConvert.ToInt32(0.1 * GridWidth));
				SaleGrid.ColWidth(CNSTSALECOLSALE, FCConvert.ToInt32(0.2 * GridWidth));
				SaleGrid.ColWidth(CNSTSALECOLFINANCING, FCConvert.ToInt32(0.2 * GridWidth));
				SaleGrid.ColWidth(CNSTSALECOLVERIFIED, FCConvert.ToInt32(0.2 * GridWidth));
				//SaleGrid.HeightOriginal = SaleGrid.RowHeight(0) + SaleGrid.RowHeight(1) * 2 + 50;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ResizeSaleGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillExemptGrid()
		{
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
				{
					if (booleditland)
					{
						GridExempts.ForeColor = Color.Black;
						GridExempts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					SetupGridExempts();
					for (x = 1; x <= 3; x++)
					{
						// TODO Get_Fields: Field [riexemptcd] not found!! (maybe it is an alias?)
						if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields("riexemptcd" + FCConvert.ToString(x)) + "") > 0)
						{
							// TODO Get_Fields: Field [riexemptcd] not found!! (maybe it is an alias?)
							GridExempts.TextMatrix(x, CNSTEXEMPTCOLCODE, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("riexemptcd" + FCConvert.ToString(x)) + "")));
							// TODO Get_Fields: Field [ExemptPct] not found!! (maybe it is an alias?)
							GridExempts.TextMatrix(x, CNSTEXEMPTCOLPERCENT, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("ExemptPct" + FCConvert.ToString(x)) + "")));
							// TODO Get_Fields: Field [exemptval] not found!! (maybe it is an alias?)
							lngExVal[x] = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("exemptval" + FCConvert.ToString(x)) + "")));
						}
						else
						{
							GridExempts.TextMatrix(x, CNSTEXEMPTCOLCODE, FCConvert.ToString(CNSTNOEXEMPT));
							GridExempts.TextMatrix(x, CNSTEXEMPTCOLPERCENT, FCConvert.ToString(100));
							lngExVal[x] = 0;
						}
					}
					// x
				}
				else
				{
					GridExempts.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
					GridExempts.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillExemptGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillSaleGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strList = "";
			string strData = "";
			string strCode = "";
			try
			{
				// On Error GoTo ErrorHandler
				SetupSaleGrid();
				SaleGrid.TextMatrix(2, CNSTSALECOLPRICE, Strings.Format(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleprice") + "")), "#,###,###,##0"));
				// .TextMatrix(2, CNSTSALECOLPRICE) = Val(MR.Fields("pisaleprice") & "")
				if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("saledate")))
				{
					//FC:FINAL:MSH - i.issue #1026: incorrect convert from datetime to int
					//if (FCConvert.ToInt32(modDataTypes.MR.Get_Fields("saledate")) != 0)
					if (modDataTypes.Statics.MR.Get_Fields_DateTime("saledate").ToOADate() != 0)
					{
						SaleGrid.TextMatrix(2, CNSTSALECOLDATE, Strings.Format(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate"), "MM/dd/yyyy"));
					}
					else
					{
						SaleGrid.TextMatrix(2, CNSTSALECOLDATE, "");
					}
				}
				else
				{
					SaleGrid.TextMatrix(2, CNSTSALECOLDATE, "");
				}
				clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1351 and 1359 order by crecordnumber", modGlobalVariables.strREDatabase);
				strList = "#0;N/A";
				while (!clsLoad.EndOfFile())
				{
					if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
					{
						strCode = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1350) + " ";
					}
					else
					{
						strCode = "";
					}
					strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1350) + ";" + strCode + clsLoad.Get_Fields_String("cldesc");
					clsLoad.MoveNext();
				}
				SaleGrid.ColComboList(CNSTSALECOLSALE, strList);
				SaleGrid.TextMatrix(2, CNSTSALECOLSALE, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaletype") + "")));
				clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1361 and 1369 order by crecordnumber", modGlobalVariables.strREDatabase);
				strList = "#0;N/A";
				while (!clsLoad.EndOfFile())
				{
					if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
					{
						strCode = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1360) + " ";
					}
					else
					{
						strCode = "";
					}
					strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1360) + ";" + strCode + clsLoad.Get_Fields_String("cldesc");
					clsLoad.MoveNext();
				}
				SaleGrid.ColComboList(CNSTSALECOLFINANCING, strList);
				SaleGrid.TextMatrix(2, CNSTSALECOLFINANCING, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalefinANCING") + "")));
				clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1381 and 1389 order by crecordnumber", modGlobalVariables.strREDatabase);
				strList = "#0;N/A";
				while (!clsLoad.EndOfFile())
				{
					if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
					{
						strCode = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1380) + " ";
					}
					else
					{
						strCode = "";
					}
					strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1380) + ";" + strCode + clsLoad.Get_Fields_String("cldesc");
					clsLoad.MoveNext();
				}
				SaleGrid.ColComboList(CNSTSALECOLVALIDITY, strList);
				SaleGrid.TextMatrix(2, CNSTSALECOLVALIDITY, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisalevalidity") + "")));
				clsLoad.OpenRecordset("select * from costrecord where CRECORDNUMBER between 1371 and 1379 order by crecordnumber", modGlobalVariables.strREDatabase);
				strList = "#0;N/A";
				while (!clsLoad.EndOfFile())
				{
					if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
					{
						strCode = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1370) + " ";
					}
					else
					{
						strCode = "";
					}
					strList += "|#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("crecordnumber")) - 1370) + ";" + strCode + clsLoad.Get_Fields_String("cldesc");
					clsLoad.MoveNext();
				}
				SaleGrid.ColComboList(CNSTSALECOLVERIFIED, strList);
				SaleGrid.TextMatrix(2, CNSTSALECOLVERIFIED, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("pisaleverified") + "")));
				if (!modREMain.Statics.boolShortRealEstate)
				{
					SaleGrid.Row = 2;
					SaleGrid.Col = 0;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillSaleGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillTreeGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				SetupTreeGrid();
				TreeGrid.TextMatrix(1, 1, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rssoft") + "")));
				TreeGrid.TextMatrix(1, 3, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rshard") + "")));
				TreeGrid.TextMatrix(1, 2, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rsmixed") + "")));
				TreeGrid.TextMatrix(2, 1, Strings.Format(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rssoftvalue") + "")), "#,###,##0"));
				TreeGrid.TextMatrix(2, 3, Strings.Format(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rshardvalue") + "")), "#,###,##0"));
				TreeGrid.TextMatrix(2, 2, Strings.Format(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rsmixedvalue") + "")), "#,###,##0"));
				if (modREMain.Statics.boolShortRealEstate)
				{
					if (fecherFoundation.FCUtils.IsNull(modDataTypes.Statics.MR.Get_Fields_Double("rsother")) || fecherFoundation.FCUtils.IsNull(modDataTypes.Statics.MR.Get_Fields_Int32("rsothervalue")))
					{
						if ((Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rssoftvalue") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rshardvalue") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rsmixedvalue") + "")) <= 0)
						{
							modDataTypes.Statics.MR.Set_Fields("rsothervalue", FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + "")));
							modDataTypes.Statics.MR.Set_Fields("rsother", FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piacres") + "")));
							modDataTypes.Statics.MR.Set_Fields("rssoftvalue", 0);
							modDataTypes.Statics.MR.Set_Fields("rsmixedvalue", 0);
							modDataTypes.Statics.MR.Set_Fields("rshardvalue", 0);
							modDataTypes.Statics.MR.Set_Fields("rssoft", 0);
							modDataTypes.Statics.MR.Set_Fields("rsmixed", 0);
							modDataTypes.Statics.MR.Set_Fields("rshard", 0);
						}
						else
						{
							modDataTypes.Statics.MR.Set_Fields("rsothervalue", Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + "") - (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rssoftvalue") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rsmixedvalue") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rshardvalue") + "")));
							modDataTypes.Statics.MR.Set_Fields("rsother", Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piacres") + "") - (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rssoft") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rsmixed") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rshard") + "")));
						}
					}
					else
					{
						txtMRPIACRES.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rssoft") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rshard") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rsmixed") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rsother") + ""), "#,###,##0.00");
						modDataTypes.Statics.MR.Set_Fields("lastlandval", modGlobalRoutines.RERound_2(FCConvert.ToInt32(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rssoftvalue") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rshardvalue") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rsmixedvalue") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rsothervalue") + ""))));
						txtMRRLLANDVAL.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + ""), "#,###,###,##0");
						modDataTypes.Statics.MR.Set_Fields("piacres", Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rssoft") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rshard") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rsmixed") + "") + Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rsother") + ""));
					}
					TreeGrid.TextMatrix(1, 4, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rsother") + "")));
					TreeGrid.TextMatrix(2, 4, Strings.Format(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rsothervalue") + "")), "#,###,##0"));
				}
				else
				{

					TreeGrid.TextMatrix(1, 4, Strings.Format(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("rsother") + "")), "0.00"));
					TreeGrid.TextMatrix(2, 4, Strings.Format(FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rsothervalue") + "")), "#,###,###,##0"));
				}
				TreeGrid.Row = 0;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In FillTreeGrid in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtOwnerID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtOwnerID.Text) != theAccount.OwnerID)
			{
				cParty tParty = new cParty();
				cPartyController tCont = new cPartyController();
				if (!tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txtOwnerID.Text))))
				{
					e.Cancel = true;
					MessageBox.Show("That party doesn't exist.", "Invalid Party", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtOwnerID.Text = FCConvert.ToString(theAccount.OwnerID);
					return;
				}

				HandleOwnerChange(txtOwnerID.Text.ToIntegerValue());
			}
		}
		
		private void txtPreviousOwner_Enter(object sender, System.EventArgs e)
		{
			txtPreviousOwner.SelectionStart = 0;
			txtPreviousOwner.SelectionLength = modGNBas.Statics.OverType;
		}

		private void UpDownCard_ValueChanged(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				boolCardChangedByProg = true;

				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In UpDownCard_Change", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveMapLotPending(ref int lngPendID, ref DateTime dtPendingDate, ref bool boolfirstChange)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			string strSQL = "";
			string strSale = "";
			int x;
			string strBool = "";
			if (!boolMapLotGridChanged)
				return;
			clsSave.Execute("delete from pendingchanges where transactionnumber = " + FCConvert.ToString(lngPendID) + " and tablename = 'MapLot'", modGlobalVariables.strREDatabase, false);
			for (x = 1; x <= MapLotGrid.Rows - 1; x++)
			{
				if (x >= MapLotGrid.Rows)
					break;
				if (Strings.Trim(MapLotGrid.TextMatrix(x, CNSTMAPLOTCOLMAP)) == string.Empty)
				{
					MapLotGrid.RemoveItem(x);
				}
			}
			// x
			if (MapLotGrid.Rows > 1)
			{
				if (Strings.Trim(MapLotGrid.TextMatrix(MapLotGrid.Rows - 1, CNSTMAPLOTCOLMAP)) == string.Empty)
				{
					MapLotGrid.RemoveItem(MapLotGrid.Rows - 1);
				}
			}
			// now put the blank line back
			MapLotGrid.AddItem("");
			if (boolfirstChange || lngPendID == 0)
			{
				clsSave.OpenRecordset("select * from pendingchanges where transactionnumber = -3", modGlobalVariables.strREDatabase);
				clsSave.AddNew();
				lngPendID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("id"));
				boolfirstChange = false;
			}
			for (x = 1; x <= MapLotGrid.Rows - 2; x++)
			{
				strSQL = "Insert into pendingchanges (TransactionNumber,Account,Card,CardId,TableName,mindate,FieldName,FieldValue,FieldType)";
				strSQL += " values (" + FCConvert.ToString(lngPendID) + "," + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + "," + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "," + modDataTypes.Statics.MR.Get_Fields_Int32("id") + ",'MapLot',#" + FCConvert.ToString(dtPendingDate) + "#";
				clsSave.Execute(strSQL + ",'MapLot','" + MapLotGrid.TextMatrix(x, CNSTMAPLOTCOLMAP) + "',10) ", modGlobalVariables.strREDatabase, false);
				clsSave.Execute(strSQL + ",'MPDate','" + MapLotGrid.TextMatrix(x, CNSTMAPLOTCOLMPDATE) + "',10) ", modGlobalVariables.strREDatabase, false);
			}
			// x
		}

		private void SaveBPPending(ref int lngPendID, ref DateTime dtPendingDate, ref bool boolfirstChange)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			string strSQL = "";
			// vbPorter upgrade warning: strSale As string	OnWrite(string, short)
			string strSale = "";
			int x;
			string strBool = "";
			if (!boolBookPageChanged)
				return;
			clsSave.Execute("delete from pendingchanges where transactionnumber = " + FCConvert.ToString(lngPendID) + " and tablename = 'bookpage'", modGlobalVariables.strREDatabase, false);
			for (x = 1; x <= BPGrid.Rows - 1; x++)
			{
				if (x >= BPGrid.Rows)
					break;
				if (Strings.Trim(BPGrid.TextMatrix(x, BPBookCol)) == "" || Strings.Trim(BPGrid.TextMatrix(x, BPPageCol)) == "")
				{
					BPGrid.RemoveItem(x);
				}
			}
			// x
			if (BPGrid.Rows > 1)
			{
				if (Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) == "" || Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) == "")
					BPGrid.RemoveItem(BPGrid.Rows - 1);
			}

			BPGrid.AddItem(FCConvert.ToString(true));
			if (!fecherFoundation.FCUtils.IsEmptyDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate")))
			{
				//FC:FINAL:MSH - i.issue #1026: incorrect converting from datetime to int
				//if (FCConvert.ToInt32(modDataTypes.MR.Get_Fields("saledate")) != 0)
				if (FCConvert.ToDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") as object).ToOADate() != 0)
				{
					strSale = modDataTypes.Statics.MR.Get_Fields_DateTime("saledate") + "";
				}
				else
				{
					strSale = FCConvert.ToString(0);
				}
			}
			else
			{
				strSale = FCConvert.ToString(0);
			}
			if (boolfirstChange || lngPendID == 0)
			{
				clsSave.OpenRecordset("select * from pendingchanges where transactionnumber = -3", modGlobalVariables.strREDatabase);
				clsSave.AddNew();
				lngPendID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("id"));
				boolfirstChange = false;
			}
			for (x = 1; x <= BPGrid.Rows - 2; x++)
			{
				strSQL = "Insert into pendingchanges (TransactionNumber,Account,Card,CardId,TableName,mindate,FieldName,FieldValue,FieldType)";
				strSQL += " values (" + FCConvert.ToString(lngPendID) + "," + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber) + "," + FCConvert.ToString(modGNBas.Statics.gintCardNumber) + "," + modDataTypes.Statics.MR.Get_Fields_Int32("id") + ",'BookPage',#" + FCConvert.ToString(dtPendingDate) + "#";
				clsSave.Execute(strSQL + ",'Book','" + BPGrid.TextMatrix(x, BPBookCol) + "',10)", modGlobalVariables.strREDatabase, false);
				clsSave.Execute(strSQL + ",'Page','" + BPGrid.TextMatrix(x, BPPageCol) + "',10)", modGlobalVariables.strREDatabase, false);
				clsSave.Execute(strSQL + ",'BPDate','" + Strings.Trim(BPGrid.TextMatrix(x, BPSaleDateCol)) + "',10)", modGlobalVariables.strREDatabase, false);
				clsSave.Execute(strSQL + ",'SaleDate','" + strSale + "',8)", modGlobalVariables.strREDatabase, false);
				if (BPGrid.TextMatrix(x, BPCheckCol) == string.Empty)
				{
					//strBool = "FALSE";
                    strBool = "0";
                }
				else
				{
					if (FCConvert.CBool(BPGrid.TextMatrix(x, BPCheckCol)) == true)
					{
						//strBool = "TRUE";
                        strBool = "1";
                    }
					else
					{
						//strBool = "FALSE";
                        strBool = "0";
                    }
				}
				clsSave.Execute(strSQL + ",'Current','" + strBool + "',1)", modGlobalVariables.strREDatabase, false);
			}
			// x
		}

		private void FillMapLotPending(ref int lngPendID)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from pendingchanges where transactionnumber = " + FCConvert.ToString(lngPendID) + " and tablename = 'MapLot' order by id", modGlobalVariables.strREDatabase);
			if (clsLoad.EndOfFile())
				return;
			MapLotGrid.Rows = 1;
			while (!clsLoad.EndOfFile())
			{
				if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("fieldname"))) == "MAPLOT")
				{
					// new record
					MapLotGrid.Rows += 1;
					MapLotGrid.TextMatrix(MapLotGrid.Rows - 1, CNSTMAPLOTCOLMAP, FCConvert.ToString(clsLoad.Get_Fields_String("fieldvalue")));
				}
				else if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("fieldname"))) == "MPDATE")
				{
					MapLotGrid.TextMatrix(MapLotGrid.Rows - 1, CNSTMAPLOTCOLMPDATE, FCConvert.ToString(clsLoad.Get_Fields_String("fieldvalue")));
				}
				clsLoad.MoveNext();
			}
			MapLotGrid.AddItem("");
			AutoPositionMapLotGrid();
		}

		private void FillBPPending(ref int lngPendID)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from pendingchanges where transactionnumber = " + FCConvert.ToString(lngPendID) + " and tablename = 'bookpage' order by id", modGlobalVariables.strREDatabase);
			if (clsLoad.EndOfFile())
				return;
			BPGrid.Rows = 1;
			// by ordering by id the records will be in order of line and the corresponding page will be after the correct book etc.
			while (!clsLoad.EndOfFile())
			{
				if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("fieldname"))) == "BOOK")
				{
					// new record
					BPGrid.Rows += 1;
					BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol, FCConvert.ToString(clsLoad.Get_Fields_String("fieldvalue")));
				}
				else if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("fieldname"))) == "PAGE")
				{
					BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol, FCConvert.ToString(clsLoad.Get_Fields_String("fieldvalue")));
				}
				else if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("fieldname"))) == "BPDATE")
				{
					BPGrid.TextMatrix(BPGrid.Rows - 1, BPSaleDateCol, FCConvert.ToString(clsLoad.Get_Fields_String("fieldvalue")));
				}
				else if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("fieldname"))) == "CURRENT")
				{
					BPGrid.TextMatrix(BPGrid.Rows - 1, BPCheckCol, FCConvert.ToString(FCConvert.CBool(clsLoad.Get_Fields_String("fieldvalue"))));
				}
				clsLoad.MoveNext();
			}
			// add the blank line
			BPGrid.AddItem(FCConvert.ToString(true));
			AutoPositionBPGrid();
		}

		private void LoadPendingInfo(ref int lngPendID)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from pendingchanges where TRANSACtionnumber = " + FCConvert.ToString(lngPendID) + " order by id", modGlobalVariables.strREDatabase);
			while (!clsLoad.EndOfFile())
			{
				FillPendingStuff_8(clsLoad.Get_Fields_String("tablename"), clsLoad.Get_Fields_String("fieldname"), clsLoad.Get_Fields_String("fieldvalue"));
				clsLoad.MoveNext();
			}
		}

		private void FillPendingStuff_8(string strTableName, string strFieldName, string strValue)
		{
			FillPendingStuff(ref strTableName, ref strFieldName, strValue);
		}

		private void FillPendingStuff(ref string strTableName, ref string strFieldName, string strValue)
		{
			if (Strings.UCase(strTableName) == "BOOKPAGE")
			{
				// do nothing
			}
			else if (Strings.UCase(strTableName) == "MAPLOT")
			{
				// do nothing
			}
			else if (Strings.UCase(strTableName) == "MASTER")
			{
				modDataTypes.Statics.MR.Set_Fields(strFieldName, strValue);
			}
			else if (Strings.UCase(strTableName) == "DWELLING")
			{
			}
			else if (Strings.UCase(strTableName) == "COMMERCIAL")
			{
			}
			else if (Strings.UCase(strTableName) == "OUTBUILDING")
			{
			}
		}

		public void Init(int lngAcct, short intCard, bool boolInPending, int lngPendingID = 0)
		{
			try
			{
				// On Error GoTo ErrorHandler
				modGlobalVariables.Statics.boolInPendingMode = boolInPending;
				lngCurrentPendingID = lngPendingID;
				/*? 3 */
				modGlobalVariables.Statics.gintLastAccountNumber = lngAcct;
				modGNBas.Statics.gintCardNumber = intCard;
				boolShown = false;
				if (boolInPending && lngPendingID > 0)
				{
					mnuPrint.Visible = false;
					mnuOptions.Visible = false;
					cardNumber.Enabled = false;
					/*? 11 */// VScrollCard.Enabled = False
					//UpDownCard.Enabled = false;
				}
				SetupBPGrid();
				SetupMapLotGrid();
				// load current info
				FillTheScreen();
				// load pending info
				if (boolInPending)
				{
					LoadPendingInfo(ref lngPendingID);
				}
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					// 18    strOriginalName = Trim(MR.Fields("rsname") & "")
					lngOriginalOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("OwnerPartyID"))));
					ShowOwner();
					ShowSecOwner();
					ShowAddress();
                    ShowDeeds();
					REMRToText();
				}
				else
				{
					SaleMRToText();
				}
				FillTreeGrid();
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ritrancode") + "")));
				}
				CalculateValues();
				FillBPPending(ref lngPendingID);
				FillMapLotPending(ref lngPendingID);

				this.Show(App.MainForm);
				modGNBas.Statics.gboolLoadingDone = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Init in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillTheScreen()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			int costnumber;
			try
			{
				// On Error GoTo ErrorHandler
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					framSaleRecord.Visible = false;
					//FC:FINAL:RPU: #i1364 - fcPanel5 must be also set visible true or false because some controls from framSaleRecord were moved here
				
					//FC:FINAL:RPU: #i1364 - Fix design
					FixDesign();
				}
				else
				{
					//FC:FINAL:MSH - issue #1687: add identification that the form opened for Sales Record
					HeaderText.Text += " - SALES RECORD";
					framSaleRecord.Visible = true;
					//FC:FINAL:RPU: #i1364 - fcPanel5 must be also set visible true or false because some controls from framSaleRecord were moved here
				
					//FC:FINAL:RPU: #i1364 - Fix design
					FixDesign();
				}
				if (FormLoaded != 'Y')
				{
					FormLoaded = 'Y';
					// 
					modSHREBLUpdateMRTEXT.Statics.BillingRate = FCConvert.ToDouble(Strings.Format(modGlobalVariables.Statics.CustomizedInfo.BillRate, "00.000"));
					modSHREBLUpdateMRTEXT.Statics.BillingYear = FCConvert.ToInt32(Strings.Format(modGlobalVariables.Statics.CustomizedInfo.BillYear, "0000"));
					// 
					if (!(modGlobalVariables.Statics.boolInPendingMode && lngCurrentPendingID > 0))
					{
						modGNBas.Statics.gintCardNumber = 1;
						modGlobalVariables.Statics.intCurrentCard = 1;
					}
					else
					{
						modGlobalVariables.Statics.intCurrentCard = modGNBas.Statics.gintCardNumber;
					}
					if (modGlobalVariables.Statics.gintLastAccountNumber == 0)
					{

					}
					else
					{
						clsDRWrapper temp1 = modDataTypes.Statics.MR;
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							modREMain.OpenMasterTable(ref temp1, modGlobalVariables.Statics.gintLastAccountNumber);
							// strAccountID = MR.GUIDToString(MR.Fields("accountid"))
							strAccountID = FCConvert.ToString(temp1.Get_Fields_String("accountid"));
							modGNBas.Statics.gintMaxCards = FCConvert.ToInt32(modREMain.GetRecordCount("Master", "RSAccount"));
							LoadAccount(modGlobalVariables.Statics.gintLastAccountNumber);
						}
						else
						{
							modREMain.OpenMasterTable_36(ref temp1, modGlobalVariables.Statics.gintLastAccountNumber, modGlobalVariables.Statics.glngSaleID);
							modGNBas.Statics.gintMaxCards = FCConvert.ToInt32(modREMain.GetRecordCount("SRMaster", "RSAccount", modGlobalVariables.Statics.glngSaleID));
						}
						modDataTypes.Statics.MR = temp1;
						if (!(modGlobalVariables.Statics.boolInPendingMode && lngCurrentPendingID > 0))
						{
							cardNumber.Text = FCConvert.ToString(1);
							/*? 17 */// VScrollCard.Value = 1
							//UpDownCard.Value = 1;
							cardNumber_SelectedIndexChanged(new object(), new EventArgs());
						}
						else
						{
							cardNumber.Text = FCConvert.ToString(modGNBas.Statics.gintCardNumber);
						}
						clsDRWrapper temp2 = modDataTypes.Statics.MR;
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							modREMain.OpenMasterTable(ref temp2, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
							strAccountID = FCConvert.ToString(temp2.Get_Fields_String("accountid"));
						}
						else
						{
							modREMain.OpenMasterTable_27(ref temp2, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, modGlobalVariables.Statics.glngSaleID);
						}
						modDataTypes.Statics.MR = temp2;
						temp2 = modDataTypes.Statics.DWL;
						modREMain.OpenDwellingTable_27(ref temp2, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber, strAccountID);
						modDataTypes.Statics.DWL = temp2;
						temp2 = modDataTypes.Statics.CMR;
						modREMain.OpenCOMMERCIALTable(ref temp2, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
						modDataTypes.Statics.CMR = temp2;
						temp2 = modDataTypes.Statics.OUT;
						modREMain.OpenOutBuildingTable(ref temp2, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
						modDataTypes.Statics.OUT = temp2;
						if (modGlobalVariables.Statics.boolRegRecords)
						{
							clsTemp.OpenRecordset("Select * from commrec where crecordnumber > 0 and crecordnumber = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
							if (!clsTemp.EndOfFile())
							{
								if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("ccomment"))) != "")
								{
									lblComment.Visible = true;
								}
								else
								{
									lblComment.Visible = false;
								}
							}
							else
							{
								lblComment.Visible = false;
							}
						}
						else
						{
							lblComment.Visible = false;
						}
					}
					SetupGridTranCode();
					SetupGridBldgCode();
					SetupGridLandCode();
					SetupGridPropertyCode();
					/*? 33 */// strOriginalName = Trim(MR.Fields("rsname") & "")
					if (modGlobalVariables.Statics.boolRegRecords)
					{
						lngOriginalOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("OwnerPartyID"))));
						ShowOwner();
						ShowAddress();
						ShowSecOwner();
                        ShowDeeds();             
						REMRToText();
					}
					else
					{
						SaleMRToText();
					}
					FillTreeGrid();
					FillBPGrid();
					FillMapLotGrid();
					FillExemptGrid();
					FillSaleGrid();
				}
				// If boolShown Then frmSHREBLUpdate.Refresh
				// 
				// open costfiles to get open 1 & open 2 long descriptions
				costnumber = 1330;
				// Get #30, costnumber, CR
				clsDRWrapper temp = modDataTypes.Statics.CR;
				modREMain.OpenCRTable(ref temp, costnumber);
				Open1Label.Text = Strings.StrConv(temp.Get_Fields_String("ClDesc"), VbStrConv.ProperCase);
                costnumber = 1340;
				// Get #30, costnumber, CR
				modREMain.OpenCRTable(ref temp, costnumber);
				modDataTypes.Statics.CR = temp;
				Open2Label.Text = Strings.StrConv(modDataTypes.Statics.CR.Get_Fields_String("ClDesc"), VbStrConv.ProperCase);
                // Close #30
                // end of open 1 and open 2 long descriptions
                // 
                if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ritrancode") + "")));
				}
				CalculateValues();
				InNameField = "Y";
				if (boolShown && txtMRRSMAPLOT.Enabled && !(modGlobalVariables.Statics.boolInPendingMode && lngCurrentPendingID > 0))
				{
					txtMRRSMAPLOT.Focus();
				}
				ShowPicture = "N".ToCharArray();

				if (modSHREBLUpdateMRTEXT.Statics.CrdCnt == 1)
				{
					mnuGotoNextCard.Enabled = false;
					mnuGotoPreviousCard.Enabled = false;
					mnuGotoLastCard.Enabled = false;
					mnuGotoFirstCard.Enabled = false;
				}
				else
				{
					mnuGotoNextCard.Enabled = true;
					mnuGotoPreviousCard.Enabled = true;
					mnuGotoLastCard.Enabled = true;
					mnuGotoFirstCard.Enabled = true;
				}
				// End If
				SetupTreeGrid();
				if (chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked && modGlobalConstants.Statics.gboolCL)
				{
					chkBankruptcy.Enabled = false;
				}
				FillControlInformationClass();
				int counter;
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
                //FC:FINAL:AM:#3311 - set the focus
                txtMRRSMAPLOT.Focus();
                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillTheScreen in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

        private void SetupCardNumber()
        {
            cardNumber.Clear();
            clsDRWrapper tLoad = new clsDRWrapper();
            tLoad.OpenRecordset("select rscard from master where rsaccount = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), modGlobalVariables.strREDatabase);
            if (!tLoad.EndOfFile())
            {
                modGNBas.Statics.gintMaxCards = FCConvert.ToInt32(Math.Round(Conversion.Val(tLoad.RecordCount())));
                while (!tLoad.EndOfFile())
                {
                    cardNumber.AddItem(tLoad.Get_Fields_Int32("rscard").ToString());
                    tLoad.MoveNext();
                }
                cardNumber.Text = cardNumber.Items[0].ToString();
            }
            else
            {
				cardNumber.AddItem("1");
                cardNumber.Text = "1";
            }
        }


        private void SetupGridTranCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select * from tbltrancode where code = 0", modGlobalVariables.strREDatabase);
				if (clsLoad.EndOfFile())
				{
					if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
					{
						strTemp = "#0;None" + "\t" + "0|";
					}
					else
					{
						strTemp = "#0;Combined" + "\t" + "0|";
					}
				}
				clsLoad.OpenRecordset("select * from tblTranCode order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTranCode.ColComboList(0, strTemp);
				gridTranCode.TextMatrix(0, 0, FCConvert.ToString(0));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridTranCode in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridLandCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			string strDesc = "";
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select * from tbllandcode where code = 0", modGlobalVariables.strREDatabase);
				if (clsLoad.EndOfFile())
				{
					if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
					{
						strTemp = "#0;None" + "\t" + "0|";
					}
					else
					{
						strTemp = "#0;0 None" + "\t" + "0|";
					}
				}
				clsLoad.OpenRecordset("select * from tbllandCode order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					strDesc = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
					if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						strDesc = clsLoad.Get_Fields("code") + " " + strDesc;
					}
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + strDesc + "\t" + clsLoad.Get_Fields("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridLandCode.ColComboList(0, strTemp);
				gridLandCode.TextMatrix(0, 0, FCConvert.ToString(0));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridLandCode in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridBldgCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			string strDesc = "";
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select * from tblbldgcode where code = 0", modGlobalVariables.strREDatabase);
				if (clsLoad.EndOfFile())
				{
					if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
					{
						strTemp = "#0;None" + "\t" + "0|";
					}
					else
					{
						strTemp = "#0;0 None" + "\t" + "0|";
					}
				}
				clsLoad.OpenRecordset("select * from tblbldgCode order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					strDesc = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
					if (modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
					{
						// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
						strDesc = clsLoad.Get_Fields("code") + " " + strDesc;
					}
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + strDesc + "\t" + clsLoad.Get_Fields("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridBldgCode.ColComboList(0, strTemp);
				gridBldgCode.TextMatrix(0, 0, FCConvert.ToString(0));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SetupGridBldgCode in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridPropertyCode()
		{
			string strTemp = "";
			if (!modGlobalVariables.Statics.CustomizedInfo.boolShowCodeOnAccountScreen)
			{
				strTemp = "#0;None" + "\t" + " ";
				strTemp += "|#101;Rural" + "\t" + "Vacant Land|";
				strTemp += "#102;Urban" + "\t" + "Vacant Land|";
				strTemp += "#103;Oceanfront" + "\t" + "Vacant Land|";
				strTemp += "#104;Lake/Pond front" + "\t" + "Vacant Land|";
				strTemp += "#105;Stream/Riverfront" + "\t" + "Vacant Land|";
				strTemp += "#106;Agricultural" + "\t" + "Vacant Land|";
				strTemp += "#107;Commercial Zone" + "\t" + "Vacant Land|";
				strTemp += "#120;Other" + "\t" + "Vacant Land|";
				strTemp += "#201;Rural" + "\t" + "Single Family|";
				strTemp += "#202;Urban" + "\t" + "Single Family|";
				strTemp += "#203;Oceanfront" + "\t" + "Single Family|";
				strTemp += "#204;Lake/Pond front" + "\t" + "Single Family|";
				strTemp += "#205;Stream/Riverfront" + "\t" + "Single Family|";
				strTemp += "#206;Mobile Home" + "\t" + "Single Family|";
				strTemp += "#220;Other" + "\t" + "Single Family|";
				strTemp += "#301;Mixed Use" + "\t" + "Commercial|";
				strTemp += "#302;Apt/2-3 Unit" + "\t" + "Commercial|";
				strTemp += "#303;Apt/4+" + "\t" + "Commercial|";
				strTemp += "#304;Bank" + "\t" + "Commercial|";
				strTemp += "#305;Restaurant" + "\t" + "Commercial|";
				strTemp += "#306;Medical" + "\t" + "Commercial|";
				strTemp += "#307;Office" + "\t" + "Commercial|";
				strTemp += "#308;Retail" + "\t" + "Commercial|";
				strTemp += "#309;Automotive" + "\t" + "Commercial|";
				strTemp += "#310;Marina" + "\t" + "Commercial|";
				strTemp += "#311;Warehouse" + "\t" + "Commercial|";
				strTemp += "#312;Hotel/Motel/Inn" + "\t" + "Commercial|";
				strTemp += "#313;Nursing Home" + "\t" + "Commercial|";
				strTemp += "#314;Shopping Mall" + "\t" + "Commercial|";
				strTemp += "#320;Other" + "\t" + "Commercial|";
				strTemp += "#401;Gas and Oil" + "\t" + "Industrial|";
				strTemp += "#402;Utility" + "\t" + "Industrial|";
				strTemp += "#403;Gravel Pit" + "\t" + "Industrial|";
				strTemp += "#404;Lumber/Saw Mill" + "\t" + "Industrial|";
				strTemp += "#405;Pulp/Paper Mill" + "\t" + "Industrial|";
				strTemp += "#406;Light Manufacture" + "\t" + "Industrial|";
				strTemp += "#407;Heavy Manufacture" + "\t" + "Industrial|";
				strTemp += "#420;Manufacture Other" + "\t" + "Industrial|";
				strTemp += "#501;Government" + "\t" + "Misc Codes|";
				strTemp += "#502;Condominium" + "\t" + "Misc Codes|";
				strTemp += "#503;Timeshare Unit" + "\t" + "Misc Codes|";
				strTemp += "#504;Non-Profit" + "\t" + "Misc Codes|";
				strTemp += "#505;Mobile Home Park" + "\t" + "Misc Codes|";
				strTemp += "#506;Airport" + "\t" + "Misc Codes|";
				strTemp += "#507;Conservation" + "\t" + "Misc Codes|";
				strTemp += "#508;Current Use Classification" + "\t" + "Misc Codes|";
				strTemp += "#520;Other" + "\t" + "Misc Codes";
			}
			else
			{
				strTemp = "#0;0 None" + "\t" + " ";
				strTemp += "|#101;101 Rural" + "\t" + "Vacant Land|";
				strTemp += "#102;102 Urban" + "\t" + "Vacant Land|";
				strTemp += "#103;103 Oceanfront" + "\t" + "Vacant Land|";
				strTemp += "#104;104 Lake/Pond front" + "\t" + "Vacant Land|";
				strTemp += "#105;105 Stream/Riverfront" + "\t" + "Vacant Land|";
				strTemp += "#106;106 Agricultural" + "\t" + "Vacant Land|";
				strTemp += "#107;107 Commercial Zone" + "\t" + "Vacant Land|";
				strTemp += "#120;120 Other" + "\t" + "Vacant Land|";
				strTemp += "#201;201 Rural" + "\t" + "Single Family|";
				strTemp += "#202;202 Urban" + "\t" + "Single Family|";
				strTemp += "#203;203 Oceanfront" + "\t" + "Single Family|";
				strTemp += "#204;204 Lake/Pond front" + "\t" + "Single Family|";
				strTemp += "#205;205 Stream/Riverfront" + "\t" + "Single Family|";
				strTemp += "#206;206 Mobile Home" + "\t" + "Single Family|";
				strTemp += "#220;220 Other" + "\t" + "Single Family|";
				strTemp += "#301;301 Mixed Use" + "\t" + "Commercial|";
				strTemp += "#302;302 Apt/2-3 Unit" + "\t" + "Commercial|";
				strTemp += "#303;303 Apt/4+" + "\t" + "Commercial|";
				strTemp += "#304;304 Bank" + "\t" + "Commercial|";
				strTemp += "#305;305 Restaurant" + "\t" + "Commercial|";
				strTemp += "#306;306 Medical" + "\t" + "Commercial|";
				strTemp += "#307;307 Office" + "\t" + "Commercial|";
				strTemp += "#308;308 Retail" + "\t" + "Commercial|";
				strTemp += "#309;309 Automotive" + "\t" + "Commercial|";
				strTemp += "#310;310 Marina" + "\t" + "Commercial|";
				strTemp += "#311;311 Warehouse" + "\t" + "Commercial|";
				strTemp += "#312;312 Hotel/Motel/Inn" + "\t" + "Commercial|";
				strTemp += "#313;313 Nursing Home" + "\t" + "Commercial|";
				strTemp += "#314;314 Shopping Mall" + "\t" + "Commercial|";
				strTemp += "#320;320 Other" + "\t" + "Commercial|";
				strTemp += "#401;401 Gas and Oil" + "\t" + "Industrial|";
				strTemp += "#402;402 Utility" + "\t" + "Industrial|";
				strTemp += "#403;403 Gravel Pit" + "\t" + "Industrial|";
				strTemp += "#404;404 Lumber/Saw Mill" + "\t" + "Industrial|";
				strTemp += "#405;405 Pulp/Paper Mill" + "\t" + "Industrial|";
				strTemp += "#406;406 Light Manufacture" + "\t" + "Industrial|";
				strTemp += "#407;407 Heavy Manufacture" + "\t" + "Industrial|";
				strTemp += "#420;420 Manufacture Other" + "\t" + "Industrial|";
				strTemp += "#501;501 Government" + "\t" + "Misc Codes|";
				strTemp += "#502;502 Condominium" + "\t" + "Misc Codes|";
				strTemp += "#503;503 Timeshare Unit" + "\t" + "Misc Codes|";
				strTemp += "#504;504 Non-Profit" + "\t" + "Misc Codes|";
				strTemp += "#505;505 Mobile Home Park" + "\t" + "Misc Codes|";
				strTemp += "#506;506 Airport" + "\t" + "Misc Codes|";
				strTemp += "#507;507 Conservation" + "\t" + "Misc Codes|";
				strTemp += "#508;508 Current Use Classification" + "\t" + "Misc Codes|";
				strTemp += "#520;520 Other" + "\t" + "Misc Codes";
			}
			gridPropertyCode.ColComboList(0, strTemp);
			gridPropertyCode.TextMatrix(0, 0, FCConvert.ToString(0));
		}

		private void ResizeGridTranCode()
		{
			try
			{

				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ResizeGridTranCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeLandBldgCodes()
		{
			try
			{

				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In ResizeLandBldgCodes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool CheckValues()
		{
			bool CheckValues = false;
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				int lngTemp = 0;
				lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTranCode.TextMatrix(0, 0))));
				if (lngTemp < 1)
				{
					lngTemp = frmPickRegionalTown.InstancePtr.Init(-1, false, "A town has not been specified");
					if (lngTemp > 0)
					{
						gridTranCode.TextMatrix(0, 0, FCConvert.ToString(lngTemp));
					}
				}
			}
			CheckValues = true;
			return CheckValues;
		}

		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);

			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtPreviousOwner";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Previous Owner";
			intTotalNumberOfControls += 1;
			
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSLocNumAlph";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Street Number";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSLocApt";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Apartment";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSLocStreet";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Street";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSRef1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Ref 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSRef2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Ref 2";
			intTotalNumberOfControls += 1;

			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtDeedName1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Deed Name 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtDeedName2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Deed Name 2";
			intTotalNumberOfControls += 1;

			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRPIOpen1";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Open 1";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRPIOpen2";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Open 2";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRDIEntrancecode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Entrance Code";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRDIINFORMATION";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Information Code";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRDIDateInspected";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Inspection Date";
			intTotalNumberOfControls += 1;
			if (!modREMain.Statics.boolShortRealEstate)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRPIAcres";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Acres";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRLlandval";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Land Value";
				intTotalNumberOfControls += 1;
			}
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRLBldgVal";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Building Value";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRLExemption";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Total Exemption";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMRRSMapLot";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "MapLot";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkRLivingTrust";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Revocable Living Trust";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkBankruptcy";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "In Bankruptcy";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "chkTaxAcquired";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.CheckBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Tax Acquired";
			intTotalNumberOfControls += 1;
		}

		private void ReLoad()
		{
			int counter;
			FillControlInformationClass();
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			FillControlInformationClassFromGrid();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
		}
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			int intRows;
			int intCols;

			if (modREMain.Statics.boolShortRealEstate)
			{
				// must track land val changes
				for (intRows = 1; intRows <= 2; intRows++)
				{
					for (intCols = 1; intCols <= 4; intCols++)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
						//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
						clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
						clsControlInfo[intTotalNumberOfControls].ControlName = "TreeGrid";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						switch (intRows)
						{
							case 1:
								{
									switch (intCols)
									{
										case 1:
											{
												clsControlInfo[intTotalNumberOfControls].DataDescription = "Soft Acres";
												break;
											}
										case 2:
											{
												clsControlInfo[intTotalNumberOfControls].DataDescription = "Mixed Acres";
												break;
											}
										case 3:
											{
												clsControlInfo[intTotalNumberOfControls].DataDescription = "Hard Acres";
												break;
											}
										case 4:
											{
												clsControlInfo[intTotalNumberOfControls].DataDescription = "Other Acres";
												break;
											}
									}
									//end switch
									break;
								}
							case 2:
								{
									switch (intCols)
									{
										case 1:
											{
												clsControlInfo[intTotalNumberOfControls].DataDescription = "Soft Value";
												break;
											}
										case 2:
											{
												clsControlInfo[intTotalNumberOfControls].DataDescription = "Mixed Value";
												break;
											}
										case 3:
											{
												clsControlInfo[intTotalNumberOfControls].DataDescription = "Hard Value";
												break;
											}
										case 4:
											{
												clsControlInfo[intTotalNumberOfControls].DataDescription = "Other Value";
												break;
											}
									}
									//end switch
									break;
								}
						}
						//end switch
						intTotalNumberOfControls += 1;
					}
					// intCols
				}
				// intRows
			}
			for (intRows = 1; intRows <= 3; intRows++)
			{
				for (intCols = 0; intCols <= 1; intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
					//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
					clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
					clsControlInfo[intTotalNumberOfControls].ControlName = "GridExempts";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					if (intCols == CNSTEXEMPTCOLCODE)
					{
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Exempt Code " + FCConvert.ToString(intRows);
					}
					else
					{
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Exempt Percent " + FCConvert.ToString(intRows);
					}
					intTotalNumberOfControls += 1;
				}
				// intCols
			}
			// intRows
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "GridTranCode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
			clsControlInfo[intTotalNumberOfControls].GridRow = 0;
			clsControlInfo[intTotalNumberOfControls].GridCol = 0;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Tran Code ";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "GridLandCode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
			clsControlInfo[intTotalNumberOfControls].GridRow = 0;
			clsControlInfo[intTotalNumberOfControls].GridCol = 0;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Land Code ";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "GridBldgCode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
			clsControlInfo[intTotalNumberOfControls].GridRow = 0;
			clsControlInfo[intTotalNumberOfControls].GridCol = 0;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Building Code ";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "GridPropertyCode";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
			clsControlInfo[intTotalNumberOfControls].GridRow = 0;
			clsControlInfo[intTotalNumberOfControls].GridCol = 0;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Property Code ";
			intTotalNumberOfControls += 1;
			for (intRows = 1; intRows <= BPGrid.Rows - 1; intRows++)
			{
				for (intCols = BPBookCol; intCols <= BPPageCol; intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
					//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
					clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
					clsControlInfo[intTotalNumberOfControls].ControlName = "BPGrid";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					switch (intCols)
					{
						case BPBookCol:
							{
								clsControlInfo[intTotalNumberOfControls].DataDescription = "BP Row " + FCConvert.ToString(intRows) + " Book";
								break;
							}
						case BPPageCol:
							{
								clsControlInfo[intTotalNumberOfControls].DataDescription = "BP Row " + FCConvert.ToString(intRows) + " Page";
								break;
							}
					}
					//end switch
					intTotalNumberOfControls += 1;
				}
				// intCols
			}
			// intRows
			for (intCols = CNSTSALECOLPRICE; intCols <= CNSTSALECOLVALIDITY; intCols++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "SaleGrid";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = 2;
				clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
				switch (intCols)
				{
					case CNSTSALECOLPRICE:
						{
							clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Price";
							break;
						}
					case CNSTSALECOLDATE:
						{
							clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Date";
							break;
						}
					case CNSTSALECOLFINANCING:
						{
							clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Financing";
							break;
						}
					case CNSTSALECOLSALE:
						{
							clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Type";
							break;
						}
					case CNSTSALECOLVALIDITY:
						{
							clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Validity";
							break;
						}
					case CNSTSALECOLVERIFIED:
						{
							clsControlInfo[intTotalNumberOfControls].DataDescription = "Sale Verified";
							break;
						}
				}
				//end switch
				intTotalNumberOfControls += 1;
			}
			// intCols
			for (intRows = 1; intRows <= MapLotGrid.Rows - 1; intRows++)
			{
				for (intCols = 0; intCols <= 1; intCols++)
				{
					Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
					//FC:FINAL:MSH - i.issue #1071: trying to set values into not initialized object
					clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
					clsControlInfo[intTotalNumberOfControls].ControlName = "MapLotGrid";
					clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
					clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
					clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
					switch (intCols)
					{
						case 0:
							{
								clsControlInfo[intTotalNumberOfControls].DataDescription = "MapLot Row " + FCConvert.ToString(intCols);
								break;
							}
						case 1:
							{
								clsControlInfo[intTotalNumberOfControls].DataDescription = "MapLot Row " + FCConvert.ToString(intCols) + " Date";
								break;
							}
					}
					//end switch
					intTotalNumberOfControls += 1;
				}
				// intCols
			}
			// intRows
		}

		public void SaleMRToText()
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strTemp = "";
				clsDRWrapper clsTemp = new clsDRWrapper();
				int y;
				if (modGlobalVariables.Statics.gintLastAccountNumber == 0 || modSHREBLUpdateMRTEXT.Statics.gboolAddAccount)
				{
					/* object FormObjects; *///FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
					//foreach (Control FormObjects in frmSHREBLUpdate.InstancePtr.Controls)
					var controls = frmSHREBLUpdate.InstancePtr.GetAllControls();
					foreach (Control FormObjects in controls)
					{
						if (FormObjects is FCTextBox)
							FormObjects.Text = "";
						/*? 3 */
					}
					frmSHREBLUpdate.InstancePtr.lblMRHLCARDACCT1.Text = FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
					// frmSHREBLUpdate.lblMRHICARDNUM.Caption = gintCardNumber
					frmSHREBLUpdate.InstancePtr.cardNumber.Text = FCConvert.ToString(modGNBas.Statics.gintCardNumber);
					frmSHREBLUpdate.InstancePtr.lblHighCard.Text = FCConvert.ToString(modGNBas.Statics.gintMaxCards);
					clsTemp.OpenRecordset("select * from citystatezipdefaults", modGlobalVariables.strREDatabase);
					if (!clsTemp.EndOfFile())
					{
						frmSHREBLUpdate.InstancePtr.txtMRRSADDR3.Text = FCConvert.ToString(clsTemp.Get_Fields_String("city"));
						frmSHREBLUpdate.InstancePtr.txtMRRSSTATE.Text = FCConvert.ToString(clsTemp.Get_Fields_String("state"));
						frmSHREBLUpdate.InstancePtr.txtMRRSZIP.Text = FCConvert.ToString(clsTemp.Get_Fields_String("zip"));
					}
					/*? 12 */
					goto EndFill;
				}
				frmSHREBLUpdate.InstancePtr.lblMRHLCARDACCT1.Text = FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
				// frmSHREBLUpdate.lblMRHICARDNUM.Caption = Format(gintCardNumber, "###")
				frmSHREBLUpdate.InstancePtr.cardNumber.Text = FCConvert.ToString(modGNBas.Statics.gintCardNumber);
				frmSHREBLUpdate.InstancePtr.lblHighCard.Text = FCConvert.ToString(modGNBas.Statics.gintMaxCards);
				if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("hlupdate") + ""))
				{
					//FC:FINAL:MSH - i.issue #1071: incorrect converting from date to int
					//if (FCConvert.ToInt32(modDataTypes.MR.Get_Fields("hlupdate")) != 0)
					if (modDataTypes.Statics.MR.Get_Fields_DateTime("hlupdate").ToOADate() != 0)
					{
						frmSHREBLUpdate.InstancePtr.lblMRHLUPDATE.Text = Strings.Format(modDataTypes.Statics.MR.Get_Fields_DateTime("hlupdate") + "", "MM/dd/yyyy");
					}
					else
					{
						frmSHREBLUpdate.InstancePtr.lblMRHLUPDATE.Text = "";
					}
				}
				else
				{
					frmSHREBLUpdate.InstancePtr.lblMRHLUPDATE.Text = "";
				}
				// frmSHREBLUpdate.lblMRHIUPDCODE.Width = 115
				if (FCConvert.ToBoolean(modDataTypes.Statics.MR.Get_Fields_Boolean("taxacquired")))
				{
					frmSHREBLUpdate.InstancePtr.chkTaxAcquired.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					frmSHREBLUpdate.InstancePtr.chkTaxAcquired.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(modDataTypes.Statics.MR.Get_Fields_Boolean("inbankruptcy")))
				{
					frmSHREBLUpdate.InstancePtr.chkBankruptcy.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					frmSHREBLUpdate.InstancePtr.chkBankruptcy.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					if (FCConvert.ToBoolean(modDataTypes.Statics.MR.Get_Fields_Boolean("revocabletrust")))
					{
						frmSHREBLUpdate.InstancePtr.chkRLivingTrust.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						frmSHREBLUpdate.InstancePtr.chkRLivingTrust.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					frmSHREBLUpdate.InstancePtr.chkRLivingTrust.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				frmSHREBLUpdate.InstancePtr.lblMRHIUPDCODE.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("HIUPDCODE") + "");
				frmSHREBLUpdate.InstancePtr.txtMRRSMAPLOT.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsMapLot") + "");
				frmSHREBLUpdate.InstancePtr.txtMRRSNAME.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("RSNAME") + "");
				frmSHREBLUpdate.InstancePtr.txtMRRSSECOWNER.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rssecowner") + "");
				frmSHREBLUpdate.InstancePtr.txtPreviousOwner.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rspreviousmaster") + "");
				frmSHREBLUpdate.InstancePtr.txtMRRSADDR1.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsaddr1") + "");
				frmSHREBLUpdate.InstancePtr.txtMRRSADDR2.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsaddr2") + "");
				frmSHREBLUpdate.InstancePtr.txtMRRSADDR3.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsaddr3") + "");
				frmSHREBLUpdate.InstancePtr.txtMRRSSTATE.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsstate") + "");
				frmSHREBLUpdate.InstancePtr.txtMRRSZIP.Text = modDataTypes.Statics.MR.Get_Fields_String("rszip") + "";
				frmSHREBLUpdate.InstancePtr.txtMRRSZIP4.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rszip4") + "");
				// 43    frmSHREBLUpdate.txtEmail.Text = MR.Fields("email") & ""
				frmSHREBLUpdate.InstancePtr.txtMRRSLOCNUMALPH.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + "");
				frmSHREBLUpdate.InstancePtr.txtMRRSLOCAPT.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocapt") + " ");
				// frmSHREBLUpdate.txtMRRSLOCAPT.Width = 115
				frmSHREBLUpdate.InstancePtr.txtMRRSLOCSTREET.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocstreet") + " ");
				frmSHREBLUpdate.InstancePtr.txtMRRSREF1.Text = modDataTypes.Statics.MR.Get_Fields_String("rsref1") + " ";
				frmSHREBLUpdate.InstancePtr.txtMRRSREF2.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsref2") + " ");
				// frmSHREBLUpdate.txtBook.Text = Val(MR.Fields("rsbook") & "")
				// frmSHREBLUpdate.txtPage.Text = Val(MR.Fields("rspage") & "")
				// 
				frmSHREBLUpdate.InstancePtr.txtMRRLLANDVAL.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + ""), "##,###,###,##0");
				frmSHREBLUpdate.InstancePtr.txtMRRLBLDGVAL.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval") + ""), "##,###,###,##0");
				frmSHREBLUpdate.InstancePtr.txtMRRLEXEMPTION.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rlexemption") + ""), "##,###,###,##0");
				frmSHREBLUpdate.InstancePtr.gridLandCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rilandcode") + "")));
				frmSHREBLUpdate.InstancePtr.gridBldgCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ribldgcode") + "")));
				frmSHREBLUpdate.InstancePtr.gridTranCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ritrancode") + "")));
				frmSHREBLUpdate.InstancePtr.gridPropertyCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("propertycode") + "")));
				frmSHREBLUpdate.InstancePtr.txtMRPIOPEN1.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "");
				frmSHREBLUpdate.InstancePtr.txtMRPIOPEN2.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + "");
				frmSHREBLUpdate.InstancePtr.txtMRPIACRES.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piacres") + ""), "0000.00");
				// frmSHREBLUpdate.txtMRDIENTRANCECODE.Text = Val(OUT.Fields("oientrancecode") & "")
				// TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
				frmSHREBLUpdate.InstancePtr.txtMRDIENTRANCECODE.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("entrancecode") + ""));
				// Call OpenCRTable(CR, (Val(OUT.Fields("oientrancecode") & "") + 1700))
				// TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
				clsDRWrapper temp = modDataTypes.Statics.CR;
				modREMain.OpenCRTable(ref temp, (Conversion.Val(modDataTypes.Statics.MR.Get_Fields("entrancecode") + "") + 1700));
				frmSHREBLUpdate.InstancePtr.lblMRDIENTRANCE.Text = temp.Get_Fields_String("ClDesc") + "";
				// frmSHREBLUpdate.txtMRDIINFORMATION.Width = 115
				// frmSHREBLUpdate.txtMRDIINFORMATION.Text = Trim(OUT.Fields("oiinformation") & "")
				// TODO Get_Fields: Check the table for the column [informationcode] and replace with corresponding Get_Field method
				frmSHREBLUpdate.InstancePtr.txtMRDIINFORMATION.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields("informationcode") + "");
				// Call OpenCRTable(CR, (Val(OUT.Fields("oiinformation") & "") + 1710))
				// TODO Get_Fields: Check the table for the column [informationcode] and replace with corresponding Get_Field method
				modREMain.OpenCRTable(ref temp, (Conversion.Val(modDataTypes.Statics.MR.Get_Fields("informationcode") + "") + 1710));
				modDataTypes.Statics.CR = temp;
				frmSHREBLUpdate.InstancePtr.lblMRDIINFORMATION.Text = modDataTypes.Statics.CR.Get_Fields_String("ClDesc") + "";
				DateTime tempdate;

				if (!fecherFoundation.FCUtils.IsEmptyDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("dateinspected")))
				{
					if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("dateinspected")))
					{
						tempdate = modDataTypes.Statics.MR.Get_Fields_DateTime("dateinspected");
						if (tempdate.ToOADate() == 0)
						{
							frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text = "00/00/00";
						}
						else
						{
							frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text = Strings.Format(tempdate, "MM/dd/yy");
						}
					}
					else
					{
						frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text = "00/00/00";
					}
				}
				else
				{
					frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text = "00/00/00";
				}
				EndFill:
				;
				modSHREBLUpdateMRTEXT.Statics.gboolAddAccount = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				modSHREBLUpdateMRTEXT.Statics.gboolAddAccount = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaleMRToText in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void textToSaleMR()
		{
			/*? On Error Resume Next  */
			try
			{
				bool boolBillValsChanged;
				clsDRWrapper clsSave = new clsDRWrapper();
				bool boolExemptionChanged;
				string strTemp = "";
				string strPhone = "";
				int x;
				boolBillValsChanged = false;
				boolExemptionChanged = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				modDataTypes.Statics.MR.Set_Fields("rssoft", FCConvert.ToString(Conversion.Val(TreeGrid.TextMatrix(1, 1) + "")));
				modDataTypes.Statics.MR.Set_Fields("rshard", FCConvert.ToString(Conversion.Val(TreeGrid.TextMatrix(1, 3) + "")));
				modDataTypes.Statics.MR.Set_Fields("rsmixed", FCConvert.ToString(Conversion.Val(TreeGrid.TextMatrix(1, 2) + "")));
				modDataTypes.Statics.MR.Set_Fields("rsother", FCConvert.ToString(Conversion.Val(TreeGrid.TextMatrix(1, 4))));
				modDataTypes.Statics.MR.Set_Fields("rssoftvalue", FCConvert.ToInt32(FCConvert.ToDouble(TreeGrid.TextMatrix(2, 1) + "")));
				modDataTypes.Statics.MR.Set_Fields("rshardvalue", FCConvert.ToInt32(FCConvert.ToDouble(TreeGrid.TextMatrix(2, 3) + "")));
				modDataTypes.Statics.MR.Set_Fields("rsmixedvalue", FCConvert.ToInt32(FCConvert.ToDouble(TreeGrid.TextMatrix(2, 2) + "")));
				modDataTypes.Statics.MR.Set_Fields("rsothervalue", FCConvert.ToInt32(FCConvert.ToDouble(TreeGrid.TextMatrix(2, 4))));
				modDataTypes.Statics.MR.Set_Fields("taxacquired", frmSHREBLUpdate.InstancePtr.chkTaxAcquired.CheckState == Wisej.Web.CheckState.Checked);
				modDataTypes.Statics.MR.Set_Fields("inbankruptcy", frmSHREBLUpdate.InstancePtr.chkBankruptcy.CheckState == Wisej.Web.CheckState.Checked);
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					modDataTypes.Statics.MR.Set_Fields("revocabletrust", frmSHREBLUpdate.InstancePtr.chkRLivingTrust.CheckState == Wisej.Web.CheckState.Checked);
				}
				modDataTypes.Statics.MR.Set_Fields("hlupdate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				modDataTypes.Statics.MR.Set_Fields("rsMapLot", frmSHREBLUpdate.InstancePtr.txtMRRSMAPLOT.Text);
				modDataTypes.Statics.MR.Set_Fields("RSPREVIOUSMASTER", Strings.Trim(frmSHREBLUpdate.InstancePtr.txtPreviousOwner.Text));
				modDataTypes.Statics.MR.Set_Fields("RSNAME", frmSHREBLUpdate.InstancePtr.txtMRRSNAME.Text);
				modDataTypes.Statics.MR.Set_Fields("rssecowner", frmSHREBLUpdate.InstancePtr.txtMRRSSECOWNER.Text);
				modDataTypes.Statics.MR.Set_Fields("rsaddr1", frmSHREBLUpdate.InstancePtr.txtMRRSADDR1.Text);
				modDataTypes.Statics.MR.Set_Fields("rsaddr2", frmSHREBLUpdate.InstancePtr.txtMRRSADDR2.Text);
				modDataTypes.Statics.MR.Set_Fields("rsaddr3", frmSHREBLUpdate.InstancePtr.txtMRRSADDR3.Text);
				modDataTypes.Statics.MR.Set_Fields("rsstate", frmSHREBLUpdate.InstancePtr.txtMRRSSTATE.Text);
				modDataTypes.Statics.MR.Set_Fields("rszip", frmSHREBLUpdate.InstancePtr.txtMRRSZIP.Text);
				modDataTypes.Statics.MR.Set_Fields("rszip4", frmSHREBLUpdate.InstancePtr.txtMRRSZIP4.Text);
				modDataTypes.Statics.MR.Set_Fields("rslocnumalph", frmSHREBLUpdate.InstancePtr.txtMRRSLOCNUMALPH.Text);
				modDataTypes.Statics.MR.Set_Fields("rslocapt", frmSHREBLUpdate.InstancePtr.txtMRRSLOCAPT.Text);
				modDataTypes.Statics.MR.Set_Fields("rslocstreet", frmSHREBLUpdate.InstancePtr.txtMRRSLOCSTREET.Text);
				modDataTypes.Statics.MR.Set_Fields("rsref1", frmSHREBLUpdate.InstancePtr.txtMRRSREF1.Text);
				modDataTypes.Statics.MR.Set_Fields("rsref2", frmSHREBLUpdate.InstancePtr.txtMRRSREF2.Text);
				// these lines are for the backfill numeric fields
				if (frmSHREBLUpdate.InstancePtr.txtMRRLLANDVAL.Text != "")
				{
					if (FCConvert.ToDecimal(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval"))) != FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLLANDVAL.Text + ""))
					{
						boolBillValsChanged = true;
					}
					modDataTypes.Statics.MR.Set_Fields("lastlandval", FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLLANDVAL.Text + ""));
				}
				else
				{
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval")) != 0)
						boolBillValsChanged = true;
				}
				if (frmSHREBLUpdate.InstancePtr.txtMRRLBLDGVAL.Text != "")
				{
					if (FCConvert.ToDecimal(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval"))) != FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLBLDGVAL.Text + ""))
					{
						boolBillValsChanged = true;
					}
					modDataTypes.Statics.MR.Set_Fields("lastbldgval", FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLBLDGVAL.Text + ""));
				}
				else
				{
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval")) != 0)
						boolBillValsChanged = true;
				}
				for (x = 1; x <= 3; x++)
				{
					if (Conversion.Val(frmSHREBLUpdate.InstancePtr.GridExempts.TextMatrix(x, 0)) > -1)
					{
						modDataTypes.Statics.MR.Set_Fields("riexemptcd" + x, FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.GridExempts.TextMatrix(x, 0))));
						modDataTypes.Statics.MR.Set_Fields("exemptpct" + x, FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.GridExempts.TextMatrix(x, 1))));
					}
					else
					{
						modDataTypes.Statics.MR.Set_Fields("riexemptcd" + x, 0);
						modDataTypes.Statics.MR.Set_Fields("exemptpct" + x, 100);
					}
				}
				// x
				if (frmSHREBLUpdate.InstancePtr.txtMRRLEXEMPTION.Text != "")
				{
					if (FCConvert.ToDecimal(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rlexemption"))) != FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLEXEMPTION.Text + ""))
					{
						boolBillValsChanged = true;
						boolExemptionChanged = true;
					}
					modDataTypes.Statics.MR.Set_Fields("rlexemption", FCConvert.ToDecimal(frmSHREBLUpdate.InstancePtr.txtMRRLEXEMPTION.Text + ""));
				}
				else
				{
					if (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rlexemption")) != 0)
					{
						boolBillValsChanged = true;
						boolExemptionChanged = true;
						modDataTypes.Statics.MR.Set_Fields("rlexemption", 0);
					}
				}
				if (boolBillValsChanged)
				{
					clsSave.Execute("update status set accountschange = #" + FCConvert.ToString(DateTime.Now) + "#", modGlobalVariables.strREDatabase);
				}
				// must calc exemptions if the exemption changed and we have legitimate codes entered
				// end of backfill numeric fields
				modDataTypes.Statics.MR.Set_Fields("rilandcode", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.gridLandCode.TextMatrix(0, 0))));
				modDataTypes.Statics.MR.Set_Fields("ribldgcode", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.gridBldgCode.TextMatrix(0, 0))));
				modDataTypes.Statics.MR.Set_Fields("propertycode", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.gridPropertyCode.TextMatrix(0, 0))));
				modDataTypes.Statics.MR.Set_Fields("piopen1", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRPIOPEN1.Text + "")));
				modDataTypes.Statics.MR.Set_Fields("piopen2", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRPIOPEN2.Text + "")));
				if (Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRPIACRES.Text + "") != 0)
				{
					modDataTypes.Statics.MR.Set_Fields("piacres", FCConvert.ToDouble(frmSHREBLUpdate.InstancePtr.txtMRPIACRES.Text));
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("piacres", 0);
				}
				modDataTypes.Statics.MR.Set_Fields("entrancecode", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRDIENTRANCECODE.Text + "")));
				modDataTypes.Statics.MR.Set_Fields("informationcode", FCConvert.ToString(Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRDIENTRANCECODE.Text + "")));
				if (Information.IsDate(frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text))
				{
					if (Conversion.Val(frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text) > 0)
					{
						modDataTypes.Statics.MR.Set_Fields("dateinspected", Strings.Format(frmSHREBLUpdate.InstancePtr.txtMRDIDATEINSPECTED.Text, "MM/dd/yy"));
					}
					else
					{
						modDataTypes.Statics.MR.Set_Fields("dateinspected", 0);
					}
				}
				else
				{
					modDataTypes.Statics.MR.Set_Fields("dateinspected", 0);
				}
				modDataTypes.Statics.MR.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.MR.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
				modDataTypes.Statics.DWL.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.DWL.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
				modDataTypes.Statics.CMR.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.CMR.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
				modDataTypes.Statics.OUT.Set_Fields("rscard", modGNBas.Statics.gintCardNumber);
				modDataTypes.Statics.OUT.Set_Fields("rsaccount", modGlobalVariables.Statics.gintLastAccountNumber);
				if (!modGlobalVariables.Statics.boolInPendingMode)
				{
					clsDRWrapper temp = modDataTypes.Statics.MR;
					modREMain.SaveMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
					modDataTypes.Statics.MR = temp;
					temp = modDataTypes.Statics.DWL;
					modREMain.SaveDwellingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
					modDataTypes.Statics.DWL = temp;
					temp = modDataTypes.Statics.CMR;
					modREMain.SaveCommercialTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
					modDataTypes.Statics.CMR = temp;
					temp = modDataTypes.Statics.OUT;
					modREMain.SaveOutBuildingTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
					modDataTypes.Statics.OUT = temp;
					if (boolExemptionChanged)
					{
						modGlobalRoutines.SplitExemption(ref modGlobalVariables.Statics.gintLastAccountNumber);
						temp = modDataTypes.Statics.MR;
						modREMain.OpenMasterTable(ref temp, modGlobalVariables.Statics.gintLastAccountNumber, modGNBas.Statics.gintCardNumber);
						modDataTypes.Statics.MR = temp;
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
			catch
			{
			}
		}

		private void ShowOwner()
		{
			if (theAccount.OwnerID > 0)
			{
				if (!(theAccount.OwnerParty == null))
				{
					lblOwner1.Text = theAccount.OwnerParty.FullNameLastFirst;
				}
				else
				{
					lblOwner1.Text = "";
				}
				txtOwnerID.Text = FCConvert.ToString(theAccount.OwnerID);
			}
			else
			{
				lblOwner1.Text = "";
				txtOwnerID.Text = FCConvert.ToString(0);
			}
		}

		private void ShowAddress()
		{
			if (!(theAccount.OwnerParty == null))
			{
				cPartyAddress tAdd;
				tAdd = theAccount.OwnerParty.GetPrimaryAddress();
				if (!(tAdd == null))
				{
					lblAddress.Text = tAdd.GetFormattedAddress();
				}
				else
				{
					lblAddress.Text = "";
				}
			}
			else
			{
				lblAddress.Text = "";
			}
		}

		private void ShowSecOwner()
		{
			if (theAccount.SecOwnerID > 0)
			{
				if (!(theAccount.SecOwnerParty == null))
				{
					lbl2ndOwner.Text = theAccount.SecOwnerParty.FullNameLastFirst;
				}
				else
				{
					lbl2ndOwner.Text = "";
				}
				txt2ndOwnerID.Text = FCConvert.ToString(theAccount.SecOwnerID);
			}
			else
			{
				lbl2ndOwner.Text = "";
				txt2ndOwnerID.Text = FCConvert.ToString(0);
			}
		}

		public void REMRToText()
		{
			try
			{
				// On Error GoTo ErrorHandler
				string strTemp = "";
				clsDRWrapper clsTemp = new clsDRWrapper();
				int y;
				if (modGlobalVariables.Statics.gintLastAccountNumber == 0 || modSHREBLUpdateMRTEXT.Statics.gboolAddAccount)
				{
					/* object FormObjects; *///FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
					//foreach (Control FormObjects in frmSHREBLUpdate.InstancePtr.ClientArea.Controls)
					var controls = frmSHREBLUpdate.InstancePtr.ClientArea.GetAllControls();
					foreach (Control FormObjects in controls)
					{
						if (FormObjects is FCTextBox)
							FormObjects.Text = "";
						/*? 3 */
					}
					lblMRHLCARDACCT1.Text = FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
					// lblMRHICARDNUM.Caption = gintCardNumber
					cardNumber.Text = FCConvert.ToString(modGNBas.Statics.gintCardNumber);
					lblHighCard.Text = FCConvert.ToString(modGNBas.Statics.gintMaxCards);
					goto EndFill;
				}
				lblMRHLCARDACCT1.Text = FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber);
				// lblMRHICARDNUM.Caption = Format(gintCardNumber, "###")
				cardNumber.Text = FCConvert.ToString(modGNBas.Statics.gintCardNumber);
				lblHighCard.Text = FCConvert.ToString(modGNBas.Statics.gintMaxCards);
				if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("hlupdate") + ""))
				{
					//FC:FINAL:MSH - i.issue #1071: incorrect converting from date to int
					//if (FCConvert.ToInt32(modDataTypes.MR.Get_Fields("hlupdate")) != 0)
					if (modDataTypes.Statics.MR.Get_Fields_DateTime("hlupdate").ToOADate() != 0)
					{
						lblMRHLUPDATE.Text = Strings.Format(modDataTypes.Statics.MR.Get_Fields_DateTime("hlupdate") + "", "MM/dd/yyyy");
					}
					else
					{
						lblMRHLUPDATE.Text = "";
					}
				}
				else
				{
					lblMRHLUPDATE.Text = "";
				}
				// lblMRHIUPDCODE.Width = 115
				if (FCConvert.ToBoolean(modDataTypes.Statics.MR.Get_Fields_Boolean("taxacquired")))
				{
					chkTaxAcquired.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkTaxAcquired.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(modDataTypes.Statics.MR.Get_Fields_Boolean("inbankruptcy")))
				{
					chkBankruptcy.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkBankruptcy.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (modGlobalVariables.Statics.boolRegRecords)
				{
					if (FCConvert.ToBoolean(modDataTypes.Statics.MR.Get_Fields_Boolean("revocabletrust")))
					{
						chkRLivingTrust.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkRLivingTrust.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					chkRLivingTrust.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				lblMRHIUPDCODE.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("HIUPDCODE") + "");
				txtMRRSMAPLOT.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsMapLot") + "");
				
				txtPreviousOwner.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rspreviousmaster") + "");
				txtMRRSLOCNUMALPH.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + "");
				txtMRRSLOCAPT.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocapt") + " ");
				txtMRRSLOCSTREET.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocstreet") + " ");
				txtMRRSREF1.Text = modDataTypes.Statics.MR.Get_Fields_String("rsref1") + " ";
				txtMRRSREF2.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsref2") + " ");

				txtMRRLLANDVAL.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + ""), "##,###,###,##0");
				txtMRRLBLDGVAL.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval") + ""), "##,###,###,##0");
				txtMRRLEXEMPTION.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rlexemption") + ""), "##,###,###,##0");
				gridLandCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rilandcode") + "")));
				gridBldgCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ribldgcode") + "")));
				gridTranCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("ritrancode") + "")));
				gridPropertyCode.TextMatrix(0, 0, FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("propertycode") + "")));
				txtMRPIOPEN1.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piopen1") + "");
				txtMRPIOPEN2.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("piopen2") + "");
				txtMRPIACRES.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Double("piacres") + ""), "0000.00");
				// txtMRDIENTRANCECODE.Text = Val(OUT.Fields("oientrancecode") & "")
				// TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
				txtMRDIENTRANCECODE.Text = FCConvert.ToString(Conversion.Val(modDataTypes.Statics.MR.Get_Fields("entrancecode") + ""));
				// Call OpenCRTable(CR, (Val(OUT.Fields("oientrancecode") & "") + 1700))
				// TODO Get_Fields: Check the table for the column [entrancecode] and replace with corresponding Get_Field method
				clsDRWrapper temp = modDataTypes.Statics.CR;
				modREMain.OpenCRTable(ref temp, (Conversion.Val(modDataTypes.Statics.MR.Get_Fields("entrancecode") + "") + 1700));
				lblMRDIENTRANCE.Text = temp.Get_Fields_String("ClDesc") + "";

				// TODO Get_Fields: Check the table for the column [informationcode] and replace with corresponding Get_Field method
				txtMRDIINFORMATION.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields("informationcode") + "");
				// Call OpenCRTable(CR, (Val(OUT.Fields("oiinformation") & "") + 1710))
				// TODO Get_Fields: Check the table for the column [informationcode] and replace with corresponding Get_Field method
				modREMain.OpenCRTable(ref temp, (Conversion.Val(modDataTypes.Statics.MR.Get_Fields("informationcode") + "") + 1710));
				modDataTypes.Statics.CR = temp;
				lblMRDIINFORMATION.Text = modDataTypes.Statics.CR.Get_Fields_String("ClDesc") + "";
				DateTime tempdate;

				if (!fecherFoundation.FCUtils.IsEmptyDateTime(modDataTypes.Statics.MR.Get_Fields_DateTime("dateinspected")))
				{
					if (Information.IsDate(modDataTypes.Statics.MR.Get_Fields("dateinspected")))
					{
						tempdate = modDataTypes.Statics.MR.Get_Fields_DateTime("dateinspected");
						if (tempdate.ToOADate() == 0)
						{
							txtMRDIDATEINSPECTED.Text = "00/00/00";
						}
						else
						{
							txtMRDIDATEINSPECTED.Text = Strings.Format(tempdate, "MM/dd/yy");
						}
					}
					else
					{
						txtMRDIDATEINSPECTED.Text = "00/00/00";
					}
				}
				else
				{
					txtMRDIDATEINSPECTED.Text = "00/00/00";
				}
				EndFill:
				;
				modSHREBLUpdateMRTEXT.Statics.gboolAddAccount = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				modSHREBLUpdateMRTEXT.Statics.gboolAddAccount = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In REMRToText in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadAccount(int lngAcct)
		{
			try
			{
				// On Error GoTo ErrorHandler
				cREAccountController tCont = new cREAccountController();
				tCont.LoadAccount(ref theAccount, lngAcct);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In LoadAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveAccount()
		{
			cREAccountController tCont = new cREAccountController();
			tCont.SaveAccount(theAccount);
		}

		private void cmdSearchSecOwner_Click(object sender, System.EventArgs e)
		{
			int lngReturnID;
            bool updateDeedName = false;

			lngReturnID = frmCentralPartySearch.InstancePtr.Init();
			if (lngReturnID != theAccount.SecOwnerID && lngReturnID > 0)
            {
                updateDeedName = true;
				cPartyController tCont = new cPartyController();
				cParty tParty = theAccount.SecOwnerParty;
				tCont.LoadParty(ref tParty, lngReturnID);
				theAccount.SecOwnerID = lngReturnID;
				modDataTypes.Statics.MR.Set_Fields("SecOwnerPartyID", theAccount.SecOwnerID);
				ShowSecOwner();
                if (updateDeedName)
                {
                    theAccount.DeedName2 = theAccount.SecOwnerParty.FullNameLastFirst;
                }
			}
		}

		private void cmdEditOwner_Click(object sender, System.EventArgs e)
		{
			if (!(theAccount == null))
			{
				if (theAccount.OwnerID > 0)
				{
					int lngReturn;
					int lngID = theAccount.OwnerID;
					lngReturn = frmEditCentralParties.InstancePtr.Init(ref lngID);
					cPartyController tCont = new cPartyController();
					cParty tParty = theAccount.OwnerParty;
					tCont.LoadParty(ref tParty, theAccount.OwnerID);
					ShowOwner();
					ShowAddress();
				}
			}
		}

		private void cmdEditSecOwner_Click(object sender, System.EventArgs e)
		{
			if (!(theAccount == null))
			{
				if (theAccount.SecOwnerID > 0)
				{
					int lngReturn;
					int lngID = theAccount.SecOwnerID;
					lngReturn = frmEditCentralParties.InstancePtr.Init(ref lngID);
					cPartyController tCont = new cPartyController();
					cParty tParty = theAccount.SecOwnerParty;
					tCont.LoadParty(ref tParty, theAccount.SecOwnerID);
					ShowSecOwner();
				}
			}
		}

		private void cmdRemoveOwner_Click(object sender, System.EventArgs e)
		{
			theAccount.OwnerID = 0;
			if (!(theAccount.OwnerParty == null))
			{
				theAccount.OwnerParty.Clear();
			}
			ShowOwner();
			ShowAddress();
		}

		private void cmdRemoveSecOwner_Click(object sender, System.EventArgs e)
		{
			theAccount.SecOwnerID = 0;
			if (!(theAccount.SecOwnerParty == null))
			{
				theAccount.SecOwnerParty.Clear();
			}
			ShowSecOwner();
		}

        private void ShowDeeds()
        {
            txtDeedName1.Text = theAccount.DeedName1;
            txtDeedName2.Text = theAccount.DeedName2;
        }

        private void cardNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            cardNumber_Validate(false);
        }

        private void cardNumber_Validate(bool Cancel)
        {
            cardNumber_Validating(cardNumber, new System.ComponentModel.CancelEventArgs(Cancel));
        }

        private void cardNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                if (!boolCardChangedByProg)
                {
                    if (Conversion.Val(cardNumber.Text) < 0)                    {
                        cardNumber.Text = FCConvert.ToString(modGNBas.Statics.gintCardNumber);
                        e.Cancel = true;
                        return;
                    }
                    if (modGNBas.Statics.gintCardNumber == Conversion.Val(cardNumber.Text))
                        return;
                    ChangeCard(FCConvert.ToInt32(Conversion.Val(cardNumber.Text)));

                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In txtCardNumber_Validate in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

		private void mnuViewGroupInformation_Click(object sender, EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngGroupNumber = 0;
			// vbPorter upgrade warning: intRes As short, int --> As DialogResult
			DialogResult intRes;
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select * from moduleassociation where module = 'RE' and account = " + FCConvert.ToString(modGlobalVariables.Statics.gintLastAccountNumber), "CentralData");
				if (!clsLoad.EndOfFile())
				{
					lngGroupNumber = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("groupnumber"));
					clsLoad.OpenRecordset("select * from groupmaster where id = " + FCConvert.ToString(lngGroupNumber), "CentralData");
					if (!clsLoad.EndOfFile())
					{
						//! Load frmGroup;
						frmGroup.InstancePtr.Init(lngGroupNumber);
						return;
					}
					else
					{
						MessageBox.Show("This account is associated with a group but the group data is missing.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				else
				{
					intRes = MessageBox.Show("This account is not in any group." + "\r\n" + "Do you want to create a new one?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intRes == DialogResult.Yes)
					{
						//! Load frmGroup;
						frmGroup.InstancePtr.Init(0);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuGroup_Click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuViewMortgageInformation_Click(object sender, EventArgs e)
		{
			frmMortgageHolder.InstancePtr.Unload();
			//! Load frmMortgageHolder;
			frmMortgageHolder.InstancePtr.Init(modGlobalVariables.Statics.gintLastAccountNumber, 2);
			frmMortgageHolder.InstancePtr.Show(App.MainForm);
		}
	}
}
