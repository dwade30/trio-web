﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptBldgStyle.
	/// </summary>
	public partial class srptBldgStyle : FCSectionReport
	{
		public static srptBldgStyle InstancePtr
		{
			get
			{
				return (srptBldgStyle)Sys.GetInstance(typeof(srptBldgStyle));
			}
		}

		protected srptBldgStyle _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsbldgcode?.Dispose();
                clsbldgcode = null;
				clsBldg?.Dispose();
                clsBldg = null;
            }
			base.Dispose(disposing);
		}

		public srptBldgStyle()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptBldgStyle	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		
		clsDRWrapper clsbldgcode = new clsDRWrapper();
		clsDRWrapper clsBldg = new clsDRWrapper();
		int lngCode;
		int lngTotal;
		int lngCount;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			string strSQL = "";
			// Call clsbldgcode.OpenRecordset("select * from costrecord where crecordnumber between 1491 and 1499", strREDatabase)
			clsbldgcode.OpenRecordset("select code as crecordnumber,description as cldesc,shortdescription as csdesc,factor as cunit from dwellingstyle order by code", modGlobalVariables.strREDatabase);
			// Call clsBldg.OpenRecordset("select sstyle,count(*) as thecount,sum(rlbldgval) as thetotal from master where sstyle between '1' and '9' group by sstyle order by val(sstyle)", strredatabase)
			// Call clsBldg.OpenRecordset("select dwelling.distyle,count(distyle) as thecount, sum(master.rlbldgval) as thetotal from    (dwelling inner join master on (master.rsaccount = dwelling.rsaccount) and(dwelling.rscard = master.rscard)) group by dwelling.distyle order by dwelling.distyle", strredatabase)
			if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
			{
				strSQL = "select distyle,count(distyle) as thecount,sum(cast(summrecord.sdwellrcnld as bigint)) as thetotal from (dwelling inner join (master inner join summrecord on (master.rsaccount = summrecord.srecordnumber) and (master.rscard = summrecord.cardnumber)) on (dwelling.rsaccount = master.rsaccount) and (dwelling.rscard = master.rscard)) where not master.rsdeleted = 1 group by dwelling.distyle order by distyle";
			}
			else
			{
				strSQL = "select distyle,count(distyle) as thecount,sum(cast(summrecord.sdwellrcnld as bigint)) as thetotal from (dwelling inner join (master inner join summrecord on (master.rsaccount = summrecord.srecordnumber) and (master.rscard = summrecord.cardnumber)) on (dwelling.rsaccount = master.rsaccount) and (dwelling.rscard = master.rscard)) where not master.rsdeleted = 1 and ritrancode = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " group by dwelling.distyle  order by dwelling.distyle";
			}
			clsBldg.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
			if (clsBldg.EndOfFile())
			{
				this.Close();
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!clsBldg.EndOfFile())
				eArgs.EOF = false;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBldg.Get_Fields_Int32("distyle"))));
			txtBldgStyle.Text = lngCode.ToString();
			// If clsbldgcode.FindFirstRecord("crecordnumber", lngCode + 1490) Then
			// If clsbldgcode.FindFirstRecord("crecordnumber", lngCode) Then
			if (clsbldgcode.FindFirst("crecordnumber = " + FCConvert.ToString(lngCode)))
			{
				txtBldgStyle.Text = txtBldgStyle.Text + " " + clsbldgcode.Get_Fields_String("cldesc");
			}
			else
			{
				txtBldgStyle.Text = txtBldgStyle.Text + " " + "Description Not Found";
			}
			// TODO Get_Fields: Field [thecount] not found!! (maybe it is an alias?)
			lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBldg.Get_Fields("thecount"))));
			txtCount.Text = lngCount.ToString();
			// TODO Get_Fields: Field [thetotal] not found!! (maybe it is an alias?)
			lngTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(clsBldg.Get_Fields("thetotal"))));
			txtAssess.Text = Strings.Format(lngTotal, "###,###,###,##0");
			if (lngCount > 0)
			{
				txtAve.Text = Strings.Format(FCConvert.ToDouble(lngTotal) / lngCount, "#,###,###,##0");
			}
			else
			{
				txtAve.Text = "0";
			}
			clsBldg.MoveNext();
		}

		
	}
}
