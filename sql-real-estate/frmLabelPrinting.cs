﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using Wisej.Web;

namespace TWRE0000
{
    /// <summary>
    /// Summary description for frmLabelPrinting.
    /// </summary>
    public partial class frmLabelPrinting : BaseForm
    {
        public frmLabelPrinting()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmLabelPrinting InstancePtr
        {
            get
            {
                return (frmLabelPrinting)Sys.GetInstance(typeof(frmLabelPrinting));
            }
        }

        protected frmLabelPrinting _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        const int LOC_CODE = 64;
        const int REF2_CODE = 32;
        const int ACCT_CODE = 16;
        const int REF1_CODE = 8;
        const int ADDR_CODE = 4;
        const int MAP_CODE = 2;
        const int ESTTAX_CODE = 1;
        const int SEC_CODE = 128;
        const int ACRES_CODE = 256;
        const int CNSTINCOWNER = 512;
        private int intLinesAllowed;

        private void chkAcres_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cmbtLabelType.Text == "Selected Items")
            {
                CalcNumLines();
            }
        }

        private void chkAddress_CheckedChanged(object sender, System.EventArgs e)
        {
            // If chkAddress.Value = 1 Then
            // ChkLocation.Enabled = False
            // ChkRef2.Enabled = False
            // Else
            // ChkLocation.Enabled = True
            // ChkRef2.Enabled = True
            // End If
            if (cmbtLabelType.Text == "Selected Items")
            {
                CalcNumLines();
            }
        }

        private void chkIgnoreNumLines_CheckedChanged(object sender, System.EventArgs e)
        {
            CalcNumLines();
        }

        private void ChkLocation_CheckedChanged(object sender, System.EventArgs e)
        {
            // If ChkLocation.Value = 1 Then
            // chkAddress.Enabled = False
            // Else
            // If ChkRef2.Value <> 1 Then
            // chkAddress.Enabled = True
            // End If
            // End If
            if (cmbtLabelType.Text == "Selected Items")
            {
                CalcNumLines();
            }
        }

        private void chkMapLot_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cmbtLabelType.Text == "Selected Items")
            {
                CalcNumLines();
            }
        }

        private void chkOwner_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cmbtLabelType.Text == "Selected Items")
            {
                CalcNumLines();
            }
        }

        private void chkRef1_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cmbtLabelType.Text == "Selected Items")
            {
                CalcNumLines();
            }
        }

        private void ChkRef2_CheckedChanged(object sender, System.EventArgs e)
        {
            // If ChkRef2.Value = 1 Then
            // chkAddress.Enabled = False
            // Else
            // If ChkLocation.Value <> 1 Then
            // chkAddress.Enabled = True
            // End If
            // End If
            if (cmbtLabelType.Text == "Selected Items")
            {
                CalcNumLines();
            }
        }

        private void CalcNumLines()
        {
            // determines what choices are left
            int intLinesUsed;
            if (chkIgnoreNumLines.Checked)
            {
                chkMapLot.Enabled = true;
                chkSecOwner.Enabled = true;
                chkRef1.Enabled = true;
                chkAddress.Enabled = true;
                ChkRef2.Enabled = true;
                ChkLocation.Enabled = true;
                chkAcres.Enabled = true;
                return;
            }
            // intLinesUsed = 1    'account for the name which is always printed
            // account doesn't add lines
            // maplot adds lines depending on the printer setting
            intLinesUsed = 0;
            if (chkOwner.CheckState == Wisej.Web.CheckState.Checked)
            {
                intLinesUsed += 1;
            }
            if (chkSecOwner.CheckState == Wisej.Web.CheckState.Checked)
            {
                intLinesUsed += 1;
            }
            //FC:FINAL:CHN - issue #1658: Incorrect code converting.
            if (cmbLabelType.ItemData(cmbLabelType.SelectedIndex) > 2)
            {
                if (chkMapLot.CheckState == Wisej.Web.CheckState.Checked)
                    intLinesUsed += 1;
            }
            if (chkAddress.CheckState == Wisej.Web.CheckState.Checked)
            {
                intLinesUsed += 3;
            }
            if (chkRef1.CheckState == Wisej.Web.CheckState.Checked)
            {
                if (intLinesUsed < intLinesAllowed)
                {
                    intLinesUsed += 1;
                }
                else
                {
                    chkRef1.CheckState = Wisej.Web.CheckState.Unchecked;
                }
            }
            if (ChkLocation.CheckState == Wisej.Web.CheckState.Checked)
            {
                if (intLinesUsed < intLinesAllowed)
                {
                    intLinesUsed += 1;
                }
                else
                {
                    ChkLocation.CheckState = Wisej.Web.CheckState.Unchecked;
                }
            }
            if (ChkRef2.CheckState == Wisej.Web.CheckState.Checked)
            {
                if (intLinesUsed < intLinesAllowed)
                {
                    intLinesUsed += 1;
                }
                else
                {
                    ChkRef2.CheckState = Wisej.Web.CheckState.Unchecked;
                }
            }
            if (chkAcres.CheckState == Wisej.Web.CheckState.Checked)
            {
                if (intLinesUsed < intLinesAllowed)
                {
                    intLinesUsed += 1;
                }
                else
                {
                    chkAcres.CheckState = Wisej.Web.CheckState.Unchecked;
                }
            }
            if (intLinesUsed >= intLinesAllowed)
            {
                if (cmbLabelType.ItemData(cmbLabelType.SelectedIndex) > 2 && chkMapLot.CheckState != Wisej.Web.CheckState.Checked)
                {
                    chkMapLot.Enabled = false;
                }
                if (chkRef1.CheckState != Wisej.Web.CheckState.Checked)
                {
                    chkRef1.Enabled = false;
                }
                if (chkSecOwner.CheckState != Wisej.Web.CheckState.Checked)
                {
                    chkSecOwner.Enabled = false;
                }
                if (chkAddress.CheckState != Wisej.Web.CheckState.Checked)
                {
                    chkAddress.Enabled = false;
                }
                if (ChkLocation.CheckState != Wisej.Web.CheckState.Checked)
                {
                    ChkLocation.Enabled = false;
                }
                if (ChkRef2.CheckState != Wisej.Web.CheckState.Checked)
                {
                    ChkRef2.Enabled = false;
                }
                if (chkAcres.CheckState != Wisej.Web.CheckState.Checked)
                {
                    chkAcres.Enabled = false;
                }
                if (chkOwner.CheckState != Wisej.Web.CheckState.Checked)
                {
                    chkOwner.Enabled = false;
                }
            }
            else
            {
                if (intLinesUsed > intLinesAllowed - 3)
                {
                    if (chkAddress.CheckState != Wisej.Web.CheckState.Checked)
                    {
                        chkAddress.Enabled = false;
                    }
                    chkOwner.Enabled = true;
                    chkMapLot.Enabled = true;
                    chkRef1.Enabled = true;
                    chkSecOwner.Enabled = true;
                    ChkLocation.Enabled = true;
                    ChkRef2.Enabled = true;
                    chkAcres.Enabled = true;
                }
                else
                {
                    chkAddress.Enabled = true;
                    chkMapLot.Enabled = true;
                    chkRef1.Enabled = true;
                    chkSecOwner.Enabled = true;
                    ChkLocation.Enabled = true;
                    ChkRef2.Enabled = true;
                    chkAcres.Enabled = true;
                    chkOwner.Enabled = true;
                }
            }
        }

        private void FillLabelTypeCombo()
        {
            cmbLabelType.Clear();
            if (cmbtLabelType.Text != "Certified Mail Forms")
            {

                cmbLabelType.AddItem("Avery 5026,5027");
                cmbLabelType.ItemData(cmbLabelType.NewIndex, modLabels.CNSTLBLTYPE5026);
                cmbLabelType.AddItem("Avery 5066,5266");
                cmbLabelType.ItemData(cmbLabelType.NewIndex, modLabels.CNSTLBLTYPE5066);
                cmbLabelType.AddItem("Avery 5160,5260,5970");
                cmbLabelType.ItemData(cmbLabelType.NewIndex, modLabels.CNSTLBLTYPE5160);
                cmbLabelType.AddItem("Avery 5161,5261,5661");
                cmbLabelType.ItemData(cmbLabelType.NewIndex, modLabels.CNSTLBLTYPE5161);
                cmbLabelType.AddItem("Avery 5162,5262,5662");
                cmbLabelType.ItemData(cmbLabelType.NewIndex, modLabels.CNSTLBLTYPE5162);
                cmbLabelType.AddItem("Avery 5163,5263,5663");
                cmbLabelType.ItemData(cmbLabelType.NewIndex, modLabels.CNSTLBLTYPE5163);
                cmbLabelType.AddItem("Dymo 30252");
                cmbLabelType.ItemData(cmbLabelType.NewIndex, modLabels.CNSTLBLTYPEDYMO30252);
                cmbLabelType.AddItem("Dymo 30256");
                cmbLabelType.ItemData(cmbLabelType.NewIndex, modLabels.CNSTLBLTYPEDYMO30256);
            }
            else
            {
                cmbLabelType.AddItem("Hygrade Certified Mail - Laser");
                cmbLabelType.ItemData(cmbLabelType.NewIndex, modLabels.CNSTLBLTYPECERTMAILLASER);
            }
        }

        private void chkSecOwner_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cmbtLabelType.Text == "Selected Items")
            {
                CalcNumLines();
            }
        }

        private void cmbLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            string strDesc = "";
            bool boolShowChkSepMapLot;
            int intLabelIndex = 0;
            txtCMFNumber.Visible = false;
            boolShowChkSepMapLot = false;
            lblCMFNumber.Visible = false;
            chkIMPB.Visible = false;
            lblIMPB.Visible = false;
            txtIMPB.Visible = false;
            if (cmbLabelType.SelectedIndex < 0)
                return;
            chkSeparateMapLot.Visible = false;
            if (cmbtLabelType.Text == "Mailing Labels")
            {
                intLabelIndex = 0;
            }
            else if (cmbtLabelType.Text == "Address Labels - Card")
            {
                intLabelIndex = 1;
            }
            else if (cmbtLabelType.Text == "Address Labels - Card")
            {
                intLabelIndex = 2;
            }
            else if (cmbtLabelType.Text == "Assessment Labels - Card")
            {
                intLabelIndex = 3;
            }
            else if (cmbtLabelType.Text == "Selected Items")
            {
                intLabelIndex = 4;
            }
            else if (cmbtLabelType.Text == "Card Label")
            {
                intLabelIndex = 5;
            }
            switch (intLabelIndex)
            {
                case 0:
                    {
                        boolShowChkSepMapLot = true;
                        break;
                    }
                default:
                    {
                        boolShowChkSepMapLot = false;
                        break;
                    }
            }
            //end switch
            switch (cmbLabelType.ItemData(cmbLabelType.SelectedIndex))
            {
                case modLabels.CNSTLBLTYPECERTMAILLASER:
                    {
                        chkSeparateMapLot.Visible = false;
                        break;
                    }
                default:
                    {
                        if (boolShowChkSepMapLot)
                        {
                            chkSeparateMapLot.Visible = true;
                        }
                        break;
                    }
            }
            //end switch
           
                if (intLabelIndex == 0)
                    chkMapLot.Enabled = true;
                if (intLabelIndex == 2 || intLabelIndex == 3)
                {
                    chkSecOwner.Enabled = false;
                }
                else if (intLabelIndex != 4)
                {
                    chkSecOwner.Enabled = true;
                }
            

            chkSeparateMapLot.Visible = false;
            // for now, not using it
            switch (cmbLabelType.ItemData(cmbLabelType.SelectedIndex))
            {

                case modLabels.CNSTLBLTYPE5160:
                    {
                        strDesc = "Style 5160, 5260, 5560, 5660, 5960, 5970, 5971, 5972, 5979, 5980, 6241, 6460. Sheet of 3 x 10. 1 in. X 2 5/8 in.";
                        framLaser.Visible = true;
                        cmbCharWidth.Visible = false;
                        lblCharWidth.Visible = false;
                        intLinesAllowed = 6;
                        break;
                    }
                case modLabels.CNSTLBLTYPE5161:
                    {
                        strDesc = "Style 5161, 5261, 5661, 5961. Sheet of 2 X 10. 1 in. X 4 in.";
                        framLaser.Visible = true;
                        cmbCharWidth.Visible = false;
                        lblCharWidth.Visible = false;
                        intLinesAllowed = 6;
                        break;
                    }
                case modLabels.CNSTLBLTYPE5162:
                    {
                        strDesc = "Style 5162, 5262, 5662, 5962. Sheet of 2 X 7. 1 1/3 in. X 4 in.";
                        framLaser.Visible = true;
                        cmbCharWidth.Visible = false;
                        lblCharWidth.Visible = false;
                        intLinesAllowed = 8;
                        break;
                    }
                case modLabels.CNSTLBLTYPE5163:
                    {
                        strDesc = "Style 5163, 5263, 5663, 5963. Sheet of 2 X 5. 2 in. X 4 in.";
                        framLaser.Visible = true;
                        cmbCharWidth.Visible = false;
                        lblCharWidth.Visible = false;
                        intLinesAllowed = 12;
                        break;
                    }

                case modLabels.CNSTLBLTYPE5026:
                    {
                        strDesc = "Style 5026, 5027. Sheet of 2 X 9.  1 in. X 3.5 in.";
                        framLaser.Visible = true;
                        cmbCharWidth.Visible = false;
                        lblCharWidth.Visible = false;
                        intLinesAllowed = 6;
                        break;
                    }
                case modLabels.CNSTLBLTYPE5066:
                    {
                        strDesc = "Style 5066, 5266, 5366, 5666, 5766, 5866, 5966, 8366. Sheet of 2 X 15.  .66 in. X 3.5 in.";
                        framLaser.Visible = true;
                        cmbCharWidth.Visible = false;
                        lblCharWidth.Visible = false;
                        if (intLabelIndex == 0)
                        {
                            chkSecOwner.Enabled = false;
                            chkMapLot.Enabled = false;
                        }
                        intLinesAllowed = 3;
                        break;
                    }
                case modLabels.CNSTLBLTYPEDYMO30256:
                    {
                        strDesc = "Dymo Label Printer Style 30256.  2 5/16  in. X 4 in.";
                        framLaser.Visible = false;
                        cmbCharWidth.Visible = false;
                        lblCharWidth.Visible = false;
                        intLinesAllowed = 13;
                        break;
                    }
                case modLabels.CNSTLBLTYPEDYMO30252:
                    {
                        strDesc = "Dymo Label Printer Style 30252.  1.1 in. X 3.5 in.";
                        framLaser.Visible = false;
                        cmbCharWidth.Visible = false;
                        lblCharWidth.Visible = false;
                        intLinesAllowed = 6;
                        break;
                    }
                case modLabels.CNSTLBLTYPECERTMAILLASER:
                    {
                        strDesc = "Certified mail form from Hygrade for laser printers";
                        framLaser.Visible = true;
                        cmbCharWidth.Visible = false;
                        lblCharWidth.Visible = false;
                        chkSeparateMapLot.Visible = false;
                        txtCMFNumber.Visible = true;
                        lblCMFNumber.Visible = true;
                        chkIMPB.Visible = true;
                        lblIMPB.Visible = true;
                        txtIMPB.Visible = true;
                        intLinesAllowed = 6;
                        break;
                    }
            }
            //end switch
            lblDescription.Text = strDesc;
        }

        private void frmLabelPrinting_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = FCConvert.ToInt32(e.KeyData) / 0x10000;
            if (KeyCode == Keys.Escape)
            {
                KeyCode = (Keys)0;
                mnuExit_Click();
            }
        }

        private void frmLabelPrinting_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmLabelPrinting properties;
            //frmLabelPrinting.ScaleWidth	= 9045;
            //frmLabelPrinting.ScaleHeight	= 7410;
            //frmLabelPrinting.LinkTopic	= "Form1";
            //frmLabelPrinting.LockControls	= true;
            //End Unmaped Properties
            string strRep;
            int x;
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
            lblDescription.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
            FillLabelTypeCombo();
            cmbLabelType.SelectedIndex = 1;
            // the 4014 labels
            chkAccount.Enabled = false;
            chkRef1.Enabled = false;
            chkAddress.Enabled = false;
            chkMapLot.Enabled = true;
            chkETax.Enabled = false;
            ChkRef2.Enabled = false;
            ChkLocation.Enabled = false;
            chkAcres.Enabled = false;
            chkOwner.Enabled = false;
            // now check to see what the last label type was
            strRep = modRegistry.GetRegistryKey("RELastLabelType");
            if (Strings.Trim(strRep) != string.Empty)
            {
                if (Conversion.Val(strRep) >= 0)
                {
                    for (x = 0; x <= cmbLabelType.Items.Count - 1; x++)
                    {
                        if (cmbLabelType.ItemData(x) == Conversion.Val(strRep))
                        {
                            cmbLabelType.SelectedIndex = x;
                        }
                    }
                    // x
                }
            }

            strRep = modRegistry.GetRegistryKey("RELabelSeparateMapLot");
            if (Strings.Trim(strRep) != string.Empty && FCConvert.CBool(strRep))
            {
                chkSeparateMapLot.CheckState = Wisej.Web.CheckState.Checked;
            }
        }

        public void mnuExit_Click()
        {
            Unload();
        }

        private void mnuPrintPreview_Click(object sender, System.EventArgs e)
        {
            string strResponse = "";
            string minaccount;
            string maxaccount;
            short intHowToPrint;
            short intTypePrint = 0;
            short intOrdBy = 0;
            int bytOptions;
            string strOrder = "";
            string strYear;
            string strTax = "";
            float sngTax = 0;
            bool boolElim = false;
            string strSQL = "";
            // vbPorter upgrade warning: sngAdjust As Variant --> As double	OnWrite(double, short)
            double sngAdjust = 0;
            int intFirstLabelPos;
            int NRows = 0;
            int NCols = 0;
            string strPrinterToUse;
            try
            {
                // On Error GoTo ErrorHandler
                intHowToPrint = 0;
                bytOptions = 0;
                minaccount = " ";
                maxaccount = " ";
                strYear = " ";
                strPrinterToUse = "";
                intFirstLabelPos = 1;
                if (chkChooseLabelStart.CheckState == Wisej.Web.CheckState.Checked)
                {
                    switch (cmbLabelType.ItemData(cmbLabelType.SelectedIndex))
                    {
                        case modLabels.CNSTLBLTYPE5160:
                        case modLabels.CNSTLBLTYPE5161:
                        case modLabels.CNSTLBLTYPE5162:
                        case modLabels.CNSTLBLTYPE5163:
                        case modLabels.CNSTLBLTYPE5026:
                        case modLabels.CNSTLBLTYPE5066:
                            {
                                switch (cmbLabelType.ItemData(cmbLabelType.SelectedIndex))
                                {
                                    case modLabels.CNSTLBLTYPE5161:
                                    case modLabels.CNSTLBLTYPE5162:
                                    case modLabels.CNSTLBLTYPE5163:
                                    case modLabels.CNSTLBLTYPE5026:
                                    case modLabels.CNSTLBLTYPE5066:
                                        {
                                            NCols = 2;
                                            break;
                                        }
                                    case modLabels.CNSTLBLTYPE5160:
                                        {
                                            NCols = 3;
                                            break;
                                        }
                                }
                                //end switch
                                switch (cmbLabelType.ItemData(cmbLabelType.SelectedIndex))
                                {
                                    case modLabels.CNSTLBLTYPE5160:
                                    case modLabels.CNSTLBLTYPE5161:
                                        {
                                            NRows = 10;
                                            break;
                                        }
                                    case modLabels.CNSTLBLTYPE5162:
                                        {
                                            NRows = 7;
                                            break;
                                        }
                                    case modLabels.CNSTLBLTYPE5163:
                                        {
                                            NRows = 5;
                                            break;
                                        }
                                    case modLabels.CNSTLBLTYPE5026:
                                        {
                                            NRows = 9;
                                            break;
                                        }
                                    case modLabels.CNSTLBLTYPE5066:
                                        {
                                            NRows = 15;
                                            break;
                                        }
                                }
                                //end switch
                                intFirstLabelPos = frmChooseLabelPosition.InstancePtr.Init(NRows, NCols);
                                if (intFirstLabelPos < 0)
                                {
                                    // they aborted
                                    return;
                                }
                                break;
                            }
                        default:
                            {
                                intFirstLabelPos = 1;
                                break;
                            }
                    }
                    //end switch
                }
                if (chkEliminate.Checked && chkEliminate.Enabled)
                {
                    boolElim = true;
                }
                else
                {
                    boolElim = false;
                }
                // boolSaleData = False
                if (cmbtLabelType.Text == "Mailing Labels")
                {
                    intTypePrint = 1;
                }
                else if (cmbtLabelType.Text == "Address Labels - Card")
                {
                    intTypePrint = 2;
                }
                else if (cmbtLabelType.Text == "Change of Assessment")
                {
                    intTypePrint = 3;
                    if (chkETax.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        strTax = Interaction.InputBox("Please Enter the New Tax Rate Per 1000", "Enter Tax Rate", null/*, -1, -1*/);
                        sngTax = FCConvert.ToSingle(Conversion.Val(strTax));
                        if (sngTax <= 0)
                            return;
                    }
                }
                else if (cmbtLabelType.Text == "Assessment Labels - Card")
                {
                    intTypePrint = 4;
                    strYear = Interaction.InputBox("Please enter the Tax Year" + "\r\n" + "This may be in the form 2001 or 00/01", "Enter Tax Year", null/*, -1, -1*/);
                    strYear = Strings.Mid(strYear, 1, 5);
                    if (chkETax.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        strTax = Interaction.InputBox("Please Enter the New Tax Rate Per 1000", "Enter Tax Rate", null/*, -1, -1*/);
                        sngTax = FCConvert.ToSingle(Conversion.Val(strTax));
                        if (sngTax <= 0)
                            return;
                    }
                }
                else if (cmbtLabelType.Text == "Selected Items")
                {
                    intTypePrint = 5;
                }
                else if (cmbtLabelType.Text == "Card Label")
                {
                    intTypePrint = 7;
                    // 6 is address label from F2
                }
                else if (cmbtLabelType.Text == "Certified Mail Forms")
                {
                    intTypePrint = 1;
                    // same as mailing labels
                }
                if (cmbOrder.Text == "Account Number")
                {
                    strOrder = "Account";
                    intOrdBy = 1;
                }
                else if (cmbOrder.Text == "Map/Lot")
                {
                    strOrder = "Map/Lot";
                    intOrdBy = 2;
                }
                else if (cmbOrder.Text == "Zip Code")
                {
                    strOrder = "Zipcode";
                    intOrdBy = 3;
                }
                else if (cmbOrder.Text == "Name")
                {
                    strOrder = "Name";
                    intOrdBy = 4;
                }
                else if (cmbOrder.Text == "Location")
                {
                    strOrder = "Location";
                    intOrdBy = 5;
                }
                if (cmbtPrint.Text == "All Accounts")
                {
                    intHowToPrint = 1;
                }
                else if (cmbtPrint.Text == "Range")
                {
                minagain:
                    ;
                    strResponse = Interaction.InputBox("Please enter the minimum " + strOrder + " value", "Enter Minimum", null/*, -1, -1*/);
                    if (strResponse == string.Empty)
                        return;
                    if (cmbOrder.Text == "Account Number")
                    {
                        if (Conversion.Val(strResponse) < 1)
                        {
                            MessageBox.Show("This is an invalid minimum account", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            goto minagain;
                        }
                        minaccount = Strings.Trim(strResponse);
                    }
                    else
                    {
                        minaccount = Strings.Trim(modGlobalRoutines.escapequote(ref strResponse));
                        // minaccount = Trim(strResponse)
                    }
                maxagain:
                    ;
                    strResponse = Interaction.InputBox("Please enter the maximum " + strOrder + " value", "Enter Maximum", null/*, -1, -1*/);
                    if (strResponse == string.Empty)
                        return;
                    if (cmbOrder.Text == "Account Number")
                    {
                        if (Conversion.Val(strResponse) < 1 || Conversion.Val(strResponse) < Conversion.Val(minaccount))
                        {
                            MessageBox.Show("This is an invalid range", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            goto minagain;
                        }
                        maxaccount = Strings.Trim(strResponse);
                    }
                    else
                    {
                        // maxaccount = Trim(strResponse)
                        maxaccount = Strings.Trim(modGlobalRoutines.escapequote(ref strResponse));
                    }
                    intHowToPrint = 2;
                }
                else if (cmbtPrint.Text == "Individual")
                {
                    // must get individual accounts to save
                    modGlobalVariables.Statics.CancelledIt = false;
                    frmGetAccountsToPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    if (modGlobalVariables.Statics.CancelledIt)
                        return;
                    intHowToPrint = 3;
                }
                else if (cmbtPrint.Text == "From Extract")
                {
                    modGlobalVariables.Statics.CancelledIt = false;
                    frmGetGroupsToPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    if (modGlobalVariables.Statics.CancelledIt)
                        return;
                    intHowToPrint = 4;
                }
                // set options
                if (chkAccount.CheckState == Wisej.Web.CheckState.Checked && chkAccount.Enabled)
                {
                    bytOptions = bytOptions | ACCT_CODE;
                }
                if (chkRef1.CheckState == Wisej.Web.CheckState.Checked && chkRef1.Enabled)
                {
                    bytOptions = bytOptions | REF1_CODE;
                }
                if (chkAddress.CheckState == Wisej.Web.CheckState.Checked && chkAddress.Enabled)
                {
                    bytOptions = bytOptions | ADDR_CODE;
                }
                if (chkMapLot.CheckState == Wisej.Web.CheckState.Checked && chkMapLot.Enabled)
                {
                    bytOptions = bytOptions | MAP_CODE;
                }
                if (chkETax.CheckState == Wisej.Web.CheckState.Checked && chkETax.Enabled)
                {
                    bytOptions = bytOptions | ESTTAX_CODE;
                }
                if (ChkRef2.CheckState == Wisej.Web.CheckState.Checked && ChkRef2.Enabled)
                {
                    bytOptions = bytOptions | REF2_CODE;
                }
                if (ChkLocation.CheckState == Wisej.Web.CheckState.Checked && ChkLocation.Enabled)
                {
                    bytOptions = bytOptions | LOC_CODE;
                }
                if (chkSecOwner.CheckState == Wisej.Web.CheckState.Checked && chkSecOwner.Enabled)
                {
                    bytOptions = bytOptions | SEC_CODE;
                }
                if (chkAcres.CheckState == Wisej.Web.CheckState.Checked && chkAcres.Enabled)
                {
                    bytOptions = bytOptions | ACRES_CODE;
                }
                if (chkOwner.CheckState == Wisej.Web.CheckState.Checked && chkOwner.Enabled || cmbtLabelType.Text != "Selected Items")
                {
                    bytOptions = bytOptions | CNSTINCOWNER;
                }
                if (chkSeparateMapLot.Visible == true)
                {
                    modRegistry.SaveRegistryKey("RELabelSeparateMapLot", FCConvert.ToString(chkSeparateMapLot.CheckState == Wisej.Web.CheckState.Checked));
                }
                string strBarCode;
                strBarCode = "";
                string strIMPBCode;
                strIMPBCode = "";
                if (cmbLabelType.ItemData(cmbLabelType.SelectedIndex) == modLabels.CNSTLBLTYPECERTMAILLASER)
                {
                    strBarCode = Strings.Trim(txtCMFNumber.Text);
                    strBarCode = strBarCode.Replace(" ", "");
                    // this will remove any spaces in the string
                    strBarCode = strBarCode.Replace("-", "");
                    // this will remove any hyphens in the string
                    if (!CheckCMFBarCode(ref strBarCode))
                    {
                        MessageBox.Show("The Certified Mail barcode number is invalid. Please enter the correct 20-digit number.");
                        return;
                    }
                    if (!(chkIMPB.CheckState == Wisej.Web.CheckState.Checked))
                    {
                        strIMPBCode = Strings.Trim(txtIMPB.Text);
                        strIMPBCode = strIMPBCode.Replace(" ", "");
                        // this will remove any spaces in the string
                        strIMPBCode = strIMPBCode.Replace("-", "");
                        // this will remove any hyphens in the string
                        if (!CheckCMFBarCode_6(strIMPBCode, true))
                        {
                            MessageBox.Show("The IMPB tracking number is invalid. Please enter the correct 22-digit number.");
                            return;
                        }
                    }
                }
                
                    if (chkAdjust.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        sngAdjust = Conversion.Val(txtAlignment.Text);
                    }
                    else
                    {
                        sngAdjust = 0;
                    }
                    rptLaserLabels.InstancePtr.Init(boolElim, intHowToPrint, intTypePrint, intOrdBy, cmbLabelType.ItemData(cmbLabelType.SelectedIndex), bytOptions, minaccount, maxaccount, strYear, sngTax, strPrinterToUse, FCConvert.ToSingle(sngAdjust), FCConvert.ToInt32(Conversion.Val(txtNumDuplicates.Text)), intFirstLabelPos, strBarCode, chkEightPoint.CheckState == Wisej.Web.CheckState.Checked, false, true, strIMPBCode);
                

            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Label Printing form", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuReprintCertifiedMailList_Click(object sender, System.EventArgs e)
        {
            string strList = "";
            string strTemp = "";
            clsDRWrapper clsLoad = new clsDRWrapper();
            clsLoad.OpenRecordset("select top 1 * from cmfnumbers ", modGlobalVariables.strREDatabase);
            if (clsLoad.EndOfFile())
            {
                MessageBox.Show("There are no certified mail lists to show", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            frmReprintCMFList.InstancePtr.Show(App.MainForm);
        }

        private void OptLabelType_CheckedChanged(int Index, object sender, System.EventArgs e)
        {
            bool boolShowChkSepMapLot = false;
            int x;
            cmbLabelType.Enabled = true;
            chkSecOwner.Enabled = true;
            chkIgnoreNumLines.Visible = false;
            chkSeparateMapLot.Visible = false;
            chkOwner.Enabled = false;
            switch (cmbLabelType.SelectedItem.ToString())
            {
                case "Mailing Labels":
                case "Address Labels - Card":
                case "Selected Items":
                    {
                        boolShowChkSepMapLot = true;
                        break;
                    }
                default:
                    {
                        boolShowChkSepMapLot = false;
                        break;
                    }
            }
            //end switch
            if (cmbLabelType.SelectedIndex >= 0)
            {
 
                            if (boolShowChkSepMapLot)
                            {
                                chkSeparateMapLot.Visible = true;
                            }
 
            }
            chkSeparateMapLot.Visible = false;

            switch (cmbtLabelType.Text)
            {
                // for now not using it
                case "Mailing Labels":
                {
                    chkAccount.Enabled = false;
                    chkRef1.Enabled = false;
                    ChkRef2.Enabled = false;
                    ChkLocation.Enabled = false;
                    chkAddress.Enabled = false;
                    if (intLinesAllowed > 4)
                    {
                        chkMapLot.Enabled = true;
                        chkSecOwner.Enabled = true;
                    }
                    else
                    {
                        chkSecOwner.Enabled = false;
                        chkMapLot.Enabled = false;
                    }
                    chkETax.Enabled = false;
                    chkEliminate.Enabled = true;
                    chkAcres.Enabled = false;
                    // optOrder(2).Enabled = True
                    break;
                }

                case "Address Labels - Card":
                    chkAccount.Enabled = false;
                    chkRef1.Enabled = false;
                    ChkRef2.Enabled = false;
                    ChkLocation.Enabled = false;
                    chkAddress.Enabled = false;
                    chkMapLot.Enabled = false;
                    chkETax.Enabled = false;
                    chkEliminate.Enabled = false;
                    chkAcres.Enabled = false;

                    break;
                case "Change of Assessment":
                    chkAccount.Enabled = false;
                    chkRef1.Enabled = false;
                    ChkRef2.Enabled = false;
                    ChkLocation.Enabled = false;
                    chkAddress.Enabled = false;
                    chkMapLot.Enabled = false;
                    chkETax.Enabled = true;
                    chkEliminate.Enabled = false;
                    chkSecOwner.Enabled = false;
                    chkAcres.Enabled = false;
                    // optOrder(2).Enabled = True
                    break;
                case "Assessment Labels - Card":

                    // assessment labels card
                    chkSeparateMapLot.Visible = false;
                    chkAccount.Enabled = false;
                    chkRef1.Enabled = false;
                    ChkRef2.Enabled = false;
                    ChkLocation.Enabled = false;
                    chkAddress.Enabled = false;
                    chkMapLot.Enabled = false;
                    chkAcres.Enabled = false;
                    chkETax.Enabled = true;
                    chkEliminate.Enabled = false;

                    cmbLabelType.Enabled = false;
                    chkSecOwner.Enabled = false;
                    // optOrder(2).Enabled = True
                    break;
                case "Selected Items":
                    chkOwner.Enabled = true;
                    chkAccount.Enabled = true;
                    chkRef1.Enabled = true;
                    ChkRef2.Enabled = true;
                    ChkLocation.Enabled = true;
                    chkAddress.Enabled = true;
                    chkMapLot.Enabled = true;
                    chkAcres.Enabled = true;
                    chkETax.Enabled = false;
                    chkEliminate.Enabled = true;
                    chkIgnoreNumLines.Visible = true;

                    break;
                case "Card Label":
                    chkAccount.Enabled = false;
                    chkRef1.Enabled = false;
                    ChkRef2.Enabled = false;
                    ChkLocation.Enabled = false;
                    chkAddress.Enabled = false;
                    chkMapLot.Enabled = false;
                    chkETax.Enabled = false;
                    chkEliminate.Enabled = false;
                    chkAcres.Enabled = false;

                    break;
                case "Certified Mail Forms":
                    chkAccount.Enabled = false;
                    chkRef1.Enabled = false;
                    ChkRef2.Enabled = false;
                    ChkLocation.Enabled = false;
                    chkAddress.Enabled = false;
                    chkMapLot.Enabled = true;
                    chkETax.Enabled = false;
                    chkEliminate.Enabled = true;
                    chkAcres.Enabled = false;

                    break;
            }
            string strRep = "";
            if (cmbtLabelType.Text != "Certified Mail Forms")
            {
                switch (cmbLabelType.ItemData(0))
                {
                    case modLabels.CNSTLBLTYPECERTMAILLASER:
                        {
                            FillLabelTypeCombo();
                            cmbLabelType.SelectedIndex = 0;
                            // now check to see what the last label type was
                            strRep = modRegistry.GetRegistryKey("RELastLabelType");
                            if (Strings.Trim(strRep) != string.Empty)
                            {
                                if (Conversion.Val(strRep) >= 0)
                                {
                                    for (x = 0; x <= cmbLabelType.Items.Count - 1; x++)
                                    {
                                        if (cmbLabelType.ItemData(x) == Conversion.Val(strRep))
                                        {
                                            cmbLabelType.SelectedIndex = x;
                                        }
                                    }
                                    // x
                                }
                            }
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
                //end switch
            }
            else
            {
                chkEliminate.CheckState = Wisej.Web.CheckState.Checked;
                // don't want card 2 showing up for certified mail
                switch (cmbLabelType.ItemData(0))
                {
                    case modLabels.CNSTLBLTYPECERTMAILLASER:
                        {
                            break;
                        }
                    default:
                        {
                            FillLabelTypeCombo();
                            cmbLabelType.SelectedIndex = 0;
                            // now check to see what the last label type was
                            strRep = modRegistry.GetRegistryKey("RELastLabelType");
                            if (Strings.Trim(strRep) != string.Empty)
                            {
                                if (Conversion.Val(strRep) >= 0)
                                {
                                    for (x = 0; x <= cmbLabelType.Items.Count - 1; x++)
                                    {
                                        if (cmbLabelType.ItemData(x) == Conversion.Val(strRep))
                                        {
                                            cmbLabelType.SelectedIndex = x;
                                        }
                                    }
                                    // x
                                }
                            }
                            break;
                        }
                }
                //end switch
            }
        }

        private void OptLabelType_CheckedChanged(object sender, System.EventArgs e)
        {
            int index = cmbLabelType.SelectedIndex;
            OptLabelType_CheckedChanged(index, sender, e);
        }

        private bool CheckCMFBarCode_6(string strMFCode, bool boolIMPB = false)
        {
            return CheckCMFBarCode(ref strMFCode, boolIMPB);
        }

        private bool CheckCMFBarCode(ref string strMFCode, bool boolIMPB = false)
        {
            bool CheckCMFBarCode = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                string strCheckDigit = "";
                string strTemp = "";
                int intCodeLen = 0;
                bool boolTemp;
                if (boolIMPB)
                {
                    intCodeLen = 22;
                }
                else
                {
                    intCodeLen = 20;
                }
                boolTemp = false;
                if (Information.IsNumeric(strMFCode))
                {
                    if (strMFCode.Length == intCodeLen)
                    {
                        strCheckDigit = Strings.Right(strMFCode, 1);
                        strTemp = Strings.Left(strMFCode, intCodeLen - 1);
                        strTemp = modGlobalFunctions.CalcCMFCheckDigit(ref strTemp);
                        // this will check the check digit to the calculated check digit to make sure that this is a valid number
                        boolTemp = FCConvert.CBool(strTemp == strCheckDigit);
                    }
                }
                CheckCMFBarCode = boolTemp;
                return CheckCMFBarCode;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Mail Form Code Calculation", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                CheckCMFBarCode = false;
            }
            return CheckCMFBarCode;
        }
    }
}
