﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;
using SharedApplication.Extensions;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPendingTransfers.
	/// </summary>
	public partial class frmPendingTransfers : BaseForm
	{
		public frmPendingTransfers()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPendingTransfers InstancePtr
		{
			get
			{
				return (frmPendingTransfers)Sys.GetInstance(typeof(frmPendingTransfers));
			}
		}

		protected frmPendingTransfers _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmPendingTransfers_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this);
			SetupGridXfer();
			SetupGridChanges();
			FillGridChanges();
			FillGridXfer();
		}

		private void frmPendingTransfers_Resize(object sender, System.EventArgs e)
		{
			ResizeGridXfer();
			ResizeGridChanges();
		}

		private void GridChanges_DblClick(object sender, System.EventArgs e)
		{
			mnuEditCurrentPending_Click();
		}

		private void GridChanges_RowColChange(object sender, System.EventArgs e)
		{
			if (GridChanges.Col == 3)
			{
				GridChanges.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				GridChanges.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void GridXfer_DblClick(object sender, System.EventArgs e)
		{
			mnuEditCurrentPending_Click();
		}

		private void GridXfer_RowColChange(object sender, System.EventArgs e)
		{
			if (GridXfer.Col == 6)
			{
				GridXfer.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				GridXfer.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void mnuDel_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsExecute = new clsDRWrapper();
			if (GridChanges.Visible == true)
			{
				if (GridChanges.Row < 1)
					return;
				// Call clsExecute.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Delete Pending Change','Deleted a pending change for account " & .TextMatrix(.Row, 1) & " card " & .TextMatrix(.Row, 2) & " Transaction " & .TextMatrix(.Row, 0) & "')", strredatabase)
				modGlobalFunctions.AddCYAEntry_26("RE", "Delete Pending Change", "Deleted a pending change for account " + GridChanges.TextMatrix(GridChanges.Row, 1) + " card " + GridChanges.TextMatrix(GridChanges.Row, 2) + " Transaction " + GridChanges.TextMatrix(GridChanges.Row, 0));
				clsExecute.Execute("delete from pendingchanges where transactionnumber = " + GridChanges.TextMatrix(GridChanges.Row, 0), modGlobalVariables.strREDatabase);
				GridChanges.RemoveItem(GridChanges.Row);
			}
			else
			{
				if (GridXfer.Row < 1)
					return;
				// Call clsExecute.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Delete Pending Transfer','Deleted a pending Transfer for account " & .TextMatrix(.Row, 1) & " Transaction " & .TextMatrix(.Row, 0) & "')", strredatabase)
				modGlobalFunctions.AddCYAEntry_26("RE", "Delete Pending Transfer", "Deleted a pending Transfer for account " + GridXfer.TextMatrix(GridXfer.Row, 1) + " Transaction " + GridXfer.TextMatrix(GridXfer.Row, 0));
				clsExecute.Execute("delete from pending where id = " + GridXfer.TextMatrix(GridXfer.Row, 0), modGlobalVariables.strREDatabase);
				GridXfer.RemoveItem(GridXfer.Row);
			}
		}

		private void mnuEditCurrentPending_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			if (GridChanges.Visible)
			{
				if (GridChanges.Row < 1)
					return;
				if (!modREMain.Statics.boolShortRealEstate)
				{
					frmREProperty.InstancePtr.Init(FCConvert.ToInt32(GridChanges.TextMatrix(GridChanges.Row, 1)), FCConvert.ToInt16(GridChanges.TextMatrix(GridChanges.Row, 2)), true, FCConvert.ToInt32(GridChanges.TextMatrix(GridChanges.Row, 0)));
				}
				else
				{
					frmSHREBLUpdate.InstancePtr.Init(FCConvert.ToInt32(GridChanges.TextMatrix(GridChanges.Row, 1)), FCConvert.ToInt16(GridChanges.TextMatrix(GridChanges.Row, 2)), true, FCConvert.ToInt32(GridChanges.TextMatrix(GridChanges.Row, 0)));
				}
			}
			else
			{
				if (GridXfer.Row < 1)
					return;
				frmTransferAccount.InstancePtr.Init(FCConvert.ToInt32(GridXfer.TextMatrix(GridXfer.Row, 0)), true);
				clsLoad.OpenRecordset("select * from pending where id = " + GridXfer.TextMatrix(GridXfer.Row, 0), modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					GridXfer.RowData(GridXfer.Row, Strings.Format(clsLoad.Get_Fields_DateTime("mindate"), "MM/dd/yyyy"));
					GridXfer.TextMatrix(GridXfer.Row, 6, Strings.Format(clsLoad.Get_Fields_DateTime("mindate"), "MM/dd/yyyy"));
					if (clsLoad.Get_Fields_Int32("transfertype") == 0)
					{
						GridXfer.TextMatrix(GridXfer.Row, 4, "Account");
					}
					else if (clsLoad.Get_Fields_Int32("transfertype") == 1)
					{
						GridXfer.TextMatrix(GridXfer.Row, 4, "Card(s)");
					}
					if (clsLoad.Get_Fields_Int32("transferwhat") == 0)
					{
						GridXfer.TextMatrix(GridXfer.Row, 5, "Same");
					}
					else if (clsLoad.Get_Fields_Int32("transferwhat") == 1)
					{
						GridXfer.TextMatrix(GridXfer.Row, 5, "New");
					}
					else if (clsLoad.Get_Fields_Int32("transferwhat") == 2)
					{
						GridXfer.TextMatrix(GridXfer.Row, 5, FCConvert.ToString(clsLoad.Get_Fields_Int32("toaccount")));
					}
				}
			}
		}

		public void mnuEditCurrentPending_Click()
		{
			mnuEditCurrentPending_Click(mnuEditCurrentPending, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void SetupGridXfer()
		{
			GridXfer.Cols = 7;
			GridXfer.TextMatrix(0, 0, "Transaction");
			GridXfer.TextMatrix(0, 1, "Account");
			GridXfer.TextMatrix(0, 2, "Name");
			GridXfer.TextMatrix(0, 3, "Map / Lot");
			GridXfer.TextMatrix(0, 4, "Transfer");
			GridXfer.TextMatrix(0, 5, "Xfer to");
			GridXfer.TextMatrix(0, 6, "Min Date");
            GridXfer.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridXfer.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridXfer.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridXfer.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridXfer.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void SetupGridChanges()
		{
			GridChanges.TextMatrix(0, 0, "Transaction");
			GridChanges.TextMatrix(0, 1, "Account");
			GridChanges.TextMatrix(0, 2, "Card");
			GridChanges.TextMatrix(0, 3, "Min Date");
			GridChanges.TextMatrix(0, 4, "Name");
			GridChanges.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridChanges.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridChanges.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GridChanges.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void ResizeGridXfer()
		{
			int GridWidth = 0;
			//FC:FINAL:MSH - i.issue #1190: replace 'Width' by 'WidthOriginal' for correct calculation of columns size
			//GridWidth = GridXfer.Width;
			GridWidth = GridXfer.WidthOriginal;
			GridXfer.ColWidth(0, FCConvert.ToInt32(0.12 * GridWidth));
			GridXfer.ColWidth(1, FCConvert.ToInt32(0.1 * GridWidth));
			GridXfer.ColWidth(2, FCConvert.ToInt32(0.27 * GridWidth));
			GridXfer.ColWidth(3, FCConvert.ToInt32(0.14 * GridWidth));
			GridXfer.ColWidth(4, FCConvert.ToInt32(0.11 * GridWidth));
			GridXfer.ColWidth(5, FCConvert.ToInt32(0.1 * GridWidth));
		}

		private void ResizeGridChanges()
		{
			int GridWidth = 0;
			//FC:FINAL:MSH - i.issue #1190: replace 'Width' by 'WidthOriginal' for correct calculation of columns size
			//GridWidth = GridChanges.Width;
			GridWidth = GridChanges.WidthOriginal;
			GridChanges.ColWidth(0, FCConvert.ToInt32(0.12 * GridWidth));
			GridChanges.ColWidth(1, FCConvert.ToInt32(0.12 * GridWidth));
			GridChanges.ColWidth(2, FCConvert.ToInt32(0.1 * GridWidth));
			GridChanges.ColWidth(3, FCConvert.ToInt32(0.12 * GridWidth));
			GridChanges.ColWidth(4, FCConvert.ToInt32(0.4 * GridWidth));
		}

		private void FillGridChanges()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsMaster = new clsDRWrapper();
				string strREFullDBName;
				string strMasterJoin;
				string strMasterJoinJoin = "";
				strREFullDBName = clsLoad.Get_GetFullDBName("RealEstate");
				strMasterJoin = modREMain.GetMasterJoin();
				string strMasterJoinQuery;
				strMasterJoinQuery = "(" + strMasterJoin + ") mj";
				clsLoad.OpenRecordset("select account,transactionnumber,card,mindate from pendingchanges GROUP by transactionnumber,account,card,mindate order by transactionnumber", modGlobalVariables.strREDatabase);
				clsMaster.OpenRecordset("select rsaccount,rsname from " + strMasterJoinQuery + " where rsaccount in (select account from pendingchanges)", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [card] and replace with corresponding Get_Field method
					GridChanges.AddItem(clsLoad.Get_Fields_Int32("transactionnumber") + "\t" + clsLoad.Get_Fields("account") + "\t" + clsLoad.Get_Fields("card") + "\t" + Strings.Format(clsLoad.Get_Fields("mindate"), "MM/dd/yyyy"));
					GridChanges.RowData(GridChanges.Rows - 1, GridChanges.TextMatrix(GridChanges.Rows - 1, 3));
					// Call clsMaster.FindFirstRecord("rsaccount", clsLoad.Fields("account"))
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					if (clsMaster.FindFirst("rsaccount = " + clsLoad.Get_Fields("account")))
					{
						// If Not clsMaster.NoMatch Then
						GridChanges.TextMatrix(GridChanges.Rows - 1, 4, Strings.Trim(FCConvert.ToString(clsMaster.Get_Fields_String("rsname"))));
					}
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In fill grid changes", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillGridXfer()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsMaster = new clsDRWrapper();
				string strREFullDBName;
				string strMasterJoin;
				string strMasterJoinJoin = "";
				strREFullDBName = clsLoad.Get_GetFullDBName("RealEstate");
				strMasterJoin = modREMain.GetMasterJoin();
				string strMasterJoinQuery;
				strMasterJoinQuery = "(" + strMasterJoin + ") mj";
				clsLoad.OpenRecordset("Select * from pending order by id", modGlobalVariables.strREDatabase);
				clsMaster.OpenRecordset("select rsaccount,rsname,rsmaplot from " + strMasterJoinQuery + " where rscard = 1 and rsaccount in (select account from pending)", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					GridXfer.AddItem(clsLoad.Get_Fields_Int32("id") + "\t" + clsLoad.Get_Fields("account") + "\t" + "\t" + "\t" + "\t" + "\t" + Strings.Format(clsLoad.Get_Fields_DateTime("mindate"), "MM/dd/yyyy"));
					GridXfer.RowData(GridXfer.Rows - 1, Strings.Format(clsLoad.Get_Fields_DateTime("mindate"), "MM/dd/yyyy"));
					// Call clsMaster.FindFirstRecord("rsaccount", clsLoad.Fields("account"))
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					if (clsMaster.FindFirst("rsaccount = " + clsLoad.Get_Fields("account")))
					{
						// If Not clsMaster.NoMatch Then
						GridXfer.TextMatrix(GridXfer.Rows - 1, 2, Strings.Trim(FCConvert.ToString(clsMaster.Get_Fields_String("rsname"))));
						GridXfer.TextMatrix(GridXfer.Rows - 1, 3, Strings.Trim(FCConvert.ToString(clsMaster.Get_Fields_String("rsmaplot"))));
					}
					if (clsLoad.Get_Fields_Int32("transfertype") == 0)
					{
						GridXfer.TextMatrix(GridXfer.Rows - 1, 4, "Account");
					}
					else if (clsLoad.Get_Fields_Int32("transfertype") == 1)
					{
						GridXfer.TextMatrix(GridXfer.Rows - 1, 4, "Card(s)");
					}
					if (clsLoad.Get_Fields_Int32("transferwhat") == 0)
					{
						GridXfer.TextMatrix(GridXfer.Rows - 1, 5, "Same");
					}
					else if (clsLoad.Get_Fields_Int32("transferwhat") == 1)
					{
						GridXfer.TextMatrix(GridXfer.Rows - 1, 5, "New");
					}
					else if (clsLoad.Get_Fields_Int32("transferwhat") == 2)
					{
						GridXfer.TextMatrix(GridXfer.Rows - 1, 5, FCConvert.ToString(clsLoad.Get_Fields_Int32("toaccount")));
					}
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Fill Xfer Grid", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void UpdateMinDates()
		{
			int x;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				for (x = 1; x <= GridChanges.Rows - 1; x++)
				{
					if (Information.IsDate(GridChanges.TextMatrix(x, 3)))
					{
						//FC:FINAL:DDU:#i1287 - added verification for RowData
						if (Information.IsDate(GridChanges.RowData(x)))
						{
							if (DateAndTime.DateDiff("d", FCConvert.ToDateTime(GridChanges.TextMatrix(x, 3)), Convert.ToDateTime(GridChanges.RowData(x))) != 0)
							{
								// changed the date so put in cya table and then change it
								// Call clsSave.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Changed Pending MinDate','Changed Mindate for transaction " & GridChanges.TextMatrix(x, 0) & " Account " & GridChanges.TextMatrix(x, 1) & " to " & GridChanges.TextMatrix(x, 3) & "')", strredatabase)
								modGlobalFunctions.AddCYAEntry_26("RE", "Changed Pending MinDate", "Changed Mindate for transaction " + GridChanges.TextMatrix(x, 0) + " Account " + GridChanges.TextMatrix(x, 1) + " to " + GridChanges.TextMatrix(x, 3));
								clsSave.Execute("update pendingchanges SET MINdate = #" + GridChanges.TextMatrix(x, 3) + "# where transactionnumber = " + FCConvert.ToString(Conversion.Val(GridChanges.TextMatrix(x, 0))), modGlobalVariables.strREDatabase);
								GridChanges.RowData(x, GridChanges.TextMatrix(x, 3));
							}
						}
					}
				}
				// x
				for (x = 1; x <= GridXfer.Rows - 1; x++)
				{
					if (Information.IsDate(GridXfer.TextMatrix(x, 6)))
					{
						//FC:FINAL:DDU:#i1287 - added verification for RowData
						if (Information.IsDate(GridXfer.RowData(x)))
						{
							if (DateAndTime.DateDiff("d", FCConvert.ToDateTime(GridXfer.TextMatrix(x, 6)), Convert.ToDateTime(GridXfer.RowData(x))) != 0)
							{
								// Call clsSave.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Changed Pending Transfer MinDate','Changed Transfer Mindate for transaction " & GridXfer.TextMatrix(x, 0) & " Account " & GridXfer.TextMatrix(x, 1) & " to " & GridXfer.TextMatrix(x, 6) & "')", strredatabase)
								modGlobalFunctions.AddCYAEntry_26("RE", "Changed Pending Transfer MinDate", "Changed Transfer Mindate for transaction " + GridXfer.TextMatrix(x, 0) + " Account " + GridXfer.TextMatrix(x, 1) + " to " + GridXfer.TextMatrix(x, 6));
								clsSave.Execute("update pending SET MINdate = '" + GridXfer.TextMatrix(x, 6) + "' where id = " + FCConvert.ToString(Conversion.Val(GridXfer.TextMatrix(x, 0))), modGlobalVariables.strREDatabase);
								GridXfer.RowData(x, GridXfer.TextMatrix(x, 6));
							}
						}
					}
				}
				// x
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In UpdateMinDates", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuPendingTransfersReport_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptPendingXfers.InstancePtr);
		}

		private void mnuPrintPendingChanges_Click(object sender, System.EventArgs e)
		{
			rptPendingChanges.InstancePtr.Init();
		}

		private void ProcessPendingChanges()
		{
			clsDRWrapper clsPending = new clsDRWrapper();
			string strValue = "";
			int lngTransactionID;
			int lngOldAcct;
			int intoldcard;
			clsDRWrapper clsSave = new clsDRWrapper();
			int intLine = 0;
			DateTime dtDate;
			try
			{
				// On Error GoTo ErrorHandler
				clsPending.OpenRecordset("select * from pendingchanges where mindate <= '" + Strings.Format(DateTime.Today, "MM/dd/yyyy") + "'  order by transactionnumber, id", modGlobalVariables.strREDatabase);
				lngTransactionID = -1;
				intoldcard = 0;
				lngOldAcct = 0;
				if (!clsPending.EndOfFile())
				{
					while (!clsPending.EndOfFile())
					{
						if (lngTransactionID != Conversion.Val(clsPending.Get_Fields_Int32("transactionnumber")))
						{
							clsSave.Execute("delete from pendingchanges where tablename = 'bookpage' and transactionnumber = " + FCConvert.ToString(lngTransactionID), modGlobalVariables.strREDatabase, false);
							lngTransactionID = FCConvert.ToInt32(Math.Round(Conversion.Val(clsPending.Get_Fields_Int32("transactionnumber"))));
                            GridReport.AddItem(clsPending.Get_Fields("account") + "\t" + clsPending.Get_Fields("card") + "\t" + "Processed changes" + "\t" + Strings.Format(clsPending.Get_Fields_DateTime("mindate"), "MM/dd/yyyy") + "\t" + "\t" + "Changes");
                            modGlobalFunctions.AddCYAEntry_80("RE", "Processed Pending", "Processed Changes for " + clsPending.Get_Fields("account"), "Card " + clsPending.Get_Fields("card"));
							intLine = 0;
						}
						if (clsPending.Get_Fields_String("fieldtype") == DataTypeEnum.dbLong.ToStringES() || clsPending.Get_Fields_String("fieldtype") == DataTypeEnum.dbInteger.ToStringES() || clsPending.Get_Fields_String("fieldtype") == DataTypeEnum.dbNumeric.ToStringES() || clsPending.Get_Fields_String("fieldtype") == DataTypeEnum.dbFloat.ToStringES() || clsPending.Get_Fields_String("fieldtype") == DataTypeEnum.dbDouble.ToStringES())
						{
							strValue = FCConvert.ToString(clsPending.Get_Fields_String("fieldvalue"));
						}
						else if (clsPending.Get_Fields_String("fieldtype") == DataTypeEnum.dbDate.ToStringES())
						{
							strValue = "'" + clsPending.Get_Fields_String("fieldvalue") + "'";
						}
						else if (clsPending.Get_Fields_String("fieldtype") == DataTypeEnum.dbBoolean.ToStringES())
						{
							strValue = FCConvert.ToString(clsPending.Get_Fields_String("fieldvalue"));
						}
						else
						{
							strValue = "'" + modGlobalFunctions.EscapeQuotes(clsPending.Get_Fields_String("fieldvalue")) + "'";
						}
						if (Strings.UCase(FCConvert.ToString(clsPending.Get_Fields_String("tablename"))) != "BOOKPAGE" && Strings.UCase(FCConvert.ToString(clsPending.Get_Fields_String("tablename"))) != "GRIDPHONES")
						{
                            if (clsPending.Execute("update " + clsPending.Get_Fields_String("tablename") + " set " + clsPending.Get_Fields_String("fieldname") + " = " + strValue + " where RSACCOUNT = " + clsPending.Get_Fields("account") + " and rscard = " + clsPending.Get_Fields("card"), modGlobalVariables.strREDatabase))
							{
								// successful
								clsPending.Edit();
								clsPending.Delete();
								clsPending.Update();
							}
							else
							{
								// unsuccessful
                                GridReport.AddItem(clsPending.Get_Fields("account") + "\t" + clsPending.Get_Fields("card") + "\t" + "Error in Transaction # " + FCConvert.ToString(lngTransactionID) + " ChangeID: " + clsPending.Get_Fields_Int32("id") + "\t" + "\t" + "Changes");
							}
						}
						else if (Strings.UCase(FCConvert.ToString(clsPending.Get_Fields_String("tablename"))) == "GRIDPHONES")
						{
							if (Strings.UCase(FCConvert.ToString(clsPending.Get_Fields_String("fieldname"))) == "DESCRIPTION")
							{
                                if (clsPending.Execute("update phonenumbers set phonedescription = " + strValue + " where parentid = " + clsPending.Get_Fields("account"), modGlobalVariables.strREDatabase))
								{
									clsPending.Edit();
									clsPending.Delete();
									clsPending.Update();
								}
								else
								{
                                    GridReport.AddItem(clsPending.Get_Fields("account") + "\t" + clsPending.Get_Fields("card") + "\t" + "Error in Transaction # " + FCConvert.ToString(lngTransactionID) + " ChangeID: " + clsPending.Get_Fields_Int32("id") + "\t" + "\t" + "Changes");
								}
							}
							else
							{
                                if (clsPending.Execute("update phonenumbers set " + clsPending.Get_Fields_String("fieldname") + " = " + strValue + " where parentid = " + clsPending.Get_Fields("account"), modGlobalVariables.strREDatabase))
								{
									clsPending.Edit();
									clsPending.Delete();
									clsPending.Update();
								}
								else
								{
                                    GridReport.AddItem(clsPending.Get_Fields("account") + "\t" + clsPending.Get_Fields("card") + "\t" + "Error in Transaction # " + FCConvert.ToString(lngTransactionID) + " ChangeID: " + clsPending.Get_Fields_Int32("id") + "\t" + "\t" + "Changes");
								}
							}
						}
						else
						{
							if (Strings.UCase(FCConvert.ToString(clsPending.Get_Fields_String("fieldname"))) == "BOOK")
							{
								intLine += 1;
								if (intLine == 1)
								{
                                    clsSave.Execute("delete from bookpage where account = " + clsPending.Get_Fields("account"), modGlobalVariables.strREDatabase);
								}
								clsSave.OpenRecordset("select * from bookpage where account = -3", modGlobalVariables.strREDatabase);
								clsSave.AddNew();
                                clsSave.Set_Fields("account", clsPending.Get_Fields("account"));
								clsSave.Set_Fields("card", 1);
								clsSave.Set_Fields("Line", intLine);
								clsSave.Set_Fields("book", clsPending.Get_Fields_String("fieldvalue"));
							}
							else if (Strings.UCase(FCConvert.ToString(clsPending.Get_Fields_String("fieldname"))) == "PAGE")
							{
								clsSave.Set_Fields("page", clsPending.Get_Fields_String("fieldvalue"));
							}
							else if (Strings.UCase(FCConvert.ToString(clsPending.Get_Fields_String("fieldname"))) == "BPDATE")
							{
								clsSave.Set_Fields("bpdate", clsPending.Get_Fields_String("fieldvalue"));
							}
							else if (Strings.UCase(FCConvert.ToString(clsPending.Get_Fields_String("fieldname"))) == "SALEDATE")
							{
								if (Conversion.Val(clsPending.Get_Fields_String("fieldvalue")) == 0)
								{
									clsSave.Set_Fields("saledate", 0);
								}
								else
								{
									clsSave.Set_Fields("saledate", clsPending.Get_Fields_String("fieldvalue"));
								}
							}
							else if (Strings.UCase(FCConvert.ToString(clsPending.Get_Fields_String("fieldname"))) == "CURRENT")
							{
								clsSave.Set_Fields("Current", FCConvert.CBool(clsPending.Get_Fields_String("fieldvalue")));
								clsSave.Update();
							}
						}
						clsPending.MoveNext();
					}
					clsSave.Execute("delete from pendingchanges where tablename = 'bookpage' and transactionnumber = " + FCConvert.ToString(lngTransactionID), modGlobalVariables.strREDatabase, false);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In process pending changes", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSaveChanges_Click(object sender, System.EventArgs e)
		{
			UpdateMinDates();
		}

		private void mnuToggleView_Click(object sender, System.EventArgs e)
		{
			if (GridChanges.Visible == true)
			{
				GridChanges.Visible = false;
				GridXfer.Visible = true;
				mnuToggleView.Text = "View pending changes";
				lblGridTitle.Text = "Pending Transfers";
			}
			else
			{
				GridChanges.Visible = true;
				GridXfer.Visible = false;
				mnuToggleView.Text = "View pending transfers";
				lblGridTitle.Text = "Pending Changes";
			}
		}

		private void cmdProcess_Click(object sender, EventArgs e)
		{
			// This subroutine will go through the list of pending transfers and perform any transfers that can
			// be done as of today's date
			clsDRWrapper clsPending = new clsDRWrapper();
			int lngAccount = 0;
			string strSQL = "";
			string strCols = "";
			bool boolSaveSales = false;
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper clsExecute = new clsDRWrapper();
			// vbPorter upgrade warning: dtSaleDate As DateTime	OnWriteFCConvert.ToInt16(
			DateTime dtSaleDate = default(DateTime);
			int intLine = 0;
			int lngMastAccount = 0;
			int intCard = 0;
			int intNewCard = 0;
			string strList = "";
			string strTemp = "";
			string[] Cardarray = null;
			int x;
			// vbPorter upgrade warning: intModal As short --> As int	OnWrite(FCForm.FormShowEnum)
			int intModal;
			bool boolDontAdd = false;
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = clsPending.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			try
			{
				// On Error GoTo ErrorHandler
				if (MessageBox.Show("This will process all pending activities with a minimum date of today or prior" + "\r\n" + "Are you sure you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				{
					return;
				}
				UpdateMinDates();
				ProcessPendingChanges();
				clsPending.OpenRecordset("select * from pending where mindate <= '" + Strings.Format(DateTime.Today, "MM/dd/yyyy") + "' order by id", modGlobalVariables.strREDatabase);
				// Load frmREWait
				// 
				// frmREWait.lblMessage = "Please Wait."
				frmWait.InstancePtr.Init("Please Wait.", false, 100, true);
				while (!clsPending.EndOfFile())
				{
					boolSaveSales = false;
					bool executeTransferWhole = false;
					if (clsPending.Get_Fields_Int32("transferwhat") == 0)
					{
						// transferring to same account. I.E. selling the whole account
						// if there is a sale date and the sale is valid, it will also be saved in the
						// sale records
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						lngAccount = FCConvert.ToInt32(clsPending.Get_Fields("account"));
						GridReport.AddItem(FCConvert.ToString(lngAccount) + "\t" + "Transferred Ownership" + "\t" + FCConvert.ToString(lngAccount) + "\t" + Strings.Format(clsPending.Get_Fields_DateTime("mindate"), "MM/dd/yyyy") + "\t" + "\t" + "Transfers");
						modGlobalFunctions.AddCYAEntry_26("RE", "Processed Pending", "Processed Transfer for " + FCConvert.ToString(lngAccount));
						//FC:FINAL:RPU:#i1472 - Set executeTransferWhole true
						executeTransferWhole = true;
					}
					else if (clsPending.Get_Fields_Int32("transferwhat") == 1)
					{
						// transferring to a new account.  Most likely selling one card.  This creates the account
						// skips to endofloop if it can't create the account
						lngAccount = FCConvert.ToInt32(modGlobalRoutines.GetMaxAccounts()) + 1;
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						lngMastAccount = FCConvert.ToInt32(clsPending.Get_Fields("account"));
						modGlobalFunctions.AddCYAEntry_80("RE", "Processed Pending", "Processed Transfer for " + FCConvert.ToString(lngMastAccount), "To New Account " + FCConvert.ToString(lngAccount));
						GridReport.AddItem(FCConvert.ToString(lngMastAccount) + "\t" + "Transferred Account to new Account" + "\t" + FCConvert.ToString(lngAccount) + "\t" + Strings.Format(clsPending.Get_Fields_DateTime("mindate"), "MM/dd/yyyy") + "\t" + "\t" + "Transfers");
						if (FCConvert.ToInt32(clsPending.Get_Fields_Int32("transferTYPE")) == 0)
						{
							// entire account
							// just change the account number then treat it as a regular whole account transfer
							if (modGlobalRoutines.AccountExists(ref lngMastAccount))
							{
								clsTemp.Execute("update master set rsaccount = " + FCConvert.ToString(lngAccount) + " where rsaccount = " + FCConvert.ToString(lngMastAccount), modGlobalVariables.strREDatabase);
								executeTransferWhole = true;
								goto TransferWhole;
							}
							else
							{
								GridReport.TextMatrix(GridReport.Rows - 1, 1, "Account " + FCConvert.ToString(lngMastAccount) + " not found.");
								modGlobalFunctions.AddCYAEntry_242("RE", "Failed Pending", "Transfer for " + FCConvert.ToString(lngMastAccount), "To " + FCConvert.ToString(lngAccount), "Source Account Not Found");
							}
						}
						else
						{
							// specific cards
							if (modGlobalRoutines.AccountExists(ref lngMastAccount))
							{
								GridReport.TextMatrix(GridReport.Rows - 1, 1, "Transferred cards to new account");
								// reserved the account number so someone can be adding accounts on another computer and this won't be a problem
								strList = FCConvert.ToString(clsPending.Get_Fields_String("cards"));
								if (strList != string.Empty)
								{
									Cardarray = Strings.Split(strList, ",", -1, CompareConstants.vbTextCompare);
									if (Information.UBound(Cardarray, 1) >= 0)
									{
										for (x = 0; x <= Information.UBound(Cardarray, 1); x++)
										{
											// each element in the array is an id to a card
											clsTemp.OpenRecordset("select Rscard from master where id = " + Cardarray[x], modGlobalVariables.strREDatabase);
											if (!clsTemp.EndOfFile())
											{
												intCard = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("rscard"));
                                                var cardGuid = clsTemp.Get_Fields_String("CardId");
												clsTemp.Execute("UPdate Master SET RSaccount = " + FCConvert.ToString(lngAccount) + " where RSAccount = " + FCConvert.ToString(lngMastAccount) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
												clsTemp.Execute("UPdate Commercial SET RSaccount = " + FCConvert.ToString(lngAccount) + " where RSAccount = " + FCConvert.ToString(lngMastAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
												clsTemp.Execute("UPdate Dwelling SET RSaccount = " + FCConvert.ToString(lngAccount) + " where RSAccount = " + FCConvert.ToString(lngMastAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
												clsTemp.Execute("UPdate OutBuilding SET RSaccount = " + FCConvert.ToString(lngAccount) + " where RSAccount = " + FCConvert.ToString(lngMastAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
												clsTemp.Execute("UPdate IncomevALUE SET account = " + FCConvert.ToString(lngAccount) + " where Account = " + FCConvert.ToString(lngMastAccount) + " and Card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
												clsTemp.Execute("UPdate IncomeApproach SET account = " + FCConvert.ToString(lngAccount) + " where Account = " + FCConvert.ToString(lngMastAccount) + " and Card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
												clsTemp.Execute("UPdate IncomeExpense SET account = " + FCConvert.ToString(lngAccount) + " where Account = " + FCConvert.ToString(lngMastAccount) + " and Card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
												clsTemp.Execute("UPdate PictureRecord SET mRaccountnumber = " + FCConvert.ToString(lngAccount) + " where MRAccountNumber = " + FCConvert.ToString(lngMastAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
                                                clsTemp.Execute(
                                                    "Update Documents set ReferenceId = " + lngAccount +
                                                    " where ReferenceId = " + lngMastAccount +
                                                    " and ReferenceGroup = 'RealEstate'", "CentralDocuments");
                                            }
											else
											{
												GridReport.TextMatrix(GridReport.Rows - 1, 1, GridReport.TextMatrix(GridReport.Rows - 1, 1) + "\r\n" + " Record number " + Cardarray[x] + " was missing.");
											}
										}
										// x
									}
								}
								FCUtils.EraseSafe(Cardarray);
								// now fix the card numbers
								modGlobalRoutines.RenumberCards(ref lngMastAccount);
								modGlobalRoutines.RenumberCards(ref lngAccount);
								// it is renumbered and the account is changed so treat as a normal transfer now
								executeTransferWhole = true;
								goto TransferWhole;
							}
							else
							{
								// account doesn't exist
								GridReport.TextMatrix(GridReport.Rows - 1, 1, "Account " + FCConvert.ToString(lngMastAccount) + " not found.");
							}
						}
						clsTemp.Execute("update master set datecreated = '" + Strings.Format(DateTime.Today, "MM/dd/yyyy") + "' where rsaccount = " + FCConvert.ToString(lngAccount), modGlobalVariables.strREDatabase);
					}
					else if (clsPending.Get_Fields_Int32("transferwhat") == 2)
					{
						// transferring one card up to all cards to an existing account.
						// not likely to happen this way unless the card is a mobile home or
						// maybe the properties are adjoining already
						lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsPending.Get_Fields_Int32("toaccount"))));
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						lngMastAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsPending.Get_Fields("account"))));
						GridReport.AddItem(FCConvert.ToString(lngMastAccount) + "\t" + "Transferred account to existing account" + "\t" + FCConvert.ToString(lngAccount) + "\t" + Strings.Format(clsPending.Get_Fields_DateTime("mindate"), "MM/dd/yyyy") + "\t" + "\t" + "Transfers");
						modGlobalFunctions.AddCYAEntry_80("RE", "Processed Pending", "Processed Transfer for " + FCConvert.ToString(lngMastAccount), "To Existing Account " + FCConvert.ToString(lngAccount));
						if (modGlobalRoutines.AccountExists(ref lngAccount))
						{
							if (modGlobalRoutines.AccountExists(ref lngMastAccount))
							{
								if (FCConvert.ToInt32(clsPending.Get_Fields_Int32("transferTYPE")) == 0)
								{
									// whole account
									clsTemp.OpenRecordset("select id,rsaccount,rscard from master where rsaccount = " + FCConvert.ToString(lngMastAccount) + " order by rscard", modGlobalVariables.strREDatabase);
								}
								else
								{
									// only certain cards
									GridReport.TextMatrix(GridReport.Rows - 1, 1, "Transferred cards to existing account.");
									strList = "(" + clsPending.Get_Fields_String("cards") + ")";
									clsTemp.OpenRecordset("select id,rsaccount,rscard from master where id in " + strList + " order by rscard", modGlobalVariables.strREDatabase);
								}
								if (!clsTemp.EndOfFile())
								{
									intNewCard = 30000;
									// some ridiculously high card number no one should reach
									while (!clsTemp.EndOfFile())
									{
										intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("rscard"))));
										clsExecute.Execute("UPdate Master SET RSaccount = " + FCConvert.ToString(lngAccount) + ",rscard = " + FCConvert.ToString(intNewCard) + " where RSAccount = " + FCConvert.ToString(lngMastAccount) + " and rscard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
										clsExecute.Execute("UPdate Commercial SET RSaccount = " + FCConvert.ToString(lngAccount) + ",rscard = " + FCConvert.ToString(intNewCard) + " where RSAccount = " + FCConvert.ToString(lngMastAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
										clsExecute.Execute("UPdate Dwelling SET RSaccount = " + FCConvert.ToString(lngAccount) + ",rscard = " + FCConvert.ToString(intNewCard) + " where RSAccount = " + FCConvert.ToString(lngMastAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
										clsExecute.Execute("UPdate OutBuilding SET RSaccount = " + FCConvert.ToString(lngAccount) + ",rscard = " + FCConvert.ToString(intNewCard) + " where RSAccount = " + FCConvert.ToString(lngMastAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
										clsExecute.Execute("UPdate IncomevALUE SET account = " + FCConvert.ToString(lngAccount) + ",card = " + FCConvert.ToString(intNewCard) + " where Account = " + FCConvert.ToString(lngMastAccount) + " and Card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
										clsExecute.Execute("UPdate IncomeApproach SET account = " + FCConvert.ToString(lngAccount) + ",card = " + FCConvert.ToString(intNewCard) + " where Account = " + FCConvert.ToString(lngMastAccount) + " and Card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
										clsExecute.Execute("UPdate IncomeExpense SET account = " + FCConvert.ToString(lngAccount) + ",card = " + FCConvert.ToString(intNewCard) + " where Account = " + FCConvert.ToString(lngMastAccount) + " and Card = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
										clsExecute.Execute("UPdate PictureRecord SET mraccountnumber = " + FCConvert.ToString(lngAccount) + ",RSCARD = " + FCConvert.ToString(intNewCard) + " where MRAccountNumber = " + FCConvert.ToString(lngMastAccount) + " and RSCard = " + FCConvert.ToString(intCard), modGlobalVariables.strREDatabase);
                                        clsTemp.Execute(
                                            "Update Documents set ReferenceId = " + lngAccount +
                                            " where ReferenceId = " + lngMastAccount +
                                            " and ReferenceGroup = 'RealEstate'", "CentralDocuments");

                                        intNewCard += 1;
										clsTemp.MoveNext();
									}
									modGlobalRoutines.RenumberCards(ref lngMastAccount);
									modGlobalRoutines.RenumberCards(ref lngAccount);
								}
								else
								{
									// record this in the grid for the report
									GridReport.TextMatrix(GridReport.Rows - 1, 1, "Specifed cards not found.");
								}
							}
							else
							{
								// record this in the grid for the report
								GridReport.TextMatrix(GridReport.Rows - 1, 1, "Source account " + FCConvert.ToString(lngMastAccount) + " not found.");
								modGlobalFunctions.AddCYAEntry_80("RE", "Failed Pending", "Transfer for " + FCConvert.ToString(lngMastAccount), "Source Account Doesn't Exist");
							}
						}
						else
						{
							// record this in the grid for the report
							GridReport.TextMatrix(GridReport.Rows - 1, 1, "Destination account " + FCConvert.ToString(lngAccount) + " not found.");
							modGlobalFunctions.AddCYAEntry_242("RE", "Failed Pending", "Transfer for " + FCConvert.ToString(lngMastAccount), "To " + FCConvert.ToString(lngAccount), "Dest. Account Doesn't Exist");
						}
					}
					TransferWhole:
					;
					if (executeTransferWhole)
					{
						#region TransferWhole
						// now put info in previousowner table
						clsExecute.OpenRecordset(strMasterJoin + " where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = 1", modGlobalVariables.strREDatabase);
						if (!clsExecute.EndOfFile())
						{
							strSQL = "Insert into previousowner (account,name,secowner,address1,address2,city,state,zip,zip4,saledate,partyid1,partyid2) values (";
							strSQL += FCConvert.ToString(lngAccount);
							strTemp = FCConvert.ToString(clsExecute.Get_Fields_String("DeedName1"));
							modGlobalRoutines.escapequote(ref strTemp);
							strSQL += ",'" + strTemp + "'";
							strTemp = FCConvert.ToString(clsExecute.Get_Fields_String("Deedname2"));
							modGlobalRoutines.escapequote(ref strTemp);
							strSQL += ",'" + strTemp + "'";
							strTemp = FCConvert.ToString(clsExecute.Get_Fields_String("rsaddr1"));
							strTemp = modGlobalRoutines.escapequote(ref strTemp);
							strSQL += ",'" + strTemp + "'";
							strTemp = FCConvert.ToString(clsExecute.Get_Fields_String("rsaddr2"));
							strTemp = modGlobalRoutines.escapequote(ref strTemp);
							strSQL += ",'" + strTemp + "'";
							strSQL += ",'" + clsExecute.Get_Fields_String("rsaddr3") + "'";
							strSQL += ",'" + clsExecute.Get_Fields_String("rsstate") + "'";
							strSQL += ",'" + clsExecute.Get_Fields_String("rszip") + "'";
							strSQL += ",'" + clsExecute.Get_Fields_String("rszip4") + "'";
							dtSaleDate = DateTime.FromOADate(0);
							if (Information.IsDate(clsPending.Get_Fields("saledate")))
							{
								if (clsPending.Get_Fields_DateTime("saledate").ToOADate() != 0)
								{
									dtSaleDate = (DateTime)clsPending.Get_Fields_DateTime("saledate");
								}
							}
							if (dtSaleDate.ToOADate() == 0)
							{
								strSQL += ",0";
							}
							else
							{
								strSQL += ",'" + Strings.Format(dtSaleDate, "MM/dd/yyyy") + "'";
							}
							strSQL += "," + clsExecute.Get_Fields_Int32("ownerpartyid");
							strSQL += "," + clsExecute.Get_Fields_Int32("secownerpartyid");
							strSQL += ")";
							clsExecute.Execute(strSQL, modGlobalVariables.strREDatabase);
						}
						strSQL = "Update master set ";
						strSQL += "RSPREvIOUSmaster = '" + clsExecute.Get_Fields_String("deedname1") + "',ownerpartyid = " + clsPending.Get_Fields_Int32("OwnerPartyID") + ",secownerpartyid = " + clsPending.Get_Fields_Int32("secownerpartyid") + "";
						strSQL += ",deedname1 = '" + clsPending.Get_Fields_String("Deedname1").EscapeSingleQuotes() +
								  "', deedname2 = '" + clsPending.Get_Fields_String("deedname2").EscapeSingleQuotes() + "'";
						strSQL += ",rsmaplot = '" + clsPending.Get_Fields_String("maplot") + "'";
                        strTemp = clsPending.Get_Fields_String("ref1").EscapeSingleQuotes();
						strSQL += ",rsref1 = '" + strTemp + "'";
                        strSQL += ",rsref2 = '" + clsPending.Get_Fields_String("ref2").EscapeSingleQuotes() + "',rslocnumalph = '" + clsPending.Get_Fields("streetnumber") + "'";
						strSQL += ",rslocapt = '" + clsPending.Get_Fields_String("apt") + "', rslocstreet = '" + clsPending.Get_Fields_String("streetname") + "'";
						if (dtSaleDate.ToOADate() != 0)
						{
							GridReport.TextMatrix(GridReport.Rows - 1, 4, FCConvert.ToString(dtSaleDate));
							dtSaleDate = (DateTime)clsPending.Get_Fields_DateTime("saledate");
							strSQL += ",saledate = '" + clsPending.Get_Fields_DateTime("saledate") + "',pisaleprice = " + clsPending.Get_Fields_Int32("saleprice");
							strSQL += ",pisaletype = " + clsPending.Get_Fields_Int32("saletype") + ",piSALEfinancing = " + clsPending.Get_Fields_Int32("salefinancing");
							strSQL += ",pisaleverified = " + clsPending.Get_Fields_Int32("saleverified") + ",pisalevalidity = " + clsPending.Get_Fields_Int32("salevalidity");
							if (Conversion.Val(clsPending.Get_Fields_Int32("salevalidity")) == 1)
							{
								// save only if it is a valid sale
								boolSaveSales = true;
							}
						}
						// End If
						strSQL += " where rsaccount = " + FCConvert.ToString(lngAccount);
						// update master
						clsPending.Execute(strSQL, modGlobalVariables.strREDatabase);
						boolDontAdd = false;
						// update book and page
						// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
						if (FCConvert.ToString(clsPending.Get_Fields("book")) != "")
						{
							clsTemp.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAccount) + " order by line", modGlobalVariables.strREDatabase);
							intLine = 0;
							while (!clsTemp.EndOfFile())
							{
								// TODO Get_Fields: Check the table for the column [line] and replace with corresponding Get_Field method
								intLine = FCConvert.ToInt32(clsTemp.Get_Fields("line"));
								// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
								if (clsTemp.Get_Fields("book") == clsPending.Get_Fields("book") && clsTemp.Get_Fields("page") == clsPending.Get_Fields("page"))
								{
									clsTemp.Edit();
									clsTemp.Set_Fields("saledate", Strings.Format(dtSaleDate, "MM/dd/yyyy"));
									if (Information.IsDate(clsPending.Get_Fields("bpdate")))
									{
										clsTemp.Set_Fields("bpdate", clsPending.Get_Fields_DateTime("bpdate"));
									}
									boolDontAdd = true;
									clsTemp.Update();
								}
								else
								{
									clsTemp.Edit();
									clsTemp.Set_Fields("current", false);
									clsTemp.Update();
								}
								clsTemp.MoveNext();
							}
							if (!boolDontAdd)
							{
								intLine += 1;
								strSQL = "Insert into BookPage (book,page,bpdate,account,card,saledate,line,[current]) values (";
								// TODO Get_Fields: Check the table for the column [book] and replace with corresponding Get_Field method
								strSQL += "'" + clsPending.Get_Fields("book") + "'";
								// TODO Get_Fields: Check the table for the column [page] and replace with corresponding Get_Field method
								strSQL += ",'" + clsPending.Get_Fields("page") + "'";
								strSQL += ",'" + clsPending.Get_Fields_String("bpdate") + "'";
								strSQL += "," + FCConvert.ToString(lngAccount);
								strSQL += ",1";
								strSQL += ",'" + FCConvert.ToString(dtSaleDate) + "'";
								strSQL += "," + FCConvert.ToString(intLine);
								strSQL += ",1";
								strSQL += ")";
								clsTemp.Execute(strSQL, modGlobalVariables.strREDatabase);
							}
						}
						#endregion
					}
					clsPending.Delete();
					// DON'T leave it in the table to be done again
					clsPending.Update();
					if (boolSaveSales)
					{
						clsTemp.OpenRecordset("select * from srmaster where rscard = 1 and rsaccount = " + FCConvert.ToString(lngAccount) + " and saledate = '" + FCConvert.ToString(dtSaleDate) + "'", modGlobalVariables.strREDatabase);
						if (!clsTemp.EndOfFile())
						{
							frmWait.InstancePtr.Unload();
							if (MessageBox.Show("There is already a sales record for account " + FCConvert.ToString(lngAccount) + " on " + FCConvert.ToString(dtSaleDate) + "." + "\r\n" + "Do you want to Overwrite it?" + "\r\n" + "Answer NO to add a new record", "Overwrite or Add New?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								boolSaveSales = false;
								if (modGlobalRoutines.OverwriteSalesRecord_6(lngAccount, clsTemp.Get_Fields_Int32("saleid")))
								{
									GridReport.AddItem(FCConvert.ToString(lngAccount) + "\t" + "Overwrote a sale record for account " + FCConvert.ToString(lngAccount) + "\t" + "\t" + "\t" + FCConvert.ToString(dtSaleDate));
								}
								else
								{
									GridReport.AddItem(FCConvert.ToString(lngAccount) + "\t" + "Could not overwrite the sale record for account " + FCConvert.ToString(lngAccount) + "\t" + "\t" + "\t" + FCConvert.ToString(dtSaleDate));
								}
							}
							else
							{
								// do nothing, then next if boolsavesales will take care of it
							}
						}
					}
					frmWait.InstancePtr.Init("Please Wait.", false, 100, true);
					if (boolSaveSales)
					{
						// this must be a transfer to the same account or to a new one
						// cannot save a sales record for an attached card
						if (dtSaleDate.ToOADate() != 0)
						{
							// had a legit sale date
							// If CreateSalesRecord(lngAccount) Then
							if (modGlobalRoutines.CreateSalesRecord(ref lngAccount))
							{
								GridReport.AddItem(FCConvert.ToString(lngAccount) + "\t" + "Created a sale record for account " + FCConvert.ToString(lngAccount) + "\t" + "\t" + "\t" + FCConvert.ToString(dtSaleDate));
							}
							else
							{
								GridReport.AddItem(FCConvert.ToString(lngAccount) + "\t" + "Could not create sale record for account " + FCConvert.ToString(lngAccount) + "\t" + "\t" + "\t" + FCConvert.ToString(dtSaleDate));
							}
						}
					}
					EndofLoop:
					;
					clsPending.MoveNext();
				}
				// Call clsExecute.Execute("insert into cyatable (user,actiondate,action,misc) values ('" & clsSecurityClass.Get_UserName & "',#" & Now & "#,'Processed Pending','Processed pending activity ')", strredatabase)
				modGlobalFunctions.AddCYAEntry_80("RE", "Processed Pending", "Processed pending activity", "Successfully Completed");
				frmWait.InstancePtr.Unload();
				// rptProcessedPendingActivity.Show vbModal, MDIParent
				intModal = FCConvert.ToInt32(FCForm.FormShowEnum.Modal);
				frmReportViewer.InstancePtr.Init(rptProcessedPendingActivity.InstancePtr, "", FCConvert.ToInt16(intModal), this.Modal);
				//Application.DoEvents();
				//FC:FINAL:RPU: #i1472 - Don't do this here
				//this.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				if (Information.Err(ex).Number == 13)
				{
					Information.Err(ex).Clear();
					this.Unload();
				}
				else
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Process Pending", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}
	}
}
