﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptListingNameLocMapAsses.
	/// </summary>
	public partial class rptListingNameLocMapAsses : BaseSectionReport
	{
		public static rptListingNameLocMapAsses InstancePtr
		{
			get
			{
				return (rptListingNameLocMapAsses)Sys.GetInstance(typeof(rptListingNameLocMapAsses));
			}
		}

		protected rptListingNameLocMapAsses _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
            {
				rsTemp?.Dispose();
                rsTemp = null;
            }
			base.Dispose(disposing);
		}

		public rptListingNameLocMapAsses()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Name Location Map/Lot Assessment";
		}
		// nObj = 1
		//   0	rptListingNameLocMapAsses	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsTemp = new clsDRWrapper();
		// Dim dbTemp As DAO.Database
		object varAccount;
		int intPage;
		bool boolByCard;
		string strland = "";
		string strbldg = "";
		double dblTotalLand;
		double dblTotalBldg;
		double dblTotalExempt;
		double dblTotalTotal;
		int lngCount;
		string strExempt = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string strTemp = "";
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                string strSQL = "";
                nextaccount: ;
                if (rsTemp.EndOfFile())
                    return;
                txtAccount.Text = Strings.Format(rsTemp.Get_Fields_Int32("RSAccount"), "00000");
                txtCard.Text = Strings.Format(rsTemp.Get_Fields_Int32("RSCard"), "000");
                txtName.Text = Strings.Trim(rsTemp.Get_Fields_String("RSName") + " ");
                txtSecondOwner.Text = Strings.Trim(rsTemp.Get_Fields_String("rssecowner") + "");
                if (Information.IsNumeric(rsTemp.Get_Fields_String("rslocnumalph") + ""))
                {
                    strTemp = Conversion.Str(
                        FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_String("rslocnumalph") + "")));
                    if (Conversion.Val(rsTemp.Get_Fields_String("rslocnumalph") + "") == 0)
                        strTemp = " ";
                }
                else
                {
                    strTemp = rsTemp.Get_Fields_String("rslocnumalph") + "";
                }

                strTemp += " " + rsTemp.Get_Fields_String("rslocapt") + "";
                txtLocation.Text = Strings.Trim(strTemp + " " + rsTemp.Get_Fields_String("RSLOCStreet") + " ");
                txtMap.Text = Strings.Trim(rsTemp.Get_Fields_String("RSMAPLOT") + " ");
                if (boolByCard)
                {
                    txtExemption.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("RLEXEMPTION") + ""),
                        "##,###,##0");
                    if (modGlobalVariables.Statics.boolBillingorCorrelated)
                    {
                        txtLand.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("lastlandval") + ""),
                            "##,###,##0");
                        txtBuilding.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("lastbldgval") + ""),
                            "###,###,##0");
                        txtTotal.Text = Strings.Format(
                            Conversion.Val(rsTemp.Get_Fields_Int32("lastlandval") + "") +
                            Conversion.Val(rsTemp.Get_Fields_Int32("lastbldgval") + "") -
                            Conversion.Val(rsTemp.Get_Fields_Int32("RLEXEMPTION") + ""), "#,###,###,##0");
                        dblTotalLand += Conversion.Val(rsTemp.Get_Fields_Int32("lastlandval"));
                        dblTotalBldg += Conversion.Val(rsTemp.Get_Fields_Int32("lastbldgval"));
                        dblTotalExempt += Conversion.Val(rsTemp.Get_Fields_Int32("rlexemption"));
                    }
                    else
                    {
                        txtLand.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("RLLANDVAL") + ""),
                            "##,###,##0");
                        txtBuilding.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("RLBLDGVAL") + ""),
                            "###,###,##0");
                        txtTotal.Text = Strings.Format(
                            Conversion.Val(rsTemp.Get_Fields_Int32("RLLANDVAL") + "") +
                            Conversion.Val(rsTemp.Get_Fields_Int32("RLBLDGVAL") + "") -
                            Conversion.Val(rsTemp.Get_Fields_Int32("corrEXEMPTION") + ""), "###,###,##0");
                        dblTotalLand += Conversion.Val(rsTemp.Get_Fields_Int32("rllandval"));
                        dblTotalBldg += Conversion.Val(rsTemp.Get_Fields_Int32("rlbldgval"));
                        dblTotalExempt += Conversion.Val(rsTemp.Get_Fields_Int32("rlexemption"));
                    }
                }
                else
                {
                    strSQL = "select sum(" + strExempt + ") as exemptsum,sum(" + strland + ") as landsum, sum(" +
                             strbldg + ") as bldgsum from master where rsaccount = " +
                             rsTemp.Get_Fields_Int32("rsaccount") + " and not rsdeleted = 1";
                    clsTemp.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
                    txtLand.Text = Strings.Format(Conversion.Val(clsTemp.GetData("landsum") + ""), "##,###,##0");
                    txtBuilding.Text = Strings.Format(Conversion.Val(clsTemp.GetData("bldgsum")), "###,###,##0");
                    txtExemption.Text = Strings.Format(Conversion.Val(clsTemp.GetData("exemptsum")), "###,###,##0");
                    txtTotal.Text =
                        Strings.Format(
                            Conversion.Val(clsTemp.GetData("landsum")) - Conversion.Val(clsTemp.GetData("exemptsum")) +
                            Conversion.Val(clsTemp.GetData("bldgsum")), "#,###,###,##0");
                    // TODO Get_Fields: Field [landsum] not found!! (maybe it is an alias?)
                    dblTotalLand += Conversion.Val(clsTemp.Get_Fields("landsum") + "");
                    // TODO Get_Fields: Field [bldgsum] not found!! (maybe it is an alias?)
                    dblTotalBldg += Conversion.Val(clsTemp.Get_Fields("bldgsum"));
                    // TODO Get_Fields: Field [exemptsum] not found!! (maybe it is an alias?)
                    dblTotalExempt += Conversion.Val(clsTemp.Get_Fields("exemptsum"));
                }

                lngCount += 1;
                if (Strings.Trim(txtLocation.Text) == string.Empty)
                {
                    txtLocation.Text = txtMap.Text;
                    txtMap.Text = "";
                }

                if (Strings.Trim(txtSecondOwner.Text) == string.Empty)
                {
                    txtSecondOwner.Text = txtLocation.Text;
                    txtLocation.Text = txtMap.Text;
                    txtMap.Text = "";
                }

                // varAccount = rsTemp.Fields(gstrFieldName)
                if (!rsTemp.EndOfFile())
                    rsTemp.MoveNext();
                eArgs.EOF = false;
            }
        }

		private void ActiveReport_Initialize()
		{
			if (rptListingNameLocMapAsses.InstancePtr.Document.Printer != null)
			{
				// rptListingNameLocMapAsses.Printer.PrintQuality = ddPQMedium
			}
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			if (rptListingNameLocMapAsses.InstancePtr.Document.Printer != null)
			{
				//rptListingNameLocMapAsses.InstancePtr.Document.Printer.PrintQuality = ddPQDraft;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtCaption.Text = "Real Estate";
			if (Strings.Trim(this.Document.Printer.PrinterName) != string.Empty)
			{
				//this.Document.Printer.RenderMode = 1;
			}
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			dblTotalLand = 0;
			dblTotalBldg = 0;
			dblTotalExempt = 0;
			dblTotalTotal = 0;
			lngCount = 0;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			intPage += 1;
			txtPage.Text = "Page " + FCConvert.ToString(intPage);
		}

		public void Start(bool boolByExtract, bool boolByAccount)
		{
			string strSQL = "";
			int lngUID;
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			boolByCard = !boolByAccount;
			if (boolByCard)
				lblCards.Visible = true;
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = rsTemp.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
			{
				txtCaption2.Text = "Account List by Account";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
			{
				txtCaption2.Text = "Account List by Location";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSMAPLOT")
			{
				txtCaption2.Text = "Account List by Map/Lot";
			}
			else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSNAME")
			{
				txtCaption2.Text = "Account List by Name";
			}
			if (modGlobalVariables.Statics.boolRange)
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
				{
					txtCaption2.Text = txtCaption2.Text + " (" + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " - " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + ")";
				}
				else
				{
					txtCaption2.Text = txtCaption2.Text + " (" + modPrintRoutines.Statics.gstrMinAccountRange + " - " + modPrintRoutines.Statics.gstrMaxAccountRange + ")";
				}
			}
			int lngID;
			lngID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			if (boolByExtract)
			{
				if (boolByAccount)
				{
					strSQL = strMasterJoin + " where rsaccount in (select extracttable.accountnumber from extracttable inner join labelaccounts on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) where extracttable.userid = " + FCConvert.ToString(lngUID) + ") AND rsdeleted = 0 ";
				}
				else
				{
					// strSQL = "select  master.* from (master inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (master.rsaccount = extracttable.accountnumber) and (master.rscard = extracttable.cardnumber)) where extracttable.userid = " & lngUID & " and master.rsdeleted = 0"
					strSQL = "select  mj.* from (" + strMasterJoinQuery + " inner join (extracttable inner join(labelaccounts) on (extracttable.groupnumber = labelaccounts.accountnumber) and (extracttable.userid = labelaccounts.userid) ) on (mj.rsaccount = extracttable.accountnumber) and (mj.rscard = extracttable.cardnumber)) where extracttable.userid = " + FCConvert.ToString(lngUID) + " and mj.rsdeleted = 0";
				}
				rsTemp.OpenRecordset("select TITLE FROM EXTRACT WHERe reportnumber = 0 and userid = " + FCConvert.ToString(lngUID), modGlobalVariables.strREDatabase);
				if (!rsTemp.EndOfFile())
				{
					if (FCConvert.ToString(rsTemp.Get_Fields_String("title")) != string.Empty)
					{
						txtCaption2.Text = rsTemp.Get_Fields_String("title");
					}
				}
			}
			else
			{
				strSQL = strMasterJoin + " where rsdeleted = 0";
			}
			if (boolByAccount)
			{
				strSQL += " and rscard = 1";
			}
			if (modGlobalVariables.Statics.boolRange)
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSACCOUNT")
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " + modPrintRoutines.Statics.gstrFieldName + " <= " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + " order by " + modPrintRoutines.Statics.gstrFieldName + ",RSCard", modGlobalVariables.strREDatabase);
				}
				else if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' order by " + modPrintRoutines.Statics.gstrFieldName + ",CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float)  ", modGlobalVariables.strREDatabase);
				}
				else
				{
					rsTemp.OpenRecordset(strSQL + " and " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,RSCard", modGlobalVariables.strREDatabase);
				}
			}
			else
			{
				if (Strings.UCase(modPrintRoutines.Statics.gstrFieldName) == "RSLOCSTREET")
				{
					rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",CAST(LEFT(isnull(rslocnumalph,''), Patindex('%[^0-9]%', rslocnumalph + 'x') - 1) AS Float)  ", modGlobalVariables.strREDatabase);
				}
				else
				{
					if (Strings.LCase(modPrintRoutines.Statics.gstrFieldName) != "rsaccount")
					{
						rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,rscard", modGlobalVariables.strREDatabase);
					}
					else
					{
						rsTemp.OpenRecordset(strSQL + " order by " + modPrintRoutines.Statics.gstrFieldName + ",rscard", modGlobalVariables.strREDatabase);
					}
				}
			}
			if (modGlobalVariables.Statics.boolBillingorCorrelated)
			{
				strland = "lastlandval";
				strbldg = "lastbldgval";
				strExempt = "rlexemption";
			}
			else
			{
				strland = "rllandval";
				strbldg = "rlbldgval";
				strExempt = "correxemption";
			}
			// Me.Show
			frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "NameLocMapAssess");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalLand.Text = Strings.Format(dblTotalLand, "#,###,###,##0");
			txtTotalBuilding.Text = Strings.Format(dblTotalBldg, "#,###,###,##0");
			txtTotalExemption.Text = Strings.Format(dblTotalExempt, "#,###,###,##0");
			dblTotalTotal = dblTotalLand + dblTotalBldg - dblTotalExempt;
			txtTotalTotal.Text = Strings.Format(dblTotalTotal, "#,###,###,##0");
			txtCount.Text = Strings.Format(lngCount, "#,###,###,##0");
		}

		
	}
}
