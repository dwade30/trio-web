﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptIncomeApproach.
	/// </summary>
	public partial class srptIncomeApproach : FCSectionReport
	{
		public static srptIncomeApproach InstancePtr
		{
			get
			{
				return (srptIncomeApproach)Sys.GetInstance(typeof(srptIncomeApproach));
			}
		}

		protected srptIncomeApproach _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
                clsLoad = null;
            }
			base.Dispose(disposing);
		}

		public srptIncomeApproach()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptIncomeApproach	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		int lngTotSQFT;
		// vbPorter upgrade warning: curPotGross As Decimal	OnWrite(short, Decimal)
		Decimal curPotGross;
		bool boolLoadFromScreen;
		int lngRow;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolLoadFromScreen)
			{
				eArgs.EOF = clsLoad.EndOfFile();
			}
			else
			{
				//FC:FINAL:RPU: #i1029 - See first if the form is loaded
				if (!frmREProperty.InstancePtr.IsLoaded)
				{
					frmREProperty.InstancePtr.LoadForm();
				}
				if (lngRow >= frmREProperty.InstancePtr.Grid1.Rows)
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			string[] strAry = null;
			strAry = Strings.Split(FCConvert.ToString(this.UserData), ",", -1, CompareConstants.vbTextCompare);
			boolLoadFromScreen = FCConvert.CBool(strAry[2]);
			lngRow = 2;
			if (!boolLoadFromScreen)
			{
				lngTotSQFT = 0;
				curPotGross = 0;
				strSQL = "Select * from INCOMEAPPROACH where account = " + strAry[0] + " and card = " + strAry[1] + " order by id";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
				if (clsLoad.EndOfFile())
				{
					this.Close();
					return;
				}
			}
			else
			{
				txtTotalSQFT.Text = frmNewREProperty.InstancePtr.txtTotalSF.Text;
				txtPotGrossIncome.Text = frmNewREProperty.InstancePtr.GridValues.TextMatrix(0, 2);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngSQFT = 0;
			double dblMarket = 0;
			double dblPot = 0;
            using (clsDRWrapper clsDesc = new clsDRWrapper())
            {
                if (!boolLoadFromScreen)
                {
                    if (!clsLoad.EndOfFile())
                    {
                        lngSQFT = FCConvert.ToInt32(
                            Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("squarefootage"))));
                        txtSquareFootage.Text = Strings.Format(lngSQFT, "#,###,##");
                        lngTotSQFT += lngSQFT;
                        txtOCC.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("occupancycode")));
                        clsDesc.OpenRecordset(
                            "select * from occupancycodes where code = " +
                            FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("occupancycode"))),
                            modGlobalVariables.Statics.strRECostFileDatabase);
                        if (!clsDesc.EndOfFile())
                        {
                            txtDescription.Text = clsDesc.Get_Fields_String("description");
                        }
                        else
                        {
                            txtDescription.Text = "";
                        }

                        txtGN.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("grossornet")));
                        txtActualRent.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Int32("actualrent")),
                            "#,###,##0");
                        txtActualYear.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("actualsfyear")),
                            "#,###,##0.00");
                        dblMarket = Conversion.Val(clsLoad.Get_Fields_Double("marketsfyear"));
                        txtMarketYear.Text = Strings.Format(dblMarket, "#,###,##0.00");
                        dblPot = dblMarket * lngSQFT;
                        txtMarketRent.Text = Strings.Format(dblPot, "#,###,###,##0");
                        curPotGross += FCConvert.ToDecimal(dblPot);
                        clsLoad.MoveNext();
                    }
                }
                else
                {
                    if (lngRow < frmNewREProperty.InstancePtr.Grid1.Rows)
                    {
                        txtOCC.Text = frmNewREProperty.InstancePtr.Grid1.TextMatrix(lngRow, 0);
                        clsDesc.OpenRecordset(
                            "select * from occupancycodes where code = " +
                            FCConvert.ToString(
                                Conversion.Val(frmNewREProperty.InstancePtr.Grid1.TextMatrix(lngRow, 0))),
                            modGlobalVariables.Statics.strRECostFileDatabase);
                        if (!clsDesc.EndOfFile())
                        {
                            txtDescription.Text = clsDesc.Get_Fields_String("description");
                        }
                        else
                        {
                            txtDescription.Text = "";
                        }

                        txtSquareFootage.Text = frmNewREProperty.InstancePtr.Grid1.TextMatrix(lngRow, 1);
                        txtGN.Text = frmNewREProperty.InstancePtr.Grid1.TextMatrix(lngRow, 2);
                        txtActualRent.Text = frmNewREProperty.InstancePtr.Grid1.TextMatrix(lngRow, 3);
                        txtActualYear.Text = frmNewREProperty.InstancePtr.Grid1.TextMatrix(lngRow, 4);
                        txtMarketYear.Text = frmNewREProperty.InstancePtr.Grid1.TextMatrix(lngRow, 5);
                        txtMarketRent.Text = frmNewREProperty.InstancePtr.Grid1.TextMatrix(lngRow, 6);
                        lngRow += 1;
                    }
                }
            }
        }

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (!boolLoadFromScreen)
			{
				txtTotalSQFT.Text = Strings.Format(lngTotSQFT, "#,###,###,##0");
				txtPotGrossIncome.Text = Strings.Format(curPotGross, "#,###,##0");
			}
		}

		
	}
}
