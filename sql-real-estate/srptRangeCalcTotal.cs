﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptRangeCalcTotal.
	/// </summary>
	public partial class srptRangeCalcTotal : FCSectionReport
	{
		public static srptRangeCalcTotal InstancePtr
		{
			get
			{
				return (srptRangeCalcTotal)Sys.GetInstance(typeof(srptRangeCalcTotal));
			}
		}

		protected srptRangeCalcTotal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				GridSummary?.Dispose();
                GridSummary = null;
            }
			base.Dispose(disposing);
		}

		public srptRangeCalcTotal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptRangeCalcTotal	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intNumLines;
		int intLine;
		
		double dblTLand;
		double dblTBldg;
		double dblTTot;
		double dblTCLand;
		double dblTCBldg;
		double dblTCTot;
		FCGrid GridSummary = rptRangeValuation.InstancePtr.GridSummary;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = (intLine >= intNumLines);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intNumLines = GridSummary.Rows;
			intLine = 0;
			// If boolDotMatrix Then
			// Call instReportFunctions.SetReportFontsByTag(Me, "text", Me.ParentReport.strFonttoUse)
			// Call instReportFunctions.SetReportFontsByTag(Me, "bold", Me.ParentReport.strFonttoUse, True)
			// End If
			dblTLand = 0;
			dblTBldg = 0;
			dblTTot = 0;
			dblTCLand = 0;
			dblTCBldg = 0;
			dblTCTot = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intLine <= intNumLines)
			{
				txtCard.Text = GridSummary.TextMatrix(intLine, 0);
				dblTCLand += Conversion.Val(GridSummary.TextMatrix(intLine, 1));
				dblTCBldg += Conversion.Val(GridSummary.TextMatrix(intLine, 2));
				dblTCTot = dblTCLand + dblTCBldg;
				txtCLand.Text = Strings.Format(GridSummary.TextMatrix(intLine, 1), "#,###,###,##0");
				txtCBldg.Text = Strings.Format(GridSummary.TextMatrix(intLine, 2), "#,###,###,##0");
				txtCTotal.Text = Strings.Format(GridSummary.TextMatrix(intLine, 3), "#,###,###,##0");
				dblTLand += Conversion.Val(GridSummary.TextMatrix(intLine, 4));
				dblTBldg += Conversion.Val(GridSummary.TextMatrix(intLine, 5));
				dblTTot = dblTLand + dblTBldg;
				txtLand.Text = Strings.Format(GridSummary.TextMatrix(intLine, 4), "#,###,###,##0");
				txtBldg.Text = Strings.Format(GridSummary.TextMatrix(intLine, 5), "#,###,###,##0");
				txtTotal.Text = Strings.Format(GridSummary.TextMatrix(intLine, 6), "#,###,###,##0");
			}
			intLine += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCLandT.Text = Strings.Format(dblTCLand, "#,###,###,##0");
			txtCBLDGT.Text = Strings.Format(dblTCBldg, "#,###,###,##0");
			txtCTT.Text = Strings.Format(dblTCTot, "#,###,###,##0");
			txtLandT.Text = Strings.Format(dblTLand, "#,###,###,##0");
			txtBldgT.Text = Strings.Format(dblTBldg, "#,###,###,##0");
			txtTT.Text = Strings.Format(dblTTot, "#,###,###,##0");
		}

		
	}
}
