﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCostFiles.
	/// </summary>
	public partial class frmCostFiles : BaseForm
	{
		public frmCostFiles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCostFiles InstancePtr
		{
			get
			{
				return (frmCostFiles)Sys.GetInstance(typeof(frmCostFiles));
			}
		}

		protected frmCostFiles _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intFirstRec;
		int intLastRec;
		bool boolCommCost;
		string strCostTable = "";
		bool boolLoaded;
		bool boolChanged;
		int lngLastTownCode;
		string strCaption;

		private void gridTownCode_ComboCloseUp(object sender, EventArgs e)
		{
			gridTownCode.EndEdit();
			//Application.DoEvents();
			Text1.Focus();
			//Application.DoEvents();
		}

		private void gridTownCode_Leave(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			if (lngLastTownCode == Conversion.Val(gridTownCode.TextMatrix(0, 0)))
				return;
			FillGrid1();
			lngLastTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from tblTranCode where code > 0 order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = true;
			}
			else
			{
				gridTownCode.Rows = 1;
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = false;
			}
		}

		private void ResizeGridTownCode()
		{
			//gridTownCode.Height = gridTownCode.RowHeight(0) + 60;
		}

		private void SetupGrid1()
		{
			int lngWidth;
			lngWidth = Grid1.WidthOriginal;
			Grid1.Cols = 6;
			Grid1.Rows = 1;
			// Grid1.ColWidth(0) = 500
			// Grid1.ColWidth(1) = 1440
			// Grid1.ColWidth(2) = 1440
			// Grid1.ColWidth(3) = 2160
			// Grid1.ColWidth(4) = 1080
			// Grid1.ColWidth(0) = lngWidth * 0.067
			// Grid1.ColWidth(1) = lngWidth * 0.192
			// Grid1.ColWidth(2) = lngWidth * 0.192
			// Grid1.ColWidth(3) = lngWidth * 0.288
			// Grid1.ColWidth(4) = lngWidth * 0.144
			// Grid1.ColDataType(5) = flexDTBoolean
			Grid1.TextMatrix(0, 0, "Key");
			Grid1.TextMatrix(0, 1, "Unit");
			Grid1.TextMatrix(0, 2, "Base");
			Grid1.TextMatrix(0, 3, "Long Description");
			Grid1.TextMatrix(0, 4, "Short Desc");
			Grid1.ColHidden(5, true);
			Grid1.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//FC:FINAL:CHN - i.issue #1629: Fix changing cell alignment after edit.
			Grid1.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			Grid1.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			if (Strings.UCase(this.Text) == "EXEMPTION CODES")
			{
				Grid1.TextMatrix(0, 1, "Assessment Amount");
				Grid1.TextMatrix(0, 2, "Dollar Exemption");
			}
		}

		public void Init(short intStart, short intEnd, bool boolUseCommCost, string strTitle)
		{
			try
			{
				// On Error GoTo ErrorHandler
				boolChanged = false;
				strCaption = strTitle;
				intFirstRec = intStart;
				intLastRec = intEnd;
				boolCommCost = boolUseCommCost;
				if (boolCommCost)
				{
					strCostTable = "commercialcost";
				}
				else
				{
					strCostTable = "costrecord";
				}
				this.Text = strTitle;
				//FC:FINAL:RPU:#i1374 - Show the form in App.MainForm
				//this.Show(FCForm.FormShowEnum.Modal);
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Form Cost Files Init", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillGrid1()
		{
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
			int lngTownCode = 0;
			bool boolHide = false;
			Grid1.Rows = 1;
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				lngTownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
			}
			else
			{
				lngTownCode = 0;
			}
			clsTemp.OpenRecordset("select * from " + strCostTable + " where crecordnumber between " + FCConvert.ToString(intFirstRec) + " and " + FCConvert.ToString(intLastRec) + " and townnumber  = " + FCConvert.ToString(lngTownCode) + " order by crecordnumber", modGlobalVariables.strREDatabase);
			while (!clsTemp.EndOfFile())
			{
				// If UCase(Me.Caption) = "EXEMPTION CODES" And Val(clsTemp.Fields("crecordnumber")) = 1900 Then
				// this is certified ratio
				// GRID1.AddItem (clsTemp.Fields("crecordnumber") & vbTab & Val(clsTemp.Fields("Cunit")) / 100 & vbTab & CustomizedInfo.CertifiedRatio * 100 & vbTab & clsTemp.Fields("cldesc") & vbTab & clsTemp.Fields("csdesc") & vbTab & "False")
				// Else
				boolHide = false;
				if (Strings.UCase(strCaption) == "DWELLING")
				{
					if (Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) >= 1490 && Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) <= 1509)
					{
						boolHide = true;
					}
					else if (Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) >= 1590 && Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) <= 1599)
					{
						boolHide = true;
					}
					else if (Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) >= 1690 && Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) <= 1699)
					{
						boolHide = true;
					}
				}
				else if (Strings.UCase(strCaption) == "PROPERTY")
				{
					if (Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) >= 1000 && Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) <= 1090)
					{
						boolHide = true;
					}
					else if (Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) >= 1093 && Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) <= 1199)
					{
						boolHide = true;
					}
					else if (Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) >= 1200 && Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) <= 1299)
					{
						boolHide = true;
					}
					else if (Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) >= 1390 && Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) <= 1429)
					{
						boolHide = true;
					}
					else if (Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) >= 1430 && Conversion.Val(clsTemp.Get_Fields_Int32("crecordnumber")) <= 1439)
					{
						boolHide = true;
					}
				}
				Grid1.AddItem(clsTemp.Get_Fields_Int32("crecordnumber") + "\t" + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_String("cunit")) / 100) + "\t" + FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields_String("cbase")) / 100) + "\t" + clsTemp.Get_Fields_String("cldesc") + "\t" + clsTemp.Get_Fields_String("csdesc") + "\t" + "False");
				// End If
				if (Conversion.Val(clsTemp.Get_Fields_String("cunit") + "") < 0)
				{
					Grid1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid1.Rows - 1, 1, Information.RGB(210, 0, 0));
				}
				if (Conversion.Val(clsTemp.Get_Fields_String("cbase") + "") < 0)
				{
					Grid1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid1.Rows - 1, 2, Information.RGB(210, 0, 0));
				}
				if (boolHide)
				{
					Grid1.RowHidden(Grid1.Rows - 1, true);
				}
				clsTemp.MoveNext();
			}
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			int oldrow;
			oldrow = Grid1.Row;
			Grid1.Row = 0;
			SaveStuff();
			Grid1.Row = oldrow;
			if (boolCommCost)
			{
				modGlobalVariables.Statics.boolComCostChanged = true;
			}
			else
			{
				modGlobalVariables.Statics.boolCostRecChanged = true;
			}
			boolChanged = false;
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			int intTemp;
			int x;
			intTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Text1.Text)));
			x = Grid1.FindRow(intTemp, 1, 0);
			if (x > 0)
			{
				Grid1.Row = x;
				Grid1.TopRow = x;
				//FC:FINAL:DDU:#i1375 - make cell selected
				Grid1.AllowSelection = true;
				Grid1.Select(x, 1);
				Grid1.EditCell();
			}
			else
			{
				MessageBox.Show("That number does not exist in this collection of Cost Files", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			Text1.Text = string.Empty;
		}

		public void Command1_Click()
		{
			Command1_Click(Command1, new System.EventArgs());
		}

		private void frmCostFiles_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				// Call SetupGrid1
				// Call SetupGridTownCode
				// Call FillGrid1
				boolLoaded = true;
			}
		}

		private void frmCostFiles_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				this.Unload();
			}
		}

		private void frmCostFiles_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCostFiles properties;
			//frmCostFiles.ScaleWidth	= 9045;
			//frmCostFiles.ScaleHeight	= 7260;
			//frmCostFiles.LinkTopic	= "Form1";
			//End Unmaped Properties
			boolLoaded = false;
			modGlobalFunctions.SetFixedSize(this);
			SetupGrid1();
			SetupGridTownCode();
			if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
			{
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(1));
			}
			FillGrid1();
			boolLoaded = true;
		}

		private void SaveStuff()
		{
			// searches for true values in the last column.  This means the data was potentially edited
			try
			{
				// On Error GoTo ErrorHandler
				int intTemp;
				clsDRWrapper clsTemp = new clsDRWrapper();
				string strTemp1 = "";
				string strTemp2 = "";
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				intTemp = Grid1.FindRow("True", 1, 5);
				while (intTemp > 0)
				{
					strTemp1 = Grid1.TextMatrix(intTemp, 3);
					strTemp2 = Grid1.TextMatrix(intTemp, 4);
					modGlobalRoutines.escapequote(ref strTemp1);
					modGlobalRoutines.escapequote(ref strTemp2);
					if (strTemp1 == string.Empty)
						strTemp1 = " ";
					if (strTemp2 == string.Empty)
						strTemp2 = " ";
					modGlobalVariables.Statics.strSQL = "cunit = " + FCConvert.ToString(Conversion.Val(Grid1.TextMatrix(intTemp, 1)) * 100) + "";
					modGlobalVariables.Statics.strSQL += ",cbase = " + FCConvert.ToString(Conversion.Val(Grid1.TextMatrix(intTemp, 2)) * 100) + "";
					modGlobalVariables.Statics.strSQL += ",cldesc = '" + strTemp1 + "'";
					modGlobalVariables.Statics.strSQL += ",csdesc = '" + strTemp2 + "'";
					clsTemp.Execute("update " + strCostTable + " set " + modGlobalVariables.Statics.strSQL + " where crecordnumber = " + Grid1.TextMatrix(intTemp, 0) + " and townnumber = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))), modGlobalVariables.strREDatabase);
					// If UCase(Me.Caption) = "EXEMPTION CODES" And Grid1.TextMatrix(intTemp, 0) = 1900 Then
					// CustomizedInfo.CertifiedRatio = Val(Grid1.TextMatrix(intTemp, 2)) / 100
					// Call clsTemp.Execute("update customize set certifiedratio = " & CustomizedInfo.CertifiedRatio, strREDatabase)
					// End If
					intTemp = Grid1.FindRow("True", intTemp + 1, 5);
				}
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpText, 1, 5, Grid1.Rows - 1, 5, "False");
				FCGlobal.Screen.MousePointer = (MousePointerConstants)FileAttribute.Normal;
				MessageBox.Show("Save Complete", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = (MousePointerConstants)FileAttribute.Normal;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Save Cost Files Routine", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intRes As Variant --> As DialogResult
			DialogResult intRes;
			if (boolChanged)
			{
				intRes = MessageBox.Show("Data has changed.  Do you wish to save first?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intRes == DialogResult.Yes)
				{
					Grid1.Row = 0;
					Grid1.Col = 0;
					cmdSave_Click();
				}
			}
		}

		private void frmCostFiles_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			ResizeGridTownCode();
		}

		private void ResizeGrid()
		{
			int lngWidth;
			lngWidth = Grid1.WidthOriginal;
			Grid1.ColWidth(0, FCConvert.ToInt32(lngWidth * 0.1));
			Grid1.ColWidth(1, FCConvert.ToInt32(lngWidth * 0.215));
			Grid1.ColWidth(2, FCConvert.ToInt32(lngWidth * 0.195));
			Grid1.ColWidth(3, FCConvert.ToInt32(lngWidth * 0.32));
			Grid1.ColWidth(4, FCConvert.ToInt32(lngWidth * 0.13));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void Grid1_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			boolChanged = true;
			if (Conversion.Val(Grid1.TextMatrix(Grid1.Row, 1) + "") < 0)
			{
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid1.Row, 1, Information.RGB(210, 0, 0));
			}
			else
			{
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid1.Row, 1, Information.RGB(1, 1, 1));
			}
			if (Conversion.Val(Grid1.TextMatrix(Grid1.Row, 2) + "") < 0)
			{
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid1.Row, 2, Information.RGB(210, 0, 0));
			}
			else
			{
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid1.Row, 2, Information.RGB(1, 1, 1));
			}
		}

		private void Grid1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			Grid1.TextMatrix(Grid1.GetFlexRowIndex(e.RowIndex), 5, "True");
		}

		public void mnuExit_Click()
		{
			this.Unload();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// rptCostFiles.Show vbModal, MDIParent
			// vbPorter upgrade warning: rObj As object --> As rptCostFiles
			rptCostFiles rObj = null;
			rObj = new rptCostFiles();
			frmReportViewer.InstancePtr.Init(rObj, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), this.Modal);
			rObj = null;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(cmdSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			mnuExit_Click();
		}

		private void Text1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						Command1_Click();
						break;
					}
			}
			//end switch
		}
	}
}
