﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srpt1CardLand.
	/// </summary>
	public partial class srpt1CardLand : FCSectionReport
	{
		public static srpt1CardLand InstancePtr
		{
			get
			{
				return (srpt1CardLand)Sys.GetInstance(typeof(srpt1CardLand));
			}
		}

		protected srpt1CardLand _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srpt1CardLand()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srpt1CardLand	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngAcct;
		int intCard;
		bool boolPrintTest;
		int intIndex;
		double dblTotalLand;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = intIndex > (this.ParentReport as rpt1PagePropertyCard).LandSummary.Count;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rpt1PagePropertyCard).CurrentAccount)));
			intCard = FCConvert.ToInt32(Math.Round(Conversion.Val((this.ParentReport as rpt1PagePropertyCard).CurrentCard)));
			boolPrintTest = (this.ParentReport as rpt1PagePropertyCard).PrintTest;
			intIndex = 1;
			dblTotalLand = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intIndex <= (this.ParentReport as rpt1PagePropertyCard).LandSummary.Count)
			{
				txtLandType.Text = (this.ParentReport as rpt1PagePropertyCard).LandSummary[intIndex].Description;
				txtFactor.Text = (this.ParentReport as rpt1PagePropertyCard).LandSummary[intIndex].Factor;
				txtUnits.Text = (this.ParentReport as rpt1PagePropertyCard).LandSummary[intIndex].Units;
				txtValue.Text = (this.ParentReport as rpt1PagePropertyCard).LandSummary[intIndex].LandValue;
				txtMethod.Text = (this.ParentReport as rpt1PagePropertyCard).LandSummary[intIndex].Method;
				if ((this.ParentReport as rpt1PagePropertyCard).LandSummary[intIndex].LandValue != "")
				{
					dblTotalLand += FCConvert.ToDouble((this.ParentReport as rpt1PagePropertyCard).LandSummary[intIndex].LandValue);
				}
				intIndex += 1;
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalValue.Text = Strings.Format(dblTotalLand, "#,###,###,##0");
		}

		
	}
}
