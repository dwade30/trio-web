﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPendingChanges.
	/// </summary>
	public partial class rptPendingChanges : BaseSectionReport
	{
		public static rptPendingChanges InstancePtr
		{
			get
			{
				return (rptPendingChanges)Sys.GetInstance(typeof(rptPendingChanges));
			}
		}

		protected rptPendingChanges _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsLoad?.Dispose();
				clsMaster?.Dispose();
                clsLoad = null;
                clsMaster = null;
            }
			base.Dispose(disposing);
		}

		public rptPendingChanges()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Pending Changes";
		}
		// nObj = 1
		//   0	rptPendingChanges	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsLoad = new clsDRWrapper();
		clsDRWrapper clsMaster = new clsDRWrapper();
		cPartyController tPC = new cPartyController();
		// vbPorter upgrade warning: tP As cParty	OnWrite(cParty)
		cParty tP = new cParty();

		public void Init()
		{
			clsLoad.OpenRecordset("select account,transactionnumber,card,mindate from pendingchanges GROUP by transactionnumber,account,card,mindate order by transactionnumber", modGlobalVariables.strREDatabase);
			clsMaster.OpenRecordset("select rsaccount,ownerpartyid from master where rscard = 1 and rsaccount in (select account from pendingchanges)", modGlobalVariables.strREDatabase);
			// Call clsMaster.InsertName("OwnerPartyID,SecOwnerPartyID", "Owner,SecOwner", False, True, False, "RE", False, "", True)
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No pending changes found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsLoad.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!clsLoad.EndOfFile())
			{
				txtTransaction.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("transactionnumber"));
				// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
				txtAccount.Text = FCConvert.ToString(clsLoad.Get_Fields("account"));
				// TODO Get_Fields: Check the table for the column [card] and replace with corresponding Get_Field method
				txtCard.Text = FCConvert.ToString(clsLoad.Get_Fields("card"));
				//FC:FINAL:RPU:#i1624 - Use GetFields instead of Get_Fields_DateTime
				//txtMinDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("mindate"), "MM/dd/yyyy");
				txtMinDate.Text = Strings.Format(clsLoad.Get_Fields("mindate"), "MM/dd/yyyy");
				txtName.Text = "";
				if (!(clsMaster.EndOfFile() && clsMaster.BeginningOfFile()))
				{
					// If clsMaster.FindFirstRecord("rsaccount", Val(clsLoad.Fields("account"))) Then
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					if (clsMaster.FindFirst("rsaccount = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("account")))))
					{
						tP = tPC.GetParty(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("ownerpartyid"))));
						if (!(tP == null))
						{
							txtName.Text = tP.FullNameLastFirst;
						}
					}
				}
				clsLoad.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
