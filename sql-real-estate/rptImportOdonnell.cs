﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptImportOdonnell.
	/// </summary>
	public partial class rptImportOdonnell : BaseSectionReport
	{
		public static rptImportOdonnell InstancePtr
		{
			get
			{
				return (rptImportOdonnell)Sys.GetInstance(typeof(rptImportOdonnell));
			}
		}

		protected rptImportOdonnell _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptImportOdonnell()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.Name = "Imported Accounts";

        }
		// nObj = 1
		//   0	rptImportOdonnell	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngRow;
		const int CNSTGRIDCOLACCOUNT = 0;
		const int CNSTGRIDCOLNAME = 1;
		const int CNSTGRIDCOLMAPLOT = 2;
		const int CNSTGRIDCOLLAND = 3;
		const int CNSTGRIDCOLBLDG = 4;
		const int CNSTGRIDCOLEXEMPTION = 5;
		const int CNSTGRIDCOLNEW = 6;
		const int CNSTGRIDCOLTOTACRES = 7;
		const int CNSTGRIDCOLSOFTACRES = 8;
		const int CNSTGRIDCOLMIXEDACRES = 9;
		const int CNSTGRIDCOLHARDACRES = 10;
		const int CNSTGRIDCOLSOFTVALUE = 11;
		const int CNSTGRIDCOLMIXEDVALUE = 12;
		const int CNSTGRIDCOLHARDVALUE = 13;
		double dblLand;
		double dblBldg;
		double dblExempt;
		double dblSoftAcres;
		private double dblMixedAcres;
		private double dblHardAcres;
		private double dblTotAcres;
		private double dblSoftValue;
		private double dblMixedValue;
		private double dblHardValue;

		public void Init(bool modalDialog)
		{
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "Import", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngRow == frmImportODonnell.InstancePtr.Grid.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngRow = 0;
			txtNumUpdated.Text = frmImportODonnell.InstancePtr.lblUpdated.Text;
			txtNumNew.Text = frmImportODonnell.InstancePtr.lblNew;
			dblLand = 0;
			dblBldg = 0;
			dblExempt = 0;
			dblSoftAcres = 0;
			dblMixedAcres = 0;
			dblHardAcres = 0;
			dblSoftValue = 0;
			dblMixedValue = 0;
			dblHardValue = 0;
			dblTotAcres = 0;
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngRow < frmImportODonnell.InstancePtr.Grid.Rows)
			{
				txtAcct.Text = frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT);
				txtName.Text = frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME);
				txtMapLot.Text = frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT);
				txtExemption.Text = Strings.Format(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLEXEMPTION), "#,###,###,##0");
				txtLand.Text = Strings.Format(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLLAND), "#,###,###,##0");
				txtBldg.Text = Strings.Format(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLBLDG), "#,###,###,##0");
				dblLand += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLLAND));
				dblBldg += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLBLDG));
				dblExempt += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLEXEMPTION));
				dblSoftAcres += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLSOFTACRES));
				dblMixedAcres += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLMIXEDACRES));
				dblHardAcres += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLHARDACRES));
				dblTotAcres += Conversion.Val(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLTOTACRES));
				if (FCConvert.CBool(frmImportODonnell.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLNEW)))
				{
					txtNew.Text = "NEW";
				}
				else
				{
					txtNew.Text = "";
				}
				lngRow += 1;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotBuilding.Text = Strings.Format(dblBldg, "#,###,###,##0");
			txtTotLand.Text = Strings.Format(dblLand, "#,###,###,##0");
			txtTotExemption.Text = Strings.Format(dblExempt, "#,###,###,##0");
			txtSoftAcres.Text = Strings.Format(dblSoftAcres, "#,###,###,##0.00");
			txtMixedAcres.Text = Strings.Format(dblMixedAcres, "#,###,###,##0.00");
			txtHardAcres.Text = Strings.Format(dblHardAcres, "#,###,###,##0.00");
			txtTotalAcres.Text = Strings.Format(dblTotAcres, "#,###,###,##0.00");
			txtSoftValue.Text = Strings.Format(dblSoftValue, "#,###,###,##0");
			txtMixedValue.Text = Strings.Format(dblMixedValue, "#,###,###,##0");
			txtHardValue.Text = Strings.Format(dblHardValue, "#,###,###,##0");
			txtTotalLand.Text = txtTotLand.Text;
			txtOtherAcres.Text = Strings.Format((dblTotAcres - (dblMixedAcres + dblSoftAcres + dblHardAcres)), "#,###,###,##0.00");
			txtOtherValue.Text = Strings.Format(dblLand - (dblSoftValue + dblMixedValue + dblHardValue), "#,###,###,###,##0");
		}

		
	}
}
