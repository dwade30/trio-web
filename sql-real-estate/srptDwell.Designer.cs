﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptDwell.
	/// </summary>
	partial class srptDwell
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptDwell));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.lblStyle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDwelling = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblopen3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblopen4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBase = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblO3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblO4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblO5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblsqft = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGrade = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStyle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFoundation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFinBsmt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeating = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRooms = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBedrooms = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBaths = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAttic = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFireplaces = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnfinLiving = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOpen5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMasonry = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoofCover = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBasement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCooling = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOpen5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFixtures = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHalfBaths = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTrim = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRoof = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtO3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtO4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBsmt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFBsmt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHeatCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtO5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlumbing = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAtticCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFireplaceCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtInsulationCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnfinCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBuilt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRenovated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtKitchensDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBathsDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLayout = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCondition = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFobs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEcObs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhys = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEco = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSfla = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtSfla = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDwelling)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblopen3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblopen4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblO3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblO4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblO5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblsqft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinBsmt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeating)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRooms)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBedrooms)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttic)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFireplaces)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnfinLiving)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMasonry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCover)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCooling)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFixtures)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHalfBaths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTrim)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoof)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtO3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtO4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBsmt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFBsmt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtO5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlumbing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFireplaceCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnfinCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRenovated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchensDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathsDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFobs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEcObs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEco)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSfla)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSfla)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblStyle,
				this.Label2,
				this.lblDwelling,
				this.lblopen3,
				this.lblopen4,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Field1,
				this.Line1,
				this.Line2,
				this.Label16,
				this.txtBase,
				this.Label17,
				this.Label18,
				this.Label19,
				this.lblO3,
				this.lblO4,
				this.Label20,
				this.Label21,
				this.Label22,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.lblO5,
				this.lblsqft,
				this.txtGrade,
				this.txtStyle,
				this.txtExt,
				this.txtUnits,
				this.txtOpen3,
				this.txtOpen4,
				this.txtFoundation,
				this.txtFinBsmt,
				this.txtHeating,
				this.txtRooms,
				this.txtBedrooms,
				this.txtBaths,
				this.txtAttic,
				this.txtFireplaces,
				this.txtInsulation,
				this.txtUnfinLiving,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Label31,
				this.Label32,
				this.lblOpen5,
				this.Label33,
				this.Label34,
				this.txtMasonry,
				this.txtRoofCover,
				this.txtBasement,
				this.txtGar,
				this.txtCooling,
				this.txtOpen5,
				this.txtFixtures,
				this.txtHalfBaths,
				this.Field2,
				this.Line3,
				this.Line4,
				this.txtTrim,
				this.txtRoof,
				this.txtO3,
				this.txtO4,
				this.txtBsmt,
				this.txtFBsmt,
				this.txtHeatCost,
				this.txtO5,
				this.txtPlumbing,
				this.txtAtticCost,
				this.txtFireplaceCost,
				this.txtInsulationCost,
				this.txtUnfinCost,
				this.Label35,
				this.Label36,
				this.Label37,
				this.Label38,
				this.Label39,
				this.Label40,
				this.Label41,
				this.txtBuilt,
				this.txtRenovated,
				this.txtKitchensDesc,
				this.txtTotal,
				this.txtBathsDesc,
				this.txtLayout,
				this.txtCondition,
				this.Label42,
				this.Label43,
				this.Label44,
				this.Label45,
				this.Label46,
				this.Label47,
				this.txtFobs,
				this.txtEcObs,
				this.txtPhys,
				this.txtFunc,
				this.txtEco,
				this.txtValue,
				this.lblSfla,
				this.txtSfla
			});
			this.Detail.Height = 3.21875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// lblStyle
			// 
			this.lblStyle.Height = 0.19625F;
			this.lblStyle.HyperLink = null;
			this.lblStyle.Left = 0F;
			this.lblStyle.MultiLine = false;
			this.lblStyle.Name = "lblStyle";
			this.lblStyle.Style = "font-family: \'Tahoma\'";
			this.lblStyle.Tag = "text";
			this.lblStyle.Text = "Label1";
			this.lblStyle.Top = 0.15625F;
			this.lblStyle.Width = 1.4375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19625F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.MultiLine = false;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'";
			this.Label2.Tag = "text";
			this.Label2.Text = "Exterior";
			this.Label2.Top = 0.3125F;
			this.Label2.Width = 1.4375F;
			// 
			// lblDwelling
			// 
			this.lblDwelling.Height = 0.19625F;
			this.lblDwelling.HyperLink = null;
			this.lblDwelling.Left = 0F;
			this.lblDwelling.MultiLine = false;
			this.lblDwelling.Name = "lblDwelling";
			this.lblDwelling.Style = "font-family: \'Tahoma\'";
			this.lblDwelling.Tag = "text";
			this.lblDwelling.Text = "Dwelling Units";
			this.lblDwelling.Top = 0.46875F;
			this.lblDwelling.Width = 1.4375F;
			// 
			// lblopen3
			// 
			this.lblopen3.Height = 0.19625F;
			this.lblopen3.HyperLink = null;
			this.lblopen3.Left = 0F;
			this.lblopen3.MultiLine = false;
			this.lblopen3.Name = "lblopen3";
			this.lblopen3.Style = "font-family: \'Tahoma\'";
			this.lblopen3.Tag = "text";
			this.lblopen3.Text = "Street Setback";
			this.lblopen3.Top = 0.625F;
			this.lblopen3.Width = 1.4375F;
			// 
			// lblopen4
			// 
			this.lblopen4.Height = 0.19625F;
			this.lblopen4.HyperLink = null;
			this.lblopen4.Left = 0F;
			this.lblopen4.MultiLine = false;
			this.lblopen4.Name = "lblopen4";
			this.lblopen4.Style = "font-family: \'Tahoma\'";
			this.lblopen4.Tag = "text";
			this.lblopen4.Text = "Label5";
			this.lblopen4.Top = 0.78125F;
			this.lblopen4.Width = 1.4375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19625F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.MultiLine = false;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'";
			this.Label6.Tag = "text";
			this.Label6.Text = "Foundation";
			this.Label6.Top = 0.9375F;
			this.Label6.Width = 1.4375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19625F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.MultiLine = false;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'";
			this.Label7.Tag = "text";
			this.Label7.Text = "Fin. Basement Area";
			this.Label7.Top = 1.09375F;
			this.Label7.Width = 1.4375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19625F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.MultiLine = false;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'";
			this.Label8.Tag = "text";
			this.Label8.Text = "Heating";
			this.Label8.Top = 1.25F;
			this.Label8.Width = 1.4375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19625F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.MultiLine = false;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'";
			this.Label9.Tag = "text";
			this.Label9.Text = "Rooms";
			this.Label9.Top = 1.40625F;
			this.Label9.Width = 1.4375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19625F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0F;
			this.Label10.MultiLine = false;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'";
			this.Label10.Tag = "text";
			this.Label10.Text = "Bedrooms";
			this.Label10.Top = 1.5625F;
			this.Label10.Width = 1.4375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19625F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0F;
			this.Label11.MultiLine = false;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'";
			this.Label11.Tag = "text";
			this.Label11.Text = "Baths";
			this.Label11.Top = 1.71875F;
			this.Label11.Width = 1.4375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19625F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0F;
			this.Label12.MultiLine = false;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'";
			this.Label12.Tag = "text";
			this.Label12.Text = "Attic";
			this.Label12.Top = 1.875F;
			this.Label12.Width = 1.4375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19625F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0F;
			this.Label13.MultiLine = false;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'";
			this.Label13.Tag = "text";
			this.Label13.Text = "FirePlaces";
			this.Label13.Top = 2.03125F;
			this.Label13.Width = 1.4375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19625F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0F;
			this.Label14.MultiLine = false;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'";
			this.Label14.Tag = "text";
			this.Label14.Text = "Insulation";
			this.Label14.Top = 2.1875F;
			this.Label14.Width = 1.4375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19625F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.MultiLine = false;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'";
			this.Label15.Tag = "text";
			this.Label15.Text = "Unfin. Living Area";
			this.Label15.Top = 2.34375F;
			this.Label15.Width = 1.4375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.19625F;
			this.Field1.Left = 1.375F;
			this.Field1.MultiLine = false;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field1.Tag = "bold";
			this.Field1.Text = "Dwelling Description";
			this.Field1.Top = 0F;
			this.Field1.Width = 1.8125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.09375F;
			this.Line1.Width = 1.25F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 1.3125F;
			this.Line1.Y1 = 0.09375F;
			this.Line1.Y2 = 0.09375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.1875F;
			this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.09375F;
			this.Line2.Width = 1.25F;
			this.Line2.X1 = 3.1875F;
			this.Line2.X2 = 4.4375F;
			this.Line2.Y1 = 0.09375F;
			this.Line2.Y2 = 0.09375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19625F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 5.5625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Label16.Tag = "bold";
			this.Label16.Text = "Replacement Cost New";
			this.Label16.Top = 0F;
			this.Label16.Width = 1.8125F;
			// 
			// txtBase
			// 
			this.txtBase.CanGrow = false;
			this.txtBase.Height = 0.19625F;
			this.txtBase.Left = 6.5625F;
			this.txtBase.MultiLine = false;
			this.txtBase.Name = "txtBase";
			this.txtBase.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBase.Tag = "text";
			this.txtBase.Text = "Field2";
			this.txtBase.Top = 0.15625F;
			this.txtBase.Width = 0.875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19625F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 5.5625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'";
			this.Label17.Tag = "text";
			this.Label17.Text = "Base";
			this.Label17.Top = 0.15625F;
			this.Label17.Width = 1F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19625F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 5.5625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'";
			this.Label18.Tag = "text";
			this.Label18.Text = "Trim";
			this.Label18.Top = 0.3125F;
			this.Label18.Width = 1F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19625F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 5.5625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'";
			this.Label19.Tag = "text";
			this.Label19.Text = "Roof";
			this.Label19.Top = 0.46875F;
			this.Label19.Width = 1F;
			// 
			// lblO3
			// 
			this.lblO3.Height = 0.19625F;
			this.lblO3.HyperLink = null;
			this.lblO3.Left = 5.5625F;
			this.lblO3.Name = "lblO3";
			this.lblO3.Style = "font-family: \'Tahoma\'";
			this.lblO3.Tag = "text";
			this.lblO3.Text = "Label20";
			this.lblO3.Top = 0.625F;
			this.lblO3.Width = 1F;
			// 
			// lblO4
			// 
			this.lblO4.Height = 0.19625F;
			this.lblO4.HyperLink = null;
			this.lblO4.Left = 5.5625F;
			this.lblO4.Name = "lblO4";
			this.lblO4.Style = "font-family: \'Tahoma\'";
			this.lblO4.Tag = "text";
			this.lblO4.Text = "Label20";
			this.lblO4.Top = 0.78125F;
			this.lblO4.Width = 1F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19625F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 5.5625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'";
			this.Label20.Tag = "text";
			this.Label20.Text = "Basement";
			this.Label20.Top = 0.9375F;
			this.Label20.Width = 1F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19625F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 5.5625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'";
			this.Label21.Tag = "text";
			this.Label21.Text = "Fin Bsmt";
			this.Label21.Top = 1.09375F;
			this.Label21.Width = 1F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.19625F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 5.5625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'";
			this.Label22.Tag = "text";
			this.Label22.Text = "Heat";
			this.Label22.Top = 1.25F;
			this.Label22.Width = 1F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19625F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 5.5625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'";
			this.Label23.Tag = "text";
			this.Label23.Text = "Plumbing";
			this.Label23.Top = 1.71875F;
			this.Label23.Width = 1F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19625F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 5.5625F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'";
			this.Label24.Tag = "text";
			this.Label24.Text = "Attic";
			this.Label24.Top = 1.875F;
			this.Label24.Width = 1F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.19625F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 5.5625F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'";
			this.Label25.Tag = "text";
			this.Label25.Text = "Fireplace";
			this.Label25.Top = 2.03125F;
			this.Label25.Width = 1F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.19625F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 5.5625F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'";
			this.Label26.Tag = "text";
			this.Label26.Text = "Insulation";
			this.Label26.Top = 2.1875F;
			this.Label26.Width = 1F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.19625F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 5.5625F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'";
			this.Label27.Tag = "text";
			this.Label27.Text = "Unfinished";
			this.Label27.Top = 2.34375F;
			this.Label27.Width = 1F;
			// 
			// lblO5
			// 
			this.lblO5.Height = 0.19625F;
			this.lblO5.HyperLink = null;
			this.lblO5.Left = 5.5625F;
			this.lblO5.Name = "lblO5";
			this.lblO5.Style = "font-family: \'Tahoma\'";
			this.lblO5.Tag = "text";
			this.lblO5.Text = null;
			this.lblO5.Top = 1.40625F;
			this.lblO5.Width = 1F;
			// 
			// lblsqft
			// 
			this.lblsqft.Height = 0.19625F;
			this.lblsqft.HyperLink = null;
			this.lblsqft.Left = 2.9375F;
			this.lblsqft.MultiLine = false;
			this.lblsqft.Name = "lblsqft";
			this.lblsqft.Style = "font-family: \'Tahoma\'";
			this.lblsqft.Tag = "text";
			this.lblsqft.Text = "Label28";
			this.lblsqft.Top = 0.15625F;
			this.lblsqft.Width = 1.125F;
			// 
			// txtGrade
			// 
			this.txtGrade.CanGrow = false;
			this.txtGrade.Height = 0.19625F;
			this.txtGrade.Left = 4.0625F;
			this.txtGrade.MultiLine = false;
			this.txtGrade.Name = "txtGrade";
			this.txtGrade.Style = "font-family: \'Tahoma\'";
			this.txtGrade.Tag = "text";
			this.txtGrade.Text = "Asphalt Shingles";
			this.txtGrade.Top = 0.15625F;
			this.txtGrade.Width = 1.4375F;
			// 
			// txtStyle
			// 
			this.txtStyle.CanGrow = false;
			this.txtStyle.Height = 0.19625F;
			this.txtStyle.Left = 1.5F;
			this.txtStyle.MultiLine = false;
			this.txtStyle.Name = "txtStyle";
			this.txtStyle.Style = "font-family: \'Tahoma\'";
			this.txtStyle.Tag = "text";
			this.txtStyle.Text = "Field3";
			this.txtStyle.Top = 0.15625F;
			this.txtStyle.Width = 1.375F;
			// 
			// txtExt
			// 
			this.txtExt.CanGrow = false;
			this.txtExt.Height = 0.19625F;
			this.txtExt.Left = 1.5F;
			this.txtExt.MultiLine = false;
			this.txtExt.Name = "txtExt";
			this.txtExt.Style = "font-family: \'Tahoma\'";
			this.txtExt.Tag = "text";
			this.txtExt.Text = "Field2";
			this.txtExt.Top = 0.3125F;
			this.txtExt.Width = 1.375F;
			// 
			// txtUnits
			// 
			this.txtUnits.CanGrow = false;
			this.txtUnits.Height = 0.19625F;
			this.txtUnits.Left = 1.5F;
			this.txtUnits.MultiLine = false;
			this.txtUnits.Name = "txtUnits";
			this.txtUnits.Style = "font-family: \'Tahoma\'";
			this.txtUnits.Tag = "text";
			this.txtUnits.Text = "Field3";
			this.txtUnits.Top = 0.46875F;
			this.txtUnits.Width = 1.375F;
			// 
			// txtOpen3
			// 
			this.txtOpen3.CanGrow = false;
			this.txtOpen3.Height = 0.19625F;
			this.txtOpen3.Left = 1.5F;
			this.txtOpen3.MultiLine = false;
			this.txtOpen3.Name = "txtOpen3";
			this.txtOpen3.Style = "font-family: \'Tahoma\'";
			this.txtOpen3.Tag = "text";
			this.txtOpen3.Text = "Field4";
			this.txtOpen3.Top = 0.625F;
			this.txtOpen3.Width = 1.375F;
			// 
			// txtOpen4
			// 
			this.txtOpen4.CanGrow = false;
			this.txtOpen4.Height = 0.19625F;
			this.txtOpen4.Left = 1.5F;
			this.txtOpen4.MultiLine = false;
			this.txtOpen4.Name = "txtOpen4";
			this.txtOpen4.Style = "font-family: \'Tahoma\'";
			this.txtOpen4.Tag = "text";
			this.txtOpen4.Text = "Field5";
			this.txtOpen4.Top = 0.78125F;
			this.txtOpen4.Width = 1.375F;
			// 
			// txtFoundation
			// 
			this.txtFoundation.CanGrow = false;
			this.txtFoundation.Height = 0.19625F;
			this.txtFoundation.Left = 1.5F;
			this.txtFoundation.MultiLine = false;
			this.txtFoundation.Name = "txtFoundation";
			this.txtFoundation.Style = "font-family: \'Tahoma\'";
			this.txtFoundation.Tag = "text";
			this.txtFoundation.Text = "Field6";
			this.txtFoundation.Top = 0.9375F;
			this.txtFoundation.Width = 1.375F;
			// 
			// txtFinBsmt
			// 
			this.txtFinBsmt.CanGrow = false;
			this.txtFinBsmt.Height = 0.19625F;
			this.txtFinBsmt.Left = 1.5F;
			this.txtFinBsmt.MultiLine = false;
			this.txtFinBsmt.Name = "txtFinBsmt";
			this.txtFinBsmt.Style = "font-family: \'Tahoma\'";
			this.txtFinBsmt.Tag = "text";
			this.txtFinBsmt.Text = "Field7";
			this.txtFinBsmt.Top = 1.09375F;
			this.txtFinBsmt.Width = 1.375F;
			// 
			// txtHeating
			// 
			this.txtHeating.CanGrow = false;
			this.txtHeating.Height = 0.19625F;
			this.txtHeating.Left = 1.5F;
			this.txtHeating.MultiLine = false;
			this.txtHeating.Name = "txtHeating";
			this.txtHeating.Style = "font-family: \'Tahoma\'";
			this.txtHeating.Tag = "text";
			this.txtHeating.Text = "Field8";
			this.txtHeating.Top = 1.25F;
			this.txtHeating.Width = 1.375F;
			// 
			// txtRooms
			// 
			this.txtRooms.CanGrow = false;
			this.txtRooms.Height = 0.19625F;
			this.txtRooms.Left = 1.5F;
			this.txtRooms.MultiLine = false;
			this.txtRooms.Name = "txtRooms";
			this.txtRooms.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtRooms.Tag = "text";
			this.txtRooms.Text = "Field9";
			this.txtRooms.Top = 1.40625F;
			this.txtRooms.Width = 1.375F;
			// 
			// txtBedrooms
			// 
			this.txtBedrooms.CanGrow = false;
			this.txtBedrooms.Height = 0.19625F;
			this.txtBedrooms.Left = 1.5F;
			this.txtBedrooms.MultiLine = false;
			this.txtBedrooms.Name = "txtBedrooms";
			this.txtBedrooms.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtBedrooms.Tag = "text";
			this.txtBedrooms.Text = "Field10";
			this.txtBedrooms.Top = 1.5625F;
			this.txtBedrooms.Width = 1.375F;
			// 
			// txtBaths
			// 
			this.txtBaths.CanGrow = false;
			this.txtBaths.Height = 0.19625F;
			this.txtBaths.Left = 1.5F;
			this.txtBaths.MultiLine = false;
			this.txtBaths.Name = "txtBaths";
			this.txtBaths.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtBaths.Tag = "text";
			this.txtBaths.Text = "Field11";
			this.txtBaths.Top = 1.71875F;
			this.txtBaths.Width = 1.375F;
			// 
			// txtAttic
			// 
			this.txtAttic.CanGrow = false;
			this.txtAttic.Height = 0.19625F;
			this.txtAttic.Left = 1.5F;
			this.txtAttic.MultiLine = false;
			this.txtAttic.Name = "txtAttic";
			this.txtAttic.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtAttic.Tag = "text";
			this.txtAttic.Text = "Field12";
			this.txtAttic.Top = 1.875F;
			this.txtAttic.Width = 1.375F;
			// 
			// txtFireplaces
			// 
			this.txtFireplaces.CanGrow = false;
			this.txtFireplaces.Height = 0.19625F;
			this.txtFireplaces.Left = 1.5F;
			this.txtFireplaces.MultiLine = false;
			this.txtFireplaces.Name = "txtFireplaces";
			this.txtFireplaces.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtFireplaces.Tag = "text";
			this.txtFireplaces.Text = "Field13";
			this.txtFireplaces.Top = 2.03125F;
			this.txtFireplaces.Width = 1.375F;
			// 
			// txtInsulation
			// 
			this.txtInsulation.CanGrow = false;
			this.txtInsulation.Height = 0.19625F;
			this.txtInsulation.Left = 1.5F;
			this.txtInsulation.MultiLine = false;
			this.txtInsulation.Name = "txtInsulation";
			this.txtInsulation.Style = "font-family: \'Tahoma\'";
			this.txtInsulation.Tag = "text";
			this.txtInsulation.Text = "Field14";
			this.txtInsulation.Top = 2.1875F;
			this.txtInsulation.Width = 1.375F;
			// 
			// txtUnfinLiving
			// 
			this.txtUnfinLiving.CanGrow = false;
			this.txtUnfinLiving.Height = 0.19625F;
			this.txtUnfinLiving.Left = 1.5F;
			this.txtUnfinLiving.MultiLine = false;
			this.txtUnfinLiving.Name = "txtUnfinLiving";
			this.txtUnfinLiving.Style = "font-family: \'Tahoma\'";
			this.txtUnfinLiving.Tag = "text";
			this.txtUnfinLiving.Text = "Field15";
			this.txtUnfinLiving.Top = 2.34375F;
			this.txtUnfinLiving.Width = 1.375F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.19625F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 2.9375F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'";
			this.Label28.Tag = "text";
			this.Label28.Text = "Masonry Trim";
			this.Label28.Top = 0.3125F;
			this.Label28.Width = 1.125F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.19625F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 2.9375F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'";
			this.Label29.Tag = "text";
			this.Label29.Text = "Roof Cover";
			this.Label29.Top = 0.46875F;
			this.Label29.Width = 1.125F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.19625F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 2.9375F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'";
			this.Label30.Tag = "text";
			this.Label30.Text = "Basement";
			this.Label30.Top = 0.9375F;
			this.Label30.Width = 1.125F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.19625F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 2.9375F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'";
			this.Label31.Tag = "text";
			this.Label31.Text = "Basement Gar";
			this.Label31.Top = 1.09375F;
			this.Label31.Width = 1.125F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.19625F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 2.9375F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'";
			this.Label32.Tag = "text";
			this.Label32.Text = "Cooling";
			this.Label32.Top = 1.25F;
			this.Label32.Width = 1.125F;
			// 
			// lblOpen5
			// 
			this.lblOpen5.Height = 0.19625F;
			this.lblOpen5.HyperLink = null;
			this.lblOpen5.Left = 2.9375F;
			this.lblOpen5.Name = "lblOpen5";
			this.lblOpen5.Style = "font-family: \'Tahoma\'";
			this.lblOpen5.Tag = "text";
			this.lblOpen5.Text = null;
			this.lblOpen5.Top = 1.40625F;
			this.lblOpen5.Width = 1.125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.19625F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 2.9375F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'";
			this.Label33.Tag = "text";
			this.Label33.Text = "Add Fixtures";
			this.Label33.Top = 1.5625F;
			this.Label33.Width = 1.125F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.19625F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 2.9375F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'";
			this.Label34.Tag = "text";
			this.Label34.Text = "Half Baths";
			this.Label34.Top = 1.71875F;
			this.Label34.Width = 1.125F;
			// 
			// txtMasonry
			// 
			this.txtMasonry.CanGrow = false;
			this.txtMasonry.Height = 0.19625F;
			this.txtMasonry.Left = 4.0625F;
			this.txtMasonry.Name = "txtMasonry";
			this.txtMasonry.Style = "font-family: \'Tahoma\'";
			this.txtMasonry.Tag = "text";
			this.txtMasonry.Text = "Field2";
			this.txtMasonry.Top = 0.3125F;
			this.txtMasonry.Width = 1.4375F;
			// 
			// txtRoofCover
			// 
			this.txtRoofCover.CanGrow = false;
			this.txtRoofCover.Height = 0.19625F;
			this.txtRoofCover.Left = 4.0625F;
			this.txtRoofCover.Name = "txtRoofCover";
			this.txtRoofCover.Style = "font-family: \'Tahoma\'";
			this.txtRoofCover.Tag = "text";
			this.txtRoofCover.Text = "Field3";
			this.txtRoofCover.Top = 0.46875F;
			this.txtRoofCover.Width = 1.4375F;
			// 
			// txtBasement
			// 
			this.txtBasement.CanGrow = false;
			this.txtBasement.Height = 0.19625F;
			this.txtBasement.Left = 4.0625F;
			this.txtBasement.Name = "txtBasement";
			this.txtBasement.Style = "font-family: \'Tahoma\'";
			this.txtBasement.Tag = "text";
			this.txtBasement.Text = "Field4";
			this.txtBasement.Top = 0.9375F;
			this.txtBasement.Width = 1.4375F;
			// 
			// txtGar
			// 
			this.txtGar.CanGrow = false;
			this.txtGar.Height = 0.19625F;
			this.txtGar.Left = 4.0625F;
			this.txtGar.Name = "txtGar";
			this.txtGar.Style = "font-family: \'Tahoma\'";
			this.txtGar.Tag = "text";
			this.txtGar.Text = "Field5";
			this.txtGar.Top = 1.09375F;
			this.txtGar.Width = 1.4375F;
			// 
			// txtCooling
			// 
			this.txtCooling.CanGrow = false;
			this.txtCooling.Height = 0.19625F;
			this.txtCooling.Left = 4.0625F;
			this.txtCooling.Name = "txtCooling";
			this.txtCooling.Style = "font-family: \'Tahoma\'";
			this.txtCooling.Tag = "text";
			this.txtCooling.Text = "Field6";
			this.txtCooling.Top = 1.25F;
			this.txtCooling.Width = 1.4375F;
			// 
			// txtOpen5
			// 
			this.txtOpen5.CanGrow = false;
			this.txtOpen5.Height = 0.19625F;
			this.txtOpen5.Left = 4.0625F;
			this.txtOpen5.Name = "txtOpen5";
			this.txtOpen5.Style = "font-family: \'Tahoma\'";
			this.txtOpen5.Tag = "text";
			this.txtOpen5.Text = null;
			this.txtOpen5.Top = 1.40625F;
			this.txtOpen5.Width = 1.4375F;
			// 
			// txtFixtures
			// 
			this.txtFixtures.CanGrow = false;
			this.txtFixtures.Height = 0.19625F;
			this.txtFixtures.Left = 4.0625F;
			this.txtFixtures.Name = "txtFixtures";
			this.txtFixtures.Style = "font-family: \'Tahoma\'";
			this.txtFixtures.Tag = "text";
			this.txtFixtures.Text = "Field8";
			this.txtFixtures.Top = 1.5625F;
			this.txtFixtures.Width = 1.4375F;
			// 
			// txtHalfBaths
			// 
			this.txtHalfBaths.CanGrow = false;
			this.txtHalfBaths.Height = 0.19625F;
			this.txtHalfBaths.Left = 4.0625F;
			this.txtHalfBaths.Name = "txtHalfBaths";
			this.txtHalfBaths.Style = "font-family: \'Tahoma\'";
			this.txtHalfBaths.Tag = "text";
			this.txtHalfBaths.Text = "Field9";
			this.txtHalfBaths.Top = 1.71875F;
			this.txtHalfBaths.Width = 1.4375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.19625F;
			this.Field2.Left = 2.8125F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.Field2.Tag = "bold";
			this.Field2.Text = "Dwelling Condition";
			this.Field2.Top = 2.46875F;
			this.Field2.Width = 1.8125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.0625F;
			this.Line3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line3.LineWeight = 3F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2.5625F;
			this.Line3.Width = 2.75F;
			this.Line3.X1 = 2.8125F;
			this.Line3.X2 = 0.0625F;
			this.Line3.Y1 = 2.5625F;
			this.Line3.Y2 = 2.5625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 4.625F;
			this.Line4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line4.LineWeight = 3F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 2.5625F;
			this.Line4.Width = 2.75F;
			this.Line4.X1 = 4.625F;
			this.Line4.X2 = 7.375F;
			this.Line4.Y1 = 2.5625F;
			this.Line4.Y2 = 2.5625F;
			// 
			// txtTrim
			// 
			this.txtTrim.CanGrow = false;
			this.txtTrim.Height = 0.19625F;
			this.txtTrim.Left = 6.5625F;
			this.txtTrim.MultiLine = false;
			this.txtTrim.Name = "txtTrim";
			this.txtTrim.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTrim.Tag = "text";
			this.txtTrim.Text = "Field3";
			this.txtTrim.Top = 0.3125F;
			this.txtTrim.Width = 0.875F;
			// 
			// txtRoof
			// 
			this.txtRoof.CanGrow = false;
			this.txtRoof.Height = 0.19625F;
			this.txtRoof.Left = 6.5625F;
			this.txtRoof.MultiLine = false;
			this.txtRoof.Name = "txtRoof";
			this.txtRoof.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtRoof.Tag = "text";
			this.txtRoof.Text = "Field3";
			this.txtRoof.Top = 0.46875F;
			this.txtRoof.Width = 0.875F;
			// 
			// txtO3
			// 
			this.txtO3.CanGrow = false;
			this.txtO3.Height = 0.19625F;
			this.txtO3.Left = 6.5625F;
			this.txtO3.MultiLine = false;
			this.txtO3.Name = "txtO3";
			this.txtO3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtO3.Tag = "text";
			this.txtO3.Text = "Field3";
			this.txtO3.Top = 0.625F;
			this.txtO3.Width = 0.875F;
			// 
			// txtO4
			// 
			this.txtO4.CanGrow = false;
			this.txtO4.Height = 0.19625F;
			this.txtO4.Left = 6.5625F;
			this.txtO4.MultiLine = false;
			this.txtO4.Name = "txtO4";
			this.txtO4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtO4.Tag = "text";
			this.txtO4.Text = "Field3";
			this.txtO4.Top = 0.78125F;
			this.txtO4.Width = 0.875F;
			// 
			// txtBsmt
			// 
			this.txtBsmt.CanGrow = false;
			this.txtBsmt.Height = 0.19625F;
			this.txtBsmt.Left = 6.5625F;
			this.txtBsmt.MultiLine = false;
			this.txtBsmt.Name = "txtBsmt";
			this.txtBsmt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBsmt.Tag = "text";
			this.txtBsmt.Text = "Field3";
			this.txtBsmt.Top = 0.9375F;
			this.txtBsmt.Width = 0.875F;
			// 
			// txtFBsmt
			// 
			this.txtFBsmt.CanGrow = false;
			this.txtFBsmt.Height = 0.19625F;
			this.txtFBsmt.Left = 6.5625F;
			this.txtFBsmt.MultiLine = false;
			this.txtFBsmt.Name = "txtFBsmt";
			this.txtFBsmt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFBsmt.Tag = "text";
			this.txtFBsmt.Text = "Field3";
			this.txtFBsmt.Top = 1.09375F;
			this.txtFBsmt.Width = 0.875F;
			// 
			// txtHeatCost
			// 
			this.txtHeatCost.CanGrow = false;
			this.txtHeatCost.Height = 0.19625F;
			this.txtHeatCost.Left = 6.5625F;
			this.txtHeatCost.MultiLine = false;
			this.txtHeatCost.Name = "txtHeatCost";
			this.txtHeatCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtHeatCost.Tag = "text";
			this.txtHeatCost.Text = "Field3";
			this.txtHeatCost.Top = 1.25F;
			this.txtHeatCost.Width = 0.875F;
			// 
			// txtO5
			// 
			this.txtO5.CanGrow = false;
			this.txtO5.Height = 0.19625F;
			this.txtO5.Left = 6.5625F;
			this.txtO5.MultiLine = false;
			this.txtO5.Name = "txtO5";
			this.txtO5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtO5.Tag = "text";
			this.txtO5.Text = null;
			this.txtO5.Top = 1.40625F;
			this.txtO5.Width = 0.875F;
			// 
			// txtPlumbing
			// 
			this.txtPlumbing.CanGrow = false;
			this.txtPlumbing.Height = 0.19625F;
			this.txtPlumbing.Left = 6.5625F;
			this.txtPlumbing.MultiLine = false;
			this.txtPlumbing.Name = "txtPlumbing";
			this.txtPlumbing.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPlumbing.Tag = "text";
			this.txtPlumbing.Text = "Field3";
			this.txtPlumbing.Top = 1.71875F;
			this.txtPlumbing.Width = 0.875F;
			// 
			// txtAtticCost
			// 
			this.txtAtticCost.CanGrow = false;
			this.txtAtticCost.Height = 0.19625F;
			this.txtAtticCost.Left = 6.5625F;
			this.txtAtticCost.MultiLine = false;
			this.txtAtticCost.Name = "txtAtticCost";
			this.txtAtticCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAtticCost.Tag = "text";
			this.txtAtticCost.Text = "Field3";
			this.txtAtticCost.Top = 1.875F;
			this.txtAtticCost.Width = 0.875F;
			// 
			// txtFireplaceCost
			// 
			this.txtFireplaceCost.CanGrow = false;
			this.txtFireplaceCost.Height = 0.19625F;
			this.txtFireplaceCost.Left = 6.5625F;
			this.txtFireplaceCost.MultiLine = false;
			this.txtFireplaceCost.Name = "txtFireplaceCost";
			this.txtFireplaceCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtFireplaceCost.Tag = "text";
			this.txtFireplaceCost.Text = "Field3";
			this.txtFireplaceCost.Top = 2.03125F;
			this.txtFireplaceCost.Width = 0.875F;
			// 
			// txtInsulationCost
			// 
			this.txtInsulationCost.CanGrow = false;
			this.txtInsulationCost.Height = 0.19625F;
			this.txtInsulationCost.Left = 6.5625F;
			this.txtInsulationCost.MultiLine = false;
			this.txtInsulationCost.Name = "txtInsulationCost";
			this.txtInsulationCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtInsulationCost.Tag = "text";
			this.txtInsulationCost.Text = "Field3";
			this.txtInsulationCost.Top = 2.1875F;
			this.txtInsulationCost.Width = 0.875F;
			// 
			// txtUnfinCost
			// 
			this.txtUnfinCost.CanGrow = false;
			this.txtUnfinCost.Height = 0.19625F;
			this.txtUnfinCost.Left = 6.5625F;
			this.txtUnfinCost.MultiLine = false;
			this.txtUnfinCost.Name = "txtUnfinCost";
			this.txtUnfinCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtUnfinCost.Tag = "text";
			this.txtUnfinCost.Text = "Field3";
			this.txtUnfinCost.Top = 2.34375F;
			this.txtUnfinCost.Width = 0.875F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.19625F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0.0625F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label35.Tag = "bold";
			this.Label35.Text = "Built";
			this.Label35.Top = 2.59375F;
			this.Label35.Width = 0.5F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.19625F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 0.625F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label36.Tag = "bold";
			this.Label36.Text = "Renovated";
			this.Label36.Top = 2.59375F;
			this.Label36.Width = 0.875F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.19625F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 1.5F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label37.Tag = "bold";
			this.Label37.Text = "Kitchens";
			this.Label37.Top = 2.59375F;
			this.Label37.Width = 0.875F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.19625F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 2.75F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label38.Tag = "bold";
			this.Label38.Text = "Baths";
			this.Label38.Top = 2.59375F;
			this.Label38.Width = 0.625F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.19625F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 3.9375F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label39.Tag = "bold";
			this.Label39.Text = "Condition";
			this.Label39.Top = 2.59375F;
			this.Label39.Width = 0.875F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.19625F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 5.3125F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label40.Tag = "bold";
			this.Label40.Text = "Layout";
			this.Label40.Top = 2.59375F;
			this.Label40.Width = 0.75F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.19625F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 6.5625F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label41.Tag = "bold";
			this.Label41.Text = "Total";
			this.Label41.Top = 2.59375F;
			this.Label41.Width = 0.8125F;
			// 
			// txtBuilt
			// 
			this.txtBuilt.CanGrow = false;
			this.txtBuilt.Height = 0.19625F;
			this.txtBuilt.Left = 0.0625F;
			this.txtBuilt.MultiLine = false;
			this.txtBuilt.Name = "txtBuilt";
			this.txtBuilt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtBuilt.Tag = "text";
			this.txtBuilt.Text = "Field3";
			this.txtBuilt.Top = 2.75F;
			this.txtBuilt.Width = 0.5F;
			// 
			// txtRenovated
			// 
			this.txtRenovated.CanGrow = false;
			this.txtRenovated.Height = 0.19625F;
			this.txtRenovated.Left = 0.625F;
			this.txtRenovated.MultiLine = false;
			this.txtRenovated.Name = "txtRenovated";
			this.txtRenovated.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtRenovated.Tag = "text";
			this.txtRenovated.Text = "Field4";
			this.txtRenovated.Top = 2.75F;
			this.txtRenovated.Width = 0.75F;
			// 
			// txtKitchensDesc
			// 
			this.txtKitchensDesc.CanGrow = false;
			this.txtKitchensDesc.Height = 0.19625F;
			this.txtKitchensDesc.Left = 1.5F;
			this.txtKitchensDesc.MultiLine = false;
			this.txtKitchensDesc.Name = "txtKitchensDesc";
			this.txtKitchensDesc.Style = "font-family: \'Tahoma\'";
			this.txtKitchensDesc.Tag = "text";
			this.txtKitchensDesc.Text = "Field5";
			this.txtKitchensDesc.Top = 2.75F;
			this.txtKitchensDesc.Width = 1.1875F;
			// 
			// txtTotal
			// 
			this.txtTotal.CanGrow = false;
			this.txtTotal.Height = 0.19625F;
			this.txtTotal.Left = 6.5625F;
			this.txtTotal.MultiLine = false;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotal.Tag = "text";
			this.txtTotal.Text = "Field6";
			this.txtTotal.Top = 2.75F;
			this.txtTotal.Width = 0.875F;
			// 
			// txtBathsDesc
			// 
			this.txtBathsDesc.CanGrow = false;
			this.txtBathsDesc.Height = 0.19625F;
			this.txtBathsDesc.Left = 2.75F;
			this.txtBathsDesc.MultiLine = false;
			this.txtBathsDesc.Name = "txtBathsDesc";
			this.txtBathsDesc.Style = "font-family: \'Tahoma\'";
			this.txtBathsDesc.Tag = "text";
			this.txtBathsDesc.Text = "Field7";
			this.txtBathsDesc.Top = 2.75F;
			this.txtBathsDesc.Width = 1.125F;
			// 
			// txtLayout
			// 
			this.txtLayout.CanGrow = false;
			this.txtLayout.Height = 0.19625F;
			this.txtLayout.Left = 5.3125F;
			this.txtLayout.MultiLine = false;
			this.txtLayout.Name = "txtLayout";
			this.txtLayout.Style = "font-family: \'Tahoma\'";
			this.txtLayout.Tag = "text";
			this.txtLayout.Text = "Field8";
			this.txtLayout.Top = 2.75F;
			this.txtLayout.Width = 1.1875F;
			// 
			// txtCondition
			// 
			this.txtCondition.CanGrow = false;
			this.txtCondition.Height = 0.19625F;
			this.txtCondition.Left = 3.9375F;
			this.txtCondition.MultiLine = false;
			this.txtCondition.Name = "txtCondition";
			this.txtCondition.Style = "font-family: \'Tahoma\'";
			this.txtCondition.Tag = "text";
			this.txtCondition.Text = "Field9";
			this.txtCondition.Top = 2.75F;
			this.txtCondition.Width = 1.3125F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.19625F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 0.0625F;
			this.Label42.MultiLine = false;
			this.Label42.Name = "Label42";
			this.Label42.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label42.Tag = "bold";
			this.Label42.Text = "Functional Obsolescence";
			this.Label42.Top = 2.90625F;
			this.Label42.Width = 2.0625F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.19625F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 2.125F;
			this.Label43.MultiLine = false;
			this.Label43.Name = "Label43";
			this.Label43.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label43.Tag = "bold";
			this.Label43.Text = "Economic Obsolescence";
			this.Label43.Top = 2.90625F;
			this.Label43.Width = 1.875F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.19625F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 4F;
			this.Label44.MultiLine = false;
			this.Label44.Name = "Label44";
			this.Label44.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label44.Tag = "bold";
			this.Label44.Text = "Phys. %";
			this.Label44.Top = 2.90625F;
			this.Label44.Width = 0.6875F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.19625F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 4.6875F;
			this.Label45.MultiLine = false;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label45.Tag = "bold";
			this.Label45.Text = "Func. %";
			this.Label45.Top = 2.90625F;
			this.Label45.Width = 0.6875F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.19625F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 5.375F;
			this.Label46.MultiLine = false;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label46.Tag = "bold";
			this.Label46.Text = "Econ. %";
			this.Label46.Top = 2.90625F;
			this.Label46.Width = 0.75F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.19625F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 6.25F;
			this.Label47.MultiLine = false;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label47.Tag = "bold";
			this.Label47.Text = "Value(Rcnld)";
			this.Label47.Top = 2.90625F;
			this.Label47.Width = 1.1875F;
			// 
			// txtFobs
			// 
			this.txtFobs.CanGrow = false;
			this.txtFobs.Height = 0.19625F;
			this.txtFobs.Left = 0.0625F;
			this.txtFobs.Name = "txtFobs";
			this.txtFobs.Style = "font-family: \'Tahoma\'";
			this.txtFobs.Tag = "text";
			this.txtFobs.Text = "Field10";
			this.txtFobs.Top = 3.0625F;
			this.txtFobs.Width = 2.0625F;
			// 
			// txtEcObs
			// 
			this.txtEcObs.CanGrow = false;
			this.txtEcObs.Height = 0.19625F;
			this.txtEcObs.Left = 2.125F;
			this.txtEcObs.Name = "txtEcObs";
			this.txtEcObs.Style = "font-family: \'Tahoma\'";
			this.txtEcObs.Tag = "text";
			this.txtEcObs.Text = "Field11";
			this.txtEcObs.Top = 3.0625F;
			this.txtEcObs.Width = 1.875F;
			// 
			// txtPhys
			// 
			this.txtPhys.CanGrow = false;
			this.txtPhys.Height = 0.19625F;
			this.txtPhys.Left = 4F;
			this.txtPhys.Name = "txtPhys";
			this.txtPhys.Style = "font-family: \'Tahoma\'";
			this.txtPhys.Tag = "text";
			this.txtPhys.Text = "Field12";
			this.txtPhys.Top = 3.0625F;
			this.txtPhys.Width = 0.6875F;
			// 
			// txtFunc
			// 
			this.txtFunc.CanGrow = false;
			this.txtFunc.Height = 0.19625F;
			this.txtFunc.Left = 4.6875F;
			this.txtFunc.Name = "txtFunc";
			this.txtFunc.Style = "font-family: \'Tahoma\'";
			this.txtFunc.Tag = "text";
			this.txtFunc.Text = "Field13";
			this.txtFunc.Top = 3.0625F;
			this.txtFunc.Width = 0.6875F;
			// 
			// txtEco
			// 
			this.txtEco.CanGrow = false;
			this.txtEco.Height = 0.19625F;
			this.txtEco.Left = 5.375F;
			this.txtEco.Name = "txtEco";
			this.txtEco.Style = "font-family: \'Tahoma\'";
			this.txtEco.Tag = "text";
			this.txtEco.Text = "Field14";
			this.txtEco.Top = 3.0625F;
			this.txtEco.Width = 0.75F;
			// 
			// txtValue
			// 
			this.txtValue.CanGrow = false;
			this.txtValue.Height = 0.19625F;
			this.txtValue.Left = 6.25F;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtValue.Tag = "text";
			this.txtValue.Text = "Field15";
			this.txtValue.Top = 3.0625F;
			this.txtValue.Width = 1.1875F;
			// 
			// lblSfla
			// 
			this.lblSfla.Height = 0.19625F;
			this.lblSfla.HyperLink = null;
			this.lblSfla.Left = 2.9375F;
			this.lblSfla.Name = "lblSfla";
			this.lblSfla.Style = "font-family: \'Tahoma\'";
			this.lblSfla.Tag = "text";
			this.lblSfla.Text = "SFLA";
			this.lblSfla.Top = 2.1875F;
			this.lblSfla.Visible = false;
			this.lblSfla.Width = 1.125F;
			// 
			// txtSfla
			// 
			this.txtSfla.CanGrow = false;
			this.txtSfla.Height = 0.19625F;
			this.txtSfla.Left = 4.0625F;
			this.txtSfla.Name = "txtSfla";
			this.txtSfla.Style = "font-family: \'Tahoma\'";
			this.txtSfla.Tag = "text";
			this.txtSfla.Text = "Field9";
			this.txtSfla.Top = 2.1875F;
			this.txtSfla.Visible = false;
			this.txtSfla.Width = 1.4375F;
			// 
			// srptDwell
			//
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDwelling)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblopen3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblopen4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblO3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblO4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblO5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblsqft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFoundation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFinBsmt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeating)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRooms)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBedrooms)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBaths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAttic)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFireplaces)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnfinLiving)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOpen5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMasonry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoofCover)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBasement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCooling)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOpen5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFixtures)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHalfBaths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTrim)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRoof)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtO3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtO4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBsmt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFBsmt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeatCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtO5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlumbing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAtticCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFireplaceCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtInsulationCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnfinCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBuilt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRenovated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtKitchensDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBathsDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLayout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCondition)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFobs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEcObs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhys)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEco)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSfla)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSfla)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStyle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDwelling;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblopen3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblopen4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBase;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblO3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblO4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblO5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblsqft;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStyle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFoundation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFinBsmt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeating;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRooms;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBedrooms;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBaths;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAttic;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFireplaces;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnfinLiving;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMasonry;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoofCover;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBasement;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCooling;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOpen5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFixtures;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHalfBaths;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTrim;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRoof;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtO3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtO4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBsmt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFBsmt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHeatCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtO5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlumbing;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAtticCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFireplaceCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtInsulationCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnfinCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBuilt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRenovated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKitchensDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBathsDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLayout;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCondition;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFobs;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEcObs;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhys;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEco;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSfla;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSfla;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
