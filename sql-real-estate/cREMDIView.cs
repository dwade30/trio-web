﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;

namespace TWRE0000
{
	public class cREMDIView
	{
		//=========================================================
		private cRECommandHandler theCHandler;
		private cGenericCollection gcollCurrentMenu = new cGenericCollection();
		private cMenuController menuController = new cMenuController();
		private string strCurrentMenuName = "";
		private string strCurrentFormCaption = "";

		public delegate void MenuChangedEventHandler();

		public event MenuChangedEventHandler MenuChanged;

		public void Reset()
		{
			SetMenu("Main");
		}

		public string CurrentFormCaption
		{
			get
			{
				string CurrentFormCaption = "";
				CurrentFormCaption = strCurrentFormCaption;
				return CurrentFormCaption;
			}
		}

		public string CurrentMenuName
		{
			get
			{
				string CurrentMenuName = "";
				CurrentMenuName = strCurrentMenuName;
				return CurrentMenuName;
			}
		}

		public cGenericCollection CurrentMenu
		{
			get
			{
				cGenericCollection CurrentMenu = null;
				CurrentMenu = gcollCurrentMenu;
				return CurrentMenu;
			}
		}
		// Public Sub ExecuteMenuCommand(ByVal strMenu As String, ByVal lngCode As Long)
		// Call theCHandler.ExecuteMenuCommand(strMenu, lngCode)
		// End Sub
		public void ExecuteCommand(string strCommand)
		{
			theCHandler.ExecuteCommand(strCommand);
		}

		public cREMDIView() : base()
		{
			theCHandler = new cRECommandHandler();
		}

		private void thecHandler_MenuChanged(string strMenuName)
		{
			SetMenu(strMenuName);
		}

		private cGenericCollection GetMainMenu()
		{
			cGenericCollection GetMainMenu = null;
			int CurRow;
			string strTemp = "";
			int lngFCode;
			cGenericCollection gcollMenu = new cGenericCollection();
			//FC:FINAL:MSH - i.issue #1076: clear the menu to prevent multi adding menu items
			FCMainForm.InstancePtr.NavigationMenu.Clear();
			strCurrentFormCaption = "TRIO Software - Real Estate       [Main]";
			strCurrentMenuName = "Main";
			CurRow = 1;
			cMenuChoice tItem;
			gcollMenu.ClearList();
			if (!modREMain.Statics.boolShortRealEstate)
			{
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Form_GetAccountFull", 1, "Account Maintenance", modGlobalVariables.CNSTLONGSCREEN, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Form_GetAccountShort", 2, "Short Maintenance", modGlobalVariables.CNSTSHORTSCREEN, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Menu_CostFileUpdate", 3, "Cost File Update", modGlobalVariables.CNSTMNUCOSTUPDATE, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Menu_Printing", 4, "Printing", modGlobalVariables.CNSTPRINTROUTINES, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Menu_SaleRecords", 5, "Sale Records", modGlobalVariables.CNSTSALESRECORDS, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Menu_Compute", 6, "Compute", modGlobalVariables.CNSTMNUCALCULATE, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Menu_ImportExport", 7, "Import / Export", modGlobalVariables.CNSTIMPORTEXPORT, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Menu_File", 8, "File Maintenance", modGlobalVariables.CNSTFILEMAINTENANCE, "M", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Exit", 9, "Exit Real Estate", 0, "X", "", ref CurRow));
			}
			else
			{
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Form_GetAccountShort", 2, "Short Maintenance", modGlobalVariables.CNSTSHORTSCREEN, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Menu_CostFileUpdate", 3, "Cost File Update", modGlobalVariables.CNSTMNUCOSTUPDATE, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Menu_Printing", 4, "Printing", modGlobalVariables.CNSTPRINTROUTINES, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Menu_ImportExport", 7, "Import / Export", modGlobalVariables.CNSTIMPORTEXPORT, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Form_Exemptions", 0, "Calculate Exemptions", modGlobalVariables.CNSTCALCEXEMPTION, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_CalculateTreeGrowth", 0, "Calculate Tree Growth", modGlobalVariables.CNSTMNUCALCULATE, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Form_Audit", 0, "Audit of Billing Amounts", modGlobalVariables.CNSTAUDITBILLING, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Form_REPPAuditSummary", 0, "RE/PP Audit Summary", modGlobalVariables.CNSTAUDITBILLING, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Form_TaxRateCalculator", 0, "Tax Rate Calculator", modGlobalVariables.CNSTTAXRATECALCULATOR, "", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Menu_File", 8, "File Maintenance", modGlobalVariables.CNSTFILEMAINTENANCE, "M", "", ref CurRow));
				gcollMenu.AddItem(menuController.CreateMenuItem("RE_Exit", 9, "Exit Real Estate", 0, "X", "", ref CurRow));
			}
			GetMainMenu = gcollMenu;
			return GetMainMenu;
		}

		private void SetMenu(string strMenuName)
		{
			if (Strings.LCase(strMenuName) == "main")
			{
				gcollCurrentMenu = GetMainMenu();
			}
			if (this.MenuChanged != null)
				this.MenuChanged();
		}
	}
}
