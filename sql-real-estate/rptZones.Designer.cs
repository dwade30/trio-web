﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptZones.
	/// </summary>
	partial class rptZones
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptZones));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtZone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSecondary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtShort = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtShortSecondary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShort)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShortSecondary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtZone,
				this.txtDescription,
				this.txtSecondary,
				this.txtShort,
				this.txtShortSecondary
			});
			this.Detail.Height = 0.2395833F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPage,
				this.lblTitle,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5
			});
			this.PageHeader.Height = 0.7708333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.125F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.375F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.25F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 2F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitle.Text = "Zones";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 3.5625F;
			// 
			// txtMuni
			// 
			this.txtMuni.CanGrow = false;
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.Left = 0F;
			this.txtMuni.MultiLine = false;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "font-family: \'Tahoma\'";
			this.txtMuni.Text = "Field2";
			this.txtMuni.Top = 0.0625F;
			this.txtMuni.Width = 2.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = "Field2";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.375F;
			// 
			// txtDate
			// 
			this.txtDate.CanGrow = false;
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.125F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = "Field2";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 1.375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold; text-align: right";
			this.Label1.Text = "Zone";
			this.Label1.Top = 0.5625F;
			this.Label1.Width = 0.40625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.2083333F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.4375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: left";
			this.Label2.Text = "Description";
			this.Label2.Top = 0.5625F;
			this.Label2.Width = 2.177083F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.2083333F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.958333F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: left";
			this.Label3.Text = "Secondary Description";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 2.177083F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.2083333F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.666667F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold; text-align: left";
			this.Label4.Text = "Short Description";
			this.Label4.Top = 0.5625F;
			this.Label4.Width = 1.302083F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.2083333F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 6.1875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold; text-align: left";
			this.Label5.Text = "Secondary Short";
			this.Label5.Top = 0.5625F;
			this.Label5.Width = 1.25F;
			// 
			// txtZone
			// 
			this.txtZone.Height = 0.2083333F;
			this.txtZone.Left = 0F;
			this.txtZone.Name = "txtZone";
			this.txtZone.Style = "text-align: right";
			this.txtZone.Text = null;
			this.txtZone.Top = 0.02083333F;
			this.txtZone.Width = 0.40625F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.2083333F;
			this.txtDescription.Left = 0.4375F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0.02083333F;
			this.txtDescription.Width = 2.177083F;
			// 
			// txtSecondary
			// 
			this.txtSecondary.Height = 0.2083333F;
			this.txtSecondary.Left = 3.958333F;
			this.txtSecondary.Name = "txtSecondary";
			this.txtSecondary.Text = null;
			this.txtSecondary.Top = 0.02083333F;
			this.txtSecondary.Width = 2.177083F;
			// 
			// txtShort
			// 
			this.txtShort.Height = 0.2083333F;
			this.txtShort.Left = 2.666667F;
			this.txtShort.Name = "txtShort";
			this.txtShort.Text = null;
			this.txtShort.Top = 0.02083333F;
			this.txtShort.Width = 1.239583F;
			// 
			// txtShortSecondary
			// 
			this.txtShortSecondary.Height = 0.2083333F;
			this.txtShortSecondary.Left = 6.1875F;
			this.txtShortSecondary.Name = "txtShortSecondary";
			this.txtShortSecondary.Text = null;
			this.txtShortSecondary.Top = 0.02083333F;
			this.txtShortSecondary.Width = 1.239583F;
			// 
			// rptZones
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtZone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSecondary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShort)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShortSecondary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSecondary;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShort;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShortSecondary;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
