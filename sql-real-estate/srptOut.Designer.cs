﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptOut.
	/// </summary>
	partial class srptOut
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptOut));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGrade = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRCN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCond = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEcon = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFunc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPhy = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOutTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSFLA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPerSFLA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRCN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEcon)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOutTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSFLA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPerSFLA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDescription,
				this.txtYear,
				this.txtUnits,
				this.txtGrade,
				this.txtRCN,
				this.txtCond,
				this.txtValue,
				this.txtEcon,
				this.txtFunc,
				this.txtPhy
			});
			this.Detail.Height = 0.1458333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.CanGrow = false;
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Field1,
				this.Line1,
				this.Line2
			});
			this.ReportHeader.Height = 0.28125F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.CanGrow = false;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label13,
				this.txtOutTotal,
				this.txtSFLA,
				this.txtPerSFLA
			});
			this.ReportFooter.Height = 0.19625F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.19625F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.0625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'";
			this.Label1.Tag = "text";
			this.Label1.Text = "Description";
			this.Label1.Top = 0.125F;
			this.Label1.Width = 1.125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19625F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.5625F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'";
			this.Label2.Tag = "text";
			this.Label2.Text = "Year";
			this.Label2.Top = 0.125F;
			this.Label2.Width = 0.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19625F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'";
			this.Label3.Tag = "text";
			this.Label3.Text = "Units";
			this.Label3.Top = 0.125F;
			this.Label3.Width = 0.5625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19625F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.75F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'";
			this.Label4.Tag = "text";
			this.Label4.Text = "Grade";
			this.Label4.Top = 0.125F;
			this.Label4.Width = 0.5625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19625F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.5625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label5.Tag = "text";
			this.Label5.Text = "RCN";
			this.Label5.Top = 0.125F;
			this.Label5.Width = 0.625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19625F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 4.25F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'";
			this.Label6.Tag = "text";
			this.Label6.Text = "Cond";
			this.Label6.Top = 0.125F;
			this.Label6.Width = 0.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.19625F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 5.125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'";
			this.Label7.Tag = "text";
			this.Label7.Text = "Percent Good";
			this.Label7.Top = 0F;
			this.Label7.Width = 1.0625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19625F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.875F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'";
			this.Label8.Tag = "text";
			this.Label8.Text = "Phy";
			this.Label8.Top = 0.125F;
			this.Label8.Width = 0.375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19625F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 5.4375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'";
			this.Label9.Tag = "text";
			this.Label9.Text = "Func";
			this.Label9.Top = 0.125F;
			this.Label9.Width = 0.4375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19625F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 6F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'";
			this.Label10.Tag = "text";
			this.Label10.Text = "Econ";
			this.Label10.Top = 0.125F;
			this.Label10.Width = 0.4375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19625F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'";
			this.Label11.Tag = "text";
			this.Label11.Text = "Rcnld";
			this.Label11.Top = 0.125F;
			this.Label11.Width = 0.5625F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19625F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 6.875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'";
			this.Label12.Tag = "text";
			this.Label12.Text = "Value";
			this.Label12.Top = 0F;
			this.Label12.Width = 0.5625F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.19F;
			this.Field1.Left = 0.75F;
			this.Field1.MultiLine = false;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; vertical-align: top" + "";
			this.Field1.Tag = "bold";
			this.Field1.Text = "Outbuildings/Additions/Improvements";
			this.Field1.Top = 0F;
			this.Field1.Width = 2.916667F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.09375F;
			this.Line1.Width = 0.5833333F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 0.6458333F;
			this.Line1.Y1 = 0.09375F;
			this.Line1.Y2 = 0.09375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.770833F;
			this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
			this.Line2.LineWeight = 3F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.09375F;
			this.Line2.Width = 0.6666667F;
			this.Line2.X1 = 3.770833F;
			this.Line2.X2 = 4.4375F;
			this.Line2.Y1 = 0.09375F;
			this.Line2.Y2 = 0.09375F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.19625F;
			this.txtDescription.Left = 0.0625F;
			this.txtDescription.MultiLine = false;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'";
			this.txtDescription.Tag = "text";
			this.txtDescription.Text = "Field1";
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 1.4375F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.19625F;
			this.txtYear.Left = 1.5625F;
			this.txtYear.MultiLine = false;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Tahoma\'";
			this.txtYear.Tag = "text";
			this.txtYear.Text = "Field2";
			this.txtYear.Top = 0F;
			this.txtYear.Width = 0.5F;
			// 
			// txtUnits
			// 
			this.txtUnits.CanShrink = true;
			this.txtUnits.Height = 0.19625F;
			this.txtUnits.Left = 2.125F;
			this.txtUnits.MultiLine = false;
			this.txtUnits.Name = "txtUnits";
			this.txtUnits.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtUnits.Tag = "text";
			this.txtUnits.Text = null;
			this.txtUnits.Top = 0F;
			this.txtUnits.Width = 0.5625F;
			// 
			// txtGrade
			// 
			this.txtGrade.Height = 0.19625F;
			this.txtGrade.Left = 2.75F;
			this.txtGrade.MultiLine = false;
			this.txtGrade.Name = "txtGrade";
			this.txtGrade.Style = "font-family: \'Tahoma\'";
			this.txtGrade.Tag = "text";
			this.txtGrade.Text = null;
			this.txtGrade.Top = 0F;
			this.txtGrade.Width = 0.625F;
			// 
			// txtRCN
			// 
			this.txtRCN.Height = 0.19625F;
			this.txtRCN.Left = 3.4375F;
			this.txtRCN.MultiLine = false;
			this.txtRCN.Name = "txtRCN";
			this.txtRCN.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtRCN.Tag = "text";
			this.txtRCN.Text = "Field5";
			this.txtRCN.Top = 0F;
			this.txtRCN.Width = 0.75F;
			// 
			// txtCond
			// 
			this.txtCond.Height = 0.19625F;
			this.txtCond.Left = 4.25F;
			this.txtCond.MultiLine = false;
			this.txtCond.Name = "txtCond";
			this.txtCond.Style = "font-family: \'Tahoma\'";
			this.txtCond.Tag = "text";
			this.txtCond.Text = "Field6";
			this.txtCond.Top = 0F;
			this.txtCond.Width = 0.5625F;
			// 
			// txtValue
			// 
			this.txtValue.CanGrow = false;
			this.txtValue.Height = 0.19625F;
			this.txtValue.Left = 6.5625F;
			this.txtValue.MultiLine = false;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtValue.Tag = "text";
			this.txtValue.Text = "Field7";
			this.txtValue.Top = 0F;
			this.txtValue.Width = 0.875F;
			// 
			// txtEcon
			// 
			this.txtEcon.Height = 0.19625F;
			this.txtEcon.Left = 6F;
			this.txtEcon.MultiLine = false;
			this.txtEcon.Name = "txtEcon";
			this.txtEcon.Style = "font-family: \'Tahoma\'";
			this.txtEcon.Tag = "text";
			this.txtEcon.Text = "Field8";
			this.txtEcon.Top = 0F;
			this.txtEcon.Width = 0.5F;
			// 
			// txtFunc
			// 
			this.txtFunc.Height = 0.19625F;
			this.txtFunc.Left = 5.4375F;
			this.txtFunc.MultiLine = false;
			this.txtFunc.Name = "txtFunc";
			this.txtFunc.Style = "font-family: \'Tahoma\'";
			this.txtFunc.Tag = "text";
			this.txtFunc.Text = "Field9";
			this.txtFunc.Top = 0F;
			this.txtFunc.Width = 0.5F;
			// 
			// txtPhy
			// 
			this.txtPhy.Height = 0.19625F;
			this.txtPhy.Left = 4.875F;
			this.txtPhy.MultiLine = false;
			this.txtPhy.Name = "txtPhy";
			this.txtPhy.Style = "font-family: \'Tahoma\'";
			this.txtPhy.Tag = "text";
			this.txtPhy.Text = "Field10";
			this.txtPhy.Top = 0F;
			this.txtPhy.Width = 0.5F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19625F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4.75F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Label13.Tag = "text";
			this.Label13.Text = "Outbuilding Total";
			this.Label13.Top = 0F;
			this.Label13.Width = 1.5F;
			// 
			// txtOutTotal
			// 
			this.txtOutTotal.Height = 0.19625F;
			this.txtOutTotal.Left = 6.3125F;
			this.txtOutTotal.MultiLine = false;
			this.txtOutTotal.Name = "txtOutTotal";
			this.txtOutTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtOutTotal.Tag = "text";
			this.txtOutTotal.Text = "Field1";
			this.txtOutTotal.Top = 0F;
			this.txtOutTotal.Width = 1.125F;
			// 
			// txtSFLA
			// 
			this.txtSFLA.Height = 0.19625F;
			this.txtSFLA.Left = 0.0625F;
			this.txtSFLA.MultiLine = false;
			this.txtSFLA.Name = "txtSFLA";
			this.txtSFLA.Style = "font-family: \'Tahoma\'";
			this.txtSFLA.Tag = "text";
			this.txtSFLA.Text = null;
			this.txtSFLA.Top = 0F;
			this.txtSFLA.Width = 1.5F;
			// 
			// txtPerSFLA
			// 
			this.txtPerSFLA.Height = 0.19625F;
			this.txtPerSFLA.Left = 2F;
			this.txtPerSFLA.MultiLine = false;
			this.txtPerSFLA.Name = "txtPerSFLA";
			this.txtPerSFLA.Style = "font-family: \'Tahoma\'";
			this.txtPerSFLA.Tag = "text";
			this.txtPerSFLA.Text = null;
			this.txtPerSFLA.Top = 0F;
			this.txtPerSFLA.Width = 2F;
			// 
			// srptOut
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGrade)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRCN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCond)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEcon)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFunc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOutTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSFLA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPerSFLA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGrade;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRCN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCond;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEcon;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFunc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPhy;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOutTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSFLA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPerSFLA;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
