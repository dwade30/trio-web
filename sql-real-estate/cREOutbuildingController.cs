﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWRE0000
{
	public class cREOutbuildingController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cREOutbuilding GetOutbuildingByAccountCard(int lngAccount, short intCard)
		{
			cREOutbuilding GetOutbuildingByAccountCard = null;
			ClearErrors();
			try
			{
				cREOutbuilding outB = new cREOutbuilding();
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset("Select * from outbuilding where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = " + FCConvert.ToString(intCard), "RealEstate");
				FillOutbuilding(ref outB, ref rs);
				outB.AccountNumber = lngAccount;
				outB.CardNumber = intCard;
				outB.IsUpdated = false;
				GetOutbuildingByAccountCard = outB;
				return GetOutbuildingByAccountCard;
			}
			catch (Exception ex)
			{
				//ErrorHandler: ;
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
				return GetOutbuildingByAccountCard;
			}
		}

		private void SetError(int lngError, string strError)
		{
			strLastError = strError;
			lngLastError = lngError;
		}

		private void FillOutbuilding(ref cREOutbuilding outB, ref clsDRWrapper rs)
		{
			try
			{
				// On Error GoTo ErrorHandler
				short x;
				if (!(outB == null))
				{
					if (!(rs == null))
					{
						cREOutbuildingItem outItem;
						outB.OutBuildings.ClearList();
						if (!rs.EndOfFile())
						{
							for (x = 1; x <= 10; x++)
							{
								outItem = new cREOutbuildingItem();
								outItem.BuildingNumber = x;
								// TODO Get_Fields: Field [oitype] not found!! (maybe it is an alias?)
								outItem.BuildingType = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("oitype" + FCConvert.ToString(x)))));
								// TODO Get_Fields: Field [oicond] not found!! (maybe it is an alias?)
								outItem.Condition = FCConvert.ToInt16(Math.Round(Conversion.Val(rs.Get_Fields("oicond" + FCConvert.ToString(x)))));
								// TODO Get_Fields: Field [oigradecd] not found!! (maybe it is an alias?)
								outItem.GradeCode = FCConvert.ToInt16(Math.Round(Conversion.Val(rs.Get_Fields("oigradecd" + FCConvert.ToString(x)))));
								// TODO Get_Fields: Field [oigradepct] not found!! (maybe it is an alias?)
								outItem.GradePercent = FCConvert.ToInt16(Math.Round(Conversion.Val(rs.Get_Fields("oigradepct" + FCConvert.ToString(x)))));
								// TODO Get_Fields: Field [oipctfunct] not found!! (maybe it is an alias?)
								outItem.PercentFunctional = FCConvert.ToInt16(Math.Round(Conversion.Val(rs.Get_Fields("oipctfunct" + FCConvert.ToString(x)))));
								// TODO Get_Fields: Field [oipctphys] not found!! (maybe it is an alias?)
								outItem.PercentPhysical = FCConvert.ToInt16(Math.Round(Conversion.Val(rs.Get_Fields("oipctphys" + FCConvert.ToString(x)))));
								// TODO Get_Fields: Field [oisoundvalue] not found!! (maybe it is an alias?)
								outItem.SoundValue = Conversion.Val(rs.Get_Fields("oisoundvalue" + FCConvert.ToString(x)));
								// TODO Get_Fields: Field [oiunits] not found!! (maybe it is an alias?)
								outItem.Units = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("oiunits" + FCConvert.ToString(x)))));
								// TODO Get_Fields: Field [oiyear] not found!! (maybe it is an alias?)
								outItem.YearBuilt = FCConvert.ToInt16(Math.Round(Conversion.Val(rs.Get_Fields("oiyear" + FCConvert.ToString(x)))));
								outItem.IsUpdated = false;
							}
							// x
							outB.IsUpdated = false;
						}
						else
						{
							for (x = 1; x <= 10; x++)
							{
								outItem = new cREOutbuildingItem();
								outItem.BuildingNumber = x;
								outB.OutBuildings.AddItem(outItem);
							}
							// x
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}
	}
}
