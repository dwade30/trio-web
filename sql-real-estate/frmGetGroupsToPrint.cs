﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetGroupsToPrint.
	/// </summary>
	public partial class frmGetGroupsToPrint : BaseForm
	{
		public frmGetGroupsToPrint()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetGroupsToPrint InstancePtr
		{
			get
			{
				return (frmGetGroupsToPrint)Sys.GetInstance(typeof(frmGetGroupsToPrint));
			}
		}

		protected frmGetGroupsToPrint _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// Dim dbTemp As DAO.Database
		clsDRWrapper rsTemp = new clsDRWrapper();
		clsDRWrapper rsReport = new clsDRWrapper();
		bool boolLeave;
		int lngID;
		bool boolDontAdd;
		bool boolSavedOne;

		private void Command1_Click(object sender, System.EventArgs e)
		{
			// CancelledIt = False
			this.Unload();
		}

		public void Command1_Click()
		{
			Command1_Click(Command1, new System.EventArgs());
		}

		private void frmGetGroupsToPrint_Activated(object sender, System.EventArgs e)
		{
			if (boolLeave)
			{
				this.Unload();
			}
			else
			{
				frmGetGroupsToPrint.InstancePtr.Visible = true;
			}
		}

		private void frmGetGroupsToPrint_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						mnuQuit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGetGroupsToPrint_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetGroupsToPrint properties;
			//frmGetGroupsToPrint.ScaleWidth	= 3960;
			//frmGetGroupsToPrint.ScaleHeight	= 3525;
			//frmGetGroupsToPrint.LinkTopic	= "Form1";
			//End Unmaped Properties
			string strGroup = "";
			int intGroup = 0;
			boolSavedOne = false;
			boolLeave = false;
			lngID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			//FC:FINAL:MSH - i.issue #1436: the form won't be displayed if it invisible
			//frmGetGroupsToPrint.InstancePtr.Visible = false;
			rsTemp.Execute("delete from LABELACCOUNTS  where userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
			rsTemp.OpenRecordset("select * from extract where reportnumber > 0 and userid = " + FCConvert.ToString(lngID) + " order by groupnumber", modGlobalVariables.strREDatabase);
			modGlobalVariables.Statics.CancelledIt = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			if (rsTemp.EndOfFile())
			{
				MessageBox.Show("The extract is empty.  You must perform an extract first.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				modGlobalVariables.Statics.CancelledIt = true;
				return;
			}
			while (!rsTemp.EndOfFile())
			{
				rsReport.OpenRecordset("select * from reporttitles where reportnumber = " + rsTemp.Get_Fields_Int32("reportnumber"), modGlobalVariables.strREDatabase);
				strGroup = "  ";
				fecherFoundation.Strings.MidSet(ref strGroup, 1, 2, FCConvert.ToString(rsTemp.Get_Fields_Int32("groupnumber")));
				List1.AddItem(strGroup + "  " + Strings.Trim(rsReport.Get_Fields_String("Title") + ""));
				rsTemp.MoveNext();
			}
			boolDontAdd = false;
			if (List1.Items.Count == 1)
			{
				boolDontAdd = true;
				boolSavedOne = true;
				List1.SelectedIndex = 0;
				// Call List1_Click
				intGroup = Strings.InStr(1, Strings.Trim(strGroup), " ", CompareConstants.vbTextCompare);
				if (intGroup > 0)
				{
					intGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strGroup, 1, intGroup - 1))));
				}
				else
				{
					intGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(strGroup)));
				}
				rsTemp.Execute("insert into labelaccounts (accountNUMBER,userid) values (" + FCConvert.ToString(intGroup) + "," + FCConvert.ToString(lngID) + ")", modGlobalVariables.strREDatabase);
				boolLeave = true;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (!boolSavedOne)
			{
				modGlobalVariables.Statics.CancelledIt = true;
			}
		}

		private void List1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int intGroup;
			string strGroup;
			clsDRWrapper clsTemp = new clsDRWrapper();
			//FC:TEMP:RPU:#i1241 - Check if SelectedIndexndex !=-1
			if (List1.SelectedIndex != -1)
			{
				strGroup = List1.List(List1.ListIndex);
				intGroup = Strings.InStr(1, Strings.Trim(strGroup), " ", CompareConstants.vbTextCompare);
				if (intGroup > 0)
				{
					intGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(List1.Items[List1.SelectedIndex].Text, 1, intGroup))));
				}
				else
				{
					intGroup = FCConvert.ToInt32(Math.Round(Conversion.Val(List1.Items[List1.SelectedIndex].Text)));
				}
				// intGroup = Val(List1.List(List1.listindex))
				// strgroup = List1.List(List1.listindex)
				if (!boolDontAdd)
				{
					boolSavedOne = true;
					clsTemp.Execute("insert into labelaccounts (accountNUMBER,userid) values (" + FCConvert.ToString(intGroup) + "," + FCConvert.ToString(lngID) + ")", modGlobalVariables.strREDatabase);
					MessageBox.Show("Group  " + strGroup + " saved for printing/reporting.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			if (!boolSavedOne)
			{
				MessageBox.Show("You have not chosen a group", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			Command1_Click();
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			this.Unload();
		}

		public void mnuQuit_Click()
		{
			//mnuQuit_Click(mnuQuit, new System.EventArgs());
		}
	}
}
