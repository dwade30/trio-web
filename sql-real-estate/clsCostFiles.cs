﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class clsCostFiles
	{
		//=========================================================
		// Written by:
		// Corey Gray
		// 10/31/2005
		//
		// This class will load the cost files and provides an interface
		// for getting the data from the cost files
		//FC:FINAL:RPU:#i1606 -  Auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper clsDwellCost = new clsDRWrapper();
		private clsDRWrapper clsDwellCost_AutoInitialized;

		private clsDRWrapper clsDwellCost
		{
			get
			{
				if (clsDwellCost_AutoInitialized == null)
				{
					clsDwellCost_AutoInitialized = new clsDRWrapper();
				}
				return clsDwellCost_AutoInitialized;
			}
			set
			{
				clsDwellCost_AutoInitialized = value;
			}
		}
		// Private clsMiscCost As New clsDRWrapper
		//private clsDRWrapper clsPropertyCost = new clsDRWrapper();
		private clsDRWrapper clsPropertyCost_AutoInitialized;

		private clsDRWrapper clsPropertyCost
		{
			get
			{
				if (clsPropertyCost_AutoInitialized == null)
				{
					clsPropertyCost_AutoInitialized = new clsDRWrapper();
				}
				return clsPropertyCost_AutoInitialized;
			}
			set
			{
				clsPropertyCost_AutoInitialized = value;
			}
		}
		//private clsDRWrapper clsCommercialCost = new clsDRWrapper();
		private clsDRWrapper clsCommercialCost_AutoInitialized;

		private clsDRWrapper clsCommercialCost
		{
			get
			{
				if (clsCommercialCost_AutoInitialized == null)
				{
					clsCommercialCost_AutoInitialized = new clsDRWrapper();
				}
				return clsCommercialCost_AutoInitialized;
			}
			set
			{
				clsCommercialCost_AutoInitialized = value;
			}
		}
		//private clsDRWrapper clsOutSFFactor = new clsDRWrapper();
		private clsDRWrapper clsOutSFFactor_AutoInitialized;

		private clsDRWrapper clsOutSFFactor
		{
			get
			{
				if (clsOutSFFactor_AutoInitialized == null)
				{
					clsOutSFFactor_AutoInitialized = new clsDRWrapper();
				}
				return clsOutSFFactor_AutoInitialized;
			}
			set
			{
				clsOutSFFactor_AutoInitialized = value;
			}
		}

		private clsCostRecord[] MiscArray = null;
		private int lngMiscOffset;

		public void InitCostFiles()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// vbPorter upgrade warning: lngIndex As int	OnWriteFCConvert.ToDouble(
			int lngIndex;
			try
			{
				// On Error GoTo ErrorHandler
				// Call clsDwellCost.OpenRecordset("select * from (select id,code,description,shortdescription,factor as unit,0 as base,'STYLE' as type,townnumber from DwellingStyle) union all (select id,code,description,shortdescription,unit,base,'HEAT' as type,townnumber from dwellingheat) union all (select id,code,description,shortdescription,factor as unit,0 as base,'EXTERIOR' as type,townnumber from DwellingExterior) union all (select id,code,description,shortdescription,factor as unit,0 as base,'ECONOMIC' as type,townnumber from EconomicCode) order by type,CODE,townnumber", strRECostFileDatabase)
				clsDwellCost.OpenRecordset("(select id,code,description,shortdescription,factor as unit,0 as base,'STYLE' as type,townnumber from DwellingStyle) union all (select id,code,description,shortdescription,unit,base,'HEAT' as type,townnumber from dwellingheat) union all (select id,code,description,shortdescription,factor as unit,0 as base,'EXTERIOR' as type,townnumber from DwellingExterior) union all (select id,code,description,shortdescription,factor as unit,0 as base,'ECONOMIC' as type,townnumber from EconomicCode) order by type,CODE,townnumber", modGlobalVariables.Statics.strRECostFileDatabase);
				// Call clsPropertyCost.OpenRecordset("select * from (select id,code,description,shortdescription,0 as unit,0 as base,'NEIGHBORHOOD' as type,townnumber from neighborhood) union all (select id,code,description,shortdescription,factor as unit,0 as base,'LANDTYPE' as type,0 as townnumber from landtype) union all (select id,code,description,shortdescription,0 as unit,0 as base,'ZONE' as type,townnumber from zones) union all (select id,code,secondarydescription as description,secondaryshort as shortdescription,0 as unit,0 as base,'SECZONE' as type,townnumber from zones) order by type,CODE,townnumber", strRECostFileDatabase)
				clsPropertyCost.OpenRecordset(" (select id,code,description,shortdescription,0 as unit,0 as base,'NEIGHBORHOOD' as type,townnumber from neighborhood) union all (select id,code,description,shortdescription,factor as unit,0 as base,'LANDTYPE' as type,0 as townnumber from landtype) union all (select id,code,description,shortdescription,0 as unit,0 as base,'ZONE' as type,townnumber from zones) union all (select id,code,secondarydescription as description,secondaryshort as shortdescription,0 as unit,0 as base,'SECZONE' as type,townnumber from zones) order by type,CODE,townnumber", modGlobalVariables.Statics.strRECostFileDatabase);
				// Call clsMiscCost.OpenRecordset("select * from costrecord order by crecordnumber,townnumber", strRECostFileDatabase)
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					clsLoad.OpenRecordset("select max(townnumber) as maxtown,max(crecordnumber) as maxrecord from costrecord", modGlobalVariables.Statics.strRECostFileDatabase);
					// TODO Get_Fields: Field [maxrecord] not found!! (maybe it is an alias?)
					lngMiscOffset = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("maxrecord"))));
					// TODO Get_Fields: Field [maxtown] not found!! (maybe it is an alias?)
					if (Conversion.Val(clsLoad.Get_Fields("maxtown")) > 0)
					{
						// TODO Get_Fields: Field [maxtown] not found!! (maybe it is an alias?)
						MiscArray = new clsCostRecord[FCConvert.ToInt32(lngMiscOffset * Conversion.Val(clsLoad.Get_Fields("maxtown"))) + 1];
					}
					else
					{
						MiscArray = new clsCostRecord[(lngMiscOffset) * 1 + 1];
						lngMiscOffset = 0;
					}
				}
				else
				{
					clsLoad.OpenRecordset("select max(crecordnumber) as maxrecord from costrecord", modGlobalVariables.Statics.strRECostFileDatabase);
					// TODO Get_Fields: Field [maxrecord] not found!! (maybe it is an alias?)
					MiscArray = new clsCostRecord[FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("maxrecord"))) + 1];
					lngMiscOffset = 0;
				}
				clsLoad.OpenRecordset("select * from costrecord order by crecordnumber,townnumber", modGlobalVariables.Statics.strRECostFileDatabase);
				while (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					lngIndex = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("crecordnumber") + ((Conversion.Val(clsLoad.Get_Fields("townnumber")) - 1) * lngMiscOffset));
					MiscArray[lngIndex] = new clsCostRecord();
					MiscArray[lngIndex].CBase = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("cbase")));
					MiscArray[lngIndex].ClDesc = FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"));
					MiscArray[lngIndex].CSDesc = FCConvert.ToString(clsLoad.Get_Fields_String("csdesc"));
					MiscArray[lngIndex].CUnit = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("cunit")));
					// TODO Get_Fields: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					MiscArray[lngIndex].TownCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("townnumber"))));
					clsLoad.MoveNext();
				}
				clsCommercialCost.OpenRecordset("select * from commercialcost order by crecordnumber,townnumber", modGlobalVariables.Statics.strRECostFileDatabase);
				clsOutSFFactor.OpenRecordset("select * from outbuildingsqftfactors order by code,townnumber", modGlobalVariables.Statics.strRECostFileDatabase);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In InitCostFiles", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public double GetValue(ref int recnum, string strCat, string strType, int lngTownNumber = 0)
		{
			double GetValue = 0;
			try
			{
				// On Error GoTo ErrorHandler
				int lngTownCode = 0;
				double dblReturn = 0;
				GetValue = 0;
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					if (lngTownNumber == 0)
					{
						lngTownCode = 1;
						if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber > 0)
						{
							lngTownCode = modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
						}
					}
					else
					{
						lngTownCode = lngTownNumber;
					}
				}
				else
				{
					lngTownCode = 0;
				}
				if (Strings.UCase(strCat) == "OUTBUILDING")
				{
					if (Strings.UCase(strType) == "SQFTFACTOR")
					{
						if (modGlobalVariables.Statics.boolUseArrays)
						{
							// If clsOutSFFactor.FindFirstRecord(, , "val(townnumber & '') = " & lngTownCode & " and code = " & recnum) Then
							// If clsOutSFFactor.FindFirstRecord2("townnumber,code", lngTownCode & "," & recnum, ",") Then
							if (clsOutSFFactor.FindFirst("isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode) + " and code = " + FCConvert.ToString(recnum)))
							{
								dblReturn = Conversion.Val(clsOutSFFactor.Get_Fields_Double("factor"));
							}
							else
							{
								dblReturn = 0;
							}
						}
						else
						{
							clsDRWrapper clsLoad = new clsDRWrapper();
							clsLoad.OpenRecordset("select * from outbuildingsqftfactors where code = " + FCConvert.ToString(recnum) + " and isnull(townnumber,0)  = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strRECostFileDatabase);
							if (!clsLoad.EndOfFile())
							{
								dblReturn = Conversion.Val(clsLoad.Get_Fields_Double("factor"));
							}
							else
							{
								dblReturn = 0;
							}
						}
					}
				}
				GetValue = dblReturn;
				return GetValue;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetValue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetValue;
		}
		// vbPorter upgrade warning: recnum As short	OnWrite(int, double)
		// vbPorter upgrade warning: lngTownNumber As int	OnWrite(int, double)
		public bool Get_Cost_Record(int recnum, string strCat = "", string strType = "", int lngTownNumber = 0)
		{
			bool Get_Cost_Record = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngTownCode = 0;
				int lngIndex = 0;
				Get_Cost_Record = true;
				if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
				{
					if (lngTownNumber == 0)
					{
						lngTownCode = 1;
						// default to the first town
						if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber > 0)
						{
							lngTownCode = modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
						}
					}
					else
					{
						lngTownCode = lngTownNumber;
					}
				}
				else
				{
					lngTownCode = 0;
				}
				if (strType == "")
				{
					// misc cost files
					if (modGlobalVariables.Statics.boolUseArrays)
					{
						if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
						{
							lngIndex = recnum + ((lngTownCode - 1) * lngMiscOffset);
						}
						else
						{
							lngIndex = recnum;
						}
						modGlobalVariables.Statics.CostRec.CBase = MiscArray[lngIndex].CBase;
						modGlobalVariables.Statics.CostRec.ClDesc = MiscArray[lngIndex].ClDesc;
						modGlobalVariables.Statics.CostRec.CSDesc = MiscArray[lngIndex].CSDesc;
						modGlobalVariables.Statics.CostRec.CUnit = MiscArray[lngIndex].CUnit;
						modGlobalVariables.Statics.CostRec.TownCode = lngTownCode;
						return Get_Cost_Record;
						// If clsMiscCost.FindFirstRecord(, , "Crecordnumber = " & recnum & " and townnumber = " & lngTownCode) Then
						// CostRec.CBase = clsMiscCost.Fields("cbase")
						// CostRec.ClDesc = clsMiscCost.Fields("cldesc")
						// CostRec.CSDesc = clsMiscCost.Fields("csdesc")
						// CostRec.CUnit = clsMiscCost.Fields("cunit")
						// Else
						// CostRec.CBase = 0
						// CostRec.ClDesc = ""
						// CostRec.CSDesc = ""
						// CostRec.CUnit = 0
						// Get_Cost_Record = False
						// Exit Function
						// End If
					}
					else
					{
						clsDRWrapper clsTemp = new clsDRWrapper();
						clsTemp.OpenRecordset("select * from costrecord where crecordnumber = " + FCConvert.ToString(recnum) + " and townnumber = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strRECostFileDatabase);
						if (!clsTemp.EndOfFile())
						{
							modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(clsTemp.Get_Fields_String("CBase"));
							modGlobalVariables.Statics.CostRec.ClDesc = FCConvert.ToString(clsTemp.Get_Fields_String("ClDesc"));
							modGlobalVariables.Statics.CostRec.CSDesc = FCConvert.ToString(clsTemp.Get_Fields_String("CSDesc"));
							modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(clsTemp.Get_Fields_String("CUnit"));
							return Get_Cost_Record;
						}
						else
						{
							modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(0);
							modGlobalVariables.Statics.CostRec.ClDesc = "";
							modGlobalVariables.Statics.CostRec.CSDesc = "";
							modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(0);
							Get_Cost_Record = false;
							return Get_Cost_Record;
						}
					}
				}
				else
				{
					if (modGlobalVariables.Statics.boolUseArrays)
					{
						if (Strings.UCase(strCat) == "DWELLING")
						{
							// check dwelling cost recordset for type,recnum
							// If clsDwellCost.FindFirstRecord(, , "code = " & recnum & " and type = '" & strType & "' and val(townnumber & '') = " & lngTownCode) Then
							// If clsDwellCost.FindFirstRecord2("Code,Type,townnumber", recnum & "," & strType & "," & lngTownCode, ",") Then
							if (clsDwellCost.FindFirst("code = " + FCConvert.ToString(recnum) + " and type = '" + strType + "' and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode)))
							{
								// TODO Get_Fields: Check the table for the column [base] and replace with corresponding Get_Field method
								modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(Conversion.Val(clsDwellCost.Get_Fields("base")));
								modGlobalVariables.Statics.CostRec.ClDesc = FCConvert.ToString(clsDwellCost.Get_Fields_String("description"));
								modGlobalVariables.Statics.CostRec.CSDesc = FCConvert.ToString(clsDwellCost.Get_Fields_String("shortdescription"));
								// TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
								modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(Conversion.Val(clsDwellCost.Get_Fields("unit")));
							}
							else
							{
								modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(99999999);
								modGlobalVariables.Statics.CostRec.ClDesc = "";
								modGlobalVariables.Statics.CostRec.CSDesc = "";
								modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(99999999);
								Get_Cost_Record = false;
							}
						}
						else if (Strings.UCase(strCat) == "PROPERTY")
						{
							if (Strings.UCase(strType) != "LANDTYPE")
							{
								// If clsPropertyCost.FindFirstRecord2("Code,Type,townnumber", recnum & "," & strType & "," & lngTownCode, ",") Then
								if (clsPropertyCost.FindFirst("Code = " + FCConvert.ToString(recnum) + " and type = '" + strType + "' and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode)))
								{
									// TODO Get_Fields: Check the table for the column [base] and replace with corresponding Get_Field method
									modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(Conversion.Val(clsPropertyCost.Get_Fields("base")));
									modGlobalVariables.Statics.CostRec.ClDesc = FCConvert.ToString(clsPropertyCost.Get_Fields_String("description"));
									modGlobalVariables.Statics.CostRec.CSDesc = FCConvert.ToString(clsPropertyCost.Get_Fields_String("shortdescription"));
									// TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
									modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(Conversion.Val(clsPropertyCost.Get_Fields("unit")));
								}
								else
								{
									modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(99999999);
									modGlobalVariables.Statics.CostRec.ClDesc = "";
									modGlobalVariables.Statics.CostRec.CSDesc = "";
									modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(99999999);
									Get_Cost_Record = false;
								}
							}
							else
							{
								// If clsPropertyCost.FindFirstRecord(, , "code = " & recnum & " and type = '" & strType & "'") Then
								// If clsPropertyCost.FindFirstRecord2("Code,Type", recnum & "," & strType, ",") Then
								if (clsPropertyCost.FindFirst("Code = " + FCConvert.ToString(recnum) + " and type = '" + strType + "'"))
								{
									// TODO Get_Fields: Check the table for the column [base] and replace with corresponding Get_Field method
									modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(Conversion.Val(clsPropertyCost.Get_Fields("base")));
									modGlobalVariables.Statics.CostRec.ClDesc = FCConvert.ToString(clsPropertyCost.Get_Fields_String("description"));
									modGlobalVariables.Statics.CostRec.CSDesc = FCConvert.ToString(clsPropertyCost.Get_Fields_String("shortdescription"));
									// TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
									modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(Conversion.Val(clsPropertyCost.Get_Fields("unit")));
								}
								else
								{
									modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(99999999);
									modGlobalVariables.Statics.CostRec.ClDesc = "";
									modGlobalVariables.Statics.CostRec.CSDesc = "";
									modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(99999999);
									Get_Cost_Record = false;
								}
							}
						}
						else if (Strings.UCase(strCat) == "COMMERCIAL")
						{
							// If clsCommercialCost.FindFirstRecord(, , "crecordnumber = " & recnum & " and val(townnumber & '') = " & lngTownCode) Then
							// If clsCommercialCost.FindFirstRecord2("crecordnumber,townnumber", recnum & "," & lngTownCode, ",") Then
							if (clsCommercialCost.FindFirst("crecordnumber = " + FCConvert.ToString(recnum) + " and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode)))
							{
								modGlobalVariables.Statics.ComCostRec.CBase = FCConvert.ToString(Conversion.Val(clsCommercialCost.Get_Fields_String("cbase")));
								modGlobalVariables.Statics.ComCostRec.ClDesc = FCConvert.ToString(clsCommercialCost.Get_Fields_String("cldesc"));
								modGlobalVariables.Statics.ComCostRec.CSDesc = FCConvert.ToString(clsCommercialCost.Get_Fields_String("csdesc"));
								modGlobalVariables.Statics.ComCostRec.CUnit = FCConvert.ToString(Conversion.Val(clsCommercialCost.Get_Fields_String("cunit")));
								return Get_Cost_Record;
							}
							else
							{
								modGlobalVariables.Statics.ComCostRec.CBase = FCConvert.ToString(99999999);
								modGlobalVariables.Statics.ComCostRec.ClDesc = "";
								modGlobalVariables.Statics.ComCostRec.CSDesc = "";
								modGlobalVariables.Statics.ComCostRec.CUnit = FCConvert.ToString(99999999);
								Get_Cost_Record = false;
								return Get_Cost_Record;
							}
						}
					}
					else
					{
						clsDRWrapper clsLoad = new clsDRWrapper();
						if (Strings.UCase(strCat) == "DWELLING")
						{
							if (Strings.UCase(strType) == "STYLE")
							{
								clsLoad.OpenRecordset("select code,factor as unit,description,shortdescription, 0 as base from dwellingstyle where code = " + FCConvert.ToString(recnum) + " and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strRECostFileDatabase);
							}
							else if (Strings.UCase(strType) == "HEAT")
							{
								clsLoad.OpenRecordset("select code,unit,base,description,shortdescription from dwellingheat where code = " + FCConvert.ToString(recnum) + " and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strRECostFileDatabase);
							}
							else if (Strings.UCase(strType) == "EXTERIOR")
							{
								clsLoad.OpenRecordset("select code,factor as unit,description,shortdescription,0 as base from dwellingexterior where code = " + FCConvert.ToString(recnum) + " and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strRECostFileDatabase);
							}
							else if (Strings.UCase(strType) == "ECONOMIC")
							{
								clsLoad.OpenRecordset("select code,factor as unit,description,shortdescription,0 as base from economiccode where code = " + FCConvert.ToString(recnum) + " and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strRECostFileDatabase);
							}
						}
						else if (Strings.UCase(strCat) == "PROPERTY")
						{
							if (Strings.UCase(strType) == "LANDTYPE")
							{
								clsLoad.OpenRecordset("select code,factor as unit,description,shortdescription,0 as base from landtype where code = " + FCConvert.ToString(recnum), modGlobalVariables.Statics.strRECostFileDatabase);
							}
							else if (Strings.UCase(strType) == "NEIGHBORHOOD")
							{
								clsLoad.OpenRecordset("select code,0 as unit,description,shortdescription,0 as base from neighborhood where code = " + FCConvert.ToString(recnum) + " and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strRECostFileDatabase);
							}
							else if (Strings.UCase(strType) == "ZONE")
							{
								clsLoad.OpenRecordset("select code,0 as unit,description,shortdescription,0 as base from zones where code = " + FCConvert.ToString(recnum) + " and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strRECostFileDatabase);
							}
							else if (Strings.UCase(strType) == "SECZONE")
							{
								clsLoad.OpenRecordset("select code,0 as unit,secondarydescription as description,secondaryshort as shortdescription,0 as base from zones where code = " + FCConvert.ToString(recnum) + " and isnull(townnumber,0) = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strRECostFileDatabase);
							}
						}
						else if (Strings.UCase(strCat) == "COMMERCIAL")
						{
							clsLoad.OpenRecordset("select * from commercialcost where crecordnumber = " + FCConvert.ToString(recnum) + " and isnull(townnumber,0)  = " + FCConvert.ToString(lngTownCode), modGlobalVariables.Statics.strRECostFileDatabase);
							if (!clsLoad.EndOfFile())
							{
								modGlobalVariables.Statics.ComCostRec.CBase = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("cbase")));
								modGlobalVariables.Statics.ComCostRec.ClDesc = FCConvert.ToString(clsLoad.Get_Fields_String("cldesc"));
								modGlobalVariables.Statics.ComCostRec.CSDesc = FCConvert.ToString(clsLoad.Get_Fields_String("csdesc"));
								modGlobalVariables.Statics.ComCostRec.CUnit = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("cunit")));
								return Get_Cost_Record;
							}
							else
							{
								Get_Cost_Record = false;
								modGlobalVariables.Statics.ComCostRec.CBase = FCConvert.ToString(0);
								modGlobalVariables.Statics.ComCostRec.ClDesc = "";
								modGlobalVariables.Statics.ComCostRec.CSDesc = "";
								modGlobalVariables.Statics.ComCostRec.CUnit = FCConvert.ToString(0);
								return Get_Cost_Record;
							}
						}
						if (!clsLoad.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [base] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("base")));
							modGlobalVariables.Statics.CostRec.ClDesc = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
							modGlobalVariables.Statics.CostRec.CSDesc = FCConvert.ToString(clsLoad.Get_Fields_String("shortdescription"));
							// TODO Get_Fields: Check the table for the column [unit] and replace with corresponding Get_Field method
							modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("unit")));
						}
						else
						{
							Get_Cost_Record = false;
							modGlobalVariables.Statics.CostRec.CBase = FCConvert.ToString(0);
							modGlobalVariables.Statics.CostRec.ClDesc = "";
							modGlobalVariables.Statics.CostRec.CSDesc = "";
							modGlobalVariables.Statics.CostRec.CUnit = FCConvert.ToString(0);
						}
					}
				}
				return Get_Cost_Record;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Get_Cost_Record", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Get_Cost_Record;
		}

		~clsCostFiles()
		{
			// clsDwellCost.DisconnectDatabase
			// clsMiscCost.DisconnectDatabase
			// clsPropertyCost.DisconnectDatabase
			// clsCommercialCost.DisconnectDatabase
			// clsOutSFFactor.DisconnectDatabase
			clsDwellCost = null;
			// Set clsMiscCost = Nothing
			clsPropertyCost = null;
			clsCommercialCost = null;
			clsOutSFFactor = null;
		}
	}
}
