﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptExemptionsByAccount.
	/// </summary>
	partial class rptExemptionsByAccount
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptExemptionsByAccount));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExemptName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtmuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtdate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtpage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtAccount,
            this.txtName,
            this.txtAmount,
            this.txtExemptName,
            this.txtTotal,
            this.Field5,
            this.Line1});
			this.Detail.Height = 0.4166667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.1875F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 0.6875F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.75F;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtName.Text = "Name";
			this.txtName.Top = 0F;
			this.txtName.Width = 3.125F;
			// 
			// txtAmount
			// 
			this.txtAmount.CanShrink = true;
			this.txtAmount.Height = 0.1875F;
			this.txtAmount.Left = 6.3125F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; text-align: right; white-space: nowrap; ddo-char-set: 0";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 1.125F;
			// 
			// txtExemptName
			// 
			this.txtExemptName.Height = 0.1875F;
			this.txtExemptName.Left = 3.9375F;
			this.txtExemptName.Name = "txtExemptName";
			this.txtExemptName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.txtExemptName.Text = null;
			this.txtExemptName.Top = 0F;
			this.txtExemptName.Width = 2.3125F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.1875F;
			this.txtTotal.Left = 6.3125F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0.1875F;
			this.txtTotal.Width = 1.125F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 3.9375F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.Field5.Text = "Total";
			this.Field5.Top = 0.1875F;
			this.Field5.Width = 2.3125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 5.763889F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.1888889F;
			this.Line1.Width = 1.694444F;
			this.Line1.X1 = 5.763889F;
			this.Line1.X2 = 7.458333F;
			this.Line1.Y1 = 0.1888889F;
			this.Line1.Y2 = 0.1888889F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtmuni,
            this.Field2,
            this.txtdate,
            this.txtpage,
            this.txtTime,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4});
			this.PageHeader.Height = 0.7916667F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// txtmuni
			// 
			this.txtmuni.Height = 0.1875F;
			this.txtmuni.Left = 0.0625F;
			this.txtmuni.Name = "txtmuni";
			this.txtmuni.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtmuni.Text = null;
			this.txtmuni.Top = 0F;
			this.txtmuni.Width = 1.4375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 2.125F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.Field2.Text = "Exemptions By Account";
			this.Field2.Top = 0F;
			this.Field2.Width = 3.125F;
			// 
			// txtdate
			// 
			this.txtdate.Height = 0.1875F;
			this.txtdate.Left = 6.25F;
			this.txtdate.Name = "txtdate";
			this.txtdate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtdate.Text = null;
			this.txtdate.Top = 0F;
			this.txtdate.Width = 1.1875F;
			// 
			// txtpage
			// 
			this.txtpage.Height = 0.1875F;
			this.txtpage.Left = 6.25F;
			this.txtpage.Name = "txtpage";
			this.txtpage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtpage.Text = null;
			this.txtpage.Top = 0.1875F;
			this.txtpage.Width = 1.1875F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.MultiLine = false;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.Label1.Text = "Account";
			this.Label1.Top = 0.5625F;
			this.Label1.Width = 0.5625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.75F;
			this.Label2.MultiLine = false;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.Label2.Text = "Name";
			this.Label2.Top = 0.5625F;
			this.Label2.Width = 0.5625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.9375F;
			this.Label3.MultiLine = false;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.Label3.Text = "Exemption";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 1.25F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.5625F;
			this.Label4.MultiLine = false;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 0";
			this.Label4.Text = "Amount";
			this.Label4.Top = 0.5625F;
			this.Label4.Width = 0.875F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptExemptionsByAccount
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExemptName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtmuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtpage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExemptName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtmuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtdate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtpage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
