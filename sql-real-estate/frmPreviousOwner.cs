﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmPreviousOwner.
	/// </summary>
	public partial class frmPreviousOwner : BaseForm
	{
		public frmPreviousOwner()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label8 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label8.AddControlArrayElement(Label8_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPreviousOwner InstancePtr
		{
			get
			{
				return (frmPreviousOwner)Sys.GetInstance(typeof(frmPreviousOwner));
			}
		}

		protected frmPreviousOwner _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int lngOwnerID;
		int lngAccount;
		int lngCurrAcct;

		private void frmPreviousOwner_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPreviousOwner_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPreviousOwner properties;
			//frmPreviousOwner.ScaleWidth	= 6555;
			//frmPreviousOwner.ScaleHeight	= 5160;
			//frmPreviousOwner.LinkTopic	= "Form1";
			//frmPreviousOwner.LockControls	= true;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ErrorHandler
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				modGlobalFunctions.SetTRIOColors(this);
				Label9.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				lblAccount.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Form_Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intResp As short, int --> As DialogResult
			DialogResult intResp;
			clsDRWrapper clsExecute = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				intResp = MessageBox.Show("This will permanently remove this previous owner information." + "\r\n" + "Do you wish to continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intResp == DialogResult.Yes)
				{
					clsExecute.Execute("delete from previousowner where id = " + FCConvert.ToString(lngOwnerID), modGlobalVariables.strREDatabase);
					this.Unload();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In delete");
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuInterestedParties_Click(object sender, System.EventArgs e)
		{
			if (lngCurrAcct > 0 && lngOwnerID > 0)
			{
				frmInterestedParties.InstancePtr.Init(lngCurrAcct, "RE", lngOwnerID, FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modGlobalVariables.CNSTEDITADDRESSES)) == "F");
			}
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (lngAccount <= 0)
				{
					object account = lngAccount;
					if (frmInput.InstancePtr.Init(ref account, "Choose Account", "Account", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber))
					{
						lngOwnerID = -2;
						// ensure no matches
						ClearInfo();
						lblAccount.Text = FCConvert.ToString(account);
					}
				}
				else
				{
					lngOwnerID = -2;
					ClearInfo();
					lblAccount.Text = FCConvert.ToString(lngAccount);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuNew_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuNew_Click()
		{
			mnuNew_Click(cmdNew, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			// vbPorter upgrade warning: dttemp As DateTime	OnWrite(string)
			DateTime dttemp;
			try
			{
				// On Error GoTo ErrorHandler
				clsSave.OpenRecordset("select * from previousowner where id = " + FCConvert.ToString(lngOwnerID), modGlobalVariables.strREDatabase);
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					if (lngAccount <= 0)
					{
						MessageBox.Show("Must have a valid account number to save", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					clsSave.AddNew();
					clsSave.Set_Fields("account", lngAccount);
					lngCurrAcct = lngAccount;
					clsSave.Set_Fields("datecreated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					modGlobalFunctions.AddCYAEntry_80("RE", "Manually added a previous owner", "Account " + FCConvert.ToString(lngAccount), "id = " + clsSave.Get_Fields_Int32("id"));
				}
				clsSave.Set_Fields("name", Strings.Trim(txtName.Text));
				clsSave.Set_Fields("secowner", Strings.Trim(txtSecOwner.Text));
				clsSave.Set_Fields("address1", Strings.Trim(txtAddress1.Text));
				clsSave.Set_Fields("address2", Strings.Trim(txtAddress2.Text));
				clsSave.Set_Fields("city", Strings.Trim(txtCity.Text));
				clsSave.Set_Fields("state", Strings.Trim(txtState.Text));
				clsSave.Set_Fields("zip", Strings.Trim(txtZip.Text));
				clsSave.Set_Fields("zip4", Strings.Trim(txtZip4.Text));
				if (Information.IsDate(T2KSaleDate.Text))
				{
					dttemp = FCConvert.ToDateTime(Strings.Format(T2KSaleDate.Text, "MM/dd/yyyy"));
					if (dttemp.ToOADate() == 0)
					{
						clsSave.Set_Fields("saledate", 0);
					}
					else
					{
						clsSave.Set_Fields("saledate", Strings.Format(dttemp, "MM/dd/yyyy"));
					}
				}
				else
				{
					clsSave.Set_Fields("saledate", 0);
				}
				clsSave.Update();
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In save and exit");
			}
		}
		// vbPorter upgrade warning: lngAutoID As int	OnWrite(short, string)
		// vbPorter upgrade warning: lngNewAccount As int	OnWriteFCConvert.ToDouble(
		public void Init(int lngAutoID, int lngNewAccount = 0)
		{
			try
			{
				// On Error GoTo ErrorHandler
				lngOwnerID = lngAutoID;
				lngCurrAcct = lngNewAccount;
				if (lngOwnerID > 0 && lngCurrAcct <= 0)
				{
					clsDRWrapper rsLoad = new clsDRWrapper();
					rsLoad.OpenRecordset("select * from previousowner where id = " + FCConvert.ToString(lngOwnerID), modGlobalVariables.strREDatabase);
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					lngCurrAcct = FCConvert.ToInt32(rsLoad.Get_Fields("account"));
				}
				if (lngOwnerID > 0)
				{
					FillOwnerInfo();
					lngAccount = 0;
				}
				else
				{
					// new
					lngAccount = lngNewAccount;
					mnuNew_Click();
				}
				//Application.DoEvents();
				if (lngAccount == 0 && lngOwnerID <= 0)
				{
					this.Unload();
				}
				else
				{
					this.Show(FCForm.FormShowEnum.Modal);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillOwnerInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			DateTime dttemp;
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select * from previousowner where id = " + FCConvert.ToString(lngOwnerID), modGlobalVariables.strREDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					lblAccount.Text = FCConvert.ToString(clsLoad.Get_Fields("account"));
					txtName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("name"));
					txtSecOwner.Text = FCConvert.ToString(clsLoad.Get_Fields_String("secowner"));
					txtAddress1.Text = FCConvert.ToString(clsLoad.Get_Fields_String("address1"));
					txtAddress2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("address2"));
					txtCity.Text = FCConvert.ToString(clsLoad.Get_Fields_String("city"));
					txtState.Text = FCConvert.ToString(clsLoad.Get_Fields_String("state"));
					txtZip.Text = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
					txtZip4.Text = FCConvert.ToString(clsLoad.Get_Fields_String("zip4"));
					if (Information.IsDate(clsLoad.Get_Fields("saledate")))
					{
						dttemp = (DateTime)clsLoad.Get_Fields_DateTime("saledate");
						if (dttemp.ToOADate() != 0)
						{
							T2KSaleDate.Text = Strings.Format(dttemp, "MM/dd/yyyy");
						}
					}
					// mebTelephone.Text = clsLoad.Fields("telephone")
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In fill owner info", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ClearInfo()
		{
			txtName.Text = "";
			txtSecOwner.Text = "";
			txtAddress1.Text = "";
			txtAddress2.Text = "";
			txtCity.Text = "";
			txtState.Text = "";
			txtZip.Text = "";
			txtZip4.Text = "";
			T2KSaleDate.Text = "";
		}
	}
}
