﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmexemptions.
	/// </summary>
	public partial class frmexemptions : BaseForm
	{
		public frmexemptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmexemptions InstancePtr
		{
			get
			{
				return (frmexemptions)Sys.GetInstance(typeof(frmexemptions));
			}
		}

		protected frmexemptions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool boolSubTotalOnly;
		string strSpecificList = "";

		private void cmdcancel_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.CancelledIt = true;
			frmexemptions.InstancePtr.Unload();
		}

		public void cmdcancel_Click()
		{
			//cmdcancel_Click(cmdcancel, new System.EventArgs());
		}

		private void cmdok_Click(object sender, System.EventArgs e)
		{
			int forindex;
			string tstring = "";
			string strReturn;
			bool boolSpecific;
			GridStart.Row = -1;
			GridEnd.Row = -1;
			//Application.DoEvents();
			modGlobalVariables.Statics.boolRange = false;
			boolSpecific = false;
			modGlobalVariables.Statics.CancelledIt = false;
			strReturn = "";
			modGlobalVariables.Statics.gintMinAccountRange = 0;
			modGlobalVariables.Statics.gintMaxAccountRange = 99;
			// 1 through 99 are the only valid exemptcodes
			// set minrange to 0 since you may have an exemption amount with no exempt codes
			for (forindex = 1; forindex <= 6; forindex++)
			{
				if (cmbtsort.SelectedIndex == forindex - 1)
				{
					modPrintRoutines.Statics.intwhichorder = forindex;
				}
			}
			// forindex
			for (forindex = 1; forindex <= 4; forindex++)
			{
				if (cmbtdisplay.SelectedIndex == forindex - 1)
				{
					modGlobalVariables.Statics.whatinclude = forindex;
				}
			}
			// forindex
			switch (modGlobalVariables.Statics.whatinclude)
			{
				case 1:
					{
						break;
					}
				case 2:
					{
						break;
					}
				case 3:
					{
						break;
					}
				case 4:
					{
						// specific exemptcode
						boolSpecific = true;
						strReturn = frmListChoiceWithIDs.InstancePtr.Init(ref strSpecificList, "Exempt Codes", "", true);
						if (Strings.Trim(strReturn) == string.Empty)
							return;
						// exemptcodetouse = Val(cmbStart.ItemData(cmbStart.listindex))
						break;
					}
				case 5:
					{
						modGlobalVariables.Statics.boolRange = true;
						// gintMinAccountRange = Val(cmbStart.ItemData(cmbStart.listindex))
						modGlobalVariables.Statics.gintMinAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(GridStart.TextMatrix(0, 0))));
						// gintMaxAccountRange = Val(cmbEnd.ItemData(cmbEnd.listindex))
						modGlobalVariables.Statics.gintMaxAccountRange = FCConvert.ToInt32(Math.Round(Conversion.Val(GridEnd.TextMatrix(0, 0))));
						// askagain1:
						// tstring = InputBox("Enter the First Exempt Code")
						// If tstring <> vbNullString Then
						// gintMinAccountRange = Val(tstring)
						// If gintMinAccountRange < 0 Then
						// MsgBox "This is an invalid code." & vbNewLine & "Please try again.", vbExclamation
						// GoTo askagain1
						// End If
						// askagain2:
						// tstring = InputBox("Enter the Last Exempt Code")
						// If tstring <> vbNullString Then
						// gintMaxAccountRange = Val(tstring)
						if (modGlobalVariables.Statics.gintMaxAccountRange < modGlobalVariables.Statics.gintMinAccountRange)
						{
							MessageBox.Show("This is an Invalid Range." + "\r\n" + "Please choose a different range.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						// If (gintMaxAccountRange > 99) Then
						// MsgBox "This is an Invalid Code." & vbNewLine & "Please try again.", vbExclamation
						// GoTo askagain2
						// End If
						// boolRange = True
						// Else
						// CancelledIt = True
						// End If
						// Else
						// CancelledIt = True
						// End If
						break;
					}
			}
			//end switch
			modPrintRoutines.Statics.gstrMinAccountRange = FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange);
			modPrintRoutines.Statics.gstrMaxAccountRange = FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange);
			// If Not CancelledIt Then
			// rptaudit.Show
			// Call frmReportViewer.Init(rptaudit)
			// Call rptaudit.Init(True, whatinclude, intwhichorder, boolBillingorCorrelated, boolRange, gstrMinAccountRange, gstrMaxAccountRange, boolSubTotalOnly)
			frmexemptions.InstancePtr.Unload();
			rptExemptAudit.InstancePtr.Init(ref modGlobalVariables.Statics.whatinclude, ref modPrintRoutines.Statics.intwhichorder, ref modPrintRoutines.Statics.gstrMinAccountRange, ref modPrintRoutines.Statics.gstrMaxAccountRange, modGlobalVariables.Statics.boolBillingorCorrelated, modGlobalVariables.Statics.boolRange, boolSubTotalOnly, strReturn);
			// End If
		}

		public void cmdok_Click()
		{
			cmdok_Click(cmdContinue, new System.EventArgs());
		}

		private void frmexemptions_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmexemptions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmexemptions properties;
			//frmexemptions.ScaleWidth	= 5880;
			//frmexemptions.ScaleHeight	= 4425;
			//frmexemptions.LinkTopic	= "Form1";
			//frmexemptions.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			if (modGlobalVariables.Statics.whatinclude == 2)
			{
				boolSubTotalOnly = true;
			}
			else
			{
				boolSubTotalOnly = false;
			}
			FillRangeCombos();
		}

		private void frmexemptions_Resize(object sender, System.EventArgs e)
		{
			int GridHeight;
			//GridHeight = GridStart.RowHeight(0) + 20;
			//GridStart.Height = GridHeight;
			//GridEnd.Height = GridHeight;
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdok_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdcancel_Click();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void Optdisplay_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// If Index > 3 Then
			// framRange.Visible = True
			// If Index >= 4 Then
			// GridEnd.Visible = True
			// lblTo.Visible = True
			// Else
			// GridEnd.Visible = False
			// lblTo.Visible = False
			// End If
			// Else
			// framRange.Visible = False
			// End If
		}

		private void Optdisplay_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtdisplay.SelectedIndex;
			Optdisplay_CheckedChanged(index, sender, e);
		}

		private void FillRangeCombos()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp;
			int intFirstCode;
			clsLoad.OpenRecordset("select * from exemptcode order by description,code", modGlobalVariables.strREDatabase);
			strTemp = "";
			strSpecificList = "";
			intFirstCode = 0;
			while (!clsLoad.EndOfFile())
			{
				if (intFirstCode == 0)
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					intFirstCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("code"))));
				}
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				else if (Conversion.Val(clsLoad.Get_Fields("code")) < intFirstCode)
				{
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					intFirstCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("code"))));
				}
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				strTemp += "#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + ";" + clsLoad.Get_Fields_String("description") + "\t" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + "|";
				// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
				strSpecificList += FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))) + ";" + clsLoad.Get_Fields_String("description") + "|";
				// cmbStart.AddItem clsLoad.Fields("description")
				// cmbStart.ItemData(cmbStart.NewIndex) = Val(clsLoad.Fields("code"))
				// cmbEnd.AddItem clsLoad.Fields("description")
				// cmbEnd.ItemData(cmbEnd.NewIndex) = Val(clsLoad.Fields("code"))
				clsLoad.MoveNext();
			}
			// GridStart.ColDataType(0) = flexDTBoolean
			// GridEnd.ColDataType(0) = flexDTBoolean
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				strSpecificList = Strings.Mid(strSpecificList, 1, strSpecificList.Length - 1);
				GridStart.ColComboList(0, strTemp);
				GridEnd.ColComboList(0, strTemp);
				GridStart.TextMatrix(0, 0, FCConvert.ToString(intFirstCode));
				GridEnd.TextMatrix(0, 0, FCConvert.ToString(intFirstCode));
			}
			else
			{
				GridStart.ColComboList(0, "#0;None");
				GridEnd.ColComboList(0, "#0;None");
			}
		}
	}
}
