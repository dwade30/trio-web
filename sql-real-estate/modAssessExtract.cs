﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	public class modAssessExtract
	{
		public static void MakeExtractionRecord_2(string strWhichTable)
		{
			MakeExtractionRecord(ref strWhichTable);
		}

		public static void MakeExtractionRecord(ref string strWhichTable)
		{
			// Opens the extract table and fills in the extracttable table
			// according to the include and exclude fields of the reports in
			// the extract table
			int intGroupNumber = 0;
			bool boolAlreadyExists;
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsDRWrapper dbTemp = new clsDRWrapper();
			bool boolHasAnIncEx = false;
			int lngID;
			clsReportParameter tCode;
			int lngCodeID = 0;
			try
			{
				// On Error GoTo ErrorHandler
				Statics.clCodeList.LoadCodes();
				lngID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
				Statics.strMaster = strWhichTable;
				if (Strings.UCase(Statics.strMaster) == "MASTER")
				{
					Statics.strDwelling = "Dwelling";
					Statics.strCommercial = "Commercial";
					Statics.strOutbuilding = "Outbuilding";
					Statics.strPreviousAssessment = "PreviousAssessment";
				}
				else if (Strings.UCase(Statics.strMaster) == "SRMASTER")
				{
					Statics.strDwelling = "SRDwelling";
					Statics.strCommercial = "SRCommercial";
					Statics.strOutbuilding = "SROutbuilding";
					Statics.strPreviousAssessment = "PreviousAssessment";
				}
				if (Strings.UCase(Statics.strMaster) == "MASTER")
				{
					dbTemp.Execute("update extract set exttype = 'Regular' where reportnumber = 0 and userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
				}
				else
				{
					dbTemp.Execute("update extract set exttype = 'Sale' where reportnumber = 0 and userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
				}
				Statics.RSExtract.OpenRecordset("Select * from extract where reportnumber > 0 and userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
				// Set RSInclude = DBTemp.OpenRecordset("select * from extracttable where groupnumber > 0")
				// Set RSExclude = DBTemp.OpenRecordset("Select * from extracttable where groupnumber > 0")
				// rsinclude and exclude should be empty
				dbTemp.Execute("Delete from ExtractTable where userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
				dbTemp.Execute("Delete from Exclude where userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
				// dbTemp.Execute ("Delete from Include")
				// Set dbRE = OpenDatabase(strredatabase, False, False, ";PWD=" & DATABASEPASSWORD)
				// boolAlreadyExists = False
				// 
				// For Each tbl In dbRE.TableDefs
				// If UCase(tbl.Name) = "EXTRACTTABLE" Then
				// For Each fld In tbl.Fields
				// If UCase(fld.Name) = "SALEDATE" Then
				// boolAlreadyExists = True
				// GoTo ExitForLoop
				// End If
				// Next
				// GoTo ExitForLoop
				// End If
				// Next
				ExitForLoop:
				;
				// If Not boolAlreadyExists Then
				// Set fld = tbl.CreateField("SaleDate", dbDate)
				// tbl.Fields.Append fld
				// End If
				if (Statics.RSExtract.EndOfFile())
				{
					MessageBox.Show("No matches found", "No Matches", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				while (!Statics.RSExtract.EndOfFile())
				{
					Statics.strSQLString = "Insert into ExtractTable (accountnumber,cardnumber,saledate,userid) select distinct ";
					Statics.strWhereClause = " where ";
					// ("
					Statics.strSQLSecondHalf = "";
					intGroupNumber = FCConvert.ToInt32(Statics.RSExtract.Get_Fields_Int32("reportnumber"));
					Statics.boolUsedMasterTable = false;
					Statics.boolUsedDwellingTable = false;
					Statics.boolUsedCommercialTable = false;
					Statics.boolUsedOutBuildingTable = false;
					boolHasAnIncEx = false;
					Statics.rsReport.OpenRecordset("Select * from reporttable where reportnumber = " + FCConvert.ToString(intGroupNumber) + " order by code", modGlobalVariables.strREDatabase);
					Statics.rsTemp.OpenRecordset("Select * from reporttable where reportnumber = " + FCConvert.ToString(intGroupNumber) + " and code = 401", modGlobalVariables.strREDatabase);
					if (Statics.rsTemp.EndOfFile())
					{
						Statics.bool401CodePresent = false;
						Statics.strOutQry = "";
					}
					else
					{
						Statics.bool401CodePresent = true;
					}
					while (!Statics.rsReport.EndOfFile())
					{
						if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("Incorexc")) != "N")
						{
							boolHasAnIncEx = true;
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							Statics.intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Statics.rsReport.Get_Fields("Code") + "")));
							lngCodeID = FCConvert.ToInt32(Math.Round(Conversion.Val(Statics.rsReport.Get_Fields_Int32("codeid"))));
							// Set rsCode = dbTemp.OpenRecordset("select * from reportcodes where code = " & intCode,strredatabase)
							tCode = Statics.clCodeList.GetCodeByCode(Statics.intCode, lngCodeID);
							if (!(tCode == null))
							{
								if (!tCode.SpecialCase)
								{
									HandleRegularCode(ref tCode);
								}
								else
								{
									HandleSpecialCases(ref tCode);
								}
							}
						}
						Statics.rsReport.MoveNext();
					}
					if (!boolHasAnIncEx)
					{
						clsTemp.OpenRecordset("select * from reporttitles where reportnumber = " + FCConvert.ToString(intGroupNumber), modGlobalVariables.strREDatabase);
						if (!clsTemp.EndOfFile())
						{
							MessageBox.Show("The extract cannot be finished because report: " + FCConvert.ToString(intGroupNumber) + " " + clsTemp.Get_Fields_String("TITLE") + " has no fields that the extract is included or excluded by", "Bad Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
					}
					BuildJoinClause();
					// strSQLString = strSQLString & "(" & strJoinClause & ")"
					Statics.strSQLString += Statics.strJoinClause;
					Statics.strSQLString += Statics.strWhereClause;
					// & ")"
					dbTemp.Execute(Statics.strSQLString, modGlobalVariables.strREDatabase);
					dbTemp.Execute("Update extracttable set groupnumber = " + Statics.RSExtract.Get_Fields_Int32("groupnumber") + " where isnull(extracttable.groupnumber,0) < 1 and userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
					Statics.RSExtract.MoveNext();
				}
				// This will take all of the excluded accounts out of the included accounts
				// DBTemp.Execute ("delete from extracttable where (select * from extracttable where accountnumber = (select accountnumber from exclude))")
				// DBTemp.Execute ("Delete * from extracttable where not exists (select * from include)")
				dbTemp.Execute("delete from extracttable where id in (select extracttable.id from ExtractTable INNER JOIN Exclude ON (ExtractTable.groupNumber = Exclude.groupNumber) AND (ExtractTable.CardNumber = Exclude.CardNumber) AND (ExtractTable.AccountNumber = Exclude.AccountNumber) and (extracttable.userid = exclude.userid) where extracttable.userid = " + FCConvert.ToString(lngID) + ")", modGlobalVariables.strREDatabase);
				dbTemp.Execute("Delete  from extracttable where groupnumber < 1", modGlobalVariables.strREDatabase);
				// temporary
				clsTemp.OpenRecordset("select * from extracttable where userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
				if (clsTemp.EndOfFile())
				{
					MessageBox.Show("No matches found.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Extraction Completed. " + FCConvert.ToString(clsTemp.RecordCount()) + " records found.", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				// Call rptExtractReport.Show
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In MakeExtractionRecord", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void HandleRegularCode(ref clsReportParameter tCode)
		{
			string strTableName;
			string strFieldName;
			string strMin;
			string strMax;
			string strSQL = "";
			clsDRWrapper temprs = new clsDRWrapper();
			// Dim qdf As QueryDef
			string strTemp = "";
			bool boolNumberType = false;
			int lngID;
			lngID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			strTableName = tCode.TableName;
			strFieldName = tCode.FieldName;
			strMin = Statics.rsReport.Get_Fields_String("Min") + "";
			strMax = Statics.rsReport.Get_Fields_String("Max") + "";
			switch (tCode.Code)
			{
				case 35:
				case 36:
				case 37:
				case 38:
				case 39:
				case 43:
				case 75:
				case 47:
				case 68:
					{
						boolNumberType = false;
						break;
					}
				default:
					{
						temprs.OpenRecordset("select top 1 * from " + strTableName, modGlobalVariables.strREDatabase);
						switch ((clsDRWrapper.DRDataTypes)temprs.Get_FieldsDataType(strFieldName))
						{
							case clsDRWrapper.DRDataTypes.dtDecimal:
							case clsDRWrapper.DRDataTypes.dtDouble:
							case clsDRWrapper.DRDataTypes.dtInt16:
							case clsDRWrapper.DRDataTypes.dtInt32:
								{
									boolNumberType = true;
									break;
								}
							case clsDRWrapper.DRDataTypes.dtInt64:
							case clsDRWrapper.DRDataTypes.dtSingle:
							case clsDRWrapper.DRDataTypes.dtUInt16:
							case clsDRWrapper.DRDataTypes.dtUInt32:
							case clsDRWrapper.DRDataTypes.dtUInt64:
								{
									boolNumberType = true;
									break;
								}
							case clsDRWrapper.DRDataTypes.dtBoolean:
								{
									boolNumberType = true;
									break;
								}
							default:
								{
									boolNumberType = false;
									break;
								}
						}
						//end switch
						break;
					}
			}
			//end switch
			// Case dbLong, dbInteger, dbDouble, dbSingle
			// boolNumberType = True
			// Case dbBoolean
			// boolNumberType = True
			// Case Else
			// boolNumberType = False
			// End Select
			if (Strings.UCase(strTableName + "") == "MASTER")
			{
				Statics.boolUsedMasterTable = true;
				strTableName = Statics.strMaster;
				if (Strings.LCase(Statics.strMaster) == "master")
				{
					strTableName = "mj";
				}
			}
			else if (Strings.UCase(strTableName + "") == "DWELLING")
			{
				Statics.boolUsedDwellingTable = true;
				strTableName = Statics.strDwelling;
			}
			else if (Strings.UCase(strTableName + "") == "COMMERCIAL")
			{
				Statics.boolUsedCommercialTable = true;
				strTableName = Statics.strCommercial;
			}
			else if (Strings.UCase(strTableName + "") == "OUTBUILDING")
			{
				Statics.boolUsedOutBuildingTable = true;
				strTableName = Statics.strOutbuilding;
			}
			// strSQL = "Insert into ExtractTable " ' (Select rsaccount,rscard from " & strTableName & " where " & strFieldName & " between " & lngMin & " and " & lngMax & " order by rsaccount, rscard)"
			// strSQL = strSQL & "(Select rsaccount,rscard from " & strTableName & " where " & strFieldName & " >= '" & strMin & "' and " & strFieldName & " <= '" & strMax & "' order by rsaccount, rscard)"
			// strSQL = strSQL & "Select accountnumber,cardnumber,groupnumber from Exclude"
			// Set qdf = DBtemp.createQueryDef("", strSQL)
			if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("Incorexc")) == "I")
			{
				// If IsNumeric(strmin) Then
				if (boolNumberType)
				{
					strTemp = " " + strTableName + "." + strFieldName + " >= " + strMin + " and " + strTableName + "." + strFieldName + " <= " + strMax;
				}
				else
				{
					if (tCode.Code != 72 && tCode.Code != 73 && tCode.Code != 47)
					{
						strTemp = " " + strTableName + "." + strFieldName + " >= '" + strMin + "' and " + strTableName + "." + strFieldName + " <= '" + strMax + "'";
					}
					else
					{
						strMax += "zzz";
						strTemp = " " + strTableName + "." + strFieldName + " >= '" + strMin + "' and " + strTableName + "." + strFieldName + " <= '" + strMax + "'";
					}
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.strSQLSecondHalf = " AND";
			}
			else
			{
				// strSQL = "Insert into Exclude (accountnumber,cardnumber) "
				// strSQL = strSQL & "select rsaccount,rscard from " & strTableName & " where " & strFieldName & " >= " & strMin & " and " & strFieldName & " <= " & strMax & " order by rsaccount, rscard"
				// DBTemp.Execute (strSQL)
				// If IsNumeric(strmin) Then
				if (boolNumberType)
				{
					strTemp = " (" + strTableName + "." + strFieldName + " < " + strMin + " or " + strTableName + "." + strFieldName + " > " + strMax + ")";
				}
				else
				{
					if (tCode.Code != 72 && tCode.Code != 73 && tCode.Code != 47)
					{
						strTemp = " (" + strTableName + "." + strFieldName + " < '" + strMin + "' or " + strTableName + "." + strFieldName + " > '" + strMax + "')";
					}
					else
					{
						strMax += "zzz";
						strTemp = " (" + strTableName + "." + strFieldName + " < '" + strMin + "' or " + strTableName + "." + strFieldName + " > '" + strMax + "')";
					}
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.strSQLSecondHalf = " AND";
			}
			// strSQL = strSQL & "select rsaccount,rscard from " & strTableName & " where " & strFieldName & " >= '" & strMin & "' and " & strFieldName & " <= '" & strMax & "' order by rsaccount, rscard"
			// strSQLString = strSQLString & strSQLSecondHalf
			// strSQLString = strSQLString & " (select rsaccount,rscard from " & strTableName & " where " & strFieldName & " >= '" & strMin & "' and " & strFieldName & " <= '" & strMax & "')"
			if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("Incorexc")) == "E")
			{
				temprs.Execute("update exclude set groupnumber = " + Statics.RSExtract.Get_Fields_Int32("groupnumber") + " where  exclude.groupnumber < 1 AND exclude.userid = " + FCConvert.ToString(lngID), modGlobalVariables.strREDatabase);
			}
		}

		private static void HandleSpecialCases(ref clsReportParameter tCode)
		{
			// these codes must be handled on an idividual basis
			string strTableName;
			string strFieldName;
			string strMin;
			string strMax;
			string strSQL = "";
			string strTemp = "";
			int intTempYear = 0;
			int intTempMonth = 0;
			int intTempDay;
			int intLandCode = 0;
			int x;
			bool boolSale = false;
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strTemp2 = "";
			string strMasterToUse = "";
			if (Strings.UCase(Statics.strMaster) == "SRMASTER")
			{
				boolSale = true;
				strMasterToUse = Statics.strMaster;
			}
			else
			{
				boolSale = false;
				strMasterToUse = "mj";
			}
			strTableName = tCode.TableName;
			strFieldName = tCode.FieldName;
			strMin = Statics.rsReport.Get_Fields_String("Min") + "";
			strMax = Statics.rsReport.Get_Fields_String("Max") + "";
			// If RSReport.fields("incorexc") = "I" Then
			// strSQL = "Insert into extracttable (accountnumber,cardnumber) "
			// Else
			// strSQL = "Insert into Exclude (accountnumber,cardnumber) "
			// End If
			int vbPorterVar = tCode.Code;
			if (vbPorterVar == 7)
			{
				// topography
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ( (" + strMasterToUse + ".pitopography1 >= " + strMin + " and " + strMasterToUse + ".pitopography1 <= " + strMax + ") or (" + strMasterToUse + ".pitopography2 >= " + strMin + " and " + strMasterToUse + ".pitopography2 <= " + strMax + "))";
				}
				else
				{
					strTemp = " ( (" + strMasterToUse + ".pitopography1 < " + strMin + " or " + strMasterToUse + ".pitopography1 > " + strMax + ") and (" + strMasterToUse + ".pitopography2 < " + strMin + " or " + strMasterToUse + ".pitopography2 > " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 8)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ( (" + strMasterToUse + ".piutilities1 >= " + strMin + " and " + strMasterToUse + ".piutilities1 <= " + strMax + ") or (" + strMasterToUse + ".piutilities2 >= " + strMin + " and " + strMasterToUse + ".piutilities2 <= " + strMax + "))";
				}
				else
				{
					strTemp = " ( (" + strMasterToUse + ".piutilities1 < " + strMin + " or " + strMasterToUse + ".piutilities1 > " + strMax + ") and (" + strMasterToUse + ".piutilities2 < " + strMin + " or " + strMasterToUse + ".piutilities2 > " + strMax + "))";
				}
				Statics.boolUsedMasterTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 15)
			{
				// saledate
				if (Information.IsDate(strMin))
				{
					if (Information.IsDate(strMax))
					{
						if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
						{
							strTemp = " " + strMasterToUse + ".saledate >= '" + strMin + "' and " + strMasterToUse + ".saledate <= '" + strMax + "'";
						}
						else
						{
							strTemp = " " + strMasterToUse + ".saledate < '" + strMin + "' or " + strMasterToUse + ".saledate > '" + strMax + "'";
						}
					}
					else
					{
						if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
						{
							strTemp = " " + strMasterToUse + ".saledate >= '" + strMin + "' ";
						}
						else
						{
							strTemp = " " + strMasterToUse + ".saledate < '" + strMin + "' ";
						}
					}
					Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
					Statics.boolUsedMasterTable = true;
				}
				else if (Information.IsDate(strMax))
				{
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						strTemp = " " + strMasterToUse + ".saledate <= '" + strMax + "' ";
					}
					else
					{
						strTemp = " " + strMasterToUse + ".saledate > '" + strMin + "' ";
					}
					Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
					Statics.boolUsedMasterTable = true;
				}
				// intTempMonth = Month(strmin)
				// intTempYear = Val(Right(Trim(strmin), 2))
				// strmin = intTempMonth & "/1/" & Right(Trim(strmin), 2)
				// 
				// 
				// intTempMonth = Month(strmax)
				// strmax = intTempMonth & "/1/" & Right(Trim(strmax), 2)
				// intTempYear = Year(Trim(strmax))
				// If intTempMonth = 12 Then
				// intTempMonth = 1
				// intTempYear = intTempYear + 1
				// Else
				// intTempMonth = intTempMonth + 1
				// End If
				// strmax = intTempMonth & "/1/" & intTempYear
				// If rsReport.fields("incorexc") = "I" Then
				// strTemp = " " & strMaster & ".saledate >= #" & strmin & "# and " & strMaster & ".saledate < #" & strmax & "#"
				// Else
				// strTemp = " " & strMaster & ".saledate < #" & strmin & "# or " & strMaster & ".saledate >= #" & strmax & "#"
				// End If
				// strWhereClause = strWhereClause & strSQLSecondHalf & strTemp
				// boolUsedMasterTable = True
			}
			else if (vbPorterVar == 18)
			{
				// Exempt Code
				if (Conversion.Val(strMax) > 32000)
				{
					strMax = "32000";
				}
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + strMasterToUse + ".riexemptcd1 >= " + strMin + " and " + strMasterToUse + ".riexemptcd1 <= " + strMax + ") or (" + strMasterToUse + ".riexemptcd2 >= " + strMin + " and " + strMasterToUse + ".riexemptcd2 <= " + strMax + ") or (" + strMasterToUse + ".riexemptcd3 >= " + strMin + " and " + strMasterToUse + ".riexemptcd3 <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + strMasterToUse + ".riexemptcd1 < " + strMin + " or " + strMasterToUse + ".riexemptcd1 > " + strMax + ") and (" + strMasterToUse + ".riexemptcd2 < " + strMin + " or ";
					strTemp += strMasterToUse + ".riexemptcd2 > " + strMax + ") and (" + strMasterToUse + ".riexemptcd3 < " + strMin + " or " + strMasterToUse + ".riexemptcd3 > " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 24)
			{
				// land schedule
				int intZone = 0;
				int intNeigh = 0;
				int intMaxZone;
				clsDRWrapper clsTemp2 = new clsDRWrapper();
				double dblFactor;
				// vbPorter upgrade warning: lngSched As int	OnWriteFCConvert.ToDouble(
				int lngSched = 0;
				intMaxZone = 50;
				clsTemp.OpenRecordset("select rsaccount,pizone,pineighborhood,piseczone,zoneoverride,ritrancode from " + Statics.strMaster + " where rscard = 1 and not rsdeleted = 1", modGlobalVariables.strREDatabase);
				if (!clsTemp.EndOfFile())
				{
					strTemp = "(";
					while (!clsTemp.EndOfFile())
					{
						intZone = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("pizone"))));
						// If intZone > intMaxZone Then
						// intZone = Val(clsTemp.Fields("pizone")) - intMaxZone
						// End If
						if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("zoneoverride")))
						{
							// intZone = Val(clsTemp.Fields("piseczone")) - intMaxZone
							intZone = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("piseczone"))));
						}
						intNeigh = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("pineighborhood"))));
						// dblFactor = LandTRec.LandFactor(intNeigh, intZone)
						if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
						{
							modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(clsTemp.Get_Fields_Int32("ritrancode"))));
						}
						lngSched = FCConvert.ToInt32(modGlobalVariables.Statics.LandTRec.Schedule(ref intNeigh, ref intZone));
						// If intNeigh > 0 And intNeigh < 100 Then
						// Call clsTemp2.OpenRecordset("select * from landtrecord where lrecordnumber = " & intZone & Format(intNeigh, "00"), strREDatabase)
						// If Not clsTemp2.EndOfFile Then
						if ((lngSched >= Conversion.Val(strMin)) && (lngSched <= Conversion.Val(strMax)))
						{
							strTemp += clsTemp.Get_Fields_Int32("rsaccount") + ",";
						}
						// End If
						// End If
						clsTemp.MoveNext();
					}
					if (strTemp.Length == 1)
					{
						// no matches
						if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
						{
							strTemp = " " + strMasterToUse + ".rsaccount = - 3 ";
						}
						else
						{
							strTemp = " " + strMasterToUse + ".rsaccount > 0";
						}
					}
					else
					{
						// get rid of comma
						strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
						strTemp += ")";
						if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
						{
							strTemp = " " + strMasterToUse + ".rsaccount in " + strTemp;
						}
						else
						{
							strTemp = " " + strMasterToUse + ".rsaccount not in " + strTemp;
						}
					}
				}
				else
				{
					// no matches possible
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						strTemp = " " + strMasterToUse + ".rsaccount = - 3 ";
					}
					else
					{
						strTemp = " " + strMasterToUse + ".rsaccount > 0";
					}
				}
				// Call clsTemp.OpenRecordset("select * from LANDTRECORD where lrecordnumber > 1100 and val(ltcode & '') between " & Val(strmin) & " and " & Val(strmax), strredatabase)
				// If Not clsTemp.EndOfFile Then
				// strTemp = "("
				// Do While Not clsTemp.EndOfFile
				// strTemp2 = Format(clsTemp.Fields("lrecordnumber"), "0000")
				// intZone = Val(Mid(strTemp2, 1, 2))
				// intNeigh = Val(Mid(strTemp2, 3, 2))
				// strTemp2 = " (" & strMaster & ".pizone = " & intZone & " and " & strMaster & ".pineighborhood = " & intNeigh & ")"
				// If RSReport.Fields("incorexc") = "I" Then
				// strTemp = strTemp & strTemp2 & "  or"
				// Else
				// strTemp = strTemp & " not " & strTemp2 & " and"
				// End If
				// clsTemp.MoveNext
				// Loop
				// get rid of last and
				// strTemp = Mid(strTemp, 1, Len(strTemp) - 3)
				// strTemp = strTemp & ")"
				// Else
				// no matches possible
				// strTemp = " " & strMaster & ".rsaccount = - 3 "
				// End If
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 27)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " " + strMasterToUse + ".hlcompvalland  + " + strMasterToUse + ".hlcompvalbldg  >= " + strMin + "  and " + strMasterToUse + ".hlcompvalland  +  " + strMasterToUse + ".hlcompvalbldg  <= " + strMax;
				}
				else
				{
					strTemp = " " + strMasterToUse + ".hlcompvalland  + " + strMasterToUse + ".hlcompvalbldg  < " + strMin + "  or " + strMasterToUse + ".hlcompvalland  +  " + strMasterToUse + ".hlcompvalbldg  > " + strMax;
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 30)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " " + strMasterToUse + ".lastlandval + " + strMasterToUse + ".lastbldgval >= " + strMin + " and " + strMasterToUse + ".lastlandval + " + strMasterToUse + ".lastbldgval <= " + strMax;
				}
				else
				{
					strTemp = " " + strMasterToUse + ".lastlandval + " + strMasterToUse + ".lastbldgval < " + strMin + " or " + strMasterToUse + ".lastlandval + " + strMasterToUse + ".lastbldgval > " + strMax;
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 34)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " " + strMasterToUse + ".rsmaplot between '" + strMin + "' and '" + strMax + "'";
				}
				else
				{
					strTemp = " " + strMasterToUse + ".rsmaplot not between '" + strMin + "' and '" + strMax + "'";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 35)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " " + strMasterToUse + ".rsname between '" + strMin + "' and '" + strMax + "'";
				}
				else
				{
					strTemp = " " + strMasterToUse + ".rsname not between '" + strMin + "' and '" + strMax + "'";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 40)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("Incorexc")) == "I")
				{
					strTemp = " " + strMasterToUse + ".rslocstreet between '" + strMin + "' and '" + strMax + "'";
				}
				else
				{
					strTemp = " (not " + strMasterToUse + ".rslocstreet between '" + strMin + "' and '" + strMax + "')";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 44)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " " + strMasterToUse + ".hlcompvalland  + " + strMasterToUse + ".hlcompvalbldg - " + strMasterToUse + ".rlexemption >= " + strMin + "  and " + strMasterToUse + ".hlcompvalland  +  " + strMasterToUse + ".hlcompvalbldg - " + strMasterToUse + ".rlexemption <= " + strMax;
				}
				else
				{
					strTemp = " " + strMasterToUse + ".hlcompvalland  + " + strMasterToUse + ".hlcompvalbldg - " + strMasterToUse + ".rlexemption < " + strMin + "  or " + strMasterToUse + ".hlcompvalland  +  " + strMasterToUse + ".hlcompvalbldg  - " + strMasterToUse + ".rlexemption> " + strMax;
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 45)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " " + strMasterToUse + ".lastlandval + " + strMasterToUse + ".lastbldgval - " + strMasterToUse + ".rlexemption >= " + strMin + " and " + strMasterToUse + ".lastlandval + " + strMasterToUse + ".lastbldgval - " + strMasterToUse + ".rlexemption <= " + strMax;
				}
				else
				{
					strTemp = " " + strMasterToUse + ".lastlandval + " + strMasterToUse + ".lastbldgval - " + strMasterToUse + ".rlexemption < " + strMin + " or " + strMasterToUse + ".lastlandval + " + strMasterToUse + ".lastbldgval - " + strMasterToUse + ".rlexemption > " + strMax;
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 46)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " " + strMasterToUse + ".rllandval + " + strMasterToUse + ".rlbldgval - " + strMasterToUse + ".rlexemption >= " + strMin + " and " + strMasterToUse + ".rllandval + " + strMasterToUse + ".rlbldgval - " + strMasterToUse + ".rlexemption <= " + strMax;
				}
				else
				{
					strTemp = " " + strMasterToUse + ".rllandval + " + strMasterToUse + ".rlbldgval - " + strMasterToUse + ".rlexemption < " + strMin + " or " + strMasterToUse + ".rllandval + " + strMasterToUse + ".rlbldgval - " + strMasterToUse + ".rlexemption > " + strMax;
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 50)
			{
				// date inspected
				// intTempYear = Year(strmax)
				// 
				// intTempMonth = Month(strmax)
				// If intTempMonth = 12 Then
				// intTempMonth = 1
				// intTempYear = intTempYear + 1
				// Else
				// intTempMonth = intTempMonth + 1
				// End If
				// strmax = intTempMonth & "/1/" & intTempYear
				if (Information.IsDate(strMin))
				{
					if (Information.IsDate(strMax))
					{
						if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
						{
							strTemp = " " + strMasterToUse + ".dateinspected >= '" + strMin + "' and " + strMasterToUse + ".dateinspected < '" + strMax + "'";
						}
						else
						{
							strTemp = " " + strMasterToUse + ".dateinspected < '" + strMin + "' or " + strMasterToUse + ".dateinspected >= '" + strMax + "'";
						}
					}
					else
					{
						if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
						{
							strTemp = " " + strMasterToUse + ".dateinspected >= '" + strMin + "' ";
						}
						else
						{
							strTemp = " " + strMasterToUse + ".dateinspected < '" + strMin + "' ";
						}
					}
					Statics.boolUsedMasterTable = true;
					Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				}
				else if (Information.IsDate(strMax))
				{
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						strTemp = " " + strMasterToUse + ".dateinspected <= '" + strMax + "' ";
					}
					else
					{
						strTemp = " " + strMasterToUse + ".dateinspected > '" + strMax + "' ";
					}
					Statics.boolUsedMasterTable = true;
					Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				}
			}
			else if (vbPorterVar == 52)
			{
				// street number
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " val(" + strMasterToUse + ".rslocnumalph & '') between " + strMin + " and " + strMax;
				}
				else
				{
					strTemp = " ( not val(" + strMasterToUse + ".rslocnumalph & '') between " + strMin + " and " + strMax + ")";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 62)
			{
				// ref1 1-23
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " left(" + strMasterToUse + ".rsref1 & '                       ',23) between '" + strMin + "' and '" + strMax + "' ";
				}
				else
				{
					strTemp = " ( not left(" + strMasterToUse + ".rsref1 & '                       ',23) between '" + strMin + "' and '" + strMax + "') ";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 63)
			{
				// ref1 24 - 35
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " mid(" + strMasterToUse + ".rsref1 & '                                   ',24,11) between '" + strMin + "' and '" + strMax + "' ";
				}
				else
				{
					strTemp = " ( not mid(" + strMasterToUse + ".rsref1 & '                                   ',24,11) between '" + strMin + "' and '" + strMax + "') ";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 64)
			{
				// Difference in assessment
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "   (val(" + strMasterToUse + ".rllandval & '') + val(" + strMasterToUse + ".rlbldgval & '')) - (val(" + strMasterToUse + ".lastlandval & '') + val(" + strMasterToUse + ".lastbldgval & '')) between " + strMin + " and " + strMax;
				}
				else
				{
					strTemp = " ( not   (val(" + strMasterToUse + ".rllandval & '') + val(" + strMasterToUse + ".rlbldgval & ''))- (val(" + strMasterToUse + ".lastlandval & '') + val(" + strMasterToUse + ".lastbldgval & '')) between " + strMin + " and " + strMax + ") ";
				}
				Statics.boolUsedMasterTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 65)
			{
				// tax amount
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " (val(" + strMasterToUse + ".lastlandval & '') + val(" + strMasterToUse + ".lastbldgval & '') - val(" + strMasterToUse + ".rlexemption & '')) * " + FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.CustomizedInfo.BillRate) / 1000) + " between " + strMin + " and " + strMax;
				}
				else
				{
					strTemp = " not ((val(" + strMasterToUse + ".lastlandval & '') + val(" + strMasterToUse + ".lastbldgval & '') - val(" + strMasterToUse + ".rlexemption & '')) * " + FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.CustomizedInfo.BillRate) / 1000) + " between " + strMin + " and " + strMax + ") ";
				}
				Statics.boolUsedMasterTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 66)
			{
				// difference in billing
				if (modGlobalVariables.Statics.CustomizedInfo.BillYear > 1900)
				{
					intTempYear = modGlobalVariables.Statics.CustomizedInfo.BillYear - 1;
				}
				else
				{
					intTempYear = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy")).Year - 1;
				}
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " (val(" + strMasterToUse + ".lastlandval & '') + val(" + strMasterToUse + ".lastbldgval & '') - val(" + strMasterToUse + ".rlexemption & '')) - (val(previousassessment.LAND  & '') + val(previousassessment.building & '') - val(previousassessment.EXEMPT & '')) between " + strMin + " and " + strMax + " and previousassessment.taxyear = " + FCConvert.ToString(intTempYear);
				}
				else
				{
					strTemp = " not ((val(" + strMasterToUse + ".lastlandval & '') + val(" + strMasterToUse + ".lastbldgval & '')- val(" + strMasterToUse + ".rlexemption & '')) - (val(previousassessment.land & '') + val(previousassessment.building & '') - val(previousassessment.exempt & '')) between " + strMin + " and " + strMax + ") and previousassessment.taxyear = " + FCConvert.ToString(intTempYear);
				}
				Statics.boolUsedMasterTable = true;
				Statics.boolUsedPreviousAssessmentTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 67)
			{
				// date updated
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ( hlupdate between '" + strMin + "' and '" + strMax + "'";
					strTemp += ")";
				}
				else
				{
					strTemp = " ( not hlupdate between '" + strMin + "' and '" + strMax + "'";
					strTemp += "))";
				}
				Statics.boolUsedMasterTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 74)
			{
				// date created
				// intTempMonth = Month(strmin)
				// intTempYear = Val(Right(Trim(strmin), 2))
				// strmin = intTempMonth & "/1/" & Right(Trim(strmin), 2)
				// 
				// 
				// intTempMonth = Month(strmax)
				// strmax = intTempMonth & "/1/" & Right(Trim(strmax), 2)
				// intTempYear = Year(Trim(strmax))
				// If intTempMonth = 12 Then
				// intTempMonth = 1
				// intTempYear = intTempYear + 1
				// Else
				// intTempMonth = intTempMonth + 1
				// End If
				// strmax = intTempMonth & "/1/" & intTempYear
				if (Information.IsDate(strMin))
				{
					if (Information.IsDate(strMax))
					{
						if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
						{
							strTemp = " " + strMasterToUse + ".datecreated >= '" + strMin + "' and " + strMasterToUse + ".datecreated <= '" + strMax + "'";
						}
						else
						{
							strTemp = " " + strMasterToUse + ".datecreated < '" + strMin + "' or " + strMasterToUse + ".datecreated > '" + strMax + "'";
						}
					}
					else
					{
						if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
						{
							strTemp = " " + strMasterToUse + ".datecreated >= '" + strMin + "' ";
						}
						else
						{
							strTemp = " " + strMasterToUse + ".datecreated < '" + strMin + "' ";
						}
					}
					Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
					Statics.boolUsedMasterTable = true;
				}
				else if (Information.IsDate(strMax))
				{
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						strTemp = " " + strMasterToUse + ".datecreated <= '" + strMax + "' ";
					}
					else
					{
						strTemp = " " + strMasterToUse + ".datecreated > '" + strMax + "' ";
					}
					Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
					Statics.boolUsedMasterTable = true;
				}
			}
			else if (vbPorterVar == 75)
			{
				// email
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " email between '" + strMin + "' and '" + strMax + "'";
				}
				else
				{
					strTemp = " email between '" + strMin + "' and '" + strMax + "'";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 98)
			{
				// influence factor
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "((" + strMasterToUse + ".piland1inf >= " + strMin + " and " + strMasterToUse + ".piland1inf <= " + strMax + ") or (" + strMasterToUse + ".piland2inf >= " + strMin + " and " + strMasterToUse + ".piland2inf <= " + strMax + ") or (" + strMasterToUse + ".piland3inf >= " + strMin + " and " + strMasterToUse + ".piland3inf <= " + strMax + ") or (" + strMasterToUse + ".piland4inf >= " + strMin + " and " + strMasterToUse + ".piland4inf <= " + strMax + ") or (" + strMasterToUse + ".piland5inf >= " + strMin + " and " + strMasterToUse + ".piland5inf <= " + strMax + ") or (" + strMasterToUse + ".piland6inf >= " + strMin + " and " + strMasterToUse + ".piland6inf <= " + strMax + ") or (" + strMasterToUse + ".piland7inf >= " + strMin + " and " + strMasterToUse + ".piland7inf <= " + strMax + "))";
				}
				else
				{
					strTemp = "((" + strMasterToUse + ".piland1inf < " + strMin + " or " + strMasterToUse + ".piland1inf > " + strMax + ") and (" + strMasterToUse + ".piland2inf < " + strMin + " or " + strMasterToUse + ".piland2inf > " + strMax + ") and (" + strMasterToUse + ".piland3inf < " + strMin + " or " + strMasterToUse + ".piland3inf > " + strMax + ") and (" + strMasterToUse + ".piland4inf < " + strMin + " or " + strMasterToUse + ".piland4inf > " + strMax + ") and (" + strMasterToUse + ".piland5inf < " + strMin + " or " + strMasterToUse + ".piland5inf > " + strMax + ") and (" + strMasterToUse + ".piland6inf < " + strMin + " or " + strMasterToUse + ".piland6inf > " + strMax + ") and (" + strMasterToUse + ".piland7inf < " + strMin + " or " + strMasterToUse + ".piland7inf > " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 99)
			{
				// influence factor code
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "((" + strMasterToUse + ".piland1infcode >= " + strMin + " and " + strMasterToUse + ".piland1infcode <= " + strMax + ") or (" + strMasterToUse + ".piland2infcode >= " + strMin + " and " + strMasterToUse + ".piland2infcode <= " + strMax + ") or (" + strMasterToUse + ".piland3infcode >= " + strMin + " and " + strMasterToUse + ".piland3infcode <= " + strMax + ") or (" + strMasterToUse + ".piland4infcode >= " + strMin + " and " + strMasterToUse + ".piland4infcode <= " + strMax + ") or (" + strMasterToUse + ".piland5infcode >= " + strMin + " and " + strMasterToUse + ".piland5infcode <= " + strMax + ") or (" + strMasterToUse + ".piland6infcode >= " + strMin + " and " + strMasterToUse + ".piland6infcode <= " + strMax + ") or (" + strMasterToUse + ".piland7infcode >= " + strMin + " and " + strMasterToUse + ".piland7infcode <= " + strMax + "))";
				}
				else
				{
					strTemp = "((" + strMasterToUse + ".piland1infcode < " + strMin + " or " + strMasterToUse + ".piland1infcode > " + strMax + ") and (" + strMasterToUse + ".piland2infcode < " + strMin + " or " + strMasterToUse + ".piland2infcode > " + strMax + ") and (" + strMasterToUse + ".piland3infcode < " + strMin + " or " + strMasterToUse + ".piland3infcode > " + strMax + ") and (" + strMasterToUse + ".piland4infcode < " + strMin + " or " + strMasterToUse + ".piland4infcode > " + strMax + ") and (" + strMasterToUse + ".piland5infcode < " + strMin + " or " + strMasterToUse + ".piland5infcode > " + strMax + ") and (" + strMasterToUse + ".piland6infcode < " + strMin + " or " + strMasterToUse + ".piland6infcode > " + strMax + ") and (" + strMasterToUse + ".piland7infcode < " + strMin + " or " + strMasterToUse + ".piland7infcode > " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 100)
			{
				// land type
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "((" + strMasterToUse + ".piland1type >= " + strMin + " and " + strMasterToUse + ".piland1type <= " + strMax + ") or (" + strMasterToUse + ".piland2type >= " + strMin + " and " + strMasterToUse + ".piland2type <= " + strMax + ") or (" + strMasterToUse + ".piland3type >= " + strMin + " and " + strMasterToUse + ".piland3type <= " + strMax + ") or (" + strMasterToUse + ".piland4type >= " + strMin + " and " + strMasterToUse + ".piland4type <= " + strMax + ") or (" + strMasterToUse + ".piland5type >= " + strMin + " and " + strMasterToUse + ".piland5type <= " + strMax + ") or (" + strMasterToUse + ".piland6type >= " + strMin + " and " + strMasterToUse + ".piland6type <= " + strMax + ") or (" + strMasterToUse + ".piland7type >= " + strMin + " and " + strMasterToUse + ".piland7type <= " + strMax + "))";
				}
				else
				{
					strTemp = "((" + strMasterToUse + ".piland1type < " + strMin + " or " + strMasterToUse + ".piland1type > " + strMax + ") and (" + strMasterToUse + ".piland2type < " + strMin + " or " + strMasterToUse + ".piland2type > " + strMax + ") and (" + strMasterToUse + ".piland3type < " + strMin + " or " + strMasterToUse + ".piland3type > " + strMax + ") and (" + strMasterToUse + ".piland4type < " + strMin + " or " + strMasterToUse + ".piland4type > " + strMax + ") and (" + strMasterToUse + ".piland5type < " + strMin + " or " + strMasterToUse + ".piland5type > " + strMax + ") and (" + strMasterToUse + ".piland6type < " + strMin + " or " + strMasterToUse + ".piland6type > " + strMax + ") and (" + strMasterToUse + ".piland7type < " + strMin + " or " + strMasterToUse + ".piland7type > " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 101)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "((" + strMasterToUse + ".piland1type >= 11 and " + strMasterToUse + ".piland1type <= 15 and " + strMasterToUse + ".piland1unitsa >= " + strMin + " and " + strMasterToUse + ".piland1unitsa <= " + strMax + ")or(" + strMasterToUse + ".piland2type >= 11 and " + strMasterToUse + ".piland2type <= 15 and " + strMasterToUse + ".piland2unitsa >= " + strMin + " and " + strMasterToUse + ".piland2unitsa <= " + strMax + ")or(" + strMasterToUse + ".piland3type >= 11 and " + strMasterToUse + ".piland3type <= 15 and " + strMasterToUse + ".piland3unitsa >= " + strMin + " and " + strMasterToUse + ".piland3unitsa <= " + strMax + ")or(" + strMasterToUse + ".piland4type >= 11 and " + strMasterToUse + ".piland4type <= 15 and " + strMasterToUse + ".piland4unitsa >= ";
					strTemp += strMin + " and " + strMasterToUse + ".piland4unitsa <= " + strMax + ")or(" + strMasterToUse + ".piland5type >= 11 and " + strMasterToUse + ".piland5type <= 15 and " + strMasterToUse + ".piland5unitsa >= " + strMin + " and " + strMasterToUse + ".piland5unitsa <= " + strMax + ")or(" + strMasterToUse + ".piland6type >= 11 and " + strMasterToUse + ".piland6type <= 15 and " + strMasterToUse + ".piland6unitsa >= " + strMin + " and " + strMasterToUse + ".piland6unitsa <= " + strMax + ")or";
					strTemp += "(" + strMasterToUse + ".piland7type >= 11 and " + strMasterToUse + ".piland7type <= 15 and " + strMasterToUse + ".piland7unitsa >= " + strMin + " and " + strMasterToUse + ".piland7unitsa <= " + strMax + "))";
				}
				else
				{
					strTemp = "((" + strMasterToUse + ".piland1type < 11 or " + strMasterToUse + ".piland1type > 15 or(" + strMasterToUse + ".piland1unitsa < " + strMin + " or " + strMasterToUse + ".piland1unista > " + strMax + "))and(" + strMasterToUse + ".piland2type < 11 or " + strMasterToUse + ".piland2type > 15 or(" + strMasterToUse + ".piland2unitsa < " + strMin + " or " + strMasterToUse + ".piland2unista > " + strMax + "))and(" + strMasterToUse + ".piland3type < 11 or " + strMasterToUse + ".piland3type > 15 or(" + strMasterToUse + ".piland3unitsa < " + strMin + " or " + strMasterToUse + ".piland3unista > " + strMax + "))and(" + strMasterToUse + ".piland4type < 11 or " + strMasterToUse + ".piland4type > 15 or(" + strMasterToUse + ".piland4unitsa < " + strMin + " or " + strMasterToUse + ".piland4unista > " + strMax + "))";
					strTemp += "and(" + strMasterToUse + ".piland5infcode < 11 or " + strMasterToUse + ".piland5infcode > 15 or(" + strMasterToUse + ".piland5unitsa < " + strMin + " or " + strMasterToUse + ".piland5unista > " + strMax + "))and (" + strMasterToUse + ".piland6infcode < 11 or " + strMasterToUse + ".piland6infcode > 15 or(" + strMasterToUse + ".piland6unitsa < " + strMin + " or " + strMasterToUse + ".piland6unista > " + strMax + ")) and (" + strMasterToUse + ".piland7infcode < 11 or " + strMasterToUse + ".piland7infcode > 15 or(" + strMasterToUse + ".piland7unitsa < " + strMin + " or " + strMasterToUse + ".piland7unista > " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 102)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "((" + Statics.strMaster + ".piland1type >= 11 and " + Statics.strMaster + ".piland1type <= 15 and " + Statics.strMaster + ".piland1unitsb >= " + strMin + " and " + Statics.strMaster + ".piland1unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland2type >= 11 and " + Statics.strMaster + ".piland2type <= 15 and " + Statics.strMaster + ".piland2unitsb >= " + strMin + " and " + Statics.strMaster + ".piland2unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland3type >= 11 and " + Statics.strMaster + ".piland3type <= 15 and " + Statics.strMaster + ".piland3unitsb >= " + strMin + " and " + Statics.strMaster + ".piland3unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland4type >= 11 and " + Statics.strMaster + ".piland4type <= 15 and " + Statics.strMaster + ".piland4unitsb >= ";
					strTemp += strMin + " and " + Statics.strMaster + ".piland4unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland5type >= 11 and " + Statics.strMaster + ".piland5type <= 15 and " + Statics.strMaster + ".piland5unitsb >= " + strMin + " and " + Statics.strMaster + ".piland5unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland6type >= 11 and " + Statics.strMaster + ".piland6type <= 15 and " + Statics.strMaster + ".piland6unitsb >= " + strMin + " and " + Statics.strMaster + ".piland6unitsb <= " + strMax + ")or";
					strTemp += "(" + Statics.strMaster + ".piland7type >= 11 and " + Statics.strMaster + ".piland7type <= 15 and " + Statics.strMaster + ".piland7unitsb >= " + strMin + " and " + Statics.strMaster + ".piland7unitsb <= " + strMax + "))";
				}
				else
				{
					strTemp = "((" + Statics.strMaster + ".piland1type < 11 or " + Statics.strMaster + ".piland1type > 15 or(" + Statics.strMaster + ".piland1unitsb < " + strMin + " or " + Statics.strMaster + ".piland1unitsb > " + strMax + "))and(" + Statics.strMaster + ".piland2type < 11 or " + Statics.strMaster + ".piland2type > 15 or(" + Statics.strMaster + ".piland2unitsb < " + strMin + " or " + Statics.strMaster + ".piland2unitsa > " + strMax + "))and(" + Statics.strMaster + ".piland3type < 11 or " + Statics.strMaster + ".piland3type > 15 or(" + Statics.strMaster + ".piland3unitsb < " + strMin + " or " + Statics.strMaster + ".piland3unitsa > " + strMax + "))and(" + Statics.strMaster + ".piland4type < 11 or " + Statics.strMaster + ".piland4type > 15 or(" + Statics.strMaster + ".piland4unitsb < " + strMin + " or " + Statics.strMaster + ".piland4unitsa > " + strMax + "))";
					strTemp += "and(" + Statics.strMaster + ".piland5type < 11 or " + Statics.strMaster + ".piland5type > 15 or(" + Statics.strMaster + ".piland5unitsb < " + strMin + " or " + Statics.strMaster + ".piland5unitsa > " + strMax + "))and (" + Statics.strMaster + ".piland6type < 11 or " + Statics.strMaster + ".piland6type > 15 or(" + Statics.strMaster + ".piland6unitsb < " + strMin + " or " + Statics.strMaster + ".piland6unitsa > " + strMax + ")) and (" + Statics.strMaster + ".piland7type < 11 or " + Statics.strMaster + ".piland7type > 15 or(" + Statics.strMaster + ".piland7unitsb < " + strMin + " or " + Statics.strMaster + ".piland7unitsa > " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 103)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "((" + Statics.strMaster + ".piland1type >= 16 and " + Statics.strMaster + ".piland1type <= 20 and " + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb >= " + strMin + " and " + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland2type >= 16 and " + Statics.strMaster + ".piland2type <= 20 and " + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb >= " + strMin + " and " + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb <= " + strMax + ")";
					strTemp += "or(" + Statics.strMaster + ".piland3type >= 16 and " + Statics.strMaster + ".piland3type <= 20 and " + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb >= " + strMin + " and " + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland4type >= 16 and " + Statics.strMaster + ".piland4type <= 20 and " + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb >= " + strMin + " and " + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb <= " + strMax + ")";
					strTemp += "or(" + Statics.strMaster + ".piland5type >= 16 and " + Statics.strMaster + ".piland5type <= 20 and " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb >= " + strMin + " and " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland6type >= 16 and " + Statics.strMaster + ".piland6type <= 20 and " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb >= " + strMin + " and " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland7type >= 16 and " + Statics.strMaster + ".piland7type <= 20 and " + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb >= " + strMin + " and " + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb <= " + strMax + "))";
				}
				else
				{
					strTemp = "((" + Statics.strMaster + ".piland1type < 16 or " + Statics.strMaster + ".piland1type > 20 or " + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb < " + strMin + " or " + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb > " + strMax + ")and(" + Statics.strMaster + ".piland2type < 16 or " + Statics.strMaster + ".piland2type > 20 or " + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb < " + strMin + " or " + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb > " + strMax + ")and(" + Statics.strMaster + ".piland3type < 16 or " + Statics.strMaster + ".piland3type > 20 or " + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb < " + strMin + " or " + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb > " + strMax + ")";
					strTemp += "and(" + Statics.strMaster + ".piland4type < 16 or " + Statics.strMaster + ".piland4type > 20 or " + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb < " + strMin + " or " + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb > " + strMax + ")and(" + Statics.strMaster + ".piland5type < 16 or " + Statics.strMaster + ".piland5type > 20 or " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb < " + strMin + " or " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb > " + strMax + ")and(" + Statics.strMaster + ".piland6type < 16 or " + Statics.strMaster + ".piland6type > 20 or " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb < ";
					strTemp += strMin + " or " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb > " + strMax + ")and(" + Statics.strMaster + ".piland7type < 16 or " + Statics.strMaster + ".piland7type > 20 or " + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb < " + strMin + " or " + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb > " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 104)
			{
				strMin = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strMin))) * 100, "000000");
				strMax = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strMax))) * 100, "000000");
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "((" + Statics.strMaster + ".piland1type >= 21 and " + Statics.strMaster + ".piland1type <= 41 and " + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb >= " + strMin + " and " + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland2type >= 21 and " + Statics.strMaster + ".piland2type <= 41 and " + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb >= " + strMin + " and " + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland3type >= 21 and " + Statics.strMaster + ".piland3type <= 41 and " + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb >= " + strMin + " and " + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb <= " + strMax + ")";
					strTemp += "or(" + Statics.strMaster + ".piland4type >= 21 and " + Statics.strMaster + ".piland4type <= 41 and " + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb >= " + strMin + " and " + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland5type >= 21 and " + Statics.strMaster + ".piland5type <= 41 and " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb >= " + strMin + " and " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland6type >= 21 and " + Statics.strMaster + ".piland6type <= 41 and " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb >= ";
					strTemp += strMin + " and " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland1type >= 21 and " + Statics.strMaster + ".piland1type <= 41 and " + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb >= " + strMin + " and " + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb <= " + strMax + "))";
				}
				else
				{
					strTemp = "((" + Statics.strMaster + ".piland1type < 21 or " + Statics.strMaster + ".piland1type > 41 or(" + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb < " + strMin + " or " + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb > " + strMax + "))and(" + Statics.strMaster + ".piland2type < 21 or " + Statics.strMaster + ".piland2type > 41 or(" + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb < " + strMin + " or " + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb > " + strMax + "))and(" + Statics.strMaster + ".piland3type < 21 or " + Statics.strMaster + ".piland3type > 41 or(" + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb < " + strMin + " or " + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb > " + strMax + "))";
					strTemp += "and(" + Statics.strMaster + ".piland4type < 21 or " + Statics.strMaster + ".piland4type > 41 or(" + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb < " + strMin + " or " + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb > " + strMax + "))and(" + Statics.strMaster + ".piland5type < 21 or " + Statics.strMaster + ".piland5type > 41 or(" + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb < " + strMin + " or " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb > ";
					strTemp += strMax + "))and(" + Statics.strMaster + ".piland6type < 21 or " + Statics.strMaster + ".piland6type > 41 or(" + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb < " + strMin + " or " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb > " + strMax + "))and(" + Statics.strMaster + ".piland7type < 21 or " + Statics.strMaster + ".piland7type > 41 or(" + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb < " + strMin + " or " + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb > " + strMax + ")))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 105)
			{
				strMin = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strMin))) * 100, "000000");
				strMax = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strMax))) * 100, "000000");
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "((" + Statics.strMaster + ".piland1type >= 42 and " + Statics.strMaster + ".piland1type <= 46 and " + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb >= " + strMin + " and " + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland2type >= 42 and " + Statics.strMaster + ".piland2type <= 46 and " + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb >= " + strMin + " and " + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland3type >= 42 and " + Statics.strMaster + ".piland3type <= 46 and " + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb >= " + strMin + " and " + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb <= " + strMax + ")";
					strTemp += "or(" + Statics.strMaster + ".piland4type >= 42 and " + Statics.strMaster + ".piland4type <= 46 and " + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb >= " + strMin + " and " + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland5type >= 42 and " + Statics.strMaster + ".piland5type <= 46 and " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb >= ";
					strTemp += strMin + " and " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland6type >= 42 and " + Statics.strMaster + ".piland6type <= 46 and " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb >= " + strMin + " and " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb <= " + strMax + ")or(" + Statics.strMaster + ".piland7type >= 42 and " + Statics.strMaster + ".piland7type <= 46 and " + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb >= " + strMin + " and " + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb <= " + strMax + "))";
				}
				else
				{
					strTemp = "((" + Statics.strMaster + ".piland1type < 42 or " + Statics.strMaster + ".piland1type > 46 or(" + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb < " + strMin + " or " + Statics.strMaster + ".piland1unitsa & " + Statics.strMaster + ".piland1unitsb > " + strMax + "))and(" + Statics.strMaster + ".piland2type < 42 or " + Statics.strMaster + ".piland2type > 46 or(" + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb < " + strMin + " or " + Statics.strMaster + ".piland2unitsa & " + Statics.strMaster + ".piland2unitsb > " + strMax + "))and(" + Statics.strMaster + ".piland3type < 42 or " + Statics.strMaster + ".piland3type > 46 or(" + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb < " + strMin + " or " + Statics.strMaster + ".piland3unitsa & " + Statics.strMaster + ".piland3unitsb > " + strMax + "))";
					strTemp += "and(" + Statics.strMaster + ".piland4type < 42 or " + Statics.strMaster + ".piland4type > 46 or(" + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb < " + strMin + " or " + Statics.strMaster + ".piland4unitsa & " + Statics.strMaster + ".piland4unitsb > " + strMax + "))and(" + Statics.strMaster + ".piland5type < 42 or " + Statics.strMaster + ".piland5type > 46 or(" + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb < " + strMin + " or " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb > " + strMax + "))and(" + Statics.strMaster + ".piland6type < 42 or " + Statics.strMaster + ".piland6type > 46 or(" + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb < ";
					strTemp += strMin + " or " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb > " + strMax + "))and(" + Statics.strMaster + ".piland7type < 42 or " + Statics.strMaster + ".piland7type > 46 or(" + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb < " + strMin + " or " + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb > " + strMax + ")))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar >= 106 && vbPorterVar <= 110)
			{
				intLandCode = Statics.intCode - 95;
				strMin = Strings.Format(FCConvert.ToString(Conversion.Val(strMin)), "000");
				strMax = Strings.Format(FCConvert.ToString(Conversion.Val(strMax)), "000");
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + strMasterToUse + ".piland1type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland1unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland1unitsb  <= '" + strMax + "') or (" + strMasterToUse + ".piland2type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland2unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland2unitsb <= '" + strMax + "') or (" + strMasterToUse + ".piland3type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland3unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland3unitsb <= '" + strMax + "')";
					strTemp += " or (" + strMasterToUse + ".piland4type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland4unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland4unitsb <= '" + strMax + "') or (" + strMasterToUse + ".piland5type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland5unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland5unitsb <= '" + strMax + "') or (" + strMasterToUse + ".piland6type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland6unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland6unitsb <= '" + strMax + "') or (" + strMasterToUse + ".piland7type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland7unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland7unitsb <= '" + strMax + "'))";
				}
				else
				{
					strTemp = " ((" + strMasterToUse + ".piland1type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland1type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland1unitsb < '" + strMin + "' or " + strMasterToUse + ".piland1unitsb  > '" + strMax + "'))) and (" + strMasterToUse + ".piland2type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland2type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland2unitsb < '" + strMin + "' or " + strMasterToUse + ".piland2unitsb > '" + strMax + "'))) and (" + strMasterToUse + ".piland3type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland3type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland3unitsb < '" + strMin + "' or " + strMasterToUse + ".piland3unitsb > '" + strMax + "')))";
					strTemp += " and (" + strMasterToUse + ".piland4type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland4type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland4unitsb < '" + strMin + "' or " + strMasterToUse + ".piland4unitsb > '" + strMax + "'))) and (" + strMasterToUse + ".piland5type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland5type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland5unitsb < '" + strMin + "' or " + strMasterToUse + ".piland5unitsb > '" + strMax + "'))) and (" + strMasterToUse + ".piland6type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland6type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland6unitsb < '" + strMin + "' or " + strMasterToUse + ".piland6unitsb > '" + strMax + "'))) and (" + strMasterToUse + ".piland7type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland7type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland7unitsb < '" + strMin + "' or " + strMasterToUse + ".piland7unitsb > '" + strMax + "'))))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedMasterTable = true;
			}
			else if (vbPorterVar == 500)
			{
				intLandCode = tCode.ID;
				if (modGlobalVariables.Statics.LandTypes.FindCode(intLandCode))
				{
					switch (modGlobalVariables.Statics.LandTypes.UnitsType)
					{
						case modREConstants.CNSTLANDTYPEFRONTFOOT:
						case modREConstants.CNSTLANDTYPEFRONTFOOTSCALED:
							{
								strMin = Strings.Format(FCConvert.ToString(Conversion.Val(strMin)), "000");
								strMax = Strings.Format(FCConvert.ToString(Conversion.Val(strMax)), "000");
								if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
								{
									strTemp = " ((" + strMasterToUse + ".piland1type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland1unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland1unitsb  <= '" + strMax + "') or (" + strMasterToUse + ".piland2type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland2unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland2unitsb <= '" + strMax + "') or (" + strMasterToUse + ".piland3type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland3unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland3unitsb <= '" + strMax + "')";
									strTemp += " or (" + strMasterToUse + ".piland4type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland4unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland4unitsb <= '" + strMax + "') or (" + strMasterToUse + ".piland5type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland5unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland5unitsb <= '" + strMax + "') or (" + strMasterToUse + ".piland6type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland6unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland6unitsb <= '" + strMax + "') or (" + strMasterToUse + ".piland7type = " + FCConvert.ToString(intLandCode) + " and " + strMasterToUse + ".piland7unitsb >= '" + strMin + "' and " + strMasterToUse + ".piland7unitsb <= '" + strMax + "'))";
								}
								else
								{
									strTemp = " ((" + strMasterToUse + ".piland1type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland1type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland1unitsb < '" + strMin + "' or " + strMasterToUse + ".piland1unitsb  > '" + strMax + "'))) and (" + strMasterToUse + ".piland2type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland2type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland2unitsb < '" + strMin + "' or " + strMasterToUse + ".piland2unitsb > '" + strMax + "'))) and (" + strMasterToUse + ".piland3type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland3type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland3unitsb < '" + strMin + "' or " + strMasterToUse + ".piland3unitsb > '" + strMax + "')))";
									strTemp += " and (" + Statics.strMaster + ".piland4type <> " + FCConvert.ToString(intLandCode) + " or (" + Statics.strMaster + ".piland4type = " + FCConvert.ToString(intLandCode) + " and (" + Statics.strMaster + ".piland4unitsb < '" + strMin + "' or " + Statics.strMaster + ".piland4unitsb > '" + strMax + "'))) and (" + Statics.strMaster + ".piland5type <> " + FCConvert.ToString(intLandCode) + " or (" + Statics.strMaster + ".piland5type = " + FCConvert.ToString(intLandCode) + " and (" + Statics.strMaster + ".piland5unitsb < '" + strMin + "' or " + Statics.strMaster + ".piland5unitsb > '" + strMax + "'))) and (" + Statics.strMaster + ".piland6type <> " + FCConvert.ToString(intLandCode) + " or (" + Statics.strMaster + ".piland6type = " + FCConvert.ToString(intLandCode) + " and (" + Statics.strMaster + ".piland6unitsb < '" + strMin + "' or " + Statics.strMaster + ".piland6unitsb > '" + strMax + "'))) and (" + Statics.strMaster + ".piland7type <> " + FCConvert.ToString(intLandCode) + " or (" + Statics.strMaster + ".piland7type = " + FCConvert.ToString(intLandCode) + " and (";
									strTemp += strMasterToUse + ".piland7unitsb < '" + strMin + "' or " + strMasterToUse + ".piland7unitsb > '" + strMax + "'))))";
								}
								Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
								Statics.boolUsedMasterTable = true;
								break;
							}
						case modREConstants.CNSTLANDTYPELINEARFEET:
						case modREConstants.CNSTLANDTYPELINEARFEETSCALED:
							{
								strMin = Strings.Format(FCConvert.ToString(Conversion.Val(strMin)), "000000");
								strMax = Strings.Format(FCConvert.ToString(Conversion.Val(strMax)), "000000");
								if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
								{
									strTemp = " ((" + strMasterToUse + ".piland1type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland1unitsa & " + strMasterToUse + ".piland1unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland1unitsa & " + strMasterToUse + ".piland1unitsb & '') <= val('" + strMax + "')) or (" + strMasterToUse + ".piland2type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland2unitsa & " + strMasterToUse + ".piland2unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland2unitsa & " + strMasterToUse + ".piland2unitsb & '') <= val('" + strMax + "')) or (" + strMasterToUse + ".piland3type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland3unitsa & " + strMasterToUse + ".piland3unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland3unitsa & " + strMasterToUse + ".piland3unitsb & '') <= val('" + strMax + "'))";
									strTemp += " or (" + strMasterToUse + ".piland4type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland4unitsa & " + strMasterToUse + ".piland4unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland4unitsa & " + strMasterToUse + ".piland4unitsb & '') <= val('" + strMax + "'))";
									strTemp += " or (" + strMasterToUse + ".piland5type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland5unitsa & " + strMasterToUse + ".piland5unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland5unitsa & " + strMasterToUse + ".piland5unitsb & '') <= val('" + strMax + "'))";
									strTemp += " or (" + strMasterToUse + ".piland6type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland6unitsa & " + strMasterToUse + ".piland6unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland6unitsa & " + strMasterToUse + ".piland6unitsb & '') <= val('" + strMax + "'))";
									strTemp += " or (" + strMasterToUse + ".piland7type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland7unitsa & " + strMasterToUse + ".piland7unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland7unitsa & " + strMasterToUse + ".piland7unitsb & '') <= val('" + strMax + "')))";
								}
								else
								{
									strTemp = " ((" + strMasterToUse + ".piland1type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland1type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland1unitsa & " + strMasterToUse + ".piland1unitsb < '" + strMin + "' or " + strMasterToUse + ".piland1unitsa & " + strMasterToUse + ".piland1unitsb > '" + strMax + "')) and (" + strMasterToUse + ".piland2type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland2type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland2unitsa & " + strMasterToUse + ".piland2unitsb < '" + strMin + "' or " + strMasterToUse + ".piland2unitsa & " + strMasterToUse + ".piland2unitsb > '" + strMax + "')) and (" + strMasterToUse + ".piland3type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland3type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland3unitsa & " + strMasterToUse + ".piland3unitsb < '" + strMin + "' or " + strMasterToUse + ".piland3unitsa & " + strMasterToUse + ".piland3unitsb > '" + strMax + "'))";
									strTemp += " and (" + strMasterToUse + ".piland4type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland4type = " + FCConvert.ToString(intLandCode) + " and (" + strMasterToUse + ".piland4unitsa & " + strMasterToUse + ".piland4unitsb < '" + strMin + "' or " + strMasterToUse + ".piland4unitsa & " + strMasterToUse + ".piland4unitsb > '";
									strTemp += strMax + "')) and (" + Statics.strMaster + ".piland5type <> " + FCConvert.ToString(intLandCode) + " or (" + Statics.strMaster + ".piland5type = " + FCConvert.ToString(intLandCode) + " and (" + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb < '" + strMin + "' or " + Statics.strMaster + ".piland5unitsa & " + Statics.strMaster + ".piland5unitsb > '" + strMax + "')) and (" + Statics.strMaster + ".piland6type <> " + FCConvert.ToString(intLandCode) + " or (" + Statics.strMaster + ".piland6type = " + FCConvert.ToString(intLandCode) + " and (" + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb < '" + strMin + "' or " + Statics.strMaster + ".piland6unitsa & " + Statics.strMaster + ".piland6unitsb > '" + strMax + "')) and (" + Statics.strMaster + ".piland7type <> " + FCConvert.ToString(intLandCode) + " or (" + Statics.strMaster + ".piland7type = " + FCConvert.ToString(intLandCode) + " and (" + Statics.strMaster + ".piland7unitsa & " + Statics.strMaster + ".piland7unitsb < '" + strMin + "' or " + Statics.strMaster + ".piland7unitsa & ";
									strTemp += strMasterToUse + ".piland7unitsb > '" + strMax + "')))";
								}
								Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
								Statics.boolUsedMasterTable = true;
								break;
							}
						default:
							{
								strMin = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strMin))) * 100, "000000");
								strMax = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strMax))) * 100, "000000");
								if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
								{
									// strTemp = " ((" & strMaster & ".piland1type = " & intLandCode & " and " & strMaster & ".piland1unitsa & " & strMaster & ".piland1unitsb >= '" & strmin & "' and " & strMaster & ".piland1unitsa & " & strMaster & ".piland1unitsb <= '" & strmax & "') or (" & strMaster & ".piland2type = " & intLandCode & " and " & strMaster & ".piland2unitsa & " & strMaster & ".piland2unitsb >= '" & strmin & "' and " & strMaster & ".piland2unitsa & " & strMaster & ".piland2unitsb <= '" & strmax & "') or (" & strMaster & ".piland3type = " & intLandCode & " and " & strMaster & ".piland3unitsa & " & strMaster & ".piland3unitsb >= '" & strmin & "' and " & strMaster & ".piland3unitsa & " & strMaster & ".piland3unitsb <= '" & strmax & "')"
									// strTemp = strTemp & " or (" & strMaster & ".piland4type = " & intLandCode & " and " & strMaster & ".piland4unitsa & " & strMaster & ".piland4unitsb >= '" & strmin & "' and " & strMaster & ".piland4unitsa & " & strMaster & ".piland4unitsb <= '" & strmax & "') or (" & strMaster & ".piland5type = " & intLandCode & " and " & strMaster & ".piland5unitsa & " & strMaster & ".piland5unitsb >= '" & strmin & "' and " & strMaster & ".piland5unitsa & " & strMaster & ".piland5unitsb <= '" & strmax & "') or (" & strMaster & ".piland6type = " & intLandCode & " and " & strMaster & ".piland6unitsa & " & strMaster & ".piland6unitsb >= '" & strmin & "' and " & strMaster & ".piland6unitsa & " & strMaster & ".piland6unitsb <= '" & strmax & "') or (" & strMaster & ".piland7type = " & intLandCode & " and " & strMaster & ".piland7unitsa & " & strMaster & ".piland7unitsb >= '" & strmin & "' and " & strMaster & ".piland7unitsa & " & strMaster & ".piland7unitsb <= '" & strmax & "'))"
									strTemp = " ((" + strMasterToUse + ".piland1type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland1unitsa & " + strMasterToUse + ".piland1unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland1unitsa & " + strMasterToUse + ".piland1unitsb & '') <= val('" + strMax + "')) or (" + strMasterToUse + ".piland2type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland2unitsa & " + strMasterToUse + ".piland2unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland2unitsa & " + strMasterToUse + ".piland2unitsb & '') <= val('" + strMax + "')) or (" + strMasterToUse + ".piland3type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland3unitsa & " + strMasterToUse + ".piland3unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland3unitsa & " + strMasterToUse + ".piland3unitsb & '') <= val('" + strMax + "'))";
									strTemp += " or (" + strMasterToUse + ".piland4type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland4unitsa & " + strMasterToUse + ".piland4unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland4unitsa & " + strMasterToUse + ".piland4unitsb & '') <= val('" + strMax + "'))";
									strTemp += " or (" + strMasterToUse + ".piland5type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland5unitsa & " + strMasterToUse + ".piland5unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland5unitsa & " + strMasterToUse + ".piland5unitsb & '') <= val('" + strMax + "')) or (" + strMasterToUse + ".piland6type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland6unitsa & " + strMasterToUse + ".piland6unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland6unitsa & " + strMasterToUse + ".piland6unitsb & '') <= val('" + strMax + "')) or (" + strMasterToUse + ".piland7type = " + FCConvert.ToString(intLandCode) + " and val(" + strMasterToUse + ".piland7unitsa & " + strMasterToUse + ".piland7unitsb & '') >= val('" + strMin + "') and val(" + strMasterToUse + ".piland7unitsa & " + strMasterToUse + ".piland7unitsb & '') <= val('" + strMax + "')))";
								}
								else
								{
									// strTemp = " ((" & strMaster & ".piland1type <> " & intLandCode & " or (" & strMaster & ".piland1type = " & intLandCode & " and (" & strMaster & ".piland1unitsa & " & strMaster & ".piland1unitsb < '" & strmin & "' or " & strMaster & ".piland1unitsa & " & strMaster & ".piland1unitsb > '" & strmax & "')) and (" & strMaster & ".piland2type <> " & intLandCode & " or (" & strMaster & ".piland2type = " & intLandCode & " and (" & strMaster & ".piland2unitsa & " & strMaster & ".piland2unitsb < '" & strmin & "' or " & strMaster & ".piland2unitsa & " & strMaster & ".piland2unitsb > '" & strmax & "')) and (" & strMaster & ".piland3type <> " & intLandCode & " or (" & strMaster & ".piland3type = " & intLandCode & " and (" & strMaster & ".piland3unitsa & " & strMaster & ".piland3unitsb < '" & strmin & "' or " & strMaster & ".piland3unitsa & " & strMaster & ".piland3unitsb > '" & strmax & "'))"
									// strTemp = strTemp & " and (" & strMaster & ".piland4type <> " & intLandCode & " or (" & strMaster & ".piland4type = " & intLandCode & " and (" & strMaster & ".piland4unitsa & " & strMaster & ".piland4unitsb < '" & strmin & "' or " & strMaster & ".piland4unitsa & " & strMaster & ".piland4unitsb > '"
									// strTemp = strTemp & strmax & "')) and (" & strMaster & ".piland5type <> " & intLandCode & " or (" & strMaster & ".piland5type = " & intLandCode & " and (" & strMaster & ".piland5unitsa & " & strMaster & ".piland5unitsb < '" & strmin & "' or " & strMaster & ".piland5unitsa & " & strMaster & ".piland5unitsb > '" & strmax & "')) and (" & strMaster & ".piland6type <> " & intLandCode & " or (" & strMaster & ".piland6type = " & intLandCode & " and (" & strMaster & ".piland6unitsa & " & strMaster & ".piland6unitsb < '" & strmin & "' or " & strMaster & ".piland6unitsa & " & strMaster & ".piland6unitsb > '" & strmax & "')) and (" & strMaster & ".piland7type <> " & intLandCode & " or (" & strMaster & ".piland7type = " & intLandCode & " and (" & strMaster & ".piland7unitsa & " & strMaster & ".piland7unitsb < '" & strmin & "' or " & strMaster & ".piland7unitsa & " & strMaster & ".piland7unitsb > '" & strmax & "')))"
									strTemp = " ((" + strMasterToUse + ".piland1type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland1type = " + FCConvert.ToString(intLandCode) + " and (val(";
									strTemp += strMasterToUse + ".piland1unitsa & " + strMasterToUse + ".piland1unitsb & '') < val('" + strMin + "') or val(" + strMasterToUse + ".piland1unitsa & " + strMasterToUse + ".piland1unitsb & '') > val('" + strMax + "')))) and (" + strMasterToUse + ".piland2type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland2type = " + FCConvert.ToString(intLandCode) + " and (val(" + strMasterToUse + ".piland2unitsa & " + strMasterToUse + ".piland2unitsb & '') < val('" + strMin + "') or val(" + strMasterToUse + ".piland2unitsa & " + strMasterToUse + ".piland2unitsb & '') > val('" + strMax + "')))) and (" + strMasterToUse + ".piland3type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland3type = " + FCConvert.ToString(intLandCode) + " and (val(" + strMasterToUse + ".piland3unitsa & " + strMasterToUse + ".piland3unitsb & '') < val('" + strMin + "') or val(" + strMasterToUse + ".piland3unitsa & " + strMasterToUse + ".piland3unitsb & '') > val('" + strMax + "'))))";
									strTemp += " and (" + strMasterToUse + ".piland4type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland4type = " + FCConvert.ToString(intLandCode) + " and (val(" + strMasterToUse + ".piland4unitsa & " + strMasterToUse + ".piland4unitsb & '') < val('" + strMin + "') or val(" + strMasterToUse + ".piland4unitsa & " + strMasterToUse + ".piland4unitsb & '') > val('";
									strTemp += strMax + "')))) and (" + strMasterToUse + ".piland5type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland5type = " + FCConvert.ToString(intLandCode) + " and (val(" + strMasterToUse + ".piland5unitsa & " + strMasterToUse + ".piland5unitsb & '') < val('" + strMin + "') or val(" + strMasterToUse + ".piland5unitsa & " + strMasterToUse + ".piland5unitsb & '') > val('" + strMax + "')))) and (" + strMasterToUse + ".piland6type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland6type = " + FCConvert.ToString(intLandCode) + " and (val(" + strMasterToUse + ".piland6unitsa & " + strMasterToUse + ".piland6unitsb & '') < val('" + strMin + "') or val(" + strMasterToUse + ".piland6unitsa & " + strMasterToUse + ".piland6unitsb & '') > val('" + strMax + "')))) and (" + strMasterToUse + ".piland7type <> " + FCConvert.ToString(intLandCode) + " or (" + strMasterToUse + ".piland7type = " + FCConvert.ToString(intLandCode) + " and (val(" + strMasterToUse + ".piland7unitsa & " + strMasterToUse + ".piland7unitsb & '') < val('" + strMin + "') or val(";
									strTemp += strMasterToUse + ".piland7unitsa & " + strMasterToUse + ".piland7unitsb & '') > val('" + strMax + "')))))";
								}
								Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
								Statics.boolUsedMasterTable = true;
								break;
							}
					}
					//end switch
				}
			}
			else if (vbPorterVar == 201)
			{
				if (!boolSale)
				{
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount in (select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount  having sum(" + Statics.strDwelling + ".diunitsdwelling) between " + strMin + " and " + strMax + "))";
					}
					else
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount not in ((select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount having sum(" + Statics.strDwelling + ".diunitsdwelling) between " + strMin + " and " + strMax + "))";
					}
				}
				else
				{
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount in (select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount,srdwelling.saledate having sum(" + Statics.strDwelling + ".diunitsdwelling) between " + strMin + " and " + strMax + "))";
					}
					else
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount not in ((select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount, srdwelling.saledate having sum(" + Statics.strDwelling + ".diunitsdwelling) between " + strMin + " and " + strMax + "))";
					}
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedDwellingTable = true;
			}
			else if (vbPorterVar == 203)
			{
				if (!boolSale)
				{
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount in (select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount having sum(" + Statics.strDwelling + ".diunitsother) between " + strMin + " and " + strMax + "))";
					}
					else
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount not in ((select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount having sum(" + Statics.strDwelling + ".diunitsother) between " + strMin + " and " + strMax + "))";
					}
				}
				else
				{
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount in (select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount,saledate having sum(" + Statics.strDwelling + ".diunitsother) between " + strMin + " and " + strMax + "))";
					}
					else
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount not in ((select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount,saledate having sum(" + Statics.strDwelling + ".diunitsother) between " + strMin + " and " + strMax + "))";
					}
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedDwellingTable = true;
			}
			else if (vbPorterVar == 208)
			{
				if (!boolSale)
				{
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount in (select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount having sum(" + Statics.strDwelling + ".disfmasonry) between " + strMin + " and " + strMax + "))";
					}
					else
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount not in ((select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount having sum(" + Statics.strDwelling + ".disfmasonry) between " + strMin + " and " + strMax + "))";
					}
				}
				else
				{
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount in (select " + Statics.strDwelling + ".rsaccount,saledate from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount,saledate having sum(" + Statics.strDwelling + ".disfmasonry) between " + strMin + " and " + strMax + "))";
					}
					else
					{
						strTemp = "(" + Statics.strDwelling + ".rsaccount not in ((select " + Statics.strDwelling + ".rsaccount,saledate from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount,saledate having sum(" + Statics.strDwelling + ".disfmasonry) between " + strMin + " and " + strMax + "))";
					}
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedDwellingTable = true;
			}
			else if (vbPorterVar == 218)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "(" + Statics.strDwelling + ".rsaccount in (select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount having sum(" + Statics.strDwelling + ".disfbsmtliving) between " + strMin + " and " + strMax + "))";
				}
				else
				{
					strTemp = "(" + Statics.strDwelling + ".rsaccount not in ((select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount having sum(" + Statics.strDwelling + ".disfbsmtliving) between " + strMin + " and " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedDwellingTable = true;
			}
			else if (vbPorterVar == 220)
			{
				// finished basement grade and factor
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " " + Statics.strDwelling + ".dibsmtfingrade1 & " + Statics.strDwelling + ".dibsmtfingrade2 >= " + strMin + " and " + Statics.strDwelling + ".dibsmtfingrade1 & " + Statics.strDwelling + ".dibsmtfingrade2 <= " + strMax;
				}
				else
				{
					strTemp = " " + Statics.strDwelling + ".dibsmtfingrade1 & " + Statics.strDwelling + ".dibsmtfingrade2 < " + strMin + " or " + Statics.strDwelling + ".dibsmtfingrade1 & " + Statics.strDwelling + ".dibsmtfingrade2 > " + strMax;
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedDwellingTable = true;
			}
			else if (vbPorterVar == 238)
			{
				// dwelling grade and factor
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " " + Statics.strDwelling + ".digrade1 & " + Statics.strDwelling + ".digrade2 >= " + strMin + " and " + Statics.strDwelling + ".digrade1 & " + Statics.strDwelling + ".digrade2 <= " + strMax;
				}
				else
				{
					strTemp = " " + Statics.strDwelling + ".digrade1 & " + Statics.strDwelling + ".digrade2 < " + strMin + " or " + Statics.strDwelling + ".digrade1 & " + Statics.strDwelling + ".digrade2 > " + strMax;
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedDwellingTable = true;
			}
			else if (vbPorterVar == 239)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "(" + Statics.strDwelling + ".rsaccount in (select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount having sum(" + Statics.strDwelling + ".disqft) between " + strMin + " and " + strMax + "))";
				}
				else
				{
					strTemp = "(" + Statics.strDwelling + ".rsaccount not in ((select " + Statics.strDwelling + ".rsaccount from " + Statics.strDwelling + " group by " + Statics.strDwelling + ".rsaccount having sum(" + Statics.strDwelling + ".disqft) between " + strMin + " and " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedDwellingTable = true;
			}
			else if (vbPorterVar == 249)
			{
				// date inspected
				intTempYear = FCConvert.ToDateTime(strMax).Year;
				intTempMonth = FCConvert.ToDateTime(strMax).Month;
				if (intTempMonth == 12)
				{
					intTempMonth = 1;
					intTempYear += 1;
				}
				else
				{
					intTempMonth += 1;
				}
				strMax = FCConvert.ToString(intTempMonth) + "/1/" + FCConvert.ToString(intTempYear);
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " " + Statics.strOutbuilding + ".oiinspectiondate >= '" + strMin + "' and " + Statics.strOutbuilding + ".oiinspectiondate < '" + strMax + "'";
				}
				else
				{
					strTemp = " " + Statics.strOutbuilding + ".oiinspectiondate < '" + strMin + "' or " + Statics.strOutbuilding + ".oiinspectiondate >= '" + strMax + "'";
				}
				Statics.boolUsedOutBuildingTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 301)
			{
				// occupancy code
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".occ1 >= " + strMin + " and " + Statics.strCommercial + ".occ1 <= " + strMax + ")or(" + Statics.strCommercial + ".occ2 >= " + strMin + " and " + Statics.strCommercial + ".occ2 <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".occ1 < " + strMin + " or " + Statics.strCommercial + ".occ1 > " + strMax + ")and(" + Statics.strCommercial + ".occ2 < " + strMin + " or " + Statics.strCommercial + ".occ2 > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 302)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "(" + Statics.strCommercial + ".rsaccount in (select " + Statics.strCommercial + ".rsaccount from " + Statics.strCommercial + " group by " + Statics.strCommercial + ".rsaccount having sum(" + Statics.strCommercial + ".dwel1) + sum(" + Statics.strCommercial + ".dwel2) between " + strMin + " and " + strMax + "))";
				}
				else
				{
					strTemp = "(" + Statics.strCommercial + ".rsaccount not in ((select " + Statics.strCommercial + ".rsaccount from " + Statics.strCommercial + " group by " + Statics.strCommercial + ".rsaccount having sum(" + Statics.strCommercial + ".dwel1) + sum(" + Statics.strCommercial + ".dwel2) between " + strMin + " and " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedCommercialTable = true;
			}
			else if (vbPorterVar == 303)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".dwel1 >= " + strMin + " and " + Statics.strCommercial + ".dwel1 <= " + strMax + ")or(" + Statics.strCommercial + ".dwel2 >= " + strMin + " and " + Statics.strCommercial + ".dwel2 <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".dwel1 < " + strMin + " or " + Statics.strCommercial + ".dwel1 > " + strMax + ")and(" + Statics.strCommercial + ".dwel2 < " + strMin + " or " + Statics.strCommercial + ".dwel2 > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 304)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1class >= " + strMin + " and " + Statics.strCommercial + ".c1class <= " + strMax + ")or(" + Statics.strCommercial + ".c2class >= " + strMin + " and " + Statics.strCommercial + ".c2class <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1class < " + strMin + " or " + Statics.strCommercial + ".c1class > " + strMax + ")and(" + Statics.strCommercial + ".c2class < " + strMin + " or " + Statics.strCommercial + ".c2class > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 305)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1quality >= " + strMin + " and " + Statics.strCommercial + ".c1quality <= " + strMax + ")or(" + Statics.strCommercial + ".c2quality >= " + strMin + " and " + Statics.strCommercial + ".c2quality <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1quality < " + strMin + " or " + Statics.strCommercial + ".c1quality > " + strMax + ")and(" + Statics.strCommercial + ".c2quality < " + strMin + " or " + Statics.strCommercial + ".c2quality > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 306)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1grade >= " + strMin + " and " + Statics.strCommercial + ".c1grade <= " + strMax + ") or (" + Statics.strCommercial + ".c2grade >= " + strMin + " and " + Statics.strCommercial + ".c2grade <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1grade < " + strMin + " or " + Statics.strCommercial + ".c1grade > " + strMax + ") and (" + Statics.strCommercial + ".c2grade < " + strMin + " or " + Statics.strCommercial + ".c2grade > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 307)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1extwalls >= " + strMin + " and " + Statics.strCommercial + ".c1extwalls <= " + strMax + ")or(" + Statics.strCommercial + ".c2extwalls >= " + strMin + " and " + Statics.strCommercial + ".c2extwalls <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1extwalls < " + strMin + " or " + Statics.strCommercial + ".c1extwalls > " + strMax + ")and(" + Statics.strCommercial + ".c2extwalls < " + strMin + " or " + Statics.strCommercial + ".c2extwalls > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 308)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1stories >= " + strMin + " and " + Statics.strCommercial + ".c1stories <= " + strMax + ")or(" + Statics.strCommercial + ".c2stories >= " + strMin + " and " + Statics.strCommercial + ".c2stories <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1stories < " + strMin + " or " + Statics.strCommercial + ".c1stories > " + strMax + ")and(" + Statics.strCommercial + ".c2stories < " + strMin + " or " + Statics.strCommercial + ".c2stories > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 309)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1height >= " + strMin + " and " + Statics.strCommercial + ".c1height <= " + strMax + ")or(" + Statics.strCommercial + ".c2height >= " + strMin + " and " + Statics.strCommercial + ".c2height <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1height < " + strMin + " or " + Statics.strCommercial + ".c1height > " + strMax + ")and(" + Statics.strCommercial + ".c2height < " + strMin + " or " + Statics.strCommercial + ".c2height > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 310)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "(" + Statics.strCommercial + ".rsaccount in (select " + Statics.strCommercial + ".rsaccount from " + Statics.strCommercial + " group by " + Statics.strCommercial + ".rsaccount having sum(" + Statics.strCommercial + ".c1floor) + sum(" + Statics.strCommercial + ".c2floor) between " + strMin + " and " + strMax + "))";
				}
				else
				{
					strTemp = "(" + Statics.strCommercial + ".rsaccount not in ((select " + Statics.strCommercial + ".rsaccount from " + Statics.strCommercial + " group by " + Statics.strCommercial + ".rsaccount having sum(" + Statics.strCommercial + ".c1floor) + sum(" + Statics.strCommercial + ".c2floor) between " + strMin + " and " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedCommercialTable = true;
			}
			else if (vbPorterVar == 311)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1floor >= " + strMin + " and " + Statics.strCommercial + ".c1floor <= " + strMax + ")or(" + Statics.strCommercial + ".c2floor >= " + strMin + " and " + Statics.strCommercial + ".c2floor <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1floor < " + strMin + " or " + Statics.strCommercial + ".c1floor > " + strMax + ")and(" + Statics.strCommercial + ".c2floor < " + strMin + " or " + Statics.strCommercial + ".c2floor > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 312)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1perimeter >= " + strMin + " and " + Statics.strCommercial + ".c1perimeter <= " + strMax + ")or(" + Statics.strCommercial + ".c2perimeter >= " + strMin + " and " + Statics.strCommercial + ".c2perimeter <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1perimeter < " + strMin + " or " + Statics.strCommercial + ".c1perimeter > " + strMax + ")and(" + Statics.strCommercial + ".c2perimeter < " + strMin + " or " + Statics.strCommercial + ".c2perimeter > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 313)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1heat >= " + strMin + " and " + Statics.strCommercial + ".c1heat <= " + strMax + ")or(" + Statics.strCommercial + ".c2heat >= " + strMin + " and " + Statics.strCommercial + ".c2heat <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1heat < " + strMin + " or " + Statics.strCommercial + ".c1heat > " + strMax + ")and(" + Statics.strCommercial + ".c2heat < " + strMin + " or " + Statics.strCommercial + ".c2heat > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 314)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1built >= " + strMin + " and " + Statics.strCommercial + ".c1built <= " + strMax + ")or(" + Statics.strCommercial + ".c2built >= " + strMin + " and " + Statics.strCommercial + ".c2built <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1built < " + strMin + " or " + Statics.strCommercial + ".c1built > " + strMax + ")and(" + Statics.strCommercial + ".c2built < " + strMin + " or " + Statics.strCommercial + ".c2built > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 315)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1remodel >= " + strMin + " and " + Statics.strCommercial + ".c1remodel <= " + strMax + ")or(" + Statics.strCommercial + ".c2remodel >= " + strMin + " and " + Statics.strCommercial + ".c2remodel <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1remodel < " + strMin + " or " + Statics.strCommercial + ".c1remodel > " + strMax + ")and(" + Statics.strCommercial + ".c2remodel < " + strMin + " or " + Statics.strCommercial + ".c2remodel > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 316)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1condition >= " + strMin + " and " + Statics.strCommercial + ".c1condition <= " + strMax + ")or(" + Statics.strCommercial + ".c2condition >= " + strMin + " and " + Statics.strCommercial + ".c2condition <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1condition < " + strMin + " or " + Statics.strCommercial + ".c1condition > " + strMax + ")and(" + Statics.strCommercial + ".c2condition < " + strMin + " or " + Statics.strCommercial + ".c2condition > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 317)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1phys >= " + strMin + " and " + Statics.strCommercial + ".c1phys <= " + strMax + ")or(" + Statics.strCommercial + ".c2phys >= " + strMin + " and " + Statics.strCommercial + ".c2phys <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1phys < " + strMin + " or " + Statics.strCommercial + ".c1phys > " + strMax + ")and(" + Statics.strCommercial + ".c2phys < " + strMin + " or " + Statics.strCommercial + ".c2phys > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 318)
			{
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strCommercial + ".c1funct >= " + strMin + " and " + Statics.strCommercial + ".c1funct <= " + strMax + ")or(" + Statics.strCommercial + ".c2funct >= " + strMin + " and " + Statics.strCommercial + ".c2funct <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strCommercial + ".c1funct < " + strMin + " or " + Statics.strCommercial + ".c1funct > " + strMax + ")and(" + Statics.strCommercial + ".c2funct < " + strMin + " or " + Statics.strCommercial + ".c2funct > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 320)
			{
				// square footage by account
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "(" + Statics.strCommercial + ".rsaccount in (select " + Statics.strCommercial + ".rsaccount from " + Statics.strCommercial + " group by " + Statics.strCommercial + ".rsaccount having sum(val(" + Statics.strCommercial + ".c1floor & '') * val(" + Statics.strCommercial + ".c1stories & '')) + sum(val(" + Statics.strCommercial + ".c2floor & '') * val(" + Statics.strCommercial + ".c2stories & '')) between " + strMin + " and " + strMax + "))";
				}
				else
				{
					strTemp = "(" + Statics.strCommercial + ".rsaccount not in ((select " + Statics.strCommercial + ".rsaccount from " + Statics.strCommercial + " group by " + Statics.strCommercial + ".rsaccount having sum(val(" + Statics.strCommercial + ".c1floor & '') * val(" + Statics.strCommercial + ".c1stories & '')) + sum(val(" + Statics.strCommercial + ".c2floor & '') * val(" + Statics.strCommercial + ".c2stories & '')) between " + strMin + " and " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedCommercialTable = true;
			}
			else if (vbPorterVar == 321)
			{
				// square footage by card
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((val(" + Statics.strCommercial + ".c1floor & '') * val(" + Statics.strCommercial + ".c1stories & '') >= " + strMin + " and val(" + Statics.strCommercial + ".c1floor & '') * val(" + Statics.strCommercial + ".c1stories & '') <= " + strMax + ") or (val(" + Statics.strCommercial + ".c2floor & '') * val(" + Statics.strCommercial + ".c2stories & '') >= " + strMin + " and val(" + Statics.strCommercial + ".c2floor & '') * val(" + Statics.strCommercial + ".c2stories & '') <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((val(" + Statics.strCommercial + ".c1floor & '') * val(" + Statics.strCommercial + ".c1stories & '') < " + strMin + " or val(" + Statics.strCommercial + ".c1floor & '') * val(" + Statics.strCommercial + ".c1stories & '') > " + strMax + ")and(val(" + Statics.strCommercial + ".c2floor & '') * val(" + Statics.strCommercial + ".c2stories & '') < " + strMin + " or val(" + Statics.strCommercial + ".c2floor & '') * val(" + Statics.strCommercial + ".c2stories & '') > " + strMax + "))";
				}
				Statics.boolUsedCommercialTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 322)
			{
				// total sqft by account
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = "(" + Statics.strCommercial + ".rsaccount in (select " + Statics.strCommercial + ".rsaccount from " + Statics.strCommercial + " group by " + Statics.strCommercial + ".rsaccount having sum(val(" + Statics.strCommercial + ".sqft & '') )  between " + strMin + " and " + strMax + "))";
				}
				else
				{
					strTemp = "(" + Statics.strCommercial + ".rsaccount not in ((select " + Statics.strCommercial + ".rsaccount from " + Statics.strCommercial + " group by " + Statics.strCommercial + ".rsaccount having sum(val(" + Statics.strCommercial + ".sqft & '') ) between " + strMin + " and " + strMax + "))";
				}
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.boolUsedCommercialTable = true;
			}
			else if (vbPorterVar == 401)
			{
				// outbuilding type
				// OutMin = strmin
				// outmax = strmax
				strMin = FCConvert.ToString(Conversion.Val(strMin));
				strMax = FCConvert.ToString(Conversion.Val(strMax));
				Statics.OutMin = strMin;
				Statics.outmax = strMax;
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					strTemp = " ((" + Statics.strOutbuilding + ".oitype1 >= " + strMin + " and " + Statics.strOutbuilding + ".oitype1 <= " + strMax + ") or (" + Statics.strOutbuilding + ".oitype2 >= " + strMin + " and " + Statics.strOutbuilding + ".oitype2 <= " + strMax + ")or(" + Statics.strOutbuilding + ".oitype3 >= " + strMin + " and " + Statics.strOutbuilding + ".oitype3 <= " + strMax + ")or(" + Statics.strOutbuilding + ".oitype4 >= " + strMin + " and " + Statics.strOutbuilding + ".oitype4 <= " + strMax + ")or(" + Statics.strOutbuilding + ".oitype5 >= " + strMin + " and " + Statics.strOutbuilding + ".oitype5 <= " + strMax + ")";
					strTemp += " or(" + Statics.strOutbuilding + ".oitype6 >= " + strMin + " and " + Statics.strOutbuilding + ".oitype6 <= " + strMax + ")or(" + Statics.strOutbuilding + ".oitype7 >= " + strMin + " and " + Statics.strOutbuilding + ".oitype7 <= " + strMax + ")or(" + Statics.strOutbuilding + ".oitype8 >= " + strMin + " and " + Statics.strOutbuilding + ".oitype8 <= " + strMax + ")or(" + Statics.strOutbuilding + ".oitype9 >= " + strMin + " and " + Statics.strOutbuilding + ".oitype9 <= " + strMax + ")or(" + Statics.strOutbuilding + ".oitype10 >= " + strMin + " and " + Statics.strOutbuilding + ".oitype10 <= " + strMax + "))";
				}
				else
				{
					strTemp = " ((" + Statics.strOutbuilding + ".oitype1 < " + strMin + " or " + Statics.strOutbuilding + ".oitype1 > " + strMax + ") and (" + Statics.strOutbuilding + ".oitype2 < " + strMin + " or " + Statics.strOutbuilding + ".oitype2 > " + strMax + ")and(" + Statics.strOutbuilding + ".oitype3 < " + strMin + " or " + Statics.strOutbuilding + ".oitype3 > " + strMax + ")and(" + Statics.strOutbuilding + ".oitype4 < " + strMin + " or " + Statics.strOutbuilding + ".oitype4 > " + strMax + ")and(" + Statics.strOutbuilding + ".oitype5 < " + strMin + " or " + Statics.strOutbuilding + ".oitype5 > " + strMax + ")";
					strTemp += " and(" + Statics.strOutbuilding + ".oitype6 < " + strMin + " or " + Statics.strOutbuilding + ".oitype6 > " + strMax + ")and(" + Statics.strOutbuilding + ".oitype7 < " + strMin + " or " + Statics.strOutbuilding + ".oitype7 > " + strMax + ")and(" + Statics.strOutbuilding + ".oitype8 < " + strMin + " or " + Statics.strOutbuilding + ".oitype8 > " + strMax + ")and(" + Statics.strOutbuilding + ".oitype9 < " + strMin + " or " + Statics.strOutbuilding + ".oitype9 > " + strMax + ")and(" + Statics.strOutbuilding + ".oitype10 < " + strMin + " or " + Statics.strOutbuilding + ".oitype10 > " + strMax + "))";
				}
				Statics.boolUsedOutBuildingTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
				Statics.strOutQry = " " + Statics.strOutbuilding + ".oitype1  >= " + strMin + " and " + Statics.strOutbuilding + ".oitype1  <= " + strMax;
				Statics.intPlace = Strings.InStr(1, Statics.strOutQry, ".oitype1  <=", CompareConstants.vbTextCompare);
				Statics.intPlace += 7;
				Statics.bool401CodePresent = true;
			}
			else if ((vbPorterVar == 402) || (vbPorterVar == 403) || (vbPorterVar == 405) || (vbPorterVar == 406) || (vbPorterVar == 407))
			{
				// yearbuilt
				switch (Statics.intCode)
				{
					case 402:
						{
							strFieldName = "oiyear";
							break;
						}
					case 403:
						{
							strFieldName = "oiunits";
							break;
						}
					case 405:
						{
							strFieldName = "oicond";
							break;
						}
					case 406:
						{
							strFieldName = "oipctphys";
							break;
						}
					case 407:
						{
							strFieldName = "oipctfunct";
							break;
						}
				}
				//end switch
				strTemp = " (";
				for (x = 1; x <= 9; x++)
				{
					strTemp += "(";
					if (Statics.bool401CodePresent)
					{
						if (Strings.UCase(Statics.strOutbuilding) != "SROUTBUILDING")
						{
							fecherFoundation.Strings.MidSet(ref Statics.strOutQry, 20, 2, FCConvert.ToString(x) + " ");
							fecherFoundation.Strings.MidSet(ref Statics.strOutQry, Statics.intPlace, 2, FCConvert.ToString(x) + " ");
						}
						else
						{
							fecherFoundation.Strings.MidSet(ref Statics.strOutQry, 22, 2, FCConvert.ToString(x) + " ");
							fecherFoundation.Strings.MidSet(ref Statics.strOutQry, Statics.intPlace, 2, FCConvert.ToString(x) + " ");
						}
						strTemp += Statics.strOutQry;
					}
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						if (Statics.bool401CodePresent)
						{
							strTemp += " and " + Statics.strOutbuilding + "." + strFieldName + FCConvert.ToString(x) + " >= " + strMin + " and " + Statics.strOutbuilding + "." + strFieldName + FCConvert.ToString(x) + " <= " + strMax;
							strTemp += ") or";
						}
						else
						{
							strTemp += "  " + Statics.strOutbuilding + "." + strFieldName + FCConvert.ToString(x) + " >= " + strMin + " and " + Statics.strOutbuilding + "." + strFieldName + FCConvert.ToString(x) + " <= " + strMax;
							strTemp += ") or";
						}
					}
					else
					{
						strTemp += " and (" + Statics.strOutbuilding + "." + strFieldName + FCConvert.ToString(x) + " < " + strMin + " or " + Statics.strOutbuilding + "." + strFieldName + FCConvert.ToString(x) + " > " + strMax + ")";
						strTemp += ") and";
					}
				}
				// x
				strTemp += "(";
				if (Statics.bool401CodePresent)
				{
					if (Strings.UCase(Statics.strOutbuilding) != "SROUTBUILDING")
					{
						fecherFoundation.Strings.MidSet(ref Statics.strOutQry, 20, 2, FCConvert.ToString(10));
						fecherFoundation.Strings.MidSet(ref Statics.strOutQry, Statics.intPlace, 2, FCConvert.ToString(10));
					}
					else
					{
						fecherFoundation.Strings.MidSet(ref Statics.strOutQry, 22, 2, FCConvert.ToString(10));
						fecherFoundation.Strings.MidSet(ref Statics.strOutQry, Statics.intPlace, 2, FCConvert.ToString(10));
					}
					strTemp += Statics.strOutQry;
				}
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					if (Statics.bool401CodePresent)
					{
						strTemp += " and " + Statics.strOutbuilding + "." + strFieldName + "10 >= " + strMin + " and " + Statics.strOutbuilding + "." + strFieldName + "10 <= " + strMax + "))";
					}
					else
					{
						strTemp += "  " + Statics.strOutbuilding + "." + strFieldName + "10 >= " + strMin + " and " + Statics.strOutbuilding + "." + strFieldName + "10 <= " + strMax + "))";
					}
				}
				else
				{
					strTemp += " and (" + Statics.strOutbuilding + "." + strFieldName + "10 < " + strMin + " or " + Statics.strOutbuilding + "." + strFieldName + "10 > " + strMax + ")))";
				}
				Statics.boolUsedOutBuildingTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			else if (vbPorterVar == 404)
			{
				strTemp = " (";
				Statics.intPlace = Strings.InStr(1, Statics.strOutQry, ".oitype1  <=", CompareConstants.vbTextCompare);
				Statics.intPlace += 7;
				for (x = 1; x <= 9; x++)
				{
					strTemp += "(";
					if (Statics.bool401CodePresent)
					{
						fecherFoundation.Strings.MidSet(ref Statics.strOutQry, 20, 2, FCConvert.ToString(x) + " ");
						fecherFoundation.Strings.MidSet(ref Statics.strOutQry, Statics.intPlace, 2, FCConvert.ToString(x) + " ");
						strTemp += Statics.strOutQry;
					}
					if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
					{
						if (!Statics.bool401CodePresent)
						{
							strTemp += " " + Statics.strOutbuilding + ".oigradecd" + FCConvert.ToString(x) + " & " + Statics.strOutbuilding + ".oigradepct" + FCConvert.ToString(x) + " >= " + strMin + " and " + Statics.strOutbuilding + ".oigradecd" + FCConvert.ToString(x) + " & " + Statics.strOutbuilding + ".oigradepct" + FCConvert.ToString(x) + " <= " + strMax;
						}
						else
						{
							strTemp += " and " + Statics.strOutbuilding + ".oigradecd" + FCConvert.ToString(x) + " & " + Statics.strOutbuilding + ".oigradepct" + FCConvert.ToString(x) + " >= " + strMin + " and " + Statics.strOutbuilding + ".oigradecd" + FCConvert.ToString(x) + " & " + Statics.strOutbuilding + ".oigradepct" + FCConvert.ToString(x) + " <= " + strMax;
						}
						strTemp += ") or";
					}
					else
					{
						if (!Statics.bool401CodePresent)
						{
							strTemp += " (" + Statics.strOutbuilding + ".oigradecd" + FCConvert.ToString(x) + " & " + Statics.strOutbuilding + ".oigradepct" + FCConvert.ToString(x) + " < " + strMin + " or " + Statics.strOutbuilding + ".oigradecd" + FCConvert.ToString(x) + " & " + Statics.strOutbuilding + ".oigradepct" + FCConvert.ToString(x) + " > " + strMax;
						}
						else
						{
							strTemp += " and (" + Statics.strOutbuilding + ".oigradecd" + FCConvert.ToString(x) + " & " + Statics.strOutbuilding + ".oigradepct" + FCConvert.ToString(x) + " < " + strMin + " or " + Statics.strOutbuilding + ".oigradecd" + FCConvert.ToString(x) + " & " + Statics.strOutbuilding + ".oigradepct" + FCConvert.ToString(x) + " > " + strMax;
						}
						strTemp += ")) and";
					}
				}
				// x
				strTemp += "(";
				if (Statics.bool401CodePresent)
				{
					fecherFoundation.Strings.MidSet(ref Statics.strOutQry, 20, 2, FCConvert.ToString(10));
					fecherFoundation.Strings.MidSet(ref Statics.strOutQry, Statics.intPlace, 2, FCConvert.ToString(10));
					strTemp += Statics.strOutQry;
				}
				if (FCConvert.ToString(Statics.rsReport.Get_Fields_String("incorexc")) == "I")
				{
					if (!Statics.bool401CodePresent)
					{
						strTemp += "  " + Statics.strOutbuilding + ".oigradecd10 & " + Statics.strOutbuilding + ".oigradepct10 >= " + strMin + " and " + Statics.strOutbuilding + ".oigradecd10 & " + Statics.strOutbuilding + ".oigradepct10 <= " + strMax;
					}
					else
					{
						strTemp += " and " + Statics.strOutbuilding + ".oigradecd10 & " + Statics.strOutbuilding + ".oigradepct10 >= " + strMin + " and " + Statics.strOutbuilding + ".oigradecd10 & " + Statics.strOutbuilding + ".oigradepct10 <= " + strMax;
					}
					strTemp += "))";
				}
				else
				{
					strTemp += " and (" + Statics.strOutbuilding + ".oigradecd10 & " + Statics.strOutbuilding + ".oigradepct10 < " + strMin + " or " + Statics.strOutbuilding + ".oigradecd10 & " + Statics.strOutbuilding + ".oigradepct10 > " + strMax;
					strTemp += ")))";
				}
				Statics.boolUsedOutBuildingTable = true;
				Statics.strWhereClause += Statics.strSQLSecondHalf + strTemp;
			}
			Statics.strSQLSecondHalf = " and";
		}

		private static void BuildJoinClause()
		{
			bool boolFirstTable;
			string strTemp = "";
			int lngID;
			bool boolSecondTable;
			lngID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			string strREFullDBName = "";
			string strMasterJoin;
			string strMasterJoinJoin = "";
			// strREFullDBName = rsLoad.GetFullDBName("RealEstate")
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			boolFirstTable = true;
			boolSecondTable = true;
			if (Statics.boolUsedDwellingTable)
			{
				if (boolFirstTable)
				{
					// strJoinClause = "Dwelling "
					Statics.strJoinClause = Statics.strDwelling + " ";
					strTemp = Statics.strDwelling;
				}
				else
				{
					strTemp = Strings.Mid(Statics.strJoinClause, 1, Strings.InStr(1, Statics.strJoinClause, " ", CompareConstants.vbTextCompare) - 1);
					// strJoinClause = "Dwelling INNER JOIN (" & strJoinClause & ")"
					if (!boolSecondTable)
					{
						Statics.strJoinClause = Statics.strDwelling + " inner join (" + Statics.strJoinClause + ")";
					}
					else
					{
						Statics.strJoinClause = Statics.strDwelling + " inner join " + Statics.strJoinClause;
						boolSecondTable = false;
					}
					if (Strings.UCase(Statics.strDwelling) == "DWELLING")
					{
						Statics.strJoinClause += " ON (dwelling.rscard = " + strTemp + ".rscard) AND (dwelling.rsaccount = " + strTemp + ".rsaccount)";
						strTemp = "Dwelling";
					}
					else
					{
						Statics.strJoinClause += " ON (srdwelling.rscard = " + strTemp + ".rscard) and (srdwelling.rsaccount = " + strTemp + ".rsaccount) and (srdwelling.saledate = " + strTemp + ".saledate)";
						strTemp = "SRDwelling";
					}
				}
				boolFirstTable = false;
			}
			if (Statics.boolUsedCommercialTable)
			{
				if (boolFirstTable)
				{
					// strJoinClause = "Commercial "
					Statics.strJoinClause = Statics.strCommercial + " ";
					strTemp = Statics.strCommercial;
				}
				else
				{
					strTemp = Strings.Mid(Statics.strJoinClause, 1, Strings.InStr(1, Statics.strJoinClause, " ", CompareConstants.vbTextCompare) - 1);
					// strJoinClause = "Commercial INNER JOIN (" & strJoinClause & ")"
					if (!boolSecondTable)
					{
						Statics.strJoinClause = Statics.strCommercial + " INNER JOIN (" + Statics.strJoinClause + ")";
					}
					else
					{
						Statics.strJoinClause = Statics.strCommercial + " inner join " + Statics.strJoinClause;
						boolSecondTable = false;
					}
					if (Strings.UCase(Statics.strCommercial) == "COMMERCIAL")
					{
						Statics.strJoinClause += " ON (commercial.rscard = " + strTemp + ".rscard) AND (commercial.rsaccount = " + strTemp + ".rsaccount)";
						strTemp = "Commercial";
					}
					else
					{
						Statics.strJoinClause += " ON (SRcommercial.rscard = " + strTemp + ".rscard) AND (SRcommercial.rsaccount = " + strTemp + ".rsaccount) and (srcommercial.saledate = " + strTemp + ".saledate)";
						strTemp = "SRCommercial";
					}
				}
				boolFirstTable = false;
				// strTemp = "Commercial"
			}
			if (Statics.boolUsedOutBuildingTable)
			{
				if (boolFirstTable)
				{
					// strJoinClause = "Outbuilding "
					Statics.strJoinClause = Statics.strOutbuilding + " ";
					strTemp = Statics.strOutbuilding;
				}
				else
				{
					strTemp = Strings.Mid(Statics.strJoinClause, 1, Strings.InStr(1, Statics.strJoinClause, " ", CompareConstants.vbTextCompare) - 1);
					// strJoinClause = "Outbuilding INNER JOIN (" & strJoinClause & ")"
					if (!boolSecondTable)
					{
						Statics.strJoinClause = Statics.strOutbuilding + " INNER JOIN (" + Statics.strJoinClause + ")";
					}
					else
					{
						Statics.strJoinClause = Statics.strOutbuilding + " inner join " + Statics.strJoinClause;
						boolSecondTable = false;
					}
					if (Strings.UCase(Statics.strOutbuilding) == "OUTBUILDING")
					{
						Statics.strJoinClause += " ON (outbuilding.rscard = " + strTemp + ".rscard) AND (outbuilding.rsaccount = " + strTemp + ".rsaccount)";
						strTemp = "Outbuilding";
					}
					else
					{
						Statics.strJoinClause += " ON (SRoutbuilding.rscard = " + strTemp + ".rscard) AND (SRoutbuilding.rsaccount = " + strTemp + ".rsaccount) and (sroutbuilding.saledate = " + strTemp + ".saledate)";
						strTemp = "SROutbuilding";
					}
				}
				boolFirstTable = false;
				// strTemp = "outbuilding"
			}
			// If boolUsedMasterTable Then
			if (boolFirstTable)
			{
				// strJoinClause = "Master "
				if (Strings.LCase(Statics.strMaster) == "master")
				{
					Statics.strJoinClause = strMasterJoinQuery + " ";
				}
				else
				{
					Statics.strJoinClause = Statics.strMaster + " ";
				}
				strTemp = Statics.strMaster;
			}
			else
			{
				strTemp = Strings.Mid(Statics.strJoinClause, 1, Strings.InStr(1, Statics.strJoinClause, " ", CompareConstants.vbTextCompare) - 1);
				if (Strings.LCase(Statics.strMaster) == "master")
				{
					if (!boolSecondTable)
					{
						Statics.strJoinClause = strMasterJoinQuery + " inner join (" + Statics.strJoinClause + ")";
					}
					else
					{
						Statics.strJoinClause = strMasterJoinQuery + " inner join " + Statics.strJoinClause;
						boolSecondTable = false;
					}
				}
				else
				{
					if (!boolSecondTable)
					{
						Statics.strJoinClause = Statics.strMaster + " inner join (" + Statics.strJoinClause + ")";
					}
					else
					{
						Statics.strJoinClause = Statics.strMaster + " inner join " + Statics.strJoinClause;
						boolSecondTable = false;
					}
				}
				if (Strings.UCase(Statics.strMaster) == "MASTER")
				{
					Statics.strJoinClause += " ON (mj.rscard = " + strTemp + ".rscard) AND (mj.rsaccount = " + strTemp + ".rsaccount)";
					strTemp = "Master";
				}
				else if (Strings.UCase(Statics.strMaster) == "SRMASTER")
				{
					Statics.strJoinClause += " ON (srmaster.rscard = " + strTemp + ".rscard) and (srmaster.rsaccount = " + strTemp + ".rsaccount) and (srmaster.saledate = " + strTemp + ".saledate)";
					strTemp = "SRMaster";
				}
			}
			boolFirstTable = false;
			// strTemp = "Master"
			strTemp = Statics.strMaster;
			if (Strings.LCase(Statics.strMaster) == "srmaster")
			{
				Statics.strWhereClause += " and not " + Statics.strMaster + ".rsdeleted = 1 ";
			}
			else
			{
				Statics.strWhereClause += " and not mj.rsdeleted = 1 ";
			}
			// End If
			if (Statics.boolUsedPreviousAssessmentTable)
			{
				if (boolFirstTable)
				{
					Statics.strJoinClause = Statics.strPreviousAssessment + " ";
					// strTemp = strPreviousAssessment
				}
				else
				{
					strTemp = Strings.Mid(Statics.strJoinClause, 1, Strings.InStr(1, Statics.strJoinClause, " ", CompareConstants.vbTextCompare) - 1);
					if (!boolSecondTable)
					{
						Statics.strJoinClause = Statics.strPreviousAssessment + " inner join (" + Statics.strJoinClause + ")";
					}
					else
					{
						Statics.strJoinClause = Statics.strPreviousAssessment + " inner join " + Statics.strJoinClause;
						boolSecondTable = false;
					}
					Statics.strJoinClause += " on (previousassessment.card = " + strTemp + ".rscard) and (previousassessment.account = " + strTemp + ".rsaccount)";
				}
			}
			if (Strings.UCase(strTemp) == "SRMASTER")
			{
				Statics.strSQLString = "Insert into ExtractTable (accountnumber,cardnumber,saleid,saledate,userid) select distinct " + strTemp + ".rsaccount," + strTemp + ".rscard," + strTemp + ".saleid," + strTemp + ".saledate," + FCConvert.ToString(lngID) + " as userid from ";
				// strSQLString = "Insert into ExtractTable (accountnumber,cardnumber,saleid,saledate) select  " & strtemp & ".rsaccount," & strtemp & ".rscard," & strtemp & ".saleid," & strtemp & ".saledate from "
			}
			else
			{
				Statics.strSQLString += "mj.rsaccount,mj.rscard,mj.saledate," + FCConvert.ToString(lngID) + " as userid from ";
			}
		}

		public class StaticVariables
		{
			//=========================================================
			public string strMaster = string.Empty;
			public string strDwelling = "";
			public string strCommercial = "";
			public string strOutbuilding = "";
			public string strPreviousAssessment = "";
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper RSExtract = new clsDRWrapper();
			public clsDRWrapper RSExtract_AutoInitialized = null;
			public clsDRWrapper RSExtract
			{
				get
				{
					if ( RSExtract_AutoInitialized == null)
					{
						 RSExtract_AutoInitialized = new clsDRWrapper();
					}
					return RSExtract_AutoInitialized;
				}
				set
				{
					 RSExtract_AutoInitialized = value;
				}
			}
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsReport = new clsDRWrapper();
			public clsDRWrapper rsReport_AutoInitialized = null;
			public clsDRWrapper rsReport
			{
				get
				{
					if ( rsReport_AutoInitialized == null)
					{
						 rsReport_AutoInitialized = new clsDRWrapper();
					}
					return rsReport_AutoInitialized;
				}
				set
				{
					 rsReport_AutoInitialized = value;
				}
			}
			
			public clsDRWrapper rsTemp_AutoInitialized = null;
			public clsDRWrapper rsTemp
			{
				get
				{
					if ( rsTemp_AutoInitialized == null)
					{
						 rsTemp_AutoInitialized = new clsDRWrapper();
					}
					return rsTemp_AutoInitialized;
				}
				set
				{
					 rsTemp_AutoInitialized = value;
				}
			}
			
			// Dim dbTemp As DAO.Database
			public int intCode;
			public string strSQLString = "";
			public string strSQLSecondHalf = "";
			public string strJoinClause = "";
			public string strWhereClause = "";
			public bool boolUsedMasterTable;
			public bool boolUsedDwellingTable;
			public bool boolUsedOutBuildingTable;
			public bool boolUsedCommercialTable;
			public bool boolUsedPreviousAssessmentTable;
			public bool bool401CodePresent;
			public string OutMin = "";
			public string outmax = "";
			public string strOutQry = "";
			public clsReportCodes clCodeList = new clsReportCodes();
			public int intPlace = 0;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
