﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	public class clsProperty
	{
		//=========================================================
		public int ID;
		public int ClientID;
		// vbPorter upgrade warning: DateUpdated As DateTime	OnWriteFCConvert.ToInt16(
		public DateTime DateUpdated;
		public bool DetailView;
		public int TaxYear;
		public string MapLot;
		public int Account;
		public int StreetNumber;
		public string StreetName;
		public string Apartment;
		public double LandValue;
		public double BuildingValue;
		public double totalexemption;
		public double Taxable;
		public double TotalAcres;
		public double Frontage;
		public string Map;
		public string Block;
		public string Lot;
		public string Sublot;
		public string Book;
		public string PAGE;
		public bool Residential;
		public bool Commercial;
		public string Zone;
		public string Neighborhood;
		public double Tax;
		public double TotalTaxOutstanding;
		public string TopographyDescription1;
		public string TopographyDescription2;
		public string UtilitiesDescription1;
		public string UtilitiesDescription2;
		public string StreetType;
		public int lngOwnerID;
		public string strReference1;
		public string strReference2;
		public bool boolIncludeRef1;
		public bool boolIncludeRef2;
		private FCCollection LandParcels = new FCCollection();
		private FCCollection Buildings = new FCCollection();
		private FCCollection Pictures = new FCCollection();
		private FCCollection Owners = new FCCollection();
		private FCCollection Sales = new FCCollection();

		public void Clear()
		{
			ID = 0;
			ClientID = 0;
			DateUpdated = DateTime.FromOADate(0);
			DetailView = false;
			TaxYear = 0;
			MapLot = "";
			Account = 0;
			StreetNumber = 0;
			StreetName = "";
			Apartment = "";
			LandValue = 0;
			BuildingValue = 0;
			totalexemption = 0;
			Taxable = 0;
			TotalAcres = 0;
			Frontage = 0;
			Map = "";
			Block = "";
			Lot = "";
			Sublot = "";
			Book = "";
			PAGE = "";
			Residential = true;
			Commercial = false;
			Zone = "";
			Neighborhood = "";
			Tax = 0;
			TotalTaxOutstanding = 0;
			TopographyDescription1 = "";
			TopographyDescription2 = "";
			UtilitiesDescription1 = "";
			UtilitiesDescription2 = "";
			StreetType = "";
			strReference1 = "";
			strReference2 = "";
			boolIncludeRef1 = false;
			boolIncludeRef2 = false;
			int x;
			if (!(LandParcels == null))
			{
				for (x = 1; x <= LandParcels.Count; x++)
				{
					LandParcels.Remove(1);
				}
				// x
			}
			if (!(Buildings == null))
			{
				for (x = 1; x <= Buildings.Count; x++)
				{
					Buildings.Remove(1);
				}
				// x
			}
			if (!(Pictures == null))
			{
				for (x = 1; x <= Pictures.Count; x++)
				{
					Pictures.Remove(1);
				}
				// x
			}
			if (!(Owners == null))
			{
				for (x = 1; x <= Owners.Count; x++)
				{
					Owners.Remove(1);
				}
				// x
			}
			if (!(Sales == null))
			{
				for (x = 1; x <= Sales.Count; x++)
				{
					Sales.Remove(1);
				}
				// x
			}
		}

		public void AddLand(ref double dblUnits, ref string strDescription, ref string strType, ref double dblUnitsDepth, ref int lngAmount)
		{
			clsParcel tParcel = new clsParcel();
			tParcel.ClientID = ClientID;
			tParcel.Account = Account;
			tParcel.Units = dblUnits;
			tParcel.Description = strDescription;
			tParcel.strType = strType;
			tParcel.UnitsDepth = dblUnitsDepth;
			tParcel.Amount = lngAmount;
			LandParcels.Add(tParcel);
		}

		public void AddBuilding(ref clsBuilding tBuilding)
		{
			tBuilding.ClientID = ClientID;
			tBuilding.Account = Account;
			Buildings.Add(tBuilding);
		}

		public void AddPicture(ref string strDescription, ref short intOrder, ref string strPicName, ref string strMimeType)
		{
			clsPicInfo tPic = new clsPicInfo();
			tPic.ClientID = ClientID;
			tPic.Account = Account;
			tPic.Description = strDescription;
			tPic.FileName = strPicName;
			tPic.MimeType = strMimeType;
			tPic.OrderNo = intOrder;
			Pictures.Add(tPic);
		}

		public void AddOwner(ref clsOwners tOwner)
		{
			Owners.Add(tOwner);
		}

		public void AddSale(ref clsSale tSale)
		{
			Sales.Add(tSale);
		}

		public bool SaveOwnersToCSV(StreamWriter ts, StreamWriter pots)
		{
			bool SaveOwnersToCSV = false;
			try
			{
				// On Error GoTo ErrorHandler
				SaveOwnersToCSV = false;
				string strLine = "";
				int x;
				if (!(Owners == null))
				{
					for (x = 1; x <= Owners.Count; x++)
					{
						if (!(Owners[x] == null))
						{
							strLine = GetCSVOwnerLine(ref x);
							if (strLine != string.Empty)
							{
								ts.WriteLine(strLine);
							}
							strLine = GetCSVPropertyOwnerLine(ref x);
							if (strLine != string.Empty)
							{
								pots.WriteLine(strLine);
							}
						}
					}
					// x
				}
				SaveOwnersToCSV = true;
				return SaveOwnersToCSV;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveOwnersToCSV", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveOwnersToCSV;
		}

		public bool SaveSalesToCSV(StreamWriter ts)
		{
			bool SaveSalesToCSV = false;
			try
			{
				// On Error GoTo ErrorHandler
				SaveSalesToCSV = false;
				string strLine = "";
				int x;
				if (!(Sales == null))
				{
					for (x = 1; x <= Sales.Count; x++)
					{
						if (!(Sales[x] == null))
						{
							strLine = GetCSVSalesLine(ref x);
							if (strLine != string.Empty)
							{
								ts.WriteLine(strLine);
							}
						}
					}
					// x
				}
				SaveSalesToCSV = true;
				return SaveSalesToCSV;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveSalesToCSV", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveSalesToCSV;
		}

		public bool SavePicsToCSV(StreamWriter ts)
		{
			bool SavePicsToCSV = false;
			try
			{
				// On Error GoTo ErrorHandler
				SavePicsToCSV = false;
				string strLine = "";
				int x;
				if (!(Pictures == null))
				{
					for (x = 1; x <= Pictures.Count; x++)
					{
						if (!(Pictures[x] == null))
						{
							strLine = GetCSVPictureLine(ref x);
							if (strLine != string.Empty)
							{
								ts.WriteLine(strLine);
							}
						}
					}
					// x
				}
				SavePicsToCSV = true;
				return SavePicsToCSV;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SavePicsToCSV", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SavePicsToCSV;
		}

		public bool SaveBuildingsToCSV(StreamWriter ts)
		{
			bool SaveBuildingsToCSV = false;
			try
			{
				// On Error GoTo ErrorHandler
				SaveBuildingsToCSV = false;
				string strLine = "";
				int x;
				if (!(Buildings == null))
				{
					for (x = 1; x <= Buildings.Count; x++)
					{
						if (!(Buildings[x] == null))
						{
							strLine = GetCSVBuildingLine(ref x);
							if (strLine != string.Empty)
							{
								ts.WriteLine(strLine);
							}
						}
					}
					// x
				}
				SaveBuildingsToCSV = true;
				return SaveBuildingsToCSV;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveBuildingsToCSV", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveBuildingsToCSV;
		}

		public bool SaveParcelsToCSV(StreamWriter ts)
		{
			bool SaveParcelsToCSV = false;
			SaveParcelsToCSV = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strLine = "";
				int x;
				if (!(LandParcels == null))
				{
					for (x = 1; x <= LandParcels.Count; x++)
					{
						if (!(LandParcels[x] == null))
						{
							strLine = GetCSVParcelLine(ref x);
							if (strLine != string.Empty)
							{
								ts.WriteLine(strLine);
							}
						}
					}
					// x
				}
				SaveParcelsToCSV = true;
				return SaveParcelsToCSV;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveParcelsToCSV", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveParcelsToCSV;
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		public string GetCSVBuildingLine(ref int intIndex)
		{
			string GetCSVBuildingLine = "";
			string strReturn;
			clsBuilding tBuilding = new clsBuilding();
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetCSVBuildingLine = "";
				if (!(Buildings == null))
				{
					if (Buildings.Count >= intIndex)
					{
						if (!(Buildings[intIndex] == null))
						{
							strReturn = Buildings[intIndex].ClientID;
							strReturn += "|" + Buildings[intIndex].Account;
							strReturn += "|" + Buildings[intIndex].Description;
							strReturn += "|" + Buildings[intIndex].Value;
							strReturn += "|" + Buildings[intIndex].strType;
							strReturn += "|" + Buildings[intIndex].YearBuilt;
							strReturn += "|" + Buildings[intIndex].Width;
							strReturn += "|" + Buildings[intIndex].Stories;
							strReturn += "|" + Buildings[intIndex].Length;
							strReturn += "|" + Buildings[intIndex].Area;
							strReturn += "|" + Buildings[intIndex].Rooms;
							strReturn += "|" + Buildings[intIndex].Bedrooms;
							strReturn += "|" + Buildings[intIndex].FullBaths;
							strReturn += "|" + Buildings[intIndex].HalfBaths;
							strReturn += "|" + Buildings[intIndex].Fireplaces;
							strReturn += "|" + Buildings[intIndex].YearRemodeled;
							strReturn += "|" + Buildings[intIndex].NumCars;
						}
					}
				}
				GetCSVBuildingLine = strReturn;
				return GetCSVBuildingLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetCSVBuildingLine", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCSVBuildingLine;
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		public string GetCSVPropertyOwnerLine(ref int intIndex)
		{
			string GetCSVPropertyOwnerLine = "";
			// vbPorter upgrade warning: strReturn As string	OnWrite(string, int)
			string strReturn;
			clsOwners tOwner;
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetCSVPropertyOwnerLine = "";
				if (!(Owners == null))
				{
					if (Owners.Count >= intIndex)
					{
						if (!(Owners[intIndex] == null))
						{
							tOwner = Owners[intIndex];
							strReturn = FCConvert.ToString(tOwner.ID);
							strReturn += "|" + FCConvert.ToString(Account);
							if (intIndex == 1)
							{
								strReturn += "|" + "TRUE";
							}
							else
							{
								strReturn += "|" + "FALSE";
							}
							strReturn += "|" + FCConvert.ToString(ID);
						}
					}
				}
				GetCSVPropertyOwnerLine = strReturn;
				return GetCSVPropertyOwnerLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetCSVPropertyOwnerLine", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCSVPropertyOwnerLine;
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		public string GetCSVOwnerLine(ref int intIndex)
		{
			string GetCSVOwnerLine = "";
			// vbPorter upgrade warning: strReturn As string	OnWrite(string, int)
			string strReturn;
			clsOwners tOwner;
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetCSVOwnerLine = "";
				if (!(Owners == null))
				{
					if (Owners.Count >= intIndex)
					{
						if (!(Owners[intIndex] == null))
						{
							tOwner = Owners[intIndex];
							strReturn = FCConvert.ToString(tOwner.ID);
							strReturn += "|" + FCConvert.ToString(tOwner.ClientID);
							strReturn += "|" + FCConvert.ToString(tOwner.MiscID);
							strReturn += "|" + tOwner.FirstName;
							strReturn += "|" + tOwner.LastName;
							strReturn += "|" + tOwner.MiddleName;
							strReturn += "|" + tOwner.Desig;
							strReturn += "|" + tOwner.FullName;
							strReturn += "|" + tOwner.Address1;
							strReturn += "|" + tOwner.Address2;
							strReturn += "|" + tOwner.City;
							strReturn += "|" + tOwner.StateProvince;
							strReturn += "|" + tOwner.Country;
							strReturn += "|" + tOwner.Zip;
							strReturn += "|" + tOwner.ZipExt;
							strReturn += "|" + tOwner.PhoneNumber1;
							strReturn += "|" + tOwner.PhoneDescription1;
							strReturn += "|" + tOwner.PhoneNumber2;
							strReturn += "|" + tOwner.PhoneDescription2;
							strReturn += "|" + tOwner.EMail;
						}
					}
				}
				GetCSVOwnerLine = strReturn;
				return GetCSVOwnerLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetCSVOwnerLine", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCSVOwnerLine;
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		public string GetCSVSalesLine(ref int intIndex)
		{
			string GetCSVSalesLine = "";
			// vbPorter upgrade warning: strReturn As string	OnWrite(string, int)
			string strReturn;
			clsSale tSale;
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetCSVSalesLine = "";
				if (!(Sales == null))
				{
					if (Sales.Count >= intIndex)
					{
						if (!(Sales[intIndex] == null))
						{
							tSale = Sales[intIndex];
							strReturn = FCConvert.ToString(tSale.ClientID);
							strReturn += "|" + FCConvert.ToString(Account);
							strReturn += "|" + FCConvert.ToString(tSale.SaleDate);
							strReturn += "|" + FCConvert.ToString(tSale.Price);
							strReturn += "|" + tSale.OriginalOwner;
							strReturn += "|" + FCConvert.ToString(tSale.LandValue);
							strReturn += "|" + FCConvert.ToString(tSale.BuildingValue);
						}
					}
				}
				GetCSVSalesLine = strReturn;
				return GetCSVSalesLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetCSVSalesLine", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCSVSalesLine;
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		public string GetCSVPictureLine(ref int intIndex)
		{
			string GetCSVPictureLine = "";
			string strReturn;
			clsPicInfo tPic = new clsPicInfo();
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetCSVPictureLine = "";
				if (!(Pictures == null))
				{
					if (Pictures.Count >= intIndex)
					{
						if (!(Pictures[intIndex] == null))
						{
							strReturn = Pictures[intIndex].ClientID;
							strReturn += "|" + Pictures[intIndex].Account;
							strReturn += "|" + Pictures[intIndex].Description;
							strReturn += "|" + Pictures[intIndex].OrderNo;
							strReturn += "|" + Pictures[intIndex].FileName;
							strReturn += "|" + Pictures[intIndex].MimeType;
						}
					}
				}
				GetCSVPictureLine = strReturn;
				return GetCSVPictureLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetCSVPictureLine", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCSVPictureLine;
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		public string GetCSVParcelLine(ref int intIndex)
		{
			string GetCSVParcelLine = "";
			string strReturn;
			clsParcel tParcel = new clsParcel();
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetCSVParcelLine = "";
				if (!(LandParcels == null))
				{
					if (LandParcels.Count >= intIndex)
					{
						if (!(LandParcels[intIndex] == null))
						{
							strReturn = LandParcels[intIndex].ClientID;
							strReturn += "|" + LandParcels[intIndex].Account;
							strReturn += "|" + LandParcels[intIndex].Units;
							strReturn += "|" + LandParcels[intIndex].Description;
							strReturn += "|" + LandParcels[intIndex].strType;
							strReturn += "|" + LandParcels[intIndex].UnitsDepth;
							strReturn += "|" + LandParcels[intIndex].Amount;
						}
					}
				}
				GetCSVParcelLine = strReturn;
				return GetCSVParcelLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetCSVParcelLine", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCSVParcelLine;
		}

		public string CSVLine
		{
			get
			{
				string CSVLine = "";
				string strReturn;
				strReturn = "";
				strReturn += FCConvert.ToString(ID);
				strReturn += "|" + FCConvert.ToString(ClientID);
				strReturn += "|" + FCConvert.ToString(DateUpdated);
				strReturn += "|" + DetailView.ToString();
				strReturn += "|" + FCConvert.ToString(TaxYear);
				strReturn += "|" + Strings.Replace(MapLot, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + FCConvert.ToString(Account);
				strReturn += "|" + FCConvert.ToString(StreetNumber);
				strReturn += "|" + Strings.Replace(StreetName, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Apartment;
				strReturn += "|" + FCConvert.ToString(LandValue);
				strReturn += "|" + FCConvert.ToString(BuildingValue);
				strReturn += "|" + FCConvert.ToString(totalexemption);
				strReturn += "|" + FCConvert.ToString(Taxable);
				strReturn += "|" + FCConvert.ToString(TotalAcres);
				strReturn += "|" + FCConvert.ToString(Frontage);
				strReturn += "|" + Strings.Replace(Map, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Strings.Replace(Block, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Strings.Replace(Lot, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Strings.Replace(Sublot, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Book;
				strReturn += "|" + PAGE;
				strReturn += "|" + Strings.Replace(Residential.ToString(), FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Strings.Replace(Commercial.ToString(), FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Strings.Replace(Zone, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Strings.Replace(Neighborhood, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + FCConvert.ToString(Tax);
				strReturn += "|" + FCConvert.ToString(TotalTaxOutstanding);
				strReturn += "|" + Strings.Replace(TopographyDescription1, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Strings.Replace(TopographyDescription2, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Strings.Replace(UtilitiesDescription1, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Strings.Replace(UtilitiesDescription2, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				strReturn += "|" + Strings.Replace(StreetType, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				// 33
				if (boolIncludeRef1 && Strings.Trim(strReference1) != "")
				{
					strReturn += "|" + Strings.Replace(modGlobalVariables.Statics.CustomizedInfo.Ref1Description, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
					strReturn += "|" + Strings.Replace(strReference1, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				}
				if (boolIncludeRef2 && Strings.Trim(strReference2) != "")
				{
					strReturn += "|" + Strings.Replace(modGlobalVariables.Statics.CustomizedInfo.Ref2Description, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
					strReturn += "|" + Strings.Replace(strReference2, FCConvert.ToString(Convert.ToChar(34)), "", 1, -1, CompareConstants.vbTextCompare);
				}
				CSVLine = strReturn;
				return CSVLine;
			}
		}
	}
}
