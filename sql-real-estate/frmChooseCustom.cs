﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmChooseCustom.
	/// </summary>
	public partial class frmChooseCustom : BaseForm
	{
		public frmChooseCustom()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChooseCustom InstancePtr
		{
			get
			{
				return (frmChooseCustom)Sys.GetInstance(typeof(frmChooseCustom));
			}
		}

		protected frmChooseCustom _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void cmdCancel_Click()
		{
			//cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void DeleteReport()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				clsTemp.Execute("delete from customreports where reportnumber = " + FCConvert.ToString(Conversion.Val(Grid1.TextMatrix(Grid1.Row, 1))), modGlobalVariables.strREDatabase);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "In Delete Report", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdLoad_Click(object sender, System.EventArgs e)
		{
			// ldkjfkd
			if (frmCustomReport.InstancePtr.boolDelete)
			{
				DeleteReport();
				this.Unload();
				return;
			}
			frmCustomReport.InstancePtr.lngCurrentReportNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid1.TextMatrix(Grid1.Row, 1))));
			frmCustomReport.InstancePtr.FillGrid1();
			this.Unload();
		}

		public void cmdLoad_Click()
		{
			cmdLoad_Click(cmdLoad, new System.EventArgs());
		}

		private void frmChooseCustom_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						mnuContinue_Click();
						break;
					}
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmChooseCustom_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseCustom properties;
			//frmChooseCustom.ScaleWidth	= 5880;
			//frmChooseCustom.ScaleHeight	= 4515;
			//frmChooseCustom.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			if (frmCustomReport.InstancePtr.boolDelete)
			{
				this.Text = "Delete A Report";
				//FC:FINAL:MSH - i.issue #1171: set the same text as in form title to HeaderText
				HeaderText.Text = "Delete A Report";
				cmdLoad.Text = "Delete";
			}
			SetupGrid();
		}

		private void Grid1_DblClick(object sender, System.EventArgs e)
		{
			cmdLoad_Click();
		}

		private void SetupGrid()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				Grid1.Rows = 1;
				Grid1.TextMatrix(0, 0, "Report Title");
				Grid1.ColHidden(1, true);
				// Grid1.ColWidth(0) = 3600
				clsTemp.OpenRecordset("select * from customreports where reporttitle <> '' order by reportnumber", modGlobalVariables.strREDatabase);
				while (!clsTemp.EndOfFile())
				{
					Grid1.AddItem(clsTemp.Get_Fields_String("reporttitle") + "\t" + clsTemp.Get_Fields_Int32("reportnumber"));
					clsTemp.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SetupGrid", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuContinue_Click(object sender, System.EventArgs e)
		{
			cmdLoad_Click();
		}

		public void mnuContinue_Click()
		{
			//mnuContinue_Click(cmdContinue, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdCancel_Click();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
