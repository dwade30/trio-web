﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWXF0000
{
	public class modMain
	{
		//=========================================================
		public const string DEFAULTDATABASE = "TWRE0000.vb1";
		// Public Const DATABASEPASSWORD = "TRIO2001"
		public const int CNSTREOWNERINFOOLD = 0;
		public const int CNSTREOWNERINFOOLDCONEW = 1;
		public const int CNSTREOWNERINFONEW = 2;

		public static void Main()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SettingsInfo s = new SettingsInfo();
				modGlobalVariables.Statics.strREDatabase = "RealEstate";
				// strREConnectionString = "Provider=Micros
				string strDataPath;
				strDataPath = Environment.CurrentDirectory;
				if (Strings.Right(strDataPath, 1) != "\\")
				{
					strDataPath += "\\";
				}
				// S.LoadSettingsAndArchives
				// If Not s.LoadSettingsAndArchives(strDataPath & "trioworkstationsetup.xml") Then
				// Call MsgBox("Unable to load settings file", vbCritical, "Cannot Continue")
				// End
				// Exit Sub
				// End If
				if (!Convert.ToBoolean(modGlobalFunctions.LoadSQLConfig("")))
				{
					MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				if (App.LogMode == 0)
				{
					// debug mode
				}
				clsLoad.DefaultGroup = "Live";
                // 
                // If clsLoad.MegaGroupsCount > 0 Then
                // clsLoad.DefaultMegaGroup = clsLoad.AvailableMegaGroups(0).GroupName
                // End If
                //FC:FINAL:IPI - not needed on web
                //if (App.PrevInstance())
				//{
				//	MessageBox.Show("Warning. There is already a version of this program running on this computer.", "Already Running", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//	Application.Exit();
				//	return;
				//}
				// gintSecurityID = IIf(GetRegistryKey("SecurityID") <> vbNullString, Val(gstrReturn), "0")
				cSystemSettings tSys = new cSystemSettings();
				cGlobalVariable tGV;
				tGV = tSys.GetGlobalVariables();
				modGlobalConstants.Statics.MuniName = tGV.MuniName;
				// gstrUseSecurity = tGV.UseSecurity
				modReplaceWorkFiles.Statics.gintSecurityID = (FCConvert.ToString(modRegistry.GetRegistryKey("SecurityID")) != string.Empty ? FCConvert.ToInt32(Math.Round(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn))) : 0);
				modReplaceWorkFiles.EntryFlagFile(true, "");
				clsLoad.OpenRecordset("select * from globalvariables", "SystemSettings");
				CheckVersion();
				// Set clsSecurityClass = New clsTrioSecurity
				// Call clsSecurityClass.Init("XF", "TWGN0000.vb1")
				LoadCustomizedInfo();
				//MDIParent.InstancePtr.Show();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Main", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Application.Exit();
			}
		}

		private static void CheckVersion()
		{
			try
			{
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckVersion", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}

		public static void LoadCustomizedInfo()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cREXferSettingsController xSetCont = new cREXferSettingsController();
				modGlobalVariables.Statics.CustomizedInfo = xSetCont.LoadSettings();
				modGlobalVariables.Statics.CustomizedInfo.MuniName = modGlobalConstants.Statics.MuniName;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Numbre " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadCustomizedInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static bool MyIsNumeric(string strInput)
		{
			bool MyIsNumeric = false;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			MyIsNumeric = false;
			if (strInput == string.Empty)
				return MyIsNumeric;
			if (!Information.IsNumeric(strInput))
			{
				return MyIsNumeric;
			}
			MyIsNumeric = true;
			// still could be 10D1 is considered numeric
			for (x = 1; x <= (strInput.Length); x++)
			{
				if (Convert.ToByte(Strings.Mid(strInput, x, 1)[0]) >= 58 && Convert.ToByte(Strings.Mid(strInput, x, 1)[0]) <= 126)
				{
					MyIsNumeric = false;
				}
				else if (Convert.ToByte(Strings.Mid(strInput, x, 1)[0]) >= 0 && Convert.ToByte(Strings.Mid(strInput, x, 1)[0]) <= 43)
				{
					MyIsNumeric = false;
				}
			}
			// x
			return MyIsNumeric;
		}
		// Public Function GetPPMasterJoin(Optional ByVal boolNameOnly As Boolean = False) As String
		// Dim strReturn As String
		// Dim rsTemp As New clsDRWrapper
		// Dim strFullDBName As String
		// strFullDBName = rsTemp.GetFullDBName("CentralParties")
		// If Not boolNameOnly Then
		// strReturn = "SELECT *, FullNameLF as Name, PartyState as State, '' as Zip4 FROM ppMaster CROSS APPLY " & strFullDBName & ".dbo.GetCentralPartyNameAndAddress(PartyID,NULL,'PP',NULL) "
		// Else
		// strReturn = "SELECT *, FullNameLF as Name FROM ppMaster CROSS APPLY " & strFullDBName & ".dbo.GetCentralPartyName(PartyID) "
		// End If
		// GetPPMasterJoin = strReturn
		// End Function
		public static string GetPPMasterJoin(bool boolNameOnly = false)
		{
			string GetPPMasterJoin = "";
			string strReturn = "";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			string strPP;
			if (!boolNameOnly)
			{
				strReturn = "select ppmaster.*, cpo.FullNameLF as Name,cpo.address1 as address1,cpo.address2 as address2,cpo.address3 as address3,cpo.city as City, cpo.state as state,cpo.zip as zip, '' as zip4, cpo.email as email from ";
			}
			else
			{
				strReturn = "select ppmaster.*, cpo.FullNameLF as Name from ";
			}
			strTemp = tLoad.Get_GetFullDBName("PersonalProperty");
			strPP = strTemp;
			strReturn += strTemp + ".dbo.ppmaster left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			if (!boolNameOnly)
			{
				strTemp += ".dbo.PartyAndAddressView ";
				strReturn += strTemp + " as cpo on (" + strPP + ".dbo.ppmaster.partyid = cpo.PartyID)  ";
			}
			else
			{
				strTemp += ".dbo.PartyNameView ";
				strReturn += strTemp + " as cpo on (" + strPP + ".dbo.ppmaster.partyid = cpo.ID)  ";
			}
			strReturn = " select * from (" + strReturn + ") mparty ";
			GetPPMasterJoin = strReturn;
			return GetPPMasterJoin;
		}

		public static string GetPPMasterJoinForJoin(bool boolNameOnly = false)
		{
			string GetPPMasterJoinForJoin = "";
			string strReturn;
			strReturn = "(" + GetPPMasterJoin(boolNameOnly) + ") mj ";
			GetPPMasterJoinForJoin = strReturn;
			return GetPPMasterJoinForJoin;
		}

		public static string GetREMasterJoin()
		{
			string GetREMasterJoin = "";
			// select master.*, cpo.FullNameLastFirst as RSName,cpso.FullNameLastFirst as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4 from TRIO_Test_Live_RealEstate.dbo.master left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpo on (master.ownerpartyid = cpo.PartyID) left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpso on (master.SecOwnerPartyID = cpso.PartyID)
			string strReturn;
			strReturn = "select master.*, cpo.FullNameLF as RSName,cpso.FullNameLF as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4, cpo.email as email from ";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			strTemp = tLoad.Get_GetFullDBName("RealEstate");
			strReturn += strTemp + ".dbo.master left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			strTemp += ".dbo.PartyAndAddressView ";
			strReturn += strTemp + " as cpo on (master.ownerpartyid = cpo.PartyID) left join ";
			strReturn += strTemp + " as cpso on (master.SecOwnerPartyID = cpso.ID)";
			strReturn = " select * from (" + strReturn + ") mparty ";
			GetREMasterJoin = strReturn;
			return GetREMasterJoin;
		}

		public static string GetREMasterJoinForJoin()
		{
			string GetREMasterJoinForJoin = "";
			string strReturn;
			strReturn = "(" + GetREMasterJoin() + ") mj ";
			GetREMasterJoinForJoin = strReturn;
			return GetREMasterJoinForJoin;
		}
	}
}
