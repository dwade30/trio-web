﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptPPUpdatedAccounts.
	/// </summary>
	public partial class rptPPUpdatedAccounts : BaseSectionReport
	{
		public rptPPUpdatedAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Accounts Updated";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPPUpdatedAccounts InstancePtr
		{
			get
			{
				return (rptPPUpdatedAccounts)Sys.GetInstance(typeof(rptPPUpdatedAccounts));
			}
		}

		protected rptPPUpdatedAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPPUpdatedAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// goes through each row in gridbadaccounts and puts it in the report
		int lngCRow;
		double lngTotValue;
		double lngTotAssess;
		double lngTotOldValue;
		double lngTotOldAssess;
		bool boolUseExceeds;
		bool boolChangesOnly;
		int lngTotalAccounts;
		bool boolInfoOnly;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool boolHidden = false;
			if (boolChangesOnly)
			{
				if (lngCRow < frmPPImportXF.InstancePtr.GridGood.Rows)
				{
					if (frmPPImportXF.InstancePtr.GridGood.RowHidden(lngCRow))
					{
						boolHidden = true;
					}
					else
					{
						boolHidden = false;
					}
					while (boolHidden && lngCRow < frmPPImportXF.InstancePtr.GridGood.Rows)
					{
						if (frmPPImportXF.InstancePtr.GridGood.RowHidden(lngCRow))
						{
							boolHidden = true;
						}
						else
						{
							boolHidden = false;
						}
						if (boolHidden)
						{
							lngCRow += 1;
						}
					}
				}
			}
			eArgs.EOF = lngCRow >= frmPPImportXF.InstancePtr.GridGood.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			double dblChange = 0;
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lngCRow = 1;
			txtTotal.Text = Strings.Format(frmPPImportXF.InstancePtr.GridGood.Rows - 1, "#,###,###,##0");
			if (frmPPImportXF.InstancePtr.chkFlagChanges.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblExceeds.Visible = true;
				txtExceed.Visible = true;
				boolUseExceeds = true;
				dblChange = Conversion.Val(frmPPImportXF.InstancePtr.txtPercentChg.Text);
				lblExceeds.Text = "Exceeds " + FCConvert.ToString(dblChange) + "%";
			}
			else
			{
				lblExceeds.Visible = false;
				txtExceed.Visible = false;
				boolUseExceeds = false;
			}
			if (frmPPImportXF.InstancePtr.cmbUpdateType.ItemData(frmPPImportXF.InstancePtr.cmbUpdateType.SelectedIndex) == modGlobalVariablesXF.CNSTUPDATEINFOONLY)
			{
				boolInfoOnly = true;
				Detail.CanShrink = true;
				Label11.Visible = false;
				Label12.Visible = false;
				Label13.Visible = false;
				lblExceeds.Visible = false;
				txtAssessment.Visible = false;
				txtExceed.Visible = false;
				txtOldAssessment.Visible = false;
				txtTotalAssessment.Visible = false;
				txtTotalOldAssessment.Visible = false;
			}
			else
			{
				boolInfoOnly = false;
			}
			lngTotalAccounts = 0;
			if (frmPPImportXF.InstancePtr.chkShowChangesOnly.CheckState == Wisej.Web.CheckState.Checked)
			{
				boolChangesOnly = true;
			}
			else
			{
				boolChangesOnly = false;
			}
			lngTotValue = 0;
			lngTotAssess = 0;
			lngTotOldValue = 0;
			lngTotOldAssess = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngValue = 0;
			int lngAssess = 0;
			int lngOldValue = 0;
			int lngOldAssess = 0;
			int lngExempt = 0;
			int lngOldExempt = 0;
			if (lngCRow <= frmPPImportXF.InstancePtr.GridGood.Rows - 1)
			{
				txtName.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPOWNER));
				if (Conversion.Val(frmPPImportXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREETNUMBER)) > 0)
				{
					txtLocation.Text = FCConvert.ToString(Conversion.Val(frmPPImportXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREETNUMBER))) + " " + fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREET));
				}
				else
				{
					txtLocation.Text = fecherFoundation.Strings.Trim(frmPPImportXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPSTREET));
				}
				lngValue = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPVALUE))));
				lngOldValue = FCConvert.ToInt32(Math.Round(Conversion.Val(frmPPImportXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPORIGINALVALUE))));
				if (boolUseExceeds)
				{
					if (Conversion.Val(frmPPImportXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPEXCEED)) != 0)
					{
						if (Conversion.Val(frmPPImportXF.InstancePtr.GridGood.TextMatrix(lngCRow, modGridConstantsXF.CNSTPPEXCEED)) > 0)
						{
							txtExceed.Text = "UP";
						}
						else
						{
							txtExceed.Text = "DOWN";
						}
					}
					else
					{
						txtExceed.Text = "";
					}
				}
				lngExempt = 0;
				lngOldExempt = 0;
				lngAssess = lngValue - lngExempt;
				lngOldAssess = lngOldValue - lngOldExempt;
				lngTotValue += lngValue;
				lngTotOldValue += lngOldValue;
				lngTotAssess += lngAssess;
				lngTotOldAssess += lngOldAssess;
				txtAssessment.Text = Strings.Format(lngAssess, "#,###,###,##0");
				txtOldAssessment.Text = Strings.Format(lngOldAssess, "#,###,###,##0");
				lngTotalAccounts += 1;
			}
			else
			{
				txtName.Text = "";
				txtExceed.Text = "";
				txtAssessment.Text = "";
				txtOldAssessment.Text = "";
			}
			lngCRow += 1;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalAssessment.Text = Strings.Format(lngTotAssess, "#,###,###,##0");
			txtTotalOldAssessment.Text = Strings.Format(lngTotOldAssess, "#,###,###,##0");
			txtTotal.Text = Strings.Format(lngTotalAccounts, "#,###,###,##0");
		}

		
	}
}
