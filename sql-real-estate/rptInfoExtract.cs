﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using System.IO;
using Global;
using TWSharedLibrary;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptInfoExtract.
	/// </summary>
	public partial class rptInfoExtract : BaseSectionReport
	{
		public static rptInfoExtract InstancePtr
		{
			get
			{
				return (rptInfoExtract)Sys.GetInstance(typeof(rptInfoExtract));
			}
		}

		protected rptInfoExtract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptInfoExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptInfoExtract	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngCount;
		StreamReader ts;
		string strRecord;

		public void Init(string strFile, bool modalDialog)
		{
			if (!File.Exists(strFile))
			{
				MessageBox.Show("File not found", "No File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Close();
				return;
			}
			ts = File.OpenText(strFile);
			if (ts.EndOfStream)
			{
				ts.Close();
				MessageBox.Show("File Empty", "Empty File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "Info Extract", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int intCard;
			intCard = 0;
			eArgs.EOF = ts.EndOfStream;
			strRecord = "";
			while (!ts.EndOfStream && intCard != 1)
			{
				strRecord = ts.ReadLine();
				if (strRecord.Length > 7)
				{
					intCard = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strRecord, 7, 2))));
				}
				else
				{
					intCard = 0;
				}
			}
			if (ts.EndOfStream && intCard != 1)
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			ts.Close();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
			lngCount = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngStreet;
			string strLocation;
			lngCount += 1;
			txtAccount.Text = FCConvert.ToString(Conversion.Val(Strings.Mid(strRecord, 1, 6)));
			txtName.Text = Strings.Trim(Strings.Mid(strRecord, 29, 50));
			txtMap.Text = Strings.Trim(Strings.Mid(strRecord, 9, 20));
			lngStreet = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strRecord, 340, 6))));
			strLocation = "";
			if (lngStreet > 0)
			{
				strLocation = FCConvert.ToString(lngStreet) + " ";
			}
			strLocation += Strings.Trim(Strings.Mid(strRecord, 346, 50));
			txtLocation.Text = strLocation;
			txtAcres.Text = Strings.Format(Conversion.Val(Strings.Mid(strRecord, 876, 11)), "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtCount.Text = Strings.Format(lngCount, "#,###,##0");
		}

		
	}
}
