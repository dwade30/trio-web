﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptNewAssessment.
	/// </summary>
	public partial class rptNewAssessment : BaseSectionReport
	{
		public static rptNewAssessment InstancePtr
		{
			get
			{
				return (rptNewAssessment)Sys.GetInstance(typeof(rptNewAssessment));
			}
		}

		protected rptNewAssessment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				modDataTypes.Statics.MR.DisposeOf();
            }
			base.Dispose(disposing);
		}

		public rptNewAssessment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Assessment";
		}
		// nObj = 1
		//   0	rptNewAssessment	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// Dim dbTemp As DAO.Database
		// vbPorter upgrade warning: varAccount As Variant --> As double
		double varAccount;
		// vbPorter upgrade warning: varcard As Variant --> As double
		double varcard;
		int intPage;
		string strOrder = "";
		// vbPorter upgrade warning: prevtotal As int	OnWrite(int, double)
		int prevtotal;
		int newtotal;
		double dblincrement;
		Decimal prlandtotal;
		// running total of previous land
		Decimal prbldgtotal;
		Decimal nrlandtotal;
		// running total of new land
		Decimal nrbldgtotal;
		Decimal nrtotaltotal;
		// running total of new totals
		bool alldone;
		int NumRecords;
		bool boolOverride;
		string tstring = "";
		string strLineofData = "";
		int intcnum;
		int lngbldg;
		int lngLand;
		int lngTotal;
		int lngPrevLand;
		int lngPrevBldg;
		int lngCount;
		int lngAcctCount;
		int lngLastAccount;
		bool boolSpecific;
		bool boolFromExtract;
		int intTownNumber;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			// create the fields to use for subtotals
			this.Fields.Add("fldPrevLand");
			this.Fields.Add("fldPrevBldg");
			this.Fields.Add("fldNewLand");
			this.Fields.Add("fldNewBldg");
			this.Fields.Add("fldNewTotal");
			this.Fields.Add("grpHeader");
			// Me.Fields.Add ("GroupBinder")
			// Me.Fields("groupbinder").Value = "0"
			this.Fields["grpHeader"].Value = "0";
			lngCount = 0;
			lngAcctCount = 0;
			lngLastAccount = 0;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int intTemp = 0;
			string strTemp = "";
			eArgs.EOF = (modDataTypes.Statics.MR.EndOfFile() || reportCanceled);
			////nextaccount:
			////;
			if (eArgs.EOF)
			{
				alldone = true;
				//Frmassessmentprogress.InstancePtr.Unload();
				this.Fields["fldprevland"].Value = Conversion.Val(this.Fields["fldprevland"].Value) + lngPrevLand;
				this.Fields["fldprevbldg"].Value = Conversion.Val(this.Fields["fldprevbldg"].Value) + lngPrevBldg;
				// txtNewLandGroup.Text = Format(Val(Me.Fields("fldprevland")), "#,###,###,##0")
				this.Fields["fldNewLand"].Value = Conversion.Val(this.Fields["fldnewland"].Value) + lngLand;
				this.Fields["fldNewBldg"].Value = Conversion.Val(this.Fields["fldnewbldg"].Value) + lngbldg;
				this.Fields["fldNewTotal"].Value = Conversion.Val(this.Fields["fldnewtotal"].Value) + lngLand + lngbldg;
				return;
			}
			else
			{
				this.Fields["fldprevland"].Value = Conversion.Val(this.Fields["fldprevland"].Value) + lngPrevLand;
				this.Fields["fldprevbldg"].Value = Conversion.Val(this.Fields["fldprevbldg"].Value) + lngPrevBldg;
				this.Fields["fldNewLand"].Value = Conversion.Val(this.Fields["fldnewland"].Value) + lngLand;
				this.Fields["fldNewBldg"].Value = Conversion.Val(this.Fields["fldnewbldg"].Value) + lngbldg;
				this.Fields["fldNewTotal"].Value = Conversion.Val(this.Fields["fldnewtotal"].Value) + lngLand + lngbldg;
				switch (modPrintRoutines.Statics.intwhichorder)
				{
					case 1:
						{
							// account
							break;
						}
					case 2:
						{
							// name
							strTemp = modDataTypes.Statics.MR.Get_Fields_String("DeedName1");
							if (strTemp.Length > 1)
							{
								this.Fields["grpHeader"].Value = Strings.Left(strTemp, 1);
							}
							else
							{
								this.Fields["grpHeader"].Value = strTemp;
							}
							break;
						}
					case 3:
						{
							strTemp = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + "");
							if (strTemp.Length > 3)
							{
								intTemp = Strings.InStr(1, strTemp, "-", CompareConstants.vbTextCompare);
								if (intTemp > 0)
								{
									strTemp = Strings.Mid(strTemp, 1, intTemp - 1);
								}
								else
								{
									strTemp = Strings.Left(strTemp, 4);
								}
								this.Fields["grpHeader"].Value = strTemp;
							}
							else
							{
								this.Fields["grpHeader"].Value = strTemp;
							}
							break;
						}
					case 4:
						{
							// location
							strTemp = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocstreet") + "");
							this.Fields["grpHeader"].Value = strTemp;
							break;
						}
				}
				//end switch
			}
		}

		private void ActiveReport_PrintProgress(int pageNumber)
		{
			//rptNewAssessment.InstancePtr.Document.Printer.PrintQuality = ddPQMedium;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			
			modGlobalVariables.Statics.boolUseArrays = false;
			modGlobalVariables.Statics.LandTypes.UnloadLandTypes();
            if (!reportCanceled)
            {
                if (modGlobalVariables.Statics.boolUpdateWhenCalculate)
                {
                    modSpeedCalc.SaveSummary();
                }

                if (modSpeedCalc.Statics.boolCalcErrors)
                {
                    MessageBox.Show("There were errors calculating.  Following is a list.", null, MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    //! Load frmShowResults;
                    frmShowResults.InstancePtr.Init(ref modSpeedCalc.Statics.CalcLog);
                    frmShowResults.InstancePtr.lblScreenTitle.Text = "Calculation Errors Log";
                    frmShowResults.InstancePtr.Show();
                }
            }

            modSpeedCalc.Statics.boolCalcErrors = false;
			modSpeedCalc.Statics.CalcLog = "";
		}

        private void RptNewAssessment_ReportEndedAndCanceled(object sender, System.EventArgs e)
        {
          //  Frmassessmentprogress.InstancePtr.Unload();
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			int lngUID;
			string strREFullDBName;
			string strMasterJoin;
			string strMasterJoinJoin = "";
			strREFullDBName = clsTemp.Get_GetFullDBName("RealEstate");
			strMasterJoin = modREMain.GetMasterJoin();
			string strMasterJoinQuery;
			strMasterJoinQuery = "(" + strMasterJoin + ") mj";
			lngUID = modGlobalConstants.Statics.clsSecurityClass.Get_UserID();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			// rptassessment.Zoom = 90
			// 
			// If rptassessment.Printer <> "" Then
			// rptassessment.Printer.PrintQuality = ddPQMedium
			// 
			// End If
			// If boolDotMatrix Then
			// Call MakeTextBoxesInvisible
			// Else
			// Call MakeTextBoxesVisible
			// End If
			// 
			// If RegularPrinter <> vbNullString Then
			// rptassessment.FCGlobal.Printer.DeviceName = RegularPrinter
			// End If
			txtmuniname.Text = modGlobalConstants.Statics.MuniName;
			txtdate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txttotaltotal1.Visible = false;
			txttotaltotal.Visible = false;
			object tempnum;
			// corey
			//Frmassessmentprogress.InstancePtr.Initialize("Only some records were calculated." + "\r\n" + "Cancel anyway?");
			//Frmassessmentprogress.InstancePtr.Label2.Text = "Formatting Summary Pages. Please Wait.";
			//Frmassessmentprogress.InstancePtr.Text = "Summary";
			//Frmassessmentprogress.InstancePtr.cmdcancexempt.Visible = true;
			//Frmassessmentprogress.InstancePtr.Show();
			clsDRWrapper temp = modDataTypes.Statics.CR;
			modREMain.OpenCRTable(ref temp, 1452);
			modDataTypes.Statics.CR = temp;
			txtratio.Text = "Ratio " + Strings.Format(Conversion.Val(modDataTypes.Statics.CR.Get_Fields_String("CBase")) / 100, "#00%");
			switch (modPrintRoutines.Statics.intwhichorder)
			{
				case 1:
					{
						txtorder.Text = "By Account";
						GroupFooter1.Visible = false;
						break;
					}
				case 2:
					{
						txtorder.Text = "By Name";
						break;
					}
				case 3:
					{
						txtorder.Text = "By Map/Lot";
						break;
					}
				case 4:
					{
						txtorder.Text = "By Location";
						break;
					}
			}
			//end switch
			if (intTownNumber > 0)
			{
				txtorder.Text = txtorder.Text + "  (" + modRegionalTown.GetTownKeyName_2(intTownNumber) + ")";
			}
			else if (modGlobalVariables.Statics.boolByRange)
			{
				txtorder.Text = txtorder.Text + "  (";
				if (modPrintRoutines.Statics.gstrFieldName == "RSAccount")
				{
					txtorder.Text = txtorder.Text + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " To " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange);
				}
				else
				{
					txtorder.Text = txtorder.Text + modPrintRoutines.Statics.gstrMinAccountRange + " To " + modPrintRoutines.Statics.gstrMaxAccountRange;
				}
				txtorder.Text = txtorder.Text + ")";
			}
			else if (boolSpecific)
			{
				txtorder.Text = txtorder.Text + "  (Specific)";
			}
			else if (boolFromExtract)
			{
				txtorder.Text = txtorder.Text + " - ";
				clsTemp.OpenRecordset("select * from extract where userid = " + FCConvert.ToString(lngUID) + " and reportnumber = 0", modGlobalVariables.strREDatabase);
				if (!clsTemp.EndOfFile())
				{
					if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("title"))) != string.Empty)
					{
						txtorder.Text = txtorder.Text + Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("title")));
					}
					else
					{
						txtorder.Text = txtorder.Text + "(From Extract)";
					}
				}
				else
				{
					MessageBox.Show("No extract has been done.  Cannot continue", "No Extract", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
					return;
				}
			}
			else
			{
				//FC:FINAL:MSH - i.issue #1189: missed using of 'Text' property
				txtorder.Text = txtorder.Text + "  (ALL)";
			}
			if (modDataTypes.Statics.MR == null)
			{
				modDataTypes.Statics.MR = new clsDRWrapper();
			}
			if (modPrintRoutines.Statics.gstrFieldName == "RSAccount")
			{
				if (intTownNumber > 0)
				{
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where ritrancode = " + FCConvert.ToString(intTownNumber) + " and not rsdeleted = 1 order by rsaccount,RSCard", modGlobalVariables.strREDatabase);
					// Call MR.OpenRecordset(strMasterJoin & " where ritrancode = " & intTownNumber & " and not rsdeleted = 1 order by rsaccount,RSCard", strREDatabase)
				}
				else if (boolSpecific)
				{
					modDataTypes.Statics.MR.OpenRecordset("select  * from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ") order by rsaccount,rscard", modGlobalVariables.strREDatabase);
					// Call MR.OpenRecordset(strMasterJoin & " where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " & lngUID & ") order by rsaccount,rscard", strREDatabase)
				}
				else if (boolFromExtract)
				{
					modDataTypes.Statics.MR.OpenRecordset("select  * from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from extracttable where userid = " + FCConvert.ToString(lngUID) + ") order by rsaccount,rscard", modGlobalVariables.strREDatabase);
					// Call MR.OpenRecordset(strMasterJoin & " where (not rsdeleted = 1) and rsaccount in (select accountnumber from extracttable where userid = " & lngUID & ") order by rsaccount,rscard", strREDatabase)
				}
				else if (modGlobalVariables.Statics.boolByRange)
				{
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where " + modPrintRoutines.Statics.gstrFieldName + " >= " + FCConvert.ToString(modGlobalVariables.Statics.gintMinAccountRange) + " and " + modPrintRoutines.Statics.gstrFieldName + " <= " + FCConvert.ToString(modGlobalVariables.Statics.gintMaxAccountRange) + " and not rsdeleted  = 1 order by rsaccount,RSCard", modGlobalVariables.strREDatabase);
					// Call MR.OpenRecordset(strMasterJoin & " where " & gstrFieldName & " >= " & gintMinAccountRange & " and " & gstrFieldName & " <= " & gintMaxAccountRange & " and not rsdeleted = 1 = 1 order by rsaccount,RSCard", strREDatabase)
				}
				else
				{
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where not rsdeleted = 1 order by rsaccount,RSCard", modGlobalVariables.strREDatabase);
					// Call MR.OpenRecordset(strMasterJoin & " where not rsdeleted = 1 order by rsaccount,RSCard", strREDatabase)
				}
			}
			else
			{
				if (intTownNumber > 0)
				{
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where ritrancode = " + FCConvert.ToString(intTownNumber) + " and not rsdeleted = 1 order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,RSCard", modGlobalVariables.strREDatabase);
					// Call MR.OpenRecordset(strMasterJoin & " where ritrancode = " & intTownNumber & " and not rsdeleted = 1 order by " & gstrFieldName & ",rsaccount,RSCard", strREDatabase)
				}
				else if (boolSpecific)
				{
					modDataTypes.Statics.MR.OpenRecordset("select  * from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " + FCConvert.ToString(lngUID) + ") order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,rscard", modGlobalVariables.strREDatabase);
					// Call MR.OpenRecordset(strMasterJoin & " where (not rsdeleted = 1) and rsaccount in (select accountnumber from labelaccounts where userid = " & lngUID & ") order by " & gstrFieldName & ",rsaccount,rscard", strREDatabase)
                }                
                else if (boolFromExtract)
                {
					// Call MR.OpenRecordset(strMasterJoin & " where (not rsdeleted = 1) and rsaccount in (select accountnumber from extracttable where userid = " & lngUID & ") order by " & gstrFieldName & ",rsaccount,rscard", strREDatabase)
				    modDataTypes.Statics.MR.OpenRecordset("select  * from master where (not rsdeleted = 1) and rsaccount in (select accountnumber from extracttable where userid = " + FCConvert.ToString(lngUID) + ") order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,rscard", modGlobalVariables.strREDatabase);
                }
				else if (modGlobalVariables.Statics.boolByRange)
                {
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where " + modPrintRoutines.Statics.gstrFieldName + " >= '" + modPrintRoutines.Statics.gstrMinAccountRange + "' and " + modPrintRoutines.Statics.gstrFieldName + " <= '" + modPrintRoutines.Statics.gstrMaxAccountRange + "' and  rsdeleted = 0 order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,RSCard", modGlobalVariables.strREDatabase);
					// Call MR.OpenRecordset(strMasterJoin & " where " & gstrFieldName & " >= '" & gstrMinAccountRange & "' and " & gstrFieldName & " <= '" & gstrMaxAccountRange & "' and  rsdeleted = 0 order by " & gstrFieldName & ",rsaccount,RSCard", strREDatabase)
				}
				else
				{
					modDataTypes.Statics.MR.OpenRecordset("SELECT  * FROM Master where not rsdeleted = 1 order by " + modPrintRoutines.Statics.gstrFieldName + ",rsaccount,RSCard", modGlobalVariables.strREDatabase);
					// Call MR.OpenRecordset(strMasterJoin & " where not rsdeleted = 1 order by " & gstrFieldName & ",rsaccount,RSCard", strREDatabase)
				}
			}
			// Call MR.InsertName("OwnerPartyID,SecOwnerPartyID", "Owner,SecOwner", False, True, False, "RE", False, "", True)
			modDataTypes.Statics.MR.MoveLast();
			modDataTypes.Statics.MR.MoveFirst();
			modDataTypes.Statics.MR.Edit();
			NumRecords = modDataTypes.Statics.MR.RecordCount();
			//Frmassessmentprogress.InstancePtr.ProgressBar1.Maximum = 100;
			modGlobalVariables.Statics.boolfromcalcandassessment = true;
			modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
			modGlobalVariables.Statics.gintMaxAccounts = FCConvert.ToInt32(modGlobalRoutines.GetMaxAccounts());
			modGNBas.Statics.gintMaxCards = FCConvert.ToInt32(modREMain.GetRecordCount("Master", "RSAccount"));
			modGlobalVariables.Statics.boolfromcalcandassessment = false;
			if (modDataTypes.Statics.OUT == null)
			{
				modDataTypes.Statics.OUT = new clsDRWrapper();
			}
			if (modDataTypes.Statics.CMR == null)
			{
				modDataTypes.Statics.CMR = new clsDRWrapper();
			}
			if (modDataTypes.Statics.DWL == null)
			{
				modDataTypes.Statics.DWL = new clsDRWrapper();
			}
			// Set MR = dbTemp.OpenRecordset("select * from master where not rsdeleted = 1 order by rsaccount,rscard",strredatabase)
			modDataTypes.Statics.OUT.OpenRecordset("select * from outbuilding order by rsaccount,rscard", modGlobalVariables.strREDatabase);
			modDataTypes.Statics.CMR.OpenRecordset("select * from commercial order by rsaccount,rscard", modGlobalVariables.strREDatabase);
			modDataTypes.Statics.DWL.OpenRecordset("select * from dwelling order by rsaccount,rscard", modGlobalVariables.strREDatabase);
			modSpeedCalc.REAS03_1075_GET_PARAM_RECORD();
			Array.Resize(ref modSpeedCalc.Statics.SummaryList, NumRecords + 1 + 1);
			modSpeedCalc.Statics.SummaryListIndex = 0;
			modProperty.Statics.RUNTYPE = "P";
			modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
		}					              

		private void Detail_Format(object sender, EventArgs e)
		{
			CalcAgain:
			;
			this.Detail.Visible = true;
			if (!modDataTypes.Statics.MR.EndOfFile() && !reportCanceled)
			{
				modDataTypes.Statics.MR.Edit();
				// If MR.Fields("rscard") > 1 Then GoTo SkipCalc
				if (FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rscard")) == 1)
				{
					modREASValuations.Statics.BBPhysicalPercent = 0;
					modREASValuations.Statics.HoldDwellingEconomic = 0;
				}
				modGlobalVariables.Statics.gintLastAccountNumber = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
				modGlobalVariables.Statics.boolfromcalcandassessment = true;
				modSpeedCalc.CalcCard();
				modGlobalVariables.Statics.boolfromcalcandassessment = false;
				SkipCalc:
				;
				intcnum = modGlobalVariables.Statics.intCurrentCard;
				lngLand = modProperty.Statics.lngCorrelatedLand[intcnum];
				lngbldg = modProperty.Statics.lngCorrelatedBldg[intcnum];
				
				lngPrevLand = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + "")));
				lngPrevBldg = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval") + "")));
				boolOverride = false;
				//Frmassessmentprogress.InstancePtr.BringToFront();
                //FC:FINAL:DSE:#1620 Update application on client side
               // FCUtils.ApplicationUpdate(Frmassessmentprogress.InstancePtr);
				prevtotal = FCConvert.ToInt32(Math.Round(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + "")));
				prevtotal += FCConvert.ToInt32(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval") + ""));
				lngTotal = lngLand + lngbldg;
				newtotal = lngTotal;
				txtOV.Visible = false;
				if ((Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivalbldgcode") + "") > 1) || (Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("hivallandcode") + "") > 1))
				{
					boolOverride = true;
					txtOV.Visible = true;
				}
				dblincrement = 0;
				if (prevtotal > 0)
				{
					if (newtotal > 0)
					{
						dblincrement = (newtotal - prevtotal);
						dblincrement = Math.Abs(dblincrement);
						dblincrement /= prevtotal;
						dblincrement *= 100;
						if (dblincrement > 0 && dblincrement < 1)
						{
							dblincrement = 0.5;
						}
						else
						{
							dblincrement = Conversion.Int(0.5 + dblincrement);
						}
					}
				}
				if ((modPrintRoutines.Statics.regorchanged == "chg") && (newtotal == prevtotal) && !boolOverride)
				{
					modDataTypes.Statics.MR.MoveNext();

					this.Detail.Visible = false;
					return;
					// End If
				}
				else
				{
					nrtotaltotal += newtotal;
					nrlandtotal += lngLand;
					nrbldgtotal += lngbldg;
					prlandtotal += FCConvert.ToDecimal(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + ""));
					prbldgtotal += FCConvert.ToDecimal(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval") + ""));
				}
				lngCount += 1;
				if (lngLastAccount != FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount")))
				{
					lngLastAccount = FCConvert.ToInt32(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount"));
					lngAcctCount += 1;
				}
				txtincdec.Text = dblincrement.ToString();
				txtsign.Text = "";
				if (prevtotal > newtotal)
					txtsign.Text = "-";
				txttotal.Text = Strings.Format(newtotal, "###,###,###");
				txtprevtotal.Text = Strings.Format(prlandtotal, "###,###,###");
				txtprevbldgtotal.Text = Strings.Format(prbldgtotal, "###,###,###");
				txtnltotal.Text = Strings.Format(nrlandtotal, "###,###,###");
				txtnbtotal.Text = Strings.Format(nrbldgtotal, "###,###,###");
				txtaccount.Text = modGlobalVariables.Statics.gintLastAccountNumber.ToString();

				txtname.Text = modDataTypes.Statics.MR.Get_Fields_String("DeedName1");

				// txtLocation = Trim(rstemp.fields("RSLOCNUMALPH") & " " & String(5 - Len(Trim(rstemp.fields("RSLOCNUMALPH") & " ")), " ") & rstemp.fields("RSLOCStreet") & " ")
				txtmaplot.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rsmaplot") + "");
				if (modPrintRoutines.Statics.gstrFieldName == "RSLocStreet")
				{
					txtmaplot.Text = Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocnumalph") + "") + Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocapt") + "") + " " + Strings.Trim(modDataTypes.Statics.MR.Get_Fields_String("rslocstreet") + "");
				}
				txtcard.Text = Strings.Format(Strings.Trim(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + ""), " 0");
				txtprevland.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastlandval") + ""), "###,###,##0");
				txtnewland.Text = Strings.Format(lngLand, "###,###,##0");
				txtprevbldg.Text = Strings.Format(Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("lastbldgval") + ""), "##,###,##0");
				txtnewbldg.Text = Strings.Format(lngbldg, "##,###,##0");
				// If boolDotMatrix Then
				// Call FillForDotMatrix
				// End If
				varAccount = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rsaccount") + "");
				varcard = Conversion.Val(modDataTypes.Statics.MR.Get_Fields_Int32("rscard") + "");
				//Application.DoEvents();
				if (modDataTypes.Statics.MR.EndOfFile() == false)
                {
                    var progressPercent = Convert.ToDouble(lngCount) / NumRecords * 100;
                    var progress = Convert.ToInt32(progressPercent);
                    if (progressPercent > 100)
                    {
                        progressPercent = 100;
                    }
					
					//Frmassessmentprogress.InstancePtr.ProgressBar1.Value = progress;
					//Frmassessmentprogress.InstancePtr.lblpercentdone.Text = Strings.Format(progressPercent, "##0.00") + "%";
				}
				else
				{
					//Frmassessmentprogress.InstancePtr.ProgressBar1.Value = NumRecords;
					//Frmassessmentprogress.InstancePtr.lblpercentdone.Text = "100%";
				}
			}
			modDataTypes.Statics.MR.MoveNext();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtPrevLandGroup.Text = Strings.Format(Conversion.Val(this.Fields["fldprevland"].Value), "###,###,###,##0");
			txtPrevBldgGroup.Text = Strings.Format(Conversion.Val(this.Fields["fldprevbldg"].Value), "###,###,###,##0");
			txtNewLandGroup.Text = Strings.Format(Conversion.Val(this.Fields["fldnewland"].Value), "###,###,###,##0");
			txtNewBldgGroup.Text = Strings.Format(Conversion.Val(this.Fields["fldnewbldg"].Value), "###,###,###,##0");
			txtTotalGroup.Text = Strings.Format(Conversion.Val(this.Fields["fldnewtotal"].Value), "###,###,###,##0");
			txtAcctCountGroup.Text = lngAcctCount.ToString();
			txtCountGroup.Text = lngCount.ToString();
			this.Fields["fldprevland"].Value = 0;
			this.Fields["fldprevbldg"].Value = 0;
			this.Fields["fldNewLand"].Value = 0;
			this.Fields["fldNewBldg"].Value = 0;
			this.Fields["fldNewTotal"].Value = 0;
			lngCount = 0;
			lngAcctCount = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			//this.Fields["txtGroup"].Value = rsTemp.Get_Fields("txtGroup");
		}

		private void PageFooter_Format(object sender, EventArgs e)
		{
			if (alldone)
			{
				txttotaltotal.Text = Strings.Format(nrtotaltotal, "###,###,###");
				txttotaltotal.Visible = true;
				txttotaltotal1.Visible = true;
				txtprevtotal.Visible = true;
				txtprevbldgtotal.Visible = true;
				txtnltotal.Visible = true;
				txtnbtotal.Visible = true;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			intPage += 1;
			txtpage.Text = "Page " + FCConvert.ToString(intPage);
		}

		public void Init(ref int intRangeType, ref int intOrderType, int intTownCode = -1)
		{
			intTownNumber = intTownCode;
			boolSpecific = false;
			boolFromExtract = false;
			modGlobalVariables.Statics.boolByRange = false;
			switch (intRangeType)
			{
				case 0:
					{
						// no range
						modGlobalVariables.Statics.boolByRange = false;
						break;
					}
				case 1:
					{
						// range
						modGlobalVariables.Statics.boolByRange = true;
						break;
					}
				case 2:
					{
						// specific accounts
						boolSpecific = true;
						break;
					}
				case 3:
					{
						boolFromExtract = true;
						break;
					}
			}
			//end switch
			modPrintRoutines.Statics.intwhichorder = intOrderType;
			frmReportViewer.InstancePtr.Init(this,allowCancel:true,showModal: true);
		}
		
		
	}
}
