﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using Global;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptBldgOV.
	/// </summary>
	public partial class srptBldgOV : FCSectionReport
	{
		public static srptBldgOV InstancePtr
		{
			get
			{
				return (srptBldgOV)Sys.GetInstance(typeof(srptBldgOV));
			}
		}

		protected srptBldgOV _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsbldgcode?.Dispose();
				clsbldgov?.Dispose();
                clsbldgov = null;
                clsbldgcode = null;
            }
			base.Dispose(disposing);
		}

		public srptBldgOV()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptBldgOV	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper clsbldgcode = new clsDRWrapper();
		clsDRWrapper clsbldgov = new clsDRWrapper();
		int lngCode;
		int lngAmount;
		// vbPorter upgrade warning: lngaverage As int	OnWrite(double, short)
		int lngaverage;
		int lngCount;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsbldgov.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Call clsbldgcode.OpenRecordset("select * from costrecord where crecordnumber between 1800 and 1899 order by crecordnumber", strredatabase)
			string strSQL = "";
			if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber < 1)
			{
				strSQL = "select ribldgcode, sum(hlvalbldg) as thesum, count(ribldgcode) as thecount from master where not rsdeleted = 1 and hivalbldgcode > 2 group by ribldgcode order by ribldgcode";
			}
			else
			{
				strSQL = "select ribldgcode, sum(hlvalbldg) as thesum, count(ribldgcode) as thecount from master where not rsdeleted = 1 and hivalbldgcode > 2 and val(ritrancode & '') = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber) + " group by ribldgcode order by ribldgcode";
			}
			clsbldgcode.OpenRecordset("select * from tblbldgcode order by code", modGlobalVariables.strREDatabase);
			clsbldgov.OpenRecordset(strSQL, modGlobalVariables.strREDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!clsbldgov.EndOfFile())
				{
					lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(clsbldgov.GetData("ribldgcode"))));
					txtBldgCode.Text = lngCode.ToString();
					if (lngCode == 0)
					{
						txtBldgCode.Text = txtBldgCode.Text + " Uncoded";
					}
					else
					{
						// Call clsbldgcode.FindFirstRecord("crecordnumber", 1800 + lngCode)
						// Call clsbldgcode.FindFirstRecord("code", lngCode)
						clsbldgcode.FindFirst("code = " + FCConvert.ToString(lngCode));
						if (!clsbldgcode.NoMatch)
						{
							// txtBldgCode.Text = txtBldgCode.Text & " " & clsbldgcode.GetData("cldesc")
							txtBldgCode.Text = txtBldgCode.Text + " " + clsbldgcode.Get_Fields_String("description");
						}
						else
						{
							txtBldgCode.Text = txtBldgCode.Text + " Unknown";
						}
					}
					lngCount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsbldgov.GetData("thecount"))));
					lngAmount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsbldgov.GetData("thesum"))));
					if (lngCount > 0)
					{
						lngaverage = FCConvert.ToInt32(FCConvert.ToDouble(lngAmount) / lngCount);
					}
					else
					{
						lngaverage = 0;
					}
				}
				else
				{
					txtBldgCode.Text = "";
					lngCount = 0;
					lngAmount = 0;
					lngaverage = 0;
				}
				txtcount.Text = lngCount.ToString();
				txtamount.Text = Strings.Format(lngAmount, "#,###,###,###,##0");
				txtave.Text = Strings.Format(lngaverage, "###,###,###,##0");
				clsbldgov.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
