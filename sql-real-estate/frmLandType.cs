﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmLandType.
	/// </summary>
	public partial class frmLandType : BaseForm
	{
		public frmLandType()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLandType InstancePtr
		{
			get
			{
				return (frmLandType)Sys.GetInstance(typeof(frmLandType));
			}
		}

		protected frmLandType _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		bool boolDataChanged;
		const int CNSTCOLAUTOID = 6;
		const int CNSTCOLCODE = 0;
		const int CNSTCOLDESCRIPTION = 1;
		const int CNSTCOLSHORTDESCRIPTION = 2;
		const int CNSTCOLTYPE = 3;
		const int CNSTCOLCATEGORY = 4;
		const int CNSTCOLFACTOR = 5;

		private void frmLandType_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmLandType_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLandType properties;
			//frmLandType.FillStyle	= 0;
			//frmLandType.ScaleWidth	= 9300;
			//frmLandType.ScaleHeight	= 7530;
			//frmLandType.LinkTopic	= "Form2";
			//frmLandType.LockControls	= true;
			//frmLandType.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			FillGrid();
		}

		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				if (MessageBox.Show("Data has changed. Do you wish to save first?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveData())
						e.Cancel = true;
				}
			}
		}

		private void frmLandType_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			Grid.RowData(Grid.Row, true);
			boolDataChanged = true;
		}

		private void Grid_ComboCloseUp(object sender, EventArgs e)
		{
			if (Grid.Row <= 0)
				return;
			if (Grid.Col == CNSTCOLTYPE)
			{
				if (Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTCOLAUTOID)) > 0)
				{
					if (Grid.TextMatrix(Grid.Row, CNSTCOLTYPE) != Grid.ComboData())
					{
						MessageBox.Show("If there are parcels using this land type," + "\r\n" + "changing the unit type could alter how parcels' units are interpreted and acreage calculated", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
			Grid.RowData(Grid.Row, true);
			boolDataChanged = true;
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						if (Grid.Row > 0)
						{
							mnuDeleteCode_Click();
						}
						break;
					}
			}
			//end switch
		}

        private void Grid_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int tCol;
            //tCol = Grid.MouseCol;
            tCol = Grid.GetFlexColIndex(e.ColumnIndex);
            if (tCol == CNSTCOLFACTOR)
            {
                //ToolTip1.SetToolTip(Grid, "Set this field to 0 if this land type should not add to total acreage");
                Grid[e.ColumnIndex, e.RowIndex].ToolTipText = "Set this field to 0 if this land type should not add to total acreage";
            }
            //else
            //{
            //    ToolTip1.SetToolTip(Grid, "");
            //}
        }

        // Private Sub Grid_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
        // Select Case Col
        // Case CNSTCOLTYPE
        // Select Case GetTypeCode(Grid.TextMatrix(Row, CNSTCOLTYPE))
        // Case CNSTLANDTYPEACRES, CNSTLANDTYPEFRACTIONALACREAGE
        // Grid.ComboList = "Acres|Fractional Acres"
        // Case Else
        // Grid.ComboList = Grid.TextMatrix(Row, CNSTCOLTYPE)
        // End Select
        // Grid.ComboList = "Acres|Fractional Acres|Front Foot|Square Feet"
        // Case CNSTCOLCATEGORY
        // Case Else
        // Grid.ComboList = ""
        // End Select
        // End Sub
        private void mnuAddCode_Click(object sender, System.EventArgs e)
		{
			Grid.Rows += 1;
			Grid.TextMatrix(Grid.Rows - 1, CNSTCOLCATEGORY, FCConvert.ToString(modREConstants.CNSTLANDCATNONE));
			Grid.TextMatrix(Grid.Rows - 1, CNSTCOLTYPE, FCConvert.ToString(modREConstants.CNSTLANDTYPEACRES));
			Grid.TextMatrix(Grid.Rows - 1, CNSTCOLFACTOR, FCConvert.ToString(1));
			Grid.TopRow = Grid.Rows - 1;
		}

		private void mnuDeleteCode_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			GridDelete.AddItem(Grid.TextMatrix(Grid.Row, CNSTCOLAUTOID) + "\t" + Grid.TextMatrix(Grid.Row, CNSTCOLCODE));
			Grid.RemoveItem(Grid.Row);
		}

		public void mnuDeleteCode_Click()
		{
			mnuDeleteCode_Click(cmdDelete, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			rptLandTypes.InstancePtr.Init();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			Grid.Col = 0;
			//Application.DoEvents();
			SaveData();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			Grid.Col = 0;
			//Application.DoEvents();
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private void SetupGrid()
		{
			string strTemp = "";
			Grid.Rows = 1;
			Grid.FixedCols = 0;
			Grid.TextMatrix(0, CNSTCOLCODE, "Code");
			Grid.TextMatrix(0, CNSTCOLDESCRIPTION, "Description");
			Grid.TextMatrix(0, CNSTCOLSHORTDESCRIPTION, "Short Desc.");
			Grid.TextMatrix(0, CNSTCOLTYPE, "Type");
			Grid.TextMatrix(0, CNSTCOLCATEGORY, "Category");
			Grid.TextMatrix(0, CNSTCOLFACTOR, "Acre Fctr");
			Grid.ColHidden(CNSTCOLAUTOID, true);
			strTemp = "#" + FCConvert.ToString(modREConstants.CNSTLANDCATNONE) + ";None|#" + FCConvert.ToString(modREConstants.CNSTLANDCATCROP) + ";Crop Land|#" + FCConvert.ToString(modREConstants.CNSTLANDCATHARDWOOD) + ";Hardwood|#" + FCConvert.ToString(modREConstants.CNSTLANDCATHARDWOODFARM) + ";Hardwood (Farm)";
			strTemp += "|#" + FCConvert.ToString(modREConstants.CNSTLANDCATMIXEDWOOD) + ";Mixed Wood|#" + FCConvert.ToString(modREConstants.CNSTLANDCATMIXEDWOODFARM) + ";Mixed Wood (Farm)|#" + FCConvert.ToString(modREConstants.CNSTLANDCATOPENSPACE) + ";Open Space|#" + FCConvert.ToString(modREConstants.CNSTLANDCATORCHARD) + ";Orchard Land";
			strTemp += "|#" + FCConvert.ToString(modREConstants.CNSTLANDCATPASTURE) + ";Pasture Land|#" + FCConvert.ToString(modREConstants.CNSTLANDCATSOFTWOOD) + ";Softwood|#" + FCConvert.ToString(modREConstants.CNSTLANDCATSOFTWOODFARM) + ";Softwood (Farm)|#" + FCConvert.ToString(modREConstants.CNSTLANDCATWATERFRONT) + ";Working Waterfront";
			Grid.ColComboList(CNSTCOLCATEGORY, strTemp);
			strTemp = "#" + FCConvert.ToString(modREConstants.CNSTLANDTYPEACRES) + ";Acres|#" + FCConvert.ToString(modREConstants.CNSTLANDTYPEFRACTIONALACREAGE) + ";Fractional Acres|#" + FCConvert.ToString(modREConstants.CNSTLANDTYPEFRONTFOOT) + ";Front Foot|#" + FCConvert.ToString(modREConstants.CNSTLANDTYPEFRONTFOOTSCALED) + ";Front Foot - Width|#" + FCConvert.ToString(modREConstants.CNSTLANDTYPEIMPROVEMENTS) + ";Improvements|#" + FCConvert.ToString(modREConstants.CNSTLANDTYPELINEARFEET) + ";Linear Feet|#" + FCConvert.ToString(modREConstants.CNSTLANDTYPELINEARFEETSCALED) + ";Linear Feet - Width|#" + FCConvert.ToString(modREConstants.CNSTLANDTYPESITE) + ";Site|#" + FCConvert.ToString(modREConstants.CNSTLANDTYPESQUAREFEET) + ";Square Feet";
			Grid.ColComboList(CNSTCOLTYPE, strTemp);
			//FC:FINAL:CHN - issue #1616: Land Types Form redesign.
			Grid.ColAlignment(CNSTCOLCODE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(CNSTCOLFACTOR, FCGrid.AlignmentSettings.flexAlignLeftCenter);

        }

		private void FillGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				Grid.Rows = 1;
				clsLoad.OpenRecordset("select * from landtype order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("id")));
					// TODO Get_Fields: Check the table for the column [code] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTCOLCODE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))));
					Grid.TextMatrix(lngRow, CNSTCOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
					Grid.TextMatrix(lngRow, CNSTCOLSHORTDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("shortdescription")));
					Grid.TextMatrix(lngRow, CNSTCOLFACTOR, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("factor"))), "0.00"));
					// TODO Get_Fields: Check the table for the column [type] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTCOLTYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("type"))));
					// TODO Get_Fields: Check the table for the column [category] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngRow, CNSTCOLCATEGORY, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("category"))));
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTCOLCODE, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTCOLDESCRIPTION, FCConvert.ToInt32(0.27 * GridWidth));
			Grid.ColWidth(CNSTCOLSHORTDESCRIPTION, FCConvert.ToInt32(0.13 * GridWidth));
			Grid.ColWidth(CNSTCOLTYPE, FCConvert.ToInt32(0.17 * GridWidth));
			Grid.ColWidth(CNSTCOLCATEGORY, FCConvert.ToInt32(0.24 * GridWidth));
		}

		private bool SaveData()
		{
			bool SaveData = false;
			int lngTemp = 0;
			string strTemp1 = "";
			string strTemp2 = "";
			int intCategory = 0;
			int intType = 0;
			clsDRWrapper clsSave = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			int x;
			bool boolAdd = false;
			try
			{
				// On Error GoTo ErrorHandler
				SaveData = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// get rid of deleteds first
				for (x = 0; x <= GridDelete.Rows - 1; x++)
				{
					if (Conversion.Val(GridDelete.TextMatrix(x, 1)) > 0)
					{
						clsSave.Execute("delete from landschedule where code = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(x, 1))), modGlobalVariables.Statics.strRECostFileDatabase);
					}
					modGlobalFunctions.AddCYAEntry_80("RE", "deleted landtype ", "id " + GridDelete.TextMatrix(x, 0), "Code " + GridDelete.TextMatrix(x, 1));
					clsSave.Execute("delete from landtype where id = " + GridDelete.TextMatrix(x, 0), modGlobalVariables.Statics.strRECostFileDatabase);
				}
				// x
				GridDelete.Rows = 0;
				// CHECK for duplicate codes
				if (Grid.Rows > 1)
				{
					for (x = 1; x <= Grid.Rows - 2; x++)
					{
						if (Conversion.Val(Grid.TextMatrix(x, CNSTCOLCODE)) == 0)
						{
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							MessageBox.Show("Cannot have a blank or 0 code", "Bad Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return SaveData;
						}
						else if (Conversion.Val(Grid.TextMatrix(x, CNSTCOLCODE)) < 11)
						{
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							MessageBox.Show("Cannot have a code less than 11", "Bad Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return SaveData;
						}
						else if (Conversion.Val(Grid.TextMatrix(x, CNSTCOLCODE)) == 99)
						{
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							MessageBox.Show("Cannot save because code 99 is reserved by the system as the extra influence code", "Reserved Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return SaveData;
						}
						lngTemp = Grid.FindRow(Grid.TextMatrix(x, CNSTCOLCODE), x + 1, CNSTCOLCODE);
						if (lngTemp >= 0)
						{
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
							MessageBox.Show("Cannot save because you have two land types with the same code", "Duplicate Codes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return SaveData;
						}
					}
					// x
					x = Grid.Rows - 1;
					if (Conversion.Val(Grid.TextMatrix(x, CNSTCOLCODE)) == 0)
					{
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						MessageBox.Show("Cannot have a blank or 0 code", "Bad Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveData;
					}
					else if (Conversion.Val(Grid.TextMatrix(x, CNSTCOLCODE)) < 11)
					{
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						MessageBox.Show("Cannot have a code less than 11", "Bad Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveData;
					}
					else if (Conversion.Val(Grid.TextMatrix(x, CNSTCOLCODE)) == 99)
					{
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						MessageBox.Show("Cannot save because code 99 is reserved by the system as the extra influence code", "Reserved Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveData;
					}
				}
				// now save the changed ones
				lngTemp = Grid.FindRow(true, 1);
				while (lngTemp > 0)
				{
					boolAdd = false;
					clsSave.OpenRecordset("select * from landtype where id = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLAUTOID))), modGlobalVariables.Statics.strRECostFileDatabase);
					if (Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLAUTOID)) > 0)
					{
						clsSave.Edit();
					}
					else
					{
						clsSave.AddNew();
						modProperty.Statics.gintCurrentLandSchedule = 0;
						// force to update types next time it is calculated
						boolAdd = true;
						modGlobalFunctions.AddCYAEntry_80("RE", "Added land type", "id " + clsSave.Get_Fields_Int32("id"), "Code " + Grid.TextMatrix(lngTemp, CNSTCOLCODE));
					}
					intCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLCATEGORY))));
					intType = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLTYPE))));
					clsSave.Set_Fields("factor", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLFACTOR))));
					clsSave.Set_Fields("Description", Grid.TextMatrix(lngTemp, CNSTCOLDESCRIPTION));
					clsSave.Set_Fields("shortdescription", Grid.TextMatrix(lngTemp, CNSTCOLSHORTDESCRIPTION));
					clsSave.Set_Fields("code", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLCODE))));
					clsSave.Set_Fields("category", intCategory);
					clsSave.Set_Fields("type", intType);
					clsSave.Update();
					clsSave.MoveFirst();
					Grid.TextMatrix(lngTemp, CNSTCOLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("id")));
					Grid.RowData(lngTemp, false);
					if (boolAdd)
					{
						// ADD TO SCHEDULES
						if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
						{
							clsTemp.Execute("insert into landschedule (schedule,code,amount,exponent1,exponent2,townnumber,widthexponent1,widthexponent2) select schedule," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLCODE))) + " as code,0 as amount,.5 as exponent1,.5 as exponent2,0 as townnumber, .5 as widthexponent1, .5 as widthexponent2 from landschedule group by schedule", modGlobalVariables.strREDatabase);
						}
						else
						{
							clsTemp.Execute("insert into landschedule (schedule,code,amount,exponent1,exponent2,townnumber,widthexponent1,widthexponent2) select schedule," + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngTemp, CNSTCOLCODE))) + " as code,0 as amount,.5 as exponent1,.5 as exponent2,townnumber, .5 as widthexponent1, .5 as widthexponent2 from landschedule group by schedule, townnumber", modGlobalVariables.strREDatabase);
						}
					}
					lngTemp = Grid.FindRow(true, lngTemp + 1);
				}
				modGlobalFunctions.AddCYAEntry_8("RE", "Saved Land Types");
				boolDataChanged = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveData = true;
				FillGrid();
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}
	}
}
