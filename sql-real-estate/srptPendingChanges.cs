﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for srptPendingChanges.
	/// </summary>
	public partial class srptPendingChanges : FCSectionReport
	{
		public static srptPendingChanges InstancePtr
		{
			get
			{
				return (srptPendingChanges)Sys.GetInstance(typeof(srptPendingChanges));
			}
		}

		protected srptPendingChanges _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptPendingChanges()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptPendingChanges	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngCurrentRow;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngCurrentRow >= frmPendingTransfers.InstancePtr.GridReport.Rows;
			if (!eArgs.EOF)
			{
				eArgs.EOF = frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 5) == "Transfers";
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngCurrentRow = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!(frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 5) == "Transfers"))
			{
				txtAccount.Text = frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 0);
				txtCard.Text = frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 1);
				txtAction.Text = frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 2);
				txtMinDate.Text = frmPendingTransfers.InstancePtr.GridReport.TextMatrix(lngCurrentRow, 3);
				lngCurrentRow += 1;
			}
		}

		
	}
}
