﻿using fecherFoundation;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptLabels.
	/// </summary>
	partial class rptLabels
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLabels));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.txtLabel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.txtLabel2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtlabel3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtland = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtbldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtexempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txttotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblLand = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBldg = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExempt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtExempt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBldg2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtland2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			//this.Grid1 = new GrapeCity.ActiveReports.SectionReportModel.CustomControl();
			((System.ComponentModel.ISupportInitialize)(this.txtLabel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLabel2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtlabel3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtexempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBldg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExempt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.ColumnDirection = GrapeCity.ActiveReports.SectionReportModel.ColumnDirection.AcrossDown;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtLabel1,
				this.Shape1,
				this.txtLabel2,
				this.txtlabel3,
				this.txtYear,
				this.txtland,
				this.txtbldg,
				this.txtexempt,
				this.txttotal,
				this.txtTax,
				this.lblLand,
				this.lblBldg,
				this.lblExempt,
				this.lblTotal,
				this.txtTotal2,
				this.txtExempt2,
				this.txtBldg2,
				this.txtland2
			});
			this.Detail.Height = 0.875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			//this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
			//this.Grid1});
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// txtLabel1
			// 
			this.txtLabel1.CanGrow = false;
			this.txtLabel1.Height = 1F;
			this.txtLabel1.Left = 0.0625F;
			this.txtLabel1.Name = "txtLabel1";
			this.txtLabel1.Style = "font-family: \'Courier New\'; font-size: 10pt; ddo-char-set: 0";
			this.txtLabel1.Tag = "label";
			this.txtLabel1.Text = "Field1";
			this.txtLabel1.Top = 0F;
			this.txtLabel1.Width = 3.9375F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 0.875F;
			this.Shape1.Left = 0F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0.0625F;
			this.Shape1.Visible = false;
			this.Shape1.Width = 3.9375F;
			// 
			// txtLabel2
			// 
			this.txtLabel2.Height = 0.1875F;
			this.txtLabel2.Left = 5.625F;
			this.txtLabel2.Name = "txtLabel2";
			this.txtLabel2.Style = "font-family: \'Courier New\'";
			this.txtLabel2.Tag = "label";
			this.txtLabel2.Text = "label2";
			this.txtLabel2.Top = 0.3125F;
			this.txtLabel2.Visible = false;
			this.txtLabel2.Width = 0.75F;
			// 
			// txtlabel3
			// 
			this.txtlabel3.Height = 0.1875F;
			this.txtlabel3.Left = 5.625F;
			this.txtlabel3.Name = "txtlabel3";
			this.txtlabel3.Style = "font-family: \'Courier New\'";
			this.txtlabel3.Tag = "label";
			this.txtlabel3.Text = "label3";
			this.txtlabel3.Top = 0.0625F;
			this.txtlabel3.Visible = false;
			this.txtlabel3.Width = 0.75F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.1875F;
			this.txtYear.Left = 0.0625F;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Courier New\'; text-align: right; ddo-char-set: 0";
			this.txtYear.Tag = "label";
			this.txtYear.Text = "acct";
			this.txtYear.Top = 0.0625F;
			this.txtYear.Width = 0.5625F;
			// 
			// txtland
			// 
			this.txtland.Height = 0.1875F;
			this.txtland.Left = 0.625F;
			this.txtland.Name = "txtland";
			this.txtland.OutputFormat = resources.GetString("txtland.OutputFormat");
			this.txtland.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtland.Tag = "label";
			this.txtland.Text = "land value";
			this.txtland.Top = 0.0625F;
			this.txtland.Width = 1.25F;
			// 
			// txtbldg
			// 
			this.txtbldg.Height = 0.1875F;
			this.txtbldg.Left = 1.875F;
			this.txtbldg.Name = "txtbldg";
			this.txtbldg.OutputFormat = resources.GetString("txtbldg.OutputFormat");
			this.txtbldg.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtbldg.Tag = "label";
			this.txtbldg.Text = "bldg value";
			this.txtbldg.Top = 0.0625F;
			this.txtbldg.Width = 1.125F;
			// 
			// txtexempt
			// 
			this.txtexempt.Height = 0.1875F;
			this.txtexempt.Left = 3F;
			this.txtexempt.Name = "txtexempt";
			this.txtexempt.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtexempt.Text = "exempt";
			this.txtexempt.Top = 0.0625F;
			this.txtexempt.Width = 0.75F;
			// 
			// txttotal
			// 
			this.txttotal.Height = 0.1875F;
			this.txttotal.Left = 3.875F;
			this.txttotal.Name = "txttotal";
			this.txttotal.OutputFormat = resources.GetString("txttotal.OutputFormat");
			this.txttotal.Style = "font-family: \'Courier New\'; text-align: right";
			this.txttotal.Tag = "label";
			this.txttotal.Text = "total";
			this.txttotal.Top = 0.0625F;
			this.txttotal.Visible = false;
			this.txttotal.Width = 0.9375F;
			// 
			// txtTax
			// 
			this.txtTax.Height = 0.19F;
			this.txtTax.Left = 2.875F;
			this.txtTax.Name = "txtTax";
			this.txtTax.OutputFormat = resources.GetString("txtTax.OutputFormat");
			this.txtTax.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtTax.Tag = "label";
			this.txtTax.Text = "Field1";
			this.txtTax.Top = 0.71875F;
			this.txtTax.Visible = false;
			this.txtTax.Width = 1.0625F;
			// 
			// lblLand
			// 
			this.lblLand.Height = 0.19F;
			this.lblLand.HyperLink = null;
			this.lblLand.Left = 2.3125F;
			this.lblLand.MultiLine = false;
			this.lblLand.Name = "lblLand";
			this.lblLand.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblLand.Tag = "label";
			this.lblLand.Text = "Land";
			this.lblLand.Top = 0.0625F;
			this.lblLand.Visible = false;
			this.lblLand.Width = 0.5625F;
			// 
			// lblBldg
			// 
			this.lblBldg.Height = 0.19F;
			this.lblBldg.HyperLink = null;
			this.lblBldg.Left = 2.3125F;
			this.lblBldg.MultiLine = false;
			this.lblBldg.Name = "lblBldg";
			this.lblBldg.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblBldg.Tag = "label";
			this.lblBldg.Text = "BLDG";
			this.lblBldg.Top = 0.25F;
			this.lblBldg.Visible = false;
			this.lblBldg.Width = 0.5625F;
			// 
			// lblExempt
			// 
			this.lblExempt.Height = 0.19F;
			this.lblExempt.HyperLink = null;
			this.lblExempt.Left = 2.208333F;
			this.lblExempt.Name = "lblExempt";
			this.lblExempt.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblExempt.Tag = "label";
			this.lblExempt.Text = "Exempt";
			this.lblExempt.Top = 0.40625F;
			this.lblExempt.Visible = false;
			this.lblExempt.Width = 0.6666667F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.19F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 2.3125F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblTotal.Tag = "label";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.5625F;
			this.lblTotal.Visible = false;
			this.lblTotal.Width = 0.5625F;
			// 
			// txtTotal2
			// 
			this.txtTotal2.Height = 0.19F;
			this.txtTotal2.Left = 2.875F;
			this.txtTotal2.Name = "txtTotal2";
			this.txtTotal2.OutputFormat = resources.GetString("txtTotal2.OutputFormat");
			this.txtTotal2.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtTotal2.Tag = "label";
			this.txtTotal2.Text = "Field1";
			this.txtTotal2.Top = 0.5625F;
			this.txtTotal2.Width = 1.0625F;
			// 
			// txtExempt2
			// 
			this.txtExempt2.Height = 0.19F;
			this.txtExempt2.Left = 2.875F;
			this.txtExempt2.Name = "txtExempt2";
			this.txtExempt2.OutputFormat = resources.GetString("txtExempt2.OutputFormat");
			this.txtExempt2.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtExempt2.Tag = "label";
			this.txtExempt2.Text = "Field2";
			this.txtExempt2.Top = 0.40625F;
			this.txtExempt2.Visible = false;
			this.txtExempt2.Width = 1.0625F;
			// 
			// txtBldg2
			// 
			this.txtBldg2.Height = 0.1875F;
			this.txtBldg2.Left = 2.875F;
			this.txtBldg2.Name = "txtBldg2";
			this.txtBldg2.OutputFormat = resources.GetString("txtBldg2.OutputFormat");
			this.txtBldg2.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtBldg2.Tag = "label";
			this.txtBldg2.Text = "Field1";
			this.txtBldg2.Top = 0.21875F;
			this.txtBldg2.Width = 1.0625F;
			// 
			// txtland2
			// 
			this.txtland2.Height = 0.1875F;
			this.txtland2.Left = 2.875F;
			this.txtland2.Name = "txtland2";
			this.txtland2.OutputFormat = resources.GetString("txtland2.OutputFormat");
			this.txtland2.Style = "font-family: \'Courier New\'; text-align: right";
			this.txtland2.Tag = "label";
			this.txtland2.Text = "Field1";
			this.txtland2.Top = 0.0625F;
			this.txtland2.Visible = false;
			this.txtland2.Width = 1.041667F;
			// 
			// GRID1
			// 
			//         this.Grid1.Height = 0.09375F;
			//         this.Grid1.Left = 0.0625F;
			//         this.Grid1.Name = "GRID1";
			//         this.Grid1.Top = 0.0625F;
			//         this.Grid1.Visible = false;
			//         this.Grid1.Width = 6.375F;
			//this.Grid1.Type = typeof(fecherFoundation.FCReportGrid);
			// 
			// rptLabels
			//
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.1875F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Courier New\'; font-style: italic; font-variant: inherit; font-weigh" + "t: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtLabel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLabel2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtlabel3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtbldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtexempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txttotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBldg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExempt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtExempt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBldg2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtland2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLabel1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLabel2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtlabel3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtland;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtbldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtexempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txttotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLand;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBldg;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExempt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtExempt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBldg2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtland2;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		//private GrapeCity.ActiveReports.SectionReportModel.CustomControl Grid1;
	}
}
