﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmGetDirectory.
	/// </summary>
	public partial class frmGetDirectory : BaseForm
	{
		public frmGetDirectory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetDirectory InstancePtr
		{
			get
			{
				return (frmGetDirectory)Sys.GetInstance(typeof(frmGetDirectory));
			}
		}

		protected frmGetDirectory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strReturn;
		int oldIndex;
		bool boolCanExit;

		public string Init(string strCaption1 = "", string strCaption2 = "", bool boolAllowNewDirs = true, string strCaption = "", bool boolAllowEscape = false, string strInitPath = "")
		{
			string Init = "";
			//FileSystemObject fso = new FileSystemObject();
			boolCanExit = boolAllowEscape;
			Label1.Text = strCaption1;
			Label2.Text = strCaption2;
			if (strCaption != string.Empty)
				this.Text = strCaption;
			strReturn = "";
			Init = "";
			if (!boolAllowNewDirs)
			{
				image1.Visible = false;
			}
			//if (boolAllowEscape) cmdCancel.Visible = true;
			if (strInitPath != string.Empty)
			{
				Drive1.Drive = Directory.GetDirectoryRoot(strInitPath);
				Dir1.Path = strInitPath;
			}
			this.Show(FCForm.FormShowEnum.Modal);
			Init = strReturn;
			return Init;
		}

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			string strDLL = "";
			try
			{
				// On Error GoTo ErrorHandler
				modGlobalVariables.Statics.strGetPath = Dir1.Path;
				strReturn = Dir1.Path;
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + " occured.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Unload();
			}
		}

		public void cmdDone_Click()
		{
			cmdDone_Click(cmdDone, new System.EventArgs());
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			modGlobalVariables.Statics.strGetPath = "";
			strReturn = "CANCEL";
			this.Unload();
		}

		public void cmdCancel_Click()
		{
			//cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void Dir1_Click(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			if (Dir1.SelectedIndex >= 0)
			{
				Dir1.Path = Dir1.Items[Dir1.SelectedIndex].ToString();
			}
		}

		private void Drive1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				Dir1.Path = Strings.Mid(Drive1.Drive, 1, 2) + "\\";
				oldIndex = Drive1.SelectedIndex;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 68)
				{
					MessageBox.Show("Drive Not Ready", "Drive Not Ready", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Drive1.SelectedIndex = oldIndex;
					return;
				}
				else
				{
					MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void frmGetDirectory_Activated(object sender, System.EventArgs e)
		{
			oldIndex = Drive1.SelectedIndex;
		}

		private void frmGetDirectory_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						if (boolCanExit)
						{
							cmdCancel_Click();
						}
						break;
					}
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						cmdDone_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGetDirectory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetDirectory properties;
			//frmGetDirectory.ScaleWidth	= 5880;
			//frmGetDirectory.ScaleHeight	= 4275;
			//frmGetDirectory.LinkTopic	= "Form1";
			//frmGetDirectory.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void image1_Click(object sender, System.EventArgs e)
		{
			//FileSystemObject fso = new FileSystemObject();
			object strTemp = "";
			if (Directory.Exists(Dir1.Path))
			{
				if (frmInput.InstancePtr.Init(ref strTemp, "Create Folder", "Enter a name for the new folder", 1440, false, modGlobalConstants.InputDTypes.idtString))
				{
					if (Strings.Trim(FCConvert.ToString(strTemp)) != string.Empty)
					{
						Directory.CreateDirectory(Dir1.Path + "\\" + strTemp);
						Dir1.Path = Dir1.Path + "\\" + strTemp;
						Dir1.Refresh();
					}
				}
			}
		}

		private void image1_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			image1.BorderStyle = BorderStyle.Solid;
			//Application.DoEvents();
		}

		private void image1_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			image1.BorderStyle = BorderStyle.None;
		}
	}
}
