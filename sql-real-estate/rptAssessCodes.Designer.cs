﻿namespace TWRE0000
{
	/// <summary>
	/// Summary description for rptAssessCodes.
	/// </summary>
	partial class rptAssessCodes
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAssessCodes));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLBCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLBLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLBBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLBExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLBTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtLLCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLLLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLLBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLLExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLLTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport3 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtLTCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLTLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLTBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLTExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLTTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport4 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCBCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCBLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCBBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCBExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCBTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport5 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCLCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCLLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCLBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCLExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCLTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.SubReport6 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtCTCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCTLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCTBldg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCTExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCTTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLBCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLBLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLBBldg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLBExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLBTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLBldg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTBldg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCBCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCBLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCBBldg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCBExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCBTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCLCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCLLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCLBldg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCLExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCLTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTBldg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Field1,
            this.txtLBCount,
            this.txtLBLand,
            this.txtLBBldg,
            this.txtLBExempt,
            this.txtLBTotal,
            this.Label10,
            this.SubReport1,
            this.SubReport2,
            this.Label11,
            this.Label12,
            this.txtLLCount,
            this.txtLLLand,
            this.txtLLBldg,
            this.txtLLExempt,
            this.txtLLTotal,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Label23,
            this.Label24,
            this.Label25,
            this.SubReport3,
            this.Label26,
            this.txtLTCount,
            this.txtLTLand,
            this.txtLTBldg,
            this.txtLTExempt,
            this.txtLTTotal,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.Label32,
            this.Label33,
            this.SubReport4,
            this.Label34,
            this.txtCBCount,
            this.txtCBLand,
            this.txtCBBldg,
            this.txtCBExempt,
            this.txtCBTotal,
            this.Label35,
            this.SubReport5,
            this.Label36,
            this.Label37,
            this.Label38,
            this.Label39,
            this.Label40,
            this.Label41,
            this.Label42,
            this.txtCLCount,
            this.txtCLLand,
            this.txtCLBldg,
            this.txtCLExempt,
            this.txtCLTotal,
            this.Label43,
            this.Label44,
            this.Label45,
            this.Label46,
            this.Label47,
            this.Label48,
            this.Label49,
            this.SubReport6,
            this.Label50,
            this.txtCTCount,
            this.txtCTLand,
            this.txtCTBldg,
            this.txtCTExempt,
            this.txtCTTotal});
            this.Detail.Height = 7.9375F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // Label2
            // 
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.Label2.Text = "Bldg Code";
            this.Label2.Top = 0.21875F;
            this.Label2.Width = 0.875F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.34375F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 1.125F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.Label3.Text = "Card Count";
            this.Label3.Top = 0.21875F;
            this.Label3.Width = 0.5F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 2F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.Label4.Text = "Land";
            this.Label4.Top = 0.21875F;
            this.Label4.Width = 0.9375F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1875F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 3.25F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.Label5.Text = "Buildings";
            this.Label5.Top = 0.21875F;
            this.Label5.Width = 1.1875F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.1875F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 4.875F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.Label6.Text = "Exemption";
            this.Label6.Top = 0.21875F;
            this.Label6.Width = 1.0625F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 6.5F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.Label7.Text = "Total";
            this.Label7.Top = 0.21875F;
            this.Label7.Width = 0.9375F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.1875F;
            this.Field1.Left = 2F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Field1.Text = "Billing Amounts By Bldg Code";
            this.Field1.Top = 0F;
            this.Field1.Width = 3.5625F;
            // 
            // txtLBCount
            // 
            this.txtLBCount.Height = 0.1875F;
            this.txtLBCount.Left = 1.0625F;
            this.txtLBCount.Name = "txtLBCount";
            this.txtLBCount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; white-space: nowrap";
            this.txtLBCount.Text = "Field2";
            this.txtLBCount.Top = 0.75F;
            this.txtLBCount.Width = 0.5625F;
            // 
            // txtLBLand
            // 
            this.txtLBLand.Height = 0.19F;
            this.txtLBLand.Left = 1.75F;
            this.txtLBLand.Name = "txtLBLand";
            this.txtLBLand.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; white-space: nowrap";
            this.txtLBLand.Text = "Field3";
            this.txtLBLand.Top = 0.75F;
            this.txtLBLand.Width = 1.1875F;
            // 
            // txtLBBldg
            // 
            this.txtLBBldg.Height = 0.15625F;
            this.txtLBBldg.Left = 3.0625F;
            this.txtLBBldg.Name = "txtLBBldg";
            this.txtLBBldg.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; white-space: nowrap";
            this.txtLBBldg.Text = "Field4";
            this.txtLBBldg.Top = 0.75F;
            this.txtLBBldg.Width = 1.375F;
            // 
            // txtLBExempt
            // 
            this.txtLBExempt.Height = 0.19F;
            this.txtLBExempt.Left = 4.5625F;
            this.txtLBExempt.MultiLine = false;
            this.txtLBExempt.Name = "txtLBExempt";
            this.txtLBExempt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; white-space: nowrap";
            this.txtLBExempt.Text = "Field5";
            this.txtLBExempt.Top = 0.75F;
            this.txtLBExempt.Width = 1.375F;
            // 
            // txtLBTotal
            // 
            this.txtLBTotal.Height = 0.1875F;
            this.txtLBTotal.Left = 6F;
            this.txtLBTotal.Name = "txtLBTotal";
            this.txtLBTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; white-space: nowrap";
            this.txtLBTotal.Text = "Field6";
            this.txtLBTotal.Top = 0.75F;
            this.txtLBTotal.Width = 1.4375F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.15625F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0.0625F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label10.Text = "Total";
            this.Label10.Top = 0.75F;
            this.Label10.Width = 0.875F;
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.15625F;
            this.SubReport1.Left = 0F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0.5625F;
            this.SubReport1.Width = 7.5F;
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.1875F;
            this.SubReport2.Left = 0F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 1.8125F;
            this.SubReport2.Width = 7.4375F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1875F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 2.0625F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label11.Text = "Billing Amounts By Land Code";
            this.Label11.Top = 1.28125F;
            this.Label11.Width = 3.5625F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.15625F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 0.0625F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label12.Text = "Total";
            this.Label12.Top = 2.03125F;
            this.Label12.Width = 0.875F;
            // 
            // txtLLCount
            // 
            this.txtLLCount.Height = 0.15625F;
            this.txtLLCount.Left = 1F;
            this.txtLLCount.Name = "txtLLCount";
            this.txtLLCount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtLLCount.Text = "Field2";
            this.txtLLCount.Top = 2.03125F;
            this.txtLLCount.Width = 0.5625F;
            // 
            // txtLLLand
            // 
            this.txtLLLand.Height = 0.15625F;
            this.txtLLLand.Left = 1.75F;
            this.txtLLLand.Name = "txtLLLand";
            this.txtLLLand.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtLLLand.Text = "Field2";
            this.txtLLLand.Top = 2.03125F;
            this.txtLLLand.Width = 1.1875F;
            // 
            // txtLLBldg
            // 
            this.txtLLBldg.Height = 0.15625F;
            this.txtLLBldg.Left = 3F;
            this.txtLLBldg.Name = "txtLLBldg";
            this.txtLLBldg.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtLLBldg.Text = "Field2";
            this.txtLLBldg.Top = 2.03125F;
            this.txtLLBldg.Width = 1.4375F;
            // 
            // txtLLExempt
            // 
            this.txtLLExempt.Height = 0.15625F;
            this.txtLLExempt.Left = 4.5625F;
            this.txtLLExempt.Name = "txtLLExempt";
            this.txtLLExempt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtLLExempt.Text = "Field2";
            this.txtLLExempt.Top = 2.03125F;
            this.txtLLExempt.Width = 1.3125F;
            // 
            // txtLLTotal
            // 
            this.txtLLTotal.Height = 0.15625F;
            this.txtLLTotal.Left = 5.9375F;
            this.txtLLTotal.Name = "txtLLTotal";
            this.txtLLTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtLLTotal.Text = "Field2";
            this.txtLLTotal.Top = 2.03125F;
            this.txtLLTotal.Width = 1.5F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.344F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 1.0625F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label13.Text = "Card Count";
            this.Label13.Top = 1.5F;
            this.Label13.Width = 0.5F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.188F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 0F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label14.Text = "Land Code";
            this.Label14.Top = 1.5F;
            this.Label14.Width = 0.9375F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.188F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 1.8125F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label15.Text = "Land";
            this.Label15.Top = 1.5F;
            this.Label15.Width = 1.125F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1875F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 3.3125F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label16.Text = "Buildings";
            this.Label16.Top = 1.5F;
            this.Label16.Width = 1.125F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1875F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 4.75F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label17.Text = "Exemption";
            this.Label17.Top = 1.5F;
            this.Label17.Width = 1.125F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.188F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 6.4375F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label18.Text = "Total";
            this.Label18.Top = 1.46875F;
            this.Label18.Width = 1F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.1875F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 2.0625F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label19.Text = "Billing Amounts By Tran Code";
            this.Label19.Top = 2.59375F;
            this.Label19.Width = 3.5625F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.1875F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 0.0625F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label20.Text = "Tran Code";
            this.Label20.Top = 2.8125F;
            this.Label20.Width = 0.875F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.34375F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 1.0625F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label21.Text = "Card Count";
            this.Label21.Top = 2.8125F;
            this.Label21.Width = 0.5F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.1875F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 1.6875F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label22.Text = "Land";
            this.Label22.Top = 2.8125F;
            this.Label22.Width = 1.125F;
            // 
            // Label23
            // 
            this.Label23.Height = 0.1875F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 3.3125F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label23.Text = "Buildings";
            this.Label23.Top = 2.8125F;
            this.Label23.Width = 1.125F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.1875F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 4.6875F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label24.Text = "Exemption";
            this.Label24.Top = 2.8125F;
            this.Label24.Width = 1.125F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.1875F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 6.4375F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label25.Text = "Total";
            this.Label25.Top = 2.8125F;
            this.Label25.Width = 1F;
            // 
            // SubReport3
            // 
            this.SubReport3.CloseBorder = false;
            this.SubReport3.Height = 0.1875F;
            this.SubReport3.Left = 0F;
            this.SubReport3.Name = "SubReport3";
            this.SubReport3.Report = null;
            this.SubReport3.Top = 3.1875F;
            this.SubReport3.Width = 7.5F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.1875F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 0.0625F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label26.Text = "Total";
            this.Label26.Top = 3.40625F;
            this.Label26.Width = 0.875F;
            // 
            // txtLTCount
            // 
            this.txtLTCount.CanGrow = false;
            this.txtLTCount.Height = 0.1875F;
            this.txtLTCount.Left = 1F;
            this.txtLTCount.MultiLine = false;
            this.txtLTCount.Name = "txtLTCount";
            this.txtLTCount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtLTCount.Text = "Field2";
            this.txtLTCount.Top = 3.40625F;
            this.txtLTCount.Width = 0.5625F;
            // 
            // txtLTLand
            // 
            this.txtLTLand.CanGrow = false;
            this.txtLTLand.Height = 0.1875F;
            this.txtLTLand.Left = 1.625F;
            this.txtLTLand.MultiLine = false;
            this.txtLTLand.Name = "txtLTLand";
            this.txtLTLand.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtLTLand.Text = "Field3";
            this.txtLTLand.Top = 3.40625F;
            this.txtLTLand.Width = 1.1875F;
            // 
            // txtLTBldg
            // 
            this.txtLTBldg.CanGrow = false;
            this.txtLTBldg.Height = 0.1875F;
            this.txtLTBldg.Left = 2.9375F;
            this.txtLTBldg.MultiLine = false;
            this.txtLTBldg.Name = "txtLTBldg";
            this.txtLTBldg.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtLTBldg.Text = "Field4";
            this.txtLTBldg.Top = 3.40625F;
            this.txtLTBldg.Width = 1.5F;
            // 
            // txtLTExempt
            // 
            this.txtLTExempt.CanGrow = false;
            this.txtLTExempt.Height = 0.1875F;
            this.txtLTExempt.Left = 4.5F;
            this.txtLTExempt.MultiLine = false;
            this.txtLTExempt.Name = "txtLTExempt";
            this.txtLTExempt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtLTExempt.Text = "Field5";
            this.txtLTExempt.Top = 3.40625F;
            this.txtLTExempt.Width = 1.3125F;
            // 
            // txtLTTotal
            // 
            this.txtLTTotal.CanGrow = false;
            this.txtLTTotal.Height = 0.1875F;
            this.txtLTTotal.Left = 5.875F;
            this.txtLTTotal.MultiLine = false;
            this.txtLTTotal.Name = "txtLTTotal";
            this.txtLTTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtLTTotal.Text = "Field6";
            this.txtLTTotal.Top = 3.40625F;
            this.txtLTTotal.Width = 1.5625F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.1875F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 2.0625F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label27.Text = "Correlated Amounts By Bldg Code";
            this.Label27.Top = 4.125F;
            this.Label27.Width = 3.5625F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.1875F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 0.0625F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label28.Text = "Bldg Code";
            this.Label28.Top = 4.3125F;
            this.Label28.Width = 0.875F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.375F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 1.125F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label29.Text = "Card Count";
            this.Label29.Top = 4.3125F;
            this.Label29.Width = 0.5000001F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.21875F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 1.6875F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label30.Text = "Land";
            this.Label30.Top = 4.3125F;
            this.Label30.Width = 1.125F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.1875F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 3.25F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label31.Text = "Buildings";
            this.Label31.Top = 4.3125F;
            this.Label31.Width = 1.125F;
            // 
            // Label32
            // 
            this.Label32.Height = 0.1875F;
            this.Label32.HyperLink = null;
            this.Label32.Left = 4.625F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label32.Text = "Exemption";
            this.Label32.Top = 4.3125F;
            this.Label32.Width = 1.125F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.1875F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 6.4375F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label33.Text = "Total";
            this.Label33.Top = 4.3125F;
            this.Label33.Width = 1F;
            // 
            // SubReport4
            // 
            this.SubReport4.CloseBorder = false;
            this.SubReport4.Height = 0.1875F;
            this.SubReport4.Left = 0F;
            this.SubReport4.Name = "SubReport4";
            this.SubReport4.Report = null;
            this.SubReport4.Top = 4.6875F;
            this.SubReport4.Width = 7.5F;
            // 
            // Label34
            // 
            this.Label34.Height = 0.1875F;
            this.Label34.HyperLink = null;
            this.Label34.Left = 0.0625F;
            this.Label34.Name = "Label34";
            this.Label34.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label34.Text = "Total";
            this.Label34.Top = 4.90625F;
            this.Label34.Width = 0.875F;
            // 
            // txtCBCount
            // 
            this.txtCBCount.CanGrow = false;
            this.txtCBCount.Height = 0.1875F;
            this.txtCBCount.Left = 1F;
            this.txtCBCount.MultiLine = false;
            this.txtCBCount.Name = "txtCBCount";
            this.txtCBCount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.txtCBCount.Text = "Field2";
            this.txtCBCount.Top = 4.90625F;
            this.txtCBCount.Width = 0.5625F;
            // 
            // txtCBLand
            // 
            this.txtCBLand.CanGrow = false;
            this.txtCBLand.Height = 0.1875F;
            this.txtCBLand.Left = 1.625F;
            this.txtCBLand.MultiLine = false;
            this.txtCBLand.Name = "txtCBLand";
            this.txtCBLand.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.txtCBLand.Text = "Field3";
            this.txtCBLand.Top = 4.90625F;
            this.txtCBLand.Width = 1.1875F;
            // 
            // txtCBBldg
            // 
            this.txtCBBldg.CanGrow = false;
            this.txtCBBldg.Height = 0.1875F;
            this.txtCBBldg.Left = 2.875F;
            this.txtCBBldg.MultiLine = false;
            this.txtCBBldg.Name = "txtCBBldg";
            this.txtCBBldg.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.txtCBBldg.Text = "Field4";
            this.txtCBBldg.Top = 4.90625F;
            this.txtCBBldg.Width = 1.5F;
            // 
            // txtCBExempt
            // 
            this.txtCBExempt.CanGrow = false;
            this.txtCBExempt.Height = 0.1875F;
            this.txtCBExempt.Left = 4.4375F;
            this.txtCBExempt.MultiLine = false;
            this.txtCBExempt.Name = "txtCBExempt";
            this.txtCBExempt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.txtCBExempt.Text = "Field5";
            this.txtCBExempt.Top = 4.90625F;
            this.txtCBExempt.Width = 1.375F;
            // 
            // txtCBTotal
            // 
            this.txtCBTotal.CanGrow = false;
            this.txtCBTotal.Height = 0.1875F;
            this.txtCBTotal.Left = 5.875F;
            this.txtCBTotal.MultiLine = false;
            this.txtCBTotal.Name = "txtCBTotal";
            this.txtCBTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.txtCBTotal.Text = "Field6";
            this.txtCBTotal.Top = 4.90625F;
            this.txtCBTotal.Width = 1.5625F;
            // 
            // Label35
            // 
            this.Label35.Height = 0.1875F;
            this.Label35.HyperLink = null;
            this.Label35.Left = 2.0625F;
            this.Label35.Name = "Label35";
            this.Label35.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label35.Text = "Correlated Amounts By Land Code";
            this.Label35.Top = 5.5625F;
            this.Label35.Width = 3.5625F;
            // 
            // SubReport5
            // 
            this.SubReport5.CloseBorder = false;
            this.SubReport5.Height = 0.1875F;
            this.SubReport5.Left = 0F;
            this.SubReport5.Name = "SubReport5";
            this.SubReport5.Report = null;
            this.SubReport5.Top = 6.125F;
            this.SubReport5.Width = 7.5F;
            // 
            // Label36
            // 
            this.Label36.Height = 0.1875F;
            this.Label36.HyperLink = null;
            this.Label36.Left = 0F;
            this.Label36.Name = "Label36";
            this.Label36.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label36.Text = "Land Code";
            this.Label36.Top = 5.78125F;
            this.Label36.Width = 0.875F;
            // 
            // Label37
            // 
            this.Label37.Height = 0.34375F;
            this.Label37.HyperLink = null;
            this.Label37.Left = 1.125F;
            this.Label37.Name = "Label37";
            this.Label37.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label37.Text = "Card Count";
            this.Label37.Top = 5.78125F;
            this.Label37.Width = 0.4375F;
            // 
            // Label38
            // 
            this.Label38.Height = 0.21875F;
            this.Label38.HyperLink = null;
            this.Label38.Left = 1.8125F;
            this.Label38.Name = "Label38";
            this.Label38.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label38.Text = "Land";
            this.Label38.Top = 5.78125F;
            this.Label38.Width = 1.125F;
            // 
            // Label39
            // 
            this.Label39.Height = 0.1875F;
            this.Label39.HyperLink = null;
            this.Label39.Left = 3.3125F;
            this.Label39.Name = "Label39";
            this.Label39.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label39.Text = "Buildings";
            this.Label39.Top = 5.78125F;
            this.Label39.Width = 1.125F;
            // 
            // Label40
            // 
            this.Label40.Height = 0.1875F;
            this.Label40.HyperLink = null;
            this.Label40.Left = 4.75F;
            this.Label40.Name = "Label40";
            this.Label40.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label40.Text = "Exemption";
            this.Label40.Top = 5.78125F;
            this.Label40.Width = 1.125F;
            // 
            // Label41
            // 
            this.Label41.Height = 0.1875F;
            this.Label41.HyperLink = null;
            this.Label41.Left = 6.4375F;
            this.Label41.Name = "Label41";
            this.Label41.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label41.Text = "Total";
            this.Label41.Top = 5.78125F;
            this.Label41.Width = 1F;
            // 
            // Label42
            // 
            this.Label42.Height = 0.1875F;
            this.Label42.HyperLink = null;
            this.Label42.Left = 0.0625F;
            this.Label42.Name = "Label42";
            this.Label42.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label42.Text = "Total";
            this.Label42.Top = 6.34375F;
            this.Label42.Width = 0.875F;
            // 
            // txtCLCount
            // 
            this.txtCLCount.CanGrow = false;
            this.txtCLCount.Height = 0.1875F;
            this.txtCLCount.Left = 1F;
            this.txtCLCount.MultiLine = false;
            this.txtCLCount.Name = "txtCLCount";
            this.txtCLCount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtCLCount.Text = "Field2";
            this.txtCLCount.Top = 6.34375F;
            this.txtCLCount.Width = 0.5625F;
            // 
            // txtCLLand
            // 
            this.txtCLLand.CanGrow = false;
            this.txtCLLand.Height = 0.21875F;
            this.txtCLLand.Left = 1.75F;
            this.txtCLLand.MultiLine = false;
            this.txtCLLand.Name = "txtCLLand";
            this.txtCLLand.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtCLLand.Text = "Field3";
            this.txtCLLand.Top = 6.34375F;
            this.txtCLLand.Width = 1.1875F;
            // 
            // txtCLBldg
            // 
            this.txtCLBldg.CanGrow = false;
            this.txtCLBldg.Height = 0.21875F;
            this.txtCLBldg.Left = 3F;
            this.txtCLBldg.MultiLine = false;
            this.txtCLBldg.Name = "txtCLBldg";
            this.txtCLBldg.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtCLBldg.Text = "Field4";
            this.txtCLBldg.Top = 6.34375F;
            this.txtCLBldg.Width = 1.4375F;
            // 
            // txtCLExempt
            // 
            this.txtCLExempt.CanGrow = false;
            this.txtCLExempt.Height = 0.1875F;
            this.txtCLExempt.Left = 4.5F;
            this.txtCLExempt.MultiLine = false;
            this.txtCLExempt.Name = "txtCLExempt";
            this.txtCLExempt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtCLExempt.Text = "Field5";
            this.txtCLExempt.Top = 6.34375F;
            this.txtCLExempt.Width = 1.375F;
            // 
            // txtCLTotal
            // 
            this.txtCLTotal.CanGrow = false;
            this.txtCLTotal.Height = 0.21875F;
            this.txtCLTotal.Left = 5.9375F;
            this.txtCLTotal.MultiLine = false;
            this.txtCLTotal.Name = "txtCLTotal";
            this.txtCLTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtCLTotal.Text = "Field6";
            this.txtCLTotal.Top = 6.34375F;
            this.txtCLTotal.Width = 1.5F;
            // 
            // Label43
            // 
            this.Label43.Height = 0.21875F;
            this.Label43.HyperLink = null;
            this.Label43.Left = 2.0625F;
            this.Label43.Name = "Label43";
            this.Label43.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label43.Text = "Correlated Amounts By Tran Code";
            this.Label43.Top = 6.96875F;
            this.Label43.Width = 3.5625F;
            // 
            // Label44
            // 
            this.Label44.Height = 0.21875F;
            this.Label44.HyperLink = null;
            this.Label44.Left = 0F;
            this.Label44.Name = "Label44";
            this.Label44.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label44.Text = "Tran Code";
            this.Label44.Top = 7.15625F;
            this.Label44.Width = 0.875F;
            // 
            // Label45
            // 
            this.Label45.Height = 0.344F;
            this.Label45.HyperLink = null;
            this.Label45.Left = 1.125F;
            this.Label45.Name = "Label45";
            this.Label45.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label45.Text = "Card Count";
            this.Label45.Top = 7.15625F;
            this.Label45.Width = 0.5000001F;
            // 
            // Label46
            // 
            this.Label46.Height = 0.1875F;
            this.Label46.HyperLink = null;
            this.Label46.Left = 1.6875F;
            this.Label46.Name = "Label46";
            this.Label46.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label46.Text = "Land";
            this.Label46.Top = 7.15625F;
            this.Label46.Width = 1.125F;
            // 
            // Label47
            // 
            this.Label47.Height = 0.1875F;
            this.Label47.HyperLink = null;
            this.Label47.Left = 3.3125F;
            this.Label47.Name = "Label47";
            this.Label47.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label47.Text = "Buildings";
            this.Label47.Top = 7.15625F;
            this.Label47.Width = 1.125F;
            // 
            // Label48
            // 
            this.Label48.Height = 0.1875F;
            this.Label48.HyperLink = null;
            this.Label48.Left = 4.6875F;
            this.Label48.Name = "Label48";
            this.Label48.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label48.Text = "Exemption";
            this.Label48.Top = 7.15625F;
            this.Label48.Width = 1.125F;
            // 
            // Label49
            // 
            this.Label49.Height = 0.21875F;
            this.Label49.HyperLink = null;
            this.Label49.Left = 6.4375F;
            this.Label49.Name = "Label49";
            this.Label49.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label49.Text = "Total";
            this.Label49.Top = 7.15625F;
            this.Label49.Width = 1F;
            // 
            // SubReport6
            // 
            this.SubReport6.CloseBorder = false;
            this.SubReport6.Height = 0.1875F;
            this.SubReport6.Left = 0F;
            this.SubReport6.Name = "SubReport6";
            this.SubReport6.Report = null;
            this.SubReport6.Top = 7.5F;
            this.SubReport6.Width = 7.5F;
            // 
            // Label50
            // 
            this.Label50.Height = 0.21875F;
            this.Label50.HyperLink = null;
            this.Label50.Left = 0.0625F;
            this.Label50.Name = "Label50";
            this.Label50.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label50.Text = "Total";
            this.Label50.Top = 7.6875F;
            this.Label50.Width = 0.875F;
            // 
            // txtCTCount
            // 
            this.txtCTCount.CanGrow = false;
            this.txtCTCount.Height = 0.1875F;
            this.txtCTCount.Left = 1F;
            this.txtCTCount.MultiLine = false;
            this.txtCTCount.Name = "txtCTCount";
            this.txtCTCount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtCTCount.Text = "Field2";
            this.txtCTCount.Top = 7.71875F;
            this.txtCTCount.Width = 0.5625F;
            // 
            // txtCTLand
            // 
            this.txtCTLand.CanGrow = false;
            this.txtCTLand.Height = 0.1875F;
            this.txtCTLand.Left = 1.625F;
            this.txtCTLand.MultiLine = false;
            this.txtCTLand.Name = "txtCTLand";
            this.txtCTLand.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtCTLand.Text = "Field3";
            this.txtCTLand.Top = 7.71875F;
            this.txtCTLand.Width = 1.1875F;
            // 
            // txtCTBldg
            // 
            this.txtCTBldg.Height = 0.1875F;
            this.txtCTBldg.Left = 2.9375F;
            this.txtCTBldg.Name = "txtCTBldg";
            this.txtCTBldg.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtCTBldg.Text = "Field4";
            this.txtCTBldg.Top = 7.71875F;
            this.txtCTBldg.Width = 1.5F;
            // 
            // txtCTExempt
            // 
            this.txtCTExempt.Height = 0.1875F;
            this.txtCTExempt.Left = 4.5F;
            this.txtCTExempt.Name = "txtCTExempt";
            this.txtCTExempt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtCTExempt.Text = "Field5";
            this.txtCTExempt.Top = 7.71875F;
            this.txtCTExempt.Width = 1.3125F;
            // 
            // txtCTTotal
            // 
            this.txtCTTotal.CanGrow = false;
            this.txtCTTotal.Height = 0.1875F;
            this.txtCTTotal.Left = 5.875F;
            this.txtCTTotal.MultiLine = false;
            this.txtCTTotal.Name = "txtCTTotal";
            this.txtCTTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.txtCTTotal.Text = "Field6";
            this.txtCTTotal.Top = 7.71875F;
            this.txtCTTotal.Width = 1.5625F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtMuni,
            this.txtTime,
            this.txtDate,
            this.txtPage});
            this.PageHeader.Height = 0.5F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Label1
            // 
            this.Label1.Height = 0.21875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1.875F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.Label1.Text = "Assessment Summary By Codes";
            this.Label1.Top = 0.03125F;
            this.Label1.Width = 3.6875F;
            // 
            // txtMuni
            // 
            this.txtMuni.CanGrow = false;
            this.txtMuni.Height = 0.19F;
            this.txtMuni.Left = 0F;
            this.txtMuni.MultiLine = false;
            this.txtMuni.Name = "txtMuni";
            this.txtMuni.Style = "font-family: \'Tahoma\'";
            this.txtMuni.Text = "Field2";
            this.txtMuni.Top = 0.03125F;
            this.txtMuni.Width = 1.75F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.19F;
            this.txtTime.Left = 0F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Tahoma\'";
            this.txtTime.Text = "Field2";
            this.txtTime.Top = 0.1875F;
            this.txtTime.Width = 1.375F;
            // 
            // txtDate
            // 
            this.txtDate.CanGrow = false;
            this.txtDate.Height = 0.19F;
            this.txtDate.Left = 6.0625F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtDate.Text = "Field2";
            this.txtDate.Top = 0.03125F;
            this.txtDate.Width = 1.375F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1875F;
            this.txtPage.Left = 6.0625F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.1875F;
            this.txtPage.Width = 1.375F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptAssessCodes
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.Script = "\r\nSub ActiveReport_ReportStart\r\n\r\nEnd Sub\r\n";
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLBCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLBLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLBBldg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLBExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLBTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLBldg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLLTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTBldg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLTTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCBCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCBLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCBBldg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCBExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCBTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCLCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCLLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCLBldg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCLExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCLTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTBldg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCTTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLBCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLBLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLBBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLBExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLBTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLLCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLLLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLLBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLLExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLLTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLTCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLTLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLTBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLTExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLTTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCBCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCBLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCBBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCBExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCBTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCLCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCLLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCLBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCLExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCLTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label48;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCTCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCTLand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCTBldg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCTExempt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCTTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
