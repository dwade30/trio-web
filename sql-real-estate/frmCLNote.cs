﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWRE0000
{
	/// <summary>
	/// Summary description for frmCLNote.
	/// </summary>
	public partial class frmCLNote : BaseForm
	{
		public frmCLNote()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCLNote InstancePtr
		{
			get
			{
				return (frmCLNote)Sys.GetInstance(typeof(frmCLNote));
			}
		}

		protected frmCLNote _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int lngAcct;

		public void Init(ref int lngAccount)
		{
			lngAcct = lngAccount;
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void LoadNote()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from comments where account = " + FCConvert.ToString(lngAcct) + " and type = 'RE'", modGlobalVariables.strCLDatabase);
			if (!rsLoad.EndOfFile())
			{
				txtNote.Text = FCConvert.ToString(rsLoad.Get_Fields_String("comment"));
				if (Conversion.Val(rsLoad.Get_Fields_Int32("priority")) == 1)
				{
					chkPopUp.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkPopUp.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ShowInRE")))
				{
					chkInRE.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkInRE.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				txtNote.Text = "";
				chkPopUp.CheckState = Wisej.Web.CheckState.Unchecked;
				chkInRE.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void frmCLNote_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCLNote_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCLNote properties;
			//frmCLNote.FillStyle	= 0;
			//frmCLNote.ScaleWidth	= 3885;
			//frmCLNote.ScaleHeight	= 2265;
			//frmCLNote.LinkTopic	= "Form2";
			//frmCLNote.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			LoadNote();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveNote())
			{
				this.Unload();
			}
		}

		private bool SaveNote()
		{
			bool SaveNote = false;
			try
			{
				// On Error GoTo ErrorMessage
				SaveNote = false;
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from comments where type = 'RE' and account = " + FCConvert.ToString(lngAcct), modGlobalVariables.strCLDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("account", lngAcct);
					rsSave.Set_Fields("type", "RE");
				}
				rsSave.Set_Fields("comment", txtNote.Text);
				if (chkInRE.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsSave.Set_Fields("ShowInRE", true);
				}
				else
				{
					rsSave.Set_Fields("ShowInRE", false);
				}
				if (chkPopUp.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsSave.Set_Fields("Priority", 1);
				}
				else
				{
					rsSave.Set_Fields("priority", 0);
				}
				rsSave.Update();
				SaveNote = true;
				return SaveNote;
			}
			catch
			{
				// ErrorMessage:
			}
			return SaveNote;
		}
	}
}
