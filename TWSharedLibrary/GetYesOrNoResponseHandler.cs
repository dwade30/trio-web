﻿using fecherFoundation;
using SharedApplication;
using SharedApplication.Messaging;
using Wisej.Web;

namespace TWSharedLibrary
{
    public class GetYesOrNoResponseHandler : CommandHandler<GetYesOrNoResponse,bool>
    {
        public GetYesOrNoResponseHandler()
        {

        }
        protected override bool Handle(GetYesOrNoResponse command)
        {
            if (command.ShowWarningIcon)
            {
                return FCMessageBox.Show(command.Message, MsgBoxStyle.YesNo | MsgBoxStyle.Exclamation,command.Title) == DialogResult.Yes;
            }

            return FCMessageBox.Show(command.Message, MsgBoxStyle.YesNo | MsgBoxStyle.Question, command.Title) ==
                   DialogResult.Yes;
        }
    }
}