﻿using SharedApplication;
using SharedApplication.Messaging;

namespace TWSharedLibrary
{
    public class ChooseEffectiveDateHandler : CommandHandler<ChooseEffectiveDate,EffectiveDateChoice>
    {
        private IModalView<IEffectiveDateChangeViewModel> view;

        public ChooseEffectiveDateHandler(IModalView<IEffectiveDateChangeViewModel> view)
        {
            this.view = view;
        }
        protected override EffectiveDateChoice Handle(ChooseEffectiveDate command)
        {
            view.ViewModel.AllowChangingRecordedDate = command.AllowChangingRecordedDate;
            view.ViewModel.EffectiveDate = command.DefaultDate;
            view.ViewModel.UpdateRecordedDate = command.UpdateRecordedDate;
            view.ShowModal();
            return new EffectiveDateChoice()
            {
                EffectiveDate = view.ViewModel.EffectiveDate,
                Cancelled = view.ViewModel.Cancelled,
                UpdateRecordedDate = view.ViewModel.UpdateRecordedDate
            };
        }
    }
}