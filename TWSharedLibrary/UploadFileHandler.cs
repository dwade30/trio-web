﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fecherFoundation;
using SharedApplication;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace TWSharedLibrary
{

	public class UploadFileHandler : CommandHandler<UploadFile, string>
	{
		protected override string Handle(UploadFile command)
		{
			try
			{
				var dialog = new FCCommonDialog();
				dialog.DialogTitle = command.DialogTitle;
				dialog.CancelError = true;
				dialog.FileName = "";
				dialog.Filter = "*.*";

				if (dialog.ShowOpen())
				{
					return dialog.FileName;
				}
				else
				{
					return "";
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return "";
			}
		}
	}
}
