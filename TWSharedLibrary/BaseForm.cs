﻿using System;
using System.Drawing;
using System.Linq;
using fecherFoundation;
using SharedApplication.Messaging;
using SharedApplication.Telemetry;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace TWSharedLibrary
{
	public partial class SharedBaseForm : fecherFoundation.FCForm
    {
        public ITelemetryService Telemetry { get; private set; }
        private bool disableFormClosingDuringWait = false;
        private bool showProgressBar = false;
        private bool enableCancel = false;
        private int height = 0;
        public event EventHandler Cancelled;
        public SharedBaseForm()
        {
            InitializeComponent();
            this.btnCancel.Click += BtnCancel_Click;
            this.TopPanel.ControlAdded += TopPanel_ControlAdded;
            if (Telemetry == null) Telemetry = StaticSettings.GlobalTelemetryService;
            Telemetry?.TrackHit(this.ToString().Substring(0, this.ToString().IndexOf(",")));
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            EndWait();

            if (Cancelled != null)
            {
                Cancelled(this, new EventArgs());
            }
        }

        private void ClientArea_Layout(object sender, LayoutEventArgs e)
        {
            LayoutBottomcontrol();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            LayoutBottomcontrol();
        }

        private void LayoutBottomcontrol()
        {
            if (this.BottomPanel != null && this.ClientArea != null)
            {
                this.BottomPanel.Width = this.ClientArea.Width - 20;
                var bottomControl = this.ClientArea.Controls.Where(c => c != this.BottomPanel && c.Visible).OrderByDescending(c => (c.Top + c.Height)).FirstOrDefault();
                if (bottomControl != null)
                {
                    this.BottomPanel.Top = bottomControl.Top + bottomControl.Height;
                }
            }
        }

        public SharedBaseForm(ITelemetryService telemetryService)
        {
            Telemetry = telemetryService;
            InitializeComponent();
            this.TopPanel.ControlAdded += TopPanel_ControlAdded;
            Telemetry?.TrackHit(this.ToString().Substring(0, this.ToString().IndexOf(",")));
        }
        public void LockCloseDuringLongProcess()
        {
            disableFormClosingDuringWait = true;
        }

        public void UnlockCloseDuringLongProcess()
        {
            disableFormClosingDuringWait = false;
        }

        public void ShowWait(bool disableformClosing = true, bool showprogressBar = false,string message = "Waiting ...", bool enableCancel = false)
        {
            this.waitPanel.Visible = true;
            this.waitLabel.Text = "Waiting ...";
            waitLabel.Text = message;
            this.enableCancel = enableCancel;
            this.ClientArea.Visible = false;
            btnCancel.Visible = this.enableCancel;
            //FC:FINAL:AM:#3169 - can't disable the TopPanel because the hidden buttons on it gets visible after enabling it back
            //this.TopPanel.Enabled = false;
            this.height = this.TopPanel.Height;
            this.TopPanel.Height = 0;
            this.BottomPanel.Enabled = false;
            this.disableFormClosingDuringWait = disableformClosing;
            this.panel1.Size = !showprogressBar ? new System.Drawing.Size(312, 77) : new System.Drawing.Size(362, 121);
            showProgressBar = showprogressBar;
            this.progressBar1.Visible = showprogressBar;
        }

        public void UpdateWait(string text = "", int progressBarValue = -1, int progressBarMaximum = -1)
        {
            //update progress label text
            if (text != "")
            {
                this.waitLabel.Text = text;
                FCUtils.ApplicationUpdate(this.waitLabel);
            }

            //update panel width based on label/progressbar width
            int labelWidth = this.waitLabel.Left + this.waitLabel.Width + 10;
            int width = labelWidth;
            if (showProgressBar)
            {
                int progressBarWidth = this.progressBar1.Left + this.progressBar1.Width + 10;
                width = labelWidth > progressBarWidth ? labelWidth : progressBarWidth;
            }
            if (width > this.panel1.Width)
            {
                this.panel1.Width = width;
            }

            //update progress bar value
            if (showProgressBar)
            {
                if (progressBarMaximum != -1)
                {
                    this.progressBar1.Maximum = progressBarMaximum;
                    FCUtils.ApplicationUpdate(this.progressBar1);
                }
                if (progressBarValue != -1)
                {
                    this.progressBar1.Value = progressBarValue;
                    FCUtils.ApplicationUpdate(this.progressBar1);
                }
            }
        }

        public void EndWait()
        {
            this.waitPanel.Visible = false;
            this.ClientArea.Visible = true;
            //this.TopPanel.Enabled = true;
            this.TopPanel.Height = height;
            this.BottomPanel.Enabled = true;
            FCUtils.ApplicationUpdate(this);
            this.disableFormClosingDuringWait = false;
            fecherFoundation.FCUtils.UnlockUserInterface();
        }

        private void TopPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            if (e.Control is Button)
            {
                //FC:FINAL:AM: hide buttons when they are disabled
                e.Control.VisibleChanged += Control_VisibleChanged;
                e.Control.EnabledChanged += Control_EnabledChanged;
                e.Control.SizeChanged += Control_SizeChanged;
                GenerateLayout();
            }
        }

        private void Control_SizeChanged(object sender, EventArgs e)
        {
            GenerateLayout();
        }

        private void Control_EnabledChanged(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                if (!button.IsDisposed)
                {
                    button.UserData.FromEnabledChanged = true;
                    button.Visible = button.Enabled && (button.UserData.Visible == null || button.UserData.Visible);
                    button.UserData.FromEnabledChanged = false;
                }
            }

        }

        private void Control_VisibleChanged(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                if (button.UserData.FromEnabledChanged == null || !button.UserData.FromEnabledChanged)
                {
                    button.UserData.Visible = button.Visible;
                }
                GenerateLayout();
            }
        }

        private void GenerateLayout()
        {
            if (DesignMode)
            {
                return;
            }
            int rightPosition = this.TopPanel.Width - 30;
            for (int i = this.TopPanel.Controls.Count - 1; i >= 0; i--)
            {
                Control control = this.TopPanel.Controls[i];
                if (control is Button && control.Visible && control.Enabled)
                {
                    control.Location = new Point(rightPosition - control.Width, 29);
                    rightPosition -= control.Width + 5;
                }
            }
        }

        private void BaseForm_Load(object sender, System.EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.HeaderText.Font = new System.Drawing.Font("@header", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            }

            this.CenterBottomControls();
            this.waitPanel.Size = this.ClientArea.Size;
            this.panel1.Location = new Point(this.waitPanel.Width / 2, this.waitPanel.Height / 2) - new Size(this.panel1.Width / 2, this.panel1.Height / 2);
            this.waitPanel.Dock = DockStyle.Fill;
        }

        private void BottomPanel_SizeChanged(object sender, EventArgs e)
        {
            this.CenterBottomControls();
        }

        private void BottomPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            this.CenterBottomControls();
        }

        private void BottomPanel_ControlRemoved(object sender, ControlEventArgs e)
        {
            this.CenterBottomControls();
        }

        public void CenterBottomControls()
        {
            if (this.DesignMode || this.BottomPanel.Controls.Count == 0)
            {
                return;
            }
            int width = 0;
            foreach (Control control in this.BottomPanel.Controls)
            {
                width += control.Width;
            }
            int boundsWidth = width + (this.BottomPanel.Controls.Count - 1) * 30;
            int leftDistance = (this.BottomPanel.Width - boundsWidth) / 2;
            //center controls
            int leftAdd = 0;
            foreach (Control control in this.BottomPanel.Controls)
            {
                control.Left = leftDistance + leftAdd;
                leftAdd += control.Width + 30;
            }
        }

        private void BaseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!fecherFoundation.App.MainForm.ExitingApplication)
            {
                e.Cancel = this.disableFormClosingDuringWait;
            }
        }
    }
}
