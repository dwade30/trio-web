﻿using System;
using fecherFoundation;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.LegacyCommands;
using SharedApplication.Extensions;
using TWSharedLibrary.Data;
using Wisej.Web;

namespace TWSharedLibrary
{
    public class NewAccountBox
    {
        public const string DefaultAccountType = "E";
        public const string DefaultInfochoolAccountType = "P";
        // VBto upgrade warning: Grid As object	OnWrite(FCGrid.VSFlexGrid, string)
        public static void CheckAccountKeyPress(FCGrid Grid, int Row, int Col, int KeyAscii, int SelectStart, string EditText)
        {
            // On Error GoTo ERROR_HANDLER
            int intStart/*unused?*/;
            string str = "";
            int intOriginalStart;
            if (Grid.EditSelStart == Statics.intMaxLength)
            {
                KeyAscii = 0;
                return;
            }
            // make sure a letter is highlighted
            if (Grid.EditSelLength < 1)
            {
                Grid.EditSelLength = 1;
            }
            if (KeyAscii == 27)
            {
                return;
            }
            else if (KeyAscii == 8)
            {
                // the backspace ID was hit
                KeyAscii = 0;
                if (SelectStart == EditText.Length)
                {
                    SelectStart -= 1;
                }
                // save where the cursor was
                intOriginalStart = SelectStart;
                if (SelectStart >= 2)
                {
                    // if not the first two spaces then move to previous character
                    SelectStart -= 1;
                    if (Strings.Left(EditText, 1) != "A" && Strings.Left(EditText, 1) != "M")
                    {
                        if (Strings.Mid(EditText, SelectStart + 1, 1) == "-" || Strings.Mid(EditText, SelectStart + 1, 1) == " ")
                        {
                            // if the previous char is "-" then move previous again
                            SelectStart -= 1;
                        }
                    }
                    // replace the character with the underscore
                    Strings.MidSet(ref EditText, intOriginalStart + 1, 1, "_");
                    Grid.EditText = EditText;
                    Grid.EditSelStart = SelectStart;
                    Grid.EditSelLength = 1;
                }
            }
            else
            {
                // if we are not on the letter part of the account
                if (SelectStart > 0)
                {
                    // if we are using an E, R, or G account then only allow numbers
                    if (Strings.Left(EditText, 1) != "M" && Strings.Left(EditText, 1) != "A")
                    {
                        if (KeyAscii < 48 || KeyAscii > 57)
                        {
                            KeyAscii = 0;
                        }
                        if (SelectStart != EditText.Length)
                        {
                            if (Strings.Mid(EditText, SelectStart + 1, 1) != "-" && Strings.Mid(EditText, SelectStart + 1, 1) != " ")
                            {
                                Grid.EditSelLength = 1;
                            }
                        }
                    }
                    else
                    {
                        // else allow anything and capitolize letters
                        if (KeyAscii >= 97 && KeyAscii <= 122)
                        {
                            // if the keypress is lower case the uppercase it
                            KeyAscii -= 32;
                        }
                    }
                }
                else
                {
                    // if we are on the letter part then capitolize any letters typed
                    if (KeyAscii >= 97 && KeyAscii <= 122)
                    {
                        // if the keypress is lower case the uppercase it
                        KeyAscii -= 32;
                    }
                    GetMaxLength(FCConvert.ToString(Convert.ToChar(KeyAscii)), ref Grid, Row, Col);
                    Grid.EditMaxLength = Statics.intMaxLength;
                    Grid.EditSelStart = 2;
                    Grid.EditSelLength = 1;
                    KeyAscii = 0;
                    return;
                }
                if (SelectStart != EditText.Length)
                {
                    if (Strings.Mid(EditText, SelectStart + 1, 1) != "-" && Strings.Mid(EditText, SelectStart + 1, 1) != " ")
                    {
                        Grid.EditSelLength = 1;
                    }
                }
            }
            return;
            ERROR_HANDLER:
            ;
            FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", MsgBoxStyle.Critical, "Account ID Press Error");
        }

        public static void GetMaxLength(string strData, ref FCGrid Grid, int Row, int Col, string strStartType = "")
        {
            string temp = "";
            int counter;
            int length/*unused?*/;
            string holder = "";
            string x = "";
            if (strStartType == "")
            {
                strStartType = DefaultAccountType;
            }
            if (Strings.Trim(strData) == "")
            {
                strData = strStartType;
            }
            if (strData.Length > 0)
            {
                if (Strings.Left(strData, 1) == "E")
                {
                    x = Statics.ESegmentSize;
                    temp = "E";
                }
                else if (Strings.Left(strData, 1) == "R")
                {
                    x = Statics.RSegmentSize;
                    temp = "R";
                }
                else if (Strings.Left(strData, 1) == "G")
                {
                    x = Statics.GSegmentSize;
                    temp = "G";
                }
                else
                {
                    x = "230000";
                    temp = "M";
                }
                // create the blank format
                holder = string.Empty;
                for (counter = 2; counter <= x.Length; counter += 2)
                {
                    if (Conversion.Val(Strings.Mid(x, counter - 1, 2)) > 0)
                    {
                        holder += Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(x, counter - 1, 2))), "_") + "-";
                    }
                }
                // remove the trailing "-"
                holder = Strings.Mid(holder, 1, holder.Length - 1);
                // show blank format in box
                if (temp == "E")
                    Statics.e_Format = holder;
                if (temp == "G")
                    Statics.g_Format = holder;
                if (temp == "R")
                    Statics.r_Format = holder;
                temp += " " + holder;
                Statics.intMaxLength = Strings.Trim(temp).Length;
                // Dave 10/25/05
                // If Len(strData) > 1 Then
                Grid.TextMatrix(Row, Col, temp);
                Grid.EditText = temp;
                // End If
            }
        }
        // sets up the format for the account number
        public static string AccountFormatSetup_2(string x)
        {
            return AccountFormatSetup(ref x);
        }

        public static string AccountFormatSetup(ref string x)
        {
            string AccountFormatSetup = "";
            string temp = "";
            int counter;
            int length/*unused?*/;
            string holder;
            // if no formats are setup then give an error message
            if (Strings.Trim(x) == "")
            {
                FCMessageBox.Show("One or more of your account formats needs to be setup before you may continue", MsgBoxStyle.Information, "Invalid Account Format");
                return AccountFormatSetup;
            }
            // create the blank format
            holder = string.Empty;
            for (counter = 2; counter <= x.Length; counter += 2)
            {
                if (Conversion.Val(Strings.Mid(x, counter - 1, 2)) > 0)
                {
                    holder += Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(x, counter - 1, 2))), "_") + "-";
                }
            }
            // remove the trailing "-"
            holder = Strings.Mid(holder, 1, holder.Length - 1);
            // show blank format in box
            if (Statics.strAccount == "E")
                Statics.e_Format = holder;
            if (Statics.strAccount == "G")
                Statics.g_Format = holder;
            if (Statics.strAccount == "R")
                Statics.r_Format = holder;
            if (Statics.strAccount == "P")
                Statics.p_Format = holder;
            if (Statics.strAccount == "L")
                Statics.l_Format = holder;
            if (Statics.strAccount == "V")
                Statics.v_Format = holder;
            Statics.strAccount += " " + holder;
            Statics.intMaxLength = Strings.Trim(Statics.strAccount).Length;
            AccountFormatSetup = Statics.strAccount;
            return AccountFormatSetup;
        }

        public static void GetFormats()
        {
            clsDRWrapper rsFormat = new clsDRWrapper();
            clsDRWrapper rsBudgetary = new clsDRWrapper();
            // check to see if E, R, G, and A formats are allowed
            rsBudgetary.OpenRecordset("SELECT * FROM Modules", "SystemSettings");
            if (rsBudgetary.EndOfFile() != true && rsBudgetary.BeginningOfFile() != true)
            {
                if (FCConvert.ToBoolean(rsBudgetary.Get_Fields_Boolean("BD")))
                {
                    if (DateAndTime.DateValue(FCConvert.ToString(rsBudgetary.Get_Fields_DateTime("BDDate"))).ToOADate() >= DateTime.Today.ToOADate())
                    {
                        Statics.AllowFormats = true;
                    }
                    else
                    {
                        Statics.AllowFormats = false;
                    }
                }
                else
                {
                    Statics.AllowFormats = false;
                }
            }
            else
            {
                Statics.AllowFormats = false;
            }
            // get formats for different accont types
            if (Statics.AllowFormats)
            {
                rsFormat.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
                if (!rsFormat.EndOfFile())
                {
                    Statics.ESegmentSize = FCConvert.ToString(rsFormat.Get_Fields_String("Expenditure"));
                    Statics.RSegmentSize = FCConvert.ToString(rsFormat.Get_Fields_String("Revenue"));
                    Statics.GSegmentSize = FCConvert.ToString(rsFormat.Get_Fields_String("Ledger"));
                    Statics.PSegmentSize = "0304040402";
                    Statics.VSegmentSize = "0304";
                    Statics.LSegmentSize = "030402";
                    //modAccountTitle.Statics.Exp = Statics.ESegmentSize;
                    //modAccountTitle.Statics.Rev = Statics.RSegmentSize;
                    //modAccountTitle.Statics.Ledger = Statics.GSegmentSize;
                }
            }
            else
            {
                Statics.ESegmentSize = "02020202";
                Statics.RSegmentSize = "020203";
                Statics.GSegmentSize = "010302";
                Statics.PSegmentSize = "0304040402";
                Statics.VSegmentSize = "0304";
                Statics.LSegmentSize = "030402";
                //modAccountTitle.Statics.Exp = Statics.ESegmentSize;
                //modAccountTitle.Statics.Rev = Statics.RSegmentSize;
                //modAccountTitle.Statics.Ledger = Statics.GSegmentSize;
            }
        }
        // VBto upgrade warning: 'Return' As Variant --> As string
        public static string MMessage()
        {
            string MMessage = "";
            // else give them the M format and tell them why
            FCMessageBox.Show("The only acceptable account type is M", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "Account Code");
            Statics.intMaxLength = 23;
            MMessage = "M _____________________";
            return MMessage;
        }
        // VBto upgrade warning: Grid As object	OnWrite(FCGrid.VSFlexGrid, string)
        public static bool CheckAccountValidate(FCGrid Grid, int Row, int Col, bool Cancel, bool blnValidateMultiBreakdown = false)
        {
            bool CheckAccountValidate = false;
            // VBto upgrade warning: intValid As short --> As int	OnWrite(bool)
            int intValid;
            DialogResult intReturn = 0;
            string strEditText;
            // validate the account
            CheckAccountValidate = false;
            strEditText = Grid.EditText;
            if (Strings.Mid(Grid.EditText, 3, 1) == "_" || string.IsNullOrEmpty(Grid.EditText))
            {
                Grid.EditText = "";
                return CheckAccountValidate;
            }
            intValid = (StaticSettings.GlobalCommandDispatcher.Send(new AccountValidate(Grid.EditText,blnValidateMultiBreakdown)).Result ? -1 : 0);
            //intValid = (modValidateAccount.AccountValidate(Grid.EditText, blnValidateMultiBreakdown) ? -1 : 0);
            if (intValid == 0)
            {
                // if account is bad ask them if they want to continue or try to type in a valid account
                Grid.TextMatrix(Row, Col, string.Empty);
                //FC:FINAL:AM:#4347 - set the EditText too because showing the message box will trigger validate again
                Grid.EditText = string.Empty;
                intReturn = FCMessageBox.Show("This is a Bad Account Number.  Do you wish to continue?", MsgBoxStyle.Information | MsgBoxStyle.YesNo | MsgBoxStyle.DefaultButton2, "Invalid Account");
                if (intReturn == DialogResult.No)
                {
                    // if they dont let them continue trying to type in a valid account
                    CheckAccountValidate = true;
                    Grid.TextMatrix(Row, Col, strEditText);
                    Grid.EditCell();
                    Grid.EditSelStart = 0;
                    Grid.EditSelLength = 1;
                }
                else if (intReturn == DialogResult.Yes)
                {
                    // if they want to just move on then clear out the account box and continue
                    // if all formats are allowed then set the account box to a blank default format
                    Grid.EditText = string.Empty;
                    Grid.TextMatrix(Row, Col, string.Empty);
                }
                else
                {
                    Grid.EditText = string.Empty;
                    Grid.TextMatrix(Row, Col, string.Empty);
                }
            }
            else if (intValid == 1)
            {
                // do nothing because it has been handled in the CheckValidAccount function
            }
            else if (intValid == -1)
            {
                // do nothing because it is valid
            }
            else
            {
                CheckAccountValidate = true;
            }
            return CheckAccountValidate;
        }
        // VBto upgrade warning: Grid As object	OnWrite(FCGrid.VSFlexGrid, string)
        public static void CheckAccountKeyCode(FCGrid Grid, int Row, int Col, int KeyCode, int Shift, int SelectStart, string EditText, int SelectLength, string strStartType = "")
        {
            int intSelectStart;
            int intSelectLength = 0;
            string strSelectText = "";
            if (strStartType == "")
            {
                strStartType = DefaultAccountType;
            }
            if (Grid.EditSelStart == Statics.intMaxLength)
            {
                KeyCode = 0;
                return;
            }
            intSelectStart = SelectStart;
            Statics.strTempAccount = Strings.Trim(EditText);
            if (intSelectStart == 0 && intSelectLength == 0)
            {
                Grid.EditSelLength = 1;
                intSelectLength = 1;
            }
            if (intSelectStart == 1)
            {
                // they are entering the character code for this account
                if (Statics.boolBack || Statics.boolRight)
                {
                    // the arrow keys were hit so do not replace the format
                }
                else
                {
                    // set a new mask format
                    Statics.strAccount = Strings.UCase(Strings.Left(Statics.strTempAccount, 1));
                    if (KeyCode == 82)
                    {
                        // this is R
                        // check to see if they can use E, R, G, or A accounts
                        if (Statics.AllowFormats )
                        {
                            // if so show a blank R account box
                            Statics.strAccount = "R";
                            Grid.EditText = AccountFormatSetup(ref Statics.RSegmentSize);
                        }
                        else
                        {
                            Grid.EditText = MMessage();
                        }
                    }
                    else if (KeyCode == 69)
                    {
                        // this is E
                        // check to see if they can use E, R, G, or A accounts
                        if (Statics.AllowFormats)
                        {
                            Statics.strAccount = "E";
                            Grid.EditText = AccountFormatSetup(ref Statics.ESegmentSize);
                        }
                        else
                        {
                            Grid.EditText = MMessage();
                        }
                    }
                    else if (KeyCode == 71)
                    {
                        // This is G
                        // check to see if they can use E, R, G, or A accounts
                        if (Statics.AllowFormats )
                        {
                            Statics.strAccount = "G";
                            Grid.EditText = AccountFormatSetup(ref Statics.GSegmentSize);
                        }
                        else
                        {
                            Grid.EditText = MMessage();
                        }
                    }
                    else if (KeyCode == 80)
                    {
                        Grid.EditText = MMessage();
                    }
                    else if (KeyCode == 77)
                    {
                        // This is M
                        Statics.strAccount = "M";
                        Grid.EditText = AccountFormatSetup_2("210000");
                    }
                    else if (KeyCode == 65)
                    {
                        // This is A
                        // check to see if they can use E, R, G, or A accounts
                        if (Statics.AllowFormats)
                        {
                            Grid.EditText = "A _____________________";
                        }
                        else
                        {
                            Grid.EditText = MMessage();
                        }
                    }
                    else
                    {
                        // if no acceptable letter was typed then turn the account box back to the blank default account
                        // if E, R, G, or A accounts are accepted then change it to the default
                        if (Statics.AllowFormats)
                        {
                            FCMessageBox.Show("The only acceptable account types are A, E, R, G and M", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "Account Code");

                            Grid.EditCell();
                            Statics.strAccount = strStartType;
                            if (strStartType == "E")
                            {
                                Grid.EditText = AccountFormatSetup(ref Statics.ESegmentSize);
                            }
                            else if (strStartType == "R")
                            {
                                Grid.EditText = AccountFormatSetup(ref Statics.RSegmentSize);
                            }
                            else if (strStartType == "G")
                            {
                                Grid.EditText = AccountFormatSetup(ref Statics.GSegmentSize);
                            }
                            else
                            {
                                Grid.EditText = "M _____________________";
                            }
                        }
                        else
                        {
                            // if E, R, G, and A accounts arent accepted then change it to an M account
                            Grid.EditText = MMessage();
                        }
                    }
                    KeyCode = 0;
                    Grid.EditSelStart = 2;
                    Grid.EditSelLength = 1;
                }
            }
            else
            {
                if (KeyCode == 46)
                {
                    // DELETE ID HAS BEEN PRESSED
                    if (SelectStart >= 2)
                    {
                        // replace the character with an underscore and highlight the underscore
                        intSelectStart = SelectStart;
                        if (Statics.strTempAccount.Length > 0)
                        {
                            Statics.strTempAccount = Strings.Left(Statics.strTempAccount, intSelectStart) + "_" + Strings.Mid(Statics.strTempAccount, intSelectStart + 2, Statics.strTempAccount.Length - intSelectStart + 2);
                        }
                        Grid.EditText = Statics.strTempAccount;
                        Grid.EditSelStart = intSelectStart;
                        Grid.EditSelLength = 1;
                        KeyCode = 0;
                    }
                }
                else if (KeyCode == 37)
                {
                    // LEFT ID
                    intSelectStart = SelectStart;
                    if (intSelectStart <= 0)
                        intSelectStart = 1;
                    // if there is a - or a space to the left of us jump 2 places
                    if (Strings.Mid(Statics.strTempAccount, intSelectStart, 1) == "-")
                    {
                        Grid.EditSelStart = SelectStart - 2;
                    }
                    else if (Strings.Mid(Statics.strTempAccount, intSelectStart, 1) == " ")
                    {
                        Grid.EditSelStart = SelectStart - 2;
                    }
                    else
                    {
                        // else move 1 space to the left
                        Grid.EditSelStart = intSelectStart - 1;
                    }
                    // make sure to highlight a character
                    Grid.EditSelLength = 1;
                }
                else if (KeyCode == 39)
                {
                    // RIGHT ID
                    intSelectStart = SelectStart - 1;
                    if (intSelectStart < 0)
                        intSelectStart = 0;
                    // if there is a - or a space to the left jump 2 spaces
                    if (Strings.Mid(Statics.strTempAccount, intSelectStart + 1, 1) == "-")
                    {
                        Grid.EditSelStart = SelectStart;
                    }
                    else if (Strings.Mid(Statics.strTempAccount, intSelectStart + 1, 1) == " ")
                    {
                        Grid.EditSelStart = SelectStart;
                    }
                    else
                    {
                        // else move 1 space
                        Grid.EditSelStart = intSelectStart;
                    }
                    // highlight a character
                    Grid.EditSelLength = 1;
                }
                else
                {
                    // if a part of the account then highlight the character
                    if (Strings.Mid(Statics.strTempAccount, SelectStart + 1, 1) == "_")
                    {
                        Grid.EditSelStart = SelectStart;
                        Grid.EditSelLength = 1;
                        // if next character is a - then move past it and highlight a character
                    }
                    else if (Strings.Mid(Statics.strTempAccount, SelectStart + 1, 1) == "-")
                    {
                        Grid.EditSelStart = SelectStart + 1;
                        Grid.EditSelLength = 1;
                    }
                }
                KeyCode = 0;
            }
        }

        public static string CheckKeyDownEditF2(FCGrid Grid, int Row, int Col, Keys KeyCode, int Shift, int SelectStart, ref string EditText, int SelectLength, bool blnLimitSearch = false)
        {
            string strAcct = "";
            //if (KeyCode == Keys.F2 && Shift == 1)
            //{
                // this will show the valid accoutns form to select from
                if (Statics.AllowFormats)
                {
                    if (blnLimitSearch)
                    {
                        var acctType = "";
                        if (!string.IsNullOrWhiteSpace(EditText))
                        {
                            acctType = EditText.Left(1);
                        }
                        strAcct = StaticSettings.GlobalCommandDispatcher
                            .Send(new SelectValidAccount(EditText, acctType)).Result;
                        //strAcct = frmLoadValidAccounts.InstancePtr.Init(EditText, Strings.Left(EditText, 1));
                    }
                    else
                    {
                        strAcct = StaticSettings.GlobalCommandDispatcher
                            .Send(new SelectValidAccount(EditText, "")).Result;
                        //strAcct = frmLoadValidAccounts.InstancePtr.Init(EditText);
                    }
                    //if (strAcct != "")
                    //{
                    //    EditText = strAcct;
                    //    Grid.EditText = strAcct;
                    //    Grid.TextMatrix(Row, Col, strAcct);
                    //}

                   
                }
                return strAcct;
            //}
        }
        // VBto upgrade warning: Grid As object	OnWrite(FCGrid.VSFlexGrid, string)
        //public static void CheckKeyDownEdit(FCGrid Grid, int Row, int Col, Keys KeyCode, int Shift, int SelectStart, ref string EditText, int SelectLength, bool blnLimitSearch = false)
        //{
        //    int intStart/*unused?*/;
        //    string strAcct = "";
        //    // these are used in the keyup event to allow the arrow keys to work
        //    Statics.boolBack = false;
        //    Statics.boolRight = false;
        //    if (KeyCode == Keys.Right)
        //    {
        //        // the right arrow ID was pressed
        //        Statics.boolRight = true;
        //        KeyCode = 0;
        //    }
        //    else if (KeyCode == Keys.Left)
        //    {
        //        // the left arrow ID was pressed
        //        Statics.boolBack = true;
        //        KeyCode = 0;
        //    }
        //    else if (KeyCode == Keys.Up)
        //    {
        //        if (Grid.Rows == 1 && Grid.Cols == 1)
        //        {
        //            KeyCode = 0;
        //        }
        //    }
        //    else if (KeyCode == Keys.Down)
        //    {
        //        if (Grid.Rows == 1 && Grid.Cols == 1)
        //        {
        //            KeyCode = 0;
        //        }
        //    }
        //    else if (KeyCode == Keys.F2 && Shift == 1)
        //    {
        //        // this will show the valid accoutns form to select from
        //        if (Statics.AllowFormats)
        //        {
        //            if (blnLimitSearch)
        //            {
        //                strAcct = StaticSettings.GlobalCommandDispatcher.Send(new SelectValidAccount(EditText, Strings.Left(EditText, 1))).Result;
        //                //strAcct = frmLoadValidAccounts.InstancePtr.Init(EditText, Strings.Left(EditText, 1));
        //            }
        //            else
        //            {
        //                strAcct = StaticSettings.GlobalCommandDispatcher.Send(new SelectValidAccount(EditText,"")).Result;
        //                //strAcct = frmLoadValidAccounts.InstancePtr.Init(EditText);
        //            }
        //            if (strAcct != "")
        //            {
        //                EditText = strAcct;
        //                Grid.EditText = strAcct;
        //                Grid.TextMatrix(Row, Col, strAcct);
        //            }
        //        }
        //    }
        //}
        // VBto upgrade warning: Grid As object	OnWrite(FCGrid.VSFlexGrid, string)
        public static void SetGridFormat(FCGrid Grid, int Row, int Col, bool boolRowColChange, string strText = "", string strStartType = "")
        {
            string strTemp = "";
            if (strStartType == "")
            {
                strStartType = DefaultAccountType;
            }
            if (Strings.Trim(Grid.TextMatrix(Row, Col)) == "")
            {
                // if E, R, G, or A accounts are allowed then set the account box to the blank default account
                if (Statics.AllowFormats)
                {
                    Statics.strAccount = strStartType;
                    if (strStartType == "E")
                    {
                        Grid.TextMatrix(Row, Col, AccountFormatSetup(ref Statics.ESegmentSize));
                        strText = Statics.strAccount;
                    }
                    else if (strStartType == "G")
                    {
                        Grid.TextMatrix(Row, Col, AccountFormatSetup(ref Statics.GSegmentSize));
                        strText = Statics.strAccount;
                    }
                    else if (strStartType == "R")
                    {
                        Grid.TextMatrix(Row, Col, AccountFormatSetup(ref Statics.RSegmentSize));
                        strText = Statics.strAccount;
                    }
                    else
                    {
                        Grid.TextMatrix(Row, Col, strStartType + " _____________________");
                        strText = strStartType + " _____________________";
                    }
                }
                else
                {
                    // if E, R, G, and A accounts arent allowed show the blank M account
                    Grid.TextMatrix(Row, Col, "M _____________________");
                    strText = "M _____________________";
                    Statics.intMaxLength = 23;
                }
            }
            else
            {
                if (Statics.AllowFormats)
                {
                    // strAccount = DEFAULTACCOUNTTYPE
                    Statics.strAccount = Strings.Left(Grid.TextMatrix(Row, Col), 1);
                    if (Statics.strAccount == "E")
                    {
                        strTemp = AccountFormatSetup(ref Statics.ESegmentSize);
                        strText = Statics.strAccount;
                    }
                    else if (Statics.strAccount == "G")
                    {
                        strTemp = AccountFormatSetup(ref Statics.GSegmentSize);
                        strText = Statics.strAccount;
                    }
                    else if (Statics.strAccount == "R")
                    {
                        strTemp = AccountFormatSetup(ref Statics.RSegmentSize);
                        strText = Statics.strAccount;
                    }
                    else
                    {
                        strTemp = AccountFormatSetup(ref Statics.ESegmentSize);
                        strText = Statics.strAccount;
                        Statics.intMaxLength = 23;
                    }
                }
                else
                {
                    // if E, R, G, and A accounts arent allowed show the blank M account
                    // GRID.TextMatrix(Row, Col) = DEFAULTACCOUNTTYPE & " _______________"
                    // just let this go through so that the user can keep their account
                    Statics.intMaxLength = 23;
                }
            }
            if (boolRowColChange)
            {
                Grid.EditCell();
                Grid.EditSelStart = 0;
                Grid.EditSelLength = 1;
            }
            // End If
        }

        public static void CheckFormKeyDown(FCGrid Grid, int Row, int Col, Keys KeyCode, int Shift, int SelectStart, string EditText, int SelectLength)
        {
            CheckAgain:
            ;
            if (KeyCode == Keys.F1 || KeyCode == Keys.F2 || KeyCode == Keys.F3 || KeyCode == Keys.F4 || KeyCode == Keys.F5 || KeyCode == Keys.F6 || KeyCode == Keys.F7 || KeyCode == Keys.F8 || KeyCode == Keys.F9 || KeyCode == Keys.F10 || KeyCode == Keys.F11 || KeyCode == Keys.F12 || KeyCode == Keys.Return)
            {
                return;
            }
            if (Grid.EditSelStart == Statics.intMaxLength)
            {
                KeyCode = 0;
                return;
            }
            if (SelectStart == Grid.EditMaxLength)
            {
                Grid.EditSelStart = SelectStart - 1;
                Grid.EditSelLength = 1;
            }
            else
            {
                Grid.EditSelLength = 1;
            }
            if (Strings.Mid(EditText, SelectStart + 1, 1) == "-" || Strings.Mid(EditText, SelectStart + 1, 1) == " ")
            {
                SelectStart += 1;
                Grid.EditSelStart = SelectStart;
                Grid.EditSelLength = 1;
                goto CheckAgain;
            }
            if (SelectStart == 0)
            {
                if (KeyCode != Keys.R && KeyCode != Keys.E && KeyCode != Keys.G && KeyCode != Keys.M && KeyCode != Keys.A)
                {
                    KeyCode = 0;
                }                
            }
            if (KeyCode == Keys.Delete)
            {
                KeyCode = 0;
            }
        }

        public class StaticVariables
        {           
            public bool boolBack;
            public bool boolRight;
            public bool AllowFormats;
            public string strAccount = "";
            public string strTempAccount = string.Empty;
            public int intMaxLength;
            public string e_Format = "";
            public string g_Format = "";
            public string r_Format = "";
            public string p_Format = "";
            public string l_Format = "";
            public string v_Format = "";
            public string ESegmentSize = "";
            public string GSegmentSize = "";
            public string RSegmentSize = "";
            public string PSegmentSize = "";
            public string LSegmentSize = "";
            public string VSegmentSize = "";
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
            }
        }

    }
}