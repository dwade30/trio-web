﻿using System.Collections.Generic;

namespace TWSharedLibrary
{
    public class MenuItemInformation
    {
        public string Caption { get; set; } = "";
        public string ActionHandlerName { get; set; } = "";
        public int CommandCode { get; set; } = 0;
        public int Level { get; set; } = 1;
        public string ImageKey { get; set; } = "";
        public string Name { get; set; } = "";
        public IEnumerable<MenuItemInformation> MenuItems { get; private set; }
        public MenuItemInformation(IEnumerable<MenuItemInformation> menuItems)
        {
            MenuItems = menuItems != null ? new List<MenuItemInformation>(menuItems) : new List<MenuItemInformation>();
        }
    }
}