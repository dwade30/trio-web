﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fecherFoundation;
using SharedApplication;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace TWSharedLibrary.CashReceipts
{
	public class OpenCashDrawerHandler : CommandHandler<OpenCashDrawer, OpenCashDrawerResult>
	{
		private GlobalCashReceiptSettings globalCashReceiptSettings;
		private Setting cashReceiptPrinter;
		private ISettingService settingService;

		public OpenCashDrawerHandler(GlobalCashReceiptSettings globalCashReceiptSettings, ISettingService settingService)
		{
			this.globalCashReceiptSettings = globalCashReceiptSettings;
			this.settingService = settingService;

			cashReceiptPrinter = settingService.GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName");
		}

		protected override OpenCashDrawerResult Handle(OpenCashDrawer command)
		{
			try
			{
				if (!globalCashReceiptSettings.CashDrawerConnected || cashReceiptPrinter.SettingValue.Trim() == "")
				{
					return OpenCashDrawerResult.PrinterNotSetUp;
				}
				else if (cashReceiptPrinter.SettingValue.Trim().ToUpper() == "PRNT.TXT")
				{
					return OpenCashDrawerResult.PrintingToFile;
				}

				for (int counter = 1; counter <= globalCashReceiptSettings.CashDrawerRepeatCount; counter++)
				{
					var p = new ParameterObjects();
					int temp = 0;
					p.FunctionToExecute = FunctionToExecute.WritePrinter;
					p.Parameter[0] = new ParameterObject(cashReceiptPrinter.SettingValue.Trim(), false);
					p.Parameter[1] = new ParameterObject(globalCashReceiptSettings.CashDrawerCode, false);
					PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
				}

				return OpenCashDrawerResult.Success;
			}
			catch (Exception e)
			{
				return OpenCashDrawerResult.ErrorOpeningPort;
			}
		}
	}
}
