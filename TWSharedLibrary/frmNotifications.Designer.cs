﻿namespace TWSharedLibrary
{
    partial class frmNotifications
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
            fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonClose = new Wisej.Web.Button();
            this.flowLayoutPanel1 = new Wisej.Web.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Right)));
            this.buttonClose.AppearanceKey = "acceptButton";
            this.buttonClose.DialogResult = Wisej.Web.DialogResult.Cancel;
            this.buttonClose.Location = new System.Drawing.Point(245, 425);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(129, 36);
            this.buttonClose.TabIndex = 1;
            this.buttonClose.Text = "OK";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.FlowDirection = Wisej.Web.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(610, 404);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // frmNotifications
            // 
            this.AcceptButton = this.buttonClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.AutoSizeMode = Wisej.Web.AutoSizeMode.GrowOnly;
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(613, 481);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.buttonClose);
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNotifications";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion
        private Wisej.Web.Button buttonClose;
        private Wisej.Web.FlowLayoutPanel flowLayoutPanel1;
    }
}