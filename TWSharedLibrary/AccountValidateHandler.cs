﻿using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.LegacyCommands;
using SharedApplication.Messaging;

namespace TWSharedLibrary
{
    public class AccountValidateHandler : CommandHandler<AccountValidate,bool>
    {
        private IAccountValidationService validationService;
        public AccountValidateHandler(IAccountValidationService validationService)
        {
            this.validationService = validationService;
        }
        protected override bool Handle(AccountValidate command)
        {
            return validationService.AccountValidate(command.Account, command.ValidateMulti);
        }
    }
}