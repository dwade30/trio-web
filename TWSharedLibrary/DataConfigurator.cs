﻿using System;
using System.Data.SqlClient;
using System.Linq;
using fecherFoundation;
using Microsoft.Ajax.Utilities;
using SharedApplication.Extensions;
using Wisej.Web;

namespace TWSharedLibrary.Data
{
    public static class DataConfigurator
    {
        public static bool LoadSQLConfig(string strDefDatabase = "")
        {
            bool LoadSQLConfig = false;
            try
            {
                string strConfigPath;
                string strConnGroup;
                string clientName = "";

                SettingsInfo tSet = new SettingsInfo();
                clsDRWrapper tRs = new clsDRWrapper();
                LoadSQLConfig = false;
                strConfigPath = Wisej.Web.Application.MapPath("\\");
                strConnGroup = TWSharedLibrary.Variables.Statics.DataGroup;
                // If the environment variables are not set then just fail
                if (strConfigPath != "" && strConnGroup != "")
                {
                    // Load the config file
                    if (Strings.Right(strConfigPath, 1) != "\\")
                    {
                        strConfigPath += "\\";
                    }

                    string url = Application.Url;
                    if (url.ToUpper().Contains(".TRIO-WEB.COM"))
                    {
	                    clientName = url.Left(url.ToUpper().IndexOf(".TRIO-WEB.COM"));
						clientName = clientName.ToUpper().Replace("HTTPS://", "");
					}

                    if (tSet.LoadSettings(strConfigPath + tSet.GetDefaultSettingsFileName(), clientName))
                    {
                        StaticSettings.gGlobalSettings.DataEnvironment = strConnGroup;
                        StaticSettings.gGlobalSettings.MasterPath = tSet.MasterDirectory;
                        StaticSettings.gGlobalSettings.ApplicationPath = tSet.ApplicationDirectory;
                        StaticSettings.gGlobalSettings.LocalDataDirectory = tSet.LocalDataDirectory;
                        //StaticSettings.gGlobalSettings.GlobalDataDirectory = tSet.GlobalDataDirectory;
                        StaticSettings.gGlobalSettings.BackupPath = Application.StartupPath + "\\backups";
                        var currentGroup = SettingsInfo.Statics.WorkstationGroups.FirstOrDefault(g => g.GroupName == strConnGroup);
                        StaticSettings.gGlobalSettings.GlobalDataDirectory = currentGroup != null ? currentGroup.GlobalDataDirectory : "";
                        tSet.LoadArchives();
                        // Configure the connection to SQL Server
                        tRs.DefaultMegaGroup = strConnGroup;
                        tRs.DefaultGroup = "Live";
                        var connectionBuilder = new SqlConnectionStringBuilder(tRs.Get_ConnectionInformation("SystemSettings"));
                        StaticSettings.gGlobalSettings.DataSource = connectionBuilder.DataSource;
                        StaticSettings.gGlobalSettings.UserName = connectionBuilder.UserID;
                        StaticSettings.gGlobalSettings.Password = connectionBuilder.Password;
                        if (strDefDatabase != "")
                        {
                            tRs.SharedDefaultDB = strDefDatabase;
                            // Verify a connection to the DB
                            if (tRs.DBExists(strDefDatabase))
                            {
                                LoadSQLConfig = true;
                            }
                        }
                        else
                        {
                            if (tRs.DBExists("SystemSettings"))
                            {
                                LoadSQLConfig = true;
                            }
                        }

                        tRs.OpenRecordset("SELECT * FROM Clients", "ClientSettings");
                        if (tRs.RecordCount() == 1)
                        {
	                        StaticSettings.gGlobalSettings.ClientIdentifier = tRs.Get_Fields_Guid("ClientIdentifier");
                        }
                        else
                        {
	                        if (tRs.FindFirst("Name = '" + (StaticSettings.gGlobalSettings.ClientEnvironment == "" ? "Default" : StaticSettings.gGlobalSettings.ClientEnvironment).ToLower() + "'"))
                            {
		                        StaticSettings.gGlobalSettings.ClientIdentifier = tRs.Get_Fields_Guid("ClientIdentifier");
	                        }
	                        else
	                        {
		                        throw new Exception("Unable to Load Client Information for " + (StaticSettings.gGlobalSettings.ClientEnvironment == "" ? "Default" : StaticSettings.gGlobalSettings.ClientEnvironment));
                            }
                        }
                    }
                }
                return LoadSQLConfig;
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading configuration");
            }
            return LoadSQLConfig;
        }
    }
}