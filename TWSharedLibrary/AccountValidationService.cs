﻿using fecherFoundation;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.CentralData;
using SharedApplication.Extensions;
using SharedDataAccess;
using SharedDataAccess.Budgetary;
using SharedDataAccess.CentralData;
using System;
using System.Linq;
using Wisej.Web;

namespace TWSharedLibrary
{

    public class AccountValidationService : IAccountValidationService
    {
        private BudgetaryContext budgetaryContext;
        private CentralDataContext centralDataContext;
        private IRegionalTownService regionalTownService;
        private IBudgetaryAccountingService budgetaryAccountingService;
        private IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings;

        public AccountValidationService(ITrioContextFactory trioContextFactory,
            IRegionalTownService regionalTownService, IBudgetaryAccountingService budgetaryAccountingService,
            IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings)
        {
            //ITrioContextFactory trioContextFactory = contextFactoryService.GetContextFactory();

            budgetaryContext = trioContextFactory.GetBudgetaryContext();
            centralDataContext = trioContextFactory.GetCentralDataContext();
            this.regionalTownService = regionalTownService;
            this.budgetaryAccountingService = budgetaryAccountingService;
            this.globalBudgetaryAccountSettings = globalBudgetaryAccountSettings;

        }

        public bool AccountValidate(string account, bool blnValidateMulti = false)
        {
            bool isValidAccount = false;

            var accountFirstCharacter = account.Left(1);

            switch (accountFirstCharacter)
            {
                // E is for Expense
                case "E" when blnValidateMulti && regionalTownService.IsRegionalTown() &&
                              budgetaryAccountingService.GetDepartment(account).ToIntegerValue() < 100:
                    {
                        var expResult = budgetaryContext.ExpObjTitles.FirstOrDefault(x =>
                                                                                         (x.BreakdownCode ?? 0) != 0 && x.Expense == budgetaryAccountingService.GetExpense(account) &&
                                                                                         (!globalBudgetaryAccountSettings.ObjectExistsInExpense() ||
                                                                                          (x.Object == globalBudgetaryAccountSettings.ObjectZeroString())));
                        if (expResult != null)
                        {
                            var breakdownResults =
                                centralDataContext.RegionalTownBreakdowns.Where(x => x.Code == expResult.BreakdownCode);

                            foreach (var breakdown in breakdownResults)
                            {
                                isValidAccount = ValidExpense(account.Left(2) + breakdown.TownNumber + account.Right(account.Length - 3));
                                if (isValidAccount == false) break;
                            }

                        }
                        else
                        {
                            var deptResult = budgetaryContext.DeptDivTitles.FirstOrDefault(x =>
                                                                                               ((x.BreakdownCode ?? 0) != 0) &&
                                                                                               x.Department == "1" + budgetaryAccountingService.GetDepartment(account)
                                                                                                                                               .Right(globalBudgetaryAccountSettings.ExpenseDepartmentLength() - 1) &&
                                                                                               (!globalBudgetaryAccountSettings.DivisionExistsInExpense() ||
                                                                                                (x.Division == globalBudgetaryAccountSettings.ExpenseZeroDivisionString())));
                            if (deptResult != null)
                            {
                                var breakdownResults =
                                    centralDataContext.RegionalTownBreakdowns.Where(x => x.Code == deptResult.BreakdownCode);

                                
                                    foreach (var breakdown in breakdownResults)
                                    {
                                        isValidAccount =
                                            ValidExpense(account.Left(2) + breakdown.TownNumber +
                                                         account.Right(account.Length - 3));
                                        if (isValidAccount == false) break;
                                    }
                                
                            }
                        }

                        break;
                    }
                case "E":
                    isValidAccount = ValidExpense(account);

                    break;
                case "R":
                    isValidAccount = ValidRevenue(account);

                    break;
                case "G":
                    isValidAccount = ValidLedger(account);

                    break;
                case "M":
                    isValidAccount = account.Length > 2;
                    break;

                default:
                    break;
            }

            return isValidAccount;
        }

        public bool ValidExpense(string account)
        {
            try
            {
                string Dept = "";
                string Div = "";
                string tempDiv = "";
                string tempObj = "";
                string Expense = "";
                string Object = "";

                AccountBreakdownInformation accountBreakdown;

                accountBreakdown = budgetaryAccountingService.GetAccountBreakDownInformation(account);

                tempDiv = globalBudgetaryAccountSettings.ExpenseZeroDivisionString();
                tempObj = globalBudgetaryAccountSettings.ObjectZeroString();

                Dept = accountBreakdown.FirstAccountField;

                Div = globalBudgetaryAccountSettings.DivisionExistsInExpense()
                    ? accountBreakdown.SecondAccountField
                    : "";

                Expense = globalBudgetaryAccountSettings.DivisionExistsInExpense()
                    ? accountBreakdown.ThirdAccountField
                    : accountBreakdown.SecondAccountField;

                Object = !globalBudgetaryAccountSettings.ObjectExistsInExpense() ? "" :
                    globalBudgetaryAccountSettings.DivisionExistsInExpense() ? accountBreakdown.FourthAccountField :
                    accountBreakdown.ThirdAccountField;

                var deptDivCheck = budgetaryContext.DeptDivTitles.FirstOrDefault(x =>
                    x.Department == Dept && x.Division ==
                    (globalBudgetaryAccountSettings.DivisionExistsInExpense() ? Div : tempDiv));
                if (deptDivCheck == null) return false;

                var expObjCheck = budgetaryContext.ExpObjTitles.FirstOrDefault(x =>
                    x.Expense == Expense && x.Object ==
                    (globalBudgetaryAccountSettings.ObjectExistsInExpense() ? Object : tempObj));

                return expObjCheck != null && CheckValidAccount(account);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool ValidRevenue(string account)
        {
            bool ValidRevenue = false;

            try
            {
                string Dept = "";
                string Div = "";
                string tempRevDiv = "";
                string Revenue = "";

                AccountBreakdownInformation accountBreakdown;

                accountBreakdown = budgetaryAccountingService.GetAccountBreakDownInformation(account);

                tempRevDiv = globalBudgetaryAccountSettings.RevenueZeroDivisionString();

                Dept = accountBreakdown.FirstAccountField;

                Div = globalBudgetaryAccountSettings.DivisionExistsInRevenue()
                        ? accountBreakdown.SecondAccountField
                        : "";
                Revenue = globalBudgetaryAccountSettings.DivisionExistsInRevenue()
                        ? accountBreakdown.ThirdAccountField
                        : accountBreakdown.SecondAccountField;

                var deptDivCheck = budgetaryContext.DeptDivTitles.FirstOrDefault(x =>
                    x.Department == Dept && x.Division ==
                    (globalBudgetaryAccountSettings.DivisionExistsInRevenue() ? Div : tempRevDiv));

                if (deptDivCheck == null) return false;

                var revCheck = budgetaryContext.RevTitles.FirstOrDefault(x =>
                    x.Department == Dept &&
                    (!globalBudgetaryAccountSettings.DivisionExistsInRevenue() || x.Division == Div) &&
                    x.Revenue == Revenue);

                return revCheck != null && CheckValidAccount(account);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool ValidLedger(string account)
        {
            try
            {
                string Fund = "";
                string acct = "";
                string Year = "";

                AccountBreakdownInformation accountBreakdown;

                accountBreakdown = budgetaryAccountingService.GetAccountBreakDownInformation(account);

                Fund = accountBreakdown.FirstAccountField;
                acct = accountBreakdown.SecondAccountField;
                Year = accountBreakdown.ThirdAccountField;

                var acctCheck = budgetaryContext.LedgerTitles.FirstOrDefault(x =>
                    x.Fund == Fund && x.Account == acct &&
                    (!globalBudgetaryAccountSettings.SuffixExistsInLedger() || x.Year == Year));

                return acctCheck != null && CheckValidAccount(account);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CheckValidAccount(string x)
        {
            AccountBreakdownInformation accountBreakdown;
            int messageResult;

            accountBreakdown = CheckValidAccountNoUserInteraction(x);

            if (accountBreakdown.Valid) return true;

            switch (globalBudgetaryAccountSettings.ValidAcctCheck)
            {
                case ValidAccountSetting.ValidOrInvalidAccounts:
                {
                    messageResult = FCConvert.ToInt32(FCMessageBox.Show(
                                                          accountBreakdown.Account +
                                                          " is not a Valid Account.  Would you like to add it to the Valid Accounts file?",
                                                          MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Invalid Account Warning"));
                    
                    if (messageResult == FCConvert.ToInt32(DialogResult.Yes)) return MakeExistingAccountValid(accountBreakdown);

                    return messageResult == FCConvert.ToInt32(DialogResult.No);
                }
                case ValidAccountSetting.AnyExistingAccount:
                    return MakeExistingAccountValid(accountBreakdown);

                default:
                    FCMessageBox.Show(accountBreakdown.Account + " is not set up in your valid accounts file",
                                      MsgBoxStyle.Information, "Invalid Account");
                    return false;
            }
        }

        public bool MakeExistingAccountValid(AccountBreakdownInformation accountBreakdown)
        {
            try
            {
                var acct = budgetaryContext.AccountMasters.FirstOrDefault(x => x.Account == accountBreakdown.Account);
                if (acct != null)
                {
                    acct.Valid = true;
                    budgetaryContext.SaveChanges();
                }
                else
                {
                    budgetaryContext.AccountMasters.Add(new AccountMaster
                    {
                        Account = accountBreakdown.Account,
                        AccountType = accountBreakdown.AccountType,
                        FirstAccountField = accountBreakdown.FirstAccountField,
                        SecondAccountField = accountBreakdown.SecondAccountField,
                        ThirdAccountField = accountBreakdown.ThirdAccountField,
                        FourthAccountField = accountBreakdown.FourthAccountField,
                        FifthAccountField = accountBreakdown.FifthAccountField,
                        DateCreated = DateTime.Today,
                        Valid = true
                    });
                    budgetaryContext.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public AccountBreakdownInformation CheckValidAccountNoUserInteraction(string x)
        {
            AccountBreakdownInformation accountBreakdown;

            accountBreakdown = budgetaryAccountingService.GetAccountBreakDownInformation(x);
            accountBreakdown.Valid = ValidAccountExists(accountBreakdown);

            return accountBreakdown;
        }

        private bool ValidAccountExists(AccountBreakdownInformation accountBreakdown)
        {
            bool AccountCreated = false;


            //var result = budgetaryContext.AccountMasters.FirstOrDefault(y => y.AccountType == accountBreakdown.AccountType && (y.FirstAccountField == accountBreakdown.FirstAccountField || accountBreakdown.FirstAccountField == "") && (y.SecondAccountField == accountBreakdown.SecondAccountField || accountBreakdown.SecondAccountField == "") && (y.ThirdAccountField == accountBreakdown.ThirdAccountField || accountBreakdown.ThirdAccountField == "") && (y.FourthAccountField == accountBreakdown.FourthAccountField || accountBreakdown.FourthAccountField == "") && (y.FifthAccountField == accountBreakdown.FifthAccountField || accountBreakdown.FifthAccountField == ""));

            var query = budgetaryContext.AccountMasters.Where(y => y.AccountType == accountBreakdown.AccountType);
            if (accountBreakdown.FirstAccountField != "")
            {
                query = query.Where(y => y.FirstAccountField == accountBreakdown.FirstAccountField);
            }
            if (accountBreakdown.SecondAccountField != "")
            {
                query = query.Where(y => y.SecondAccountField == accountBreakdown.SecondAccountField);
            }

            if (accountBreakdown.ThirdAccountField != "")
            {
                query = query.Where(y => y.ThirdAccountField == accountBreakdown.ThirdAccountField);
            }

            if (accountBreakdown.FourthAccountField != "")
            {
                query = query.Where(y => y.FourthAccountField == accountBreakdown.FourthAccountField);
            }

            if (accountBreakdown.FifthAccountField != "")
            {
                query = query.Where(y => y.FifthAccountField == accountBreakdown.FifthAccountField);
            }

            var result = query.FirstOrDefault();

            return result != null && (result.Valid ?? false);
        }


    }
}
