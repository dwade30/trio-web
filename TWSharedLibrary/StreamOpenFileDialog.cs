﻿using fecherFoundation.VisualBasicLayer;
using System;
using System.IO;
using System.Linq;
using System.Web;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class StreamOpenFileDialog : Form
    {
        private string filter = "";

        public StreamOpenFileDialog()
        {
            InitializeComponent();
            
        }

        public HttpFileCollection UploadedFiles { get; private set; } = null;

        public string FileName { get; set; }

        public string Filter
        {
            get
            {
                return filter;
            }
            set
            {
                if (value != filter)
                {
                    filter = value;

                    upload1.AllowedFileTypes = String.Join(", ", filter.Split('|').Select(f => f.Trim()).Where(f => f.StartsWith("*.")).Select(f =>f.Substring(1)));
                }
            }
        }

        private void upload1_Uploaded(object sender, UploadedEventArgs e)
        {
            UploadedFiles = e.Files;
            this.FileName = e.Files[0].FileName;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
