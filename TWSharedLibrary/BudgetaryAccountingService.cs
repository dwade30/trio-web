﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fecherFoundation;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace TWSharedLibrary
{
	public class BudgetaryAccountingService : IBudgetaryAccountingService
	{
		private IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings;
		private IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>> deptDivTitleQueryHandler;
		private IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>> ledgerTitleQueryHandler;
		private CommandDispatcher commandDispatcher;

		public BudgetaryAccountingService(IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings, IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>> deptDivTitleQueryHandler, IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>> ledgerTitleQueryHandler, CommandDispatcher commandDispatcher)
		{
			this.globalBudgetaryAccountSettings = globalBudgetaryAccountSettings;
			this.deptDivTitleQueryHandler = deptDivTitleQueryHandler;
			this.ledgerTitleQueryHandler = ledgerTitleQueryHandler;
			this.commandDispatcher = commandDispatcher;
		}

		public string GetDepartment(string account)
		{
			try
			{
				return account.Substring(2, globalBudgetaryAccountSettings.ExpenseDepartmentLength());
			}
			catch
			{
				return new string('0', globalBudgetaryAccountSettings.ExpenseDepartmentLength());
			}
		}

		public string GetExpense(string account)
		{
			try
			{
				if (globalBudgetaryAccountSettings.DivisionExistsInExpense())
				{
					return account.Substring(4 + globalBudgetaryAccountSettings.ExpenseDepartmentLength() + globalBudgetaryAccountSettings.ExpenseDivisionLength(), globalBudgetaryAccountSettings.ExpenseExpenseLength());
				}
				else
				{
					return account.Substring(3 + globalBudgetaryAccountSettings.ExpenseDepartmentLength(), globalBudgetaryAccountSettings.ExpenseExpenseLength());
				}
			}
			catch
			{
				return new string('0', globalBudgetaryAccountSettings.ExpenseExpenseLength());
			}

			
		}

		public AccountBreakdownInformation GetAccountBreakDownInformation(string account)
		{
			var accountBreakdown = new AccountBreakdownInformation
			{
				Account = account,
				Valid = false,
				FirstAccountField = "",
				SecondAccountField = "",
				ThirdAccountField = "",
				FourthAccountField = "",
				FifthAccountField = ""
			};

			int tempSpace;

			accountBreakdown.AccountType = Strings.Left(account, 1);
			account = Strings.Right(account, account.Length - 2);
			tempSpace = Strings.InStr(1, account, "-");

			accountBreakdown.FirstAccountField = Strings.Left(account, tempSpace - 1);
			account = Strings.Right(account, account.Length - tempSpace);
			tempSpace = Strings.InStr(1, account, "-");
			if (tempSpace > 0)
			{
				accountBreakdown.SecondAccountField = Strings.Left(account, tempSpace - 1);
				account = Strings.Right(account, account.Length - tempSpace);
			}
			else if (account.Length > 0)
			{
				accountBreakdown.SecondAccountField = account;
				account = "";
			}

			if (account != "")
			{
				tempSpace = Strings.InStr(1, account, "-");
				if (tempSpace > 0)
				{
					accountBreakdown.ThirdAccountField = Strings.Left(account, tempSpace - 1);
					account = Strings.Right(account, account.Length - tempSpace);
				}
				else if (account.Length > 0)
				{
					accountBreakdown.ThirdAccountField = account;
					account = "";
				}
			}

			if (account != "")
			{
				tempSpace = Strings.InStr(1, account, "-");
				if (tempSpace > 0)
				{
					accountBreakdown.FourthAccountField = Strings.Left(account, tempSpace - 1);
					account = Strings.Right(account, account.Length - tempSpace);
				}
				else if (account.Length > 0)
				{
					accountBreakdown.FourthAccountField = account;
					account = "";
				}
			}

			if (account != "")
			{
				accountBreakdown.FifthAccountField = account;
			}

			return accountBreakdown;
		}

		public ExpenseAccountBreakdown ConvertAccountBreakdownToExpenseAccountBreakdown(AccountBreakdownInformation accountBreakdown)
		{
			return new ExpenseAccountBreakdown
			{
				Department = accountBreakdown.FirstAccountField,
				Division = globalBudgetaryAccountSettings.DivisionExistsInExpense() ? accountBreakdown.SecondAccountField : "",
				Expense = globalBudgetaryAccountSettings.DivisionExistsInExpense() ? accountBreakdown.ThirdAccountField : accountBreakdown.SecondAccountField,
				Object = !globalBudgetaryAccountSettings.ObjectExistsInExpense() ? "" : globalBudgetaryAccountSettings.DivisionExistsInExpense() ? accountBreakdown.FourthAccountField : accountBreakdown.ThirdAccountField
			};
		}

		public LedgerAccountBreakdown ConvertAccountBreakdownToLedgerAccountBreakdown(AccountBreakdownInformation accountBreakdown)
		{
			return new LedgerAccountBreakdown
			{
				Fund = accountBreakdown.FirstAccountField,
				Account = accountBreakdown.SecondAccountField,
				Suffix = globalBudgetaryAccountSettings.SuffixExistsInLedger() ? accountBreakdown.ThirdAccountField : ""
			};
		}

		public RevenueAccountBreakdown ConvertAccountBreakdownToRevenueAccountBreakdown(AccountBreakdownInformation accountBreakdown)
		{
			return new RevenueAccountBreakdown
			{
				Department = accountBreakdown.FirstAccountField,
				Division = globalBudgetaryAccountSettings.DivisionExistsInRevenue() ? accountBreakdown.SecondAccountField : "",
				Revenue = globalBudgetaryAccountSettings.DivisionExistsInRevenue() ? accountBreakdown.ThirdAccountField : accountBreakdown.SecondAccountField,
			};
		}

		public string GetFundFromLedger(string account)
		{
			try
			{
				return account.Substring(2, globalBudgetaryAccountSettings.LedgerFundLength());
			}
			catch
			{
				return new string ('0', globalBudgetaryAccountSettings.LedgerFundLength());
			}
		}

		public int GetFundFromAccount(string account)
		{
			try
			{
				if (account != "" && account.Length > 2 && !account.Contains("_"))
				{
					// if it is not empty
					if (account.Left(1) != "M")
					{
						if (account.Left(1) == "G")
						{
							return GetFundFromLedger(account).ToIntegerValue();
						}
						else
						{
							return GetFundFromDepartment(account).ToIntegerValue();
						}
					}
				}

				return 0;
			}
			catch
			{
				return 0;
			}
		}

		private string GetFundFromDepartment(string account)
		{
			try
			{
				var result = deptDivTitleQueryHandler.ExecuteQuery(new DeptDivTitleSearchCriteria
				{
					Department = GetDepartment(account),
					Division = globalBudgetaryAccountSettings.ExpenseZeroDivisionString()
				}).FirstOrDefault();
				
				if (result != null)
				{
					return result.Fund;
				}
				else
				{
					return new string('0', globalBudgetaryAccountSettings.LedgerFundLength());
				}
			}
			catch
			{
				return new string('0', globalBudgetaryAccountSettings.LedgerFundLength());
			}
		}

		private IEnumerable<JournalFundInfo> CalculateFundCash(IEnumerable<JournalAccountInfo> entries)
		{
			var result = new List<JournalFundInfo>();

			foreach (var entry in entries)
			{
				var fund = GetFundFromAccount(entry.BudgetaryAccountNumber);

				var fundInfo = result.FirstOrDefault(x => x.Fund == fund);
				if (fundInfo != null)
				{
					fundInfo.Amount = fundInfo.Amount + entry.Amount;
				}
				else
				{
					var searchResult = ledgerTitleQueryHandler.ExecuteQuery(new LedgerTitleSearchCriteria
					{
						Fund = globalBudgetaryAccountSettings.FormatBudgetaryFund(fund),
						Account = globalBudgetaryAccountSettings.AccountZeroString()
					}).FirstOrDefault();

					result.Add(new JournalFundInfo
					{
						Fund = fund,
						Amount = entry.Amount,
						DefaultCashFund = searchResult?.CashFund ?? "",
						UseDueToFrom = searchResult?.UseDueToFrom ?? false,
						OverrideAccountsPayableCashFund = searchResult?.ApoverrideCashFund ?? "",
						OverrideCashReceiptsCashFund = searchResult?.CroverrideCashFund ?? "",
						OverridePayrollCashFund = searchResult?.PyoverrideCashFund ?? ""
					});
				}
			}

			return result;
		}

		public IEnumerable<JournalAccountInfo> CreateCashEntries(IEnumerable<JournalAccountInfo> entries, JournalType journalType, CashAccountType cashType)
		{
			var funds = CalculateFundCash(entries);
			var result = new List<JournalAccountInfo>();
			var standardCashAccount = commandDispatcher.Send(new GetStandardCashAccount
			{
				CashAccountType = cashType
			}).Result;

			foreach (var fundInfo in funds)
			{
				var fund = ReturnCorrectCashFund(fundInfo, journalType);

				result.Add(new JournalAccountInfo
				{
					BudgetaryAccountNumber = globalBudgetaryAccountSettings.BuildLedgerAccount(fund, standardCashAccount.ToIntegerValue()),
					Amount = fundInfo.Amount * -1
				});
			}

			return result;
		}

		private int ReturnCorrectCashFund(JournalFundInfo fundInfo, JournalType type)
		{
			return !fundInfo.UseDueToFrom ? fundInfo.Fund :
				type == JournalType.AccountsPayable ? fundInfo.OverrideAccountsPayableCashFund != ""
					? fundInfo.OverrideAccountsPayableCashFund.ToIntegerValue()
					: fundInfo.DefaultCashFund.ToIntegerValue() :
				type == JournalType.CashReceipts ? fundInfo.OverrideCashReceiptsCashFund != ""
					? fundInfo.OverrideCashReceiptsCashFund.ToIntegerValue()
					: fundInfo.DefaultCashFund.ToIntegerValue() :
				type == JournalType.CashReceipts ? fundInfo.OverrideCashReceiptsCashFund != ""
					? fundInfo.OverrideCashReceiptsCashFund.ToIntegerValue()
					: fundInfo.DefaultCashFund.ToIntegerValue() :
				fundInfo.DefaultCashFund.ToIntegerValue();
		}
	}

	
}
