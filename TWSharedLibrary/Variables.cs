﻿using fecherFoundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;
using Wisej.Core;

namespace TWSharedLibrary
{
    public class Variables
    {
        public class StaticVariables
        {
            public int IntUserID;
            public string UserID = string.Empty;
            public string UserName = string.Empty;
            public string DataGroup = string.Empty;
            //FC:FINAL:IPI:#1430 - add variables used in saving/getting old registry settings 
            public string OwnerType = "Machine";
            public string OwnerID = Application.ClientFingerprint.ToString();
            public object MVModuleLocker = new object();
            public bool MVModuleWait = false;
            public object BDModuleArchiveLocker = new object();
            public bool BDModuleArchiveWait = false;
			public object RBModuleLocker = new object();
            public bool RBModuleWait = false;
            public bool CKModuleWait = false;
            public object CKModuleLocker = new object();
            public bool ShowOpIDForm = true;
            public string ReportPath = "";
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
