﻿using Autofac;
using SharedApplication;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CentralData;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralDocuments.Interfaces;
using TWSharedLibrary.CentralDocuments;

namespace TWSharedLibrary.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<AccountValidationService>().As<IAccountValidationService>();
			builder.RegisterType<RegionalTownService>().As<IRegionalTownService>();
			builder.RegisterType<BudgetaryAccountingService>().As<IBudgetaryAccountingService>();
            builder.RegisterType<frmEffectiveDateChange>().As<IModalView<IEffectiveDateChangeViewModel>>();

            builder.RegisterType<GetCentralDocument_Recordset>().As<IGetCentralDocument>();
            builder.RegisterType<GetHeadersByAltReference_Recordset>().As<IGetCentralDocumentHeadersByAltReference>();
            builder.RegisterType<GetCentralDocumentHeadersByReference_Recordset>().As<IGetCentralDocumentHeadersByReference>();
            //
            builder.RegisterType<DeleteCentralDocument_Recordset>().As<IDeleteCentralDocument>();
            builder.RegisterType<DeleteCentralDocumentsByAltReference_Recordset>().As<IDeleteCentralDocumentsByAltReference>();
            builder.RegisterType<DeleteCentralDocumentsByReference_Recordset>().As<IDeleteCentralDocumentsByReference>();
            //
            builder.RegisterType<UpdateCentralDocument_Recordset>().As<IUpdateCentralDocument>();
            builder.RegisterType<InsertCentralDocument_Recordset>().As<IInsertCentralDocument>();
            builder.RegisterType<frmDocumentViewer>().As<IModalView<IDocumentViewerViewModel>>();
            builder.RegisterType<frmAddCentralDocument>().As<IModalView<IAddCentralDocumentViewModel>>();
        }

	}
}
