﻿using System;
using System.Collections.Generic;
using Wisej.Web;

namespace TWSharedLibrary
{
    public partial class frmNotifications : Form
    {
        private List<string> notifications = new List<string>();
        public frmNotifications()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {

        }

        public frmNotifications(IEnumerable<string> notifications) : this()
        {
            this.notifications.AddRange(notifications);
            ShowNotifications();
        }
        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ShowNotifications()
        {
            foreach (var notification in notifications)
            {
                flowLayoutPanel1.Controls.Add(new Label(){Text = notification, Visible =  true, Anchor = AnchorStyles.Left & AnchorStyles.Right & AnchorStyles.Top, AutoSize = true, Padding =  new Padding(0,3,0,3)});
            }
        }
    }
}
