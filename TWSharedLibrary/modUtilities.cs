﻿using fecherFoundation;

namespace TWSharedLibrary
{
    public static class modUtilities
    {
        private const string FORMAT_CURRENCY = "#,##0.00";

        public static string FormatTotal(double amt)
        {
            return Strings.Format(amt, FORMAT_CURRENCY);
        }

        public static string FormatTotal(decimal amt)
        {
            return Strings.Format(amt, FORMAT_CURRENCY);
        }
    }
}