﻿using System.IO;
using System.Net;
using System.Windows.Forms;
using fecherFoundation.VisualBasicLayer;
using SharedApplication;
using SharedApplication.Messaging;

namespace TWSharedLibrary
{
    public class SendAlertMessageHandler : CommandHandler<SendAlertMessage>
    {
        protected override void Handle(SendAlertMessage command)
        {
            //var writer = FCFileSystem.CreateTextFile("testlog.txt");
            //writer.WriteLine(command.Message);
            //writer.Flush();
            //writer.Close();
            //writer.Dispose();

                MessageBox.Show(command.Message, command.Title, MessageBoxButtons.OK);
        }
    }
}