using System;
using SharedApplication.Telemetry;

namespace TWSharedLibrary
{
    public static class TelemetryMethods
    {
        public static void DoTelemetry_Event(string eventName)
        {
            StaticSettings.GlobalCommandDispatcher.Send(new TrackEventCommand(eventName));
        }

        private static void DoTelemetry_Timing(string title, DateTime startDateTime, DateTime endDateTime)
        {
            StaticSettings.GlobalCommandDispatcher.Send(new TrackTimingCommand(title,0,startDateTime,endDateTime));
        }

        private static void DoTelemetry_Timing(string title, long milliseconds)
        {
            StaticSettings.GlobalCommandDispatcher.Send(new TrackTimingCommand(title, milliseconds, DateTime.MinValue,DateTime.MinValue));
        }
    }
}