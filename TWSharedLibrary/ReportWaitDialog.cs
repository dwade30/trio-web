﻿using fecherFoundation;
using System;
using Wisej.Web;

namespace Global
{
    public partial class ReportWaitDialog : FCForm
    {
        private int pageNumber;

        public ReportWaitDialog()
        {
            InitializeComponent();
        }

        public static ReportWaitDialog InstancePtr
        {
            get
            {
                return (ReportWaitDialog)Sys.GetInstance(typeof(ReportWaitDialog));
            }
        }

        public int PageNumber
        {
            get
            {
                return this.pageNumber;
            }
            set
            {
                this.pageNumber = value;
                this.label1.Text = $"Running report... Page {pageNumber}+";
                FCUtils.ApplicationUpdate(this.label1);
            }
        }

        public void ShowWaitAndRunReport(FCSectionReport rptObj)
        {
            rptObj.PageEnd += (s, e) => { this.PageNumber = rptObj.PageNumber; };
            FCUtils.StartTask(this, () =>
            {
                rptObj.RunReport();
                this.Done();
            });
            this.PageNumber = 0;
            this.ShowDialog(App.MainForm);
        }

        private void Done()
        {
            this.Close();
        }
    }
}
