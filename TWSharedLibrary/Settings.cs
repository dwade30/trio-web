﻿using System;
using fecherFoundation;
using SharedApplication;
using SharedApplication.Authorization;
using SharedApplication.CashReceipts;
using SharedApplication.UtilityBilling;
using System.Collections.Generic;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections;
using SharedApplication.Telemetry;
using Wisej.Web;

namespace TWSharedLibrary
{
    public class StaticSettings
    {
        public static cGlobalSettings gGlobalSettings
        {
            get
            {
                return (cGlobalSettings)Sys.GetInstance(typeof(cGlobalSettings));
            }
        }

        public static GlobalActiveModuleSettings gGlobalActiveModuleSettings
		{
	        get
	        {
                return (GlobalActiveModuleSettings)Sys.GetInstance(typeof(GlobalActiveModuleSettings));
	        }
        }

        public static GlobalBudgetaryAccountSettings gGlobalBudgetaryAccountSettings
		{
	        get
	        {
                return (GlobalBudgetaryAccountSettings)Sys.GetInstance(typeof(GlobalBudgetaryAccountSettings));
	        }
        }

        public static GlobalCashReceiptSettings gGlobalCashReceiptSettings
        {
	        get
	        {
                var settings = (GlobalCashReceiptSettings)Sys.GetInstance(typeof(GlobalCashReceiptSettings));
                //var tempUri = new Uri(Application.Url);
                //settings.ResponseURL = tempUri.GetLeftPart(System.UriPartial.Authority) + @"/";
                return settings;
            }
        }

        public static GlobalAccountsReceivableSettings gGlobalAccountsReceivableSettings
		{
	        get
	        {
                return (GlobalAccountsReceivableSettings)Sys.GetInstance(typeof(GlobalAccountsReceivableSettings));
            }
        }

		private static GlobalColorSettings trioColorSettings;
        public static GlobalColorSettings TrioColorSettings {
            get
            {
                if (trioColorSettings == null)
                {
                    trioColorSettings = GetTrioColors();
                }

                return trioColorSettings;
            }
            set { trioColorSettings = value; }
        }

        public static GlobalColorSettings GetTrioColors()
        {
            var trioColors = new GlobalColorSettings();
            trioColors.TRIOCOLORGRAYEDOUT = Information.RGB(99, 99, 99);
            trioColors.TRIOCOLORBLACK = Information.RGB(0, 0, 1);
            trioColors.TRIOCOLORRED = Information.RGB(255, 0, 0);
            trioColors.TRIOCOLORBLUE = Information.RGB(45, 46, 188);
            trioColors.TRIOCOLORFORECOLOR = Information.RGB(0, 0, 1);
            trioColors.TRIOCOLORDISABLEDOPTION = Information.RGB(150, 150, 150);
            trioColors.TRIOCOLORGRAYEDOUTTEXTBOX = Information.RGB(215, 215, 215);
            trioColors.TRIOCOLORGRAYBACKGROUND = Information.RGB(190, 190, 190);
            trioColors.TRIOCOLORHIGHLIGHT = Information.RGB(254, 247, 122);
            return trioColors;
        }

        public static DataEnvironmentSettings EnvironmentSettings
        {
            get
            {
                return (DataEnvironmentSettings) Sys.GetInstance(typeof(DataEnvironmentSettings));

            }
        }

        public static IUserPermissionSet CurrentUserPermissionSet
        {
            get
            {
                var permissionSet = (IUserPermissionSet) Sys.GetInstance(typeof(IUserPermissionSet));
                if (permissionSet != null)
                {
                    return permissionSet;
                }
                string key = typeof(IUserPermissionSet).Assembly.GetName().Name + "." + typeof(IUserPermissionSet).FullName;
                permissionSet = new UserPermissionSet(0, new List<PermissionItem>());
                Sys.SaveInstance(key,permissionSet);
                return permissionSet;
            }
            set
            {
                string key = typeof(IUserPermissionSet).Assembly.GetName().Name + "." + typeof(IUserPermissionSet).FullName;
                Sys.SaveInstance(key, value);
            }
        }

        public static GlobalUtilityBillingSettings UtilityBillingSettings
        {
            get { return (GlobalUtilityBillingSettings)Sys.GetInstance(typeof(GlobalUtilityBillingSettings)); }
            set => Sys.SaveInstance(value);
        }

        public static UserInformation UserInformation
        {
            get =>  (UserInformation) Sys.GetInstance(typeof(UserInformation));
            set
            {
                string key = typeof(UserInformation).Assembly.GetName().Name + "." + typeof(UserInformation).FullName;
                Sys.SaveInstance(key,value);
            }
        }

		public static GlobalTaxCollectionSettings TaxCollectionSettings
        {
            get =>  (GlobalTaxCollectionSettings) Sys.GetInstance(typeof(GlobalTaxCollectionSettings));
        }

        public static GlobalTaxCollectionValues TaxCollectionValues
        {
            get =>  (GlobalTaxCollectionValues) Sys.GetInstance(typeof(GlobalTaxCollectionValues));
        }

        public static GlobalRealEstateSettings RealEstateSettings
        {
            get => (GlobalRealEstateSettings) Sys.GetInstance(typeof(GlobalRealEstateSettings));
        }

        public static CommandDispatcher GlobalCommandDispatcher
        {
            get => (CommandDispatcher) Sys.GetInstance(typeof(CommandDispatcher));

            set
            {
                string key = typeof(CommandDispatcher).Assembly.GetName().Name + "." + typeof(CommandDispatcher).FullName;
                Sys.SaveInstance(key, value);
            }
        }

        public static EventPublisher GlobalEventPublisher
        {
            get => (EventPublisher) Sys.GetInstance(typeof(CommandDispatcher));
            set
            {
                string key = typeof(EventPublisher).Assembly.GetName().Name + "." + typeof(EventPublisher).FullName;
                Sys.SaveInstance(key,value);
            }
        }

        public static ISettingService GlobalSettingService {
	        get => (ISettingService)Sys.GetInstance(typeof(ISettingService));
			set
	        {
		        string key = typeof(ISettingService).Assembly.GetName().Name + "." + typeof(ISettingService).FullName;
		        Sys.SaveInstance(key, value);
	        }
		}

		private static ITelemetryService _telemetryService;
        public static ITelemetryService GlobalTelemetryService {	       
            get => (ITelemetryService) Sys.GetInstance(typeof(ITelemetryService));

            set
            {
                string key = typeof(ITelemetryService).Assembly.GetName().Name + "." + typeof(ITelemetryService).FullName;
                Sys.SaveInstance(key, value);
            }
        }
	}
}
