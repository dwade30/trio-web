﻿using System.Xml.Linq;
using System.Runtime.InteropServices;
using System.Collections;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using System.Xml;

namespace TWSharedLibrary.Data
{
	public class WorkstationGroup
	{
		private string strGlobalDataDirectory = "";

		public string GlobalDataDirectory
		{
			get
			{
				return strGlobalDataDirectory;
			}
			set
			{
				strGlobalDataDirectory = value;
			}
		}

		private string strGroupName = "";

		public string GroupName
		{
			get
			{
				return strGroupName;
			}
			set
			{
				strGroupName = value;
			}
		}
	}

	public class SettingsInfo
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "38e0ed3c-7aea-43df-9a90-244a43ed75a1";
		public const string InterfaceId = "9be4fc55-fca4-42dd-9aa3-cd37257b5052";
		#endregion

		public const string EventsId = "e4c9825c-bb8a-4ff5-94df-4e86959cf20a";
		// A creatable COM class must have a Public Sub New()
		// with no parameters, otherwise, the class will not be
		// registered in the COM registry and cannot be created
		// via CreateObject.
		public SettingsInfo()
		{
			//base.New();
		}

		static SettingsInfo()
		{
			Statics.strMachineName = System.Environment.MachineName;
		}

		public string GetItem(string strItemName)
		{
			if (Statics.tItems.ContainsKey(strItemName))
			{
				return Statics.tItems[strItemName];
			}
			else
			{
				return "";
			}
		}

		public void SetItem(string strItemName, string value)
		{
			if (Statics.tItems.ContainsKey(strItemName))
			{
				Statics.tItems[strItemName] = value;
			}
			else
			{
				Statics.tItems.Add(strItemName, value);
			}
		}

		public WorkstationGroup[] GetAvailableSetups()
		{
			return Statics.WorkstationGroups.ToArray();
		}

		public string MasterDirectory
		{
			get
			{
				return Statics.strMasterDirectory;
			}
			set
			{
				Statics.strMasterDirectory = value;
			}
		}

		public string BackupDirectory
		{
			get
			{
				return Statics.strBackupDirectory;
			}
			set
			{
				Statics.strBackupDirectory = value;
			}
		}

		public string ApplicationDirectory
		{
			get
			{
				return Statics.strApplicationDirectory;
			}
			set
			{
				Statics.strApplicationDirectory = value;
			}
		}

		public string GlobalDataDirectory
		{
			get
			{
				return Statics.strGlobalDataDirectory;
			}
			set
			{
				Statics.strGlobalDataDirectory = value;
			}
		}

		public string LocalDataDirectory
		{
			get
			{
				return Statics.strLocalDataDirectory;
			}
			set
			{
				Statics.strLocalDataDirectory = value;
			}
		}

		public string GetDefaultSettingsFileName()
		{
			return "TRIOClientConfig.xml";
		}

		public void ClearWorkstationSettings()
		{
			Statics.WorkstationGroups.Clear();
		}

		public bool LoadSettings(string strFileName, string clientName = "")
		{
			XDocument SettingsDoc = null;
			if (!System.IO.File.Exists(strFileName))
			{
				return false;
			}
			try
			{
				SettingsDoc = XDocument.Load(strFileName);
				ConnectionMegaGroup tMega = null;
				ConnectionGroup tGroup = null;
				ConnectionInfo tInfo = null;
				WorkstationGroup tWgroup = null;
				System.Collections.Generic.Dictionary<string, string> tD = new System.Collections.Generic.Dictionary<string, string>();
				XElement tRoot;
				if (SettingsDoc != null)
				{
					ConnectionPool.ClearSettings();
					tRoot = SettingsDoc.Element("Setup");
					if (Conversion.Val(tRoot.Attribute("Version").Value) >= 3.0)
					{
						if (clientName != "")
						{
							tRoot = tRoot.Element("Clients").Elements("Client").FirstOrDefault(x => x.Attribute("Name").Value.Trim().ToUpper() == clientName.Trim().ToUpper());
						}
						else
						{
							tRoot = tRoot.Element("Clients").Element("Client");
						}

						if (tRoot == null)
						{
							return false;
						}

						ClearWorkstationSettings();
						Statics.strBackupDirectory = tRoot.Element("Global").Element("BackupDirectory").Value;
						Statics.strMasterDirectory = tRoot.Element("Global").Element("MasterDirectory").Value;
						SetItem("DataSource", tRoot.Element("Server").Element("DataSource").Value);
						SetItem("UserName", CryptoUtility.DecryptByPassword(tRoot.Element("Server").Element("UserName").Value, Statics.strToUse, Statics.sToUse));
						SetItem("Password", CryptoUtility.DecryptByPassword(tRoot.Element("Server").Element("Password").Value, Statics.strToUse, Statics.sToUse));


						foreach (XElement tEnv in tRoot.Element("Envs").Elements("Env"))
						{
							tWgroup = new WorkstationGroup();
							tWgroup.GroupName = tEnv.Element("Name").Value;
							tWgroup.GlobalDataDirectory = tEnv.Element("DataDirectory").Value;
							Statics.WorkstationGroups.Add(tWgroup);
							tMega = new ConnectionMegaGroup();
							tMega.GroupName = tEnv.Element("Name").Value;
							tMega.NetworkLibrary = "";
							//Not sure what this is for
							tMega.DataSource = tRoot.Element("Server").Element("DataSource").Value;
							switch (tRoot.Element("Server").Element("ConnectionType").Value.ToLower())
							{
								case "sqlserver":
									tMega.ConnectionType = dbConnectionTypes.SQLServer;
									break;
								case "oledb":
									tMega.ConnectionType = dbConnectionTypes.OLEDB;
									break;
								case "odbc":
									tMega.ConnectionType = dbConnectionTypes.ODBC;
									break;
								default:
									tMega.ConnectionType = dbConnectionTypes.SQLServer;
									break;
							}
							if (tRoot.Element("Server").Element("UserName").Value != "")
							{
								tMega.Login = CryptoUtility.DecryptByPassword(tRoot.Element("Server").Element("UserName").Value, Statics.strToUse, Statics.sToUse);
							}
							if (tRoot.Element("Server").Element("Password").Value != "")
							{
								tMega.Password = CryptoUtility.DecryptByPassword(tRoot.Element("Server").Element("Password").Value, Statics.strToUse, Statics.sToUse);
							}
							tMega.AddLive();
							ConnectionPool.AddMegaGroup(tMega);
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
				return true;
			}
			catch (XmlException xex)
			{
				FCMessageBox.Show(xex.Message.ToString(), MsgBoxStyle.Critical, "Error");
				return false;
			}
			catch (Exception ex)
			{
				FCMessageBox.Show(ex.Message.ToString(), MsgBoxStyle.Critical, "Error");
				return false;
			}
		}

		public bool LoadArchives()
		{
			try
			{
				ConnectionPool tPool = new ConnectionPool();
				//ConnectionMegaGroup tMega = null;
				System.Data.Common.DbConnection tCon = null;
				System.Data.Common.DbConnection aCon = null;
				System.Data.Common.DbCommand tCmd = null;
				System.Data.Common.DbDataReader tRdr = null;
				System.Data.Common.DbDataReader aRdr = null;
				System.Data.Common.DbCommand aCmd = null;
				ConnectionGroup tGroup = null;
				string[] strAry;
				string strArchiveType = "";
				string strArchiveID = "";
				Dictionary<string, string> archivesDict = new Dictionary<string, string>();
				foreach (ConnectionMegaGroup tMegaItem in ConnectionPool.Statics.MegaGroups)
				{
					//open a connection to the live group and get the archive info
					ConnectionInfo tInfo = null;
					tInfo = tMegaItem.GetConnectionInformation("SystemSettings", "Live");
					if (tInfo != null)
					{
						if (tCon != null)
						{
							if (tCon.State == ConnectionState.Open)
							{
								tCon.Close();
							}
							tCon.Dispose();
						}
						if (aCon != null)
						{
							if (aCon.State == ConnectionState.Open)
							{
								aCon.Close();
							}
							aCon.Dispose();
						}
						tCon = tInfo.GetConnection();
						aCon = tInfo.GetConnection();
						if (tCon != null)
						{
							if (tCmd != null)
							{
								tCmd.Dispose();
							}
							tCmd = tCon.CreateCommand();
							tCmd.CommandText = "Select [name] from sys.databases order by [name]";
							// tCmd.CommandText = "Select * from Archives order by archivetype,archiveid"
							try
							{
								tCon.Open();
								tRdr = tCmd.ExecuteReader();
								archivesDict.Clear();
								while (tRdr.Read())
								{
									strAry = tRdr["Name"].ToString().Split(new char[] {
										'_'
									});
									if (strAry.Length > 4)
									{
										if (strAry[0].ToLower() == "trio")
										{
											if (strAry[1].ToLower() == tMegaItem.GroupName.ToLower())
											{
												if (strAry[2].ToLower() != "live")
												{
													strArchiveType = strAry[2].ToString();
													strArchiveID = strAry[3].ToString();
													if (!archivesDict.ContainsKey(strArchiveType + "_" + strArchiveID))
													{
														archivesDict.Add(strArchiveType + "_" + strArchiveID, strArchiveType + "_" + strArchiveID);
														tGroup = new ConnectionGroup();
														tGroup.GroupName = strArchiveType + "_" + strArchiveID;
														tGroup.ConnectionType = tMegaItem.ConnectionType;
														tGroup.DataSource = tMegaItem.DataSource;
														tGroup.Description = "";
														tGroup.GroupType = strArchiveType;
														tGroup.ID = FCConvert.ToInt32(Conversion.Val(strArchiveID));
														tGroup.Login = tMegaItem.Login;
														tGroup.NetworkLibrary = tMegaItem.NetworkLibrary;
														tGroup.Password = tMegaItem.Password;
														if (aCmd != null)
														{
															aCmd.Dispose();
														}
														aCmd = aCon.CreateCommand();
														aCmd.CommandText = "select * from archives where archivetype = '" + strArchiveType + "' and archiveid = " + strArchiveID;
														if (aCon.State != ConnectionState.Open)
														{
															aCon.Open();
														}
														aRdr = aCmd.ExecuteReader();
														if (aRdr.Read())
														{
															tGroup.Description = aRdr["Description"].ToString();
														}
														aRdr.Close();
														tMegaItem.AddGroup(tGroup);
													}
												}
											}
										}
									}
								}
								tRdr.Close();
								tCon.Close();
								aCon.Close();
							}
							catch (Exception tEx)
							{
							}
							tCmd.Dispose();
							tCon.Dispose();
							aCon.Dispose();
							tCon = null;
							aCon = null;
							tCmd = null;
							archivesDict.Clear();
						}
					}
				}
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public class StaticVariables
		{
			public string strMasterDirectory = "";
			public string strApplicationDirectory = "";
			public string strGlobalDataDirectory = "";
			public string strLocalDataDirectory = "";
			public string strBackupDirectory = "";
			public string strMachineName = "";
			public List<WorkstationGroup> WorkstationGroups = new List<WorkstationGroup>();
			public string strToUse = "B4A45E16-2828-4157-908E-F2266FA1810A";
			public string sToUse = "O72SajdzgdE=";
			public System.Collections.Generic.Dictionary<string, string> tItems = new System.Collections.Generic.Dictionary<string, string>();
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
