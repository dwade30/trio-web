﻿using fecherFoundation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Linq;

namespace TWSharedLibrary.Data
{
	public enum dbConnectionTypes : int
	{
		SQLServer = 0,
		ODBC = 1,
		OLEDB = 2
	}
	//Public Enum dbConnectionSubTypes As Integer
	//    MSACCESS = 0
	//    MSEXCEL = 1
	//End Enum
	//[ComClass(ConnectionPool.ClassId, ConnectionPool.InterfaceId, ConnectionPool.EventsId)]
	public class ConnectionPool
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "482CECB2-09C2-3303-8F40-332A3ED8157C";
		public const string InterfaceId = "2BD0137C-4832-43BA-B360-A62503DC212E";
		#endregion

		public const string EventsId = "C2722533-32CD-4D9B-8087-21E6DFF847F2";
		//Private Shared ConnectionsList As New LinkedList(Of ConnectionNode)
		//Private ListPointer As LinkedList(Of ConnectionNode).Enumerator
		// ''' <summary>
		// ''' Returns a connection object based on the connection string and the type of connection.
		// ''' It will create a connection if that connection string isn't found
		// ''' </summary>
		// ''' <param name="strConnection"></param>
		// ''' <returns>DbConnection object</returns>
		// ''' <remarks></remarks>
		//Public Function GetConnection(ByVal strConnection As String, ByVal ConnectionType As dbConnectionTypes, Optional ByVal strNickName As String = "DefaultDatabase") As DbConnection
		//    Dim tNode As ConnectionNode
		//    Dim tEnum As LinkedList(Of ConnectionNode).Enumerator
		//    Dim boolFound As Boolean
		//    tEnum = ConnectionsList.GetEnumerator
		//    boolFound = False
		//    tNode = Nothing
		//    Do While tEnum.MoveNext() And Not boolFound
		//        tNode = tEnum.Current
		//        If tNode.strName.ToString().ToLower()= strNickName.ToString().ToLower()Then
		//            boolFound = True
		//        End If
		//    Loop
		//    If boolFound Then
		//        'Return tNode.TheConnection
		//        Return tNode.GetConnection
		//    Else
		//        'try to create the connection
		//        tNode = New ConnectionNode
		//        tNode.strName = strNickName
		//        Select Case ConnectionType
		//            Case dbConnectionTypes.SQLServer
		//                'sql server
		//                Try
		//                    Dim tcon As New SqlConnection(strConnection)
		//                    'tNode.TheConnection = tcon
		//                    tNode.strName = strNickName
		//                    tNode.TypeOfConnection = ConnectionType
		//                    tNode.ConnectionString = strConnection
		//                    ConnectionsList.AddLast(tNode)
		//                Catch ex As Exception
		//                    //MsgBox("Unable to make connection " & strConnection, MsgBoxStyle.Critical, "Cannot Connect")
		//                End Try
		//            Case dbConnectionTypes.ODBC
		//                'odbc
		//                Try
		//                    Dim tcon As New Odbc.OdbcConnection(strConnection)
		//                    ' tNode.TheConnection = tcon
		//                    tNode.strName = strNickName
		//                    tNode.TypeOfConnection = ConnectionType
		//                    tNode.ConnectionString = strConnection
		//                    ConnectionsList.AddLast(tNode)
		//                Catch ex As Exception
		//                    //MsgBox("Unable to make connection " & strConnection, MsgBoxStyle.Critical, "Cannot Connect")
		//                End Try
		//            Case dbConnectionTypes.OLEDB
		//                'oledb
		//                Try
		//                    Dim tcon As New OleDb.OleDbConnection(strConnection)
		//                    'tNode.TheConnection = tcon
		//                    tNode.strName = strNickName
		//                    tNode.TypeOfConnection = ConnectionType
		//                    tNode.ConnectionString = strConnection
		//                    ConnectionsList.AddLast(tNode)
		//                Catch ex As Exception
		//                    //MsgBox("Unable to make connection " & strConnection, MsgBoxStyle.Critical, "Cannot Connect")
		//                End Try
		//        End Select
		//        'Return tNode.TheConnection
		//        Return tNode.GetConnection
		//    End If
		//End Function
		/// <summary>
		/// Returns a connection based on a connection nickname
		/// </summary>
		/// <param name="strConnectionName"></param>
		/// <returns>a DbConnection</returns>
		/// <remarks></remarks>
		public DbConnection GetConnectionByName(string strConnectionName, string strMegaGroup, string strGroup)
		{
			ConnectionInfo tInfo = null;
			tInfo = GetConnectionInformationByName(strConnectionName, strMegaGroup, strGroup);
			if (tInfo != null)
			{
				return tInfo.GetConnection();
			}
			else
			{
				return null;
			}
		}

		public string GetPrefix(string strMegaGroup, string strGroup)
		{
			if (strMegaGroup != "")
			{
				foreach (ConnectionMegaGroup tMega in Statics.MegaGroups)
				{
					if (tMega.GroupName.ToString().ToLower() == strMegaGroup.ToString().ToLower())
					{
						return tMega.GetPrefix(strGroup);
					}
				}
				return "";
			}
			else
			{
				return "";
			}
		}

		public ConnectionInfo GetConnectionInformationByName(string strConnectionName, string strMegaGroup, string strGroup)
		{
			if (strMegaGroup != "")
			{
				foreach (ConnectionMegaGroup tMega in Statics.MegaGroups)
				{
					if (tMega.GroupName.ToString().ToLower() == strMegaGroup.ToString().ToLower())
					{
						return tMega.GetConnectionInformation(strConnectionName, strGroup);
					}
				}
			}
			else
			{
				foreach (ConnectionInfo tInfo in Statics.ConnectionCollection)
				{
					if (tInfo.Name.ToString().ToLower() == strConnectionName.ToString().ToLower())
					{
						return tInfo;
					}
				}
			}
			return null;
		}

		public static void AddMegaGroup(ConnectionMegaGroup tMega)
		{
			Statics.MegaGroups.Add(tMega);
		}

		public static void AddConnectionInfo(ref ConnectionInfo tInfo)
		{
			Statics.ConnectionCollection.Add(tInfo);
		}

		public static void ClearSettings()
		{
			if (Statics.MegaGroups != null)
			{
				foreach (ConnectionMegaGroup tmega in Statics.MegaGroups)
				{
					tmega.ClearGroups();
				}
				Statics.MegaGroups.Clear();
			}
			if (Statics.ConnectionCollection != null)
			{
				Statics.ConnectionCollection.Clear();
			}
		}
		//Public Function GetConnectionInformationByName(ByVal strConnectionName As String) As ConnectionNode
		//    Dim tNode As ConnectionNode
		//    Dim tEnum As LinkedList(Of ConnectionNode).Enumerator
		//    Dim boolFound As Boolean
		//    tEnum = ConnectionsList.GetEnumerator
		//    boolFound = False
		//    tNode = Nothing
		//    Do While tEnum.MoveNext And Not boolFound
		//        tNode = tEnum.Current
		//        If UCase(tNode.ConnectionName) = UCase(strConnectionName) Then
		//            boolFound = True
		//        End If
		//    Loop
		//    If boolFound Then
		//        Return tNode
		//    Else
		//        Return Nothing
		//    End If
		//End Function
		public string GetConnectionString(string strConnectionName, string strConnectionGroup, string strConnectionMegaGroup)
		{
			ConnectionInfo tInfo = null;
			tInfo = GetConnectionInformation(strConnectionName, strConnectionGroup, strConnectionMegaGroup);
			if (tInfo != null)
			{
				return tInfo.GetConnectionString();
			}
			else
			{
				return "";
			}
		}

		public string[] GetConnectionNames(string strConnectionGroup, string strConnectionMegaGroup)
		{
			if (Statics.MegaGroups != null)
			{
				try
				{
					if (strConnectionMegaGroup != "")
					{
						ConnectionGroup tGroup = null;
						foreach (ConnectionMegaGroup tMega in Statics.MegaGroups)
						{
							if (tMega.GroupName.ToString().ToLower() == strConnectionMegaGroup.ToString().ToLower())
							{
								tGroup = tMega.GetGroup(strConnectionGroup);
								if (tGroup != null)
								{
									return tGroup.GetAvailableConnectionNames();
								}
								else
								{
									return new string[] {

									};
								}
							}
						}
						return new string[] {

						};
					}
					else
					{
						return new string[] {

						};
					}
				}
				catch (Exception ex)
				{
					return new string[] {

					};
				}
			}
			else
			{
				return new string[] {

				};
			}
		}

		public ConnectionInfo GetConnectionInformation(string strConnectionName, string strConnectionGroup, string strConnectionMegaGroup)
		{
			if (Statics.MegaGroups != null)
			{
				try
				{
					if (strConnectionMegaGroup != "")
					{
						foreach (ConnectionMegaGroup tMega in Statics.MegaGroups)
						{
							if (tMega.GroupName.ToString().ToLower() == strConnectionMegaGroup.ToString().ToLower())
							{
								return tMega.GetConnectionInformation(strConnectionName, strConnectionGroup);
							}
						}
						return null;
					}
					else
					{
						if (Statics.ConnectionCollection != null)
						{
							foreach (ConnectionInfo tInfo in Statics.ConnectionCollection)
							{
								if (tInfo.Name.ToString().ToLower() == strConnectionName.ToString().ToLower())
								{
									return tInfo;
								}
							}
							return null;
						}
						else
						{
							return null;
						}
					}
				}
				catch (Exception ex)
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		public bool AddConnection(string strConnection, dbConnectionTypes ConnectionType, string strNickName = "DefaultDatabase")
		{
			//, Optional SubConnectionType As dbConnectionSubTypes = dbConnectionSubTypes.MSACCESS) As Boolean
			//no group, no mega group
			if (strConnection == "")
				return false;
			ConnectionInfo theInfo = null;
			foreach (ConnectionInfo tInfo in Statics.ConnectionCollection)
			{
				if (tInfo.Name.ToString().ToLower() == strNickName.ToString().ToLower())
				{
					theInfo = tInfo;
					break;
				}
			}
			if (theInfo == null)
			{
				theInfo = new ConnectionInfo();
				Statics.ConnectionCollection.Add(theInfo);
			}
			switch (ConnectionType)
			{
				case dbConnectionTypes.SQLServer:
					SqlConnectionStringBuilder tb1 = new SqlConnectionStringBuilder(strConnection);
					theInfo.DataSource = tb1.DataSource;
					theInfo.GroupName = "";
					theInfo.InitialCatalog = tb1.InitialCatalog;
					theInfo.Name = strNickName;
					theInfo.NetworkLibrary = tb1.NetworkLibrary;
					theInfo.AdminUser = tb1.UserID;
					theInfo.AdminPass = tb1.Password;
					break;
				case dbConnectionTypes.ODBC:
					OdbcConnectionStringBuilder tb2 = new OdbcConnectionStringBuilder(strConnection);
					theInfo.DataSource = tb2["Dbq"].ToString();
					theInfo.Provider = tb2.Driver;
					theInfo.Name = strNickName;
					theInfo.ConnectionType = dbConnectionTypes.ODBC;
					// theInfo.ConnectionSubType = SubConnectionType
					//If SubConnectionType = dbConnectionSubTypes.MSACCESS Then
					try
					{
						theInfo.AdminPass = tb2["Pwd"].ToString();
					}
					catch (Exception ex)
					{
					}
					try
					{
						theInfo.AdminUser = tb2["Uid"].ToString();
					}
					catch (Exception ex)
					{
					}
					//ElseIf SubConnectionType = dbConnectionSubTypes.MSEXCEL Then
					//Try
					//    theInfo.DefaultDir = tb.Item("DefaultDir")
					//Catch ex As Exception
					//End Try
					//End If
					break;
				case dbConnectionTypes.OLEDB:
					OleDbConnectionStringBuilder tb = new OleDbConnectionStringBuilder(strConnection);
					theInfo.DataSource = tb.DataSource;
					theInfo.Provider = tb.Provider;
					theInfo.Name = strNickName;
					theInfo.ConnectionType = dbConnectionTypes.OLEDB;
					try
					{
						theInfo.AdminPass = tb["Password"].ToString();
					}
					catch (Exception ex)
					{
					}
					try
					{
						theInfo.AdminUser = tb["User Id"].ToString();
					}
					catch (Exception ex)
					{
					}
					break;
				default:
					return false;
			}
			return true;
		}

		public string DefaultMegaGroup
		{
			get
			{
				return Statics.strDefaultMegaGroup;
			}
			set
			{
				Statics.strDefaultMegaGroup = value;
			}
		}

		public string DefaultGroup
		{
			get
			{
				return Statics.strDefaultGroup;
			}
			set
			{
				Statics.strDefaultGroup = value;
			}
		}

		public ConnectionMegaGroup GetMegaGroup(string strMegaGroupName)
		{
			try
			{
				foreach (ConnectionMegaGroup tMega in Statics.MegaGroups)
				{
					if (tMega.GroupName.ToString().ToLower() == strMegaGroupName.ToString().ToLower())
					{
						return tMega;
					}
				}
				return null;
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public bool MegaGroupExists(string strGroupName)
		{
			if (Statics.MegaGroups != null)
			{
				foreach (ConnectionMegaGroup tMega in Statics.MegaGroups)
				{
					if (tMega.GroupName.ToString().ToLower() == strGroupName.ToString().ToLower())
					{
						return true;
					}
				}
				return false;
			}
			else
			{
				return false;
			}
		}

		public bool GroupExists(string strGroupName, string strMegaGroupName)
		{
			ConnectionMegaGroup tMega = null;
			tMega = GetMegaGroup(strMegaGroupName);
			if (tMega != null)
			{
				return tMega.GroupExists(strGroupName);
			}
			else
			{
				return false;
			}
		}
		//Public Function LoadConnections(ByVal strFileName As String) As Boolean
		//    If Not System.IO.File.Exists(strFileName) Then
		//        Return False
		//    End If
		//    Dim xmlDoc As New XmlDocument
		//    Dim boolFailed As Boolean = False
		//    Try
		//        Dim strReturn As String
		//        strReturn = LoadFileToString(strFileName)
		//        If strReturn.ToString().Trim() = "" Then
		//            Return False
		//        End If
		//        xmlDoc.LoadXml(strReturn)
		//        Dim xElement As XmlElement = Nothing
		//        Dim SetupElem As XmlElement = Nothing
		//        If xmlDoc.DocumentElement.Name.ToString().ToUpper = "CONNECTIONSETUP" Then
		//            SetupElem = xmlDoc.DocumentElement
		//            Return ParseConnectionSetup(SetupElem)
		//        Else
		//            For Each tElement As XmlElement In xmlDoc.DocumentElement
		//                If tElement.Name.ToString().ToUpper = "CONNECTIONSETUP" Then
		//                    SetupElem = tElement
		//                    If Not ParseConnectionSetup(SetupElem) Then
		//                        Return False
		//                    End If
		//                End If
		//            Next tElement
		//            Return True
		//        End If
		//        'shouldn't get here
		//        Return False
		//    Catch ex As Exception
		//        Return False
		//    Finally
		//    End Try
		//End Function
		public class StaticVariables
		{
			public System.Collections.ObjectModel.Collection<ConnectionMegaGroup> MegaGroups = new System.Collections.ObjectModel.Collection<ConnectionMegaGroup>();
			public System.Collections.ObjectModel.Collection<ConnectionInfo> ConnectionCollection = new System.Collections.ObjectModel.Collection<ConnectionInfo>();
			public string strDefaultMegaGroup = "";
			public string strDefaultGroup = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
	//[ComClass(ConnectionInfo.ClassId, ConnectionInfo.InterfaceId, ConnectionInfo.EventsId)]
	public class ConnectionInfo
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "4F83A5EF-22AF-3E23-ACA8-2D9537B6F096";
		public const string InterfaceId = "2A1D7928-3A23-4956-87D9-D66D33B1C68A";
		#endregion

		public const string EventsId = "456DC013-3B5B-4608-B713-AD299E03978E";
		private string strAlias = string.Empty;
		private string strDataSource = string.Empty;
		// Private strReportSource As String
		private string strInitialCatalog = string.Empty;
		private dbConnectionTypes ctConnectionType = dbConnectionTypes.SQLServer;
		//Private stConnectionSubType As dbConnectionSubTypes = dbConnectionSubTypes.MSACCESS
		private string strProvider = string.Empty;
		private string strAdditional = string.Empty;
		private string strAdminUser = string.Empty;
		private string strAdminPass = string.Empty;
		private string strGroupAlias = string.Empty;
		private string strMegaGroupAlias = "";
		private string strNetworkLibrary = string.Empty;
		private bool boolNoPrefix = false;

		public bool NoPrefix
		{
			get
			{
				return boolNoPrefix;
			}
			set
			{
				boolNoPrefix = value;
			}
		}

		public DbConnection GetConnection()
		{
			try
			{
				switch (ctConnectionType)
				{
					case dbConnectionTypes.SQLServer:
						SqlConnection tcon = new SqlConnection(GetConnectionString());
						return tcon;
					case dbConnectionTypes.ODBC:
						OdbcConnection tcon1 = new OdbcConnection(GetConnectionString());
						return tcon1;
					case dbConnectionTypes.OLEDB:
						OleDbConnection tcon2 = new OleDbConnection(GetConnectionString());
						return tcon2;
					default:
						return null;
				}
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public string GetConnectionString()
		{
			try
			{
				switch (ctConnectionType)
				{
					case dbConnectionTypes.SQLServer:
						SqlConnectionStringBuilder tCon = new SqlConnectionStringBuilder();
						tCon.DataSource = strDataSource;
						tCon.InitialCatalog = strInitialCatalog;
						if (strNetworkLibrary != "")
						{
							tCon.NetworkLibrary = strNetworkLibrary;
						}
						tCon.IntegratedSecurity = false;
						tCon.PersistSecurityInfo = true;
						tCon.Password = strAdminPass;
						tCon.UserID = strAdminUser;
						tCon.ColumnEncryptionSetting = SqlConnectionColumnEncryptionSetting.Enabled;
						return tCon.ConnectionString;
					case dbConnectionTypes.ODBC:
						OdbcConnectionStringBuilder tCon1 = new OdbcConnectionStringBuilder();
						tCon1.Driver = strProvider;
						tCon1["Dbq"] = strDataSource;
						// If stConnectionSubType = dbConnectionSubTypes.MSACCESS Then
						tCon1["Pwd"] = strAdminPass;
						tCon1["Uid"] = strAdminUser;
						//ElseIf stConnectionSubType = dbConnectionSubTypes.MSEXCEL Then
						//tCon["DefaultDir"] = DefaultDir
						//End If
						return tCon1.ConnectionString;
					case dbConnectionTypes.OLEDB:
						OleDbConnectionStringBuilder tCon2 = new OleDbConnectionStringBuilder();
						tCon2.DataSource = strDataSource;
						tCon2.Provider = strProvider;
						tCon2.PersistSecurityInfo = true;
						tCon2["Password"] = strAdminPass;
						tCon2["User Id"] = strAdminUser;
						return tCon2.ConnectionString;
				}
				return "";
			}
			catch (Exception ex)
			{
				return "";
			}
		}

		public string MegaGroupName
		{
			get
			{
				return strMegaGroupAlias;
			}
			set
			{
				strMegaGroupAlias = value;
			}
		}

		public string NetworkLibrary
		{
			get
			{
				return strNetworkLibrary;
			}
			set
			{
				strNetworkLibrary = value;
			}
		}

		public string Name
		{
			get
			{
				return strAlias;
			}
			set
			{
				strAlias = value;
			}
		}

		public string DataSource
		{
			get
			{
				return strDataSource;
			}
			set
			{
				strDataSource = value;
			}
		}

		public string ReportSource
		{
			get
			{
				string strReportSource = "";
				string[] strAry;
				if (strDataSource != "")
				{
					strAry = strDataSource.Split(new char[] {
						'\\'
					}, 2);
					strReportSource = strAry[0];
					if (strAry.Length > 1)
					{
						if (strAry[1].ToLower() != "localhost")
						{
							strReportSource = strReportSource + "/ReportServer_" + strAry[1];
						}
						else
						{
							strReportSource = strReportSource + "/ReportServer";
						}
					}
					else
					{
						strReportSource = strReportSource + "/ReportServer";
					}
				}
				return strReportSource;
			}
		}

		public string InitialCatalog
		{
			get
			{
				return strInitialCatalog;
			}
			set
			{
				strInitialCatalog = value;
			}
		}

		public dbConnectionTypes ConnectionType
		{
			get
			{
				return ctConnectionType;
			}
			set
			{
				ctConnectionType = value;
			}
		}
		//Public Property ConnectionSubType As dbConnectionSubTypes
		//    Get
		//        Return stConnectionSubType
		//    End Get
		//    Set(value As dbConnectionSubTypes)
		//        stConnectionSubType = value
		//    End Set
		//End Property
		public string Provider
		{
			get
			{
				return strProvider;
			}
			set
			{
				strProvider = value;
			}
		}

		public string AdditionalInfo
		{
			get
			{
				return strAdditional;
			}
			set
			{
				strAdditional = value;
			}
		}

		public string AdminUser
		{
			get
			{
				return strAdminUser;
			}
			set
			{
				strAdminUser = value;
			}
		}

		public string AdminPass
		{
			get
			{
				return strAdminPass;
			}
			set
			{
				strAdminPass = value;
			}
		}

		public string GroupName
		{
			get
			{
				return strGroupAlias;
			}
			set
			{
				strGroupAlias = value;
			}
		}
		// Public Property DefaultDir As String = ""
	}
	//[ComClass(ConnectionGroup.ClassId, ConnectionGroup.InterfaceId, ConnectionGroup.EventsId)]
	public class ConnectionGroup : IDisposable
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "8F2E7BF7-0BE5-3215-932E-B10B3A1982B8";
		public const string InterfaceId = "07ACC9B8-4FA0-4C39-9878-1E98098F56F5";
		#endregion

		public const string EventsId = "6C1C7D23-8396-4672-889D-ED59B3FE940B";
		private string strName = "";
		private string strDescription = "";
		private string strType = "";
		private int intID = 0;
		private System.Collections.ObjectModel.Collection<ConnectionInfo> ConnectionCollection = new System.Collections.ObjectModel.Collection<ConnectionInfo>();
		private string strPassword = "";
		private string strLogin = "";
		private dbConnectionTypes ctConnectionType = dbConnectionTypes.SQLServer;
		private string strDataSource = "";
		private string strNetWorkLibrary = "";
		//Private strReportSource As String = ""
		public string ReportSource
		{
			get
			{
				string strReportSource = "";
				string[] strAry;
				if (strDataSource != "")
				{
					strAry = strDataSource.Split(new char[] {
						'\\'
					}, 2);
					strReportSource = strAry[0];
					if (strAry.Length > 1)
					{
						if (strAry[1].ToLower() != "localhost")
						{
							strReportSource = strReportSource + "/ReportServer_" + strAry[1];
						}
						else
						{
							strReportSource = strReportSource + "/ReportServer";
						}
					}
					else
					{
						strReportSource = strReportSource + "/ReportServer";
					}
				}
				return strReportSource;
			}
		}

		public dbConnectionTypes ConnectionType
		{
			get
			{
				return ctConnectionType;
			}
			set
			{
				ctConnectionType = value;
			}
		}

		public string Password
		{
			get
			{
				return strPassword;
			}
			set
			{
				strPassword = value;
			}
		}

		public string Login
		{
			get
			{
				return strLogin;
			}
			set
			{
				strLogin = value;
			}
		}

		public string Description
		{
			get
			{
				return strDescription;
			}
			set
			{
				strDescription = value;
			}
		}

		public string PrefixString
		{
			// Return strType & "_" & intID
			get
			{
				return strName;
			}
		}

		public string DataSource
		{
			get
			{
				return strDataSource;
			}
			set
			{
				strDataSource = value;
			}
		}

		public string NetworkLibrary
		{
			get
			{
				return strNetWorkLibrary;
			}
			set
			{
				strNetWorkLibrary = value;
			}
		}

		public int ID
		{
			get
			{
				return intID;
			}
			set
			{
				intID = value;
			}
		}

		public string GroupType
		{
			get
			{
				return strType;
			}
			set
			{
				strType = value;
			}
		}

		public string GroupName
		{
			get
			{
				return strName;
			}
			set
			{
				strName = value;
			}
		}

		public ConnectionInfo GetConnectionInformation(string strConnectionName)
		{
			//If Not ConnectionCollection Is Nothing Then
			//    For Each tInfo As ConnectionInfo In ConnectionCollection
			//        If tInfo.Name.ToString().ToLower()= strConnectionName.ToString().ToLower()Then
			//            Return tInfo
			//        End If
			//    Next tInfo
			//    Return Nothing
			//Else
			//    Return Nothing
			//End If
			if (strConnectionName != null)
			{
				//ConnectionInfo tInfo = null;
				switch (strConnectionName.ToString().ToLower())
				{
					case "twbd0000.vb1":
						strConnectionName = "Budgetary";
						break;
					case "twar0000.vb1":
						strConnectionName = "AccountsReceivable";
						break;
					case "twre0000.vb1":
						strConnectionName = "RealEstate";
						break;
					case "twbl0000.vb1":
						strConnectionName = "TaxBilling";
						break;
					case "twpp0000.vb1":
						strConnectionName = "PersonalProperty";
						break;
					case "twce0000.vb1":
						strConnectionName = "CodeEnforcement";
						break;
					case "twck0000.vb1":
						strConnectionName = "Clerk";
						break;
					case "twcl0000.vb1":
						strConnectionName = "Collections";
						break;
					case "twgn0000.vb1":
						strConnectionName = "SystemSettings";
						break;
					case "twpy0000.vb1":
						strConnectionName = "Payroll";
						break;
					case "twxf0000.vb1":
						strConnectionName = "RealEstateTransfer";
						break;
					case "twmv0000.vb1":
						strConnectionName = "MotorVehicle";
						break;
					case "twrb0000.vb1":
						strConnectionName = "BlueBook";
						break;
					case "twut0000.vb1":
						strConnectionName = "UtilityBilling";
						break;
					case "twfa0000.vb1":
						strConnectionName = "FixedAssets";
						break;
					case "twe90000.vb1":
						strConnectionName = "E911";
						break;
					case "twcr0000.vb1":
						strConnectionName = "CashReceipts";
						break;
				}
				foreach (ConnectionInfo item in ConnectionCollection)
				{
					if (item.Name.ToString().ToLower() == strConnectionName)
					{
						return item;
					}
				}
				ConnectionInfo tInfo = new ConnectionInfo();
				if (strConnectionName.ToLower() != "bluebook")
				{
					tInfo.InitialCatalog = PrefixString + "_" + strConnectionName;
				}
				else
				{
					tInfo.InitialCatalog = "Live_" + strConnectionName;
				}
				tInfo.GroupName = strName;
				tInfo.NetworkLibrary = strNetWorkLibrary;
				tInfo.DataSource = strDataSource;
				tInfo.ConnectionType = ctConnectionType;
				tInfo.AdminUser = strLogin;
				tInfo.AdminPass = strPassword;
				//tInfo.ReportSource = strReportSource
				tInfo.NoPrefix = false;
				return tInfo;
			}
			else
			{
				return null;
			}
		}

		public string[] GetAvailableConnectionNames()
		{
			try
			{
				List<string> tColl = new List<string>();
				foreach (ConnectionInfo tInfo in ConnectionCollection)
				{
					tColl.Add(tInfo.Name);
				}
				return tColl.ToArray();
			}
			catch (Exception ex)
			{
				return new string[] {

				};
			}
		}

		public void AddConnectionInfo(ref ConnectionInfo tInfo)
		{
			ConnectionCollection.Add(tInfo);
		}

		public void ClearConnections()
		{
			if (ConnectionCollection != null)
			{
				ConnectionCollection.Clear();
			}
		}
		// To detect redundant calls
		private bool disposedValue = false;
		// IDisposable
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposedValue)
			{
				if (disposing)
				{
					// TODO: free other state (managed objects).
				}
				if (ConnectionCollection != null)
				{
					int x;
					for (x = ConnectionCollection.Count - 1; x >= 0; x += -1)
					{
						ConnectionCollection.RemoveAt(x);
					}
					ConnectionCollection = null;
				}
				// TODO: free your own state (unmanaged objects).
				// TODO: set large fields to null.
			}
			this.disposedValue = true;
		}

		#region " IDisposable Support "
		// This code added by Visual Basic to correctly implement the disposable pattern.
		void IDisposable.Dispose()
		{
			// Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion
	}
	//[ComClass(ConnectionMegaGroup.ClassId, ConnectionMegaGroup.InterfaceId, ConnectionMegaGroup.EventsId)]
	public class ConnectionMegaGroup : IDisposable
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "FC40C5B2-B475-3C23-A5FE-3C1E2D26BB25";
		public const string InterfaceId = "7DB18248-C841-46B3-96EC-F7C64F9EB309";
		#endregion

		public const string EventsId = "928EF434-6EC6-447C-8DF1-B7C26F5045D0";
		private string strName = "";
		private string strDataSource = "";
		private string strNetWorkLibrary = "";
		private System.Collections.ObjectModel.Collection<ConnectionGroup> ConnectionGroups = new System.Collections.ObjectModel.Collection<ConnectionGroup>();
		//Private ConnectionCollection As System.Collections.ObjectModel.Collection(Of ConnectionInfo)
		private string strPassword = "";
		private string strLogin = "";
		private dbConnectionTypes ctConnectionType = dbConnectionTypes.SQLServer;

		public string ReportSource
		{
			get
			{
				string strReportSource = "";
				string[] strAry;
				if (strDataSource != "")
				{
					strAry = strDataSource.Split(new char[] {
						'\\'
					}, 2);
					strReportSource = strAry[0];
					if (strAry.Length > 1)
					{
						if (strAry[1].ToLower() != "localhost")
						{
							strReportSource = strReportSource + "/ReportServer_" + strAry[1];
						}
						else
						{
							strReportSource = strReportSource + "/ReportServer";
						}
					}
					else
					{
						strReportSource = strReportSource + "/ReportServer";
					}
				}
				return strReportSource;
			}
		}

		public dbConnectionTypes ConnectionType
		{
			get
			{
				return ctConnectionType;
			}
			set
			{
				ctConnectionType = value;
			}
		}

		public string Password
		{
			get
			{
				return strPassword;
			}
			set
			{
				strPassword = value;
			}
		}

		public string Login
		{
			get
			{
				return strLogin;
			}
			set
			{
				strLogin = value;
			}
		}

		public string PrefixString
		{
			get
			{
				return "TRIO_" + strName;
			}
		}

		public string DataSource
		{
			get
			{
				return strDataSource;
			}
			set
			{
				strDataSource = value;
			}
		}

		public string NetworkLibrary
		{
			get
			{
				return strNetWorkLibrary;
			}
			set
			{
				strNetWorkLibrary = value;
			}
		}

		public string GroupName
		{
			get
			{
				return strName;
			}
			set
			{
				strName = value;
			}
		}

		public string GetPrefix(string strGroup)
		{
			string strReturn = "";
			if (strGroup != "")
			{
				foreach (ConnectionGroup tGroup in ConnectionGroups)
				{
					if (tGroup.ToString().ToLower() == strGroup.ToString().ToLower())
					{
						strReturn = tGroup.PrefixString;
						break;
					}
				}
				if (strReturn != "")
				{
					return PrefixString + "_" + strReturn;
				}
				else
				{
					return PrefixString;
				}
			}
			else
			{
				return PrefixString;
			}
		}

		public ConnectionInfo GetConnectionInformation(string strConnectionName, string strConnectionGroup)
		{
			if (ConnectionGroups != null)
			{
				try
				{
					if (strConnectionGroup != "")
					{
						if (strConnectionName.ToString().ToLower() == "systemsettings")
						{
							strConnectionGroup = "Live";
						}
						else if (strConnectionName.ToString().ToLower() == "bluebook" || strConnectionName.ToString().ToLower() == "twrb0000.vb1" || strConnectionName.ToString().ToLower() == "clientsettings")
						{
							strConnectionGroup = "Live";
						}
						foreach (ConnectionGroup tGroup in ConnectionGroups)
						{
							if (tGroup.GroupName.ToString().ToLower() == strConnectionGroup.ToString().ToLower())
							{
								ConnectionInfo tInfo = null;
								tInfo = tGroup.GetConnectionInformation(strConnectionName);
								if (tInfo != null)
								{
									if (!tInfo.NoPrefix)
									{
										if (strConnectionName.ToString().ToLower() != "bluebook" & strConnectionName.ToString().ToLower() != "clientsettings" & strConnectionName.ToString().ToLower() != "twrb0000.vb1")
										{
											tInfo.InitialCatalog = PrefixString + "_" + tInfo.InitialCatalog;
										}
										else
										{
											tInfo.InitialCatalog = "TRIO_All_" + tInfo.InitialCatalog;
										}
									}
									//tInfo.GroupName = strConnectionGroup
									tInfo.MegaGroupName = strName;
									//tInfo.NetworkLibrary = strNetWorkLibrary
									//tInfo.DataSource = strDataSource
									//tInfo.ConnectionType = ctConnectionType
									//tInfo.AdminUser = strLogin
									//tInfo.AdminPass = strPassword
								}
								return tInfo;
							}
						}
						return null;
					}
					else
					{
						return null;
					}
				}
				catch (Exception ex)
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		public void AddGroup(ConnectionGroup tGroup)
		{
			ConnectionGroups.Add(tGroup);
		}

		public void AddLive()
		{
			ConnectionGroup tGroup = null;
			foreach (ConnectionGroup tGroup1 in ConnectionGroups)
			{
				if (tGroup1.GroupName.ToString().ToLower() == "live")
					return;
			}
			tGroup = new ConnectionGroup();
			tGroup.ConnectionType = ctConnectionType;
			tGroup.DataSource = strDataSource;
			//tGroup.ReportSource = strReportSource
			tGroup.Description = "Live";
			tGroup.GroupName = "Live";
			tGroup.GroupType = "Live";
			tGroup.ID = 0;
			tGroup.Login = strLogin;
			tGroup.Password = strPassword;
			AddGroup(tGroup);
		}

		public ConnectionGroup GetGroup(string strGroupName)
		{
			try
			{
				foreach (ConnectionGroup tGroup in ConnectionGroups)
				{
					if (tGroup.GroupName.ToString().ToLower() == strGroupName.ToString().ToLower())
					{
						return tGroup;
					}
				}
				return null;
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public bool GroupExists(string strGroupName)
		{
			ConnectionGroup tGroup = null;
			tGroup = GetGroup(strGroupName);
			if (tGroup != null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public int GroupCount()
		{
			if (ConnectionGroups != null)
			{
				return ConnectionGroups.Count;
			}
			else
			{
				return 0;
			}
		}

		public ConnectionGroup AvailableGroups(int intIndex)
		{
			if (ConnectionGroups != null)
			{
				if (ConnectionGroups.Count > intIndex)
				{
					return ConnectionGroups[intIndex];
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		public void ClearGroups()
		{
			if (ConnectionGroups != null)
			{
				foreach (ConnectionGroup tGroup in ConnectionGroups)
				{
					tGroup.ClearConnections();
				}
				ConnectionGroups.Clear();
			}
		}

		public void ClearNonLiveGroups()
		{
			try
			{
				if (ConnectionGroups != null)
				{
					int x = 0;
					ConnectionGroup tGroup = null;
					for (x = ConnectionGroups.Count - 1; x >= 0; x += -1)
					{
						tGroup = ConnectionGroups[x];
						if (tGroup.GroupType.ToLower() != "live")
						{
							ConnectionGroups.Remove(tGroup);
						}
					}
				}
			}
			catch (Exception ex)
			{
			}
		}

		public string[] FindExistingDatabasesByShortName(string strGroup)
		{
			List<string> tReturn = new List<string>();
			try
			{
				string strSQL = "";
				strSQL = "select * from master.dbo.sysdatabases order by name";
				string strDatabase = "";
				ConnectionPool tPool = new ConnectionPool();
				string strdName;
				System.Data.Common.DbCommand tCmd = null;
				System.Data.Common.DbDataReader tRdr = null;
				ConnectionInfo tInfo = null;
				string[] strAry;
				char[] strSep = {
					'_'
				};
				string strLoadedGroup = "";
				string strLoadedMegaGroup = "";
				string strLoadedDB = "";
				string strLoadedPrefix = "";
				string strUnderScore = "";
				bool boolMatch = false;
				tInfo = tPool.GetConnectionInformation("SystemSettings", strGroup, strName);
				if (tInfo != null)
				{
					SqlConnectionStringBuilder tSB = new SqlConnectionStringBuilder(tInfo.GetConnectionString());
					tSB.InitialCatalog = "master";
					SqlConnection tcon = new SqlConnection(tSB.ConnectionString);
					tCmd = tcon.CreateCommand();
					tCmd.CommandText = strSQL;
					tcon.Open();
					tRdr = tCmd.ExecuteReader();
					while (tRdr.Read())
					{
						strdName = tRdr["Name"].ToString();
						strAry = strdName.Split(strSep);
						if (strAry.Length > 3)
						{
							if (strAry[0].ToLower() == "trio")
							{
								boolMatch = true;
								strLoadedDB = strAry[strAry.Length - 1];
								strLoadedMegaGroup = strAry[1];
								strUnderScore = "";
								int x;
								for (x = 2; x <= strAry.Length - 2; x++)
								{
									strLoadedGroup = strUnderScore + strAry[x];
									strUnderScore = "_";
								}
								strLoadedPrefix = strLoadedMegaGroup + strUnderScore + strLoadedGroup;
								if (strLoadedMegaGroup.ToLower() != strName.ToLower())
								{
									boolMatch = false;
								}
								if (strGroup != "")
								{
									if (strGroup.ToLower() != strLoadedGroup.ToLower() & strGroup.ToLower() != strAry[2].ToLower())
									{
										boolMatch = false;
									}
								}
								if (boolMatch)
								{
									tReturn.Add(strLoadedDB);
								}
							}
						}
					}
					tRdr.Close();
					tcon.Close();
					tRdr.Dispose();
					tCmd.Dispose();
					tcon.Dispose();
				}
			}
			catch (Exception ex)
			{
			}
			return tReturn.ToArray();
		}
		//Public Sub AddConnectionInfo(ByRef tInfo As ConnectionInfo)
		//    ConnectionCollection.Add(tInfo)
		//End Sub
		// To detect redundant calls
		private bool disposedValue = false;
		// IDisposable
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposedValue)
			{
				if (disposing)
				{
				}
				if (ConnectionGroups != null)
				{
					int x;
					for (x = ConnectionGroups.Count - 1; x >= 0; x += -1)
					{
						(ConnectionGroups[x] as IDisposable).Dispose();
						ConnectionGroups.RemoveAt(x);
					}
					ConnectionGroups = null;
				}
				//If Not ConnectionCollection Is Nothing Then
				//    Dim x As Integer
				//    For x = ConnectionCollection.Count - 1 To 0 Step -1
				//        ConnectionCollection.RemoveAt(x)
				//    Next x
				//    ConnectionCollection = Nothing
				//End If
			}
			this.disposedValue = true;
		}

		#region " IDisposable Support "
		// This code added by Visual Basic to correctly implement the disposable pattern.
		void IDisposable.Dispose()
		{
			// Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion
	}
}
