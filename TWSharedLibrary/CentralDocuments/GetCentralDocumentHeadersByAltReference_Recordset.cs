﻿using fecherFoundation;
using SharedApplication.CentralDocuments;
using System;
using System.Collections.ObjectModel;
using System.Web;
using SharedApplication.Extensions;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    public class GetHeadersByAltReference_Recordset : IGetCentralDocumentHeadersByAltReference
    {
        /// <summary>
        /// Gets document headers that match the parameters given
        /// </summary>
        /// <param name="DataGroup"></param>
        /// <param name="referenceType"></param>
        /// <param name="referenceId">** NOT USED **</param>
        /// <param name="altReference"></param>
        /// <returns></returns>
        public Collection<CentralDocumentHeader> GetByAltRef(string DataGroup, string referenceType, int referenceId, string altReference)
        {
            var retDocuments = new Collection<CentralDocumentHeader>();
            var rs = new clsDRWrapper();

            try
            {
                rs.OpenRecordset($"select id, len(itemdata) as ItemLength, AltReference, ItemDescription, ItemName, MediaType, ReferenceID, ReferenceType, ReferenceGroup, DocumentIdentifier, HashedValue "
                                 + $"from [Documents] "
                                 + $"where ReferenceGroup = '{DataGroup.HtmlEncode()}' and ReferenceType = '{referenceType.HtmlEncode()}' and AltReference  = '{altReference.HtmlEncode()}' "
                                 + $"order by ItemName", "CentralDocuments");

                while (!rs.EndOfFile())
                {
                    var cenDoc = new CentralDocumentHeader
                    {
                        ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID")),
                        AltReference = FCConvert.ToString(rs.Get_Fields_String("AltReference")),
                        ItemDescription = FCConvert.ToString(rs.Get_Fields_String("ItemDescription")),
                        ItemName = FCConvert.ToString(rs.Get_Fields_String("ItemName")),
                        MediaType = FCConvert.ToString(rs.Get_Fields_String("MediaType")),
                        ReferenceId = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("ReferenceID")))),
                        ReferenceType = FCConvert.ToString(rs.Get_Fields_String("ReferenceType")),
                        DataGroup = FCConvert.ToString(rs.Get_Fields_String("ReferenceGroup")),
                        ItemLength = FCConvert.ToInt32(rs.Get_Fields_Int32("ItemLength")),
                        DocumentIdentifier = rs.Get_Fields_Guid("DocumentIdentifier"),
                        HashedValue = rs.Get_Fields_Binary("HashedValue")
                    };
                    retDocuments.Add(cenDoc);
                    rs.MoveNext();
                }

                return retDocuments;
            }
            finally
            {
                rs.DisposeOf();
            }
        }
    }
}
