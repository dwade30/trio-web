﻿using fecherFoundation;
using SharedApplication.CentralDocuments;
using SharedDataAccess;
using System;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    public class GetCentralDocument_Recordset : IGetCentralDocument
    {
        public CentralDocument Get(int id)
        {
            if (id < 0) throw new ArgumentException("id must be greater than or equal to 0", nameof(id));

            CentralDocument retDocument = null;

           // DataConfigurator.LoadSQLConfig();

            var rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                var cenDoc = new CentralDocument();
                rsLoad.OpenRecordset($"select * from [Documents] where id = {FCConvert.ToString(id)}", "CentralDocuments");

                if (!rsLoad.EndOfFile())
                {
                    cenDoc = new CentralDocument
                    {
                        ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID")),
                        AltReference = FCConvert.ToString(rsLoad.Get_Fields_String("AltReference")),
                        ItemDescription = FCConvert.ToString(rsLoad.Get_Fields_String("ItemDescription")),
                        ItemName = FCConvert.ToString(rsLoad.Get_Fields_String("ItemName")),
                        MediaType = FCConvert.ToString(rsLoad.Get_Fields_String("MediaType")),
                        ReferenceId = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ReferenceID")))),
                        ReferenceType = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceType")),
                        DataGroup = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceGroup")),
                        ItemData = rsLoad.Get_Fields_Binary("ItemData"),
                        IsUpdated = false,
                        DocumentIdentifier = rsLoad.Get_Fields_Guid("DocumentIdentifier"),
                        DateCreated =  rsLoad.Get_Fields_DateTime("DateCreated")
                    };
                }

                retDocument = cenDoc;

                return retDocument;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                throw;
            }
            finally
            {
                rsLoad.DisposeOf();
            }
        }
        public CentralDocument GetCentralDocument(int id, string dataGroup)
        {
            if (id < 0) throw new ArgumentException("id must be greater than or equal to 0", nameof(id));
            if (string.IsNullOrEmpty(dataGroup)) throw new ArgumentException("dataGroup must be specified", nameof(dataGroup));

            CentralDocument retDocument = null;

            Variables.Statics.DataGroup = dataGroup;
            DataConfigurator.LoadSQLConfig();

            var rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                var cenDoc = new CentralDocument();
                rsLoad.OpenRecordset($"select * from [Documents] where id = {FCConvert.ToString(id)}", "CentralDocuments");

                if (!rsLoad.EndOfFile())
                {
                    cenDoc = new CentralDocument
                    {
                        ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID")),
                        AltReference = FCConvert.ToString(rsLoad.Get_Fields_String("AltReference")),
                        ItemDescription = FCConvert.ToString(rsLoad.Get_Fields_String("ItemDescription")),
                        ItemName = FCConvert.ToString(rsLoad.Get_Fields_String("ItemName")),
                        MediaType = FCConvert.ToString(rsLoad.Get_Fields_String("MediaType")),
                        ReferenceId = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ReferenceID")))),
                        ReferenceType = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceType")),
                        DataGroup = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceGroup")),
                        ItemData = rsLoad.Get_Fields_Binary("ItemData"),
                        DocumentIdentifier = rsLoad.Get_Fields_Guid("DocumentIdentifier"),
                        DateCreated = rsLoad.Get_Fields_DateTime("DateCreated"),
                        IsUpdated = false
                    };
                }

                retDocument = cenDoc;

                return retDocument;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                throw;
            }
            finally
            {
                rsLoad.DisposeOf();
            }
        }
    }
}
