﻿using System;
using fecherFoundation;
using SharedApplication.CentralDocuments;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    public class DeleteCentralDocument_Recordset : IDeleteCentralDocument
    {
        public void Delete(int lngID)
        {
            //ClearErrors();
            var rs = new clsDRWrapper();

            try
            {
                rs.Execute($"Delete from [Documents] where id = {FCConvert.ToString(lngID)}", "CentralDocuments");
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                throw;
            }
            finally
            {
               rs.DisposeOf(); 
            }
        }

    }
}
