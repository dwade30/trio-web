﻿using fecherFoundation;
using System;
using SharedApplication.CentralDocuments.Interfaces;
using SharedApplication.Extensions;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    public class DeleteCentralDocumentsByReference_Recordset : IDeleteCentralDocumentsByReference
    {
        public void DeleteByReference(string strDataGroup, string strReferenceType, int lngReferenceID)
        {
            clsDRWrapper rs = new clsDRWrapper();

            try
            {
                rs.Execute($"Delete from [Documents] "
                           + $"where ReferenceGroup = '{strDataGroup.HtmlEncode()}' and ReferenceType = '{strReferenceType.HtmlEncode()}' and ReferenceId = {FCConvert.ToString(lngReferenceID)}",
                           "CentralDocuments");
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                throw;
            }
            finally
            {
                rs.DisposeOf();
            }
        }
    }
}
