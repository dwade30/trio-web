﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using Global;
using fecherFoundation;
using System.IO;

namespace TWSharedLibrary
{
	/// <summary>
	/// Summary description for frmDocumentViewer.
	/// </summary>
	partial class frmDocumentViewer : SharedBaseForm
	{
		//public AxSCRIBBLELib.AxImageViewer ImageViewer1;
		public FCViewerPanel ImageViewer1;
		public FCGrid GridList;
		public fecherFoundation.FCLabel lblDescription;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileAddDocument;
		public fecherFoundation.FCToolStripMenuItem mnuFileDeleteDocument;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileZoom;
		public fecherFoundation.FCToolStripMenuItem mnuFileZoomZoomIn;
		public fecherFoundation.FCToolStripMenuItem mnuFileZoomZoomOut;
		public fecherFoundation.FCToolStripMenuItem mnuFileZoomFitToWindow;
		public fecherFoundation.FCToolStripMenuItem mnuFileZoomAspectRatio;
		public fecherFoundation.FCToolStripMenuItem mnuFileRotate;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDocumentViewer));
            this.ImageViewer1 = new fecherFoundation.FCViewerPanel();
            this.GridList = new fecherFoundation.FCGrid();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileAddDocument = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileDeleteDocument = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileZoom = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileZoomZoomIn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileZoomZoomOut = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileZoomFitToWindow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileZoomAspectRatio = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileRotate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.rotation1 = new Wisej.Web.Rotation(this.components);
            this.cmdFileRotate = new fecherFoundation.FCButton();
            this.cmdFileZoomFitToWindow = new fecherFoundation.FCButton();
            this.cmdFileZoomZoomOut = new fecherFoundation.FCButton();
            this.cmdFileZoomZoomIn = new fecherFoundation.FCButton();
            this.cmdFileDeleteDocument = new fecherFoundation.FCButton();
            this.cmdFileAddDocument = new fecherFoundation.FCButton();
            this.cmdFilePrint = new fecherFoundation.FCButton();
            this.cmdAspectRatio = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileRotate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomFitToWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomZoomOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomZoomIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileDeleteDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileAddDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAspectRatio)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFilePrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 637);
            this.BottomPanel.Size = new System.Drawing.Size(1089, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.ImageViewer1);
            this.ClientArea.Controls.Add(this.GridList);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(1109, 662);
            this.ClientArea.Controls.SetChildIndex(this.lblDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridList, 0);
            this.ClientArea.Controls.SetChildIndex(this.ImageViewer1, 0);
            //this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAspectRatio);
            this.TopPanel.Controls.Add(this.cmdFileAddDocument);
            this.TopPanel.Controls.Add(this.cmdFileDeleteDocument);
            this.TopPanel.Controls.Add(this.cmdFileRotate);
            this.TopPanel.Controls.Add(this.cmdFileZoomFitToWindow);
            this.TopPanel.Controls.Add(this.cmdFileZoomZoomIn);
            this.TopPanel.Controls.Add(this.cmdFileZoomZoomOut);
            this.TopPanel.Size = new System.Drawing.Size(1109, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileZoomZoomOut, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileZoomZoomIn, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileZoomFitToWindow, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileRotate, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileDeleteDocument, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileAddDocument, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAspectRatio, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(301, 30);
            this.HeaderText.Text = "View Attached Documents";
            // 
            // ImageViewer1
            // 
            this.ImageViewer1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.ImageViewer1.BorderStyle = Wisej.Web.BorderStyle.Solid;
            this.ImageViewer1.Location = new System.Drawing.Point(539, 81);
            this.ImageViewer1.Name = "ImageViewer1";
            this.ImageViewer1.Size = new System.Drawing.Size(552, 556);
            // 
            // GridList
            // 
            this.GridList.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.GridList.Location = new System.Drawing.Point(30, 30);
            this.GridList.Name = "GridList";
            this.GridList.Size = new System.Drawing.Size(491, 607);
            this.GridList.CurrentCellChanged += new System.EventHandler(this.GridList_RowColChange);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(539, 30);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(552, 30);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcess});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = 0;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileAddDocument,
            this.mnuFileDeleteDocument,
            this.mnuSeperator2,
            this.mnuFileZoom,
            this.mnuFileRotate,
            this.mnuSeperator,
            this.mnuFilePrint,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileAddDocument
            // 
            this.mnuFileAddDocument.Index = 0;
            this.mnuFileAddDocument.Name = "mnuFileAddDocument";
            this.mnuFileAddDocument.Shortcut = Wisej.Web.Shortcut.CtrlA;
            this.mnuFileAddDocument.Text = "Add Document";
            this.mnuFileAddDocument.Click += new System.EventHandler(this.mnuFileAddDocument_Click);
            // 
            // mnuFileDeleteDocument
            // 
            this.mnuFileDeleteDocument.Index = 1;
            this.mnuFileDeleteDocument.Name = "mnuFileDeleteDocument";
            this.mnuFileDeleteDocument.Shortcut = Wisej.Web.Shortcut.CtrlR;
            this.mnuFileDeleteDocument.Text = "Remove Document";
            this.mnuFileDeleteDocument.Click += new System.EventHandler(this.mnuFileDeleteDocument_Click);
            // 
            // mnuSeperator2
            // 
            this.mnuSeperator2.Index = 2;
            this.mnuSeperator2.Name = "mnuSeperator2";
            this.mnuSeperator2.Text = "-";
            // 
            // mnuFileZoom
            // 
            this.mnuFileZoom.Index = 3;
            this.mnuFileZoom.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileZoomZoomIn,
            this.mnuFileZoomZoomOut,
            this.mnuFileZoomFitToWindow,
            this.mnuFileZoomAspectRatio});
            this.mnuFileZoom.Name = "mnuFileZoom";
            this.mnuFileZoom.Text = "Zoom";
            // 
            // mnuFileZoomZoomIn
            // 
            this.mnuFileZoomZoomIn.Index = 0;
            this.mnuFileZoomZoomIn.Name = "mnuFileZoomZoomIn";
            this.mnuFileZoomZoomIn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuFileZoomZoomIn.Text = "Zoom In";
            this.mnuFileZoomZoomIn.Click += new System.EventHandler(this.mnuFileZoomZoomIn_Click);
            // 
            // mnuFileZoomZoomOut
            // 
            this.mnuFileZoomZoomOut.Index = 1;
            this.mnuFileZoomZoomOut.Name = "mnuFileZoomZoomOut";
            this.mnuFileZoomZoomOut.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuFileZoomZoomOut.Text = "Zoom Out";
            this.mnuFileZoomZoomOut.Click += new System.EventHandler(this.mnuFileZoomZoomOut_Click);
            // 
            // mnuFileZoomFitToWindow
            // 
            this.mnuFileZoomFitToWindow.Index = 2;
            this.mnuFileZoomFitToWindow.Name = "mnuFileZoomFitToWindow";
            this.mnuFileZoomFitToWindow.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuFileZoomFitToWindow.Text = "Fit To Window";
            this.mnuFileZoomFitToWindow.Click += new System.EventHandler(this.mnuFileZoomFitToWindow_Click);
            // 
            // mnuFileZoomAspectRatio
            // 
            this.mnuFileZoomAspectRatio.Index = 3;
            this.mnuFileZoomAspectRatio.Name = "mnuFileZoomAspectRatio";
            this.mnuFileZoomAspectRatio.Text = "Aspect Ratio";
            this.mnuFileZoomAspectRatio.Click += new System.EventHandler(this.mnuFileZoomAspectRatio_Click);
            // 
            // mnuFileRotate
            // 
            this.mnuFileRotate.Index = 4;
            this.mnuFileRotate.Name = "mnuFileRotate";
            this.mnuFileRotate.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuFileRotate.Text = "Rotate";
            this.mnuFileRotate.Click += new System.EventHandler(this.mnuFileRotate_Click);
            // 
            // mnuSeperator
            // 
            this.mnuSeperator.Index = 5;
            this.mnuSeperator.Name = "mnuSeperator";
            this.mnuSeperator.Text = "-";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 6;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 7;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 8;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdFileRotate
            // 
            this.cmdFileRotate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileRotate.Location = new System.Drawing.Point(539, 29);
            this.cmdFileRotate.Name = "cmdFileRotate";
            this.cmdFileRotate.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdFileRotate.Size = new System.Drawing.Size(55, 24);
            this.cmdFileRotate.TabIndex = 14;
            this.cmdFileRotate.Text = "Rotate";
            this.cmdFileRotate.Click += new System.EventHandler(this.mnuFileRotate_Click);
            // 
            // cmdFileZoomFitToWindow
            // 
            this.cmdFileZoomFitToWindow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileZoomFitToWindow.Location = new System.Drawing.Point(600, 29);
            this.cmdFileZoomFitToWindow.Name = "cmdFileZoomFitToWindow";
            this.cmdFileZoomFitToWindow.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdFileZoomFitToWindow.Size = new System.Drawing.Size(100, 24);
            this.cmdFileZoomFitToWindow.TabIndex = 13;
            this.cmdFileZoomFitToWindow.Text = "Fit To Window";
            this.cmdFileZoomFitToWindow.Visible = false;
            this.cmdFileZoomFitToWindow.Click += new System.EventHandler(this.mnuFileZoomFitToWindow_Click);
            // 
            // cmdFileZoomZoomOut
            // 
            this.cmdFileZoomZoomOut.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileZoomZoomOut.Location = new System.Drawing.Point(706, 29);
            this.cmdFileZoomZoomOut.Name = "cmdFileZoomZoomOut";
            this.cmdFileZoomZoomOut.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdFileZoomZoomOut.Size = new System.Drawing.Size(74, 24);
            this.cmdFileZoomZoomOut.TabIndex = 12;
            this.cmdFileZoomZoomOut.Text = "Zoom Out";
            this.cmdFileZoomZoomOut.Visible = false;
            this.cmdFileZoomZoomOut.Click += new System.EventHandler(this.mnuFileZoomZoomOut_Click);
            // 
            // cmdFileZoomZoomIn
            // 
            this.cmdFileZoomZoomIn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileZoomZoomIn.Location = new System.Drawing.Point(786, 29);
            this.cmdFileZoomZoomIn.Name = "cmdFileZoomZoomIn";
            this.cmdFileZoomZoomIn.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdFileZoomZoomIn.Size = new System.Drawing.Size(61, 24);
            this.cmdFileZoomZoomIn.TabIndex = 11;
            this.cmdFileZoomZoomIn.Text = "Zoom In";
            this.cmdFileZoomZoomIn.Visible = false;
            this.cmdFileZoomZoomIn.Click += new System.EventHandler(this.mnuFileZoomZoomIn_Click);
            // 
            // cmdFileDeleteDocument
            // 
            this.cmdFileDeleteDocument.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileDeleteDocument.Location = new System.Drawing.Point(853, 29);
            this.cmdFileDeleteDocument.Name = "cmdFileDeleteDocument";
            this.cmdFileDeleteDocument.Shortcut = Wisej.Web.Shortcut.CtrlR;
            this.cmdFileDeleteDocument.Size = new System.Drawing.Size(127, 24);
            this.cmdFileDeleteDocument.TabIndex = 10;
            this.cmdFileDeleteDocument.Text = "Remove Document";
            this.cmdFileDeleteDocument.Click += new System.EventHandler(this.mnuFileDeleteDocument_Click);
            // 
            // cmdFileAddDocument
            // 
            this.cmdFileAddDocument.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileAddDocument.Location = new System.Drawing.Point(986, 29);
            this.cmdFileAddDocument.Name = "cmdFileAddDocument";
            this.cmdFileAddDocument.Shortcut = Wisej.Web.Shortcut.CtrlA;
            this.cmdFileAddDocument.Size = new System.Drawing.Size(107, 24);
            this.cmdFileAddDocument.TabIndex = 9;
            this.cmdFileAddDocument.Text = "Add Document";
            this.cmdFileAddDocument.Click += new System.EventHandler(this.mnuFileAddDocument_Click);
            // 
            // cmdFilePrint
            // 
            this.cmdFilePrint.AppearanceKey = "acceptButton";
            this.cmdFilePrint.Location = new System.Drawing.Point(342, 30);
            this.cmdFilePrint.Name = "cmdFilePrint";
            this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFilePrint.Size = new System.Drawing.Size(109, 48);
            this.cmdFilePrint.TabIndex = 9;
            this.cmdFilePrint.Text = "Print";
            this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // cmdAspectRatio
            // 
            this.cmdAspectRatio.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAspectRatio.Location = new System.Drawing.Point(447, 29);
            this.cmdAspectRatio.Name = "cmdAspectRatio";
            this.cmdAspectRatio.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdAspectRatio.Size = new System.Drawing.Size(86, 24);
            this.cmdAspectRatio.TabIndex = 15;
            this.cmdAspectRatio.Text = "Aspect Ratio";
            this.cmdAspectRatio.Click += new System.EventHandler(this.mnuFileZoomAspectRatio_Click);
            // 
            // frmDocumentViewer
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1109, 722);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmDocumentViewer";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "View Attached Documents";
           
            this.Resize += new System.EventHandler(this.frmDocumentViewer_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDocumentViewer_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileRotate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomFitToWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomZoomOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileZoomZoomIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileDeleteDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileAddDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAspectRatio)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Rotation rotation1;
		private FCButton cmdFileRotate;
		private FCButton cmdFileZoomFitToWindow;
		private FCButton cmdFileZoomZoomOut;
		private FCButton cmdFileZoomZoomIn;
		private FCButton cmdFileDeleteDocument;
		private FCButton cmdFileAddDocument;
		private FCButton cmdFilePrint;
		private FCButton cmdAspectRatio;
	}
}
