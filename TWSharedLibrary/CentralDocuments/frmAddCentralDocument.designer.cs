﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using System.Drawing;
using fecherFoundation;
using Global;

namespace TWSharedLibrary
{
	/// <summary>
	/// Summary description for frmAddCentralDocument.
	/// </summary>
	partial class frmAddCentralDocument : SharedBaseForm
	{
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel lblFile;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddCentralDocument));
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.lblFile = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.panel1 = new fecherFoundation.FCViewerPanel();
            this.upload1 = new Wisej.Web.Upload();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 423);
            this.BottomPanel.Size = new System.Drawing.Size(422, 98);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.upload1);
            this.ClientArea.Controls.Add(this.panel1);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.lblFile);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.BottomPanel);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(442, 526);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblFile, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.panel1, 0);
            this.ClientArea.Controls.SetChildIndex(this.upload1, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(442, 60);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(20, 65);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(387, 40);
            this.txtDescription.TabIndex = 10;
            // 
            // lblFile
            // 
            this.lblFile.Location = new System.Drawing.Point(20, 125);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(387, 30);
            this.lblFile.TabIndex = 4;
            this.lblFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(387, 15);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "DOCUMENT DESCRIPTION";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcess});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = 0;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // panel1
            // 
            this.panel1.AllowDrop = true;
            this.panel1.BackColor = System.Drawing.Color.FromName("@control");
            this.panel1.BorderStyle = Wisej.Web.BorderStyle.Solid;
            this.panel1.Location = new System.Drawing.Point(20, 175);
            this.panel1.Name = "panel1";
            this.panel1.ShowCloseButton = false;
            this.panel1.Size = new System.Drawing.Size(387, 188);
            this.panel1.TabIndex = 7;
            this.panel1.DragDrop += new Wisej.Web.DragEventHandler(this.panel1_DragDrop);
            // 
            // upload1
            // 
            this.upload1.AllowedFileTypes = "pdf|image.*";
            this.upload1.ButtonPosition = System.Drawing.ContentAlignment.MiddleCenter;
            this.upload1.HideValue = true;
            this.upload1.Location = new System.Drawing.Point(20, 383);
            this.upload1.Name = "upload1";
            this.upload1.Size = new System.Drawing.Size(100, 40);
            this.upload1.TabIndex = 8;
            this.upload1.Text = "Browse";
            this.upload1.Uploaded += new Wisej.Web.UploadedEventHandler(this.upload1_Uploaded);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(152, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(125, 48);
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmAddCentralDocument
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(442, 595);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmAddCentralDocument";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Add Document";
            this.Load += new System.EventHandler(this.frmAddCentralDocument_Load);
            this.Activated += new System.EventHandler(this.frmAddCentralDocument_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAddCentralDocument_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCViewerPanel panel1;
		private System.ComponentModel.IContainer components;
		private Upload upload1;
		private FCButton cmdProcess;
    }
}
