﻿using System;
using SharedApplication.CentralDocuments;
using SharedApplication.Messaging;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    public class GetCentralDocumentByIdentifierHandler : CommandHandler<GetCentralDocumentByIdentifier, CentralDocument>
    {
        protected override CentralDocument Handle(GetCentralDocumentByIdentifier command)
        {
            if (command.DocumentIdentifier == Guid.Empty)
            {
                return null;
            }

            var rsLoad = new clsDRWrapper();
            try
            {
                var cenDoc = new CentralDocument();
                rsLoad.OpenRecordset("select * from [Documents] where DocumentIdentifier = '" + command.DocumentIdentifier.ToString() + "'" , "CentralDocuments");

                if (!rsLoad.EndOfFile())
                {
                    cenDoc = new CentralDocument
                    {
                        ID = rsLoad.Get_Fields_Int32("ID"),
                        AltReference = rsLoad.Get_Fields_String("AltReference"),
                        ItemDescription = rsLoad.Get_Fields_String("ItemDescription"),
                        ItemName = rsLoad.Get_Fields_String("ItemName"),
                        MediaType = rsLoad.Get_Fields_String("MediaType"),
                        ReferenceId = rsLoad.Get_Fields_Int32("ReferenceID"),
                        ReferenceType = rsLoad.Get_Fields_String("ReferenceType"),
                        DataGroup = rsLoad.Get_Fields_String("ReferenceGroup"),
                        ItemData = rsLoad.Get_Fields_Binary("ItemData"),
                        IsUpdated = false,
                        DocumentIdentifier = rsLoad.Get_Fields_Guid("DocumentIdentifier"),
                        DateCreated = rsLoad.Get_Fields_DateTime("DateCreated")
                    };
                }
                return cenDoc;
            }
            catch (Exception e)
            {

                throw;
            }
            finally
            {
                rsLoad.DisposeOf();
            }
        }
    }
}

