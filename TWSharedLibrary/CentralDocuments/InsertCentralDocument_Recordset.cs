﻿using System.Data;
using fecherFoundation;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralDocuments.Interfaces;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    public class InsertCentralDocument_Recordset : IInsertCentralDocument
    {
        public void Insert(CentralDocument doc)
        {
            var rsSave = new clsDRWrapper();

            try
            {
                rsSave.SetStoredProcedure("Insert_Document", "CentralDocuments");
                rsSave.Set_ParameterValue("@ID", 0);
                rsSave.Set_ParameterValue("@AltReference", doc.AltReference);
                rsSave.Set_ParameterValue("@ItemDescription", doc.ItemDescription);
                rsSave.Set_ParameterValue("@ItemName", doc.ItemName);
                rsSave.Set_ParameterValue("@MediaType", doc.MediaType);
                rsSave.Set_ParameterValue("@ReferenceId", doc.ReferenceId);
                rsSave.Set_ParameterValue("@DataGroup", doc.DataGroup);
                rsSave.Set_ParameterValue("@ReferenceType", doc.ReferenceType);
                rsSave.Set_ParameterValue("@ItemData", doc.ItemData);
                rsSave.Set_ParameterValue("@DocumentIdentifier",doc.DocumentIdentifier);
                rsSave.Set_ParameterValue("@DateCreated",doc.DateCreated);
                rsSave.ExecuteStoredProcedure(false);
                doc.ID = FCConvert.ToInt32(rsSave.Get_ParameterValue("@ID"));
                if (doc.ID < 1) throw new DataException($"Failed to add the document. {rsSave.ErrorDescription}");
                doc.IsUpdated = false;
            }
            finally
            {
                rsSave.DisposeOf();
            }
        }
    }
}
