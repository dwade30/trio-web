﻿using fecherFoundation;
using SharedApplication.CentralDocuments;
using System;
using System.Collections.ObjectModel;
using SharedApplication.Extensions;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    class GetCentralDocumentHeadersByReference_Recordset : IGetCentralDocumentHeadersByReference
    {
        public Collection<CentralDocumentHeader> GetByRef(string strDataGroup, string referenceType, int lngReferenceID)
        {
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                var gColl = new Collection<CentralDocumentHeader>();
                CentralDocumentHeader cenDoc;
                rsLoad.OpenRecordset($"select id, len(itemdata) as ItemLength, AltReference, ItemDescription, ItemName, MediaType, ReferenceID, ReferenceType, ReferenceGroup, DocumentIdentifier,HashedValue "
                                     + $"from [Documents] where ReferenceGroup = '{strDataGroup.HtmlEncode()}' and ReferenceType = '{referenceType}' and ReferenceId = {FCConvert.ToString(lngReferenceID)} "
                                     + $"order by ItemName", "CentralDocuments");

                while (!rsLoad.EndOfFile())
                {
                    cenDoc = new CentralDocumentHeader
                    {
                        ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID")),
                        AltReference = FCConvert.ToString(rsLoad.Get_Fields_String("AltReference")),
                        ItemDescription = FCConvert.ToString(rsLoad.Get_Fields_String("ItemDescription")),
                        ItemName = FCConvert.ToString(rsLoad.Get_Fields_String("ItemName")),
                        MediaType = FCConvert.ToString(rsLoad.Get_Fields_String("MediaType")),
                        ReferenceId = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ReferenceID")))),
                        ReferenceType = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceType")),
                        DataGroup = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceGroup")),
                        ItemLength = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ItemLength")),
                        DocumentIdentifier = rsLoad.Get_Fields_Guid("DocumentIdentifier"),
                        HashedValue = rsLoad.Get_Fields_Binary("HashedValue")
                    };
                    gColl.Add(cenDoc);
                    rsLoad.MoveNext();
                }

                return gColl;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                throw;
            }
            finally
            {
                rsLoad.DisposeOf();
            }
        }

    }
}
