﻿using System;
using System.Data;
using System.IO;
using fecherFoundation;
using SharedApplication.CentralDocuments;
using SharedApplication.Messaging;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    public class CreateCentralDocumentFromFilenameHandler : CommandHandler<CreateCentralDocumentFromFilename,int>
    {
        protected override int Handle(CreateCentralDocumentFromFilename command)
        {
           // CentralDocument cenDoc;

            if (!string.IsNullOrEmpty(command.Filename))
            {
               
                string strTemp = "";

                strTemp = Path.GetFileName(command.Filename);
                var itemName = strTemp;

                strTemp = Path.GetExtension(command.Filename).TrimStart('.');
                var extension = strTemp.ToLower();
                string mediaType = "";
                switch (extension)
                {
                    case "pdf":
                        mediaType = "application/pdf";

                        break;
                    case "jpg":
                    case "jpeg":
                        mediaType = "image/jpeg";

                        break;
                    case "bmp":
                        mediaType = "image/bmp";

                        break;
                    case "png":
                        mediaType = "image/png";

                        break;
                    case "gif":
                        mediaType = "image/gif";

                        break;
                    case "tif":
                    case "tiff":
                        mediaType = "image/tif";

                        break;
                    default:
                        throw new ApplicationException("Unsupported file extension. Must be an image file or pdf.");
                }

                var rsSave = new clsDRWrapper();

                try
                {
                    rsSave.SetStoredProcedure("Insert_Document", "CentralDocuments");
                    rsSave.Set_ParameterValue("@ID", 0);
                    rsSave.Set_ParameterValue("@AltReference", command.AltReference);
                    rsSave.Set_ParameterValue("@ItemDescription", command.Description);
                    rsSave.Set_ParameterValue("@ItemName", itemName);
                    rsSave.Set_ParameterValue("@MediaType", mediaType);
                    rsSave.Set_ParameterValue("@ReferenceId", command.ReferenceId);
                    rsSave.Set_ParameterValue("@DataGroup", command.DataGroup);
                    rsSave.Set_ParameterValue("@ReferenceType", command.ReferenceType);
                    rsSave.Set_ParameterValue("@ItemData", command.ItemData);
                    rsSave.Set_ParameterValue("@DocumentIdentifier", command.DocumentIdentifier);
                    rsSave.Set_ParameterValue("@DateCreated",command.DateCreated);
                    rsSave.ExecuteStoredProcedure(false);
                    var id = FCConvert.ToInt32(rsSave.Get_ParameterValue("@ID"));
                    if (id < 1) throw new DataException($"Failed to add the document. {rsSave.ErrorDescription}");
                    return id;
                }
                finally
                {
                    rsSave.DisposeOf();
                }
            }

            return 0;
        }
    }
}