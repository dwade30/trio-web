﻿using fecherFoundation;
using SharedApplication.CentralDocuments;
using System;
using System.Collections.ObjectModel;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    class GetCentralDocumentHeadersByReference_Recordset : IGetCentralDocumentHeadersByReference
    {
        public Collection<CentralDocumentHeader> GetCentralDocuments(string strReferenceGroup, string strReferenceType, int lngReferenceID)
        {
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                var gColl = new Collection<CentralDocumentHeader>();
                CentralDocumentHeader cenDoc;
                rsLoad.OpenRecordset($"select id, len(itemdata) as ItemLength, AltReference, ItemDescription, ItemName, MediaType, ReferenceID, ReferenceType, ReferenceGroup from [Documents] where ReferenceGroup = '{strReferenceGroup}' and ReferenceType = '{strReferenceType}' and ReferenceId = {FCConvert.ToString(lngReferenceID)} order by ItemName", "CentralDocuments");

                while (!rsLoad.EndOfFile())
                {
                    cenDoc = new CentralDocumentHeader
                    {
                        ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID")),
                        AltReference = FCConvert.ToString(rsLoad.Get_Fields_String("AltReference")),
                        ItemDescription = FCConvert.ToString(rsLoad.Get_Fields_String("ItemDescription")),
                        ItemName = FCConvert.ToString(rsLoad.Get_Fields_String("ItemName")),
                        MediaType = FCConvert.ToString(rsLoad.Get_Fields_String("MediaType")),
                        ReferenceId = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ReferenceID")))),
                        ReferenceSubGroup = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceType")),
                        ReferenceGroup = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceGroup")),
                        ItemLength = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ItemLength"))
                    };
                    gColl.Add(cenDoc);
                    rsLoad.MoveNext();
                }

                return gColl;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                throw;
            }
            finally
            {
                rsLoad.DisposeOf();
            }
        }

    }
}
