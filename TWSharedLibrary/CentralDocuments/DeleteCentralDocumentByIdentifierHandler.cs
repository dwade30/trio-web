﻿using SharedApplication.CentralDocuments;
using SharedApplication.Messaging;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    public class DeleteCentralDocumentByIdentifierHandler : CommandHandler<DeleteCentralDocumentByIdentifier>
    {
        
        protected override void Handle(DeleteCentralDocumentByIdentifier command)
        {
            var rsSave = new clsDRWrapper();
            rsSave.Execute("delete from Documents where DocumentIdentifier = '" + command.DocumentIdentifier + "'",
                "CentralDocuments");
        }
    }
}