﻿using System;
using System.Data;
using fecherFoundation;
using SharedApplication.CentralDocuments;
using SharedApplication.Messaging;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    public class CreateCentralDocumentHandler: CommandHandler<CreateCentralDocument,int>
    {
        protected override int Handle(CreateCentralDocument command)
        {
            var rsSave = new clsDRWrapper();
            try
            {
                rsSave.SetStoredProcedure("Insert_Document", "CentralDocuments");
                rsSave.Set_ParameterValue("@ID", 0);
                rsSave.Set_ParameterValue("@AltReference", command.AltReference);
                rsSave.Set_ParameterValue("@ItemDescription", command.Description);
                rsSave.Set_ParameterValue("@ItemName", command.ItemName);
                rsSave.Set_ParameterValue("@MediaType", command.MediaType);
                rsSave.Set_ParameterValue("@ReferenceId", command.ReferenceId);
                rsSave.Set_ParameterValue("@DataGroup", command.DataGroup);
                rsSave.Set_ParameterValue("@ReferenceType", command.ReferenceType);
                rsSave.Set_ParameterValue("@ItemData", command.ItemData);
                rsSave.Set_ParameterValue("@DocumentIdentifier", command.DocumentIdentifier);
                rsSave.Set_ParameterValue("@DateCreated",command.DateCreated);
                rsSave.ExecuteStoredProcedure(false);
                var id = Convert.ToInt32(rsSave.Get_ParameterValue("@ID"));
                if (id < 1) throw new DataException($"Failed to add the document. {rsSave.ErrorDescription}");
                return id;
            }
            finally
            {
                rsSave.DisposeOf();
            }
        }
    }
}