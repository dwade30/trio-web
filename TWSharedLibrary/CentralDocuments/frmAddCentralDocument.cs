﻿//Fecher vbPorter - Version 1.0.0.27

using System;
using Wisej.Web;
using System.Drawing;
using fecherFoundation;
using SharedApplication;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;

namespace TWSharedLibrary
{
	/// <summary>
	/// Summary description for frmAddCentralDocument.
	/// </summary>
	public partial class frmAddCentralDocument : SharedBaseForm, IModalView<IAddCentralDocumentViewModel>
	{
		public frmAddCentralDocument()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmAddCentralDocument(IAddCentralDocumentViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
            this.Shown += FrmAddCentralDocument_Shown;
		}

        private void FrmAddCentralDocument_Shown(object sender, EventArgs e)
        {
            _dataGroup = ViewModel.ReferenceGroup;
            _referenceType = ViewModel.ReferenceType;
            _referenceId = ViewModel.ReferenceId;
            _altReference = ViewModel.AltReferenceId;

        }

        bool blnSaved;
		CentralDocumentService svc = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
		string _dataGroup;
		int _referenceId;
		string _referenceType;
		string _altReference;

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			// Browse through your directories for an already existing file
			FCCommonDialog ofd = new FCCommonDialog();
			//FC:TODO:PJ File-Handling(Options)
			ofd.Filter = "All Image Files |*.pdf;*.psd;*.j2k;*.j2c;*.gif;*.jpg;*.pcx;*.wmf;*.wbmp;*.bmp;*.tif;*.tga;*.pgx;*.ras;*.pnm;*.png;*.ico|PDF (*.pdf)|*.pdf|PhotoShop (*.psd)|*.psd|JPEG 2000 (*.j2k)|*.j2k;*.j2c|JPEG (*.jpg)|*.jpg|PCX (*.pcx)|*.pcx|WMF (*.wmf)|*.wmf|Wireless Bitmap (*.wbmp)|*.wbmp|Bitmap (*.bmp)|*.bmp|TIF (*.tif)|*.tif|TGA (*.tga)|*.tga|Gif (*.gif)|*.gif |PGX (*.pgx)|*.pgx|RAS (*.ras)|*.ras|PNM (*.pnm)|*.pnm|PNG (*.png)|*.png|Icon (*.ico)|*.ico";
			//ofd.FilterIndex = 1;
			ofd.Text = "Open Document";
			//FC:TODO:PJ File-Handling (Options)
			//ofd.Options = vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir;
            
            if (!ofd.ShowOpen()) return;

            if (ofd.FileName != "")
            {
                lblFile.Text = ofd.FileName;

            }
        }

		private void frmAddCentralDocument_Activated(object sender, System.EventArgs e)
		{

		}

		private void frmAddCentralDocument_Load(object sender, System.EventArgs e)
		{

			blnSaved = false;
			SetCustomFormColors();
			// set custom colors to be used in form
			this.panel1.Controls.Clear();
			Label infoPanel = new Label();
			infoPanel.Dock = DockStyle.Top;
			infoPanel.Text = "Drop a PDF or image to this area. Or use the upload button";
			infoPanel.Name = "infoPanel";
			this.panel1.Controls.Add(infoPanel);
		}

		private void frmAddCentralDocument_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (lblFile.Text != "")
			{
				// if a file has been selected
                var doc = svc.MakeCentralDocumentFromFile(lblFile.Text, txtDescription.Text.Trim(), _referenceType, _referenceId, _altReference, _dataGroup,Guid.NewGuid(), panel1.ItemData);
                var cmd = new SaveCommand {Document = doc};
                svc.SaveCentralDocument(cmd);
				if (svc.HadError)
				{
					MessageBox.Show($"Error {FCConvert.ToString(svc.LastErrorNumber)}  {svc.LastErrorMessage}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				blnSaved = true;
			}
			Close();
		}

		private void SetCustomFormColors()
		{
			lblFile.ForeColor = Color.Blue;
			// set file label forecolor to blue to make it show up
		}

		private void panel1_DragDrop(object sender, DragEventArgs e)
		{
			lblFile.Text = panel1.FileName;
		}

		private void upload1_Uploaded(object sender, UploadedEventArgs e)
		{
			panel1.LoadFile(e.Files);
			lblFile.Text = panel1.FileName;
		}

        public IAddCentralDocumentViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }
    }
}
