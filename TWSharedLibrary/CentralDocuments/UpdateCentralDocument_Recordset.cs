﻿using System;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralDocuments.Interfaces;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    class UpdateCentralDocument_Recordset : IUpdateCentralDocument
    {
        public void Update(CentralDocument doc)
        {
            var rsSave = new clsDRWrapper();

            try
            {
                rsSave.SetStoredProcedure("Update_Document", "CentralDocuments");
                rsSave.Set_ParameterValue("@ID", doc.ID);
                rsSave.Set_ParameterValue("@AltReference", doc.AltReference);
                rsSave.Set_ParameterValue("@ItemDescription", doc.ItemDescription);
                rsSave.Set_ParameterValue("@ItemName", doc.ItemName);
                rsSave.Set_ParameterValue("@MediaType", doc.MediaType);
                rsSave.Set_ParameterValue("@ReferenceId", doc.ReferenceId);
                rsSave.Set_ParameterValue("@DataGroup", doc.DataGroup);
                rsSave.Set_ParameterValue("@ReferenceType", doc.ReferenceType);
                rsSave.Set_ParameterValue("@ItemData", doc.ItemData);
                rsSave.ExecuteStoredProcedure(false);
                doc.IsUpdated = false;
            }
            finally
            {
                rsSave.DisposeOf();
            }
        }
    }
}
