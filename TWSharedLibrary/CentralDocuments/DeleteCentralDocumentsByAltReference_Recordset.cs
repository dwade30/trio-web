﻿using fecherFoundation;
using System;
using SharedApplication.CentralDocuments.Interfaces;
using SharedApplication.Extensions;
using TWSharedLibrary.Data;

namespace TWSharedLibrary.CentralDocuments
{
    public class DeleteCentralDocumentsByAltReference_Recordset : IDeleteCentralDocumentsByAltReference
    {
        public void DeleteByAltReference(string DataGroup, string referenceType, int referenceId, string altReference)
        {
            clsDRWrapper rs = new clsDRWrapper();

            try
            {
                rs.Execute($"Delete from [Documents] "
                           + $"where ReferenceGroup = '{DataGroup.HtmlEncode()}' and ReferenceType = '{referenceType.HtmlEncode()}' and AltReference = '{altReference.HtmlEncode()}'", 
                           "CentralDocuments");
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                throw;
            }
            finally
            {
                rs.DisposeOf();
            }
        }

    }
}
