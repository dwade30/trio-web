﻿namespace TWSharedLibrary
{
    public class GlobalColorSettings
    {
        public int TRIOCOLORGRAYEDOUT { get; set; }
        public int TRIOCOLORBLACK { get; set; }
        public int TRIOCOLORGRAYBACKGROUND { get; set; }
        // light gray
        public int TRIOCOLORRED { get; set; }
        // red
        public int TRIOCOLORBLUE { get; set; }
        // blue
        public int TRIOCOLORHIGHLIGHT { get; set; }
        // yellow highlight color
        public int TRIOCOLORFORECOLOR { get; set; }
        // black
        public int TRIOCOLORDISABLEDOPTION { get; set; }

        public int TRIOCOLORGRAYEDOUTTEXTBOX { get; set; }
    }
}