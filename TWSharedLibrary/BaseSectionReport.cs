using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using GrapeCity.ActiveReports.Document;
using GrapeCity.ActiveReports.Document.Section;
using GrapeCity.ActiveReports.Extensions;
using SharedApplication.Extensions;
using SharedApplication.Telemetry;
using Wisej.Web;

namespace TWSharedLibrary
{
    public class BaseSectionReport : FCSectionReport
    {
        protected ITelemetryService TelemetryService => StaticSettings.GlobalTelemetryService;
        private Dictionary<string,string> customTelemetryProperties = new Dictionary<string, string>();
        public bool EnablePageDiskCache { get; set; } = false;
        protected string ReportIdentifier { get; set; } = Guid.NewGuid().ToString();
        public BaseSectionReport() :base()
        {
            this.PageEnd += BaseSectionReport_PageEnd;
            this.ReportEnd += BaseSectionReport_ReportEnd;
        }

        private void BaseSectionReport_ReportEnd(object sender, EventArgs e)
        {
            if (EnablePageDiskCache && !reportCanceled && !reportDisposed && !this.IsSubReport)
            {
                SectionDocument DocTemp = new SectionDocument();
                Doc = new SectionDocument();

                try
                {
                    for (int j = 0; j < docs_count; j++)
                    {
                        var intermediateReportName = $"{Application.CommonAppDataPath}\\{ReportIdentifier}_{j}_{Application.SessionId}.ddd";
                        DocTemp.Load(intermediateReportName);
                        Doc.Pages.AddRange(DocTemp.Pages.Clone() as PagesCollection);
                        DocTemp.Pages.Clear();
                        System.IO.File.Delete(intermediateReportName);
                    }

                    foreach (GrapeCity.ActiveReports.Document.Section.Page p in this.Document.Pages)
                    {
                        Doc.Pages.Add(p.Clone() as GrapeCity.ActiveReports.Document.Section.Page);
                    }

                    if (this.Document.Pages.Count > 0)
                    {
                        this.Document.Pages.Clear();
                    }

                    this.Document.Pages.AddRange(Doc.Pages);
                }
                catch (Exception ex)
                {
                    if (this.Document != null)
                    {
                        this.Document.Pages.Add(new GrapeCity.ActiveReports.Document.Section.Page()
                            {CanvasItems = {ex.Message}});
                    }
                }
                finally
                {
                    DocTemp.Dispose();
                }
            }
        }

        protected SectionDocument Doc;
        protected int docs_count = 0;

        protected void BaseSectionReport_PageEnd(object sender, System.EventArgs e)
        {
            if (EnablePageDiskCache && !reportCanceled)
            {
                if (this.Document.Pages.Count > 52)
                {
                    Doc = new SectionDocument();
                    for (int j = 0; j < 50; j++)
                    {
                        Doc.Pages.Add(this.Document.Pages[0].Clone() as GrapeCity.ActiveReports.Document.Section.Page);
                        this.Document.Pages.RemoveAt(0);
                    }
                    Doc.Save($"{Application.CommonAppDataPath}\\{ReportIdentifier}_{docs_count}_{Application.SessionId}.ddd");
                    docs_count++;
                    Doc.Pages.Clear();
                    Doc.Dispose();
                }
            }
        }

        protected void SetAdditionalProperties(IEnumerable<KeyValuePair<string,string>> additionalProperties)
        {
            customTelemetryProperties.Clear();
            customTelemetryProperties.AddRange(additionalProperties);
        }

        protected void AddAdditionalProperty(string propertyName, string propertyValue)
        {
            customTelemetryProperties.AddOrUpdate(propertyName,propertyValue);
        }

        public override void RunReport()
        {
            //if (TelemetryService != null && !this.IsSubReport)
            //{
            //    var reportObjectFullName = this.GetType().FullName;
            //    if (!customTelemetryProperties.ContainsKey("ReportName"))
            //    {
            //        customTelemetryProperties.AddOrUpdate("Report Name", this.Name);
            //    }
            //    customTelemetryProperties.AddOrUpdate("Report Object",reportObjectFullName);
            //    TelemetryService.TrackEvent("Report Run",customTelemetryProperties);
            //}
            base.RunReport();
        }

        
        
    }
}