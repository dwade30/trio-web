﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedDataAccess;
using SharedDataAccess.SystemSettings;

namespace TWSharedLibrary
{
	public class RegionalTownService : IRegionalTownService
	{
		private ITrioContextFactory trioContextFactory;
		private SystemSettingsContext systemSettingsContext;
		private IQueryHandler<RegionSearchCriteria, IEnumerable<Region>> regionQueryHandler;

		public RegionalTownService(ITrioContextFactory contextFactory, IQueryHandler<RegionSearchCriteria, IEnumerable<Region>> regionQueryHandler)
        {
            this.trioContextFactory = contextFactory;
            this.regionQueryHandler = regionQueryHandler;

            systemSettingsContext = this.trioContextFactory.GetSystemSettingsContext();

		}

		public bool IsRegionalTown()
		{
			return systemSettingsContext.GlobalVariables.FirstOrDefault()?.UseMultipleTown ?? false;
		}

		public string TownAbbreviation(int townId)
		{
			var result = regionQueryHandler.ExecuteQuery(new RegionSearchCriteria
			{
				RegionCode = townId
			}).FirstOrDefault();

			if (result != null)
			{
				return result.TownAbbreviation;
			}

			return "";
		}

		public string TownName(int townId)
		{
			var result = regionQueryHandler.ExecuteQuery(new RegionSearchCriteria
			{
				RegionCode = townId
			}).FirstOrDefault();

			if (result != null)
			{
				return result.TownName;
			}

			return "";
		}

		public IEnumerable<Region> RegionalTowns()
		{
			return regionQueryHandler.ExecuteQuery(new RegionSearchCriteria());
		}
	}
}
