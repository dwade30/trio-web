﻿using System.Drawing;
using fecherFoundation;
using Wisej.Web;

namespace TWSharedLibrary
{
    public static class FormUtilities
    {
        public static void KeyPressHandler(KeyPressEventArgs e, FCForm form)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

            switch (KeyAscii)
            {
                case Keys.Escape:
                    KeyAscii = 0;
                    form.Close();

                    break;
                case Keys.Return:
                    KeyAscii = 0;
                    Support.SendKeys("{TAB}", false);

                    break;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        public static void LV_ClearImageListsAndRelinkToListView(ref FCListView list, ref ImageList imgLarge, ref ImageList imgSmall)
        {
            // 2003/03/06 Sub created by Larry Rebich while in La Quinta, CA.
            int i;



            // remove all but the first one which is a dummy image
            for (i = imgLarge.Images.Count - 1; i >= 2; i--)
            {
                imgLarge.Images.RemoveAt(i);
            }
            for (i = imgSmall.Images.Count - 1; i >= 2; i--)
            {
                imgSmall.Images.RemoveAt(i);
            }


        }
    }
}
