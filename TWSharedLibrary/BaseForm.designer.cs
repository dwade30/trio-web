﻿namespace TWSharedLibrary
{
	partial class SharedBaseForm
	{
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BottomPanel = new Wisej.Web.Panel();
            this.ClientArea = new Wisej.Web.Panel();
            this.TopPanel = new Wisej.Web.Panel();
            this.HeaderText = new fecherFoundation.FCHeader();
            this.waitPanel = new Wisej.Web.Panel();
            this.panel1 = new Wisej.Web.Panel();
            this.progressBar1 = new Wisej.Web.ProgressBar();
            this.pictureBox1 = new Wisej.Web.PictureBox();
            this.waitLabel = new Wisej.Web.Label();
            this.btnCancel = new Wisej.Web.Button();
            this.TopPanel.SuspendLayout();
            this.waitPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            //this.BottomPanel.Dock = Wisej.Web.DockStyle.Bottom;
            this.BottomPanel.Location = new System.Drawing.Point(0, 470);
            this.BottomPanel.Margin = new Wisej.Web.Padding(0);
            this.BottomPanel.Name = "BottomPanel";
            this.BottomPanel.Size = new System.Drawing.Size(1014, 108);
            this.BottomPanel.TabIndex = 1000;
            this.BottomPanel.SizeChanged += new System.EventHandler(this.BottomPanel_SizeChanged);
            this.BottomPanel.ControlAdded += new Wisej.Web.ControlEventHandler(this.BottomPanel_ControlAdded);
            this.BottomPanel.ControlRemoved += new Wisej.Web.ControlEventHandler(this.BottomPanel_ControlRemoved);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.BottomPanel);
            this.ClientArea.AutoScroll = true;
            this.ClientArea.Dock = Wisej.Web.DockStyle.Fill;
            this.ClientArea.Location = new System.Drawing.Point(0, 60);
            this.ClientArea.Margin = new Wisej.Web.Padding(0);
            this.ClientArea.Name = "ClientArea";
            this.ClientArea.Size = new System.Drawing.Size(1014, 410);
            this.ClientArea.TabIndex = 1;
            this.ClientArea.Layout += new Wisej.Web.LayoutEventHandler(this.ClientArea_Layout);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.HeaderText);
            this.TopPanel.Dock = Wisej.Web.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(1014, 60);
            this.TopPanel.TabIndex = 1001;
            // 
            // HeaderText
            // 
            this.HeaderText.AutoSize = true;
            this.HeaderText.Font = new System.Drawing.Font("default", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Name = "HeaderText";
            this.HeaderText.Size = new System.Drawing.Size(4, 28);
            // 
            // waitPanel
            // 
            this.waitPanel.Controls.Add(this.panel1);
            this.waitPanel.Location = new System.Drawing.Point(0, 60);
            this.waitPanel.Name = "waitPanel";
            this.waitPanel.Size = new System.Drawing.Size(1, 1);
            this.waitPanel.TabIndex = 0;
            this.waitPanel.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = Wisej.Web.AnchorStyles.None;
            this.panel1.BackColor = System.Drawing.Color.FromName("@controlLight");
            this.panel1.BorderStyle = Wisej.Web.BorderStyle.Solid;
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.waitLabel);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(312, 77);
            this.panel1.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(228, 52);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(69, 21);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(20, 73);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(320, 24);
            this.progressBar1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageSource = "ajax-loader";
            this.pictureBox1.Location = new System.Drawing.Point(20, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.TabIndex = 1;
            // 
            // waitLabel
            // 
            this.waitLabel.AutoSize = true;
            this.waitLabel.ForeColor = System.Drawing.Color.FromName("@buttonText");
            this.waitLabel.Location = new System.Drawing.Point(56, 30);
            this.waitLabel.Name = "waitLabel";
            this.waitLabel.Size = new System.Drawing.Size(62, 15);
            this.waitLabel.TabIndex = 0;
            this.waitLabel.Text = "Waiting ...";
            this.waitLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 578);
            this.Controls.Add(this.waitPanel);
            this.Controls.Add(this.ClientArea);
            //this.Controls.Add(this.BottomPanel);
            this.Controls.Add(this.TopPanel);
            this.FillColor = 16777215;
            this.Name = "BaseForm";
            this.Text = "Window";
            this.Load += new System.EventHandler(this.BaseForm_Load);
            this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.BaseForm_FormClosing);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.waitPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        public Wisej.Web.Panel BottomPanel;
        public Wisej.Web.Panel ClientArea;
        public Wisej.Web.Panel TopPanel;
        public fecherFoundation.FCHeader HeaderText;
        private Wisej.Web.Panel waitPanel;
        private Wisej.Web.Label waitLabel;
        private Wisej.Web.PictureBox pictureBox1;
        private Wisej.Web.Panel panel1;
        private Wisej.Web.ProgressBar progressBar1;
        private Wisej.Web.Button btnCancel;
    }
}
