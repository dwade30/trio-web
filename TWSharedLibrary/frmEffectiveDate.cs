﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using SharedApplication;
using SharedApplication.Extensions;

namespace TWSharedLibrary
{
	/// <summary>
	/// Summary description for frmDateChange.
	/// </summary>
	public partial class frmEffectiveDateChange : fecherFoundation.FCForm, IModalView<IEffectiveDateChangeViewModel>
    {
        private bool cancelling = true;
		public frmEffectiveDateChange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmEffectiveDateChange(IEffectiveDateChangeViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

		private void InitializeComponentEx()
		{
            this.Closing += FrmDateChange_Closing;
		}

        private void FrmDateChange_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cancelling)
            {
                ViewModel.Cancelled = true;
            }
            else
            {
                ViewModel.Cancelled = false;
            }
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        //public static frmDateChange InstancePtr
        //{
        //	get
        //	{
        //		return (frmDateChange)Sys.GetInstance(typeof(frmDateChange));
        //	}
        //}

        //protected frmDateChange _InstancePtr = null;
  //      DateTime dtNew;
		//int intFormType;
		//bool boolEffectChecked;

		//public void Init(string strText, short intType, DateTime dtEffectiveDate)
		//{
		//	// strText is the actual sting that will be shown above the date box
		//	// intType will tell me which place in my code called this form
		//	string strTemp = "";
		//	if (intType == 1)
		//	{
		//		chkEffect.Visible = true;
		//	}

		//	lblInstructions.Text = strText;
		//	txtDate.Text = Strings.Format(dtEffectiveDate, "MM/dd/yyyy");
		//	intFormType = intType;
		//	this.Show(FormShowEnum.Modal);
		//}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			DialogResult intAns = 0;
			DateTime datLowDate;
			DateTime datHighDate;
            var effectiveDate = dtpEffectiveDate.NullableValue;
           
						if (effectiveDate.HasValue)
						{
							datLowDate = DateAndTime.DateAdd("yyyy", -1, DateTime.Today);
							datHighDate = DateAndTime.DateAdd("yyyy", 1, DateTime.Today);
							if (effectiveDate.Value.ToOADate() < datLowDate.ToOADate() || effectiveDate.Value.ToOADate() > datHighDate.ToOADate())
							{
								intAns = FCMessageBox.Show("The date you are entering is different from today's date by more than a year.  Are you sure you wish to use this date?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Change Date?");
								if (intAns == DialogResult.No)
                                {
                                    return;
                                }
							}
                            ViewModel.SetEffectiveDate(new EffectiveDateChoice()
                            {
                                EffectiveDate = effectiveDate.Value,
                                UpdateRecordedDate = ViewModel.AllowChangingRecordedDate && chkEffect.Checked
                            });
							//dtNew = DateAndTime.DateValue(txtDate.Text);
							//boolEffectChecked = FCConvert.CBool(chkEffect.CheckState == Wisej.Web.CheckState.Checked);
							//this.Hide();

							//frmRECLStatus.InstancePtr.GetNewEffectiveInterestDate(ref dtNew, boolEffectChecked);
							////FC:FINAL:AM:#i178 - refresh the grid
							//frmRECLStatus.InstancePtr.FormReset = true;
							//frmRECLStatus.InstancePtr.Form_Activate();
							//frmRECLStatus.InstancePtr.FormReset = false;
                            CloseWithoutCancelling();
                            return;
                         }
            //break;
            //		}
            //}
            //end switch
        }


        private void CloseWithoutCancelling()
        {
            cancelling = false;
            Close();
        }
		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmDateChange_Activated(object sender, System.EventArgs e)
		{

		}

		private void frmDateChange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				Support.SendKeys("{tab}", false);
			}
		}

		private void frmDateChange_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 27)
				Close();
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmDateChange_Load(object sender, System.EventArgs e)
		{
			
            ShowEffectiveDate();
            //if (modCLCalculations.Statics.gboolDefaultRTDToAutoChange)
            //{
            //	chkEffect.CheckState = Wisej.Web.CheckState.Checked;
            //}
            //else
            //{
            //	chkEffect.CheckState = Wisej.Web.CheckState.Unchecked;
            //}
            dtpEffectiveDate.Select();
		}

        private void ShowEffectiveDate()
        {
            chkEffect.Visible = ViewModel.AllowChangingRecordedDate;
            chkEffect.Checked = ViewModel.AllowChangingRecordedDate && ViewModel.UpdateRecordedDate;            
            dtpEffectiveDate.Value = ViewModel.EffectiveDate;
        }


        public IEffectiveDateChangeViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);
        }
    }
}
