﻿using fecherFoundation;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Newtonsoft.Json;
using SharedApplication.Enums;

namespace TWSharedLibrary.PrinterUtility
{
    public static class PrinterUtility
    {
        public static void SaveApplicationKey(string key)
        {
            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(key, false);
            po.FunctionToExecute = FunctionToExecute.SaveApplicationKey;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
        }

        public static void LoadMachinePrinters()
        {
            var printerJson = GetMachinePrinters();

            try
            {
                // load values into global settings
                foreach (var tuple in printerJson)
                {
                    var printerTypeName = $"{tuple.Item1}";
                    StaticSettings.GlobalSettingService.SaveSetting(SettingOwnerType.Machine, printerTypeName, tuple.Item2);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }
        }

        public static List<Tuple<string, string>> GetMachinePrinters()
        {
            string printerJson = "";

            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(printerJson, true);
            po.FunctionToExecute = FunctionToExecute.GetMachinesPrinters;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
            printerJson = po.Parameter[0]?.Value;
            var ret = string.IsNullOrEmpty(printerJson)
                ? new List<Tuple<string, string>>()
                : JsonConvert.DeserializeObject<List<Tuple<string, string>>>(printerJson);
            return ret;
        }

        public static void LoadPrintingSettings()
        {
  
            //string printingKey = GetApplicationKey() ?? "";
            //if (!string.IsNullOrEmpty(printingKey))
            //{
            //    PrintingViaWebSocketsVariables.Statics.MachineName = printingKey;
            //}

            LoadMachinePrinters();
        }

        public static string GetApplicationKey()
        {
            string printerJson = "";

            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(printerJson, true);
            po.FunctionToExecute = FunctionToExecute.GetApplicationKey;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
            printerJson = po.Parameter[0]?.Value;
            var ret = printerJson;
            return ret;
        }
    }
}