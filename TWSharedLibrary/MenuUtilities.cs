﻿using fecherFoundation;

namespace TWSharedLibrary
{
    public static class MenuUtilities
    {
        public static void AddMenuItem(this FCNavigationMenu navigationMenu, MenuItemInformation menuItem)
        {
            if (navigationMenu == null || menuItem == null)
            {
                return;
            }

            var parentItem = navigationMenu.Add(menuItem.Caption,
                menuItem.ActionHandlerName + ":" + menuItem.CommandCode.ToString(), "", true, 1, menuItem.ImageKey);
            foreach (var subItem in menuItem.MenuItems)
            {
                parentItem.SubItems.Add(subItem.Caption, subItem.ActionHandlerName + ":" + subItem.CommandCode,
                    menuItem.Name, true, 2, subItem.ImageKey);
            }
        }
    }
}