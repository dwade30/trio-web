﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWSharedLibrary
{
	/// <summary>
	/// Summary description for frmDateChange.
	/// </summary>
	partial class frmEffectiveDateChange : fecherFoundation.FCForm
	{
		public fecherFoundation.FCCheckBox chkEffect;
		//public T2KDateBox txtDate;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdOk;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{			
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEffectiveDateChange));
            this.chkEffect = new fecherFoundation.FCCheckBox();
            this.cmdQuit = new fecherFoundation.FCButton();
            this.cmdOk = new fecherFoundation.FCButton();
            this.dtpEffectiveDate = new fecherFoundation.FCDateTimePicker();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            ((System.ComponentModel.ISupportInitialize)(this.chkEffect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            this.SuspendLayout();
            // 
            // chkEffect
            // 
            this.chkEffect.Checked = true;
            this.chkEffect.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkEffect.Location = new System.Drawing.Point(28, 92);
            this.chkEffect.Name = "chkEffect";
            this.chkEffect.Size = new System.Drawing.Size(242, 24);
            this.chkEffect.TabIndex = 2;
            this.chkEffect.Text = "Affect Recorded Transaction Date";
            this.chkEffect.Visible = false;
            // 
            // cmdQuit
            // 
            this.cmdQuit.AppearanceKey = "actionButton";
            this.cmdQuit.Location = new System.Drawing.Point(165, 140);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(87, 40);
            this.cmdQuit.TabIndex = 4;
            this.cmdQuit.Text = "Cancel";
            this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
            // 
            // cmdOk
            // 
            this.cmdOk.AppearanceKey = "actionButton";
            this.cmdOk.DialogResult = Wisej.Web.DialogResult.OK;
            this.cmdOk.Location = new System.Drawing.Point(58, 140);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(87, 40);
            this.cmdOk.TabIndex = 3;
            this.cmdOk.Text = "Process";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // dtpEffectiveDate
            // 
            this.dtpEffectiveDate.AutoSize = false;
            this.dtpEffectiveDate.CustomFormat = "MM/dd/yyyy";
            this.dtpEffectiveDate.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtpEffectiveDate.Location = new System.Drawing.Point(88, 33);
            this.dtpEffectiveDate.Mask = "00/00/0000";
            this.dtpEffectiveDate.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpEffectiveDate.Name = "dtpEffectiveDate";
            this.dtpEffectiveDate.ShowCalendar = false;
            this.dtpEffectiveDate.Size = new System.Drawing.Size(145, 36);
            this.dtpEffectiveDate.TabIndex = 5;
            this.dtpEffectiveDate.Value = new System.DateTime(((long)(0)));
            // 
            // fcLabel1
            // 
            this.fcLabel1.Anchor = Wisej.Web.AnchorStyles.Top;
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(101, 10);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(122, 17);
            this.fcLabel1.TabIndex = 6;
            this.fcLabel1.Text = "EFFECTIVE DATE";
            // 
            // frmEffectiveDateChange
            // 
            this.AcceptButton = this.cmdOk;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(318, 210);
            this.Controls.Add(this.fcLabel1);
            this.Controls.Add(this.dtpEffectiveDate);
            this.Controls.Add(this.chkEffect);
            this.Controls.Add(this.cmdQuit);
            this.Controls.Add(this.cmdOk);
            this.FillColor = 16777215;
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEffectiveDateChange";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Effective Date";
            this.Load += new System.EventHandler(this.frmDateChange_Load);
            this.Activated += new System.EventHandler(this.frmDateChange_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDateChange_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDateChange_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.chkEffect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        #endregion

        private FCDateTimePicker dtpEffectiveDate;
        private FCLabel fcLabel1;
    }
}
