﻿using System;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedDataAccess.RealEstate;

namespace TWSharedLibrary.RealEstate
{
    public class UpdatePictureRecordHandler : CommandHandler<UpdatePictureRecord>
    {
        private RealEstateContext reContext;

        public UpdatePictureRecordHandler(IRealEstateContext reContext)
        {
            this.reContext = (RealEstateContext)reContext;
        }
        protected override void Handle(UpdatePictureRecord command)
        {
            var picRecord = reContext.PictureRecords.FirstOrDefault(p => p.Id == command.Id);
            if (picRecord != null)
            {
                picRecord.Account = command.Account;
                picRecord.Card = command.Card;
                picRecord.Description = command.Description;
                picRecord.DocumentIdentifier = command.DocumentIdentifier;
                picRecord.PicNum = command.SequenceNumber;
                reContext.SaveChanges();
            }
            else
            {
                throw new Exception("Picture Record doesn't exist");
            }
        }
    }
}