﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Core;
using Wisej.Web;
using Wisej.Web.Ext.CustomProperties;

namespace TWSharedLibrary
{
    public class GridAccountUtility
    {
        //=========================================================
        private int intAcctCol;
        private string strAcctType = string.Empty;
        private bool blnValidate;
        private bool blnOnlyAllowDefaultType;
        private bool blnAllowSplits;
        // allow multitowns to split accounts
        private string strToolTipText = string.Empty;
        private FCGrid grid7Light;
        private bool GRID7LightValidating = false;

        public FCGrid GRID7Light
        {
            get
            {
                return grid7Light;
            }
            set
            {
                if (value == null)
                {
                    RemoveEventhandlers();
                }
                grid7Light = value;
                AttachEventhandlers();
            }
        }

        public bool AllowSplits
        {
            set
            {
                blnAllowSplits = value;
            }
            get
            {
                bool AllowSplits = false;
                AllowSplits = blnAllowSplits;
                return AllowSplits;
            }
        }

        public string ToolTipText
        {
            set
            {
                strToolTipText = value;
            }
            get
            {
                string ToolTipText = "";
                ToolTipText = strToolTipText;
                return ToolTipText;
            }
        }

        public short AccountCol
        {
            set
            {
                intAcctCol = value;
            }
            // VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
            get
            {
                short AccountCol = 0;
                AccountCol = FCConvert.ToInt16(intAcctCol);
                return AccountCol;
            }
        }

        public bool Validation
        {
            set
            {
                blnValidate = value;
            }
            get
            {
                bool Validation = false;
                Validation = blnValidate;
                return Validation;
            }
        }

        public bool OnlyAllowDefaultType
        {
            set
            {
                blnOnlyAllowDefaultType = value;
            }
            get
            {
                bool OnlyAllowDefaultType = false;
                Validation = blnOnlyAllowDefaultType;
                return OnlyAllowDefaultType;
            }
        }

        public string DefaultAccountType
        {
            set
            {
                strAcctType = value;
            }
            get
            {
                string DefaultAccountType = "";
                DefaultAccountType = strAcctType;
                return DefaultAccountType;
            }
        }

        public GridAccountUtility()
        {
            blnValidate = true;
        }
        //FC:FINAL:AM:#i173 - attach the event handlers after the grid is set
        public void AttachEventhandlers()
        {
            if (this.GRID7Light != null)
            {
                //FC:FINAL:AM:#3230 - remove first the event handlers
                RemoveEventhandlers();
                this.GRID7Light.CurrentCellChanged += new System.EventHandler(GRID7Light_RowColChange);
                this.GRID7Light.CellBeginEdit += new DataGridViewCellCancelEventHandler(this.GRID7light_BeforeEdit);
                this.GRID7Light.CellValidating += new DataGridViewCellValidatingEventHandler(this.GRID7Light_ValidateEdit);
                this.GRID7Light.Click += GRID7Light_Click;
                this.GRID7Light.CellFormatting += GRID7Light_MouseMove;
                this.GRID7Light.GotFocus += GRID7Light_GotFocus;
                //FC:FINAL:AM:#i478 - attach key edit event handlers
                this.GRID7Light.EditingControlShowing += GRID7Light_EditingControlShowing;
                if (NewAccountBox.Statics.GSegmentSize == "")
                {
                    NewAccountBox.GetFormats();
                }
            }
        }

        public void RemoveEventhandlers()
        {
            if (this.GRID7Light != null)
            {
                this.GRID7Light.CurrentCellChanged -= new System.EventHandler(GRID7Light_RowColChange);
                this.GRID7Light.CellBeginEdit -= new DataGridViewCellCancelEventHandler(this.GRID7light_BeforeEdit);
                this.GRID7Light.CellValidating -= new DataGridViewCellValidatingEventHandler(this.GRID7Light_ValidateEdit);
                this.GRID7Light.Click -= GRID7Light_Click;
                this.GRID7Light.CellFormatting -= GRID7Light_MouseMove;
                this.GRID7Light.GotFocus -= GRID7Light_GotFocus;
                //FC:FINAL:AM:#i478 - attach key edit event handlers
                this.GRID7Light.EditingControlShowing -= GRID7Light_EditingControlShowing;
            }
        }

        private void GRID7Light_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
                CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
                e.Control.KeyDown -= new KeyEventHandler(GRID7Light_KeyDownEdit);
                //e.Control.KeyPress -= new KeyPressEventHandler(GRID7Light_KeyPressEdit);
                e.Control.KeyUp -= new KeyEventHandler(GRID7Light_KeyUpEdit);
                e.Control.Appear -= Control_Appear;
                e.Control.KeyDown += new KeyEventHandler(GRID7Light_KeyDownEdit);
                //e.Control.KeyPress += new KeyPressEventHandler(GRID7Light_KeyPressEdit);
                e.Control.KeyUp += new KeyEventHandler(GRID7Light_KeyUpEdit);
                e.Control.Appear += Control_Appear;
                int col = (sender as FCGrid).Col;
                if (intAcctCol == -1 || intAcctCol == col)
                {
                    if (e.Control is TextBox)
                    {
                        (e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
                    }
                    DynamicObject customClientData = new DynamicObject();
                    customClientData["GRID7Light_Col"] = col;
                    customClientData["intAcctCol"] = intAcctCol;
                    customClientData["AllowFormats"] = NewAccountBox.Statics.AllowFormats;
                    customClientData["gboolTownAccounts"] = true;
                    customClientData["gboolSchoolAccounts"] = false;
                    customClientData["DefaultAccountType"] = NewAccountBox.DefaultAccountType;
                    customClientData["DefaultInfochoolAccountType"] = NewAccountBox.DefaultInfochoolAccountType;
                    customClientData["ESegmentSize"] = NewAccountBox.Statics.ESegmentSize;
                    customClientData["RSegmentSize"] = NewAccountBox.Statics.RSegmentSize;
                    customClientData["GSegmentSize"] = NewAccountBox.Statics.GSegmentSize;
                    customClientData["PSegmentSize"] = NewAccountBox.Statics.PSegmentSize;
                    customClientData["VSegmentSize"] = NewAccountBox.Statics.VSegmentSize;
                    customClientData["LSegmentSize"] = NewAccountBox.Statics.LSegmentSize;
                    customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
                    customProperties.SetCustomPropertiesValue(e.Control, customClientData);
                    if (e.Control.UserData.GRID7LightClientSideKeys == null)
                    {
                        e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
                        JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
                        JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
                        keyDownEvent.Event = "keydown";
                        keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
                        JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
                        keyUpEvent.Event = "keyup";
                        keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
                        clientEvents.Add(keyDownEvent);
                        clientEvents.Add(keyUpEvent);
                    }
                }
            }
        }

        private void Control_Appear(object sender, EventArgs e)
        {
            var editor = sender as TextBox;
            if (editor != null)
            {
                editor.SelectionStart = this.GRID7Light.EditSelStart < 0 ? 0 : this.GRID7Light.EditSelStart;
                editor.SelectionLength = this.GRID7Light.EditSelLength < 0 ? 0 : this.GRID7Light.EditSelLength;
            }
        }

        private void GRID7Light_Click(object sender, EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                iLabel1:
                if (GRID7Light.Col == intAcctCol || intAcctCol == -1)
                {
                    if (intAcctCol == -1)
                    {
                        iLabel2:
                        GRID7Light.Row = 0;
                        iLabel3:
                        GRID7Light.Col = 0;
                    }
                    iLabel4:
                    if (GRID7Light.TextMatrix(GRID7Light.Row, GRID7Light.Col) == "")
                    {
                        string strTemp = "";
                        iLabel5:
                        NewAccountBox.SetGridFormat(GRID7Light, GRID7Light.Row, GRID7Light.Col, false, strTemp, strAcctType);
                    }
                    iLabel6:
                    GRID7Light.EditCell();
                    iLabel7:
                    GRID7Light.EditSelStart = 0;
                    iLabel8:
                    GRID7Light.EditSelLength = 1;
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in GRID7Light_Click on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
            }
        }

        private void GRID7Light_GotFocus(object sender, EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                iLabel1:
                if (GRID7Light.Col == intAcctCol || intAcctCol == -1)
                {
                    iLabel2:
                    if (intAcctCol == -1)
                    {
                        iLabel3:
                        GRID7Light.Row = 0;
                        iLabel4:
                        GRID7Light.Col = 0;
                    }
                    iLabel5:
                    if (GRID7Light.TextMatrix(GRID7Light.Row, GRID7Light.Col) == "")
                    {
                        string strTemp = "";
                        iLabel6:
                        NewAccountBox.SetGridFormat(GRID7Light, GRID7Light.Row, GRID7Light.Col, false, strTemp, strAcctType);
                    }
                    if (NewAccountBox.Statics.intMaxLength == 0)
                    {
                        NewAccountBox.Statics.intMaxLength = GRID7Light.TextMatrix(GRID7Light.Row, GRID7Light.Col).Length;
                    }
                    GRID7Light.EditSelStart = 0;
                iLabel9:
                    GRID7Light.EditSelLength = 1;
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in GRID7Light_GotFocus on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
            }
        }

        private void GRID7Light_KeyDownEdit(object sender, KeyEventArgs e)
        {
        }

        private void GRID7Light_KeyPressEdit(object sender, KeyPressEventArgs e)
        {
            try
            {
                //FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
                if (e.KeyChar == 9)
                {
                    return;
                }
                short KeyAscii = FCConvert.ToInt16(e.KeyChar);
                // On Error GoTo ErrorHandler
                iLabel1:
                if (GRID7Light.Col == intAcctCol || intAcctCol == -1)
                {
                    iLabel2:
                    if (NewAccountBox.Statics.AllowFormats)
                    {
                        iLabel3:
                        if (blnOnlyAllowDefaultType && GRID7Light.EditSelStart == 0)
                        {
                            iLabel4:
                            if (KeyAscii != Convert.ToByte(Strings.UCase(strAcctType)[0]) && KeyAscii != Convert.ToByte(Strings.LCase(strAcctType)[0]))
                            {
                                iLabel5:
                                KeyAscii = 0;
                            }
                            else
                            {
                                iLabel6:
                                NewAccountBox.CheckAccountKeyPress(GRID7Light, GRID7Light.Row, GRID7Light.Col, KeyAscii, GRID7Light.EditSelStart, GRID7Light.EditText);
                            }
                        }
                        else
                        {
                            iLabel7:
                            NewAccountBox.CheckAccountKeyPress(GRID7Light, GRID7Light.Row, GRID7Light.Col, KeyAscii, GRID7Light.EditSelStart, GRID7Light.EditText);
                        }
                    }
                    else
                    {
                        iLabel8:
                        if (GRID7Light.EditSelStart == 0)
                        {
                            iLabel9:
                            if (KeyAscii != Convert.ToByte("M"[0]) && KeyAscii != Convert.ToByte("m"[0]))
                            {
                                iLabel10:
                                KeyAscii = 0;
                            }
                            else
                            {
                                iLabel11:
                                NewAccountBox.CheckAccountKeyPress(GRID7Light, GRID7Light.Row, GRID7Light.Col, KeyAscii, GRID7Light.EditSelStart, GRID7Light.EditText);
                            }
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in GRID7Light_KeyPressEdit on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
            }
        }

        private void GRID7Light_KeyUpEdit(object sender, KeyEventArgs e)
        {
            string strTemp;
            try
            {
                // On Error GoTo ErrorHandler
               // iLabel1:
                strTemp = GRID7Light.EditText;
               // iLabel2:
                //if (GRID7Light.Col == intAcctCol || intAcctCol == -1)
                //{
                //    iLabel3:
                //    NewAccountBox.CheckKeyDownEditF2(GRID7Light, GRID7Light.Row, GRID7Light.Col, e.KeyCode, e.Shift ? 1 : 0, GRID7Light.EditSelStart, ref strTemp, GRID7Light.EditSelLength, blnOnlyAllowDefaultType);
                //}
                iLabel4:
                if (e.KeyCode == Keys.F2 && e.Shift)
                {
                    strTemp = NewAccountBox.CheckKeyDownEditF2(GRID7Light, GRID7Light.Row, GRID7Light.Col, e.KeyCode, e.Shift ? 1 : 0, GRID7Light.EditSelStart, ref strTemp, GRID7Light.EditSelLength, blnOnlyAllowDefaultType);
                    iLabel5:
                    if (!String.IsNullOrWhiteSpace(strTemp))
                    {
                        GRID7Light.EditText = strTemp;
                        GRID7Light.Text = strTemp;
                    }

                    //iLabel6:
                    App.DoEvents();
                   // iLabel7:
                    //FC:FINAL:MSH - Issue #436: stop and then run editing fo current cell, because after sending value to EditControl and 
                    // after setting new values for carriage, carriage won't be replaced
                    GRID7Light.EndEdit();
                    //GRID7Light.EditCell();
                    //GRID7Light.EditSelStart = 0;
                    //iLabel8:
                    //GRID7Light.EditSelLength = 1;
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in GRID7Light_KeyDownEdit on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
            }           
        }

        private void GRID7Light_MouseMove(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                iLabel1:
                if (GRID7Light.IsValidCell(e.ColumnIndex, e.RowIndex))
                {
                    DataGridViewCell cell = GRID7Light[e.ColumnIndex, e.RowIndex];
                    //FC:FINAL:ASZ: if (strToolTipText == "")
                    if (String.IsNullOrEmpty(strToolTipText))
                    {
                        iLabel2:
                        if (GRID7Light.GetFlexColIndex(e.ColumnIndex) == intAcctCol || intAcctCol == -1)
                        {
                            iLabel3:
                            cell.ToolTipText = "Hit F2 to get a valid accounts list.";
                        }
                        else
                        {
                            iLabel4:
                            if (cell.ToolTipText == "Hit F2 to get a valid accounts list.")
                            {
                                iLabel5:
                                cell.ToolTipText = "";
                            }
                        }
                    }
                    else
                    {
                        iLabel6:
                        if (GRID7Light.GetFlexColIndex(e.ColumnIndex) == intAcctCol || intAcctCol == -1)
                        {
                            iLabel7:
                            cell.ToolTipText = strToolTipText;
                        }
                        else
                        {
                            iLabel8:
                            if (cell.ToolTipText == strToolTipText)
                            {
                                iLabel9:
                                cell.ToolTipText = "";
                            }
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in GRID7Light_MouseMove on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
            }
        }

        private void GRID7Light_RowColChange(object sender, EventArgs e)
        {
            string strTemp;
            try
            {
                // On Error GoTo ErrorHandler
                iLabel1:
                App.DoEvents();
                iLabel2:
                strTemp = "";
                iLabel3:
                if (GRID7Light.Col == intAcctCol || intAcctCol == -1)
                {
                    iLabel4:
                    if (intAcctCol == -1)
                    {
                        iLabel5:
                        GRID7Light.Row = 0;
                        iLabel6:
                        GRID7Light.Col = 0;
                    }
                    iLabel7:
                    NewAccountBox.SetGridFormat(GRID7Light, GRID7Light.Row, GRID7Light.Col, true, strTemp, strAcctType);
                }
                iLabel8:
                if (strTemp != "" && GRID7Light.TextMatrix(GRID7Light.Row, GRID7Light.Col) == "")
                {
                    iLabel9:
                    App.DoEvents();
                    iLabel10:
                    GRID7Light.TextMatrix(GRID7Light.Row, GRID7Light.Col, strTemp);
                    iLabel11:
                    GRID7Light.EditText = strTemp;
                    iLabel12:
                    App.DoEvents();
                    iLabel13:
                    GRID7Light.EditSelStart = 0;
                    iLabel14:
                    GRID7Light.EditSelLength = 1;
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in GRID7Light_RowColChange on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
            }
        }

        private void GRID7Light_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //FC:FINAL:SBE - #4292 - do not execute Validating event during previous Validating event
            if (GRID7LightValidating)
            {
                return;
            }
            if (GRID7Light.CurrentCell.IsInEditMode)
            {
                try
                {
                    GRID7LightValidating = true;
                    //FC:FINAL:MSH - Issue #385: saving correct indexes of cell, for which is executed validating
                    var row = GRID7Light.GetFlexRowIndex(e.RowIndex);
                    var col = GRID7Light.GetFlexColIndex(e.ColumnIndex);
                    // On Error GoTo ErrorHandler
                    if (row > 0)
                    {
                        iLabel1:
                        if ((col == intAcctCol || intAcctCol == -1) && blnValidate)
                        {
                            iLabel2:
                            ////FC:FINAL:BSE:#i2387 Prevent client changes during long time processing (Collection modified exception)
                            //GRID7Light.Redraw = false;
                            e.Cancel = NewAccountBox.CheckAccountValidate(GRID7Light, row, col, e.Cancel, blnAllowSplits);
                            ////FC:FINAL:BSE:#i2387 Re-enable client changes after long time processing
                            //GRID7Light.Redraw = true;
                        }
                    }
                    return;
                }
                catch (Exception ex)
                {
                    // ErrorHandler:
                    FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in GRID7Light_ValidateEdit on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
                }
                finally
                {
                    GRID7LightValidating = false;
                }
            }
        }

        private void GRID7light_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                iLabel1:
                if (GRID7Light.Col == intAcctCol || intAcctCol == -1)
                {
                    iLabel2:
                    GRID7Light.EditMaxLength = 23;
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                FCMessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + ex.GetBaseException().Message + "\r\n" + "\r\n" + "An error occurred in GRID7light_BeforeEdit on line " + Information.Erl(), MsgBoxStyle.Critical, "Errors Encountered");
            }
        }

        public bool AllowFormats
        {
            get => NewAccountBox.Statics.AllowFormats;
        }
    }
}