﻿using System.Drawing;
using fecherFoundation;

namespace TWSharedLibrary
{
    public static class GridColorUtility
    {
        public static long SetGridColor(int x)
        {
            long SetGridColor = 0;
            switch (x)
            {
                case 0:
                    {
                        //SetGridColor = "&HFF8080"; // Blue
                        //SetGridColor = 0xFF8080;
                        SetGridColor = ColorTranslator.ToOle(Color.FromArgb(255, 255, 255));
                        // Blue
                        break;
                    }
                case 1:
                    {
                        //SetGridColor = "&HF8C2D6"; // Purple
                        //SetGridColor = 0xF8C2D6;
                        SetGridColor = ColorTranslator.ToOle(Color.FromArgb(229, 232, 237));
                        // Purple
                        break;
                    }
                case 2:
                    {
                        //SetGridColor = "&HD8FDF8"; // Yellow
                        //SetGridColor = 0xD8FDF8;
                        SetGridColor = ColorTranslator.ToOle(Color.FromArgb(213, 217, 226));
                        // Yellow
                        break;
                    }
                case 3:
                    {
                        //SetGridColor = "&HE8E9E9"; // Gray
                        SetGridColor = 0xE8E9E9;
                        // Gray
                        break;
                    }
                case 4:
                    {
                        //SetGridColor = "&HE6F5FD"; // Tan
                        SetGridColor = 0xE6F5FD;
                        // Tan
                        break;
                    }
                case 5:
                    {
                        //SetGridColor = "&HFFFFFF"; // White
                        SetGridColor = 0xFFFFFF;
                        // White
                        break;
                    }
                case 99:
                    {
                        //SetGridColor = "&H407000"; // Green
                        SetGridColor = 0x407000;
                        // Green
                        break;
                    }
            }
            return SetGridColor;
        }
        // vbPorter upgrade warning: Grid As object	OnWrite(VSFlex6DAOCtl.vsFlexGrid)
        public static void ColorGrid(FCGrid Grid, int StartRow = 1, int EndRow = -1, int StartCol = 0, int EndCol = -1, bool blnTotal = false)
        {
            int lngCounter;
            if (EndRow == -1)
            {
                EndRow = Grid.Rows - 1;
            }
            if (EndCol == -1)
            {
                EndCol = Grid.Cols - 1;
            }
            for (lngCounter = StartRow; lngCounter <= EndRow; lngCounter++)
            {
                Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngCounter, StartCol, lngCounter, EndCol, SetGridColor(Grid.RowOutlineLevel(lngCounter) - 1));
            }
            if (blnTotal)
            {
                Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, EndRow, StartCol, EndRow, EndCol, SetGridColor(99));
            }
        }
    }
}