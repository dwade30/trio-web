﻿using System;
using System.Net.Http;
using Newtonsoft.Json;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace TWSharedLibrary
{
    public class GetMachineIdHandler :CommandHandler<GetMachineId,string>
    {
        private ISettingService settingService;
        private const string defaultPort = "44850";
        public GetMachineIdHandler(ISettingService settingService)
        {
            this.settingService = settingService;
        }
        protected override string Handle(GetMachineId command)
        {
            try
            {
                var setting = settingService.GetSettingValue(SettingOwnerType.Global, "TrioAssistant", "TrioAssistantPort");
                var port = setting.SettingValue;
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
                httpClient.Timeout = TimeSpan.FromSeconds(6);
                if (port.IsNullOrWhiteSpace())
                {
                    port = defaultPort;
                    settingService.SaveSetting(SettingOwnerType.Global,"TrioAssistant","TrioAssistantPort",defaultPort);
                }
                var requestUri = @"http://localhost:" + port + "/machineid";
                var response = httpClient.GetAsync(requestUri).Result;
                if (response.IsSuccessStatusCode)
                {
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    var responseObject = JSON.Parse(responseString);
                    return responseObject.machineId;
                }
            }
            catch
            {
                return "";
            }

            return "";
        }
    }
}