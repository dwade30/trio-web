//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rpt2000SwintecNewCTA.
	/// </summary>
	public partial class rpt2000SwintecNewCTA : BaseSectionReport
	{
		public rpt2000SwintecNewCTA()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Certificate of Title";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rpt2000SwintecNewCTA InstancePtr
		{
			get
			{
				return (rpt2000SwintecNewCTA)Sys.GetInstance(typeof(rpt2000SwintecNewCTA));
			}
		}

		protected rpt2000SwintecNewCTA _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt2000SwintecNewCTA	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string CTAName;
		clsDRWrapper clsTitle = new clsDRWrapper();

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			MotorVehicle.Statics.boolDoubleCTA = false;
			clsTitle.DisposeOf();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Printer tempPrinter = new Printer();
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				CTAName = FCConvert.ToString(modRegistry.GetRegistryKey("ctaprintername"));
				if (CTAName != string.Empty)
				{
					this.Document.Printer.PrinterName = CTAName;
				}
				/*? For Each */
				foreach (FCPrinter p in FCGlobal.Printers)
				{
					if (p.DeviceName == CTAName)
					{
						if (tempPrinter.DriverName == "EPSON24")
						{
							for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
							{
								(this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font("Courier 10cpi", (this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Size);
							}
						}
						break;
					}
				}
				/*? Next *///this.Printer.RenderMode = 1;
				clsTitle.OpenRecordset("select * from title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "New CTA ReportStart", "Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				if (MotorVehicle.Statics.boolDoubleCTA)
				{
					MotorVehicle.Statics.blnPrintDoubleCTA = false;
				}
				else
				{
					MotorVehicle.Statics.blnPrintCTA = false;
				}
				rptNewCTA.InstancePtr.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (FCConvert.ToInt32(clsTitle.Get_Fields_String("DoubleNumber")) != 0)
			{
				fldDoublCTATitle.Visible = true;
			}
			else
			{
				fldDoublCTATitle.Visible = false;
			}
			if (MotorVehicle.Statics.boolDoubleCTA)
			{
				DoubleCTA();
			}
			else
			{
				RegCTA();
			}
		}

		private void RegCTA()
		{
			string strLessee = "";
			string strSeller;
			// vbPorter upgrade warning: intPlaceHolder As int	OnWriteFCConvert.ToInt32(
			int intPlaceHolder = 0;
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				txtName1.Text = clsTitle.Get_Fields_String("name1");
				if (Information.IsDate(clsTitle.Get_Fields("dob1")))
				{
					txtDOB1.Text = Strings.Format(clsTitle.Get_Fields_DateTime("dob1"), "MM/dd/yyyy");
				}
				if (Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("TelephoneNumber"))), 5) != "(000)")
				{
					txtTelephone.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("telephonenumber")));
				}
				else
				{
					txtTelephone.Text = Strings.Right(FCConvert.ToString(clsTitle.Get_Fields_String("telephonenumber")), 8);
				}
				txtName2.Text = clsTitle.Get_Fields_String("name2");
				if (Information.IsDate(clsTitle.Get_Fields("dob2")))
				{
					txtDOB2.Text = Strings.Format(clsTitle.Get_Fields_DateTime("dob2"), "MM/dd/yyyy");
				}
				if (clsTitle.Get_Fields_Boolean("joint") == true)
				{
					txtJointOwnership.Text = "X";
				}
				txtAddress.Text = clsTitle.Get_Fields_String("address");
				txtCity.Text = clsTitle.Get_Fields_String("city");
				txtState.Text = clsTitle.Get_Fields("state");
				txtZip.Text = clsTitle.Get_Fields_String("zipandzip4");
				txtLegalRes.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("ResidenceAddress"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("residencecity"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("residencestate"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("residencezip")));
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode1")) == "R")
				{
					strLessee = fecherFoundation.Strings.Trim(MotorVehicle.GetPartyName(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"), true));
				}
				else if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode2")) == "R" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LeaseCode3")) == "R")
				{
					strLessee = fecherFoundation.Strings.Trim(MotorVehicle.GetPartyName(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"), true));
				}
				else
				{
					strLessee = "";
				}
				if (strLessee != "")
				{
					strLessee += " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("address"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("city"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("state"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("zip")));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("zip4"))).Length > 0)
					{
						strLessee += "-" + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("zip4")));
					}
				}
				string vbPorterVar = clsTitle.Get_Fields_String("msrp");
				if (vbPorterVar == "N")
				{
					txtNewMsrp.Text = "N";

					txtmsrpamount.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("baseprice"), "#,##0");
				}
				else if (vbPorterVar == "U")
				{
					txtUsedMsrp.Text = "U";
					txtmsrpamount.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("baseprice"), "#,##0");
				}
				else if (vbPorterVar == "NR")
				{
					txtNRMSRP.Text = "NR";
				}
				txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("year"));
				txtMake.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
				txtModel.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");
				if (MotorVehicle.Statics.FleetCTA)
				{
					txtVIN.Text = "S.A.L.";
				}
				else
				{
					txtVIN.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("vin");
				}
				txtBodyType.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("style");
				string vbPorterVar1 = clsTitle.Get_Fields_String("newusedrebuilt");
				if (vbPorterVar1 == "N")
				{
					txtNew.Text = "N";
				}
				else if (vbPorterVar1 == "U")
				{
					txtU.Text = "U";
				}
				else if (vbPorterVar1 == "R")
				{
					txtRebuilt.Text = "R";
				}
				txtPurchaseDate.Text = Strings.Format(clsTitle.Get_Fields("purchasedate"), "MM/dd/yyyy");
				txtPreviousTitle.Text = clsTitle.Get_Fields_String("previoustitle");
				txtStateofOrigin.Text = clsTitle.Get_Fields_String("stateoforigin");
				if (MotorVehicle.Statics.FleetCTA)
				{
					txtOther.Text = "S.A.L.";
				}
				else
				{
					txtOther.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("class") + " " + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate");
				}
				txtOdometer.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("odometer"));
				if (FCConvert.ToString(clsTitle.Get_Fields_String("odometermk")) == "K")
				{
					txtKilometer.Text = "K";
				}
				else
				{
					txtMile.Text = "M";
				}
				string vbPorterVar2 = clsTitle.Get_Fields_String("odometercondition");
				if (vbPorterVar2 == "A")
				{
					txtActual.Text = "AC";
				}
				else if (vbPorterVar2 == "E")
				{
					txtInExcess.Text = "IE";
				}
				else if (vbPorterVar2 == "C")
				{
					txtNotActual.Text = "NA";
					txtOdometerChanged.Text = "C";
				}
				else if (vbPorterVar2 == "B")
				{
					txtNotActual.Text = "NA";
					txtOdometerBroken.Text = "B";
				}
				txtFirstLienHolder.Text = clsTitle.Get_Fields_String("lh1name");
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("lh1name"))).Length > 1)
				{
					txtDateofLien.Text = Strings.Format(clsTitle.Get_Fields("lh1date"), "MM/dd/yyyy");
				}
				txtLien1Address.Text = clsTitle.Get_Fields_String("lh1address1");
				if (txtLien1Address.Text.Length > 25)
				{
					txtLien1Address.Text = Strings.Left(txtLien1Address.Text, 25);
				}
				txtLien1City.Text = clsTitle.Get_Fields_String("lh1city");
				txtLien1State.Text = clsTitle.Get_Fields_String("lh1state");
				txtLien1Zip.Text = clsTitle.Get_Fields_String("lh1zipandzip4");
				txtSecondLienHolder.Text = clsTitle.Get_Fields_String("lh2name");
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("lh2name"))).Length > 1)
				{
					txtLien2Date.Text = Strings.Format(clsTitle.Get_Fields("lh2date"), "MM/dd/yyyy");
				}
				txtLien2Address.Text = clsTitle.Get_Fields_String("lh2address1");
				txtLien2City.Text = clsTitle.Get_Fields_String("lh2city");
				txtLien2State.Text = clsTitle.Get_Fields_String("lh2state");
				txtLien2Zip.Text = clsTitle.Get_Fields_String("lh2zipandzip4");
				strSeller = fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("sellername"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("selleraddress1"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("sellercity"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("sellerstate"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(clsTitle.Get_Fields_String("sellerzipandzip4")));
				if (strSeller.Length > 40)
				{
					intPlaceHolder = Strings.InStr(30, strSeller, " ", CompareConstants.vbBinaryCompare);
					txtSellerName.Text = Strings.Left(strSeller, intPlaceHolder);
					txtSellerName2.Text = Strings.Right(strSeller, strSeller.Length - intPlaceHolder);
				}
				else
				{
					txtSellerName.Text = "";
					txtSellerName2.Text = strSeller;
				}
				txtPlateNo.Text = clsTitle.Get_Fields_String("dealerplate");
				string vbPorterVar3 = clsTitle.Get_Fields_String("dealertype");
				if (vbPorterVar3 == "U")
				{
					txtUsed.Text = "U";
				}
				else if (vbPorterVar3 == "D")
				{
					txtDealer.Text = "D";
				}
				else if (vbPorterVar3 == "M")
				{
					txtM.Text = "M";
				}
				MotorVehicle.Statics.blnPrintCTA = true;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MotorVehicle.PrinterError("CTA / Use Tax Printer");
				MotorVehicle.Statics.blnPrintCTA = false;
			}
		}

		private void DoubleCTA()
		{
			string strSeller;
			// vbPorter upgrade warning: intPlaceHolder As int	OnWriteFCConvert.ToInt32(
			int intPlaceHolder = 0;
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				txtName1.Text = clsTitle.Get_Fields_String("sellername");
				txtDOB1.Text = Strings.Format(MotorVehicle.Statics.rsDoubleTitle.Get_Fields("dob"), "MM/dd/yyyy");
				txtAddress.Text = clsTitle.Get_Fields_String("selleraddress1");
				txtCity.Text = clsTitle.Get_Fields_String("sellercity");
				txtState.Text = clsTitle.Get_Fields_String("sellerstate");
				txtZip.Text = clsTitle.Get_Fields_String("sellerzipandzip4");
				string vbPorterVar = clsTitle.Get_Fields_String("msrp");
				if (vbPorterVar == "N")
				{
					txtNewMsrp.Text = "N";
					txtmsrpamount.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("baseprice"), "#,##0");
				}
				else if (vbPorterVar == "U")
				{
					txtUsedMsrp.Text = "U";
					txtmsrpamount.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("baseprice"), "#,##0");
				}
				else if (vbPorterVar == "NR")
				{
					txtNRMSRP.Text = "NR";
				}
				txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("year"));
				txtMake.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
				txtModel.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");
				txtVIN.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("vin");
				txtBodyType.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("style");
				string vbPorterVar1 = MotorVehicle.Statics.rsDoubleTitle.Get_Fields("type");
				if (vbPorterVar1 == "N")
				{
					txtNew.Text = "N";
				}
				else if (vbPorterVar1 == "U")
				{
					txtU.Text = "U";
				}
				else if (vbPorterVar1 == "R")
				{
					txtRebuilt.Text = "R";
				}
				txtPurchaseDate.Text = Strings.Format(MotorVehicle.Statics.rsDoubleTitle.Get_Fields("purchasedate"), "MM/dd/yyyy");
				txtPreviousTitle.Text = MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("previoustitle");
				txtStateofOrigin.Text = MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("previousstate");
				txtOther.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("class") + " " + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate");
				if (FCConvert.ToString(clsTitle.Get_Fields_String("odometermk")) == "K")
				{
					txtKilometer.Text = "K";
				}
				else
				{
					txtMile.Text = "M";
				}
				string vbPorterVar2 = clsTitle.Get_Fields_String("odometercondition");
				if (vbPorterVar2 == "A")
				{
					txtActual.Text = "A";
				}
				else if (vbPorterVar2 == "E")
				{
					txtInExcess.Text = "IE";
				}
				else if (vbPorterVar2 == "C")
				{
					txtNotActual.Text = "NA";
					txtOdometerChanged.Text = "C";
				}
				else if (vbPorterVar2 == "B")
				{
					txtNotActual.Text = "NA";
					txtOdometerBroken.Text = "B";
				}
				txtOdometer.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("odometer"));
				strSeller = fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("sellername") + "") + " " + fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("selleraddress") + "") + " " + fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("sellercity") + "") + " " + fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("sellerstate") + "") + " " + fecherFoundation.Strings.Trim(MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("sellerzip") + "");
				if (strSeller.Length > 40)
				{
					intPlaceHolder = Strings.InStr(30, strSeller, " ", CompareConstants.vbBinaryCompare);
					txtSellerName.Text = Strings.Left(strSeller, intPlaceHolder);
					txtSellerName2.Text = Strings.Right(strSeller, strSeller.Length - intPlaceHolder);
				}
				else
				{
					txtSellerName.Text = "";
					txtSellerName2.Text = strSeller;
				}
				txtPlateNo.Text = MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("plate");
				string vbPorterVar3 = MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("dealertype");
				if (vbPorterVar3 == "U")
				{
					txtUsed.Text = "U";
				}
				else if (vbPorterVar3 == "D")
				{
					txtDealer.Text = "D";
				}
				else if (vbPorterVar3 == "M")
				{
					txtM.Text = "M";
				}
				MotorVehicle.Statics.blnPrintDoubleCTA = true;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MotorVehicle.PrinterError("CTA / Use Tax Printer");
				MotorVehicle.Statics.blnPrintDoubleCTA = false;
			}
		}

		private void rpt2000SwintecNewCTA_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rpt2000SwintecNewCTA properties;
			//rpt2000SwintecNewCTA.Caption	= "Certificate of Title";
			//rpt2000SwintecNewCTA.Icon	= "rpt2000SwintecNewCTA.dsx":0000";
			//rpt2000SwintecNewCTA.Left	= 0;
			//rpt2000SwintecNewCTA.Top	= 0;
			//rpt2000SwintecNewCTA.Width	= 11880;
			//rpt2000SwintecNewCTA.Height	= 8595;
			//rpt2000SwintecNewCTA.StartUpPosition	= 3;
			//rpt2000SwintecNewCTA.SectionData	= "rpt2000SwintecNewCTA.dsx":058A;
			//End Unmaped Properties
		}
	}
}
