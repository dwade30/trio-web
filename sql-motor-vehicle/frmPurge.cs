//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmPurgeHeld : BaseForm
	{
		public frmPurgeHeld()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPurgeHeld InstancePtr
		{
			get
			{
				return (frmPurgeHeld)Sys.GetInstance(typeof(frmPurgeHeld));
			}
		}

		protected frmPurgeHeld _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsPurge = new clsDRWrapper();
		int PurgeCol;
		int ClassCol;
		int PlateCol;
		int OwnerCol;
		int YearCol;
		int MakeCol;
		int ModelCol;
		int ExpireCol;

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void cmdPurge_Click(object sender, System.EventArgs e)
		{
			int totalcount;
			DateTime tempdate;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int DeletedCount;
			clsDRWrapper rsDelete = new clsDRWrapper();
			if (vsVehicles.Visible == false)
			{
				MessageBox.Show("You must search for vehicles before you may try to perform a purge of data.", "Unable to Purge", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			DeletedCount = 0;
			for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, PurgeCol)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(counter, PurgeCol)) == true)
				{
					DeletedCount += 1;
					Return_Plate_To_Inventory(vsVehicles.TextMatrix(counter, PlateCol));
					rsDelete.Execute("DELETE FROM HeldRegistrationMaster WHERE ID = " + vsVehicles.TextMatrix(counter, 0), "TWMV0000.vb1");
				}
			}
			MessageBox.Show("Purge Complete!!  " + FCConvert.ToString(DeletedCount) + " records deleted", "Purge Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			Close();
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			int totalcount = 0;
			DateTime tempdate;
			int counter = 0;
			if (cmbName.Text == "Date")
			{
				if (!Information.IsDate(txtDate.Text))
				{
					MessageBox.Show("This is not a valid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				tempdate = fecherFoundation.DateAndTime.DateValue(txtDate.Text);
				rsPurge.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE Status = 'H' AND DateUpdated <= '" + FCConvert.ToString(tempdate) + "'");
			}
			else if (cmbName.Text == "Plate")
			{
				if (fecherFoundation.Strings.Trim(txtPlate.Text) == "")
				{
					MessageBox.Show("You must enter search criteria before you may perform a search.", "No Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				rsPurge.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE Status = 'H' AND Plate like '" + fecherFoundation.Strings.Trim(txtPlate.Text) + "%'");
			}
			else
			{
				if (cmbOwner.Text == "Company")
				{
					if (fecherFoundation.Strings.Trim(txtCompanyName.Text) == "")
					{
						MessageBox.Show("You must enter search criteria before you may perform a search.", "No Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						rsPurge.OpenRecordset("SELECT c.*, p.FullName FROM HeldRegistrationMaster as c LEFT JOIN " + rsPurge.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE OwnerCode1 <> 'I' AND FullName LIKE '" + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.FixQuotes(txtCompanyName.Text))) + "%' AND Status = 'H' ORDER BY FullName");
					}
				}
				else
				{
					if (fecherFoundation.Strings.Trim(txtLastName.Text) == "" && fecherFoundation.Strings.Trim(txtFirstName.Text) == "")
					{
						MessageBox.Show("You must enter search criteria before you may perform a search.", "No Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						rsPurge.OpenRecordset("SELECT c.*, p.FullName, p.FirstName, p.LastName FROM HeldRegistrationMaster  as c LEFT JOIN " + rsPurge.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE OwnerCode1 = 'I' AND FirstName LIKE '" + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.FixQuotes(txtFirstName.Text))) + "%' AND LastName LIKE '" + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.FixQuotes(txtLastName.Text))) + "%' AND Status = 'H' ORDER BY FullName");
					}
				}
			}
			if (rsPurge.EndOfFile() != true && rsPurge.BeginningOfFile() != true)
			{
				rsPurge.MoveLast();
				rsPurge.MoveFirst();
				totalcount = rsPurge.RecordCount();
				vsVehicles.Rows = totalcount + 1;
				counter = 1;
				do
				{
					vsVehicles.TextMatrix(counter, 0, FCConvert.ToString(rsPurge.Get_Fields_Int32("ID")));
					vsVehicles.TextMatrix(counter, PurgeCol, FCConvert.ToString(false));
					if (!fecherFoundation.FCUtils.IsNull(rsPurge.Get_Fields_String("Class")))
					{
						vsVehicles.TextMatrix(counter, ClassCol, FCConvert.ToString(rsPurge.Get_Fields_String("Class")));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsPurge.Get_Fields_String("plate")))
					{
						vsVehicles.TextMatrix(counter, PlateCol, FCConvert.ToString(rsPurge.Get_Fields_String("plate")));
					}
					vsVehicles.TextMatrix(counter, OwnerCol, fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsPurge.Get_Fields_Int32("PartyID1"))));
					if (!fecherFoundation.FCUtils.IsNull(rsPurge.Get_Fields("Year")))
					{
						vsVehicles.TextMatrix(counter, YearCol, FCConvert.ToString(rsPurge.Get_Fields("Year")));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsPurge.Get_Fields_String("make")))
					{
						vsVehicles.TextMatrix(counter, MakeCol, FCConvert.ToString(rsPurge.Get_Fields_String("make")));
					}
					if (!fecherFoundation.FCUtils.IsNull(rsPurge.Get_Fields_String("model")))
					{
						vsVehicles.TextMatrix(counter, ModelCol, FCConvert.ToString(rsPurge.Get_Fields_String("model")));
					}
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(rsPurge.Get_Fields_DateTime("ExpireDate")))
					{
						vsVehicles.TextMatrix(counter, ExpireCol, FCConvert.ToString(rsPurge.Get_Fields_DateTime("ExpireDate")));
					}
					counter += 1;
					rsPurge.MoveNext();
				}
				while (rsPurge.EndOfFile() != true);
				vsVehicles.Visible = true;
				if (fraSearch.Visible == true)
				{
					txtFirstName.Text = "";
					txtLastName.Text = "";
					txtCompanyName.Text = "";
					cmbOwner.Text = "Company";
					fraSearch.Visible = false;
				}
			}
			else
			{
				MessageBox.Show("No vehicles were found that matched the criteria.", "No Matches", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
			{
				vsVehicles.TextMatrix(counter, PurgeCol, FCConvert.ToString(true));
			}
		}

		private void frmPurgeHeld_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			PurgeCol = 1;
			ClassCol = 2;
			PlateCol = 3;
			OwnerCol = 4;
			YearCol = 5;
			MakeCol = 6;
			ModelCol = 7;
			ExpireCol = 8;
			//vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsVehicles.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsVehicles.ColDataType(PurgeCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsVehicles.TextMatrix(0, PurgeCol, "Purge");
			vsVehicles.TextMatrix(0, ClassCol, "Class");
			vsVehicles.TextMatrix(0, PlateCol, "Plate");
			vsVehicles.TextMatrix(0, OwnerCol, "Owner");
			vsVehicles.TextMatrix(0, YearCol, "Year");
			vsVehicles.TextMatrix(0, MakeCol, "Make");
			vsVehicles.TextMatrix(0, ModelCol, "Model");
			vsVehicles.TextMatrix(0, ExpireCol, "Expires");
			vsVehicles.ColWidth(0, 0);
			vsVehicles.ColWidth(PurgeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1207174));
			vsVehicles.ColWidth(ClassCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.0875022));
			vsVehicles.ColWidth(PlateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1207174));
			vsVehicles.ColWidth(OwnerCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.2535783));
			vsVehicles.ColWidth(YearCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.0764304));
			vsVehicles.ColWidth(MakeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.0875022));
			vsVehicles.ColWidth(ModelCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1207174));
			vsVehicles.ColWidth(ExpireCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1207174));
			this.Refresh();
		}

		private void frmPurgeHeld_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeHeld properties;
			//frmPurgeHeld.FillStyle	= 0;
			//frmPurgeHeld.ScaleWidth	= 9300;
			//frmPurgeHeld.ScaleHeight	= 7185;
			//frmPurgeHeld.LinkTopic	= "Form2";
			//frmPurgeHeld.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmPurgeHeld_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
			vsVehicles.ColWidth(0, 0);
			vsVehicles.ColWidth(PurgeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1207174));
			vsVehicles.ColWidth(ClassCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.0875022));
			vsVehicles.ColWidth(PlateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1207174));
			vsVehicles.ColWidth(OwnerCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.2535783));
			vsVehicles.ColWidth(YearCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.0764304));
			vsVehicles.ColWidth(MakeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.0875022));
			vsVehicles.ColWidth(ModelCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1207174));
			vsVehicles.ColWidth(ExpireCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1207174));

            fraSearch.CenterToContainer(this.ClientArea);
        }

		private void frmPurgeHeld_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void mnuProcessSave_Click()
		{
			Support.SendKeys("{F10}", false);
		}

		private void Return_Plate_To_Inventory(string plate)
		{
			clsDRWrapper rsPL = new clsDRWrapper();
			rsPL.OpenRecordset("SELECT * FROM Inventory WHERE Status = 'H' AND FormattedInventory = '" + plate + "'");
			if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
			{
				rsPL.MoveLast();
				rsPL.MoveFirst();
				rsPL.Edit();
				rsPL.Set_Fields("Status", "A");
				rsPL.Update();
			}
		}

		private void optDate_CheckedChanged(object sender, System.EventArgs e)
		{
			txtPlate.Text = "";
			txtFirstName.Text = "";
			txtLastName.Text = "";
			txtCompanyName.Text = "";
			cmbOwner.Text = "Company";
			fraSearch.Visible = false;
			txtPlate.Visible = false;
			vsVehicles.Rows = 1;
			vsVehicles.Visible = false;
			txtDate.Visible = true;
			lblInstructions.Text = "Please enter a date and click the 'Search' button.  All held registrations that were saved on or before that date will be displayed.  You may then select the registrations you would like purged and click the 'Purge' button to delete them";
			txtDate.Focus();
		}

		private void optName_CheckedChanged(object sender, System.EventArgs e)
		{
			txtDate.Text = "";
			txtPlate.Text = "";
			txtPlate.Visible = false;
			txtDate.Visible = false;
			vsVehicles.Rows = 1;
			vsVehicles.Visible = false;
			fraSearch.Visible = true;
			lblInstructions.Text = "Please enter a name and click the 'Search' button.  All held registrations that were saved under this name will be displayed.  You may then select the registrations you would like purged and click the 'Purge' button to delete them";
			txtCompanyName.Focus();
		}

		private void optPlate_CheckedChanged(object sender, System.EventArgs e)
		{
			txtDate.Text = "";
			txtFirstName.Text = "";
			txtLastName.Text = "";
			txtCompanyName.Text = "";
			cmbOwner.Text = "Company";
			txtPlate.Visible = true;
			txtDate.Visible = false;
			fraSearch.Visible = false;
			vsVehicles.Rows = 1;
			vsVehicles.Visible = false;
			lblInstructions.Text = "Please enter a plate number and click the 'Search' button.  All held registrations that had that plate number will be displayed.  You may then select the registrations you would like purged and click the 'Purge' button to delete them";
			txtPlate.Focus();
		}

		private void optOwner_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (fraSearch.Visible == true)
			{
				if (Index == 0)
				{
					txtFirstName.Text = "";
					txtLastName.Text = "";
					txtFirstName.Visible = false;
					txtLastName.Visible = false;
					lblFirstName.Visible = false;
					lblLastName.Visible = false;
					txtCompanyName.Visible = true;
					lblCompanyName.Visible = true;
					txtCompanyName.Focus();
				}
				else
				{
					txtCompanyName.Text = "";
					txtFirstName.Visible = true;
					txtLastName.Visible = true;
					lblFirstName.Visible = true;
					lblLastName.Visible = true;
					txtCompanyName.Visible = false;
					lblCompanyName.Visible = false;
					txtFirstName.Focus();
				}
			}
		}

		private void optOwner_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbOwner.SelectedIndex;
			optOwner_CheckedChanged(index, sender, e);
		}

		//FC:FINAL:DDU:#i2187 - check all or uncheck all by pressing on header
		private void vsVehicles_ColumnHeaderClickEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (vsVehicles.Col == PurgeCol && vsVehicles.Rows > 0)
			{
				string temp = "true";
				if (FCUtils.CBool(vsVehicles.TextMatrix(1, PurgeCol)) != true)
				{
					temp = "true";
				}
				else
				{
					temp = "false";
				}
				for (int i = 1; i < vsVehicles.Rows; i++)
				{
					vsVehicles.TextMatrix(i, PurgeCol, temp);
				}
			}
		}

		private void vsVehicles_ClickEvent(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - wrong input value for FCConvert.ToBoolean (same with i.issue #1883)
			//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, PurgeCol)) != true)
			if (vsVehicles.Row > 0)
			{
				if (FCUtils.CBool(vsVehicles.TextMatrix(vsVehicles.Row, PurgeCol)) != true)
				{
					vsVehicles.TextMatrix(vsVehicles.Row, PurgeCol, FCConvert.ToString(true));
				}
				else
				{
					vsVehicles.TextMatrix(vsVehicles.Row, PurgeCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsVehicles_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				//FC:FINAL:MSH - wrong input value for FCConvert.ToBoolean (same with i.issue #1883)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, PurgeCol)) != true)
				if (FCUtils.CBool(vsVehicles.TextMatrix(vsVehicles.Row, PurgeCol)) != true)
				{
					vsVehicles.TextMatrix(vsVehicles.Row, PurgeCol, FCConvert.ToString(true));
				}
				else
				{
					vsVehicles.TextMatrix(vsVehicles.Row, PurgeCol, FCConvert.ToString(false));
				}
			}
		}

		private void cmbOwner_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1890: wrong comparion condition
			//if (cmbOwner.SelectedIndex == 0)
			if (cmbOwner.SelectedIndex > -1)
			{
				optOwner_CheckedChanged(sender, e);
			}
		}

		public void cmbName_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbName.Text == "Name")
			{
				optName_CheckedChanged(sender, e);
			}
			else if (cmbName.Text == "Plate")
			{
				optPlate_CheckedChanged(sender, e);
			}
			else if (cmbName.Text == "Date")
			{
				optDate_CheckedChanged(sender, e);
			}
		}
	}
}
