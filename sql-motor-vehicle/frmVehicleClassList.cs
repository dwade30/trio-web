using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmVehicleClassList : BaseForm
	{
		public frmVehicleClassList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			//FC:FINAL:MSH - i.issue #1843: replace wrong comparing
			//if (_InstancePtr == null && FCGlobal.Statics.Forms.Count == 0)
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmVehicleClassList InstancePtr
		{
			get
			{
				return (frmVehicleClassList)Sys.GetInstance(typeof(frmVehicleClassList));
			}
		}

		protected frmVehicleClassList _InstancePtr = null;

		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int SelectionCol;
		int DescriptionCol;

		private void cboClass_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			LoadSubclassCombo();
		}

		private void frmVehicleClassList_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmVehicleClassList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVehicleClassList properties;
			//frmVehicleClassList.FillStyle	= 0;
			//frmVehicleClassList.ScaleWidth	= 5880;
			//frmVehicleClassList.ScaleHeight	= 3810;
			//frmVehicleClassList.LinkTopic	= "Form2";
			//frmVehicleClassList.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			SelectionCol = 0;
			DescriptionCol = 1;
			vsClass.ColWidth(SelectionCol, FCConvert.ToInt32(vsClass.WidthOriginal * 0.3));
			vsClass.TextMatrix(0, DescriptionCol, "Class");
			vsClass.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			LoadClassCombo();
			cboClass.SelectedIndex = 0;
			LoadSubclassCombo();
			cboStatus.SelectedIndex = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmVehicleClassList_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmbAllStatus.Text == "Specific")
			{
				if (cboStatus.SelectedIndex == -1)
				{
					MessageBox.Show("You must select a status before you may proceed.", "Select Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cboStatus.Focus();
					return;
				}
			}
			if (cmbAll.Text != "All")
			{
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEndDate.Focus();
					return;
				}
				else if (fecherFoundation.DateAndTime.DateValue(txtStartDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtEndDate.Text).ToOADate())
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return;
				}
			}
			modDuplexPrinting.DuplexPrintReport(rptVehicleClassList.InstancePtr);
			rptVehicleClassList.InstancePtr.Unload();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadClassCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboClass.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT BMVCode FROM Class ORDER BY BMVCode");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboClass.AddItem(rsInfo.Get_Fields_String("BMVCode"));
					vsClass.Rows += 1;
					vsClass.TextMatrix(vsClass.Rows - 1, SelectionCol, FCConvert.ToString(false));
					vsClass.TextMatrix(vsClass.Rows - 1, DescriptionCol, FCConvert.ToString(rsInfo.Get_Fields_String("BMVCode")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadSubclassCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboSubclass.Clear();
			rsInfo.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + cboClass.Text + "' ORDER BY SystemCode");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboSubclass.AddItem(rsInfo.Get_Fields_String("SystemCode") + " - " + rsInfo.Get_Fields_String("Description"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			cboSubclass.SelectedIndex = 0;
		}

		private void cboSubclass_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSubclass.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 250, 0);
		}

		private void cboStatus_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboStatus.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 100, 0);
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			bool blnSelected;
			blnSelected = false;
			if (cmbMultiple.Text == "Multiple")
			{
				for (counter = 1; counter <= (vsClass.Rows - 1); counter++)
				{
					if (FCConvert.ToBoolean(vsClass.TextMatrix(counter, SelectionCol)) == true)
					{
						blnSelected = true;
						break;
					}
				}
				if (blnSelected == false)
				{
					MessageBox.Show("You must select a class before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					vsClass.Focus();
					return;
				}
			}
			if (cmbAllStatus.Text == "Specific")
			{
				if (cboStatus.SelectedIndex == -1)
				{
					MessageBox.Show("You must select a status before you may proceed.", "Select Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cboStatus.Focus();
					return;
				}
			}
			if (cmbAll.Text != "All")
			{
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtEndDate.Focus();
					return;
				}
				else if (fecherFoundation.DateAndTime.DateValue(txtStartDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtEndDate.Text).ToOADate())
				{
					MessageBox.Show("You must enter a valid date range before you may proceed.", "Enter Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtStartDate.Focus();
					return;
				}
			}
			frmReportViewer.InstancePtr.Init(rptVehicleClassList.InstancePtr);
			this.Hide();
		}

		private void optAllStatus_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSpecificStatus.Enabled = false;
			cboStatus.Enabled = false;
		}

		private void optAllSubclass_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSpecificSubclass.Enabled = false;
			cboSubclass.Enabled = false;
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = false;
			lblTo.Enabled = false;
			txtStartDate.Enabled = false;
			txtEndDate.Enabled = false;
			txtStartDate.Text = "";
			txtEndDate.Text = "";
		}

		private void optEffectiveDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = true;
			lblTo.Enabled = true;
			txtStartDate.Enabled = true;
			txtEndDate.Enabled = true;
			txtStartDate.Focus();
		}

		private void optExciseTaxDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = true;
			lblTo.Enabled = true;
			txtStartDate.Enabled = true;
			txtEndDate.Enabled = true;
			txtStartDate.Focus();
		}

		private void optExpirationDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			fraDateRange.Enabled = true;
			lblTo.Enabled = true;
			txtStartDate.Enabled = true;
			txtEndDate.Enabled = true;
			txtStartDate.Focus();
		}

		private void optMultiple_CheckedChanged(object sender, System.EventArgs e)
		{
			cboClass.Visible = false;
			cmbAllSubclass.Text = "All";
			fraSubclass.Visible = false;
			vsClass.Visible = true;
			vsClass.Row = 1;
			vsClass.Focus();
		}

		private void optSpecific_CheckedChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			cboClass.Visible = true;
			cmbAllSubclass.Text = "All";
			fraSubclass.Visible = true;
			vsClass.Visible = false;
			for (counter = 1; counter <= (vsClass.Rows - 1); counter++)
			{
				vsClass.TextMatrix(counter, SelectionCol, FCConvert.ToString(false));
			}
		}

		private void optSpecificStatus_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSpecificStatus.Enabled = true;
			cboStatus.Enabled = true;
		}

		private void optSpecificSubclass_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSpecificSubclass.Enabled = true;
			cboSubclass.Enabled = true;
		}

		private void vsClass_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsClass.Row > 0)
			{
				if (vsClass.TextMatrix(vsClass.Row, SelectionCol) == "0"/*false*/)
				{
					vsClass.TextMatrix(vsClass.Row, SelectionCol, FCConvert.ToString(true));
				}
				else
				{
					vsClass.TextMatrix(vsClass.Row, SelectionCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsClass_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsClass.Row > 0)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsClass.TextMatrix(vsClass.Row, SelectionCol)) == false)
					if (FCConvert.CBool(vsClass.TextMatrix(vsClass.Row, SelectionCol)) == false)
					{
						vsClass.TextMatrix(vsClass.Row, SelectionCol, FCConvert.ToString(true));
					}
					else
					{
						vsClass.TextMatrix(vsClass.Row, SelectionCol, FCConvert.ToString(false));
					}
				}
			}
		}

		public void cmbMultiple_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbMultiple.Text == "Multiple")
			{
				optMultiple_CheckedChanged(sender, e);
			}
			else if (cmbMultiple.Text == "Specific")
			{
				optSpecific_CheckedChanged(sender, e);
			}
		}

		public void cmbAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAll.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Expiration Date Range")
			{
				optExpirationDateRange_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Effective Date Range")
			{
				optEffectiveDateRange_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Excise Tax Date Range")
			{
				optExciseTaxDateRange_CheckedChanged(sender, e);
			}
		}

		public void cmbAllSubclass_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAllSubclass.Text == "All")
			{
				optAllSubclass_CheckedChanged(sender, e);
			}
			else if (cmbAllSubclass.Text == "Specific")
			{
				optSpecificSubclass_CheckedChanged(sender, e);
			}
		}

		public void cmbAllStatus_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAllStatus.Text == "All")
			{
				optAllStatus_CheckedChanged(sender, e);
			}
			else if (cmbAllStatus.Text == "Specific")
			{
				optSpecificStatus_CheckedChanged(sender, e);
			}
		}
	}
}
