//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Diagnostics;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmReport.
	/// </summary>
	partial class frmReport
	{
		public fecherFoundation.FCComboBox cmbAllRegistrations;
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCComboBox cmbInterim;
		public fecherFoundation.FCComboBox cmbReport;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> Check1;
		public fecherFoundation.FCFrame fraListOptions;
		public fecherFoundation.FCButton cmdOKList;
		public fecherFoundation.FCFrame fraInventoryOptions;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCFrame FraSelect;
		public fecherFoundation.FCCheckBox Check1_5;
		public fecherFoundation.FCCheckBox Check1_4;
		public fecherFoundation.FCCheckBox Check1_3;
		public fecherFoundation.FCCheckBox Check1_2;
		public fecherFoundation.FCCheckBox Check1_1;
		public fecherFoundation.FCCheckBox Check1_0;
		public FCCommonDialog dlg1_Save;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCFrame fraClose;
		public fecherFoundation.FCButton cmdCloseout;
		public fecherFoundation.FCFrame fraSetDates;
		public fecherFoundation.FCButton cmdSaveDates;
		public fecherFoundation.FCComboBox cboEnd;
		public fecherFoundation.FCComboBox cboStart;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdPrint;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbAllRegistrations = new fecherFoundation.FCComboBox();
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.cmbInterim = new fecherFoundation.FCComboBox();
			this.cmbReport = new fecherFoundation.FCComboBox();
			this.fraListOptions = new fecherFoundation.FCFrame();
			this.cmdOKList = new fecherFoundation.FCButton();
			this.fraInventoryOptions = new fecherFoundation.FCFrame();
			this.cmdOK = new fecherFoundation.FCButton();
			this.FraSelect = new fecherFoundation.FCFrame();
			this.Check1_5 = new fecherFoundation.FCCheckBox();
			this.Check1_4 = new fecherFoundation.FCCheckBox();
			this.Check1_3 = new fecherFoundation.FCCheckBox();
			this.Check1_2 = new fecherFoundation.FCCheckBox();
			this.Check1_1 = new fecherFoundation.FCCheckBox();
			this.Check1_0 = new fecherFoundation.FCCheckBox();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.dlg1_Save = new fecherFoundation.FCCommonDialog();
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.fraClose = new fecherFoundation.FCFrame();
			this.cmdCloseout = new fecherFoundation.FCButton();
			this.fraSetDates = new fecherFoundation.FCFrame();
			this.cmdSaveDates = new fecherFoundation.FCButton();
			this.cboEnd = new fecherFoundation.FCComboBox();
			this.cboStart = new fecherFoundation.FCComboBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.rtf1 = new fecherFoundation.FCRichTextBox();
			this.btnFullSetOfReports = new fecherFoundation.FCButton();
			this.btnCreteBMVDataFiles = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraListOptions)).BeginInit();
			this.fraListOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOKList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraInventoryOptions)).BeginInit();
			this.fraInventoryOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FraSelect)).BeginInit();
			this.FraSelect.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Check1_5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraClose)).BeginInit();
			this.fraClose.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCloseout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSetDates)).BeginInit();
			this.fraSetDates.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveDates)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.rtf1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFullSetOfReports)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnCreteBMVDataFiles)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 483);
			this.BottomPanel.Size = new System.Drawing.Size(656, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraInventoryOptions);
			this.ClientArea.Controls.Add(this.btnCreteBMVDataFiles);
			this.ClientArea.Controls.Add(this.cmdPrint);
			this.ClientArea.Controls.Add(this.btnFullSetOfReports);
			this.ClientArea.Controls.Add(this.rtf1);
			this.ClientArea.Controls.Add(this.fraClose);
			this.ClientArea.Controls.Add(this.fraListOptions);
			this.ClientArea.Controls.Add(this.cmbInterim);
			this.ClientArea.Controls.Add(this.cmbReport);
			this.ClientArea.Size = new System.Drawing.Size(676, 507);
			this.ClientArea.Controls.SetChildIndex(this.cmbReport, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbInterim, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraListOptions, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraClose, 0);
			this.ClientArea.Controls.SetChildIndex(this.rtf1, 0);
			this.ClientArea.Controls.SetChildIndex(this.btnFullSetOfReports, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdPrint, 0);
			this.ClientArea.Controls.SetChildIndex(this.btnCreteBMVDataFiles, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraInventoryOptions, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(676, 60);
			this.TopPanel.TabIndex = 0;
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(242, 28);
			this.HeaderText.Text = "Process End of Period";
			// 
			// cmbAllRegistrations
			// 
			this.cmbAllRegistrations.Items.AddRange(new object[] {
            "All Registrations",
            "In Town Registrations",
            "Out Of Town Registrations"});
			this.cmbAllRegistrations.Location = new System.Drawing.Point(20, 30);
			this.cmbAllRegistrations.Name = "cmbAllRegistrations";
			this.cmbAllRegistrations.Size = new System.Drawing.Size(331, 40);
			this.cmbAllRegistrations.TabIndex = 2;
			this.cmbAllRegistrations.Text = "All Registrations";
			// 
			// cmbAll
			// 
			this.cmbAll.Items.AddRange(new object[] {
            "Print All",
            "Print Selected"});
			this.cmbAll.Location = new System.Drawing.Point(20, 30);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(183, 40);
			this.cmbAll.TabIndex = 2;
			this.cmbAll.Text = "Print All";
			this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
			// 
			// cmbInterim
			// 
			this.cmbInterim.Items.AddRange(new object[] {
            "Interim Reports",
            "Live Reports"});
			this.cmbInterim.Location = new System.Drawing.Point(131, 287);
			this.cmbInterim.Name = "cmbInterim";
			this.cmbInterim.Size = new System.Drawing.Size(255, 40);
			this.cmbInterim.TabIndex = 1;
			this.cmbInterim.Text = "Live Reports";
			this.cmbInterim.SelectedIndexChanged += new System.EventHandler(this.optInterim_CheckedChanged);
			// 
			// cmbReport
			// 
			this.cmbReport.Items.AddRange(new object[] {
            "Registration Listing",
            "Town Summary",
            "Town Detail",
            "Title Application Report",
            "Exception Report",
            "Voided Forms Report",
            "Excise Tax Only Report",
            "Inventory Report",
            "Inventory Adjustment Report",
            "Gift Cert / Voucher Report",
            "Use Tax Summary Report"});
			this.cmbReport.Location = new System.Drawing.Point(131, 391);
			this.cmbReport.Name = "cmbReport";
			this.cmbReport.Size = new System.Drawing.Size(255, 40);
			this.cmbReport.TabIndex = 4;
			this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
			// 
			// fraListOptions
			// 
			this.fraListOptions.BackColor = System.Drawing.Color.FromName("@window");
			this.fraListOptions.Controls.Add(this.cmdOKList);
			this.fraListOptions.Controls.Add(this.cmbAllRegistrations);
			this.fraListOptions.Location = new System.Drawing.Point(200, 30);
			this.fraListOptions.Name = "fraListOptions";
			this.fraListOptions.Size = new System.Drawing.Size(371, 140);
			this.fraListOptions.TabIndex = 2;
			this.fraListOptions.Text = "Registration List Options";
			this.fraListOptions.Visible = false;
			// 
			// cmdOKList
			// 
			this.cmdOKList.AppearanceKey = "actionButton";
			this.cmdOKList.Location = new System.Drawing.Point(20, 80);
			this.cmdOKList.Name = "cmdOKList";
			this.cmdOKList.Size = new System.Drawing.Size(64, 40);
			this.cmdOKList.TabIndex = 1;
			this.cmdOKList.Text = "OK";
			this.cmdOKList.Click += new System.EventHandler(this.cmdOKList_Click);
			// 
			// fraInventoryOptions
			// 
			this.fraInventoryOptions.BackColor = System.Drawing.Color.White;
			this.fraInventoryOptions.Controls.Add(this.cmdOK);
			this.fraInventoryOptions.Controls.Add(this.cmbAll);
			this.fraInventoryOptions.Controls.Add(this.FraSelect);
			this.fraInventoryOptions.Location = new System.Drawing.Point(230, 30);
			this.fraInventoryOptions.Name = "fraInventoryOptions";
			this.fraInventoryOptions.Size = new System.Drawing.Size(223, 369);
			this.fraInventoryOptions.TabIndex = 3;
			this.fraInventoryOptions.Text = "Inventory Options";
			this.fraInventoryOptions.Visible = false;
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.Location = new System.Drawing.Point(79, 308);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(65, 40);
			this.cmdOK.TabIndex = 1;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// FraSelect
			// 
			this.FraSelect.AppearanceKey = "groupBoxNoBorders";
			this.FraSelect.Controls.Add(this.Check1_5);
			this.FraSelect.Controls.Add(this.Check1_4);
			this.FraSelect.Controls.Add(this.Check1_3);
			this.FraSelect.Controls.Add(this.Check1_2);
			this.FraSelect.Controls.Add(this.Check1_1);
			this.FraSelect.Controls.Add(this.Check1_0);
			this.FraSelect.Enabled = false;
			this.FraSelect.Location = new System.Drawing.Point(20, 80);
			this.FraSelect.Name = "FraSelect";
			this.FraSelect.Size = new System.Drawing.Size(183, 249);
			this.FraSelect.TabIndex = 34;
			// 
			// Check1_5
			// 
			this.Check1_5.Location = new System.Drawing.Point(0, 184);
			this.Check1_5.Name = "Check1_5";
			this.Check1_5.Size = new System.Drawing.Size(154, 22);
			this.Check1_5.TabIndex = 6;
			this.Check1_5.Text = "Combination Stickers";
			// 
			// Check1_4
			// 
			this.Check1_4.Location = new System.Drawing.Point(0, 147);
			this.Check1_4.Name = "Check1_4";
			this.Check1_4.Size = new System.Drawing.Size(76, 22);
			this.Check1_4.TabIndex = 5;
			this.Check1_4.Text = "Permits";
			// 
			// Check1_3
			// 
			this.Check1_3.Location = new System.Drawing.Point(0, 110);
			this.Check1_3.Name = "Check1_3";
			this.Check1_3.Size = new System.Drawing.Size(68, 22);
			this.Check1_3.TabIndex = 4;
			this.Check1_3.Text = "Plates";
			// 
			// Check1_2
			// 
			this.Check1_2.Location = new System.Drawing.Point(0, 74);
			this.Check1_2.Name = "Check1_2";
			this.Check1_2.Size = new System.Drawing.Size(123, 22);
			this.Check1_2.TabIndex = 2;
			this.Check1_2.Text = "Double Stickers";
			// 
			// Check1_1
			// 
			this.Check1_1.Location = new System.Drawing.Point(0, 37);
			this.Check1_1.Name = "Check1_1";
			this.Check1_1.Size = new System.Drawing.Size(117, 22);
			this.Check1_1.TabIndex = 1;
			this.Check1_1.Text = "Single Stickers";
			// 
			// Check1_0
			// 
			this.Check1_0.Name = "Check1_0";
			this.Check1_0.Size = new System.Drawing.Size(86, 22);
			this.Check1_0.TabIndex = 7;
			this.Check1_0.Text = "MVR - 3s";
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "actionButton";
			this.cmdPrint.Location = new System.Drawing.Point(400, 391);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(78, 40);
			this.cmdPrint.TabIndex = 5;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// dlg1_Save
			// 
			this.dlg1_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1_Save.FontName = "Microsoft Sans Serif";
			this.dlg1_Save.Name = "dlg1_Save";
			this.dlg1_Save.Size = new System.Drawing.Size(0, 0);
			// 
			// dlg1
			// 
			this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1.FontName = "Microsoft Sans Serif";
			this.dlg1.Name = "dlg1";
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			// 
			// fraClose
			// 
			this.fraClose.AppearanceKey = "groupBoxNoBorders";
			this.fraClose.Controls.Add(this.cmdCloseout);
			this.fraClose.Controls.Add(this.fraSetDates);
			this.fraClose.Location = new System.Drawing.Point(30, 19);
			this.fraClose.Name = "fraClose";
			this.fraClose.Size = new System.Drawing.Size(356, 260);
			this.fraClose.TabIndex = 22;
			this.fraClose.Text = " ";
			// 
			// cmdCloseout
			// 
			this.cmdCloseout.AppearanceKey = "actionButton";
			this.cmdCloseout.Location = new System.Drawing.Point(101, 0);
			this.cmdCloseout.Name = "cmdCloseout";
			this.cmdCloseout.Size = new System.Drawing.Size(255, 40);
			this.cmdCloseout.TabIndex = 3;
			this.cmdCloseout.Text = "Close Off Reporting Period";
			this.cmdCloseout.Click += new System.EventHandler(this.cmdCloseout_Click);
			// 
			// fraSetDates
			// 
			this.fraSetDates.Controls.Add(this.cmdSaveDates);
			this.fraSetDates.Controls.Add(this.cboEnd);
			this.fraSetDates.Controls.Add(this.cboStart);
			this.fraSetDates.Controls.Add(this.Label2);
			this.fraSetDates.Controls.Add(this.Label1);
			this.fraSetDates.Location = new System.Drawing.Point(0, 56);
			this.fraSetDates.Name = "fraSetDates";
			this.fraSetDates.Size = new System.Drawing.Size(356, 190);
			this.fraSetDates.TabIndex = 2;
			this.fraSetDates.Text = "Set Reporting Dates";
			// 
			// cmdSaveDates
			// 
			this.cmdSaveDates.AppearanceKey = "actionButton";
			this.cmdSaveDates.Location = new System.Drawing.Point(124, 130);
			this.cmdSaveDates.Name = "cmdSaveDates";
			this.cmdSaveDates.Size = new System.Drawing.Size(212, 40);
			this.cmdSaveDates.TabIndex = 4;
			this.cmdSaveDates.Text = "Save Reporting Dates";
			this.cmdSaveDates.Click += new System.EventHandler(this.cmdSaveDates_Click);
			// 
			// cboEnd
			// 
			this.cboEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cboEnd.Location = new System.Drawing.Point(122, 80);
			this.cboEnd.Name = "cboEnd";
			this.cboEnd.Size = new System.Drawing.Size(214, 40);
			this.cboEnd.TabIndex = 3;
			// 
			// cboStart
			// 
			this.cboStart.BackColor = System.Drawing.SystemColors.Window;
			this.cboStart.Location = new System.Drawing.Point(122, 30);
			this.cboStart.Name = "cboStart";
			this.cboStart.Size = new System.Drawing.Size(214, 40);
			this.cboStart.TabIndex = 1;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 94);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(70, 15);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "END DATE";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(80, 15);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "START DATE";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 0;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			// 
			// rtf1
			// 
			this.rtf1.Location = new System.Drawing.Point(582, 221);
			this.rtf1.Name = "rtf1";
			this.rtf1.Size = new System.Drawing.Size(25, 37);
			this.rtf1.TabIndex = 23;
			this.rtf1.Visible = false;
			// 
			// btnFullSetOfReports
			// 
			this.btnFullSetOfReports.AppearanceKey = "actionButton";
			this.btnFullSetOfReports.Location = new System.Drawing.Point(131, 339);
			this.btnFullSetOfReports.Name = "btnFullSetOfReports";
			this.btnFullSetOfReports.Size = new System.Drawing.Size(255, 40);
			this.btnFullSetOfReports.TabIndex = 24;
			this.btnFullSetOfReports.Text = "Full Set of Reports";
			this.btnFullSetOfReports.Click += new System.EventHandler(this.btnFullSetOfReports_Click);
			// 
			// btnCreteBMVDataFiles
			// 
			this.btnCreteBMVDataFiles.AppearanceKey = "actionButton";
			this.btnCreteBMVDataFiles.Location = new System.Drawing.Point(131, 443);
			this.btnCreteBMVDataFiles.Name = "btnCreteBMVDataFiles";
			this.btnCreteBMVDataFiles.Size = new System.Drawing.Size(255, 40);
			this.btnCreteBMVDataFiles.TabIndex = 25;
			this.btnCreteBMVDataFiles.Text = "Create BMV Data Files";
			this.btnCreteBMVDataFiles.Click += new System.EventHandler(this.btnCreteBMVDataFiles_Click);
			// 
			// frmReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(676, 567);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmReport";
			this.Text = "Process End of Period";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmReport_Load);
			this.Activated += new System.EventHandler(this.frmReport_Activated);
			this.Resize += new System.EventHandler(this.frmReport_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraListOptions)).EndInit();
			this.fraListOptions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdOKList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraInventoryOptions)).EndInit();
			this.fraInventoryOptions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FraSelect)).EndInit();
			this.FraSelect.ResumeLayout(false);
			this.FraSelect.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Check1_5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Check1_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraClose)).EndInit();
			this.fraClose.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCloseout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSetDates)).EndInit();
			this.fraSetDates.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveDates)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.rtf1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFullSetOfReports)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnCreteBMVDataFiles)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		public FCRichTextBox rtf1;
		public FCButton btnFullSetOfReports;
		public FCButton btnCreteBMVDataFiles;
	}
}