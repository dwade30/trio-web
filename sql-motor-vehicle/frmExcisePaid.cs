//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmExcisePaid : BaseForm
	{
		public frmExcisePaid()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			//FC:FINAL:DDU:#2174 - fixed textboxes to allow only numeric
			this.txtExciseTaxNumber.AllowOnlyNumericInput();
			this.txtExciseReceiptNumber.AllowOnlyNumericInput();
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmExcisePaid InstancePtr
		{
			get
			{
				return (frmExcisePaid)Sys.GetInstance(typeof(frmExcisePaid));
			}
		}

		protected frmExcisePaid _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsResCode = new clsDRWrapper();
		//FC:FINAL:DDU:#2175 - hold disposing of form, we need just to hide and use it's controls data
		public bool dontDispose = false;
		bool Quit;

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			if (txtExciseTaxNumber.Text == "")
			{
				MessageBox.Show("You must fill in the tax receipt number before you may continue.", "Unable To Process", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			Quit = false;
			MotorVehicle.Statics.ExciseAP = true;
			frmDataInput.InstancePtr.FromAP = true;
			//FC:FINAL:DDU:#2175 - hold disposing of form, we need just to hide and use it's controls data
			dontDispose = true;
			frmExcisePaid.InstancePtr.Hide();
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Quit = true;
			MotorVehicle.Statics.ExciseAP = false;
			frmDataInput.InstancePtr.FromAP = true;
			Close();
		}

		private void frmExcisePaid_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (MotorVehicle.Statics.PreviousETO)
			{
				txtExciseResCode.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
				txtExciseTaxNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExciseCreditMVR3"));
				txtExciseTaxPaid.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"), "#,##0.00");
				txtExciseCreditAmount.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"), "#,##0.00");
				txtExciseReceiptNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("ExciseCreditMVR3"));
				txtExciseTransferCharge.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"), "#,##0.00");
				CalculateTotals();
			}
			txtExcisePaidDate.Focus();
		}

		private void frmExcisePaid_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExcisePaid properties;
			//frmExcisePaid.ScaleWidth	= 5880;
			//frmExcisePaid.ScaleHeight	= 4245;
			//frmExcisePaid.LinkTopic	= "Form1";
			//End Unmaped Properties
			frmExcisePaid.InstancePtr.TopOriginal = FCConvert.ToInt32((FCGlobal.Screen.HeightOriginal - frmExcisePaid.InstancePtr.HeightOriginal) / 2.0);
			frmExcisePaid.InstancePtr.LeftOriginal = FCConvert.ToInt32((FCGlobal.Screen.WidthOriginal - frmExcisePaid.InstancePtr.WidthOriginal) / 2.0);
			Quit = false;
			MotorVehicle.Statics.strSql = "Select* FROM DefaultInfo";
			rsInfo.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				rsInfo.MoveLast();
				rsInfo.MoveFirst();
				txtExciseResCode.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("ResidenceCode")));
			}
			txtExcisePaidDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			MotorVehicle.Statics.strSql = "SELECT * FROM Residence";
			rsResCode.OpenRecordset(MotorVehicle.Statics.strSql);
			if (rsResCode.EndOfFile() != true && rsResCode.BeginningOfFile() != true)
			{
				rsResCode.MoveLast();
				rsResCode.MoveFirst();
			}
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				Quit = true;
				MotorVehicle.Statics.ExciseAP = false;
				frmDataInput.InstancePtr.FromAP = true;
				if (Quit == false)
				{
					if (Information.IsDate(txtExcisePaidDate.Text) == false)
						e.Cancel = true;
					if (txtExciseResCode.Text == "")
						e.Cancel = true;
					if (Conversion.Val(txtExciseTaxNumber.Text) == 0)
					{
						e.Cancel = true;
					}
					else
					{
						frmDataInput.InstancePtr.txtCreditNumber.Text = fecherFoundation.Strings.Trim(txtExciseTaxNumber.Text);
						MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3", FCConvert.ToString(Conversion.Val(txtExciseReceiptNumber.Text)));
						MotorVehicle.Statics.rsFinal.Set_Fields("ExcisePaidDate", txtExcisePaidDate.Text);
					}
					// If Val(txtExciseTaxPaid.Text) = 0 Then Cancel = 1
					// If Val(txtExciseSubTotal.Text) = 0 Then Cancel = 1
				}
			}
			if (e.CloseReason == FCCloseReason.FormCode)
			{
				CheckQuit:
				;
				if (Quit == false)
				{
					if (Information.IsDate(txtExcisePaidDate.Text) == false)
						e.Cancel = true;
					if (txtExciseResCode.Text == "")
						e.Cancel = true;
					if (Conversion.Val(txtExciseTaxNumber.Text) == 0)
					{
						e.Cancel = true;
					}
					else
					{
						frmDataInput.InstancePtr.txtCreditNumber.Text = fecherFoundation.Strings.Trim(txtExciseTaxNumber.Text);
						MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3", FCConvert.ToString(Conversion.Val(txtExciseReceiptNumber.Text)));
						MotorVehicle.Statics.rsFinal.Set_Fields("ExcisePaidDate", txtExcisePaidDate.Text);
					}
					// If Val(txtExciseTaxPaid.Text) = 0 Then Cancel = 1
					// If Val(txtExciseSubTotal.Text) = 0 Then Cancel = 1
				}
			}
			else
			{
				MotorVehicle.Statics.ExciseAP = false;
			}
			if (e.Cancel)
			{
				MessageBox.Show("You must enter all the information before you can proceed.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			rsInfo.Reset();
			rsResCode.Reset();
		}

		private void txtExciseCreditAmount_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtExciseCreditAmount_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			CalculateTotals();
		}

		private void txtExcisePaidDate_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtExciseReceiptNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || KeyAscii == Keys.Back)
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtExciseResCode_Enter(object sender, System.EventArgs e)
		{
			txtExciseResCode.SelectionLength = txtExciseResCode.Text.Length;
		}

		private void txtExciseResCode_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtExciseResCode_Leave(object sender, System.EventArgs e)
		{
			rsResCode.FindFirstRecord("Code", fecherFoundation.Strings.Trim(txtExciseResCode.Text));
			lblResState.Text = FCConvert.ToString(rsResCode.Get_Fields_String("Town"));
		}

		private void txtExciseSubTotal_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtExciseTaxNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || KeyAscii == Keys.Back)
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtExciseTaxPaid_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtExciseTaxPaid_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtExciseTaxPaid.Text != "")
			{
				CalculateTotals();
			}
		}

		private void txtExciseTotal_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtExciseTransferCharge_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void CalculateTotals()
		{
			double total = 0;
			if (txtExciseTaxPaid.Text != "")
			{
				total = FCConvert.ToDouble(txtExciseTaxPaid.Text);
			}
			else
			{
				return;
			}
			if (txtExciseCreditAmount.Text != "")
			{
				total -= FCConvert.ToDouble(txtExciseCreditAmount.Text);
			}
			if (total < 0)
				total = 0;
			txtExciseSubTotal.Text = Strings.Format(total, "#,##0.00");
			if (txtExciseTransferCharge.Text != "")
			{
				total += FCConvert.ToDouble(txtExciseTransferCharge.Text);
			}
			txtExciseTotal.Text = Strings.Format(total, "#,##0.00");
		}

		private void txtExciseTransferCharge_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			CalculateTotals();
		}
	}
}
