//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmVehicleAddressList : BaseForm
	{
		public frmVehicleAddressList()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			//FC:FINAL:DDU:#2223 - allow only numerics in these textboxes
			txtLow.AllowOnlyNumericInput();
			txtHigh.AllowOnlyNumericInput();
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmVehicleAddressList InstancePtr
		{
			get
			{
				return (frmVehicleAddressList)Sys.GetInstance(typeof(frmVehicleAddressList));
			}
		}

		protected frmVehicleAddressList _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int SelectCol;
		int ClassCol;
		int PlateCol;
		int NameCol;
		int AddressCol;
		clsDRWrapper rsResults = new clsDRWrapper();

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			int lngLow;
			int lngHigh;
			if (Conversion.Val(txtLow.Text) > Conversion.Val(txtHigh.Text))
			{
				MessageBox.Show("Your beginnign street number must be less than or equal to your ending street number.", "Invalid Street Number Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtLow.Focus();
				return;
			}
			if (fecherFoundation.Strings.Trim(txtStreetName.Text) == "")
			{
				MessageBox.Show("You must enter a street name before you may continue.", "Invalid Street Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStreetName.Focus();
				return;
			}
			lngLow = FCConvert.ToInt32(Math.Round(Conversion.Val(txtLow.Text)));
			lngHigh = FCConvert.ToInt32(Math.Round(Conversion.Val(txtHigh.Text)));
			rsResults.OpenRecordset("SELECT * FROM Master WHERE upper(Address) LIKE '%" + fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(txtStreetName.Text)) + "%'");
			LoadGrid(ref lngLow, ref lngHigh);
		}

		private void frmVehicleAddressList_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmVehicleAddressList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVehicleAddressList properties;
			//frmVehicleAddressList.FillStyle	= 0;
			//frmVehicleAddressList.ScaleWidth	= 9045;
			//frmVehicleAddressList.ScaleHeight	= 7350;
			//frmVehicleAddressList.LinkTopic	= "Form2";
			//frmVehicleAddressList.LockControls	= -1  'True;
			//frmVehicleAddressList.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			SelectCol = 0;
			ClassCol = 1;
			PlateCol = 2;
			NameCol = 3;
			AddressCol = 4;
			vsResults.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsResults.ColAlignment(PlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.TextMatrix(0, ClassCol, "Class");
			vsResults.TextMatrix(0, PlateCol, "Plate");
			vsResults.TextMatrix(0, NameCol, "Name");
			vsResults.TextMatrix(0, AddressCol, "Address");
			vsResults.ColWidth(SelectCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.05));
			vsResults.ColWidth(ClassCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.06));
			vsResults.ColWidth(PlateCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.12));
			vsResults.ColWidth(NameCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.3));
			vsResults.ColWidth(AddressCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.3));
			//modGlobalFunctions.SetFixedSize(ref this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmVehicleAddressList_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmVehicleAddressList_Resize(object sender, System.EventArgs e)
		{
			vsResults.ColWidth(SelectCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.05));
			vsResults.ColWidth(ClassCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.06));
			vsResults.ColWidth(PlateCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.12));
			vsResults.ColWidth(NameCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.3));
			vsResults.ColWidth(AddressCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.3));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			bool blnData = false;
			if (vsResults.Rows > 1)
			{
				blnData = false;
				for (counter = 1; counter <= (vsResults.Rows - 1); counter++)
				{
					if (FCConvert.ToBoolean(vsResults.TextMatrix(counter, SelectCol)) == true)
					{
						blnData = true;
						break;
					}
				}
				if (blnData)
				{
					frmReportViewer.InstancePtr.Init(rptVehicleAddressList.InstancePtr);
				}
				else
				{
					MessageBox.Show("You must select at least one record before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			bool blnData = false;
			if (vsResults.Rows > 1)
			{
				blnData = false;
				for (counter = 1; counter <= (vsResults.Rows - 1); counter++)
				{
					if (FCConvert.ToBoolean(vsResults.TextMatrix(counter, SelectCol)) == true)
					{
						blnData = true;
						break;
					}
				}
				if (blnData)
				{
					rptVehicleAddressList.InstancePtr.PrintReport(true);
				}
				else
				{
					MessageBox.Show("You must select at least one record before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void txtHigh_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9 && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLow_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9 && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void LoadGrid(ref int lngLow, ref int lngHigh)
		{
			string strStreetNumber = "";
			// vbPorter upgrade warning: intHolder As int	OnWriteFCConvert.ToInt32(
			int intHolder = 0;
			vsResults.Rows = 1;
			if (rsResults.EndOfFile() != true && rsResults.BeginningOfFile() != true)
			{
				do
				{
					if (lngLow == 0 && lngHigh == 0)
					{
						vsResults.Rows += 1;
						vsResults.TextMatrix(vsResults.Rows - 1, SelectCol, FCConvert.ToString(true));
						vsResults.TextMatrix(vsResults.Rows - 1, ClassCol, FCConvert.ToString(rsResults.Get_Fields_String("Class")));
						vsResults.TextMatrix(vsResults.Rows - 1, PlateCol, FCConvert.ToString(rsResults.Get_Fields_String("Plate")));
						vsResults.TextMatrix(vsResults.Rows - 1, NameCol, fecherFoundation.Strings.Trim(MotorVehicle.GetPartyName(rsResults.Get_Fields_Int32("PartyID1"), true)));
						vsResults.TextMatrix(vsResults.Rows - 1, AddressCol, FCConvert.ToString(rsResults.Get_Fields_String("Address")));
					}
					else
					{
						intHolder = Strings.InStr(1, fecherFoundation.Strings.Trim(FCConvert.ToString(rsResults.Get_Fields_String("Address"))), " ", CompareConstants.vbBinaryCompare);
						if (intHolder > 0)
						{
							strStreetNumber = fecherFoundation.Strings.Trim(Strings.Left(fecherFoundation.Strings.Trim(FCConvert.ToString(rsResults.Get_Fields_String("Address"))), intHolder));
							if (Information.IsNumeric(strStreetNumber))
							{
								if (Conversion.Val(strStreetNumber) >= lngLow && Conversion.Val(strStreetNumber) <= lngHigh)
								{
									vsResults.Rows += 1;
									vsResults.TextMatrix(vsResults.Rows - 1, SelectCol, FCConvert.ToString(true));
									vsResults.TextMatrix(vsResults.Rows - 1, ClassCol, FCConvert.ToString(rsResults.Get_Fields_String("Class")));
									vsResults.TextMatrix(vsResults.Rows - 1, PlateCol, FCConvert.ToString(rsResults.Get_Fields_String("Plate")));
									vsResults.TextMatrix(vsResults.Rows - 1, NameCol, fecherFoundation.Strings.Trim(MotorVehicle.GetPartyName(rsResults.Get_Fields_Int32("PartyID1"), true)));
									vsResults.TextMatrix(vsResults.Rows - 1, AddressCol, FCConvert.ToString(rsResults.Get_Fields_String("Address")));
								}
							}
						}
					}
					rsResults.MoveNext();
				}
				while (rsResults.EndOfFile() != true);
			}
		}

		private void txtStreetName_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void cmdClearAll_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsResults.Rows - 1); counter++)
			{
				vsResults.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsResults.Rows - 1); counter++)
			{
				vsResults.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
			}
		}

		private void vsResults_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsResults.Row > 0)
			{
				if (vsResults.TextMatrix(vsResults.Row, SelectCol) == "0")
				{
					vsResults.TextMatrix(vsResults.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsResults.TextMatrix(vsResults.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsResults_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				if (vsResults.Row > 0)
				{
					KeyCode = 0;
					if (vsResults.TextMatrix(vsResults.Row, SelectCol) == "0")
					{
						vsResults.TextMatrix(vsResults.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsResults.TextMatrix(vsResults.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}
	}
}
