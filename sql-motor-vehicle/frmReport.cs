//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Diagnostics;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.Collections.Generic;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmReport : BaseForm
	{
		public frmReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.Check1 = new System.Collections.Generic.List<FCCheckBox>();
			this.Check1.AddControlArrayElement(Check1_0, 0);
			this.Check1.AddControlArrayElement(Check1_1, 1);
			this.Check1.AddControlArrayElement(Check1_2, 2);
			this.Check1.AddControlArrayElement(Check1_3, 3);
			this.Check1.AddControlArrayElement(Check1_4, 4);
			this.Check1.AddControlArrayElement(Check1_5, 5);
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmReport InstancePtr
		{
			get
			{
				return (frmReport)Sys.GetInstance(typeof(frmReport));
			}
		}

		protected frmReport _InstancePtr = null;
		//=========================================================
		int lngMVR3Count;
		int lngSingleStickerCount;
		int lngDoubleStickerCount;
		int lngCombinationStickerCount;
		int lngDecalCount;
		int lngBoosterCount;
		int lngSpecialPermitCount;
		int lngTransitCount;
		int TOTCNTR;
		Decimal TOTAMT;
		string CLCAT = "";
		clsDRWrapper rsResCode = new clsDRWrapper();
		Decimal curSubTotal;
		Decimal curGrand;
		decimal TotalUnits;
		double dblTitleFee;
		public string strResCode = "";
		clsDRWrapper rs = new clsDRWrapper();
		string strSql;
		string strLine = new string('\0', 80);
		int PageCount;
		bool blnDisplay;
		int LineCount;
		string strHeading;
		string strMuni;
		clsDRWrapper rs1 = new clsDRWrapper();
		clsDRWrapper rsA = new clsDRWrapper();
		clsDRWrapper rsB = new clsDRWrapper();
		clsDRWrapper rsC = new clsDRWrapper();
		int lngPK;
		int lngEndPK;
		clsDRWrapper rsPC1 = new clsDRWrapper();
		clsDRWrapper rsAM = new clsDRWrapper();
		clsDRWrapper rsTDS = new clsDRWrapper();
		public clsDRWrapper rsClass_AutoInitialized = null;
		private string reportFolder =  "BMVReports";
		public clsDRWrapper rsClass
		{
			get
			{
				if ( rsClass_AutoInitialized == null)
				{
					 rsClass_AutoInitialized = new clsDRWrapper();
				}
				return rsClass_AutoInitialized;
			}
			set
			{
				 rsClass_AutoInitialized = value;
			}
		}
		clsDRWrapper rsCategory = new clsDRWrapper();

		public clsDRWrapper rsGVW_AutoInitialized = null;
		public clsDRWrapper rsGVW
		{
			get
			{
				if ( rsGVW_AutoInitialized == null)
				{
					 rsGVW_AutoInitialized = new clsDRWrapper();
				}
				return rsGVW_AutoInitialized;
			}
			set
			{
				 rsGVW_AutoInitialized = value;
			}
		}
		clsDRWrapper rsInventoryMaster = new clsDRWrapper();
		int HOLDSCT;
		string HoldCL;
		string HoldSC;
		int HOLDWG;
		int HoldRG;
		int HoldLV;
		int HoldUnits;
		Decimal HoldFee;
		Decimal[,] curSummaryRecord = new Decimal[300 + 1, 1 + 1];
		FCFixedString strSummaryDetail = new FCFixedString(1);
		bool bolInventoryReconciliation;
		DateTime DateTime;
		int lngSOP;
		int lngEOP;
		int lngAR;
		int lngVI;
		int lngAA;
		int WorkSCT;
		string WorkCL;
		string WorkSC;
		int WorkWG;
		int WorkRG;
		int WorkLV;
		int WorkUnits;
		Decimal WorkFee;
		int[] lngTotal = new int[20 + 1];
		bool AllFlag;
		int MonCorrTotal;
		int NonMonCorrTotal;
		float MonCorrMoneyTotal;
		int NoFeeDupRegTotal;
		int intCopies;
		int intPrintCounter;
		int lngSearchID;
		string strYStickerCode = "";
		string strMStickerCode = "";
		string strPlateCode = "";
		public string ReportPrinter = "";
		public int lngInventoryCounter;
		public string strReportPrinting = "";

		private void cmdCloseout_Click(object sender, System.EventArgs e)
		{
			int lngID = 0;
			DialogResult GetOut;
			clsDRWrapper rsUser = new clsDRWrapper();
			clsDRWrapper rsTellers = new clsDRWrapper();
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(Conversion.Val(rsUser.Get_Fields_String("Level"))) > 2)
				{
					MessageBox.Show("You do not have a high enough Operator Level to Close out a Period.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to Close out a Period.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			GetOut = MessageBox.Show("Please be sure that all users are out of the Motor Vehicle System before Closing Out Period.", "Everyone Out of Motor Vehicle", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
			if (GetOut == DialogResult.Cancel)
			{
				MessageBox.Show("Process was stopped by you.", "Process Not Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			fecherFoundation.Information.Err().Clear();
			bool executePeriodEndError = false;
			try
			{
				fecherFoundation.Information.Err().Clear();
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Closing Period";
				frmWait.InstancePtr.Refresh();
				FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
				rsPC1.OpenRecordset("SELECT * FROM PeriodCloseout");
				DateTime = DateTime.Now;
				rsPC1.AddNew();
				rsPC1.Set_Fields("OpID", MotorVehicle.Statics.OpID);
				rsPC1.Set_Fields("IssueDate", DateTime.ToString("MM/dd/yyyy HH:mm:ss"));
				cboStart.AddItem(DateTime.ToString());
				cboEnd.AddItem(DateTime.ToString());
				cboStart.Refresh();
				cboEnd.Refresh();
				cboStart.SelectedIndex = 0;
				cboEnd.SelectedIndex = 0;
				rsPC1.Update();
				lngID = rsPC1.Get_Fields_Int32("ID");
				if (MotorVehicle.Statics.blnUseTellerCloseout)
				{
					CloseoutTellers();
					SetPeriodCloseoutID(ref lngID);
				}
				strSql = "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1";
				if (SetCloseoutID(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				strSql = "SELECT * FROM SpecialRegistration WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				strSql = "SELECT * FROM TransitPlates WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				strSql = "SELECT * FROM Booster WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				strSql = "SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND AdjustmentCode <> 'P' AND InventoryType <> 'PXSLT'";
				if (SetCloseoutID(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				strSql = "SELECT * FROM ExciseTax WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				strSql = "SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				strSql = "SELECT * FROM TitleApplications WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				strSql = "SELECT * FROM TemporaryPlateRegister WHERE PeriodCloseoutID < 1";
				if (SetCloseoutID(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				strSql = "SELECT * FROM CloseoutInventory";
				if (PerformInventoryCloseout(ref strSql, ref lngID) == false)
				{
					executePeriodEndError = true;
					goto PeriodEndError;
				}
				rsPC1.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngID));
				rsPC1.Edit();
				rsPC1.Set_Fields("IssueDate", DateTime.ToString("MM/dd/yyyy HH:mm:ss"));
				rsPC1.Update();
				rsPC1.Reset();
				Set_Newest_Date_In_CBO();
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = 0;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				executePeriodEndError = true;
				goto PeriodEndError;
			}
			PeriodEndError:
			if (executePeriodEndError)
			{
				string strErrMessage;
				strErrMessage = "Period Closeout was not successful.  Error number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + " was encountered.  (" + fecherFoundation.Information.Err().Description + ")";
				rsPC1.FindFirstRecord("ID", lngID);
				rsPC1.Delete();
				rsPC1.Update();
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = 0;
				MessageBox.Show(strErrMessage, "Error processing closeout.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				fecherFoundation.Information.Err().Clear();
				executePeriodEndError = false;
			}
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			cmdPrint.Enabled = true;
			fraInventoryOptions.Visible = false;
		}

		private void cmdOKList_Click(object sender, System.EventArgs e)
		{
			cmdPrint.Enabled = true;
			cmbReport.Enabled = true;
			cmbInterim.Enabled = true;
			fraClose.Enabled = true;
			fraListOptions.Visible = false;
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{ 
				PrintRoutines();
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show(ex.ToString(), "Print Routines", MessageBoxButtons.OK, MessageBoxIcon.Error);
				frmReport.InstancePtr.Cursor = Wisej.Web.Cursors.Default;
				return;
			}
		}

		private void cmdSaveDates_Click(object sender, System.EventArgs e)
		{
			int lngStart = 0;
			int lngEnd = 0;
			rs.OpenRecordset("SELECT * FROM PeriodCloseout ORDER BY IssueDate DESC");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				rs.FindFirstRecord("IssueDate", cboStart.Text);
				if (rs.NoMatch == false)
					lngStart = rs.Get_Fields_Int32("ID");
				rs.FindFirstRecord("IssueDate", cboEnd.Text);
				if (rs.NoMatch == false)
					lngEnd = rs.Get_Fields_Int32("ID");
				MotorVehicle.UpdateMVVariable("ReportStartIndex", modGlobalRoutines.PadToString(lngStart, 4));
				MotorVehicle.UpdateMVVariable("ReportEndIndex", modGlobalRoutines.PadToString(lngEnd, 4));
			}
			rs.Reset();
			MessageBox.Show("Dates saved successfully!", "Info Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void frmReport_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.ENDOFPERIOD))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			strMuni = fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName);
			rtf1.Visible = true;
			rtf1.Visible = false;
		}

		

		private void frmReport_Load(object sender, System.EventArgs e)
		{
			int intStart = 0;
			int intEnd = 0;
			bool bolStart = false;
			bool bolEnd = false;
            FCFileSystem ff = new FCFileSystem();
			GetResCode();
			rs.OpenRecordset("SELECT * FROM PeriodCloseout ORDER BY ID DESC");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				if (Conversion.Val(MotorVehicle.GetMVVariable("ReportStartIndex")) != 0)
					intStart = FCConvert.ToInt32(Math.Round(Conversion.Val(MotorVehicle.GetMVVariable("ReportStartIndex"))));
				if (Conversion.Val(MotorVehicle.GetMVVariable("ReportEndIndex")) != 0)
					intEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(MotorVehicle.GetMVVariable("ReportEndIndex"))));
				while (!rs.EndOfFile())
				{
					cboStart.AddItem(rs.Get_Fields_String("IssueDate"));
					cboEnd.AddItem(rs.Get_Fields_String("IssueDate"));
					if (bolStart == false)
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("ID")) == intStart)
						{
							intStart = cboStart.Items.Count - 1;
							bolStart = true;
						}
					}
					if (bolEnd == false)
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("ID")) == intEnd)
						{
							intEnd = cboEnd.Items.Count - 1;
							bolEnd = true;
						}
					}
					rs.MoveNext();
				}
				if (intStart > cboStart.Items.Count - 1)
				{
					// Do Nothing
				}
				else
				{
					if (bolStart == true)
					{
						cboStart.SelectedIndex = intStart;
					}
					else
					{
						if (cboStart.Items.Count > 1)
						{
							cboStart.SelectedIndex = 1;
						}
						else if (cboStart.Items.Count > 0)
						{
							cboStart.SelectedIndex = 0;
						}
					}
				}
				if (intEnd > cboEnd.Items.Count - 1)
				{
					// Do Nothing
				}
				else
				{
					if (bolEnd == true)
					{
						cboEnd.SelectedIndex = intEnd;
					}
					else
					{
						cboEnd.SelectedIndex = 0;
					}
				}
			}
			rs.Reset();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			rs.Reset();
			rsPC1.Reset();
			rsAM.Reset();
			rsTDS.Reset();
			rsClass.Reset();
			rsCategory.Reset();
			rsGVW.Reset();
			rsInventoryMaster.Reset();
			rs1.Reset();
			rsA.Reset();
			rsB.Reset();
			rsC.Reset();
			strSql = "";
			strLine = "";
			PageCount = 0;
			LineCount = 0;
			strHeading = "";
			strMuni = "";
			lngPK = 0;
			HOLDSCT = 0;
			HoldCL = "";
			HoldSC = "";
			HOLDWG = 0;
			HoldRG = 0;
			HoldLV = 0;
			HoldUnits = 0;
			HoldFee = 0;
			FCUtils.EraseSafe(curSummaryRecord);
			strSummaryDetail.Value = "";
			bolInventoryReconciliation = false;
			DateTime = DateTime.FromOADate(0);
			lngSOP = 0;
			lngEOP = 0;
			lngAR = 0;
			lngVI = 0;
			lngAA = 0;
			WorkSCT = 0;
			WorkCL = FCConvert.ToString(0);
			WorkSC = FCConvert.ToString(0);
			WorkWG = 0;
			WorkRG = 0;
			WorkLV = 0;
			WorkUnits = 0;
			WorkFee = 0;
			FCUtils.EraseSafe(lngTotal);
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 5; counter++)
			{
				Check1[counter].CheckState = Wisej.Web.CheckState.Unchecked;
			}
			FraSelect.Enabled = false;
		}

		private void optInterim_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				fraSetDates.Enabled = false;
				cboEnd.Enabled = false;
				cboStart.Enabled = false;
				cmdSaveDates.Enabled = false;
				Label1.Enabled = false;
				Label2.Enabled = false;
				cmdCloseout.Enabled = false;
				if (cmbReport.Items.Contains("Inventory Report"))
				{
					cmbReport.Items.Remove("Inventory Report");
				}

				btnFullSetOfReports.Enabled = false;
				btnCreteBMVDataFiles.Enabled = false;
			}
			else
			{
				fraSetDates.Enabled = true;
				cboEnd.Enabled = true;
				cboStart.Enabled = true;
				cmdSaveDates.Enabled = true;
				Label1.Enabled = true;
				Label2.Enabled = true;
				cmdCloseout.Enabled = true;
				if (!cmbReport.Items.Contains("Inventory Report"))
				{
					cmbReport.Items.Insert(3, "Inventory Report");
				}
				btnFullSetOfReports.Enabled = true;
				btnCreteBMVDataFiles.Enabled = true;
			}
		}

		private void optInterim_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbInterim.SelectedIndex;
			optInterim_CheckedChanged(index, sender, e);
		}

		public void PrintRoutines()
		{
			clsDRWrapper rsDel = new clsDRWrapper();
			try
			{
				fecherFoundation.Information.Err().Clear();
				rtf1.Text = "";
				strSummaryDetail.Value = "";
				frmReport.InstancePtr.MousePointer = MousePointerConstants.vbHourglass;
				
				if (cmbReport.Text == "Town Summary")
				{
					strReportPrinting = "TS";
					strSummaryDetail.Value = "S";
					rsDel.Execute("DELETE FROM TownDetailSummary", "TWMV0000.vb1");
					FillTownDetailSummaryTable();
					TownSummary();
				}
				else if (cmbReport.Text == "Town Detail")
				{
					strReportPrinting = "TD";
					strSummaryDetail.Value = "D";
					rsDel.Execute("DELETE FROM TownDetailSummary", "TWMV0000.vb1");
					FillTownDetailSummaryTable();
					TownDetail();
				}
				else if (cmbReport.Text == "Inventory Report")
				{
					strReportPrinting = "IR";
					InventoryReport();
				}
				else if (cmbReport.Text == "Inventory Adjustment Report")
				{
					strReportPrinting = "IA";
					InventoryAdjustments();
				}
				else if (cmbReport.Text == "Voided Forms Report")
				{
					strReportPrinting = "VD";
					VoidedMVR3();
				}
				else if (cmbReport.Text == "Excise Tax Only Report")
				{
					strReportPrinting = "ET";
					ExciseTaxOnly();
				}
				else if (cmbReport.Text == "Exception Report")
				{
					strReportPrinting = "EX";
					ExceptionReport();
				}
				else if (cmbReport.Text == "Title Application Report")
				{
					strReportPrinting = "TA";
					TitleApplications();
				}
				else if (cmbReport.Text == "Gift Cert / Voucher Report")
				{
					strReportPrinting = "GC";
					GiftCertVouchers();
				}
				else if (cmbReport.Text == "Use Tax Summary Report")
				{
					strReportPrinting = "UT";
					UseTaxSummary();
				}
				else if (cmbReport.Text == "Registration Listing")
				{
					RegistrationListing();
				}
				rtf1.SelectionStart = 0;
				rtf1.SelectionLength = 1;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show(ex.ToString(), "Print Routines", MessageBoxButtons.OK, MessageBoxIcon.Error);
                frmReport.InstancePtr.MousePointer = MousePointerConstants.vbDefault;
			}
        }

		public void VoidedMVR3(List<string> batchReports = null)
		{
			int lngPK1;
			int lngPK2;
			int intHeadingNumber;
			// 
			LineCount = 0;
			PageCount = 0;
			// 
			LineCount = 0;
			PageCount = 0;
			//rtf1.RightMargin = 5;
			strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSql);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = rs.Get_Fields_Int32("ID");
				rs.MoveFirst();
				lngPK2 = rs.Get_Fields_Int32("ID");
			}
			// 
			if (cmbInterim.Text == "Interim Reports")
			{
				rs.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE (InventoryType = 'MXS00' OR InventoryType = 'BXSXX' OR InventoryType = 'RXSXX' OR InventoryType = 'PXSXX') AND AdjustmentCode = 'V' AND PeriodCloseoutID < 1 ORDER BY InventoryType, Low");
			}
			else
			{
				strSql = "SELECT * FROM InventoryAdjustments WHERE (InventoryType = 'MXS00' OR InventoryType = 'BXSXX' OR InventoryType = 'RXSXX' OR InventoryType = 'PXSXX') AND AdjustmentCode = 'V' AND PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " ORDER BY InventoryType, Low";
				rs.OpenRecordset(strSql);
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** VOIDED FORMS REPORT ****";
				PrintHeading(PageCount);
				while (!rs.EndOfFile())
				{
					strLine = Strings.StrDup(80, " ");
					Strings.MidSet(ref strLine, 1, 10, Strings.Format(rs.Get_Fields_DateTime("DateOfAdjustment"), "MM/dd/yyyy"));
					if (rs.Get_Fields_String("InventoryType") == "MXS00")
					{
						Strings.MidSet(ref strLine, 12, 5, "MVR3 ");
					}
					else if (rs.Get_Fields_String("InventoryType") == "PXSXX")
					{
						Strings.MidSet(ref strLine, 12, 5, "TRANS");
					}
					else if (rs.Get_Fields_String("InventoryType") == "BXSXX")
					{
						Strings.MidSet(ref strLine, 12, 5, "BOOST");
					}
					else if (rs.Get_Fields_String("InventoryType") == "RXSXX")
					{
						Strings.MidSet(ref strLine, 12, 5, "MVR10");
					}
					Strings.MidSet(ref strLine, 17, 9, Strings.Format(Strings.Format(rs.Get_Fields_Int32("QuantityAdjusted") * -1, "#########"), "@@@@@@@@@"));
					Strings.MidSet(ref strLine, 28, 17, Strings.Format(rs.Get_Fields("Low"), "00000000") + "-" + Strings.Format(rs.Get_Fields("High"), "00000000"));
					Strings.MidSet(ref strLine, 49, 3, rs.Get_Fields_String("OpID") + Strings.StrDup(3 - FCConvert.ToString(rs.Get_Fields_String("OpID")).Length, " "));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("reason"))) != "" && FCConvert.ToString(rs.Get_Fields_String("reason")).Length <= 23)
					{
						Strings.MidSet(ref strLine, 57, 23, fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("reason")))) + Strings.StrDup(23 - fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("reason"))).Length, " "));
					}
					else if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("reason"))) != "")
					{
						Strings.MidSet(ref strLine, 57, 23, fecherFoundation.Strings.UCase(Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("reason")), 1, 23)));
					}
					rtf1.Text = rtf1.Text + strLine + "\r\n";
					LineCount += 1;
					rs.MoveNext();
				}
				// print page totals here
			}
			else
			{
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** VOIDED FORMS REPORT ****";
				PrintHeading(1);
				rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
				strLine = (Strings.StrDup(33, " ") + "No Information");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
			}
			if (rtf1.Text != "")
			{
				FCFileSystem.FileOpen(33,Path.Combine("BMVReports", strResCode + "VF.RPT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.PrintLine(33, rtf1.Text);
				FCFileSystem.FileClose(33);
			}
			if (AllFlag)
			{
				rptVoidedMVR3.InstancePtr.PrintReport(false, batchReports: batchReports);
				rptVoidedMVR3.InstancePtr.Unload();
			
				CheckIt:
				;
				if (MotorVehicle.JobComplete("rptVoidedMVR3"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckIt;
				}
			}
			else
			{
				rptVoidedMVR3.InstancePtr.Unload();
				frmReportViewer.InstancePtr.Init(rptVoidedMVR3.InstancePtr);
			}
			rs.Reset();
		}

		public void TitleApplications(List<string> batchReports = null)
		{
			int lngPK1;
			int lngPK2;
			int intHeadingNumber;
			clsDRWrapper rs2 = new clsDRWrapper();
			double MoneyTotal = 0;
			int total = 0;
			clsDRWrapper rsAM = new clsDRWrapper();
			rs2.Reset();
			LineCount = 0;
			PageCount = 0;
			strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSql);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = rs.Get_Fields_Int32("ID");
				rs.MoveFirst();
				lngPK2 = rs.Get_Fields_Int32("ID");
			}
			if (cmbInterim.Text == "Interim Reports")
			{
				// kk01162018 tromv-1292  Added NoFeeCTA to Title App Report Query
				strSql = "SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 < 1 AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND (TitleDone = 1 or TitleFee <> 0 or NoFeeCTA = 1) and rtrim(CTANumber) <> '' ORDER BY CTANumber";
			}
			else
			{
				strSql = "SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " And OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND (TitleDone = 1 or TitleFee <> 0 or NoFeeCTA = 1) and rtrim(CTANumber) <> '' ORDER BY CTANumber";
			}
			rsAM.OpenRecordset(strSql);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				rsAM.MoveLast();
				rsAM.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** TITLE APPLICATION SUMMARY REPORT ****";
				PrintHeading(PageCount);
				while (!rsAM.EndOfFile())
				{
					total += 1;
					MoneyTotal += rsAM.Get_Fields_Double("TitleFee");
					strLine = Strings.StrDup(80, " ");
					Strings.MidSet(ref strLine, 1, 10, Strings.Format(rsAM.Get_Fields_String("CTANumber").ToUpper(), "!@@@@@@@@@@"));
					Strings.MidSet(ref strLine, 12, 5, Strings.Format(rsAM.Get_Fields("TitleFee"), "00.00"));
					Strings.MidSet(ref strLine, 18, 2, Strings.Format(rsAM.Get_Fields_String("Class"), "!@@"));
					Strings.MidSet(ref strLine, 21, 8, Strings.Format(rsAM.Get_Fields_String("plate"), "!@@@@@@@@"));
					Strings.MidSet(ref strLine, 31, 8, Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yy"));
					Strings.MidSet(ref strLine, 41, 33, Strings.Format(MotorVehicle.GetPartyNameMiddleInitial(rsAM.Get_Fields_Int32("PartyID1")), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"));
					// kk03062017 tromv-1246  Use name on CTA if filled in
					rs2.OpenRecordset("SELECT CustomerName FROM TitleApplications WHERE CTANumber = '" + rsAM.Get_Fields_String("CTANumber") + "' " +
					                  "AND Plate = '" + rsAM.Get_Fields_String("Plate") + "' AND PeriodCloseoutID = " + rsAM.Get_Fields_Int32("OldMVR3"));
					if (!rs2.EndOfFile())
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("CustomerName"))) != "")
						{
							Strings.MidSet(ref strLine, 41, 33, Strings.Format(rs2.Get_Fields_String("CustomerName"), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"));
						}
					}
					Strings.MidSet(ref strLine, 76, 3, Strings.Format(rsAM.Get_Fields_String("OpID"), "@@@"));
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount += 1;
					if (fecherFoundation.FCUtils.IsNull(rsAM.Get_Fields_String("DoubleCTANumber")) == false && rsAM.Get_Fields_String("DoubleCTANumber").Length != 0 && rsAM.Get_Fields_Int32("DoubleCTANumber") != 0)
					{
						// kk03082017 tromv-1246  Rework double cta to not cause RTE if no record for double cta
						total += 1;
						strLine = Strings.StrDup(80, " ");
						Strings.MidSet(ref strLine, 1, 10, Strings.Format(rsAM.Get_Fields_String("DoubleCTANumber").ToUpper(), "!@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 12, 5, "  N/A");
						Strings.MidSet(ref strLine, 18, 10, "- DOUBLE -");
						Strings.MidSet(ref strLine, 31, 8, Strings.Format(rsAM.Get_Fields_String("CTANumber"), "!@@@@@@@@"));
						rs2.OpenRecordset("SELECT Customer FROM DoubleCTA WHERE CTANumber = '" + rsAM.Get_Fields_String("DoubleCTANumber") + "'");
						if (rs2.EndOfFile() != true)
						{
							if (rs2.Get_Fields_String("Customer").Length <= 33)
							{
								Strings.MidSet(ref strLine, 41, 33, Strings.Format(rs2.Get_Fields_String("Customer"), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"));
							}
							else
							{
								Strings.MidSet(ref strLine, 41, 33, Strings.Mid(FCConvert.ToString(rs2.Get_Fields_String("Customer")), 1, 33));
							}
						}
						Strings.MidSet(ref strLine, 76, 3, Strings.Format(rsAM.Get_Fields_String("OpID"), "@@@"));
						rtf1.Text = rtf1.Text + strLine;
						rtf1.Text = rtf1.Text + "\r\n";
						LineCount += 1;
					}
					rsAM.MoveNext();
				}
				rtf1.Text = rtf1.Text + "\r\n";
				strLine = Strings.StrDup(80, " ");
				Strings.MidSet(ref strLine, 1, 10, "Totals");
				Strings.MidSet(ref strLine, 11, 6, Strings.Format(Strings.Format(MoneyTotal, "#,#00.00"), "@@@@@@"));
				Strings.MidSet(ref strLine, 18, 2, Strings.Format(total, "@@"));
				rtf1.Text = rtf1.Text + strLine;
				rtf1.Text = rtf1.Text + "\r\n";
			}
			else
			{
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** TITLE APPLICATION SUMMARY REPORT ****";
				PrintHeading(1);
				rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
				strLine = (Strings.StrDup(33, " ") + "No Information");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
			}
			if (rtf1.Text != "")
			{
				FCFileSystem.FileOpen(33, Path.Combine("BMVReports", strResCode + "TA.RPT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.PrintLine(33, rtf1.Text);
				FCFileSystem.FileClose(33);
			}
			if (AllFlag)
			{
				rptTitleApplication.InstancePtr.PrintReport(false, batchReports: batchReports);
				rptTitleApplication.InstancePtr.Unload();
			
				CheckIt:
				;
				if (MotorVehicle.JobComplete("rptTitleApplication"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckIt;
				}
			}
			else
			{
				rptTitleApplication.InstancePtr.Unload();
				frmReportViewer.InstancePtr.Init(rptTitleApplication.InstancePtr);
			}

			rs.Reset();
		}

        public void UseTaxSummary(List<string> batchReports = null)
		{
			int lngPK1;
			int lngPK2;
			int intHeadingNumber;
			clsDRWrapper rs2 = new clsDRWrapper();
			double MoneyTotal = 0;
			int FeeCount;
			int NoFeeCount;
			int total = 0;
			clsDRWrapper rsAM = new clsDRWrapper();
			rs2.Reset();
			LineCount = 0;
			PageCount = 0;
			//rtf1.RightMargin = 5;
			strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSql);
			lngPK1 = 0;
			lngPK2 = 0;
			FeeCount = 0;
			NoFeeCount = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = rs.Get_Fields_Int32("ID");
				rs.MoveFirst();
				lngPK2 = rs.Get_Fields_Int32("ID");
			}
			if (cmbInterim.Text == "Interim Reports")
			{
				// kk02242017 tromv-1242  Added check for SalesTaxExempt to pick up when Use Tax form wasn't done and exclude Dealer sales tax
				strSql = "SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 < 1 AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND DealerSalesTax <> 1 AND (UseTaxDone = 1 or SalesTax <> 0 or SalesTaxExempt = 1) ORDER BY DateUpdated";
			}
			else
			{
				strSql = "SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " And OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND DealerSalesTax <> 1 AND (UseTaxDone = 1 or SalesTax <> 0 or SalesTaxExempt = 1) ORDER BY DateUpdated";
			}
			rsAM.OpenRecordset(strSql);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				rsAM.MoveLast();
				rsAM.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** USE TAX SUMMARY REPORT ****";
				PrintHeading(PageCount);
				while (!rsAM.EndOfFile())
				{
					total += 1;
					MoneyTotal += rsAM.Get_Fields_Double("SalesTax") > 0 ? rsAM.Get_Fields_Double("SalesTax") : 0;
					if (rsAM.Get_Fields_Double("SalesTax") != 0)
					{
						FeeCount += 1;
					}
					else
					{
						NoFeeCount += 1;
					}
					strLine = Strings.StrDup(80, " ");
					if (rsAM.Get_Fields_Double("SalesTax") > 0)
					{
						Strings.MidSet(ref strLine, 2, 9, Strings.Format("PAID", "!@@@@@@@@@"));
					}
					else
					{
						Strings.MidSet(ref strLine, 2, 9, Strings.Format("NO FEE", "!@@@@@@@@@"));
					}
					Strings.MidSet(ref strLine, 12, 9, Strings.Format(rsAM.Get_Fields_Double("SalesTax") > 0 ? rsAM.Get_Fields_Double("SalesTax") : 0, "#,##0.00"));
					Strings.MidSet(ref strLine, 22, 2, Strings.Format(rsAM.Get_Fields_String("Class"), "!@@"));
					Strings.MidSet(ref strLine, 25, 8, Strings.Format(rsAM.Get_Fields_String("plate"), "!@@@@@@@@"));
					Strings.MidSet(ref strLine, 35, 8, Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yy"));
					Strings.MidSet(ref strLine, 45, 30, Strings.Format(MotorVehicle.GetPartyNameMiddleInitial(rsAM.Get_Fields_Int32("PartyID1")), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"));
					Strings.MidSet(ref strLine, 77, 3, Strings.Format(rsAM.Get_Fields_String("OpID"), "@@@"));
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount += 1;
					rsAM.MoveNext();
				}
				rtf1.Text = rtf1.Text + "\r\n";
				strLine = Strings.StrDup(80, " ");
				Strings.MidSet(ref strLine, 1, 48, "Totals:     Type         Units           Dollars");
				rtf1.Text = rtf1.Text + strLine;
				rtf1.Text = rtf1.Text + "\r\n";
				strLine = Strings.StrDup(80, " ");
				Strings.MidSet(ref strLine, 13, 6, "PAID");
				Strings.MidSet(ref strLine, 26, 3, Strings.Format(FeeCount, "@@@"));
				Strings.MidSet(ref strLine, 39, 10, Strings.Format(Strings.Format(MoneyTotal, "#,#00.00"), "@@@@@@@@@@"));
				rtf1.Text = rtf1.Text + strLine;
				rtf1.Text = rtf1.Text + "\r\n";
				strLine = Strings.StrDup(80, " ");
				Strings.MidSet(ref strLine, 13, 6, "NO FEE");
				Strings.MidSet(ref strLine, 26, 3, Strings.Format(NoFeeCount, "@@@"));
				Strings.MidSet(ref strLine, 39, 10, Strings.Format(Strings.Format(0, "#,#00.00"), "@@@@@@@@@@"));
				rtf1.Text = rtf1.Text + strLine;
				rtf1.Text = rtf1.Text + "\r\n";
				strLine = Strings.StrDup(80, " ");
			}
			else
			{
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** USE TAX SUMMARY REPORT ****";
				PrintHeading(1);
				rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
				strLine = (Strings.StrDup(33, " ") + "No Information");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
			}
			if (rtf1.Text != "")
			{
				FCFileSystem.FileOpen(33, Path.Combine("BMVReports", strResCode + "UT.RPT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.PrintLine(33, rtf1.Text);
				FCFileSystem.FileClose(33);
			}
			if (AllFlag)
			{
				rptUseTaxSummary.InstancePtr.PrintReport(false, batchReports: batchReports);
				rptUseTaxSummary.InstancePtr.Unload();
			
				CheckIt:
				;
				if (MotorVehicle.JobComplete("rptUseTaxSummary"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckIt;
				}
			}
			else
			{
				rptUseTaxSummary.InstancePtr.Unload();
				frmReportViewer.InstancePtr.Init(rptUseTaxSummary.InstancePtr);
			}

			rs.Reset();
		}

		public void GiftCertVouchers(List<string> batchReports = null)
		{
			int lngPK1;
			int lngPK2;
			int intHeadingNumber;
			clsDRWrapper rs2 = new clsDRWrapper();
			double MoneyTotal = 0;
			int total = 0;
			clsDRWrapper rsAM = new clsDRWrapper();
			int intType;
			bool blnData;
			rs2.Reset();
			intType = 1;
			LineCount = 0;
			PageCount = 0;
			blnData = false;
			//rtf1.RightMargin = 5;
			strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSql);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = rs.Get_Fields_Int32("ID");
				rs.MoveFirst();
				lngPK2 = rs.Get_Fields_Int32("ID");
			}
			if (cmbInterim.Text == "Interim Reports")
			{
				strSql = "SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID < 1 AND Issued = 1 ORDER BY CertificateNumber";
			}
			else
			{
				strSql = "SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " AND Issued = 1 ORDER BY CertificateNumber";
			}
			rsAM.OpenRecordset(strSql);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				blnData = true;
				rsAM.MoveLast();
				rsAM.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** GIFT CERTIFICATE REGISTER ****";
				PrintHeading(PageCount);
				rtf1.Text = rtf1.Text + strGiftCertificateHeading(ref intType) + "\r\n";
				while (!rsAM.EndOfFile())
				{
					total += 1;
					MoneyTotal += rsAM.Get_Fields_Double("Amount");
					strLine = Strings.StrDup(80, " ");
					Strings.MidSet(ref strLine, 2, 8, Strings.Format(rsAM.Get_Fields_String("CertificateNumber"), "!@@@@@@@@"));
					Strings.MidSet(ref strLine, 17, 8, Strings.Format(Strings.Format(rsAM.Get_Fields("Amount"), "#,#00.00"), "@@@@@@"));
					Strings.MidSet(ref strLine, 27, 30, FCConvert.ToString(rsAM.Get_Fields_String("Name")));
					Strings.MidSet(ref strLine, 59, 8, Strings.Format(rsAM.Get_Fields_DateTime("DateIssued"), "MM/dd/yy"));
					Strings.MidSet(ref strLine, 73, 3, Strings.Format(rsAM.Get_Fields_String("OpID"), "@@@"));
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount += 1;
					rsAM.MoveNext();
				}
				rtf1.Text = rtf1.Text + "\r\n";
				strLine = Strings.StrDup(80, " ");
				Strings.MidSet(ref strLine, 1, 20, "Issued Totals");
				Strings.MidSet(ref strLine, 21, 6, Strings.Format(Strings.Format(MoneyTotal, "#,#00.00"), "@@@@@@"));
				Strings.MidSet(ref strLine, 28, 2, Strings.Format(total, "@@"));
				rtf1.Text = rtf1.Text + strLine;
				rtf1.Text = rtf1.Text + "\r\n" + "\r\n" + "\r\n";
			}
			else
			{
				goto CheckRedeemed;
			}
			CheckRedeemed:
			;
			if (cmbInterim.Text == "Interim Reports")
			{
				strSql = "SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID < 1 AND Issued <> 1 ORDER BY CertificateNumber";
			}
			else
			{
				strSql = "SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " AND Issued <> 1 ORDER BY CertificateNumber";
			}
			intType = 2;
			MoneyTotal = 0;
			total = 0;
			rsAM.OpenRecordset(strSql);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				rsAM.MoveLast();
				rsAM.MoveFirst();
				if (!blnData)
				{
					PageCount = 1;
					strLine = Strings.StrDup(80, " ");
					strHeading = "**** GIFT CERTIFICATE REGISTER ****";
					PrintHeading(PageCount);
					blnData = true;
				}
				rtf1.Text = rtf1.Text + strGiftCertificateHeading(ref intType) + "\r\n";
				while (!rsAM.EndOfFile())
				{
					total += 1;
					MoneyTotal += rsAM.Get_Fields_Double("Amount");
					strLine = Strings.StrDup(80, " ");
					Strings.MidSet(ref strLine, 2, 8, Strings.Format(rsAM.Get_Fields_String("CertificateNumber"), "!@@@@@@@@"));
					Strings.MidSet(ref strLine, 17, 30, FCConvert.ToString(rsAM.Get_Fields_String("Name")));
					Strings.MidSet(ref strLine, 46, 2, Strings.Format(rsAM.Get_Fields_String("Class"), "!@@"));
					Strings.MidSet(ref strLine, 49, 8, Strings.Format(rsAM.Get_Fields_String("plate"), "!@@@@@@@@"));
					Strings.MidSet(ref strLine, 59, 8, Strings.Format(rsAM.Get_Fields_DateTime("DateRedeemed"), "MM/dd/yy"));
					Strings.MidSet(ref strLine, 75, 3, Strings.Format(rsAM.Get_Fields_String("OpID"), "@@@"));
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount += 1;
					rsAM.MoveNext();
				}
				rtf1.Text = rtf1.Text + "\r\n";
				strLine = Strings.StrDup(80, " ");
				Strings.MidSet(ref strLine, 1, 20, "Redeemed Totals");
				Strings.MidSet(ref strLine, 21, 6, Strings.Format(Strings.Format(MoneyTotal, "#,#00.00"), "@@@@@@"));
				Strings.MidSet(ref strLine, 28, 2, Strings.Format(total, "@@"));
				rtf1.Text = rtf1.Text + strLine;
				rtf1.Text = rtf1.Text + "\r\n";
			}
			else
			{
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** GIFT CERTIFICATE REGISTER ****";
				PrintHeading(1);
				rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
				strLine = (Strings.StrDup(33, " ") + "No Information");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
			}
			if (rtf1.Text != "")
			{
				FCFileSystem.FileOpen(33,Path.Combine("BMVReports", strResCode + "GC.RPT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.PrintLine(33, rtf1.Text);
				FCFileSystem.FileClose(33);
			}
			if (AllFlag)
			{
				rptGiftCertVoucher.InstancePtr.PrintReport(false, batchReports: batchReports);
				rptGiftCertVoucher.InstancePtr.Unload();
			
				CheckIt:
				;
				if (MotorVehicle.JobComplete("rptGiftCertVoucher"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckIt;
				}
			}
			else
			{
				rptGiftCertVoucher.InstancePtr.Unload();
				frmReportViewer.InstancePtr.Init(rptGiftCertVoucher.InstancePtr);
			}
			rs.Reset();
		}

		public void InventoryReport(List<string> batchReports = null)
		{
			int lngPK1;
			int lngPK2;
			int intYY;
			int intY1 = 0;
			int lng1 = 0;
			int lng2 = 0;
			int lngA;
			int lngB;
			int lngC;
			int lngD = 0;
			clsDRWrapper rsOnHand = new clsDRWrapper();
			clsDRWrapper rsInvAdjust = new clsDRWrapper();
			bool blnFound = false;
			clsDRWrapper rsNotFound = new clsDRWrapper();
			string strDisplayType = "";
			string strTC = "";
			// Inventory Type Check
			string strMM = "";
			// 
			bolInventoryReconciliation = true;
			LineCount = 0;
			PageCount = 0;
			lngInventoryCounter = 0;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Creating Inventory Report", true);
			strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSql);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = rs.Get_Fields_Int32("ID");
				lngPK = lngPK1;
				lng1 = lngPK1;
				rs.MoveFirst();
				lngPK2 = rs.Get_Fields_Int32("ID");
				lngEndPK = lngPK2;
				lng2 = lngPK2;
			}
			// 
			strSql = "SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK2) + " ORDER BY InventoryType, Low";
			rs.OpenRecordset(strSql);
			rsInvAdjust.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK2) + " ORDER BY InventoryType, Low");
			lngA = 0;
			lngB = 0;
			lngC = 0;
			bool executeCheckSingleStickers = false;
			if (cmbReport.Text == "Inventory Report")
			{
				if (cmbAll.Text != "Print All")
				{
					if (Check1[0].CheckState == Wisej.Web.CheckState.Unchecked)
					{
						executeCheckSingleStickers = true;
						goto CheckSingleStickers;
					}
				}
			}
			rs1.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE PeriodCloseoutID = " + FCConvert.ToString(lng2) + " AND Type = 'MXS00'");
			rsA.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' And (PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + ") AND InventoryType = 'MXS00'");
			if (rsA.EndOfFile() != true && rsA.BeginningOfFile() != true)
			{
				rsA.MoveLast();
				rsA.MoveFirst();
				while (!rsA.EndOfFile())
				{
					lngA += rsA.Get_Fields_Int32("QuantityAdjusted");
					rsA.MoveNext();
				}
			}
			rsB.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'I' And (PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + ") AND InventoryType = 'MXS00'");
			if (rsB.EndOfFile() != true && rsB.BeginningOfFile() != true)
			{
				rsB.MoveLast();
				rsB.MoveFirst();
				while (!rsB.EndOfFile())
				{
					lngB += rsB.Get_Fields_Int32("QuantityAdjusted");
					rsB.MoveNext();
				}
			}
			rsC.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'T' And (PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + ") AND InventoryType = 'MXS00'");
			if (rsC.EndOfFile() != true && rsC.BeginningOfFile() != true)
			{
				rsC.MoveLast();
				rsC.MoveFirst();
				while (!rsC.EndOfFile())
				{
					lngC += rsC.Get_Fields_Int32("QuantityAdjusted");
					rsC.MoveNext();
				}
			}
			rsC.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = 'MXS00' AND AdjustmentCode = 'V' AND (PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + ")");
			if (rsC.EndOfFile() != true && rsC.BeginningOfFile() != true)
			{
				rsC.MoveLast();
				rsC.MoveFirst();
				while (!rsC.EndOfFile())
				{
					lngC -= rsC.Get_Fields_Int32("QuantityAdjusted");
					rsC.MoveNext();
				}
			}
			rsC.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = 'MXS00' AND AdjustmentCode = 'A' AND (PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + ")");
			if (rsC.EndOfFile() != true && rsC.BeginningOfFile() != true)
			{
				rsC.MoveLast();
				rsC.MoveFirst();
				while (!rsC.EndOfFile())
				{
					lngC -= rsC.Get_Fields_Int32("QuantityAdjusted");
					rsC.MoveNext();
				}
				//if (lngC < 0)
				//{
				//	lngC *= -1;
				//}
			}
			lngD = 0;
			rsOnHand.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE PeriodCloseoutID = " + FCConvert.ToString(lng1) + " AND Type = 'MXS00'");
			if (rsOnHand.EndOfFile() != true && rsOnHand.BeginningOfFile() != true)
			{
				rsOnHand.MoveLast();
				rsOnHand.MoveFirst();
				lngD = 0;
				while (!rsOnHand.EndOfFile())
				{
					lngD += rsOnHand.Get_Fields_Int32("Number");
					rsOnHand.MoveNext();
				}
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
				PrintHeading(1);
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "MVR3---------------------------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				strInventoryReconciliation();
				rtf1.Text = rtf1.Text + "\r\n";
				// 
				rs1.FindFirstRecord("Type", "MXS00");
				if (rs1.NoMatch == false)
				{
					strLine = Strings.StrDup(80, " ");
					Strings.MidSet(ref strLine, 2, 10, Strings.Format(lngD, "@@@@@@@@@@"));
					Strings.MidSet(ref strLine, 18, 10, Strings.Format(lngA, "@@@@@@@@@@"));
					Strings.MidSet(ref strLine, 34, 10, Strings.Format(lngB, "@@@@@@@@@@"));
					Strings.MidSet(ref strLine, 50, 10, Strings.Format(lngC, "@@@@@@@@@@"));
					Strings.MidSet(ref strLine, 66, 10, Strings.Format(rs1.Get_Fields_Int32("Number"), "@@@@@@@@@@"));
					if (lngD + lngA - lngB + lngC != rs1.Get_Fields_Int32("Number"))
					{
						Strings.MidSet(ref strLine, 77, 2, "**");
					}
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
				}
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "ITEMIZED LISTING----------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				// *******   MVR3'S   *******
				rs.MoveFirst();
				int temp;
				while (!rs.EndOfFile())
				{
					strLine = Strings.StrDup(80, " ");
					if (Strings.Mid(rs.Get_Fields_String("InventoryType"), 1, 1) != "M")
						goto NextTag;
					if (rs.EndOfFile() != true)
					{
						Strings.MidSet(ref strLine, 1, 8, FCConvert.ToString(rs.Get_Fields("Low")));
						// Format(.fields("Low"), "@@@@@@@@")
						Strings.MidSet(ref strLine, 12, 1, "-");
						Strings.MidSet(ref strLine, 14, 8, FCConvert.ToString(rs.Get_Fields("High")));
						// Format(.fields("High"), "!@@@@@@@@")
						rs.MoveNext();
					}
					// 
					if (rs.EndOfFile() != true)
					{
						if (Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) != "M")
							goto GotOneTag;
						Strings.MidSet(ref strLine, 29, 8, Strings.Format(rs.Get_Fields("Low"), "@@@@@@@@"));
						Strings.MidSet(ref strLine, 38, 1, "-");
						Strings.MidSet(ref strLine, 40, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
						rs.MoveNext();
					}
					// 
					if (rs.EndOfFile() != true)
					{
						if (Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) != "M")
							goto GotOneTag;
						Strings.MidSet(ref strLine, 56, 8, Strings.Format(rs.Get_Fields("Low"), "@@@@@@@@"));
						Strings.MidSet(ref strLine, 64, 1, "-");
						Strings.MidSet(ref strLine, 66, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
					}
					GotOneTag:
					;
					rtf1.Text = rtf1.Text + strLine + "\r\n";
					LineCount += 1;
					NextTag:
					;
					if (rs.EndOfFile() != true)
						rs.MoveNext();
				}
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + Strings.StrDup(30, "_") + "  PAGE BREAK  " + Strings.StrDup(30, "_") + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				executeCheckSingleStickers = true;
				goto CheckSingleStickers;
			}
			CheckSingleStickers:
			;
			if (executeCheckSingleStickers)
			{
				frmWait.InstancePtr.prgProgress.Value = 20;
				if (cmbReport.Text == "Inventory Report")
				{
					if (cmbAll.Text != "Print All")
					{
						if (Check1[1].CheckState == Wisej.Web.CheckState.Unchecked)
						{
							goto CheckCombinationStickers;
						}
					}
				}
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
				PrintHeading(2);
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "SINGLE STICKERS----------------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				strInventoryReconciliation();
				rtf1.Text = rtf1.Text + "\r\n";
				rs.MoveFirst();
				lngSOP = 0;
				lngAR = 0;
				lngVI = 0;
				lngAA = 0;
				lngEOP = 0;
				strStickers("SMS01");
				strStickers("SMS02");
				strStickers("SMS03");
				strStickers("SMS04");
				strStickers("SMS05");
				strStickers("SMS06");
				strStickers("SMS07");
				strStickers("SMS08");
				strStickers("SMS09");
				strStickers("SMS10");
				strStickers("SMS11");
				strStickers("SMS12");
				intY1 = 0;
				intY1 = DateTime.Today.Year;
				blnFound = false;
				LineCount = 30;
				for (intYY = intY1 - 4; intYY <= intY1 + 4; intYY++)
				{
					rs.FindFirstRecord("InventoryType", "SYS" + Strings.Mid(intYY.ToString(), 3, 2));
					if (rs.NoMatch == false)
					{
						blnFound = true;
						strStickers("SYS" + Strings.Mid(intYY.ToString(), 3, 2));
						LineCount += 1;
					}
					else
					{
						rsInvAdjust.FindFirstRecord("InventoryType", "SYS" + Strings.Mid(intYY.ToString(), 3, 2));
						if (rsInvAdjust.NoMatch == false)
						{
							blnFound = true;
							strStickers("SYS" + Strings.Mid(intYY.ToString(), 3, 2));
							LineCount += 1;
						}
					}
				}
				// intYY
				if (blnFound == false)
				{
					rsNotFound.OpenRecordset("SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK) + " ORDER BY InventoryType, Low");
					if (rsNotFound.EndOfFile() != true && rsNotFound.BeginningOfFile() != true)
					{
						intY1 = DateTime.Today.Year;
						for (intYY = intY1 - 4; intYY <= intY1 + 4; intYY++)
						{
							rsNotFound.FindFirstRecord("InventoryType", "SYS" + Strings.Mid(intYY.ToString(), 3, 2));
							if (rsNotFound.NoMatch == false)
							{
								strStickers("SYS" + Strings.Mid(intYY.ToString(), 3, 2));
								LineCount += 1;
							}
							else
							{
								rsInvAdjust.FindFirstRecord("InventoryType", "SYS" + Strings.Mid(intYY.ToString(), 3, 2));
								if (rsInvAdjust.NoMatch == false)
								{
									strStickers("SYS" + Strings.Mid(intYY.ToString(), 3, 2));
									LineCount += 1;
								}
							}
						}
						// intYY
					}
				}
				// TOTOL LINE
				strLine = Strings.StrDup(80, " ");
				Strings.MidSet(ref strLine, 1, 7, "TOTAL--");
				Strings.MidSet(ref strLine, 8, 5, Strings.Format(lngSOP, "@@@@@"));
				Strings.MidSet(ref strLine, 18, 10, Strings.Format(lngAR, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 34, 10, Strings.Format(lngVI, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 50, 10, Strings.Format(lngAA, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 66, 10, Strings.Format(lngEOP, "@@@@@@@@@@"));
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				LineCount += 1;
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "ITEMIZED LISTING----------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				LineCount += 2;
				// *******   Single Stickers   *******
				rs.MoveFirst();
				while (!rs.EndOfFile())
				{
					strLine = Strings.StrDup(80, " ");
					strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
					strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
					if (strTC != "SS")
						goto NextTag1;
					if (rs.EndOfFile() != true)
					{
						Strings.MidSet(ref strLine, 1, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 14, 1, "-");
						Strings.MidSet(ref strLine, 16, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
						rs.MoveNext();
					}
					// 
					if (rs.EndOfFile() != true)
					{
						strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
						strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
						if (strTC != "SS")
							goto GotOne2;
						Strings.MidSet(ref strLine, 26, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 40, 1, "-");
						Strings.MidSet(ref strLine, 42, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
						rs.MoveNext();
					}
					// 
					if (rs.EndOfFile() != true)
					{
						strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
						strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
						if (strTC != "SS")
							goto GotOne2;
						Strings.MidSet(ref strLine, 53, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 66, 1, "-");
						Strings.MidSet(ref strLine, 68, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
					}
					GotOne2:
					;
					rtf1.Text = rtf1.Text + strLine + "\r\n";
					LineCount += 1;
					if (LineCount == 63)
					{
						strLine = Strings.StrDup(80, " ");
						rtf1.Text = rtf1.Text + Strings.StrDup(30, "_") + "  PAGE BREAK  " + Strings.StrDup(30, "_") + "\r\n";
						rtf1.Text = rtf1.Text + "\r\n";
						strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
						PrintHeading(5);
						rtf1.Text = rtf1.Text + "\r\n";
						rtf1.Text = rtf1.Text + "ITEMIZED LISTING----------------" + "\r\n";
						rtf1.Text = rtf1.Text + "\r\n";
						LineCount = 0;
					}
					NextTag1:
					;
					if (rs.EndOfFile() != true)
						rs.MoveNext();
				}
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + Strings.StrDup(30, "_") + "  PAGE BREAK  " + Strings.StrDup(30, "_") + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				CheckCombinationStickers:
				;
				frmWait.InstancePtr.prgProgress.Value = 30;
				if (cmbReport.Text == "Inventory Report")
				{
					if (cmbAll.Text != "Print All")
					{
						if (Check1[5].CheckState == Wisej.Web.CheckState.Unchecked)
						{
							goto CheckDoubleStickers;
						}
					}
				}
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
				PrintHeading(2);
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "MOTORCYCLE COMBINATION STICKERS----------------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				strInventoryReconciliation();
				rtf1.Text = rtf1.Text + "\r\n";
				rs.MoveFirst();
				lngSOP = 0;
				lngAR = 0;
				lngVI = 0;
				lngAA = 0;
				lngEOP = 0;
				intY1 = 0;
				intY1 = DateTime.Today.Year;
				blnFound = false;
				LineCount = 30;
				for (intYY = intY1 - 4; intYY <= intY1 + 4; intYY++)
				{
					rs.FindFirstRecord("InventoryType", "SYC" + Strings.Mid(intYY.ToString(), 3, 2));
					if (rs.NoMatch == false)
					{
						blnFound = true;
						strStickers("SYC" + Strings.Mid(intYY.ToString(), 3, 2));
						LineCount += 1;
					}
                    else
                    {
                        rsInvAdjust.FindFirstRecord("InventoryType", "SYC" + Strings.Mid(intYY.ToString(), 3, 2));
                        if (rsInvAdjust.NoMatch == false)
                        {
                            blnFound = true;
                            strStickers("SYC" + Strings.Mid(intYY.ToString(), 3, 2));
                            LineCount += 1;
                        }
                    }
				}
				// intYY
				if (blnFound == false)
				{
					rsNotFound.OpenRecordset("SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK) + " ORDER BY InventoryType, Low");
					if (rsNotFound.EndOfFile() != true && rsNotFound.BeginningOfFile() != true)
					{
						intY1 = DateTime.Today.Year;
						for (intYY = intY1 - 4; intYY <= intY1 + 4; intYY++)
						{
							rsNotFound.FindFirstRecord("InventoryType", "SYC" + Strings.Mid(intYY.ToString(), 3, 2));
							if (rsNotFound.NoMatch == false)
							{
								strStickers("SYC" + Strings.Mid(intYY.ToString(), 3, 2));
								LineCount += 1;
							}
						}
						// intYY
					}
				}
				// TOTOL LINE
				strLine = Strings.StrDup(80, " ");
				Strings.MidSet(ref strLine, 1, 7, "TOTAL--");
				Strings.MidSet(ref strLine, 8, 5, Strings.Format(lngSOP, "@@@@@"));
				Strings.MidSet(ref strLine, 18, 10, Strings.Format(lngAR, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 34, 10, Strings.Format(lngVI, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 50, 10, Strings.Format(lngAA, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 66, 10, Strings.Format(lngEOP, "@@@@@@@@@@"));
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				LineCount += 1;
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "ITEMIZED LISTING----------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				LineCount += 2;
				// *******   Combination Stickers   *******
				rs.MoveFirst();
				while (!rs.EndOfFile())
				{
					strLine = Strings.StrDup(80, " ");
					strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
					strMM = "C" + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
					if (strTC != "SC")
						goto NextTag6;
					if (rs.EndOfFile() != true)
					{
						Strings.MidSet(ref strLine, 1, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 14, 1, "-");
						Strings.MidSet(ref strLine, 16, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
						rs.MoveNext();
					}
					// 
					if (rs.EndOfFile() != true)
					{
						strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
						strMM = "C" + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
						if (strTC != "SC")
							goto GotOne7;
						Strings.MidSet(ref strLine, 26, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 40, 1, "-");
						Strings.MidSet(ref strLine, 42, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
						rs.MoveNext();
					}
					// 
					if (rs.EndOfFile() != true)
					{
						strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
						strMM = "C" + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
						if (strTC != "SC")
							goto GotOne7;
						Strings.MidSet(ref strLine, 53, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 66, 1, "-");
						Strings.MidSet(ref strLine, 68, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
					}
					GotOne7:
					;
					rtf1.Text = rtf1.Text + strLine + "\r\n";
					LineCount += 1;
					if (LineCount == 63)
					{
						strLine = Strings.StrDup(80, " ");
						rtf1.Text = rtf1.Text + Strings.StrDup(30, "_") + "  PAGE BREAK  " + Strings.StrDup(30, "_") + "\r\n";
						rtf1.Text = rtf1.Text + "\r\n";
						strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
						PrintHeading(5);
						rtf1.Text = rtf1.Text + "\r\n";
						rtf1.Text = rtf1.Text + "ITEMIZED LISTING----------------" + "\r\n";
						rtf1.Text = rtf1.Text + "\r\n";
						LineCount = 0;
					}
					NextTag6:
					;
					if (rs.EndOfFile() != true)
						rs.MoveNext();
				}
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + Strings.StrDup(30, "_") + "  PAGE BREAK  " + Strings.StrDup(30, "_") + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				CheckDoubleStickers:
				;
				frmWait.InstancePtr.prgProgress.Value = 40;
				if (cmbReport.Text == "Inventory Report")
				{
					if (cmbAll.Text != "Print All")
					{
						if (Check1[2].CheckState == Wisej.Web.CheckState.Unchecked)
						{
							goto CheckPlates;
						}
					}
				}
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
				PrintHeading(3);
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "DOUBLE STICKERS----------------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				strInventoryReconciliation();
				rtf1.Text = rtf1.Text + "\r\n";
				lngSOP = 0;
				lngAR = 0;
				lngVI = 0;
				lngAA = 0;
				lngEOP = 0;
				strStickers("SMD01");
				strStickers("SMD02");
				strStickers("SMD03");
				strStickers("SMD04");
				strStickers("SMD05");
				strStickers("SMD06");
				strStickers("SMD07");
				strStickers("SMD08");
				strStickers("SMD09");
				strStickers("SMD10");
				strStickers("SMD11");
				strStickers("SMD12");
				intY1 = 0;
				blnFound = false;
				LineCount = 30;
				intY1 = DateTime.Today.Year;
				for (intYY = intY1 - 4; intYY <= intY1 + 4; intYY++)
				{
					rs.FindFirstRecord("InventoryType", "SYD" + Strings.Mid(intYY.ToString(), 3, 2));
					if (rs.NoMatch == false)
					{
						blnFound = true;
						strStickers("SYD" + Strings.Mid(intYY.ToString(), 3, 2));
						LineCount += 1;
					}
					else
					{
						rsInvAdjust.FindFirstRecord("InventoryType", "SYD" + Strings.Mid(intYY.ToString(), 3, 2));
						if (rsInvAdjust.NoMatch == false)
						{
							blnFound = true;
							strStickers("SYD" + Strings.Mid(intYY.ToString(), 3, 2));
							LineCount += 1;
						}
					}
				}
				// intYY
				if (blnFound == false)
				{
					rsNotFound.OpenRecordset("SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK) + " ORDER BY InventoryType, Low");
					if (rsNotFound.EndOfFile() != true && rsNotFound.BeginningOfFile() != true)
					{
						intY1 = DateTime.Today.Year;
						for (intYY = intY1 - 4; intYY <= intY1 + 4; intYY++)
						{
							rsNotFound.FindFirstRecord("InventoryType", "SYD" + Strings.Mid(intYY.ToString(), 3, 2));
							if (rsNotFound.NoMatch == false)
							{
								strStickers("SYD" + Strings.Mid(intYY.ToString(), 3, 2));
								LineCount += 1;
							}
							else
							{
								rsInvAdjust.FindFirstRecord("InventoryType", "SYD" + Strings.Mid(intYY.ToString(), 3, 2));
								if (rsInvAdjust.NoMatch == false)
								{
									strStickers("SYD" + Strings.Mid(intYY.ToString(), 3, 2));
									LineCount += 1;
								}
							}
						}
						// intYY
					}
				}
				// TOTOL LINE
				strLine = Strings.StrDup(80, " ");
				Strings.MidSet(ref strLine, 1, 7, "TOTAL--");
				Strings.MidSet(ref strLine, 8, 5, Strings.Format(lngSOP, "@@@@@"));
				Strings.MidSet(ref strLine, 18, 10, Strings.Format(lngAR, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 34, 10, Strings.Format(lngVI, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 50, 10, Strings.Format(lngAA, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 66, 10, Strings.Format(lngEOP, "@@@@@@@@@@"));
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				LineCount += 1;
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "ITEMIZED LISTING----------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				LineCount += 2;
				rs.MoveFirst();
				// *******   Double Stickers   *******
				while (!rs.EndOfFile())
				{
					strLine = Strings.StrDup(80, " ");
					strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
					strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
					if (strTC != "SD")
						goto NextTag2;
					if (rs.EndOfFile() != true)
					{
						strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
						strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
						Strings.MidSet(ref strLine, 1, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 14, 1, "-");
						Strings.MidSet(ref strLine, 16, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
						rs.MoveNext();
					}
					// 
					if (rs.EndOfFile() != true)
					{
						strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
						strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
						if (strTC != "SD")
							goto GotOne3;
						Strings.MidSet(ref strLine, 26, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 40, 1, "-");
						Strings.MidSet(ref strLine, 42, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
						rs.MoveNext();
					}
					// 
					if (rs.EndOfFile() != true)
					{
						strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 3, 1);
						strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 2, 1) + Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
						if (strTC != "SD")
							goto GotOne3;
						Strings.MidSet(ref strLine, 53, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 66, 1, "-");
						Strings.MidSet(ref strLine, 68, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
					}
					GotOne3:
					;
					rtf1.Text = rtf1.Text + strLine + "\r\n";
					LineCount += 1;
					if (LineCount == 63)
					{
						strLine = Strings.StrDup(80, " ");
						rtf1.Text = rtf1.Text + Strings.StrDup(30, "_") + "  PAGE BREAK  " + Strings.StrDup(30, "_") + "\r\n";
						rtf1.Text = rtf1.Text + "\r\n";
						strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
						PrintHeading(3);
						rtf1.Text = rtf1.Text + "\r\n";
						rtf1.Text = rtf1.Text + "ITEMIZED LISTING----------------" + "\r\n";
						rtf1.Text = rtf1.Text + "\r\n";
						LineCount = 0;
					}
					NextTag2:
					;
					if (rs.EndOfFile() != true)
						rs.MoveNext();
				}
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + Strings.StrDup(30, "_") + "  PAGE BREAK  " + Strings.StrDup(30, "_") + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				CheckPlates:
				;
				frmWait.InstancePtr.prgProgress.Value = 80;
				if (cmbReport.Text == "Inventory Report")
				{
					if (cmbAll.Text != "Print All")
					{
						if (Check1[3].CheckState == Wisej.Web.CheckState.Unchecked)
						{
							goto BoosterTag;
						}
					}
				}
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
				PrintHeading(5);
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "PLATES-------------------------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				strInventoryReconciliation();
				rtf1.Text = rtf1.Text + "\r\n";
				strPlateReconciliation();
				// TOTOL LINE
				strLine = Strings.StrDup(80, " ");
				Strings.MidSet(ref strLine, 1, 7, "TOTAL--");
				Strings.MidSet(ref strLine, 8, 5, Strings.Format(lngSOP, "@@@@@"));
				Strings.MidSet(ref strLine, 18, 10, Strings.Format(lngAR, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 34, 10, Strings.Format(lngVI, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 50, 10, Strings.Format(lngAA, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 66, 10, Strings.Format(lngEOP, "@@@@@@@@@@"));
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				LineCount = 62;
				executeCheckSingleStickers = false;
			}
			// *******   Plates   *******
			if (cmbInterim.Text == "Interim Reports")
			{
				strSql = "SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID < 1 AND InventoryType <> 'PXSXX' AND InventoryType <> 'PXSLT' ORDER BY substring(InventoryType,4,2), Low";
			}
			else
			{
				strSql = "SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK2) + " AND InventoryType <> 'PXSXX' AND InventoryType <> 'PXSLT' ORDER BY substring(InventoryType,4,2), Low";
			}
			strLine = Strings.StrDup(80, " ");
			rs.OpenRecordset(strSql);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				LineCount += 1;
				if (LineCount == 63)
				{
					strLine = Strings.StrDup(80, " ");
					rtf1.Text = rtf1.Text + Strings.StrDup(30, "_") + "  PAGE BREAK  " + Strings.StrDup(30, "_") + "\r\n";
					rtf1.Text = rtf1.Text + "\r\n";
					strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
					PrintHeading(5);
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + "ITEMIZED LISTING----------------" + "\r\n";
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount = 0;
				}
				while (!rs.EndOfFile())
				{
					strLine = Strings.StrDup(80, " ");
					strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1);
					if (strTC != "P")
						goto NextTag4;
					if (rs.EndOfFile() != true)
					{
                        if (strTC == "P")
                        {
                            strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
                            Strings.MidSet(ref strLine, 1, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
                            Strings.MidSet(ref strLine, 14, 1, "-");
                            Strings.MidSet(ref strLine, 16, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
                            rs.MoveNext();
                        }
					}
					// 
					if (rs.EndOfFile() != true)
					{
						strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
						strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1);
                        if (strTC == "P")
                        {
                            Strings.MidSet(ref strLine, 27, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
                            Strings.MidSet(ref strLine, 40, 1, "-");
                            Strings.MidSet(ref strLine, 42, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
                            rs.MoveNext();
                        }
					}
					// 
					if (rs.EndOfFile() != true)
					{
						strTC = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1);
						strMM = Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 4, 2);
                        if (strTC == "P")
                        {
                            Strings.MidSet(ref strLine, 53, 12, Strings.Format(strMM + " " + rs.Get_Fields("Low"), "@@@@@@@@@@@@"));
                            Strings.MidSet(ref strLine, 66, 1, "-");
                            Strings.MidSet(ref strLine, 68, 8, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@"));
                        }
					}

                    rtf1.Text = rtf1.Text + strLine + "\r\n";
                    LineCount += 1;
                    if (LineCount == 63)
                    {
                        strLine = Strings.StrDup(80, " ");
                        rtf1.Text = rtf1.Text + Strings.StrDup(30, "_") + "  PAGE BREAK  " + Strings.StrDup(30, "_") + "\r\n";
                        rtf1.Text = rtf1.Text + "\r\n";
                        strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
                        PrintHeading(5);
                        rtf1.Text = rtf1.Text + "\r\n";
                        rtf1.Text = rtf1.Text + "ITEMIZED LISTING----------------" + "\r\n";
                        rtf1.Text = rtf1.Text + "\r\n";
                        LineCount = 0;
                    }
                NextTag4:
					;
					if (rs.EndOfFile() != true)
						rs.MoveNext();
				}
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + Strings.StrDup(30, "_") + "  PAGE BREAK  " + Strings.StrDup(30, "_") + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
			}
			BoosterTag:
			;
			frmWait.InstancePtr.prgProgress.Value = 100;
			if (cmbReport.Text == "Inventory Report")
			{
				if (cmbAll.Text != "Print All")
				{
					if (Check1[4].CheckState == Wisej.Web.CheckState.Unchecked)
					{
						goto endtag;
					}
				}
			}
			strSql = "SELECT * FROM CloseoutInventory WHERE PeriodCloseoutID = " + FCConvert.ToString(lngPK2) + " AND (InventoryType = 'BXSXX' OR InventoryType = 'PXSXX' OR InventoryType = 'RXSXX') ORDER BY InventoryType, Low";
			rs.OpenRecordset(strSql);
			rs.MoveFirst();
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
				PrintHeading(6);
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "Permits-----------------------------------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				strInventoryReconciliation();
				rtf1.Text = rtf1.Text + "\r\n";
				strPermitReconciliation();
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "ITEMIZED LISTING----------------" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				// *******   Boosters   *******
				rs.MoveFirst();
				while (!rs.EndOfFile())
				{
					strLine = Strings.StrDup(80, " ");
					if (rs.EndOfFile() != true)
					{
						if (rs.Get_Fields_String("InventoryType") == "BXSXX")
						{
							strDisplayType = "BST";
						}
						else if (rs.Get_Fields_String("InventoryType") == "PXSXX")
						{
							strDisplayType = "TRN";
						}
						else if (rs.Get_Fields_String("InventoryType") == "RXSXX")
						{
							strDisplayType = "MVR";
						}
						Strings.MidSet(ref strLine, 1, 12, Strings.Format(strDisplayType + " " + rs.Get_Fields("Low"), "@@@@@@@@@"));
						Strings.MidSet(ref strLine, 13, 1, "-");
						Strings.MidSet(ref strLine, 14, 10, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@@@"));
						rs.MoveNext();
					}
					// 
					if (rs.EndOfFile() != true)
					{
						if (rs.Get_Fields_String("InventoryType") == "BXSXX")
						{
							strDisplayType = "BST";
						}
						else if (rs.Get_Fields_String("InventoryType") == "PXSXX")
						{
							strDisplayType = "TRN";
						}
						else if (rs.Get_Fields_String("InventoryType") == "RXSXX")
						{
							strDisplayType = "MVR";
						}
						Strings.MidSet(ref strLine, 26, 12, Strings.Format(strDisplayType + " " + rs.Get_Fields("Low"), "@@@@@@@@@"));
						Strings.MidSet(ref strLine, 39, 1, "-");
						Strings.MidSet(ref strLine, 40, 12, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@@@"));
						rs.MoveNext();
					}
					// 
					if (rs.EndOfFile() != true)
					{
						if (rs.Get_Fields_String("InventoryType") == "BXSXX")
						{
							strDisplayType = "BST";
						}
						else if (rs.Get_Fields_String("InventoryType") == "PXSXX")
						{
							strDisplayType = "TRN";
						}
						else if (rs.Get_Fields_String("InventoryType") == "RXSXX")
						{
							strDisplayType = "MVR";
						}
						Strings.MidSet(ref strLine, 54, 12, Strings.Format(strDisplayType + " " + rs.Get_Fields("Low"), "@@@@@@@@@"));
						Strings.MidSet(ref strLine, 67, 1, "-");
						Strings.MidSet(ref strLine, 68, 12, Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@@@"));
					}
					GotOneTag6:
					;
					rtf1.Text = rtf1.Text + strLine + "\r\n";
					LineCount += 1;
					NextTag5:
					;
					if (rs.EndOfFile() != true)
						rs.MoveNext();
				}
			}
			endtag:
			;
			if (rtf1.Text == "")
			{
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** INVENTORY RECONCILIATION REPORT ****";
				PrintHeading(1);
				rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
				strLine = (Strings.StrDup(33, " ") + "No Information");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
			}
			FCFileSystem.FileOpen(33, Path.Combine("BMVReports", strResCode + "SA.RPT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
			FCFileSystem.PrintLine(33, rtf1.Text);
			FCFileSystem.FileClose(33);
			frmWait.InstancePtr.Unload();
			this.Refresh();

			rptInventoryMaster.InstancePtr.Unload();
			rptInventoryMaster.InstancePtr.Init(cmbAll.Text == "Print All" || Check1[0].CheckState == Wisej.Web.CheckState.Checked, cmbAll.Text == "Print All" || Check1[1].CheckState == Wisej.Web.CheckState.Checked, cmbAll.Text == "Print All" || Check1[2].CheckState == Wisej.Web.CheckState.Checked, cmbAll.Text == "Print All" || Check1[3].CheckState == Wisej.Web.CheckState.Checked, cmbAll.Text == "Print All" || Check1[5].CheckState == Wisej.Web.CheckState.Checked, cmbAll.Text == "Print All" || Check1[4].CheckState == Wisej.Web.CheckState.Checked);
			if (AllFlag)
			{
				rptInventoryMaster.InstancePtr.PrintReport(false, batchReports: batchReports);
				rptInventoryMaster.InstancePtr.Unload();

				CheckIt:
				;
				if (MotorVehicle.JobComplete("rptInventoryMaster"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckIt;
				}
			}
			else
			{
				frmReportViewer.InstancePtr.Init(rptInventoryMaster.InstancePtr);
			}

			rs.Reset();
		}

        public void ExciseTaxOnly(List<string> batchReports = null)
		{
			int lngPK1 = 0;
			int lngPK2 = 0;
			LineCount = 0;
			PageCount = 0;
			//rtf1.RightMargin = 5;
			if (cmbInterim.Text == "Interim Reports")
			{
				rs.OpenRecordset("SELECT * FROM ExciseTax WHERE PeriodCloseoutID < 1 ORDER BY DateOfTransaction, MVR3Number");
			}
			else
			{
				strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(strSql);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = rs.Get_Fields_Int32("ID");
					rs.MoveFirst();
					lngPK2 = rs.Get_Fields_Int32("ID");
				}
				rs.OpenRecordset("SELECT * FROM ExciseTax WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " ORDER BY DateOfTransaction, MVR3Number");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** EXCISE TAX TRANSACTIONS ****";
				PrintHeading(PageCount);
				while (!rs.EndOfFile())
				{
					strLine = Strings.StrDup(80, " ");
					Strings.MidSet(ref strLine, 2, 7, Strings.Format(Strings.Format(rs.Get_Fields_Int32("MVR3Number"), "#######"), "@@@@@@@"));
					Strings.MidSet(ref strLine, 10, 3, Strings.StrDup(3 - FCConvert.ToString(rs.Get_Fields_String("OpID")).Length, " ") + rs.Get_Fields_String("OpID"));
					rs.MoveNext();
					if (!rs.EndOfFile())
					{
						Strings.MidSet(ref strLine, 17, 7, Strings.Format(Strings.Format(rs.Get_Fields_Int32("MVR3Number"), "#######"), "@@@@@@@"));
						Strings.MidSet(ref strLine, 25, 3, Strings.StrDup(3 - FCConvert.ToString(rs.Get_Fields_String("OpID")).Length, " ") + rs.Get_Fields_String("OpID"));
						rs.MoveNext();
					}
					if (!rs.EndOfFile())
					{
						Strings.MidSet(ref strLine, 32, 7, Strings.Format(Strings.Format(rs.Get_Fields_Int32("MVR3Number"), "#######"), "@@@@@@@"));
						Strings.MidSet(ref strLine, 40, 3, Strings.StrDup(3 - FCConvert.ToString(rs.Get_Fields_String("OpID")).Length, " ") + rs.Get_Fields_String("OpID"));
						rs.MoveNext();
					}
					if (!rs.EndOfFile())
					{
						Strings.MidSet(ref strLine, 47, 7, Strings.Format(Strings.Format(rs.Get_Fields_Int32("MVR3Number"), "#######"), "@@@@@@@"));
						Strings.MidSet(ref strLine, 55, 3, Strings.StrDup(3 - FCConvert.ToString(rs.Get_Fields_String("OpID")).Length, " ") + rs.Get_Fields_String("OpID"));
						rs.MoveNext();
					}
					if (!rs.EndOfFile())
					{
						Strings.MidSet(ref strLine, 62, 7, Strings.Format(Strings.Format(rs.Get_Fields_Int32("MVR3Number"), "#######"), "@@@@@@@"));
						Strings.MidSet(ref strLine, 70, 3, Strings.StrDup(3 - FCConvert.ToString(rs.Get_Fields_String("OpID")).Length, " ") + rs.Get_Fields_String("OpID"));
					}
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount += 1;
					if (!rs.EndOfFile())
						rs.MoveNext();
				}
			}
			else
			{
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** EXCISE TAX TRANSACTIONS ****";
				PrintHeading(1);
				rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
				strLine = (Strings.StrDup(33, " ") + "No Information");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				// MsgBox "There are no records for this reporting period.", vbOKOnly, "Excise Tax Only Report"
				// Close
				// Exit Sub
			}
			if (rtf1.Text != "")
			{
				FCFileSystem.FileOpen(33, Path.Combine("BMVReports", strResCode + "TT.RPT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.PrintLine(33, rtf1.Text);
				FCFileSystem.FileClose(33);
			}
			if (AllFlag)
			{
				rptExciseTaxOnly.InstancePtr.PrintReport(false, batchReports: batchReports);
				rptExciseTaxOnly.InstancePtr.Unload();
			
				CheckIt:
				;
				if (MotorVehicle.JobComplete("rptExciseTaxOnly"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckIt;
				}
			}
			else
			{
				rptExciseTaxOnly.InstancePtr.Unload();
				frmReportViewer.InstancePtr.Init(rptExciseTaxOnly.InstancePtr);
			}

			rs.Reset();
		}

		public void ExceptionReport(List<string> batchReports = null)
		{
			string strType = "";
			string strTypeHeading = "";
			int lngPK1;
			int lngPK2;
			int intHeadingNumber = 0;
			bool CompletedECorrects;
			clsDRWrapper rsTemp = new clsDRWrapper();
			int StickerTotal = 0;
			int PlateTotal = 0;
			int MonAdjTotal = 0;
			float MonAdjMoneyTotal = 0;
			int PermitTotal = 0;
			float PermitMoneyTotal = 0;
			string strDS = "";
			CompletedECorrects = false;
			LineCount = 0;
			PageCount = 1;
			//rtf1.RightMargin = 5;
			rsCategory.OpenRecordset("SELECT * FROM TownSummaryHeadings");
			// 
			strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSql);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = rs.Get_Fields_Int32("ID");
				rs.MoveFirst();
				lngPK2 = rs.Get_Fields_Int32("ID");
			}
			// 
			if (cmbInterim.Text == "Interim Reports")
			{
				strSql = "SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1 ORDER BY Type, ExceptionReportDate";
			}
			else
			{
				strSql = "SELECT * FROM ExceptionReport WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " ORDER BY Type, ExceptionReportDate";
			}
			rs.OpenRecordset(strSql);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** EXCEPTION REPORT ****";
				PrintHeading(PageCount);
				strTypeHeading = "---- STICKERS ----";
				strType = "1S";
				intHeadingNumber = 1;
				while (!rs.EndOfFile())
				{
					TestTypeAgain:
					;
					strLine = Strings.StrDup(80, " ");
					if (strType != rs.Get_Fields_String("Type"))
					{
						if (strType == "1S")
						{
							if (StickerTotal > 0)
							{
								PrintTotalLine("TOTAL STICKERS:", ref StickerTotal, 0);
							}
							strTypeHeading = "---- PLATES ----";
							strType = "2P";
							intHeadingNumber = 3;
						}
						else if (strType == "2P")
						{
							if (PlateTotal > 0)
							{
								PrintTotalLine("TOTAL PLATES:", ref PlateTotal, 0);
							}
							strTypeHeading = "---- MONETARY CORRECTIONS ----";
							strType = "3CM";
							intHeadingNumber = 4;
							ShowMonetaryCorrections(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
							while (rs.EndOfFile() != true)
							{
								if (rs.Get_Fields_String("Type") == "3CM")
								{
									rs.MoveNext();
								}
								else
								{
									break;
								}
							}
							CompletedECorrects = true;
							if (rs.EndOfFile())
							{
								break;
							}
						}
						else if (strType == "3CM")
						{
							strTypeHeading = "---- NON-MONETARY CORRECTIONS ----";
							strType = "4CN";
							intHeadingNumber = 5;
							ShowNonMonetaryCorrections(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
							while (rs.EndOfFile() != true)
							{
								if (rs.Get_Fields_String("Type") == "4CN")
								{
									rs.MoveNext();
								}
								else
								{
									break;
								}
							}
							if (rs.EndOfFile())
							{
								break;
							}
						}
						else if (strType == "4CN")
						{
							strTypeHeading = "---- MONETARY ADJUSTMENTS ----";
							strType = "5MA";
							intHeadingNumber = 6;
						}
						else if (strType == "5MA")
						{
							if (MonAdjTotal > 0)
							{
								PrintTotalLine("TOTAL MONEY ADJUSTMENTS:", ref MonAdjTotal, MonAdjMoneyTotal);
							}
							strTypeHeading = "---- PERMITS ----";
							strType = "6PM";
							intHeadingNumber = 7;
						}
						else if (strType == "6PM")
						{
							if (PermitTotal != 0)
							{
								PrintTotalLine("TOTAL PERMITS:", ref PermitTotal, PermitMoneyTotal);
							}
							strTypeHeading = "---- NO FEE - DUPLICATE - int TERM RENTAL ----";
							strType = "7DR";
							intHeadingNumber = 8;
							ShowNoFeeDupRegs(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
							while (rs.EndOfFile() != true)
							{
								if (rs.Get_Fields_String("Type") == "7DR")
								{
									rs.MoveNext();
								}
								else
								{
									break;
								}
							}
							if (rs.EndOfFile())
							{
								break;
							}
						}
						goto TestTypeAgain;
					}
					// Category Headings
					if (intHeadingNumber != 0)
					{
						rtf1.Text = rtf1.Text + "\r\n";
						rtf1.Text = rtf1.Text + "\r\n";
						strLine = strHeadingLine(FCConvert.ToInt16(40 - strTypeHeading.Length / 2.0), strTypeHeading);
						rtf1.Text = rtf1.Text + strLine;
						rtf1.Text = rtf1.Text + "\r\n";
						rtf1.Text = rtf1.Text + strExceptionHeading(ref intHeadingNumber) + "\r\n";
						intHeadingNumber = 0;
						strLine = Strings.StrDup(80, " ");
					}
					// End Category Headings
					string tempSticker = "";
					if (strType == "1S")
					{
						StickerTotal += 1;
						Strings.MidSet(ref strLine, 2, 3, Strings.Format(rs.Get_Fields_String("OriginalClass"), "!@@@"));
						Strings.MidSet(ref strLine, 5, 8, Strings.Format(rs.Get_Fields_String("OriginalPlate"), "!@@@@@@@@"));
						Strings.MidSet(ref strLine, 13, 9, Strings.Format(Strings.Format(rs.Get_Fields_DateTime("ExceptionReportDate"), "MM/dd/yy"), "@@@@@@@@@"));
						if (rs.Get_Fields_Int32("ReplacementNumberOfStickers") == 2)
						{
							strDS = "D";
						}
						else
						{
							strDS = "S";
						}
						if (rs.Get_Fields_Int32("ReplacementMonthYear") > 99)
						{
							tempSticker = rs.Get_Fields_String("ReplacementMMYY") + Strings.Format(Strings.Right(fecherFoundation.Strings.Trim(rs.Get_Fields_Int32("ReplacementMonthYear").ToString()), 2), "00") + " " + rs.Get_Fields_Int32("ReplacementStickerNumber") + " " + strDS;
						}
						else
						{
							tempSticker = rs.Get_Fields_String("ReplacementMMYY") + Strings.Format(rs.Get_Fields_Int32("ReplacementMonthYear"), "00") + " " + rs.Get_Fields_Int32("ReplacementStickerNumber") + " " + strDS;
						}
						Strings.MidSet(ref strLine, 29, 15, tempSticker);
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("reason"))) != "")
						{
							if (FCConvert.ToString(rs.Get_Fields_String("reason")).Length <= 20)
							{
								Strings.MidSet(ref strLine, 44, 20, rs.Get_Fields_String("reason") + Strings.StrDup(20 - FCConvert.ToString(rs.Get_Fields_String("reason")).Length, " "));
							}
							else
							{
								Strings.MidSet(ref strLine, 44, 20, Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("reason")), 1, 20));
							}
						}
						Strings.MidSet(ref strLine, 68, 5, Strings.Format(Strings.Format(rs.Get_Fields("Fee"), "#0.00"), "!@@@@@"));
						Strings.MidSet(ref strLine, 76, 3, Strings.Format(rs.Get_Fields_String("OpID"), "!@@@"));
					}
					else if (strType == "2P")
					{
						PlateTotal += 1;
						Strings.MidSet(ref strLine, 4, 2, FCConvert.ToString(rs.Get_Fields_String("OriginalClass")));
						Strings.MidSet(ref strLine, 7, 7, FCConvert.ToString(rs.Get_Fields_String("OriginalPlate")));
						Strings.MidSet(ref strLine, 23, 2, FCConvert.ToString(rs.Get_Fields_String("newclass")));
						Strings.MidSet(ref strLine, 26, 7, FCConvert.ToString(rs.Get_Fields_String("NewPlate")));
						Strings.MidSet(ref strLine, 35, 9, Strings.Format(Strings.Format(rs.Get_Fields_DateTime("ExceptionReportDate"), "MM/dd/yy"), "@@@@@@@@@"));
						Strings.MidSet(ref strLine, 50, 20, FCConvert.ToString(rs.Get_Fields_String("reason")));
						Strings.MidSet(ref strLine, 76, 3, Strings.Format(rs.Get_Fields_String("OpID"), "!@@@"));
					}
					else if (strType == "3CM")
					{
						MonCorrTotal += 1;
						Strings.MidSet(ref strLine, 1, 8, Strings.Format(rs.Get_Fields_Int32("MVR3Number"), "00000000"));
						Strings.MidSet(ref strLine, 11, 2, Strings.Format(rs.Get_Fields_String("newclass"), "!@@"));
						Strings.MidSet(ref strLine, 14, 8, Strings.Format(rs.Get_Fields_String("NewPlate"), "!@@@@@@@@"));
						Strings.MidSet(ref strLine, 24, 9, Strings.Format(Strings.Format(rs.Get_Fields("Fee"), "#,###.00"), "@@@@@@@@@"));
						MonCorrMoneyTotal += FCConvert.ToSingle(rs.Get_Fields("Fee"));
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("reason"))) != "")
						{
							if (FCConvert.ToString(rs.Get_Fields_String("reason")).Length <= 20)
							{
								Strings.MidSet(ref strLine, 35, 20, rs.Get_Fields_String("reason") + Strings.StrDup(20 - FCConvert.ToString(rs.Get_Fields_String("reason")).Length, " "));
							}
							else
							{
								Strings.MidSet(ref strLine, 35, 20, Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("reason")), 1, 20));
							}
						}
						rsCategory.FindFirstRecord("CategoryCode", rs.Get_Fields_String("CategoryAdjusted"));
						if (rs.NoMatch == false)
						{
							Strings.MidSet(ref strLine, 56, 20, Strings.Format(rsCategory.Get_Fields_String("Heading"), "!@@@@@@@@@@@@@@@@@@@@"));
						}
						Strings.MidSet(ref strLine, 76, 3, Strings.Format(rs.Get_Fields_String("OpID"), "!@@@"));
					}
					else if (strType == "4CN")
					{
						// NonMonCorrTotal = NonMonCorrTotal + 1
						// Mid$(strLine, 1, 8) = Format(.fields("MVR3Number"), "00000000")
						// Mid$(strLine, 11, 2) = Format(.fields("newclass"), "!@@")
						// Mid$(strLine, 14, 8) = Format(.fields("NewPlate"), "!@@@@@@@@")
						// If IsNull(.fields("reason")) = False Then
						// If Len(.fields("reason")) <= 40 Then
						// Mid$(strLine, 24, 40) = .fields("reason")  & String(40 - Len(.fields("reason")), " ")
						// Else
						// Mid$(strLine, 24, 40) = Mid$(.fields("reason"), 1, 40)
						// End If
						// End If
						// Mid$(strLine, 76, 3) = Format(.fields("OpID"), "!@@@")
					}
					else if (strType == "6PM")
					{
						PermitTotal += 1;
						PermitMoneyTotal += FCConvert.ToSingle(rs.Get_Fields("Fee"));
						Strings.MidSet(ref strLine, 1, 10, Strings.Format(rs.Get_Fields("PermitNumber"), "!@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 11, 15, Strings.Format(rs.Get_Fields("PermitType"), "!@@@@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 26, 23, Strings.Format(rs.Get_Fields_String("ApplicantName"), "!@@@@@@@@@@@@@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 49, 2, Strings.Format(rs.Get_Fields_String("newclass"), "!@@"));
						Strings.MidSet(ref strLine, 52, 8, Strings.Format(rs.Get_Fields_String("NewPlate"), "!@@@@@@@@"));
						Strings.MidSet(ref strLine, 63, 9, Strings.Format(Strings.Format(rs.Get_Fields("Fee"), "#,###.00"), "@@@@@@@@@"));
						Strings.MidSet(ref strLine, 76, 3, Strings.Format(rs.Get_Fields_String("OpID"), "!@@@"));
					}
					else if (strType == "5MA")
					{
						MonAdjTotal += 1;
						Strings.MidSet(ref strLine, 1, 11, Strings.Format(Strings.Format(rs.Get_Fields("Fee"), "#,###.00"), "@@@@@@@@@@@"));
						MonAdjMoneyTotal += FCConvert.ToSingle(rs.Get_Fields("Fee"));
						Strings.MidSet(ref strLine, 14, 9, Strings.Format(Strings.Format(rs.Get_Fields_DateTime("ExceptionReportDate"), "MM/dd/yy"), "@@@@@@@@@"));
						rsCategory.FindFirstRecord("CategoryCode", Conversion.Val(rs.Get_Fields_String("CategoryAdjusted")));
						if (rs.NoMatch == false)
						{
							Strings.MidSet(ref strLine, 26, 20, Strings.Format(rsCategory.Get_Fields_String("Heading"), "!@@@@@@@@@@@@@@@@@@@@"));
						}
						Strings.MidSet(ref strLine, 76, 3, Strings.Format(rs.Get_Fields_String("OpID"), "!@@@"));
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("reason"))) != "")
						{
							if (FCConvert.ToString(rs.Get_Fields_String("reason")).Length <= 23)
							{
								Strings.MidSet(ref strLine, 47, 23, rs.Get_Fields_String("reason") + Strings.StrDup(23 - FCConvert.ToString(rs.Get_Fields_String("reason")).Length, " "));
							}
							else
							{
								int placeholder = 0;
								placeholder = 1;
								while (placeholder + 22 < FCConvert.ToString(rs.Get_Fields_String("reason")).Length)
								{
									Strings.MidSet(ref strLine, 47, 23, Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("reason")), placeholder, 23));
									rtf1.Text = rtf1.Text + strLine;
									rtf1.Text = rtf1.Text + "\r\n";
									strLine = Strings.StrDup(80, " ");
									placeholder += 23;
								}
								Strings.MidSet(ref strLine, 47, 23, Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("reason")), placeholder, (FCConvert.ToString(rs.Get_Fields_String("reason")).Length - placeholder) + 1) + Strings.StrDup(23 - ((FCConvert.ToString(rs.Get_Fields_String("reason")).Length - placeholder) + 1), " "));
							}
						}
					}
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount += 1;
					rs.MoveNext();
				}
				if (strType == "1S")
				{
					if (StickerTotal > 0)
					{
						PrintTotalLine("TOTAL STICKERS:", ref StickerTotal, 0);
					}
				}
				else if (strType == "2P")
				{
					if (PlateTotal > 0)
					{
						PrintTotalLine("TOTAL PLATES:", ref PlateTotal, 0);
					}
				}
				else if (strType == "5MA")
				{
					if (MonAdjTotal > 0)
					{
						PrintTotalLine("TOTAL MONEY ADJUSTMENTS:", ref MonAdjTotal, MonAdjMoneyTotal);
					}
				}
				else if (strType == "6PM")
				{
					if (PermitTotal != 0)
					{
						PrintTotalLine("TOTAL PERMITS:", ref PermitTotal, 0);
					}
				}
				if (CompletedECorrects == false)
				{
					strTypeHeading = "---- MONETARY CORRECTIONS ----";
					intHeadingNumber = 4;
					ShowMonetaryCorrections(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
					strTypeHeading = "---- NON-MONETARY CORRECTIONS ----";
					intHeadingNumber = 5;
					ShowNonMonetaryCorrections(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
				}
				rsTemp.OpenRecordset("SELECT * FROM ActivityMaster WHERE NoFeeDupReg = 1 AND DuplicateRegistrationFee = 0");
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					strTypeHeading = "---- NO FEE - DUPLICATE - int TERM RENTAL ----";
					intHeadingNumber = 8;
					ShowNoFeeDupRegs(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
				}
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				PrintTotals(ref StickerTotal, ref PlateTotal, ref MonCorrTotal, ref MonCorrMoneyTotal, ref NonMonCorrTotal, ref MonAdjTotal, ref MonAdjMoneyTotal, ref PermitTotal, ref PermitMoneyTotal, ref NoFeeDupRegTotal);
			}
			else if (CompletedECorrects == false)
			{
				rsTemp.OpenRecordset("SELECT * FROM ActivityMaster WHERE TransactionType = 'ECO' or TransactionType = 'LPS'");
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					strHeading = "**** EXCEPTION REPORT ****";
					PrintHeading(PageCount);
					strTypeHeading = "---- MONETARY CORRECTIONS ----";
					intHeadingNumber = 4;
					ShowMonetaryCorrections(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
					strTypeHeading = "---- NON-MONETARY CORRECTIONS ----";
					intHeadingNumber = 5;
					ShowNonMonetaryCorrections(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
					rsTemp.OpenRecordset("SELECT * FROM ActivityMaster WHERE NoFeeDupReg = 1 AND DuplicateRegistrationFee = 0");
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						strTypeHeading = "---- NO FEE - DUPLICATE - int TERM RENTAL ----";
						intHeadingNumber = 8;
						ShowNoFeeDupRegs(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
					}
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + "\r\n";
					PrintTotals(ref StickerTotal, ref PlateTotal, ref MonCorrTotal, ref MonCorrMoneyTotal, ref NonMonCorrTotal, ref MonAdjTotal, ref MonAdjMoneyTotal, ref PermitTotal, ref PermitMoneyTotal, ref NoFeeDupRegTotal);
					// print page totals here
				}
				else
				{
					rsTemp.OpenRecordset("SELECT * FROM ActivityMaster WHERE NoFeeDupReg = 1 AND DuplicateRegistrationFee = 0");
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						strTypeHeading = "---- NO FEE - DUPLICATE - int TERM RENTAL ----";
						intHeadingNumber = 8;
						ShowNoFeeDupRegs(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
						rtf1.Text = rtf1.Text + "\r\n";
						rtf1.Text = rtf1.Text + "\r\n";
						PrintTotals(ref StickerTotal, ref PlateTotal, ref MonCorrTotal, ref MonCorrMoneyTotal, ref NonMonCorrTotal, ref MonAdjTotal, ref MonAdjMoneyTotal, ref PermitTotal, ref PermitMoneyTotal, ref NoFeeDupRegTotal);
					}
					else
					{
						strLine = Strings.StrDup(80, " ");
						strHeading = "**** EXCEPTION REPORT ****";
						PrintHeading(1);
						rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
						strLine = (Strings.StrDup(33, " ") + "No Information");
						rtf1.Text = rtf1.Text + strLine + "\r\n";
					}
				}
			}
			else
			{
				rsTemp.OpenRecordset("SELECT * FROM ActivityMaster WHERE NoFeeDupReg = 1 AND DuplicateRegistrationFee = 0");
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					strTypeHeading = "---- NO FEE - DUPLICATE - int TERM RENTAL ----";
					intHeadingNumber = 8;
					ShowNoFeeDupRegs(ref lngPK1, ref lngPK2, ref intHeadingNumber, ref strTypeHeading);
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + "\r\n";
					PrintTotals(ref StickerTotal, ref PlateTotal, ref MonCorrTotal, ref MonCorrMoneyTotal, ref NonMonCorrTotal, ref MonAdjTotal, ref MonAdjMoneyTotal, ref PermitTotal, ref PermitMoneyTotal, ref NoFeeDupRegTotal);
				}
				else
				{
					strLine = Strings.StrDup(80, " ");
					strHeading = "**** EXCEPTION REPORT ****";
					PrintHeading(1);
					rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
					strLine = (Strings.StrDup(33, " ") + "No Information");
					rtf1.Text = rtf1.Text + strLine + "\r\n";
				}
			}
			if (rtf1.Text != "")
			{
				FCFileSystem.FileOpen(33, Path.Combine("BMVReports", strResCode + "EX.RPT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.PrintLine(33, rtf1.Text);
				FCFileSystem.FileClose(33);
			}
			if (AllFlag)
			{
				rptNewExceptionReport.InstancePtr.PrintReport(false, batchReports: batchReports);
				rptNewExceptionReport.InstancePtr.Unload();
			
				CheckIt:
				;
				if (MotorVehicle.JobComplete("rptNewExceptionReport"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckIt;
				}
			}
			else
			{
                rptNewExceptionReport.InstancePtr.Unload();
				frmReportViewer.InstancePtr.Init(rptNewExceptionReport.InstancePtr);
			}
			rs.Reset();
		}

		private void ShowNonMonetaryCorrections(ref int lngPK1, ref int lngPK2, ref int intHeadingNumber, ref string strTypeHeading)
		{
			clsDRWrapper rsAM = new clsDRWrapper();
			clsDRWrapper rsEx = new clsDRWrapper();
			NonMonCorrTotal = 0;
			if (cmbInterim.Text == "Interim Reports")
			{
				rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND Status <> 'V' AND ETO <> 1 AND ((TransactionType = 'ECO' AND StatePaid <= 0) or (TransactionType = 'LPS' and ReplacementFee = 0 and StickerFee = 0)) ORDER BY DateUpdated");
				rsEx.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1 AND Type = '4CN' ORDER BY ExceptionReportDate");
			}
			else
			{
				rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND ETO <> 1 AND ((TransactionType = 'ECO' AND StatePaid <= 0) or (TransactionType = 'LPS' and ReplacementFee = 0 and StickerFee = 0)) ORDER BY DateUpdated");
				rsEx.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " AND Type = '4CN' ORDER BY ExceptionReportDate");
			}
			if ((rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true) || (rsEx.EndOfFile() != true && rsEx.BeginningOfFile() != true))
			{
				if (intHeadingNumber != 0)
				{
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + "\r\n";
					strLine = strHeadingLine(FCConvert.ToInt16(40 - strTypeHeading.Length / 2.0), strTypeHeading);
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + strExceptionHeading(ref intHeadingNumber) + "\r\n";
					intHeadingNumber = 0;
				}
				while (rsAM.EndOfFile() != true)
				{
					NonMonCorrTotal += 1;
					strLine = Strings.StrDup(80, " ");
					Strings.MidSet(ref strLine, 1, 8, Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yy"));
					Strings.MidSet(ref strLine, 11, 8, Strings.Format(rsAM.Get_Fields_Int32("MVR3"), "00000000"));
					Strings.MidSet(ref strLine, 21, 2, Strings.Format(rsAM.Get_Fields_String("Class"), "!@@"));
					Strings.MidSet(ref strLine, 24, 8, Strings.Format(rsAM.Get_Fields_String("plate"), "!@@@@@@@@"));
					if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "LPS")
					{
						Strings.MidSet(ref strLine, 34, 40, "Lost Plate/Stickers " + Strings.StrDup(20, " "));
					}
					else
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage"))) != "")
						{
							if (FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")).Length <= 40)
							{
								Strings.MidSet(ref strLine, 34, 40, rsAM.Get_Fields_String("InfoMessage") + Strings.StrDup(40 - FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")).Length, " "));
							}
							else
							{
								Strings.MidSet(ref strLine, 34, 40, Strings.Mid(FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")), 1, 40));
							}
						}
					}
					Strings.MidSet(ref strLine, 76, 3, Strings.Format(rsAM.Get_Fields_String("OpID"), "!@@@"));
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount += 1;
					rsAM.MoveNext();
				}
				while (rsEx.EndOfFile() != true)
				{
					NonMonCorrTotal += 1;
					Strings.MidSet(ref strLine, 1, 8, Strings.Format(rsEx.Get_Fields_DateTime("ExceptionReportDate"), "MM/dd/yy"));
					Strings.MidSet(ref strLine, 11, 8, Strings.Format(rsEx.Get_Fields_Int32("MVR3Number"), "00000000"));
					Strings.MidSet(ref strLine, 21, 2, Strings.Format(rsEx.Get_Fields_String("newclass"), "!@@"));
					Strings.MidSet(ref strLine, 24, 8, Strings.Format(rsEx.Get_Fields_String("NewPlate"), "!@@@@@@@@"));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsEx.Get_Fields_String("reason"))) != "")
					{
						if (FCConvert.ToString(rsEx.Get_Fields_String("reason")).Length <= 40)
						{
							Strings.MidSet(ref strLine, 34, 40, rsEx.Get_Fields_String("reason") + Strings.StrDup(40 - FCConvert.ToString(rsEx.Get_Fields_String("reason")).Length, " "));
						}
						else
						{
							Strings.MidSet(ref strLine, 34, 40, Strings.Mid(FCConvert.ToString(rsEx.Get_Fields_String("reason")), 1, 40));
						}
					}
					Strings.MidSet(ref strLine, 76, 3, Strings.Format(rsEx.Get_Fields_String("OpID"), "!@@@"));
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount += 1;
					rsEx.MoveNext();
				}
				if (NonMonCorrTotal > 0)
				{
					PrintTotalLine("TOTAL NON MONETARY CORRECTIONS:", ref NonMonCorrTotal, 0);
				}
			}
		}

		private void ShowNoFeeDupRegs(ref int lngPK1, ref int lngPK2, ref int intHeadingNumber, ref string strTypeHeading)
		{
			clsDRWrapper rsAM = new clsDRWrapper();
			string strOwner = "";
			NoFeeDupRegTotal = 0;
			if (cmbInterim.Text == "Interim Reports")
			{
				rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND Status <> 'V' AND NoFeeDupReg = 1 AND DuplicateRegistrationFee = 0 ORDER BY DateUpdated");
			}
			else
			{
				rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND NoFeeDupReg = 1 AND DuplicateRegistrationFee = 0 ORDER BY DateUpdated");
			}
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				if (intHeadingNumber != 0)
				{
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + "\r\n";
					strLine = strHeadingLine(FCConvert.ToInt16(40 - strTypeHeading.Length / 2.0), strTypeHeading);
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + strExceptionHeading(ref intHeadingNumber) + "\r\n";
					intHeadingNumber = 0;
				}
				while (rsAM.EndOfFile() != true)
				{
					NoFeeDupRegTotal += 1;
					strLine = Strings.StrDup(80, " ");
					Strings.MidSet(ref strLine, 1, 8, Strings.Format(rsAM.Get_Fields_Int32("NoFeeMVR3Number"), "00000000"));
					Strings.MidSet(ref strLine, 11, 8, Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yy"));
					Strings.MidSet(ref strLine, 21, 2, Strings.Format(rsAM.Get_Fields_String("Class"), "!@@"));
					Strings.MidSet(ref strLine, 24, 8, Strings.Format(rsAM.Get_Fields_String("plate"), "!@@@@@@@@"));
					strOwner = MotorVehicle.GetPartyNameMiddleInitial(rsAM.Get_Fields_Int32("PartyID1"));
					if (fecherFoundation.Strings.Trim(strOwner) != "")
					{
						if (strOwner.Length <= 40)
						{
							Strings.MidSet(ref strLine, 34, 40, strOwner + Strings.StrDup(40 - strOwner.Length, " "));
						}
						else
						{
							Strings.MidSet(ref strLine, 34, 40, Strings.Mid(strOwner, 1, 40));
						}
					}
					Strings.MidSet(ref strLine, 76, 3, Strings.Format(rsAM.Get_Fields_String("OpID"), "!@@@"));
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount += 1;
					rsAM.MoveNext();
				}
				if (NonMonCorrTotal > 0)
				{
					PrintTotalLine("TOTAL NO FEE - DUPLICATE - int TERM RENTAL:", ref NoFeeDupRegTotal, 0);
				}
			}
		}

		private void ShowMonetaryCorrections(ref int lngPK1, ref int lngPK2, ref int intHeadingNumber, ref string strTypeHeading)
		{
			clsDRWrapper rsAM = new clsDRWrapper();
			string temp = "";
			MonCorrMoneyTotal = 0;
			MonCorrTotal = 0;
			string strSql = "";
			if (cmbInterim.Text == "Interim Reports")
			{
				strSql = "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND Status <> 'V' AND (((TransactionType = 'ECO' OR TransactionType = 'GVW') AND StatePaid > 0) or (TransactionType = 'LPS' and (ReplacementFee <> 0 or StickerFee <> 0))) ORDER BY DateUpdated";
			}
			else
			{
				strSql = "SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND (((TransactionType = 'ECO' OR TransactionType = 'GVW') AND StatePaid > 0) or (TransactionType = 'LPS' and (ReplacementFee <> 0 or StickerFee <> 0))) ORDER BY DateUpdated";
			}
			rsAM.OpenRecordset(strSql);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				if (intHeadingNumber != 0)
				{
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + "\r\n";
					strLine = strHeadingLine(FCConvert.ToInt16(40 - strTypeHeading.Length / 2.0), strTypeHeading);
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + strExceptionHeading(ref intHeadingNumber) + "\r\n";
					intHeadingNumber = 0;
				}
				while (rsAM.EndOfFile() != true)
				{
					MonCorrTotal += 1;
					strLine = Strings.StrDup(80, " ");
					Strings.MidSet(ref strLine, 1, 8, Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yy"));
					Strings.MidSet(ref strLine, 11, 8, Strings.Format(rsAM.Get_Fields_Int32("MVR3"), "00000000"));
					Strings.MidSet(ref strLine, 21, 2, Strings.Format(rsAM.Get_Fields_String("Class"), "!@@"));
					Strings.MidSet(ref strLine, 24, 8, Strings.Format(rsAM.Get_Fields_String("plate"), "!@@@@@@@@"));
					if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "LPS")
					{
						Strings.MidSet(ref strLine, 33, 7, Strings.Format(Strings.Format(rsAM.Get_Fields_Decimal("ReplacementFee") + rsAM.Get_Fields_Decimal("StickerFee"), "#,###.00"), "@@@@@@@"));
						Strings.MidSet(ref strLine, 42, 17, "Lost Plate/Stick ");
						MonCorrMoneyTotal += FCConvert.ToSingle(rsAM.Get_Fields_Double("ReplacementFee") + rsAM.Get_Fields_Double("StickerFee"));
					}
					else
					{
						if (Conversion.Val(rsAM.Get_Fields("StatePaid")) != 0)
						{
							// And Not IsNull(.fields("LocalPaid")) Then
							Strings.MidSet(ref strLine, 33, 7, Strings.Format(Strings.Format(rsAM.Get_Fields("StatePaid"), "#,###.00"), "@@@@@@@"));
							MonCorrMoneyTotal += FCConvert.ToSingle(rsAM.Get_Fields("StatePaid"));
						}
						else
						{
							// If IsNull(.fields("StatePaid")) And IsNull(.fields("LocalPaid")) Then
							Strings.MidSet(ref strLine, 33, 7, Strings.Format(Strings.Format(0, "#,###.00"), "@@@@@@@"));
							// ElseIf IsNull(.fields("StatePaid")) Then
							// Mid$(strLine, 24, 9) = Format(Format(.fields("LocalPaid, "#,###.00"), "@@@@@@@@@")
							// MonCorrMoneyTotal = MonCorrMoneyTotal + CSng(.fields("LocalPaid"))
							// Else
							// Mid$(strLine, 24, 9) = Format(Format(.fields("StatePaid"), "#,###.00"), "@@@@@@@@@")
							// MonCorrMoneyTotal = MonCorrMoneyTotal + CSng(.fields("StatePaid"))
						}
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage"))) != "")
						{
							if (FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")).Length <= 17)
							{
								Strings.MidSet(ref strLine, 42, 17, rsAM.Get_Fields_String("InfoMessage") + Strings.StrDup(17 - FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")).Length, " "));
							}
							else
							{
								Strings.MidSet(ref strLine, 42, 17, Strings.Mid(FCConvert.ToString(rsAM.Get_Fields_String("InfoMessage")), 1, 17));
							}
						}
					}
					temp = BuildCategoryCode(ref rsAM);
					if (temp.Length < 15)
					{
						Strings.MidSet(ref strLine, 61, 15, Strings.Format(temp, "!@@@@@@@@@@@@@@@@@@@@"));
					}
					else
					{
						Strings.MidSet(ref strLine, 61, 15, Strings.Format(Strings.Mid(temp, 1, 15), "!@@@@@@@@@@@@@@@@@@@@"));
					}
					Strings.MidSet(ref strLine, 77, 3, Strings.Format(rsAM.Get_Fields_String("OpID"), "!@@@"));
					rtf1.Text = rtf1.Text + strLine;
					rtf1.Text = rtf1.Text + "\r\n";
					LineCount += 1;
					rsAM.MoveNext();
				}
				if (MonCorrTotal > 0)
				{
					PrintTotalLine("TOTAL MONETARY CORRECTIONS:", ref MonCorrTotal, MonCorrMoneyTotal);
				}
			}
		}

		public string BuildCategoryCode(ref clsDRWrapper temp)
		{
			string BuildCategoryCode = "";
			if (temp.Get_Fields_String("TransactionType") != "LPS")
			{
				if (temp.Get_Fields_Double("registeredWeightNew") != 0 && temp.Get_Fields_Double("registeredWeightOld") != 0)
				{
					if (temp.Get_Fields_Double("registeredWeightNew") != temp.Get_Fields_Double("registeredWeightOld"))
					{
						BuildCategoryCode = BuildCategoryCode + "GVW CHANGE ";
					}
				}
				if (temp.Get_Fields_Double("SalesTax") != 0)
				{
					BuildCategoryCode = BuildCategoryCode + "Sales Tax ";
				}
				if (temp.Get_Fields_Double("TitleFee") != 0)
				{
					BuildCategoryCode = BuildCategoryCode + "Title Fee ";
				}
				if (temp.Get_Fields_Double("RegRateCharge") != 0)
				{
					BuildCategoryCode = BuildCategoryCode + "Reg Fee ";
				}
				if (temp.Get_Fields_Double("InitialFee") != 0)
				{
					BuildCategoryCode = BuildCategoryCode + "Vanity Plate Fee ";
				}
				if (temp.Get_Fields_Double("PlateFeeNew") != 0 || temp.Get_Fields_Double("PlateFeeReReg") != 0)
				{
					BuildCategoryCode = BuildCategoryCode + "Specialty Plate Fee ";
				}
				if (temp.Get_Fields_Double("ReplacementFee") != 0)
				{
					BuildCategoryCode = BuildCategoryCode + "Replacement Fee ";
				}
				if (temp.Get_Fields_Double("TransferFee") != 0)
				{
					BuildCategoryCode = BuildCategoryCode + "Transfer Fee ";
				}
			}
			else
			{
				if (temp.Get_Fields_Double("ReplacementFee") != 0)
				{
					BuildCategoryCode = BuildCategoryCode + "Replacement Fee ";
				}
				if (temp.Get_Fields_Double("StickerFee") != 0)
				{
					BuildCategoryCode = BuildCategoryCode + "Sticker Fee ";
				}
			}
			return BuildCategoryCode;
		}

		public string strExceptionHeading(ref int int1)
		{
			string strExceptionHeading = "";
			// "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
			if (int1 == 1)
			{
				strExceptionHeading = "                            REPLACEMENT" + "\r\n";
				strExceptionHeading = strExceptionHeading + " CLASS/PLATE  DATE ISSUED   STICKER        EXPLANATION---------     FEE   OPID" + "\r\n";
			}
			else if (int1 == 2)
			{
				strExceptionHeading = "   ORIGINAL                             REPLACEMENT" + "\r\n";
				strExceptionHeading = strExceptionHeading + "    DECAL      CLASS/PLATE  DATE ISSUED    DECAL     EXPLANATION--------- OPID" + "\r\n";
			}
			else if (int1 == 3)
			{
				strExceptionHeading = "   ORIGINAL          REPLACEMENT" + "\r\n";
				strExceptionHeading = strExceptionHeading + "  CLASS/PLATE        CLASS/PLATE  DATE ISSUED    EXPLANATION---------     OPID" + "\r\n";
			}
			else if (int1 == 4)
			{
				strExceptionHeading = "          TAX RCPT  CLASS" + "\r\n";
				strExceptionHeading = strExceptionHeading + "DATE       NUMBER   PLATE        AMOUNT  EXPLANATION------ CATY ADJUSTED    OPID" + "\r\n";
			}
			else if (int1 == 5)
			{
				strExceptionHeading = "          TAX RCPT" + "\r\n";
				strExceptionHeading = strExceptionHeading + "DATE       NUMBER   CLASS/PLATE     EXPLANATION--------                              OPID" + "\r\n";
			}
			else if (int1 == 6)
			{
				strExceptionHeading = "ADJUSTMENTS  CATEGORY------       EXPLANATION--------                     OPID";
			}
			else if (int1 == 7)
			{
				strExceptionHeading = "PERMIT" + "\r\n";
				strExceptionHeading = strExceptionHeading + "NUMBER    PERMIT TYPE    APPLICANT NAME------   CLASS/PLATE   FEE------    OPID" + "\r\n";
			}
			else if (int1 == 8)
			{
				strExceptionHeading = strExceptionHeading + "FORM #    DATE      CLASS/PLATE  REGISTRANT'S NAME                        OPID" + "\r\n";
			}
			return strExceptionHeading;
		}

		public string strGiftCertificateHeading(ref int int1)
		{
			string strGiftCertificateHeading = "";
			// "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
			if (int1 == 1)
			{
				strGiftCertificateHeading = "                                     ISSUED" + "\r\n" + "\r\n";
				strGiftCertificateHeading = strGiftCertificateHeading + " GIFT CERT /" + "\r\n";
				strGiftCertificateHeading = strGiftCertificateHeading + " VOUCHER" + "\r\n";
				strGiftCertificateHeading = strGiftCertificateHeading + " NUMBER         AMOUNT    NAME                            DATE ISSUED   OPID" + "\r\n";
			}
			else if (int1 == 2)
			{
				strGiftCertificateHeading = "                                    REDEEMED" + "\r\n" + "\r\n";
				strGiftCertificateHeading = strGiftCertificateHeading + " GIFT CERT /" + "\r\n";
				strGiftCertificateHeading = strGiftCertificateHeading + " VOUCHER" + "\r\n";
				strGiftCertificateHeading = strGiftCertificateHeading + " NUMBER         NAME                         CLASS/PLATE  DATE REDEEMED   OPID" + "\r\n";
			}
			return strGiftCertificateHeading;
		}

		public void InventoryAdjustments(List<string> batchReports = null)
		{
			// VB6 Bad Scope Dim:
			int lng1 = 0;
			int lng2 = 0;
			string strLow = "";
			string strHigh = "";
			string strIT = "";
			int counter;
			lngMVR3Count = 0;
			lngSingleStickerCount = 0;
			lngDoubleStickerCount = 0;
			lngCombinationStickerCount = 0;
			lngDecalCount = 0;
			lngBoosterCount = 0;
			lngSpecialPermitCount = 0;
			lngTransitCount = 0;
			MotorVehicle.Statics.pcPlateInfo = new MotorVehicle.PlateCounter[0 + 1];
			MotorVehicle.Statics.intPlateCountCounter = 0;
			LineCount = 0;
			PageCount = 0;
			//rtf1.RightMargin = 5;
			if (cmbInterim.Text == "Interim Reports")
			{
				strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND InventoryType <> 'PXSLT' AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
			}
			else
			{
				// strSQL = Get_SQL_Dates(cboStart.Text, cboEnd.Text, "CloseoutInventory", "PeriodCloseoutID")
				strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(strSql);
				lng1 = 0;
				lng2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lng1 = rs.Get_Fields_Int32("ID");
					rs.MoveFirst();
					lng2 = rs.Get_Fields_Int32("ID");
				}
				strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND InventoryType <> 'PXSLT' AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
			}
			rs.OpenRecordset(strSql);
			// 
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** INVENTORY ADJUSTMENT REPORT ****";
				PrintHeading(PageCount);
				// Show MVR3 forms
				strIT = "M";
				rtf1.Text = rtf1.Text + strHeadingLine(29, "--------- MVR FORMS ----------");
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				if (cmbInterim.Text == "Interim Reports")
				{
					strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
				}
				else
				{
					strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
				}
				rs.OpenRecordset(strSql);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (!rs.EndOfFile())
					{
						strLine = Strings.StrDup(80, " ");
						if (rs.Get_Fields_String("AdjustmentCode") == "A" || rs.Get_Fields_String("AdjustmentCode") == "R")
						{
							ShowAdjustmentRecord(ref rs);
						}
						rs.MoveNext();
					}
				}
				// Show Booster forms
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(29, "--------- PERMITS ----------");
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				if (cmbInterim.Text == "Interim Reports")
				{
					strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND (InventoryType = 'BXSXX' OR InventoryType = 'PXSXX' OR InventoryType = 'RXSXX') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
				}
				else
				{
					strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND (InventoryType = 'BXSXX' OR InventoryType = 'PXSXX' OR InventoryType = 'RXSXX') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
				}
				rs.OpenRecordset(strSql);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (!rs.EndOfFile())
					{
						strLine = Strings.StrDup(80, " ");
						if (rs.Get_Fields_String("AdjustmentCode") == "A" || rs.Get_Fields_String("AdjustmentCode") == "R")
						{
							ShowAdjustmentRecord(ref rs);
						}
						rs.MoveNext();
					}
				}
				// Show Decals
				strIT = "D";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(29, "---------- DECALS -----------");
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				if (cmbInterim.Text == "Interim Reports")
				{
					strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
				}
				else
				{
					strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
				}
				rs.OpenRecordset(strSql);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (!rs.EndOfFile())
					{
						strLine = Strings.StrDup(80, " ");
						if (rs.Get_Fields_String("AdjustmentCode") == "A" || rs.Get_Fields_String("AdjustmentCode") == "R")
						{
							ShowAdjustmentRecord(ref rs);
						}
						rs.MoveNext();
					}
				}
				// Show Plates
				strIT = "P";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(29, "---------- PLATES -----------");
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				if (cmbInterim.Text == "Interim Reports")
				{
					strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND (Left(InventoryType, 1) = '" + strIT + "' AND InventoryType <> 'PXSXX') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
				}
				else
				{
					strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND (Left(InventoryType, 1) = '" + strIT + "' AND InventoryType <> 'PXSXX') ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
				}
				rs.OpenRecordset(strSql);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (!rs.EndOfFile())
					{
						strLine = Strings.StrDup(80, " ");
						if (rs.Get_Fields_String("AdjustmentCode") == "A" || rs.Get_Fields_String("AdjustmentCode") == "R")
						{
							ShowAdjustmentRecord(ref rs);
						}
						rs.MoveNext();
					}
				}
				// Show stickers
				strIT = "S";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(29, "--------- STICKERS ----------");
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				if (cmbInterim.Text == "Interim Reports")
				{
					strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID < 1 AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
				}
				else
				{
					strSql = "SELECT * FROM InventoryAdjustments WHERE PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + " AND (AdjustmentCode = 'A' OR AdjustmentCode = 'R') AND Left(InventoryType, 1) = '" + strIT + "' ORDER BY substring(InventoryType,1,1), DateOfAdjustment";
				}
				rs.OpenRecordset(strSql);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (!rs.EndOfFile())
					{
						strLine = Strings.StrDup(80, " ");
						if (rs.Get_Fields_String("AdjustmentCode") == "A" || rs.Get_Fields_String("AdjustmentCode") == "R")
						{
							ShowAdjustmentRecord(ref rs);
						}
						rs.MoveNext();
					}
				}
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(1, "SUMMARY OF INVENTORY ADJUSTMENTS:");
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(1, "FORMS:       MVR-3               :" + FCConvert.ToString(lngMVR3Count));
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(1, "STICKERS:    SINGLE              :" + FCConvert.ToString(lngSingleStickerCount));
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(1, "             DOUBLE              :" + FCConvert.ToString(lngDoubleStickerCount));
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(1, "             COMBINATION         :" + FCConvert.ToString(lngCombinationStickerCount));
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(1, "             DECALS              :" + FCConvert.ToString(lngDecalCount));
				rtf1.Text = rtf1.Text + "\r\n";
				if (MotorVehicle.Statics.intPlateCountCounter > 0)
				{
					for (counter = 0; counter <= MotorVehicle.Statics.intPlateCountCounter - 1; counter++)
					{
						if (counter == 0)
						{
							rtf1.Text = rtf1.Text + strHeadingLine(1, "PLATES:      CLASS TYPE " + MotorVehicle.Statics.pcPlateInfo[counter].Class + "       :" + FCConvert.ToString(MotorVehicle.Statics.pcPlateInfo[counter].AdjustmentCount));
							rtf1.Text = rtf1.Text + "\r\n";
						}
						else
						{
							rtf1.Text = rtf1.Text + strHeadingLine(1, "             CLASS TYPE " + MotorVehicle.Statics.pcPlateInfo[counter].Class + "       :" + FCConvert.ToString(MotorVehicle.Statics.pcPlateInfo[counter].AdjustmentCount));
							rtf1.Text = rtf1.Text + "\r\n";
						}
					}
				}
				rtf1.Text = rtf1.Text + strHeadingLine(1, "PERMITS:     BOOSTERS            :" + FCConvert.ToString(lngBoosterCount));
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(1, "             MVR-10              :" + FCConvert.ToString(lngSpecialPermitCount));
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(1, "             TRANSIT             :" + FCConvert.ToString(lngTransitCount));
				rtf1.Text = rtf1.Text + "\r\n";
			}
			else
			{
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** INVENTORY ADJUSTMENT REPORT ****";
				PrintHeading(1);
				rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
				strLine = (Strings.StrDup(33, " ") + "No Information");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
			}
			if (rtf1.Text != "")
			{
				FCFileSystem.FileOpen(33,Path.Combine("BMVReports", strResCode + "IA.RPT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.PrintLine(33, rtf1.Text);
				FCFileSystem.FileClose(33);
			}

			if (AllFlag)
			{
				rptInventoryAdjustments.InstancePtr.PrintReport(false, batchReports: batchReports);
				rptInventoryAdjustments.InstancePtr.Unload();

				CheckIt: ;
				if (MotorVehicle.JobComplete("rptInventoryAdjustments"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckIt;
				}
			}
			else
			{
				rptInventoryAdjustments.InstancePtr.Unload();
				frmReportViewer.InstancePtr.Init(rptInventoryAdjustments.InstancePtr);
			}

			rs.Reset();
		}

		public void PrintHeading(int intpage)
		{
			clsDRWrapper rs1 = new clsDRWrapper();
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (MotorVehicle.Statics.AllowRentals)
			{
				strLevel += "/RENTAL";
			}
			if (cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(cboEnd.Text) == true)
				{
					if (Information.IsDate(cboStart.Text) == true)
					{
						if (cboEnd.Text == cboStart.Text)
						{
							int lngPK1 = 0;
							strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + cboEnd.Text + "'";
							rs.OpenRecordset(strSql);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = rs.Get_Fields_Int32("ID");
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = FCConvert.ToString(rs.Get_Fields_DateTime("IssueDate"));//Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			// 
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				strVendorID = "TRIO";
				strMuni = FCConvert.ToString(rs1.Get_Fields_String("ReportTown"));
				strTownCode = Strings.Format(rs1.Get_Fields_String("ResidenceCode"), "00000");
				strAgent = fecherFoundation.Strings.Trim(FCConvert.ToString(rs1.Get_Fields_String("ReportAgent")));
				strVersion = Application.ProductVersion;
				strPhone = FCConvert.ToString(rs1.Get_Fields_String("ReportTelephone"));
			}
			int intHeadingTab;
			intHeadingTab = 40 - (strHeading.Length / 2);
			// 
			// 
			strLine = Strings.StrDup(80, " ");
			Strings.MidSet(ref strLine, intHeadingTab, Strings.Len(strHeading), strHeading);
			rtf1.Text = rtf1.Text + strLine;
			rtf1.Text = rtf1.Text + "\r\n";
			strLine = Strings.StrDup(80, " ");
			intHeadingTab = 40 - ("BUREAU OF MOTOR VEHICLES".Length / 2);
			Strings.MidSet(ref strLine, intHeadingTab, Strings.Len("BUREAU OF MOTOR VEHICLES"), "BUREAU OF MOTOR VEHICLES");
			rtf1.Text = rtf1.Text + strLine;
			rtf1.Text = rtf1.Text + "\r\n";
			rtf1.Text = rtf1.Text + "\r\n";
			// 
			strLine = Strings.StrDup(80, " ");
			Strings.MidSet(ref strLine, 1, 20 + Strings.Len(strVendorID), "VENDOR ID#: " + strVendorID);
			Strings.MidSet(ref strLine, 37, 20 + Strings.Len(strVersion), "VERSION UPDATE#   : " + strVersion);
			Strings.MidSet(ref strLine, 73, 6, "PAGE " + FCConvert.ToString(intpage));
			rtf1.Text = rtf1.Text + strLine + "\r\n";
			// 
			strLine = Strings.StrDup(80, " ");
			Strings.MidSet(ref strLine, 1, 20 + Strings.Len(strMuni), "MUNICIPALITY: " + strMuni);
			Strings.MidSet(ref strLine, 37, 30, "DATE OF REPORT    : " + Strings.Format(DateTime.Now, "MM/dd/yyyy"));
			rtf1.Text = rtf1.Text + strLine + "\r\n";
			// 
			strLine = Strings.StrDup(80, " ");
			Strings.MidSet(ref strLine, 1, 20 + Strings.Len(strTownCode), "TOWN/COUNTY CODE: " + strTownCode);
			Strings.MidSet(ref strLine, 37, 43, "PROCESS DATE RANGE: " + strProcessDate);
			rtf1.Text = rtf1.Text + strLine + "\r\n";
			// 
			strLine = Strings.StrDup(80, " ");
			Strings.MidSet(ref strLine, 1, 20 + Strings.Len(strLevel), "AUTHORIZATION TYPE: " + strLevel);
			Strings.MidSet(ref strLine, 37, 31, "DATE RECEIVED     : " + "___/___/___");
			rtf1.Text = rtf1.Text + strLine + "\r\n";
			// 
			strLine = Strings.StrDup(80, " ");
			Strings.MidSet(ref strLine, 1, 20 + Strings.Len(strAgent), "AGENT: " + strAgent);
			Strings.MidSet(ref strLine, 37, 20 + Strings.Len(strPhone), "TOWN PHONE NUMBER : " + strPhone);
			rtf1.Text = rtf1.Text + strLine + "\r\n";
			// 
			rtf1.Text = rtf1.Text + "\r\n";
			rtf1.Text = rtf1.Text + "\r\n";
			if (cmbReport.Text == "Full Set Of Reports")
			{
				if (strReportPrinting == "TS")
				{
					rtf1.Text = rtf1.Text + strHeadingLine(7, "---- TOWN SUMMARY ----          |        ---- FOR BMV USE ONLY ----");
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + strHeadingLine(39, "|");
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + strHeadingLine(1, "CATEGORY             UNITS    DOLLARS |     CATEGORY         UNITS    DOLLARS");
				}
				else if (strReportPrinting == "TD")
				{
					rtf1.Text = rtf1.Text + strHeadingLine(52, "     UNITS       DOLLARS");
					rtf1.Text = rtf1.Text + "\r\n";
				}
				else if (strReportPrinting == "IR")
				{
				}
				else if (strReportPrinting == "VD")
				{
					rtf1.Text = rtf1.Text + strHeadingLine(3, "DATE     INV   QUANTITY  ------RANGE------  OPERATOR") + "\r\n";
					rtf1.Text = rtf1.Text + strHeadingLine(2, "ADJUSTED  TYPE  ADJUSTED      ADJUSTED       INITIALS  EXPLANATION-------------") + "\r\n";
				}
				else if (strReportPrinting == "IA")
				{
					rtf1.Text = rtf1.Text + strHeadingLine(3, "DATE     QUANTITY  ------RANGE------  OPERATOR") + "\r\n";
					rtf1.Text = rtf1.Text + strHeadingLine(2, "ADJUSTED   ADJUSTED      ADJUSTED       INITIALS  EXPLANATION-------------") + "\r\n";
				}
				else if (strReportPrinting == "ET")
				{
					rtf1.Text = rtf1.Text + strHeadingLine(3, "FORM# USED     FORM# USED     FORM# USED     FORM# USED     FORM# USED") + "\r\n";
				}
				else if (strReportPrinting == "EX")
				{
					// Do Nothing    ** Will Print Headings in ExceptionReport Routine
				}
				else if (strReportPrinting == "GC")
				{
					// Do Nothing    ** Will Print Headings in GiftCertVouchers Routine
				}
				else if (strReportPrinting == "TA")
				{
					rtf1.Text = rtf1.Text + strHeadingLine(2, "TITLE") + "\r\n";
					rtf1.Text = rtf1.Text + strHeadingLine(2, "APP #     FEE   CLASS/PLATE    DATE    CUSTOMER---------                 OPID") + "\r\n";
				}
				else if (strReportPrinting == "UT")
				{
					rtf1.Text = rtf1.Text + strHeadingLine(2, "USE TAX   USE TAX") + "\r\n";
					rtf1.Text = rtf1.Text + strHeadingLine(2, "TYPE      AMOUNT    CLASS/PLATE    DATE    CUSTOMER---------               OPID") + "\r\n";
				}
				if (strReportPrinting != "TD" && strReportPrinting != "TS" && strReportPrinting != "IR")
				{
					rtf1.Text = rtf1.Text + "\r\n";
					rtf1.Text = rtf1.Text + "\r\n";
				}
			}
			else if (cmbReport.Text == "Town Summary")
			{
				rtf1.Text = rtf1.Text + strHeadingLine(7, "---- TOWN SUMMARY ----          |        ---- FOR BMV USE ONLY ----");
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(39, "|");
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(1, "CATEGORY             UNITS    DOLLARS |     CATEGORY         UNITS    DOLLARS");
			}
			else if (cmbReport.Text == "Town Detail")
			{
				rtf1.Text = rtf1.Text + strHeadingLine(52, "     UNITS       DOLLARS");
				rtf1.Text = rtf1.Text + "\r\n";
			}
			else if (cmbReport.Text == "Inventory Report")
			{
			}
			else if (cmbReport.Text == "Inventory Adjustment Report")
			{
				rtf1.Text = rtf1.Text + strHeadingLine(3, "DATE     QUANTITY  ------RANGE------  OPERATOR") + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(2, "ADJUSTED   ADJUSTED      ADJUSTED       INITIALS  EXPLANATION-------------") + "\r\n";
			}
			else if (cmbReport.Text == "Voided Forms Report")
			{
				rtf1.Text = rtf1.Text + strHeadingLine(3, "DATE     INV   QUANTITY  ------RANGE------  OPERATOR") + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(2, "ADJUSTED  TYPE  ADJUSTED      ADJUSTED       INITIALS  EXPLANATION-------------") + "\r\n";
			}
			else if (cmbReport.Text == "Excise Tax Only Report")
			{
				rtf1.Text = rtf1.Text + strHeadingLine(3, "FORM# USED     FORM# USED     FORM# USED     FORM# USED     FORM# USED") + "\r\n";
			}
			else if (cmbReport.Text == "Exception Report")
			{
				// Do Nothing    ** Will Print Headings in ExceptionReport Routine
			}
			else if (cmbReport.Text == "Title Application Report")
			{
				rtf1.Text = rtf1.Text + strHeadingLine(2, "TITLE") + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(2, "APP #     FEE   CLASS/PLATE    DATE    CUSTOMER---------                 OPID") + "\r\n";
			}
			else if (cmbReport.Text == "Gift Cert / Voucher Report")
			{
				// Do Nothing    ** Will Print Headings in GiftCertVouchers Routine
			}
			else if (cmbReport.Text == "Gift Cert / Voucher Report")
			{
			}
			else if (cmbReport.Text == "Temporary Plate Register")
			{
				rtf1.Text = rtf1.Text + strHeadingLine(2, "TEMPORARY                                                 CLASS/") + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(2, "PLATE NUMBER    NAME-----------                              PLATE     OPID") + "\r\n";
			}
			else if (cmbReport.Text == "File - Create / Verify / Reports")
			{
			}
			else if (cmbReport.Text == "File Verify Report")
			{
				// rtf1.Text = rtf1.Text & strHeadingLine(2, "CLASS/PLATE  TX RCPT       FEE  OWNER----------                     YR     STKR") & vbCrLf
			}
			else if (cmbReport.Text == "Use Tax Summary Report")
			{
				rtf1.Text = rtf1.Text + strHeadingLine(2, "USE TAX   USE TAX") + "\r\n";
				rtf1.Text = rtf1.Text + strHeadingLine(2, "TYPE      AMOUNT    CLASS/PLATE    DATE    CUSTOMER---------               OPID") + "\r\n";
			}
			if (cmbReport.Text != "Town Detail" && cmbReport.Text != "Town Detail" && cmbReport.Text != "Inventory Report")
			{
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
			}
		}
		// vbPorter upgrade warning: Source As RichTextBox	OnWrite(RichTextLib.RichTextBox)
		private void PrinterPrintMV(ref fecherFoundation.FCRichTextBox Source, int Indent)
		{
			int c;
			bool firstpage;
			firstpage = true;
			for (c = 1; c <= modAPIsConst.SendMessageByNum(Source.Handle.ToInt32(), MotorVehicle.lngGETLINECOUNT, 0, 0) - 1; c++)
			{
				FCGlobal.Printer.CurrentX = Indent;
				if (GetLine(ref Source, c - 1) == "CL  PLATE   TX RCPT   FEE  OWNER------------                       YR STKR ")
				{
					if (firstpage)
					{
						firstpage = false;
						FCGlobal.Printer.Print(GetLine(ref Source, c - 1));
					}
					else
					{
						FCGlobal.Printer.NewPage();
						FCGlobal.Printer.Print(GetLine(ref Source, c - 1));
					}
				}
				else if (fecherFoundation.Strings.Trim(GetLine(ref Source, c - 1)) == "**** DISKETTE VERIFICATION SUMMARY REPORT ****")
				{
					FCGlobal.Printer.NewPage();
					FCGlobal.Printer.Print(GetLine(ref Source, c - 1));
				}
				else if (GetLine(ref Source, c - 1) == "______________________________  PAGE BREAK  ______________________________")
				{
					FCGlobal.Printer.NewPage();
				}
				else
				{
					FCGlobal.Printer.Print(GetLine(ref Source, c - 1));
					if (bolInventoryReconciliation == true)
					{
						// Do Nothing
					}
					else
					{
						// If c Mod 64 = 0 Then
						// Printer.NewPage
						// PageCount = PageCount + 1
						// Call PrintHeading(PageCount)
						// End If
					}
				}
			}
			// c
			FCGlobal.Printer.EndDoc();
		}

		private string GetLine(ref fecherFoundation.FCRichTextBox Source, int LineNumber)
		{
			string GetLine = "";
			// vbPorter upgrade warning: byteLow As byte	OnWriteFCConvert.ToInt32(
			byte byteLow;
			// vbPorter upgrade warning: byteHigh As byte	OnWriteFCConvert.ToInt32(
			byte byteHigh;
			string strBuffer;
			int lngReturnedSize;
			// 
			byteLow = Convert.ToByte(MotorVehicle.lngMAX_CHAR_PER_LINE & (255));
			byteHigh = Convert.ToByte(Conversion.Int(MotorVehicle.lngMAX_CHAR_PER_LINE / 256.0));
			// 
			strBuffer = FCConvert.ToString(Convert.ToChar(byteLow)) + FCConvert.ToString(Convert.ToChar(byteHigh)) + Strings.Space(MotorVehicle.lngMAX_CHAR_PER_LINE - 2);
			// 
			lngReturnedSize = MotorVehicle.SendMessageAsString(Source.Handle.ToInt32(), MotorVehicle.lngGETLINE, LineNumber, strBuffer);
			// *** If last two characters are CRLF then remove them
			if (Strings.InStrRev(strBuffer, "\r\n", -1, CompareConstants.vbTextCompare/*?*/) == lngReturnedSize - 1 && strBuffer.Length > 2)
				lngReturnedSize -= 2;
			// 
			GetLine = Strings.Left(strBuffer, lngReturnedSize);
			return GetLine;
		}
		// vbPorter upgrade warning: intStart As int	OnWrite(double, int)
		private string strHeadingLine(int intStart, string strPrint)
		{
			string strHeadingLine = "";
			strHeadingLine = Strings.StrDup(80, " ");
			Strings.MidSet(ref strHeadingLine, intStart, Strings.Len(strPrint), strPrint);
			return strHeadingLine;
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (cmbReport.Text == "Full Set Of Reports" || cmbReport.Text == "File - Create / Verify / Reports" ||
				cmbReport.Text == "Create Data File" || cmbReport.Text == "File Verify Report" || cmbReport.Text == "Copy Reports")
			{
				if (cmbReport.Text != "Full Set Of Reports")
				{
					cmdPrint.Text = "Process";
				}
				else
				{
					cmdPrint.Text = "Print";
				}
			}
			else if (cmbReport.Text == "Inventory Report")
			{
				cmdPrint.Text = "Print";
				cmdPrint.Enabled = false;
				fraInventoryOptions.Visible = true;
			}
			else if (cmbReport.Text == "Registration Listing")
			{
				cmdPrint.Text = "Print";
				cmdPrint.Enabled = false;
				cmbReport.Enabled = false;
				cmbInterim.Enabled = false;
				fraClose.Enabled = false;
				fraListOptions.Visible = true;
			}
			else
			{
				cmdPrint.Text = "Print";
			}
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}

		private bool SetCloseoutID(ref string str1, ref int lng1)
		{
			bool SetCloseoutID = false;
			SetCloseoutID = false;
			clsDRWrapper rs1 = new clsDRWrapper();
			rs1.OpenRecordset(str1);
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				while (!rs1.EndOfFile())
				{
					if (MotorVehicle.Statics.blnUseTellerCloseout)
					{
						if (rs1.Get_Fields_Int32("TellerCloseoutID") != 0)
						{
							rs1.Edit();
							if (Strings.InStr(1, str1, "OldMVR3", CompareConstants.vbBinaryCompare) != 0)
							{
								rs1.Set_Fields("OldMVR3", lng1);
							}
							else
							{
								rs1.Set_Fields("PeriodCloseoutID", lng1);
							}
							rs1.Update();
						}
					}
					else
					{
						rs1.Edit();
						if (Strings.InStr(1, str1, "OldMVR3", CompareConstants.vbBinaryCompare) != 0)
						{
							rs1.Set_Fields("OldMVR3", lng1);
						}
						else
						{
							rs1.Set_Fields("PeriodCloseoutID", lng1);
						}
						rs1.Update();
					}
					rs1.MoveNext();
				}
				SetCloseoutID = true;
			}
			else
			{
				SetCloseoutID = true;
			}
			return SetCloseoutID;
		}

		public bool PerformInventoryCloseout(ref string str1, ref int lng1)
		{
			bool PerformInventoryCloseout = false;
			PerformInventoryCloseout = false;
			int lngHoldNumber = 0;
			int lngHoldHigh = 0;
			int lngNewNumber = 0;
			clsDRWrapper rsIOH = new clsDRWrapper();
			clsDRWrapper rs1 = new clsDRWrapper();
			clsDRWrapper rs2 = new clsDRWrapper();
			string strHoldType = "";
			int lngCount = 0;
			string strCode = "";
			string strOriginalPrefix = "";
			string strStartPlate = "";
			rs1.OpenRecordset(str1);
			rsIOH.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout");
			// MVR-3's
			if (MotorVehicle.Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'M' And (((Status = 'A' OR Status = 'P') AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY Prefix, Number, Suffix");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'M' And (Status = 'A' OR Status = 'P') ORDER BY Prefix, Number, Suffix");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
				if (rsIOH.NoMatch == true)
				{
					rsIOH.AddNew();
				}
				else
				{
					rsIOH.Edit();
				}
				rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
				if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
				{
					rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + rs2.RecordCount());
				}
				else
				{
					rsIOH.Set_Fields("Number", rs2.RecordCount());
				}
				rsIOH.Set_Fields("PeriodCloseoutID", lng1);
				rsIOH.Update();
				lngHoldNumber = rs2.Get_Fields_Int32("Number");
				lngNewNumber = rs2.Get_Fields_Int32("Number") - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					if (lngNewNumber != rs2.Get_Fields_Int32("Number"))
					{
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = rs1.Get_Fields_Int32("High");
						rs1.Set_Fields("CloseoutInventoryDate", DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						lngHoldNumber = rs2.Get_Fields_Int32("Number");
						lngNewNumber = rs2.Get_Fields_Int32("Number");
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", "MXS00");
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Boosters
			if (MotorVehicle.Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'B' And ((Status = 'A' AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY Prefix, Number, Suffix");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'B' And Status = 'A' ORDER BY Prefix, Number, Suffix");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
				if (rsIOH.NoMatch == true)
				{
					rsIOH.AddNew();
				}
				else
				{
					rsIOH.Edit();
				}
				rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
				if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
				{
					rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + rs2.RecordCount());
				}
				else
				{
					rsIOH.Set_Fields("Number", rs2.RecordCount());
				}
				rsIOH.Set_Fields("PeriodCloseoutID", lng1);
				rsIOH.Update();
				lngHoldNumber = rs2.Get_Fields_Int32("Number");
				lngNewNumber = rs2.Get_Fields_Int32("Number") - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					if (lngNewNumber != rs2.Get_Fields_Int32("Number"))
					{
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = rs1.Get_Fields_Int32("High");
						rs1.Set_Fields("CloseoutInventoryDate", DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						lngHoldNumber = rs2.Get_Fields_Int32("Number");
						lngNewNumber = rs2.Get_Fields_Int32("Number");
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", "BXSXX");
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Special Reg Permits
			if (MotorVehicle.Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,2) = 'RX' And ((Status = 'A' AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY Prefix, Number, Suffix");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,2) = 'RX' And Status = 'A' ORDER BY Prefix, Number, Suffix");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
				if (rsIOH.NoMatch == true)
				{
					rsIOH.AddNew();
				}
				else
				{
					rsIOH.Edit();
				}
				rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
				if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
				{
					rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + rs2.RecordCount());
				}
				else
				{
					rsIOH.Set_Fields("Number", rs2.RecordCount());
				}
				rsIOH.Set_Fields("PeriodCloseoutID", lng1);
				rsIOH.Update();
				lngHoldNumber = rs2.Get_Fields_Int32("Number");
				lngNewNumber = rs2.Get_Fields_Int32("Number") - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					if (lngNewNumber != rs2.Get_Fields_Int32("Number"))
					{
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = rs1.Get_Fields_Int32("High");
						rs1.Set_Fields("CloseoutInventoryDate", DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						lngHoldNumber = rs2.Get_Fields_Int32("Number");
						lngNewNumber = rs2.Get_Fields_Int32("Number");
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", "RXSXX");
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Plate's
			string strprefix = "";
			string strSuffix = "";
			if (MotorVehicle.Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' And (((Status = 'A' OR Status = 'H' OR Status = 'P') AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY substring(Code,4,2), Suffix, Prefix,  Number");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' And (Status = 'A' OR Status = 'H' OR Status = 'P') ORDER BY substring(Code,4,2), Suffix, Prefix,  Number");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				strCode = rs2.Get_Fields_String("Code");
				lngHoldNumber = rs2.Get_Fields_Int32("Number");
				lngNewNumber = rs2.Get_Fields_Int32("Number") - 1;
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("prefix"))) != "")
					strprefix = FCConvert.ToString(rs2.Get_Fields_String("prefix"));
				else
					strprefix = "";
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "")
					strSuffix = FCConvert.ToString(rs2.Get_Fields_String("Suffix"));
				else
					strSuffix = "";
				strOriginalPrefix = strprefix;
				strStartPlate = FCConvert.ToString(rs2.Get_Fields_String("FormattedInventory"));
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					if (strprefix.Length > FCConvert.ToString(rs2.Get_Fields_String("prefix")).Length && Strings.Right(strprefix, 1) == "0")
					{
						strprefix = Strings.Left(strprefix, (strprefix.Length - (strprefix.Length - FCConvert.ToString(rs2.Get_Fields_String("prefix")).Length)));
					}
					if ((fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("prefix"))) != "" && strprefix != FCConvert.ToString(rs2.Get_Fields_String("prefix"))) || (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "" && strSuffix != FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) || (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"))))
					{
						rs2.MovePrevious();
						rsIOH.AddNew();
						rsIOH.Set_Fields("Type", strCode);
						if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
						{
							rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngNewNumber - lngHoldNumber);
						}
						else
						{
							rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber);
						}
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("prefix"))) != "")
							strprefix = FCConvert.ToString(rs2.Get_Fields_String("prefix"));
						else
							strprefix = "";
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "")
							strSuffix = FCConvert.ToString(rs2.Get_Fields_String("Suffix"));
						else
							strSuffix = "";
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", strCode);
						rs1.Set_Fields("Low", strStartPlate);
						// strOriginalPrefix & lngHoldNumber & strSuffix
						rs1.Set_Fields("High", rs2.Get_Fields_String("FormattedInventory"));
						// strPrefix & lngNewNumber - 1 & strSuffix
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						rs2.MoveNext();
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("prefix"))) != "")
							strprefix = FCConvert.ToString(rs2.Get_Fields_String("prefix"));
						else
							strprefix = "";
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("Suffix"))) != "")
							strSuffix = FCConvert.ToString(rs2.Get_Fields_String("Suffix"));
						else
							strSuffix = "";
						strOriginalPrefix = strprefix;
						strStartPlate = FCConvert.ToString(rs2.Get_Fields_String("FormattedInventory"));
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						strCode = FCConvert.ToString(rs2.Get_Fields("Code"));
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
					if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
					{
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", strStartPlate);
					// strOriginalPrefix & lngHoldNumber & strSuffix
					rs1.Set_Fields("High", rs2.Get_Fields_String("FormattedInventory"));
					// If .Fields("Number") = lngNewNumber Then
					// rs1.Fields("High") = strPrefix & lngNewNumber & strSuffix
					// Else
					// rs1.Fields("High") = strPrefix & lngNewNumber - 1 & strSuffix
					// End If
					rs1.Set_Fields("CloseoutInventoryDate", DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Single Sickers
			if (MotorVehicle.Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'S' And (((Status = 'A' OR Status = 'P') AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'S' And (Status = 'A' OR Status = 'P') ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				strCode = rs2.Get_Fields_String("Code");
				lngHoldNumber = rs2.Get_Fields_Int32("Number");
				lngNewNumber = rs2.Get_Fields_Int32("Number") - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					if (lngNewNumber != rs2.Get_Fields_Int32("Number") || strCode != rs2.Get_Fields_String("Code"))
					{
						rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
						if (rsIOH.NoMatch == true)
						{
							rsIOH.AddNew();
						}
						else
						{
							rsIOH.Edit();
						}
						rsIOH.Set_Fields("Type", strCode);
						if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
						{
							rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngNewNumber - lngHoldNumber);
						}
						else
						{
							rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber);
						}
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", strCode);
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						strCode = FCConvert.ToString(rs2.Get_Fields("Code"));
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
					if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
					{
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Combination Sickers
			if (MotorVehicle.Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'C' And (((Status = 'A' OR Status = 'P') AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'C' And (Status = 'A' OR Status = 'P') ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				strCode = FCConvert.ToString(rs2.Get_Fields("Code"));
				lngHoldNumber = rs2.Get_Fields_Int32("Number");
				lngNewNumber = rs2.Get_Fields_Int32("Number") - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					if (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields_Int32("Number")) || strCode != FCConvert.ToString(rs2.Get_Fields("Code")))
					{
						rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
						if (rsIOH.NoMatch == true)
						{
							rsIOH.AddNew();
						}
						else
						{
							rsIOH.Edit();
						}
						rsIOH.Set_Fields("Type", strCode);
						if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
						{
							rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngNewNumber - lngHoldNumber);
						}
						else
						{
							rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber);
						}
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", strCode);
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						rs1.Update();
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						strCode = FCConvert.ToString(rs2.Get_Fields("Code"));
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
					if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
					{
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Double Sickers
			if (MotorVehicle.Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'D' And (((Status = 'A' OR Status = 'P') AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'S' And substring(Code,3,1) = 'D' And (Status = 'A' OR Status = 'P') ORDER BY substring(Code,2,1), substring(Code,4,2), Prefix, Number, Suffix");
			}
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				strCode = rs2.Get_Fields_String("Code");
				lngHoldNumber = rs2.Get_Fields_Int32("Number");
				lngNewNumber = rs2.Get_Fields_Int32("Number") - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					if (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields_Int32("Number")) || strCode != FCConvert.ToString(rs2.Get_Fields("Code")))
					{
						rsIOH.AddNew();
						rsIOH.Set_Fields("Type", strCode);
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber);
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", strCode);
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						strCode = FCConvert.ToString(rs2.Get_Fields("Code"));
						rs1.Update();
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
					if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
					{
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// Truck Decals
			if (MotorVehicle.Statics.blnUseTellerCloseout)
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'D' And ((Status = 'A' AND TellerCloseoutID <> 0) OR ((Status = 'D' OR Status = 'I' OR Status = 'V') AND TellerCloseoutID = 0)) ORDER BY substring(Code,3,2), Prefix, Number, Suffix");
			}
			else
			{
				rs2.OpenRecordset("SELECT * FROM Inventory WHERE substring(Code,1,1) = 'D' And Status = 'A' ORDER BY substring(Code,3,2), Prefix, Number, Suffix");
			}
			string holdInventoryType = "";
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
				holdInventoryType = FCConvert.ToString(rs2.Get_Fields("Code"));
				lngNewNumber = rs2.Get_Fields_Int32("Number") - 1;
				while (!rs2.EndOfFile())
				{
					lngNewNumber += 1;
					if (lngNewNumber != FCConvert.ToInt32(rs2.Get_Fields_Int32("Number")) || holdInventoryType != FCConvert.ToString(rs2.Get_Fields("Code")))
					{
						rs1.AddNew();
						rs1.Set_Fields("InventoryType", holdInventoryType);
						rs1.Set_Fields("Low", lngHoldNumber);
						rs1.Set_Fields("High", lngNewNumber - 1);
						lngHoldHigh = lngNewNumber - 1;
						rs1.Set_Fields("CloseoutInventoryDate", DateTime);
						rs1.Set_Fields("PeriodCloseoutID", lng1);
						lngCount = lngNewNumber - lngHoldNumber;
						rs1.Update();
						rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + holdInventoryType, ",");
						if (rsIOH.NoMatch == true)
						{
							rsIOH.AddNew();
						}
						else
						{
							rsIOH.Edit();
						}
						rsIOH.Set_Fields("Type", holdInventoryType);
						if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
						{
							rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngCount);
						}
						else
						{
							rsIOH.Set_Fields("Number", lngCount);
						}
						rsIOH.Set_Fields("PeriodCloseoutID", lng1);
						rsIOH.Update();
						lngHoldNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						lngNewNumber = FCConvert.ToInt32(rs2.Get_Fields_Int32("Number"));
						holdInventoryType = FCConvert.ToString(rs2.Get_Fields("Code"));
					}
					rs2.MoveNext();
				}
				rs2.MovePrevious();
				if (lngHoldHigh != lngNewNumber)
				{
					rsIOH.FindFirstRecord2("PeriodCloseoutID, Type", FCConvert.ToString(lng1) + "," + rs2.Get_Fields_String("Code"), ",");
					if (rsIOH.NoMatch == true)
					{
						rsIOH.AddNew();
					}
					else
					{
						rsIOH.Edit();
					}
					rsIOH.Set_Fields("Type", rs2.Get_Fields("Code"));
					if (Conversion.Val(rsIOH.Get_Fields_Int32("Number")) > 0)
					{
						rsIOH.Set_Fields("Number", Conversion.Val(rsIOH.Get_Fields_Int32("Number")) + lngNewNumber - lngHoldNumber + 1);
					}
					else
					{
						rsIOH.Set_Fields("Number", lngNewNumber - lngHoldNumber + 1);
					}
					rsIOH.Set_Fields("PeriodCloseoutID", lng1);
					rsIOH.Update();
					rs1.AddNew();
					rs1.Set_Fields("InventoryType", rs2.Get_Fields("Code"));
					rs1.Set_Fields("Low", lngHoldNumber);
					rs1.Set_Fields("High", lngNewNumber);
					rs1.Set_Fields("CloseoutInventoryDate", DateTime);
					rs1.Set_Fields("PeriodCloseoutID", lng1);
					rs1.Update();
				}
				PerformInventoryCloseout = true;
			}
			// 
			PerformInventoryCloseout = true;
			rs1.Reset();
			rs2.Reset();
			return PerformInventoryCloseout;
		}

		private void MasterRecordFound()
		{
			clsDRWrapper rsTransferFee = new clsDRWrapper();
			double TransferAmount = 0;
			clsDRWrapper rsCheck = new clsDRWrapper();
			// If Duplicate Registration then
			/*? On Error GoTo 0 */
			if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "DPR")
			{
				DuplicateRegistration();
				return;
			}
			else if (rsAM.Get_Fields_Boolean("NoFeeDupReg") == true && rsAM.Get_Fields_Boolean("RENTAL") == true && (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "RRR" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "RRT" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "NRR" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "NRT" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "ECO"))
			{
				DuplicateRegistration();
			}
			// If Increased GVW
			if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "GVW")
			{
				IncreaseGVW();
				return;
			}
			// Add Excise Tax
			if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) != "LPS")
			{
				if (rsAM.Get_Fields_Decimal("ExciseTaxCharged") != 0)
				{
					WorkSCT = 185;
					WorkUnits = 1;
					if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "NRT" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "RRT" || rsAM.Get_Fields_Boolean("ExciseProrate") == true)
					{
						WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed"));
						if (WorkFee < 0)
							WorkFee = 0;
						rsCheck.OpenRecordset("SELECT * FROM DefaultInfo");
						if (FCConvert.ToString(rsCheck.Get_Fields_String("ExciseVSAgent")) == "E")
						{
							WorkFee += rsAM.Get_Fields_Decimal("ExciseTransferCharge");
						}
					}
					else
					{
						WorkFee = rsAM.Get_Fields_Decimal("ExciseTaxCharged");
					}
					if (WorkFee < 0)
						WorkFee = 0;
					if (strSummaryDetail == "D")
					{
						WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
					}
					else
					{
						curSummaryRecord[WorkSCT, 0] += WorkUnits;
						curSummaryRecord[WorkSCT, 1] += WorkFee;
					}
				}
			}
			if (rsAM.Get_Fields_Boolean("ETO") == true)
				return;
			// 
			rsClass.FindFirstRecord2("BMVCode, SystemCode", rsAM.Get_Fields_String("Class") + "," + rsAM.Get_Fields_String("Subclass"), ",");
			if (rsClass.NoMatch == true)
			{
				rsClass.FindFirstRecord("BMVCode", rsAM.Get_Fields_String("Class"));
			}
			// 
			if (strSummaryDetail == "D" && (Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 16 || Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 25 || Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 30 || Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 35 || Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 100 || Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 105 || Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 152 || Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 155 || Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 161 || Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 165))
			{
				// kk07202016  Leave Antique out of WorkSCT 5 (Pass Full)    'Val(rsClass.Fields("ShortDescription")) = 15 Or
				WorkSCT = 5;
			}
			else
			{
				WorkSCT = FCConvert.ToInt32(Math.Round(Conversion.Val(rsClass.Get_Fields("ShortDescription"))));
			}
			WorkCL = rsAM.Get_Fields_String("Class");
			WorkSC = rsAM.Get_Fields_String("Subclass");
			WorkWG = 0;
			if (Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 30)
			{
				WorkRG = 0;
			}
			else if (Conversion.Val(rsClass.Get_Fields("ShortDescription")) == 35)
			{
				WorkRG = 5;
			}
			else
			{
				WorkRG = 10;
			}
			WorkLV = 5;
			if (strSummaryDetail == "S" && rsAM.Get_Fields_Boolean("RENTAL") == true)
			{
				if (WorkSCT == 5)
				{
					WorkSCT = 25;
				}
				else if (WorkSCT == 39)
				{
					WorkSCT = 27;
				}
			}

			// kk01102018 tromvs-96  Force DV and VX > 600 lbs to show commercial weight on report
			if (rsClass.Get_Fields_Decimal("RegistrationFee") == 0.01m || WorkSCT == 40 && (WorkCL == "DV" || WorkCL == "VX"))
			{
				rsGVW.FindFirstRecord2("Type, Low, High", "TK," + rsAM.Get_Fields_String("registeredWeightNew") + "," + rsAM.Get_Fields_String("RegisteredWeightNew"), ",", "=,<=,>=");
				WorkWG = rsGVW.Get_Fields_Int32("High");
			}
			else if (rsClass.Get_Fields_Decimal("RegistrationFee") == 0.02m)
			{
				rsGVW.FindFirstRecord2("Type, Low, High", "FM," + rsAM.Get_Fields_String("registeredWeightNew") + "," + rsAM.Get_Fields_String("RegisteredWeightNew"), ",", "=,<=,>=");
				WorkWG = rsGVW.Get_Fields_Int32("High");
			}
			else if (rsClass.Get_Fields_Decimal("RegistrationFee") == 0.03m)
			{
				if (rsAM.Get_Fields_String("Class") == "SE")
				{
					if (Conversion.Val(rsAM.Get_Fields("NetWeight")) == 0)
					{
						rsGVW.FindFirstRecord2("Type, Low, High", "F2," + rsAM.Get_Fields_Int32("registeredWeightNew") + "," + rsAM.Get_Fields_Int32("RegisteredWeightNew"), ",", "=,<=,>=");
						WorkWG = FCConvert.ToInt32(rsGVW.Get_Fields("High"));
					}
					else
					{
						rsGVW.FindFirstRecord2("Type, Low, High", "SE," + rsAM.Get_Fields("NetWeight") + "," + rsAM.Get_Fields("NetWeight"), ",", "=,<=,>=");
						WorkWG = FCConvert.ToInt32(rsGVW.Get_Fields("High"));
					}
				}
				else
				{
					rsGVW.FindFirstRecord2("Type, Low, High", "F2," + rsAM.Get_Fields_Int32("registeredWeightNew") + "," + rsAM.Get_Fields_Int32("RegisteredWeightNew"), ",", "=,<=,>=");
					WorkWG = FCConvert.ToInt32(rsGVW.Get_Fields("High"));
				}
			}
			//FC:FINAL:MSH - i.issue #1831: replace wrong conversion
			//else if (FCConvert.ToInt32(rsClass.Get_Fields("RegistrationFee")) == 0)
			else if (rsClass.Get_Fields_Decimal("RegistrationFee") == 0)
			{
				WorkRG = 0;
				if (WorkSCT == 5 && strSummaryDetail == "S")
				{
					// kk07202016  Only change cat to Passenger No Fee on Summary Report
					WorkSCT = 30;
				}
			}
			if (rsAM.Get_Fields_Boolean("RegExempt") == true)
			{
				WorkRG = 0;
				if (WorkSCT == 5 && strSummaryDetail == "S")
				{
					// kk07202016  Only change cat to Passenger No Fee on Summary Report
					WorkSCT = 30;
				}
			}
			else if (rsAM.Get_Fields_Boolean("RegHalf") == true)
			{
				WorkRG = 5;
				if (WorkSCT == 5 && strSummaryDetail == "S")
				{
					// kk07202016  Only change cat to Passenger No Fee on Summary Report
					WorkSCT = 35;
				}
			}
			if (rsAM.Get_Fields_Boolean("TwoYear") == true)
			{
				WorkRG = 20;
				if (rsAM.Get_Fields_Boolean("RegHalf") == true)
					WorkRG = 15;
			}
			// 
			WorkUnits = 1;
			if (rsAM.Get_Fields_Decimal("TransferFee") != 0)
				WorkUnits = 0;
			if (rsAM.Get_Fields_Boolean("RegProrate") == true)
			{
				int intMonth = 0;
				WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("RegRateCharge") - rsAM.Get_Fields_Decimal("TransferCreditUsed"));
				intMonth = rsAM.Get_Fields_Int32("ProrateMonth");
				WorkRG = 100 + intMonth;
			}
			else
			{
				WorkFee = rsAM.Get_Fields_Decimal("RegRateCharge");
			}
			if (WorkFee < 0)
				WorkFee = 0;
			// 
			HOLDSCT = WorkSCT;
			HoldCL = WorkCL;
			HoldSC = WorkSC;
			HOLDWG = WorkWG;
			HoldRG = WorkRG;
			HoldLV = WorkLV;
			HoldUnits = WorkUnits;
			HoldFee = WorkFee;
			// 
			if (rsAM.Get_Fields_String("TransactionType") == "DPS")
			{
				goto DupSticker;
			}

			WorkUnits = 1;

			if (strSummaryDetail == "D")
			{
				if (Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "NRT" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "RRT" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "LPS" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "ECO")
				{
					// Transaction is a transfer do not add to passanger full
					if (Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "NRT" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "RRT" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "ECO")
					{
						rsTransferFee.OpenRecordset("SELECT * FROM DefaultInfo");
						TransferAmount = rsAM.Get_Fields_Double("RegRateCharge") - rsAM.Get_Fields_Double("TransferCreditUsed");
						if (TransferAmount < 0)
						{
							TransferAmount = 0;
						}
						// Dave 8/27/04 We can't take the transfer into account when deciding whether or not to add a fee because the state fee will always equal the amount we come up with
						if (rsAM.Get_Fields_Decimal("StatePaid") > rsAM.Get_Fields_Decimal("TransferFee") + rsAM.Get_Fields_Decimal("SalesTax") + rsAM.Get_Fields_Decimal("TitleFee") + rsAM.Get_Fields_Decimal("PlateFeeNew") + rsAM.Get_Fields_Decimal("PlateFeeReReg") + rsAM.Get_Fields_Decimal("ReplacementFee") + rsAM.Get_Fields_Decimal("StickerFee") + rsAM.Get_Fields_Decimal("InitialFee") + rsAM.Get_Fields_Decimal("OutOfRotation") - rsAM.Get_Fields_Decimal("GiftCertificateAmount"))
						{
							WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("StatePaid") - (rsAM.Get_Fields_Decimal("TransferFee") + rsAM.Get_Fields_Decimal("SalesTax") + rsAM.Get_Fields_Decimal("TitleFee") + rsAM.Get_Fields_Decimal("PlateFeeNew") + rsAM.Get_Fields_Decimal("PlateFeeReReg") + rsAM.Get_Fields_Decimal("ReplacementFee") + rsAM.Get_Fields_Decimal("StickerFee") + rsAM.Get_Fields_Decimal("InitialFee") + rsAM.Get_Fields_Decimal("OutOfRotation") - rsAM.Get_Fields_Decimal("GiftCertificateAmount")));

							FCFileSystem.Print(98, "" + FCConvert.ToString(rsAM.Get_Fields_String("plate")));
							FCFileSystem.Print(98, "" + FCConvert.ToString(rsAM.Get_Fields_Int32("MVR3")));
							FCFileSystem.PrintLine(98, "" + FCConvert.ToString(WorkFee));
							WorkSCT = 130;

							WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
						}
					}
				}
				else
				{
					//if (WorkSCT == 5 && rsAM.Get_Fields_Boolean("RENTAL") == true)
					//{
					//	//WorkSC = "RT";
					//	//WorkCL = "RT";
					//}
					if (WorkSCT == 5 && WorkCL == "PC")
					{
						if (WorkSC == "P1")
						{
							// Stock Car
							WorkSC = "SC";
							WorkCL = "SC";
						}
						else if (WorkSC == "P4")
						{
							// Dune Buggy
							WorkSC = "DY";
							WorkCL = "DY";
						}
						else if (WorkSC == "P7")
						{
							// National Guard 1-6000
							WorkSC = "NG";
							WorkCL = "NG";
						}
					}
					if (WorkSCT == 39 && WorkCL == "PC")
					{
						if (WorkSC == "P8")
						{
							// National Guard 6001-10000
							WorkSC = "NG";
							WorkCL = "NG";
						}
					}
					// kk07212016  Don't generate separate lines for Disable Vet disability levels, just Passenger and Pass Truck
					// DV-D3 -> DV-D1, DV-D4 -> DV-2, VX-V3 -> VX-V1, VX-V4 -> VX-V2
					if (WorkSCT == 5 && WorkCL == "DV")
					{
						if (WorkSC == "D3")
						{
							WorkSC = "D1";
						}
					}
					if (WorkSCT == 5 && WorkCL == "VX")
					{
						if (WorkSC == "V3")
						{
							WorkSC = "V1";
						}
					}
					if (WorkSCT == 40 && WorkCL == "DV")
					{
						// kk01102018 tromv1355  Changed DV/D2 from Pass Truck(39) to Commercial(40)
						if (WorkSC == "D4")
						{
							WorkSC = "D2";
						}
					}
					if (WorkSCT == 40 && WorkCL == "VX")
					{
						// kk01102018 tromv1355  Changed VX/C2 from Pass Truck(39) to Commercial(40)
						if (WorkSC == "V4")
						{
							WorkSC = "V2";
						}
					}

                    if ((WorkSCT == 5 || WorkSCT == 39) && WorkCL == "BB")
                    {
                        WorkCL = "BZ";
                    }

                    if ((WorkSCT == 5 || WorkSCT == 39) && WorkCL == "BC")
                    {
                        WorkCL = "BY";
                    }

					// kk02212017 tromvs-70  Correct SME/A and SME/B subcategories on Town Detail report
					if (WorkSCT == 50 && WorkCL == "TR")
					{
						if (WorkSC == "T1" || WorkSC == "T2")
						{
							// SME/A or SME/B
							WorkCL = "TZ";
						}
						else if (WorkSC == "T4")
						{
							// Road Tractor
							WorkSC = "RT";
						}
						else
						{
							// Farm Tractor (T5)
							WorkSC = "FT";
						}
					}

					if (rsAM.Get_Fields_Boolean("RENTAL") == true)
					{
						WorkSC = WorkSC + "R";
					}

					WriteNewRecord(WorkSCT, WorkCL, WorkSC, WorkWG, WorkRG, 5, WorkUnits, WorkFee);
					if (WorkWG > 0)
					{
						WriteNewRecord(WorkSCT, WorkCL, WorkSC, WorkWG, 0, 4, WorkUnits, WorkFee);
					}
					// kk07212016  Change to Only show Level 1, 4 and 5 for Island Use, Antique, Antique Motorcycle, Moped, Special Eq abd Bus
					if (WorkSCT != 10 && WorkSCT != 15 && WorkSCT != 17 && WorkSCT != 70 && WorkSCT != 75 && WorkSCT != 80)
					{
						if ((WorkCL == "CM") || (WorkCL == "CV") || (WorkCL == "CD") || (WorkCL == "DS") || (WorkCL == "FD") || (WorkCL == "GS") || (WorkCL == "VT") || (WorkCL == "WB") || (WorkCL == "DV") || (WorkCL == "DX") || (WorkCL == "MO") || (WorkCL == "PS") || (WorkCL == "PO") || (WorkCL == "PH") || (WorkCL == "VX") || (WorkCL == "EM"))
						{
							// Don't break these down
							WriteNewRecord(WorkSCT, WorkCL, "00", 0, 0, 2, WorkUnits, WorkFee);
						}
						else
						{
							WriteNewRecord(WorkSCT, WorkCL, WorkSC, 0, 0, 3, WorkUnits, WorkFee);
							WriteNewRecord(WorkSCT, WorkCL, "00", 0, 0, 2, WorkUnits, WorkFee);
						}
					}
					WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
					// End If
				}
			}
			else
			{
				if (Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "NRT" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "RRT" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "LPS" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "ECO")
				{
					// Transaction is a transfer do not add to passanger full
					if (Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "NRT" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "RRT" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "ECO")
					{
						rsTransferFee.OpenRecordset("SELECT * FROM DefaultInfo");
						TransferAmount = rsAM.Get_Fields_Double("RegRateCharge") - rsAM.Get_Fields_Double("TransferCreditUsed");
						if (TransferAmount < 0)
						{
							TransferAmount = 0;
						}
						// Dave 8/27/04 We can't take the transfer into account when deciding whether or not to add a fee because the state fee will always equal the amount we come up with
						if (rsAM.Get_Fields_Decimal("StatePaid") > rsAM.Get_Fields_Decimal("TransferFee") + rsAM.Get_Fields_Decimal("SalesTax") + rsAM.Get_Fields_Decimal("TitleFee") + rsAM.Get_Fields_Decimal("PlateFeeNew") + rsAM.Get_Fields_Decimal("PlateFeeReReg") + rsAM.Get_Fields_Decimal("ReplacementFee") + rsAM.Get_Fields_Decimal("StickerFee") + rsAM.Get_Fields_Decimal("InitialFee") + rsAM.Get_Fields_Decimal("OutOfRotation") - rsAM.Get_Fields_Decimal("GiftCertificateAmount"))
						{
							WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("StatePaid") - (rsAM.Get_Fields_Decimal("TransferFee") + rsAM.Get_Fields_Decimal("SalesTax") + rsAM.Get_Fields_Decimal("TitleFee") + rsAM.Get_Fields_Decimal("PlateFeeNew") + rsAM.Get_Fields_Decimal("PlateFeeReReg") + rsAM.Get_Fields_Decimal("ReplacementFee") + rsAM.Get_Fields_Decimal("StickerFee") + rsAM.Get_Fields_Decimal("InitialFee") + rsAM.Get_Fields_Decimal("OutOfRotation") - rsAM.Get_Fields_Decimal("GiftCertificateAmount")));
							if (WorkSCT == 5)
							{
								FCFileSystem.Print(97, "" + FCConvert.ToString(rsAM.Get_Fields_String("plate")));
								FCFileSystem.Print(97, "" + FCConvert.ToString(rsAM.Get_Fields_Int32("MVR3")));
								FCFileSystem.Print(97, "" + FCConvert.ToString(WorkFee));
								FCFileSystem.PrintLine(97, "" + FCConvert.ToString(rsAM.Get_Fields_Decimal("ExciseTaxCharged")));
							}
							// If rsClass.fields("RegistrationFee") <= 0.03 Then
							WorkSCT = 130;
							// End If
							// Output data to GVW.CHK
							FCFileSystem.Print(98, "" + FCConvert.ToString(rsAM.Get_Fields_String("plate")));
							FCFileSystem.Print(98, "" + FCConvert.ToString(rsAM.Get_Fields_Int32("MVR3")));
							FCFileSystem.PrintLine(98, "" + FCConvert.ToString(WorkFee));
							curSummaryRecord[WorkSCT, 0] += WorkUnits;
							curSummaryRecord[WorkSCT, 1] += WorkFee;
						}
					}
				}
				else
				{
					if (WorkSCT == 5)
					{
						FCFileSystem.Print(97, "" + FCConvert.ToString(rsAM.Get_Fields_String("plate")));
						FCFileSystem.Print(97, "" + FCConvert.ToString(rsAM.Get_Fields_Int32("MVR3")));
						FCFileSystem.Print(97, "" + FCConvert.ToString(WorkFee));
						FCFileSystem.PrintLine(97, "" + FCConvert.ToString(rsAM.Get_Fields_Decimal("ExciseTaxCharged")));
					}
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
					// End If
				}
			}
			// 
			WorkSCT = HOLDSCT;
			WorkCL = HoldCL;
			WorkSC = HoldSC;
			WorkWG = HOLDWG;
			WorkRG = HoldRG;
			// 
			// Commercial Credit
			if (Conversion.Val(rsAM.Get_Fields_Decimal("CommercialCredit")) != 0)
			{
				WorkRG = 25;
				WorkUnits = 0;
				WorkFee = FCConvert.ToDecimal(Conversion.Val(rsAM.Get_Fields_Decimal("CommercialCredit")) * -1);
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "ECO" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "LPS")
				{
					if (WorkFee < 0)
						WorkFee = 0;
				}
				else if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "NRT" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "RRT")
				{
					WorkSCT = 130;
					if (WorkFee < 0)
						WorkFee = 0;
				}
				if (strSummaryDetail == "D")
				{
					if (WorkFee != 0)
					{
						WriteNewRecord(WorkSCT, WorkCL, WorkSC, WorkWG, WorkRG, 5, WorkUnits, WorkFee);
						if (WorkWG > 0)
						{
							WriteNewRecord(WorkSCT, WorkCL, WorkSC, WorkWG, 0, 4, WorkUnits, WorkFee);
						}
						if (WorkCL != WorkSC)
						{
							WriteNewRecord(WorkSCT, WorkCL, WorkSC, 0, 0, 3, WorkUnits, WorkFee);
						}
						WriteNewRecord(WorkSCT, WorkCL, "00", 0, 0, 2, WorkUnits, WorkFee);
						WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
					}
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
			// Title()
			// kk01182018 tromv-1292  Correct the Title entries on Town Detail and Summary to include No Fee CTA
			if (rsAM.Get_Fields_Boolean("TitleDone") == true || rsAM.Get_Fields_Decimal("TitleFee") != 0 && FCConvert.ToString(rsAM.Get_Fields("TransactionType")) != "LPS" || rsAM.Get_Fields_Boolean("NoFeeCTA") && fecherFoundation.Strings.Trim(FCConvert.ToString(rsAM.Get_Fields_String("CTANumber"))) != "")
			{
				WorkSCT = 85;
				WorkRG = 1;
				if (rsAM.Get_Fields_Decimal("TitleFee") == 0)
					WorkRG = 0;
				WorkUnits = 1;
				if (FCConvert.ToString(rsAM.Get_Fields_String("DoubleCTANumber")).Length != 0 || FCUtils.iDiv(rsAM.Get_Fields("TitleFee"), 2) == dblTitleFee)
				{
					WorkUnits = 2;
				}
				WorkFee = rsAM.Get_Fields_Decimal("TitleFee");
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "ECO")
				{
					if (WorkFee < 0)
						WorkFee = 0;
				}
				// 
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, "00", "00", 0, WorkRG, 5, WorkUnits, WorkFee);
					WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
			// InitialPlateFee()
			if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) != "LPS" && FCConvert.ToString(rsAM.Get_Fields("TransactionType")) != "DPR")
			{
				if (rsAM.Get_Fields_Boolean("InitialFee") && ((FCConvert.ToString(rsAM.Get_Fields("TransactionType")) != "ECO" || FCConvert.ToString(rsAM.Get_Fields_String("PlateType")) != "N") || MotorVehicle.GetInitialPlateFees2(rsAM.Get_Fields_String("Class"), rsAM.Get_Fields_String("Plate"), rsAM.Get_Fields_Boolean("TrailerVanity")) != 0))
				{
					WorkSCT = 90;
					WorkUnits = 1;
					WorkFee = rsAM.Get_Fields_Decimal("InitialFee");
					if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "ECO")
					{
						if (WorkFee < 0)
							WorkFee = 0;
					}
					if (strSummaryDetail == "D")
					{
						WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
					}
					else
					{
						curSummaryRecord[WorkSCT, 0] += WorkUnits;
						curSummaryRecord[WorkSCT, 1] += WorkFee;
					}
				}
			}
			// ReservePlateFee()
			if (rsAM.Get_Fields_Boolean("ReservePlateYN") == true || Conversion.Val(rsAM.Get_Fields("outofrotation")) != 0)
			{
				WorkSCT = 95;
				WorkUnits = 1;
				if (rsAM.Get_Fields_Boolean("TwoYear") == true)
					WorkUnits = 2;
				WorkFee = FCConvert.ToDecimal(Conversion.Val(rsAM.Get_Fields_Decimal("ReservePlate")) + Conversion.Val(rsAM.Get_Fields("outofrotation")));
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "ECO")
				{
					if (WorkFee < 0)
						WorkFee = 0;
				}
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
			// PlateFeeNew()
			if (rsAM.Get_Fields_Decimal("PlateFeeNew") != 0)
			{
				if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "CR" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "CC" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "CL" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "RV" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "CD")
				{
					WorkSCT = 152;
				}
				else if (rsAM.Get_Fields_String("Class") == "UM")
				{
					WorkSCT = 161;
				}
                else if (rsAM.Get_Fields_String("Class") == "BH")
                {
                    WorkSCT = 146;
                }
				else if (rsAM.Get_Fields_String("Class") == "BC")
				{
					WorkSCT = 148;
				}
				else if (rsAM.Get_Fields_String("Class") == "BB")
				{
					WorkSCT = 150;
				}
				else if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "LB" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "LC")
				{
					WorkSCT = 156;
				}
				else if (rsAM.Get_Fields_String("Class") == "TS")
				{
					WorkSCT = 159;
				}
				else if (rsAM.Get_Fields_String("Class") == "SW")
				{
					WorkSCT = 158;
				}
				else if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "AG" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "AC" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "AF")
				{
					WorkSCT = 142;
				}
				else if (rsAM.Get_Fields_String("Class") == "AW")
				{
					WorkSCT = 144;
				}
				else
				{
					WorkSCT = 170;
				}
				WorkUnits = 1;
				WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("PlateFeeNew") - rsAM.Get_Fields_Decimal("GiftCertificateAmount"));
				if (FCConvert.ToString(rsAM.Get_Fields_String("TransactionType")) == "ECO")
				{
					if (WorkFee < 0)
						WorkFee = 0;
				}
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
			// PlateFeeReReg()
			if (rsAM.Get_Fields_Decimal("PlateFeeReReg") > 0)
			{
				// rsTDS.AddNew
				if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "CR" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "CC" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "CL" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "RV" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "CD")
				{
					WorkSCT = 155;
				}
				else if (rsAM.Get_Fields_String("Class") == "UM")
				{
					WorkSCT = 165;
				}
                else if (rsAM.Get_Fields_String("Class") == "BH")
                {
                    WorkSCT = 147;
                }
				else if (rsAM.Get_Fields_String("Class") == "BC")
				{
					WorkSCT = 149;
				}
				else if (rsAM.Get_Fields_String("Class") == "BB")
				{
					WorkSCT = 151;
				}
				else if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "LB" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "LC")
				{
					WorkSCT = 157;
				}
				else if (rsAM.Get_Fields_String("Class") == "TS")
				{
					WorkSCT = 160;
				}
				else if (rsAM.Get_Fields_String("Class") == "SW")
				{
					WorkSCT = 158;
				}
				else if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "AG" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "AC" || FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "AF")
				{
					WorkSCT = 143;
				}
				else if (rsAM.Get_Fields_String("Class") == "AW")
				{
					WorkSCT = 145;
				}
				else
				{
					WorkSCT = 170;
				}
				WorkUnits = 1;
				if (rsAM.Get_Fields_Boolean("TwoYear") == true && rsAM.Get_Fields_Decimal("PlateFeeReReg") == 0)
					WorkUnits = 2;
				WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("PlateFeeReReg") - rsAM.Get_Fields_Decimal("GiftCertificateAmount"));
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "ECO")
				{
					if (WorkFee < 0)
						WorkFee = 0;
				}
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
			// ReplacementFee()
			if (rsAM.Get_Fields_Decimal("ReplacementFee") != 0)
			{
				WorkSCT = 110;
				WorkUnits = FCUtils.iDiv(rsAM.Get_Fields_Double("ReplacementFee"), 5);
				WorkFee = rsAM.Get_Fields_Decimal("ReplacementFee");
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "ECO")
				{
					if (WorkFee < 0)
						WorkFee = 0;
				}
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
			// StickerFee()
			DupSticker:
			;
			if (rsAM.Get_Fields_Decimal("StickerFee") != 0 || ((Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "DPS" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "LPS") && rsAM.Get_Fields_Int16("MonthStickerNoCharge") + rsAM.Get_Fields_Int16("MonthStickerCharge") + rsAM.Get_Fields_Int16("YearStickerNoCharge") + rsAM.Get_Fields_Int16("YearStickerCharge") > 0))
			{
				WorkSCT = 115;
				WorkCL = "00";
				if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "TC")
					WorkCL = "TC";
				if (Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) != "DPS" && Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) != "LPS")
				{
					if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "IU")
					{
						WorkUnits = 2;
					}
					else
					{
						WorkUnits = FCConvert.ToInt16(rsAM.Get_Fields_Int16("MonthStickerCharge") + rsAM.Get_Fields_Int16("YearStickerCharge"));
					}
				}
				else
				{
					WorkUnits = FCConvert.ToInt16(rsAM.Get_Fields_Int16("MonthStickerCharge") + rsAM.Get_Fields_Int16("YearStickerCharge") + rsAM.Get_Fields_Int16("MonthStickerNoCharge") + rsAM.Get_Fields_Int16("YearStickerNoCharge"));
				}
				WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("StickerFee"));
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "ECO")
				{
					if (WorkFee < 0)
						WorkFee = 0;
				}
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, WorkCL, "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
			// TransferFee()
			if (Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "NRT" || Strings.Mid(FCConvert.ToString(rsAM.Get_Fields("TransactionType")), 1, 3) == "RRT")
			{
				WorkSCT = 141;
				WorkCL = "00";
				WorkSC = "00";
				if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "MP")
				{
					WorkCL = "MP";
				}
				else if (rsAM.Get_Fields_String("Class") == "TL")
				{
					WorkCL = "TL";
					if (FCConvert.ToString(rsAM.Get_Fields_String("Subclass")) == "L1" || FCConvert.ToString(rsAM.Get_Fields_String("Subclass")) == "L2" || FCConvert.ToString(rsAM.Get_Fields_String("Subclass")) == "L6")
					{
						if (rsAM.Get_Fields_Double("TransferCreditFull") < 16.0)
						{
							WorkSC = "T1";
						}
						else
						{
							WorkSC = "T4";
						}
					}
					else if (FCConvert.ToString(rsAM.Get_Fields_String("Subclass")) == "L3" || FCConvert.ToString(rsAM.Get_Fields_String("Subclass")) == "L4")
					{
						if (rsAM.Get_Fields_Double("TransferCreditFull") < 16.0)
						{
							WorkSC = "T2";
						}
						else
						{
							WorkSC = "T3";
						}
					}
				}
				WorkRG = 99;
				if (rsAM.Get_Fields_Decimal("TransferFee") == 0)
					WorkRG = 0;
				WorkUnits = 1;
				WorkFee = rsAM.Get_Fields_Decimal("TransferFee");
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, WorkCL, WorkSC, 0, WorkRG, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
			// SalesTax()
			// kk02242017 tromv-1242  Added check for SalesTaxExempt to pick up when Use Tax form wasn't done and exclude Dealer sales tax
			if ((rsAM.Get_Fields_Boolean("UseTaxDone") == true || rsAM.Get_Fields_Decimal("SalesTax") != 0 || rsAM.Get_Fields_Boolean("SalesTaxExempt") == true) && !rsAM.Get_Fields_Boolean("DealerSalesTax") && FCConvert.ToString(rsAM.Get_Fields("TransactionType")) != "LPS" && FCConvert.ToString(rsAM.Get_Fields("TransactionType")) != "DPS")
			{
				WorkSCT = 175;
				if (rsAM.Get_Fields_Decimal("SalesTax") > 0)
					WorkSCT = 180;
				WorkUnits = 1;
				WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields("SalesTax"));
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "ECO")
				{
					if (WorkFee < 0)
						WorkFee = 0;
				}
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
		}

		private void LostPlateStickerRecordFound()
		{
			// Replacement Fee
			if (rsAM.Get_Fields_Decimal("ReplacementFee") != 0)
			{
				WorkSCT = 110;
				WorkUnits = (FCUtils.iDiv(Conversion.Val(rsAM.Get_Fields_Decimal("ReplacementFee")), 5.0));
				WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("ReplacementFee"));
				if (WorkFee < 0)
					WorkFee = 0;
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
			// Sticker Fee
			if (rsAM.Get_Fields_Decimal("StickerFee") != 0)
			{
				WorkSCT = 115;
				WorkCL = "00";
				if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "TC")
					WorkCL = "TC";
				WorkUnits = FCConvert.ToInt16(rsAM.Get_Fields_Int16("MonthStickerCharge") + rsAM.Get_Fields_Int16("YearStickerCharge"));
				WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("StickerFee"));
				if (WorkFee < 0)
					WorkFee = 0;
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, WorkCL, "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
		}

		private void DuplicateSticker()
		{
			WorkSCT = 115;
			WorkCL = "00";
			if (FCConvert.ToString(rsAM.Get_Fields_String("Class")) == "TC")
				WorkCL = "TC";
			WorkUnits = FCConvert.ToInt16(rsAM.Get_Fields_Int16("MonthStickerNoCharge") + rsAM.Get_Fields_Int16("YearStickerNoCharge"));
			if (WorkUnits > 0)
			{
				WorkFee = 0;
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, WorkCL, "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
			WorkUnits = FCConvert.ToInt16(rsAM.Get_Fields_Int16("MonthStickerCharge") + rsAM.Get_Fields_Int16("YearStickerCharge"));
			if (WorkUnits > 0)
			{
				WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("StickerFee"));
				if (WorkFee < 0)
					WorkFee = 0;
				if (strSummaryDetail == "D")
				{
					WriteNewRecord(WorkSCT, WorkCL, "00", 0, 0, 1, WorkUnits, WorkFee);
				}
				else
				{
					curSummaryRecord[WorkSCT, 0] += WorkUnits;
					curSummaryRecord[WorkSCT, 1] += WorkFee;
				}
			}
		}

		private void DuplicateRegistration()
		{
			WorkSCT = 120;
			WorkUnits = 1;
			WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields_Decimal("DuplicateRegistrationFee"));
			if (WorkFee < 0)
				WorkFee = 0;
			if (strSummaryDetail == "D")
			{
				WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
			}
			else
			{
				curSummaryRecord[WorkSCT, 0] += WorkUnits;
				curSummaryRecord[WorkSCT, 1] += WorkFee;
			}
		}

		private void IncreaseGVW()
		{
			WorkSCT = 130;
			WorkUnits = 1;
			// WorkFee = rsAM.Fields("RegRateCharge") - rsAM.Fields("TransferCreditUsed")
			WorkFee = FCConvert.ToDecimal(rsAM.Get_Fields("StatePaid"));
			if (WorkFee < 0)
				WorkFee = 0;
			// Output data to GVW.CHK
			FCFileSystem.Print(98, "" + FCConvert.ToString(rsAM.Get_Fields_String("plate")));
			FCFileSystem.Print(98, "" + FCConvert.ToString(rsAM.Get_Fields_Int32("MVR3")));
			FCFileSystem.PrintLine(98, "" + FCConvert.ToString(WorkFee));
			if (strSummaryDetail == "D")
			{
				WriteNewRecord(WorkSCT, "00", "00", 0, 0, 1, WorkUnits, WorkFee);
			}
			else
			{
				curSummaryRecord[WorkSCT, 0] += WorkUnits;
				curSummaryRecord[WorkSCT, 1] += WorkFee;
			}
		}

		private void FillTownDetailSummaryTable()
		{
			int intxx;
			clsDRWrapper tmpTF = new clsDRWrapper();
			clsDRWrapper tmpSR = new clsDRWrapper();
			int lngPK1;
			int lngPK2;
			double dblSpecialFee;
			double dblTransitFee;
			int overShortCount = 0;
			try
			{
				fecherFoundation.Information.Err().Clear();
				tmpTF.OpenRecordset("SELECT * FROM DefaultInfo");
				tmpTF.MoveLast();
				tmpTF.MoveFirst();
				lngPK1 = 0;
				lngPK2 = 0;
				dblTitleFee = tmpTF.Get_Fields_Double("TitleFee");
				dblSpecialFee = tmpTF.Get_Fields_Double("SpecialRegistrationFee");
				dblTransitFee = tmpTF.Get_Fields_Double("TransitPlateFee");
				tmpTF.Reset();
				rsTDS.OpenRecordset("SELECT * FROM TownDetailSummary");
				rsClass.OpenRecordset("SELECT * FROM Class");
				rsGVW.OpenRecordset("SELECT * FROM GrossVehicleWeight");
				if (cmbInterim.Text == "Interim Reports")
				{
					rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 < 1 ORDER BY MVR3");
				}
				else
				{
					strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
					rs.OpenRecordset(strSql);
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.MoveLast();
						lngPK1 = FCConvert.ToInt16(rs.Get_Fields_Int32("ID"));
						rs.MoveFirst();
						lngPK2 = FCConvert.ToInt16(rs.Get_Fields_Int32("ID"));
					}
					rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND (OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + ") ORDER BY MVR3");
				}
                FCUtils.EraseSafe(curSummaryRecord);

				if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
				{
                    rsAM.MoveLast();
                    rsAM.MoveFirst();
                    FCFileSystem.FileClose(97);
                    FCFileSystem.FileClose(98);
                    FCFileSystem.FileOpen(97,Path.Combine(reportFolder, "PASSFULL.TXT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
                    FCFileSystem.FileOpen(98,Path.Combine(reportFolder, "GVW.TXT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
                    curSubTotal = 0;
                    curGrand = 0;
                    TotalUnits = 0;
                    while (!rsAM.EndOfFile())
					{
                        if (FCConvert.ToString(rsAM.Get_Fields_String("Status")) != "V")
							MasterRecordFound();
						rsAM.MoveNext();
					}
                    FCFileSystem.FileClose(97);
					FCFileSystem.FileClose(98);
				}
				else
				{
					curGrand = 0;
					curSubTotal = 0;
					TotalUnits = 0;
				}
                Exception_Report_Subtraction();
                for (intxx = 1; intxx <= 300; intxx++)
				{
					if (curSummaryRecord[intxx, 0] != 0 || curSummaryRecord[intxx, 1] != 0)
					{
						if (intxx == 195 && strSummaryDetail == "D")
						{
							// Exception Item do not write to file
						}
						else
						{
							rsTDS.AddNew();
							rsTDS.Set_Fields("SummaryCategory", intxx);
							rsTDS.Set_Fields("RateGroup", 99999);
							rsTDS.Set_Fields("Units", curSummaryRecord[intxx, 0]);
							rsTDS.Set_Fields("Fee", curSummaryRecord[intxx, 1]);
							if ((intxx != 185 || MotorVehicle.Statics.gboolAllowLongTermTrailers) && intxx != 200)
							{
								if (intxx != 195)
								{
									curSubTotal += curSummaryRecord[intxx, 1];
								}
								else
								{
									overShortCount++;
								}
								curGrand += curSummaryRecord[intxx, 1];
								if (intxx != 90)
								{
									TotalUnits += curSummaryRecord[intxx, 0];
								}
							}
							rsTDS.Update();
						}
					}
				}
				// MsgBox "9"
				// Special Registrations
				if (cmbInterim.Text == "Interim Reports")
				{
					tmpSR.OpenRecordset("SELECT * FROM SpecialRegistration WHERE isnull(Voided, 0) = 0 AND PeriodCloseoutID < 1");
				}
				else
				{
					tmpSR.OpenRecordset("SELECT * FROM SpecialRegistration WHERE isnull(Voided, 0) = 0 AND PeriodCloseoutID >" + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <=" + FCConvert.ToString(lngPK2));
				}
				if (tmpSR.EndOfFile() != true && tmpSR.BeginningOfFile() != true)
				{
					tmpSR.MoveLast();
					tmpSR.MoveFirst();
					rsTDS.AddNew();
					rsTDS.Set_Fields("SummaryCategory", 125);
					rsTDS.Set_Fields("RateGroup", 99999);
					rsTDS.Set_Fields("GroupLevel", 1);
					rsTDS.Set_Fields("Units", tmpSR.RecordCount());
					rsTDS.Set_Fields("Fee", 0);
					while (tmpSR.EndOfFile() != true)
					{
						rsTDS.Set_Fields("Fee", rsTDS.Get_Fields_Decimal("Fee") + tmpSR.Get_Fields_Decimal("StatePaid"));
						tmpSR.MoveNext();
					}
					curSubTotal += rsTDS.Get_Fields_Decimal("Fee");
					curGrand += rsTDS.Get_Fields_Decimal("Fee");
					TotalUnits += tmpSR.RecordCount();
					rsTDS.Update();
				}
				// MsgBox "10"
				// Transit Plates
				if (cmbInterim.Text == "Interim Reports")
				{
					tmpSR.OpenRecordset("SELECT * FROM TransitPlates WHERE isnull(Voided, 0) = 0 AND PeriodCloseoutID < 1");
				}
				else
				{
					tmpSR.OpenRecordset("SELECT * FROM TransitPlates WHERE isnull(Voided, 0) = 0 AND PeriodCloseoutID >" + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <=" + FCConvert.ToString(lngPK2));
				}
				if (tmpSR.EndOfFile() != true && tmpSR.BeginningOfFile() != true)
				{
					FCFileSystem.FileClose(99);
					FCFileSystem.FileOpen(99,Path.Combine(reportFolder, "TRANSIT.TXT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					if (strSummaryDetail == "D")
					{
						do
						{
							rsTDS.AddNew();
							rsTDS.Set_Fields("SummaryCategory", 135);
							if (tmpSR.Get_Fields_Boolean("RoundTrip") == true)
							{
								rsTDS.Set_Fields("Class", "X2");
								rsTDS.Set_Fields("Subclass", "X2");
							}
							else
							{
								rsTDS.Set_Fields("Class", "X1");
								rsTDS.Set_Fields("Subclass", "X1");
							}
							rsTDS.Set_Fields("RateGroup", 1);
							rsTDS.Set_Fields("GroupLevel", 5);
							rsTDS.Set_Fields("Units", 1);
							rsTDS.Set_Fields("Fee", tmpSR.Get_Fields("StatePaid"));
							rsTDS.Update();
							rsTDS.AddNew();
							rsTDS.Set_Fields("SummaryCategory", 135);
							if (tmpSR.Get_Fields_Boolean("RoundTrip") == true)
							{
								rsTDS.Set_Fields("Class", "X2");
							}
							else
							{
								rsTDS.Set_Fields("Class", "X1");
							}
							rsTDS.Set_Fields("Subclass", "00");
							rsTDS.Set_Fields("RateGroup", 0);
							rsTDS.Set_Fields("GroupLevel", 2);
							rsTDS.Set_Fields("Units", 1);
							rsTDS.Set_Fields("Fee", tmpSR.Get_Fields("StatePaid"));
							rsTDS.Update();
							rsTDS.AddNew();
							rsTDS.Set_Fields("SummaryCategory", 135);
							rsTDS.Set_Fields("Class", "00");
							rsTDS.Set_Fields("Subclass", "00");
							rsTDS.Set_Fields("RateGroup", 0);
							rsTDS.Set_Fields("GroupLevel", 1);
							rsTDS.Set_Fields("Units", 1);
							rsTDS.Set_Fields("Fee", tmpSR.Get_Fields("StatePaid"));
							rsTDS.Update();
							curSubTotal += tmpSR.Get_Fields_Decimal("StatePaid");
							curGrand += tmpSR.Get_Fields_Decimal("StatePaid");
							TotalUnits += 1;
							// Output data to TRANSIT.CHK
							FCFileSystem.Print(99, "" + FCConvert.ToString(tmpSR.Get_Fields_String("plate")));
							FCFileSystem.PrintLine(99, "" + FCConvert.ToString(dblTransitFee));
							tmpSR.MoveNext();
						}
						while (tmpSR.EndOfFile() != true);
					}
					else
					{
						rsTDS.AddNew();
						do
						{
							rsTDS.Set_Fields("SummaryCategory", 135);
							rsTDS.Set_Fields("RateGroup", 99999);
							rsTDS.Set_Fields("Units", Conversion.Val(rsTDS.Get_Fields("Units")) + 1);
							rsTDS.Set_Fields("Fee", Conversion.Val(rsTDS.Get_Fields("Fee")) + tmpSR.Get_Fields_Double("StatePaid"));
							curSubTotal += tmpSR.Get_Fields_Decimal("StatePaid");
							curGrand += tmpSR.Get_Fields_Decimal("StatePaid");
							TotalUnits += 1;
							// Output data to TRANSIT.CHK
							FCFileSystem.Print(99, "" + FCConvert.ToString(tmpSR.Get_Fields_String("plate")));
							FCFileSystem.PrintLine(99, "" + FCConvert.ToString(dblTransitFee));
							tmpSR.MoveNext();
						}
						while (tmpSR.EndOfFile() != true);
						rsTDS.Update();
					}
					FCFileSystem.FileClose(99);
				}

				if (cmbInterim.Text == "Interim Reports")
				{
					tmpSR.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1 AND (CategoryAdjusted = '130' or PermitType = 'BOOSTER') ORDER BY Type, ExceptionReportDate");
				}
				else
				{
					tmpSR.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " AND (CategoryAdjusted = '130' or PermitType = 'BOOSTER') ORDER BY Type, ExceptionReportDate");
				}
				double BoosterTotal = 0;
				if (tmpSR.EndOfFile() != true && tmpSR.BeginningOfFile() != true)
				{
					BoosterTotal = 0;
					tmpSR.MoveLast();
					tmpSR.MoveFirst();
					while (tmpSR.EndOfFile() != true)
					{
						BoosterTotal += tmpSR.Get_Fields_Double("Fee");
						tmpSR.MoveNext();
					}

					rsTDS.OpenRecordset("SELECT * FROM TownDetailSummary WHERE SummaryCategory = 130");
					if (!rsTDS.EndOfFile())
					{
						rsTDS.Set_Fields("Units", rsTDS.Get_Fields_Int32("Units") + tmpSR.RecordCount());
						rsTDS.Set_Fields("Fee", rsTDS.Get_Fields_Decimal("Fee") + Convert.ToDecimal(BoosterTotal));
					}
					else
					{
						rsTDS.AddNew();
						rsTDS.Set_Fields("SummaryCategory", 130);
						rsTDS.Set_Fields("RateGroup", 99999);
						rsTDS.Set_Fields("GroupLevel", 1);
						rsTDS.Set_Fields("Units", tmpSR.RecordCount());
						rsTDS.Set_Fields("Fee", BoosterTotal);
					}
					curSubTotal += FCConvert.ToDecimal(BoosterTotal);
					curGrand += FCConvert.ToDecimal(BoosterTotal);
					TotalUnits += tmpSR.RecordCount();
					rsTDS.Update();
				}
				// End If
				// MsgBox "12"
				// SubTotal
				rsTDS.AddNew();
				rsTDS.Set_Fields("SummaryCategory", 190);
				rsTDS.Set_Fields("RateGroup", 99999);
				rsTDS.Set_Fields("GroupLevel", 9);
				rsTDS.Set_Fields("Units", TotalUnits - overShortCount);
				rsTDS.Set_Fields("Fee", curSubTotal);
				rsTDS.Update();
				// Grand Total
				rsTDS.AddNew();
				rsTDS.Set_Fields("SummaryCategory", 200);
				rsTDS.Set_Fields("RateGroup", 99999);
				rsTDS.Set_Fields("GroupLevel", 9);
				rsTDS.Set_Fields("Units", TotalUnits);
				rsTDS.Set_Fields("Fee", curGrand);
				rsTDS.Update();
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description);
			}
		}

		private void TownSummary(List<string> batchReports = null)
		{
			clsDRWrapper rsTDH = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			string str1 = "";
            strHeading = "";

			rsTDH.OpenRecordset("SELECT * FROM TownSummaryHeadings ORDER BY CategoryCode");
			LineCount = 0;
			PageCount = 0;
			if (rsTDH.EndOfFile() != true && rsTDH.BeginningOfFile() != true)
			{
				rsTDH.MoveLast();
				rsTDH.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** TOWN SUMMARY ****";
				PrintHeading(PageCount);
				strLine = Strings.StrDup(80, " ");
				strLine = (Strings.StrDup(79, "-") + " ");
				rtf1.Text = rtf1.Text + strLine;
				rtf1.Text = rtf1.Text + "\r\n";
				while (!rsTDH.EndOfFile())
				{
					if (FCConvert.ToString(rsTDH.Get_Fields_String("Heading")) == "TRUCK CAMPER")
					{
						rsTDH.MoveNext();
					}
					if (FCConvert.ToString(rsTDH.Get_Fields_String("Heading")) == "SALES TAX - NO FEE")
					{
						rsTDH.MoveNext();
						str1 = "SELECT * FROM TownDetailSummary WHERE RateGroup = 99999 And SummaryCategory = " + rsTDH.Get_Fields_Int32("CategoryCode") + " ORDER BY SummaryCategory";
						rsTemp.OpenRecordset(str1);
						rsTDH.MovePrevious();
						str1 = "SELECT SUM(Units) as TotalUnits, SUM(Fee) as TotalFee FROM TownDetailSummary WHERE RateGroup = 99999 And SummaryCategory = " + rsTDH.Get_Fields_Int32("CategoryCode") + " GROUP BY SummaryCategory ORDER BY SummaryCategory";
						rsTDS.OpenRecordset(str1);
						rsTDH.MoveNext();
						strLine = Strings.StrDup(80, " ");
						if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
						{
							rsTemp.MoveLast();
							rsTemp.MoveFirst();
							Strings.MidSet(ref strLine, 1, 20, rsTDH.Get_Fields_String("Heading") + Strings.StrDup(20 - FCConvert.ToString(rsTDH.Get_Fields_String("Heading")).Length, "."));
							Strings.MidSet(ref strLine, 21, 5, Strings.Format(Strings.Format(rsTemp.Get_Fields("Units"), "#,###"), "@@@@@"));
							Strings.MidSet(ref strLine, 27, 11, Strings.Format(Strings.Format(rsTemp.Get_Fields("Fee"), "#,###.00"), "@@@@@@@@@@@"));
							Strings.MidSet(ref strLine, 39, 1, "|");
							Strings.MidSet(ref strLine, 41, 38, rsTDH.Get_Fields_String("Heading") + Strings.StrDup(20 - FCConvert.ToString(rsTDH.Get_Fields_String("Heading")).Length, "."));
							rtf1.Text = rtf1.Text + strLine;
							rtf1.Text = rtf1.Text + "\r\n";
							strLine = Strings.StrDup(79, "-");
							rtf1.Text = rtf1.Text + strLine;
							rtf1.Text = rtf1.Text + "\r\n";
						}
						else
						{
							if (FCConvert.ToInt32(rsTDH.Get_Fields_Int32("CategoryCode")) != 299)
							{
								Strings.MidSet(ref strLine, 1, 20, rsTDH.Get_Fields_String("Heading") + Strings.StrDup(20 - FCConvert.ToString(rsTDH.Get_Fields_String("Heading")).Length, "."));
								Strings.MidSet(ref strLine, 21, 5, "     ");
								Strings.MidSet(ref strLine, 27, 11, Strings.Format(Strings.Format(0, "#,###.00"), "@@@@@@@@@@@"));
								Strings.MidSet(ref strLine, 39, 1, "|");
								Strings.MidSet(ref strLine, 41, 38, rsTDH.Get_Fields_String("Heading") + Strings.StrDup(20 - FCConvert.ToString(rsTDH.Get_Fields_String("Heading")).Length, "."));
								rtf1.Text = rtf1.Text + strLine;
								rtf1.Text = rtf1.Text + "\r\n";
								strLine = Strings.StrDup(79, "-");
								rtf1.Text = rtf1.Text + strLine;
								rtf1.Text = rtf1.Text + "\r\n";
							}
						}
						strLine = Strings.StrDup(80, " ");
						rsTDH.MovePrevious();
					}
					else
					{
						str1 = "SELECT SUM(Units) as TotalUnits, SUM(Fee) as TotalFee FROM TownDetailSummary WHERE RateGroup = 99999 And SummaryCategory = " + rsTDH.Get_Fields_Int32("CategoryCode") + " GROUP BY SummaryCategory ORDER BY SummaryCategory";
						rsTDS.OpenRecordset(str1,"MotorVehicle");
						strLine = Strings.StrDup(80, " ");
					}

                    if (rsTDH.Get_Fields_String("Heading").Length < 21)
                    {
                        strHeading = rsTDH.Get_Fields_String("Heading");
                    }
                    else
                    {
                        strHeading = rsTDH.Get_Fields_String("Heading").Replace(" - ", "");
                    }
					if (rsTDS.EndOfFile() != true && rsTDS.BeginningOfFile() != true)
					{
						rsTDS.MoveLast();
						rsTDS.MoveFirst();
						Strings.MidSet(ref strLine, 1, 20,strHeading + Strings.StrDup(20 - strHeading.Length, "."));
						Strings.MidSet(ref strLine, 21, 5, Strings.Format(Strings.Format(rsTDS.Get_Fields("TotalUnits"), "#,###"), "@@@@@"));
						Strings.MidSet(ref strLine, 27, 11, Strings.Format(Strings.Format(rsTDS.Get_Fields("TotalFee"), "#,###.00"), "@@@@@@@@@@@"));
						Strings.MidSet(ref strLine, 39, 1, "|");
						Strings.MidSet(ref strLine, 41, 38, strHeading + Strings.StrDup(20 - strHeading.Length, "."));
						rtf1.Text = rtf1.Text + strLine;
						rtf1.Text = rtf1.Text + "\r\n";
						strLine = Strings.StrDup(79, "-");
						rtf1.Text = rtf1.Text + strLine;
						rtf1.Text = rtf1.Text + "\r\n";
					}
					else
					{
						if (FCConvert.ToInt32(rsTDH.Get_Fields_Int32("CategoryCode")) != 299)
						{
							Strings.MidSet(ref strLine, 1, 20, strHeading + Strings.StrDup(20 - strHeading.Length, "."));
							Strings.MidSet(ref strLine, 21, 5, "    ");
							Strings.MidSet(ref strLine, 27, 11, Strings.Format(Strings.Format(0, "#,###.00"), "@@@@@@@@@@@"));
							Strings.MidSet(ref strLine, 39, 1, "|");
							Strings.MidSet(ref strLine, 41, 38, strHeading + Strings.StrDup(20 - strHeading.Length, "."));
							rtf1.Text = rtf1.Text + strLine;
							rtf1.Text = rtf1.Text + "\r\n";
							strLine = Strings.StrDup(79, "-");
							rtf1.Text = rtf1.Text + strLine;
							rtf1.Text = rtf1.Text + "\r\n";
						}
					}
					if (FCConvert.ToString(rsTDH.Get_Fields_String("Heading")) == "SALES TAX - NO FEE")
					{
						rsTDH.MoveNext();
					}
					rsTDH.MoveNext();
				}
				rtf1.Text = rtf1.Text + "\r\n";
				strLine = ("REPORT COMPLETED BY: " + Strings.StrDup(55, "_"));
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				strLine = "Mail one copy of all reports,";
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				strLine = "data upload verification, and";
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				strLine = ("remittance to:          " + Strings.StrDup(11, " ") + "TOTAL AMOUNT REMITTED TO BMV ____________");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n";
				strLine = ("ATTN: ACCOUNTING UNIT   " + Strings.StrDup(11, " ") + "(  )Bank Deposit    " + Strings.StrDup(4, " ") + "PROCESSED BY BMV");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				strLine = ("BUREAU OF MOTOR VEHICLES" + Strings.StrDup(11, " ") + "(  )Cash            " + Strings.StrDup(4, " ") + "INITIALS:________");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				strLine = ("101 HOSPITAL STREET     " + Strings.StrDup(11, " ") + "(  )Checks/Money Ord" + Strings.StrDup(4, " ") + "DATE:___/___/___");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				strLine = "29 STATE HOUSE STATION";
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				strLine = "AUGUSTA, ME 04333-0029";
				rtf1.Text = rtf1.Text + strLine;
			}
			if (rtf1.Text != "")
			{
				FCFileSystem.FileOpen(33, Path.Combine("BMVReports",strResCode + "TS.RPT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.PrintLine(33, rtf1.Text);
				FCFileSystem.FileClose(33);
			}
			if (AllFlag)
			{
				rptTownSummary.InstancePtr.PrintReport(false, batchReports: batchReports);
				rptTownSummary.InstancePtr.Unload();
				CheckIt:
				;
				if (MotorVehicle.JobComplete("rptTownSummary"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckIt;
				}
			}
			else
			{
				rptTownSummary.InstancePtr.Unload();
				frmReportViewer.InstancePtr.Init(rptTownSummary.InstancePtr);
			}
		}

		private void TownDetail(List<string> batchReports = null)
		{
			// VB6 Bad Scope Dim:
			string strWeight = "";
			Decimal curException = 0;
			Decimal curSubTotal = 0;
			Decimal curGrand = 0;
			int TotalUnits = 0;
			// vbPorter upgrade warning: TotalAmt As Decimal	OnWrite(int, Decimal)
			Decimal TotalAmt = 0;
			string strTitle = "";
			// 
			HOLDSCT = 0;
			HoldCL = "";
			HoldSC = "";
			HOLDWG = 0;
			HoldRG = 0;
			HoldLV = 0;
			// 
			rsInventoryMaster.OpenRecordset("SELECT * FROM InventoryMaster");
			rsTDS.OpenRecordset("SELECT * FROM TownDetailSummary ORDER BY SummaryCategory, Class, SubClass, WeightGroup, RateGroup, GroupLevel");
			rsCategory.OpenRecordset("SELECT * FROM TownSummaryHeadings");
			//rtf1.RightMargin = 5;
			if (rsTDS.EndOfFile() != true && rsTDS.BeginningOfFile() != true)
			{
				rsTDS.MoveLast();
				rsTDS.MoveFirst();
				PageCount = 1;
				LineCount = 0;
				strLine = Strings.StrDup(80, " ");
				strHeading = "**** TOWN DETAIL REPORT ****";
				PrintHeading(PageCount);
				while (!rsTDS.EndOfFile())
				{
					strLine = Strings.StrDup(80, " ");
					if (HOLDSCT != 0)
					{
						if (FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) != HOLDSCT || FCConvert.ToString(rsTDS.Get_Fields_String("Class")) != HoldCL || FCConvert.ToString(rsTDS.Get_Fields_String("Subclass")) != HoldSC || Conversion.Val(rsTDS.Get_Fields_Int32("WeightGroup")) != HOLDWG || FCConvert.ToInt32(rsTDS.Get_Fields_Int32("RateGroup")) != HoldRG || FCConvert.ToInt32(rsTDS.Get_Fields_Int32("GroupLevel")) != HoldLV)
						{
							if (HoldLV == 1)
							{
								rtf1.Text = rtf1.Text + "\r\n";
								rsCategory.FindFirstRecord("CategoryCode", HOLDSCT);
								if (FCConvert.ToString(rsCategory.Get_Fields_String("Heading")) == "PASSENGER FULL")
								{
									Strings.MidSet(ref strLine, 1, Strings.Len("PASSENGER CARS"), "PASSENGER CARS");
								}
								else
								{
									Strings.MidSet(ref strLine, 1, Strings.Len(rsCategory.Get_Fields_String("Heading")), FCConvert.ToString(rsCategory.Get_Fields_String("Heading")));
								}
							}
							else if (HoldLV == 2)
							{
								// kk02212017 tromvs-70  Correct breakdown for Tractor/SME								
                                if (HoldCL != "X1" && HoldCL != "X2" && HoldCL != "TC" && HoldCL != "RT" && HoldCL != "LS" && HoldCL != "MQ" 
                                    && HoldCL != "DY" && HoldCL != "NG" && HoldCL != "SC" && HoldCL != "TZ" && HoldCL != "BY" && HoldCL != "BZ") 
                                {
                                    rsInventoryMaster.FindFirstRecord("MonthYearClass", HoldCL);
									if (rsInventoryMaster.NoMatch == true)
									{
										Strings.MidSet(ref strLine, 5, 10, "Uncoded" + Strings.Format(HoldCL, "@@@"));
									}
									else
									{
										Strings.MidSet(ref strLine, 5, Strings.Len(rsInventoryMaster.Get_Fields_String("Description")), FCConvert.ToString(rsInventoryMaster.Get_Fields_String("Description")));
									}
									// ElseIf HoldCL = "TC" Then              'kk01102018 tromvs-96  Removed Truck Camper Category - no longer processing
									// Mid$(strLine, 5, 12) = "TRUCK CAMPER"
								}
								else if (HoldCL == "LS")
								{
									Strings.MidSet(ref strLine, 5, 17, "LOW SPEED VEHICLE");
								}
								else if (HoldCL == "MQ")
								{
									Strings.MidSet(ref strLine, 5, 18, "ANTIQUE MOTORCYCLE");
								}
								else if (HoldCL == "X1")
								{
									Strings.MidSet(ref strLine, 5, 17, "TRANSIT (ONE WAY)");
								}
								else if (HoldCL == "X2")
								{
									Strings.MidSet(ref strLine, 5, 20, "TRANSIT (ROUND TRIP)");
								}
								else if (HoldCL == "DY")
								{
									Strings.MidSet(ref strLine, 5, 10, "DUNE BUGGY");
								}
								else if (HoldCL == "NG")
								{
									Strings.MidSet(ref strLine, 5, 14, "NATIONAL GUARD");
								}
								else if (HoldCL == "SC")
								{
									Strings.MidSet(ref strLine, 5, 9, "STOCK CAR");
								}
								else if (HoldCL == "TZ")
								{
									Strings.MidSet(ref strLine, 5, 17, "SPEC MOBILE EQUIP");
								}
                                else if (HoldCL == "BY")
                                {
                                    Strings.MidSet(ref strLine,5,13, "BREAST CANCER");
                                }
                                else if (HoldCL == "BZ")
                                {
                                    Strings.MidSet(ref strLine, 5,10,"BLACK BEAR");
                                }
								else
								{
									Strings.MidSet(ref strLine, 5, 6, "RENTAL");
								}
							}
							else if (HoldLV == 3)
							{
								// kk02212017 tromvs-70
								if (HoldCL == "TR")
								{
									if (HoldSC == "RT")
									{
										rsClass.FindFirstRecord2("SystemCode, BMVCode", "T4,TR", ",");
									}
									else if (HoldSC == "FT")
									{
										rsClass.FindFirstRecord2("SystemCode, BMVCode", "T5,TR", ",");
									}
								}
								else if (HoldCL == "TZ")
								{
									rsClass.FindFirstRecord2("SystemCode, BMVCode", HoldSC + ",TR", ",");
								}
                                else if (HoldCL	== "BZ")
                                {
                                    rsClass.FindFirst("SystemCode = '" + HoldSC + "' and BMVCode = 'BB'");
                                }
                                else if (HoldCL == "BY")
                                {
                                    rsClass.FindFirst("SystemCode = '" + HoldSC + "' and BMVCode = 'BC'");
                                }
								else
								{
									rsClass.FindFirstRecord2("SystemCode, BMVCode", HoldSC + "," + HoldCL, ",");
								}
								Strings.MidSet(ref strLine, 10, Strings.Len(rsClass.Get_Fields_String("Description")), FCConvert.ToString(rsClass.Get_Fields_String("Description")));
							}
							else if (HoldLV == 4)
							{
								if (HoldCL == "SE")
								{
									rsGVW.FindFirstRecord2("Type, High", "SE, " + FCConvert.ToString(HOLDWG), ",");
								}
								else
								{
									rsClass.FindFirstRecord2("SystemCode, BMVCode", HoldSC + "," + HoldCL, ",");
									// kk01102018 tromvs-96  Add BMVCode clause to query
									if (rsClass.Get_Fields_Decimal("RegistrationFee") == 0.01m || HOLDSCT == 40 && (HoldCL == "DV" || HoldCL == "VX"))
									{
										// kk01102018 tromvs-96  Force DV and VX > 6000 lbs to show weight
										rsGVW.FindFirstRecord2("Type, High", "TK, " + FCConvert.ToString(HOLDWG), ",");
									}
									else if (rsClass.Get_Fields_Decimal("RegistrationFee") == 0.02m)
									{
										rsGVW.FindFirstRecord2("Type, High", "FM, " + FCConvert.ToString(HOLDWG), ",");
									}
									else if (rsClass.Get_Fields_Decimal("RegistrationFee") == 0.03m)
									{
										rsGVW.FindFirstRecord2("Type, High", "F2, " + FCConvert.ToString(HOLDWG), ",");
									}
								}
								strWeight = Strings.Format(rsGVW.Get_Fields("Low"), "#,##0") + " - " + Strings.Format(rsGVW.Get_Fields("High"), "#,##0") + "Lbs";
								Strings.MidSet(ref strLine, 15, Strings.Len(strWeight), strWeight);
							}
							else if (HoldLV == 5)
							{
								if (HoldRG == 0)
								{
									strTitle = "No Fee.................";
								}
								else if (HoldRG == 1)
								{
									strTitle = "Regular................";
								}
								else if (HoldRG == 5)
								{
									if (HoldCL == "TL" || HoldCL == "CL" || HoldCL == "SE")
									{
										strTitle = "Half Year..............";
									}
									else
									{
										strTitle = "Half Rate..............";
									}
								}
								else if (HoldRG == 10)
								{
									if (HoldCL == "TL" || HoldCL == "CL" || HoldCL == "SE")
									{
										strTitle = "One Year...............";
									}
									else
									{
										strTitle = "Full Rate..............";
									}
								}
								else if (HoldRG == 15)
								{
									strTitle = "One and One Half Years.";
								}
								else if (HoldRG == 20)
								{
									strTitle = "Two Year...............";
								}
								else if (HoldRG == 25)
								{
									strTitle = "          $40.00 Credit";
								}
								else if (HoldRG == 101)
								{
									strTitle = "PRO-RATE - JANUARY.....";
								}
								else if (HoldRG == 102)
								{
									strTitle = "PRO-RATE - FEBRUARY....";
								}
								else if (HoldRG == 103)
								{
									strTitle = "PRO-RATE - MARCH.......";
								}
								else if (HoldRG == 104)
								{
									strTitle = "PRO-RATE - APRIL.......";
								}
								else if (HoldRG == 105)
								{
									strTitle = "PRO-RATE - MAY.........";
								}
								else if (HoldRG == 106)
								{
									strTitle = "PRO-RATE - JUNE........";
								}
								else if (HoldRG == 107)
								{
									strTitle = "PRO-RATE - JULY........";
								}
								else if (HoldRG == 108)
								{
									strTitle = "PRO-RATE - AUGUST......";
								}
								else if (HoldRG == 109)
								{
									strTitle = "PRO-RATE - SEPTEMBER...";
								}
								else if (HoldRG == 110)
								{
									strTitle = "PRO-RATE - OCTOBER.....";
								}
								else if (HoldRG == 111)
								{
									strTitle = "PRO-RATE - NOVEMBER....";
								}
								else if (HoldRG == 112)
								{
									strTitle = "PRO-RATE - DECEMBER....";
								}
								else
								{
									strTitle = "Uncoded................";
								}
								Strings.MidSet(ref strLine, 25, Strings.Len(strTitle), strTitle);
							}
							else if (HoldLV == 9 && FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) == 195)
							{
								// Exception Report Items HERE
								rtf1.Text = rtf1.Text + "\r\n";
								Strings.MidSet(ref strLine, 1, 34, "EXCEPTION REPORT ADJUSTMENTS TOTAL");
								Strings.MidSet(ref strLine, 56, 6, Strings.Format(Strings.Format(rsTDS.Get_Fields("Units"), "##,###"), "@@@@@@"));
								Strings.MidSet(ref strLine, 62, 14, Strings.Format(Strings.FormatCurrency(rsTDS.Get_Fields_Decimal("Fee"), 2, VbTriState.vbFalse, VbTriState.vbTrue), "@@@@@@@@@@@@@@"));
								curException = FCConvert.ToDecimal(rsTDS.Get_Fields("Fee"));
								goto ExceptionTag;
							}
							else if (FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) == 190 || FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) == 200)
							{
								goto Sub_Grand;
							}
							Strings.MidSet(ref strLine, 56, 6, Strings.Format(Strings.Format(TotalUnits, "##,###"), "@@@@@@"));
							Strings.MidSet(ref strLine, 62, 14, Strings.Format(Strings.FormatCurrency(TotalAmt, 2, VbTriState.vbFalse, VbTriState.vbTrue), "@@@@@@@@@@@@@@"));
							if (HoldLV == 1)
							{
								Strings.MidSet(ref strLine, 76, 4, "****");
							}
							else if (HoldLV == 2)
							{
								Strings.MidSet(ref strLine, 76, 4, "*** ");
							}
							else if (HoldLV == 3)
							{
								Strings.MidSet(ref strLine, 76, 4, "**  ");
							}
							else if (HoldLV == 4)
							{
								Strings.MidSet(ref strLine, 76, 4, "*   ");
							}
							else
							{
								Strings.MidSet(ref strLine, 76, 4, "    ");
							}
							ExceptionTag:
							;
							TotalUnits = 0;
							TotalAmt = 0;
							rtf1.Text = rtf1.Text + strLine;
							rtf1.Text = rtf1.Text + "\r\n";
						}
					}
					// 
					Sub_Grand:
					;
					HOLDSCT = FCConvert.ToInt16(rsTDS.Get_Fields_Int32("SummaryCategory"));
					HoldCL = FCConvert.ToString(rsTDS.Get_Fields_String("Class"));
					HoldSC = FCConvert.ToString(rsTDS.Get_Fields_String("Subclass"));
					HOLDWG = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTDS.Get_Fields_Int32("WeightGroup"))));
					HoldRG = FCConvert.ToInt32(rsTDS.Get_Fields_Int32("RateGroup"));
					HoldLV = FCConvert.ToInt16(rsTDS.Get_Fields_Int32("GroupLevel"));
					if (HoldLV == 1)
					{
						if (FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) != 185 || MotorVehicle.Statics.gboolAllowLongTermTrailers)
						{
							curSubTotal += rsTDS.Get_Fields_Decimal("Fee");
							curGrand += rsTDS.Get_Fields_Decimal("Fee");
						}
						else
						{
							// Do not add Excise Tax
						}
					}
					TotalUnits += rsTDS.Get_Fields_Int32("Units");
					TotalAmt += rsTDS.Get_Fields_Decimal("Fee");
					//FC:FINAL:MSH - i.issue #1835: remove unnecessary code to avoid exceptions
					//Debug.WriteLine(rsTDS.Get_Fields("SummaryCategory") + "\t" + rsTDS.Get_Fields("Class") + "\t" + rsTDS.Get_Fields("Units") + "\t" + rsTDS.Get_Fields("Fee"));
					if (FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) == 190)
					{
						rtf1.Text = rtf1.Text + "\r\n";
						strLine = Strings.StrDup(80, " ");
						Strings.MidSet(ref strLine, 1, 8, "SUBTOTAL");
						Strings.MidSet(ref strLine, 56, 6, Strings.Format(Strings.Format(0, "##,###"), "@@@@@@"));
						Strings.MidSet(ref strLine, 62, 14, Strings.Format(Strings.FormatCurrency(curSubTotal, 2, VbTriState.vbFalse, VbTriState.vbTrue), "@@@@@@@@@@@@@@"));
						rtf1.Text = rtf1.Text + strLine;
						rtf1.Text = rtf1.Text + "\r\n";
					}
					else if (FCConvert.ToInt32(rsTDS.Get_Fields_Int32("SummaryCategory")) == 200)
					{
						rtf1.Text = rtf1.Text + "\r\n";
						strLine = Strings.StrDup(80, " ");
						Strings.MidSet(ref strLine, 1, 34, "ADJUSTED DETAIL REPORT GRAND TOTAL");
						Strings.MidSet(ref strLine, 56, 6, Strings.Format(Strings.Format(0, "##,###"), "@@@@@@"));
						Strings.MidSet(ref strLine, 62, 14, Strings.Format(Strings.FormatCurrency(curGrand + curException, 2, VbTriState.vbFalse, VbTriState.vbTrue), "@@@@@@@@@@@@@@"));
						rtf1.Text = rtf1.Text + strLine;
						rtf1.Text = rtf1.Text + "\r\n";
					}
					// Sub And Grand TOTALS
					rsTDS.MoveNext();
				}
			}
			if (rtf1.Text != "")
			{
				FCFileSystem.FileOpen(33,Path.Combine("BMVReports", strResCode + "TD.RPT"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.PrintLine(33, rtf1.Text);
				FCFileSystem.FileClose(33);
			}


			if (AllFlag)
			{
				rptTownDetail.InstancePtr.PrintReport(false, batchReports: batchReports);
				rptTownDetail.InstancePtr.Unload();

				CheckIt:
				;
				if (MotorVehicle.JobComplete("rptTownDetail"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckIt;
				}
			}
			else
			{
				rptTownDetail.InstancePtr.Unload();
				frmReportViewer.InstancePtr.Init(rptTownDetail.InstancePtr);
			}
		}

		private void WriteNewRecord(int SummaryCategory, string Class, string Subclass, int WeightGroup, int RateGroup, int Level, int Units, Decimal Fee)
		{
			rsTDS.AddNew();
			rsTDS.Set_Fields("SummaryCategory", SummaryCategory);
			rsTDS.Set_Fields("Class", Class);
			rsTDS.Set_Fields("Subclass", Subclass);
			rsTDS.Set_Fields("WeightGroup", WeightGroup);
			rsTDS.Set_Fields("RateGroup", RateGroup);
			rsTDS.Set_Fields("GroupLevel", Level);
			rsTDS.Set_Fields("Units", Units);
			rsTDS.Set_Fields("Fee", Fee);
			rsTDS.Update();
		}

		private void strInventoryReconciliation()
		{
			strLine = "AMOUNT ON HAND    AMOUNT REC'D    VALID ISSUES   AMOUNT ADJ'D   AMOUNT ON HAND  ";
			rtf1.Text = rtf1.Text + strLine + "\r\n";
			strLine = "START OF PERIOD   DURING PERIOD   DURING PERIOD  DURING PERIOD  END OF PERIOD   ";
			rtf1.Text = rtf1.Text + strLine + "\r\n";
		}

		private void strStickers(string strMonth)
		{
			clsDRWrapper rsGG = new clsDRWrapper();
			clsDRWrapper rsHH = new clsDRWrapper();
			clsDRWrapper rsII = new clsDRWrapper();
			clsDRWrapper rsJJ = new clsDRWrapper();
			clsDRWrapper rsKK = new clsDRWrapper();
			int lngGG;
			int lngHH;
			int lngII;
			int lngJJ;
			int lngKK;
			string strMMYY = "";
			lngGG = 0;
			lngHH = 0;
			lngII = 0;
			lngJJ = 0;
			lngKK = 0;
			// 
			rsGG.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE Type = '" + strMonth + "' And PeriodCloseoutID = " + FCConvert.ToString(lngEndPK));
			rsHH.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strMonth + "' And AdjustmentCode = 'R' And PeriodCloseoutID >" + FCConvert.ToString(lngPK) + "AND PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsHH.EndOfFile() != true && rsHH.BeginningOfFile() != true)
			{
				rsHH.MoveLast();
				rsHH.MoveFirst();
				while (!rsHH.EndOfFile())
				{
					lngHH += rsHH.Get_Fields_Int32("QuantityAdjusted");
					rsHH.MoveNext();
				}
			}
			rsII.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strMonth + "' And AdjustmentCode = 'I' And PeriodCloseoutID >" + FCConvert.ToString(lngPK) + "AND PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsII.EndOfFile() != true && rsII.BeginningOfFile() != true)
			{
				rsII.MoveLast();
				rsII.MoveFirst();
				while (!rsII.EndOfFile())
				{
					lngII += rsII.Get_Fields_Int32("QuantityAdjusted");
					rsII.MoveNext();
				}
			}
			rsJJ.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strMonth + "' And AdjustmentCode = 'A' And PeriodCloseoutID >" + FCConvert.ToString(lngPK) + "AND PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
			if (rsJJ.EndOfFile() != true && rsJJ.BeginningOfFile() != true)
			{
				rsJJ.MoveLast();
				rsJJ.MoveFirst();
				while (!rsJJ.EndOfFile())
				{
					lngJJ += rsJJ.Get_Fields_Int32("QuantityAdjusted");
					rsJJ.MoveNext();
				}
			}
			rsKK.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE Type = '" + strMonth + "' And PeriodCloseoutID = " + FCConvert.ToString(lngPK));
			if (rsKK.EndOfFile() != true && rsKK.BeginningOfFile() != true)
			{
				rsKK.MoveLast();
				rsKK.MoveFirst();
				lngKK = 0;
				while (!rsKK.EndOfFile())
				{
					lngKK += rsKK.Get_Fields_Int32("Number");
					rsKK.MoveNext();
				}
			}
			if (rsGG.EndOfFile() != true && rsGG.BeginningOfFile() != true)
			{
				rsGG.MoveLast();
				rsGG.MoveFirst();
				lngGG = 0;
				while (!rsGG.EndOfFile())
				{
					lngGG += rsGG.Get_Fields_Int32("Number");
					rsGG.MoveNext();
				}
			}
			strLine = Strings.StrDup(80, " ");
			string str1 = Strings.Mid(strMonth, 2, 1) + Strings.Mid(strMonth, 4, 2);
			Strings.MidSet(ref strLine, 1, 3, strMMYY1(ref str1));
			Strings.MidSet(ref strLine, 5, 8, Strings.Format(lngKK, "@@@@@@@@"));
			Strings.MidSet(ref strLine, 18, 10, Strings.Format(lngHH, "@@@@@@@@@@"));
			Strings.MidSet(ref strLine, 34, 10, Strings.Format(lngII, "@@@@@@@@@@"));
			Strings.MidSet(ref strLine, 50, 10, Strings.Format(lngJJ, "@@@@@@@@@@"));
			Strings.MidSet(ref strLine, 66, 10, Strings.Format(lngGG, "@@@@@@@@@@"));
			if (lngKK + lngHH - lngII - lngJJ != lngGG)
			{
				Strings.MidSet(ref strLine, 77, 2, "**");
			}
			rtf1.Text = rtf1.Text + strLine;
			rtf1.Text = rtf1.Text + "\r\n";
			lngSOP += lngKK;
			lngAR += lngHH;
			lngVI += lngII;
			lngAA += lngJJ;
			lngEOP += lngGG;
		}

		private string strMMYY1(ref string str1)
		{
			string strMMYY1 = "";
			strMMYY1 = "";
			if (Strings.Mid(str1, 1, 1) == "M")
			{
				str1 = Strings.Mid(str1, 2, 2);
				if (str1 == "01")
					strMMYY1 = "JAN";
				if (str1 == "02")
					strMMYY1 = "FEB";
				if (str1 == "03")
					strMMYY1 = "MAR";
				if (str1 == "04")
					strMMYY1 = "APR";
				if (str1 == "05")
					strMMYY1 = "MAY";
				if (str1 == "06")
					strMMYY1 = "JUN";
				if (str1 == "07")
					strMMYY1 = "JUL";
				if (str1 == "08")
					strMMYY1 = "AUG";
				if (str1 == "09")
					strMMYY1 = "SEP";
				if (str1 == "10")
					strMMYY1 = "OCT";
				if (str1 == "11")
					strMMYY1 = "NOV";
				if (str1 == "12")
					strMMYY1 = "DEC";
			}
			else
			{
				strMMYY1 = Strings.Mid(str1, 2, 2);
			}
			return strMMYY1;
		}

		private void strPlateReconciliation()
		{
			clsDRWrapper rsP = new clsDRWrapper();
			clsDRWrapper rsQ = new clsDRWrapper();
			// Amount On Hand At Start
			clsDRWrapper rsR = new clsDRWrapper();
			// Amount Received
			clsDRWrapper rsS = new clsDRWrapper();
			// Amount Issued
			clsDRWrapper rsT = new clsDRWrapper();
			// Amount Adjusted
			clsDRWrapper rsU = new clsDRWrapper();
			// Amount On Hand At End
			int lngQ = 0;
			int lngR = 0;
			int lngS = 0;
			int lngT = 0;
			int lngU = 0;
			lngSOP = 0;
			lngAR = 0;
			lngVI = 0;
			lngAA = 0;
			lngEOP = 0;
			// 
			string strSSS;
			string strBMVCode = "";
			strSSS = "SELECT DISTINCT BMVCode FROM Class ORDER BY BMVCode";
			rsP.OpenRecordset(strSSS);
			rsP.MoveLast();
			rsP.MoveFirst();
			while (!rsP.EndOfFile())
			{
				strBMVCode = FCConvert.ToString(rsP.Get_Fields_String("BMVCode"));
				TM_XX_Tag:
				;
				lngQ = 0;
				lngR = 0;
				lngS = 0;
				lngT = 0;
				lngU = 0;
				strLine = Strings.StrDup(80, " ");
				rsQ.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE substring(Type,1,1) = 'P' AND substring(Type,4,2) = '" + strBMVCode + "' And PeriodCloseoutID = " + FCConvert.ToString(lngPK));
				if (rsQ.EndOfFile() != true && rsQ.BeginningOfFile() != true)
				{
					rsQ.MoveLast();
					rsQ.MoveFirst();
					while (!rsQ.EndOfFile())
					{
						lngQ += rsQ.Get_Fields_Int32("Number");
						rsQ.MoveNext();
					}
				}
				rsR.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' AND substring(InventoryType,4,2) = '" + strBMVCode + "' And AdjustmentCode = 'R' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
				if (rsR.EndOfFile() != true && rsR.BeginningOfFile() != true)
				{
					rsR.MoveLast();
					rsR.MoveFirst();
					while (!rsR.EndOfFile())
					{
						lngR += rsR.Get_Fields_Int32("QuantityAdjusted");
						rsR.MoveNext();
					}
				}
				rsS.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' AND substring(InventoryType,4,2) = '" + strBMVCode + "' And AdjustmentCode = 'I' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
				if (rsS.EndOfFile() != true && rsS.BeginningOfFile() != true)
				{
					rsS.MoveLast();
					rsS.MoveFirst();
					while (!rsS.EndOfFile())
					{
						lngS += rsS.Get_Fields_Int32("QuantityAdjusted");
						rsS.MoveNext();
					}
				}
				rsT.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE substring(InventoryType,1,1) = 'P' AND substring(InventoryType,4,2) = '" + strBMVCode + "' And AdjustmentCode = 'A' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
				if (rsT.EndOfFile() != true && rsT.BeginningOfFile() != true)
				{
					rsT.MoveLast();
					rsT.MoveFirst();
					while (!rsT.EndOfFile())
					{
						lngT += rsT.Get_Fields_Int32("QuantityAdjusted");
						rsT.MoveNext();
					}
				}
				rsU.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE substring(Type,1,1) = 'P' AND substring(Type,4,2) = '" + strBMVCode + "' And PeriodCloseoutID = " + FCConvert.ToString(lngEndPK));
				if (rsU.EndOfFile() != true && rsU.BeginningOfFile() != true)
				{
					rsU.MoveLast();
					rsU.MoveFirst();
					while (!rsU.EndOfFile())
					{
						lngU += rsU.Get_Fields_Int32("Number");
						rsU.MoveNext();
					}
				}
				// 
				Strings.MidSet(ref strLine, 1, 2, strBMVCode);
				Strings.MidSet(ref strLine, 5, 8, Strings.Format(lngQ, "@@@@@@@@"));
				Strings.MidSet(ref strLine, 18, 10, Strings.Format(lngR, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 34, 10, Strings.Format(lngS, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 50, 10, Strings.Format(lngT, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 66, 10, Strings.Format(lngU, "@@@@@@@@@@"));
				if (lngQ + lngR - lngS - lngT != lngU)
				{
					Strings.MidSet(ref strLine, 77, 2, "**");
				}
				rtf1.Text = rtf1.Text + strLine;
				rtf1.Text = rtf1.Text + "\r\n";
				lngSOP += lngQ;
				lngAR += lngR;
				lngVI += lngS;
				lngAA += lngT;
				lngEOP += lngU;
				if (strBMVCode == "TL")
				{
					// Put the TM plates next cause they arent in the class table
					strBMVCode = "TM";
					goto TM_XX_Tag;
				}
				rsP.MoveNext();
			}
		}

		private void strPermitReconciliation()
		{
			clsDRWrapper rsQ = new clsDRWrapper();
			// Amount On Hand At Start
			clsDRWrapper rsR = new clsDRWrapper();
			// Amount Received
			clsDRWrapper rsS = new clsDRWrapper();
			// Amount Issued
			clsDRWrapper rsT = new clsDRWrapper();
			// Amount Adjusted
			clsDRWrapper rsU = new clsDRWrapper();
			// Amount On Hand At End
			int lngQ = 0;
			int lngR = 0;
			int lngS = 0;
			int lngT = 0;
			int lngU = 0;
			string strType;
			lngSOP = 0;
			lngAR = 0;
			lngVI = 0;
			lngAA = 0;
			lngEOP = 0;
			strType = "B";
			// 
			while (!(strType == "Z"))
			{
				TM_XX_Tag:
				;
				lngQ = 0;
				lngR = 0;
				lngS = 0;
				lngT = 0;
				lngU = 0;
				strLine = Strings.StrDup(80, " ");
				rsQ.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE Type = '" + strType + "XSXX' And PeriodCloseoutID = " + FCConvert.ToString(lngPK));
				if (rsQ.EndOfFile() != true && rsQ.BeginningOfFile() != true)
				{
					rsQ.MoveLast();
					rsQ.MoveFirst();
					while (!rsQ.EndOfFile())
					{
						lngQ += rsQ.Get_Fields_Int32("Number");
						rsQ.MoveNext();
					}
				}
				rsR.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strType + "XSXX' And AdjustmentCode = 'R' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
				if (rsR.EndOfFile() != true && rsR.BeginningOfFile() != true)
				{
					rsR.MoveLast();
					rsR.MoveFirst();
					while (!rsR.EndOfFile())
					{
						lngR += rsR.Get_Fields_Int32("QuantityAdjusted");
						rsR.MoveNext();
					}
				}
				rsS.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strType + "XSXX' And AdjustmentCode = 'I' And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
				if (rsS.EndOfFile() != true && rsS.BeginningOfFile() != true)
				{
					rsS.MoveLast();
					rsS.MoveFirst();
					while (!rsS.EndOfFile())
					{
						lngS += rsS.Get_Fields_Int32("QuantityAdjusted");
						rsS.MoveNext();
					}
				}
				rsT.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strType + "XSXX' And (AdjustmentCode = 'A' OR AdjustmentCode = 'V') And PeriodCloseoutID > " + FCConvert.ToString(lngPK) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngEndPK));
				if (rsT.EndOfFile() != true && rsT.BeginningOfFile() != true)
				{
					rsT.MoveLast();
					rsT.MoveFirst();
					while (!rsT.EndOfFile())
					{
						lngT += rsT.Get_Fields_Int32("QuantityAdjusted");
						rsT.MoveNext();
					}
				}
				rsU.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE Type = '" + strType + "XSXX' And PeriodCloseoutID = " + FCConvert.ToString(lngEndPK));
				if (rsU.EndOfFile() != true && rsU.BeginningOfFile() != true)
				{
					rsU.MoveLast();
					rsU.MoveFirst();
					while (!rsU.EndOfFile())
					{
						lngU += rsU.Get_Fields_Int32("Number");
						rsU.MoveNext();
					}
				}
				// 
				if (strType == "B")
				{
					Strings.MidSet(ref strLine, 1, 7, "BOOSTER");
				}
				else if (strType == "P")
				{
					Strings.MidSet(ref strLine, 1, 7, "TRANSIT");
				}
				else if (strType == "R")
				{
					Strings.MidSet(ref strLine, 1, 5, "MVR10");
				}
				Strings.MidSet(ref strLine, 8, 5, Strings.Format(lngQ, "@@@@@"));
				Strings.MidSet(ref strLine, 18, 10, Strings.Format(lngR, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 34, 10, Strings.Format(lngS, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 50, 10, Strings.Format(lngT, "@@@@@@@@@@"));
				Strings.MidSet(ref strLine, 66, 10, Strings.Format(lngU, "@@@@@@@@@@"));
				if (lngQ + lngR - lngS - lngT != lngU)
				{
					Strings.MidSet(ref strLine, 77, 2, "**");
				}
				rtf1.Text = rtf1.Text + strLine;
				rtf1.Text = rtf1.Text + "\r\n";
				lngSOP += lngQ;
				lngAR += lngR;
				lngVI += lngS;
				lngAA += lngT;
				lngEOP += lngU;
				if (strType == "B")
				{
					strType = "P";
				}
				else if (strType == "P")
				{
					strType = "R";
				}
				else if (strType == "R")
				{
					strType = "Z";
				}
			}
		}

		private void Set_Newest_Date_In_CBO()
		{
			int jj;
			// Set Start
			for (jj = 0; jj <= cboStart.Items.Count - 1; jj++)
			{
				if (cboStart.Items[jj].ToString() == FCConvert.ToString(DateTime))
				{
					cboStart.SelectedIndex = jj - 1;
					break;
				}
			}
			// jj
			if (cboStart.SelectedIndex == -1)
			{
				cboStart.SelectedIndex = 0;
			}
			if (fecherFoundation.DateAndTime.DateValue(cboStart.Items[cboStart.SelectedIndex].ToString()).ToOADate() > fecherFoundation.DateAndTime.DateValue(cboStart.Items[0].ToString()).ToOADate())
			{
				// do nothing
			}
			else
			{
				cboStart.SelectedIndex = 0;
			}
			// Set Ending
			for (jj = 0; jj <= cboEnd.Items.Count - 1; jj++)
			{
				if (cboEnd.Items[jj].ToString() == FCConvert.ToString(DateTime))
				{
					cboEnd.SelectedIndex = jj;
					break;
				}
			}
			// jj
		}

		private string Get_SQL_Dates(ref string strStart, ref string strEnd, ref string strTableName, ref object strOrderBy)
		{
			string Get_SQL_Dates = "";
            DateTime dat1;
            DateTime dat2;
			dat1 = FCConvert.ToDateTime(strStart);
			dat2 = FCConvert.ToDateTime(strEnd);
			if (dat2.ToOADate() < dat1.ToOADate())
			{
				Get_SQL_Dates = "SELECT * FROM " + strTableName + " WHERE Date BETWEEN '" + FCConvert.ToString(dat2) + "' AND '" + FCConvert.ToString(dat1) + "' ORDER BY " + strOrderBy;
			}
			else
			{
				Get_SQL_Dates = "SELECT * FROM " + strTableName + " WHERE Date BETWEEN '" + FCConvert.ToString(dat1) + "' AND '" + FCConvert.ToString(dat2) + "' ORDER BY " + strOrderBy;
			}
			return Get_SQL_Dates;
		}

		private void Exception_Report_Subtraction()
		{
			int lngPK1 = 0;
			int lngPK2 = 0;
			int lngCategory = 0;
			// 
			rsCategory.OpenRecordset("SELECT * FROM TownSummaryHeadings");
			// 
			if (cmbInterim.Text == "Interim Reports")
			{
				strSql = "SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1 AND CategoryAdjusted <> '130' ORDER BY Type, ExceptionReportDate";
			}
			else
			{
				strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(strSql);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = rs.Get_Fields_Int32("ID");
					rs.MoveFirst();
					lngPK2 = rs.Get_Fields_Int32("ID");
				}
				strSql = "SELECT * FROM ExceptionReport WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " AND CategoryAdjusted <> '130' ORDER BY Type, ExceptionReportDate";
			}
			rs.OpenRecordset(strSql);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				while (!rs.EndOfFile())
				{
					if (rs.Get_Fields_String("reason") != "Voiding MVR3 from Prior Closeout Period")
					{
						if (Conversion.Val(rs.Get_Fields_String("CategoryAdjusted")) == 0)
						{
							lngCategory = 0;
						}
						else
						{
							lngCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_String("CategoryAdjusted"))));
						}
						if (lngCategory != 0)
						{
							if (strSummaryDetail == "D")
							{
								lngCategory = 195;
								curSummaryRecord[lngCategory, 1] += rs.Get_Fields_Decimal("Fee");
								curSummaryRecord[lngCategory, 0] += 1;
							}
							else
							{
								curSummaryRecord[lngCategory, 1] += rs.Get_Fields_Decimal("Fee");
								if (rs.Get_Fields_Decimal("Fee") < 0)
								{
									curSummaryRecord[lngCategory, 0] -= 1;
								}
								else if (rs.Get_Fields_Decimal("Fee") > 0)
								{
									curSummaryRecord[lngCategory, 0] += 1;
								}
								else
								{
									if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("AddOrSubtract")) != "")
									{
										if (rs.Get_Fields_String("AddOrSubtract") == "-")
										{
											curSummaryRecord[lngCategory, 0] -= 1;
										}
										else
										{
											curSummaryRecord[lngCategory, 0] += 1;
										}
									}
									else
									{
										curSummaryRecord[lngCategory, 0] += 1;
									}
								}
							}
						}
					}
					rs.MoveNext();
				}
				if (strSummaryDetail == "D" && curSummaryRecord[195, 0] != 0)
				{
					WriteNewRecord(195, "99999", "99999", 99999, 99, 9, FCConvert.ToInt16(FCConvert.ToDouble(curSummaryRecord[195, 0])), curSummaryRecord[195, 1]);
				}
			}
		}

		public void DisketteVerificationReport()
		{
            string CL = "";
            string PL = "";
            string TR = "";
            string FeeString = "";
            Decimal FeeCurr;
			string OWN = "";
			string MST = "";
			string YST = "";
			string MM = "";
			string YY = "";
			FCFixedString Rec1 = new FCFixedString(251);
			clsDRWrapper rsItemized = new clsDRWrapper();
			clsDRWrapper rsSummary = new clsDRWrapper();
			Decimal TotalFee = 0;
			int TotalUnits = 0;
			Decimal TotalTotals = 0;
			int lngPK1;
			int lngPK2;
			string strReportDrive;
            int counter;
            FCFileSystem fs = new FCFileSystem();
			string strFilePath = "";
			FCFileSystem.FileClose();
			strReportDrive = "";

			strReportDrive = Path.Combine(FCFileSystem.Statics.UserDataFolder,"BMVReports");

            if (!Directory.Exists(strReportDrive))
            {
                Directory.CreateDirectory(strReportDrive);
            }
            FCFileSystem.FileOpen(5, Path.Combine("BMVReports",strResCode + "ED.rpt"), OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
            strFilePath = Path.Combine("BMVReports", strResCode + "in.dat");
			
			if (FCFileSystem.FileExists(strFilePath))
			{
				FCFileSystem.FileOpen(12, strFilePath, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(MotorVehicle.Statics.BMVRecord));
				if (FCFileSystem.LOF(12) == 0)
				{
					strLine = Strings.StrDup(80, " ");
					strHeading = "                   **** FILE VERIFICATION REPORT **** ";
					PrintHeading(1);
					rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
					strLine = (Strings.StrDup(33, " ") + "No Information");
					rtf1.Text = rtf1.Text + strLine + "\r\n";
					FCFileSystem.PrintLine(5, rtf1.Text);
					FCFileSystem.FileClose();
					return;
				}
			}
			else
			{
				strLine = Strings.StrDup(80, " ");
				strHeading = "                   **** FILE VERIFICATION REPORT **** ";
				PrintHeading(1);
				rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
				strLine = (Strings.StrDup(33, " ") + "No Information");
				rtf1.Text = rtf1.Text + strLine + "\r\n";
				FCFileSystem.PrintLine(5, rtf1.Text);
				FCFileSystem.FileClose();
				return;
			}
			strHeading = "                   **** FILE VERIFICATION REPORT **** ";
			rtf1.Text = "";
			PrintHeading(1);
			FCFileSystem.PrintLine(5, rtf1.Text);
			Page_Heading_Diskette_Verification_Report();
			LineCount = 10;
			for (counter = 1; counter <= FCConvert.ToInt16((FCFileSystem.LOF(12) / Marshal.SizeOf(MotorVehicle.Statics.BMVRecord))); counter++)
			{
				if (LineCount >= 60)
				{
					PageCount += 1;
					LineCount = 0;
					Page_Heading_Diskette_Verification_Report();
				}
				FCFileSystem.FileGet(12, ref MotorVehicle.Statics.BMVRecord, counter);
				CL = MotorVehicle.Statics.BMVRecord.Class;
				PL = MotorVehicle.Statics.BMVRecord.plate;
				TR = MotorVehicle.Statics.BMVRecord.MVR3;
				FeeString = FCConvert.ToString(FCConvert.ToDecimal(MotorVehicle.Statics.BMVRecord.RateFee) - FCConvert.ToDecimal(MotorVehicle.Statics.BMVRecord.CreditFee));
				FeeCurr = FCConvert.ToDecimal(MotorVehicle.Statics.BMVRecord.RateFee) - FCConvert.ToDecimal(MotorVehicle.Statics.BMVRecord.CreditFee);
				if (FeeCurr < 0)
					FeeCurr = 0;
				if (MotorVehicle.Statics.BMVRecord.DOB1 == "99999999")
				{
					OWN = Strings.Left(MotorVehicle.Statics.BMVRecord.Owner1, 39);
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1837: use custon constructor for initializing FixedString variables
					MotorVehicle.Statics.BMVRecordIndividualOwner = new MotorVehicle.IndividualOwner(0);
					MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 1, 50);
					MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 51, 20);
					MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 71, 1);
					MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = Strings.Mid(MotorVehicle.Statics.BMVRecord.Owner1, 72, 3);
					OWN = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.LastName) + " " + fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName) + " " + fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial) + " " + fecherFoundation.Strings.Trim(MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix));
					if (OWN.Length > 39)
					{
						OWN = Strings.Left(OWN, 39);
					}
				}
				if (OWN.Length < 39)
				{
					OWN += Strings.StrDup(39 - OWN.Length, " ");
				}
				MST = "       ";
				YST = Strings.Format(MotorVehicle.Statics.BMVRecord.YearStickerNumber, "00000000");
				MM = "  ";
				YY = Strings.Mid(MotorVehicle.Statics.BMVRecord.ExpirationDate, 3, 2);
				// 
				FCFileSystem.Print(5, CL + " " + PL + " " + TR);
				FCFileSystem.Print(5, Strings.Format(Strings.Format(FeeCurr, "  ##0.00 "), "@@@@@@@@@"));
				FCFileSystem.PrintLine(5, OWN + " " + YY + " " + YST);
				LineCount += 1;
				TOTCNTR += 1;
				TOTAMT += FeeCurr;
				// 
				CLCAT = CL + FeeString;
				// Line Input #12, Rec1
			}
			FCFileSystem.FileClose(12);
			strHeading = "               **** FILE VERIFICATION SUMMARY REPORT **** ";
			rtf1.Text = "";
			PrintHeading(1);
			FCFileSystem.PrintLine(5, rtf1.Text);
			strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSql);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = rs.Get_Fields_Int32("ID");
				rs.MoveFirst();
				lngPK2 = rs.Get_Fields_Int32("ID");
			}
			strSql = "SELECT DISTINCT COUNT(Class) AS TotalCount, SUM(RegRateCharge) AS TotalRegRate, Class, RegrateCharge FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " and OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND TransactionType <> 'DPS' AND TransactionType <> 'LPS' AND TransactionType <> 'ETO' AND Class <> 'AP' AND Plate <> 'NEW' AND ETO = 0 AND TransactionType <> 'ECO' AND TransactionType <> 'NRT' AND TransactionType <> 'RRT' GROUP BY Class, RegRateCharge ORDER BY Class, RegRateCharge";
			rsSummary.OpenRecordset(strSql);
			if (rsSummary.EndOfFile() != true && rsSummary.BeginningOfFile() != true)
			{
				rsSummary.MoveLast();
				rsSummary.MoveFirst();
				PrintHeading(2);
				FCFileSystem.PrintLine(5, "");
				FCFileSystem.PrintLine(5, "");
				FCFileSystem.PrintLine(5, "\t" + "CL" + "\t" + "FEE" + "\t" + "UNITS" + "\t" + "TOTAL");
				for (MotorVehicle.Statics.fnx = 1; MotorVehicle.Statics.fnx <= rsSummary.RecordCount(); MotorVehicle.Statics.fnx++)
				{
					FCFileSystem.PrintLine(5, "\t" + FCConvert.ToString(rsSummary.Get_Fields_String("Class")) + "\t" + Strings.Format(rsSummary.Get_Fields_Decimal("RegRateCharge"), "currency") + "\t" + FCConvert.ToString(rsSummary.Get_Fields("totalcount")) + "\t" + Strings.Format(rsSummary.Get_Fields("TotalRegRate"), "currency"));
					TotalFee += rsSummary.Get_Fields_Decimal("RegRateCharge");
					TotalUnits += rsSummary.Get_Fields_Int32("totalcount");
					TotalTotals += rsSummary.Get_Fields_Decimal("TotalRegRate");
					rsSummary.MoveNext();
				}
				// fnx
			}
			strSql = "SELECT DISTINCT COUNT(Class) AS TotalCount, SUM(StatePaid) AS TotalRegRate, StatePaid FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " and OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND (TransactionType = 'NRT' OR TransactionType = 'RRT') AND Class <> 'AP' AND Plate <> 'NEW' AND ETO = 0 GROUP BY StatePaid ORDER BY StatePaid";
			rsSummary.OpenRecordset(strSql);
			if (rsSummary.EndOfFile() != true && rsSummary.BeginningOfFile() != true)
			{
				rsSummary.MoveLast();
				rsSummary.MoveFirst();
				FCFileSystem.PrintLine(5, "\t" + "Transfers");
				for (MotorVehicle.Statics.fnx = 1; MotorVehicle.Statics.fnx <= rsSummary.RecordCount(); MotorVehicle.Statics.fnx++)
				{
					FCFileSystem.PrintLine(5, "\t" + "\t" + Strings.Format(rsSummary.Get_Fields("StatePaid"), "currency") + "\t" + FCConvert.ToString(rsSummary.Get_Fields("totalcount")) + "\t" + Strings.Format(rsSummary.Get_Fields("TotalRegRate"), "currency"));
					TotalFee += rsSummary.Get_Fields_Decimal("StatePaid");
					TotalUnits += rsSummary.Get_Fields_Int32("totalcount");
					TotalTotals += rsSummary.Get_Fields_Decimal("TotalRegRate");
					rsSummary.MoveNext();
				}
				// fnx
			}
			// strSQL = "SELECT DISTINCT COUNT(Class) AS TotalCount, SUM(StatePaid) AS TotalRegRate, StatePaid FROM ActivityMaster WHERE OldMVR3 > " & lngPK1 & " and OldMVR3 <= " & lngPK2 & " AND Status <> 'V' AND (TransactionType = 'ECO' or TransactionType = 'LPS') AND Class <> 'AP' AND Plate <> 'NEW' AND ETO = 0 GROUP BY StatePaid ORDER BY StatePaid"
			strSql = "SELECT DISTINCT COUNT(Class) AS TotalCount, (SUM(StatePaid) + SUM(StickerFee) + SUM(ReplacementFee)) AS TotalRegRate, StatePaid, ReplacementFee, StickerFee FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " and OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND Status <> 'V' AND (TransactionType = 'ECO' or TransactionType = 'LPS') AND Class <> 'AP' AND Plate <> 'NEW' AND ETO = 0 GROUP BY StatePaid, ReplacementFee, StickerFee ORDER BY StatePaid, ReplacementFee, StickerFee";
			rsSummary.OpenRecordset(strSql);
			if (rsSummary.EndOfFile() != true && rsSummary.BeginningOfFile() != true)
			{
				rsSummary.MoveLast();
				rsSummary.MoveFirst();
				FCFileSystem.PrintLine(5, "\t" + "Corrections");
				for (MotorVehicle.Statics.fnx = 1; MotorVehicle.Statics.fnx <= rsSummary.RecordCount(); MotorVehicle.Statics.fnx++)
				{
					FCFileSystem.PrintLine(5, "\t" + "\t" + Strings.Format(rsSummary.Get_Fields_Decimal("StatePaid") + rsSummary.Get_Fields_Decimal("ReplacementFee") + rsSummary.Get_Fields_Decimal("StickerFee"), "currency") + "\t" + FCConvert.ToString(rsSummary.Get_Fields("totalcount")) + "\t" + Strings.Format(rsSummary.Get_Fields("TotalRegRate"), "currency"));
					if (Conversion.Val(rsSummary.Get_Fields_Decimal("StatePaid") + rsSummary.Get_Fields_Decimal("ReplacementFee") + rsSummary.Get_Fields_Decimal("StickerFee")) != 0)
					{
						TotalFee += rsSummary.Get_Fields_Decimal("StatePaid") + rsSummary.Get_Fields_Decimal("ReplacementFee") + rsSummary.Get_Fields_Decimal("StickerFee");
					}
					// If Val(.Fields("StatePaid")) <> 0 Then
					// TotalFee = TotalFee + .Fields("StatePaid")
					// End If
					TotalUnits += rsSummary.Get_Fields_Int32("totalcount");
					if (Conversion.Val(rsSummary.Get_Fields_String("TotalRegRate")) != 0)
					{
						TotalTotals += rsSummary.Get_Fields_Decimal("TotalRegRate");
					}
					rsSummary.MoveNext();
				}
				// fnx
			}
			FCFileSystem.PrintLine(5, "");
			FCFileSystem.PrintLine(5, "");
			FCFileSystem.Print(5, "TOTALS");
			FCFileSystem.PrintLine(5, "\t" + "\t" + Strings.Format(TotalFee, "currency") + "\t" + FCConvert.ToString(TotalUnits) + "\t" + Strings.Format(TotalTotals, "currency"));
			FCFileSystem.FileClose(5);
		}

		public bool CreateDiskFile()
		{
			bool CreateDiskFile = false;
			clsDRWrapper rsDiskReport = new clsDRWrapper();
            FCFileSystem ff = new FCFileSystem();
			clsDRWrapper rs1 = new clsDRWrapper();
			DateTime dat1;
			DateTime dat2;
			int lngPK1;
			int lngPK2;
			string LocalFile;
			string strReportDrive;
			int counter = 0;

			dat1 = FCConvert.ToDateTime(frmReport.InstancePtr.cboStart.Text);
			dat2 = FCConvert.ToDateTime(frmReport.InstancePtr.cboEnd.Text);
			strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSql);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = rs.Get_Fields_Int32("ID");
				rs.MoveFirst();
				lngPK2 = rs.Get_Fields_Int32("ID");
			}

			if (cmbInterim.Text == "Live Reports")
			{
				strSql = "SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND ETO = 0 AND Status <> 'V' AND TransactionType <> 'DPS' ORDER BY mvr3";
			}
			else
			{
				return CreateDiskFile;
			}

			string FileName;
            string downloadName;
            FileName = Path.Combine("BMVReports\\", strResCode + "IN.DAT");
			downloadName = strResCode + "IN.DAT";
			rsDiskReport.OpenRecordset(strSql);
			FCFileSystem.FileClose();
            strReportDrive = Path.Combine(FCFileSystem.Statics.UserDataFolder,"BMVReports");
            if (!Directory.Exists(strReportDrive))
            {
                Directory.CreateDirectory(strReportDrive);
            }
			if (FCFileSystem.FileExists(FileName))
			{
                FCFileSystem.DeleteFile(FileName);
			}
			FCFileSystem.FileOpen(22, FileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(MotorVehicle.Statics.BMVRecord));
			if (rsDiskReport.EndOfFile() != true && rsDiskReport.BeginningOfFile() != true)
			{
				rsDiskReport.MoveLast();
				rsDiskReport.MoveFirst();
				FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
				counter = 1;
				while (!rsDiskReport.EndOfFile())
				{
					CreateRecord(rsDiskReport);
					FCFileSystem.FilePut(22, MotorVehicle.Statics.BMVRecord, -1/*? counter */);
					rsDiskReport.MoveNext();
					counter += 1;
				}
				FCFileSystem.FileClose(22);
				FCGlobal.Screen.MousePointer = 0;
			}
			else
			{
				FCFileSystem.FileClose(22);
				MessageBox.Show("No registration data found for specified reporting range.", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FCFileSystem.DeleteFile(FileName);//, true);
				CreateDiskFile = false;
				return CreateDiskFile;
			}
            FCFileSystem a;
			a = new FCFileSystem();
            
            if (FCFileSystem.FileExists(FileName))
			{
            
				using(var stream = new FileStream(Path.Combine(FCFileSystem.Statics.UserDataFolder, FileName), FileMode.Open))
				{
					FCUtils.Download(stream, downloadName);
				}
				CreateDiskFile = true;
			}
			else
			{
				MessageBox.Show("Could not find file " + FileName + ".  Please check your reporting dates and try again.", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				CreateDiskFile = false;
			}

			return CreateDiskFile;
		}

		public void CreateRecord(clsDRWrapper rs2)
		{
			string temp = "";
			int checker;
			clsDRWrapper rsResCodeCheck = new clsDRWrapper();
			clsDRWrapper rsMaster = new clsDRWrapper();
			clsDRWrapper rsClass = new clsDRWrapper();
			int lngWeight = 0;
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			clsPrintCodes oPrtCodes;
			rsResCodeCheck.OpenRecordset("SELECT * FROM DefaultInfo");
			MotorVehicle.ClearBMVRecord();
			MotorVehicle.Statics.BMVRecord.Class = rs2.Get_Fields_String("Class");
			MotorVehicle.Statics.BMVRecord.plate = rs2.Get_Fields_String("plate");

            if ((rs2.Get_Fields_String("OldClass") != rs2.Get_Fields_String("Class") && fecherFoundation.Strings.Trim(rs2.Get_Fields_String("OldClass")) != "") || (rs2.Get_Fields_String("OldPlate") != rs2.Get_Fields_String("plate") && fecherFoundation.Strings.Trim(rs2.Get_Fields_String("OldPlate")) != ""))
			{
				MotorVehicle.Statics.BMVRecord.OldClass = rs2.Get_Fields_String("OldClass");
				MotorVehicle.Statics.BMVRecord.OldPlate = rs2.Get_Fields_String("OldPlate");
			}
			MotorVehicle.Statics.BMVRecord.MVR3 = modGlobalFunctions.PadStringWithSpaces(rs2.Get_Fields_String("MVR3"), 8);
			if (rs2.Get_Fields_String("TransactionType") == "RRT" || rs2.Get_Fields_String("TransactionType") == "RRR")
			{
				MotorVehicle.Statics.BMVRecord.RegFlag = "Y";
			}
			else if (rs2.Get_Fields_String("TransactionType") == "NRT" || rs2.Get_Fields_String("TransactionType") == "NRR")
			{
				MotorVehicle.Statics.BMVRecord.RegFlag = "N";
			}
			else
            {
                MotorVehicle.Statics.BMVRecord.RegFlag = rs2.Get_Fields_String("ReReg") == "Y" ? "Y" : "N";
            }
			MotorVehicle.Statics.BMVRecord.ExciseExemptFlag = rs2.Get_Fields_Boolean("ExciseExempt") == true ? "Y" : "N";
			MotorVehicle.Statics.BMVRecord.Status = "A";
			if (rs2.Get_Fields_String("Class") != "AP")
			{
				rsClass.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + rs2.Get_Fields_String("Class") + "' AND SystemCode = '" + rs2.Get_Fields_String("Subclass") + "'", "TWMV0000.vb1");
				if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
				{
					if (rsClass.Get_Fields_Decimal("RegistrationFee") == 0)
					{
						MotorVehicle.Statics.BMVRecord.FeeExemptFlag = "Y";
					}
				}
			}
			if (MotorVehicle.Statics.BMVRecord.FeeExemptFlag != "Y")
            {
                MotorVehicle.Statics.BMVRecord.FeeExemptFlag = rs2.Get_Fields_Boolean("RegExempt") == true ? "Y" : "N";
            }
			if (rs2.Get_Fields_String("TransactionType") != "DPR")
            {
                MotorVehicle.Statics.BMVRecord.YearStickerNumber = rs2.Get_Fields_Int32("YearStickerNumber") != 0 ? modGlobalFunctions.PadStringWithSpaces(rs2.Get_Fields_String("YearStickerNumber"), 8) : Strings.StrDup(8, " ");

                MotorVehicle.Statics.BMVRecord.MonthStickerNumber = rs2.Get_Fields_Int32("MonthStickerNumber") != 0 ? modGlobalFunctions.PadStringWithSpaces(rs2.Get_Fields_String("MonthStickerNumber"), 8) : Strings.StrDup(8, " ");
            }
			MotorVehicle.Statics.BMVRecord.EffectiveDate = Strings.Format(rs2.Get_Fields_DateTime("EffectiveDate"), "yyyymmdd");
			MotorVehicle.Statics.BMVRecord.ExpirationDate = Strings.Format(rs2.Get_Fields_DateTime("ExpireDate"), "yyyymmdd");
			MotorVehicle.Statics.BMVRecord.VIN = rs2.Get_Fields_String("Vin");
			MotorVehicle.Statics.BMVRecord.Year = rs2.Get_Fields_String("Year");
			MotorVehicle.Statics.BMVRecord.make = fecherFoundation.Strings.Trim(rs2.Get_Fields_String("make") + "");
			MotorVehicle.Statics.BMVRecord.model = fecherFoundation.Strings.Trim(rs2.Get_Fields_String("model") + "");

			MotorVehicle.Statics.BMVRecord.Style = fecherFoundation.Strings.Trim(rs2.Get_Fields_String("Style") + "");
			MotorVehicle.Statics.BMVRecord.Color1 = rs2.Get_Fields_String("color1");
			MotorVehicle.Statics.BMVRecord.Color2 = rs2.Get_Fields_String("color2") == "NA" ? "  " : rs2.Get_Fields_String("color2");
			MotorVehicle.Statics.BMVRecord.Axles = Conversion.Val(rs2.Get_Fields_Int32("Axles")) != 0 ? Strings.Format(Conversion.Val(rs2.Get_Fields_Int32("Axles")), "00") : "  ";
			MotorVehicle.Statics.BMVRecord.Tires = Conversion.Val(rs2.Get_Fields_Int32("Tires")) != 0 ? Strings.Format(Conversion.Val(rs2.Get_Fields_Int32("Tires")), "000") : "   ";
			MotorVehicle.Statics.BMVRecord.NetWeight = Conversion.Val(rs2.Get_Fields("NetWeight")) != 0 ? (string) Strings.Format(rs2.Get_Fields("NetWeight"), "000000") : "000000";
			MotorVehicle.Statics.BMVRecord.RegisteredWeight = Conversion.Val(rs2.Get_Fields_Int32("RegisteredWeightNew")) != 0 ? Strings.Format(rs2.Get_Fields_Int32("RegisteredWeightNew"), "000000") : "000000";
			MotorVehicle.Statics.BMVRecord.Fuel = rs2.Get_Fields_String("Fuel") != "N" ? rs2.Get_Fields_String("Fuel") : " ";

			pInfo = pCont.GetParty(rs2.Get_Fields_Int32("PartyID1"));
			BMV_Registrant1(rs2, pInfo);

            if (Conversion.Val(rs2.Get_Fields_Int32("Odometer")) != 0)
			{
				MotorVehicle.Statics.BMVRecord.Mileage = Strings.Format(rs2.Get_Fields_Int32("Odometer").ToString(), "000000");
            }

			if (rs2.Get_Fields_Boolean("BaseIsSalePrice") && (rs2.Get_Fields_String("TransactionType") == "NRR" || rs2.Get_Fields_String("TransactionType") == "RRR") && (rs2.Get_Fields_String("Class") == "CC" || rs2.Get_Fields_String("Class") == "AF" || rs2.Get_Fields_String("Class") == "AC" || rs2.Get_Fields_String("Class") == "LC" || rs2.Get_Fields_String("Class") == "AP" || rs2.Get_Fields_String("Class") == "CO" || rs2.Get_Fields_String("Class") == "TT" || rs2.Get_Fields_String("Class") == "FM" || (rs2.Get_Fields_String("Class") == "TR" && rs2.Get_Fields_String("Subclass") == "T1") || (rs2.Get_Fields_String("Class") == "TR" && rs2.Get_Fields_String("Subclass") == "T2")) && (Conversion.Val(rs2.Get_Fields("Year")) >= 1996 && Conversion.Val(rs2.Get_Fields_Int32("RegisteredWeightNew")) > 26000))
            {
                if (Conversion.Val(rs2.Get_Fields("Value")) != 0)
				{
					MotorVehicle.Statics.BMVRecord.BasePrice = Strings.Format(rs2.Get_Fields("Value").ToString(), "00000000");
					
				}

                MotorVehicle.Statics.BMVRecord.SalePrice = Conversion.Val(rs2.Get_Fields_Int32("BasePrice")) != 0 ? Strings.Format(rs2.Get_Fields_Int32("BasePrice").ToString(), "00000000.00") : "00000000.00";
            }
			else
			{
				if (Conversion.Val(rs2.Get_Fields_Int32("BasePrice")) != 0)
				{
					MotorVehicle.Statics.BMVRecord.BasePrice = Strings.Format(rs2.Get_Fields_Int32("BasePrice").ToString(), "00000000");
				}
			}
			if (rs2.Get_Fields_Boolean("EAP"))
			{
				MotorVehicle.Statics.BMVRecord.ExciseTax = Strings.Format(rs2.Get_Fields_Decimal("ExciseTaxFull").ToString(), "000000.00");
			}
			else
            {
                MotorVehicle.Statics.BMVRecord.ExciseTax = Conversion.Val(rs2.Get_Fields_Decimal("ExciseTaxCharged")) > 0 ? Strings.Format(rs2.Get_Fields_Decimal("ExciseTaxCharged").ToString(), "000000.00") : "000000.00";
            }

			if (MotorVehicle.Statics.BMVRecord.ExciseExemptFlag != "Y")
			{
				MotorVehicle.Statics.BMVRecord.MilYear = rs2.Get_Fields_String("MillYear");
			}
			
			if (rs2.Get_Fields_String("OwnerCode1") == "M" || rs2.Get_Fields_String("OwnerCode1") == "C" || rs2.Get_Fields_String("OwnerCode2") == "M" || rs2.Get_Fields_String("OwnerCode2") == "C")
			{
				if (fecherFoundation.Strings.Trim(rs2.Get_Fields_String("TaxIDNumber")) != "")
				{
					temp = rs2.Get_Fields_String("TaxIDNumber");
					MotorVehicle.Statics.BMVRecord.TaxIDNumber = modGlobalFunctions.PadStringWithSpaces(temp.Replace("-", ""), 12);
				}
			}
			MotorVehicle.Statics.BMVRecord.DOTNumber = (rs2.Get_Fields_String("DOTNumber") + "");
			if (rs2.Get_Fields_String("Class") == "VT" || rs2.Get_Fields_String("Class") == "DS")
			{
				if (!fecherFoundation.FCUtils.IsNull(rs2.Get_Fields_String("Battle")))
				{
					if (fecherFoundation.Strings.Trim(rs2.Get_Fields_String("Battle")) != "X" && fecherFoundation.Strings.Trim(rs2.Get_Fields_String("Battle")) != "")
					{
						// kk07192016  Moved decal definitions to clsPrintCodes
						oPrtCodes = new clsPrintCodes();
						MotorVehicle.Statics.BMVRecord.Battle = oPrtCodes.GetVeteranDecalText(rs2.Get_Fields_String("Battle"));
						oPrtCodes = null;
					}
				}
			}
			MotorVehicle.Statics.BMVRecord.Address1 = rs2.Get_Fields_String("Address");
			MotorVehicle.Statics.BMVRecord.Address2 = rs2.Get_Fields_String("Address2");
			MotorVehicle.Statics.BMVRecord.City = rs2.Get_Fields_String("City");
			MotorVehicle.Statics.BMVRecord.State = rs2.Get_Fields_String("State");
			MotorVehicle.Statics.BMVRecord.Zip = rs2.Get_Fields_String("Zip");
			MotorVehicle.Statics.BMVRecord.Country = rs2.Get_Fields_String("Country");
			MotorVehicle.Statics.BMVRecord.ResidenceCity = rs2.Get_Fields_String("Residence");
			MotorVehicle.Statics.BMVRecord.ResidenceState = fecherFoundation.Strings.Trim(rs2.Get_Fields_String("ResidenceState") + "");
            MotorVehicle.Statics.BMVRecord.ResidenceCode = fecherFoundation.Strings.Trim(rs2.Get_Fields_String("ResidenceCode")) == ""
                ? rsResCodeCheck.Get_Fields_String("ResidenceCode")
                : rs2.Get_Fields_String("ResidenceCode").Length == 4
                    ? "0" + rs2.Get_Fields_String("ResidenceCode")
                    : rs2.Get_Fields_String("ResidenceCode");
            MotorVehicle.Statics.BMVRecord.ResidenceCountry = rs2.Get_Fields_String("ResidenceCountry");
			MotorVehicle.Statics.BMVRecord.ResidenceAddress = fecherFoundation.Strings.Trim(rs2.Get_Fields_String("ResidenceAddress") + "");
			if (rs2.Get_Fields_String("OwnerCode2") == "D")
			{
				MotorVehicle.Statics.BMVRecord.DBAName = rs2.Get_Fields_String("Reg2FirstName").Trim().ToUpper();
				if (String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Reg2FirstName")))
				{
					pInfo = pCont.GetParty(rs2.Get_Fields_Int32("PartyID2"));
					MotorVehicle.Statics.BMVRecord.DBAName = fecherFoundation.Strings.UCase(pInfo.FirstName);
				}
			}
			else if (rs2.Get_Fields_String("OwnerCode3") == "D")
			{
				MotorVehicle.Statics.BMVRecord.DBAName = rs2.Get_Fields_String("Reg3FirstName").Trim().ToUpper();
				if (String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Reg3FirstName")))
				{
					pInfo = pCont.GetParty(rs2.Get_Fields_Int32("PartyID3"));
					MotorVehicle.Statics.BMVRecord.DBAName = fecherFoundation.Strings.UCase(pInfo.FirstName);
				}
			}

			pInfo = pCont.GetParty(rs2.Get_Fields_Int32("PartyID2"));
			BMV_Registrant2(rs2, pInfo);

            pInfo = pCont.GetParty(rs2.Get_Fields_Int32("PartyID3"));
			BMV_Registrant3(rs2, pInfo);

            MotorVehicle.Statics.BMVRecord.TransactionDate = (Strings.Format(rs2.Get_Fields_DateTime("DateUpdated"), "yyyymmdd") + ":" + Strings.Format(rs2.Get_Fields_DateTime("DateUpdated"), "HH:mm:ss"));
			MotorVehicle.Statics.BMVRecord.Unit = rs2.Get_Fields_String("UnitNumber");
			MotorVehicle.Statics.BMVRecord.VanityFlag = ((rs2.Get_Fields_Decimal("InitialFee") != 0 || rs2.Get_Fields_Boolean("VanityFlag")) ? "Y" : "N");

			if (!String.IsNullOrEmpty(rs2.Get_Fields_String("PriorTaxReceipt")) && Convert.ToInt32(rs2.Get_Fields_String("PriorTaxReceipt")) != 0)
			{
				MotorVehicle.Statics.BMVRecord.OldMVR3 = rs2.Get_Fields_String("PriorTaxReceipt");
			}
			else
			{
				rsMaster.OpenRecordset("SELECT * FROM Master WHERE MVR3 = " + rs2.Get_Fields_Int32("MVR3"));
				if (rsMaster.EndOfFile() != true && rsMaster.BeginningOfFile() != true)
				{
					if (Conversion.Val(rsMaster.Get_Fields_Int32("OldMVR3")) != 0)
					{
						MotorVehicle.Statics.BMVRecord.OldMVR3 =
							modGlobalFunctions.PadStringWithSpaces(rsMaster.Get_Fields_String("OldMVR3"), 8);
					}
				}
			}
			MotorVehicle.Statics.BMVRecord.SpecialtyFee = Strings.Format(rs2.Get_Fields_Decimal("PlateFeeNew") + rs2.Get_Fields_Decimal("PlateFeeReReg"), "0000.00");
            MotorVehicle.Statics.BMVRecord.VanityFee = Strings.Format(rs2.Get_Fields_String("TransactionType") != "DPR" ? rs2.Get_Fields_Decimal("InitialFee") : 0, "0000.00");

            MotorVehicle.Statics.BMVRecord.TransferFee = Strings.Format(rs2.Get_Fields("TransferFee"), "0000.00");
			MotorVehicle.Statics.BMVRecord.OtherFee = Strings.Format(rs2.Get_Fields_Decimal("StickerFee") + rs2.Get_Fields_Decimal("ReplacementFee") + rs2.Get_Fields_Decimal("ReservePlate") + rs2.Get_Fields_Decimal("OutOfRotation") + rs2.Get_Fields_Decimal("DuplicateRegistrationFee"), "0000.00");
			if (rs2.Get_Fields_String("TransactionType") == "ECO" && rs2.Get_Fields_Decimal("RegRateCharge") == 0)
			{
                lngWeight = rs2.Get_Fields_Int32(rs2.Get_Fields_String("Class") != "SE" ? "RegisteredWeightNew" : "NetWeight");

                MotorVehicle.Statics.BMVRecord.RateFee = Strings.Format(MotorVehicle.GetRegistrationRate(lngWeight, rs2.Get_Fields_String("Class"), rs2.Get_Fields_String("Subclass")), "0000.00");
				MotorVehicle.Statics.BMVRecord.CreditFee = MotorVehicle.Statics.BMVRecord.RateFee;
			}
			else
			{
				MotorVehicle.Statics.BMVRecord.RateFee = Strings.Format(rs2.Get_Fields_Decimal("RegRateCharge"), "0000.00");
				MotorVehicle.Statics.BMVRecord.CreditFee = Strings.Format(Conversion.Val(rs2.Get_Fields_Decimal("TransferCreditUsed")) + Conversion.Val(rs2.Get_Fields_Decimal("CommercialCredit")), "0000.00");
			}
			// BMVRecord.RegistrationFee = Format(.Fields("RegRateCharge") - (.Fields("TransferCreditUsed") + .Fields("CommercialCredit")), "0000.00")
			MotorVehicle.Statics.BMVRecord.SalesTaxFee = Strings.Format(rs2.Get_Fields_Double("SalesTax") > 0 ? rs2.Get_Fields_Double("SalesTax") : 0, "0000.00");
			MotorVehicle.Statics.BMVRecord.TitleFee = Strings.Format(rs2.Get_Fields("TitleFee"), "0000.00");
			MotorVehicle.Statics.BMVRecord.TownResidenceCode = rsResCodeCheck.Get_Fields_String("ResidenceCode");
			if (fecherFoundation.Strings.Trim(rs2.Get_Fields_String("CTANumber")) != "")
			{
				MotorVehicle.Statics.BMVRecord.CTA1 = rs2.Get_Fields_String("CTANumber").ToUpper	();
			}
			if (fecherFoundation.Strings.Trim(rs2.Get_Fields_String("DoubleCTANumber")) != "")
			{
				MotorVehicle.Statics.BMVRecord.CTA2 = rs2.Get_Fields_String("DoubleCTANumber").ToUpper	();
			}
			MotorVehicle.Statics.BMVRecord.PriorTitle = rs2.Get_Fields_String("PriorTitleNumber");
			MotorVehicle.Statics.BMVRecord.PriorState = rs2.Get_Fields_String("PriorTitleState");
			MotorVehicle.Statics.BMVRecord.RentalFlag = (rs2.Get_Fields_Boolean("Rental") ? "Y" : "N");
			MotorVehicle.Statics.BMVRecord.ExciseTaxDate = Strings.Format(rs2.Get_Fields_DateTime("ExciseTaxDate"), "yyyymmdd");
			MotorVehicle.Statics.BMVRecord.CRLF = "\r\n";
		}

        private static void BMV_Registrant1(clsDRWrapper rs2, cParty pInfo)
        {
            if (rs2.Get_Fields_String("OwnerCode1") == "I")
            {
                MotorVehicle.Statics.BMVRecord.DOB1 = Strings.Format(rs2.Get_Fields_DateTime("DOB1"), "yyyymmdd");

                //FC:FINAL:MSH - i.issue #1837: use custon constructor for initializing FixedString variables
                MotorVehicle.Statics.BMVRecordIndividualOwner = new MotorVehicle.IndividualOwner(0);
                MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = rs2.Get_Fields_String("Reg1LastName").Trim().ToUpper();
                MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = rs2.Get_Fields_String("Reg1FirstName").Trim().ToUpper();
                MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = rs2.Get_Fields_String("Reg1MI").Left(1).ToUpper();
                MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = rs2.Get_Fields_String("Reg1Designation").Left(3).ToUpper();
                if (String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Reg1LastName")) && String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Reg1FirstName")))
                {
	                MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(pInfo.LastName));
	                MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(pInfo.FirstName));
	                MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(pInfo.MiddleName + ""), 1));
	                MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(pInfo.Designation + ""), 3));
                }
                var refVar = MotorVehicle.Statics.BMVRecord.Owner1;
                Strings.MidSet(ref refVar, 1, 50, MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value);
                MotorVehicle.Statics.BMVRecord.Owner1 = refVar;
                Strings.MidSet(ref refVar, 51, 20, MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value);
                MotorVehicle.Statics.BMVRecord.Owner1 = refVar;
                Strings.MidSet(ref refVar, 71, 1, MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value);
                MotorVehicle.Statics.BMVRecord.Owner1 = refVar;
                Strings.MidSet(ref refVar, 72, 3, MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value);
                MotorVehicle.Statics.BMVRecord.Owner1 = refVar;
            }
            else if (rs2.Get_Fields_String("OwnerCode1") != "N")
            {
                MotorVehicle.Statics.BMVRecord.DOB1 = "99999999";
                MotorVehicle.Statics.BMVRecord.Owner1 = rs2.Get_Fields_String("Owner1").Trim().ToUpper();
                if (String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Owner1")))
                {
	                MotorVehicle.Statics.BMVRecord.Owner1 = fecherFoundation.Strings.UCase(pInfo.FirstName);
                }
            }

            var leaseCode1 = rs2.Get_Fields_String("LeaseCode1");

            switch (leaseCode1)
            {
                case "E":
                    MotorVehicle.Statics.BMVRecord.LeaseFlag1 = "E";

                    break;
                case "R":
                    MotorVehicle.Statics.BMVRecord.LeaseFlag1 = "R";

                    break;
                case "T":
                    MotorVehicle.Statics.BMVRecord.TrustFlag1 = "E";

                    break;
            }
        }

        private static void BMV_Registrant2(clsDRWrapper rs2, cParty pInfo)
        {
            if (rs2.Get_Fields_String("OwnerCode2") != "N" && rs2.Get_Fields_String("OwnerCode2") != "D")
            {
                if (rs2.Get_Fields_String("OwnerCode2") == "I")
                {
					//FC:FINAL:MSH - i.issue #1837: use custon constructor for initializing FixedString variables
					MotorVehicle.Statics.BMVRecordIndividualOwner = new MotorVehicle.IndividualOwner(0);
					MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = rs2.Get_Fields_String("Reg2LastName").Trim().ToUpper();
					MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = rs2.Get_Fields_String("Reg2FirstName").Trim().ToUpper();
					MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = rs2.Get_Fields_String("Reg2MI").Left(1).ToUpper();
					MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = rs2.Get_Fields_String("Reg2Designation").Left(3).ToUpper();
					if (String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Reg2LastName")) && String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Reg2FirstName")))
					{
						MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(pInfo.LastName));
						MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(pInfo.FirstName));
						MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(pInfo.MiddleName + ""), 1));
						MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(pInfo.Designation + ""), 3));
					}
					var refVar = MotorVehicle.Statics.BMVRecord.Owner2;
                    Strings.MidSet(ref refVar, 1, 50, MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value);
                    MotorVehicle.Statics.BMVRecord.Owner2 = refVar;
                    Strings.MidSet(ref refVar, 51, 20, MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value);
                    MotorVehicle.Statics.BMVRecord.Owner2 = refVar;
                    Strings.MidSet(ref refVar, 71, 1, MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value);
                    MotorVehicle.Statics.BMVRecord.Owner2 = refVar;
                    Strings.MidSet(ref refVar, 72, 3, MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value);
                    MotorVehicle.Statics.BMVRecord.Owner2 = refVar;
                }
                else
                {
	                MotorVehicle.Statics.BMVRecord.Owner2 = rs2.Get_Fields_String("Owner2").Trim().ToUpper();
	                if (String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Owner2")))
	                {
		                MotorVehicle.Statics.BMVRecord.Owner2 = fecherFoundation.Strings.UCase(pInfo.FirstName);
	                }
                }
            }

            if (rs2.Get_Fields_String("OwnerCode2") == "I")
            {
                MotorVehicle.Statics.BMVRecord.DOB2 = Strings.Format(rs2.Get_Fields_DateTime("DOB2"), "yyyymmdd");
            }
            else if (rs2.Get_Fields_String("OwnerCode2") != "N" && rs2.Get_Fields_String("OwnerCode2") != "D")
            {
                MotorVehicle.Statics.BMVRecord.DOB2 = "99999999";
            }

            var leaseCode2 = rs2.Get_Fields_String("LeaseCode2");

            switch (leaseCode2)
            {
                case "E":
                    MotorVehicle.Statics.BMVRecord.LeaseFlag2 = "E";

                    break;
                case "R":
                    MotorVehicle.Statics.BMVRecord.LeaseFlag2 = "R";

                    break;
                case "T":
                    MotorVehicle.Statics.BMVRecord.TrustFlag2 = "E";

                    break;
            }
        }

        private static void BMV_Registrant3(clsDRWrapper rs2, cParty pInfo)
        {
            if (rs2.Get_Fields_String("OwnerCode3") != "N" && rs2.Get_Fields_String("OwnerCode3") != "D")
            {
                if (rs2.Get_Fields_String("OwnerCode3") == "I")
                {
                    //FC:FINAL:MSH - i.issue #1837: use custon constructor for initializing FixedString variables
                    MotorVehicle.Statics.BMVRecordIndividualOwner = new MotorVehicle.IndividualOwner(0);
                    MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = rs2.Get_Fields_String("Reg3LastName").Trim().ToUpper();
                    MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = rs2.Get_Fields_String("Reg3FirstName").Trim().ToUpper();
                    MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = rs2.Get_Fields_String("Reg3MI").Left(1).ToUpper();
                    MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = rs2.Get_Fields_String("Reg3Designation").Left(3).ToUpper();
                    if (String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Reg3LastName")) && String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Reg3FirstName")))
                    {
	                    MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(pInfo.LastName));
	                    MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(pInfo.FirstName));
	                    MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(pInfo.MiddleName + ""), 1));
	                    MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value = fecherFoundation.Strings.UCase(Strings.Left(fecherFoundation.Strings.Trim(pInfo.Designation + ""), 3));
                    }
                    var refVar = MotorVehicle.Statics.BMVRecord.Owner3;
                    Strings.MidSet(ref refVar, 1, 50, MotorVehicle.Statics.BMVRecordIndividualOwner.LastName.Value);
                    MotorVehicle.Statics.BMVRecord.Owner3 = refVar;
                    Strings.MidSet(ref refVar, 51, 20, MotorVehicle.Statics.BMVRecordIndividualOwner.FirstName.Value);
                    MotorVehicle.Statics.BMVRecord.Owner3 = refVar;
                    Strings.MidSet(ref refVar, 71, 1, MotorVehicle.Statics.BMVRecordIndividualOwner.MiddleInitial.Value);
                    MotorVehicle.Statics.BMVRecord.Owner3 = refVar;
                    Strings.MidSet(ref refVar, 72, 3, MotorVehicle.Statics.BMVRecordIndividualOwner.Suffix.Value);
                    MotorVehicle.Statics.BMVRecord.Owner3 = refVar;
                }
                else
                {
	                MotorVehicle.Statics.BMVRecord.Owner3 = rs2.Get_Fields_String("Owner3").Trim().ToUpper();
	                if (String.IsNullOrWhiteSpace(rs2.Get_Fields_String("Owner3")))
	                {
		                MotorVehicle.Statics.BMVRecord.Owner3 = fecherFoundation.Strings.UCase(pInfo.FirstName);
	                }
                }
            }

            if (rs2.Get_Fields_String("OwnerCode3") == "I")
            {
                MotorVehicle.Statics.BMVRecord.DOB3 = Strings.Format(rs2.Get_Fields_DateTime("DOB3"), "yyyymmdd");
            }
            else
                if (rs2.Get_Fields_String("OwnerCode3") != "N")
                {
                    MotorVehicle.Statics.BMVRecord.DOB3 = "99999999";
                }

            var leaseCode3 = rs2.Get_Fields_String("LeaseCode3");

            switch (leaseCode3)
            {
                case "E":
                    MotorVehicle.Statics.BMVRecord.LeaseFlag3 = "E";

                    break;
                case "R":
                    MotorVehicle.Statics.BMVRecord.LeaseFlag3 = "R";

                    break;
                case "T":
                    MotorVehicle.Statics.BMVRecord.TrustFlag3 = "E";

                    break;
            }
        }

        public void CopyFileToDiskette()
		{
            FCFileSystem ff = new FCFileSystem();
			int xx;
			string LocalFile = "";
			string rescode = "";
			string FileName = "";
			string strReportDrive;
			try
			{
				fecherFoundation.Information.Err().Clear();

                strReportDrive = "BMVReports";
                var fullPathToReports = Path.Combine(FCFileSystem.Statics.UserDataFolder, strReportDrive);
				
				if (!Directory.Exists(fullPathToReports))
				{
					Directory.CreateDirectory(fullPathToReports);
				}
				//if (fecherFoundation.Strings.UCase(Environment.CurrentDirectory) != fecherFoundation.Strings.UCase(Strings.Left(strReportDrive, strReportDrive.Length - 1)))
				//{
					//FC:FINAL:MSH - i.issue #1837: create zip file for senging all files to client side
					using (var zipToOpen = new FileStream(Path.Combine(fullPathToReports , "TempReportZip.zip"), FileMode.Create))
					{
						using (System.IO.Compression.ZipArchive archive = new System.IO.Compression.ZipArchive(zipToOpen, System.IO.Compression.ZipArchiveMode.Create))
						{
							FileName = strResCode + "IN.DAT";
                           // FCFileSystem.FileCopy(FileName, strReportDrive + FileName);//, true);
                            archive.CreateEntryFromFile(Path.Combine(fullPathToReports , FileName), FileName);
							//FCFileSystem.DeleteFile(FileName);//, true);
                            var sourceName = Path.Combine(fullPathToReports, strResCode + "VF.RPT");								  // 
                            var localName = Path.Combine(strReportDrive,strResCode + "VF.RPT");
                            if (FCFileSystem.FileExists(localName) == true)
							{
								//FCFileSystem.FileCopy(strResCode + "VF.RPT", strReportDrive + strResCode + "VF.RPT");//, true);
                                archive.CreateEntryFromFile(sourceName, strResCode + "VF.RPT");
                            }

                            sourceName = Path.Combine(fullPathToReports, strResCode + "TA.RPT");
                            localName = Path.Combine(strReportDrive,strResCode + "TA.RPT");
							if (FCFileSystem.FileExists(localName) == true)
							{
								//FCFileSystem.FileCopy(strResCode + "TA.RPT", strReportDrive + strResCode + "TA.RPT");//, true);
                                archive.CreateEntryFromFile(sourceName, strResCode + "TA.RPT");
                            }

                            sourceName = Path.Combine(fullPathToReports, strResCode + "SA.RPT");
                            localName = Path.Combine(strReportDrive, strResCode + "SA.RPT");
							if (FCFileSystem.FileExists(localName) == true)
							{
								//FCFileSystem.FileCopy(strResCode + "SA.RPT", strReportDrive + strResCode + "SA.RPT");//, true);
                                archive.CreateEntryFromFile(sourceName, strResCode + "SA.RPT");
                            }

                            sourceName = Path.Combine(fullPathToReports, strResCode + "TT.RPT");
                            localName = Path.Combine(strReportDrive, strResCode + "TT.RPT");
							if (FCFileSystem.FileExists(localName) == true)
							{
								//FCFileSystem.FileCopy(strResCode + "TT.RPT", strReportDrive + strResCode + "TT.RPT");//, true);
                                archive.CreateEntryFromFile(sourceName, strResCode + "TT.RPT");
                            }

                            sourceName = Path.Combine(fullPathToReports, strResCode + "EX.RPT");
                            localName = Path.Combine(strReportDrive, strResCode + "EX.RPT");
							if (FCFileSystem.FileExists(localName) == true)
							{
								//FCFileSystem.FileCopy(strResCode + "EX.RPT", strReportDrive + strResCode + "EX.RPT");//, true);
                                archive.CreateEntryFromFile(sourceName, strResCode + "EX.RPT");
                            }

                            sourceName = Path.Combine(fullPathToReports, strResCode + "IA.RPT");
                            localName = Path.Combine(strReportDrive, strResCode + "IA.RPT");
							if (FCFileSystem.FileExists(localName) == true)
							{
								//FCFileSystem.FileCopy(strResCode + "IA.RPT", strReportDrive + strResCode + "IA.RPT");//, true);
                                archive.CreateEntryFromFile(sourceName, strResCode + "IA.RPT");
                            }

                            sourceName = Path.Combine(fullPathToReports, strResCode + "GC.RPT");
                            localName = Path.Combine(strReportDrive, strResCode + "GC.RPT");
						if (FCFileSystem.FileExists(localName) == true)
							{
								//FCFileSystem.FileCopy(strResCode + "GC.RPT", strReportDrive + strResCode + "GC.RPT");//, true);
                                archive.CreateEntryFromFile(sourceName, strResCode + "GC.RPT");
                            }

						sourceName = Path.Combine(fullPathToReports, strResCode + "TS.RPT");
                        localName = Path.Combine(strReportDrive, strResCode + "TS.RPT");
						if (FCFileSystem.FileExists(localName) == true)
							{
								//FCFileSystem.FileCopy(strResCode + "TS.RPT", strReportDrive + strResCode + "TS.RPT");//, true);
                                archive.CreateEntryFromFile(sourceName, strResCode + "TS.RPT");
                            }

                        sourceName = Path.Combine(fullPathToReports, strResCode + "TD.RPT");
                        localName = Path.Combine(strReportDrive, strResCode + "TD.RPT");
						if (FCFileSystem.FileExists(localName) == true)
							{
                                archive.CreateEntryFromFile(sourceName, strResCode + "TD.RPT");
                            }

                        sourceName = Path.Combine(fullPathToReports, strResCode + "ED.RPT");
                        localName = Path.Combine(strReportDrive, strResCode + "ED.RPT");
						if (FCFileSystem.FileExists(localName) == true)
							{
                                archive.CreateEntryFromFile(sourceName, strResCode + "ED.RPT");
                            }

                        sourceName = Path.Combine(fullPathToReports, strResCode + "UT.RPT");
                        localName = Path.Combine(strReportDrive, strResCode + "UT.RPT");
						if (FCFileSystem.FileExists(localName) == true)
							{
                                archive.CreateEntryFromFile(sourceName, strResCode + "UT.RPT");
                            }
						}
					}
					//FC:FINAL:MSH - i.issue #1837: send created archive to client
					using(var stream = new FileStream(Path.Combine(fullPathToReports , "TempReportZip.zip"), FileMode.Open))
					{
						FCUtils.Download(stream, "Reports.zip");
					}
				//}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				if (fecherFoundation.Information.Err(ex).Number == 53 || fecherFoundation.Information.Err(ex).Number == 76)
				{
					MessageBox.Show("File not Found.  Please check your reports directory in File Maintenance > Customize.", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				}
			}
		}

		public void GetResCode()
		{
			rsResCode.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rsResCode.EndOfFile() != true && rsResCode.BeginningOfFile() != true)
			{
				rsResCode.MoveLast();
				rsResCode.MoveFirst();
				strResCode = fecherFoundation.Strings.Trim(FCConvert.ToString(rsResCode.Get_Fields_String("ResidenceCode")));
			}
			//App.DoEvents();
			// rsResCode.Close
			rsResCode.Reset();
		}

		private void Page_Heading_Diskette_Verification_Report()
		{
			FCFileSystem.PrintLine(5, "CL  PLATE   TX RCPT   FEE  OWNER------------                       YR STKR ");
			FCFileSystem.PrintLine(5, " ");
		}

		public void PrintDiskVerificationReport()
		{
			string strDirectory = "";
			try
			{
				// On Error GoTo errtag4
				fecherFoundation.Information.Err().Clear();
				fecherFoundation.Information.Err().Clear();
				// rtf1.FileName = strResCode & "verify.txt"
				LoadTextBox();
				//App.DoEvents();
				modGNBas.Sleep(5000);
				if (rtf1.Text != "")
				{
					fecherFoundation.Information.Err().Clear();
					// dlg1_Save.Flags = 0	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
					//- dlg1.CancelError = true;
					/*? On Error Resume Next  */
					dlg1.Show();
					if (fecherFoundation.Information.Err().Number == 0)
					{
						if ((dlg1.Flags & FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDPrintToFile)) == FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDPrintToFile))
						{
							strDirectory = Environment.CurrentDirectory;
							fecherFoundation.Information.Err().Clear();
							// dlg1_Save.Flags = 0	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
							//- dlg1.CancelError = true;
							/*? On Error Resume Next  */
							dlg1_Save.Filter = "*.txt";
							dlg1_Save.Show();
							if (fecherFoundation.Information.Err().Number == 0)
							{
								FCFileSystem.FileOpen(1, dlg1_Save.FileName, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
								FCFileSystem.PrintLine(1, rtf1.Text);
								FCFileSystem.FileClose(1);
							}
							Environment.CurrentDirectory = strDirectory;
						}
						else
						{
							// User Clicked PRINT
							FCGlobal.Printer.Print();
							// If rtf1.Visible = False Then Call PrintRoutines
							// rtf1.Text = rtf1.Text & vbcrlf & vbcrlf
							FCGlobal.Printer.FontBold = false;
							FCGlobal.Printer.FontItalic = false;
							FCGlobal.Printer.FontName = "Courier New";
							FCGlobal.Printer.FontSize = 11;
							PrinterPrintMV(ref rtf1, 0);
						}
					}
					else
					{
						// User Clicked Cancel   **** DO NOT PRINT ****
						fecherFoundation.Information.Err().Clear();
					}
				}
				FCFileSystem.FileClose();
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				if (fecherFoundation.Information.Err(ex).Number == 71)
				{
					MessageBox.Show("The disk is not ready. Check disk and try again.", "Disk Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else if (fecherFoundation.Information.Err(ex).Number == 53)
				{
					MessageBox.Show("The verification file is not on this disk. Check disk and try again.", "Disk Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					MessageBox.Show("An error has occured trying to print the report. The error description and number are:  " + fecherFoundation.Information.Err(ex).Description + "   " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number), "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				fecherFoundation.Information.Err(ex).Clear();
				FCFileSystem.FileClose();
			}
		}

		private void PrintReport()
		{
			rtf1.Text = rtf1.Text + "\r\n" + "\r\n";
			FCGlobal.Printer.FontBold = false;
			FCGlobal.Printer.FontItalic = false;
			FCGlobal.Printer.FontName = "Courier New";
			FCGlobal.Printer.FontSize = 11;
			PrinterPrintMV(ref rtf1, 0);
		}

		private void LoadTextBox()
		{
			string strLine = "";
			int intCount;
			rtf1.Text = "";
			FCFileSystem.FileOpen(1, strResCode + "ver.txt", OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
			while (!FCFileSystem.EOF(1))
			{
				strLine = string.Empty;
				strLine = FCFileSystem.LineInput(1);
				rtf1.Text = rtf1.Text + strLine + "\r\n";
			}
		}

		private void RegistrationListing()
		{
			int lngPK1 = 0;
			int lngPK2 = 0;
			int intHeadingNumber;
			clsDRWrapper rsResCode = new clsDRWrapper();
			int tempcode = 0;
			clsDRWrapper rsFees = new clsDRWrapper();
			// vbPorter upgrade warning: curAmountToSubtract As Decimal	OnWrite(double, int)
			Decimal curAmountToSubtract;
			// 
			LineCount = 0;
			PageCount = 0;
			// 
			LineCount = 0;
			PageCount = 0;
			//rtf1.RightMargin = 5;
			// 
			rsFees.OpenRecordset("SELECT * FROM DefaultInfo");
			if (cmbInterim.Text != "Interim Reports")
			{
				strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(strSql);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.MoveFirst();
					lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				}
			}
			// 
			if (cmbAllRegistrations.Text == "All Registrations")
			{
				if (cmbInterim.Text == "Interim Reports")
				{
					strSql = "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND ETO = 0 ORDER BY mvr3";
					rs.OpenRecordset(strSql);
				}
				else
				{
					strSql = "SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND ETO = 0 ORDER BY mvr3";
					rs.OpenRecordset(strSql);
				}
			}
			else if (cmbAllRegistrations.Text == "In Town Registrations")
			{
				rsResCode.OpenRecordset("SELECT * FROM DefaultInfo");
				tempcode = FCConvert.ToInt32(rsResCode.Get_Fields_String("ResidenceCode"));
				if (cmbInterim.Text == "Interim Reports")
				{
					strSql = "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND ETO = 0 AND ResidenceCode = " + FCConvert.ToString(tempcode) + " ORDER BY mvr3";
					rs.OpenRecordset(strSql);
				}
				else
				{
					strSql = "SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND ETO = 0 AND ResidenceCode = " + FCConvert.ToString(tempcode) + " ORDER BY mvr3";
					rs.OpenRecordset(strSql);
				}
			}
			else
			{
				rsResCode.OpenRecordset("SELECT * FROM DefaultInfo");
				tempcode = FCConvert.ToInt32(rsResCode.Get_Fields_String("ResidenceCode"));
				if (cmbInterim.Text == "Interim Reports")
				{
					strSql = "SELECT * FROM ActivityMaster WHERE OldMVR3 < 1 AND ETO = 0 AND ResidenceCode <> " + FCConvert.ToString(tempcode) + " ORDER BY mvr3";
					rs.OpenRecordset(strSql);
				}
				else
				{
					strSql = "SELECT * FROM ActivityMaster WHERE OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND ETO = 0 AND ResidenceCode <> " + FCConvert.ToString(tempcode) + " ORDER BY mvr3";
					rs.OpenRecordset(strSql);
				}
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				PageCount = 1;
				strLine = Strings.StrDup(80, " ");
				rtf1.Text = rtf1.Text + "                         **** REGISTRATION LISTING ****             " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
				//FC:FINAL:MSH - i.issue #1825: use correct format for displaying time value
				//rtf1.Text = rtf1.Text + "\r\n" + Strings.Space(67) + Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "MM/dd/yyyy") + "\r\n" + "\r\n";
				rtf1.Text = rtf1.Text + "\r\n" + Strings.Space(67) + Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm:ss tt") + "\r\n" + "\r\n";
				rtf1.Text = rtf1.Text + "--CLASS--  --PLATE--  --MVR3--  --Status--  --Mo Stkr--  --Yr Stkr--  --Fees--" + "\r\n" + "\r\n";
				while (!rs.EndOfFile())
				{
					strLine = Strings.StrDup(80, " ");
					Strings.MidSet(ref strLine, 5, 10, FCConvert.ToString(rs.Get_Fields_String("Class")));
					Strings.MidSet(ref strLine, 13, 10, FCConvert.ToString(rs.Get_Fields_String("plate")));
					Strings.MidSet(ref strLine, 24, 10, FCConvert.ToString(rs.Get_Fields_Int32("MVR3")));
					Strings.MidSet(ref strLine, 38, 10, FCConvert.ToString(rs.Get_Fields_String("Status")));
					if (Conversion.Val(rs.Get_Fields_Int32("MonthStickerNumber")) != 0)
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("MonthStickerNumber")) == 0)
						{
							// do nothing
						}
						else
						{
							Strings.MidSet(ref strLine, 48, 10, FCConvert.ToString(rs.Get_Fields_Int32("MonthStickerNumber")));
						}
					}
					if (Conversion.Val(rs.Get_Fields_Int32("YearStickerNumber")) != 0)
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("YearStickerNumber")) == 0)
						{
							// do nothing
						}
						else
						{
							Strings.MidSet(ref strLine, 61, 10, FCConvert.ToString(rs.Get_Fields_Int32("YearStickerNumber")));
						}
					}
					if (FCConvert.ToString(rs.Get_Fields("TransactionType")) == "DPR")
					{
						Strings.MidSet(ref strLine, 71, 8, FCConvert.ToString(MotorVehicle.strPadSpace(Strings.Format(rs.Get_Fields_Decimal("DuplicateRegistrationFee") + rsFees.Get_Fields_Decimal("DupRegAgentFee"), "#,##0.00"), 8)));
					}
					else if (FCConvert.ToString(rs.Get_Fields("TransactionType")) == "LPS" || FCConvert.ToString(rs.Get_Fields("TransactionType")) == "DPS")
					{
						Strings.MidSet(ref strLine, 71, 8, FCConvert.ToString(MotorVehicle.strPadSpace(Strings.Format(rs.Get_Fields_Decimal("ReplacementFee") + rs.Get_Fields_Decimal("StickerFee"), "#,##0.00"), 8)));
					}
					else
					{
						if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("EAP")))
						{
							if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("ExciseHalfRate")))
							{
								curAmountToSubtract = FCConvert.ToDecimal((rs.Get_Fields_Decimal("ExciseTaxFull") / 2) - (rs.Get_Fields_Decimal("ExciseCreditFull") / 2));
							}
							else
							{
								curAmountToSubtract = FCConvert.ToDecimal(rs.Get_Fields_Decimal("ExciseTaxFull") - rs.Get_Fields_Decimal("ExciseCreditFull"));
							}
							if (curAmountToSubtract < 0)
							{
								curAmountToSubtract = 0;
							}
							Strings.MidSet(ref strLine, 71, 8, FCConvert.ToString(MotorVehicle.strPadSpace(Strings.Format((rs.Get_Fields_Decimal("LocalPaid") - curAmountToSubtract) + rs.Get_Fields_Decimal("StatePaid"), "#,##0.00"), 8)));
						}
						else
						{
							Strings.MidSet(ref strLine, 71, 8, FCConvert.ToString(MotorVehicle.strPadSpace(Strings.Format(rs.Get_Fields_Decimal("LocalPaid") + rs.Get_Fields_Decimal("StatePaid"), "#,##0.00"), 8)));
						}
					}
					rtf1.Text = rtf1.Text + strLine + "\r\n";
					LineCount += 1;
					rs.MoveNext();
				}
				rtf1.SelectionStart = 0;
				rtf1.SelectionLength = 1;
			}
			else
			{
				MessageBox.Show("There are no records for this reporting period.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCFileSystem.FileClose();
				return;
			}

			rptRegistration.InstancePtr.Unload();
			frmReportViewer.InstancePtr.Init(rptRegistration.InstancePtr);
			
			rs.Reset();
		}

		private void PrintTotalLine(string Title, ref int SectionCount, float SectionTotal)
		{
			rtf1.Text = rtf1.Text + "\r\n";
			if (SectionTotal != 0)
			{
				rtf1.Text = rtf1.Text + Title + "  " + FCConvert.ToString(SectionCount) + "         " + Strings.Format(SectionTotal, "#,##0.00") + "\r\n";
			}
			else
			{
				rtf1.Text = rtf1.Text + Title + "  " + FCConvert.ToString(SectionCount) + "         " + "\r\n";
			}
		}

		private void PrintTotals(ref int ST, ref int pt, ref int mc, ref float MCTotal, ref int NC, ref int MA, ref float MATotal, ref int pm, ref float PMTotal, ref int dr)
		{
			rtf1.Text = rtf1.Text + "     " + "TOTAL STICKER ADJUSTMENTS:       " + FCConvert.ToString(ST) + "\r\n";
			rtf1.Text = rtf1.Text + "     " + "TOTAL PLATE ADJUSTMENTS:         " + FCConvert.ToString(pt) + "\r\n";
			rtf1.Text = rtf1.Text + "     " + "TOTAL MONETARY CORRECTIONS:      " + FCConvert.ToString(mc) + "       " + Strings.Format(Strings.Format(MCTotal, "#,##0.00"), "@@@@@@@@") + "\r\n";
			rtf1.Text = rtf1.Text + "     " + "TOTAL NON MONETARY CORRECTIONS:  " + FCConvert.ToString(NC) + "\r\n";
			rtf1.Text = rtf1.Text + "     " + "TOTAL MONETARY ADJUSTMENTS:      " + FCConvert.ToString(MA) + "       " + Strings.Format(Strings.Format(MATotal, "#,##0.00"), "@@@@@@@@") + "\r\n";
			rtf1.Text = rtf1.Text + "     " + "TOTAL PERMITS:                   " + FCConvert.ToString(pm) + "       " + Strings.Format(Strings.Format(PMTotal, "#,##0.00"), "@@@@@@@@") + "\r\n";
			rtf1.Text = rtf1.Text + "     " + "TOTAL NO-FEE DUPLICATE-STR:      " + FCConvert.ToString(dr) + "\r\n";
		}

		private void optReport_DoubleClick(int Index, object sender, System.EventArgs e)
		{
			if (Index == 3)
			{
				fraInventoryOptions.Visible = true;
			}
		}

		private void optSelect_CheckedChanged(object sender, System.EventArgs e)
		{
			FraSelect.Enabled = true;
		}

		private void CloseoutTellers()
		{
			clsDRWrapper rsActivity = new clsDRWrapper();
			clsDRWrapper rsTransit = new clsDRWrapper();
			clsDRWrapper rsSpecial = new clsDRWrapper();
			clsDRWrapper rsBooster = new clsDRWrapper();
			clsDRWrapper rsTellers = new clsDRWrapper();
			clsDRWrapper rsAdjustment = new clsDRWrapper();
			clsDRWrapper rsException = new clsDRWrapper();
			clsDRWrapper rsGiftCert = new clsDRWrapper();
			string[] Tellers = null;
			int intCounter;
			int intCounter2;
			bool blnClosed;
			clsDRWrapper rsPC1 = new clsDRWrapper();
			int lngID = 0;
			string strSql = "";
			string strOpID = "";
			int intUnclosedTellers = 0;
			clsDRWrapper rsRecieved = new clsDRWrapper();
			clsDRWrapper rsInventory = new clsDRWrapper();
			rsTellers.OpenRecordset("SELECT TellerID FROM TellerCloseoutTable WHERE TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
			rsActivity.OpenRecordset("SELECT DISTINCT OpID FROM ActivityMaster WHERE TellerCloseoutID = 0");
			rsTransit.OpenRecordset("SELECT DISTINCT OpID FROM TransitPlates WHERE TellerCloseoutID = 0");
			rsSpecial.OpenRecordset("SELECT DISTINCT OpID FROM SpecialRegistration WHERE TellerCloseoutID = 0");
			rsBooster.OpenRecordset("SELECT DISTINCT OpID FROM Booster WHERE TellerCloseoutID = 0");
			rsAdjustment.OpenRecordset("SELECT DISTINCT OpID FROM InventoryAdjustments WHERE TellerCloseoutID = 0");
			rsException.OpenRecordset("SELECT DISTINCT OpID FROM ExceptionReport WHERE TellerCloseoutID = 0");
			rsGiftCert.OpenRecordset("SELECT DISTINCT OpID FROM GiftCertificateRegister WHERE TellerCloseoutID = 0");
			blnClosed = false;
			if (rsTellers.EndOfFile() != true && rsTellers.BeginningOfFile() != true)
			{
				rsTellers.MoveLast();
				rsTellers.MoveFirst();
				intUnclosedTellers = 0;
				if (rsActivity.EndOfFile() != true && rsActivity.BeginningOfFile() != true)
				{
					rsActivity.MoveLast();
					rsActivity.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						do
						{
							if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsActivity.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
							{
								blnClosed = true;
								break;
							}
							rsTellers.MoveNext();
						}
						while (rsTellers.EndOfFile() != true);
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsActivity.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsActivity.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsActivity.MoveNext();
					}
					while (rsActivity.EndOfFile() != true);
				}
				if (rsTransit.EndOfFile() != true && rsTransit.BeginningOfFile() != true)
				{
					rsTransit.MoveLast();
					rsTransit.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (blnClosed == false)
						{
							do
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
								{
									blnClosed = true;
									break;
								}
								rsTellers.MoveNext();
							}
							while (rsTellers.EndOfFile() != true);
						}
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsTransit.MoveNext();
					}
					while (rsTransit.EndOfFile() != true);
				}
				if (rsSpecial.EndOfFile() != true && rsSpecial.BeginningOfFile() != true)
				{
					rsSpecial.MoveLast();
					rsSpecial.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (blnClosed == false)
						{
							do
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
								{
									blnClosed = true;
									break;
								}
								rsTellers.MoveNext();
							}
							while (rsTellers.EndOfFile() != true);
						}
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsSpecial.MoveNext();
					}
					while (rsSpecial.EndOfFile() != true);
				}
				if (rsBooster.EndOfFile() != true && rsBooster.BeginningOfFile() != true)
				{
					rsBooster.MoveLast();
					rsBooster.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (blnClosed == false)
						{
							do
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
								{
									blnClosed = true;
									break;
								}
								rsTellers.MoveNext();
							}
							while (rsTellers.EndOfFile() != true);
						}
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsBooster.MoveNext();
					}
					while (rsBooster.EndOfFile() != true);
				}
				if (rsAdjustment.EndOfFile() != true && rsAdjustment.BeginningOfFile() != true)
				{
					rsAdjustment.MoveLast();
					rsAdjustment.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (blnClosed == false)
						{
							do
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
								{
									blnClosed = true;
									break;
								}
								rsTellers.MoveNext();
							}
							while (rsTellers.EndOfFile() != true);
						}
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsAdjustment.MoveNext();
					}
					while (rsAdjustment.EndOfFile() != true);
				}
				if (rsException.EndOfFile() != true && rsException.BeginningOfFile() != true)
				{
					rsException.MoveLast();
					rsException.MoveFirst();
					do
					{
						blnClosed = false;
						rsTellers.MoveFirst();
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (blnClosed == false)
						{
							do
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(FCConvert.ToString(rsTellers.Get_Fields_String("TellerID"))))
								{
									blnClosed = true;
									break;
								}
								rsTellers.MoveNext();
							}
							while (rsTellers.EndOfFile() != true);
						}
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsException.MoveNext();
					}
					while (rsException.EndOfFile() != true);
				}
			}
			else
			{
				intUnclosedTellers = 0;
				if (rsActivity.EndOfFile() != true && rsActivity.BeginningOfFile() != true)
				{
					rsActivity.MoveLast();
					rsActivity.MoveFirst();
					do
					{
						strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsActivity.Get_Fields_String("OpID")));
						// check to see if there is a closeout entry for this teller or if this is the first one
						rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
						if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
						{
							// do nothing
						}
						else
						{
							// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
						}
						// make a closeout entry in the TellerCloseoutTable table in the database
						rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
						rsPC1.AddNew();
						rsPC1.Set_Fields("TellerID", strOpID);
						rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
						rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
						rsPC1.Set_Fields("PeriodCloseoutID", 0);
						rsPC1.Update();
						lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
						// Set Teller CloseoutID for ActivityMaster
						strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						// Set Teller CloseoutID for Special Registrations
						strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						// Set Teller CloseoutID for Transit Plates
						strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						// Set Teller CloseoutID for Booster Permits
						strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						// Set Teller CloseoutID for Gift Certificates
						strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						// Set Teller CloseoutID for InventoryAdjustments
						strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						// Set Teller CloseoutID for ExciseTax
						strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						// Set Teller CloseoutID for ExceptionReport
						strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						// Set Teller CloseoutID for TitleApplications
						strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						// Set Teller CloseoutID for TemporaryPlateRegister
						strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						// Set Teller CloseoutID for Inventory
						strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
						MotorVehicle.SetTellerID(ref strSql, ref lngID);
						intUnclosedTellers += 1;
						Array.Resize(ref Tellers, intUnclosedTellers + 1);
						Tellers[intUnclosedTellers - 1] = strOpID;
						rsActivity.MoveNext();
					}
					while (rsActivity.EndOfFile() != true);
				}
				if (rsTransit.EndOfFile() != true && rsTransit.BeginningOfFile() != true)
				{
					rsTransit.MoveLast();
					rsTransit.MoveFirst();
					do
					{
						blnClosed = false;
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsTransit.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsTransit.MoveNext();
					}
					while (rsTransit.EndOfFile() != true);
				}
				if (rsSpecial.EndOfFile() != true && rsSpecial.BeginningOfFile() != true)
				{
					rsSpecial.MoveLast();
					rsSpecial.MoveFirst();
					do
					{
						blnClosed = false;
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsSpecial.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsSpecial.MoveNext();
					}
					while (rsSpecial.EndOfFile() != true);
				}
				if (rsBooster.EndOfFile() != true && rsBooster.BeginningOfFile() != true)
				{
					rsBooster.MoveLast();
					rsBooster.MoveFirst();
					do
					{
						blnClosed = false;
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsBooster.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsBooster.MoveNext();
					}
					while (rsBooster.EndOfFile() != true);
				}
				if (rsAdjustment.EndOfFile() != true && rsAdjustment.BeginningOfFile() != true)
				{
					rsAdjustment.MoveLast();
					rsAdjustment.MoveFirst();
					do
					{
						blnClosed = false;
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsAdjustment.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsAdjustment.MoveNext();
					}
					while (rsAdjustment.EndOfFile() != true);
				}
				if (rsException.EndOfFile() != true && rsException.BeginningOfFile() != true)
				{
					rsException.MoveLast();
					rsException.MoveFirst();
					do
					{
						blnClosed = false;
						if (intUnclosedTellers > 0)
						{
							for (intCounter = 0; intCounter <= intUnclosedTellers; intCounter++)
							{
								if (fecherFoundation.Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID"))) == fecherFoundation.Strings.UCase(Tellers[intCounter]))
								{
									blnClosed = true;
									break;
								}
							}
						}
						if (!blnClosed)
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID")));
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(strOpID) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", strOpID);
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE ID = 0");
							rsPC1.AddNew();
							rsPC1.Set_Fields("TellerID", strOpID);
							rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
							rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							rsPC1.Set_Fields("PeriodCloseoutID", 0);
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Special Registrations
							strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Transit Plates
							strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Booster Permits
							strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Gift Certificates
							strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for InventoryAdjustments
							strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExciseTax
							strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for ExceptionReport
							strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TitleApplications
							strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for TemporaryPlateRegister
							strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							// Set Teller CloseoutID for Inventory
							strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + strOpID + "'";
							MotorVehicle.SetTellerID(ref strSql, ref lngID);
							intUnclosedTellers += 1;
							Array.Resize(ref Tellers, intUnclosedTellers + 1);
							Tellers[intUnclosedTellers - 1] = strOpID;
						}
						else
						{
							strOpID = fecherFoundation.Strings.UCase(FCConvert.ToString(rsException.Get_Fields_String("OpID")));
							rsRecieved.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' AND TellerCloseoutID = 0 AND UPPER(OpID) = '" + strOpID + "'");
							if (rsRecieved.EndOfFile() != true && rsRecieved.BeginningOfFile() != true)
							{
								rsRecieved.MoveLast();
								rsRecieved.MoveFirst();
								rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + strOpID + "' AND TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "'");
								if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
								{
									do
									{
										rsRecieved.Edit();
										rsRecieved.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsRecieved.Update();
										rsRecieved.MoveNext();
									}
									while (rsRecieved.EndOfFile() != true);
								}
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE TellerCloseoutID = 0 AND Status = 'A' AND UPPER(OpID) = '" + strOpID + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									do
									{
										rsInventory.Edit();
										rsInventory.Set_Fields("TellerCloseoutID", rsPC1.Get_Fields_Int32("ID"));
										rsInventory.Update();
										rsInventory.MoveNext();
									}
									while (rsInventory.EndOfFile() != true);
								}
							}
						}
						rsException.MoveNext();
					}
					while (rsException.EndOfFile() != true);
				}
			}
			rsTellers.Reset();
			rsActivity.Reset();
			rsTransit.Reset();
			rsSpecial.Reset();
			rsBooster.Reset();
			rsAdjustment.Reset();
			rsException.Reset();
			rsPC1.Reset();
			rsRecieved.Reset();
			rsInventory.Reset();
			rsGiftCert.Reset();
		}

		private void SetPeriodCloseoutID(ref int PeriodID)
		{
			clsDRWrapper rsTellers = new clsDRWrapper();
			rsTellers.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE PeriodCloseoutID = 0");
			if (rsTellers.EndOfFile() != true && rsTellers.BeginningOfFile() != true)
			{
				rsTellers.MoveLast();
				rsTellers.MoveFirst();
				do
				{
					rsTellers.Edit();
					rsTellers.Set_Fields("PeriodCloseoutID", PeriodID);
					rsTellers.Update();
					rsTellers.MoveNext();
				}
				while (rsTellers.EndOfFile() != true);
			}
		}

		private void MakeCode(string x, ref clsDRWrapper rsTemp)
		{
			if (x == "M")
			{
				if (rsTemp.Get_Fields_Double("MonthStickerCharge") + rsTemp.Get_Fields_Double("MonthStickerNoCharge") > 1)
				{
					strMStickerCode = "SMD";
					if (rsTemp.Get_Fields_Double("MonthStickerMonth") < 10)
					{
						strMStickerCode += "0" + fecherFoundation.Strings.Trim(Conversion.Str(rsTemp.Get_Fields_Int32("MonthStickerMonth")));
					}
					else
					{
						strMStickerCode += fecherFoundation.Strings.Trim(Conversion.Str(rsTemp.Get_Fields_Int32("MonthStickerMonth")));
					}
				}
				else
				{
					MotorVehicle.Statics.strCodeM = "SMS";
					if (rsTemp.Get_Fields_Double("MonthStickerMonth") < 10)
					{
						strMStickerCode += "0" + fecherFoundation.Strings.Trim(Conversion.Str(rsTemp.Get_Fields_Int32("MonthStickerMonth")));
					}
					else
					{
						strMStickerCode += fecherFoundation.Strings.Trim(Conversion.Str(rsTemp.Get_Fields_Int32("MonthStickerMonth")));
					}
				}
			}
			else
			{
				if (rsTemp.Get_Fields_Double("YearStickerCharge") + rsTemp.Get_Fields_Double("YearStickerNoCharge") > 1)
				{
					strYStickerCode = "SYD" + Strings.Right(FCConvert.ToString(rsTemp.Get_Fields_DateTime("ExpireDate")), 2);
				}
				else
				{
					strYStickerCode = "SYS" + Strings.Right(FCConvert.ToString(rsTemp.Get_Fields_DateTime("ExpireDate")), 2);
				}
			}
		}

		//FC:FINAL:MSH - i.issue #1831: remove 'ref' keyword to avoid exceptions at calling method
		//private void MakeCodeP(ref string x)
		private void MakeCodeP(string x)
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM Inventory WHERE FormattedInventory = '" + x + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				strPlateCode = rs.Get_Fields_String("Code");
			}
			else
			{
				strPlateCode = "";
			}
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList;
			string strPerm = "";
			int lngChild = 0;
			clsChildList = new clsDRWrapper();
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case MotorVehicle.INTERIMREPORTS:
						{
							if (strPerm == "N")
							{
								if (cmbInterim.Items.Contains("Interim Reports"))
								{
									cmbInterim.Items.Remove("Interim Reports");
								}
							}
							else
							{
								if (!cmbInterim.Items.Contains("Interim Reports"))
								{
									cmbInterim.Items.Insert(0, "Interim Reports");
								}
							}
							break;
						}
					case MotorVehicle.CLOSEPERIOD:
						{
							if (strPerm == "N")
							{
								cmdCloseout.Enabled = false;
							}
							else
							{
								cmdCloseout.Enabled = true;
							}
							break;
						}
					case MotorVehicle.VERIFICATIONREPORT:
						{
							if (strPerm == "N")
							{
								if (cmbReport.Items.Contains("File - Create / Verify / Reports"))
								{
									cmbReport.Items.Remove("File - Create / Verify / Reports");
								}
								if (cmbReport.Items.Contains("File Verify Report"))
								{
									cmbReport.Items.Remove("File Verify Report");
								}
								if (cmbReport.Items.Contains("Copy Reports"))
								{
									cmbReport.Items.Remove("Copy Reports");
								}
								if (cmbReport.Items.Contains("Create Data File"))
								{
									cmbReport.Items.Remove("Create Data File");
								}
							}
							else
							{
								if (!cmbReport.Items.Contains("File - Create / Verify / Reports"))
								{
									cmbReport.Items.Insert(12, "File - Create / Verify / Reports");
								}
								if (!cmbReport.Items.Contains("File Verify Report"))
								{
									cmbReport.Items.Insert(13, "File Verify Report");
								}
								if (!cmbReport.Items.Contains("Copy Reports"))
								{
									cmbReport.Items.Insert(14, "Copy Reports");
								}
								if (!cmbReport.Items.Contains("Create Data File"))
								{
									cmbReport.Items.Insert(15, "Create Data File");
								}
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
		}

		private void ShowAdjustmentRecord(ref clsDRWrapper rsInfo)
		{
			string strLow = "";
			string strHigh = "";
			int lngAmount = 0;
			int counter;
			string strClass = "";
			bool blnFound = false;
			Strings.MidSet(ref strLine, 1, 10, Strings.Format(rsInfo.Get_Fields_DateTime("DateOfAdjustment"), "MM/dd/yyyy"));
			if (rsInfo.Get_Fields_String("AdjustmentCode") == "R")
			{
				Strings.MidSet(ref strLine, 13, 6, Strings.StrDup(6 - rsInfo.Get_Fields_Int32("QuantityAdjusted").ToString().Length, " ") + rsInfo.Get_Fields_Int32("QuantityAdjusted"));
				lngAmount = rsInfo.Get_Fields_Int32("QuantityAdjusted");
			}
			else
			{
				Strings.MidSet(ref strLine, 13, 6, Strings.StrDup(5 - rsInfo.Get_Fields_Int32("QuantityAdjusted").ToString().Length, " ") + "-" + rsInfo.Get_Fields_Int32("QuantityAdjusted"));
				lngAmount = rsInfo.Get_Fields_Int32("QuantityAdjusted") * -1;
			}
			strLow = rsInfo.Get_Fields_String("Low").ToString();
			strHigh = rsInfo.Get_Fields_String("High").ToString();
			if (rsInfo.Get_Fields_String("InventoryType") == "RXSXX")
			{
				Strings.MidSet(ref strLine, 22, 8 + Strings.Len(strLow) + Strings.Len(strHigh), "SRP " + strLow + " - " + strHigh);
				lngSpecialPermitCount += lngAmount;
			}
			else if (rsInfo.Get_Fields_String("InventoryType") == "PXSXX")
			{
				Strings.MidSet(ref strLine, 22, 8 + Strings.Len(strLow) + Strings.Len(strHigh), "TRN " + strLow + " - " + strHigh);
				lngTransitCount += lngAmount;
			}
			else if (rsInfo.Get_Fields_String("InventoryType") == "BXSXX")
			{
				Strings.MidSet(ref strLine, 22, 8 + Strings.Len(strLow) + Strings.Len(strHigh), "BST " + strLow + " - " + strHigh);
				lngBoosterCount += lngAmount;
			}
			else
			{
				if (Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 1, 1) == "D")
				{
					Strings.MidSet(ref strLine, 22, 1, Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 1, 1));
					lngDecalCount += lngAmount;
				}
				else if (Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 3, 1) == "C")
				{
					Strings.MidSet(ref strLine, 22, 1, Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 3, 1));
					lngCombinationStickerCount += lngAmount;
				}
				else
				{
					Strings.MidSet(ref strLine, 22, 1, Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 2, 1));
					if (Strings.Left(rsInfo.Get_Fields_String("InventoryType"), 1) == "M")
					{
						lngMVR3Count += lngAmount;
					}
					else if (Strings.Left(rsInfo.Get_Fields_String("InventoryType"), 1) == "S")
					{
						if (Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 3, 1) == "S")
						{
							lngSingleStickerCount += lngAmount;
						}
						else
						{
							lngDoubleStickerCount += lngAmount;
						}
					}
					else
					{
						strClass = Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 4, 2);
						blnFound = false;
						for (counter = 0; counter <= MotorVehicle.Statics.intPlateCountCounter - 1; counter++)
						{
							if (strClass == MotorVehicle.Statics.pcPlateInfo[counter].Class)
							{
								MotorVehicle.Statics.pcPlateInfo[counter].AdjustmentCount += lngAmount;
								blnFound = true;
								break;
							}
						}
						if (!blnFound)
						{
							Array.Resize(ref MotorVehicle.Statics.pcPlateInfo, MotorVehicle.Statics.intPlateCountCounter + 1);
							MotorVehicle.Statics.pcPlateInfo[MotorVehicle.Statics.intPlateCountCounter].Class = strClass;
							MotorVehicle.Statics.pcPlateInfo[MotorVehicle.Statics.intPlateCountCounter].AdjustmentCount = lngAmount;
							MotorVehicle.Statics.intPlateCountCounter += 1;
						}
					}
				}
				Strings.MidSet(ref strLine, 23, 7 + Strings.Len(strLow) + Strings.Len(strHigh), Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 4, 2) + " " + strLow + " - " + strHigh);
			}
			// 
			Strings.MidSet(ref strLine, 45, 3, Strings.StrDup(3 - rsInfo.Get_Fields_String("OpID").Length, " ") + rsInfo.Get_Fields_String("OpID"));
			if (fecherFoundation.Strings.Trim(rsInfo.Get_Fields_String("reason")) == "")
			{
				if (Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 1, 1) == "S")
				{
					Strings.MidSet(ref strLine, 51, 27, "RECEIVED" + Strings.StrDup(19, " "));
					Strings.MidSet(ref strLine, 79, 1, Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 3, 1));
				}
				else
				{
					Strings.MidSet(ref strLine, 51, 28, "RECEIVED" + Strings.StrDup(20, " "));
				}
			}
			else
			{
				if (Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 1, 1) == "S")
				{
					if (rsInfo.Get_Fields_String("reason").Length >= 26)
					{
						Strings.MidSet(ref strLine, 51, 26, Strings.Mid(rsInfo.Get_Fields_String("reason"), 1, 26));
					}
					else
					{
						Strings.MidSet(ref strLine, 51, Strings.Len(rsInfo.Get_Fields_String("reason")), rsInfo.Get_Fields_String("reason"));
					}
					Strings.MidSet(ref strLine, 79, 1, Strings.Mid(rsInfo.Get_Fields_String("InventoryType"), 3, 1));
				}
				else
				{
					if (rsInfo.Get_Fields_String("reason").Length >= 28)
					{
						Strings.MidSet(ref strLine, 51, 28, Strings.Mid(rsInfo.Get_Fields_String("reason"), 1, 28));
					}
					else
					{
						Strings.MidSet(ref strLine, 51, Strings.Len(rsInfo.Get_Fields_String("reason")), rsInfo.Get_Fields_String("reason"));
					}
				}
			}
			rtf1.Text = rtf1.Text + strLine + "\r\n";
			LineCount += 1;
		}

		public void cmbAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAll.Text == "Print All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Print Selected")
			{
				optSelect_CheckedChanged(sender, e);
			}
		}

        private void frmReport_Resize(object sender, EventArgs e)
        {
            fraListOptions.CenterToContainer(this.ClientArea);
            fraInventoryOptions.CenterToContainer(this.ClientArea);
        }

        private void btnCreteBMVDataFiles_Click(object sender, EventArgs e)
        {
            string strReportDrive;

            strReportDrive = Path.Combine(FCFileSystem.Statics.UserDataFolder,"BMVReports");

	        if (!Directory.Exists(strReportDrive))
	        {
		        Directory.CreateDirectory(strReportDrive);
	        }
	        
	        frmReport.InstancePtr.MousePointer = MousePointerConstants.vbHourglass;

	        if (CreateDiskFile())
	        {
		        DisketteVerificationReport();
		        rptDiskVerification.InstancePtr.Unload();
		        rptDiskVerificationSummary.InstancePtr.Unload();
				frmReportViewer.InstancePtr.Init(rptDiskVerification.InstancePtr, showModal: true);
		        rtf1.Text = "";
		        CopyFileToDiskette();
	        }

	        frmReport.InstancePtr.Cursor = Wisej.Web.Cursors.Default;
	        return;
        }

		private void btnFullSetOfReports_Click(object sender, EventArgs e)
		{
			clsDRWrapper rsDel = new clsDRWrapper();
            var reportsPath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "BMVReports");
            if (!Directory.Exists(reportsPath))
            {
                Directory.CreateDirectory(reportsPath);
            }
			FCUtils.StartTask(this, () =>
			{
				List<string> batchReports = new List<string>();
				this.ShowWait();
				this.UpdateWait("Processing Data ...");
				AllFlag = true;
				rptCoverSheet.InstancePtr.Init(Convert.ToDateTime(cboStart.Text), Convert.ToDateTime(cboEnd.Text), batchReports: batchReports);
				rptCoverSheet.InstancePtr.Unload();
				CheckItCoverSheet:
				;
				if (MotorVehicle.JobComplete("rptCoverSheet"))
				{
					rtf1.Text = "";
				}
				else
				{
					goto CheckItCoverSheet;
				}

				strReportPrinting = "TS";
				strSummaryDetail.Value = "S";
				rsDel.Execute("DELETE FROM TownDetailSummary", "TWMV0000.vb1");
				this.UpdateWait("Processing Data 1 of 12");
				FillTownDetailSummaryTable();
				this.UpdateWait("Processing Data 2 of 12");
				TownSummary(batchReports: batchReports);
				strReportPrinting = "TD";
				strSummaryDetail.Value = "D";
				rsDel.Execute("DELETE FROM TownDetailSummary", "TWMV0000.vb1");
				this.UpdateWait("Processing Data 3 of 12");
				FillTownDetailSummaryTable();
				this.UpdateWait("Processing Data 4 of 12");
				TownDetail(batchReports: batchReports);
				strReportPrinting = "GC";
				this.UpdateWait("Processing Data 5 of 12");
				GiftCertVouchers(batchReports: batchReports);
				strReportPrinting = "TA";
				this.UpdateWait("Processing Data 6 of 12");
				TitleApplications(batchReports: batchReports);
				strReportPrinting = "EX";
				this.UpdateWait("Processing Data 7 of 12");
				ExceptionReport(batchReports: batchReports);
				strReportPrinting = "VD";
				this.UpdateWait("Processing Data 8 of 12");
				VoidedMVR3(batchReports: batchReports);
				strReportPrinting = "ET";
				this.UpdateWait("Processing Data 9 of 12");
				ExciseTaxOnly(batchReports: batchReports);
				strReportPrinting = "IR";
				this.UpdateWait("Processing Data 10 of 12");
				InventoryReport(batchReports: batchReports);
				strReportPrinting = "IA";
				this.UpdateWait("Processing Data 11 of 12");
				InventoryAdjustments(batchReports: batchReports);
				strReportPrinting = "UT";
				this.UpdateWait("Processing Data 12 of 12");
				UseTaxSummary(batchReports: batchReports);
				Global.Extensions.CombineAndPrintPDFs(batchReports);
				AllFlag = false;
				this.EndWait();
			});
			frmReport.InstancePtr.MousePointer = MousePointerConstants.vbDefault;
		}
	}
}
