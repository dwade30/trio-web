//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
    public partial class frmLeaseUseTax : BaseForm
    {
        public frmLeaseUseTax()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmLeaseUseTax InstancePtr
        {
            get
            {
                return (frmLeaseUseTax)Sys.GetInstance(typeof(frmLeaseUseTax));
            }
        }

        protected frmLeaseUseTax _InstancePtr = null;
        //=========================================================
        clsDRWrapper rs1 = new clsDRWrapper();
        string str1;
        int NewBackColor = ColorTranslator.ToOle(Color.Yellow);
        int OldBackColor;
        bool blnEditFlag;

        const string currencyFormat = "#,##0.00";

        private void chkRegistrant1_CheckedChanged(object sender, System.EventArgs e)
        {
            object ans;
            clsDRWrapper ctaRS = new clsDRWrapper();
            if (chkRegistrant1.CheckState == Wisej.Web.CheckState.Checked)
            {
                txtUTCPurchaserName.Text = frmDataInput.InstancePtr.txtRegistrant1.Text;
                txtUTCPurcahserAddress.Text = frmDataInput.InstancePtr.txtAddress1.Text;
                txtUTCPurchaserCity.Text = frmDataInput.InstancePtr.txtCity.Text;
                txtUTCPurchaserState.Text = frmDataInput.InstancePtr.txtState.Text;
                txtUTCPurchaserZip.Text = frmDataInput.InstancePtr.txtZip.Text;
            }
            else
            {
                txtUTCPurchaserName.Text = "";
                txtUTCPurcahserAddress.Text = "";
                txtUTCPurchaserCity.Text = "";
                txtUTCPurchaserState.Text = "";
                txtUTCPurchaserZip.Text = "";
            }
            txtPayment.Focus();
        }

        private void cmdReturn_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: ans As object	OnWrite(DialogResult)
            DialogResult ans;
            ans = MessageBox.Show("Would you like to save this Use Tax Form?", "Save Use Tax", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ans == DialogResult.No)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", false);
                MotorVehicle.Statics.NeedToPrintUseTax = false;
                Close();
                return;
            }
            else
            {
                SaveInfo();
            }
        }

        public void cmdReturn_Click()
        {
            cmdReturn_Click(cmdReturn, new System.EventArgs());
        }

        private void frmLeaseUseTax_Activated(object sender, System.EventArgs e)
        {
            if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
            {
                return;
            }
            blnEditFlag = false;
            txtRate.Text = DateTime.Today.ToOADate() >= FCConvert.ToDateTime("10/1/2013").ToOADate()
                ? "0.055"
                : "0.050";

            var reg1PartyId = FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(frmDataInput.InstancePtr.txtReg1PartyID.Text)));
            var reg2PartyId = FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(frmDataInput.InstancePtr.txtReg2PartyID.Text)));
            var reg3PartyId = FCConvert.ToInt32(Conversion.Val(fecherFoundation.Strings.Trim(frmDataInput.InstancePtr.txtReg3PartyID.Text)));

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone") == true)
            {
                if (MotorVehicle.Statics.lngUTCAddNewID != 0)
                {
                    FillScreen();
                    cmbUseTaxPrint.Text = MotorVehicle.Statics.NeedToPrintUseTax ? "This Form NEEDS to be printed" : "This Form does NOT need to be printed";
                }
                else
                {
                    SetSellerName(reg1PartyId, reg2PartyId, reg3PartyId);
                }
            }
            else
            {
                SetSellerName(reg1PartyId, reg2PartyId, reg3PartyId);
            }
            cmbUseTaxPrint.Text = "This Form NEEDS to be printed";
        }

        private void SetSellerName(int reg1PartyId, int reg2PartyId, int reg3PartyId)
        {
            switch (frmDataInput.InstancePtr.txtReg1LR.Text)
            {
                case "R":
                    txtUTCSellerName.Text = MotorVehicle.GetPartyName(reg1PartyId, true);

                    break;
                default:
                    {
                        switch (frmDataInput.InstancePtr.txtReg2LR.Text)
                        {
                            case "R" when frmDataInput.InstancePtr.txtReg2ICM.Text != "N":
                                txtUTCSellerName.Text = MotorVehicle.GetPartyName(reg2PartyId, true);

                                break;
                            default:
                                {
                                    switch (frmDataInput.InstancePtr.txtReg3LR.Text)
                                    {
                                        case "R" when frmDataInput.InstancePtr.txtReg3ICM.Text != "N":
                                            txtUTCSellerName.Text = MotorVehicle.GetPartyName(reg3PartyId, true);

                                            break;
                                    }

                                    break;
                                }
                        }

                        break;
                    }
            }
        }

        private void frmLeaseUseTax_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                Close();
            }
            else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
            {
                if (FCGlobal.Screen.ActiveControl is Global.T2KOverTypeBox || FCGlobal.Screen.ActiveControl is FCTextBox)
                {
                    KeyAscii = KeyAscii - 32;
                }
            }
            else if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                Support.SendKeys("{TAB}", false);
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmLeaseUseTax_Load(object sender, System.EventArgs e)
        {

            TopOriginal = FCConvert.ToInt32((FCGlobal.Screen.HeightOriginal - HeightOriginal) / 2.0);
            LeftOriginal = FCConvert.ToInt32((FCGlobal.Screen.WidthOriginal - WidthOriginal) / 2.0);
            MotorVehicle.Statics.NeedToPrintUseTax = false;
            modGNBas.GetWindowSize(this);
            modGlobalFunctions.SetTRIOColors(this, false);
        }

        private void mnuFileExit_Click(object sender, System.EventArgs e)
        {
            cmdReturn_Click();
        }

        private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
        {
            SaveInfo();
        }

        #region Highlight/Unhighlight Screen Fields plus some validation or tax recalc calls


        private void txtA1_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtA1);
        }

        private void txtA1_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtA1);
        }

        private void txtB1_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtB1);
        }

        private void txtB1_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtB1);
        }

        private void txtB2_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtB2);
        }

        private void txtB2_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtB2);
        }

        private void txtB3_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtB3);
        }

        private void txtB3_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtB3);
        }

        private void txtC1_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtC1);
        }

        private void txtC1_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtC1);
        }

        private void txtC2_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtC2);
        }

        private void txtC2_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtC2);
        }

        private void txtC2_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            CalculateTax();
        }

        private void txtD1_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtD1);
        }

        private void txtD1_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtD1);
        }

        private void txtPayment_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtPayment);
        }

        private void txtPayment_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtPayment);
            CalculateTax();
        }

        private void txtPayments_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtPayments);
        }

        private void txtPayments_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtPayments);
            CalculateTax();
        }

        private void txtDownPayment_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtDownPayment);
        }

        private void txtDownPayment_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtDownPayment);
            CalculateTax();
        }

        private void txtRate_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtRate);
        }

        private void txtRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && KeyAscii != Keys.Delete)
            {
                KeyAscii = (Keys)0;
            }
            else if (KeyAscii == Keys.Delete)
            {
                if (Strings.InStr(1, txtRate.Text, ".", CompareConstants.vbBinaryCompare) > 0)
                {
                    KeyAscii = (Keys)0;
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtRate_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtRate);
        }

        private void txtRate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Conversion.Val(txtUTCPrice.Text) != 0)
            {
                CalculateTax();
            }
        }

        private void txtUTCAllowance_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCAllowance);
        }

        private void txtUTCAllowance_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCAllowance);
        }

        private void txtUTCAllowance_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Conversion.Val(txtUTCAllowance.Text) != 0)
            {
                CalculateTax();
            }
        }

        private void txtUTCDate_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCDate);
        }

        private void txtUTCDate_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCDate);
        }

        private void txtUTCExempt_TextChanged(object sender, System.EventArgs e)
        {
            var exemptCode = fecherFoundation.Strings.UCase(txtUTCExempt.Text);
            txtUTCExempt.Text = exemptCode;
            switch (exemptCode)
            {
                case "A":
                    lblA1.Visible = true;
                    txtA1.Visible = true;
                    lblB1.Visible = false;
                    lblB2.Visible = false;
                    lblB3.Visible = false;
                    txtB1.Visible = false;
                    txtB2.Visible = false;
                    txtB3.Visible = false;
                    lblC1.Visible = false;
                    lblC2.Visible = false;
                    txtC1.Visible = false;
                    txtC2.Visible = false;
                    lblD1.Visible = false;
                    txtD1.Visible = false;
                    CalculateTax();
                    txtA1.Focus();

                    break;
                case "B":
                    lblA1.Visible = false;
                    txtA1.Visible = false;
                    lblB1.Visible = true;
                    lblB2.Visible = true;
                    lblB3.Visible = true;
                    txtB1.Visible = true;
                    txtB2.Visible = true;
                    txtB3.Visible = true;
                    lblC1.Visible = false;
                    lblC2.Visible = false;
                    txtC1.Visible = false;
                    txtC2.Visible = false;
                    lblD1.Visible = false;
                    txtD1.Visible = false;
                    CalculateTax();
                    txtB1.Focus();

                    break;
                case "C":
                    lblA1.Visible = false;
                    txtA1.Visible = false;
                    lblB1.Visible = false;
                    lblB2.Visible = false;
                    lblB3.Visible = false;
                    txtB1.Visible = false;
                    txtB2.Visible = false;
                    txtB3.Visible = false;
                    lblC1.Visible = true;
                    lblC2.Visible = true;
                    txtC1.Visible = true;
                    txtC2.Visible = true;
                    lblD1.Visible = false;
                    txtD1.Visible = false;
                    CalculateTax();
                    txtC1.Focus();

                    break;
                case "D":
                    lblA1.Visible = false;
                    txtA1.Visible = false;
                    lblB1.Visible = false;
                    lblB2.Visible = false;
                    lblB3.Visible = false;
                    txtB1.Visible = false;
                    txtB2.Visible = false;
                    txtB3.Visible = false;
                    lblC1.Visible = false;
                    lblC2.Visible = false;
                    txtC1.Visible = false;
                    txtC2.Visible = false;
                    lblD1.Visible = true;
                    txtD1.Visible = true;
                    CalculateTax();
                    txtD1.Focus();

                    break;
                default:
                    txtUTCExempt.Text = "N";
                    lblA1.Visible = false;
                    txtA1.Visible = false;
                    lblB1.Visible = false;
                    lblB2.Visible = false;
                    lblB3.Visible = false;
                    txtB1.Visible = false;
                    txtB2.Visible = false;
                    txtB3.Visible = false;
                    lblC1.Visible = false;
                    lblC2.Visible = false;
                    txtC1.Visible = false;
                    txtC2.Visible = false;
                    lblD1.Visible = false;
                    txtD1.Visible = false;
                    CalculateTax();
                    txtUTCSellerName.Focus();

                    break;
            }
        }

        private void txtUTCExempt_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCExempt);
            txtUTCExempt.SelectionStart = 0;
            txtUTCExempt.SelectionLength = 1;
        }

        private void txtUTCExempt_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCExempt);
        }

        private void txtUTCNetAmount_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCNetAmount);
        }

        private void txtUTCNetAmount_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCNetAmount);
        }

        private void txtUTCNetAmount_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Conversion.Val(txtUTCNetAmount.Text) != 0)
            {
                CalculateTax();
            }
        }

        private void txtUTCPrice_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCPrice);
        }

        private void txtUTCPrice_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCPrice);
        }

        private void txtUTCPrice_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Conversion.Val(txtUTCPrice.Text) != 0)
            {
                CalculateTax();
            }
        }

        private void txtUTCPurcahserAddress_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCPurcahserAddress);
        }

        private void txtUTCPurcahserAddress_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCPurcahserAddress);
        }

        private void txtUTCPurchaserCity_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCPurchaserCity);
        }

        private void txtUTCPurchaserCity_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCPurchaserCity);
        }

        private void txtUTCPurchaserName_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCPurchaserName);
        }

        private void txtUTCPurchaserName_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCPurchaserName);
        }

        private void txtUTCPurchaserState_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCPurchaserState);
        }

        private void txtUTCPurchaserState_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode != Keys.Left && KeyCode != Keys.Right)
            {
                if (fecherFoundation.Strings.Trim(txtUTCPurchaserState.Text).Length >= 2)
                {
                    txtUTCPurchaserZip.Focus();
                }
            }
        }

        private void txtUTCPurchaserState_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCPurchaserState);
        }

        private void txtUTCPurchaserZip_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCPurchaserZip);
        }

        private void txtUTCPurchaserZip_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCPurchaserZip);
        }

        private void txtUTCPurchaserZip4_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCPurchaserZip4);
        }

        private void txtUTCPurchaserZip4_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCPurchaserZip4);
        }

        private void txtUTCSellerAddress_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCSellerAddress);
        }

        private void txtUTCSellerAddress_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCSellerAddress);
        }

        private void txtUTCSellerCity_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCSellerCity);
        }

        private void txtUTCSellerCity_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCSellerCity);
        }

        private void txtUTCSellerName_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCSellerName);
        }

        private void txtUTCSellerName_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCSellerName);
        }

        private void txtUTCSellerState_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCSellerState);
        }

        private void txtUTCSellerState_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCSellerState);
        }

        private void txtUTCSellerZip_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCSellerZip);
        }

        private void txtUTCSellerZip_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCSellerZip);
        }

        private void txtUTCSellerZip4_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCSellerZip4);
        }

        private void txtUTCSellerZip4_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCSellerZip4);
        }

        private void txtUTCSSN_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCSSN);
        }

        private void txtUTCSSN_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCSSN);
        }

        private void txtUTCUseTax_Enter(object sender, System.EventArgs e)
        {
            HighlightField(txtUTCUseTax);
        }

        private void txtUTCUseTax_Leave(object sender, System.EventArgs e)
        {
            UnhighlightField(txtUTCUseTax);
        }

        #endregion

        private void FillScreen()
        {
            str1 = $"SELECT * FROM UseTax WHERE ID = {FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID)}";
            rs1.OpenRecordset(str1);
            fecherFoundation.Information.Err().Clear();
            if (HasRecords())
            {
                rs1.MoveLast();
                rs1.MoveFirst();

                txtUTCDate.Text = Strings.Format(rs1.Get_Fields("TradedDate"), "MM/dd/yyyy");

                FillSellerFields();
                FillPaymentFields();
                FillPriceFields();
                txtUTCUseTax.Text = Strings.Format(rs1.Get_Fields_Decimal("TaxAmount"), currencyFormat);
                FillExemptFields();

                CalculateTax();

                FillPurchaserFields();
            }
            fecherFoundation.Information.Err().Clear();
        }

        #region FillScreen Methods

        private void FillPurchaserFields()
        {
            txtUTCSSN.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserSSN"));
            txtUTCPurchaserName.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserFName"));

            // tromvs-98 07/2018
            txtUTCPurcahserAddress.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserAddress"));
            txtUTCPurchaserCity.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserCity"));
            txtUTCPurchaserState.Text = FCConvert.ToString(rs1.Get_Fields_String("PurchaserState"));

            if (fecherFoundation.FCUtils.IsNull(rs1.Get_Fields_String("PurchaserZipAndZip4")) == false)
            {
                var purchaserZipPlus4 = FCConvert.ToString(rs1.Get_Fields_String("PurchaserZipAndZip4"));

                if (purchaserZipPlus4.Length <= 5)
                {
                    txtUTCPurchaserZip.Text = purchaserZipPlus4;
                }
                else
                {
                    txtUTCPurchaserZip.Text = Strings.Left(purchaserZipPlus4, Strings.InStr(1, purchaserZipPlus4, "-", CompareConstants.vbBinaryCompare) - 1);
                    txtUTCPurchaserZip4.Text = Strings.Right(purchaserZipPlus4, Strings.InStrRev(purchaserZipPlus4, "-", -1, CompareConstants.vbTextCompare /*?*/) - 2);
                }
            }
        }

        private void FillExemptFields()
        {
            var exemptCode = FCConvert.ToString(rs1.Get_Fields_String("ExemptCode"));
            txtUTCExempt.Text = exemptCode;

            switch (exemptCode)
            {
                case "A":
                    txtA1.Visible = true;
                    txtA1.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptNumber"));

                    break;
                case "B":
                    txtB1.Visible = true;
                    txtB2.Visible = true;
                    txtB3.Visible = true;
                    txtB1.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptPreviousState"));
                    txtB2.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptRegistrationNumber"));
                    txtB3.Text = Strings.Format(rs1.Get_Fields("ExemptDate"), "MM/dd/yyyy");

                    break;
                case "C":
                    txtC1.Visible = true;
                    txtC2.Visible = true;
                    txtC1.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptState"));
                    txtC2.Text = Strings.Format(rs1.Get_Fields_Decimal("ExemptAmountPaid"), currencyFormat);

                    break;
                case "D":
                    txtD1.Visible = true;
                    txtD1.Text = FCConvert.ToString(rs1.Get_Fields_String("ExemptVACNumber"));

                    break;
                default:
                    // Do Nothing
                    break;
            }
        }

        private void FillPriceFields()
        {
            var purchasePrice = rs1.Get_Fields_Decimal("PurchasePrice");
            txtUTCPrice.Text = Strings.Format(purchasePrice, currencyFormat);
            var allowance = rs1.Get_Fields_Decimal("Allowance");
            txtUTCAllowance.Text = Strings.Format(allowance, currencyFormat);
            txtUTCNetAmount.Text = Strings.Format(allowance + purchasePrice, currencyFormat);
            txtRate.Text = FCConvert.ToString(rs1.Get_Fields("Rate"));
        }

        private void FillPaymentFields()
        {
            txtPayment.Text = Strings.Format(rs1.Get_Fields_Double("LeasePaymentAmount"), currencyFormat);
            txtDownPayment.Text = Strings.Format(rs1.Get_Fields_Double("LeaseDownPayment"), currencyFormat);
            txtPayments.Text = Strings.Format(rs1.Get_Fields_Int32("NumberOfPayments"), "#,##0");
        }

        private void FillSellerFields()
        {
            txtUTCSellerName.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerName"));
            txtUTCSellerAddress.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerAddress1"));
            txtUTCSellerCity.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerCity"));
            txtUTCSellerState.Text = FCConvert.ToString(rs1.Get_Fields_String("SellerState"));
            var sellerZipPlus4 = rs1.Get_Fields_String("SellerZipAndZip4");

            if (fecherFoundation.FCUtils.IsNull(sellerZipPlus4) == false)
            {
                var sellerZipPlus4String = FCConvert.ToString(sellerZipPlus4);

                if (sellerZipPlus4String.Length <= 5)
                {
                    txtUTCSellerZip.Text = sellerZipPlus4String;
                }
                else
                {
                    txtUTCSellerZip.Text = Strings.Left(sellerZipPlus4String, Strings.InStr(1, sellerZipPlus4String, "-", CompareConstants.vbBinaryCompare) - 1);
                    txtUTCSellerZip4.Text = Strings.Right(sellerZipPlus4String, Strings.InStrRev(sellerZipPlus4String, "-", -1, CompareConstants.vbTextCompare /*?*/) - 2);
                }
            }
        }

        #endregion

        private bool HasRecords()
        {
            return rs1.EndOfFile() != true && rs1.BeginningOfFile() != true;
        }

        private void CalculateTax()
        {
            const string currencyFormat = "#,###.00";

            double dblPrice = 0;
            var lngAllowance = 0;
            double dblNetPrice;
            double dblRate = 0;
            double dblTax = 0;
            double dblDownPayment = 0;
            double dblPayments = 0;
            var intPayments = 0;
            if (Conversion.Val(txtPayment.Text) != 0) dblPayments = Convert.ToDouble(txtPayment.Text);
            if (Conversion.Val(txtPayments.Text) != 0) intPayments = Convert.ToInt16(Convert.ToDouble(txtPayments.Text));
            if (Conversion.Val(txtDownPayment.Text) != 0) dblDownPayment = Convert.ToDouble(txtDownPayment.Text);
            txtUTCPrice.Text = Strings.Format((dblPayments * intPayments) + dblDownPayment, "#,##0.00");
            if (Conversion.Val(txtUTCPrice.Text) != 0) dblPrice = Convert.ToDouble(txtUTCPrice.Text);
            if (Conversion.Val(txtUTCAllowance.Text) != 0) lngAllowance = Convert.ToInt32(Convert.ToDouble(txtUTCAllowance.Text));
            dblNetPrice = dblPrice + lngAllowance;
            if (dblNetPrice < 0)
            {
                dblNetPrice = 0;
            }

            const string formatString = "@@@@@@@@@@";

            txtUTCNetAmount.Text = Strings.Format(Strings.Format(dblNetPrice, currencyFormat), formatString);
            if (Convert.ToDecimal(Strings.Right(txtRate.Text, 2)) != 0)
            {
                dblRate = FCConvert.ToDouble(txtRate.Text);
            }
            else if (Convert.ToDecimal(txtRate.Text) != 0)
            {
                dblRate = FCConvert.ToDouble(txtRate.Text);
            }
            if (txtUTCExempt.Text != "N" && fecherFoundation.Strings.Trim(txtUTCExempt.Text) != "")
            {
                const string zeroCurrency = "0.00";

                if (txtUTCExempt.Text != "C")
                {
                    txtUTCUseTax.Text = zeroCurrency;
                }
                else
                {
                    if (fecherFoundation.Strings.Trim(txtC2.Text) != "")
                    {
                        var rateTimesNetPrice = FCConvert.ToDecimal(dblRate * dblNetPrice);
                        txtUTCUseTax.Text = rateTimesNetPrice > Conversion.CCur(txtC2.Text)
                            ? Strings.Format(Strings.Format(rateTimesNetPrice - Conversion.CCur(txtC2.Text), currencyFormat), formatString)
                            : zeroCurrency;
                    }
                    else
                    {
                        txtUTCUseTax.Text = zeroCurrency;
                    }
                }
            }
            else
            {
                if (dblRate != 0)
                {
                    txtUTCUseTax.Text = Strings.Format(Strings.Format(dblRate * dblNetPrice, currencyFormat), formatString);
                }
            }
        }

        private void frmLeaseUseTax_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F10)
            {
                KeyCode = (Keys)0;
            }
        }

        private void SaveInfo()
        {
            var rs1 = new clsDRWrapper();

            try
            {
                fecherFoundation.Information.Err().Clear();

                if (!IsReadyToSave()) return;

                MousePointer = MousePointerConstants.vbHourglass;

                rs1.OpenRecordset($"SELECT * FROM UseTax WHERE ID = {FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID)}");

                AddNewOrEdit(rs1);

                // set the fields on the record
                SetDateFields(rs1);
                SetSellerFields(rs1);
                SetLeasePaymentFields(rs1);
                SetPriceFields(rs1);
                var useTaxAmt = FCConvert.ToDecimal(txtUTCUseTax.Text);
                rs1.Set_Fields("TaxAmount", useTaxAmt != 0 ? useTaxAmt : 0);
                SetExemptFields(rs1);
                SetPurchaserFields(rs1);

                // set some static fields
                MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", true);
                MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", useTaxAmt != 0 ? useTaxAmt : 0);

                // update the record
                rs1.Update();

                // set more static fields based on the result
                MotorVehicle.Statics.lngUTCAddNewID = FCConvert.ToInt32(rs1.Get_Fields_Int32("ID"));
                MotorVehicle.Statics.NeedToPrintUseTax = cmbUseTaxPrint.Text == "This Form NEEDS to be printed";

                Close();
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show("There was an error processing this Use Tax Certificate.  (Error number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " was encountered, " + fecherFoundation.Information.Err(ex).Description + ".", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                fecherFoundation.Information.Err(ex).Clear();
            }
            finally
            {
                rs1.DisposeOf();
                Cursor = Wisej.Web.Cursors.Default;
            }
        }

        #region SaveInfo Methods

        private static void AddNewOrEdit(clsDRWrapper rs1)
        {
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone") == true)
            {
                if (MotorVehicle.Statics.lngUTCAddNewID != 0)
                {
                    rs1.Edit();
                }
                else
                {
                    rs1.AddNew();
                }
            }
            else
            {
                rs1.AddNew();
                MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", true);
            }
        }

        private bool IsReadyToSave()
        {
            DialogResult ans;

            if ((txtUTCExempt.Text == "" || txtUTCExempt.Text == "C")
                && (txtUTCPrice.Text == "" || Conversion.Val(txtUTCPrice.Text) == 0))
            {
                MessageBox.Show("You must enter a Price or an Exemption Code before you can save this information.", "Invalid Price", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtUTCPrice.Focus();

                return false;
            }

            if (txtUTCExempt.Text != "B" && !Information.IsDate(txtUTCDate.Text))
            {
                MessageBox.Show("You must enter a valid lease date before you may save this information.", "Invalid Lease Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtUTCDate.Focus();

                return false;
            }

            if (fecherFoundation.Strings.Trim(txtUTCSellerName.Text) == "" || fecherFoundation.Strings.Trim(txtUTCSellerAddress.Text) == "" || fecherFoundation.Strings.Trim(txtUTCSellerCity.Text) == "" || fecherFoundation.Strings.Trim(txtUTCSellerState.Text) == "" || fecherFoundation.Strings.Trim(txtUTCSellerZip.Text) == "")
            {
                ans = MessageBox.Show("Some lessor information has not been filled out.  Do you wish to continue?", "Missing Lessor Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (ans != DialogResult.Yes)
                {
                    SetSellerFocus();

                    return false;
                }
            }

            return true;
        }

        private void SetPriceFields(clsDRWrapper rs1)
        {
            rs1.Set_Fields("PurchasePrice", FCConvert.ToDecimal(txtUTCPrice.Text) != 0
                               ? FCConvert.ToInt32(FCConvert.ToDouble(txtUTCPrice.Text))
                               : 0);

            rs1.Set_Fields("Allowance", FCConvert.ToDecimal(txtUTCAllowance.Text) != 0
                               ? FCConvert.ToInt32(FCConvert.ToDouble(txtUTCAllowance.Text))
                               : 0);

            rs1.Set_Fields("Rate", FCConvert.ToDecimal(txtRate.Text) != 0
                               ? FCConvert.ToDecimal(txtRate.Text)
                               : 0);
        }

        private void SetLeasePaymentFields(clsDRWrapper rs1)
        {
            if (FCConvert.ToDecimal(txtPayment.Text) != 0)
            {
                rs1.Set_Fields("LeasePaymentAmount", FCConvert.ToDouble(txtPayment.Text));
            }
            else
            {
                rs1.Set_Fields("LeasePaymentAmount", 0);
            }

            rs1.Set_Fields("NumberOfPayments", FCConvert.ToDecimal(txtPayments.Text) != 0
                               ? FCConvert.ToInt32(FCConvert.ToDouble(txtPayments.Text))
                               : 0);

            if (FCConvert.ToDecimal(txtDownPayment.Text) != 0)
            {
                rs1.Set_Fields("LeaseDownPayment", FCConvert.ToDouble(txtDownPayment.Text));
            }
            else
            {
                rs1.Set_Fields("LeaseDownPayment", 0);
            }
        }

        private void SetDateFields(clsDRWrapper rs1)
        {
            if (Information.IsDate(txtUTCDate.Text)) rs1.Set_Fields("TradedDate", Strings.Format(txtUTCDate.Text, "MM/dd/yyyy"));

            rs1.Set_Fields("DateUTCProcessed", Strings.Format(DateTime.Now, "MM/dd/yyyy"));
        }

        private void SetSellerFields(clsDRWrapper rs1)
        {
            rs1.Set_Fields("SellerName", txtUTCSellerName.Text);
            rs1.Set_Fields("SellerAddress1", txtUTCSellerAddress.Text);
            rs1.Set_Fields("SellerCity", txtUTCSellerCity.Text);
            rs1.Set_Fields("SellerState", txtUTCSellerState.Text);

            if (fecherFoundation.Strings.Trim(txtUTCSellerZip4.Text) == "")
            {
                rs1.Set_Fields("SellerZipAndZip4", txtUTCSellerZip.Text);
            }
            else
            {
                rs1.Set_Fields("SellerZipAndZip4", txtUTCSellerZip.Text + "-" + txtUTCSellerZip4.Text);
            }
        }

        private void SetPurchaserFields(clsDRWrapper rs1)
        {
            rs1.Set_Fields("PurchaserSSN", txtUTCSSN.Text);
            rs1.Set_Fields("PurchaserFName", txtUTCPurchaserName.Text);

            // tromvs-98 07/2018 save as FName
            rs1.Set_Fields("PurchaserAddress", txtUTCPurcahserAddress.Text);
            rs1.Set_Fields("PurchaserCity", txtUTCPurchaserCity.Text);
            rs1.Set_Fields("PurchaserState", txtUTCPurchaserState.Text);

            if (fecherFoundation.Strings.Trim(txtUTCPurchaserZip4.Text) == "")
            {
                rs1.Set_Fields("PurchaserZipAndZip4", txtUTCPurchaserZip.Text);
            }
            else
            {
                rs1.Set_Fields("PurchaserZipAndZip4", txtUTCPurchaserZip.Text + "-" + txtUTCPurchaserZip4.Text);
            }
        }

        private void SetExemptFields(clsDRWrapper rs1)
        {
            rs1.Set_Fields("ExemptCode", txtUTCExempt.Text);

            switch (txtUTCExempt.Text)
            {
                case "A":
                    rs1.Set_Fields("ExemptNumber", txtA1.Text);

                    break;
                case "B":
                    {
                        rs1.Set_Fields("ExemptPreviousState", txtB1.Text);
                        rs1.Set_Fields("ExemptRegistrationNumber", txtB2.Text);
                        if (Information.IsDate(Strings.Format(txtB3.Text, "MM/dd/yyyy"))) rs1.Set_Fields("ExemptDate", Strings.Format(txtB3.Text, "MM/dd/yyyy"));

                        break;
                    }
                case "C":
                    rs1.Set_Fields("ExemptState", txtC1.Text);
                    rs1.Set_Fields("ExemptAmountPaid", FCConvert.ToDecimal(txtC2.Text));

                    break;
                case "D":
                    rs1.Set_Fields("ExemptVACNumber", txtD1.Text);

                    break;
                default:
                    // Do Nothing
                    break;
            }
        }

        private void SetSellerFocus()
        {
            if (fecherFoundation.Strings.Trim(txtUTCSellerName.Text) == "")
            {
                txtUTCSellerName.Focus();
            }
            else
                if (fecherFoundation.Strings.Trim(txtUTCSellerAddress.Text) == "")
            {
                txtUTCSellerAddress.Focus();
            }
            else
                    if (fecherFoundation.Strings.Trim(txtUTCSellerCity.Text) == "")
            {
                txtUTCSellerCity.Focus();
            }
            else
                        if (fecherFoundation.Strings.Trim(txtUTCSellerState.Text) == "")
            {
                txtUTCSellerState.Focus();
            }
            else
            {
                txtUTCSellerZip.Focus();
            }
        }

        #endregion

        private void HighlightField(Control ctl)
        {
            OldBackColor = ColorTranslator.ToOle(ctl.BackColor);
            ctl.BackColor = ColorTranslator.FromOle(NewBackColor);
        }

        private void UnhighlightField(Control ctl)
        {
            ctl.BackColor = ColorTranslator.FromOle(OldBackColor);
        }

    }
}
