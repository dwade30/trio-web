//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTAgentSummary.
	/// </summary>
	public partial class rptLTTAgentSummary : BaseSectionReport
	{
		public rptLTTAgentSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Agent Summary";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptLTTAgentSummary_ReportEnd;
		}

        private void RptLTTAgentSummary_ReportEnd(object sender, EventArgs e)
        {
			rsTitles.DisposeOf();
            rsAM.DisposeOf();
            rs.DisposeOf();
            rsUseTax.DisposeOf();

		}

        public static rptLTTAgentSummary InstancePtr
		{
			get
			{
				return (rptLTTAgentSummary)Sys.GetInstance(typeof(rptLTTAgentSummary));
			}
		}

		protected rptLTTAgentSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLTTAgentSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngLTTLargeUnits;
		int lngLTTSmallUnits;
		int lngExtensionUnits;
		int lngTransfer25YearUnits;
		int lngTransferLargeUnits;
		int lngTransferSmallUnits;
		int lngLostPlateUnits;
		int lngDuplicateUnits;
		int lngCorrectionUnits;
		int lngTitle25YearUnits;
		int lngTitleUnits;
		int lngRushTitleUnits;
		int lngSalesTaxPaidUnits;
		int lngSalesTaxNoFeeUnits;
		int lngTotalUnits;
		int intPageNumber = 0;
		// vbPorter upgrade warning: curLTTLargeAmount As Decimal	OnWrite(int, Decimal)
		Decimal curLTTLargeAmount;
		// vbPorter upgrade warning: curLTTSmallAmount As Decimal	OnWrite(int, Decimal)
		Decimal curLTTSmallAmount;
		// vbPorter upgrade warning: curExtensionAmount As Decimal	OnWrite(int, Decimal)
		Decimal curExtensionAmount;
		// vbPorter upgrade warning: curTransfer25YearAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTransfer25YearAmount;
		// vbPorter upgrade warning: curTransferLargeAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTransferLargeAmount;
		// vbPorter upgrade warning: curTransferSmallAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTransferSmallAmount;
		// vbPorter upgrade warning: curLostPlateAmount As Decimal	OnWrite(int, Decimal)
		Decimal curLostPlateAmount;
		// vbPorter upgrade warning: curDuplicateAmount As Decimal	OnWrite(int, Decimal)
		Decimal curDuplicateAmount;
		// vbPorter upgrade warning: curCorrectionAmount As Decimal	OnWrite
		Decimal curCorrectionAmount;
		// vbPorter upgrade warning: curTitle25YearAmount As Decimal	OnWrite
		Decimal curTitle25YearAmount;
		// vbPorter upgrade warning: curTitleAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTitleAmount;
		// vbPorter upgrade warning: curRushTitleAmount As Decimal	OnWrite(int, Decimal)
		Decimal curRushTitleAmount;
		// vbPorter upgrade warning: curSalesTaxPaidAmount As Decimal	OnWrite(int, Decimal)
		Decimal curSalesTaxPaidAmount;
		// vbPorter upgrade warning: curSalesTaxNoFeeAmount As Decimal	OnWrite
		Decimal curSalesTaxNoFeeAmount;
		// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(int, Decimal)
		Decimal curTotalAmount;
        Decimal curDuplicateFee;
		clsDRWrapper rsTitles = new clsDRWrapper();
		clsDRWrapper rsAM = new clsDRWrapper();
		clsDRWrapper rs = new clsDRWrapper();
		int lngPK1;
		int lngPK2;
		clsDRWrapper rsUseTax = new clsDRWrapper();

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            clsDRWrapper rsDefaults = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			if (frmReportLongTerm.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM LongTermTrailerRegistrations WHERE Status <> 'V' AND PeriodCloseoutID < 1";
				rsTitles.OpenRecordset("SELECT * from LongTermTitleApplications WHERE PeriodCloseoutID < 1");
				rsUseTax.OpenRecordset("SELECT * FROM UseTax WHERE ID IN (SELECT DISTINCT UseTaxID FROM LongTermTrailerRegistrations WHERE Status <> 'V' AND PeriodCloseoutID < 1 AND UseTaxDone = 1 AND SalesTax > 0)");
			}
			else
			{
				MotorVehicle.Statics.strSql = "SELECT * FROM PeriodCloseoutLongTerm WHERE IssueDate BETWEEN '" + frmReportLongTerm.InstancePtr.cboStart.Text + "' AND '" + frmReportLongTerm.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(MotorVehicle.Statics.strSql);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.MoveFirst();
					lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				}
				MotorVehicle.Statics.strSql = "SELECT * FROM LongTermTrailerRegistrations WHERE Status <> 'V' AND PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2);
				rsTitles.OpenRecordset("SELECT * from LongTermTitleApplications WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2));
				rsUseTax.OpenRecordset("SELECT * FROM UseTax WHERE ID IN (SELECT DISTINCT UseTaxID FROM LongTermTrailerRegistrations WHERE Status <> 'V' AND UseTaxDone = 1 AND SalesTax > 0 AND PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + ")");
			}
            rsDefaults.OpenRecordset("SELECT * FROM Defaults");
            if (!rsDefaults.EndOfFile() && !rsDefaults.BeginningOfFile())
            {
                curDuplicateFee = rsDefaults.Get_Fields("AgentFeeLongTermTrailerDupReg");
            }
            rsAM.OpenRecordset(MotorVehicle.Statics.strSql);
			if ((rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true) || (rsTitles.EndOfFile() != true && rsTitles.BeginningOfFile() != true))
			{
				CalculateTotals();
			}
		}

		private void CalculateTotals()
		{
			lngLTTLargeUnits = 0;
			lngLTTSmallUnits = 0;
			lngExtensionUnits = 0;
			lngTransfer25YearUnits = 0;
			lngTransferLargeUnits = 0;
			lngTransferSmallUnits = 0;
			lngLostPlateUnits = 0;
			lngDuplicateUnits = 0;
			lngCorrectionUnits = 0;
			lngTitle25YearUnits = 0;
			lngTitleUnits = 0;
			lngRushTitleUnits = 0;
			lngSalesTaxPaidUnits = 0;
			lngSalesTaxNoFeeUnits = 0;
			lngTotalUnits = 0;
			curLTTLargeAmount = 0;
			curLTTSmallAmount = 0;
			curExtensionAmount = 0;
			curTransfer25YearAmount = 0;
			curTransferLargeAmount = 0;
			curTransferSmallAmount = 0;
			curLostPlateAmount = 0;
			curDuplicateAmount = 0;
			curCorrectionAmount = 0;
			curTitle25YearAmount = 0;
			curTitleAmount = 0;
			curRushTitleAmount = 0;
			curSalesTaxPaidAmount = 0;
			curSalesTaxNoFeeAmount = 0;
			curTotalAmount = 0;
			do
			{
				string vbPorterVar = rsAM.Get_Fields_String("RegistrationType");
				if ((vbPorterVar == "NRR") || (vbPorterVar == "RRR"))
				{
					if (rsAM.Get_Fields("Extension") == true && FCConvert.ToString(rsAM.Get_Fields_String("RegistrationType")) == "RRR")
					{
						lngExtensionUnits += 1;
						curExtensionAmount += rsAM.Get_Fields("RegistrationFee");
					}
					else
					{
						if (FCConvert.ToInt32(rsAM.Get_Fields("NetWeight")) == 2000)
						{
							lngLTTSmallUnits += 1;
							curLTTSmallAmount += rsAM.Get_Fields("RegistrationFee");
						}
						else
						{
							lngLTTLargeUnits += 1;
							curLTTLargeAmount += rsAM.Get_Fields("RegistrationFee");
						}
					}
				}
				else if ((vbPorterVar == "NRT") || (vbPorterVar == "RRT"))
				{
					if (rsAM.Get_Fields_Boolean("TwentyFiveYearPlate") == true)
					{
						lngTransfer25YearUnits += 1;
						curTransfer25YearAmount += rsAM.Get_Fields("TransferFee");
					}
					else
					{
						if (FCConvert.ToInt32(rsAM.Get_Fields("NetWeight")) == 2000)
						{
							lngTransferSmallUnits += 1;
							curTransferSmallAmount += rsAM.Get_Fields("TransferFee");
						}
						else
						{
							lngTransferLargeUnits += 1;
							curTransferLargeAmount += rsAM.Get_Fields("TransferFee");
						}
					}
				}
				else if (vbPorterVar == "DPR")
				{
					lngDuplicateUnits += 1;
					curDuplicateAmount += 0;
				}
				else if ((vbPorterVar == "ECO") || (vbPorterVar == "ECR"))
				{
					lngCorrectionUnits += 1;
					// curCorrectionAmount = curCorrectionAmount + .Fields("StateFee")
				}
				else if (vbPorterVar == "LPS")
				{
					lngLostPlateUnits += 1;
					curLostPlateAmount += rsAM.Get_Fields("AgentFee");
				}
				// If .Fields("TitleDone") Then
				// If .Fields("TwentyFiveYearPlate") = True Then
				// lngTitle25YearUnits = lngTitle25YearUnits + 1
				// curTitle25YearAmount = curTitle25YearAmount + .Fields("TitleFee")
				// Else
				// lngTitleUnits = lngTitleUnits + 1
				// curTitleAmount = curTitleAmount + .Fields("TitleFee")
				// End If
				// If .Fields("RushTitleFee") <> 0 Then
				// lngRushTitleUnits = lngRushTitleUnits + 1
				// curRushTitleAmount = curRushTitleAmount + .Fields("RushTitleFee")
				// End If
				// End If
				if (FCConvert.ToBoolean(rsAM.Get_Fields_Boolean("UseTaxDone")))
				{
					if (FCConvert.ToInt32(rsAM.Get_Fields("SalesTax")) == 0)
					{
						lngSalesTaxNoFeeUnits += 1;
					}
				}
				rsAM.MoveNext();
			}
			while (rsAM.EndOfFile() != true);
			while (rsTitles.EndOfFile() != true)
			{
				lngTitleUnits += 1;
				// .Fields("Units")
				curTitleAmount += rsTitles.Get_Fields("Fee");
				if (rsTitles.Get_Fields_Decimal("RushFee") != 0)
				{
					lngRushTitleUnits += 1;
					curRushTitleAmount += rsTitles.Get_Fields_Decimal("RushFee");
				}
				rsTitles.MoveNext();
			}
			while (rsUseTax.EndOfFile() != true)
			{
				lngSalesTaxPaidUnits += 1;
				// .Fields("Units")
				curSalesTaxPaidAmount += rsUseTax.Get_Fields_Decimal("TaxAmount");
				rsUseTax.MoveNext();
			}
			curTotalAmount = curLTTLargeAmount + curLTTSmallAmount + curExtensionAmount + curTransfer25YearAmount + curTransferLargeAmount + curTransferSmallAmount + curLostPlateAmount + curDuplicateAmount + curCorrectionAmount + curTitle25YearAmount + curTitleAmount + curSalesTaxPaidAmount + curSalesTaxNoFeeAmount + curRushTitleAmount;
			lngTotalUnits = lngLTTLargeUnits + lngLTTSmallUnits + lngExtensionUnits + lngTransfer25YearUnits + lngTransferLargeUnits + lngTransferSmallUnits + lngLostPlateUnits + lngDuplicateUnits + lngCorrectionUnits + lngTitle25YearUnits + lngTitleUnits + lngSalesTaxPaidUnits + lngSalesTaxNoFeeUnits + lngRushTitleUnits;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldLTTLargeAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldExtensionAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransfer25YearAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransferLargeAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransferSmallAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldLostPlateAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldDuplicateAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldCorrectionAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTitle25YearsAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTitleAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldRushTitleAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldSalesTaxPaidAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldSalesTaxNoFeeAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalAmount As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTLargeUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldLTTSmallUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldExtensionsUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransfers25YearUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransfersLargeUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransfersSmallUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldLostPlateUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldDuplicateUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldCorrectionUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTitle25YearUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTitleUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldRushTitleUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldSalesTaxPaidUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldSalesTaxNoFeeUnits As object	OnWrite(string)
			// vbPorter upgrade warning: fldTotalUnits As object	OnWrite(string)
			fldLTTLargeAmount.Text = Strings.Format(curLTTLargeAmount, "#,##0.00");
			fldLTTSmallAmount.Text = Strings.Format(curLTTSmallAmount, "#,##0.00");
			fldExtensionAmount.Text = Strings.Format(curExtensionAmount, "#,##0.00");
			fldTransfer25YearAmount.Text = Strings.Format(curTransfer25YearAmount, "#,##0.00");
			fldTransferLargeAmount.Text = Strings.Format(curTransferLargeAmount, "#,##0.00");
			fldTransferSmallAmount.Text = Strings.Format(curTransferSmallAmount, "#,##0.00");
			fldLostPlateAmount.Text = Strings.Format(curLostPlateAmount, "#,##0.00");
			fldDuplicateAmount.Text = Strings.Format(curDuplicateAmount, "#,##0.00");
			fldCorrectionAmount.Text = Strings.Format(curCorrectionAmount, "#,##0.00");
			fldTitle25YearsAmount.Text = Strings.Format(curTitle25YearAmount, "#,##0.00");
			fldTitleAmount.Text = Strings.Format(curTitleAmount, "#,##0.00");
			fldRushTitleAmount.Text = Strings.Format(curRushTitleAmount, "#,##0.00");
			fldSalesTaxPaidAmount.Text = Strings.Format(curSalesTaxPaidAmount, "#,##0.00");
			fldSalesTaxNoFeeAmount.Text = Strings.Format(curSalesTaxNoFeeAmount, "#,##0.00");
			fldTotalAmount.Text = Strings.Format(curTotalAmount, "#,##0.00");
			fldLTTLargeUnits.Text = Strings.Format(lngLTTLargeUnits, "#,##0");
			fldLTTSmallUnits.Text = Strings.Format(lngLTTSmallUnits, "#,##0");
			fldExtensionsUnits.Text = Strings.Format(lngExtensionUnits, "#,##0");
			fldTransfers25YearUnits.Text = Strings.Format(lngTransfer25YearUnits, "#,##0");
			fldTransfersLargeUnits.Text = Strings.Format(lngTransferLargeUnits, "#,##0");
			fldTransfersSmallUnits.Text = Strings.Format(lngTransferSmallUnits, "#,##0");
			fldLostPlateUnits.Text = Strings.Format(lngLostPlateUnits, "#,##0");
			fldDuplicateUnits.Text = Strings.Format(lngDuplicateUnits, "#,##0");
			fldCorrectionUnits.Text = Strings.Format(lngCorrectionUnits, "#,##0");
			fldTitle25YearUnits.Text = Strings.Format(lngTitle25YearUnits, "#,##0");
			fldTitleUnits.Text = Strings.Format(lngTitleUnits, "#,##0");
			fldRushTitleUnits.Text = Strings.Format(lngRushTitleUnits, "#,##0");
			fldSalesTaxPaidUnits.Text = Strings.Format(lngSalesTaxPaidUnits, "#,##0");
			fldSalesTaxNoFeeUnits.Text = Strings.Format(lngSalesTaxNoFeeUnits, "#,##0");
			fldTotalUnits.Text = Strings.Format(lngTotalUnits, "#,##0");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: intPageNumber As object	OnWrite
			SubReport2.Report = new rptSubReportHeadingLongTerm();
			intPageNumber += 1;
			lblPage.Text = "Page " + intPageNumber;
		}

		private void rptLTTAgentSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLTTAgentSummary properties;
			//rptLTTAgentSummary.Caption	= "Agent Summary";
			//rptLTTAgentSummary.Left	= 0;
			//rptLTTAgentSummary.Top	= 0;
			//rptLTTAgentSummary.Width	= 20280;
			//rptLTTAgentSummary.Height	= 11115;
			//rptLTTAgentSummary.StartUpPosition	= 3;
			//rptLTTAgentSummary.SectionData	= "rptLTTAgentSummary.dsx":0000;
			//End Unmaped Properties
		}
	}
}
