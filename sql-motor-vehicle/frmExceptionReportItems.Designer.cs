//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmExceptionReportItems.
	/// </summary>
	partial class frmExceptionReportItems
	{
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCComboBox cmbReturnYes;
		public System.Collections.Generic.List<fecherFoundation.FCFrame> fraAdjustments;
		public fecherFoundation.FCFrame fraAdjustments_2;
		public fecherFoundation.FCComboBox lstNewSubClass;
		public fecherFoundation.FCComboBox lstNewClass;
		public fecherFoundation.FCComboBox lstOriginalSubClass;
		public fecherFoundation.FCComboBox lstOriginalClass;
		public fecherFoundation.FCTextBox txtPlateReason;
		public fecherFoundation.FCTextBox txtNewPlate;
		public fecherFoundation.FCTextBox txtOriginalPlate;
		public Global.T2KDateBox txtPlateAdjustmentDate;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label33;
		public fecherFoundation.FCLabel Label32;
		public fecherFoundation.FCLabel Label31;
		public fecherFoundation.FCLabel Label30;
		public fecherFoundation.FCLabel Label29;
		public fecherFoundation.FCLabel Label28;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCFrame fraAdjustments_0;
		public fecherFoundation.FCComboBox lstMonetaryCategories;
		public fecherFoundation.FCTextBox txtMonetaryReason;
		public fecherFoundation.FCTextBox txtMonetaryPlusMinus;
		public Global.T2KBackFillDecimal txtMonetaryAmount;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraAdjustments_1;
		public fecherFoundation.FCComboBox lstOriginalStickerClass;
		public fecherFoundation.FCComboBox lstNewMonthYear;
		public fecherFoundation.FCComboBox lstOriginalMonthYear;
		public fecherFoundation.FCTextBox txtMonthOrYear;
		public fecherFoundation.FCTextBox txtNewStickerNumber;
		public fecherFoundation.FCTextBox txtNewStickerSD;
		public fecherFoundation.FCTextBox txtOriginalStickerSD;
		public fecherFoundation.FCTextBox txtStickerReason;
		public fecherFoundation.FCTextBox txtOriginalStickerPlate;
		public fecherFoundation.FCTextBox txtOriginalStickerNumber;
		public Global.T2KDateBox txtOriginalStickerDate;
		public fecherFoundation.FCLabel Label35;
		public fecherFoundation.FCLabel Label34;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbType = new fecherFoundation.FCComboBox();
            this.lblType = new fecherFoundation.FCLabel();
            this.cmbReturnYes = new fecherFoundation.FCComboBox();
            this.fraAdjustments_2 = new fecherFoundation.FCFrame();
            this.lstNewSubClass = new fecherFoundation.FCComboBox();
            this.lstNewClass = new fecherFoundation.FCComboBox();
            this.lstOriginalSubClass = new fecherFoundation.FCComboBox();
            this.lstOriginalClass = new fecherFoundation.FCComboBox();
            this.txtPlateReason = new fecherFoundation.FCTextBox();
            this.txtNewPlate = new fecherFoundation.FCTextBox();
            this.txtOriginalPlate = new fecherFoundation.FCTextBox();
            this.txtPlateAdjustmentDate = new Global.T2KDateBox();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label33 = new fecherFoundation.FCLabel();
            this.Label32 = new fecherFoundation.FCLabel();
            this.Label31 = new fecherFoundation.FCLabel();
            this.Label30 = new fecherFoundation.FCLabel();
            this.Label29 = new fecherFoundation.FCLabel();
            this.Label28 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.fraAdjustments_0 = new fecherFoundation.FCFrame();
            this.lstMonetaryCategories = new fecherFoundation.FCComboBox();
            this.txtMonetaryReason = new fecherFoundation.FCTextBox();
            this.txtMonetaryPlusMinus = new fecherFoundation.FCTextBox();
            this.txtMonetaryAmount = new Global.T2KBackFillDecimal();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.fraAdjustments_1 = new fecherFoundation.FCFrame();
            this.lstOriginalStickerClass = new fecherFoundation.FCComboBox();
            this.lstNewMonthYear = new fecherFoundation.FCComboBox();
            this.lstOriginalMonthYear = new fecherFoundation.FCComboBox();
            this.txtMonthOrYear = new fecherFoundation.FCTextBox();
            this.txtNewStickerNumber = new fecherFoundation.FCTextBox();
            this.txtNewStickerSD = new fecherFoundation.FCTextBox();
            this.txtOriginalStickerSD = new fecherFoundation.FCTextBox();
            this.txtStickerReason = new fecherFoundation.FCTextBox();
            this.txtOriginalStickerPlate = new fecherFoundation.FCTextBox();
            this.txtOriginalStickerNumber = new fecherFoundation.FCTextBox();
            this.txtOriginalStickerDate = new Global.T2KDateBox();
            this.Label35 = new fecherFoundation.FCLabel();
            this.Label34 = new fecherFoundation.FCLabel();
            this.Label23 = new fecherFoundation.FCLabel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdFileSaveExit = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdjustments_2)).BeginInit();
            this.fraAdjustments_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlateAdjustmentDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdjustments_0)).BeginInit();
            this.fraAdjustments_0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonetaryAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdjustments_1)).BeginInit();
            this.fraAdjustments_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalStickerDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFileSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 382);
            this.BottomPanel.Size = new System.Drawing.Size(651, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraAdjustments_1);
            this.ClientArea.Controls.Add(this.fraAdjustments_0);
            this.ClientArea.Controls.Add(this.cmbType);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Controls.Add(this.fraAdjustments_2);
            this.ClientArea.Size = new System.Drawing.Size(671, 493);
            this.ClientArea.Controls.SetChildIndex(this.fraAdjustments_2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblType, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbType, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraAdjustments_0, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraAdjustments_1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(671, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(253, 28);
            this.HeaderText.Text = "Exception Report Items";
            // 
            // cmbType
            // 
            this.cmbType.Items.AddRange(new object[] {
            "Monetary Adjustment",
            "Sticker Adjustment",
            "Plate Adjustment"});
            this.cmbType.Location = new System.Drawing.Point(227, 30);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(305, 40);
            this.cmbType.TabIndex = 57;
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.optType_CheckedChanged);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(30, 44);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(151, 15);
            this.lblType.TabIndex = 58;
            this.lblType.Text = "TYPE OF ADJUSTMENT";
            // 
            // cmbReturnYes
            // 
            this.cmbReturnYes.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbReturnYes.Location = new System.Drawing.Point(20, 214);
            this.cmbReturnYes.Name = "cmbReturnYes";
            this.cmbReturnYes.Size = new System.Drawing.Size(92, 40);
            this.cmbReturnYes.TabIndex = 20;
            this.cmbReturnYes.Text = "No";
            // 
            // fraAdjustments_2
            // 
            this.fraAdjustments_2.Controls.Add(this.lstNewSubClass);
            this.fraAdjustments_2.Controls.Add(this.lstNewClass);
            this.fraAdjustments_2.Controls.Add(this.lstOriginalSubClass);
            this.fraAdjustments_2.Controls.Add(this.lstOriginalClass);
            this.fraAdjustments_2.Controls.Add(this.txtPlateReason);
            this.fraAdjustments_2.Controls.Add(this.txtNewPlate);
            this.fraAdjustments_2.Controls.Add(this.txtOriginalPlate);
            this.fraAdjustments_2.Controls.Add(this.txtPlateAdjustmentDate);
            this.fraAdjustments_2.Controls.Add(this.Label15);
            this.fraAdjustments_2.Controls.Add(this.Label14);
            this.fraAdjustments_2.Controls.Add(this.Label33);
            this.fraAdjustments_2.Controls.Add(this.Label32);
            this.fraAdjustments_2.Controls.Add(this.Label31);
            this.fraAdjustments_2.Controls.Add(this.Label30);
            this.fraAdjustments_2.Controls.Add(this.Label29);
            this.fraAdjustments_2.Controls.Add(this.Label28);
            this.fraAdjustments_2.Controls.Add(this.Label16);
            this.fraAdjustments_2.Controls.Add(this.Label13);
            this.fraAdjustments_2.Location = new System.Drawing.Point(30, 90);
            this.fraAdjustments_2.Name = "fraAdjustments_2";
            this.fraAdjustments_2.Size = new System.Drawing.Size(613, 216);
            this.fraAdjustments_2.TabIndex = 35;
            this.fraAdjustments_2.Text = "Plate Adjustments";
            this.fraAdjustments_2.Visible = false;
            // 
            // lstNewSubClass
            // 
            this.lstNewSubClass.BackColor = System.Drawing.SystemColors.Window;
            this.lstNewSubClass.Location = new System.Drawing.Point(453, 106);
            this.lstNewSubClass.Name = "lstNewSubClass";
            this.lstNewSubClass.Size = new System.Drawing.Size(70, 40);
            this.lstNewSubClass.TabIndex = 9;
            // 
            // lstNewClass
            // 
            this.lstNewClass.BackColor = System.Drawing.SystemColors.Window;
            this.lstNewClass.Location = new System.Drawing.Point(373, 106);
            this.lstNewClass.Name = "lstNewClass";
            this.lstNewClass.Size = new System.Drawing.Size(70, 40);
            this.lstNewClass.TabIndex = 8;
            this.lstNewClass.SelectedIndexChanged += new System.EventHandler(this.lstNewClass_SelectedIndexChanged);
            // 
            // lstOriginalSubClass
            // 
            this.lstOriginalSubClass.BackColor = System.Drawing.SystemColors.Window;
            this.lstOriginalSubClass.Location = new System.Drawing.Point(100, 106);
            this.lstOriginalSubClass.Name = "lstOriginalSubClass";
            this.lstOriginalSubClass.Size = new System.Drawing.Size(70, 40);
            this.lstOriginalSubClass.TabIndex = 5;
            // 
            // lstOriginalClass
            // 
            this.lstOriginalClass.BackColor = System.Drawing.SystemColors.Window;
            this.lstOriginalClass.Location = new System.Drawing.Point(20, 106);
            this.lstOriginalClass.Name = "lstOriginalClass";
            this.lstOriginalClass.Size = new System.Drawing.Size(70, 40);
            this.lstOriginalClass.TabIndex = 4;
            this.lstOriginalClass.SelectedIndexChanged += new System.EventHandler(this.lstOriginalClass_SelectedIndexChanged);
            // 
            // txtPlateReason
            // 
            this.txtPlateReason.BackColor = System.Drawing.SystemColors.Window;
            this.txtPlateReason.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtPlateReason.Location = new System.Drawing.Point(101, 156);
            this.txtPlateReason.Name = "txtPlateReason";
            this.txtPlateReason.Size = new System.Drawing.Size(492, 40);
            this.txtPlateReason.TabIndex = 11;
            // 
            // txtNewPlate
            // 
            this.txtNewPlate.BackColor = System.Drawing.SystemColors.Window;
            this.txtNewPlate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtNewPlate.Location = new System.Drawing.Point(533, 106);
            this.txtNewPlate.MaxLength = 8;
            this.txtNewPlate.Name = "txtNewPlate";
            this.txtNewPlate.Size = new System.Drawing.Size(60, 40);
            this.txtNewPlate.TabIndex = 10;
            this.txtNewPlate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNewPlate_KeyPress);
            // 
            // txtOriginalPlate
            // 
            this.txtOriginalPlate.BackColor = System.Drawing.SystemColors.Window;
            this.txtOriginalPlate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtOriginalPlate.Location = new System.Drawing.Point(180, 106);
            this.txtOriginalPlate.MaxLength = 8;
            this.txtOriginalPlate.Name = "txtOriginalPlate";
            this.txtOriginalPlate.Size = new System.Drawing.Size(60, 40);
            this.txtOriginalPlate.TabIndex = 6;
            // 
            // txtPlateAdjustmentDate
            // 
            this.txtPlateAdjustmentDate.Location = new System.Drawing.Point(250, 106);
            this.txtPlateAdjustmentDate.Mask = "##/##/####";
            this.txtPlateAdjustmentDate.MaxLength = 10;
            this.txtPlateAdjustmentDate.Name = "txtPlateAdjustmentDate";
            this.txtPlateAdjustmentDate.Size = new System.Drawing.Size(115, 22);
            this.txtPlateAdjustmentDate.TabIndex = 7;
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(373, 30);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(220, 19);
            this.Label15.TabIndex = 54;
            this.Label15.Text = "NEW";
            this.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(75, 30);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(221, 17);
            this.Label14.TabIndex = 53;
            this.Label14.Text = "ORIGINAL ISSUE";
            this.Label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label33
            // 
            this.Label33.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.Label33.Location = new System.Drawing.Point(431, 80);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(40, 16);
            this.Label33.TabIndex = 52;
            this.Label33.Text = "PC-P2";
            // 
            // Label32
            // 
            this.Label32.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.Label32.Location = new System.Drawing.Point(77, 80);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(40, 16);
            this.Label32.TabIndex = 51;
            this.Label32.Text = "PC-P2";
            // 
            // Label31
            // 
            this.Label31.Location = new System.Drawing.Point(20, 170);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(62, 17);
            this.Label31.TabIndex = 50;
            this.Label31.Text = "REASON";
            // 
            // Label30
            // 
            this.Label30.Location = new System.Drawing.Point(180, 55);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(46, 17);
            this.Label30.TabIndex = 49;
            this.Label30.Text = "PLATE";
            // 
            // Label29
            // 
            this.Label29.Location = new System.Drawing.Point(533, 55);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(46, 17);
            this.Label29.TabIndex = 48;
            this.Label29.Text = "PLATE";
            // 
            // Label28
            // 
            this.Label28.Location = new System.Drawing.Point(250, 55);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(46, 17);
            this.Label28.TabIndex = 47;
            this.Label28.Text = "DATE";
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(431, 55);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(46, 13);
            this.Label16.TabIndex = 46;
            this.Label16.Text = "CLASS";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(77, 55);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(46, 13);
            this.Label13.TabIndex = 45;
            this.Label13.Text = "CLASS";
            // 
            // fraAdjustments_0
            // 
            this.fraAdjustments_0.Controls.Add(this.lstMonetaryCategories);
            this.fraAdjustments_0.Controls.Add(this.txtMonetaryReason);
            this.fraAdjustments_0.Controls.Add(this.txtMonetaryPlusMinus);
            this.fraAdjustments_0.Controls.Add(this.txtMonetaryAmount);
            this.fraAdjustments_0.Controls.Add(this.Label4);
            this.fraAdjustments_0.Controls.Add(this.Label3);
            this.fraAdjustments_0.Controls.Add(this.Label2);
            this.fraAdjustments_0.Controls.Add(this.Label1);
            this.fraAdjustments_0.Location = new System.Drawing.Point(30, 90);
            this.fraAdjustments_0.Name = "fraAdjustments_0";
            this.fraAdjustments_0.Size = new System.Drawing.Size(444, 168);
            this.fraAdjustments_0.TabIndex = 16;
            this.fraAdjustments_0.Text = "Monetary Adjustments";
            this.fraAdjustments_0.Visible = false;
            // 
            // lstMonetaryCategories
            // 
            this.lstMonetaryCategories.BackColor = System.Drawing.SystemColors.Window;
            this.lstMonetaryCategories.Location = new System.Drawing.Point(160, 57);
            this.lstMonetaryCategories.Name = "lstMonetaryCategories";
            this.lstMonetaryCategories.Size = new System.Drawing.Size(264, 40);
            this.lstMonetaryCategories.Sorted = true;
            this.lstMonetaryCategories.TabIndex = 14;
            // 
            // txtMonetaryReason
            // 
            this.txtMonetaryReason.BackColor = System.Drawing.SystemColors.Window;
            this.txtMonetaryReason.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMonetaryReason.Location = new System.Drawing.Point(102, 107);
            this.txtMonetaryReason.Name = "txtMonetaryReason";
            this.txtMonetaryReason.Size = new System.Drawing.Size(322, 40);
            this.txtMonetaryReason.TabIndex = 15;
            // 
            // txtMonetaryPlusMinus
            // 
            this.txtMonetaryPlusMinus.BackColor = System.Drawing.SystemColors.Window;
            this.txtMonetaryPlusMinus.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMonetaryPlusMinus.Location = new System.Drawing.Point(110, 57);
            this.txtMonetaryPlusMinus.MaxLength = 1;
            this.txtMonetaryPlusMinus.Name = "txtMonetaryPlusMinus";
            this.txtMonetaryPlusMinus.Size = new System.Drawing.Size(40, 40);
            this.txtMonetaryPlusMinus.TabIndex = 13;
            this.txtMonetaryPlusMinus.Text = "+";
            this.txtMonetaryPlusMinus.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMonetaryPlusMinus.Enter += new System.EventHandler(this.txtMonetaryPlusMinus_Enter);
            this.txtMonetaryPlusMinus.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMonetaryPlusMinus_KeyPress);
            // 
            // txtMonetaryAmount
            // 
            this.txtMonetaryAmount.Location = new System.Drawing.Point(20, 57);
            this.txtMonetaryAmount.MaxLength = 8;
            this.txtMonetaryAmount.Name = "txtMonetaryAmount";
            this.txtMonetaryAmount.Size = new System.Drawing.Size(80, 22);
            this.txtMonetaryAmount.TabIndex = 12;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 121);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(60, 17);
            this.Label4.TabIndex = 31;
            this.Label4.Text = "REASON";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(160, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(70, 17);
            this.Label3.TabIndex = 30;
            this.Label3.Text = "CATEGORY";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(120, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(20, 17);
            this.Label2.TabIndex = 29;
            this.Label2.Text = "+/-";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(62, 17);
            this.Label1.TabIndex = 28;
            this.Label1.Text = "AMOUNT";
            // 
            // fraAdjustments_1
            // 
            this.fraAdjustments_1.Controls.Add(this.lstOriginalStickerClass);
            this.fraAdjustments_1.Controls.Add(this.cmbReturnYes);
            this.fraAdjustments_1.Controls.Add(this.lstNewMonthYear);
            this.fraAdjustments_1.Controls.Add(this.lstOriginalMonthYear);
            this.fraAdjustments_1.Controls.Add(this.txtMonthOrYear);
            this.fraAdjustments_1.Controls.Add(this.txtNewStickerNumber);
            this.fraAdjustments_1.Controls.Add(this.txtNewStickerSD);
            this.fraAdjustments_1.Controls.Add(this.txtOriginalStickerSD);
            this.fraAdjustments_1.Controls.Add(this.txtStickerReason);
            this.fraAdjustments_1.Controls.Add(this.txtOriginalStickerPlate);
            this.fraAdjustments_1.Controls.Add(this.txtOriginalStickerNumber);
            this.fraAdjustments_1.Controls.Add(this.txtOriginalStickerDate);
            this.fraAdjustments_1.Controls.Add(this.Label35);
            this.fraAdjustments_1.Controls.Add(this.Label34);
            this.fraAdjustments_1.Controls.Add(this.Label23);
            this.fraAdjustments_1.Controls.Add(this.Label22);
            this.fraAdjustments_1.Controls.Add(this.Label21);
            this.fraAdjustments_1.Controls.Add(this.Label20);
            this.fraAdjustments_1.Controls.Add(this.Label19);
            this.fraAdjustments_1.Controls.Add(this.Label18);
            this.fraAdjustments_1.Controls.Add(this.Label17);
            this.fraAdjustments_1.Controls.Add(this.Label8);
            this.fraAdjustments_1.Controls.Add(this.Label7);
            this.fraAdjustments_1.Controls.Add(this.Label6);
            this.fraAdjustments_1.Controls.Add(this.Label5);
            this.fraAdjustments_1.Location = new System.Drawing.Point(30, 90);
            this.fraAdjustments_1.Name = "fraAdjustments_1";
            this.fraAdjustments_1.Size = new System.Drawing.Size(609, 292);
            this.fraAdjustments_1.TabIndex = 32;
            this.fraAdjustments_1.Text = "Sticker Adjustments";
            this.fraAdjustments_1.Visible = false;
            // 
            // lstOriginalStickerClass
            // 
            this.lstOriginalStickerClass.BackColor = System.Drawing.SystemColors.Window;
            this.lstOriginalStickerClass.Location = new System.Drawing.Point(20, 128);
            this.lstOriginalStickerClass.Name = "lstOriginalStickerClass";
            this.lstOriginalStickerClass.Size = new System.Drawing.Size(92, 40);
            this.lstOriginalStickerClass.TabIndex = 19;
            // 
            // lstNewMonthYear
            // 
            this.lstNewMonthYear.BackColor = System.Drawing.SystemColors.Window;
            this.lstNewMonthYear.Location = new System.Drawing.Point(254, 182);
            this.lstNewMonthYear.Name = "lstNewMonthYear";
            this.lstNewMonthYear.Size = new System.Drawing.Size(132, 40);
            this.lstNewMonthYear.TabIndex = 24;
            // 
            // lstOriginalMonthYear
            // 
            this.lstOriginalMonthYear.BackColor = System.Drawing.SystemColors.Window;
            this.lstOriginalMonthYear.Location = new System.Drawing.Point(254, 80);
            this.lstOriginalMonthYear.Name = "lstOriginalMonthYear";
            this.lstOriginalMonthYear.Size = new System.Drawing.Size(132, 40);
            this.lstOriginalMonthYear.TabIndex = 21;
            // 
            // txtMonthOrYear
            // 
            this.txtMonthOrYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtMonthOrYear.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMonthOrYear.Location = new System.Drawing.Point(20, 55);
            this.txtMonthOrYear.MaxLength = 1;
            this.txtMonthOrYear.Name = "txtMonthOrYear";
            this.txtMonthOrYear.Size = new System.Drawing.Size(92, 40);
            this.txtMonthOrYear.TabIndex = 17;
            this.txtMonthOrYear.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtMonthOrYear.TextChanged += new System.EventHandler(this.txtMonthOrYear_TextChanged);
            this.txtMonthOrYear.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMonthOrYear_KeyPress);
            // 
            // txtNewStickerNumber
            // 
            this.txtNewStickerNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtNewStickerNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtNewStickerNumber.Location = new System.Drawing.Point(478, 182);
            this.txtNewStickerNumber.MaxLength = 12;
            this.txtNewStickerNumber.Name = "txtNewStickerNumber";
            this.txtNewStickerNumber.Size = new System.Drawing.Size(111, 40);
            this.txtNewStickerNumber.TabIndex = 26;
            this.txtNewStickerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNewStickerNumber_KeyPress);
            // 
            // txtNewStickerSD
            // 
            this.txtNewStickerSD.BackColor = System.Drawing.SystemColors.Window;
            this.txtNewStickerSD.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtNewStickerSD.Location = new System.Drawing.Point(396, 182);
            this.txtNewStickerSD.MaxLength = 1;
            this.txtNewStickerSD.Name = "txtNewStickerSD";
            this.txtNewStickerSD.Size = new System.Drawing.Size(70, 40);
            this.txtNewStickerSD.TabIndex = 25;
            this.txtNewStickerSD.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtNewStickerSD.TextChanged += new System.EventHandler(this.txtNewStickerSD_TextChanged);
            this.txtNewStickerSD.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNewStickerSD_KeyPress);
            // 
            // txtOriginalStickerSD
            // 
            this.txtOriginalStickerSD.BackColor = System.Drawing.SystemColors.Window;
            this.txtOriginalStickerSD.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtOriginalStickerSD.Location = new System.Drawing.Point(396, 80);
            this.txtOriginalStickerSD.MaxLength = 1;
            this.txtOriginalStickerSD.Name = "txtOriginalStickerSD";
            this.txtOriginalStickerSD.Size = new System.Drawing.Size(70, 40);
            this.txtOriginalStickerSD.TabIndex = 22;
            this.txtOriginalStickerSD.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtOriginalStickerSD.TextChanged += new System.EventHandler(this.txtOriginalStickerSD_TextChanged);
            this.txtOriginalStickerSD.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOriginalStickerSD_KeyPress);
            // 
            // txtStickerReason
            // 
            this.txtStickerReason.BackColor = System.Drawing.SystemColors.Window;
            this.txtStickerReason.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtStickerReason.Location = new System.Drawing.Point(337, 232);
            this.txtStickerReason.MaxLength = 254;
            this.txtStickerReason.Name = "txtStickerReason";
            this.txtStickerReason.Size = new System.Drawing.Size(252, 40);
            this.txtStickerReason.TabIndex = 27;
            // 
            // txtOriginalStickerPlate
            // 
            this.txtOriginalStickerPlate.BackColor = System.Drawing.SystemColors.Window;
            this.txtOriginalStickerPlate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtOriginalStickerPlate.Location = new System.Drawing.Point(122, 128);
            this.txtOriginalStickerPlate.Name = "txtOriginalStickerPlate";
            this.txtOriginalStickerPlate.Size = new System.Drawing.Size(113, 40);
            this.txtOriginalStickerPlate.TabIndex = 20;
            // 
            // txtOriginalStickerNumber
            // 
            this.txtOriginalStickerNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtOriginalStickerNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtOriginalStickerNumber.Location = new System.Drawing.Point(476, 80);
            this.txtOriginalStickerNumber.MaxLength = 12;
            this.txtOriginalStickerNumber.Name = "txtOriginalStickerNumber";
            this.txtOriginalStickerNumber.Size = new System.Drawing.Size(111, 40);
            this.txtOriginalStickerNumber.TabIndex = 23;
            this.txtOriginalStickerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOriginalStickerNumber_KeyPress);
            // 
            // txtOriginalStickerDate
            // 
            this.txtOriginalStickerDate.Location = new System.Drawing.Point(122, 55);
            this.txtOriginalStickerDate.Mask = "##/##/####";
            this.txtOriginalStickerDate.Name = "txtOriginalStickerDate";
            this.txtOriginalStickerDate.Size = new System.Drawing.Size(115, 22);
            this.txtOriginalStickerDate.TabIndex = 18;
            // 
            // Label35
            // 
            this.Label35.Location = new System.Drawing.Point(20, 178);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(170, 26);
            this.Label35.TabIndex = 58;
            this.Label35.Text = "RETURN STICKERS TO INVENTORY?";
            // 
            // Label34
            // 
            this.Label34.Location = new System.Drawing.Point(20, 30);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(30, 17);
            this.Label34.TabIndex = 57;
            this.Label34.Text = "M/Y";
            // 
            // Label23
            // 
            this.Label23.Location = new System.Drawing.Point(254, 246);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(55, 17);
            this.Label23.TabIndex = 44;
            this.Label23.Text = "REASON";
            // 
            // Label22
            // 
            this.Label22.Location = new System.Drawing.Point(254, 130);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(333, 15);
            this.Label22.TabIndex = 43;
            this.Label22.Text = "NEW STICKER INFORMATION";
            this.Label22.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label21
            // 
            this.Label21.Location = new System.Drawing.Point(254, 155);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(102, 17);
            this.Label21.TabIndex = 42;
            this.Label21.Text = "MONTH / YEAR";
            // 
            // Label20
            // 
            this.Label20.Location = new System.Drawing.Point(396, 155);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(68, 17);
            this.Label20.TabIndex = 41;
            this.Label20.Text = "SGL / DBL";
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(478, 155);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(62, 17);
            this.Label19.TabIndex = 40;
            this.Label19.Text = "STICKER #";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(254, 30);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(333, 16);
            this.Label18.TabIndex = 39;
            this.Label18.Text = "ORIGINAL STICKER INFORMATION";
            this.Label18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(122, 30);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(40, 15);
            this.Label17.TabIndex = 38;
            this.Label17.Text = "DATE";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(254, 55);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(90, 17);
            this.Label8.TabIndex = 37;
            this.Label8.Text = "MONTH / YEAR";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 105);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(94, 13);
            this.Label7.TabIndex = 36;
            this.Label7.Text = "CLASS / PLATE";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(396, 55);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(68, 17);
            this.Label6.TabIndex = 34;
            this.Label6.Text = "SGL / DBL";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(478, 55);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(62, 17);
            this.Label5.TabIndex = 33;
            this.Label5.Text = "STICKER #";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSaveExit,
            this.mnuFileSeperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileSaveExit
            // 
            this.mnuFileSaveExit.Index = 0;
            this.mnuFileSaveExit.Name = "mnuFileSaveExit";
            this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSaveExit.Text = "Save & Exit";
            this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 1;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 2;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdFileSaveExit
            // 
            this.cmdFileSaveExit.AppearanceKey = "acceptButton";
            this.cmdFileSaveExit.Location = new System.Drawing.Point(281, 30);
            this.cmdFileSaveExit.Name = "cmdFileSaveExit";
            this.cmdFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFileSaveExit.Size = new System.Drawing.Size(78, 48);
            this.cmdFileSaveExit.TabIndex = 0;
            this.cmdFileSaveExit.Text = "Save";
            this.cmdFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // frmExceptionReportItems
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(671, 553);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmExceptionReportItems";
            this.Text = " Exception Report Items";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmExceptionReportItems_Load);
            this.Activated += new System.EventHandler(this.frmExceptionReportItems_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmExceptionReportItems_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdjustments_2)).EndInit();
            this.fraAdjustments_2.ResumeLayout(false);
            this.fraAdjustments_2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlateAdjustmentDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdjustments_0)).EndInit();
            this.fraAdjustments_0.ResumeLayout(false);
            this.fraAdjustments_0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonetaryAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdjustments_1)).EndInit();
            this.fraAdjustments_1.ResumeLayout(false);
            this.fraAdjustments_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOriginalStickerDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdFileSaveExit;
	}
}
