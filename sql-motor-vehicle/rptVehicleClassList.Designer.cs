﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptVehicleClassList.
	/// </summary>
	partial class rptVehicleClassList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptVehicleClassList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblClass = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblStatusDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSubclass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMVR3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStatusDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSubclass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMVR3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPlate,
				this.fldOwner,
				this.fldMake,
				this.fldModel,
				this.fldYear,
				this.fldVIN,
				this.fldExpires,
				this.fldClass,
				this.fldSubclass,
				this.fldMVR3
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblClass,
				this.lblStatusDateRange,
				this.lblTitle,
				this.Label34,
				this.Label9,
				this.Label10,
				this.Label35,
				this.Label36,
				this.Label37,
				this.Label38,
				this.Label39,
				this.Label40,
				this.Label41,
				this.Line1,
				this.Label42,
				this.Label43,
				this.Label44,
				this.Label45
			});
			this.PageHeader.Height = 0.9166667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label46,
				this.fldTotal
			});
			this.GroupFooter1.Height = 0.40625F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblClass
			// 
			this.lblClass.Height = 0.1875F;
			this.lblClass.HyperLink = null;
			this.lblClass.Left = 1.46875F;
			this.lblClass.Name = "lblClass";
			this.lblClass.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblClass.Text = "Label5";
			this.lblClass.Top = 0.21875F;
			this.lblClass.Width = 5.15625F;
			// 
			// lblStatusDateRange
			// 
			this.lblStatusDateRange.Height = 0.1875F;
			this.lblStatusDateRange.HyperLink = null;
			this.lblStatusDateRange.Left = 1.46875F;
			this.lblStatusDateRange.Name = "lblStatusDateRange";
			this.lblStatusDateRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblStatusDateRange.Text = "Label6";
			this.lblStatusDateRange.Top = 0.40625F;
			this.lblStatusDateRange.Width = 5.15625F;
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1.5F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.lblTitle.Text = "Vehicle Class List";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 5.125F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label34.Text = "Label34";
			this.Label34.Top = 0.1875F;
			this.Label34.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 6.65625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 6.65625F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label35.Text = "Label35";
			this.Label35.Top = 0F;
			this.Label35.Width = 1.3125F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 0.6875F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "";
			this.Label36.Text = "Plate";
			this.Label36.Top = 0.71875F;
			this.Label36.Width = 0.5625F;
			// 
			// Label37
			// 
			this.Label37.Height = 0.1875F;
			this.Label37.HyperLink = null;
			this.Label37.Left = 2.03125F;
			this.Label37.Name = "Label37";
			this.Label37.Style = "";
			this.Label37.Text = "Owner";
			this.Label37.Top = 0.71875F;
			this.Label37.Width = 2.28125F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.1875F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 4.34375F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "text-align: center";
			this.Label38.Text = "Make";
			this.Label38.Top = 0.71875F;
			this.Label38.Width = 0.4375F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.1875F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 4.84375F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "";
			this.Label39.Text = "Model";
			this.Label39.Top = 0.71875F;
			this.Label39.Width = 0.5625F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.1875F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 5.375F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "text-align: center";
			this.Label40.Text = "Year";
			this.Label40.Top = 0.71875F;
			this.Label40.Width = 0.40625F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.1875F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 5.84375F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "";
			this.Label41.Text = "VIN";
			this.Label41.Top = 0.71875F;
			this.Label41.Width = 1.34375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.90625F;
			this.Line1.Width = 7.96875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.96875F;
			this.Line1.Y1 = 0.90625F;
			this.Line1.Y2 = 0.90625F;
			// 
			// Label42
			// 
			this.Label42.Height = 0.1875F;
			this.Label42.HyperLink = null;
			this.Label42.Left = 1.3125F;
			this.Label42.Name = "Label42";
			this.Label42.Style = "text-align: center";
			this.Label42.Text = "Expires";
			this.Label42.Top = 0.71875F;
			this.Label42.Width = 0.6875F;
			// 
			// Label43
			// 
			this.Label43.Height = 0.1875F;
			this.Label43.HyperLink = null;
			this.Label43.Left = 0.34375F;
			this.Label43.Name = "Label43";
			this.Label43.Style = "text-align: center";
			this.Label43.Text = "Sub";
			this.Label43.Top = 0.71875F;
			this.Label43.Width = 0.3125F;
			// 
			// Label44
			// 
			this.Label44.Height = 0.1875F;
			this.Label44.HyperLink = null;
			this.Label44.Left = 0F;
			this.Label44.Name = "Label44";
			this.Label44.Style = "text-align: center";
			this.Label44.Text = "Cl";
			this.Label44.Top = 0.71875F;
			this.Label44.Width = 0.28125F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.1875F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 7.25F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "text-align: left";
			this.Label45.Text = "MVR3";
			this.Label45.Top = 0.71875F;
			this.Label45.Width = 0.625F;
			// 
			// fldPlate
			// 
			this.fldPlate.Height = 0.1875F;
			this.fldPlate.Left = 0.71875F;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPlate.Text = "Field1";
			this.fldPlate.Top = 0F;
			this.fldPlate.Width = 0.5625F;
			// 
			// fldOwner
			// 
			this.fldOwner.Height = 0.1875F;
			this.fldOwner.Left = 2.03125F;
			this.fldOwner.Name = "fldOwner";
			this.fldOwner.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldOwner.Text = "Field1";
			this.fldOwner.Top = 0F;
			this.fldOwner.Width = 2.25F;
			// 
			// fldMake
			// 
			this.fldMake.Height = 0.1875F;
			this.fldMake.Left = 4.34375F;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldMake.Text = "Field1";
			this.fldMake.Top = 0F;
			this.fldMake.Width = 0.4375F;
			// 
			// fldModel
			// 
			this.fldModel.Height = 0.1875F;
			this.fldModel.Left = 4.84375F;
			this.fldModel.Name = "fldModel";
			this.fldModel.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldModel.Text = "Field1";
			this.fldModel.Top = 0F;
			this.fldModel.Width = 0.5625F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 5.40625F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldYear.Text = "Field1";
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.40625F;
			// 
			// fldVIN
			// 
			this.fldVIN.Height = 0.1875F;
			this.fldVIN.Left = 5.8125F;
			this.fldVIN.Name = "fldVIN";
			this.fldVIN.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldVIN.Text = "Field1";
			this.fldVIN.Top = 0F;
			this.fldVIN.Width = 1.375F;
			// 
			// fldExpires
			// 
			this.fldExpires.Height = 0.1875F;
			this.fldExpires.Left = 1.3125F;
			this.fldExpires.Name = "fldExpires";
			this.fldExpires.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldExpires.Text = "Field1";
			this.fldExpires.Top = 0F;
			this.fldExpires.Width = 0.6875F;
			// 
			// fldClass
			// 
			this.fldClass.Height = 0.1875F;
			this.fldClass.Left = 0F;
			this.fldClass.Name = "fldClass";
			this.fldClass.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldClass.Text = "Field1";
			this.fldClass.Top = 0F;
			this.fldClass.Width = 0.28125F;
			// 
			// fldSubclass
			// 
			this.fldSubclass.Height = 0.1875F;
			this.fldSubclass.Left = 0.34375F;
			this.fldSubclass.Name = "fldSubclass";
			this.fldSubclass.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldSubclass.Text = "Field1";
			this.fldSubclass.Top = 0F;
			this.fldSubclass.Width = 0.3125F;
			// 
			// fldMVR3
			// 
			this.fldMVR3.Height = 0.1875F;
			this.fldMVR3.Left = 7.25F;
			this.fldMVR3.Name = "fldMVR3";
			this.fldMVR3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldMVR3.Text = "Field1";
			this.fldMVR3.Top = 0F;
			this.fldMVR3.Width = 0.625F;
			// 
			// Label46
			// 
			this.Label46.Height = 0.19F;
			this.Label46.HyperLink = null;
			this.Label46.Left = 3.21875F;
			this.Label46.Name = "Label46";
			this.Label46.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label46.Text = "Total Vehicles:";
			this.Label46.Top = 0.15625F;
			this.Label46.Width = 1F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.19F;
			this.fldTotal.Left = 4.21875F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0.15625F;
			this.fldTotal.Width = 0.75F;
			// 
			// rptVehicleClassList
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStatusDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpires)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSubclass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMVR3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpires;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSubclass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMVR3;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblClass;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStatusDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label37;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label42;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label43;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label44;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label46;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
	}
}
