//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmInventoryStatus : BaseForm
	{
		public frmInventoryStatus()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmInventoryStatus InstancePtr
		{
			get
			{
				return (frmInventoryStatus)Sys.GetInstance(typeof(frmInventoryStatus));
			}
		}

		protected frmInventoryStatus _InstancePtr = null;
		//=========================================================
		string strSQL;
		string strSQLItems;
		clsDRWrapper rsPlates = new clsDRWrapper();
		clsDRWrapper rsItems = new clsDRWrapper();
		clsDRWrapper rsPrint = new clsDRWrapper();
		clsDRWrapper rsSearch = new clsDRWrapper();
		FCFileSystem ff = new FCFileSystem();
		public string InventoryPrinter = "";
		bool ReShowFlag;
		string TitleType = "";
		int NumberOfRecords;
		int[] Stickers = new int[50 + 1];
		// vbPorter upgrade warning: MaxElements As int	OnWriteFCConvert.ToInt32(
		int MaxElements;
		public string StrSearch = "";
		// vbPorter upgrade warning: fnx As int	OnRead
		int fnx;
		int TypeCol;
		int OpIDCol;
		int LowCol;
		int HighCol;
		int StatusCol;
		int GroupCol;
		int DateCol;
		int CountCol;
		double[] dblColPerecnts = new double[7 + 1];
		public int intSelectedGroup;
		public string strHeading = "";
		public int intIndex;
		public string strSortBy = "";
		public bool blnAllInventory;

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			fraMVR10.Visible = false;
		}

		private void cmdPrintAll_Click(object sender, System.EventArgs e)
		{
			string strDirectory = "";
			int Index;
			int intCounter;
			fecherFoundation.Information.Err().Clear();
			dlg1.Show();
			if (fecherFoundation.Information.Err().Number == 0)
			{
				InventoryPrinter = FCGlobal.Printer.DeviceName;
			}
			else
			{
				return;
			}
			blnAllInventory = true;
			for (intCounter = 0; intCounter <= 9; intCounter++)
			{
				intIndex = intCounter;
				switch (intIndex)
                {
                    case 0:
                        strHeading = "MVR3 Forms";

                        break;
                    case 1:
                        strHeading = "Plate";

                        break;
                    case 2:
                        strHeading = "Double Year Stickers";

                        break;
                    case 3:
                        strHeading = "Double Month Stickers";

                        break;
                    case 4:
                        strHeading = "Single Year Stickers";

                        break;
                    case 5:
                        strHeading = "Single Month Stickers";

                        break;
                    case 6:
                        strHeading = "Decals";

                        break;
                    case 7:
                        strHeading = "Boosters";

                        break;
                    case 8:
                        strHeading = "MVR10 Forms";

                        break;
                    case 9:
                        strHeading = "Combination Stickers";

                        break;
                    default:
                        return;
                }
				modDuplexPrinting.DuplexPrintReport(rptInventoryStatus.InstancePtr, InventoryPrinter);
				rptInventoryStatus.InstancePtr.Unload();
				CheckIt:

                if (!MotorVehicle.JobComplete("rptInventoryStatus"))
                {
                    goto CheckIt;
                }
            }
		}

		private void cmdPrintSelected_Click(object sender, System.EventArgs e)
		{
			GrapeCity.ActiveReports.Export.Word.Section.RtfExport ArTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
			string temp = "";
			string strLine = "";
			string strFilePath = "";
			string strDirectory = "";
			fecherFoundation.Information.Err().Clear();
			dlg1.Show();
			if (fecherFoundation.Information.Err().Number == 0)
			{
				InventoryPrinter = FCGlobal.Printer.DeviceName;
			}
			else
			{
				return;
			}
			blnAllInventory = false;
			if ((dlg1.Flags & FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDPrintToFile)) == FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDPrintToFile))
			{
				strDirectory = Environment.CurrentDirectory;
				fecherFoundation.Information.Err().Clear();
				dlg1_Save.Filter = "*.doc";
				dlg1_Save.Show();
				if (fecherFoundation.Information.Err().Number == 0)
				{
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Building Report");
					temp = dlg1_Save.FileName;
					if (Strings.InStr(1, temp, ".", CompareConstants.vbBinaryCompare) == 0)
					{
						temp += ".doc";
					}
					else
					{
						temp = Strings.Left(temp, Strings.InStr(1, temp, ".", CompareConstants.vbBinaryCompare)) + "doc";
					}
				}
				else
				{
					Environment.CurrentDirectory = strDirectory;
					return;
				}
				switch (cmbInventoryType.Text)
                {
                    case "MVR3s":
                        strHeading = "MVR3 Forms";
                        intIndex = 0;

                        break;
                    case "Plates":
                        strHeading = "Plate";
                        intIndex = 1;

                        break;
                    case "Double Year Stickers":
                        strHeading = "Double Year Stickers";
                        intIndex = 2;

                        break;
                    case "Double Month Stickers":
                        strHeading = "Double Month Stickers";
                        intIndex = 3;

                        break;
                    case "Single Year Stickers":
                        strHeading = "Single Year Stickers";
                        intIndex = 4;

                        break;
                    case "Single Month Stickers":
                        strHeading = "Single Month Stickers";
                        intIndex = 5;

                        break;
                    case "Decals":
                        strHeading = "Decals";
                        intIndex = 6;

                        break;
                    case "Boosters":
                        strHeading = "Boosters";
                        intIndex = 7;

                        break;
                    case "MVR10s":
                        strHeading = "MVR10 Forms";
                        intIndex = 8;

                        break;
                    case "Combination Stickers":
                        strHeading = "Combination Stickers";
                        intIndex = 9;

                        break;
                    default:
                        frmWait.InstancePtr.Unload();
                        return;
                }
				ArTemp = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				rptInventoryStatus.InstancePtr.Run(false);
				string fileName = temp;
				//App.DoEvents();
				//rptInventoryStatus.InstancePtr.Export(ArTemp);
				rptInventoryStatus.InstancePtr.Unload();
				frmWait.InstancePtr.Unload();
				CheckIt2:
				;
				if (MotorVehicle.JobComplete("rptInventory"))
				{
					// do nothing
				}
				else
				{
					goto CheckIt2;
				}
				Environment.CurrentDirectory = strDirectory;
			}
			else
			{
				switch (cmbInventoryType.Text)
                {
                    case "MVR3s":
                        strHeading = "MVR3 Forms";
                        intIndex = 0;

                        break;
                    case "Plates":
                        strHeading = "Plate";
                        intIndex = 1;

                        break;
                    case "Double Year Stickers":
                        strHeading = "Double Year Stickers";
                        intIndex = 2;

                        break;
                    case "Double Month Stickers":
                        strHeading = "Double Month Stickers";
                        intIndex = 3;

                        break;
                    case "Single Year Stickers":
                        strHeading = "Single Year Stickers";
                        intIndex = 4;

                        break;
                    case "Single Month Stickers":
                        strHeading = "Single Month Stickers";
                        intIndex = 5;

                        break;
                    case "Decals":
                        strHeading = "Decals";
                        intIndex = 6;

                        break;
                    case "Boosters":
                        strHeading = "Boosters";
                        intIndex = 7;

                        break;
                    case "MVR10s":
                        strHeading = "MVR10 Forms";
                        intIndex = 8;

                        break;
                    case "Combination Stickers":
                        strHeading = "Combination Stickers";
                        intIndex = 9;

                        break;
                    default:
                        return;
                }
				modDuplexPrinting.DuplexPrintReport(rptInventoryStatus.InstancePtr, InventoryPrinter);
				rptInventoryStatus.InstancePtr.Unload();
			}
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdReturn_Click()
		{
			cmdReturn_Click(mnuExit, new System.EventArgs());
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorTag = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				string tempSticker;
				string tempType = "";
				string tempPre = "";
				string tempSuf = "";
				string tempNumber = "";
				// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
				int counter;
				bool sufflag = false;
				string tempplate = "";
				clsDRWrapper rsBooster = new clsDRWrapper();
				clsDRWrapper rsSpecialReg = new clsDRWrapper();
				if (cmbInventoryType.Text == "MVR3s")
				{
					tempType = "MVR3";
				}
				else if (cmbInventoryType.Text == "Plates")
				{
					tempType = "Plate";
				}
				else if (cmbInventoryType.Text == "Double Year Stickers")
				{
					tempType = "Double Year Sticker";
				}
				else if (cmbInventoryType.Text == "Double Month Stickers")
				{
					tempType = "Double Month Sticker";
				}
				else if (cmbInventoryType.Text == "Single Year Stickers")
				{
					tempType = "Single Year Sticker";
				}
				else if (cmbInventoryType.Text == "Single Month Stickers")
				{
					tempType = "Single Month Sticker";
				}
				else if (cmbInventoryType.Text == "Decals")
				{
					tempType = "Decal";
				}
				else if (cmbInventoryType.Text == "Boosters")
				{
					tempType = "Booster Plate";
				}
				else if (cmbInventoryType.Text == "MVR10s")
				{
					tempType = "MVR10";
				}
				else if (cmbInventoryType.Text == "Combination Stickers")
				{
					tempType = "Combination Stickers";
				}
				else
				{
					return;
				}
				tempSticker = Interaction.InputBox("Please enter the number of the " + tempType + " you wish to search for.", tempType + " Search", null);
				if (tempSticker == "")
				{
					return;
				}
				else
				{
					vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorTag;
					/* On Error GoTo ErrorTag */
					fecherFoundation.Information.Err().Clear();
					frmWait.InstancePtr.Show();
					frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Searching";
					frmWait.InstancePtr.Refresh();
					sufflag = false;
					tempPre = "";
					tempSuf = "";
					tempNumber = "";
					tempplate = tempSticker.Replace("-", "");
					//FC:FINAL:MSH - i.issue #1898: length of string will be changed inside of cycle and half of string will be missed
					int tempplateLength = tempplate.Length;
					//for (counter = 0; counter <= (tempplate.Length - 1); counter++)
					for (counter = 0; counter < tempplateLength; counter++)
					{
						if (!Information.IsNumeric(Strings.Left(tempplate, 1)))
						{
							if (!sufflag)
							{
								tempPre += Strings.Left(tempplate, 1);
							}
							else
							{
								tempSuf = tempplate;
								break;
							}
						}
						else
						{
							sufflag = true;
							tempNumber += Strings.Left(tempplate, 1);
						}
						tempplate = Strings.Right(tempplate, tempplate.Length - 1);
					}
					if (tempNumber != "")
					{
						rsSearch.OpenRecordset("SELECT * FROM Inventory WHERE Prefix = '" + tempPre + "' AND Suffix = '" + tempSuf + "' AND Number = " + tempNumber);
						if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
						{
							rsSearch.MoveLast();
							rsSearch.MoveFirst();
							if (FCConvert.ToString(rsSearch.Get_Fields_String("Status")) == "A")
							{
								frmWait.InstancePtr.Unload();
								//App.DoEvents();
								MessageBox.Show("This " + tempType + " has not been issued.", "Unissued " + tempType, MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else if (rsSearch.Get_Fields_String("Status") == "V")
							{
								frmWait.InstancePtr.Unload();
								//App.DoEvents();
								MessageBox.Show("This " + tempType + " has been voided.", "Voided " + tempType, MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else
							{
								if (cmbInventoryType.Text == "MVR3s")
								{
									MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM Master WHERE MVR3 = " + tempSticker);
								}
								else if (cmbInventoryType.Text == "Plates" || cmbInventoryType.Text == "Decals")
								{
									MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM Master WHERE Plate = '" + tempSticker + "' OR PlateStripped = '" + tempSticker + "'");
								}
								else if (cmbInventoryType.Text == "Double Year Stickers" || cmbInventoryType.Text == "Single Year Stickers")
								{
									//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
									MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM Master WHERE (Class <> 'AU' AND Class <> 'MC' AND Class <> 'MX' AND Class <> 'PM' AND Class <> 'VM' AND Class <> 'XV') AND YearStickerYear = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(lstItems.Items[lstItems.SelectedIndex].Text))) + " AND YearStickerNumber = " + tempSticker);
								}
								else if (cmbInventoryType.Text == "Combination Stickers")
								{
									//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
									MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM Master WHERE (Class = 'AU' OR Class = 'MC' OR Class = 'MX' OR Class = 'PM' OR Class = 'VM' OR Class = 'XV') AND YearStickerYear = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(lstItems.Items[lstItems.SelectedIndex].Text))) + " AND YearStickerNumber = " + tempSticker);
								}
								else if (cmbInventoryType.Text == "Double Month Stickers" || cmbInventoryType.Text == "Single Month Stickers")
								{
									MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM Master WHERE MonthStickerMonth = " + FCConvert.ToString((lstItems.SelectedIndex + 1)) + " AND MonthStickerNumber = " + tempSticker);
								}
								else if (cmbInventoryType.Text == "Boosters")
								{
									rsBooster.OpenRecordset("SELECT * FROM Booster WHERE Permit =" + tempSticker);
									if (rsBooster.EndOfFile() != true && rsBooster.BeginningOfFile() != true)
									{
										MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM Master WHERE ID =" + rsBooster.Get_Fields_Int32("VehicleID"));
									}
								}
								else if (cmbInventoryType.Text == "MVR10s")
								{
									frmWait.InstancePtr.Unload();
									//App.DoEvents();
									rsSpecialReg.OpenRecordset("SELECT * FROM SpecialRegistration WHERE PermitNumber =" + tempSticker);
									if (rsSpecialReg.EndOfFile() != true && rsSpecialReg.BeginningOfFile() != true)
									{
										rsSpecialReg.InsertAddress("PartyID", "", "MV");
										txtPermit.Text = FCConvert.ToString(rsSpecialReg.Get_Fields("PermitNumber"));
										txtEffectiveDate.Text = Strings.Format(rsSpecialReg.Get_Fields("EffectiveDate"), "MM/dd/yyyy");
										txtExpireDate.Text = Strings.Format(rsSpecialReg.Get_Fields("ExpireDate"), "MM/dd/yyyy");
										txtApplicant.Text = fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsSpecialReg.Get_Fields_Int32("PartyID")));
										txtAddress.Text = FCConvert.ToString(rsSpecialReg.Get_Fields_String("Address1"));
										txtMake.Text = FCConvert.ToString(rsSpecialReg.Get_Fields_String("make"));
										txtYear.Text = FCConvert.ToString(rsSpecialReg.Get_Fields("Year"));
										txtColor.Text = FCConvert.ToString(rsSpecialReg.Get_Fields_String("Color"));
										txtVIN.Text = FCConvert.ToString(rsSpecialReg.Get_Fields_String("Vin"));
										fraMVR10.Visible = true;
										return;
									}
									else
									{
										MessageBox.Show("No records found for this " + tempType + ".", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return;
									}
								}
								frmWait.InstancePtr.Unload();
								//App.DoEvents();
								if (MotorVehicle.Statics.rsPlateForReg.IsntAnything())
								{
									MessageBox.Show("No records found for this " + tempType + ".", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								else
								{
									if (MotorVehicle.Statics.rsPlateForReg.EndOfFile() != true && MotorVehicle.Statics.rsPlateForReg.BeginningOfFile() != true)
									{
										MotorVehicle.Statics.rsPlateForReg.MoveLast();
										MotorVehicle.Statics.rsPlateForReg.MoveFirst();
										MotorVehicle.Statics.FromInventory = true;
										if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) == "L9")
										{
											frmVehicleUpdateLongTerm.InstancePtr.Show(App.MainForm);
										}
										else
										{
											frmVehicleUpdate.InstancePtr.Show(App.MainForm);
										}
										this.Hide();
									}
									else
									{
										MessageBox.Show("No records found for this " + tempType + ".", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									}
								}
							}
						}
						else
						{
							frmWait.InstancePtr.Unload();
							//App.DoEvents();
							MessageBox.Show("No records found for this " + tempType + ".", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else
					{
						frmWait.InstancePtr.Unload();
						//App.DoEvents();
						MessageBox.Show("No records found for this " + tempType + ".", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				return;
				ErrorTag:
				;
				frmWait.InstancePtr.Unload();
				//App.DoEvents();
				MessageBox.Show("No records found for this " + tempType + ".", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_ErrorTag:
						//? goto ErrorTag;
						break;
				}
			}
		}

		private void frmInventoryStatus_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.INVENTORYSTATUS))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			TypeCol = 0;
			DateCol = 1;
			OpIDCol = 2;
			LowCol = 3;
			HighCol = 4;
			StatusCol = 5;
			GroupCol = 6;
			CountCol = 7;
			vs1.ColWidth(TypeCol, 700);
			vs1.ColWidth(DateCol, 1100);
			vs1.ColWidth(OpIDCol, 700);
			vs1.ColWidth(LowCol, 1200);
			vs1.ColWidth(HighCol, 1200);
			vs1.ColWidth(StatusCol, 800);
			vs1.ColWidth(GroupCol, 800);
			vs1.ColWidth(CountCol, 800);
			dblColPerecnts[0] = 0.086892;
			dblColPerecnts[1] = 0.110926;
			dblColPerecnts[2] = 0.086892;
			dblColPerecnts[3] = 0.1479014;
			dblColPerecnts[4] = 0.1479014;
			dblColPerecnts[5] = 0.0979846;
			dblColPerecnts[6] = 0.0979846;
			dblColPerecnts[7] = 0.1238674;
			vs1.TextMatrix(0, TypeCol, "Type");
			vs1.TextMatrix(0, DateCol, "Date");
			vs1.TextMatrix(0, OpIDCol, "OpID");
			vs1.TextMatrix(0, LowCol, "Low");
			vs1.TextMatrix(0, HighCol, "High");
			vs1.TextMatrix(0, StatusCol, "Status");
			vs1.TextMatrix(0, GroupCol, "Group");
			vs1.TextMatrix(0, CountCol, "Count");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vs1.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(OpIDCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(StatusCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(LowCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(HighCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(GroupCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColDataType(CountCol, FCGrid.DataTypeSettings.flexDTLong);
            if (!MotorVehicle.Statics.FromInventory)
			{
				if (MotorVehicle.Statics.ListRefresh == true)
				{
					if (lstPlates.SelectedIndex != -1)
					{
						lstPlates_DblClick();
					}
					else if (lstItems.SelectedIndex != -1)
					{
						lstItems_DblClick();
					}
					else
					{
						if (cmbInventoryType.Text == "MVR3s")
						{
							optInventoryType_Click(0);
						}
						else if (cmbInventoryType.Text == "Plates")
						{
							optInventoryType_Click(1);
						}
						else if (cmbInventoryType.Text == "Double Year Stickers")
						{
							optInventoryType_Click(2);
						}
						else if (cmbInventoryType.Text == "Double Month Stickers")
						{
							optInventoryType_Click(3);
						}
						else if (cmbInventoryType.Text == "Single Year Stickers")
						{
							optInventoryType_Click(4);
						}
						else if (cmbInventoryType.Text == "Single Month Stickers")
						{
							optInventoryType_Click(5);
						}
						else if (cmbInventoryType.Text == "Decals")
						{
							optInventoryType_Click(6);
						}
						else if (cmbInventoryType.Text == "Boosters")
						{
							optInventoryType_Click(7);
						}
						else if (cmbInventoryType.Text == "MVR10s")
						{
							optInventoryType_Click(8);
						}
						else if (cmbInventoryType.Text == "Combination Stickers")
						{
							optInventoryType_Click(9);
						}
					}
					MotorVehicle.Statics.ListRefresh = false;
					StrSearch = "AND Status <> 'D'";
					cmdSearch.Enabled = false;
					ReShowFlag = false;
				}
				else
				{
					StrSearch = "AND Status <> 'D'";
				}
			}
			else
			{
				MotorVehicle.Statics.FromInventory = false;
			}

            optInventoryType_CheckedChanged(0, null, EventArgs.Empty);
        }

		private void frmInventoryStatus_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				frmInventoryStatus.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmInventoryStatus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmInventoryStatus properties;
			//frmInventoryStatus.ScaleMode	= 0;
			//frmInventoryStatus.ScaleWidth	= 9656.1484375;
			//frmInventoryStatus.ScaleHeight	= 10214.5703125;
			//frmInventoryStatus.LinkTopic	= "Form1";
			//End Unmaped Properties
			//Support.ZOrder(optStart, 1);
			lstItems.Visible = false;
			intSelectedGroup = 0;
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmInventoryStatus_Resize(object sender, System.EventArgs e)
		{
			int counter;
			// vbPorter upgrade warning: intRowHeight As int	OnWriteFCConvert.ToInt32(
			//FC:FINAL:DDU: design part manages this one
			//int intRowHeight;
			//intRowHeight = vs1.RowHeight(0);
			//for (counter = 0; counter <= 7; counter++)
			//{
			//	vs1.ColWidth(counter, FCConvert.ToInt32(vs1.WidthOriginal * dblColPerecnts[counter]));
			//}
			//if (vs1.Rows < 7)
			//{
			//	vs1.Height = FCConvert.ToInt32((intRowHeight * (vs1.Rows * 1.5)) + 75);
			//}
			//else
			//{
			//	vs1.Height = FCConvert.ToInt32((intRowHeight * (7 * 1.5)) + 75);
			//}
		}

		public void Form_Resize()
		{
			frmInventoryStatus_Resize(this, new System.EventArgs());
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			strSQL = "";
			strSQLItems = "";
			rsPlates.Reset();
			rsItems.Reset();
			ff = null;
			NumberOfRecords = 0;
			for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
			{
				Stickers[fnx] = 0;
			}
			MaxElements = 0;
			fnx = 0;
			if (MotorVehicle.Statics.F9 == true)
			{
				// if the inventory screen was called from data input
				MotorVehicle.Statics.F9 = false;
			}
		}

		private void lstItems_DoubleClick(object sender, System.EventArgs e)
		{
			string Month = "";
			string xx = "";
			if (MotorVehicle.Statics.ListType == "P")
			{
				// moved to lstPlates.click
			}
			else if (MotorVehicle.Statics.ListType == "SM")
			{
				Month = FCConvert.ToString(modGlobalRoutines.PadToString((lstItems.SelectedIndex + 1), 2));
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMS' AND substring(Code,4,2) = '" + Month + "' " + StrSearch + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMS' AND substring(Code,4,2) = '" + Month + "' " + StrSearch + " AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
					ReShowFlag = true;
					cmdSearch.Enabled = true;
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					MessageBox.Show("There are no single " + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "") + "  stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				rsSearch = rsItems;
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "DM")
			{
				Month = FCConvert.ToString(modGlobalRoutines.PadToString((lstItems.SelectedIndex + 1), 2));
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMD' AND substring(Code,4,2) = '" + Month + "' " + StrSearch + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,3)  = 'SMD' AND substring(Code,4,2) = '" + Month + "' " + StrSearch + " AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
					ReShowFlag = true;
					cmdSearch.Enabled = true;
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
					MessageBox.Show("There are no double  " + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "") + "  stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				rsSearch = rsItems;
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "DY")
			{
				//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
				xx = "SYD" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' " + StrSearch + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' " + StrSearch + " AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
					ReShowFlag = true;
					cmdSearch.Enabled = true;
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					MessageBox.Show("There are no double year stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				rsSearch = rsItems;
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "COMBINATION")
			{
				//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
				xx = "SYC" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' " + StrSearch + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xx + "' " + StrSearch + " AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
					ReShowFlag = true;
					cmdSearch.Enabled = true;
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					MessageBox.Show("There are no combination stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				rsSearch = rsItems;
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "SY")
			{
				string xy = "";
				//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
				xy = "SYS" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xy + "' " + StrSearch + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code = '" + xy + "' " + StrSearch + " AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
					ReShowFlag = true;
					cmdSearch.Enabled = true;
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					MessageBox.Show("There are no single year stickers in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				rsSearch = rsItems;
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "DECAL")
			{
				string YY = "";
				//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
				YY = "DYS" + (lstItems.SelectedIndex >= 0 ? lstItems.Items[lstItems.SelectedIndex].Text : "");
				if (intSelectedGroup == 0)
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code  ='" + YY + "' " + StrSearch + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				else
				{
					strSQLItems = "SELECT * FROM Inventory WHERE Code  ='" + YY + "' " + StrSearch + " AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
				}
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					FillRoutine(ref rsItems);
					ReShowFlag = true;
					cmdSearch.Enabled = true;
				}
				else
				{
					vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
					vs1.Rows = 1;
					MessageBox.Show("There are no decals in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				rsSearch = rsItems;
				rsItems.Reset();
			}
			else if (MotorVehicle.Statics.ListType == "BOOSTER")
			{
			}
			else if (MotorVehicle.Statics.ListType == "SPECIALREGPERMIT")
			{
			}
		}

		public void lstItems_DblClick()
		{
			lstItems_DoubleClick(lstItems, new System.EventArgs());
		}

		private void lstPlates_DoubleClick(object sender, System.EventArgs e)
		{
			vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
			vs1.Rows = 1;
			//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
			if (intSelectedGroup == 0)
			{
				strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid((lstPlates.SelectedIndex >= 0 ? lstPlates.Items[lstPlates.SelectedIndex].Text : ""), 1, 2) + "' " + StrSearch + " ORDER by InventoryDate, " + strSortBy + "suffix, Prefix + convert(nvarchar, Number) + Suffix";
			}
			else
			{
				strSQL = "SELECT * FROM Inventory WHERE substring(Code,1,1) = 'P' AND substring(Code,4,2) = '" + Strings.Mid((lstPlates.SelectedIndex >= 0 ? lstPlates.Items[lstPlates.SelectedIndex].Text : ""), 1, 2) + "' " + StrSearch + " AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "suffix, Prefix + convert(nvarchar, Number) + Suffix";
			}
			rsPlates.OpenRecordset(strSQL);
			if (rsPlates.EndOfFile() != true && rsPlates.BeginningOfFile() != true)
			{
				rsPlates.MoveLast();
				rsPlates.MoveFirst();
				FillRoutine(ref rsPlates);
				ReShowFlag = true;
				cmdSearch.Enabled = true;
			}
			else
			{
				//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
				//FC:FINAL:DDU:#2269 - show all subitems of FCListView as well
				string text = "";
				if (lstPlates.SelectedIndex >= 0)
				{
					foreach (ListViewItem.ListViewSubItem item in lstPlates.Items[lstPlates.SelectedIndex].SubItems)
					{
						text += item.Text + " ";
					}
				}
				MessageBox.Show("There are no  " + text + "  plates in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			//App.DoEvents();
			// rsPlates.Close
			rsPlates.Reset();
		}

		public void lstPlates_DblClick()
		{
			lstPlates_DoubleClick(lstPlates, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
        {
            cmdReturn_Click();
        }

        private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			StrSearch = "";
			if (ReShowFlag)
			{
				ReShowInformation();
			}
		}

		private void optAvailable_CheckedChanged(object sender, System.EventArgs e)
		{
			StrSearch = "AND Status = 'A'";
			if (ReShowFlag)
			{
				ReShowInformation();
			}
		}

		private void optDeleted_CheckedChanged(object sender, System.EventArgs e)
		{
			StrSearch = "AND Status = 'D'";
			if (ReShowFlag)
			{
				ReShowInformation();
			}
		}

		private void optHeld_CheckedChanged(object sender, System.EventArgs e)
		{
			StrSearch = "AND Status = 'H'";
			if (ReShowFlag)
			{
				ReShowInformation();
			}
		}

		private void optInventoryType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// this selection decides what type of object to get from inventory. Some have multiple type and will fill
			// another list box to select from. Others will fill the Display box at the bottom.
			// FillRoutine() fills the display box
			// GetListItems() fills the list box(es) on the right
			vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
			vs1.Rows = 1;
			// bottom of screen
			lstItems.Clear();
			// top right - used to select Months - not Sorted
			lstPlates.Clear();
			// used to select Plate types - Sorted
			switch (Index)
			{
				case 0:
					{
						MotorVehicle.Statics.ListType = "MVR";
						if (intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE Code = 'MXS00' " + StrSearch + " ORDER by InventoryDate, " + strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE Code = 'MXS00' " + StrSearch + " AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
						}
						rsItems.OpenRecordset(strSQLItems);
						if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
						{
							rsItems.MoveLast();
							rsItems.MoveFirst();
							FillRoutine(ref rsItems);
							ReShowFlag = true;
							cmdSearch.Enabled = true;
						}
						else
						{
							MessageBox.Show("There are no MVR3 forms in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						rsSearch = rsItems;
						rsItems.Reset();
						lblItemType.Text = "";
						break;
					}
				case 1:
					{
						MotorVehicle.Statics.ListType = "P";
						GetListItems(MotorVehicle.Statics.ListType);
						lblItemType.Text = "Plate Type";
						ReShowFlag = false;
						cmdSearch.Enabled = false;
						break;
					}
				case 2:
					{
						MotorVehicle.Statics.ListType = "DY";
						GetListItems(MotorVehicle.Statics.ListType);
						lblItemType.Text = "Sticker Year";
						ReShowFlag = false;
						cmdSearch.Enabled = false;
						break;
					}
				case 3:
					{
						MotorVehicle.Statics.ListType = "DM";
						GetListItems(MotorVehicle.Statics.ListType);
						lblItemType.Text = "Sticker Month";
						ReShowFlag = false;
						cmdSearch.Enabled = false;
						break;
					}
				case 4:
					{
						MotorVehicle.Statics.ListType = "SY";
						GetListItems(MotorVehicle.Statics.ListType);
						lblItemType.Text = "Sticker Year";
						ReShowFlag = false;
						cmdSearch.Enabled = false;
						break;
					}
				case 5:
					{
						MotorVehicle.Statics.ListType = "SM";
						GetListItems(MotorVehicle.Statics.ListType);
						lblItemType.Text = "Sticker Month";
						rsItems.Reset();
						ReShowFlag = false;
						cmdSearch.Enabled = false;
						break;
					}
				case 6:
					{
						MotorVehicle.Statics.ListType = "DECAL";
						GetListItems(MotorVehicle.Statics.ListType);
						lblItemType.Text = "Decals Year";
						ReShowFlag = false;
						cmdSearch.Enabled = false;
						break;
					}
				case 7:
					{
						MotorVehicle.Statics.ListType = "BOOSTER";
						if (intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2)  = 'BX' " + StrSearch + " ORDER by InventoryDate, " + strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2)  = 'BX' " + StrSearch + " AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
						}
						rsItems.OpenRecordset(strSQLItems);
						if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
						{
							rsItems.MoveLast();
							rsItems.MoveFirst();
							FillRoutine(ref rsItems);
							ReShowFlag = true;
							cmdSearch.Enabled = true;
						}
						else
						{
							MessageBox.Show("There are no booster plates in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						rsSearch = rsItems;
						rsItems.Reset();
						lblItemType.Text = "";
						break;
					}
				case 8:
					{
						MotorVehicle.Statics.ListType = "SPECIALREGPERMIT";
						if (intSelectedGroup == 0)
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2)  = 'RX' " + StrSearch + " ORDER by InventoryDate, " + strSortBy + "Number";
						}
						else
						{
							strSQLItems = "SELECT * FROM Inventory WHERE substring(Code,1,2)  = 'RX' " + StrSearch + " AND [Group] = " + FCConvert.ToString(intSelectedGroup) + " ORDER by InventoryDate, " + strSortBy + "Number";
						}
						rsItems.OpenRecordset(strSQLItems);
						if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
						{
							rsItems.MoveLast();
							rsItems.MoveFirst();
							FillRoutine(ref rsItems);
							ReShowFlag = true;
							cmdSearch.Enabled = true;
						}
						else
						{
							MessageBox.Show("There are no MVR10 forms in inventory.", "No Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						rsSearch = rsItems;
						rsItems.Reset();
						lblItemType.Text = "";
						break;
					}
				case 9:
					{
						MotorVehicle.Statics.ListType = "COMBINATION";
						GetListItems(MotorVehicle.Statics.ListType);
						lblItemType.Text = "Sticker Year";
						ReShowFlag = false;
						cmdSearch.Enabled = false;
						break;
					}
				default:
					{
						// do nothing
						break;
					}
			}
			//end switch
		}

		public void optInventoryType_Click(int Index)
		{
			optInventoryType_CheckedChanged(Index, cmbInventoryType, new System.EventArgs());
		}

		private void optInventoryType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbInventoryType.SelectedIndex;
			optInventoryType_CheckedChanged(index, sender, e);
		}

		public void FillRoutine(ref clsDRWrapper rs)
		{
			bool first = false;
			int lngStart;
			int lngStop;
			int lngRow;
			int lngFirst = 0;
			int lngLast = 0;
			int intGroup = 0;
			string strprefix = "";
			string strSuffix = "";
			int Difference = 0;
			int counter = 0;
			string strStatus = "";
			string strInventoryOpID = "";
			string strOriginalPrefix = "";
			string strStartPlate = "";
			DateTime datInventoryDate;
			vs1.Clear(FCGrid.ClearWhereSettings.flexClearScrollable);
			vs1.Rows = 1;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				lngFirst = FCConvert.ToInt32(rs.Get_Fields_Int32("Number"));
				lngLast = FCConvert.ToInt32(rs.Get_Fields_Int32("Number"));
				intGroup = FCConvert.ToInt32(rs.Get_Fields_Int16("Group"));
				strStatus = FCConvert.ToString(rs.Get_Fields_String("Status"));
				strInventoryOpID = FCConvert.ToString(rs.Get_Fields_String("OpID"));
				datInventoryDate = Convert.ToDateTime(rs.Get_Fields_DateTime("InventoryDate"));
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("prefix")) == "")
					strprefix = "";
				else
					strprefix = FCConvert.ToString(rs.Get_Fields_String("prefix"));
				if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("Suffix")) == "")
					strSuffix = "";
				else
					strSuffix = FCConvert.ToString(rs.Get_Fields_String("Suffix"));
				strOriginalPrefix = strprefix;
				strStartPlate = FCConvert.ToString(rs.Get_Fields_String("FormattedInventory"));
				counter = 1;
				while (!rs.EndOfFile())
				{
					if (strprefix.Length > rs.Get_Fields_String("prefix").Length && Strings.Right(strprefix, 1) == "0")
					{
						strprefix = Strings.Left(strprefix, (strprefix.Length - (strprefix.Length - rs.Get_Fields_String("prefix").Length)));
					}
					if (FCConvert.ToInt32(rs.Get_Fields_Int32("Number")) == lngLast && (strprefix == FCConvert.ToString(rs.Get_Fields_String("prefix")) + "" && strSuffix == FCConvert.ToString(rs.Get_Fields_String("Suffix")) + "") && FCConvert.ToInt32(rs.Get_Fields_Int16("Group")) == intGroup && strStatus == FCConvert.ToString(rs.Get_Fields_String("Status")) && strInventoryOpID == FCConvert.ToString(rs.Get_Fields_String("OpID")) && datInventoryDate.ToOADate() == Convert.ToDateTime(rs.Get_Fields_DateTime("InventoryDate")).ToOADate())
					{
						if (lngLast == lngFirst)
						{
							first = true;
						}
						else
						{
							first = false;
						}
						lngLast += 1;
					}
					else if (FCConvert.ToInt32(rs.Get_Fields_Int32("Number")) == lngLast && fecherFoundation.Strings.Trim(rs.Get_Fields_String("prefix")) == "" && fecherFoundation.Strings.Trim(rs.Get_Fields_String("Suffix")) == "" && FCConvert.ToInt32(rs.Get_Fields_Int16("Group")) == intGroup && strStatus == FCConvert.ToString(rs.Get_Fields_String("Status")) && strInventoryOpID == FCConvert.ToString(rs.Get_Fields_String("OpID")) && datInventoryDate.ToOADate() == Convert.ToDateTime(rs.Get_Fields_DateTime("InventoryDate")).ToOADate())
					{
						if (lngLast == lngFirst)
						{
							first = true;
						}
						else
						{
							first = false;
						}
						lngLast += 1;
					}
					else
					{
						rs.MovePrevious();
						Difference = (FCConvert.ToInt32(rs.Get_Fields_Int32("Number")) - lngFirst) + 1;
						vs1.Rows += 1;
						vs1.TextMatrix(counter, TypeCol, Strings.Mid(rs.Get_Fields("Code"), 4, 2));
						vs1.TextMatrix(counter, DateCol, Strings.Format(rs.Get_Fields_DateTime("InventoryDate"), "MM/dd/yy"));
						vs1.TextMatrix(counter, OpIDCol, rs.Get_Fields_String("OpID"));
						vs1.TextMatrix(counter, LowCol, strStartPlate);
						// strOriginalPrefix & lngFirst & .Fields("Suffix")
						vs1.TextMatrix(counter, HighCol, rs.Get_Fields_String("FormattedInventory"));
						// .Fields("prefix") & .Fields("Number") & .Fields("Suffix")
						vs1.TextMatrix(counter, StatusCol, rs.Get_Fields_String("Status"));
						vs1.TextMatrix(counter, CountCol, FCConvert.ToString(Difference));
						if (FCConvert.ToInt32(rs.Get_Fields_Int16("Group")) == 0)
						{
							vs1.TextMatrix(counter, GroupCol, "ALL");
						}
						else
						{
							vs1.TextMatrix(counter, GroupCol, rs.Get_Fields_Int16("Group"));
						}
						counter += 1;
						rs.MoveNext();
						lngFirst = FCConvert.ToInt32(rs.Get_Fields_Int32("Number"));
						lngLast = FCConvert.ToInt32(rs.Get_Fields_Int32("Number"));
						intGroup = FCConvert.ToInt32(rs.Get_Fields_Int16("Group"));
						strStatus = FCConvert.ToString(rs.Get_Fields_String("Status"));
						strInventoryOpID = FCConvert.ToString(rs.Get_Fields_String("OpID"));
						datInventoryDate = Convert.ToDateTime(rs.Get_Fields_DateTime("InventoryDate"));
						if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("prefix")) == "")
							strprefix = "";
						else
							strprefix = FCConvert.ToString(rs.Get_Fields_String("prefix"));
						if (fecherFoundation.Strings.Trim(rs.Get_Fields_String("Suffix")) == "")
							strSuffix = "";
						else
							strSuffix = FCConvert.ToString(rs.Get_Fields_String("Suffix"));
						strOriginalPrefix = strprefix;
						strStartPlate = FCConvert.ToString(rs.Get_Fields_String("FormattedInventory"));
						rs.MovePrevious();
					}
					rs.MoveNext();
				}
				rs.MovePrevious();
				Difference = 0;
				if (FCConvert.ToInt32(rs.Get_Fields_Int32("Number")) != lngFirst || rs.RecordCount() == 1 || first)
				{
					Difference = (FCConvert.ToInt32(rs.Get_Fields_Int32("Number")) - lngFirst) + 1;
					vs1.Rows += 1;
					vs1.TextMatrix(counter, TypeCol, Strings.Mid(rs.Get_Fields("Code"), 4, 2));
					vs1.TextMatrix(counter, DateCol, Strings.Format(rs.Get_Fields_DateTime("InventoryDate"), "MM/dd/yy"));
					vs1.TextMatrix(counter, OpIDCol, rs.Get_Fields_String("OpID"));
					vs1.TextMatrix(counter, LowCol, strStartPlate);
					// strOriginalPrefix & lngFirst & .Fields("Suffix")
					vs1.TextMatrix(counter, HighCol, rs.Get_Fields_String("FormattedInventory"));
					// .Fields("prefix") & .Fields("Number") & .Fields("Suffix")
					vs1.TextMatrix(counter, StatusCol, rs.Get_Fields_String("Status"));
					vs1.TextMatrix(counter, CountCol, FCConvert.ToString(Difference));
					if (FCConvert.ToInt32(rs.Get_Fields_Int16("Group")) == 0)
					{
						vs1.TextMatrix(counter, GroupCol, "ALL");
					}
					else
					{
						vs1.TextMatrix(counter, GroupCol, rs.Get_Fields_Int16("Group"));
					}
					counter += 1;
				}
			}
			vs1.Rows = counter;
			Form_Resize();
		}

		public void GetListItems(string ListType)
		{
			int xx = 0;
			if (ListType == "P")
			{
				lstPlates.Visible = true;
				lstItems.Visible = false;
				//FC:FINAL:MSH - i.issue #1881: initialize list for displaying all data (WiseJ doesn't display tabs and spaces)
				lstPlates.Columns.Add("");
				int listWidth = lstPlates.Width;
				lstPlates.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.18);
				lstPlates.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.75);
				lstPlates.GridLineStyle = GridLineStyle.None;
				strSQLItems = "SELECT * FROM InventoryMaster WHERE Type = 'P' ORDER BY MonthYearClass";
				rsItems.OpenRecordset(strSQLItems);
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					while (!rsItems.EndOfFile())
					{
						lstPlates.AddItem(rsItems.Get_Fields_String("MonthYearClass") + "\t" + rsItems.Get_Fields_String("Description"));
						rsItems.MoveNext();
					}
				}
				rsItems.Reset();
			}
			else if (ListType == "MVR")
			{
			}
			else if (ListType == "DY")
			{
				for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
				{
					Stickers[fnx] = 0;
				}
				lstPlates.Visible = false;
				lstItems.Visible = true;
				strSQLItems = "SELECT DISTINCT Code FROM Inventory WHERE substring(Code,1,3) = 'SYD'";
				rsItems.OpenRecordset(strSQLItems);
				fnx = 0;
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					while (!rsItems.EndOfFile())
					{
						Stickers[fnx] = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2))) > 90 ? FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) : (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)) + 100));
						fnx += 1;
						rsItems.MoveNext();
					}
					lstItems.Clear();
					MaxElements = fnx;
					Jims_Sort_Array(ref Stickers);
					fnx = 0;
					for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
					{
						if (Stickers[fnx] != 0)
						{
							lstItems.AddItem(Strings.Right(FCConvert.ToString(Stickers[fnx]), 2));
						}
					}
					// fnx
					xx = FCConvert.ToInt32(Math.Round(Conversion.Val(lstItems.Items[lstItems.Items.Count - 1].Text)));
					while (!(xx == (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2)) + 3)))
					{
						xx += 1;
						lstItems.AddItem((modGlobalRoutines.PadToString(xx, 2)).ToString());
					}
				}
				else
				{
					lstItems.AddItem(Strings.Format(Strings.Format(DateTime.Today, "yy"), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 1), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 2), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 3), "00"));
				}
				rsItems.Reset();
			}
			else if (ListType == "COMBINATION")
			{
				for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
				{
					Stickers[fnx] = 0;
				}
				lstPlates.Visible = false;
				lstItems.Visible = true;
				strSQLItems = "SELECT DISTINCT Code FROM Inventory WHERE substring(Code,1,3) = 'SYC'";
				rsItems.OpenRecordset(strSQLItems);
				fnx = 0;
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					while (!rsItems.EndOfFile())
					{
						Stickers[fnx] = (FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) > 90 ? FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) : (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)) + 100));
						fnx += 1;
						rsItems.MoveNext();
					}
					lstItems.Clear();
					MaxElements = fnx;
					Jims_Sort_Array(ref Stickers);
					fnx = 0;
					for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
					{
						if (Stickers[fnx] != 0)
						{
							lstItems.AddItem(Strings.Right(FCConvert.ToString(Stickers[fnx]), 2));
						}
					}
					// fnx
					xx = FCConvert.ToInt32(Math.Round(Conversion.Val(lstItems.Items[lstItems.Items.Count - 1].Text)));
					while (!(xx == (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2)) + 3)))
					{
						xx += 1;
						lstItems.AddItem((modGlobalRoutines.PadToString(xx, 2)).ToString());
					}
				}
				else
				{
					lstItems.AddItem(Strings.Format(Strings.Format(DateTime.Today, "yy"), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 1), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 2), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 3), "00"));
				}
				rsItems.Reset();
			}
			else if (ListType == "SY")
			{
				for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
				{
					Stickers[fnx] = 0;
				}
				lstPlates.Visible = false;
				lstItems.Visible = true;
				strSQLItems = "SELECT DISTINCT Code FROM Inventory WHERE substring(Code,1,3)  = 'SYS'   ";
				rsItems.OpenRecordset(strSQLItems);
				fnx = 0;
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					while (!rsItems.EndOfFile())
					{
						Stickers[fnx] = (FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) > 90 ? FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) : (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)) + 100));
						fnx += 1;
						rsItems.MoveNext();
					}
					lstItems.Clear();
					MaxElements = fnx;
					Jims_Sort_Array(ref Stickers);
					fnx = 0;
					for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
					{
						if (Stickers[fnx] != 0)
						{
							lstItems.AddItem(Strings.Right(FCConvert.ToString(Stickers[fnx]), 2));
						}
					}
					// fnx
					xx = FCConvert.ToInt32(Math.Round(Conversion.Val(lstItems.Items[lstItems.Items.Count - 1].Text)));
					while (!(xx == (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2)) + 3)))
					{
						xx += 1;
						lstItems.AddItem((modGlobalRoutines.PadToString(xx, 2)).ToString());
					}
				}
				else
				{
					lstItems.AddItem(Strings.Format(Strings.Format(DateTime.Today, "yy"), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 1), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 2), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 3), "00"));
				}
				//lstItems.HeightOriginal = 1271;
				rsItems.Reset();
			}
			else if ((ListType == "SM") || (ListType == "DM"))
			{
				lstPlates.Visible = false;
				lstItems.Visible = true;
				lstItems.AddItem("January");
				lstItems.AddItem("February");
				lstItems.AddItem("March");
				lstItems.AddItem("April");
				lstItems.AddItem("May");
				lstItems.AddItem("June");
				lstItems.AddItem("July");
				lstItems.AddItem("August");
				lstItems.AddItem("September");
				lstItems.AddItem("October");
				lstItems.AddItem("November");
				lstItems.AddItem("December");
				//lstItems.HeightOriginal = 3000;
			}
			else if (ListType == "DECAL")
			{
				for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
				{
					Stickers[fnx] = 0;
				}
				lstPlates.Visible = false;
				lstItems.Visible = true;
				strSQLItems = "SELECT DISTINCT Code FROM Inventory WHERE substring(Code,1,2)  = 'DY'";
				rsItems.OpenRecordset(strSQLItems);
				fnx = 0;
				// 
				if (rsItems.EndOfFile() != true && rsItems.BeginningOfFile() != true)
				{
					rsItems.MoveLast();
					rsItems.MoveFirst();
					while (!rsItems.EndOfFile())
					{
						Stickers[fnx] = (FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) > 90 ? FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)))) : (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(rsItems.Get_Fields("Code")), 2)) + 100));
						// 
						fnx += 1;
						// 
						// lstItems.AddItem (Mid$(.fields("Code"), 4, 2))
						rsItems.MoveNext();
					}
					lstItems.Clear();
					MaxElements = fnx;
					xx = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(FCConvert.ToString(DateTime.Today.Year), 2))));
					while (!(xx == (FCConvert.ToInt32(Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2)) + 4)))
					{
						for (fnx = 0; fnx <= MaxElements - 1; fnx++)
						{
							if (Stickers[fnx] == xx + 100)
							{
								goto CheckNext;
							}
						}
						Stickers[MaxElements] = xx + 100;
						MaxElements += 1;
						CheckNext:
						;
						xx += 1;
					}
					Jims_Sort_Array(ref Stickers);
					fnx = 0;
					for (fnx = 0; fnx <= Information.UBound(Stickers, 1); fnx++)
					{
						if (Stickers[fnx] != 0)
						{
							lstItems.AddItem(Strings.Right(FCConvert.ToString(Stickers[fnx]), 2));
						}
					}
					// fnx
				}
				else
				{
					lstItems.AddItem(Strings.Format(Strings.Format(DateTime.Today, "yy"), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 1), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 2), "00"));
					lstItems.AddItem(Strings.Format((FCConvert.ToInt32(Strings.Format(DateTime.Today, "yy")) + 3), "00"));
					// MsgBox "There are no Decals in Inventory", vbOKOnly, "No Inventory"
				}
				//lstItems.HeightOriginal = 1271;
				rsItems.Reset();
			}
			else if (ListType == "BOOSTER")
			{
			}
			else if (ListType == "SPECIALREGPERMIT")
			{
			}
		}

		private int[] Jims_Sort_Array(ref int[] Stickers)
		{
			int[] Jims_Sort_Array = null;
			int I;
			int j;
			// vbPorter upgrade warning: Min As int	OnWriteFCConvert.ToInt32(
			int Min;
			int MinIndex;
			int temp = 0;
			// 
			Min = Stickers[0];
			MinIndex = 0;
			// 
			for (I = 1; I <= MaxElements - 1; I++)
			{
				Min = Stickers[I - 1];
				MinIndex = I - 1;
				for (j = I; j <= MaxElements - 1; j++)
				{
					if (Stickers[j] < Stickers[MinIndex])
					{
						Min = Stickers[j];
						MinIndex = j;
					}
				}
				temp = Stickers[I - 1];
				Stickers[I - 1] = Stickers[MinIndex];
				Stickers[MinIndex] = temp;
			}
			Jims_Sort_Array = Stickers;
			return Jims_Sort_Array;
		}

		private void optIssues_CheckedChanged(object sender, System.EventArgs e)
		{
			StrSearch = "AND Status = 'I'";
			if (ReShowFlag)
			{
				ReShowInformation();
			}
		}

		private void optPending_CheckedChanged(object sender, System.EventArgs e)
		{
			StrSearch = "AND Status = 'P'";
			if (ReShowFlag)
			{
				ReShowInformation();
			}
		}

		private void optVoided_CheckedChanged(object sender, System.EventArgs e)
		{
			StrSearch = "AND Status = 'V'";
			if (ReShowFlag)
			{
				ReShowInformation();
			}
		}

		private void ReShowInformation()
		{
			if (cmbInventoryType.Text == "MVR3s")
			{
				optInventoryType_Click(0);
			}
			else if (cmbInventoryType.Text == "Boosters")
			{
				optInventoryType_Click(7);
			}
			else if (cmbInventoryType.Text == "MVR10s")
			{
				optInventoryType_Click(8);
			}
			else if (cmbInventoryType.Text == "Plates")
			{
				lstPlates_DblClick();
			}
			else if (cmbInventoryType.Text == "Double Year Stickers" || cmbInventoryType.Text == "Double Month Stickers" || cmbInventoryType.Text == "Single Year Stickers" || cmbInventoryType.Text == "Single Month Stickers" || cmbInventoryType.Text == "Decals" || cmbInventoryType.Text == "Combination Stickers")
			{
				lstItems_DblClick();
			}
			else
			{
				// do nothing
			}
		}

		private void optSelection_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			intSelectedGroup = Index;
			ReShowInformation();
		}

		private void optSelection_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSelection.SelectedIndex;
			optSelection_CheckedChanged(index, sender, e);
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			if (vs1.MouseRow <= 0)
			{
				if (vs1.MouseCol == OpIDCol)
				{
					strSortBy = "OPID, ";
				}
				else if (vs1.MouseCol == TypeCol)
				{
					strSortBy = "Mid(Code, 4, 2), ";
				}
				else if (vs1.MouseCol == DateCol)
				{
					strSortBy = "";
				}
				else if (vs1.MouseCol == LowCol || vs1.MouseCol == HighCol || vs1.MouseCol == CountCol)
				{
					strSortBy = "";
				}
				else if (vs1.MouseCol == StatusCol)
				{
					strSortBy = "Status, ";
				}
				else if (vs1.MouseCol == GroupCol)
				{
					strSortBy = "GroupID, ";
				}
				if (cmbInventoryType.Text == "MVR3s")
				{
					optInventoryType_Click(0);
				}
				else if (cmbInventoryType.Text == "Plates")
				{
					lstPlates_DblClick();
				}
				else if (cmbInventoryType.Text == "Double Year Stickers" || cmbInventoryType.Text == "Double Month Stickers" || cmbInventoryType.Text == "Single Year Stickers" || cmbInventoryType.Text == "Single Month Stickers" || cmbInventoryType.Text == "Decals" || cmbInventoryType.Text == "Combination Stickers")
				{
					lstItems_DblClick();
				}
				else if (cmbInventoryType.Text == "Boosters")
				{
					optInventoryType_Click(7);
				}
				else if (cmbInventoryType.Text == "MVR10s")
				{
					optInventoryType_Click(8);
				}
				else
				{
					// do nothing
				}
			}
		}

		private void cmbVoided_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbVoided.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbVoided.Text == "Available")
			{
				optAvailable_CheckedChanged(sender, e);
			}
			else if (cmbVoided.Text == "Issued")
			{
				optIssues_CheckedChanged(sender, e);
			}
			else if (cmbVoided.Text == "Voided")
			{
				optVoided_CheckedChanged(sender, e);
			}
			else if (cmbVoided.Text == "Held")
			{
				optHeld_CheckedChanged(sender, e);
			}
			else if (cmbVoided.Text == "Pending")
			{
				optPending_CheckedChanged(sender, e);
			}
			else if (cmbVoided.Text == "Deleted")
			{
				optDeleted_CheckedChanged(sender, e);
			}
		}
	}
}
