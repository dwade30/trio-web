﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTAgentInventory.
	/// </summary>
	partial class rptLTTAgentInventory
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLTTAgentInventory));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.subIssued = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.subLost = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.subExtended = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.subIssued,
				this.subLost,
				this.subExtended
			});
			this.Detail.Height = 0.4375F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport2,
				this.Label1,
				this.lblPage
			});
			this.PageHeader.Height = 1.270833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.3125F;
			this.SubReport2.Width = 7.46875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.78125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label1.Text = "**** LTT AGENT INVENTORY REPORT****";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 3.625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.5625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.0625F;
			this.lblPage.Width = 0.9375F;
			// 
			// subIssued
			// 
			this.subIssued.CloseBorder = false;
			this.subIssued.Height = 0.125F;
			this.subIssued.Left = 0F;
			this.subIssued.Name = "subIssued";
			this.subIssued.Report = null;
			this.subIssued.Top = 0F;
			this.subIssued.Width = 7.46875F;
			// 
			// subLost
			// 
			this.subLost.CloseBorder = false;
			this.subLost.Height = 0.125F;
			this.subLost.Left = 0F;
			this.subLost.Name = "subLost";
			this.subLost.Report = null;
			this.subLost.Top = 0.15625F;
			this.subLost.Width = 7.46875F;
			// 
			// subExtended
			// 
			this.subExtended.CloseBorder = false;
			this.subExtended.Height = 0.125F;
			this.subExtended.Left = 0F;
			this.subExtended.Name = "subExtended";
			this.subExtended.Report = null;
			this.subExtended.Top = 0.3125F;
			this.subExtended.Width = 7.46875F;
			// 
			// rptLTTAgentInventory
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subIssued;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subLost;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subExtended;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
