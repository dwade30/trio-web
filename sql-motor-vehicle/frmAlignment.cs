﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	public partial class frmAlignment : BaseForm
	{
		public frmAlignment()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmAlignment InstancePtr
		{
			get
			{
				return (frmAlignment)Sys.GetInstance(typeof(frmAlignment));
			}
		}

		protected frmAlignment _InstancePtr = null;
		//=========================================================
		bool blnFirstPrint;

		private void cmdAlignDone_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.Statics.AlignmentOK = "Y";
			frmAlignment.InstancePtr.Close();
		}

		private void cmdCancelPrint_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strSQL = "";
			MotorVehicle.Statics.AlignmentOK = "C";
			frmAlignment.InstancePtr.Close();
			if (!MotorVehicle.Statics.gboolFromBatchRegistration)
			{
				if (MotorVehicle.Statics.OldMVR3Number != 0)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", MotorVehicle.Statics.OldMVR3Number);
					frmPreview.InstancePtr.txtMVR3Input.Text = FCConvert.ToString(MotorVehicle.Statics.OldMVR3Number);
					strSQL = "SELECT * FROM InventoryAdjustments WHERE InventoryType = 'MXS00' AND Low = '" + FCConvert.ToString(MotorVehicle.Statics.OldMVR3Number) + "' AND AdjustmentCode = 'V'";
					rsTemp.OpenRecordset(strSQL);
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						rsTemp.MoveLast();
						rsTemp.MoveFirst();
						rsTemp.Delete();
						rsTemp.Update();
					}
				}
			}
			else
			{
			}
		}

		private void cmdPrintXX_Click(object sender, System.EventArgs e)
		{
            if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
            {
                return;
            }
			int intRes;
			clsPrinterFunctions clsCTAPrinter = new clsPrinterFunctions();
			string strTemp;
			// open printer class function
            clsCTAPrinter.OpenPrinterObject(MotorVehicle.Statics.gboolFromLongTermDataEntry ? MotorVehicle.Statics.MVR10PrinterName : MotorVehicle.Statics.MVR3PrinterName);

            strTemp = Strings.StrDup(38, " ") + "XX";
			
			if (!blnFirstPrint)
			{
				clsCTAPrinter.PrinterReverse(2);
			}
			else
			{
				blnFirstPrint = false;
				clsCTAPrinter.PrintTextWithCr("");
				clsCTAPrinter.PrinterForward(2);
				clsCTAPrinter.PrinterReverse(2);
			}
			clsCTAPrinter.PrintTextWithCr(strTemp);
			clsCTAPrinter.PrinterForward(2);
			// close connection to printer
			//clsCTAPrinter.CloseSession();
		}

		private void frmAlignment_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAlignment properties;
			//frmAlignment.ScaleWidth	= 5910;
			//frmAlignment.ScaleHeight	= 4125;
			//frmAlignment.LinkTopic	= "Form1";
			//End Unmaped Properties
			// MsgBox "Loading Xs form"
			blnFirstPrint = true;
			modGlobalFunctions.SetTRIOColors(this, false);
			SetCustomFormColors();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			// MsgBox "End Load function"
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			clsPrinterFunctions clsCTAPrinter = new clsPrinterFunctions();
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				e.Cancel = true;
				return;
			}

            if (blnFirstPrint) return;

            // open printer class function
            clsCTAPrinter.OpenPrinterObject(MotorVehicle.Statics.gboolFromLongTermDataEntry ? MotorVehicle.Statics.MVR10PrinterName : MotorVehicle.Statics.MVR3PrinterName);
            clsCTAPrinter.PrinterReverse(2);
            //clsCTAPrinter.CloseSession();
        }

		private void SetCustomFormColors()
		{
			this.BackColor = ColorTranslator.FromOle(0x808080);
		}
	}
}
