﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptCashReports.
	/// </summary>
	public partial class rptCashReports : BaseSectionReport
	{
		public rptCashReports()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "State Reports";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCashReports InstancePtr
		{
			get
			{
				return (rptCashReports)Sys.GetInstance(typeof(rptCashReports));
			}
		}

		protected rptCashReports _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		int MaxLines;
		int counter;
		string strTemp;
		string[] strArray;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (counter > Information.UBound(strArray, 1))
			{
				eArgs.EOF = true;
				return;
			}
			fldLine.Text = FCConvert.ToString(strArray[counter]);
			if (fldLine.Text == "______________________________  PAGE BREAK  ______________________________")
			{
				Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
				counter += 1;
				if (counter > Information.UBound(strArray, 1))
				{
					eArgs.EOF = true;
					return;
				}
				fldLine.Text = FCConvert.ToString(((object[])strArray)[counter]);
			}
			else
			{
				Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
			counter += 1;
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			strTemp = frmReport.InstancePtr.rtf1.Text;
			strArray = Strings.Split(strTemp, "\r\n", -1, CompareConstants.vbBinaryCompare);
			counter = 0;
		}
	}
}
