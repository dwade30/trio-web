//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubReportHeadingLongTerm.
	/// </summary>
	public partial class rptSubReportHeadingLongTerm : BaseSectionReport
	{
		public rptSubReportHeadingLongTerm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptSubReportHeadingLongTerm InstancePtr
		{
			get
			{
				return (rptSubReportHeadingLongTerm)Sys.GetInstance(typeof(rptSubReportHeadingLongTerm));
			}
		}

		protected rptSubReportHeadingLongTerm _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSubReportHeadingLongTerm	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirst;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirst)
			{
				blnFirst = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirst = true;
			//this.Printer.RenderMode = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtTownCounty As object	OnWrite(string)
			// vbPorter upgrade warning: txtAgent As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txtProcess As object	OnWrite(string)
			// vbPorter upgrade warning: txtAuthType As object	OnWrite(string)
			// vbPorter upgrade warning: txtDateReceived As object	OnWrite(string)
			// vbPorter upgrade warning: txtVersion As object	OnWrite(string)
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rs1 = new clsDRWrapper();
			string strProcessDate = "";
			string strSQL = "";
			int lngPK1 = 0;
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (MotorVehicle.Statics.AllowRentals)
			{
				strLevel += "/RENTAL";
			}
			if (frmReportLongTerm.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReportLongTerm.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReportLongTerm.InstancePtr.cboStart.Text) == true)
					{
						if (frmReportLongTerm.InstancePtr.cboEnd.Text == frmReportLongTerm.InstancePtr.cboStart.Text)
						{
							strSQL = "SELECT * FROM PeriodCloseoutLongTerm WHERE IssueDate = '" + frmReportLongTerm.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseoutLongTerm WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReportLongTerm.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReportLongTerm.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReportLongTerm.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReportLongTerm.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReportLongTerm.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				txtMuni.Text = rs1.Get_Fields_String("ReportTown");
				txtTownCounty.Text = Strings.Format(rs1.Get_Fields_String("ResidenceCode"), "00000");
				txtAgent.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs1.Get_Fields_String("ReportAgent")));
				txtPhone.Text = rs1.Get_Fields_String("ReportTelephone");
				txtDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
				txtProcess.Text = strProcessDate;
				txtAuthType.Text = strLevel;
				txtDateReceived.Text = "___/___/___";
				txtVersion.Text = Strings.Format(App.Major, "00") + "." + App.Minor + "." + App.Revision;
			}
			rs.DisposeOf();
			rs1.DisposeOf();
		}

		private void rptSubReportHeadingLongTerm_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptSubReportHeadingLongTerm properties;
			//rptSubReportHeadingLongTerm.Caption	= "ActiveReport1";
			//rptSubReportHeadingLongTerm.Left	= 0;
			//rptSubReportHeadingLongTerm.Top	= 0;
			//rptSubReportHeadingLongTerm.Width	= 12465;
			//rptSubReportHeadingLongTerm.Height	= 5490;
			//rptSubReportHeadingLongTerm.StartUpPosition	= 3;
			//rptSubReportHeadingLongTerm.SectionData	= "rptSubReportHeadingLongTerm.dsx":0000;
			//End Unmaped Properties
		}
	}
}
