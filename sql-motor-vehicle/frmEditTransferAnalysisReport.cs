﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmEditTransferAnalysisReport : BaseForm
	{
		public frmEditTransferAnalysisReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEditTransferAnalysisReport InstancePtr
		{
			get
			{
				return (frmEditTransferAnalysisReport)Sys.GetInstance(typeof(frmEditTransferAnalysisReport));
			}
		}

		protected frmEditTransferAnalysisReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();

		private void frmEditTransferAnalysisReport_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmEditTransferAnalysisReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditTransferAnalysisReport properties;
			//frmEditTransferAnalysisReport.FillStyle	= 0;
			//frmEditTransferAnalysisReport.ScaleWidth	= 5880;
			//frmEditTransferAnalysisReport.ScaleHeight	= 3810;
			//frmEditTransferAnalysisReport.LinkTopic	= "Form2";
			//frmEditTransferAnalysisReport.PaletteMode	= 1  'UseZOrder;
			//rtfTransferComment properties;
			//rtfTransferComment.TextRTF	= $"frmEditTransferAnalysisReport.frx":058A;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			rtfTransferComment.Text = FCConvert.ToString(MotorVehicle.GetMVVariable("TransferCommentLine"));
		}

		private void frmEditTransferAnalysisReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.UpdateMVVariable("TransferCommentLine", fecherFoundation.Strings.Trim(rtfTransferComment.Text));
			Close();
		}
	}
}
