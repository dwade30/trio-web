﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptProcessFinancial.
	/// </summary>
	partial class rptProcessFinancial
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptProcessFinancial));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPlate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblVin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtVin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTrans = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldExciseTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMerchantFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTrans)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMerchantFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDate,
				this.txtPlate,
				this.txtVin,
				this.txtAmount,
				this.txtType
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblDate,
				this.lblPlate,
				this.lblVin,
				this.lblType,
				this.lblAmount,
				this.Line1,
				this.Label1,
				this.Label32,
				this.Label9,
				this.Label10,
				this.Label33
			});
			this.PageHeader.Height = 0.7916667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTrans,
				this.lblTotal,
				this.SubReport1,
				this.Label34,
				this.Label35,
				this.Line2,
				this.Label36,
				this.fldExciseTotal,
				this.fldNet,
				this.fldMerchantFee
			});
			this.GroupFooter1.Height = 0.9375F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 0.125F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblDate.Text = "--Date--";
			this.lblDate.Top = 0.5625F;
			this.lblDate.Width = 0.5625F;
			// 
			// lblPlate
			// 
			this.lblPlate.Height = 0.1875F;
			this.lblPlate.HyperLink = null;
			this.lblPlate.Left = 1F;
			this.lblPlate.Name = "lblPlate";
			this.lblPlate.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblPlate.Text = "--Plate--";
			this.lblPlate.Top = 0.5625F;
			this.lblPlate.Width = 0.5625F;
			// 
			// lblVin
			// 
			this.lblVin.Height = 0.1875F;
			this.lblVin.HyperLink = null;
			this.lblVin.Left = 1.9375F;
			this.lblVin.Name = "lblVin";
			this.lblVin.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblVin.Text = "VIN ------------------";
			this.lblVin.Top = 0.5625F;
			this.lblVin.Width = 1.1875F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 3.6875F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblType.Text = "Type";
			this.lblType.Top = 0.5625F;
			this.lblType.Width = 0.375F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 4.6875F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.5625F;
			this.lblAmount.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.75F;
			this.Line1.Width = 6.40625F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 6.46875F;
			this.Line1.Y1 = 0.75F;
			this.Line1.Y2 = 0.75F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "TRIO Software  -  Rapid Renewal Financials";
			this.Label1.Top = 0F;
			this.Label1.Width = 6.5F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label32.Text = "Label32";
			this.Label32.Top = 0.1875F;
			this.Label32.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.1875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 5.1875F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label33.Text = "Label33";
			this.Label33.Top = 0F;
			this.Label33.Width = 1.3125F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 0.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.txtDate.Text = "Field1";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 0.75F;
			// 
			// txtPlate
			// 
			this.txtPlate.Height = 0.1875F;
			this.txtPlate.Left = 1F;
			this.txtPlate.Name = "txtPlate";
			this.txtPlate.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.txtPlate.Text = null;
			this.txtPlate.Top = 0F;
			this.txtPlate.Width = 1F;
			// 
			// txtVin
			// 
			this.txtVin.Height = 0.1875F;
			this.txtVin.Left = 2F;
			this.txtVin.Name = "txtVin";
			this.txtVin.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.txtVin.Text = "Field1";
			this.txtVin.Top = 0F;
			this.txtVin.Width = 1.625F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1875F;
			this.txtAmount.Left = 4.5625F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.txtAmount.Text = "Field1";
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.6875F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.1875F;
			this.txtType.Left = 3.6875F;
			this.txtType.Name = "txtType";
			this.txtType.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.txtType.Text = "Field1";
			this.txtType.Top = 0F;
			this.txtType.Width = 0.875F;
			// 
			// lblTrans
			// 
			this.lblTrans.Height = 0.1875F;
			this.lblTrans.HyperLink = null;
			this.lblTrans.Left = 1.375F;
			this.lblTrans.Name = "lblTrans";
			this.lblTrans.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblTrans.Text = "Label1";
			this.lblTrans.Top = 0.0625F;
			this.lblTrans.Width = 2F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 3.5625F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTotal.Text = "Label1";
			this.lblTotal.Top = 0.0625F;
			this.lblTotal.Width = 1.6875F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.09375F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.84375F;
			this.SubReport1.Width = 6.46875F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.1875F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 1.8125F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label34.Text = "Excise";
			this.Label34.Top = 0.34375F;
			this.Label34.Width = 0.8125F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 2.71875F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label35.Text = "Merchant Fee";
			this.Label35.Top = 0.34375F;
			this.Label35.Width = 0.96875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.75F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.53125F;
			this.Line2.Width = 3.09375F;
			this.Line2.X1 = 1.75F;
			this.Line2.X2 = 4.84375F;
			this.Line2.Y1 = 0.53125F;
			this.Line2.Y2 = 0.53125F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.1875F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 3.75F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label36.Text = "Net";
			this.Label36.Top = 0.34375F;
			this.Label36.Width = 1.03125F;
			// 
			// fldExciseTotal
			// 
			this.fldExciseTotal.Height = 0.1875F;
			this.fldExciseTotal.Left = 1.8125F;
			this.fldExciseTotal.Name = "fldExciseTotal";
			this.fldExciseTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldExciseTotal.Text = "Field1";
			this.fldExciseTotal.Top = 0.5625F;
			this.fldExciseTotal.Width = 0.8125F;
			// 
			// fldNet
			// 
			this.fldNet.Height = 0.1875F;
			this.fldNet.Left = 3.75F;
			this.fldNet.Name = "fldNet";
			this.fldNet.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldNet.Text = "Field1";
			this.fldNet.Top = 0.5625F;
			this.fldNet.Width = 1.03125F;
			// 
			// fldMerchantFee
			// 
			this.fldMerchantFee.Height = 0.1875F;
			this.fldMerchantFee.Left = 2.71875F;
			this.fldMerchantFee.Name = "fldMerchantFee";
			this.fldMerchantFee.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldMerchantFee.Text = "Field1";
			this.fldMerchantFee.Top = 0.5625F;
			this.fldMerchantFee.Width = 0.96875F;
			// 
			// rptProcessFinancial
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTrans)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExciseTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMerchantFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtVin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPlate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVin;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTrans;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExciseTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMerchantFee;
	}
}
