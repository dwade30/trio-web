﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptChangesList.
	/// </summary>
	public partial class rptChangesList : BaseSectionReport
	{
		public rptChangesList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Reminder List";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptChangesList InstancePtr
		{
			get
			{
				return (rptChangesList)Sys.GetInstance(typeof(rptChangesList));
			}
		}

		protected rptChangesList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptChangesList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		int counter;
		bool blnFirstRecord;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			counter = 1;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			blnFirstRecord = true;
			Label33.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label32.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldPlate.Text = MotorVehicle.Statics.strPrePrintPlate[counter - 1];
			fldSystemInfo.Text = MotorVehicle.Statics.strSystemInfo[counter - 1];
			fldStateInfo.Text = MotorVehicle.Statics.strStateInfo[counter - 1];
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label34.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				counter += 1;
				if (counter > MotorVehicle.Statics.intChanges)
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void rptChangesList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptChangesList properties;
			//rptChangesList.Caption	= "Reminder List";
			//rptChangesList.Icon	= "rptChangesList.dsx":0000";
			//rptChangesList.Left	= 0;
			//rptChangesList.Top	= 0;
			//rptChangesList.Width	= 11880;
			//rptChangesList.Height	= 8595;
			//rptChangesList.StartUpPosition	= 3;
			//rptChangesList.SectionData	= "rptChangesList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
