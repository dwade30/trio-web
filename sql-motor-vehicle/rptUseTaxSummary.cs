//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptUseTaxSummary.
	/// </summary>
	public partial class rptUseTaxSummary : BaseSectionReport
	{
		public rptUseTaxSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Use Tax Summary Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptUseTaxSummary_ReportEnd;
		}

        private void RptUseTaxSummary_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
			rsAM.DisposeOf();
        }

        public static rptUseTaxSummary InstancePtr
		{
			get
			{
				return (rptUseTaxSummary)Sys.GetInstance(typeof(rptUseTaxSummary));
			}
		}

		protected rptUseTaxSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUseTaxSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL = "";
		int lngPK1;
		int lngPK2;
		int intHeadingNumber;
		//clsDRWrapper rs2 = new clsDRWrapper();
		double MoneyTotal;
		int total;
		int FeeCount;
		int NoFeeCount;
		clsDRWrapper rsAM = new clsDRWrapper();
		bool boolFirstPart;
		bool boolRsDone;
		bool boolSecondPart;
		bool boolThirdPart;
		int intPageNumber;
		bool boolData;
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			txtType.Text = "";
			txtFee.Text = "";
			txtClass.Text = "";
			txtPlate.Text = "";
			txtDT.Text = "";
			txtCustomer.Text = "";
			txtOPID.Text = "";
			if (blnFirstRecord == true)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else if (!rsAM.EndOfFile())
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//clsDRWrapper rs1 = new clsDRWrapper();
			//string strVendorID = "";
			//string strMuni = "";
			//string strTownCode = "";
			//string strAgent = "";
			//string strVersion = "";
			//string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			//Application.DoEvents();
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							// Dim lngPK1 As Long
							strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate = #" + frmReport.InstancePtr.cboEnd.Text + "#";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields("Key"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE Key = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSQL);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			}
			blnFirstRecord = true;
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				// kk02242017 tromv-1242  Added check for SalesTaxExempt to pick up when Use Tax form wasn't done and exclude Dealer sales tax
				strSQL = "SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 < 1 AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND DealerSalesTax <> 1 AND (UseTaxDone = 1 or SalesTax <> 0 or SalesTaxExempt = 1) ORDER BY DateUpdated";
			}
			else
			{
				strSQL = "SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " And OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND DealerSalesTax <> 1 AND (UseTaxDone = 1 or SalesTax <> 0 or SalesTaxExempt = 1) ORDER BY DateUpdated";
			}
			rsAM.OpenRecordset(strSQL);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				rsAM.MoveLast();
				rsAM.MoveFirst();
			}
			FeeCount = 0;
			NoFeeCount = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				boolData = true;
				if (FCConvert.ToDouble(rsAM.Get_Fields("SalesTax")) > 0)
				{
					FeeCount += 1;
					txtType.Text = Strings.Format("PAID", "!@@@@@@@@@@");
				}
				else
				{
					NoFeeCount += 1;
					txtType.Text = Strings.Format("NO FEE", "!@@@@@@@@@@");
				}
				total += 1;
				MoneyTotal += rsAM.Get_Fields_Double("SalesTax") > 0 ? rsAM.Get_Fields_Double("SalesTax") : 0;
				txtFee.Text = Strings.Format(rsAM.Get_Fields_Double("SalesTax") > 0 ? rsAM.Get_Fields_Double("SalesTax") : 0, "00.00");
				txtClass.Text = Strings.Format(rsAM.Get_Fields_String("Class"), "!@@");
				txtPlate.Text = Strings.Format(rsAM.Get_Fields_String("plate"), "!@@@@@@@@");
				txtDT.Text = Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy");
				txtCustomer.Text = Strings.Format(MotorVehicle.GetPartyName(rsAM.Get_Fields_Int32("PartyID1")), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
				txtOPID.Text = Strings.Format(rsAM.Get_Fields_String("OpID"), "@@@");
				rsAM.MoveNext();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (boolData == true)
			{
				lblTotals.Visible = true;
				lblType.Visible = true;
				lblUnits.Visible = true;
				lblDollars.Visible = true;
				linTotals.Visible = true;
				lblPaid.Visible = true;
				lblNoFee.Visible = true;
				fldPaidUnits.Visible = true;
				fldPaidAmount.Visible = true;
				fldNoFeeUnits.Visible = true;
				fldNoFeeAmount.Visible = true;
				fldPaidAmount.Text = Strings.Format(MoneyTotal, "#,#00.00");
				fldPaidUnits.Text = Strings.Format(FeeCount, "@@@@");
				fldNoFeeAmount.Text = Strings.Format(0, "#,#00.00");
				fldNoFeeUnits.Text = Strings.Format(NoFeeCount, "@@@@");
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeading();
			intPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (boolData == false)
			{
				txtNoInfo.Text = "No Information";
			}
		}
	}
}
