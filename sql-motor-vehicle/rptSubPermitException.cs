//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubPermitException.
	/// </summary>
	public partial class rptSubPermitException : BaseSectionReport
	{
		public rptSubPermitException()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Exception Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptSubPermitException_ReportEnd;
		}

        private void RptSubPermitException_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
        }

        public static rptSubPermitException InstancePtr
		{
			get
			{
				return (rptSubPermitException)Sys.GetInstance(typeof(rptSubPermitException));
			}
		}

		protected rptSubPermitException _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSubPermitException	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rs.EndOfFile();
			}
			else
			{
				rs.MoveNext();
				eArgs.EOF = rs.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				rs.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID < 1 AND Type = '6PM' ORDER BY ExceptionReportDate");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM ExceptionReport WHERE PeriodCloseoutID > " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK1) + " AND PeriodCloseoutID <= " + FCConvert.ToString(rptNewExceptionReport.InstancePtr.lngPK2) + " AND Type = '6PM' ORDER BY ExceptionReportDate");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rptNewExceptionReport.InstancePtr.boolData = true;
			}
			else
			{
				this.Close();
			}
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldClassPlate As object	OnWrite(string)
			// vbPorter upgrade warning: fldFee As object	OnWrite(string)
			rptNewExceptionReport.InstancePtr.PermitTotal += 1;
			fldDate.Text = Strings.Format(rs.Get_Fields_DateTime("ExceptionReportDate"), "MM/dd/yyyy");
			fldPermitNumber.Text = rs.Get_Fields_String("PermitNumber");
			fldPermitType.Text = rs.Get_Fields_String("PermitType");
			fldClassPlate.Text = rs.Get_Fields_String("newclass") + " " + rs.Get_Fields_String("NewPlate");
			fldApplicantName.Text = rs.Get_Fields_String("ApplicantName");
			rptNewExceptionReport.InstancePtr.PermitMoneyTotal =
				rptNewExceptionReport.InstancePtr.PermitMoneyTotal + rs.Get_Fields("Fee");
			fldFee.Text = Strings.Format(rs.Get_Fields("Fee"), "#,##0.00");
			fldOpID.Text = rs.Get_Fields_String("OpID");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotal As object	OnWrite
			fldTotalCount.Text = FCConvert.ToString(rptNewExceptionReport.InstancePtr.PermitTotal);
			fldTotalMoney.Text = rptNewExceptionReport.InstancePtr.PermitMoneyTotal.ToString("#,##0.00");
		}
	}
}
