﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptMVR3Laser.
	/// </summary>
	partial class rptMVR3Laser
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMVR3Laser));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMileage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldMileage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCounty = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblRegNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblClass = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRegNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldColor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldStyle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAxles = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldNetWeight = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRegWeight = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFuel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblVIN = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMake = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblModel = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblColor = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblStyle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTires = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAxles = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNetWeight = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblRegWeight = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFuel = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblRegistrant = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldOwner1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDOB1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldOwner2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDOB2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLessor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldUnit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDOTNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldOwner3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDOB3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldResidenceCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldResidenceState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldResidenceCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMonthSticker = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYearSticker = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldResidenceAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblEffDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDOB = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLessor = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblUnit = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDotNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMailAddr = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLegalRes = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLegalResCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMonthSticker = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYearSticker = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldValidation1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldValidation2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldValidation4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldExpires = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblExpireDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldEffective = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPriorTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInsurance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblValidation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldInsurance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldPriorTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldValidation5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblUserId = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldUserId = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRegType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblSOSURL = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFee1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFee32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFee32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRegistrationDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRegistrationCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMileage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMileage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAxles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNetWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAxles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNetWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegistrant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOwner1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDOB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOwner2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDOB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLessor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDOTNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOwner3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDOB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldResidenceCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldResidenceState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldResidenceCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMonthSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYearSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldResidenceAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEffDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLessor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailAddr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLegalRes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLegalResCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonthSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYearSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldValidation1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldValidation2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldValidation4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExpires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExpireDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEffective)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPriorTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInsurance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValidation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInsurance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPriorTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldValidation5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUserId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldUserId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSOSURL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegistrationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegistrationCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Shape3,
            this.Shape4,
            this.Shape1,
            this.lblTitle,
            this.lblMileage,
            this.fldMileage,
            this.fldCounty,
            this.lblRegNumber,
            this.lblClass,
            this.fldClass,
            this.fldRegNumber,
            this.fldVIN,
            this.fldYear,
            this.fldMake,
            this.fldModel,
            this.fldColor,
            this.fldStyle,
            this.fldTires,
            this.fldAxles,
            this.fldNetWeight,
            this.fldRegWeight,
            this.fldFuel,
            this.lblVIN,
            this.lblYear,
            this.lblMake,
            this.lblModel,
            this.lblColor,
            this.lblStyle,
            this.lblTires,
            this.lblAxles,
            this.lblNetWeight,
            this.lblRegWeight,
            this.lblFuel,
            this.lblRegistrant,
            this.fldOwner1,
            this.fldDOB1,
            this.fldOwner2,
            this.fldDOB2,
            this.fldLessor,
            this.fldUnit,
            this.fldDOTNumber,
            this.fldOwner3,
            this.fldDOB3,
            this.fldAddress1,
            this.fldCityStateZip,
            this.fldResidenceCity,
            this.fldResidenceState,
            this.fldResidenceCode,
            this.fldMonthSticker,
            this.fldYearSticker,
            this.fldAddress2,
            this.fldResidenceAddress,
            this.lblEffDate,
            this.lblDOB,
            this.lblLessor,
            this.lblUnit,
            this.lblDotNo,
            this.lblMailAddr,
            this.lblLegalRes,
            this.lblLegalResCode,
            this.lblMonthSticker,
            this.lblYearSticker,
            this.fldValidation1,
            this.fldValidation2,
            this.fldValidation4,
            this.fldExpires,
            this.lblExpireDate,
            this.fldEffective,
            this.lblPriorTitle,
            this.lblInsurance,
            this.lblValidation,
            this.fldInsurance,
            this.Line1,
            this.Line2,
            this.Line3,
            this.Line4,
            this.Line5,
            this.Line6,
            this.Line7,
            this.Line8,
            this.Line9,
            this.Line11,
            this.Line12,
            this.Line13,
            this.Line14,
            this.Line15,
            this.Line16,
            this.Line17,
            this.Line18,
            this.fldPriorTitle,
            this.fldValidation5,
            this.Line19,
            this.Line20,
            this.Line21,
            this.lblUserId,
            this.fldUserId,
            this.fldRegType,
            this.lblSOSURL,
            this.lblFee1,
            this.fldFee1,
            this.lblFee2,
            this.fldFee2,
            this.lblFee3,
            this.fldFee3,
            this.lblFee4,
            this.fldFee4,
            this.lblFee5,
            this.fldFee5,
            this.lblFee6,
            this.fldFee6,
            this.lblFee7,
            this.fldFee7,
            this.lblFee8,
            this.fldFee8,
            this.lblFee9,
            this.fldFee9,
            this.lblFee10,
            this.fldFee10,
            this.lblFee11,
            this.fldFee11,
            this.lblFee12,
            this.fldFee12,
            this.lblFee13,
            this.fldFee13,
            this.lblFee14,
            this.fldFee14,
            this.lblFee15,
            this.fldFee15,
            this.lblFee16,
            this.fldFee16,
            this.lblFee17,
            this.fldFee17,
            this.lblFee18,
            this.fldFee18,
            this.lblFee19,
            this.fldFee19,
            this.lblFee20,
            this.fldFee20,
            this.lblFee21,
            this.fldFee21,
            this.lblFee22,
            this.fldFee22,
            this.lblFee23,
            this.fldFee23,
            this.lblFee24,
            this.fldFee24,
            this.lblFee25,
            this.fldFee25,
            this.lblFee26,
            this.fldFee26,
            this.lblFee27,
            this.fldFee27,
            this.lblFee28,
            this.fldFee28,
            this.lblFee29,
            this.fldFee29,
            this.lblFee30,
            this.fldFee30,
            this.lblFee31,
            this.fldFee31,
            this.lblFee32,
            this.fldFee32,
            this.fldRegistrationDate,
            this.fldRegistrationCode});
            this.Detail.Height = 3.625F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // Shape3
            // 
            this.Shape3.Height = 1.350694F;
            this.Shape3.Left = 0F;
            this.Shape3.Name = "Shape3";
            this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape3.Top = 0.4583333F;
            this.Shape3.Width = 4.628472F;
            // 
            // Shape4
            // 
            this.Shape4.Height = 2.635417F;
            this.Shape4.Left = 4.625F;
            this.Shape4.Name = "Shape4";
            this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape4.Top = 0.4583333F;
            this.Shape4.Width = 3.3125F;
            // 
            // Shape1
            // 
            this.Shape1.Height = 0.375F;
            this.Shape1.Left = 6.114583F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
            this.Shape1.Top = 0.06597222F;
            this.Shape1.Width = 1.822917F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.1875F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 2.5F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-weight: bold";
            this.lblTitle.Text = "State of Maine Vehicle Registration";
            this.lblTitle.Top = 0.03472222F;
            this.lblTitle.Width = 2.3125F;
            // 
            // lblMileage
            // 
            this.lblMileage.Height = 0.172F;
            this.lblMileage.HyperLink = null;
            this.lblMileage.Left = 5F;
            this.lblMileage.Name = "lblMileage";
            this.lblMileage.Style = "font-size: 9pt; text-align: right; vertical-align: top";
            this.lblMileage.Text = "Mileage";
            this.lblMileage.Top = 0.056F;
            this.lblMileage.Width = 0.5F;
            // 
            // fldMileage
            // 
            this.fldMileage.CanGrow = false;
            this.fldMileage.Height = 0.172F;
            this.fldMileage.Left = 5.5F;
            this.fldMileage.Name = "fldMileage";
            this.fldMileage.Style = "vertical-align: top";
            this.fldMileage.Text = "28,750";
            this.fldMileage.Top = 0.056F;
            this.fldMileage.Width = 0.5F;
            // 
            // fldCounty
            // 
            this.fldCounty.Height = 0.1875F;
            this.fldCounty.Left = 0.125F;
            this.fldCounty.Name = "fldCounty";
            this.fldCounty.Style = "font-family: \'Courier New\'; ddo-char-set: 0";
            this.fldCounty.Text = "CUMBERLAND CNTY";
            this.fldCounty.Top = 0.1875F;
            this.fldCounty.Width = 2F;
            // 
            // lblRegNumber
            // 
            this.lblRegNumber.Height = 0.125F;
            this.lblRegNumber.HyperLink = null;
            this.lblRegNumber.Left = 6.569445F;
            this.lblRegNumber.Name = "lblRegNumber";
            this.lblRegNumber.Style = "font-size: 5pt";
            this.lblRegNumber.Text = "REGISTRATION NUMBER";
            this.lblRegNumber.Top = 0.0625F;
            this.lblRegNumber.Width = 1F;
            // 
            // lblClass
            // 
            this.lblClass.Height = 0.125F;
            this.lblClass.HyperLink = null;
            this.lblClass.Left = 6.125F;
            this.lblClass.Name = "lblClass";
            this.lblClass.Style = "font-size: 5pt; vertical-align: top";
            this.lblClass.Text = "CLASS";
            this.lblClass.Top = 0.0625F;
            this.lblClass.Width = 0.375F;
            // 
            // fldClass
            // 
            this.fldClass.Height = 0.125F;
            this.fldClass.Left = 6.1875F;
            this.fldClass.Name = "fldClass";
            this.fldClass.Style = "font-family: \'Courier New\'; font-size: 12pt; text-align: center; vertical-align: " +
    "middle";
            this.fldClass.Text = "CO";
            this.fldClass.Top = 0.2083333F;
            this.fldClass.Width = 0.3125F;
            // 
            // fldRegNumber
            // 
            this.fldRegNumber.Height = 0.125F;
            this.fldRegNumber.Left = 6.635417F;
            this.fldRegNumber.Name = "fldRegNumber";
            this.fldRegNumber.Style = "font-family: \'Courier New\'; font-size: 12pt; text-align: left; vertical-align: mi" +
    "ddle";
            this.fldRegNumber.Text = "315-ATZ";
            this.fldRegNumber.Top = 0.2083333F;
            this.fldRegNumber.Width = 1.111111F;
            // 
            // fldVIN
            // 
            this.fldVIN.Height = 0.1875F;
            this.fldVIN.Left = 0.0625F;
            this.fldVIN.Name = "fldVIN";
            this.fldVIN.Style = "font-family: \'Courier New\'; font-size: 11pt";
            this.fldVIN.Text = "1FTEX1EM1EFC58292";
            this.fldVIN.Top = 0.5625F;
            this.fldVIN.Width = 1.8125F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 2.0625F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Courier New\'; font-size: 11pt; text-align: center";
            this.fldYear.Text = "2014";
            this.fldYear.Top = 0.5625F;
            this.fldYear.Width = 0.4375F;
            // 
            // fldMake
            // 
            this.fldMake.Height = 0.1875F;
            this.fldMake.Left = 2.625F;
            this.fldMake.Name = "fldMake";
            this.fldMake.Style = "font-family: \'Courier New\'; font-size: 11pt; text-align: center";
            this.fldMake.Text = "FORD";
            this.fldMake.Top = 0.5625F;
            this.fldMake.Width = 0.5F;
            // 
            // fldModel
            // 
            this.fldModel.Height = 0.1875F;
            this.fldModel.Left = 3.25F;
            this.fldModel.Name = "fldModel";
            this.fldModel.Style = "font-family: \'Courier New\'; font-size: 11pt; text-align: center";
            this.fldModel.Text = "F150";
            this.fldModel.Top = 0.5625F;
            this.fldModel.Width = 0.6875F;
            // 
            // fldColor
            // 
            this.fldColor.Height = 0.1875F;
            this.fldColor.Left = 4.0625F;
            this.fldColor.Name = "fldColor";
            this.fldColor.Style = "font-family: \'Courier New\'; font-size: 11pt";
            this.fldColor.Text = "BK/WH";
            this.fldColor.Top = 0.5625F;
            this.fldColor.Width = 0.5F;
            // 
            // fldStyle
            // 
            this.fldStyle.CanGrow = false;
            this.fldStyle.Height = 0.1875F;
            this.fldStyle.Left = 4.6875F;
            this.fldStyle.MultiLine = false;
            this.fldStyle.Name = "fldStyle";
            this.fldStyle.Style = "font-family: \'Courier New\'; font-size: 11pt; text-align: center; ddo-char-set: 0";
            this.fldStyle.Text = "00";
            this.fldStyle.Top = 0.5625F;
            this.fldStyle.Width = 0.3125F;
            // 
            // fldTires
            // 
            this.fldTires.CanGrow = false;
            this.fldTires.Height = 0.1875F;
            this.fldTires.Left = 5.1875F;
            this.fldTires.MultiLine = false;
            this.fldTires.Name = "fldTires";
            this.fldTires.Style = "font-family: \'Courier New\'; font-size: 11pt; text-align: left; ddo-char-set: 0";
            this.fldTires.Text = "4";
            this.fldTires.Top = 0.5625F;
            this.fldTires.Width = 0.3125F;
            // 
            // fldAxles
            // 
            this.fldAxles.CanGrow = false;
            this.fldAxles.Height = 0.1875F;
            this.fldAxles.Left = 5.625F;
            this.fldAxles.MultiLine = false;
            this.fldAxles.Name = "fldAxles";
            this.fldAxles.Style = "font-family: \'Courier New\'; font-size: 11pt; text-align: left; ddo-char-set: 0";
            this.fldAxles.Text = "2";
            this.fldAxles.Top = 0.5625F;
            this.fldAxles.Width = 0.3125F;
            // 
            // fldNetWeight
            // 
            this.fldNetWeight.CanGrow = false;
            this.fldNetWeight.Height = 0.1875F;
            this.fldNetWeight.Left = 6.090278F;
            this.fldNetWeight.MultiLine = false;
            this.fldNetWeight.Name = "fldNetWeight";
            this.fldNetWeight.Style = "font-family: \'Courier New\'; font-size: 11pt; ddo-char-set: 0";
            this.fldNetWeight.Text = "999999";
            this.fldNetWeight.Top = 0.5625F;
            this.fldNetWeight.Width = 0.625F;
            // 
            // fldRegWeight
            // 
            this.fldRegWeight.CanGrow = false;
            this.fldRegWeight.Height = 0.1875F;
            this.fldRegWeight.Left = 6.9375F;
            this.fldRegWeight.MultiLine = false;
            this.fldRegWeight.Name = "fldRegWeight";
            this.fldRegWeight.Style = "font-family: \'Courier New\'; font-size: 11pt; ddo-char-set: 0";
            this.fldRegWeight.Text = "10000";
            this.fldRegWeight.Top = 0.5625F;
            this.fldRegWeight.Width = 0.625F;
            // 
            // fldFuel
            // 
            this.fldFuel.CanGrow = false;
            this.fldFuel.Height = 0.1875F;
            this.fldFuel.Left = 7.638889F;
            this.fldFuel.MultiLine = false;
            this.fldFuel.Name = "fldFuel";
            this.fldFuel.Style = "font-family: \'Courier New\'; font-size: 11pt; text-align: left; ddo-char-set: 0";
            this.fldFuel.Text = "G";
            this.fldFuel.Top = 0.5625F;
            this.fldFuel.Width = 0.1875F;
            // 
            // lblVIN
            // 
            this.lblVIN.Height = 0.125F;
            this.lblVIN.HyperLink = null;
            this.lblVIN.Left = 0F;
            this.lblVIN.Name = "lblVIN";
            this.lblVIN.Style = "font-size: 5pt; vertical-align: top";
            this.lblVIN.Text = "VIN";
            this.lblVIN.Top = 0.4513889F;
            this.lblVIN.Width = 0.25F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.125F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 2.0625F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-size: 5pt";
            this.lblYear.Text = "YEAR";
            this.lblYear.Top = 0.4513889F;
            this.lblYear.Width = 0.375F;
            // 
            // lblMake
            // 
            this.lblMake.Height = 0.125F;
            this.lblMake.HyperLink = null;
            this.lblMake.Left = 2.534722F;
            this.lblMake.Name = "lblMake";
            this.lblMake.Style = "font-size: 5pt";
            this.lblMake.Text = "MAKE";
            this.lblMake.Top = 0.4513889F;
            this.lblMake.Width = 0.375F;
            // 
            // lblModel
            // 
            this.lblModel.Height = 0.125F;
            this.lblModel.HyperLink = null;
            this.lblModel.Left = 3.1875F;
            this.lblModel.Name = "lblModel";
            this.lblModel.Style = "font-size: 5pt";
            this.lblModel.Text = "MODEL";
            this.lblModel.Top = 0.4513889F;
            this.lblModel.Width = 0.375F;
            // 
            // lblColor
            // 
            this.lblColor.Height = 0.125F;
            this.lblColor.HyperLink = null;
            this.lblColor.Left = 4.0625F;
            this.lblColor.Name = "lblColor";
            this.lblColor.Style = "font-size: 5pt";
            this.lblColor.Text = "COLOR";
            this.lblColor.Top = 0.4513889F;
            this.lblColor.Width = 0.375F;
            // 
            // lblStyle
            // 
            this.lblStyle.Height = 0.125F;
            this.lblStyle.HyperLink = null;
            this.lblStyle.Left = 4.625F;
            this.lblStyle.Name = "lblStyle";
            this.lblStyle.Style = "font-size: 5pt";
            this.lblStyle.Text = "STYLE";
            this.lblStyle.Top = 0.4513889F;
            this.lblStyle.Width = 0.375F;
            // 
            // lblTires
            // 
            this.lblTires.Height = 0.125F;
            this.lblTires.HyperLink = null;
            this.lblTires.Left = 5.090278F;
            this.lblTires.Name = "lblTires";
            this.lblTires.Style = "font-size: 5pt";
            this.lblTires.Text = "TIRES";
            this.lblTires.Top = 0.4513889F;
            this.lblTires.Width = 0.25F;
            // 
            // lblAxles
            // 
            this.lblAxles.Height = 0.125F;
            this.lblAxles.HyperLink = null;
            this.lblAxles.Left = 5.559722F;
            this.lblAxles.Name = "lblAxles";
            this.lblAxles.Style = "font-size: 5pt";
            this.lblAxles.Text = "AXLES";
            this.lblAxles.Top = 0.4513889F;
            this.lblAxles.Width = 0.3772783F;
            // 
            // lblNetWeight
            // 
            this.lblNetWeight.Height = 0.125F;
            this.lblNetWeight.HyperLink = null;
            this.lblNetWeight.Left = 6.027778F;
            this.lblNetWeight.Name = "lblNetWeight";
            this.lblNetWeight.Style = "font-size: 5pt";
            this.lblNetWeight.Text = "NET WEIGHT";
            this.lblNetWeight.Top = 0.4513889F;
            this.lblNetWeight.Width = 0.5625F;
            // 
            // lblRegWeight
            // 
            this.lblRegWeight.Height = 0.125F;
            this.lblRegWeight.HyperLink = null;
            this.lblRegWeight.Left = 6.8125F;
            this.lblRegWeight.Name = "lblRegWeight";
            this.lblRegWeight.Style = "font-size: 5pt";
            this.lblRegWeight.Text = "REGISTERED WEIGHT";
            this.lblRegWeight.Top = 0.4513889F;
            this.lblRegWeight.Width = 0.75F;
            // 
            // lblFuel
            // 
            this.lblFuel.Height = 0.125F;
            this.lblFuel.HyperLink = null;
            this.lblFuel.Left = 7.569445F;
            this.lblFuel.Name = "lblFuel";
            this.lblFuel.Style = "font-size: 5pt";
            this.lblFuel.Text = "FUEL";
            this.lblFuel.Top = 0.4513889F;
            this.lblFuel.Width = 0.25F;
            // 
            // lblRegistrant
            // 
            this.lblRegistrant.Height = 0.125F;
            this.lblRegistrant.HyperLink = null;
            this.lblRegistrant.Left = 0F;
            this.lblRegistrant.Name = "lblRegistrant";
            this.lblRegistrant.Style = "font-size: 5pt";
            this.lblRegistrant.Text = "REGISTRANT(s)";
            this.lblRegistrant.Top = 0.7881944F;
            this.lblRegistrant.Width = 1F;
            // 
            // fldOwner1
            // 
            this.fldOwner1.CanGrow = false;
            this.fldOwner1.Height = 0.155F;
            this.fldOwner1.Left = 0.06150004F;
            this.fldOwner1.MultiLine = false;
            this.fldOwner1.Name = "fldOwner1";
            this.fldOwner1.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top; ddo-char-set: 0" +
    "";
            this.fldOwner1.Text = "WILDE, MARTIN D, LESSEE";
            this.fldOwner1.Top = 0.913F;
            this.fldOwner1.Width = 3.5F;
            // 
            // fldDOB1
            // 
            this.fldDOB1.CanGrow = false;
            this.fldDOB1.Height = 0.155F;
            this.fldDOB1.Left = 3.624F;
            this.fldDOB1.MultiLine = false;
            this.fldDOB1.Name = "fldDOB1";
            this.fldDOB1.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top; ddo-char-set: 0" +
    "";
            this.fldDOB1.Text = "02/08/1949";
            this.fldDOB1.Top = 0.913F;
            this.fldDOB1.Width = 0.938F;
            // 
            // fldOwner2
            // 
            this.fldOwner2.CanGrow = false;
            this.fldOwner2.Height = 0.155F;
            this.fldOwner2.Left = 0.063F;
            this.fldOwner2.MultiLine = false;
            this.fldOwner2.Name = "fldOwner2";
            this.fldOwner2.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top; ddo-char-set: 0" +
    "";
            this.fldOwner2.Text = "OWNER, SECOND";
            this.fldOwner2.Top = 1.094F;
            this.fldOwner2.Width = 3.5F;
            // 
            // fldDOB2
            // 
            this.fldDOB2.CanGrow = false;
            this.fldDOB2.Height = 0.155F;
            this.fldDOB2.Left = 3.6255F;
            this.fldDOB2.MultiLine = false;
            this.fldDOB2.Name = "fldDOB2";
            this.fldDOB2.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top; ddo-char-set: 0" +
    "";
            this.fldDOB2.Text = "01/01/1966";
            this.fldDOB2.Top = 1.094F;
            this.fldDOB2.Width = 0.938F;
            // 
            // fldLessor
            // 
            this.fldLessor.CanGrow = false;
            this.fldLessor.Height = 0.1875F;
            this.fldLessor.Left = 0.0625F;
            this.fldLessor.MultiLine = false;
            this.fldLessor.Name = "fldLessor";
            this.fldLessor.Style = "font-family: \'Courier New\'; font-size: 10pt; ddo-char-set: 0";
            this.fldLessor.Text = null;
            this.fldLessor.Top = 1.5625F;
            this.fldLessor.Width = 2.875F;
            // 
            // fldUnit
            // 
            this.fldUnit.CanGrow = false;
            this.fldUnit.Height = 0.1875F;
            this.fldUnit.Left = 2.9375F;
            this.fldUnit.MultiLine = false;
            this.fldUnit.Name = "fldUnit";
            this.fldUnit.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; ddo-char-set: 0";
            this.fldUnit.Text = "WWWWWWW";
            this.fldUnit.Top = 1.5625F;
            this.fldUnit.Width = 0.625F;
            // 
            // fldDOTNumber
            // 
            this.fldDOTNumber.CanGrow = false;
            this.fldDOTNumber.Height = 0.1875F;
            this.fldDOTNumber.Left = 3.625F;
            this.fldDOTNumber.MultiLine = false;
            this.fldDOTNumber.Name = "fldDOTNumber";
            this.fldDOTNumber.Style = "font-family: \'Courier New\'; font-size: 10pt; ddo-char-set: 0";
            this.fldDOTNumber.Text = null;
            this.fldDOTNumber.Top = 1.5625F;
            this.fldDOTNumber.Width = 0.9375F;
            // 
            // fldOwner3
            // 
            this.fldOwner3.CanGrow = false;
            this.fldOwner3.Height = 0.155F;
            this.fldOwner3.Left = 0.062F;
            this.fldOwner3.MultiLine = false;
            this.fldOwner3.Name = "fldOwner3";
            this.fldOwner3.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top; ddo-char-set: 0" +
    "";
            this.fldOwner3.Text = "DBA/THIRD OWNER";
            this.fldOwner3.Top = 1.296F;
            this.fldOwner3.Width = 3.5F;
            // 
            // fldDOB3
            // 
            this.fldDOB3.CanGrow = false;
            this.fldDOB3.Height = 0.155F;
            this.fldDOB3.Left = 3.6245F;
            this.fldDOB3.MultiLine = false;
            this.fldDOB3.Name = "fldDOB3";
            this.fldDOB3.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top; ddo-char-set: 0" +
    "";
            this.fldDOB3.Text = "01/01/1966";
            this.fldDOB3.Top = 1.296F;
            this.fldDOB3.Width = 0.938F;
            // 
            // fldAddress1
            // 
            this.fldAddress1.CanGrow = false;
            this.fldAddress1.Height = 0.155F;
            this.fldAddress1.Left = 0.0625F;
            this.fldAddress1.MultiLine = false;
            this.fldAddress1.Name = "fldAddress1";
            this.fldAddress1.Style = "font-family: \'Courier New\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 0" +
    "";
            this.fldAddress1.Text = "111 ADDRESS RD";
            this.fldAddress1.Top = 1.854167F;
            this.fldAddress1.Width = 2.875F;
            // 
            // fldCityStateZip
            // 
            this.fldCityStateZip.CanGrow = false;
            this.fldCityStateZip.Height = 0.155F;
            this.fldCityStateZip.Left = 0.0625F;
            this.fldCityStateZip.MultiLine = false;
            this.fldCityStateZip.Name = "fldCityStateZip";
            this.fldCityStateZip.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: top; ddo-char-set: 0" +
    "";
            this.fldCityStateZip.Text = "AUGUSTA  ME  04410";
            this.fldCityStateZip.Top = 2.104167F;
            this.fldCityStateZip.Width = 2.875F;
            // 
            // fldResidenceCity
            // 
            this.fldResidenceCity.CanGrow = false;
            this.fldResidenceCity.Height = 0.1875F;
            this.fldResidenceCity.Left = 0.0625F;
            this.fldResidenceCity.MultiLine = false;
            this.fldResidenceCity.Name = "fldResidenceCity";
            this.fldResidenceCity.Style = "font-family: \'Courier New\'; font-size: 10pt; ddo-char-set: 0";
            this.fldResidenceCity.Text = "SQL VERSION";
            this.fldResidenceCity.Top = 2.541667F;
            this.fldResidenceCity.Width = 2.125F;
            // 
            // fldResidenceState
            // 
            this.fldResidenceState.CanGrow = false;
            this.fldResidenceState.Height = 0.1875F;
            this.fldResidenceState.Left = 2.1875F;
            this.fldResidenceState.MultiLine = false;
            this.fldResidenceState.Name = "fldResidenceState";
            this.fldResidenceState.Style = "font-family: \'Courier New\'; font-size: 10pt; ddo-char-set: 0";
            this.fldResidenceState.Text = null;
            this.fldResidenceState.Top = 2.541667F;
            this.fldResidenceState.Width = 0.375F;
            // 
            // fldResidenceCode
            // 
            this.fldResidenceCode.CanGrow = false;
            this.fldResidenceCode.Height = 0.1875F;
            this.fldResidenceCode.Left = 2.1875F;
            this.fldResidenceCode.MultiLine = false;
            this.fldResidenceCode.Name = "fldResidenceCode";
            this.fldResidenceCode.Style = "font-family: \'Courier New\'; font-size: 10pt; ddo-char-set: 0";
            this.fldResidenceCode.Text = null;
            this.fldResidenceCode.Top = 2.354167F;
            this.fldResidenceCode.Width = 0.625F;
            // 
            // fldMonthSticker
            // 
            this.fldMonthSticker.CanGrow = false;
            this.fldMonthSticker.Height = 0.1875F;
            this.fldMonthSticker.Left = 2.0625F;
            this.fldMonthSticker.MultiLine = false;
            this.fldMonthSticker.Name = "fldMonthSticker";
            this.fldMonthSticker.Style = "font-family: \'Courier New\'; font-size: 9pt; vertical-align: bottom; ddo-char-set:" +
    " 0";
            this.fldMonthSticker.Text = "99D 99999999";
            this.fldMonthSticker.Top = 3.09375F;
            this.fldMonthSticker.Width = 1.0625F;
            // 
            // fldYearSticker
            // 
            this.fldYearSticker.CanGrow = false;
            this.fldYearSticker.Height = 0.1875F;
            this.fldYearSticker.Left = 3.3125F;
            this.fldYearSticker.MultiLine = false;
            this.fldYearSticker.Name = "fldYearSticker";
            this.fldYearSticker.Style = "font-family: \'Courier New\'; font-size: 9pt; vertical-align: bottom; ddo-char-set:" +
    " 0";
            this.fldYearSticker.Text = "99D 99999999";
            this.fldYearSticker.Top = 3.09375F;
            this.fldYearSticker.Width = 1.1875F;
            // 
            // fldAddress2
            // 
            this.fldAddress2.CanGrow = false;
            this.fldAddress2.Height = 0.155F;
            this.fldAddress2.Left = 0.0625F;
            this.fldAddress2.MultiLine = false;
            this.fldAddress2.Name = "fldAddress2";
            this.fldAddress2.Style = "font-family: \'Courier New\'; font-size: 10pt; white-space: nowrap; ddo-char-set: 0" +
    "";
            this.fldAddress2.Text = "APT 222";
            this.fldAddress2.Top = 1.979167F;
            this.fldAddress2.Width = 2.875F;
            // 
            // fldResidenceAddress
            // 
            this.fldResidenceAddress.CanGrow = false;
            this.fldResidenceAddress.Height = 0.1875F;
            this.fldResidenceAddress.Left = 0.0625F;
            this.fldResidenceAddress.MultiLine = false;
            this.fldResidenceAddress.Name = "fldResidenceAddress";
            this.fldResidenceAddress.Style = "font-family: \'Courier New\'; font-size: 10pt; ddo-char-set: 0";
            this.fldResidenceAddress.Text = "LEGAL STREET";
            this.fldResidenceAddress.Top = 2.354167F;
            this.fldResidenceAddress.Width = 2.125F;
            // 
            // lblEffDate
            // 
            this.lblEffDate.Height = 0.125F;
            this.lblEffDate.HyperLink = null;
            this.lblEffDate.Left = 0F;
            this.lblEffDate.Name = "lblEffDate";
            this.lblEffDate.Style = "font-size: 7pt";
            this.lblEffDate.Text = "Eff. Date is Validation Date But Not Prior To:";
            this.lblEffDate.Top = 0.3472222F;
            this.lblEffDate.Width = 1.875F;
            // 
            // lblDOB
            // 
            this.lblDOB.Height = 0.125F;
            this.lblDOB.HyperLink = null;
            this.lblDOB.Left = 3.5625F;
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Style = "font-size: 5pt";
            this.lblDOB.Text = "DOB(s)/ID #";
            this.lblDOB.Top = 0.7881944F;
            this.lblDOB.Width = 0.4375F;
            // 
            // lblLessor
            // 
            this.lblLessor.Height = 0.125F;
            this.lblLessor.HyperLink = null;
            this.lblLessor.Left = 0F;
            this.lblLessor.Name = "lblLessor";
            this.lblLessor.Style = "font-size: 5pt";
            this.lblLessor.Text = "LESSOR";
            this.lblLessor.Top = 1.475694F;
            this.lblLessor.Width = 0.4375F;
            // 
            // lblUnit
            // 
            this.lblUnit.Height = 0.125F;
            this.lblUnit.HyperLink = null;
            this.lblUnit.Left = 2.9375F;
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Style = "font-size: 5pt";
            this.lblUnit.Text = "UNIT #";
            this.lblUnit.Top = 1.475694F;
            this.lblUnit.Width = 0.3125F;
            // 
            // lblDotNo
            // 
            this.lblDotNo.Height = 0.125F;
            this.lblDotNo.HyperLink = null;
            this.lblDotNo.Left = 3.5625F;
            this.lblDotNo.Name = "lblDotNo";
            this.lblDotNo.Style = "font-size: 5pt";
            this.lblDotNo.Text = "DOT #";
            this.lblDotNo.Top = 1.475694F;
            this.lblDotNo.Width = 0.4375F;
            // 
            // lblMailAddr
            // 
            this.lblMailAddr.Height = 0.125F;
            this.lblMailAddr.HyperLink = null;
            this.lblMailAddr.Left = 0F;
            this.lblMailAddr.Name = "lblMailAddr";
            this.lblMailAddr.Style = "font-size: 5pt";
            this.lblMailAddr.Text = "MAILING ADDRESS";
            this.lblMailAddr.Top = 1.805556F;
            this.lblMailAddr.Width = 0.75F;
            // 
            // lblLegalRes
            // 
            this.lblLegalRes.Height = 0.125F;
            this.lblLegalRes.HyperLink = null;
            this.lblLegalRes.Left = 0F;
            this.lblLegalRes.Name = "lblLegalRes";
            this.lblLegalRes.Style = "font-size: 5pt";
            this.lblLegalRes.Text = "LEGAL RESIDENCE";
            this.lblLegalRes.Top = 2.256944F;
            this.lblLegalRes.Width = 0.75F;
            // 
            // lblLegalResCode
            // 
            this.lblLegalResCode.Height = 0.125F;
            this.lblLegalResCode.HyperLink = null;
            this.lblLegalResCode.Left = 1.9375F;
            this.lblLegalResCode.Name = "lblLegalResCode";
            this.lblLegalResCode.Style = "font-size: 5pt";
            this.lblLegalResCode.Text = "LEGAL RESIDENCE CODE";
            this.lblLegalResCode.Top = 2.256944F;
            this.lblLegalResCode.Width = 0.875F;
            // 
            // lblMonthSticker
            // 
            this.lblMonthSticker.Height = 0.125F;
            this.lblMonthSticker.HyperLink = null;
            this.lblMonthSticker.Left = 1.506944F;
            this.lblMonthSticker.Name = "lblMonthSticker";
            this.lblMonthSticker.Style = "font-size: 6pt; vertical-align: bottom";
            this.lblMonthSticker.Text = "Sticker #  (M)";
            this.lblMonthSticker.Top = 3.152778F;
            this.lblMonthSticker.Width = 0.5555556F;
            // 
            // lblYearSticker
            // 
            this.lblYearSticker.Height = 0.125F;
            this.lblYearSticker.HyperLink = null;
            this.lblYearSticker.Left = 3.125F;
            this.lblYearSticker.Name = "lblYearSticker";
            this.lblYearSticker.Style = "font-size: 6pt; vertical-align: bottom";
            this.lblYearSticker.Text = "(Y)";
            this.lblYearSticker.Top = 3.15625F;
            this.lblYearSticker.Width = 0.1875F;
            // 
            // fldValidation1
            // 
            this.fldValidation1.CanGrow = false;
            this.fldValidation1.Height = 0.155F;
            this.fldValidation1.Left = 2.9375F;
            this.fldValidation1.MultiLine = false;
            this.fldValidation1.Name = "fldValidation1";
            this.fldValidation1.Style = "font-size: 7pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.fldValidation1.Text = "VALIDATED REGISTRATION";
            this.fldValidation1.Top = 2.0625F;
            this.fldValidation1.Width = 1.688F;
            // 
            // fldValidation2
            // 
            this.fldValidation2.CanGrow = false;
            this.fldValidation2.Height = 0.1597503F;
            this.fldValidation2.Left = 2.937F;
            this.fldValidation2.MultiLine = false;
            this.fldValidation2.Name = "fldValidation2";
            this.fldValidation2.Style = "font-size: 7pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.fldValidation2.Text = "AUGUSTA";
            this.fldValidation2.Top = 2.177F;
            this.fldValidation2.Width = 1.688F;
            // 
            // fldValidation4
            // 
            this.fldValidation4.CanGrow = false;
            this.fldValidation4.Height = 0.125F;
            this.fldValidation4.Left = 2.937F;
            this.fldValidation4.MultiLine = false;
            this.fldValidation4.Name = "fldValidation4";
            this.fldValidation4.Style = "font-size: 7pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.fldValidation4.Top = 2.394F;
            this.fldValidation4.Width = 1.6875F;
            // 
            // fldExpires
            // 
            this.fldExpires.CanGrow = false;
            this.fldExpires.Height = 0.1666667F;
            this.fldExpires.Left = 3.583333F;
            this.fldExpires.MultiLine = false;
            this.fldExpires.Name = "fldExpires";
            this.fldExpires.Style = "font-family: \'Courier New\'; font-size: 11pt; vertical-align: bottom; ddo-char-set" +
    ": 0";
            this.fldExpires.Text = null;
            this.fldExpires.Top = 0.2986111F;
            this.fldExpires.Width = 1.020833F;
            // 
            // lblExpireDate
            // 
            this.lblExpireDate.Height = 0.125F;
            this.lblExpireDate.HyperLink = null;
            this.lblExpireDate.Left = 3.1875F;
            this.lblExpireDate.Name = "lblExpireDate";
            this.lblExpireDate.Style = "font-size: 7pt";
            this.lblExpireDate.Text = "Expires:";
            this.lblExpireDate.Top = 0.3472222F;
            this.lblExpireDate.Width = 0.3819444F;
            // 
            // fldEffective
            // 
            this.fldEffective.CanGrow = false;
            this.fldEffective.Height = 0.1666667F;
            this.fldEffective.Left = 1.9375F;
            this.fldEffective.MultiLine = false;
            this.fldEffective.Name = "fldEffective";
            this.fldEffective.Style = "font-family: \'Courier New\'; font-size: 11pt; vertical-align: bottom; ddo-char-set" +
    ": 0";
            this.fldEffective.Text = "12/01/2012";
            this.fldEffective.Top = 0.2986111F;
            this.fldEffective.Width = 1.020833F;
            // 
            // lblPriorTitle
            // 
            this.lblPriorTitle.Height = 0.125F;
            this.lblPriorTitle.HyperLink = null;
            this.lblPriorTitle.Left = 1.6875F;
            this.lblPriorTitle.Name = "lblPriorTitle";
            this.lblPriorTitle.Style = "font-size: 6pt; vertical-align: bottom";
            this.lblPriorTitle.Text = "PT";
            this.lblPriorTitle.Top = 2.8125F;
            this.lblPriorTitle.Width = 0.1875F;
            // 
            // lblInsurance
            // 
            this.lblInsurance.Height = 0.125F;
            this.lblInsurance.HyperLink = null;
            this.lblInsurance.Left = 4.625F;
            this.lblInsurance.Name = "lblInsurance";
            this.lblInsurance.Style = "font-size: 7pt";
            this.lblInsurance.Text = "Insurance:";
            this.lblInsurance.Top = 0.3472222F;
            this.lblInsurance.Width = 0.5F;
            // 
            // lblValidation
            // 
            this.lblValidation.Height = 0.198F;
            this.lblValidation.HyperLink = null;
            this.lblValidation.Left = 3.437F;
            this.lblValidation.Name = "lblValidation";
            this.lblValidation.Style = "font-size: 6pt";
            this.lblValidation.Text = "Registration Void Unless Validated";
            this.lblValidation.Top = 1.854F;
            this.lblValidation.Width = 0.688F;
            // 
            // fldInsurance
            // 
            this.fldInsurance.CanGrow = false;
            this.fldInsurance.Height = 0.125F;
            this.fldInsurance.Left = 5F;
            this.fldInsurance.MultiLine = false;
            this.fldInsurance.Name = "fldInsurance";
            this.fldInsurance.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: bottom; ddo-char-set" +
    ": 0";
            this.fldInsurance.Text = null;
            this.fldInsurance.Top = 0.25F;
            this.fldInsurance.Width = 1.0625F;
            // 
            // Line1
            // 
            this.Line1.Height = 0.3645834F;
            this.Line1.Left = 6.569445F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.06597222F;
            this.Line1.Width = 0F;
            this.Line1.X1 = 6.569445F;
            this.Line1.X2 = 6.569445F;
            this.Line1.Y1 = 0.06597222F;
            this.Line1.Y2 = 0.4305556F;
            // 
            // Line2
            // 
            this.Line2.Height = 0.3333334F;
            this.Line2.Left = 2.0625F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.4583333F;
            this.Line2.Width = 0F;
            this.Line2.X1 = 2.0625F;
            this.Line2.X2 = 2.0625F;
            this.Line2.Y1 = 0.4583333F;
            this.Line2.Y2 = 0.7916667F;
            // 
            // Line3
            // 
            this.Line3.Height = 0.3333334F;
            this.Line3.Left = 2.534722F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.4583333F;
            this.Line3.Width = 0F;
            this.Line3.X1 = 2.534722F;
            this.Line3.X2 = 2.534722F;
            this.Line3.Y1 = 0.4583333F;
            this.Line3.Y2 = 0.7916667F;
            // 
            // Line4
            // 
            this.Line4.Height = 0.3333334F;
            this.Line4.Left = 3.1875F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.4583333F;
            this.Line4.Width = 0F;
            this.Line4.X1 = 3.1875F;
            this.Line4.X2 = 3.1875F;
            this.Line4.Y1 = 0.4583333F;
            this.Line4.Y2 = 0.7916667F;
            // 
            // Line5
            // 
            this.Line5.Height = 0.3333334F;
            this.Line5.Left = 7.569445F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.4583333F;
            this.Line5.Width = 0F;
            this.Line5.X1 = 7.569445F;
            this.Line5.X2 = 7.569445F;
            this.Line5.Y1 = 0.4583333F;
            this.Line5.Y2 = 0.7916667F;
            // 
            // Line6
            // 
            this.Line6.Height = 0.3333334F;
            this.Line6.Left = 6.8125F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 0.4583333F;
            this.Line6.Width = 0F;
            this.Line6.X1 = 6.8125F;
            this.Line6.X2 = 6.8125F;
            this.Line6.Y1 = 0.4583333F;
            this.Line6.Y2 = 0.7916667F;
            // 
            // Line7
            // 
            this.Line7.Height = 2.628473F;
            this.Line7.Left = 6.027778F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0.4583333F;
            this.Line7.Width = 0F;
            this.Line7.X1 = 6.027778F;
            this.Line7.X2 = 6.027778F;
            this.Line7.Y1 = 0.4583333F;
            this.Line7.Y2 = 3.086806F;
            // 
            // Line8
            // 
            this.Line8.Height = 0.3333334F;
            this.Line8.Left = 5.527778F;
            this.Line8.LineWeight = 1F;
            this.Line8.Name = "Line8";
            this.Line8.Top = 0.4583333F;
            this.Line8.Width = 0F;
            this.Line8.X1 = 5.527778F;
            this.Line8.X2 = 5.527778F;
            this.Line8.Y1 = 0.4583333F;
            this.Line8.Y2 = 0.7916667F;
            // 
            // Line9
            // 
            this.Line9.Height = 0.3333334F;
            this.Line9.Left = 5.090278F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 0.4583333F;
            this.Line9.Width = 0F;
            this.Line9.X1 = 5.090278F;
            this.Line9.X2 = 5.090278F;
            this.Line9.Y1 = 0.4583333F;
            this.Line9.Y2 = 0.7916667F;
            // 
            // Line11
            // 
            this.Line11.Height = 0.3333334F;
            this.Line11.Left = 4.0625F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.4583333F;
            this.Line11.Width = 0F;
            this.Line11.X1 = 4.0625F;
            this.Line11.X2 = 4.0625F;
            this.Line11.Y1 = 0.4583333F;
            this.Line11.Y2 = 0.7916667F;
            // 
            // Line12
            // 
            this.Line12.Height = 0F;
            this.Line12.Left = 0F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0.7916667F;
            this.Line12.Width = 7.9375F;
            this.Line12.X1 = 0F;
            this.Line12.X2 = 7.9375F;
            this.Line12.Y1 = 0.7916667F;
            this.Line12.Y2 = 0.7916667F;
            // 
            // Line13
            // 
            this.Line13.Height = 0F;
            this.Line13.Left = 0F;
            this.Line13.LineWeight = 1F;
            this.Line13.Name = "Line13";
            this.Line13.Top = 1.479167F;
            this.Line13.Width = 4.625F;
            this.Line13.X1 = 0F;
            this.Line13.X2 = 4.625F;
            this.Line13.Y1 = 1.479167F;
            this.Line13.Y2 = 1.479167F;
            // 
            // Line14
            // 
            this.Line14.Height = 1.21875F;
            this.Line14.Left = 2.909722F;
            this.Line14.LineWeight = 1F;
            this.Line14.Name = "Line14";
            this.Line14.Top = 1.479167F;
            this.Line14.Width = 0F;
            this.Line14.X1 = 2.909722F;
            this.Line14.X2 = 2.909722F;
            this.Line14.Y1 = 1.479167F;
            this.Line14.Y2 = 2.697917F;
            // 
            // Line15
            // 
            this.Line15.Height = 1.013889F;
            this.Line15.Left = 3.5625F;
            this.Line15.LineWeight = 1F;
            this.Line15.Name = "Line15";
            this.Line15.Top = 0.7916667F;
            this.Line15.Width = 0F;
            this.Line15.X1 = 3.5625F;
            this.Line15.X2 = 3.5625F;
            this.Line15.Y1 = 0.7916667F;
            this.Line15.Y2 = 1.805556F;
            // 
            // Line16
            // 
            this.Line16.Height = 0.947917F;
            this.Line16.Left = 0F;
            this.Line16.LineWeight = 1F;
            this.Line16.Name = "Line16";
            this.Line16.Top = 1.75F;
            this.Line16.Width = 0F;
            this.Line16.X1 = 0F;
            this.Line16.X2 = 0F;
            this.Line16.Y1 = 1.75F;
            this.Line16.Y2 = 2.697917F;
            // 
            // Line17
            // 
            this.Line17.Height = 0F;
            this.Line17.Left = 0F;
            this.Line17.LineWeight = 1F;
            this.Line17.Name = "Line17";
            this.Line17.Top = 2.697917F;
            this.Line17.Width = 2.90625F;
            this.Line17.X1 = 0F;
            this.Line17.X2 = 2.90625F;
            this.Line17.Y1 = 2.697917F;
            this.Line17.Y2 = 2.697917F;
            // 
            // Line18
            // 
            this.Line18.Height = 0F;
            this.Line18.Left = 0F;
            this.Line18.LineWeight = 1F;
            this.Line18.Name = "Line18";
            this.Line18.Top = 2.260417F;
            this.Line18.Width = 2.909722F;
            this.Line18.X1 = 0F;
            this.Line18.X2 = 2.909722F;
            this.Line18.Y1 = 2.260417F;
            this.Line18.Y2 = 2.260417F;
            // 
            // fldPriorTitle
            // 
            this.fldPriorTitle.CanGrow = false;
            this.fldPriorTitle.Height = 0.1875F;
            this.fldPriorTitle.Left = 1.875F;
            this.fldPriorTitle.MultiLine = false;
            this.fldPriorTitle.Name = "fldPriorTitle";
            this.fldPriorTitle.Style = "font-family: \'Courier New\'; font-size: 10pt; vertical-align: bottom; ddo-char-set" +
    ": 0";
            this.fldPriorTitle.Text = "99999999999999999999 XX";
            this.fldPriorTitle.Top = 2.75F;
            this.fldPriorTitle.Width = 2.625F;
            // 
            // fldValidation5
            // 
            this.fldValidation5.CanGrow = false;
            this.fldValidation5.Height = 0.125F;
            this.fldValidation5.Left = 2.937F;
            this.fldValidation5.MultiLine = false;
            this.fldValidation5.Name = "fldValidation5";
            this.fldValidation5.Style = "font-size: 7pt; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.fldValidation5.Top = 2.518F;
            this.fldValidation5.Width = 1.6875F;
            // 
            // Line19
            // 
            this.Line19.Height = 0F;
            this.Line19.Left = 1.8125F;
            this.Line19.LineWeight = 1F;
            this.Line19.Name = "Line19";
            this.Line19.Top = 2.9375F;
            this.Line19.Width = 2.743055F;
            this.Line19.X1 = 1.8125F;
            this.Line19.X2 = 4.555555F;
            this.Line19.Y1 = 2.9375F;
            this.Line19.Y2 = 2.9375F;
            // 
            // Line20
            // 
            this.Line20.Height = 0F;
            this.Line20.Left = 2F;
            this.Line20.LineWeight = 1F;
            this.Line20.Name = "Line20";
            this.Line20.Top = 3.270833F;
            this.Line20.Width = 1.104167F;
            this.Line20.X1 = 2F;
            this.Line20.X2 = 3.104167F;
            this.Line20.Y1 = 3.270833F;
            this.Line20.Y2 = 3.270833F;
            // 
            // Line21
            // 
            this.Line21.Height = 0F;
            this.Line21.Left = 3.263889F;
            this.Line21.LineWeight = 1F;
            this.Line21.Name = "Line21";
            this.Line21.Top = 3.270833F;
            this.Line21.Width = 1.291666F;
            this.Line21.X1 = 3.263889F;
            this.Line21.X2 = 4.555555F;
            this.Line21.Y1 = 3.270833F;
            this.Line21.Y2 = 3.270833F;
            // 
            // lblUserId
            // 
            this.lblUserId.Height = 0.1458333F;
            this.lblUserId.HyperLink = null;
            this.lblUserId.Left = 4.6875F;
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Style = "font-size: 7pt; font-weight: bold";
            this.lblUserId.Text = "User Id";
            this.lblUserId.Top = 2.871528F;
            this.lblUserId.Width = 0.4479167F;
            // 
            // fldUserId
            // 
            this.fldUserId.Height = 0.1354167F;
            this.fldUserId.Left = 5.34375F;
            this.fldUserId.Name = "fldUserId";
            this.fldUserId.Style = "font-size: 7pt; text-align: right";
            this.fldUserId.Text = "UID";
            this.fldUserId.Top = 2.875F;
            this.fldUserId.Width = 0.6145833F;
            // 
            // fldRegType
            // 
            this.fldRegType.Height = 0.1458333F;
            this.fldRegType.Left = 6.0625F;
            this.fldRegType.Name = "fldRegType";
            this.fldRegType.Style = "font-size: 7pt; text-align: left; white-space: nowrap";
            this.fldRegType.Text = "New Reg Excise Tax Only-IRP";
            this.fldRegType.Top = 2.875F;
            this.fldRegType.Width = 1.416667F;
            // 
            // lblSOSURL
            // 
            this.lblSOSURL.Height = 0.1875F;
            this.lblSOSURL.HyperLink = null;
            this.lblSOSURL.Left = 5.666667F;
            this.lblSOSURL.Name = "lblSOSURL";
            this.lblSOSURL.Style = "font-family: \'Courier New\'";
            this.lblSOSURL.Text = "www.maine.gov/sos";
            this.lblSOSURL.Top = 3.104167F;
            this.lblSOSURL.Visible = false;
            this.lblSOSURL.Width = 1.5625F;
            // 
            // lblFee1
            // 
            this.lblFee1.Height = 0.1458333F;
            this.lblFee1.HyperLink = null;
            this.lblFee1.Left = 4.6875F;
            this.lblFee1.Name = "lblFee1";
            this.lblFee1.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee1.Text = "Fee 1";
            this.lblFee1.Top = 0.8715278F;
            this.lblFee1.Width = 0.5208333F;
            // 
            // fldFee1
            // 
            this.fldFee1.Height = 0.1354167F;
            this.fldFee1.Left = 4.659722F;
            this.fldFee1.MultiLine = false;
            this.fldFee1.Name = "fldFee1";
            this.fldFee1.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee1.Text = "1,999.00";
            this.fldFee1.Top = 0.875F;
            this.fldFee1.Width = 1.298611F;
            // 
            // lblFee2
            // 
            this.lblFee2.Height = 0.1458333F;
            this.lblFee2.HyperLink = null;
            this.lblFee2.Left = 4.6875F;
            this.lblFee2.Name = "lblFee2";
            this.lblFee2.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee2.Text = "Fee 2";
            this.lblFee2.Top = 0.9826389F;
            this.lblFee2.Width = 0.5208333F;
            // 
            // fldFee2
            // 
            this.fldFee2.Height = 0.1354167F;
            this.fldFee2.Left = 4.659722F;
            this.fldFee2.MultiLine = false;
            this.fldFee2.Name = "fldFee2";
            this.fldFee2.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee2.Text = "1,999.00";
            this.fldFee2.Top = 0.9826389F;
            this.fldFee2.Width = 1.298611F;
            // 
            // lblFee3
            // 
            this.lblFee3.Height = 0.1458333F;
            this.lblFee3.HyperLink = null;
            this.lblFee3.Left = 4.6875F;
            this.lblFee3.Name = "lblFee3";
            this.lblFee3.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee3.Text = "Fee 3";
            this.lblFee3.Top = 1.09375F;
            this.lblFee3.Width = 0.5208333F;
            // 
            // fldFee3
            // 
            this.fldFee3.Height = 0.1354167F;
            this.fldFee3.Left = 4.659722F;
            this.fldFee3.MultiLine = false;
            this.fldFee3.Name = "fldFee3";
            this.fldFee3.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee3.Text = "1,999.00";
            this.fldFee3.Top = 1.09375F;
            this.fldFee3.Width = 1.298611F;
            // 
            // lblFee4
            // 
            this.lblFee4.Height = 0.1458333F;
            this.lblFee4.HyperLink = null;
            this.lblFee4.Left = 4.6875F;
            this.lblFee4.Name = "lblFee4";
            this.lblFee4.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee4.Text = "Fee 4";
            this.lblFee4.Top = 1.204861F;
            this.lblFee4.Width = 0.5208333F;
            // 
            // fldFee4
            // 
            this.fldFee4.Height = 0.1354167F;
            this.fldFee4.Left = 4.659722F;
            this.fldFee4.MultiLine = false;
            this.fldFee4.Name = "fldFee4";
            this.fldFee4.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee4.Text = "1,999.00";
            this.fldFee4.Top = 1.204861F;
            this.fldFee4.Width = 1.298611F;
            // 
            // lblFee5
            // 
            this.lblFee5.Height = 0.1458333F;
            this.lblFee5.HyperLink = null;
            this.lblFee5.Left = 4.6875F;
            this.lblFee5.Name = "lblFee5";
            this.lblFee5.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee5.Text = "Fee 5";
            this.lblFee5.Top = 1.315972F;
            this.lblFee5.Width = 0.5208333F;
            // 
            // fldFee5
            // 
            this.fldFee5.Height = 0.1354167F;
            this.fldFee5.Left = 4.659722F;
            this.fldFee5.MultiLine = false;
            this.fldFee5.Name = "fldFee5";
            this.fldFee5.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee5.Text = "1,999.00";
            this.fldFee5.Top = 1.315972F;
            this.fldFee5.Width = 1.298611F;
            // 
            // lblFee6
            // 
            this.lblFee6.Height = 0.1458333F;
            this.lblFee6.HyperLink = null;
            this.lblFee6.Left = 4.6875F;
            this.lblFee6.Name = "lblFee6";
            this.lblFee6.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee6.Text = "Fee 6";
            this.lblFee6.Top = 1.427083F;
            this.lblFee6.Width = 0.5208333F;
            // 
            // fldFee6
            // 
            this.fldFee6.Height = 0.1354167F;
            this.fldFee6.Left = 4.659722F;
            this.fldFee6.MultiLine = false;
            this.fldFee6.Name = "fldFee6";
            this.fldFee6.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee6.Text = "1,999.00";
            this.fldFee6.Top = 1.427083F;
            this.fldFee6.Width = 1.298611F;
            // 
            // lblFee7
            // 
            this.lblFee7.Height = 0.1458333F;
            this.lblFee7.HyperLink = null;
            this.lblFee7.Left = 4.6875F;
            this.lblFee7.Name = "lblFee7";
            this.lblFee7.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee7.Text = "Fee 7";
            this.lblFee7.Top = 1.538194F;
            this.lblFee7.Width = 0.5208333F;
            // 
            // fldFee7
            // 
            this.fldFee7.Height = 0.1354167F;
            this.fldFee7.Left = 4.659722F;
            this.fldFee7.MultiLine = false;
            this.fldFee7.Name = "fldFee7";
            this.fldFee7.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee7.Text = "1,999.00";
            this.fldFee7.Top = 1.538194F;
            this.fldFee7.Width = 1.298611F;
            // 
            // lblFee8
            // 
            this.lblFee8.Height = 0.1458333F;
            this.lblFee8.HyperLink = null;
            this.lblFee8.Left = 4.6875F;
            this.lblFee8.Name = "lblFee8";
            this.lblFee8.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee8.Text = "Fee 8";
            this.lblFee8.Top = 1.649306F;
            this.lblFee8.Width = 0.5208333F;
            // 
            // fldFee8
            // 
            this.fldFee8.Height = 0.1354167F;
            this.fldFee8.Left = 4.659722F;
            this.fldFee8.MultiLine = false;
            this.fldFee8.Name = "fldFee8";
            this.fldFee8.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee8.Text = "1,999.00";
            this.fldFee8.Top = 1.649306F;
            this.fldFee8.Width = 1.298611F;
            // 
            // lblFee9
            // 
            this.lblFee9.Height = 0.1458333F;
            this.lblFee9.HyperLink = null;
            this.lblFee9.Left = 4.6875F;
            this.lblFee9.Name = "lblFee9";
            this.lblFee9.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee9.Text = "Fee 9";
            this.lblFee9.Top = 1.760417F;
            this.lblFee9.Width = 0.5208333F;
            // 
            // fldFee9
            // 
            this.fldFee9.Height = 0.1354167F;
            this.fldFee9.Left = 4.659722F;
            this.fldFee9.MultiLine = false;
            this.fldFee9.Name = "fldFee9";
            this.fldFee9.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee9.Text = "1,999.00";
            this.fldFee9.Top = 1.760417F;
            this.fldFee9.Width = 1.298611F;
            // 
            // lblFee10
            // 
            this.lblFee10.Height = 0.1458333F;
            this.lblFee10.HyperLink = null;
            this.lblFee10.Left = 4.6875F;
            this.lblFee10.Name = "lblFee10";
            this.lblFee10.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee10.Text = "Fee 10";
            this.lblFee10.Top = 1.871528F;
            this.lblFee10.Width = 0.5208333F;
            // 
            // fldFee10
            // 
            this.fldFee10.Height = 0.1354167F;
            this.fldFee10.Left = 4.659722F;
            this.fldFee10.MultiLine = false;
            this.fldFee10.Name = "fldFee10";
            this.fldFee10.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee10.Text = "1,999.00";
            this.fldFee10.Top = 1.871528F;
            this.fldFee10.Width = 1.298611F;
            // 
            // lblFee11
            // 
            this.lblFee11.Height = 0.1458333F;
            this.lblFee11.HyperLink = null;
            this.lblFee11.Left = 4.6875F;
            this.lblFee11.Name = "lblFee11";
            this.lblFee11.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee11.Text = "Fee 11";
            this.lblFee11.Top = 1.982639F;
            this.lblFee11.Width = 0.5208333F;
            // 
            // fldFee11
            // 
            this.fldFee11.Height = 0.1354167F;
            this.fldFee11.Left = 4.659722F;
            this.fldFee11.MultiLine = false;
            this.fldFee11.Name = "fldFee11";
            this.fldFee11.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee11.Text = "1,999.00";
            this.fldFee11.Top = 1.982639F;
            this.fldFee11.Width = 1.298611F;
            // 
            // lblFee12
            // 
            this.lblFee12.Height = 0.1458333F;
            this.lblFee12.HyperLink = null;
            this.lblFee12.Left = 4.6875F;
            this.lblFee12.Name = "lblFee12";
            this.lblFee12.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee12.Text = "Fee 12";
            this.lblFee12.Top = 2.09375F;
            this.lblFee12.Width = 0.5208333F;
            // 
            // fldFee12
            // 
            this.fldFee12.Height = 0.1354167F;
            this.fldFee12.Left = 4.659722F;
            this.fldFee12.MultiLine = false;
            this.fldFee12.Name = "fldFee12";
            this.fldFee12.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee12.Text = "1,999.00";
            this.fldFee12.Top = 2.09375F;
            this.fldFee12.Width = 1.298611F;
            // 
            // lblFee13
            // 
            this.lblFee13.Height = 0.1458333F;
            this.lblFee13.HyperLink = null;
            this.lblFee13.Left = 4.6875F;
            this.lblFee13.Name = "lblFee13";
            this.lblFee13.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee13.Text = "Fee 13";
            this.lblFee13.Top = 2.204861F;
            this.lblFee13.Width = 0.5208333F;
            // 
            // fldFee13
            // 
            this.fldFee13.Height = 0.1354167F;
            this.fldFee13.Left = 4.659722F;
            this.fldFee13.MultiLine = false;
            this.fldFee13.Name = "fldFee13";
            this.fldFee13.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee13.Text = "1,999.00";
            this.fldFee13.Top = 2.204861F;
            this.fldFee13.Width = 1.298611F;
            // 
            // lblFee14
            // 
            this.lblFee14.Height = 0.1458333F;
            this.lblFee14.HyperLink = null;
            this.lblFee14.Left = 4.6875F;
            this.lblFee14.Name = "lblFee14";
            this.lblFee14.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee14.Text = "Fee 14";
            this.lblFee14.Top = 2.315972F;
            this.lblFee14.Width = 0.5208333F;
            // 
            // fldFee14
            // 
            this.fldFee14.Height = 0.1354167F;
            this.fldFee14.Left = 4.659722F;
            this.fldFee14.MultiLine = false;
            this.fldFee14.Name = "fldFee14";
            this.fldFee14.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee14.Text = "1,999.00";
            this.fldFee14.Top = 2.315972F;
            this.fldFee14.Width = 1.298611F;
            // 
            // lblFee15
            // 
            this.lblFee15.Height = 0.1458333F;
            this.lblFee15.HyperLink = null;
            this.lblFee15.Left = 4.6875F;
            this.lblFee15.Name = "lblFee15";
            this.lblFee15.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee15.Text = "Fee 15";
            this.lblFee15.Top = 2.427083F;
            this.lblFee15.Width = 0.5208333F;
            // 
            // fldFee15
            // 
            this.fldFee15.Height = 0.1354167F;
            this.fldFee15.Left = 4.659722F;
            this.fldFee15.MultiLine = false;
            this.fldFee15.Name = "fldFee15";
            this.fldFee15.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee15.Text = "1,999.00";
            this.fldFee15.Top = 2.427083F;
            this.fldFee15.Width = 1.298611F;
            // 
            // lblFee16
            // 
            this.lblFee16.Height = 0.1458333F;
            this.lblFee16.HyperLink = null;
            this.lblFee16.Left = 4.6875F;
            this.lblFee16.Name = "lblFee16";
            this.lblFee16.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee16.Text = "Fee 16";
            this.lblFee16.Top = 2.538194F;
            this.lblFee16.Width = 0.5208333F;
            // 
            // fldFee16
            // 
            this.fldFee16.Height = 0.1354167F;
            this.fldFee16.Left = 4.659722F;
            this.fldFee16.MultiLine = false;
            this.fldFee16.Name = "fldFee16";
            this.fldFee16.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee16.Text = "1,999.00";
            this.fldFee16.Top = 2.538194F;
            this.fldFee16.Width = 1.298611F;
            // 
            // lblFee17
            // 
            this.lblFee17.Height = 0.1458333F;
            this.lblFee17.HyperLink = null;
            this.lblFee17.Left = 4.6875F;
            this.lblFee17.Name = "lblFee17";
            this.lblFee17.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee17.Text = "Fee 17";
            this.lblFee17.Top = 2.649306F;
            this.lblFee17.Width = 0.5208333F;
            // 
            // fldFee17
            // 
            this.fldFee17.Height = 0.1354167F;
            this.fldFee17.Left = 4.659722F;
            this.fldFee17.MultiLine = false;
            this.fldFee17.Name = "fldFee17";
            this.fldFee17.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee17.Text = "1,999.00";
            this.fldFee17.Top = 2.649306F;
            this.fldFee17.Width = 1.298611F;
            // 
            // lblFee18
            // 
            this.lblFee18.Height = 0.1458333F;
            this.lblFee18.HyperLink = null;
            this.lblFee18.Left = 4.6875F;
            this.lblFee18.Name = "lblFee18";
            this.lblFee18.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee18.Text = "Fee 18";
            this.lblFee18.Top = 2.760417F;
            this.lblFee18.Width = 0.5208333F;
            // 
            // fldFee18
            // 
            this.fldFee18.Height = 0.1354167F;
            this.fldFee18.Left = 4.659722F;
            this.fldFee18.MultiLine = false;
            this.fldFee18.Name = "fldFee18";
            this.fldFee18.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee18.Text = "1,999.00";
            this.fldFee18.Top = 2.760417F;
            this.fldFee18.Width = 1.298611F;
            // 
            // lblFee19
            // 
            this.lblFee19.Height = 0.1458333F;
            this.lblFee19.HyperLink = null;
            this.lblFee19.Left = 6.090278F;
            this.lblFee19.Name = "lblFee19";
            this.lblFee19.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee19.Text = "Fee 19";
            this.lblFee19.Top = 0.8715278F;
            this.lblFee19.Width = 0.5208333F;
            // 
            // fldFee19
            // 
            this.fldFee19.Height = 0.1354167F;
            this.fldFee19.Left = 6.0625F;
            this.fldFee19.MultiLine = false;
            this.fldFee19.Name = "fldFee19";
            this.fldFee19.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee19.Text = "1,999.00";
            this.fldFee19.Top = 0.875F;
            this.fldFee19.Width = 1.802083F;
            // 
            // lblFee20
            // 
            this.lblFee20.Height = 0.1458333F;
            this.lblFee20.HyperLink = null;
            this.lblFee20.Left = 6.090278F;
            this.lblFee20.Name = "lblFee20";
            this.lblFee20.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee20.Text = "Fee 20";
            this.lblFee20.Top = 0.9826389F;
            this.lblFee20.Width = 0.5208333F;
            // 
            // fldFee20
            // 
            this.fldFee20.Height = 0.1354167F;
            this.fldFee20.Left = 6.0625F;
            this.fldFee20.MultiLine = false;
            this.fldFee20.Name = "fldFee20";
            this.fldFee20.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee20.Text = "1,999.00";
            this.fldFee20.Top = 0.9826389F;
            this.fldFee20.Width = 1.802083F;
            // 
            // lblFee21
            // 
            this.lblFee21.Height = 0.1458333F;
            this.lblFee21.HyperLink = null;
            this.lblFee21.Left = 6.090278F;
            this.lblFee21.Name = "lblFee21";
            this.lblFee21.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee21.Text = "Fee 21";
            this.lblFee21.Top = 1.09375F;
            this.lblFee21.Width = 0.5208333F;
            // 
            // fldFee21
            // 
            this.fldFee21.Height = 0.1354167F;
            this.fldFee21.Left = 6.0625F;
            this.fldFee21.MultiLine = false;
            this.fldFee21.Name = "fldFee21";
            this.fldFee21.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee21.Text = "1,999.00";
            this.fldFee21.Top = 1.09375F;
            this.fldFee21.Width = 1.802083F;
            // 
            // lblFee22
            // 
            this.lblFee22.Height = 0.1458333F;
            this.lblFee22.HyperLink = null;
            this.lblFee22.Left = 6.090278F;
            this.lblFee22.Name = "lblFee22";
            this.lblFee22.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee22.Text = "Fee 22";
            this.lblFee22.Top = 1.204861F;
            this.lblFee22.Width = 0.5208333F;
            // 
            // fldFee22
            // 
            this.fldFee22.Height = 0.1354167F;
            this.fldFee22.Left = 6.0625F;
            this.fldFee22.MultiLine = false;
            this.fldFee22.Name = "fldFee22";
            this.fldFee22.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee22.Text = "1,999.00";
            this.fldFee22.Top = 1.204861F;
            this.fldFee22.Width = 1.802083F;
            // 
            // lblFee23
            // 
            this.lblFee23.Height = 0.1458333F;
            this.lblFee23.HyperLink = null;
            this.lblFee23.Left = 6.090278F;
            this.lblFee23.Name = "lblFee23";
            this.lblFee23.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee23.Text = "Fee 23";
            this.lblFee23.Top = 1.315972F;
            this.lblFee23.Width = 0.5208333F;
            // 
            // fldFee23
            // 
            this.fldFee23.Height = 0.1354167F;
            this.fldFee23.Left = 6.0625F;
            this.fldFee23.MultiLine = false;
            this.fldFee23.Name = "fldFee23";
            this.fldFee23.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee23.Text = "1,999.00";
            this.fldFee23.Top = 1.315972F;
            this.fldFee23.Width = 1.802083F;
            // 
            // lblFee24
            // 
            this.lblFee24.Height = 0.1458333F;
            this.lblFee24.HyperLink = null;
            this.lblFee24.Left = 6.090278F;
            this.lblFee24.Name = "lblFee24";
            this.lblFee24.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee24.Text = "Fee 24";
            this.lblFee24.Top = 1.427083F;
            this.lblFee24.Width = 0.5208333F;
            // 
            // fldFee24
            // 
            this.fldFee24.Height = 0.1354167F;
            this.fldFee24.Left = 6.0625F;
            this.fldFee24.MultiLine = false;
            this.fldFee24.Name = "fldFee24";
            this.fldFee24.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee24.Text = "1,999.00";
            this.fldFee24.Top = 1.427083F;
            this.fldFee24.Width = 1.802083F;
            // 
            // lblFee25
            // 
            this.lblFee25.Height = 0.1458333F;
            this.lblFee25.HyperLink = null;
            this.lblFee25.Left = 6.090278F;
            this.lblFee25.Name = "lblFee25";
            this.lblFee25.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee25.Text = "Fee 25";
            this.lblFee25.Top = 1.538194F;
            this.lblFee25.Width = 0.5208333F;
            // 
            // fldFee25
            // 
            this.fldFee25.Height = 0.1354167F;
            this.fldFee25.Left = 6.0625F;
            this.fldFee25.MultiLine = false;
            this.fldFee25.Name = "fldFee25";
            this.fldFee25.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee25.Text = "1,999.00";
            this.fldFee25.Top = 1.538194F;
            this.fldFee25.Width = 1.802083F;
            // 
            // lblFee26
            // 
            this.lblFee26.Height = 0.1458333F;
            this.lblFee26.HyperLink = null;
            this.lblFee26.Left = 6.090278F;
            this.lblFee26.Name = "lblFee26";
            this.lblFee26.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee26.Text = "Fee 26";
            this.lblFee26.Top = 1.649306F;
            this.lblFee26.Width = 0.5208333F;
            // 
            // fldFee26
            // 
            this.fldFee26.Height = 0.1354167F;
            this.fldFee26.Left = 6.0625F;
            this.fldFee26.MultiLine = false;
            this.fldFee26.Name = "fldFee26";
            this.fldFee26.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee26.Text = "1,999.00";
            this.fldFee26.Top = 1.649306F;
            this.fldFee26.Width = 1.802083F;
            // 
            // lblFee27
            // 
            this.lblFee27.Height = 0.1458333F;
            this.lblFee27.HyperLink = null;
            this.lblFee27.Left = 6.090278F;
            this.lblFee27.Name = "lblFee27";
            this.lblFee27.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee27.Text = "Fee 27";
            this.lblFee27.Top = 1.760417F;
            this.lblFee27.Width = 0.5208333F;
            // 
            // fldFee27
            // 
            this.fldFee27.Height = 0.1354167F;
            this.fldFee27.Left = 6.0625F;
            this.fldFee27.MultiLine = false;
            this.fldFee27.Name = "fldFee27";
            this.fldFee27.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee27.Text = "1,999.00";
            this.fldFee27.Top = 1.760417F;
            this.fldFee27.Width = 1.802083F;
            // 
            // lblFee28
            // 
            this.lblFee28.Height = 0.1458333F;
            this.lblFee28.HyperLink = null;
            this.lblFee28.Left = 6.090278F;
            this.lblFee28.Name = "lblFee28";
            this.lblFee28.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee28.Text = "Fee 28";
            this.lblFee28.Top = 1.871528F;
            this.lblFee28.Width = 0.5208333F;
            // 
            // fldFee28
            // 
            this.fldFee28.Height = 0.1354167F;
            this.fldFee28.Left = 6.0625F;
            this.fldFee28.MultiLine = false;
            this.fldFee28.Name = "fldFee28";
            this.fldFee28.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee28.Text = "1,999.00";
            this.fldFee28.Top = 1.871528F;
            this.fldFee28.Width = 1.802083F;
            // 
            // lblFee29
            // 
            this.lblFee29.Height = 0.1458333F;
            this.lblFee29.HyperLink = null;
            this.lblFee29.Left = 6.090278F;
            this.lblFee29.Name = "lblFee29";
            this.lblFee29.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee29.Text = "Fee 29";
            this.lblFee29.Top = 1.982639F;
            this.lblFee29.Width = 0.5208333F;
            // 
            // fldFee29
            // 
            this.fldFee29.Height = 0.1354167F;
            this.fldFee29.Left = 6.0625F;
            this.fldFee29.MultiLine = false;
            this.fldFee29.Name = "fldFee29";
            this.fldFee29.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee29.Text = "1,999.00";
            this.fldFee29.Top = 1.982639F;
            this.fldFee29.Width = 1.802083F;
            // 
            // lblFee30
            // 
            this.lblFee30.Height = 0.1458333F;
            this.lblFee30.HyperLink = null;
            this.lblFee30.Left = 6.090278F;
            this.lblFee30.Name = "lblFee30";
            this.lblFee30.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee30.Text = "Fee 30";
            this.lblFee30.Top = 2.09375F;
            this.lblFee30.Width = 0.5208333F;
            // 
            // fldFee30
            // 
            this.fldFee30.Height = 0.1354167F;
            this.fldFee30.Left = 6.0625F;
            this.fldFee30.MultiLine = false;
            this.fldFee30.Name = "fldFee30";
            this.fldFee30.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee30.Text = "1,999.00";
            this.fldFee30.Top = 2.09375F;
            this.fldFee30.Width = 1.802083F;
            // 
            // lblFee31
            // 
            this.lblFee31.Height = 0.1458333F;
            this.lblFee31.HyperLink = null;
            this.lblFee31.Left = 6.090278F;
            this.lblFee31.Name = "lblFee31";
            this.lblFee31.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee31.Text = "Fee 31";
            this.lblFee31.Top = 2.204861F;
            this.lblFee31.Width = 0.5208333F;
            // 
            // fldFee31
            // 
            this.fldFee31.Height = 0.1354167F;
            this.fldFee31.Left = 6.0625F;
            this.fldFee31.MultiLine = false;
            this.fldFee31.Name = "fldFee31";
            this.fldFee31.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee31.Text = "1,999.00";
            this.fldFee31.Top = 2.204861F;
            this.fldFee31.Width = 1.802083F;
            // 
            // lblFee32
            // 
            this.lblFee32.Height = 0.1458333F;
            this.lblFee32.HyperLink = null;
            this.lblFee32.Left = 6.090278F;
            this.lblFee32.Name = "lblFee32";
            this.lblFee32.Style = "font-size: 7pt; font-weight: bold";
            this.lblFee32.Text = "Fee 32";
            this.lblFee32.Top = 2.315972F;
            this.lblFee32.Width = 0.5208333F;
            // 
            // fldFee32
            // 
            this.fldFee32.Height = 0.1354167F;
            this.fldFee32.Left = 6.0625F;
            this.fldFee32.MultiLine = false;
            this.fldFee32.Name = "fldFee32";
            this.fldFee32.Style = "font-size: 7pt; text-align: right; white-space: nowrap";
            this.fldFee32.Text = "1,999.00";
            this.fldFee32.Top = 2.315972F;
            this.fldFee32.Width = 1.802083F;
            // 
            // fldRegistrationDate
            // 
            this.fldRegistrationDate.CanGrow = false;
            this.fldRegistrationDate.Height = 0.125F;
            this.fldRegistrationDate.Left = 3.687F;
            this.fldRegistrationDate.MultiLine = false;
            this.fldRegistrationDate.Name = "fldRegistrationDate";
            this.fldRegistrationDate.Style = "font-size: 7pt; font-weight: bold; ddo-char-set: 1";
            this.fldRegistrationDate.Text = "12/10/2015";
            this.fldRegistrationDate.Top = 2.29F;
            this.fldRegistrationDate.Width = 0.5625F;
            // 
            // fldRegistrationCode
            // 
            this.fldRegistrationCode.CanGrow = false;
            this.fldRegistrationCode.Height = 0.125F;
            this.fldRegistrationCode.Left = 3.333F;
            this.fldRegistrationCode.MultiLine = false;
            this.fldRegistrationCode.Name = "fldRegistrationCode";
            this.fldRegistrationCode.Style = "font-size: 7pt; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.fldRegistrationCode.Text = "55555";
            this.fldRegistrationCode.Top = 2.29F;
            this.fldRegistrationCode.Width = 0.3125F;
            // 
            // rptMVR3Laser
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.2708333F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0.125F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.958333F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMileage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMileage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCounty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAxles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNetWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAxles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNetWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegistrant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOwner1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDOB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOwner2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDOB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLessor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDOTNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOwner3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDOB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldResidenceCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldResidenceState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldResidenceCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMonthSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYearSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldResidenceAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEffDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLessor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailAddr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLegalRes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLegalResCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonthSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYearSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldValidation1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldValidation2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldValidation4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExpires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExpireDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEffective)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPriorTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInsurance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValidation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInsurance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPriorTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldValidation5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUserId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldUserId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSOSURL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFee32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFee32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegistrationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRegistrationCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMileage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMileage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCounty;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRegNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldColor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStyle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTires;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAxles;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetWeight;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegWeight;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFuel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVIN;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMake;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblModel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblColor;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStyle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTires;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAxles;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNetWeight;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRegWeight;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFuel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRegistrant;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDOB1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDOB2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLessor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDOTNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwner3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDOB3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthSticker;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYearSticker;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrationCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegistrationDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldResidenceAddress;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEffDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDOB;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLessor;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUnit;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDotNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailAddr;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLegalRes;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLegalResCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMonthSticker;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYearSticker;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidation1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidation2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidation4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpires;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExpireDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEffective;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPriorTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInsurance;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblValidation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInsurance;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPriorTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldValidation5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line21;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUserId;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUserId;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSOSURL;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee13;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee14;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee15;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee16;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee17;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee18;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee19;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee20;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee21;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee22;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee23;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee24;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee25;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee26;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee27;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee28;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee29;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee30;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee31;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFee32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee32;
	}
}
