//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptFleetReminderListStaab.
	/// </summary>
	public partial class rptFleetReminderListStaab : BaseSectionReport
	{
		public rptFleetReminderListStaab()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "Fleet Reminder List";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptFleetReminderListStaab_ReportEnd;
		}

        private void RptFleetReminderListStaab_ReportEnd(object sender, EventArgs e)
        {
			rsFleets.DisposeOf();
            rsVehicles.DisposeOf();

		}

        public static rptFleetReminderListStaab InstancePtr
		{
			get
			{
				return (rptFleetReminderListStaab)Sys.GetInstance(typeof(rptFleetReminderListStaab));
			}
		}

		protected rptFleetReminderListStaab _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptFleetReminderListStaab	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsFleets = new clsDRWrapper();
		clsDRWrapper rsVehicles = new clsDRWrapper();
		int PageCounter;
		bool blnFirstRecord;
		// vbPorter upgrade warning: datStartDate As DateTime	OnWrite(string)
		DateTime datStartDate;
		// vbPorter upgrade warning: datEndDate As DateTime	OnWrite(string)
		DateTime datEndDate;
		string strFleetIn = "";
		string strOrderBy = "";
		// Dim blnLongTermOnly As Boolean
		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeCheckNext = false;
			CheckVehicle:
			;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsVehicles.EndOfFile() != true && rsVehicles.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					executeCheckNext = true;
					goto CheckNext;
				}
				eArgs.EOF = false;
			}
			else
			{
				executeCheckNext = true;
				goto CheckNext;
			}
			CheckNext:
			;
			if (executeCheckNext)
			{
				rsVehicles.MoveNext();
				if (rsVehicles.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					rsFleets.MoveNext();
					if (rsFleets.EndOfFile() != true)
					{
						rsVehicles.OpenRecordset("SELECT * FROM MASTER WHERE FleetNumber = " + rsFleets.Get_Fields_Int32("FleetNumber") + " AND ExpireDate >= '" + FCConvert.ToString(datStartDate) + "' AND ExpireDate <= '" + FCConvert.ToString(datEndDate) + "' AND Status <> 'T' ORDER BY " + strOrderBy);
						blnFirstRecord = true;
						goto CheckVehicle;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
				executeCheckNext = false;
			}
			if (eArgs.EOF == false)
			{
				if (this.Fields["Binder"].Value != (object)rsFleets.Get_Fields_Int32("FleetNumber"))
				{
					PageCounter = 0;
				}
				this.Fields["Binder"].Value = rsFleets.Get_Fields_Int32("FleetNumber");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label9 As object	OnWrite(string)
			// vbPorter upgrade warning: Label11 As object	OnWrite(string)
			// vbPorter upgrade warning: Label8 As object	OnWrite(string)
			// vbPorter upgrade warning: lblExpirationMonth As object	OnWrite(string)
			string strFleetSearch = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label11.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label8.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			datStartDate = FCConvert.ToDateTime(frmFleetExpiringListMMTA.InstancePtr.txtStartDate.Text);
			datEndDate = FCConvert.ToDateTime(frmFleetExpiringListMMTA.InstancePtr.txtEndDate.Text);
			strFleetIn = frmFleetExpiringListMMTA.InstancePtr.strFleets;
			// blnLongTermOnly = .chkLongTermOnly.Value = vbChecked
			lblExpirationMonth.Text = "Vehicles Expiring " + frmFleetExpiringListMMTA.InstancePtr.txtStartDate.Text + " to " + frmFleetExpiringListMMTA.InstancePtr.txtEndDate.Text;
			if (frmFleetExpiringListMMTA.InstancePtr.cmbOrderByPlate.Text == "Unit")
			{
				strOrderBy = "UnitNumber, Plate";
			}
			else
			{
				strOrderBy = "Plate";
			}
			PageCounter = 0;
			blnFirstRecord = true;

			if (strFleetIn == "")
			{
				rsTemp.OpenRecordset("SELECT DISTINCT FleetNumber FROM Master WHERE FleetNumber <> 0 AND ExpireDate >= '" + FCConvert.ToString(datStartDate) + "' AND ExpireDate <= '" + FCConvert.ToString(datEndDate) + "'");
			}
			else
			{
				rsTemp.OpenRecordset("SELECT DISTINCT FleetNumber FROM Master WHERE FleetNumber <> 0 AND ExpireDate >= '" + FCConvert.ToString(datStartDate) + "' AND ExpireDate <= '" + FCConvert.ToString(datEndDate) + "' AND FleetNumber IN (" + strFleetIn + ")");
			}

			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				do
				{
					if (strFleetSearch == "")
					{
						strFleetSearch = FCConvert.ToString(rsTemp.Get_Fields_Int32("FleetNumber"));
					}
					else
					{
						strFleetSearch += ", " + rsTemp.Get_Fields_Int32("FleetNumber");
					}
					rsTemp.MoveNext();
				}
				while (rsTemp.EndOfFile() != true);
			}
			bool executeCancelReport = false;
			if (strFleetSearch != "")
			{
				if (strFleetIn == "")
				{
					rsFleets.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted <> 1 AND FleetNumber IN (" + strFleetSearch + ") ORDER BY Owner1Name");
				}
				else
				{
					rsFleets.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted <> 1 AND FleetNumber IN (" + strFleetSearch + ") ORDER BY Owner1Name");
				}

				rsVehicles.OpenRecordset("SELECT * FROM MASTER WHERE FleetNumber = " + rsFleets.Get_Fields_Int32("FleetNumber") + " AND ExpireDate >= '" + FCConvert.ToString(datStartDate) + "' AND ExpireDate <= '" + FCConvert.ToString(datEndDate) + "' AND Status <> 'T' ORDER BY " + strOrderBy);

				CheckVehicles:
				;
				if (rsVehicles.EndOfFile() != true && rsVehicles.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					rsFleets.MoveNext();
					if (rsFleets.EndOfFile() != true && rsFleets.BeginningOfFile() != true)
					{
						rsVehicles.OpenRecordset("SELECT * FROM MASTER WHERE FleetNumber = " + rsFleets.Get_Fields_Int32("FleetNumber") + " AND ExpireDate >= '" + FCConvert.ToString(datStartDate) + "' AND ExpireDate <= '" + FCConvert.ToString(datEndDate) + "' AND Status <> 'T' ORDER BY " + strOrderBy);
						goto CheckVehicles;
					}
					else
					{
						executeCancelReport = true;
						goto CancelReport;
					}
				}
			}
			else
			{
				executeCancelReport = true;
				goto CancelReport;
			}
			CancelReport:
			;
			rsTemp.DisposeOf();
			if (executeCancelReport)
			{
				MessageBox.Show("No records found that match the criteria.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
				executeCancelReport = false;
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldClassPlate As object	OnWrite(string)
			// vbPorter upgrade warning: fldVIN As object	OnWrite(string)
			// vbPorter upgrade warning: fldExpires As object	OnWrite(string)
			fldClassPlate.Text = rsVehicles.Get_Fields_String("Class") + " " + rsVehicles.Get_Fields_String("plate");
			fldVIN.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("Vin")));
			fldYear.Text = rsVehicles.Get_Fields_String("Year");
			fldMake.Text = rsVehicles.Get_Fields_String("make");
			fldUnit.Text = rsVehicles.Get_Fields_String("UnitNumber");
			fldExpires.Text = Strings.Format(rsVehicles.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldFleetName As object	OnWrite(string)
			if (FCConvert.ToInt32(rsFleets.Get_Fields_Int32("ExpiryMonth")) != 99)
			{
				fldFleetName.Text = "FLEET " + rsFleets.Get_Fields_Int32("FleetNumber") + "  " + rsFleets.Get_Fields("Owner1Name");
			}
			else
			{
				fldFleetName.Text = "GROUP " + rsFleets.Get_Fields_Int32("FleetNumber") + "  " + rsFleets.Get_Fields("Owner1Name");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label10 As object	OnWrite(string)
			// vbPorter upgrade warning: lblAddress3 As object	OnWrite(string)
			clsDRWrapper rsFleetName = new clsDRWrapper();
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
			rsFleetName.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted <> 1 AND FleetNumber = " + this.Fields["Binder"].Value);
			lblFleetOwner.Text = rsFleetName.Get_Fields_String("Owner1Name");
			lblAddress1.Text = rsFleetName.Get_Fields_String("Address");
			lblAddress2.Text = rsFleetName.Get_Fields_String("Address2");
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleetName.Get_Fields_String("Zip4"))) == "")
			{
				lblAddress3.Text = rsFleetName.Get_Fields_String("City") + ", " + rsFleetName.Get_Fields("State") + " " + rsFleetName.Get_Fields_String("Zip");
			}
			else
			{
				lblAddress3.Text = rsFleetName.Get_Fields_String("City") + ", " + rsFleetName.Get_Fields("State") + " " + rsFleetName.Get_Fields_String("Zip") + "-" + rsFleetName.Get_Fields_String("Zip4");
			}
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(lblAddress1)) == "")
			{
				lblAddress1.Text = lblAddress2.Text;
				lblAddress2.Text = lblAddress3.Text;
				lblAddress3.Text = "";
			}
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(lblAddress1)) == "")
			{
				lblAddress1.Text = lblAddress2.Text;
				lblAddress2.Text = lblAddress3.Text;
				lblAddress3.Text = "";
			}
			if (fecherFoundation.Strings.Trim(FCConvert.ToString(lblAddress2)) == "")
			{
				lblAddress2.Text = lblAddress3.Text;
				lblAddress3.Text = "";
			}
			rsFleetName.DisposeOf();
		}

		private void rptFleetReminderListStaab_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptFleetReminderListStaab properties;
			//rptFleetReminderListStaab.Caption	= "Fleet Reminder List";
			//rptFleetReminderListStaab.Icon	= "rptFleetExpirationListStaab.dsx":0000";
			//rptFleetReminderListStaab.Left	= 0;
			//rptFleetReminderListStaab.Top	= 0;
			//rptFleetReminderListStaab.Width	= 11880;
			//rptFleetReminderListStaab.Height	= 8595;
			//rptFleetReminderListStaab.StartUpPosition	= 3;
			//rptFleetReminderListStaab.SectionData	= "rptFleetExpirationListStaab.dsx":508A;
			//End Unmaped Properties
		}
	}
}
