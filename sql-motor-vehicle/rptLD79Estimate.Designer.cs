﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLD79Estimate.
	/// </summary>
	partial class rptLD79Estimate
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLD79Estimate));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMilYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalExcise = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMilYear3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMilYear5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear1Proposed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear2Proposed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalExciseProposed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear3Proposed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear4Proposed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear5Proposed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear6Proposed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRate3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRate4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRate5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRate6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMilYear1Count = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear2Count = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear3Count = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear4Count = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear5Count = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMilYear6Count = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeading = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear1Proposed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear2Proposed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalExciseProposed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear3Proposed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear4Proposed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear5Proposed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear6Proposed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear1Count)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear2Count)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear3Count)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear4Count)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear5Count)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear6Count)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label12,
            this.Label13,
            this.fldMilYear1,
            this.fldMilYear2,
            this.Label14,
            this.fldTotalExcise,
            this.Label15,
            this.Label16,
            this.fldMilYear3,
            this.fldMilYear4,
            this.Label17,
            this.Label18,
            this.fldMilYear5,
            this.fldMilYear6,
            this.fldMilYear1Proposed,
            this.fldMilYear2Proposed,
            this.fldTotalExciseProposed,
            this.fldMilYear3Proposed,
            this.fldMilYear4Proposed,
            this.fldMilYear5Proposed,
            this.fldMilYear6Proposed,
            this.Line2,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Line3,
            this.Label23,
            this.Label24,
            this.Label25,
            this.fldRate1,
            this.fldRate2,
            this.fldRate3,
            this.fldRate4,
            this.fldRate5,
            this.fldRate6,
            this.Label26,
            this.fldMilYear1Count,
            this.fldMilYear2Count,
            this.fldMilYear3Count,
            this.fldMilYear4Count,
            this.fldMilYear5Count,
            this.fldMilYear6Count,
            this.fldTotalCount});
			this.Detail.Height = 3.083F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.375F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "text-align: right";
			this.Label12.Text = "Mil Year 1:";
			this.Label12.Top = 0.3125F;
			this.Label12.Width = 1.03125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "text-align: right";
			this.Label13.Text = "Mil Year 2:";
			this.Label13.Top = 0.552F;
			this.Label13.Width = 1.03125F;
			// 
			// fldMilYear1
			// 
			this.fldMilYear1.Height = 0.15F;
			this.fldMilYear1.Left = 1.46875F;
			this.fldMilYear1.Name = "fldMilYear1";
			this.fldMilYear1.Style = "text-align: right";
			this.fldMilYear1.Text = "Field1";
			this.fldMilYear1.Top = 0.3125F;
			this.fldMilYear1.Width = 1.15F;
			// 
			// fldMilYear2
			// 
			this.fldMilYear2.Height = 0.15F;
			this.fldMilYear2.Left = 1.469F;
			this.fldMilYear2.Name = "fldMilYear2";
			this.fldMilYear2.Style = "text-align: right";
			this.fldMilYear2.Text = "Field1";
			this.fldMilYear2.Top = 0.558F;
			this.fldMilYear2.Width = 1.15F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.375F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "text-align: right";
			this.Label14.Text = "Total Excise:";
			this.Label14.Top = 1.77F;
			this.Label14.Width = 1.03125F;
			// 
			// fldTotalExcise
			// 
			this.fldTotalExcise.Height = 0.15F;
			this.fldTotalExcise.Left = 1.469F;
			this.fldTotalExcise.Name = "fldTotalExcise";
			this.fldTotalExcise.Style = "text-align: right";
			this.fldTotalExcise.Text = "Field1";
			this.fldTotalExcise.Top = 1.769F;
			this.fldTotalExcise.Width = 1.15F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "text-align: right";
			this.Label15.Text = "Mil Year 3:";
			this.Label15.Top = 0.808F;
			this.Label15.Width = 1.03125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.375F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "text-align: right";
			this.Label16.Text = "Mil Year 4:";
			this.Label16.Top = 1.05F;
			this.Label16.Width = 1.03125F;
			// 
			// fldMilYear3
			// 
			this.fldMilYear3.Height = 0.15F;
			this.fldMilYear3.Left = 1.469F;
			this.fldMilYear3.Name = "fldMilYear3";
			this.fldMilYear3.Style = "text-align: right";
			this.fldMilYear3.Text = "Field1";
			this.fldMilYear3.Top = 0.814F;
			this.fldMilYear3.Width = 1.15F;
			// 
			// fldMilYear4
			// 
			this.fldMilYear4.Height = 0.15F;
			this.fldMilYear4.Left = 1.469F;
			this.fldMilYear4.Name = "fldMilYear4";
			this.fldMilYear4.Style = "text-align: right";
			this.fldMilYear4.Text = "Field1";
			this.fldMilYear4.Top = 1.056F;
			this.fldMilYear4.Width = 1.15F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.375F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "text-align: right";
			this.Label17.Text = "Mil Year 5:";
			this.Label17.Top = 1.27075F;
			this.Label17.Width = 1.03125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.375F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "text-align: right";
			this.Label18.Text = "Mil Year 6:";
			this.Label18.Top = 1.52F;
			this.Label18.Width = 1.03125F;
			// 
			// fldMilYear5
			// 
			this.fldMilYear5.Height = 0.15F;
			this.fldMilYear5.Left = 1.46875F;
			this.fldMilYear5.Name = "fldMilYear5";
			this.fldMilYear5.Style = "text-align: right";
			this.fldMilYear5.Text = "Field1";
			this.fldMilYear5.Top = 1.27075F;
			this.fldMilYear5.Width = 1.15F;
			// 
			// fldMilYear6
			// 
			this.fldMilYear6.Height = 0.15F;
			this.fldMilYear6.Left = 1.469F;
			this.fldMilYear6.Name = "fldMilYear6";
			this.fldMilYear6.Style = "text-align: right";
			this.fldMilYear6.Text = "Field1";
			this.fldMilYear6.Top = 1.526F;
			this.fldMilYear6.Width = 1.15F;
			// 
			// fldMilYear1Proposed
			// 
			this.fldMilYear1Proposed.Height = 0.15F;
			this.fldMilYear1Proposed.Left = 2.656F;
			this.fldMilYear1Proposed.Name = "fldMilYear1Proposed";
			this.fldMilYear1Proposed.Style = "text-align: right";
			this.fldMilYear1Proposed.Text = "Field1";
			this.fldMilYear1Proposed.Top = 0.31275F;
			this.fldMilYear1Proposed.Width = 1.15F;
			// 
			// fldMilYear2Proposed
			// 
			this.fldMilYear2Proposed.Height = 0.15F;
			this.fldMilYear2Proposed.Left = 2.65625F;
			this.fldMilYear2Proposed.Name = "fldMilYear2Proposed";
			this.fldMilYear2Proposed.Style = "text-align: right";
			this.fldMilYear2Proposed.Text = "Field1";
			this.fldMilYear2Proposed.Top = 0.55825F;
			this.fldMilYear2Proposed.Width = 1.15F;
			// 
			// fldTotalExciseProposed
			// 
			this.fldTotalExciseProposed.Height = 0.15F;
			this.fldTotalExciseProposed.Left = 2.656F;
			this.fldTotalExciseProposed.Name = "fldTotalExciseProposed";
			this.fldTotalExciseProposed.Style = "text-align: right";
			this.fldTotalExciseProposed.Text = "Field1";
			this.fldTotalExciseProposed.Top = 1.76975F;
			this.fldTotalExciseProposed.Width = 1.15F;
			// 
			// fldMilYear3Proposed
			// 
			this.fldMilYear3Proposed.Height = 0.15F;
			this.fldMilYear3Proposed.Left = 2.65625F;
			this.fldMilYear3Proposed.Name = "fldMilYear3Proposed";
			this.fldMilYear3Proposed.Style = "text-align: right";
			this.fldMilYear3Proposed.Text = "Field1";
			this.fldMilYear3Proposed.Top = 0.81425F;
			this.fldMilYear3Proposed.Width = 1.15F;
			// 
			// fldMilYear4Proposed
			// 
			this.fldMilYear4Proposed.Height = 0.15F;
			this.fldMilYear4Proposed.Left = 2.65625F;
			this.fldMilYear4Proposed.Name = "fldMilYear4Proposed";
			this.fldMilYear4Proposed.Style = "text-align: right";
			this.fldMilYear4Proposed.Text = "Field1";
			this.fldMilYear4Proposed.Top = 1.05625F;
			this.fldMilYear4Proposed.Width = 1.15F;
			// 
			// fldMilYear5Proposed
			// 
			this.fldMilYear5Proposed.Height = 0.15F;
			this.fldMilYear5Proposed.Left = 2.656F;
			this.fldMilYear5Proposed.Name = "fldMilYear5Proposed";
			this.fldMilYear5Proposed.Style = "text-align: right";
			this.fldMilYear5Proposed.Text = "Field1";
			this.fldMilYear5Proposed.Top = 1.271F;
			this.fldMilYear5Proposed.Width = 1.15F;
			// 
			// fldMilYear6Proposed
			// 
			this.fldMilYear6Proposed.Height = 0.15F;
			this.fldMilYear6Proposed.Left = 2.65625F;
			this.fldMilYear6Proposed.Name = "fldMilYear6Proposed";
			this.fldMilYear6Proposed.Style = "text-align: right";
			this.fldMilYear6Proposed.Text = "Field1";
			this.fldMilYear6Proposed.Top = 1.52625F;
			this.fldMilYear6Proposed.Width = 1.15F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.5F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.281F;
			this.Line2.Width = 4.801F;
			this.Line2.X1 = 1.5F;
			this.Line2.X2 = 6.301F;
			this.Line2.Y1 = 0.281F;
			this.Line2.Y2 = 0.281F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 1.657F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold; text-align: right";
			this.Label19.Text = "Current";
			this.Label19.Top = 0.094F;
			this.Label19.Width = 0.9375F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.84425F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold; text-align: right";
			this.Label20.Text = "Proposed";
			this.Label20.Top = 0.09425002F;
			this.Label20.Width = 0.9375F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.156F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-weight: bold";
			this.Label21.Text = "WARNING:";
			this.Label21.Top = 2.264F;
			this.Label21.Visible = false;
			this.Label21.Width = 0.84375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.71875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 1.06225F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-weight: bold";
			this.Label22.Text = resources.GetString("Label22.Text");
			this.Label22.Top = 2.264F;
			this.Label22.Visible = false;
			this.Label22.Width = 5.28125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.7190005F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 1.68225F;
			this.Line3.Width = 4.54425F;
			this.Line3.X1 = 0.7190005F;
			this.Line3.X2 = 5.26325F;
			this.Line3.Y1 = 1.68225F;
			this.Line3.Y2 = 1.68225F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 3.788F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-size: 14pt; font-weight: bold";
			this.Label23.Text = "*";
			this.Label23.Top = 1.526F;
			this.Label23.Visible = false;
			this.Label23.Width = 0.15625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 3.78775F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 14pt; font-weight: bold";
			this.Label24.Text = "*";
			this.Label24.Top = 1.77025F;
			this.Label24.Visible = false;
			this.Label24.Width = 0.15625F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.19F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 5.2505F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-weight: bold; text-align: right";
			this.Label25.Text = "Proposed Rate";
			this.Label25.Top = 0.09400003F;
			this.Label25.Width = 1.0625F;
			// 
			// fldRate1
			// 
			this.fldRate1.Height = 0.156F;
			this.fldRate1.Left = 5.187501F;
			this.fldRate1.Name = "fldRate1";
			this.fldRate1.Style = "text-align: right";
			this.fldRate1.Text = "Field1";
			this.fldRate1.Top = 0.31225F;
			this.fldRate1.Width = 1.06F;
			// 
			// fldRate2
			// 
			this.fldRate2.Height = 0.156F;
			this.fldRate2.Left = 5.187751F;
			this.fldRate2.Name = "fldRate2";
			this.fldRate2.Style = "text-align: right";
			this.fldRate2.Text = "Field1";
			this.fldRate2.Top = 0.55775F;
			this.fldRate2.Width = 1.06F;
			// 
			// fldRate3
			// 
			this.fldRate3.Height = 0.156F;
			this.fldRate3.Left = 5.187751F;
			this.fldRate3.Name = "fldRate3";
			this.fldRate3.Style = "text-align: right";
			this.fldRate3.Text = "Field1";
			this.fldRate3.Top = 0.81375F;
			this.fldRate3.Width = 1.06F;
			// 
			// fldRate4
			// 
			this.fldRate4.Height = 0.156F;
			this.fldRate4.Left = 5.187751F;
			this.fldRate4.Name = "fldRate4";
			this.fldRate4.Style = "text-align: right";
			this.fldRate4.Text = "Field1";
			this.fldRate4.Top = 1.05575F;
			this.fldRate4.Width = 1.06F;
			// 
			// fldRate5
			// 
			this.fldRate5.Height = 0.156F;
			this.fldRate5.Left = 5.187501F;
			this.fldRate5.Name = "fldRate5";
			this.fldRate5.Style = "text-align: right";
			this.fldRate5.Text = "Field1";
			this.fldRate5.Top = 1.2705F;
			this.fldRate5.Width = 1.06F;
			// 
			// fldRate6
			// 
			this.fldRate6.Height = 0.156F;
			this.fldRate6.Left = 5.187751F;
			this.fldRate6.Name = "fldRate6";
			this.fldRate6.Style = "text-align: right";
			this.fldRate6.Text = "Field1";
			this.fldRate6.Top = 1.52575F;
			this.fldRate6.Width = 1.06F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.19F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 4.03175F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-weight: bold; text-align: right";
			this.Label26.Text = "Vehicle Count";
			this.Label26.Top = 0.09400003F;
			this.Label26.Width = 1.03125F;
			// 
			// fldMilYear1Count
			// 
			this.fldMilYear1Count.Height = 0.15F;
			this.fldMilYear1Count.Left = 4.037F;
			this.fldMilYear1Count.Name = "fldMilYear1Count";
			this.fldMilYear1Count.Style = "text-align: right";
			this.fldMilYear1Count.Text = "Field1";
			this.fldMilYear1Count.Top = 0.313F;
			this.fldMilYear1Count.Width = 1.15F;
			// 
			// fldMilYear2Count
			// 
			this.fldMilYear2Count.Height = 0.15F;
			this.fldMilYear2Count.Left = 4.037251F;
			this.fldMilYear2Count.Name = "fldMilYear2Count";
			this.fldMilYear2Count.Style = "text-align: right";
			this.fldMilYear2Count.Text = "Field1";
			this.fldMilYear2Count.Top = 0.5585F;
			this.fldMilYear2Count.Width = 1.15F;
			// 
			// fldMilYear3Count
			// 
			this.fldMilYear3Count.Height = 0.15F;
			this.fldMilYear3Count.Left = 4.037251F;
			this.fldMilYear3Count.Name = "fldMilYear3Count";
			this.fldMilYear3Count.Style = "text-align: right";
			this.fldMilYear3Count.Text = "Field1";
			this.fldMilYear3Count.Top = 0.8145001F;
			this.fldMilYear3Count.Width = 1.15F;
			// 
			// fldMilYear4Count
			// 
			this.fldMilYear4Count.Height = 0.15F;
			this.fldMilYear4Count.Left = 4.037251F;
			this.fldMilYear4Count.Name = "fldMilYear4Count";
			this.fldMilYear4Count.Style = "text-align: right";
			this.fldMilYear4Count.Text = "Field1";
			this.fldMilYear4Count.Top = 1.0565F;
			this.fldMilYear4Count.Width = 1.15F;
			// 
			// fldMilYear5Count
			// 
			this.fldMilYear5Count.Height = 0.15F;
			this.fldMilYear5Count.Left = 4.037F;
			this.fldMilYear5Count.Name = "fldMilYear5Count";
			this.fldMilYear5Count.Style = "text-align: right";
			this.fldMilYear5Count.Text = "Field1";
			this.fldMilYear5Count.Top = 1.27125F;
			this.fldMilYear5Count.Width = 1.15F;
			// 
			// fldMilYear6Count
			// 
			this.fldMilYear6Count.Height = 0.15F;
			this.fldMilYear6Count.Left = 4.037251F;
			this.fldMilYear6Count.Name = "fldMilYear6Count";
			this.fldMilYear6Count.Style = "text-align: right";
			this.fldMilYear6Count.Text = "Field1";
			this.fldMilYear6Count.Top = 1.5265F;
			this.fldMilYear6Count.Width = 1.15F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.15F;
			this.fldTotalCount.Left = 4.037F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "text-align: right";
			this.fldTotalCount.Text = "Field1";
			this.fldTotalCount.Top = 1.77F;
			this.fldTotalCount.Width = 1.15F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeading,
            this.Label1,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Line1});
			this.PageHeader.Height = 0.8645833F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// lblHeading
			// 
			this.lblHeading.Height = 0.375F;
			this.lblHeading.HyperLink = null;
			this.lblHeading.Left = 0F;
			this.lblHeading.Name = "lblHeading";
			this.lblHeading.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.lblHeading.Text = "The excise tax estimates are based on New Registrations, Re Registrations, New Re" +
    "g Transfers, and Re Reg Transfers completed in 2006.";
			this.lblHeading.Top = 0.4375F;
			this.lblHeading.Width = 6.46875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.59375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
			this.Label1.Text = "Excise Rate Comparison";
			this.Label1.Top = 0F;
			this.Label1.Width = 3.5F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label8.Text = "Label8";
			this.Label8.Top = 0.1875F;
			this.Label8.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.1875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.1875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label11.Text = "Label11";
			this.Label11.Top = 0F;
			this.Label11.Width = 1.3125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.84375F;
			this.Line1.Width = 6.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 6.5F;
			this.Line1.Y1 = 0.84375F;
			this.Line1.Y2 = 0.84375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptLD79Estimate
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear1Proposed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear2Proposed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalExciseProposed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear3Proposed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear4Proposed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear5Proposed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear6Proposed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRate6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear1Count)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear2Count)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear3Count)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear4Count)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear5Count)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMilYear6Count)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalExcise;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear1Proposed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear2Proposed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalExciseProposed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear3Proposed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear4Proposed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear5Proposed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear6Proposed;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRate3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRate4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRate5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRate6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear1Count;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear2Count;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear3Count;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear4Count;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear5Count;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMilYear6Count;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
