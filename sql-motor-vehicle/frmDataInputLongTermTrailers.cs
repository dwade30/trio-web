//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmDataInputLongTermTrailers : BaseForm
	{
		public frmDataInputLongTermTrailers()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            Label2 = new System.Collections.Generic.List<FCLabel>();
            this.Label2.AddControlArrayElement(Label2_0, 0);
            this.Label2.AddControlArrayElement(Label2_1, 1);
            this.Label2.AddControlArrayElement(Label2_2, 2);
            this.Label2.AddControlArrayElement(Label2_3, 3);
            this.Label2.AddControlArrayElement(Label2_4, 4);
            this.Label2.AddControlArrayElement(Label2_5, 5);
            this.Label2.AddControlArrayElement(Label2_6, 6);
            this.Label2.AddControlArrayElement(Label2_7, 7);
            this.Label2.AddControlArrayElement(Label2_8, 8);
            this.Label2.AddControlArrayElement(Label2_10, 9);
          
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDataInputLongTermTrailers InstancePtr
		{
			get
			{
				return (frmDataInputLongTermTrailers)Sys.GetInstance(typeof(frmDataInputLongTermTrailers));
			}
		}

		protected frmDataInputLongTermTrailers _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int NewBackColor = ColorTranslator.ToOle(Color.Yellow);
		bool blnLoadingForm;
		bool blnBadPlate;
		bool blnNextYear;

		private void cboLength_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DateTime datExpiresDate;
			if (MotorVehicle.Statics.RegistrationType == "CORR")
			{
				datExpiresDate = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate").Year));
			}
			else
			{
				if (MotorVehicle.Statics.gboolLongRegExtension || MotorVehicle.Statics.gboolLongRegExtensionSamePlate)
				{
					datExpiresDate = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate").Year));
				}
				else
				{
					if (cmbNoMaineReReg.Text == "NO")
					{
						if (DateTime.Today.Month == 12 && blnNextYear)
						{
							datExpiresDate = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(DateTime.Today.Year + 1));
						}
						else
						{
							datExpiresDate = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(DateTime.Today.Year));
						}
					}
					else if (cmbNoMaineReReg.Text == "YES - Expires this year")
					{
						datExpiresDate = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(DateTime.Today.Year));
					}
					else
					{
						datExpiresDate = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(DateTime.Today.Year + 1));
					}
				}
			}
			if (cboLength.ItemData(cboLength.ListIndex) == 24)
			{
				chk25YearPlate.CheckState = Wisej.Web.CheckState.Checked;
				datExpiresDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(datExpiresDate.Year + 1));
			}
			else
			{
				chk25YearPlate.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			datExpiresDate = fecherFoundation.DateAndTime.DateAdd("yyyy", cboLength.ItemData(cboLength.ListIndex), datExpiresDate);
			datExpiresDate = fecherFoundation.DateAndTime.DateAdd("d", -1, datExpiresDate);
			if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
			{
				txtExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("ExpireDate"), "MM/dd/yyyy");
			}
			else
			{
				txtExpires.Text = Strings.Format(datExpiresDate, "MM/dd/yyyy");
			}
			if (cboLength.ItemData(cboLength.ListIndex) < 8 || cboLength.ItemData(cboLength.ListIndex) == 24)
			{
				chkTimePayment.Enabled = false;
				chkTimePayment.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			else
			{
				if (chk25YearPlate.CheckState == Wisej.Web.CheckState.Checked)
				{
					chkTimePayment.Enabled = false;
					chkTimePayment.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				else
				{
					if (MotorVehicle.Statics.gblnLongTermTimePaymentsAllowed)
					{
						chkTimePayment.Enabled = true;
					}
				}
			}
			Recalculate();
		}

		public void cboLength_Click()
		{
			cboLength_SelectedIndexChanged(cboLength, new System.EventArgs());
		}

		private void chk25YearPlate_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chk25YearPlate.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkTimePayment.Enabled = false;
				chkTimePayment.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			else
			{
				if (MotorVehicle.Statics.gblnLongTermTimePaymentsAllowed)
				{
					chkTimePayment.Enabled = true;
				}
			}
			Recalculate();
		}

		private void chkTimePayment_CheckedChanged(object sender, System.EventArgs e)
		{
			Recalculate();
		}

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtMessage.Text) != "" && cboMessageCodes.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a message code to go with your message before you continue to process this registration.", "Invalid Message Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (cboMessageCodes.SelectedIndex != -1 && fecherFoundation.Strings.Trim(txtMessage.Text) == "")
			{
				MessageBox.Show("You must input a message to go with your code before you continue processing this registration.", "No Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			fraAdditionalInfo.Visible = false;
		}

		private void cmdErase_Click(object sender, System.EventArgs e)
		{
			cboMessageCodes.SelectedIndex = -1;
			txtMessage.Text = "";
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			fraFeesInfo.Visible = false;
			fraDataInput.Enabled = true;
			txtMake.Focus();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdMoreInfo_Click(object sender, System.EventArgs e)
		{
			fraAdditionalInfo.Visible = true;
			cmbYes.Focus();
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			CalculateTotal();
			txtRate.Text = Strings.Format(txtFEERegFee.Text, "#,##0.00");
			txtCredit.Text = Strings.Format(txtFEECreditTransfer.Text, "#,##0.00");
			txtFees.Text = Strings.Format(FCConvert.ToDecimal(txtFEETotal.Text) - FCConvert.ToDecimal(txtFEEAgentFeeReg.Text) - FCConvert.ToDecimal(txtFeeAgentFeeCTA.Text) - FCConvert.ToDecimal(txtFeeAgentFeeCorrection.Text) - FCConvert.ToDecimal(txtFEETitleFee.Text) - FCConvert.ToDecimal(txtFEERushTitleFee.Text) - FCConvert.ToDecimal(txtFEESalesTax.Text), "#,##0.00");
			if (MotorVehicle.Statics.RegistrationType != "CORR")
			{
				lblTotalDue.Text = "Total Amount Due   " + Strings.Format(txtFEETotal.Text, "Currency");
			}
			else
			{
				CalculateLongTermTrailerECorrectFee();
			}
			cmdExit_Click();
		}

		private void frmDataInputLongTermTrailers_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (blnBadPlate)
			{
				MessageBox.Show("You can't register this vehicle for " + MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"), "Invalid Expiration", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
				return;
			}
			this.Refresh();
		}

		private void frmDataInputLongTermTrailers_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
				if (!(FCGlobal.Screen.ActiveControl is FCComboBox) && !(FCGlobal.Screen.ActiveControl is FCCheckBox) && !(FCGlobal.Screen.ActiveControl is FCRadioButton))
				{
					if (((FCGlobal.Screen.ActiveControl as T2KBackFillWhole).MaxLength == (FCGlobal.Screen.ActiveControl as T2KBackFillWhole).SelectionStart && (FCGlobal.Screen.ActiveControl as T2KBackFillWhole).MaxLength != 0) && KeyCode != Keys.Return && KeyCode != Keys.Down && KeyCode != Keys.Up && KeyCode != Keys.Left)
					{
						KeyCode = (Keys)0;
						//App.DoEvents();
						Support.SendKeys("{TAB}", false);
					}
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		private void frmDataInputLongTermTrailers_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper defRS = new clsDRWrapper();
			clsDRWrapper rsResCode = new clsDRWrapper();
			clsDRWrapper rsFleet = new clsDRWrapper();
			// vbPorter upgrade warning: intStartYear As int	OnWriteFCConvert.ToInt32(
			int intStartYear = 0;
			// vbPorter upgrade warning: intLengthOfTime As int	OnWriteFCConvert.ToInt32(
			int intLengthOfTime = 0;
			blnLoadingForm = true;
			blnNextYear = false;
			LoadLengthCombo();
			cboLength.SelectedIndex = 0;
			chkTimePayment.Enabled = MotorVehicle.Statics.gblnLongTermTimePaymentsAllowed;
			chk25YearPlate.Enabled = MotorVehicle.Statics.gintLongTermYearsAllowed == 24;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetCustomFormColors();
			defRS.OpenRecordset("SELECT * FROM DefaultInfo");
			// Set DefaultInfo
			blnBadPlate = false;
			SetUserFields();
			FillExtraInfo();
			if (MotorVehicle.Statics.RegistrationType != "HELD" && MotorVehicle.Statics.RegistrationType != "CORR")
			{
				if (defRS.EndOfFile() != true && defRS.BeginningOfFile() != true)
				{
					txtCity.Text = FCConvert.ToString(defRS.Get_Fields_String("Town"));
					txtState.Text = FCConvert.ToString(defRS.Get_Fields("State"));
					txtAddress1.Text = FCConvert.ToString(defRS.Get_Fields_String("Address"));
					txtLegalZip.Text = FCConvert.ToString(defRS.Get_Fields_String("ResidenceCode"));
					txtZip.Text = Strings.Mid(FCConvert.ToString(defRS.Get_Fields_String("Zip")), 1, 5);
				}
				clsDRWrapper temp = MotorVehicle.Statics.rsFinal;
				FillOwnerInfo(ref temp);
				MotorVehicle.Statics.rsFinal = temp;
				if (MotorVehicle.Statics.RegistrationType == "RRR" || MotorVehicle.Statics.RegistrationType == "RRT")
				{
					if (!MotorVehicle.Statics.gboolLongRegExtension)
					{
						if (!MotorVehicle.Statics.NewToTheSystem)
						{
							if (MotorVehicle.Statics.RegistrationType == "RRT")
							{
								MotorVehicle.Statics.rsFinalCompare.Edit();
								MotorVehicle.Statics.rsFinalCompare.Set_Fields("ExpireDate", MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate"));
								MotorVehicle.Statics.rsFinalCompare.Update();
							}
							if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate").Year) <= DateTime.Today.Year)
							{
								cmbNoMaineReReg.Text = "YES - Expires this year";
							}
							else
							{
								cmbNoMaineReReg.Text = "YES - Expires next year";
							}
						}
						else
						{
							cmbNoMaineReReg.Text = "YES - Expires this year";
						}
					}
					txtUnit.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
					txtStyle.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
					txtModel.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Model"));
					// kk01252016 tromv-1080
					txtColor1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color1"));
					txtColor2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color2"));
					txtMake.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make"));
					txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
					txtVIN.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin"));
					txtNetWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"));
				}
				else
				{
					cmbNoMaineReReg.Text = "NO";
				}
				if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber")) != 0)
				{
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("SubClass")) == "L9")
					{
						if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetOld") == true || MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetNew") == true)
						{
							// do nothing
						}
						else
						{
							MotorVehicle.Statics.rsFinal.Set_Fields("FleetOld", true);
						}
					}
					else
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("FleetNumber", 0);
						MotorVehicle.Statics.rsFinal.Set_Fields("FleetOld", false);
						MotorVehicle.Statics.rsFinal.Set_Fields("FleetNew", false);
					}
				}
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
				{
					cboLength.Enabled = false;
                    if (MotorVehicle.Statics.gboolLongRegExtensionSamePlate)
                    {
                        cboLength.Enabled = true;
                    }
					if (MotorVehicle.Statics.gboolLongRegExtension)
					{
						intStartYear = ((DateTime)MotorVehicle.Statics.rsFinalCompare.Get_Fields_DateTime("ExpireDate")).Year;
					}
					else
					{
						if (cmbNoMaineReReg.Text == "NO")
						{
							if (DateTime.Today.Month == 12)
							{
								if (MessageBox.Show("Should this registration become effectve next year?", "Register for Next Year?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									blnNextYear = true;
									intStartYear = (DateTime.Today.Year + 1);
									cboLength_Click();
								}
								else
								{
									intStartYear = DateTime.Today.Year;
								}
							}
							else
							{
								intStartYear = DateTime.Today.Year;
							}
						}
						else if (cmbNoMaineReReg.Text == "YES - Expires this year")
						{
							intStartYear = DateTime.Today.Year;
						}
						else
						{
							intStartYear = (DateTime.Today.Year + 1);
						}
					}
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N")
					{
						intLengthOfTime = FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) - intStartYear - 2;
						if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
						{
							if (intLengthOfTime < 2)
							{
								intLengthOfTime = 2;
							}
						}
						if (intLengthOfTime < 2)
						{
							blnBadPlate = true;
						}
						else
						{
							if (intLengthOfTime >= GetMaxLengthComboValue())
							{
								blnBadPlate = true;
							}
							else
                            {
                                SetLengthCombo(intLengthOfTime);
                            }
						}
					}
					else
					{
                        if (!(MotorVehicle.Statics.RegistrationType == "RRR" && MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "S"))
                        {
                            if (2000 + FCConvert.ToInt16(FCConvert.ToDouble(Strings.Left(
                                    FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")), 2))) -
                                intStartYear - 2 >= cboLength.Items.Count)
                            {
                                blnBadPlate = true;
                            }
                            else
                            {
                                intLengthOfTime =
                                    2000 + FCConvert.ToInt16(FCConvert.ToDouble(
                                        Strings.Left(
                                            FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")),
                                            2))) - intStartYear - 2;
                                if (MotorVehicle.Statics.RegistrationType == "NRT" ||
                                    MotorVehicle.Statics.RegistrationType == "RRT")
                                {
                                    if (intLengthOfTime < 2)
                                    {
                                        intLengthOfTime = 2;
                                    }
                                }

                                if (intLengthOfTime < 2)
                                {
                                    blnBadPlate = true;
                                }
                                else
                                {
                                    SetLengthCombo(intLengthOfTime);
                                }
                            }
                        }
                    }
				}
				else
				{
					if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
					{
						cboLength.Enabled = false;
						if (cmbNoMaineReReg.Text == "NO")
						{
							if (DateTime.Today.Month == 12)
							{
								intStartYear = DateTime.Today.Year + 1;
							}
							else
							{
								intStartYear = DateTime.Today.Year;
							}
						}
						else if (cmbNoMaineReReg.Text == "YES - Expires this year")
						{
							intStartYear = DateTime.Today.Year;
						}
						else
						{
							intStartYear = DateTime.Today.Year + 1;
						}
						intLengthOfTime = FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Year) - intStartYear - 2;
						if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
						{
							if (intLengthOfTime < 2)
							{
								intLengthOfTime = 2;
							}
						}
						if (intLengthOfTime < 2)
						{
							blnBadPlate = true;
						}
						else
						{
							if (intLengthOfTime >= GetMaxLengthComboValue())
							{
								blnBadPlate = true;
							}
							else
                            {
                                SetLengthCombo(intLengthOfTime);
                            }
						}
					}
					else
					{
						if (cmbNoMaineReReg.Text == "NO" && DateTime.Today.Month == 12)
						{
							if (MessageBox.Show("Should this registration become effective next year?", "Register for Next Year?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								blnNextYear = true;
								cboLength_Click();
							}
						}
						cboLength.Enabled = true;
					}
				}
				if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) > 2000 && FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) < 3000)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Plate", MotorVehicle.GetLongTermPlate(FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"))));
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
				}
				txtPlate.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
			}
			else if (MotorVehicle.Statics.RegistrationType == "CORR")
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("TransactionType", "ECO");
				if ((FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Year) - FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate").Year)  >= GetMaxLengthComboValue()))
				{
					blnBadPlate = true;
				}
				else
				{
					SetLengthCombo (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Year) - FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate").Year) );
				}
				txtAddress1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
				txtCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
				txtState.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("State"));
				txtZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
				txtFeeAgentFeeCorrection.Text = Strings.Format(FCConvert.ToString(Conversion.Val(defRS.Get_Fields_Decimal("AgentFeeLongTermTrailerCorrection"))), "#,##0.00");
				txtFEEAgentFeeReg.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee"), "#,##0.00");
				txtFeeAgentFeeCTA.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCTA"))), "#,##0.00");
				txtFeeAgentFeeTransfer.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeTransfer"))), "#,##0.00");
				txtReg1PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyId1"));
				GetPartyInfo(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"), 1);
				if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2")) != 0)
				{
					txtReg2PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"));
					GetPartyInfo(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"), 2);
				}
				else
				{
					ClearPartyInfo(2);
				}
				// 
				txtLegalResCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
				txtLegalResState.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
				txtLegalResAddress.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResAddress"));
				txtLegalZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
				txtUnit.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
				txtStyle.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
				txtModel.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Model"));
				// kk01252016 tromv-1080
				txtColor1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color1"));
				txtColor2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color2"));
				txtMake.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make"));
				txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
				txtVIN.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin"));
				txtNetWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"));
				txtFEETitleFee.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"), "#,##0.00");
				txtFEERushTitleFee.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("RushTitleFee"), "#,##0.00");
				txtFEESalesTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"), "#,##0.00");
				txtFEERegFee.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"), "#,##0.00");
				txtFEECreditTransfer.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed"), "#,##0.00");
				txtFEETransferFee.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee"), "#,##0.00");
				if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) > 2000 && FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) < 3000)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Plate", MotorVehicle.GetLongTermPlate(FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"))));
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
				}
				txtPlate.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
				if (MotorVehicle.Statics.PlateType == 2)
				{
					if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber")) != 0)
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("SubClass")) == "L9")
						{
							if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetOld") == true || MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetNew") == true)
							{
								// do nothing
							}
							else
							{
								MotorVehicle.Statics.rsFinal.Set_Fields("FleetOld", true);
							}
						}
						else
						{
							MotorVehicle.Statics.rsFinal.Set_Fields("FleetNumber", 0);
							MotorVehicle.Statics.rsFinal.Set_Fields("FleetOld", false);
							MotorVehicle.Statics.rsFinal.Set_Fields("FleetNew", false);
						}
					}
				}
			}
			else if (MotorVehicle.Statics.RegistrationType == "HELD")
			{
				if ((((DateTime)MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")).Year - ((DateTime)MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate")).Year)  >= GetMaxLengthComboValue())
				{
					blnBadPlate = true;
				}
				else
				{
					 SetLengthCombo (((DateTime)MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")).Year - ((DateTime)MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate")).Year) ;
				}
				txtAddress1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
				txtCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
				txtState.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("State"));
				txtZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
				txtFEEAgentFeeReg.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee"), "#,##0.00");
				txtFeeAgentFeeCTA.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCTA"))), "#,##0.00");
				txtFeeAgentFeeTransfer.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeTransfer"))), "#,##0.00");
				txtReg1PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyId1"));
				GetPartyInfo(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"), 1);
				if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2")) != 0)
				{
					txtReg2PartyID.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"));
					GetPartyInfo(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"), 2);
				}
				else
				{
					ClearPartyInfo(2);
				}
				// 
				txtLegalResCity.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
				txtLegalResState.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
				txtLegalResAddress.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResAddress"));
				txtLegalZip.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
				txtUnit.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
				txtStyle.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
				txtModel.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Model"));
				// kk01252016 tromv-1080
				txtColor1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color1"));
				txtColor2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color2"));
				txtMake.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("make"));
				txtYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
				txtVIN.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin"));
				txtNetWeight.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"));
				txtFEETitleFee.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"), "#,##0.00");
				txtFEERushTitleFee.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("RushTitleFee"), "#,##0.00");
				txtFEESalesTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"), "#,##0.00");
				txtFEERegFee.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"), "#,##0.00");
				txtFEECreditTransfer.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed"), "#,##0.00");
				MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", txtExpires.Text);
				txtFEETransferFee.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee"), "#,##0.00");
				if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) > 2000 && FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")) < 3000)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Plate", MotorVehicle.GetLongTermPlate(FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"))));
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
				}
				txtPlate.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
				MotorVehicle.Statics.RegistrationType = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType"));
			}
			if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber")) != 0)
			{
				rsFleet.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted <> 1 AND FleetNumber = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
				if (rsFleet.EndOfFile() != true && rsFleet.BeginningOfFile() != true)
				{
					if (FCConvert.ToInt32(rsFleet.Get_Fields_Int32("ExpiryMonth")) == 99)
					{
						cmbFleet.Text = "Group";
					}
					else
					{
						cmbFleet.Text = "Fleet";
					}
				}
				else
				{
					cmbFleet.Text = "Fleet";
				}
				if (!MotorVehicle.Statics.rsFinal.IsFieldNull("FleetNumber"))
				{
					txtFleetNumber.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
					if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
					{
						// do nothing
					}
					else
					{
						txtFleetNumber_Validate(false);
					}
				}

			}
			else
			{
				cmbFleet.Text = "No";
			}
			cmbFleet.TabStop = false;
			MotorVehicle.Statics.rsFinal.Set_Fields("Subclass", "L9");
			blnLoadingForm = false;
			Recalculate();
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT")
			{
				txtLegalResCity.Text = "MMTA SERVICES INC. 44004";
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "ACE REGISTRATION SERVICES LLC")
			{
				txtLegalResCity.Text = "ACE REGISTRATION";
				txtLegalResState.Text = "ME";
				txtLegalZip.Text = "44021";
			}
			else if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER")
			{
				txtLegalResCity.Text = "COUNTRYWIDE TRAILER 44018";
			}
			else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES")
			{
				txtLegalResCity.Text = "AB LEDUE ENTERPRISES";
				txtLegalResState.Text = "ME";
				txtLegalZip.Text = "44003";
			}
			else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER")
			{
				txtLegalResCity.Text = "MAINE TRAILER ME 44005";
			}
			else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
			{
				txtLegalResCity.Text = "HASKELL REGISTRATION 44002";
			}
			if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "RRR")
			{
				chk25YearPlate.Enabled = false;
				if (cboLength.ItemData(cboLength.ListIndex) == 24)
				{
					chk25YearPlate.CheckState = Wisej.Web.CheckState.Checked;
				}
			}
			else
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("TwentyFiveYearPlate"));
				if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwentyFiveYearPlate")))
				{
					chk25YearPlate.CheckState = Wisej.Web.CheckState.Checked;
				}
			}
		}

		private void frmDataInputLongTermTrailers_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraAdditionalInfo.Visible == true)
				{
					fraAdditionalInfo.Visible = false;
				}
				else if (fraFeesInfo.Visible == true)
				{
					fraFeesInfo.Visible = false;
				}
				else
				{
					mnuProcessQuit_Click();
				}
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				if (FCGlobal.Screen.ActiveControl is Global.T2KOverTypeBox || FCGlobal.Screen.ActiveControl is FCTextBox)
				{
					KeyAscii = KeyAscii - 32;
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
			{
				if (e.CloseReason == FCCloseReason.FormControlMenu)
				{
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" && (MotorVehicle.Statics.RegistrationType != "CORR" || MotorVehicle.Statics.PlateType != 1))
					{
						MotorVehicle.ReturnLongTermPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
					}
				}
			}
		}

		private void Image1_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage")), "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void Label27_Click(object sender, System.EventArgs e)
		{
			fraFeesInfo.Visible = true;
			fraDataInput.Enabled = false;
			txtFEERegFee.Focus();
		}

		private void Label29_Click(object sender, System.EventArgs e)
		{
			fraFeesInfo.Visible = true;
			fraDataInput.Enabled = false;
			txtFEECreditTransfer.Focus();
		}

		private void Label31_Click(object sender, System.EventArgs e)
		{
			fraFeesInfo.Visible = true;
			fraDataInput.Enabled = false;
			txtFEEAgentFeeReg.Focus();
		}

		private void mnuCTA_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: blnUserCancel As bool	OnWrite(bool, DialogResult)
			DialogResult blnUserCancel = DialogResult.None;
			double dblTitle = 0;
			clsDRWrapper rsTitleCost = new clsDRWrapper();
			if (fecherFoundation.Strings.Trim(txtYear.Text) == "")
			{
				MessageBox.Show("You must input a year before you may process a Title.", "Year Needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
				// ElseIf Val(txtNetWeight.Text) <= 3000 Then
				// MsgBox "A title is not required for a trailer with a net weight of 3000lbs or less.", vbExclamation, "Title Not Required"
				// Exit Sub
			}
			// If Year(Date) - 16 >= txtYear.Text Then
			if (FCConvert.ToDouble(txtYear.Text) < 1995)
			{
				MessageBox.Show("You may not process a Title on a Vehicle that was made earlier than 1995.", "Vehicle Too Old", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (MotorVehicle.Statics.blnTownPrintsLongTermUseTaxCTA)
			{
				MotorVehicle.Statics.gboolFromLongTermDataEntry = true;
				frmCTA.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				MotorVehicle.Statics.gboolFromLongTermDataEntry = false;
				Recalculate();
			}
			else
			{
				rsTitleCost.OpenRecordset("SELECT * FROM DefaultInfo");
				if (rsTitleCost.EndOfFile() != true && rsTitleCost.BeginningOfFile() != true)
				{
					rsTitleCost.MoveLast();
					rsTitleCost.MoveFirst();
					MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", rsTitleCost.Get_Fields("TitleFee"));
				}
				dblTitle = rsTitleCost.Get_Fields("TitleFee");
				object temp = dblTitle;
				bool cancel = frmInput.InstancePtr.Init(ref temp, "Input CTA Fee", "Please enter the CTA fee for this vehicle.", 1000, false, modGlobalConstants.InputDTypes.idtNumber);
				dblTitle = FCConvert.ToDouble(temp);
				if (!cancel)
				{
					blnUserCancel = DialogResult.OK;
				}
				if (blnUserCancel == DialogResult.OK)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", dblTitle);
					MotorVehicle.Statics.rsFinal.Set_Fields("TitleDone", true);
					MotorVehicle.Statics.blnPrintCTA = false;
					txtFEETitleFee.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"), "#,##0.00");
					Recalculate();
				}
				else
				{
					if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone")))
					{
						blnUserCancel = MessageBox.Show("Do you wish to delete the CTA information associated with this registration?", "Delete CTA Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (blnUserCancel != DialogResult.Yes)
						{
							MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", 0);
							MotorVehicle.Statics.rsFinal.Set_Fields("TitleDone", false);
							txtFEETitleFee.Text = Strings.Format(0, "#,##0.00");
							Recalculate();
						}
					}
				}
				return;
			}
		}

		public void mnuCTA_Click()
		{
			mnuCTA_Click(mnuCTA, new System.EventArgs());
		}

		private void mnuFileCalculate_Click(object sender, System.EventArgs e)
		{
			Recalculate();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
			{
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" && (MotorVehicle.Statics.RegistrationType != "CORR" || MotorVehicle.Statics.PlateType != 1))
				{
					MotorVehicle.ReturnLongTermPlate(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
				}
			}
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			Decimal curValue;
			string strValue = "";
			// do validation
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "ACE REGISTRATION SERVICES LLC" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER")
			{
				if (cmbFleet.Text == "Fleet" || cmbFleet.Text == "Group")
				{
					if (Conversion.Val(txtFleetNumber.Text) == 0)
					{
						MessageBox.Show("You must either enter a fleet or group number before you may proceed", "Invalid Fleet or Group Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtFleetNumber.Focus();
						return;
					}
				}
				else
				{
					MessageBox.Show("You must select a fleet or group before you may proceed.", "Select Group / Fleet", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (cmbFleet.Text == "Fleet" || cmbFleet.Text == "Group")
			{
				if (Conversion.Val(txtFleetNumber.Text) == 0)
				{
					MessageBox.Show("You must either enter a fleet or group number before you may proceed", "Invalid Fleet or Group Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtFleetNumber.Focus();
					return;
				}
			}
			if (fecherFoundation.Strings.Trim(txtMake.Text) == "")
			{
				MessageBox.Show("You must enter a make for this vehicle before you may continue.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtMake.Focus();
				return;
			}
			if (fecherFoundation.Strings.Trim(txtYear.Text) == "")
			{
				MessageBox.Show("You must enter a year for this vehicle before you may continue.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtYear.Focus();
				return;
			}
			// If Trim(txtColor1.Text) = "" Then
			// MsgBox "You must enter a color for this vehicle before you may continue.", vbInformation, "Invalid Information"
			// txtColor1.SetFocus
			// Exit Sub
			// End If
			if (fecherFoundation.Strings.Trim(txtStyle.Text) == "")
			{
				MessageBox.Show("You must enter a style for this vehicle before you may continue.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtStyle.Focus();
				return;
			}
			if (fecherFoundation.Strings.Trim(txtVIN.Text) == "" || Strings.InStr(1, txtVIN.Text, "_", CompareConstants.vbBinaryCompare) != 0)
			{
				MessageBox.Show("You must enter a VIN for this vehicle before you may continue.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtVIN.Focus();
				return;
			}
			// kk12212015 tromv-1101  Additional checks for Valid VIN - check for Duplicate
			if (VerifyVIN() == false)
			{
				txtVIN.Focus();
				return;
			}
			if (Conversion.Val(fecherFoundation.Strings.Trim(txtReg1PartyID.Text)) == 0)
			{
				MessageBox.Show("You must enter an owner for this vehicle before you may continue.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtReg1PartyID.Focus();
				return;
			}
			if (fecherFoundation.Strings.Trim(txtAddress1.Text) == "")
			{
				MessageBox.Show("You must enter an address for this vehicle before you may continue.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtAddress1.Focus();
				return;
			}
			if (fecherFoundation.Strings.Trim(txtCity.Text) == "")
			{
				MessageBox.Show("You must enter an address for this vehicle before you may continue.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtCity.Focus();
				return;
			}
			if (fecherFoundation.Strings.Trim(txtState.Text) == "")
			{
				MessageBox.Show("You must enter an address for this vehicle before you may continue.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtState.Focus();
				return;
			}
			if (fecherFoundation.Strings.Trim(txtZip.Text) == "")
			{
				MessageBox.Show("You must enter an address for this vehicle before you may continue.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtZip.Focus();
				return;
			}
			// And Year(Date) - 16 < txtYear.Text
			if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
			{
				if (MotorVehicle.Statics.ShowReminders)
				{
					if (Conversion.Val(txtNetWeight.Text) != 2000 && FCConvert.ToDouble(txtYear.Text) >= 1995)
					{
						if ((MotorVehicle.Statics.blnTownPrintsLongTermUseTaxCTA && MotorVehicle.Statics.lngCTAAddNewID == 0) || (!MotorVehicle.Statics.blnTownPrintsLongTermUseTaxCTA && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone") == false))
						{
							answer = MessageBox.Show("You have not completed your CTA form yet.  Would you like to do that now?", "CTA Form", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (answer == DialogResult.Yes)
							{
								mnuCTA_Click();
								return;
							}
						}
					}
					if ((MotorVehicle.Statics.blnTownPrintsLongTermUseTaxCTA && MotorVehicle.Statics.lngUTCAddNewID == 0) || (!MotorVehicle.Statics.blnTownPrintsLongTermUseTaxCTA && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone") == false))
					{
						answer = MessageBox.Show("You have not completed your Sales Tax form yet.  Would you like to do that now?", "Sale Tax Form", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (answer == DialogResult.Yes)
						{
							mnuUseTax_Click();
							return;
						}
					}
				}
			}
			FillRSFinal();
			if (MotorVehicle.Statics.RegistrationType == "NRR")
			{
				FillSaveInfo();
			}
			if (MotorVehicle.Statics.RegistrationType == "CORR")
			{
				string tempstr = "";
				EnterReason:
				;
				tempstr = "CORRECTED CERTIFICATE";
				tempstr = Interaction.InputBox("Please enter a reason for this correction", "Reason Needed", tempstr);
				if (tempstr == "")
				{
					MessageBox.Show("You must enter a reason before you can proceed.", "Invalid Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					goto EnterReason;
				}
				else
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("InfoMessage", tempstr);
				}
			}
			MotorVehicle.Statics.gboolFromLongTermDataEntry = true;
			frmSavePrint.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			MotorVehicle.Statics.gboolFromLongTermDataEntry = false;
		}

		private void mnuUseTax_Click(object sender, System.EventArgs e)
		{
			string str1 = "";
			clsDRWrapper rs1 = new clsDRWrapper();
			// vbPorter upgrade warning: blnUserCancel As bool	OnWrite(bool, DialogResult)
			DialogResult blnUserCancel = DialogResult.None;
			double dblSalesTax = 0;
			MotorVehicle.Statics.UseTaxFlag = true;
			if (MotorVehicle.Statics.blnTownPrintsLongTermUseTaxCTA)
			{
				MotorVehicle.Statics.gboolFromLongTermDataEntry = true;
				frmUseTax.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				MotorVehicle.Statics.gboolFromLongTermDataEntry = false;
			}
			else
			{
				dblSalesTax = MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax");
				object temp = dblSalesTax;
				bool cancel = frmInput.InstancePtr.Init(ref temp, "Input Sales Tax", "Please enter the sales tax for this vehicle.", 1000, false, modGlobalConstants.InputDTypes.idtNumber);
				if (!cancel)
				{
					blnUserCancel = DialogResult.OK;
				}
				dblSalesTax = FCConvert.ToDouble(temp);
				if (blnUserCancel == DialogResult.OK)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", dblSalesTax);
					MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", true);
					MotorVehicle.Statics.blnPrintUseTax = false;
					txtFEESalesTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"), "#,##0.00");
					Recalculate();
				}
				else
				{
					if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone")))
					{
						blnUserCancel = MessageBox.Show("Do you wish to delete the Use Tax information associated with this registration?", "Delete Use Tax Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (blnUserCancel != DialogResult.Yes)
						{
							MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", 0);
							MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", false);
							txtFEESalesTax.Text = Strings.Format(0, "#,##0.00");
							Recalculate();
						}
					}
				}
				return;
			}
			if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax")) != 0)
			{
				str1 = "SELECT * FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID);
				rs1.OpenRecordset(str1);
				if (rs1.BeginningOfFile() != true && rs1.EndOfFile() != true)
				{
					rs1.MoveLast();
					rs1.MoveFirst();
					if (FCConvert.ToString(rs1.Get_Fields_String("ExemptCode")) == "C")
					{
						txtFEESalesTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"), "#,##0.00");
					}
					else if (rs1.Get_Fields_String("ExemptCode") != "")
					{
						txtFEESalesTax.Text = Strings.Format(0, "#,##0.00");
					}
					else
					{
						txtFEESalesTax.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"), "#,##0.00");
					}
				}
			}
			else
			{
				txtFEESalesTax.Text = "0.00";
			}
			Recalculate();
		}

		public void mnuUseTax_Click()
		{
			mnuUseTax_Click(mnuUseTax, new System.EventArgs());
		}

		private void optFleet_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (MotorVehicle.Statics.RegistrationType == "CORR")
			{
				if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetOld") == false && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetNew") == false)
				{
					cmbFleet.Text = "No";
					return;
				}
			}
			if (MotorVehicle.Statics.blnFleetRunOnce && MotorVehicle.Statics.blnChangeToFleet)
			{
				cmbFleet.Text = "Fleet";
				return;
			}
			else if (MotorVehicle.Statics.blnChangeToFleet)
			{
				MotorVehicle.Statics.blnFleetRunOnce = true;
			}
			switch (Index)
			{
				case 0:
					{
						txtFleetName.Enabled = true;
						txtFleetNumber.Enabled = true;
						lblFleetName.Enabled = true;
						lblFleetNumber.Enabled = true;
						if (MotorVehicle.Statics.RegistrationType != "GVW")
						{
							if (!FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetOld")) && !FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("FleetNew")))
							{
								MotorVehicle.Statics.NewFleet = true;
							}
						}
						lblFleetNumber.Text = "Enter Fleet Number  (Enter 'A' to Add/Search)";
						lblFleetName.Text = "Fleet Name";
						break;
					}
				case 1:
					{
						txtFleetName.Enabled = false;
						txtFleetNumber.Enabled = false;
						lblFleetName.Enabled = false;
						lblFleetNumber.Enabled = false;
						lblFleetNumber.Text = "Enter Fleet Number  (Enter 'A' to Add/Search)";
						lblFleetName.Text = "Fleet Name";
						MotorVehicle.Statics.NewFleet = false;
						break;
					}
				case 2:
					{
						txtFleetName.Enabled = true;
						txtFleetNumber.Enabled = true;
						lblFleetName.Enabled = true;
						lblFleetNumber.Enabled = true;
						lblFleetNumber.Text = "Enter Group Number  (Enter 'A' to Add/Search)";
						lblFleetName.Text = "Group Name";
						MotorVehicle.Statics.NewFleet = false;
						break;
					}
			}
			//end switch
		}

		private void optFleet_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbFleet.SelectedIndex;
			optFleet_CheckedChanged(index, sender, e);
		}

		private void optNoMaineReReg_CheckedChanged(object sender, System.EventArgs e)
		{
			cboLength_Click();
		}

		private void optYesMaineReRegCurrentYear_CheckedChanged(object sender, System.EventArgs e)
		{
			cboLength_Click();
		}

		private void optYesMaineReRegNextYear_CheckedChanged(object sender, System.EventArgs e)
		{
			if (DateTime.Today.Month < 10)
			{
				MessageBox.Show("You must wait until October 1st to re register any long term trailer expiring next year.", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Information);
				cmbNoMaineReReg.Text = "NO";
			}
			cboLength_Click();
		}

		private void txtAddress1_Enter(object sender, System.EventArgs e)
		{
			txtAddress1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtAddress1_Leave(object sender, System.EventArgs e)
		{
			txtAddress1.BackColor = Color.White;
		}

		private void txtCity_Enter(object sender, System.EventArgs e)
		{
			txtCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCity_Leave(object sender, System.EventArgs e)
		{
			txtCity.BackColor = Color.White;
		}

		private void txtColor1_Enter(object sender, System.EventArgs e)
		{
			txtColor1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtColor1_Leave(object sender, System.EventArgs e)
		{
			txtColor1.BackColor = Color.White;
		}

		private void txtColor2_Enter(object sender, System.EventArgs e)
		{
			txtColor2.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtColor2_Leave(object sender, System.EventArgs e)
		{
			txtColor2.BackColor = Color.White;
		}

		private void txtCredit_Enter(object sender, System.EventArgs e)
		{
			txtCredit.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtCredit_Leave(object sender, System.EventArgs e)
		{
			txtCredit.BackColor = Color.White;
		}

		private void txtEMail_Enter(object sender, System.EventArgs e)
		{
			txtEMail.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtEMail_Leave(object sender, System.EventArgs e)
		{
			txtEMail.BackColor = Color.White;
		}

		private void txtExpires_Enter(object sender, System.EventArgs e)
		{
			txtExpires.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtExpires_Leave(object sender, System.EventArgs e)
		{
			txtExpires.BackColor = SystemColors.Control;
		}

		private void txtFEEAgentFeeReg_Enter(object sender, System.EventArgs e)
		{
			txtFEEAgentFeeReg.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEEAgentFeeReg_Leave(object sender, System.EventArgs e)
		{
			txtFEEAgentFeeReg.BackColor = Color.White;
		}

		private void txtFEEAgentFeeReg_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEEAgentFeeReg.Text))
			{
				txtFEEAgentFeeReg.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEEAgentFeeReg.Text) == "")
			{
				txtFEEAgentFeeReg.Text = "0.00";
			}
		}

		private void txtFeeAgentFeeCTA_Enter(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeCTA.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFeeAgentFeeCTA_Leave(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeCTA.BackColor = Color.White;
		}

		private void txtFeeAgentFeeCTA_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFeeAgentFeeCTA.Text))
			{
				txtFeeAgentFeeCTA.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFeeAgentFeeCTA.Text) == "")
			{
				txtFeeAgentFeeCTA.Text = "0.00";
			}
		}

		private void txtFeeAgentFeeTransfer_Enter(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeTransfer.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFeeAgentFeeTransfer_Leave(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeTransfer.BackColor = Color.White;
		}

		private void txtFeeAgentFeeTransfer_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFeeAgentFeeTransfer.Text))
			{
				txtFeeAgentFeeTransfer.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFeeAgentFeeTransfer.Text) == "")
			{
				txtFeeAgentFeeTransfer.Text = "0.00";
			}
		}

		private void txtFeeAgentFeeCorrection_Enter(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeCorrection.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFeeAgentFeeCorrection_Leave(object sender, System.EventArgs e)
		{
			txtFeeAgentFeeCorrection.BackColor = Color.White;
		}

		private void txtFeeAgentFeeCorrection_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFeeAgentFeeCorrection.Text))
			{
				txtFeeAgentFeeCorrection.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFeeAgentFeeCorrection.Text) == "")
			{
				txtFeeAgentFeeCorrection.Text = "0.00";
			}
		}

		private void txtFEECreditTransfer_Enter(object sender, System.EventArgs e)
		{
			txtFEECreditTransfer.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEECreditTransfer_Leave(object sender, System.EventArgs e)
		{
			txtFEECreditTransfer.BackColor = Color.White;
		}

		private void txtFEECreditTransfer_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEECreditTransfer.Text))
			{
				txtFEECreditTransfer.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEECreditTransfer.Text) == "")
			{
				txtFEECreditTransfer.Text = "0.00";
			}
		}

		private void txtFEERegFee_Enter(object sender, System.EventArgs e)
		{
			txtFEERegFee.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEERegFee_Leave(object sender, System.EventArgs e)
		{
			txtFEERegFee.BackColor = Color.White;
		}

		private void txtFEERegFee_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEERegFee.Text))
			{
				txtFEERegFee.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEERegFee.Text) == "")
			{
				txtFEERegFee.Text = "0.00";
			}
		}

		private void txtFees_Enter(object sender, System.EventArgs e)
		{
			txtFees.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFees_Leave(object sender, System.EventArgs e)
		{
			txtFees.BackColor = Color.White;
		}

		private void txtFEESalesTax_Enter(object sender, System.EventArgs e)
		{
			txtFEESalesTax.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEESalesTax_Leave(object sender, System.EventArgs e)
		{
			txtFEESalesTax.BackColor = Color.White;
		}

		private void txtFEESalesTax_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEESalesTax.Text))
			{
				txtFEESalesTax.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEESalesTax.Text) == "")
			{
				txtFEESalesTax.Text = "0.00";
			}
		}

		private void txtFEETitleFee_Enter(object sender, System.EventArgs e)
		{
			txtFEETitleFee.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEETitleFee_Leave(object sender, System.EventArgs e)
		{
			txtFEETitleFee.BackColor = Color.White;
		}

		private void txtFEETitleFee_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEETitleFee.Text))
			{
				txtFEETitleFee.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEETitleFee.Text) == "")
			{
				txtFEETitleFee.Text = "0.00";
			}
		}

		private void txtFEERushTitleFee_Enter(object sender, System.EventArgs e)
		{
			txtFEERushTitleFee.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEERushTitleFee_Leave(object sender, System.EventArgs e)
		{
			txtFEERushTitleFee.BackColor = Color.White;
		}

		private void txtFEERushTitleFee_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEERushTitleFee.Text))
			{
				txtFEERushTitleFee.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEERushTitleFee.Text) == "")
			{
				txtFEERushTitleFee.Text = "0.00";
			}
		}

		private void txtFEETransferFee_Enter(object sender, System.EventArgs e)
		{
			txtFEETransferFee.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtFEETransferFee_Leave(object sender, System.EventArgs e)
		{
			txtFEETransferFee.BackColor = Color.White;
		}

		private void txtFEETransferFee_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtFEETransferFee.Text))
			{
				txtFEETransferFee.Text = "0.00";
			}
			else if (fecherFoundation.Strings.Trim(txtFEETransferFee.Text) == "")
			{
				txtFEETransferFee.Text = "0.00";
			}
		}

		private void txtFleetNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			clsDRWrapper rsFT = new clsDRWrapper();
			DateTime xdate;
			string tempdate = "";
			clsDRWrapper rsClassInfo = new clsDRWrapper();
			clsDRWrapper rsOverride = new clsDRWrapper();
			int intHolder1;
			int intHolder2;
			string strMI = "";
			if (txtFleetNumber.Text == "A")
			{
				MotorVehicle.Statics.bolFromLongTermDataInput = true;
				frmFleetMaster.InstancePtr.Unload();
				frmFleetMaster.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				MotorVehicle.Statics.bolFromLongTermDataInput = false;
			}
			if (Conversion.Val(txtFleetNumber.Text) != 0)
			{
				if (cmbFleet.Text == "Group")
				{
					rsFT.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsFT.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsFT.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE Deleted <> 1 AND Type = 'L' AND ExpiryMonth = 99 ORDER BY p.FullName");
				}
				else
				{
					rsFT.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsFT.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsFT.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE Deleted <> 1 AND Type = 'L' AND FleetNumber = " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + " AND ExpiryMonth <> 99");
					rsClassInfo.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + "' AND SystemCode = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass") + "'");
					if (rsClassInfo.EndOfFile() != true && rsClassInfo.BeginningOfFile() != true)
					{
						if (FCConvert.ToString(rsClassInfo.Get_Fields_String("RenewalDate")) == "F")
						{
							if (rsFT.EndOfFile() != true && rsFT.BeginningOfFile() != true)
							{
								if (Conversion.Val(rsFT.Get_Fields_Int32("ExpiryMonth")) != MotorVehicle.Statics.gintFixedMonthMonth)
								{
									MessageBox.Show("You may not add a vehicle with a fixed renewal date to a fleet that has an expiration month other than the required expiration month", "Invalid Expiration Month", MessageBoxButtons.OK, MessageBoxIcon.Hand);
									txtFleetNumber.Text = "";
									cmbFleet.Text = "No";
									e.Cancel = true;
									return;
								}
							}
						}
					}
				}
				if (rsFT.EndOfFile() != true && rsFT.BeginningOfFile() != true)
				{
					rsFT.MoveLast();
					rsFT.MoveFirst();
					if (!rsFT.FindFirstRecord("FleetNumber", Conversion.Val(txtFleetNumber.Text)))
					{
						if (cmbFleet.Text == "Group")
						{
							MessageBox.Show("There was no match found for Group Master Number " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + ".", "Invalid Group", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							MessageBox.Show("There was no match found for Fleet Master Number " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + ".", "Invalid Fleet", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						txtFleetNumber.Text = "";
						cmbFleet.Text = "No";
						e.Cancel = true;
						return;
					}
					else
					{
						txtFleetName.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(rsFT.Get_Fields("Party1FullName")));
						// If UCase(MuniName) = "MAINE MOTOR TRANSPORT" And Trim(.Fields("State")) <> "ME" Then
						// rsOverride.OpenRecordset "SELECT * FROM GroupAddressOverride"
						// If rsOverride.EndOfFile <> True And rsOverride.BeginningOfFile <> True Then
						// If Not blnSuspended Then
						// If Not IsNull(rsOverride.Fields("Address")) Then txtAddress1.Text = Trim(rsOverride.Fields("Address"))
						// Else
						// txtAddress1.Text = "REGISTRATION SUSPENDED"
						// End If
						// If Not IsNull(rsOverride.Fields("City")) Then txtCity.Text = Trim(rsOverride.Fields("City"))
						// If Not IsNull(rsOverride.Fields("State")) Then txtState.Text = Trim(rsOverride.Fields("State"))
						// If Not IsNull(rsOverride.Fields("Zip")) Then
						// If Len(rsOverride.Fields("Zip")) < 5 Then
						// txtZip.Text = "0" & rsOverride.Fields("Zip")
						// Else
						// txtZip.Text = rsOverride.Fields("Zip")
						// End If
						// End If
						// End If
						// Else
						if (!MotorVehicle.Statics.blnSuspended)
						{
							if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Address")))
								txtAddress1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("Address")));
						}
						else
						{
							txtAddress1.Text = "REGISTRATION SUSPENDED";
						}
						if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("City")))
							txtCity.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("City")));
						if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields("State")))
							txtState.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields("State")));
						if (!fecherFoundation.FCUtils.IsNull(rsFT.Get_Fields_String("Zip")))
						{
							if (FCConvert.ToString(rsFT.Get_Fields_String("Zip")).Length < 5)
							{
								txtZip.Text = "0" + rsFT.Get_Fields_String("Zip");
							}
							else
							{
								txtZip.Text = FCConvert.ToString(rsFT.Get_Fields_String("Zip"));
							}
						}
						// End If
						txtReg1PartyID.Text = FCConvert.ToString(rsFT.Get_Fields_Int32("PartyID1"));
						lblRegistrant1.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(rsFT.Get_Fields("Party1FullName")));
						if (Conversion.Val(rsFT.Get_Fields_Int32("PartyID2")) != 0)
						{
							txtReg2PartyID.Text = FCConvert.ToString(rsFT.Get_Fields_Int32("PartyID2"));
							lblRegistrant2.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(rsFT.Get_Fields("Party2FullName")));
						}
						else
						{
							ClearPartyInfo(2);
						}
						txtEMail.Text = FCConvert.ToString(rsFT.Get_Fields_String("EmailAddress"));
						txtEMail.Enabled = false;
						chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
						chkOverrideFleetEmail.Visible = true;
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsFT.Get_Fields_String("Comment"))) != "")
						{
							imgFleetComment.Visible = true;
						}
						else
						{
							imgFleetComment.Visible = false;
						}
						cmdGroupComment.Enabled = true;
						if ((MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("FleetNew") == true || MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("FleetOld") == true) || (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("FleetNumber")) == txtFleetNumber.Text))
						{
							// do nothing
						}
						else
						{
							MotorVehicle.Statics.NewFleet = true;
						}
						frmDataInputLongTermTrailers.InstancePtr.Refresh();
					}
				}
				else
				{
					txtFleetNumber.Text = "";
					txtFleetName.Text = "";
					chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
					chkOverrideFleetEmail.Visible = false;
					txtEMail.Text = "";
					txtEMail.Enabled = true;
					imgFleetComment.Visible = false;
					cmdGroupComment.Enabled = false;
					if (cmbFleet.Text == "Group")
					{
						MessageBox.Show("There are no Groups.", "No Groups", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						MessageBox.Show("There are no Fleets.", "No Fleets", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
			else
			{
				chkOverrideFleetEmail.CheckState = Wisej.Web.CheckState.Unchecked;
				chkOverrideFleetEmail.Visible = false;
				txtEMail.Text = "";
				txtEMail.Enabled = true;
				imgFleetComment.Visible = false;
				cmdGroupComment.Enabled = false;
			}
		}

		public void txtFleetNumber_Validate(bool Cancel)
		{
			txtFleetNumber_Validating(txtFleetNumber, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtLegalResCity_Enter(object sender, System.EventArgs e)
		{
			txtLegalResCity.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtLegalResCity_Leave(object sender, System.EventArgs e)
		{
			txtLegalResCity.BackColor = Color.White;
		}

		private void txtLegalResState_Enter(object sender, System.EventArgs e)
		{
			txtLegalResState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtLegalResState_Leave(object sender, System.EventArgs e)
		{
			txtLegalResState.BackColor = Color.White;
		}

		private void txtMake_Enter(object sender, System.EventArgs e)
		{
			txtMake.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtMake_Leave(object sender, System.EventArgs e)
		{
			txtMake.BackColor = Color.White;
		}

		private void txtMessage_Enter(object sender, System.EventArgs e)
		{
			txtMessage.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtMessage_Leave(object sender, System.EventArgs e)
		{
			txtMessage.BackColor = Color.White;
		}

		private void txtModel_Enter(object sender, System.EventArgs e)
		{
			txtModel.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtModel_Leave(object sender, System.EventArgs e)
		{
			if (txtModel.Text != "")
			{
				txtModel.TabStop = true;
			}
			else
			{
				txtModel.TabStop = false;
			}
			txtModel.BackColor = Color.White;
		}

		private void txtReg1PartyID_Enter(object sender, System.EventArgs e)
		{
			txtReg1PartyID.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtReg1PartyID_Leave(object sender, System.EventArgs e)
		{
			txtReg1PartyID.BackColor = Color.White;
		}

		private void txtReg2PartyID_Enter(object sender, System.EventArgs e)
		{
			txtReg2PartyID.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtReg2PartyID_Leave(object sender, System.EventArgs e)
		{
			txtReg2PartyID.BackColor = Color.White;
		}

		private void txtNetWeight_Enter(object sender, System.EventArgs e)
		{
			txtNetWeight.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtNetWeight_Leave(object sender, System.EventArgs e)
		{
			txtNetWeight.BackColor = Color.White;
			Recalculate();
		}

		private void txtPlate_Enter(object sender, System.EventArgs e)
		{
			txtPlate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtPlate_Leave(object sender, System.EventArgs e)
		{
			txtPlate.BackColor = Color.White;
		}

		private void txtRate_Enter(object sender, System.EventArgs e)
		{
			txtRate.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtRate_Leave(object sender, System.EventArgs e)
		{
			txtRate.BackColor = Color.White;
		}

		private void txtState_Enter(object sender, System.EventArgs e)
		{
			txtState.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtState_Leave(object sender, System.EventArgs e)
		{
			txtState.BackColor = Color.White;
		}

		private void txtStyle_Enter(object sender, System.EventArgs e)
		{
			txtStyle.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtStyle_Leave(object sender, System.EventArgs e)
		{
			txtStyle.BackColor = Color.White;
		}

		private void txtTitle_Enter(object sender, System.EventArgs e)
		{
			txtTitle.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtTitle_Leave(object sender, System.EventArgs e)
		{
			txtTitle.BackColor = Color.White;
		}

		private void txtUnit_Enter(object sender, System.EventArgs e)
		{
			txtUnit.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUnit_Leave(object sender, System.EventArgs e)
		{
			txtUnit.BackColor = Color.White;
		}

		private void txtUserField1_Enter(object sender, System.EventArgs e)
		{
			txtUserField1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserField1_Leave(object sender, System.EventArgs e)
		{
			txtUserField1.BackColor = Color.White;
		}

		private void txtUserField2_Enter(object sender, System.EventArgs e)
		{
			txtUserField2.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserField2_Leave(object sender, System.EventArgs e)
		{
			txtUserField2.BackColor = Color.White;
		}

		private void txtUserField3_Enter(object sender, System.EventArgs e)
		{
			txtUserField3.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserField3_Leave(object sender, System.EventArgs e)
		{
			txtUserField3.BackColor = Color.White;
		}

		private void txtUserReason1_Enter(object sender, System.EventArgs e)
		{
			txtUserReason1.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserReason1_Leave(object sender, System.EventArgs e)
		{
			txtUserReason1.BackColor = Color.White;
		}

		private void txtUserReason2_Enter(object sender, System.EventArgs e)
		{
			txtUserReason2.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserReason2_Leave(object sender, System.EventArgs e)
		{
			txtUserReason2.BackColor = Color.White;
		}

		private void txtUserReason3_Enter(object sender, System.EventArgs e)
		{
			txtUserReason3.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtUserReason3_Leave(object sender, System.EventArgs e)
		{
			txtUserReason3.BackColor = Color.White;
		}

		private void txtVIN_Enter(object sender, System.EventArgs e)
		{
			txtVIN.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtVIN_Leave(object sender, System.EventArgs e)
		{
			txtVIN.BackColor = Color.White;
		}

		private void txtVIN_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtVIN.Text != "")
			{
				VerifyVIN();
			}
		}

		private void txtYear_Enter(object sender, System.EventArgs e)
		{
			txtYear.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtYear_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtYear_Leave(object sender, System.EventArgs e)
		{
			txtYear.BackColor = Color.White;
		}

		private void txtZip_Enter(object sender, System.EventArgs e)
		{
			txtZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtZip_Leave(object sender, System.EventArgs e)
		{
			txtZip.BackColor = Color.White;
		}

		private void txtLegalZip_Enter(object sender, System.EventArgs e)
		{
			txtLegalZip.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtLegalZip_Leave(object sender, System.EventArgs e)
		{
			txtLegalZip.BackColor = Color.White;
		}

		private void txtLegalResAddress_Enter(object sender, System.EventArgs e)
		{
			txtLegalResAddress.BackColor = ColorTranslator.FromOle(NewBackColor);
		}

		private void txtLegalResAddress_Leave(object sender, System.EventArgs e)
		{
			txtLegalResAddress.BackColor = Color.White;
		}

		private void CalculateTotal()
		{
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(Decimal, int)
			Decimal curTotal;
			if (fecherFoundation.Strings.Trim(txtFEECreditTransfer.Text) == "")
			{
				txtFEECreditTransfer.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtFEEAgentFeeReg.Text) == "")
			{
				txtFEEAgentFeeReg.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtFEESalesTax.Text) == "")
			{
				txtFEESalesTax.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtFEETitleFee.Text) == "")
			{
				txtFEETitleFee.Text = "0.00";
			}
			if (fecherFoundation.Strings.Trim(txtFEERushTitleFee.Text) == "")
			{
				txtFEERushTitleFee.Text = "0.00";
			}
			curTotal = FCConvert.ToDecimal(txtFEERegFee.Text) - FCConvert.ToDecimal(txtFEECreditTransfer.Text);
			if (curTotal < 0)
				curTotal = 0;
			curTotal += FCConvert.ToDecimal(txtFEEAgentFeeReg.Text);
			curTotal += FCConvert.ToDecimal(txtFeeAgentFeeCTA.Text);
			curTotal += FCConvert.ToDecimal(txtFeeAgentFeeTransfer.Text);
			curTotal += FCConvert.ToDecimal(txtFeeAgentFeeCorrection.Text);
			curTotal += FCConvert.ToDecimal(txtFEETransferFee.Text);
			curTotal += FCConvert.ToDecimal(txtFEESalesTax.Text);
			curTotal += FCConvert.ToDecimal(txtFEETitleFee.Text);
			curTotal += FCConvert.ToDecimal(txtFEERushTitleFee.Text);
			txtFEETotal.Text = Strings.Format(curTotal, "#,##0.00");
		}

		private void CalculateLongTermTrailerECorrectFee()
		{
			double totalcredit;
			double ExciseCredit;
			double ExciseCharge;
			double OldCredit;
			double NewCredit;
			FillRSFinal();
			totalcredit = 0;
			MotorVehicle.Statics.ECTotal = FCConvert.ToDecimal((MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee") - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TitleFee"))));
			MotorVehicle.Statics.ECTotal += MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax") - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("SalesTax"));
			if (MotorVehicle.Statics.PlateType == 2)
			{
				MotorVehicle.Statics.ECTotal += MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge");
				totalcredit -= Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge"));
			}
			else
			{
				MotorVehicle.Statics.ECTotal += MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge") - MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge");
				totalcredit += Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("TransferCreditUsed")) - Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed"));
			}
			if ((totalcredit * -1) > Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")))
			{
				MotorVehicle.Statics.ECTotal -= MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge");
			}
			else
			{
				MotorVehicle.Statics.ECTotal -= FCConvert.ToDecimal(totalcredit * -1);
			}
			MotorVehicle.Statics.ECTotal += (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferFee") - MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("TransferFee"));
			MotorVehicle.Statics.ECTotal += (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFee") - MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("AgentFee"));
			MotorVehicle.Statics.ECTotal += MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCTA") - MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("AgentFeeCTA");
			MotorVehicle.Statics.ECTotal += (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeTransfer") - MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("AgentFeeTransfer"));
			if (MotorVehicle.Statics.ECTotal < 0)
				MotorVehicle.Statics.ECTotal = 0;
			MotorVehicle.Statics.ECTotal += MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCorrection");
			frmDataInputLongTermTrailers.InstancePtr.lblTotalDue.Text = "Total Amount Due:  " + Strings.Format(MotorVehicle.Statics.ECTotal, "#,##0.00");
		}

		private void Recalculate()
		{
			clsDRWrapper rsDefaults = new clsDRWrapper();
			if (MotorVehicle.Statics.RegistrationType != "CORR")
			{
				rsDefaults.OpenRecordset("SELECT * FROM DefaultInfo");
				if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
				{
					txtFEEAgentFeeReg.Text = Strings.Format(FCConvert.ToString(Conversion.Val(rsDefaults.Get_Fields_Decimal("AgentFeeLongTermTrailer"))), "#,##0.00");
				}
				if (chkTimePayment.CheckState != Wisej.Web.CheckState.Checked)
				{
					if (chk25YearPlate.CheckState == Wisej.Web.CheckState.Checked)
					{
						txtFEERegFee.Text = Strings.Format(80, "#,##0.00");
					}
					else
					{
						if (Conversion.Val(txtNetWeight.Text) != 2000)
						{
							txtFEERegFee.Text = Strings.Format(12 * (cboLength.ItemData(cboLength.ListIndex)), "#,##0.00");
						}
						else
						{
							txtFEERegFee.Text = Strings.Format(5 * (cboLength.ItemData(cboLength.ListIndex)), "#,##0.00");
						}
					}
				}
				else
				{
					if (cboLength.ItemData(cboLength.ListIndex) > 12)
					{
						if (Conversion.Val(txtNetWeight.Text) != 2000)
						{
							txtFEERegFee.Text = Strings.Format(12 * 3, "#,##0.00");
						}
						else
						{
							txtFEERegFee.Text = Strings.Format(5 * 3, "#,##0.00");
						}
					}
					else
					{
						if (Conversion.Val(txtNetWeight.Text) != 2000)
						{
							txtFEERegFee.Text = Strings.Format(12 * 4, "#,##0.00");
						}
						else
						{
							txtFEERegFee.Text = Strings.Format(5 * 4, "#,##0.00");
						}
					}
				}
				if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
				{
					if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed") != 0)
					{
						txtFEECreditTransfer.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed"))), "#,##0.00");
					}
					else
					{
						txtFEECreditTransfer.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge"))), "#,##0.00");
					}
					if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
					{
						txtFeeAgentFeeTransfer.Text = Strings.Format(FCConvert.ToString(Conversion.Val(rsDefaults.Get_Fields_Decimal("AgentFeeLongTermTrailerTransfer"))), "#,##0.00");
					}
					else
					{
						txtFeeAgentFeeTransfer.Text = "0.00";
					}
					if (chk25YearPlate.CheckState == Wisej.Web.CheckState.Checked)
					{
						txtFEETransferFee.Text = "20.00";
					}
					else
					{
						if (Conversion.Val(txtNetWeight.Text) == 2000)
						{
							txtFEETransferFee.Text = "5.00";
						}
						else
						{
							txtFEETransferFee.Text = "8.00";
						}
					}
				}
				else
				{
					txtFEECreditTransfer.Text = "0.00";
					txtFEETransferFee.Text = "0.00";
					txtFeeAgentFeeTransfer.Text = "0.00";
				}
				if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone") == true)
				{
					if (chk25YearPlate.CheckState == Wisej.Web.CheckState.Checked)
					{
						txtFEETitleFee.Text = Strings.Format(18, "#,##0.00");
					}
					else
					{
						txtFEETitleFee.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"))), "#,##0.00");
					}
					txtFEERushTitleFee.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("RushTitleFee"))), "#,##0.00");
					if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
					{
						txtFeeAgentFeeCTA.Text = Strings.Format(FCConvert.ToString(Conversion.Val(rsDefaults.Get_Fields_Decimal("AgentFeeLongTermTrailerCTA"))), "#,##0.00");
					}
				}
				else
				{
					txtFEETitleFee.Text = "0.00";
					txtFEERushTitleFee.Text = "0.00";
					txtFeeAgentFeeCTA.Text = "0.00";
				}
				if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone") == true)
				{
					txtFEESalesTax.Text = Strings.Format(FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"))), "#,##0.00");
				}
				else
				{
					txtFEESalesTax.Text = "0.00";
				}
				CalculateTotal();
				txtRate.Text = Strings.Format(txtFEERegFee.Text, "#,##0.00");
				txtCredit.Text = Strings.Format(txtFEECreditTransfer.Text, "#,##0.00");
				txtFees.Text = Strings.Format(FCConvert.ToDecimal(txtFEETotal.Text) - FCConvert.ToDecimal(txtFEEAgentFeeReg.Text) - FCConvert.ToDecimal(txtFeeAgentFeeCTA.Text) - FCConvert.ToDecimal(txtFeeAgentFeeCorrection.Text) - FCConvert.ToDecimal(txtFEETitleFee.Text) - FCConvert.ToDecimal(txtFEERushTitleFee.Text) - FCConvert.ToDecimal(txtFEESalesTax.Text), "#,##0.00");
				lblTotalDue.Text = "Total Amount Due   " + Strings.Format(txtFEETotal.Text, "Currency");
			}
			else
			{
				if (!blnLoadingForm)
				{
					if (chkTimePayment.CheckState != Wisej.Web.CheckState.Checked)
					{
						if (chk25YearPlate.CheckState == Wisej.Web.CheckState.Checked)
						{
							txtFEERegFee.Text = Strings.Format(80, "#,##0.00");
						}
						else
						{
							if (Conversion.Val(txtNetWeight.Text) != 2000)
							{
								txtFEERegFee.Text = Strings.Format(12 * (cboLength.ItemData(cboLength.ListIndex)), "#,##0.00");
							}
							else
							{
								txtFEERegFee.Text = Strings.Format(5 * (cboLength.ItemData(cboLength.ListIndex)), "#,##0.00");
							}
						}
					}
					else
					{
						if (cboLength.ItemData(cboLength.ListIndex) > 12)
						{
							if (Conversion.Val(txtNetWeight.Text) != 2000)
							{
								txtFEERegFee.Text = Strings.Format(12 * 3, "#,##0.00");
							}
							else
							{
								txtFEERegFee.Text = Strings.Format(5 * 3, "#,##0.00");
							}
						}
						else
						{
							if (Conversion.Val(txtNetWeight.Text) != 2000)
							{
								txtFEERegFee.Text = Strings.Format(12 * 4, "#,##0.00");
							}
							else
							{
								txtFEERegFee.Text = Strings.Format(5 * 4, "#,##0.00");
							}
						}
					}
					CalculateTotal();
					txtRate.Text = Strings.Format(txtFEERegFee.Text, "#,##0.00");
					txtCredit.Text = Strings.Format(txtFEECreditTransfer.Text, "#,##0.00");
					txtFees.Text = Strings.Format(FCConvert.ToDecimal(txtFEETotal.Text) - FCConvert.ToDecimal(txtFEEAgentFeeReg.Text) - FCConvert.ToDecimal(txtFeeAgentFeeCTA.Text) - FCConvert.ToDecimal(txtFeeAgentFeeCorrection.Text) - FCConvert.ToDecimal(txtFEETitleFee.Text) - FCConvert.ToDecimal(txtFEERushTitleFee.Text) - FCConvert.ToDecimal(txtFEESalesTax.Text), "#,##0.00");
					CalculateLongTermTrailerECorrectFee();
				}
			}
		}

		private void SetCustomFormColors()
		{
			Label5.BackColor = Color.White;
			Label6.BackColor = Color.White;
			//Label7.BackColor = Color.White;
			Label8.BackColor = Color.White;
			Label9.BackColor = Color.White;
			Label10.BackColor = Color.White;
			Label11.BackColor = Color.White;
			Label12.BackColor = Color.White;
			Label13.BackColor = Color.White;
			Label14.BackColor = Color.White;
			//Label15.BackColor = Color.White;
			Label16.BackColor = Color.White;
			//Label17.BackColor = Color.White;
			//Label18.BackColor = Color.White;
			Label19.BackColor = Color.White;
			//Label20.BackColor = Color.White;
			//Label21.BackColor = Color.White;
			//Label22.BackColor = Color.White;
			Label23.BackColor = Color.White;
			//Label24.BackColor = Color.White;
			//Label25.BackColor = Color.White;
			//Label26.BackColor = Color.White;
			Label27.BackColor = Color.White;
			//Label28.BackColor = Color.White;
			Label29.BackColor = Color.White;
			//Label30.BackColor = Color.White;
			Label31.BackColor = Color.White;
			Label36.BackColor = Color.White;
			lblRegistrant1.BackColor = Color.White;
			lblRegistrant2.BackColor = Color.White;
			Label27.ForeColor = Color.Blue;
			Label29.ForeColor = Color.Blue;
			Label31.ForeColor = Color.Blue;
			lblTotalDue.BackColor = ColorTranslator.FromOle(0x800000);
			lblTotalDue.ForeColor = Color.White;
			lblTotalDue.Font = FCUtils.FontChangeSize(lblTotalDue.Font, 12);
		}

		private void FillExtraInfo()
		{
			int counter;
			if (FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Mail")))
			{
				cmbYes.Text = "Yes";
			}
			else
			{
				cmbYes.Text = "Yes";
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("EMailAddress")))
			{
				txtEMail.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("EMailAddress"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage")) && !fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag")))
			{
				if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag")) > 0)
				{
					cboMessageCodes.SelectedIndex = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag")) - 1);
					txtMessage.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage"));
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField1")))
			{
				txtUserField1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField1"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason1")))
			{
				txtUserReason1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason1"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField2")))
			{
				txtUserField2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField2"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason2")))
			{
				txtUserReason2.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason2"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField3")))
			{
				txtUserField3.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField3"));
			}
			if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason3")))
			{
				txtUserReason3.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason3"));
			}
		}

		public void FillOwnerInfo(ref clsDRWrapper plate)
		{
			txtReg1PartyID.Text = FCConvert.ToString(plate.Get_Fields_Int32("PartyID1"));
			GetPartyInfo(plate.Get_Fields_Int32("PartyID1"), 1);
			if (Conversion.Val(plate.Get_Fields_Int32("PartyID2")) != 0)
			{
				txtReg2PartyID.Text = FCConvert.ToString(plate.Get_Fields_Int32("PartyID2"));
				GetPartyInfo(plate.Get_Fields_Int32("PartyID2"), 2);
			}
			if (fecherFoundation.Strings.Trim(plate.Get_Fields_String("City")) != "")
			{
				txtCity.Text = fecherFoundation.Strings.Trim(plate.Get_Fields_String("City"));
			}
			if (plate.Get_Fields("State") != "")
			{
				txtState.Text = plate.Get_Fields("State");
			}
			if (plate.Get_Fields_String("Zip") != "")
			{
				txtZip.Text = plate.Get_Fields_String("Zip");
			}
			if (fecherFoundation.Strings.Trim(plate.Get_Fields_String("Residence")) != "")
			{
				txtLegalResCity.Text = fecherFoundation.Strings.Trim(plate.Get_Fields_String("Residence"));
			}
			txtLegalResState.Text = fecherFoundation.Strings.Trim(plate.Get_Fields_String("ResidenceState"));
			txtLegalResAddress.Text = fecherFoundation.Strings.Trim(plate.Get_Fields_String("LegalResAddress"));
			txtLegalZip.Text = fecherFoundation.Strings.Trim(plate.Get_Fields_String("LegalResZip"));
		}

		private void FillRSFinal()
		{
			if (MotorVehicle.Statics.RegistrationType != "CORR")
			{
				if (MotorVehicle.Statics.NewFleet)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("FleetNew", true);
				}
				else
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("FleetNew", false);
				}
			}
			MotorVehicle.Statics.rsFinal.Set_Fields("FleetNumber", FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)));
			if (cmbYes.Text == "Yes")
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("Mail", true);
			}
			else
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("Mail", false);
			}
			MotorVehicle.Statics.rsFinal.Set_Fields("EMailAddress", fecherFoundation.Strings.Trim(txtEMail.Text));
			if ((cboMessageCodes.SelectedIndex != -1 && fecherFoundation.Strings.Trim(txtMessage.Text) != "") || (cboMessageCodes.SelectedIndex == -1 && fecherFoundation.Strings.Trim(txtMessage.Text) == ""))
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("MessageFlag", cboMessageCodes.SelectedIndex + 1);
				MotorVehicle.Statics.rsFinal.Set_Fields("UserMessage", fecherFoundation.Strings.Trim(txtMessage.Text));
			}
			MotorVehicle.Statics.rsFinal.Set_Fields("UserField1", fecherFoundation.Strings.Trim(txtUserField1.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserReason1", fecherFoundation.Strings.Trim(txtUserReason1.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserField2", fecherFoundation.Strings.Trim(txtUserField2.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserReason2", fecherFoundation.Strings.Trim(txtUserReason2.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserField3", fecherFoundation.Strings.Trim(txtUserField3.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserReason3", fecherFoundation.Strings.Trim(txtUserReason3.Text));
			if (MotorVehicle.Statics.rsDoubleTitle.IsntAnything())
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("DoubleCTANumber", "");
			}
			else
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("DoubleCTANumber", MotorVehicle.Statics.rsDoubleTitle.Get_Fields_String("CTANumber").ToUpper());
			}
			MotorVehicle.Statics.rsFinal.Set_Fields("Status", "A");
			if (!MotorVehicle.Statics.blnSuspended)
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("Address", txtAddress1.Text);
			}
			else if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address")))
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("Address", "");
			}
			MotorVehicle.Statics.rsFinal.Set_Fields("City", txtCity.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("State", txtState.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("Zip", txtZip.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode1", "C");
			MotorVehicle.Statics.rsFinal.Set_Fields("PartyID1", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtReg1PartyID.Text))));
			if (Conversion.Val(fecherFoundation.Strings.Trim(txtReg2PartyID.Text)) != 0)
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode2", "C");
				MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtReg2PartyID.Text))));
			}
			else
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode2", "N");
				MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", 0);
			}
			MotorVehicle.Statics.rsFinal.Set_Fields("OpID", MotorVehicle.Statics.OpID);
			// 
			MotorVehicle.Statics.rsFinal.Set_Fields("Residence", txtLegalResCity.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", txtLegalResState.Text);
			// rsFinal.Fields("LegalResAddress") = txtLegalResAddress.Text
			MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", txtLegalZip.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber", txtUnit.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("Style", txtStyle.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("Model", txtModel.Text);
			// kk01252016 tromv-1080
			MotorVehicle.Statics.rsFinal.Set_Fields("color1", txtColor1.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("color2", txtColor2.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("make", txtMake.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("Year", FCConvert.ToString(Conversion.Val(txtYear.Text)));
			MotorVehicle.Statics.rsFinal.Set_Fields("Vin", txtVIN.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("NetWeight", FCConvert.ToString(Conversion.Val(txtNetWeight.Text)));
			MotorVehicle.Statics.rsFinal.Set_Fields("AgentFee", FCConvert.ToString(Conversion.Val(txtFEEAgentFeeReg.Text)));
			MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeCTA", FCConvert.ToString(Conversion.Val(txtFeeAgentFeeCTA.Text)));
			MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeTransfer", FCConvert.ToString(Conversion.Val(txtFeeAgentFeeTransfer.Text)));
			MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeCorrection", FCConvert.ToString(Conversion.Val(txtFeeAgentFeeCorrection.Text)));
			MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", FCConvert.ToString(Conversion.Val(txtFEETitleFee.Text)));
			MotorVehicle.Statics.rsFinal.Set_Fields("RushTitleFee", FCConvert.ToString(Conversion.Val(txtFEERushTitleFee.Text)));
			MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", FCConvert.ToDecimal(txtFEESalesTax.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", FCConvert.ToDecimal(txtFEERegFee.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("RegRateFull", FCConvert.ToDecimal(txtFEERegFee.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed", FCConvert.ToDecimal(txtFEECreditTransfer.Text));
			MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditFull", FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", txtExpires.Text);
			if (MotorVehicle.Statics.RegistrationType != "CORR")
			{
				if (blnNextYear)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("EffectiveDate", "3/1/" + FCConvert.ToString(DateTime.Today.Year + 1));
				}
				else
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("EffectiveDate", DateTime.Today);
				}
			}
			MotorVehicle.Statics.rsFinal.Set_Fields("CommercialCredit", 0);
			MotorVehicle.Statics.rsFinal.Set_Fields("InitialFee", 0);
			MotorVehicle.Statics.rsFinal.Set_Fields("outofrotation", 0);
			MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlate", 0);
			if (MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReservePlate") > 0)
			{
				MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlateYN", -1);
			}
			MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew", 0);
			MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg", 0);
			MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee", 0);
			MotorVehicle.Statics.rsFinal.Set_Fields("StickerFee", 0);
			MotorVehicle.Statics.rsFinal.Set_Fields("TransferFee", txtFEETransferFee.Text);
			MotorVehicle.Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 0);
			MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid", FCConvert.ToDecimal(txtFEETotal.Text) - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFee") - FCConvert.ToDecimal(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCTA"))) - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeTransfer") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCorrection"));
			MotorVehicle.Statics.rsFinal.Set_Fields("LocalPaid", MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee") + MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCTA") + MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeTransfer"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ForcedPlate", MotorVehicle.Statics.ForcedPlate);
			MotorVehicle.Statics.rsFinal.Set_Fields("TitleNumber", fecherFoundation.Strings.Trim(txtTitle.Text).ToUpper());
			MotorVehicle.Statics.rsFinal.Set_Fields("Leased", "N");
			MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", chk25YearPlate.CheckState == Wisej.Web.CheckState.Checked);
		}

		private void FillSaveInfo()
		{
			MotorVehicle.Statics.rsSaveVehicle.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
			MotorVehicle.Statics.rsSaveVehicle.AddNew();
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TransactionType", MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ReReg", MotorVehicle.Statics.rsFinal.Get_Fields_String("ReReg"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Transfer", MotorVehicle.Statics.rsFinal.Get_Fields("Transfer"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PlateType", MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Subclass", MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("MonthStickerCharge", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("MonthStickerNoCharge", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNoCharge"));
			// rsSaveVehicle.Fields("MonthStickerMonth") = rsFinal.Fields("MonthStickerMonth")
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("MonthStickerNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("YearStickerCharge", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("YearStickerNoCharge", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNoCharge"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("YearStickerYear", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerYear"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("YearStickerNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTaxFull", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTaxCharged", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseCreditFull", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseCreditUsed", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTransferCharge", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTransferCharge"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseCreditMVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExciseCreditMVR3"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExciseTaxDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
			// rsSaveVehicle.Fields("ExcisePaidDate") = rsFinal.Fields("ExcisePaidDate")
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExcisePaidMVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ExcisePaidMVR3"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("GiftCertificateAmount", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("GiftCertificateAmount"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("GiftCertificateNumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Mail", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Mail"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("EMailAddress", MotorVehicle.Statics.rsFinal.Get_Fields_String("EMailAddress"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("MessageFlag", MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserMessage", MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserField1", MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField1"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserReason1", MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason1"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserField2", MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField2"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserReason2", MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason2"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserField3", MotorVehicle.Statics.rsFinal.Get_Fields_String("UserField3"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UserReason3", MotorVehicle.Statics.rsFinal.Get_Fields_String("UserReason3"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DoubleCTANumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("DoubleCTANumber").ToUpper());
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Status", MotorVehicle.Statics.rsFinal.Get_Fields_String("Status"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Address", MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("City", MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("State", MotorVehicle.Statics.rsFinal.Get_Fields("State"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Zip", MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("AgentFee", MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("AgentFeeCTA", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCTA"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("AgentFeeTransfer", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeTransfer"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("AgentFeeCorrection", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCorrection"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("OwnerCode1", MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PartyID1", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PartyID2", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("OpID", MotorVehicle.Statics.rsFinal.Get_Fields_String("OpID"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Residence", MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ResidenceState", MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ResidenceCode", MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("UnitNumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Style", MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Model", MotorVehicle.Statics.rsFinal.Get_Fields_String("Model"));
			// kk01252016 tromv-1080
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("color1", MotorVehicle.Statics.rsFinal.Get_Fields_String("color1"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("color2", MotorVehicle.Statics.rsFinal.Get_Fields_String("color2"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("make", MotorVehicle.Statics.rsFinal.Get_Fields_String("make"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Year", MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Vin", MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("NetWeight", MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TitleFee", MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("SalesTax", MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("RegRateCharge", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("RegRateFull", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TransferCreditUsed", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TransferCreditFull", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditFull"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ExpireDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("EffectiveDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("CommercialCredit", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("CommercialCredit"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("InitialFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("InitialFee"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("outofrotation", MotorVehicle.Statics.rsFinal.Get_Fields("outofrotation"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ReservePlate", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReservePlate"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ReservePlateYN", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ReservePlateYN"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PlateFeeNew", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("PlateFeeNew"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("PlateFeeReReg", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("PlateFeeReReg"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ReplacementFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("StickerFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("StickerFee"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TransferFee", MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("DuplicateRegistrationFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("StatePaid", MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("LocalPaid", MotorVehicle.Statics.rsFinal.Get_Fields("LocalPaid"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("ForcedPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate"));
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("TitleNumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber").ToUpper());
			MotorVehicle.Statics.rsSaveVehicle.Set_Fields("Leased", MotorVehicle.Statics.rsFinal.Get_Fields_String("Leased"));
		}

		public void FillSavedInfo()
		{
			string strTemp;
			MotorVehicle.Statics.rsFinal.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
			MotorVehicle.Statics.rsFinal.AddNew();
			MotorVehicle.Statics.rsFinal.Set_Fields("Plate", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Plate"));
			MotorVehicle.Statics.rsFinal.Set_Fields("TransactionType", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("TransactionType"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ReReg", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("ReReg"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Transfer", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("Transfer"));
			MotorVehicle.Statics.rsFinal.Set_Fields("PlateType", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("PlateType"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Subclass", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Subclass"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Class", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Class"));
			MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerCharge", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("MonthStickerCharge"));
			MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNoCharge", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("MonthStickerNoCharge"));
			// rsFinal.Fields("MonthStickerMonth") = rsSaveVehicle.Fields("MonthStickerMonth")
			MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNumber", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("MonthStickerNumber"));
			MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerCharge", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("YearStickerCharge"));
			MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNoCharge", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("YearStickerNoCharge"));
			MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerYear", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("YearStickerYear"));
			MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNumber", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("YearStickerNumber"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxFull", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ExciseTaxFull"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxCharged", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ExciseTaxCharged"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditFull", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ExciseCreditFull"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditUsed", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ExciseCreditUsed"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTransferCharge", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ExciseTransferCharge"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("ExciseCreditMVR3"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxDate", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("ExciseTaxDate"));
			// rsFinal.Fields("ExcisePaidDate") = rsSaveVehicle.Fields("ExcisePaidDate")
			MotorVehicle.Statics.rsFinal.Set_Fields("ExcisePaidMVR3", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("ExcisePaidMVR3"));
			MotorVehicle.Statics.rsFinal.Set_Fields("GiftCertificateAmount", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("GiftCertificateAmount"));
			MotorVehicle.Statics.rsFinal.Set_Fields("GiftCertificateNumber", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("GiftCertificateNumber"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Mail", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("Mail"));
			MotorVehicle.Statics.rsFinal.Set_Fields("EMailAddress", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("EMailAddress"));
			MotorVehicle.Statics.rsFinal.Set_Fields("MessageFlag", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("MessageFlag"));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserMessage", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("UserMessage"));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserField1", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("UserField1"));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserReason1", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("UserReason1"));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserField2", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("UserField2"));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserReason2", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("UserReason2"));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserField3", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("UserField3"));
			MotorVehicle.Statics.rsFinal.Set_Fields("UserReason3", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("UserReason3"));
			MotorVehicle.Statics.rsFinal.Set_Fields("DoubleCTANumber", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("DoubleCTANumber"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Status", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Status"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Address", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Address"));
			MotorVehicle.Statics.rsFinal.Set_Fields("City", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("City"));
			MotorVehicle.Statics.rsFinal.Set_Fields("State", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("State"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Zip", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Zip"));
			MotorVehicle.Statics.rsFinal.Set_Fields("AgentFee", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("AgentFee"));
			MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeCTA", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("AgentFeeCTA"));
			MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeTransfer", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("AgentFeeTransfer"));
			MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeCorrection", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("AgentFeeCorrection"));
			MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode1", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("OwnerCode1"));
			MotorVehicle.Statics.rsFinal.Set_Fields("PartyID1", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("PartyID1"));
			MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Int32("PartyID2"));
			MotorVehicle.Statics.rsFinal.Set_Fields("OpID", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("OpID"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Residence", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Residence"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("ResidenceState"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("ResidenceCode"));
			MotorVehicle.Statics.rsFinal.Set_Fields("UnitNumber", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("UnitNumber"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Style", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Style"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Model", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Model"));
			// kk01252016 tromv-1080
			MotorVehicle.Statics.rsFinal.Set_Fields("color1", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("color1"));
			MotorVehicle.Statics.rsFinal.Set_Fields("color2", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("color2"));
			MotorVehicle.Statics.rsFinal.Set_Fields("make", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("make"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Year", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("Year"));
			MotorVehicle.Statics.rsFinal.Set_Fields("Vin", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Vin"));
			MotorVehicle.Statics.rsFinal.Set_Fields("NetWeight", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("NetWeight"));
			MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("TitleFee"));
			MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("SalesTax"));
			MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("RegRateCharge"));
			MotorVehicle.Statics.rsFinal.Set_Fields("RegRateFull", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("RegRateFull"));
			MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("TransferCreditUsed"));
			MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditFull", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("TransferCreditFull"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("ExpireDate"));
			MotorVehicle.Statics.rsFinal.Set_Fields("EffectiveDate", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_DateTime("EffectiveDate"));
			MotorVehicle.Statics.rsFinal.Set_Fields("CommercialCredit", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("CommercialCredit"));
			MotorVehicle.Statics.rsFinal.Set_Fields("InitialFee", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("InitialFee"));
			MotorVehicle.Statics.rsFinal.Set_Fields("outofrotation", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("outofrotation"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlate", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ReservePlate"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlateYN", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Boolean("ReservePlateYN"));
			MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("PlateFeeNew"));
			MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("PlateFeeReReg"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("ReplacementFee"));
			MotorVehicle.Statics.rsFinal.Set_Fields("StickerFee", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("StickerFee"));
			MotorVehicle.Statics.rsFinal.Set_Fields("TransferFee", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("TransferFee"));
			MotorVehicle.Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_Decimal("DuplicateRegistrationFee"));
			MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("StatePaid"));
			MotorVehicle.Statics.rsFinal.Set_Fields("LocalPaid", MotorVehicle.Statics.rsSaveVehicle.Get_Fields("LocalPaid"));
			MotorVehicle.Statics.rsFinal.Set_Fields("ForcedPlate", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("ForcedPlate"));
			MotorVehicle.Statics.rsFinal.Set_Fields("TitleNumber", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("TitleNumber").ToUpper());
			MotorVehicle.Statics.rsFinal.Set_Fields("Leased", MotorVehicle.Statics.rsSaveVehicle.Get_Fields_String("Leased"));
			strTemp = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin"));
			strTemp = Strings.Left(strTemp, 8) + "_" + Strings.Mid(strTemp, 10, 6) + "_";
			txtVIN.Text = strTemp;
		}

		private void SetUserFields()
		{
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			if (!fecherFoundation.FCUtils.IsNull(rsDefaultInfo.Get_Fields_String("UserLabel1")))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1"))) != "")
				{
					fraUserFields.Visible = true;
					lblUserField1.Visible = true;
					txtUserField1.Visible = true;
					lblInfo1.Visible = true;
					txtUserReason1.Visible = true;
					lblUserField1.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel1")));
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(rsDefaultInfo.Get_Fields_String("UserLabel2")))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2"))) != "")
				{
					fraUserFields.Visible = true;
					lblUserField2.Visible = true;
					txtUserField2.Visible = true;
					lblInfo2.Visible = true;
					txtUserReason2.Visible = true;
					lblUserField2.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel2")));
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(rsDefaultInfo.Get_Fields_String("UserLabel3")))
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3"))) != "")
				{
					fraUserFields.Visible = true;
					lblUserField3.Visible = true;
					txtUserField3.Visible = true;
					lblInfo3.Visible = true;
					txtUserReason3.Visible = true;
					lblUserField3.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("UserLabel3")));
				}
			}
			rsDefaultInfo.Reset();
		}

		private void cmdReg1Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID;
			lngID = FCConvert.ToInt32(txtReg1PartyID.Text);
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtReg1PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 1);
			}
			cmdReg1Edit.Focus();
		}

		private void cmdReg1Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtReg1PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 1);
			}
			cmdReg1Search.Focus();
		}

		private void cmdReg2Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID;
			lngID = FCConvert.ToInt32(txtReg2PartyID.Text);
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtReg2PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 2);
			}
			cmdReg2Edit.Focus();
		}

		private void cmdReg2Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtReg2PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 2);
			}
			cmdReg2Search.Focus();
		}

		private void txtReg1PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReg1PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtReg1PartyID.Text) && Conversion.Val(txtReg1PartyID.Text) > 0)
			{
				GetPartyInfo(FCConvert.ToInt16(FCConvert.ToDouble(txtReg1PartyID.Text)), 1);
			}
			else
			{
				ClearPartyInfo(1);
			}
		}

		private void txtReg2PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReg2PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtReg2PartyID.Text) && Conversion.Val(txtReg2PartyID.Text) > 0)
			{
				GetPartyInfo(FCConvert.ToInt16(FCConvert.ToDouble(txtReg2PartyID.Text)), 2);
			}
			else
			{
				ClearPartyInfo(2);
			}
		}

		public void ClearPartyInfo(int intReg)
		{
			switch (intReg)
			{
				case 1:
					{
						lblRegistrant1.Text = "";
						break;
					}
				case 2:
					{
						lblRegistrant2.Text = "";
						break;
					}
				default:
					{
						// do nothing
						break;
					}
			}
			//end switch
		}

		public void GetPartyInfo(int intPartyID, int intReg)
		{
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			cPartyAddress pAdd;
			FCCollection pComments = new FCCollection();
			pInfo = pCont.GetParty(intPartyID);
			if (!(pInfo == null))
			{
				switch (intReg)
				{
					case 1:
						{
							lblRegistrant1.Text = fecherFoundation.Strings.UCase(pInfo.FullName);
							break;
						}
					case 2:
						{
							lblRegistrant2.Text = fecherFoundation.Strings.UCase(pInfo.FullName);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
				//end switch
			}
		}

		private void LoadLengthCombo()
		{
			int counter;
			cboLength.Clear();
            if (!MotorVehicle.Statics.gboolLongRegExtensionSamePlate)
            {
                for (counter = 2; counter <= MotorVehicle.Statics.gintLongTermYearsAllowed; counter++)
                {
                    cboLength.AddItem(FCConvert.ToString(counter) + " Years");
                    cboLength.ItemData(cboLength.NewIndex, counter);
                }
            }
            else
            {
                for (counter = 3; counter <= 12; counter++)
                {
                    cboLength.AddItem(FCConvert.ToString(counter) + " Years");
                    cboLength.ItemData(cboLength.NewIndex, counter);
                }
            }
			
		}

		private void imgFleetComment_Click(object sender, System.EventArgs e)
		{
			mnuFleetGroupComment_Click();
		}

		private void mnuFleetGroupComment_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtFleetNumber.Text) > 0)
			{
				if (!frmCommentOld.InstancePtr.Init("MV", "FleetMaster", "Comment", "FleetNumber", FCConvert.ToInt32(FCConvert.ToDouble(txtFleetNumber.Text)), boolModal: true))
				{
					imgFleetComment.Visible = true;
				}
				else
				{
					imgFleetComment.Visible = false;
				}
			}
		}

		public void mnuFleetGroupComment_Click()
		{
			mnuFleetGroupComment_Click(mnuFleetGroupComment, new System.EventArgs());
		}

		private void chkOverrideFleetEmail_CheckedChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (chkOverrideFleetEmail.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtEMail.Enabled = true;
			}
			else
			{
				if (cmbFleet.Text == "Group")
				{
					rsTemp.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted = false AND Type <> 'L' AND ExpiryMonth = 99 ORDER BY Owner1Name");
				}
				else
				{
					rsTemp.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted = false AND Type <> 'L' AND FleetNumber = " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + " AND ExpiryMonth <> 99");
				}
				if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
				{
					txtEMail.Text = FCConvert.ToString(rsTemp.Get_Fields_String("EmailAddress"));
				}
				txtEMail.Enabled = false;
			}
		}

		public bool VerifyVIN()
		{
			bool VerifyVIN = false;
			// vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
			DialogResult ans = 0;
			clsDRWrapper rsCheck = new clsDRWrapper();
			VerifyVIN = true;
			if (fecherFoundation.Strings.Trim(txtVIN.Text) == "")
			{
				MessageBox.Show("You must enter a VIN before you may proceed.", "No VIN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				VerifyVIN = false;
				return VerifyVIN;
			}

			if (MotorVehicle.Statics.RegistrationType == "NRT")
			{
				// kk12222015 This isn't working because the VIN isn't in the Compare record.?
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("Vin")) == fecherFoundation.Strings.Trim(txtVIN.Text))
				{
					ans = MessageBox.Show("The VIN you entered is the same VIN the transfer vehicle had.  Would you like to continue?", "Duplicate VIN", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.No)
					{
						VerifyVIN = false;
						txtVIN.Focus();
						return VerifyVIN;
					}
				}
			}
			if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
			{
				if (txtVIN.Text != "0" && fecherFoundation.Strings.UCase(txtVIN.Text) != "NONE" && fecherFoundation.Strings.UCase(txtVIN.Text) != "HOMEMADE" && fecherFoundation.Strings.UCase(txtVIN.Text) != "HOME MADE")
				{
					rsCheck.OpenRecordset("SELECT * FROM MASTER WHERE ExpireDate > '" + DateTime.Today.ToShortDateString() + "' AND VIN = '" + txtVIN.Text + "' AND ID <> " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						ans = MessageBox.Show("The VIN you entered is the same VIN that is registered on the following vehicle." + "\r\n" + "\r\n" + "Class: " + rsCheck.Get_Fields_String("Class") + "\r\n" + "Plate: " + rsCheck.Get_Fields_String("Plate") + "\r\n" + "Expires: " + Strings.Format(rsCheck.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy") + "\r\n" + "\r\n" + "Would you like to continue?", "Duplicate VIN", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.No)
						{
							VerifyVIN = false;
							txtVIN.Focus();
							return VerifyVIN;
						}
					}
				}
			}			
			return VerifyVIN;
		}

		private void cmbNoMaineReReg_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbNoMaineReReg.Text == "NO")
			{
				optNoMaineReReg_CheckedChanged(sender, e);
			}
			else if (cmbNoMaineReReg.Text == "YES - Expires this year")
			{
				optYesMaineReRegCurrentYear_CheckedChanged(sender, e);
			}
			else if (cmbNoMaineReReg.Text == "YES - Expires next year")
			{
				optYesMaineReRegNextYear_CheckedChanged(sender, e);
			}
		}

        private void SetLengthCombo(int years)
        {
            for (int x = 0; x < cboLength.ListCount; x++)
            {
                if (cboLength.ItemData(x) == years)
                {
                    cboLength.ListIndex = x;
                    return;
                }
            }
        }

        private int GetMaxLengthComboValue()
        {
            return cboLength.ItemData(cboLength.ListCount - 1);
        }
	}
}
