﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmTransitPlateInformation.
	/// </summary>
	partial class frmTransitPlateInformation
	{
		public FCGrid vsInfo;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.vsInfo = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsInfo)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 538);
			this.BottomPanel.Size = new System.Drawing.Size(673, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsInfo);
			this.ClientArea.Size = new System.Drawing.Size(673, 586);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(673, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(281, 30);
			this.HeaderText.Text = "Transit Plate Information";
			// 
			// vsInfo
			// 
			this.vsInfo.AllowSelection = false;
			this.vsInfo.AllowUserToResizeColumns = false;
			this.vsInfo.AllowUserToResizeRows = false;
			this.vsInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsInfo.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsInfo.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsInfo.BackColorBkg = System.Drawing.Color.Empty;
			this.vsInfo.BackColorFixed = System.Drawing.Color.Empty;
			this.vsInfo.BackColorSel = System.Drawing.Color.Empty;
			this.vsInfo.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsInfo.Cols = 7;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsInfo.ColumnHeadersHeight = 30;
			this.vsInfo.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsInfo.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsInfo.DragIcon = null;
			this.vsInfo.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsInfo.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsInfo.ExtendLastCol = true;
			this.vsInfo.FixedCols = 0;
			this.vsInfo.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsInfo.FrozenCols = 0;
			this.vsInfo.GridColor = System.Drawing.Color.Empty;
			this.vsInfo.GridColorFixed = System.Drawing.Color.Empty;
			this.vsInfo.Location = new System.Drawing.Point(30, 30);
			this.vsInfo.Name = "vsInfo";
			this.vsInfo.OutlineCol = 0;
			this.vsInfo.ReadOnly = true;
			this.vsInfo.RowHeadersVisible = false;
			this.vsInfo.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsInfo.RowHeightMin = 0;
			this.vsInfo.Rows = 1;
			this.vsInfo.ScrollTipText = null;
			this.vsInfo.ShowColumnVisibilityMenu = false;
			this.vsInfo.ShowFocusCell = false;
			this.vsInfo.Size = new System.Drawing.Size(615, 531);
			this.vsInfo.StandardTab = true;
			this.vsInfo.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsInfo.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsInfo.TabIndex = 0;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 0;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmTransitPlateInformation
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(673, 646);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmTransitPlateInformation";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Transit Plate Information";
			this.Load += new System.EventHandler(this.frmTransitPlateInformation_Load);
			this.Activated += new System.EventHandler(this.frmTransitPlateInformation_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTransitPlateInformation_KeyPress);
			this.Resize += new System.EventHandler(this.frmTransitPlateInformation_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsInfo)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
