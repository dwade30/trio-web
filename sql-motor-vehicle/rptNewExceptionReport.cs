//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptNewExceptionReport.
	/// </summary>
	public partial class rptNewExceptionReport : BaseSectionReport
	{
		public rptNewExceptionReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Exception Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptNewExceptionReport_ReportEnd;
		}

        private void RptNewExceptionReport_ReportEnd(object sender, EventArgs e)
        {
			rs.DisposeOf();
            rsCategory.DisposeOf();

		}

        public static rptNewExceptionReport InstancePtr
		{
			get
			{
				return (rptNewExceptionReport)Sys.GetInstance(typeof(rptNewExceptionReport));
			}
		}

		protected rptNewExceptionReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewExceptionReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public int lngPK1;
		public int lngPK2;
		clsDRWrapper rs = new clsDRWrapper();
		public string strType = "";
		string strTypeHeading = "";
		int intHeadingNumber;
		bool CompletedECorrects;
		//clsDRWrapper rsTemp = new clsDRWrapper();
		public int StickerTotal;
		public decimal StickerMoneyTotal;
		public int PlateTotal;
		public int PermitTotal;
		public int NoFeeDupRegTotal;
		public decimal PermitMoneyTotal;
		public int MonAdjTotal;
		public float MonAdjMoneyTotal;
		bool boolFirst;
		string tempSticker = "";
		clsDRWrapper rsCategory = new clsDRWrapper();
		public int MonCorrTotal;
		public int NonMonCorrTotal;
		public float MonCorrMoneyTotal;
		bool boolMonetaryDone;
		int intPageNumber;
		public bool boolNonMonetaryDone;
		int intFieldSize;
		bool boolTotalDone;
		public bool boolData;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            if (strType != "")
            {
                eArgs.EOF = false;
			}
            else
            {
                eArgs.EOF = true;
            }
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rs1 = new clsDRWrapper();
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			string strLevel = "";
			//Application.DoEvents();
			boolData = false;
			intPageNumber = 0;
			StickerTotal = 0;
			StickerMoneyTotal = 0;
			PlateTotal = 0;
			PermitTotal = 0;
			PermitMoneyTotal = 0;
			NoFeeDupRegTotal = 0;
			NonMonCorrTotal = 0;
			MonAdjMoneyTotal = 0;
			MonCorrTotal = 0;
			MonAdjMoneyTotal = 0;
			MonAdjTotal = 0;
			rsCategory.OpenRecordset("SELECT * FROM TownSummaryHeadings");
			MotorVehicle.Statics.strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(MotorVehicle.Statics.strSql);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			}
			strType = "1S";
			//Application.DoEvents();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeading();
			intPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			rptNewExceptionReport.InstancePtr.Fields.Add("Binder");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (boolData == false)
			{
				txtNoInfo.Text = "No Information";
			}
			else
			{
				txtTSticker.Text = "TOTAL STICKER ADJUSTMENTS:";
				txtTSticker1.Text = StickerTotal.ToString();
				txtStickerFeeTotal.Text = Strings.Format(Strings.Format(StickerMoneyTotal, "#,##0.00"), "@@@@@@@@");
				txtTMC.Text = "TOTAL MONETARY CORRECTIONS:";
				txtTMC1.Text = MonCorrTotal.ToString();
				txtTMC2.Text = Strings.Format(Strings.Format(MonCorrMoneyTotal, "#,##0.00"), "@@@@@@@@");
				txtTNMC.Text = "TOTAL NON-MONETARY CORRECTIONS:";
				txtTNMC1.Text = NonMonCorrTotal.ToString();
				txtTMA.Text = "TOTAL MONETARY ADJUSTMENTS:";
				txtTMA1.Text = MonAdjTotal.ToString();
				txtTMA2.Text = Strings.Format(Strings.Format(MonAdjMoneyTotal, "#,##0.00"), "@@@@@@@@");
				txtTPM.Text = "TOTAL PERMITS:";
				txtTPM1.Text = PermitTotal.ToString();
				txtTPM2.Text = Strings.Format(Strings.Format(PermitMoneyTotal, "#,##0.00"), "@@@@@@@@");
				txtTDR.Text = "TOTAL NO-FEE DUPLICATE-STR:";
				txtTDR1.Text = NoFeeDupRegTotal.ToString();
			}
		}

        private void Detail_Format(object sender, EventArgs e)
        {
            if (strType == "1S")
            {
                SubReport1.Report = new rptSubStickerException();
                strType = "2P";
            }
            else if (strType == "2P")
            {
                SubReport1.Report = new rptSubPlateException();
                strType = "3CM";
            }
            else if (strType == "3CM")
            {
                SubReport1.Report = new rptSubMonetary();
                strType = "4CN";
            }
            else if (strType == "4CN")
            {
                SubReport1.Report = new rptSubNonMonetary();
                strType = "5MA";
            }
            else if (strType == "5MA")
            {
                SubReport1.Report = new rptSubMonetaryAdjustments();
                strType = "6PM";
            }
            else if (strType == "6PM")
            {
                SubReport1.Report = new rptSubPermitException();
                strType = "7DR";
            }
            else if (strType == "7DR")
            {
                SubReport1.Report = new rptSubNoFeeDupRegException();
                strType = "";
            }
            else
            {
                SubReport1.Report = null;
            }
		}
    }
}
