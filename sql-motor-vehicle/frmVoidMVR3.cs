//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmVoidMVR3 : BaseForm
	{
		public frmVoidMVR3()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
            this.txtVoidedMVR3.AllowOnlyNumericInput();
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmVoidMVR3 InstancePtr
		{
			get
			{
				return (frmVoidMVR3)Sys.GetInstance(typeof(frmVoidMVR3));
			}
		}

		protected frmVoidMVR3 _InstancePtr = null;
		//=========================================================
		private int recIDtoVoid;
		private bool MVR3Issued;
		int lngHeldID;
		clsDRWrapper rsVoid = new clsDRWrapper();
		string strSQL = "";
		string strHoldPlate = "";
		string strHoldClass = "";
		bool UnIssued;
		int VoidMVR3;
		int ArchiveMVR3;
		bool PreviousPeriod;
		bool blnRRTrans;
		clsDRWrapper rsAM = new clsDRWrapper();
		bool blnMVRT10Void;
		int IDCol;
		int TransactionDateCol;
		int NameCol;
		int YearCol;
		int MakeCol;
		int VINCol;
		string strSaveMVR3 = "";
		// kk01062017  trommv-1251/tromv-1256
		private void cmdNO_Click(object sender, System.EventArgs e)
		{
			cmdProcess.Enabled = false;
			cmdFileProcess.Enabled = false;
			// kk01062017  trommv-1251/tromv-1256  Disable menu with button
			Frame1.Visible = false;
			chkMonth.Visible = false;
			chkYear.Visible = false;
			chkPlate.Visible = false;
			fraUnissued.Visible = false;
			if (cmbMVR3.Text == "MVR-3")
			{
				fraMVRT10.Visible = false;
				// kk05032017 tromv-1281  Check for unissued MVR with same number
				rsVoid.Reset();
				strSQL = "SELECT * FROM Inventory WHERE Code = 'MXS00' AND Number = " + FCConvert.ToString(Conversion.Val(txtVoidedMVR3.Text)) + " And Status = 'A'";
				rsVoid.OpenRecordset(strSQL);
				if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
				{
					// vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
					DialogResult answer = 0;
					answer = MessageBox.Show("Is this a new, unissued MVR3?", "New MVR3?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (answer == DialogResult.No)
					{
						txtVoidedMVR3.Focus();
					}
					else
					{
						UnIssued = true;
						fraUnissued.Visible = true;
						cmdProcess.Enabled = true;
						cmdFileProcess.Enabled = true;
					}
				}
				else
				{
					txtVoidedMVR3.Focus();
				}
			}
			else
			{
				fraMVRT10.Visible = true;
				vsMVRT10.Focus();
			}
		}

		public void cmdNO_Click()
		{
			cmdNO_Click(cmdNO, new System.EventArgs());
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			// call process routine here
			// when form is unloaded and focus returns to the
			// plate info screen the plateinfo screen is going to unload
			// and automatically return to the menu.
			// vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
			DialogResult ans = 0;
			if (txtReason.Text == "")
			{
				MessageBox.Show("You must enter a reason before you can continue with the void.", "Reason Needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (chkMonth.CheckState == Wisej.Web.CheckState.Checked || chkPlate.CheckState == Wisej.Web.CheckState.Checked || chkYear.CheckState == Wisej.Web.CheckState.Checked)
			{
				ans = MessageBox.Show("Inventory should only be returned if it was issued with this registration.  Are you sure you wish to return this inventory?", "Return Inventory?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.No)
				{
					return;
				}
			}
			if (!blnMVRT10Void)
			{
				if (Process_Routine() == true)
				{
					MessageBox.Show("The void of this MVR3 was successful.", "Sucessful Void", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
					if (MotorVehicle.Statics.bolFromWindowsCR)
					{
						//MDIParent.InstancePtr.GetMainMenu();
						MDIParent.InstancePtr.Menu18();
					}
				}
				else
				{
					MessageBox.Show("There is an error Voiding this MVR3.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				if (Process_Routine_MVRT10() == true)
				{
					MessageBox.Show("The void of this MVRT-10 was successful.", "Sucessful Void", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
					if (MotorVehicle.Statics.bolFromWindowsCR)
					{
						//MDIParent.InstancePtr.GetMainMenu();
						MDIParent.InstancePtr.Menu18();
					}
				}
				else
				{
					MessageBox.Show("There is an error Voiding this MVRT-10.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			UnIssued = false;
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			modGNBas.Statics.Response = "QUIT";
			Close();
		}

        private void FrmVoidMVR3_FormClosed(object sender, FormClosedEventArgs e)
        {
            //FC:FINAL:SBE - #2389 - execute the same code as Quit button did
            modGNBas.Statics.Response = "QUIT";
        }

		public void cmdQuit_Click()
		{
			cmdQuit_Click(cmdQuit, new System.EventArgs());
		}

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			if (this.ActiveControl.GetName() != "cmdQuit")
			{
				if (vsMVRT10.Row != 0)
				{
					Get_Master_Record();
					fraMVRT10.Visible = false;
				}
				if (cmdYes.Visible == true)
				{
					cmdYes.Focus();
				}
				else
				{
					//cmdQuit.Focus();
				}
			}
		}

		public void cmdSelect_Click()
		{
			cmdSelect_Click(cmdSelect, new System.EventArgs());
		}

		private void cmdYes_Click(object sender, System.EventArgs e)
		{
			bool boolPlateIssued = false;
			bool boolMStickerIssued = false;
			bool boolYStickerIssued = false;
			// kk01052017  tromv-1256/tromv-1251  Move this to after doing the inventory checks
			// cmdProcess.Enabled = True
			if (!blnMVRT10Void)
			{
				// kk11292016 tromv-1191  Only enable return checkboxes for inventroy issued with this mvr - sync from Access version
				CheckInventoryIssued(ref boolMStickerIssued, ref boolYStickerIssued, ref boolPlateIssued);
				if (Strings.Left(lblStickers.Text, 1) == "C")
				{
					chkMonth.CheckState = Wisej.Web.CheckState.Unchecked;
					chkMonth.Visible = false;
					chkMonth.Enabled = false;
				}
				else
				{
					chkMonth.Visible = true;
					if (boolMStickerIssued)
					{
						chkMonth.Enabled = true;
					}
					else
					{
						chkMonth.CheckState = Wisej.Web.CheckState.Unchecked;
						chkMonth.Enabled = false;
					}
				}
				chkYear.Visible = true;
				if (boolYStickerIssued)
				{
					chkYear.Enabled = true;
				}
				else
				{
					chkYear.CheckState = Wisej.Web.CheckState.Unchecked;
					chkYear.Enabled = false;
				}
				chkPlate.Visible = true;
				if (boolPlateIssued)
				{
					chkPlate.Enabled = true;
				}
				else
				{
					chkPlate.CheckState = Wisej.Web.CheckState.Unchecked;
					chkPlate.Enabled = false;
				}
				// chkMonth.SetFocus
			}
			else
			{
				chkPlate.Visible = true;
				// txtReason.SetFocus
			}
			cmdProcess.Enabled = true;
			// kk01052017 Moved from top
			cmdFileProcess.Enabled = true;
			// kk01062017  trommv-1251/tromv-1256  Enable menu with button
			txtReason.Focus();
		}

		private void frmVoidMVR3_Load(object sender, System.EventArgs e)
		{
            txtVoidedMVR3.Text = FCConvert.ToString(modRegistry.GetRegistryKey("LastMVR3Used"));
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			if (MotorVehicle.Statics.gboolAllowLongTermTrailers)
			{
				cmbMVR3.Visible = true;
				IDCol = 0;
				TransactionDateCol = 1;
				NameCol = 2;
				YearCol = 3;
				MakeCol = 4;
				VINCol = 5;
				vsMVRT10.ColHidden(IDCol, true);
				vsMVRT10.TextMatrix(0, TransactionDateCol, "Date");
				vsMVRT10.TextMatrix(0, NameCol, "Owner");
				vsMVRT10.TextMatrix(0, YearCol, "Year");
				vsMVRT10.TextMatrix(0, MakeCol, "Make");
				vsMVRT10.TextMatrix(0, VINCol, "VIN");
				vsMVRT10.ColWidth(TransactionDateCol, FCConvert.ToInt32(vsMVRT10.WidthOriginal * 0.13));
				vsMVRT10.ColWidth(NameCol, FCConvert.ToInt32(vsMVRT10.WidthOriginal * 0.4));
				vsMVRT10.ColWidth(YearCol, FCConvert.ToInt32(vsMVRT10.WidthOriginal * 0.08));
				vsMVRT10.ColWidth(MakeCol, FCConvert.ToInt32(vsMVRT10.WidthOriginal * 0.1));
				vsMVRT10.ColWidth(VINCol, FCConvert.ToInt32(vsMVRT10.WidthOriginal * 0.1));
				vsMVRT10.ColAlignment(TransactionDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsMVRT10.ColAlignment(YearCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				FillMVRT10Grid();
			}
			blnMVRT10Void = false;
			// kk01062017  trommv-1251/tromv-1256  Start with Process menu and button disabled
			cmdProcess.Enabled = false;
			cmdFileProcess.Enabled = false;
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			// kk01062017 tromv-1251/1256  Force validation for txtVoidedMVR3
			if (this.ActiveControl.GetName() == "txtVoidedMVR3")
			{
				if (strSaveMVR3 != txtVoidedMVR3.Text)
				{
					ValidateVoidMVR3();
				}
			}
			if (cmdProcess.Enabled)
			{
				cmdProcess_Click();
			}
		}

		private void optMVR3_CheckedChanged(object sender, System.EventArgs e)
		{
			blnMVRT10Void = false;
			cmdNO_Click();
		}

		private void optMVRT10_CheckedChanged(object sender, System.EventArgs e)
		{
			blnMVRT10Void = true;
			cmdNO_Click();
		}

		private void txtVoidedMVR3_Enter(object sender, System.EventArgs e)
		{
			strSaveMVR3 = txtVoidedMVR3.Text;
		}

		private void txtVoidedMVR3_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void Get_Master_Record()
		{
			// kk01062017 tromv-1251/1256 - Reset everything so Process button/menu are disabled until Yes is clicked
			Frame1.Visible = false;
			cmdProcess.Enabled = false;
			cmdFileProcess.Enabled = false;
			chkMonth.Visible = false;
			chkYear.Visible = false;
			chkPlate.Visible = false;
			fraUnissued.Visible = false;
			if (!blnMVRT10Void)
			{
				UnIssued = false;
				strSQL = "SELECT * FROM Master WHERE MVR3 = " + FCConvert.ToString(Conversion.Val(txtVoidedMVR3.Text)) + " ORDER BY Status DESC";
				rsVoid.OpenRecordset(strSQL);
				if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
				{
					rsVoid.MoveLast();
					rsVoid.MoveFirst();
					if (FCConvert.ToString(rsVoid.Get_Fields_String("Status")) == "T")
						rsVoid.MoveNext();
					lblName.Text = "OWNER 1    = " + fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsVoid.Get_Fields_Int32("PartyID1"), true));
					lblAddress.Text = "ADDRESS    = " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsVoid.Get_Fields_String("Address")));
					lblPlateClass.Text = "PLATE / CL = " + rsVoid.Get_Fields_String("plate") + " / " + rsVoid.Get_Fields_String("Class");
					lblVehicle.Text = "MAKE / MDL = " + rsVoid.Get_Fields_String("make") + " / " + rsVoid.Get_Fields_String("model");
					lblDate.Text = "TRANS DATE = " + FCConvert.ToString(rsVoid.Get_Fields("DateUpdated"));
					if (MotorVehicle.IsMotorcycle(rsVoid.Get_Fields_String("Class")))
					{
						lblStickers.Text = "COMB STKS = " + rsVoid.Get_Fields_Int32("YearStickerNumber");
						// kk11292016 tromv-1191  This get's re-enabled in cmdYes_Click
						// chkMonth.Value = False
						// chkMonth.Visible = False
						chkYear.Text = "Check this box to return the Combination Stickers";
					}
					else
					{
						lblStickers.Text = "MO/YR STKS = " + "M=" + rsVoid.Get_Fields_Int32("MonthStickerNumber") + "  /  Y=" + rsVoid.Get_Fields_Int32("YearStickerNumber");
						// kk11292016 tromv-1191  This get's re-enabled in cmdYes_Click
						// chkMonth.Visible = True
						chkYear.Text = "Check this box to return the Year Stickers";
					}
					Frame1.Visible = true;
					// kk01062017 tromv-1251/1256  Moved to top       cmdProcess.Enabled = False
				}
				else
				{
					// kk05032017 tromv-1281  Reworked Void Unissued MVR
					Frame1.Visible = false;
					rsVoid.Reset();
					strSQL = "SELECT * FROM Inventory WHERE Code = 'MXS00' AND Number = " + FCConvert.ToString(Conversion.Val(txtVoidedMVR3.Text)) + " And Status = 'A'";
					rsVoid.OpenRecordset(strSQL);
					if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
					{
						// vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
						DialogResult answer = 0;
						answer = MessageBox.Show("There are no records in the Master File with this MVR3 Number.  Is this a new, unissued MVR3?", "New MVR3?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (answer == DialogResult.Yes)
						{
							UnIssued = true;
							fraUnissued.Visible = true;
							cmdProcess.Enabled = true;
							cmdFileProcess.Enabled = true;
						}
					}
					else
					{
						MessageBox.Show("There are no records in Inventory or in the Master File with this MVR3 Number.", "MVR3 Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
			else
			{
				UnIssued = false;
				strSQL = "SELECT * FROM Master WHERE ID = " + FCConvert.ToString(Conversion.Val(vsMVRT10.TextMatrix(vsMVRT10.Row, IDCol))) + " ORDER BY Status DESC";
				rsVoid.OpenRecordset(strSQL);
				if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
				{
					rsVoid.MoveLast();
					rsVoid.MoveFirst();
					if (FCConvert.ToString(rsVoid.Get_Fields_String("Status")) == "T")
						rsVoid.MoveNext();
					lblName.Text = "OWNER 1    = " + fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsVoid.Get_Fields_Int32("PartyID1"), true));
					lblAddress.Text = "ADDRESS    = " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsVoid.Get_Fields_String("Address")));
					lblPlateClass.Text = "PLATE / CL = " + rsVoid.Get_Fields_String("plate") + " / " + rsVoid.Get_Fields_String("Class");
					lblVehicle.Text = "MAKE = " + rsVoid.Get_Fields_String("make");
					lblDate.Text = "TRANS DATE = " + FCConvert.ToString(rsVoid.Get_Fields("DateUpdated"));
					lblStickers.Text = "";
					Frame1.Visible = true;
					cmdProcess.Enabled = false;
					cmdFileProcess.Enabled = false;
					// kk01062017 tromv-1251/1256
				}
				else
				{
					MessageBox.Show("There are no records in the Master File with this MVRT-10 registration.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					UnIssued = false;
					return;
				}
			}
		}

		private bool Process_Routine()
		{
			bool Process_Routine = false;
			clsDRWrapper rsOldInfo = new clsDRWrapper();
			bool Deletion;
			clsDRWrapper rsMO = new clsDRWrapper();
			clsDRWrapper rsGiftCert = new clsDRWrapper();
			Process_Routine = true;
			try
			{
				// On Error GoTo ErrorTag1
				fecherFoundation.Information.Err().Clear();
				if (UnIssued == false)
				{
					if (fecherFoundation.FCUtils.IsNull(rsVoid.Get_Fields_Int32("OldMVR3")))
					{
						VoidMVR3 = 0;
					}
					else if (rsVoid.IsFieldNull("OldMVR3"))
					{
						VoidMVR3 = 0;
					}
					else
					{
						VoidMVR3 = FCConvert.ToInt32(rsVoid.Get_Fields_Int32("OldMVR3"));
					}
					// If rsVoid.Fields("TransactionType") = "RRT" Then
					// blnRRTrans = True
					// Else
					// blnRRTrans = False
					// End If
					if (fecherFoundation.FCUtils.IsNull(rsVoid.Get_Fields_Int32("MVR3")))
					{
						ArchiveMVR3 = 0;
					}
					else if (rsVoid.IsFieldNull("MVR3"))
					{
						ArchiveMVR3 = 0;
					}
					else
					{
						if (blnRRTrans)
						{
							ArchiveMVR3 = VoidMVR3;
						}
						else
						{
							ArchiveMVR3 = FCConvert.ToInt32(rsVoid.Get_Fields_Int32("MVR3"));
						}
					}
					if (PreviousPeriod)
					{
						rsMO.OpenRecordset("SELECT * FROM ExceptionReport WHERE ID = 0");
						rsMO.AddNew();
						rsMO.Set_Fields("Type", "4CN");
						rsMO.Set_Fields("MVR3Number", rsVoid.Get_Fields_Int32("MVR3"));
						rsMO.Set_Fields("NewPlate", rsVoid.Get_Fields_String("plate"));
						rsMO.Set_Fields("newclass", rsVoid.Get_Fields_String("Class"));
						rsMO.Set_Fields("reason", "Voiding MVR3 from Prior Closeout Period");
						rsMO.Set_Fields("OpID", MotorVehicle.Statics.OpID);
						rsMO.Set_Fields("Fee", 0);
						rsMO.Set_Fields("PeriodCloseoutID", 0);
						rsMO.Set_Fields("TellerCloseoutID", 0);
						rsMO.Set_Fields("ExceptionReportDate", DateTime.Today);
						rsMO.Update();
						PreviousPeriod = false;
					}
					if (!String.IsNullOrEmpty(rsVoid.Get_Fields_String("CTANumber")) && FCConvert.ToInt32(rsVoid.Get_Fields_String("CTANumber")) != 0)
					{
						rsAM.OpenRecordset("SELECT * FROM TitleApplications WHERE CTANumber = '" + rsVoid.Get_Fields_String("CTANumber") + "'");
						if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
						{
							rsAM.MoveLast();
							rsAM.MoveFirst();
							rsAM.Delete();
							rsAM.Update();
						}
						if (!String.IsNullOrEmpty(rsVoid.Get_Fields_String("DoubleCTANumber")) && FCConvert.ToInt32(rsVoid.Get_Fields_String("DoubleCTANumber")) != 0)
						{
							rsAM.OpenRecordset("SELECT * FROM TitleApplications WHERE CTANumber = '" + rsVoid.Get_Fields_String("DoubleCTANumber") + "'");
							if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
							{
								rsAM.MoveLast();
								rsAM.MoveFirst();
								rsAM.Delete();
								rsAM.Update();
							}
						}
					}

                    rsAM.OpenRecordset("SELECT * FROM ExciseTax WHERE MVR3Number = " + rsVoid.Get_Fields_Int32("MVR3"));
                    if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
                    {
                        rsAM.MoveLast();
                        rsAM.MoveFirst();
                        rsAM.Delete();
                        rsAM.Update();
                    }
					// rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE MVR3 = " + rsVoid.Get_Fields_Int32("MVR3") + " AND rtrim(Plate) = '" + fecherFoundation.Strings.Trim(FCConvert.ToString(rsVoid.Get_Fields_String("plate"))) + "'");
					rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE ID = " + recIDtoVoid);
					if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
					{
						rsAM.MoveLast();
						rsAM.MoveFirst();
						rsAM.Edit();
						rsAM.Set_Fields("Status", "V");
						if (FCConvert.ToInt32(rsAM.Get_Fields_Int32("OldMVR3")) == 0)
						{
							rsAM.Set_Fields("TellerCloseoutID", 0);
						}
						if (FCConvert.ToString(rsAM.Get_Fields_String("GiftCertOrVoucher")) == "G" || FCConvert.ToString(rsAM.Get_Fields_String("GiftCertOrVoucher")) == "V")
						{
							rsGiftCert.OpenRecordset("SELECT * FROM GiftCertificateRegister WHERE RegistrationID = " + rsAM.Get_Fields_Int32("ID") + " AND CertificateNumber = '" + rsAM.Get_Fields_String("GiftCertificateNumber") + "'", "TWMV0000.vb1");
							if (rsGiftCert.EndOfFile() != true && rsGiftCert.BeginningOfFile() != true)
							{
								rsGiftCert.MoveLast();
								rsGiftCert.MoveFirst();
								rsGiftCert.Delete();
								rsGiftCert.Update();
							}
						}
						rsAM.Update();
						if (MotorVehicle.Statics.bolFromWindowsCR)
						{
							Write_PDS_Work_Record_Stuff_Void();
						}
					}

					if (MVR3Issued)
					{
						AddMasterInventoryAdjustments(rsVoid.Get_Fields_Int32("MVR3"), txtReason.Text);
						if (rsVoid.Get_Fields_Boolean("NoFeeDupReg"))
						{
							AddMasterInventoryAdjustments(rsVoid.Get_Fields_Int32("NoFeeMVR3Number"), txtReason.Text);
						}
					}

					rsVoid.MoveLast();
					rsVoid.MoveFirst();
					TransferTag:
					;
					if (FCConvert.ToString(rsVoid.Get_Fields_String("Status")) == "T")
					{
						if (VoidMVR3 == 0)
						{
							rsVoid.Delete();
							rsVoid.Update();
							rsVoid.MoveNext();
						}
						else
						{
							rsVoid.Edit();
							if (rsVoid.Get_Fields("ETOFlag") == false)
								rsVoid.Set_Fields("Status", "A");
							else
								rsVoid.Set_Fields("Status", "P");
							rsVoid.Set_Fields("MVR3", rsVoid.Get_Fields_Int32("OldMVR3"));
							rsVoid.Set_Fields("OldMVR3", 0);
							rsVoid.Update();
							rsVoid.MoveNext();
						}
					}

					if (MVR3Issued)
					{
						rsVoid.Edit();
						rsVoid.Set_Fields("MVR3", VoidMVR3);
						rsVoid.Set_Fields("OldMVR3", 0);
					}

					// kk06032016 tromv-1158  Consolidate void/unissue month sticker code
					if (Conversion.Val(rsVoid.Get_Fields_Int32("MonthStickerNumber")) != 0)
					{
						Process_Void_Month_Sticker(FCConvert.CBool(chkMonth.CheckState == Wisej.Web.CheckState.Checked && chkMonth.Visible));
						// chkMonth:  1 = Return to Inventory, 0 = Void
					}
					if (chkMonth.CheckState == Wisej.Web.CheckState.Checked && chkMonth.Visible)
					{
						rsVoid.Set_Fields("MonthStickerMonth", 0);
						rsVoid.Set_Fields("MonthStickerCharge", 0);
						rsVoid.Set_Fields("MonthStickerNoCharge", 0);
						rsVoid.Set_Fields("MonthStickerNumber", 0);
					}
					// kk06032016 tromv-1158  Consolidate void/unissue year sticker code
					if (Conversion.Val(rsVoid.Get_Fields_Int32("YearStickerNumber")) != 0)
					{
						Process_Void_Year_Sticker(FCConvert.CBool(chkYear.CheckState == Wisej.Web.CheckState.Checked));
						// chkYear:  1 = Return to Inventory, 0 = Void
					}
					if (chkYear.CheckState == Wisej.Web.CheckState.Checked)
					{
						rsVoid.Set_Fields("YearStickerYear", 0);
						rsVoid.Set_Fields("YearStickerCharge", 0);
						rsVoid.Set_Fields("YearStickerNoCharge", 0);
						rsVoid.Set_Fields("YearStickerNumber", 0);
					}
					strHoldPlate = FCConvert.ToString(rsVoid.Get_Fields_String("plate"));
					strHoldClass = FCConvert.ToString(rsVoid.Get_Fields_String("Class"));
					lngHeldID = FCConvert.ToInt32(rsVoid.Get_Fields_Int32("ID"));
					rsVoid.Update();
					rsVoid.FindFirstRecord("ID", lngHeldID);
					if (chkPlate.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (FCConvert.ToString(rsVoid.Get_Fields_String("plate")) != "")
							UnIssue_Plate_From_Inventory();
					}
					else
					{
						string tempPre = "";
						string tempSuf = "";
						string tempNumber = "";
						// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
						int counter;
						bool sufflag = false;
						string tempplate = "";
						sufflag = false;
						tempPre = "";
						tempSuf = "";
						tempNumber = "";
						tempplate = FCConvert.ToString(rsVoid.Get_Fields_String("plate")).Replace("-", "");
						if (tempplate != "NEW")
						{
							for (counter = 0; counter <= (tempplate.Length - 1); counter++)
							{
								if (!Information.IsNumeric(Strings.Left(tempplate, 1)))
								{
									if (!sufflag)
									{
										tempPre += Strings.Left(tempplate, 1);
									}
									else
									{
										tempSuf = tempplate;
										break;
									}
								}
								else
								{
									sufflag = true;
									tempNumber += Strings.Left(tempplate, 1);
								}
								tempplate = Strings.Right(tempplate, tempplate.Length - 1);
							}
						}
						if (tempplate != "NEW")
						{
							// kk11292016 tromv-1191  Plate voided when it wasn't issued with the voided mvr (duplicate, correction, etc), add mvr# to query
							rsMO.OpenRecordset("SELECT * FROM Inventory WHERE Status = 'I' AND Prefix = '" + tempPre + "' AND Suffix = '" + tempSuf + "' AND Number = " + FCConvert.ToString(Conversion.Val(tempNumber)) + " AND MVR3 = " + FCConvert.ToString(ArchiveMVR3));
							if (rsMO.EndOfFile() != true && rsMO.BeginningOfFile() != true)
							{
								rsMO.MoveLast();
								rsMO.MoveFirst();
								rsMO.Edit();
								rsMO.Set_Fields("Status", "V");
								rsMO.Set_Fields("InventoryDate", DateTime.Today);
								rsMO.Set_Fields("PeriodCloseoutID", 0);
								rsMO.Set_Fields("TellerCloseoutID", 0);
								rsMO.Update();
								// kk09012016 tromv-1191  Moved from above - Only add the adjustment if the plate was issued with this mvr
								rsMO.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'I' AND Low = '" + rsVoid.Get_Fields_String("plate") + "'");
								if (rsVoid.EndOfFile() != true && rsVoid.BeginningOfFile() != true)
								{
									rsMO.MoveLast();
									rsMO.MoveFirst();
									rsMO.Edit();
									rsMO.Set_Fields("AdjustmentCode", "A");
									rsMO.Update();
								}
							}
						}
					}
					if (rsVoid.EndOfFile() != true)
					{
						if (ArchiveMVR3 == 0)
						{
							rsVoid.Delete();
							rsVoid.Update();
						}
						rsVoid.MoveNext();
						if (rsVoid.EndOfFile() != true)
							goto TransferTag;
					}
					if (ArchiveMVR3 == 0)
					{
						if (rsVoid.EndOfFile() != true)
						{
							rsVoid.Delete();
							rsVoid.Update();
						}
					}
					else
					{
						UnTerminate_Master_Record();
					}
				}
				if (UnIssued == true)
				{
					clsDRWrapper rsVD = new clsDRWrapper();
					// kk05032017 tromv-1281 Rework void unissued mvr
					strSQL = "SELECT * FROM Inventory WHERE Code = 'MXS00' AND Number = " + FCConvert.ToString(Conversion.Val(txtVoidedMVR3.Text)) + " AND Status = 'A'";
					rsVD.OpenRecordset(strSQL);
					if (rsVD.EndOfFile() != true && rsVD.BeginningOfFile() != true)
					{
						rsVD.Edit();
						rsVD.Set_Fields("Status", "V");
						rsVD.Set_Fields("InventoryDate", DateTime.Today);
						rsVD.Set_Fields("PeriodCloseoutID", 0);
						rsVD.Set_Fields("TellerCloseoutID", 0);
						rsVD.Update();
						//App.DoEvents();
						rsVD.Reset();
						rsAM.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
						rsAM.AddNew();
						rsAM.Set_Fields("DateOfAdjustment", DateTime.Today);
						rsAM.Set_Fields("High", txtVoidedMVR3.Text);
						rsAM.Set_Fields("Low", txtVoidedMVR3.Text);
						rsAM.Set_Fields("OpID", MotorVehicle.Statics.OpID);
						rsAM.Set_Fields("reason", txtReason.Text);
						rsAM.Set_Fields("PeriodCloseoutID", 0);
						rsAM.Set_Fields("TellerCloseoutID", 0);
						rsAM.Set_Fields("QuantityAdjusted", 1);
						rsAM.Set_Fields("InventoryType", "MXS00");
						rsAM.Set_Fields("AdjustmentCode", "V");
						rsAM.Update();
						rsAM.Reset();
					}
					strHoldPlate = "UnIssued";
					strHoldClass = "UnIssued";
				}
				return Process_Routine;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				fecherFoundation.Information.Err(ex).Clear();
				Process_Routine = false;
			}
			return Process_Routine;
		}
		// kk06032016 tromv-1158  Changed this Sub to process return to inventory and void
		// Private Sub UnIssue_Month_Sticker_From_Inventory()
		private void Process_Void_Month_Sticker(bool boolReturnToInventory)
		{
			clsDRWrapper rsMO = new clsDRWrapper();
			string strCode;
			strCode = "SM";
			if (Conversion.Val(rsVoid.Get_Fields_Int32("MonthStickerCharge")) + Conversion.Val(rsVoid.Get_Fields_Int32("MonthStickerNoCharge")) == 2)
			{
				strCode += "D";
			}
			else
			{
				strCode += "S";
			}
			strCode += Strings.Format(rsVoid.Get_Fields_DateTime("ExpireDate").Month, "00");
			// kk11292016 tromv-1191  Sticker voided when it wasn't issued with the voided mvr (duplicate, correction, etc), add mvr# to query
			rsMO.OpenRecordset("SELECT * FROM Inventory WHERE Status = 'I' AND Code = '" + strCode + "' AND Number = " + rsVoid.Get_Fields_Int32("MonthStickerNumber") + " AND MVR3 = " + FCConvert.ToString(ArchiveMVR3));
			if (rsMO.EndOfFile() != true && rsMO.BeginningOfFile() != true)
			{
				rsMO.MoveLast();
				rsMO.MoveFirst();
				if (boolReturnToInventory)
				{
					rsMO.Edit();
					rsMO.Set_Fields("Status", "A");
					// Make the sticker available again
					rsMO.Set_Fields("InventoryDate", DateTime.Today);
					rsMO.Set_Fields("TellerCloseoutID", 1);
					rsMO.Update();
				}
				else
				{
					rsMO.Edit();
					rsMO.Set_Fields("Status", "V");
					// Mark the sticker as voided
					rsMO.Set_Fields("InventoryDate", DateTime.Today);
					rsMO.Set_Fields("PeriodCloseoutID", 0);
					rsMO.Set_Fields("TellerCloseoutID", 0);
					rsMO.Update();
				}
				// kk11292016 tromv-1191  Only add the adjustment if the sticker was found in inventory
				// End If
				rsMO.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strCode + "' AND AdjustmentCode = 'I' AND Low = '" + rsVoid.Get_Fields_Int32("MonthStickerNumber") + "'");
				if (rsMO.EndOfFile() != true && rsMO.BeginningOfFile() != true)
				{
					rsMO.MoveLast();
					rsMO.MoveFirst();
					if (boolReturnToInventory)
					{
						rsMO.Delete();
						// Delete the Issued InventoryAdjustment record like it never happened
						rsMO.Update();
					}
					else
					{
						rsMO.Edit();
						rsMO.Set_Fields("AdjustmentCode", "A");
						// Change the Issued InventoryAdjustment record to Adjusted
						rsMO.Update();
					}
				}
			}
		}
		// kk06032016 tromv-1158  Changed this Sub to process return to inventory and void
		// Private Sub UnIssue_Year_Sticker_From_Inventory()
		private void Process_Void_Year_Sticker(bool boolReturnToInventory)
		{
			clsDRWrapper rsYR = new clsDRWrapper();
			string strCode = "";
			if (MotorVehicle.IsMotorcycle(rsVoid.Get_Fields_String("Class")))
			{
				strCode = "SYC";
			}
			else
			{
				strCode = "SY";
				if (Conversion.Val(rsVoid.Get_Fields_Int32("YearStickerCharge")) + Conversion.Val(rsVoid.Get_Fields_Int32("YearStickerNoCharge")) == 2)
				{
					strCode += "D";
				}
				else
				{
					strCode += "S";
				}
			}
			strCode += Strings.Right(FCConvert.ToString(rsVoid.Get_Fields_DateTime("ExpireDate")), 2);
			// kk11292016 tromv-1191  Sticker voided when it wasn't issued with the voided mvr (duplicate, correction, etc), add mvr# to query
			rsYR.OpenRecordset("SELECT * FROM Inventory WHERE Status = 'I' AND Code = '" + strCode + "' AND Number = " + rsVoid.Get_Fields_Int32("YearStickerNumber") + " AND MVR3 = " + FCConvert.ToString(ArchiveMVR3));
			if (rsYR.EndOfFile() != true && rsYR.BeginningOfFile() != true)
			{
				rsYR.MoveLast();
				rsYR.MoveFirst();
				if (boolReturnToInventory)
				{
					rsYR.Edit();
					rsYR.Set_Fields("Status", "A");
					// Make the sticker available again
					rsYR.Set_Fields("InventoryDate", DateTime.Today);
					rsYR.Set_Fields("TellerCloseoutID", 1);
					rsYR.Update();
				}
				else
				{
					rsYR.Edit();
					rsYR.Set_Fields("Status", "V");
					// Mark the sticker as voided
					rsYR.Set_Fields("InventoryDate", DateTime.Today);
					rsYR.Set_Fields("PeriodCloseoutID", 0);
					rsYR.Set_Fields("TellerCloseoutID", 0);
					rsYR.Update();
				}
				// kk11292016 tromv-1191  Only add the adjustment if the sticker was found in inventory
				// End If
				rsYR.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + strCode + "' AND AdjustmentCode = 'I' AND Low = '" + rsVoid.Get_Fields_Int32("YearStickerNumber") + "'");
				if (rsYR.EndOfFile() != true && rsYR.BeginningOfFile() != true)
				{
					rsYR.MoveLast();
					rsYR.MoveFirst();
					if (boolReturnToInventory)
					{
						rsYR.Delete();
						// Delete the Issued InventoryAdjustment record like it never happened
						rsYR.Update();
					}
					else
					{
						rsYR.Edit();
						rsYR.Set_Fields("AdjustmentCode", "A");
						// Change the Issued InventoryAdjustment record to Adjusted
						rsYR.Update();
					}
				}
			}
		}

		private void UnIssue_Plate_From_Inventory()
		{
			clsDRWrapper rsPL = new clsDRWrapper();
			// kk11292016 tromv-1191  Plate voided when it wasn't issued with the voided mvr (duplicate, correction, etc), add mvr# to query
			// kk11292016 tromv-1217  No MVR3# for LTT
			if (!blnMVRT10Void)
			{
				rsPL.OpenRecordset("SELECT * FROM Inventory WHERE Status = 'I' AND FormattedInventory = '" + rsVoid.Get_Fields_String("plate") + "' AND MVR3 = " + FCConvert.ToString(ArchiveMVR3));
			}
			else
			{
				rsPL.OpenRecordset("SELECT * FROM Inventory WHERE Status = 'I' AND FormattedInventory = '" + rsVoid.Get_Fields_String("plate") + "'");
			}
			if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
			{
				rsPL.MoveLast();
				rsPL.MoveFirst();
				rsPL.Edit();
				rsPL.Set_Fields("Status", "A");
				rsPL.Set_Fields("InventoryDate", DateTime.Today);
				rsPL.Set_Fields("TellerCloseoutID", 1);
				rsPL.Set_Fields("Pending", false);
				rsPL.Update();
				// kk09012016 tromv-1191  Only add the adjustment if the plate was found in inventory
				// End If
				rsPL.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'I' AND Low = '" + rsVoid.Get_Fields_String("plate") + "'");
				if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
				{
					rsPL.MoveLast();
					rsPL.MoveFirst();
					rsPL.Delete();
					rsPL.Update();
				}
			}
		}

		private void UnTerminate_Master_Record()
		{
			clsDRWrapper rsTerminated = new clsDRWrapper();
			clsDRWrapper rsArchive = new clsDRWrapper();
			DateTime dt1;
			DateTime dt2;
			//DAO.Field ff = new DAO.Field();
			rsArchive.OpenRecordset("SELECT TOP 1 * FROM ArchiveMaster WHERE MVR3 = " + FCConvert.ToString(ArchiveMVR3) + " ORDER BY ID DESC");
			//App.DoEvents();
			rsVoid.Reset();
			if (rsArchive.EndOfFile() != true && rsArchive.BeginningOfFile() != true)
			{
				rsTerminated.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(lngHeldID) + " AND (Status = 'A' OR Status = 'P')");
				if (rsTerminated.EndOfFile() != true && rsTerminated.BeginningOfFile() != true)
				{
					rsTerminated.Edit();
				}
				else
				{
					rsTerminated.AddNew();
				}
				for (MotorVehicle.Statics.fnx = 1; MotorVehicle.Statics.fnx <= rsArchive.FieldsCount - 1; MotorVehicle.Statics.fnx++)
				{
					rsTerminated.Set_Fields(rsArchive.Get_FieldsIndexName(MotorVehicle.Statics.fnx), rsArchive.Get_FieldsIndexValue(MotorVehicle.Statics.fnx));
				}
				if (MVR3Issued)
				{
					rsTerminated.Set_Fields("MVR3", rsTerminated.Get_Fields_Int32("OldMVR3"));
					rsTerminated.Set_Fields("OldMVR3", 0);
				}
				if (rsTerminated.Update(true))
				{
					rsArchive.Delete();
					rsArchive.Update();
				}
			}
			else
			{
				rsTerminated.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(lngHeldID) + " AND (Status = 'A' OR Status = 'P')");
				if (rsTerminated.EndOfFile() != true && rsTerminated.BeginningOfFile() != true)
				{
					rsTerminated.Delete();
					rsTerminated.Update();
				}
			}
		}

		private void UnTerminate_Master_Record_MVRT10()
		{
			clsDRWrapper rsTerminated = new clsDRWrapper();
			clsDRWrapper rsArchive = new clsDRWrapper();
			DateTime dt1;
			DateTime dt2;
			rsArchive.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(ArchiveMVR3) + " ORDER BY ID DESC");
			rsTerminated.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(lngHeldID) + " AND (Status = 'A' OR Status = 'P')");
			//App.DoEvents();
			// rsVoid.Close
			rsVoid.Reset();
			if (rsTerminated.EndOfFile() != true && rsTerminated.BeginningOfFile() != true)
			{
				// rsTerminated.MoveLast
				// rsTerminated.MoveFirst
				// dt1 = Date
				// dt2 = Date
				if (rsArchive.EndOfFile() != true && rsArchive.BeginningOfFile() != true)
				{
					// rsArchive.MoveLast
					// rsArchive.MoveFirst
					// dt1 = rsArchive.Fields("ExpireDate")
					// dt2 = rsArchive.Fields("EffectiveDate")
				}
				else
				{
					rsTerminated.Delete();
					rsTerminated.Update();
					return;
				}
				//DAO.Field ff = new DAO.Field();
				rsTerminated.Edit();
				for (MotorVehicle.Statics.fnx = 1; MotorVehicle.Statics.fnx <= rsArchive.FieldsCount - 1; MotorVehicle.Statics.fnx++)
				{
					rsTerminated.Set_Fields(rsArchive.Get_FieldsIndexName(MotorVehicle.Statics.fnx), rsArchive.Get_FieldsIndexValue(MotorVehicle.Statics.fnx));
				}
				rsTerminated.Update();
				rsArchive.Delete();
				rsArchive.Update();
			}
		}

		private void txtVoidedMVR3_Leave(object sender, System.EventArgs e)
		{
			int answer;
			if (this.ActiveControl.GetName() != "cmdQuit" && this.ActiveControl.GetName() != "optMVRT10")
			{
				// kk01062017 tromv-1251/1256  Move code to separate proc
				ValidateVoidMVR3();
				// If Val(txtVoidedMVR3.Text) <> 0 Then
				// strSQL = "SELECT * FROM ActivityMaster WHERE MVR3 = " & Val(txtVoidedMVR3.Text) & " ORDER BY Status DESC"
				// rsVoid.OpenRecordset strSQL
				// If rsVoid.EndOfFile <> True And rsVoid.BeginningOfFile <> True Then
				// rsVoid.MoveLast
				// rsVoid.MoveFirst
				// If rsVoid.Fields("OldMVR3") <> 0 Then
				// answer = MsgBox("The MVR3 you are trying to void is from a previous period.  If possible you should perform an E-Correct to fix the problem.  Do you wish to continue?", vbYesNo + vbQuestion, "Previous Period")
				// If answer = vbNo Then
				// Exit Sub
				// Else
				// PreviousPeriod = True
				// End If
				// MsgBox "You may not void this MVR3 because it is from a previous period.  You should perform an E-Correct to fix the problem.", vbInformation, "Previous Period"
				// Exit Sub
				// Else
				// PreviousPeriod = False
				// End If
				// End If
				// Call Get_Master_Record
				// End If
				if (cmdYes.Visible == true)
				{
					cmdYes.Focus();
				}
				else
				{
					//cmdQuit.Focus();
				}
			}
		}

		private void SetCustomFormColors()
		{
			// Label2.ForeColor = &HFF0000
		}

		private void Write_PDS_Work_Record_Stuff_Void()
		{
			FCFixedString strRec3 = new FCFixedString(241);
			FCFixedString strRec5A = new FCFixedString(241);
			clsDRWrapper rsCheck = new clsDRWrapper();
			bool AddToExcise = false;
			string strM = "";
			string strY = "";
			bool blnTest;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			clsDRWrapper rsResCheck = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			blnTest = modRegistry.GetKeyValues2(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Test", "");
			rsCheck.OpenRecordset("SELECT * FROM DefaultInfo");
			if (FCConvert.ToString(rsCheck.Get_Fields_String("ExciseVSAgent")) == "A")
			{
				AddToExcise = false;
			}
			else
			{
				AddToExcise = true;
			}
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				if (modRegionalTown.IsRegionalTown())
				{
					rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Strings.Format(rsAM.Get_Fields_String("ResidenceCode"), "00000") + "'", "CentralData");
					if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", rsResCheck.Get_Fields("TownNumber"));
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
					}
				}
				else
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
				}
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ProcessReceipt", "Y");
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "DPR" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "ECO" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "LPS" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "DPS")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
				}
				else
				{
					if (MotorVehicle.Statics.ExciseAP)
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
					}
					else
					{
						if (AddToExcise)
						{
							if (FCConvert.ToBoolean(rsAM.Get_Fields_Boolean("EAP")))
							{
								modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
							}
							else
							{
								if (rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed") < 0)
								{
									modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(rsAM.Get_Fields_Decimal("ExciseTransferCharge") * -1, "0.00"));
								}
								else
								{
									if (modRegionalTown.IsRegionalTown())
									{
										rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Strings.Format(rsAM.Get_Fields_String("ResidenceCode"), "00000") + "'", "CentralData");
										if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
										{
											modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(((rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed")) + rsAM.Get_Fields_Decimal("ExciseTransferCharge")) * -1, "0.00"));
										}
										else
										{
											modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StateExcise", Strings.Format(((rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed")) + rsAM.Get_Fields_Decimal("ExciseTransferCharge")) * -1, "0.00"));
										}
									}
									else
									{
										if (Strings.Format(rsAM.Get_Fields_String("ResidenceCode"), "00000") != FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ResidenceCode")))
										{
											modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StateExcise", Strings.Format(((rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed")) + rsAM.Get_Fields_Decimal("ExciseTransferCharge")) * -1, "0.00"));
										}
										else
										{
											modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(((rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed")) + rsAM.Get_Fields_Decimal("ExciseTransferCharge")) * -1, "0.00"));
										}
									}
								}
							}
						}
						else
						{
							if ((rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed") < 0) || rsAM.Get_Fields_Boolean("EAP"))
							{
								modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
							}
							else
							{
								if (modRegionalTown.IsRegionalTown())
								{
									rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Strings.Format(rsAM.Get_Fields_String("ResidenceCode"), "00000") + "'", "CentralData");
									if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
									{
										modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format((rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed")) * -1, "0.00"));
									}
									else
									{
										modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StateExcise", Strings.Format((rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed")) * -1, "0.00"));
									}
								}
								else
								{
									if (Strings.Format(rsAM.Get_Fields_String("ResidenceCode"), "00000") != FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ResidenceCode")))
									{
										modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StateExcise", Strings.Format((rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed")) * -1, "0.00"));
									}
									else
									{
										modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format((rsAM.Get_Fields_Decimal("ExciseTaxCharged") - rsAM.Get_Fields_Decimal("ExciseCreditUsed")) * -1, "0.00"));
									}
								}
							}
						}
					}
				}
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "DPR")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsAM.Get_Fields_Decimal("DuplicateRegistrationFee") * -1, "0.00"));
				}
				else if (rsAM.Get_Fields("TransactionType") == "DPS")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsAM.Get_Fields_Decimal("StickerFee") * -1, "0.00"));
				}
				else if (rsAM.Get_Fields("TransactionType") == "ECO")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(rsAM.Get_Fields("StatePaid") * -1, "0.00"));
				}
				else if (rsAM.Get_Fields("TransactionType") == "LPS")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format((rsAM.Get_Fields_Decimal("ReplacementFee") + rsAM.Get_Fields_Decimal("StickerFee")) * -1, "0.00"));
				}
				else if (rsAM.Get_Fields_Boolean("ETO"))
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(0, "0.00"));
				}
				else
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format((rsAM.Get_Fields("StatePaid") - rsAM.Get_Fields("SalesTax") - rsAM.Get_Fields("TitleFee")) * -1, "0.00"));
				}
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "DPR")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsAM.Get_Fields("AgentFee") * -1, "0.00"));
				}
				else if (rsAM.Get_Fields("TransactionType") == "ECO")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(0, "0.00"));
				}
				else if (rsAM.Get_Fields("TransactionType") == "LPS")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(0, "0.00"));
				}
				else if (rsAM.Get_Fields("TransactionType") == "DPS")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(0, "0.00"));
				}
				else
				{
					if (AddToExcise)
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(rsAM.Get_Fields("AgentFee") * -1, "0.00"));
					}
					else
					{
						if (FCConvert.ToBoolean(rsAM.Get_Fields_Boolean("EAP")))
						{
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(FCConvert.ToInt32(rsAM.Get_Fields("AgentFee")) * -1, "0.00"));
						}
						else
						{
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format((rsAM.Get_Fields("AgentFee") + rsAM.Get_Fields_Decimal("ExciseTransferCharge")) * -1, "0.00"));
						}
					}
				}
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "DPR" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "LPS" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "DPS")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(0, "0.00"));
				}
				else
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(rsAM.Get_Fields("SalesTax") * -1, "0.00"));
				}
				if (FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "DPR" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "LPS" || FCConvert.ToString(rsAM.Get_Fields("TransactionType")) == "DPS")
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(0, "0.00"));
				}
				else
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(rsAM.Get_Fields("TitleFee") * -1, "0.00"));
				}
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsAM.Get_Fields_Int32("PartyID1"), true)));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", FCConvert.ToString(rsAM.Get_Fields_Int32("PartyID1")));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", rsAM.Get_Fields_String("plate"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", FCConvert.ToString(rsAM.Get_Fields_Int32("MVR3")));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", FCConvert.ToString(rsAM.Get_Fields("TransactionType")));
				// SaveKeyValue HKEY_CURRENT_USER, REGISTRYKEY & "CR\MV\", "ID", TransactionID
				if (MotorVehicle.Statics.correlationId != new Guid())
				{
					MotorVehicle.CompleteMVTransaction();
				}
			}
		}

		private void Write_PDS_Work_Record_Stuff_Void_MVRT10()
		{
			FCFixedString strRec3 = new FCFixedString(241);
			FCFixedString strRec5A = new FCFixedString(241);
			clsDRWrapper rsCheck = new clsDRWrapper();
			string strM = "";
			string strY = "";
			bool blnTest;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			clsDRWrapper rsResCheck = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			rsCheck.OpenRecordset("SELECT * FROM DefaultInfo");
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				if (modRegionalTown.IsRegionalTown())
				{
					rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + Strings.Format(rsAM.Get_Fields_String("ResidenceCode"), "00000") + "'", "CentralData");
					if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", rsResCheck.Get_Fields("TownNumber"));
					}
					else
					{
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
					}
				}
				else
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
				}
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ProcessReceipt", "Y");
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(0, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format((rsAM.Get_Fields_Double("StateFee") - rsAM.Get_Fields("SalesTax") - rsAM.Get_Fields("TitleFee")) * -1, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format((rsAM.Get_Fields("AgentFeeReg") + rsAM.Get_Fields_Decimal("AgentFeeCTA")) * -1, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(rsAM.Get_Fields("SalesTax") * -1, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(rsAM.Get_Fields("TitleFee") * -1, "0.00"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner", fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsAM.Get_Fields_Int32("PartyID1"))));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID", FCConvert.ToString(rsAM.Get_Fields_Int32("PartyID1")));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate", rsAM.Get_Fields_String("plate"));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "MVR3", FCConvert.ToString(0));
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Type", rsAM.Get_Fields_String("RegistrationType"));

				if (MotorVehicle.Statics.correlationId != new Guid())
				{
					MotorVehicle.CompleteMVTransaction();
				}
			}
		}

		private void vsMVRT10_DblClick(object sender, System.EventArgs e)
		{
			cmdSelect_Click();
		}

		private void FillMVRT10Grid()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM Master INNER JOIN LongTermTrailerRegistrations ON Master.LongTermTrailerRegKey = LongTermTrailerRegistrations.ID WHERE LongTermTrailerRegKey <> 0 AND LongTermTrailerRegistrations.PeriodCloseoutID = 0 AND Master.ID NOT IN (SELECT ArchiveID FROM PendingActivityMaster) ORDER BY Master.DateUpdated DESC, Master.TimeUpdated DESC");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsMVRT10.Rows += 1;
					vsMVRT10.TextMatrix(vsMVRT10.Rows - 1, IDCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					vsMVRT10.TextMatrix(vsMVRT10.Rows - 1, TransactionDateCol, Strings.Format(rsInfo.Get_Fields_DateTime("DateUpdated"), "MM/dd/yy"));
					vsMVRT10.TextMatrix(vsMVRT10.Rows - 1, NameCol, FCConvert.ToString(rsInfo.Get_Fields("Owner1")));
					vsMVRT10.TextMatrix(vsMVRT10.Rows - 1, YearCol, FCConvert.ToString(rsInfo.Get_Fields("Year")));
					vsMVRT10.TextMatrix(vsMVRT10.Rows - 1, MakeCol, FCConvert.ToString(rsInfo.Get_Fields_String("Make")));
					vsMVRT10.TextMatrix(vsMVRT10.Rows - 1, VINCol, FCConvert.ToString(rsInfo.Get_Fields_String("VIN")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private bool Process_Routine_MVRT10()
		{
			bool Process_Routine_MVRT10 = false;
			clsDRWrapper rsOldInfo = new clsDRWrapper();
			bool Deletion;
			clsDRWrapper rsMO = new clsDRWrapper();
			bool blnPrompt = false;
			bool blnOK2Void = false;
			Process_Routine_MVRT10 = true;
			try
			{
				// On Error GoTo ErrorTag1
				fecherFoundation.Information.Err().Clear();
				if (UnIssued == false)
				{
					blnRRTrans = false;
					ArchiveMVR3 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsVoid.Get_Fields_Int32("ArchiveID"))));
					lngHeldID = FCConvert.ToInt32(rsVoid.Get_Fields_Int32("ID"));
					if (FCConvert.ToString(rsVoid.Get_Fields("TransactionType")) == "NRR")
					{
						blnPrompt = false;
					}
					else
					{
						blnPrompt = true;
					}
					if (Conversion.Val(rsVoid.Get_Fields_String("CTANumber")) != 0)
					{
						blnOK2Void = true;
						rsAM.OpenRecordset("SELECT * FROM TitleApplications WHERE CTANumber = '" + rsVoid.Get_Fields_String("CTANumber") + "'");
						if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
						{
							// kk09232016  tromv-1217  Check before voiding stuff
							if (blnPrompt)
							{
								if (MessageBox.Show("Should the CTA be voided?", "Process Void", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
								{
									blnOK2Void = false;
								}
							}
							if (blnOK2Void)
							{
								rsAM.MoveLast();
								rsAM.MoveFirst();
								rsAM.Delete();
								rsAM.Update();
							}
						}
						if (FCConvert.ToInt32(rsVoid.Get_Fields_String("DoubleCTANumber")) != 0)
						{
							rsAM.OpenRecordset("SELECT * FROM TitleApplications WHERE CTANumber = '" + rsVoid.Get_Fields_String("DoubleCTANumber") + "'");
							if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
							{
								if (blnOK2Void)
								{
									rsAM.MoveLast();
									rsAM.MoveFirst();
									rsAM.Delete();
									rsAM.Update();
								}
							}
						}
					}
					rsAM.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + rsVoid.Get_Fields_Int32("LongTermTrailerRegID"));
					if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
					{
						rsAM.Edit();
						rsAM.Set_Fields("Status", "V");
						rsAM.Set_Fields("reason", txtReason.Text);
						rsAM.Update();
						if (MotorVehicle.Statics.bolFromWindowsCR)
						{
							Write_PDS_Work_Record_Stuff_Void_MVRT10();
						}
					}
					// 
					if (chkPlate.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (FCConvert.ToString(rsVoid.Get_Fields_String("plate")) != "")
							UnIssue_Plate_From_Inventory();
					}
					else
					{
						// kk09232016  tromv-1217  Check before voiding stuff
						blnOK2Void = true;
						if (blnPrompt)
						{
							if (MessageBox.Show("Should the Plate be voided?", "Process Void", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
							{
								blnOK2Void = false;
							}
						}
						if (blnOK2Void)
						{
							rsMO.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'I' AND Low = '" + rsVoid.Get_Fields_String("plate") + "'");
							if (rsMO.EndOfFile() != true && rsMO.BeginningOfFile() != true)
							{
								rsMO.MoveLast();
								rsMO.MoveFirst();
								rsMO.Edit();
								rsMO.Set_Fields("AdjustmentCode", "A");
								rsMO.Update();
							}
							if (FCConvert.ToString(rsVoid.Get_Fields_String("plate")) != "NEW")
							{
								rsMO.OpenRecordset("SELECT * FROM Inventory WHERE Status = 'I' AND FormattedInventory = '" + rsVoid.Get_Fields_String("plate") + "'");
								if (rsMO.EndOfFile() != true && rsMO.BeginningOfFile() != true)
								{
									rsMO.MoveLast();
									rsMO.MoveFirst();
									rsMO.Edit();
									rsMO.Set_Fields("Status", "V");
									rsMO.Set_Fields("Date", DateTime.Today);
									rsMO.Set_Fields("PeriodCloseoutID", 0);
									rsMO.Set_Fields("TellerCloseoutID", 0);
									rsMO.Set_Fields("Pending", false);
									rsMO.Update();
								}
							}
						}
					}
					rsVoid.MoveLast();
					rsVoid.MoveFirst();
					TransferTag:
					;
					if (rsVoid.EndOfFile() != true)
					{
						if (ArchiveMVR3 == 0)
						{
							rsVoid.Delete();
							rsVoid.Update();
						}
						rsVoid.MoveNext();
						if (rsVoid.EndOfFile() != true)
							goto TransferTag;
					}
					if (ArchiveMVR3 == 0)
					{
						if (rsVoid.EndOfFile() != true)
						{
							rsVoid.Delete();
							rsVoid.Update();
						}
					}
					else
					{
						UnTerminate_Master_Record_MVRT10();
					}
				}
				return Process_Routine_MVRT10;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				fecherFoundation.Information.Err(ex).Clear();
				Process_Routine_MVRT10 = false;
			}
			return Process_Routine_MVRT10;
		}

		private void CheckInventoryIssued(ref bool boolMStkrIssued, ref bool boolYStkrIssued, ref bool boolPltIssued)
		{
			// Set the boolean byref parameters to true to indicate inventory was issued on the selected MVR3
			clsDRWrapper rsChk = new clsDRWrapper();
			string strCode = "";
			bool boolM;
			bool boolY;
			bool boolP;
			boolM = false;
			if (Conversion.Val(rsVoid.Get_Fields_Int32("MonthStickerNumber")) != 0)
			{
				strCode = "SM";
				if (Conversion.Val(rsVoid.Get_Fields_Int32("MonthStickerCharge")) + Conversion.Val(rsVoid.Get_Fields_Int32("MonthStickerNoCharge")) == 2)
				{
					strCode += "D";
				}
				else
				{
					strCode += "S";
				}
				strCode += Strings.Format(rsVoid.Get_Fields_DateTime("ExpireDate").Month, "00");
				rsChk.OpenRecordset("SELECT ID FROM Inventory WHERE Status = 'I' AND Code = '" + strCode + "' AND Number = " + rsVoid.Get_Fields_Int32("MonthStickerNumber") + " AND MVR3 = " + rsVoid.Get_Fields_Int32("MVR3"));
				if (!rsChk.EndOfFile() && !rsChk.BeginningOfFile())
				{
					boolM = true;
				}
			}
			boolY = false;
			if (Conversion.Val(rsVoid.Get_Fields_Int32("YearStickerNumber")) != 0)
			{
				if (MotorVehicle.IsMotorcycle(rsVoid.Get_Fields_String("Class")))
				{
					strCode = "SYC";
				}
				else
				{
					strCode = "SY";
					if (Conversion.Val(rsVoid.Get_Fields_Int32("YearStickerCharge")) + Conversion.Val(rsVoid.Get_Fields_Int32("YearStickerNoCharge")) == 2)
					{
						strCode += "D";
					}
					else
					{
						strCode += "S";
					}
				}
				strCode += Strings.Right(FCConvert.ToString(rsVoid.Get_Fields_DateTime("ExpireDate")), 2);
				rsChk.OpenRecordset("SELECT ID FROM Inventory WHERE Status = 'I' AND Code = '" + strCode + "' AND Number = " + rsVoid.Get_Fields_Int32("YearStickerNumber") + " AND MVR3 = " + rsVoid.Get_Fields_Int32("MVR3"));
				if (!rsChk.EndOfFile() && !rsChk.BeginningOfFile())
				{
					boolY = true;
				}
			}
			boolP = false;
			if (FCConvert.ToString(rsVoid.Get_Fields_String("Plate")) != "" && FCConvert.ToString(rsVoid.Get_Fields_String("Plate")) != "NEW")
			{
				rsChk.OpenRecordset("SELECT ID FROM Inventory WHERE Status = 'I' AND FormattedInventory = '" + rsVoid.Get_Fields_String("plate") + "' AND MVR3 = " + rsVoid.Get_Fields_Int32("MVR3"));
				if (!rsChk.EndOfFile() && !rsChk.BeginningOfFile())
				{
					boolP = true;
				}
			}
			boolMStkrIssued = boolM;
			boolYStkrIssued = boolY;
			boolPltIssued = boolP;
		}

		private void ValidateVoidMVR3()
		{
			bool isVoided = false;
			bool isPrevPeriod = false;
			if (Conversion.Val(txtVoidedMVR3.Text) != 0)
			{
				strSQL = "SELECT * FROM ActivityMaster WHERE MVR3 = " + FCConvert.ToString(Conversion.Val(txtVoidedMVR3.Text)) + " ORDER BY DateUpdated DESC";
				rsVoid.OpenRecordset(strSQL);
				while (!rsVoid.EndOfFile())
				{
					if (rsVoid.Get_Fields_Int32("OldMVR3") != 0 ||
					    rsVoid.Get_Fields_String("Status") == "V")
					{
						isPrevPeriod |= rsVoid.Get_Fields_Int32("OldMVR3") != 0;
						isVoided |= rsVoid.Get_Fields_String("Status") == "V";
						rsVoid.MoveNext();
					}
					else
					{
						isPrevPeriod = false;
						isVoided = false;
						break;
					}
				}

				if (isVoided)
				{
					MessageBox.Show("MVR3 " + txtVoidedMVR3.Text + " has already been voided.", "Invalid MVR3", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else if (isPrevPeriod)
				{
					MessageBox.Show("You may not void this MVR3 because it is from a previous period.  You should perform an E-Correct to fix the problem.", "Previous Period", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MVR3Issued = WasMVR3IssuedForTrx();
					PreviousPeriod = false;
					recIDtoVoid = rsVoid.Get_Fields_Int32("ID");
					Get_Master_Record();
				}
			}
			else
			{
				// kk01062017 tromv-1251/1256 - Reset everything so Process button/menu are disabled until a valid MVR3 is Entered
				MessageBox.Show("Please enter a valid MVR3 number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Frame1.Visible = false;
				cmdProcess.Enabled = false;
				cmdFileProcess.Enabled = false;
				chkMonth.Visible = false;
				chkYear.Visible = false;
				chkPlate.Visible = false;
				txtVoidedMVR3.Text = FCConvert.ToString(Conversion.Val(strSaveMVR3));
			}
		}

		private void cmbMVR3_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbMVR3.Text == "MVR-3")
			{
				optMVR3_CheckedChanged(sender, e);
			}
			else if (cmbMVR3.Text == "MVRT-10")
			{
				optMVRT10_CheckedChanged(sender, e);
			}
		}

        private void frmVoidMVR3_Resize(object sender, EventArgs e)
        {
            //FC:FINAL:BSE #3998 frame should not be centered 
            //fraUnissued.CenterToContainer(this.ClientArea);
        }

        private void AddMasterInventoryAdjustments(int mvr3ToVoid,string reasonText)
        {
            if (mvr3ToVoid < 1)
            {
                return;
            }
            rsAM.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE Low = '" + mvr3ToVoid + "' AND InventoryType = 'MXS00' AND AdjustmentCode = 'I'","MotorVehicle");
            if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
            {
                rsAM.MoveLast();
                rsAM.MoveFirst();
                rsAM.Edit();
                rsAM.Set_Fields("AdjustmentCode", "V");
                rsAM.Set_Fields("PeriodCloseoutID", 0);
                rsAM.Set_Fields("TellerCloseoutID", 0);
                rsAM.Set_Fields("reason", txtReason.Text);
                rsAM.Update();
            }
            else
            {
                rsAM.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
                rsAM.AddNew();
                rsAM.Set_Fields("DateOfAdjustment", DateTime.Today);
                rsAM.Set_Fields("High", mvr3ToVoid);
                rsAM.Set_Fields("Low", mvr3ToVoid);
                rsAM.Set_Fields("OpID", MotorVehicle.Statics.OpID);
                rsAM.Set_Fields("reason", txtReason.Text);
                rsAM.Set_Fields("PeriodCloseoutID", 0);
                rsAM.Set_Fields("TellerCloseoutID", 0);
                rsAM.Set_Fields("QuantityAdjusted", 1);
                rsAM.Set_Fields("InventoryType", "MXS00");
                rsAM.Set_Fields("AdjustmentCode", "V");
                rsAM.Update();
            }
            rsAM.OpenRecordset("SELECT * FROM Inventory WHERE Number = " + mvr3ToVoid+ " AND Code = 'MXS00' AND Status = 'I'","MotorVehicle");
            if (!rsAM.EndOfFile())
            {
                rsAM.Edit();
                rsAM.Set_Fields("Status", "V");
                rsAM.Set_Fields("InventoryDate", DateTime.Today);
                rsAM.Set_Fields("comment", reasonText);
                rsAM.Set_Fields("TellerCloseoutID", 0);
                rsAM.Set_Fields("PeriodCloseoutID", 0);
                rsAM.Update();
            }
            // 
		}

        private bool WasMVR3IssuedForTrx()
        {
			//TODO:  This needs to be changed along with the DPS and LPS transactions
			// DPS and LPS should issue a MVR3 EXCEPT when 1 out of 2 plates, or 1 out of 2 yr/mo stickers is replaced
	        string transactionToVoid = rsVoid.Get_Fields_String("TransactionType");
	        return transactionToVoid != "DPS" && transactionToVoid != "LPS";
        }
	}
}
