﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptNewExceptionReport.
	/// </summary>
	partial class rptNewExceptionReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewExceptionReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtNoInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTSticker = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTMC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTNMC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTMA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTSticker1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTMC1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTMC2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTNMC1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTMA1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTMA2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTPM = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTPM1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTPM2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTDR = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTDR1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStickerFeeTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTSticker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTNMC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTSticker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTNMC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMA1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMA2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTPM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTPM1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTPM2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTDR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTDR1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerFeeTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.SubReport1});
            this.Detail.Height = 0.2395833F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // SubReport1
            // 
            this.SubReport1.CloseBorder = false;
            this.SubReport1.Height = 0.1875F;
            this.SubReport1.Left = 0.0625F;
            this.SubReport1.Name = "SubReport1";
            this.SubReport1.Report = null;
            this.SubReport1.Top = 0.03125F;
            this.SubReport1.Width = 7.375F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtNoInfo,
            this.txtTSticker,
            this.txtTMC,
            this.txtTNMC,
            this.txtTMA,
            this.txtTSticker1,
            this.txtTMC1,
            this.txtTMC2,
            this.txtTNMC1,
            this.txtTMA1,
            this.txtTMA2,
            this.txtTPM,
            this.txtTPM1,
            this.txtTPM2,
            this.txtTDR,
            this.txtTDR1,
            this.txtStickerFeeTotal});
            this.ReportFooter.Height = 1.5F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // txtNoInfo
            // 
            this.txtNoInfo.CanGrow = false;
            this.txtNoInfo.Height = 0.3125F;
            this.txtNoInfo.Left = 2.40625F;
            this.txtNoInfo.Name = "txtNoInfo";
            this.txtNoInfo.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 12pt; font-weight: bold;" +
    " ddo-char-set: 1";
            this.txtNoInfo.Text = null;
            this.txtNoInfo.Top = 0.15625F;
            this.txtNoInfo.Width = 2.6875F;
            // 
            // txtTSticker
            // 
            this.txtTSticker.CanGrow = false;
            this.txtTSticker.Height = 0.1875F;
            this.txtTSticker.Left = 0.1875F;
            this.txtTSticker.Name = "txtTSticker";
            this.txtTSticker.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTSticker.Text = null;
            this.txtTSticker.Top = 0.1875F;
            this.txtTSticker.Width = 3.0625F;
            // 
            // txtTMC
            // 
            this.txtTMC.CanGrow = false;
            this.txtTMC.Height = 0.1875F;
            this.txtTMC.Left = 0.187F;
            this.txtTMC.Name = "txtTMC";
            this.txtTMC.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTMC.Text = null;
            this.txtTMC.Top = 0.375F;
            this.txtTMC.Width = 3.0625F;
            // 
            // txtTNMC
            // 
            this.txtTNMC.CanGrow = false;
            this.txtTNMC.Height = 0.1875F;
            this.txtTNMC.Left = 0.187F;
            this.txtTNMC.Name = "txtTNMC";
            this.txtTNMC.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: left; " +
    "ddo-char-set: 1";
            this.txtTNMC.Text = null;
            this.txtTNMC.Top = 0.5625001F;
            this.txtTNMC.Width = 3.0625F;
            // 
            // txtTMA
            // 
            this.txtTMA.CanGrow = false;
            this.txtTMA.Height = 0.1875F;
            this.txtTMA.Left = 0.187F;
            this.txtTMA.Name = "txtTMA";
            this.txtTMA.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTMA.Text = null;
            this.txtTMA.Top = 0.7500001F;
            this.txtTMA.Width = 3.0625F;
            // 
            // txtTSticker1
            // 
            this.txtTSticker1.CanGrow = false;
            this.txtTSticker1.Height = 0.1875F;
            this.txtTSticker1.Left = 3.3125F;
            this.txtTSticker1.Name = "txtTSticker1";
            this.txtTSticker1.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTSticker1.Text = null;
            this.txtTSticker1.Top = 0.1875F;
            this.txtTSticker1.Width = 0.5625F;
            // 
            // txtTMC1
            // 
            this.txtTMC1.CanGrow = false;
            this.txtTMC1.Height = 0.1875F;
            this.txtTMC1.Left = 3.312F;
            this.txtTMC1.Name = "txtTMC1";
            this.txtTMC1.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTMC1.Text = null;
            this.txtTMC1.Top = 0.375F;
            this.txtTMC1.Width = 0.5625F;
            // 
            // txtTMC2
            // 
            this.txtTMC2.CanGrow = false;
            this.txtTMC2.Height = 0.1875F;
            this.txtTMC2.Left = 4.1245F;
            this.txtTMC2.Name = "txtTMC2";
            this.txtTMC2.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTMC2.Text = null;
            this.txtTMC2.Top = 0.375F;
            this.txtTMC2.Width = 0.6875F;
            // 
            // txtTNMC1
            // 
            this.txtTNMC1.CanGrow = false;
            this.txtTNMC1.Height = 0.1875F;
            this.txtTNMC1.Left = 3.312F;
            this.txtTNMC1.Name = "txtTNMC1";
            this.txtTNMC1.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTNMC1.Text = null;
            this.txtTNMC1.Top = 0.5625001F;
            this.txtTNMC1.Width = 0.5625F;
            // 
            // txtTMA1
            // 
            this.txtTMA1.CanGrow = false;
            this.txtTMA1.Height = 0.1875F;
            this.txtTMA1.Left = 3.312F;
            this.txtTMA1.Name = "txtTMA1";
            this.txtTMA1.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTMA1.Text = null;
            this.txtTMA1.Top = 0.7500001F;
            this.txtTMA1.Width = 0.5625F;
            // 
            // txtTMA2
            // 
            this.txtTMA2.CanGrow = false;
            this.txtTMA2.Height = 0.1875F;
            this.txtTMA2.Left = 4.1245F;
            this.txtTMA2.Name = "txtTMA2";
            this.txtTMA2.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTMA2.Text = null;
            this.txtTMA2.Top = 0.7500001F;
            this.txtTMA2.Width = 0.6875F;
            // 
            // txtTPM
            // 
            this.txtTPM.CanGrow = false;
            this.txtTPM.Height = 0.1875F;
            this.txtTPM.Left = 0.187F;
            this.txtTPM.Name = "txtTPM";
            this.txtTPM.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: left; " +
    "ddo-char-set: 1";
            this.txtTPM.Text = null;
            this.txtTPM.Top = 0.9375002F;
            this.txtTPM.Width = 3.0625F;
            // 
            // txtTPM1
            // 
            this.txtTPM1.CanGrow = false;
            this.txtTPM1.Height = 0.1875F;
            this.txtTPM1.Left = 3.312F;
            this.txtTPM1.Name = "txtTPM1";
            this.txtTPM1.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTPM1.Text = null;
            this.txtTPM1.Top = 0.9375002F;
            this.txtTPM1.Width = 0.5625F;
            // 
            // txtTPM2
            // 
            this.txtTPM2.CanGrow = false;
            this.txtTPM2.Height = 0.1875F;
            this.txtTPM2.Left = 4.1245F;
            this.txtTPM2.Name = "txtTPM2";
            this.txtTPM2.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTPM2.Text = null;
            this.txtTPM2.Top = 0.9375002F;
            this.txtTPM2.Width = 0.6875F;
            // 
            // txtTDR
            // 
            this.txtTDR.CanGrow = false;
            this.txtTDR.Height = 0.1875F;
            this.txtTDR.Left = 0.187F;
            this.txtTDR.Name = "txtTDR";
            this.txtTDR.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; text-align: left; " +
    "ddo-char-set: 1";
            this.txtTDR.Text = null;
            this.txtTDR.Top = 1.125F;
            this.txtTDR.Width = 3.0625F;
            // 
            // txtTDR1
            // 
            this.txtTDR1.CanGrow = false;
            this.txtTDR1.Height = 0.1875F;
            this.txtTDR1.Left = 3.312F;
            this.txtTDR1.Name = "txtTDR1";
            this.txtTDR1.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtTDR1.Text = null;
            this.txtTDR1.Top = 1.125F;
            this.txtTDR1.Width = 0.5625F;
            // 
            // txtStickerFeeTotal
            // 
            this.txtStickerFeeTotal.CanGrow = false;
            this.txtStickerFeeTotal.Height = 0.1875F;
            this.txtStickerFeeTotal.Left = 4.125F;
            this.txtStickerFeeTotal.Name = "txtStickerFeeTotal";
            this.txtStickerFeeTotal.Style = "font-family: \\000027Roman\\00002012cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.txtStickerFeeTotal.Text = null;
            this.txtStickerFeeTotal.Top = 0.1875F;
            this.txtStickerFeeTotal.Width = 0.6875F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.SubReport2,
            this.Label1,
            this.lblPage,
            this.Label33});
            this.GroupHeader1.DataField = "Binder";
            this.GroupHeader1.Height = 1.4375F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // SubReport2
            // 
            this.SubReport2.CloseBorder = false;
            this.SubReport2.Height = 0.9375F;
            this.SubReport2.Left = 0.0625F;
            this.SubReport2.Name = "SubReport2";
            this.SubReport2.Report = null;
            this.SubReport2.Top = 0.5F;
            this.SubReport2.Width = 7.4375F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 2.0625F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \\000027Roman\\00002012cpi\\000027; text-align: center; ddo-char-set: 1" +
    "";
            this.Label1.Text = "**** EXCEPTION REPORT ****";
            this.Label1.Top = 0F;
            this.Label1.Width = 2.5F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.9375F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \\000027Roman\\00002012cpi\\000027; text-align: right; ddo-char-set: 1";
            this.lblPage.Text = "VENDOR ID#";
            this.lblPage.Top = 0.0625F;
            this.lblPage.Width = 0.9375F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.1875F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 2.25F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-family: \\000027Roman\\00002010cpi\\000027; font-size: 10pt; ddo-char-set: 1";
            this.Label33.Text = "BUREAU OF MOTOR VEHICLES";
            this.Label33.Top = 0.1875F;
            this.Label33.Width = 2.3125F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // rptNewExceptionReport
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtNoInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTSticker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTNMC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTSticker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTNMC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMA1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTMA2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTPM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTPM1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTPM2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTDR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTDR1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStickerFeeTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNoInfo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTSticker;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTMC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTNMC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTMA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTSticker1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTMC1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTMC2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTNMC1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTMA1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTMA2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTPM;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTPM1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTPM2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTDR;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTDR1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStickerFeeTotal;
	}
}
