//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptGiftCertVoucher.
	/// </summary>
	public partial class rptGiftCertVoucher : BaseSectionReport
	{
		public rptGiftCertVoucher()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Gift Certificate Register";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptGiftCertVoucher_ReportEnd;
		}

        private void RptGiftCertVoucher_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
			rsAM.DisposeOf();
        }

        public static rptGiftCertVoucher InstancePtr
		{
			get
			{
				return (rptGiftCertVoucher)Sys.GetInstance(typeof(rptGiftCertVoucher));
			}
		}

		protected rptGiftCertVoucher _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptGiftCertVoucher	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL = "";
		public int lngPK1;
		public int lngPK2;
		int intHeadingNumber;
		//clsDRWrapper rs2 = new clsDRWrapper();
		double MoneyTotal;
		int total;
		clsDRWrapper rsAM = new clsDRWrapper();
		bool blnFirstRecord;
		bool boolData;
		int intType;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord == true)
			{
				eArgs.EOF = false;
				blnFirstRecord = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			//Application.DoEvents();
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSQL);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			}
			blnFirstRecord = true;
			boolData = false;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				strSQL = "SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID < 1 AND Issued = 1 ORDER BY CertificateNumber";
			}
			else
			{
				strSQL = "SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " AND Issued = 1 ORDER BY CertificateNumber";
			}
			rsAM.OpenRecordset(strSQL);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				boolData = true;
				subIssued.Report = new srptGiftCertVoucherIssued();
			}
			else
			{
				subIssued.Report = null;
			}
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				strSQL = "SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID < 1 AND Issued <> 1 ORDER BY CertificateNumber";
			}
			else
			{
				strSQL = "SELECT * FROM GiftCertificateRegister WHERE PeriodCloseoutID > " + FCConvert.ToString(lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lngPK2) + " AND Issued <> 1 ORDER BY CertificateNumber";
			}
			rsAM.OpenRecordset(strSQL);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				boolData = true;
				subRedeemed.Report = new srptGiftCertVoucherRedeemed();
			}
			else
			{
				subRedeemed.Report = null;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeading();
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (boolData == false)
			{
				txtNoInfo.Text = "No Information";
			}
		}
	}
}
