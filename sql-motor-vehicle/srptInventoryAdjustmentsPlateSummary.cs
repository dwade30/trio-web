﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for srptInventoryAdjustmentsPlateSummary.
	/// </summary>
	public partial class srptInventoryAdjustmentsPlateSummary : FCSectionReport
	{
		public srptInventoryAdjustmentsPlateSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Inventory Adjustments Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptInventoryAdjustmentsPlateSummary InstancePtr
		{
			get
			{
				return (srptInventoryAdjustmentsPlateSummary)Sys.GetInstance(typeof(srptInventoryAdjustmentsPlateSummary));
			}
		}

		protected srptInventoryAdjustmentsPlateSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptInventoryAdjustmentsPlateSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int counter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				counter += 1;
				if (counter >= MotorVehicle.Statics.intPlateCountCounter)
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			counter = 0;
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (counter == 0)
			{
				lblHeader.Visible = true;
			}
			else
			{
				lblHeader.Visible = false;
			}
			lblPlateClass.Text = "CLASS TYPE " + MotorVehicle.Statics.pcPlateInfo[counter].Class;
			fldPlateCount.Text = Strings.Format(MotorVehicle.Statics.pcPlateInfo[counter].AdjustmentCount, "#,##0");
		}

		
	}
}
