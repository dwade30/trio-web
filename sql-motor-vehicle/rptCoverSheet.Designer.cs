﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptCoverSheet.
	/// </summary>
	partial class rptCoverSheet
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCoverSheet));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblGenerated = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblVersion = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblGenerated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVersion)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblGenerated,
				this.lblVersion,
				this.Label1
			});
			this.Detail.Height = 9.53125F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblGenerated
			// 
			this.lblGenerated.Height = 0.4375F;
			this.lblGenerated.HyperLink = null;
			this.lblGenerated.Left = 1.53125F;
			this.lblGenerated.Name = "lblGenerated";
			this.lblGenerated.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.lblGenerated.Text = null;
			this.lblGenerated.Top = 4F;
			this.lblGenerated.Width = 4.40625F;
			// 
			// lblVersion
			// 
			this.lblVersion.Height = 0.1875F;
			this.lblVersion.HyperLink = null;
			this.lblVersion.Left = 5.0625F;
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Style = "font-family: \'Roman 10cpi\'; text-align: right; ddo-char-set: 1";
			this.lblVersion.Text = "Label1";
			this.lblVersion.Top = 0F;
			this.lblVersion.Width = 2.40625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.260417F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; text-align: center; ddo-char-set: 1";
			this.Label1.Text = "Please include with BMV reports.";
			this.Label1.Top = 7.65625F;
			this.Label1.Width = 2.96875F;
			// 
			// rptCoverSheet
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblGenerated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVersion)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGenerated;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVersion;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
