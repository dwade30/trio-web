//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptReconMVR3.
	/// </summary>
	public partial class rptReconMVR3 : BaseSectionReport
	{
		public rptReconMVR3()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Inventory Reconcilliation Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptReconMVR3_ReportEnd;
		}

        private void RptReconMVR3_ReportEnd(object sender, EventArgs e)
        {
			rs.DisposeOf();
            rs1.DisposeOf();
            rsA.DisposeOf();
            rsB.DisposeOf();
            rsC.DisposeOf();
            rsOnHand.DisposeOf();

		}

        public static rptReconMVR3 InstancePtr
		{
			get
			{
				return (rptReconMVR3)Sys.GetInstance(typeof(rptReconMVR3));
			}
		}

		protected rptReconMVR3 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReconMVR3	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngPK1;
		int lngPK2;
		int lngPK;
		int lngEndPK;
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL = "";
		clsDRWrapper rs1 = new clsDRWrapper();
		clsDRWrapper rsA = new clsDRWrapper();
		clsDRWrapper rsB = new clsDRWrapper();
		clsDRWrapper rsC = new clsDRWrapper();
		clsDRWrapper rsOnHand = new clsDRWrapper();
		bool boolFirstTime;
		int temp;
		int lng1;
		int lng2;
		int lngA;
		int lngB;
		int lngC;
		int lngD;
		int intPageNumber;

		private void DoLoops()
		{
			rsA.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'R' And (PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + ") AND InventoryType = 'MXS00'");
			if (rsA.EndOfFile() != true && rsA.BeginningOfFile() != true)
			{
				rsA.MoveLast();
				rsA.MoveFirst();
				while (!rsA.EndOfFile())
				{
					lngA += rsA.Get_Fields_Int32("QuantityAdjusted");
					rsA.MoveNext();
				}
			}
			rsB.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'I' And (PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + ") AND InventoryType = 'MXS00'");
			if (rsB.EndOfFile() != true && rsB.BeginningOfFile() != true)
			{
				rsB.MoveLast();
				rsB.MoveFirst();
				while (!rsB.EndOfFile())
				{
					lngB += rsB.Get_Fields_Int32("QuantityAdjusted");
					rsB.MoveNext();
				}
			}
			rsC.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'T' And (PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + ") AND InventoryType = 'MXS00'");
			if (rsC.EndOfFile() != true && rsC.BeginningOfFile() != true)
			{
				rsC.MoveLast();
				rsC.MoveFirst();
				while (!rsC.EndOfFile())
				{
					lngC += rsC.Get_Fields_Int32("QuantityAdjusted");
					rsC.MoveNext();
				}
			}
			rsC.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = 'MXS00' AND AdjustmentCode = 'V' AND (PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + ")");
			if (rsC.EndOfFile() != true && rsC.BeginningOfFile() != true)
			{
				rsC.MoveLast();
				rsC.MoveFirst();
				while (!rsC.EndOfFile())
				{
					lngC -= rsC.Get_Fields_Int32("QuantityAdjusted");
					rsC.MoveNext();
				}
			}
			rsC.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = 'MXS00' AND AdjustmentCode = 'A' AND (PeriodCloseoutID > " + FCConvert.ToString(lng1) + " And PeriodCloseoutID <= " + FCConvert.ToString(lng2) + ")");
			if (rsC.EndOfFile() != true && rsC.BeginningOfFile() != true)
			{
				rsC.MoveLast();
				rsC.MoveFirst();
				while (!rsC.EndOfFile())
				{
					lngC -= rsC.Get_Fields_Int32("QuantityAdjusted");
					rsC.MoveNext();
				}
				//if (lngC < 0)
				//{
				//	lngC *= -1;
				//}
			}
			lngD = 0;
			rsOnHand.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE PeriodCloseoutID = " + FCConvert.ToString(lng1) + " AND Type = 'MXS00'");
			if (rsOnHand.EndOfFile() != true && rsOnHand.BeginningOfFile() != true)
			{
				rsOnHand.MoveLast();
				rsOnHand.MoveFirst();
				lngD = 0;
				while (!rsOnHand.EndOfFile())
				{
					lngD += rsOnHand.Get_Fields_Int32("Number");
					rsOnHand.MoveNext();
				}
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				// 
				if (rs1.FindFirstRecord("Type", "MXS00"))
				{
					txtOnHand.Text = Strings.Format(lngD, "@@@@@@@@@@");
					txtRecorded.Text = Strings.Format(lngA, "@@@@@@@@@@");
					txtIssues.Text = Strings.Format(lngB, "@@@@@@@@@@");
					txtAdjusted.Text = Strings.Format(lngC, "@@@@@@@@@@");
					txtEnd.Text = Strings.Format(rs1.Get_Fields_Int32("Number"), "@@@@@@@@@@");
					if (lngD + lngA - lngB + lngC != FCConvert.ToInt32(rs1.Get_Fields_Int32("Number")))
					{
						txtEnd.Text = txtEnd.Text + "    **";
					}
				}
			}
			rs.MoveFirst();
			txtItem.Text = "ITEMIZED LISTING-------------";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!rs.EndOfFile())
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
				return;
			}
			txtOnHand.Text = "";
			txtRecorded.Text = "";
			txtIssues.Text = "";
			txtAdjusted.Text = "";
			txtEnd.Text = "";
			txtItem.Text = "";
			txtItem1.Text = "";
			txtItem2.Text = "";
			txtItem3.Text = "";
			if (boolFirstTime == false)
			{
				DoLoops();
				boolFirstTime = true;
			}

			if (Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) != "M")
			{
				rs.MoveNext();
				return;
			}

			txtItem1.Text = rs.Get_Fields_String("Low");
			// Format(.fields("Low"), "@@@@@@@@")
			txtItem1.Text = txtItem1.Text + "  -  ";
			txtItem1.Text = txtItem1.Text + rs.Get_Fields_String("High");
			// Format(.fields("High"), "!@@@@@@@@")

			rs.MoveNext();
			if (rs.EndOfFile() == true)
				return;

			if (Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) != "M")
			{
				rs.MoveNext();
				return;
			}

			txtItem2.Text = Strings.Format(rs.Get_Fields("Low"), "@@@@@@@@");
			txtItem2.Text = txtItem2.Text + "  -  ";
			txtItem2.Text = txtItem2.Text + Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@");

			rs.MoveNext();
			if (rs.EndOfFile() == true)
				return;

			if (Strings.Mid(FCConvert.ToString(rs.Get_Fields_String("InventoryType")), 1, 1) != "M")
			{
				rs.MoveNext();
				return;
			}

			txtItem3.Text = Strings.Format(rs.Get_Fields("Low"), "@@@@@@@@");
			txtItem3.Text = txtItem3.Text + "  -  ";
			txtItem3.Text = txtItem3.Text + Strings.Format(rs.Get_Fields("High"), "!@@@@@@@@");

			rs.MoveNext();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intPageNumber = 0;
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			//Application.DoEvents();
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							// Dim lngPK1 As Long
							strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				// txtVendorID.Text = "TRIO"
				// txtMuni.Text = .fields("Town
				// txtTownCounty.Text = Format(.fields("ResidenceCode"), "00000")
				// txtAgent.Text = Trim$(.fields("ReportAgent"))
				// txtVersion.Text = Format(App.Major, "00") & "." & App.Minor & "." & App.Revision
				// txtPhone.Text = .fields("ReportTelephone
				// lblPage.Caption = "Page " & rptDiskVerification.PageNumber
				// txtDate.Text = Format(Now, "MM/dd/yyyy")
				// txtProcess.Text = strProcessDate
				// txtAuthType.Text = strLevel
				// txtDateReceived.Text = "___/___/___"
				txtDescTitle.Text = "MVR3--------------------------";
			}
			strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSQL);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				lngPK = lngPK1;
				lng1 = lngPK1;
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				lngEndPK = lngPK2;
				lng2 = lngPK2;
			}
			// 
			strSQL = "SELECT * FROM CloseoutInventory WHERE InventoryType = 'MXS00' AND PeriodCloseoutID = " + FCConvert.ToString(lngPK2) + " ORDER BY InventoryType, Low";
			rs.OpenRecordset(strSQL);
			lngA = 0;
			lngB = 0;
			lngC = 0;
			// With rs
			// If optReport(3).Value = True Then
			// If optAll.Value = False Then
			// If Check1(0) = vbUnchecked Then
			// GoTo CheckSingleStickers
			// End If
			// End If
			// End If
			rs1.OpenRecordset("SELECT * FROM InventoryOnHandAtPeriodCloseout WHERE PeriodCloseoutID = " + FCConvert.ToString(lng2) + " AND Type = 'MXS00'");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (txtItem.Text != "")
			{
				txtItem.Top += 180 / 1440F;
				txtItem1.Top += 180 / 1440F;
				txtItem2.Top += 180 / 1440F;
				txtItem3.Top += 180 / 1440F;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			frmReport.InstancePtr.lngInventoryCounter += 1;
			lblPage.Text = "Page " + FCConvert.ToString(frmReport.InstancePtr.lngInventoryCounter);
			SubReport2.Report = new rptSubReportHeading();
		}

		private void rptReconMVR3_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReconMVR3 properties;
			//rptReconMVR3.Caption	= "Inventory Reconcilliation Report";
			//rptReconMVR3.Icon	= "rptReconMVR3.dsx":0000";
			//rptReconMVR3.Left	= 0;
			//rptReconMVR3.Top	= 0;
			//rptReconMVR3.Width	= 11880;
			//rptReconMVR3.Height	= 8595;
			//rptReconMVR3.StartUpPosition	= 3;
			//rptReconMVR3.SectionData	= "rptReconMVR3.dsx":058A;
			//End Unmaped Properties
		}
	}
}
