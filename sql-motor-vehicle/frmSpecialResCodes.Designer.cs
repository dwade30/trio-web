﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmSpecialResCodes.
	/// </summary>
	partial class frmSpecialResCodes
	{
		public fecherFoundation.FCGrid vsCodes;
		public fecherFoundation.FCButton cmdProcessSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.vsCodes = new fecherFoundation.FCGrid();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 456);
			this.BottomPanel.Size = new System.Drawing.Size(831, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsCodes);
			this.ClientArea.Size = new System.Drawing.Size(831, 396);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(831, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(280, 30);
			this.HeaderText.Text = "Special Res Code Setup";
			// 
			// vsCodes
			// 
			this.vsCodes.AllowSelection = false;
			this.vsCodes.AllowUserToResizeColumns = false;
			this.vsCodes.AllowUserToResizeRows = false;
			this.vsCodes.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
			this.vsCodes.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsCodes.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsCodes.BackColorBkg = System.Drawing.Color.Empty;
			this.vsCodes.BackColorFixed = System.Drawing.Color.Empty;
			this.vsCodes.BackColorSel = System.Drawing.Color.Empty;
			this.vsCodes.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsCodes.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsCodes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsCodes.ColumnHeadersHeight = 30;
			this.vsCodes.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsCodes.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsCodes.DragIcon = null;
			this.vsCodes.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsCodes.ExtendLastCol = true;
			this.vsCodes.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsCodes.FrozenCols = 0;
			this.vsCodes.GridColor = System.Drawing.Color.Empty;
			this.vsCodes.GridColorFixed = System.Drawing.Color.Empty;
			this.vsCodes.Location = new System.Drawing.Point(30, 30);
			this.vsCodes.Name = "vsCodes";
			this.vsCodes.OutlineCol = 0;
			this.vsCodes.ReadOnly = true;
			this.vsCodes.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsCodes.RowHeightMin = 0;
			this.vsCodes.Rows = 9;
			this.vsCodes.ScrollTipText = null;
			this.vsCodes.ShowColumnVisibilityMenu = false;
			this.vsCodes.ShowFocusCell = false;
			this.vsCodes.Size = new System.Drawing.Size(773, 336);
			this.vsCodes.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsCodes.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsCodes.TabIndex = 0;
			this.vsCodes.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsCodes_KeyPressEdit);
			this.vsCodes.CurrentCellChanged += new System.EventHandler(this.vsCodes_RowColChange);
			this.vsCodes.Enter += new System.EventHandler(this.vsCodes_Enter);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(219, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(130, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Save & Exit";
			this.cmdProcessSave.ToolTipText = null;
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmSpecialResCodes
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(831, 564);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSpecialResCodes";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Special Res Code Setup";
			this.Load += new System.EventHandler(this.frmSpecialResCodes_Load);
			this.Activated += new System.EventHandler(this.frmSpecialResCodes_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSpecialResCodes_KeyPress);
			this.Resize += new System.EventHandler(this.frmSpecialResCodes_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
