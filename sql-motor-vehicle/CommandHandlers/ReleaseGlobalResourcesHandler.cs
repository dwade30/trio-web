﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle.Commands;
using TWSharedLibrary.Data;

namespace TWMV0000.CommandHandlers
{
    public class ReleaseGlobalResourcesHandler : CommandHandler<ReleaseGlobalResources>
    {
        private ITrioContextInterfaceFactory trioContextInterfaceFactory;
        public ReleaseGlobalResourcesHandler(ITrioContextInterfaceFactory trioContextInterfaceFactory)
        {
            this.trioContextInterfaceFactory = trioContextInterfaceFactory;
        }

        protected override void Handle(ReleaseGlobalResources command)
        {
            CleanUpTitles();
            CleanUpArchiveMaster();
            
            MotorVehicle.Statics.rsFinal.Dispose();
            MotorVehicle.Statics.rsFinalCompare.Dispose();
            MotorVehicle.Statics.rsPlateForReg.Dispose();
            MotorVehicle.Statics.rsSecondPlate.Dispose();
            MotorVehicle.Statics.rsThirdPlate.Dispose();
            MotorVehicle.Statics.rsDoubleTitle.Dispose();

            // with so many recordsets disposed, let's clear out the memory usage
            var pc = System.Diagnostics.Process.GetCurrentProcess();
            pc.MaxWorkingSet = pc.MinWorkingSet;
        }

        private void CleanUpArchiveMaster()
        {
            using (var archiveRecord = new clsDRWrapper())
            {
                archiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + MotorVehicle.Statics.FinalCompareID, "MotorVehicle");
                if (archiveRecord.EndOfFile() != true && archiveRecord.BeginningOfFile() != true)
                {
                    archiveRecord.Delete();
                    archiveRecord.Update();
                }
            }
        }

        private void CleanUpTitles()
        {
            using (var rsTitle = new clsDRWrapper())
            {
                if (!MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone")
                    || MotorVehicle.Statics.lngCTAAddNewID == 0) return;
                
                rsTitle.OpenRecordset($"SELECT DoubleNumber FROM Title WHERE ID = {MotorVehicle.Statics.lngCTAAddNewID}");
                if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
                {
                    var doubleNumber = rsTitle.Get_Fields_Int32("DoubleNumber");
                    if (doubleNumber != 0)
                    {
                        CleanUpDoubleTitle(doubleNumber);
                    }
                    rsTitle.Delete();
                    rsTitle.Update();
                }
            }
        }
        private void CleanUpDoubleTitle(int doubleNumber)
        {
            MotorVehicle.Statics.rsDoubleTitle.OpenRecordset($"SELECT * FROM DoubleCTA WHERE ID = {doubleNumber}");
            if (MotorVehicle.Statics.rsDoubleTitle.EndOfFile() != true && MotorVehicle.Statics.rsDoubleTitle.BeginningOfFile() != true)
            {
                MotorVehicle.Statics.rsDoubleTitle.Delete();
                MotorVehicle.Statics.rsDoubleTitle.Update();
            }
        }
    }
}
