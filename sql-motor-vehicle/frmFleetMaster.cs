//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	public partial class frmFleetMaster : BaseForm
	{
		public frmFleetMaster()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFleetMaster InstancePtr
		{
			get
			{
				return (frmFleetMaster)Sys.GetInstance(typeof(frmFleetMaster));
			}
		}

		protected frmFleetMaster _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsFleet = new clsDRWrapper();
		clsDRWrapper rsGroup = new clsDRWrapper();
		bool AddingFleet;
		int lngNextFleetNumber;
		bool AddingGroup;
		clsDRWrapper rsVehicleAdd = new clsDRWrapper();
		int OwnerCol;
		int ClassCol;
		int PlateCol;
		int VINCol;
		int YearCol;
		int MakeCol;
		int ModelCol;
		int UnitCol;
		int RegisterCol;
		bool SecondResize;
		bool IsFleet;
		bool blnLoadingResult;
		int GroupFleetCol;
		int FleetNumberCol;
		int Owner1Col;
		int Owner2Col;
		int FleetGroupNumberCol;
		int FleetGroupNameCol;
		bool blnLocked;

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			clsDRWrapper rsUpdateFleet = new clsDRWrapper();
			int UpdateCount = 0;
			if (cmdAdd.Text != "Remove")
			{
				if (MessageBox.Show("Warning - Fees will NOT be prorated." + "\r\n" + "If fees should prorate, please add vehicle to fleet during registration process ONLY.  Do you wish to continue?", "Add Vehicle to Fleet / Group?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				{
					return;
				}
				UpdateCount = 0;
				for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
				{
					if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, RegisterCol)) == true)
					{
						rsUpdateFleet.OpenRecordset("SELECT * FROM Master WHERE ID = " + vsVehicles.TextMatrix(counter, 0));
						rsUpdateFleet.Edit();
						if (fecherFoundation.Strings.Trim(txtEpiryMonth.Text) == "")
						{
							rsUpdateFleet.Set_Fields("FleetNumber", rsGroup.Get_Fields_Int32("FleetNumber"));
						}
						else
						{
							rsUpdateFleet.Set_Fields("FleetNumber", rsFleet.Get_Fields_Int32("FleetNumber"));
						}
						rsUpdateFleet.Set_Fields("FleetNew", true);
						rsUpdateFleet.Update();
						UpdateCount += 1;
					}
				}
				Frame1.Enabled = true;
				vsFleets.Enabled = true;
				fraVehiclesToAdd.Visible = false;
				if (UpdateCount == 1)
				{
					if (txtEpiryMonth.Enabled == false)
					{
						MessageBox.Show(FCConvert.ToString(UpdateCount) + " Vehicle has been added to the group.", "Vehicles Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show(FCConvert.ToString(UpdateCount) + " Vehicle has been added to the fleet.", "Vehicles Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					if (txtEpiryMonth.Enabled == false)
					{
						MessageBox.Show(FCConvert.ToString(UpdateCount) + " Vehicles have been added to the group.", "Vehicles Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show(FCConvert.ToString(UpdateCount) + " Vehicles have been added to the fleet.", "Vehicles Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
			else
			{
				UpdateCount = 0;
				for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
				{
					if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, RegisterCol)) == true)
					{
						rsUpdateFleet.OpenRecordset("SELECT * FROM Master WHERE ID = " + vsVehicles.TextMatrix(counter, 0));
						rsUpdateFleet.Edit();
						rsUpdateFleet.Set_Fields("FleetNumber", 0);
						rsUpdateFleet.Set_Fields("FleetOld", false);
						rsUpdateFleet.Set_Fields("FleetNew", false);
						rsUpdateFleet.Update();
						UpdateCount += 1;
					}
				}
				Frame1.Enabled = true;
				vsFleets.Enabled = true;
				fraVehiclesToAdd.Visible = false;
				if (UpdateCount == 1)
				{
					if (txtEpiryMonth.Enabled == false)
					{
						MessageBox.Show(FCConvert.ToString(UpdateCount) + " Vehicle has been removed from the group.", "Vehicles Removed", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show(FCConvert.ToString(UpdateCount) + " Vehicle has been removed from the fleet.", "Vehicles Removed", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					if (txtEpiryMonth.Enabled == false)
					{
						MessageBox.Show(FCConvert.ToString(UpdateCount) + " Vehicles have been removed from the group.", "Vehicles Removed", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show(FCConvert.ToString(UpdateCount) + " Vehicles have been removed from the fleet.", "Vehicles Removed", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
		}

		private void cmdAddFleet_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUser = new clsDRWrapper();
			Frame1.Enabled = true;
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
				{
					MessageBox.Show("You do not have a high enough Operator Level to do this operation.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to do this operation.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			AddingFleet = true;
			AddingGroup = false;
			Label10.Text = "Fleet Number";
			Frame1.Text = "Fleet Information";
			IsFleet = true;
			Get_Existing_Fleet();
		}

		private void cmdAddGroup_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUser = new clsDRWrapper();
			Frame1.Enabled = true;
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
				{
					MessageBox.Show("You do not have a high enough Operator Level to do this operation.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to do this operation.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			AddingGroup = true;
			AddingFleet = false;
			IsFleet = false;
			Label10.Text = "Group Number";
			Frame1.Text = "Group Information";
			Get_Existing_Fleet();
		}

		private void cmdUpdateEmail_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			if (MessageBox.Show("Update all vehicles in this fleet/group with the current E-mail address?", "Update E-mail?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				rsUpdate.Execute("UPDATE Master SET EMailAddress = '" + fecherFoundation.Strings.Trim(txtEmail.Text) + "' WHERE FleetNumber = " + txtFleetNumber.Text + " AND OverrideFleetEmail <> 1", "TWMV0000.vb1");
				MessageBox.Show("Update completed.", "Update Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdAddVehicles_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUser = new clsDRWrapper();
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
				{
					MessageBox.Show("You do not have a high enough Operator Level to do this operation.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to do this operation.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			Frame1.Enabled = false;
			vsFleets.Enabled = false;
			fraSearch.Visible = true;
			txtCompanyName.Focus();
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			if (cmbOwner.Text == "Company")
			{
				txtCompanyName.Text = "";
			}
			else
			{
				txtFirstName.Text = "";
				txtLastName.Text = "";
				txtFirstName.Visible = false;
				txtLastName.Visible = false;
				lblFirstName.Visible = false;
				lblLastName.Visible = false;
				cmbOwner.Text = "Company";
				txtCompanyName.Visible = true;
				lblCompanyName.Visible = true;
			}
			fraSearch.Visible = false;
			Frame1.Enabled = true;
			vsFleets.Enabled = true;
		}

		private void cmdCancelAdd_Click(object sender, System.EventArgs e)
		{
			Frame1.Enabled = true;
			vsFleets.Enabled = true;
			fraVehiclesToAdd.Visible = false;
		}

		public void cmdCancelAdd_Click()
		{
			cmdCancelAdd_Click(cmdCancelAdd, new System.EventArgs());
		}

		private void cmdCancelFleetSearch_Click(object sender, System.EventArgs e)
		{
			fraSearchFleet.Visible = false;
			txtSearchFleet.Text = "";
		}

		private void cmdRemoveVehicles_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUser = new clsDRWrapper();
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
				{
					MessageBox.Show("You do not have a high enough Operator Level to do this operation.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to do this operation.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			cmdAdd.Text = "Remove";
			rsVehicleAdd.OpenRecordset("SELECT * FROM Master WHERE Status <> 'T' AND FleetNumber = " + txtFleetNumber.Text);
			if (rsVehicleAdd.EndOfFile() != true && rsVehicleAdd.BeginningOfFile() != true)
			{
				rsVehicleAdd.MoveLast();
				rsVehicleAdd.MoveFirst();
				FillFleetData();
				if (vsVehicles.Rows > 1)
				{
					fraSearch.Visible = false;
					if (fecherFoundation.Strings.Trim(txtEpiryMonth.Text) == "")
					{
						lblInstruct.Text = "Please check all the vehicles you wish to remove from the group and click the 'Remove' button";
					}
					else
					{
						lblInstruct.Text = "Please check all the vehicles you wish to remove from the fleet and click the 'Remove' button";
					}
					fraVehiclesToAdd.Text = "Vehicles to Remove";
					fraVehiclesToAdd.Visible = true;
					//App.DoEvents();
					Form_Resize();
					Form_Resize();
				}
				else
				{
					MessageBox.Show("No Vehicles Found", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				MessageBox.Show("No Vehicles Found", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void cmdResultsCancel_Click(object sender, System.EventArgs e)
		{
			fraFleetSearchResults.Visible = false;
			txtSearchFleet.Text = "";
		}

		private void cmdResultsGetInfo_Click(object sender, System.EventArgs e)
		{
			if (vsResults.Row > 0 && !blnLoadingResult)
			{
				blnLoadingResult = true;
				GetFleetInfo();
				blnLoadingResult = false;
			}
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			if (cmbOwner.Text == "Company")
			{
				if (fecherFoundation.Strings.Trim(txtCompanyName.Text) == "")
				{
					MessageBox.Show("You must enter Search Criteria before you may perform a Search.", "No Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					rsVehicleAdd.OpenRecordset("SELECT c.*, p.FullName FROM Master as c LEFT JOIN " + rsVehicleAdd.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE OwnerCode1 <> 'I' AND FullName LIKE '" + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.FixQuotes(txtCompanyName.Text))) + "%' AND FleetNumber = 0 AND Status <> 'T' ORDER BY FullName");
				}
			}
			else
			{
				if (fecherFoundation.Strings.Trim(txtLastName.Text) == "" && fecherFoundation.Strings.Trim(txtFirstName.Text) == "")
				{
					MessageBox.Show("You must enter Search Criteria before you may perform a Search.", "No Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					rsVehicleAdd.OpenRecordset("SELECT c.*, p.FullName, p.FirstName, p.LastName FROM Master  as c LEFT JOIN " + rsVehicleAdd.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE OwnerCode1 = 'I' AND FirstName LIKE '" + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.FixQuotes(txtFirstName.Text))) + "%' AND LastName LIKE '" + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.FixQuotes(txtLastName.Text))) + "%' AND FleetNumber = 0 AND Status <> 'T' ORDER BY LastName");
				}
			}
			if (rsVehicleAdd.EndOfFile() != true && rsVehicleAdd.BeginningOfFile() != true)
			{
				FillData();
				if (vsVehicles.Rows > 1)
				{
					fraSearch.Visible = false;
					if (fecherFoundation.Strings.Trim(txtEpiryMonth.Text) == "")
					{
						lblInstruct.Text = "Please check all the vehicles you wish to add to the group and click the 'Add to Group' button";
						cmdAdd.Text = "Add to Group";
					}
					else
					{
						lblInstruct.Text = "Please check all the vehicles you wish to add to the fleet and click the 'Add to Fleet' button";
						cmdAdd.Text = "Add to Fleet";
					}
					fraVehiclesToAdd.Text = "Vehicles to Add";
					fraVehiclesToAdd.Visible = true;
					if (cmbOwner.Text == "Company")
					{
						txtCompanyName.Text = "";
					}
					else
					{
						txtFirstName.Text = "";
						txtLastName.Text = "";
						txtFirstName.Visible = false;
						txtLastName.Visible = false;
						lblFirstName.Visible = false;
						lblLastName.Visible = false;
						cmbOwner.Text = "Company";
						txtCompanyName.Visible = true;
						lblCompanyName.Visible = true;
					}
					//App.DoEvents();
					Form_Resize();
					Form_Resize();
				}
				else
				{
					MessageBox.Show("No vehicles found that match search criteria.", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				MessageBox.Show("No vehicles found that match search criteria.", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void cmdSearchFleetSearch_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSearch = new clsDRWrapper();
			if (Information.IsNumeric(txtSearchFleet.Text))
			{
				if (MotorVehicle.Statics.bolFromDataInput || cmbFilterAnnual.Text == "Annual" || MotorVehicle.Statics.bolFromVehicleUpdate)
				{
					rsSearch.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE (p.FullName LIKE '" + txtSearchFleet.Text + "%' or q.FullName LIKE '" + txtSearchFleet.Text + "%' OR c.FleetNumber = " + txtSearchFleet.Text + ") AND Type <> 'L' ORDER BY p.FullName");
				}
				else if (MotorVehicle.Statics.bolFromLongTermDataInput || cmbFilterAnnual.Text == "Long Term" || MotorVehicle.Statics.bolFromLongTermVehicleUpdate)
				{
					rsSearch.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE (p.FullName LIKE '" + txtSearchFleet.Text + "%' or q.FullName LIKE '" + txtSearchFleet.Text + "%' OR c.FleetNumber = " + txtSearchFleet.Text + ") AND Type = 'L' ORDER BY p.FullName");
				}
				else
				{
					rsSearch.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE (p.FullName LIKE '" + txtSearchFleet.Text + "%' or q.FullName LIKE '" + txtSearchFleet.Text + "%' OR c.FleetNumber = " + txtSearchFleet.Text + ") ORDER BY p.FullName");
				}
			}
			else
			{
				if (MotorVehicle.Statics.bolFromDataInput || cmbFilterAnnual.Text == "Annual" || MotorVehicle.Statics.bolFromVehicleUpdate)
				{
					rsSearch.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE (p.FullName LIKE '" + txtSearchFleet.Text + "%' or q.FullName LIKE '" + txtSearchFleet.Text + "%') AND Type <> 'L' ORDER BY p.FullName");
				}
				else if (MotorVehicle.Statics.bolFromLongTermDataInput || cmbFilterAnnual.Text == "Long Term" || MotorVehicle.Statics.bolFromLongTermVehicleUpdate)
				{
					rsSearch.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE (p.FullName LIKE '" + txtSearchFleet.Text + "%' or q.FullName LIKE '" + txtSearchFleet.Text + "%') AND Type = 'L' ORDER BY p.FullName");
				}
				else
				{
					rsSearch.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsSearch.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE (p.FullName LIKE '" + txtSearchFleet.Text + "%' or q.FullName LIKE '" + txtSearchFleet.Text + "%') ORDER BY p.FullName");
				}
			}
			vsResults.Rows = 1;
			if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
			{
				while (!rsSearch.EndOfFile())
				{
					vsResults.Rows += 1;
					vsResults.TextMatrix(vsResults.Rows - 1, FleetNumberCol, Strings.Format(rsSearch.Get_Fields_Int32("FleetNumber"), "00000"));
					vsResults.TextMatrix(vsResults.Rows - 1, Owner1Col, fecherFoundation.Strings.UCase(FCConvert.ToString(rsSearch.Get_Fields("Party1FullName"))));
					vsResults.TextMatrix(vsResults.Rows - 1, Owner2Col, fecherFoundation.Strings.UCase(FCConvert.ToString(rsSearch.Get_Fields("Party2FullName"))));
					if (FCConvert.ToInt32(rsSearch.Get_Fields_Int32("ExpiryMonth")) == 99)
					{
						vsResults.TextMatrix(vsResults.Rows - 1, GroupFleetCol, "G");
					}
					else
					{
						vsResults.TextMatrix(vsResults.Rows - 1, GroupFleetCol, "F");
					}
					rsSearch.MoveNext();
				}
				fraSearchFleet.Visible = false;
				fraFleetSearchResults.Visible = true;
			}
			else
			{
				MessageBox.Show("No matching fleets / groups found.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
			{
				vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(true));
			}
		}

		private void frmFleetMaster_Activated(object sender, System.EventArgs e)
		{
			// 
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.FLEETMASTER))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			// 
			if (MotorVehicle.Statics.gboolAllowLongTermTrailers)
			{
				lblAnnual.Visible = true;
				cmbAnnual.Visible = true;
			}
			vsFleets.Visible = false;
			Fill_Fleet_ListBox();
			vsFleets.Visible = true;
			vsGroups.Visible = false;
			Fill_Group_Listbox();
			vsGroups.Visible = true;
			if (MotorVehicle.Statics.bolFromDataInput || MotorVehicle.Statics.bolFromVehicleUpdate)
			{
				if (vsGroups.Rows > 1)
				{
					vsGroups.Row = 1;
					vsGroups_DblClick(null, null);
				}
				else if (vsFleets.Rows > 1)
				{
					vsFleets.Row = 1;
					vsFleets_DblClick(null, null);
				}
				cmbAnnual.Text = "Annual";
				if (cmbAnnual.Items.Contains("Long Term"))
				{
					cmbAnnual.Items.Remove("Long Term");
				}
				cmdAddVehicles.Enabled = false;
				cmdRemoveVehicles.Enabled = false;
				cmdUpdateEmail.Enabled = false;
			}
			else if (MotorVehicle.Statics.bolFromLongTermDataInput || MotorVehicle.Statics.bolFromLongTermVehicleUpdate)
			{
				if (vsGroups.Rows > 1)
				{
					vsGroups.Row = 1;
					vsGroups_DblClick(null, null);
				}
				else if (vsFleets.Rows > 1)
				{
					vsFleets.Row = 1;
					vsFleets_DblClick(null, null);
				}
				if (cmbAnnual.Items.Contains("Annual"))
				{
					cmbAnnual.Items.Remove("Annual");
				}
				cmbAnnual.Text = "Long Term";
				cmdAddVehicles.Enabled = false;
				cmdRemoveVehicles.Enabled = false;
				cmdUpdateEmail.Enabled = false;
			}
			RegisterCol = 1;
			ClassCol = 2;
			PlateCol = 3;
			VINCol = 5;
			YearCol = 6;
			MakeCol = 7;
			ModelCol = 8;
			UnitCol = 9;
			OwnerCol = 4;
			vsVehicles.ColWidth(0, 0);
			// .ColWidth(OwnerCol) = 3000
			// .ColWidth(ClassCol) = 700
			// .ColWidth(VINCol) = 2000
			// .ColWidth(YearCol) = 600
			// .ColWidth(MakeCol) = 700
			// .ColWidth(UnitCol) = 800
			vsVehicles.ColAlignment(PlateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsVehicles.ColAlignment(ModelCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsFleets.Focus();
		}

		private void frmFleetMaster_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmFleetMaster_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraVehiclesToAdd.Visible == true)
				{
					cmdCancelAdd_Click();
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmFleetMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFleetMaster properties;
			//frmFleetMaster.ScaleWidth	= 9045;
			//frmFleetMaster.ScaleHeight	= 7515;
			//frmFleetMaster.LinkTopic	= "Form1";
			//End Unmaped Properties
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "STAAB AGENCY" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "ACE REGISTRATION SERVICES LLC" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER")
			{
				lblFilterAnnual.Visible = true;
				cmbFilterAnnual.Visible = true;
				lblCompanyCode.Visible = true;
				txtCompanyCode.Visible = true;
			}
			else
			{
				lblFilterAnnual.Visible = false;
				cmbFilterAnnual.Visible = false;
				lblCompanyCode.Visible = false;
				txtCompanyCode.Visible = false;
			}
			GroupFleetCol = 1;
			FleetNumberCol = 2;
			Owner1Col = 3;
			Owner2Col = 4;
			FleetGroupNumberCol = 0;
			FleetGroupNameCol = 1;
			vsResults.ColHidden(GroupFleetCol, true);
			vsResults.ColWidth(0, FCConvert.ToInt32(vsResults.WidthOriginal * 0.02));
			vsResults.ColAlignment(FleetNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsResults.ColWidth(FleetNumberCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.2));
			vsResults.ColWidth(Owner1Col, FCConvert.ToInt32(vsResults.WidthOriginal * 0.40));
			vsResults.ColWidth(Owner2Col, FCConvert.ToInt32(vsResults.WidthOriginal * 0.40));
			vsResults.TextMatrix(0, FleetNumberCol, "Fleet #");
			vsResults.TextMatrix(0, Owner1Col, "Owner 1");
			vsResults.TextMatrix(0, Owner2Col, "Owner 2");
			vsFleets.ColWidth(FleetGroupNumberCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.25));
			vsFleets.ColAlignment(FleetGroupNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsFleets.ColWidth(FleetGroupNameCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.45));
			vsFleets.TextMatrix(0, FleetGroupNumberCol, "Fleet #");
			vsFleets.TextMatrix(0, FleetGroupNameCol, "Name");
			vsGroups.ColWidth(FleetGroupNumberCol, FCConvert.ToInt32(vsGroups.WidthOriginal * 0.25));
			vsGroups.ColAlignment(FleetGroupNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGroups.ColWidth(FleetGroupNameCol, FCConvert.ToInt32(vsGroups.WidthOriginal * 0.45));
			vsGroups.TextMatrix(0, FleetGroupNumberCol, "Group #");
			vsGroups.TextMatrix(0, FleetGroupNameCol, "Name");
			blnLoadingResult = false;
			Clear_Screen();
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
			rsFleet.Reset();
			if (blnLocked)
			{
				MotorVehicle.UnlockFleet();
			}
		}

		private void frmFleetMaster_Resize(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: fnx As int	OnWriteFCConvert.ToInt32(
			int fnx;
			int total;
			vsVehicles.AutoSize(1, vsVehicles.Cols - 1);
			total = 0;
			//FC:FINAL:DDU:design part already manages this
			//for (fnx = 0; fnx <= (vsVehicles.Cols - 1); fnx++)
			//{
			//	total += vsVehicles.ColWidth(fnx);
			//}
			//if (vsVehicles.ColWidth(OwnerCol) > 2500)
			//{
			//	vsVehicles.ColWidth(OwnerCol, 2500);
			//}
			//if (vsVehicles.BottomRow < vsVehicles.Rows - 1)
			//{
			//	vsVehicles.Width = total + 300;
			//}
			//else
			//{
			//	vsVehicles.Width = total + 100;
			//}
			//vsVehicles.Left = fraVehiclesToAdd.LeftOriginal + ((fraVehiclesToAdd.WidthOriginal - vsVehicles.WidthOriginal) / 2);
			// - (0.015 * fraVehiclesToAdd.Width)
			//if (vsVehicles.LeftOriginal < 0)
			//	vsVehicles.Left = 0;
			//if (vsVehicles.Rows <= 17)
			//{
			//	vsVehicles.HeightOriginal = vsVehicles.Rows * vsVehicles.RowHeight(0) + 75;
			//}
			//else
			//{
			//	vsVehicles.HeightOriginal = 17 * vsVehicles.RowHeight(0) + 75;
			//}
			if (SecondResize == false)
			{
				SecondResize = true;
				Form_Resize();
			}
			else
			{
				SecondResize = false;
			}
			vsResults.ColHidden(GroupFleetCol, true);
			vsResults.ColWidth(0, FCConvert.ToInt32(vsResults.WidthOriginal * 0.02));
			vsResults.ColWidth(FleetNumberCol, FCConvert.ToInt32(vsResults.WidthOriginal * 0.2));
			vsResults.ColWidth(Owner1Col, FCConvert.ToInt32(vsResults.WidthOriginal * 0.38));
			vsResults.ColWidth(Owner2Col, FCConvert.ToInt32(vsResults.WidthOriginal * 0.38));
			vsFleets.ColWidth(FleetGroupNumberCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.25));
			vsFleets.ColWidth(FleetGroupNameCol, FCConvert.ToInt32(vsFleets.WidthOriginal * 0.45));
			vsGroups.ColWidth(FleetGroupNumberCol, FCConvert.ToInt32(vsGroups.WidthOriginal * 0.25));
			vsGroups.ColWidth(FleetGroupNameCol, FCConvert.ToInt32(vsGroups.WidthOriginal * 0.45));
            vsVehicles.ColWidth(0, 0);
            vsVehicles.ColWidth(ClassCol, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsVehicles.Width * 0.05)));
            vsVehicles.ColWidth(PlateCol, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsVehicles.Width * 0.09)));
            vsVehicles.ColWidth(OwnerCol, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsVehicles.Width * 0.26)));
            vsVehicles.ColWidth(VINCol, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsVehicles.Width * 0.19)));
            vsVehicles.ColWidth(YearCol, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsVehicles.Width * 0.05)));
            vsVehicles.ColWidth(MakeCol, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsVehicles.Width * 0.06)));
            vsVehicles.ColWidth(ModelCol, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsVehicles.Width * 0.09)));
            vsVehicles.ColWidth(9, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(vsVehicles.Width * 0.12)));

            fraSearchFleet.CenterToContainer(this.ClientArea);
            fraSearch.CenterToContainer(this.ClientArea);
			fraFleetSearchResults.CenterToContainer(this.ClientArea);
			fraVehiclesToAdd.CenterToContainer(this.ClientArea);
		}

		public void Form_Resize()
		{
			frmFleetMaster_Resize(this, new System.EventArgs());
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (txtFleetNumber.Text != "AUTO")
			{
				if (MotorVehicle.Statics.bolFromDataInput == true)
				{
					frmDataInput.InstancePtr.txtFleetNumber.Text = txtFleetNumber.Text;
					frmDataInput.InstancePtr.lblFleet.Text = lblRegistrant1.Text;
                    frmDataInput.InstancePtr.VerifyFleetInfo();
                }
				else if (MotorVehicle.Statics.bolFromLongTermDataInput == true)
				{
					frmDataInputLongTermTrailers.InstancePtr.txtFleetNumber.Text = txtFleetNumber.Text;
					frmDataInputLongTermTrailers.InstancePtr.txtFleetName.Text = lblRegistrant1.Text;
				}
				else if (MotorVehicle.Statics.bolFromLongTermVehicleUpdate == true)
				{
					frmVehicleUpdateLongTerm.InstancePtr.txtFleetNumber.Text = txtFleetNumber.Text;
					frmVehicleUpdateLongTerm.InstancePtr.txtFleetName.Text = lblRegistrant1.Text;
				}
				else if (MotorVehicle.Statics.bolFromVehicleUpdate == true)
				{
					frmVehicleUpdate.InstancePtr.txtFleetNumber.Text = txtFleetNumber.Text;
					frmVehicleUpdate.InstancePtr.lblFleetName.Text = lblRegistrant1.Text;
					frmVehicleUpdate.InstancePtr.VerifyFleetInfo();
				}
			}
		}

		private bool Save_New_Fleet()
		{
			bool Save_New_Fleet = false;
			clsDRWrapper rsGetNextFleet = new clsDRWrapper();
			if (txtEpiryMonth.Enabled == false)
			{
				// do nothing
			}
			else if (Conversion.Val(txtEpiryMonth.Text) < 1 || Conversion.Val(txtEpiryMonth.Text) > 12)
			{
				MessageBox.Show("You must supply a valid expiration month before saving.", "Invalid Expire Month", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Save_New_Fleet = false;
				txtEpiryMonth.Focus();
				return Save_New_Fleet;
			}
			if (fecherFoundation.Strings.Trim(txtOwner1Code.Text) == "")
			{
				MessageBox.Show("You must supply a code for the first owner before saving.", "Invalid Owner Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Save_New_Fleet = false;
				txtOwner1Code.Focus();
				return Save_New_Fleet;
			}
			else if (Conversion.Val(fecherFoundation.Strings.Trim(txtReg1PartyID.Text)) == 0)
			{
				MessageBox.Show("You must supply a first owner before saving.", "Invalid Owner", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Save_New_Fleet = false;
				txtReg1PartyID.Focus();
				return Save_New_Fleet;
			}
			else if (fecherFoundation.Strings.Trim(txtAddress.Text) == "")
			{
				MessageBox.Show("You must supply an address before saving.", "Invalid Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Save_New_Fleet = false;
				txtAddress.Focus();
				return Save_New_Fleet;
			}
			else if (fecherFoundation.Strings.Trim(txtCity.Text) == "")
			{
				MessageBox.Show("You must supply a city before saving.", "Invalid City", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Save_New_Fleet = false;
				txtCity.Focus();
				return Save_New_Fleet;
			}
			else if (fecherFoundation.Strings.Trim(txtState.Text) == "")
			{
				MessageBox.Show("You must supply a state before saving.", "Invalid State", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Save_New_Fleet = false;
				txtState.Focus();
				return Save_New_Fleet;
			}
			else if (fecherFoundation.Strings.Trim(txtZip.Text) == "")
			{
				MessageBox.Show("You must supply a zip code before saving.", "Invalid Zip Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Save_New_Fleet = false;
				txtZip.Focus();
				return Save_New_Fleet;
			}
			else if (fecherFoundation.Strings.Trim(txtOwner2Code.Text) != "N" && fecherFoundation.Strings.Trim(txtOwner2Code.Text) != "")
			{
				if (Conversion.Val(fecherFoundation.Strings.Trim(txtReg2PartyID.Text)) == 0)
				{
					MessageBox.Show("You must either change your code for the second owner or enter a party ID for the second owner before saving.", "Invalid Owner", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Save_New_Fleet = false;
					txtReg2PartyID.Focus();
					return Save_New_Fleet;
				}
			}
			else if (cmbAnnual.Text == "Long Term")
			{
				if (IsFleet)
				{
					if (Conversion.Val(txtEpiryMonth.Text) != 2)
					{
						MessageBox.Show("You may only add a long term fleet with an expiration month of 2.", "Invalid Expiration Month", MessageBoxButtons.OK, MessageBoxIcon.Information);
						Save_New_Fleet = false;
						return Save_New_Fleet;
					}
				}
			}
			if (MotorVehicle.LockFleet() == false)
			{
				MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Save_New_Fleet = false;
				return Save_New_Fleet;
			}
			else
			{
				blnLocked = true;
			}
			if (IsFleet)
			{
				if (AddingFleet == true)
				{
					rsFleet.AddNew();
					rsGetNextFleet.OpenRecordset("SELECT TOP 1 FleetNumber FROM FleetMaster ORDER BY FleetNumber DESC");
					if (rsGetNextFleet.EndOfFile() != true && rsGetNextFleet.BeginningOfFile() != true)
					{
						rsGetNextFleet.MoveLast();
						rsGetNextFleet.MoveFirst();
						lngNextFleetNumber = FCConvert.ToInt32(rsGetNextFleet.Get_Fields_Int32("FleetNumber"));
					}
					else
					{
						lngNextFleetNumber = 0;
					}
					rsFleet.Set_Fields("FleetNumber", lngNextFleetNumber + 1);
				}
				else
				{
					rsFleet.Edit();
					rsFleet.Set_Fields("FleetNumber", FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)));
				}
				if (cmbAnnual.Text == "Long Term")
				{
					rsFleet.Set_Fields("Type", "L");
				}
				else
				{
					rsFleet.Set_Fields("Type", "A");
				}
				if (cboStatus.Text == "Deleted")
				{
					rsFleet.Set_Fields("Deleted", true);
				}
				else
				{
					rsFleet.Set_Fields("Deleted", false);
				}
				rsFleet.Set_Fields("CompanyCode", txtCompanyCode.Text);
				rsFleet.Set_Fields("OwnerCode1", txtOwner1Code.Text);
				rsFleet.Set_Fields("PartyID1", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtReg1PartyID.Text))));
				if (txtOwner2Code.Text == "")
				{
					rsFleet.Set_Fields("OwnerCode2", "N");
				}
				else
				{
					rsFleet.Set_Fields("OwnerCode2", txtOwner2Code.Text);
				}
				rsFleet.Set_Fields("PartyID2", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtReg2PartyID.Text))));
				if (txtOwner3Code.Text == "")
				{
					rsFleet.Set_Fields("OwnerCode3", "N");
				}
				else
				{
					rsFleet.Set_Fields("OwnerCode3", txtOwner3Code.Text);
				}
				rsFleet.Set_Fields("PartyID3", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtReg3PartyID.Text))));
				rsFleet.Set_Fields("Address", txtAddress.Text);
				rsFleet.Set_Fields("Address2", txtAddress2.Text);
				rsFleet.Set_Fields("City", txtCity.Text);
				rsFleet.Set_Fields("State", txtState.Text);
				rsFleet.Set_Fields("Zip", txtZip.Text);
				rsFleet.Set_Fields("Country", txtCountry.Text);
				rsFleet.Set_Fields("ExpiryMonth", FCConvert.ToString(Conversion.Val(txtEpiryMonth.Text)));
				rsFleet.Set_Fields("Contact", txtContact.Text);
				rsFleet.Set_Fields("EMailAddress", txtEmail.Text);
				rsFleet.Set_Fields("Telephone", txtTelephone.Text);
				rsFleet.Set_Fields("TaxID", txtIDNumber.Text);
				if (Information.IsDate(txtDOB1.Text))
				{
					rsFleet.Set_Fields("DOB1", Strings.Format(txtDOB1.Text, "MM/dd/yyyy"));
				}
				if (Information.IsDate(txtDOB2.Text))
				{
					rsFleet.Set_Fields("DOB2", Strings.Format(txtDOB2.Text, "MM/dd/yyyy"));
				}
				if (Information.IsDate(txtDOB3.Text))
				{
					rsFleet.Set_Fields("DOB3", Strings.Format(txtDOB3.Text, "MM/dd/yyyy"));
				}
				rsFleet.Update();
				if (AddingFleet)
				{
					txtFleetNumber.Text = FCConvert.ToString(lngNextFleetNumber + 1);
					MessageBox.Show("Fleet " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + " has been created.", "Fleet Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				Save_New_Fleet = true;
			}
			else
			{
				if (AddingGroup == true)
				{
					rsGroup.AddNew();
					rsGetNextFleet.OpenRecordset("SELECT TOP 1 FleetNumber FROM FleetMaster ORDER BY FleetNumber DESC");
					if (rsGetNextFleet.EndOfFile() != true && rsGetNextFleet.BeginningOfFile() != true)
					{
						rsGetNextFleet.MoveLast();
						rsGetNextFleet.MoveFirst();
						lngNextFleetNumber = FCConvert.ToInt32(rsGetNextFleet.Get_Fields_Int32("FleetNumber"));
					}
					else
					{
						lngNextFleetNumber = 0;
					}
					rsGroup.Set_Fields("FleetNumber", lngNextFleetNumber + 1);
				}
				else
				{
					rsGroup.Edit();
					rsGroup.Set_Fields("FleetNumber", FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)));
				}
				if (cmbAnnual.Text == "Long Term")
				{
					rsGroup.Set_Fields("Type", "L");
				}
				else
				{
					rsGroup.Set_Fields("Type", "A");
				}
				if (cboStatus.Text == "Deleted")
				{
					rsGroup.Set_Fields("Deleted", true);
				}
				else
				{
					rsGroup.Set_Fields("Deleted", false);
				}
				rsGroup.Set_Fields("CompanyCode", txtCompanyCode.Text);
				rsGroup.Set_Fields("OwnerCode1", txtOwner1Code.Text);
				rsGroup.Set_Fields("PartyID1", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtReg1PartyID.Text))));
				if (txtOwner2Code.Text == "")
				{
					rsGroup.Set_Fields("OwnerCode2", "N");
				}
				else
				{
					rsGroup.Set_Fields("OwnerCode2", txtOwner2Code.Text);
				}
				rsGroup.Set_Fields("PartyID2", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtReg2PartyID.Text))));
				if (txtOwner3Code.Text == "")
				{
					rsGroup.Set_Fields("OwnerCode3", "N");
				}
				else
				{
					rsGroup.Set_Fields("OwnerCode3", txtOwner3Code.Text);
				}
				rsGroup.Set_Fields("PartyID3", FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(txtReg3PartyID.Text))));
				rsGroup.Set_Fields("Address", txtAddress.Text);
				rsGroup.Set_Fields("Address2", txtAddress2.Text);
				rsGroup.Set_Fields("City", txtCity.Text);
				rsGroup.Set_Fields("State", txtState.Text);
				rsGroup.Set_Fields("Zip", txtZip.Text);
				rsGroup.Set_Fields("Country", txtCountry.Text);
				rsGroup.Set_Fields("ExpiryMonth", 99);
				rsGroup.Set_Fields("Contact", txtContact.Text);
				rsGroup.Set_Fields("EMailAddress", txtEmail.Text);
				rsGroup.Set_Fields("Telephone", txtTelephone.Text);
				rsGroup.Set_Fields("TaxID", txtIDNumber.Text);
				if (Information.IsDate(txtDOB1.Text))
				{
					rsGroup.Set_Fields("DOB1", Strings.Format(txtDOB1.Text, "MM/dd/yyyy"));
				}
				if (Information.IsDate(txtDOB2.Text))
				{
					rsGroup.Set_Fields("DOB2", Strings.Format(txtDOB2.Text, "MM/dd/yyyy"));
				}
				if (Information.IsDate(txtDOB3.Text))
				{
					rsGroup.Set_Fields("DOB3", Strings.Format(txtDOB3.Text, "MM/dd/yyyy"));
				}
				rsGroup.Update();
				if (AddingGroup)
				{
					txtFleetNumber.Text = FCConvert.ToString(lngNextFleetNumber + 1);
					MessageBox.Show("Group " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + " has been created.", "Group Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				Save_New_Fleet = true;
			}
			MotorVehicle.UnlockFleet();
			blnLocked = false;
			return Save_New_Fleet;
		}

		private void Get_Existing_Fleet()
		{
			int lngHoldID = 0;
			clsDRWrapper rsGetNextFleet = new clsDRWrapper();
			clsDRWrapper rsCity = new clsDRWrapper();
			// 
			lngNextFleetNumber = 0;
			if (AddingFleet == false && AddingGroup == false)
			{
				if (IsFleet)
				{
					if (vsFleets.Row > 0)
					{
						lngHoldID = FCConvert.ToInt32(Math.Round(Conversion.Val(vsFleets.TextMatrix(vsFleets.Row, FleetGroupNumberCol))));
						if (rsFleet.FindFirstRecord("FleetNumber", lngHoldID))
						{
							Clear_Screen();
							Fill_Screen();
							if (!MotorVehicle.Statics.bolFromDataInput && !MotorVehicle.Statics.bolFromLongTermDataInput && !MotorVehicle.Statics.bolFromLongTermVehicleUpdate && !MotorVehicle.Statics.bolFromVehicleUpdate)
							{
								cmdAddVehicles.Enabled = true;
								cmdRemoveVehicles.Enabled = true;
								cmdUpdateEmail.Enabled = true;
							}
							cmdComments.Enabled = true;
						}
						else
						{
							MessageBox.Show("Invalid Fleet Number.", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
				else
				{
					if (vsGroups.Row > 0)
					{
						lngHoldID = FCConvert.ToInt32(Math.Round(Conversion.Val(vsGroups.TextMatrix(vsGroups.Row, FleetGroupNumberCol))));
						if (rsGroup.FindFirstRecord("FleetNumber", lngHoldID))
						{
							Clear_Screen();
							Fill_Screen();
							if (!MotorVehicle.Statics.bolFromDataInput && !MotorVehicle.Statics.bolFromLongTermDataInput && !MotorVehicle.Statics.bolFromLongTermVehicleUpdate && !MotorVehicle.Statics.bolFromVehicleUpdate)
							{
								cmdAddVehicles.Enabled = true;
								cmdRemoveVehicles.Enabled = true;
								cmdUpdateEmail.Enabled = true;
							}
							cmdComments.Enabled = true;
						}
						else
						{
							MessageBox.Show("Invalid Group Number.", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
			}
			else
			{
				// rsGetNextFleet.OpenRecordset "SELECT TOP 1 FleetNumber FROM FleetMaster ORDER BY FleetNumber DESC"
				// If rsGetNextFleet.EndOfFile <> True And rsGetNextFleet.BeginningOfFile <> True Then
				// rsGetNextFleet.MoveLast
				// rsGetNextFleet.MoveFirst
				// lngNextFleetNumber = rsGetNextFleet.Fields("FleetNumber")
				// Else
				// lngNextFleetNumber = 0
				// End If
				Clear_Screen();
			}
			if (AddingFleet || AddingGroup)
			{
				txtFleetNumber.Text = "AUTO";
				// lngNextFleetNumber + 1
				if (cmbAnnual.Items.Contains("Annual"))
				{
					cmbAnnual.Text = "Annual";
				}
				else
				{
					cmbAnnual.Text = "Long Term";
				}
				rsCity.OpenRecordset("SELECT * FROM DefaultInfo");
				if (rsCity.EndOfFile() != true && rsCity.BeginningOfFile() != true)
				{
					txtCity.Text = FCConvert.ToString(rsCity.Get_Fields_String("Town"));
					txtState.Text = FCConvert.ToString(rsCity.Get_Fields("State"));
					txtZip.Text = FCConvert.ToString(rsCity.Get_Fields_String("Zip"));
				}
				txtCountry.Text = "US";
				rsCity.Reset();
			}
			txtOwner1Code.Focus();
			rsGetNextFleet.Reset();
		}

		private void imgReg1Comments_Click(object sender, System.EventArgs e)
		{
			frmComment.InstancePtr.Init("MV", FCConvert.ToInt32(FCConvert.ToDouble(txtReg1PartyID.Text)), "Comment", "", 0, true);
		}

		private void imgReg2Comments_Click(object sender, System.EventArgs e)
		{
			frmComment.InstancePtr.Init("MV", FCConvert.ToInt32(FCConvert.ToDouble(txtReg2PartyID.Text)), "Comment", "", 0, true);
		}

		private void imgReg3Comments_Click(object sender, System.EventArgs e)
		{
			frmComment.InstancePtr.Init("MV", FCConvert.ToInt32(FCConvert.ToDouble(txtReg3PartyID.Text)), "Comment", "", 0, true);
		}

		public void vsFleets_DblClick(object sender, System.EventArgs e)
		{
			Frame1.Enabled = true;
			AddingFleet = false;
			AddingGroup = false;
			IsFleet = true;
			Get_Existing_Fleet();
		}

		private void vsFleets_Enter(object sender, System.EventArgs e)
		{
			vsGroups.Row = -1;
		}

		public void vsGroups_DblClick(object sender, System.EventArgs e)
		{
			Frame1.Enabled = true;
			AddingFleet = false;
			AddingGroup = false;
			IsFleet = false;
			Get_Existing_Fleet();
		}

		private void Fill_Screen()
		{
			if (IsFleet)
			{
				txtFleetNumber.Text = FCConvert.ToString(rsFleet.Get_Fields_Int32("FleetNumber"));
				if (FCConvert.ToString(rsFleet.Get_Fields("Type")) != "L")
				{
					cmbAnnual.Text = "Annual";
				}
				else
				{
					cmbAnnual.Text = "Long Term";
				}
				if (rsFleet.Get_Fields_Boolean("Deleted") == true)
				{
					cboStatus.Text = "Deleted";
				}
				else
				{
					cboStatus.Text = "Active";
				}
				txtCompanyCode.Text = FCConvert.ToString(rsFleet.Get_Fields_String("CompanyCode"));
				txtOwner1Code.Text = FCConvert.ToString(rsFleet.Get_Fields_String("OwnerCode1"));
				txtReg1PartyID.Text = FCConvert.ToString(rsFleet.Get_Fields_Int32("PartyID1"));
				GetPartyInfo(rsFleet.Get_Fields_Int32("PartyID1"), 1);
				txtOwner2Code.Text = FCConvert.ToString(rsFleet.Get_Fields_String("OwnerCode2"));
				if (Conversion.Val(rsFleet.Get_Fields_Int32("PartyID2")) != 0)
				{
					txtReg2PartyID.Text = FCConvert.ToString(rsFleet.Get_Fields_Int32("PartyID2"));
					GetPartyInfo(rsFleet.Get_Fields_Int32("PartyID2"), 2);
				}
				else
				{
					ClearPartyInfo(2);
				}
				txtOwner3Code.Text = FCConvert.ToString(rsFleet.Get_Fields_String("OwnerCode3"));
				if (Conversion.Val(rsFleet.Get_Fields_Int32("PartyID3")) != 0)
				{
					txtReg3PartyID.Text = FCConvert.ToString(rsFleet.Get_Fields_Int32("PartyID3"));
					GetPartyInfo(rsFleet.Get_Fields_Int32("PartyID3"), 3);
				}
				else
				{
					ClearPartyInfo(3);
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields_String("Comment"))) != "")
				{
					Image1.Visible = true;
				}
				else
				{
					Image1.Visible = false;
				}
				txtAddress.Text = FCConvert.ToString(rsFleet.Get_Fields_String("Address"));
				txtAddress2.Text = FCConvert.ToString(rsFleet.Get_Fields_String("Address2"));
				txtCity.Text = FCConvert.ToString(rsFleet.Get_Fields_String("City"));
				txtState.Text = FCConvert.ToString(rsFleet.Get_Fields("State"));
				txtZip.Text = FCConvert.ToString(rsFleet.Get_Fields_String("Zip"));
				txtCountry.Text = FCConvert.ToString(rsFleet.Get_Fields_String("Country"));
				Label10.Text = "Fleet Number";
				Frame1.Text = "Fleet Information";
				txtEpiryMonth.Enabled = true;
				Label9.Enabled = true;
				txtEpiryMonth.Text = Strings.Format(rsFleet.Get_Fields_Int32("ExpiryMonth"), "00");
				txtContact.Text = FCConvert.ToString(rsFleet.Get_Fields_String("Contact"));
				txtEmail.Text = FCConvert.ToString(rsFleet.Get_Fields_String("EMailAddress"));
				txtTelephone.Text = FCConvert.ToString(rsFleet.Get_Fields_String("Telephone"));
				txtIDNumber.Text = FCConvert.ToString(rsFleet.Get_Fields_String("TaxID"));
				if (txtOwner1Code.Text == "I")
				{
					txtDOB1.Visible = true;
					if (Information.IsDate(rsFleet.Get_Fields("DOB1")))
					{
						txtDOB1.Text = Strings.Format(rsFleet.Get_Fields_DateTime("DOB1"), "MM/dd/yyyy");
					}
					else
					{
						txtDOB1.Text = "";
					}
				}
				else
				{
					txtDOB1.Text = "";
					txtDOB1.Visible = false;
				}
				if (txtOwner2Code.Text == "I")
				{
					txtDOB2.Visible = true;
					if (Information.IsDate(rsFleet.Get_Fields("DOB2")))
					{
						txtDOB2.Text = Strings.Format(rsFleet.Get_Fields_DateTime("DOB2"), "MM/dd/yyyy");
					}
					else
					{
						txtDOB2.Text = "";
					}
				}
				else
				{
					txtDOB2.Text = "";
					txtDOB2.Visible = false;
				}
				if (txtOwner3Code.Text == "I")
				{
					txtDOB3.Visible = true;
					if (Information.IsDate(rsFleet.Get_Fields("DOB3")))
					{
						txtDOB3.Text = Strings.Format(rsFleet.Get_Fields_DateTime("DOB3"), "MM/dd/yyyy");
					}
					else
					{
						txtDOB3.Text = "";
					}
				}
				else
				{
					txtDOB3.Text = "";
					txtDOB3.Visible = false;
				}
			}
			else
			{
				txtFleetNumber.Text = FCConvert.ToString(rsGroup.Get_Fields_Int32("FleetNumber"));
				if (FCConvert.ToString(rsGroup.Get_Fields("Type")) != "L")
				{
					cmbAnnual.Text = "Annual";
				}
				else
				{
					cmbAnnual.Text = "Long Term";
				}
				if (rsGroup.Get_Fields_Boolean("Deleted") == true)
				{
					cboStatus.Text = "Deleted";
				}
				else
				{
					cboStatus.Text = "Active";
				}
				txtCompanyCode.Text = FCConvert.ToString(rsGroup.Get_Fields_String("CompanyCode"));
				txtOwner1Code.Text = FCConvert.ToString(rsGroup.Get_Fields_String("OwnerCode1"));
				txtReg1PartyID.Text = FCConvert.ToString(rsGroup.Get_Fields_Int32("PartyID1"));
				GetPartyInfo(rsGroup.Get_Fields_Int32("PartyID1"), 1);
				txtOwner2Code.Text = FCConvert.ToString(rsGroup.Get_Fields_String("OwnerCode2"));
				if (Conversion.Val(rsGroup.Get_Fields_Int32("PartyID2")) != 0)
				{
					txtReg2PartyID.Text = FCConvert.ToString(rsGroup.Get_Fields_Int32("PartyID2"));
					GetPartyInfo(rsGroup.Get_Fields_Int32("PartyID2"), 2);
				}
				else
				{
					ClearPartyInfo(2);
				}
				txtOwner3Code.Text = FCConvert.ToString(rsGroup.Get_Fields_String("OwnerCode3"));
				if (Conversion.Val(rsGroup.Get_Fields_Int32("PartyID3")) != 0)
				{
					txtReg3PartyID.Text = FCConvert.ToString(rsGroup.Get_Fields_Int32("PartyID3"));
					GetPartyInfo(rsGroup.Get_Fields_Int32("PartyID3"), 3);
				}
				else
				{
					ClearPartyInfo(3);
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsGroup.Get_Fields_String("Comment"))) != "")
				{
					Image1.Visible = true;
				}
				else
				{
					Image1.Visible = false;
				}
				txtAddress.Text = FCConvert.ToString(rsGroup.Get_Fields_String("Address"));
				txtAddress2.Text = FCConvert.ToString(rsGroup.Get_Fields_String("Address2"));
				txtCity.Text = FCConvert.ToString(rsGroup.Get_Fields_String("City"));
				txtState.Text = FCConvert.ToString(rsGroup.Get_Fields("State"));
				txtZip.Text = FCConvert.ToString(rsGroup.Get_Fields_String("Zip"));
				txtCountry.Text = FCConvert.ToString(rsGroup.Get_Fields_String("Country"));
				Label10.Text = "Group Number";
				Frame1.Text = "Group Information";
				txtEpiryMonth.Text = "";
				txtEpiryMonth.Enabled = false;
				Label9.Enabled = false;
				txtContact.Text = FCConvert.ToString(rsGroup.Get_Fields_String("Contact"));
				txtEmail.Text = FCConvert.ToString(rsGroup.Get_Fields_String("EMailAddress"));
				txtTelephone.Text = FCConvert.ToString(rsGroup.Get_Fields_String("Telephone"));
				if (txtOwner1Code.Text == "I")
				{
					txtDOB1.Visible = true;
					if (Information.IsDate(rsGroup.Get_Fields("DOB1")))
					{
						txtDOB1.Text = Strings.Format(rsGroup.Get_Fields_DateTime("DOB1"), "MM/dd/yyyy");
					}
					else
					{
						txtDOB1.Text = "";
					}
				}
				else
				{
					txtDOB1.Text = "";
					txtDOB1.Visible = false;
				}
				if (txtOwner2Code.Text == "I")
				{
					txtDOB2.Visible = true;
					if (Information.IsDate(rsGroup.Get_Fields("DOB2")))
					{
						txtDOB2.Text = Strings.Format(rsGroup.Get_Fields_DateTime("DOB2"), "MM/dd/yyyy");
					}
					else
					{
						txtDOB2.Text = "";
					}
				}
				else
				{
					txtDOB2.Text = "";
					txtDOB2.Visible = false;
				}
				if (txtOwner3Code.Text == "I")
				{
					txtDOB3.Visible = true;
					if (Information.IsDate(rsGroup.Get_Fields("DOB3")))
					{
						txtDOB3.Text = Strings.Format(rsGroup.Get_Fields("DOB3"), "MM/dd/yyyy");
					}
					else
					{
						txtDOB3.Text = "";
					}
				}
				else
				{
					txtDOB3.Text = "";
					txtDOB3.Visible = false;
				}
			}
		}

		private void Clear_Screen()
		{
			if (AddingGroup == true)
			{
				txtEpiryMonth.Text = "";
				Label9.Enabled = false;
				txtEpiryMonth.Enabled = false;
			}
			else
			{
				Label9.Enabled = true;
				txtEpiryMonth.Enabled = true;
				txtEpiryMonth.Text = "";
			}
			txtFleetNumber.Text = "";
			txtAddress.Text = "";
			txtAddress2.Text = "";
			txtCity.Text = "";
			txtContact.Text = "";
			txtEmail.Text = "";
			ClearPartyInfo(1);
			ClearPartyInfo(2);
			ClearPartyInfo(3);
			txtFleetNumber.Text = "";
			txtIDNumber.Text = "";
			txtCompanyCode.Text = "";
			txtOwner1Code.Text = "";
			txtReg1PartyID.Text = "";
			txtOwner2Code.Text = "";
			txtReg2PartyID.Text = "";
			txtOwner3Code.Text = "";
			txtReg3PartyID.Text = "";
			txtState.Text = "";
			txtTelephone.Text = "";
			txtZip.Text = "";
			txtCountry.Text = "";
			cboStatus.Text = "Active";
			cmdAddVehicles.Enabled = false;
			cmdRemoveVehicles.Enabled = false;
			cmdUpdateEmail.Enabled = false;
			Image1.Visible = false;
			cmdComments.Enabled = false;
		}

		private void vsGroups_Enter(object sender, System.EventArgs e)
		{
			vsFleets.Row = -1;
		}

		private void mnuAddNewFleet_Click(object sender, System.EventArgs e)
		{
			Frame1.Enabled = true;
			AddingFleet = true;
			AddingGroup = false;
			Label10.Text = "Fleet Number";
			Frame1.Text = "Fleet Information";
			IsFleet = true;
			Get_Existing_Fleet();
		}

		private void mnuAddNewGroup_Click(object sender, System.EventArgs e)
		{
			Frame1.Enabled = true;
			AddingGroup = true;
			AddingFleet = false;
			Label10.Text = "Group Number";
			Frame1.Text = "Group Information";
			IsFleet = false;
			Get_Existing_Fleet();
		}

		private void mnuDeleteFleet_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsUser = new clsDRWrapper();
			// vbPorter upgrade warning: ans As int	OnWrite(DialogResult)
			DialogResult ans = 0;
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (Conversion.Val(txtFleetNumber.Text) != 0)
			{
				ans = MessageBox.Show("Are you sure you wish to delete this Fleet / Group?", "Delete Fleet?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					if (Conversion.Val(txtFleetNumber.Text) != 0)
					{
						rsTemp.Execute("UPDATE Master SET FleetNumber = 0, FleetNew = 0, FleetOld = 0 WHERE isnull(FleetNumber, 0)  = " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)), "TWMV0000.vb1");
						rsTemp.OpenRecordset("SELECT * FROM FleetMaster WHERE FleetNumber = " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)));
						if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
						{
							rsTemp.Execute("DELETE FROM FleetMaster WHERE FleetNumber = " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)), "TWMV0000.vb1");
						}
						else
						{
							MessageBox.Show("Unable to Delete Record " + FCConvert.ToString(Conversion.Val(txtFleetNumber.Text)) + ".", "Unable To Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					rsTemp.Reset();
					if (IsFleet)
					{
						Fill_Fleet_ListBox();
					}
					else
					{
						Fill_Group_Listbox();
					}
					Clear_Screen();
				}
			}
			else
			{
				MessageBox.Show("You must first select a fleet or group before you may delete one.", "Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuFileSearch_Click(object sender, System.EventArgs e)
		{
			fraSearchFleet.Visible = true;
			txtSearchFleet.Focus();
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuQuit_Click()
		{
			mnuQuit_Click(mnuQuit, new System.EventArgs());
		}

		private void Fill_Fleet_ListBox()
		{
			FCFixedString strAdd = new FCFixedString(70);
			vsFleets.Rows = 1;
			vsFleets.Redraw = false;
			cmdSave.Enabled = false;
			if (MotorVehicle.Statics.bolFromDataInput || cmbFilterAnnual.Text == "Annual" || MotorVehicle.Statics.bolFromVehicleUpdate)
			{
				rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE ExpiryMonth <> 99 and Type <> 'L' ORDER BY p.FullName");
			}
			else if (MotorVehicle.Statics.bolFromLongTermDataInput || cmbFilterAnnual.Text == "Long Term" || MotorVehicle.Statics.bolFromLongTermVehicleUpdate)
			{
				rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE ExpiryMonth <> 99 and Type = 'L' ORDER BY p.FullName");
			}
			else
			{
				rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE ExpiryMonth <> 99 ORDER BY p.FullName");
			}
			if (rsFleet.EndOfFile() != true && rsFleet.BeginningOfFile() != true)
			{
				rsFleet.MoveLast();
				rsFleet.MoveFirst();
				while (!rsFleet.EndOfFile())
				{
					vsFleets.AddItem(rsFleet.Get_Fields_Int32("FleetNumber") + "\t" + fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleet.Get_Fields_String("FullName"))));
					if (FCConvert.ToBoolean(rsFleet.Get_Fields_Boolean("Deleted")))
					{
						vsFleets.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsFleets.Rows - 1, 0, vsFleets.Rows - 1, vsFleets.Cols - 1, Color.Red);
					}
					rsFleet.MoveNext();
				}
			}
			rsFleet.OpenRecordset("SELECT * FROM FleetMaster");
			vsFleets.Redraw = true;
			cmdSave.Enabled = true;
		}

		private void Fill_Group_Listbox()
		{
			FCFixedString strAdd = new FCFixedString(70);
			vsGroups.Rows = 1;
			vsGroups.Redraw = false;
			cmdSave.Enabled = false;
			if (MotorVehicle.Statics.bolFromDataInput || cmbFilterAnnual.Text == "Annual" || MotorVehicle.Statics.bolFromVehicleUpdate)
			{
				rsGroup.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsGroup.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE ExpiryMonth = 99 and Type <> 'L' ORDER BY p.FullName");
			}
			else if (MotorVehicle.Statics.bolFromLongTermDataInput || cmbFilterAnnual.Text == "Long Term" || MotorVehicle.Statics.bolFromLongTermVehicleUpdate)
			{
				rsGroup.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsGroup.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE ExpiryMonth = 99 and Type = 'L' ORDER BY p.FullName");
			}
			else
			{
				rsGroup.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsGroup.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE ExpiryMonth = 99 ORDER BY p.FullName");
			}
			if (rsGroup.EndOfFile() != true && rsGroup.BeginningOfFile() != true)
			{
				rsGroup.MoveLast();
				rsGroup.MoveFirst();
				while (!rsGroup.EndOfFile())
				{
					vsGroups.AddItem(rsGroup.Get_Fields_Int32("FleetNumber") + "\t" + fecherFoundation.Strings.UCase(FCConvert.ToString(rsGroup.Get_Fields_String("FullName"))));
					if (FCConvert.ToBoolean(rsGroup.Get_Fields_Boolean("Deleted")))
					{
						vsGroups.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsGroups.Rows - 1, 0, vsGroups.Rows - 1, vsGroups.Cols - 1, Color.Red);
					}
					rsGroup.MoveNext();
				}
			}
			rsGroup.OpenRecordset("SELECT * FROM FleetMaster");
			vsGroups.Redraw = true;
			cmdSave.Enabled = true;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUser = new clsDRWrapper();
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (Save_New_Fleet() == true)
			{
				if (MotorVehicle.Statics.bolFromDataInput || MotorVehicle.Statics.bolFromLongTermDataInput || MotorVehicle.Statics.bolFromLongTermVehicleUpdate || MotorVehicle.Statics.bolFromVehicleUpdate)
				{
					mnuQuit_Click();
				}
				else
				{
					Clear_Screen();
					Frame1.Enabled = false;
					if (IsFleet)
					{
						Fill_Fleet_ListBox();
					}
					else
					{
						Fill_Group_Listbox();
					}

					MessageBox.Show("Save Successful");
				}
			}
		}

		private void mnuSaveQuit_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsUser = new clsDRWrapper();
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (Save_New_Fleet() == true)
			{
				mnuQuit_Click();
			}
		}

		private void optAnnual_CheckedChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsCheck = new clsDRWrapper();
			if (AddingFleet || AddingGroup)
			{
				// do nothing
			}
			else
			{
				rsCheck.OpenRecordset("SELECT * FROM Master WHERE Status <> 'T' AND Subclass = 'L9' AND FleetNumber = " + txtFleetNumber.Text + " ORDER BY Owner1");
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					MessageBox.Show("You must remove all long term vehicles tied to this fleet before you may change the type to Annual.", "Invalid Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cmbAnnual.Text = "Long Term";
				}
			}
		}

		private void optFilterAnnual_CheckedChanged(object sender, System.EventArgs e)
		{
			Fill_Fleet_ListBox();
			Fill_Group_Listbox();
		}

		private void optFilterBoth_CheckedChanged(object sender, System.EventArgs e)
		{
			Fill_Fleet_ListBox();
			Fill_Group_Listbox();
		}

		private void optFilterLongTerm_CheckedChanged(object sender, System.EventArgs e)
		{
			Fill_Fleet_ListBox();
			Fill_Group_Listbox();
		}

		private void optLongTerm_CheckedChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsCheck = new clsDRWrapper();
			if (AddingFleet || AddingGroup)
			{
				// do nothing
			}
			else
			{
				rsCheck.OpenRecordset("SELECT * FROM Master WHERE Status <> 'T' AND Subclass <> 'L9' AND FleetNumber = " + txtFleetNumber.Text );
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					MessageBox.Show("You must remove all annual vehicles tied to this fleet before you may change the type to Long Term.", "Invalid Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cmbAnnual.Text = "Annual";
				}
			}
		}

		private void optOwner_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (fraSearch.Visible == true)
			{
				if (Index == 0)
				{
					txtFirstName.Text = "";
					txtLastName.Text = "";
					txtFirstName.Visible = false;
					txtLastName.Visible = false;
					lblFirstName.Visible = false;
					lblLastName.Visible = false;
					txtCompanyName.Visible = true;
					lblCompanyName.Visible = true;
					txtCompanyName.Focus();
				}
				else
				{
					txtCompanyName.Text = "";
					txtFirstName.Visible = true;
					txtLastName.Visible = true;
					lblFirstName.Visible = true;
					lblLastName.Visible = true;
					txtCompanyName.Visible = false;
					lblCompanyName.Visible = false;
					txtFirstName.Focus();
				}
			}
		}

		private void optOwner_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbOwner.SelectedIndex;
			optOwner_CheckedChanged(index, sender, e);
		}

		private void FillData()
		{
			int counter;
			vsVehicles.Clear();
			rsVehicleAdd.MoveLast();
			rsVehicleAdd.MoveFirst();
			vsVehicles.Rows = rsVehicleAdd.RecordCount() + 1;
			counter = 1;
			//vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsVehicles.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsVehicles.ColDataType(RegisterCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsVehicles.TextMatrix(0, RegisterCol, "Add");
			vsVehicles.TextMatrix(0, ClassCol, "Class");
			vsVehicles.TextMatrix(0, PlateCol, "Plate");
			vsVehicles.TextMatrix(0, VINCol, "VIN");
			vsVehicles.TextMatrix(0, YearCol, "Year");
			vsVehicles.TextMatrix(0, MakeCol, "Make");
			vsVehicles.TextMatrix(0, ModelCol, "Model");
			if (txtEpiryMonth.Enabled == false)
			{
				vsVehicles.TextMatrix(0, UnitCol, "Expires");
				vsVehicles.ColDataType(UnitCol, FCGrid.DataTypeSettings.flexDTDate);
			}
			else
			{
				vsVehicles.TextMatrix(0, UnitCol, "Unit");
				vsVehicles.ColDataType(UnitCol, FCGrid.DataTypeSettings.flexDTString);
			}
			vsVehicles.TextMatrix(0, OwnerCol, "Owner");
			do
			{
				if (Information.IsDate(rsVehicleAdd.Get_Fields("ExpireDate")))
				{
					if (Convert.ToDateTime(rsVehicleAdd.Get_Fields_DateTime("ExpireDate")).Month == Conversion.Val(txtEpiryMonth.Text) || txtEpiryMonth.Enabled == false)
					{
						if (cmbAnnual.Text == "Long Term")
						{
							if (FCConvert.ToString(rsVehicleAdd.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(rsVehicleAdd.Get_Fields_String("SubClass")) == "L9")
							{
								// do nothing
							}
							else
							{
								goto CheckNext;
							}
						}
						else
						{
							if (FCConvert.ToString(rsVehicleAdd.Get_Fields_String("Class")) != "TL" || FCConvert.ToString(rsVehicleAdd.Get_Fields_String("SubClass")) != "L9")
							{
								// do nothing
							}
							else
							{
								goto CheckNext;
							}
						}
						vsVehicles.TextMatrix(counter, 0, FCConvert.ToString(rsVehicleAdd.Get_Fields_Int32("ID")));
						vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(false));
						vsVehicles.TextMatrix(counter, ClassCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("Class")));
						vsVehicles.TextMatrix(counter, PlateCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("plate")));
						vsVehicles.TextMatrix(counter, OwnerCol, fecherFoundation.Strings.UCase(FCConvert.ToString(rsVehicleAdd.Get_Fields_String("FullName"))));
						vsVehicles.TextMatrix(counter, VINCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("Vin")));
						vsVehicles.TextMatrix(counter, MakeCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("make")));
						vsVehicles.TextMatrix(counter, ModelCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("model")));
						vsVehicles.TextMatrix(counter, YearCol, FCConvert.ToString(rsVehicleAdd.Get_Fields("Year")));
						if (txtEpiryMonth.Enabled == false)
						{
							if (!((rsVehicleAdd.IsFieldNull("ExpireDate"))))
							{
								vsVehicles.TextMatrix(counter, UnitCol, Strings.Format(rsVehicleAdd.Get_Fields_DateTime("ExpireDate"), "MM/dd/yy"));
							}
						}
						else
						{
							vsVehicles.TextMatrix(counter, UnitCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("UnitNumber")));
						}
						counter += 1;
					}
				}
				CheckNext:
				;
				rsVehicleAdd.MoveNext();
			}
			while (rsVehicleAdd.EndOfFile() != true);
			vsVehicles.Rows = counter;
			//FC:FINAL:DDU:design part already manages this
			//if (vsVehicles.Rows <= 17)
			//{
			//	vsVehicles.Height = vsVehicles.Rows * vsVehicles.RowHeight(0) + 75;
			//}
			//else
			//{
			//	vsVehicles.Height = 17 * vsVehicles.RowHeight(0) + 75;
			//}
		}

		private void txtOwner1Code_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii != Keys.C && KeyAscii != Keys.I && KeyAscii != Keys.M && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtOwner1Code_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtOwner1Code.Text == "I")
			{
				txtDOB1.Visible = true;
			}
			else
			{
				txtDOB1.Visible = false;
				txtDOB1.Text = "";
			}
		}

		private void txtOwner2Code_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii != Keys.C && KeyAscii != Keys.D && KeyAscii != Keys.I && KeyAscii != Keys.N && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtOwner2Code_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtReg2PartyID.Enabled = txtOwner2Code.Text != "N" && !String.IsNullOrEmpty(txtOwner2Code.Text);
			cmdReg2Search.Enabled = txtOwner2Code.Text != "N" && !String.IsNullOrEmpty(txtOwner2Code.Text);
			if (txtOwner2Code.Text == "I")
			{
				txtDOB2.Visible = true;
			}
			else
			{
				txtDOB2.Visible = false;
				txtDOB2.Text = "";
			}
		}

		private void txtOwner3Code_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii != Keys.C && KeyAscii != Keys.D && KeyAscii != Keys.I && KeyAscii != Keys.N && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtOwner3Code_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			txtReg3PartyID.Enabled = txtOwner3Code.Text != "N" && !String.IsNullOrEmpty(txtOwner3Code.Text);
			cmdReg3Search.Enabled = txtOwner3Code.Text != "N" && !String.IsNullOrEmpty(txtOwner3Code.Text);
			if (txtOwner3Code.Text == "I")
			{
				txtDOB3.Visible = true;
			}
			else
			{
				txtDOB3.Visible = false;
				txtDOB3.Text = "";
			}
		}

		private void vsResults_DblClick(object sender, System.EventArgs e)
		{
			if (vsResults.MouseRow > 0 && !blnLoadingResult)
			{
				blnLoadingResult = true;
				GetFleetInfo();
				blnLoadingResult = false;
			}
		}

		private void GetFleetInfo()
		{
			int counter;
			if (vsResults.TextMatrix(vsResults.Row, GroupFleetCol) == "G")
			{
				for (counter = 1; counter <= vsGroups.Rows - 1; counter++)
				{
					if (Conversion.Val(vsGroups.TextMatrix(counter, FleetGroupNumberCol)) == Conversion.Val(vsResults.TextMatrix(vsResults.Row, FleetNumberCol)))
					{
						vsGroups.Row = counter;
						vsGroups_DblClick(null, null);
						break;
					}
				}
			}
			else
			{
				for (counter = 1; counter <= vsFleets.Rows - 1; counter++)
				{
					if (Conversion.Val(vsFleets.TextMatrix(counter, FleetGroupNumberCol)) == Conversion.Val(vsResults.TextMatrix(vsResults.Row, FleetNumberCol)))
					{
						vsFleets.Row = counter;
						vsFleets_DblClick(null, null);
						break;
					}
				}
			}
			txtSearchFleet.Text = "";
			fraFleetSearchResults.Visible = false;
		}

		private void vsVehicles_ClickEvent(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) == true)
			{
				vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(false));
			}
			else
			{
				vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(true));
			}
		}

		private void vsVehicles_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) == true)
				{
					vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(false));
				}
				else
				{
					vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(true));
				}
			}
		}

		private void FillFleetData()
		{
			int counter;
			vsVehicles.Clear();
			rsVehicleAdd.MoveLast();
			rsVehicleAdd.MoveFirst();
			vsVehicles.Rows = rsVehicleAdd.RecordCount() + 1;
			counter = 1;
			//vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsVehicles.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsVehicles.ColDataType(RegisterCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsVehicles.TextMatrix(0, RegisterCol, "Terminate");
			vsVehicles.TextMatrix(0, ClassCol, "Class");
			vsVehicles.TextMatrix(0, PlateCol, "Plate");
			vsVehicles.TextMatrix(0, VINCol, "VIN");
			vsVehicles.TextMatrix(0, YearCol, "Year");
			vsVehicles.TextMatrix(0, MakeCol, "Make");
			vsVehicles.TextMatrix(0, ModelCol, "Model");
			if (txtEpiryMonth.Enabled == false)
			{
				vsVehicles.TextMatrix(0, UnitCol, "Expires");
				vsVehicles.ColDataType(UnitCol, FCGrid.DataTypeSettings.flexDTDate);
			}
			else
			{
				vsVehicles.TextMatrix(0, UnitCol, "Unit");
				vsVehicles.ColDataType(UnitCol, FCGrid.DataTypeSettings.flexDTString);
			}
			vsVehicles.TextMatrix(0, OwnerCol, "Owner");
			do
			{
				vsVehicles.TextMatrix(counter, 0, FCConvert.ToString(rsVehicleAdd.Get_Fields_Int32("ID")));
				vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(false));
				vsVehicles.TextMatrix(counter, ClassCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("Class")));
				vsVehicles.TextMatrix(counter, PlateCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("plate")));
				vsVehicles.TextMatrix(counter, OwnerCol, fecherFoundation.Strings.Trim(MotorVehicle.GetPartyName(rsVehicleAdd.Get_Fields_Int32("PartyID1"))));
				vsVehicles.TextMatrix(counter, VINCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("Vin")));
				vsVehicles.TextMatrix(counter, MakeCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("make")));
				vsVehicles.TextMatrix(counter, ModelCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("model")));
				vsVehicles.TextMatrix(counter, YearCol, FCConvert.ToString(rsVehicleAdd.Get_Fields("Year")));
				if (txtEpiryMonth.Enabled == false)
				{
					if (!(rsVehicleAdd.IsFieldNull("ExpireDate")))
					{
						vsVehicles.TextMatrix(counter, UnitCol, Strings.Format(rsVehicleAdd.Get_Fields_DateTime("ExpireDate"), "MM/dd/yy"));
					}
				}
				else
				{
					vsVehicles.TextMatrix(counter, UnitCol, FCConvert.ToString(rsVehicleAdd.Get_Fields_String("UnitNumber")));
				}
				counter += 1;
				rsVehicleAdd.MoveNext();
			}
			while (rsVehicleAdd.EndOfFile() != true);
			vsVehicles.Rows = counter;
			//FC:FINAL:DDU:design part already manages this
			//if (vsVehicles.Rows <= 17)
			//{
			//	vsVehicles.Height = vsVehicles.Rows * vsVehicles.RowHeight(0) + 75;
			//}
			//else
			//{
			//	vsVehicles.Height = 17 * vsVehicles.RowHeight(0) + 75;
			//}
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList;
			string strPerm = "";
			int lngChild = 0;
			clsChildList = new clsDRWrapper();
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case MotorVehicle.ADDFLEET:
						{
							if (strPerm == "N")
							{
								cmdAddNewFleet.Enabled = false;
							}
							else
							{
								cmdAddNewFleet.Enabled = true;
							}
							break;
						}
					case MotorVehicle.AddGroup:
						{
							if (strPerm == "N")
							{
								cmdAddNewGroup.Enabled = false;
							}
							else
							{
								cmdAddNewGroup.Enabled = true;
							}
							break;
						}
					case MotorVehicle.DELETEFLEETGROUP:
						{
							if (strPerm == "N")
							{
								cmdDeleteFleet.Enabled = false;
							}
							else
							{
								cmdDeleteFleet.Enabled = true;
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
		}

		private void cmdReg1Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID;
			lngID = FCConvert.ToInt32(txtReg1PartyID.Text);
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtReg1PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 1);
			}
			cmdReg1Edit.Focus();
		}

		private void cmdReg1Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtReg1PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 1);
			}
			cmdReg1Search.Focus();
		}

		private void cmdReg2Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID;
			lngID = FCConvert.ToInt32(txtReg2PartyID.Text);
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtReg2PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 2);
			}
			cmdReg2Edit.Focus();
		}

		private void cmdReg2Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtReg2PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 2);
			}
			cmdReg2Search.Focus();
		}

		private void cmdReg3Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID;
			lngID = FCConvert.ToInt32(txtReg3PartyID.Text);
			lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
			if (lngID > 0)
			{
				txtReg3PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 3);
			}
			cmdReg3Edit.Focus();
		}

		private void cmdReg3Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtReg3PartyID.Text = FCConvert.ToString(lngID);
				GetPartyInfo(lngID, 3);
			}
			cmdReg3Search.Focus();
		}

		private void txtReg1PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReg1PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtReg1PartyID.Text) && Conversion.Val(txtReg1PartyID.Text) > 0)
			{
				GetPartyInfo(FCConvert.ToInt32(FCConvert.ToDouble(txtReg1PartyID.Text)), 1);
			}
			else
			{
				ClearPartyInfo(1);
			}
		}

		private void txtReg2PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReg2PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtReg2PartyID.Text) && Conversion.Val(txtReg2PartyID.Text) > 0)
			{
				GetPartyInfo(FCConvert.ToInt32(FCConvert.ToDouble(txtReg2PartyID.Text)), 2);
			}
			else
			{
				ClearPartyInfo(2);
			}
		}

		private void txtReg3PartyID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReg3PartyID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtReg3PartyID.Text) && Conversion.Val(txtReg3PartyID.Text) > 0)
			{
				GetPartyInfo(FCConvert.ToInt32(FCConvert.ToDouble(txtReg3PartyID.Text)), 3);
			}
			else
			{
				ClearPartyInfo(3);
			}
		}

		public void ClearPartyInfo(int intReg)
		{
			switch (intReg)
			{
				case 1:
					{
						lblRegistrant1.Text = "";
						txtDOB1.Text = "";
						imgReg1Comments.Visible = false;
						cmdReg1Edit.Enabled = false;
						break;
					}
				case 2:
					{
						lblRegistrant2.Text = "";
						txtDOB2.Text = "";
						imgReg2Comments.Visible = false;
						cmdReg2Edit.Enabled = false;
						cmdReg2Search.Enabled = txtOwner2Code.Text  != "N" && !String.IsNullOrEmpty(txtOwner2Code.Text);
						txtReg2PartyID.Enabled = txtOwner2Code.Text != "N" && !String.IsNullOrEmpty(txtOwner2Code.Text);
						break;
					}
				case 3:
					{
						lblRegistrant3.Text = "";
						txtDOB3.Text = "";
						imgReg3Comments.Visible = false;
						cmdReg3Edit.Enabled = false;
						cmdReg3Search.Enabled = txtOwner3Code.Text != "N" && !String.IsNullOrEmpty(txtOwner3Code.Text);
						txtReg3PartyID.Enabled = txtOwner3Code.Text != "N" && !String.IsNullOrEmpty(txtOwner3Code.Text);
						break;
					}
				default:
					{
						// do nothing
						break;
					}
			}
			//end switch
		}

		public void GetPartyInfo(int lngPartyID, int intReg)
		{
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			cPartyAddress pAdd;
			FCCollection pComments = new FCCollection();
			pInfo = pCont.GetParty(lngPartyID);
			if (!(pInfo == null))
			{
				pComments = pInfo.GetModuleSpecificComments("MV");
				switch (intReg)
				{
					case 1:
						{
							lblRegistrant1.Text = fecherFoundation.Strings.UCase(pInfo.FullName);
							if (pComments.Count > 0)
							{
								imgReg1Comments.Visible = true;
							}
							else
							{
								imgReg1Comments.Visible = false;
							}
							cmdReg1Edit.Enabled = true;

							if (txtAddress.Text.Trim() == "" && txtAddress2.Text.Trim() == "")
							{
								FillAddressFields(lngPartyID);
							}
							break;
						}
					case 2:
						{
							lblRegistrant2.Text = fecherFoundation.Strings.UCase(pInfo.FullName);
							if (pComments.Count > 0)
							{
								imgReg2Comments.Visible = true;
							}
							else
							{
								imgReg2Comments.Visible = false;
							}
							cmdReg2Edit.Enabled = true;
							break;
						}
					case 3:
						{
							lblRegistrant3.Text = fecherFoundation.Strings.UCase(pInfo.FullName);
							if (pComments.Count > 0)
							{
								imgReg3Comments.Visible = true;
							}
							else
							{
								imgReg3Comments.Visible = false;
							}
							cmdReg3Edit.Enabled = true;
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
				//end switch
			}
		}

		private void Image1_Click(object sender, System.EventArgs e)
		{
			mnuFileComments_Click();
		}

		private void mnuFileComments_Click(object sender, System.EventArgs e)
		{
			if (!frmCommentOld.InstancePtr.Init("MV", "FleetMaster", "Comment", "FleetNumber", FCConvert.ToInt32(Conversion.Val(txtFleetNumber.Text)), boolModal: true))
			{
				Image1.Visible = true;
			}
			else
			{
				Image1.Visible = false;
			}
		}

		public void mnuFileComments_Click()
		{
			mnuFileComments_Click(mnuFileComments, new System.EventArgs());
		}

		private void cmbFilterAnnual_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFilterAnnual.Text == "Annual")
			{
				optFilterAnnual_CheckedChanged(sender, e);
			}
			else if (cmbFilterAnnual.Text == "Both")
			{
				optFilterBoth_CheckedChanged(sender, e);
			}
			else if (cmbFilterAnnual.Text == "Long Term")
			{
				optFilterLongTerm_CheckedChanged(sender, e);
			}
		}

		private void cmbAnnual_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAnnual.Text == "Annual")
			{
				optAnnual_CheckedChanged(sender, e);
			}
			else if (cmbAnnual.Text == "Long Term")
			{
				optLongTerm_CheckedChanged(sender, e);
			}
		}

		private void cmbOwner_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1871: wrong comparing
			//if (cmbOwner.SelectedIndex == 0)
			if (cmbOwner.SelectedIndex > -1)
			{
				optOwner_CheckedChanged(sender, e);
			}
		}

		private void FillAddressFields(int partyId)
		{
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			cPartyAddress pAdd;

			pInfo = pCont.GetParty(partyId);
			if (!(pInfo == null))
			{
				pAdd = pInfo.GetPrimaryAddress();
				if (pAdd != null)
				{
					txtAddress.Text = pAdd.Address1;
					txtAddress2.Text = pAdd.Address2;
					txtCity.Text = pAdd.City;
					txtState.Text = pAdd.State;
					txtZip.Text = pAdd.Zip;
				}
			}
		}
	}
}
