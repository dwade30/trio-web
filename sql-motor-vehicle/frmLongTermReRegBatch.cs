//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmLongTermReRegBatch : BaseForm
	{
		public frmLongTermReRegBatch()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;

            cmbPlateOption.SelectedIndexChanged += CmbPlateOption_SelectedIndexChanged;
		}

        private void CmbPlateOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbPlateOption.ListIndex > 0)
            {
                if (!useSamePlate)
                {
                    useSamePlate = true;
                    FillFleetRegVehicles();
                }
            }
            else
            {
                if (useSamePlate)
                {
                    useSamePlate = false;
                    FillFleetRegVehicles();
                }
            }
        }

        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmLongTermReRegBatch InstancePtr
		{
			get
			{
				return (frmLongTermReRegBatch)Sys.GetInstance(typeof(frmLongTermReRegBatch));
			}
		}

		protected frmLongTermReRegBatch _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int intStep;
		int RegisterCol;
		int PlateCol;
		int VINCol;
		int YearCol;
		int MakeCol;
		int ModelCol;
		int UnitCol;
		int CurrentExpireDateCol;
		int NetWeightCol;
		int IDCol;
		public int SelectPlateCol;
		public int SelectVINCol;
		public int SelectYearCol;
		public int SelectMakeCol;
		int SelectModelCol;
		public int SelectUnitCol;
		public int SelectLengthCol;
		public int SelectTotalCol;
		public int SelectExpiresCol;
		public int SelectCurrentExpireDateCol;
		public int SelectNetWeightCol;
		public int SelectPrintedCol;
		public int SelectNewPlateCol;
		public int SelectIDCol;
		public int SelectUniqueIdentifierCol;
		clsDRWrapper rsDefaultInfo = new clsDRWrapper();
		public Decimal curAgentFee;
		bool blnBatchPrinted;
		bool blnPartiallyPrinted;
		public int intStartRow;
		// vbPorter upgrade warning: intNumberToPrint As int	OnWrite(double, int)
		public int intNumberToPrint;
        private bool useSamePlate = false;
		private void cboLength_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsRegLength.Rows - 1); counter++)
			{
				vsRegLength.TextMatrix(counter, SelectLengthCol, cboLength.ItemData(cboLength.ListIndex));
				AdjustVehicleLength(counter);
			}
			CalculateTotalDue();
		}

		public void cboLength_Click()
		{
			cboLength_SelectedIndexChanged(cboLength, new System.EventArgs());
		}
		// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
		private void AdjustVehicleLength(int intRow, int intLength = -1)
		{
			// vbPorter upgrade warning: datExpiresDate As DateTime	OnWrite(string, DateTime)
			DateTime datExpiresDate;
			datExpiresDate = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(FCConvert.ToDateTime(vsRegLength.TextMatrix(intRow, SelectCurrentExpireDateCol)).Year));
			if (intLength > -1)
			{
				if (intLength == 24)
				{
					datExpiresDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(FCConvert.ToDateTime(vsRegLength.TextMatrix(intRow, SelectCurrentExpireDateCol)).Year + 1));
				}
				else
				{
					datExpiresDate = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(FCConvert.ToDateTime(vsRegLength.TextMatrix(intRow, SelectCurrentExpireDateCol)).Year));
				}
				datExpiresDate = fecherFoundation.DateAndTime.DateAdd("yyyy", intLength, datExpiresDate);
			}
			else
			{
				if (FCConvert.ToInt16(FCConvert.ToDouble(fecherFoundation.Strings.Trim(Strings.Left(vsRegLength.TextMatrix(intRow, SelectLengthCol), 2)))) == 24)
				{
					datExpiresDate = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(FCConvert.ToDateTime(vsRegLength.TextMatrix(intRow, SelectCurrentExpireDateCol)).Year + 1));
				}
				else
				{
					datExpiresDate = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(FCConvert.ToDateTime(vsRegLength.TextMatrix(intRow, SelectCurrentExpireDateCol)).Year));
				}
				datExpiresDate = fecherFoundation.DateAndTime.DateAdd("yyyy", FCConvert.ToInt16(FCConvert.ToDouble(fecherFoundation.Strings.Trim(Strings.Left(vsRegLength.TextMatrix(intRow, SelectLengthCol), 2)))), datExpiresDate);
			}
			datExpiresDate = fecherFoundation.DateAndTime.DateAdd("d", -1, datExpiresDate);
			vsRegLength.TextMatrix(intRow, SelectExpiresCol, Strings.Format(datExpiresDate, "MM/dd/yyyy"));
			if (intLength > -1)
			{
				Recalculate(intRow, intLength);
			}
			else
			{
				Recalculate(intRow);
			}
		}

		private void CalculateTotalDue()
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(int, Decimal)
			Decimal curTotal;
			curTotal = 0;
			for (counter = 1; counter <= (vsRegLength.Rows - 1); counter++)
			{
				curTotal += FCConvert.ToDecimal(vsRegLength.TextMatrix(counter, SelectTotalCol));
			}
			lblTotalDue.Text = Strings.Format(curTotal, "#,##0.00");
		}

		private void cmdCancelReprint_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			fraReprint.Visible = false;
			blnPartiallyPrinted = false;
			blnBatchPrinted = true;
			for (intCounter = 1; intCounter <= (vsRegLength.Rows - 1); intCounter++)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsRegLength.TextMatrix(intCounter, SelectPrintedCol)) == true)
				if (FCConvert.CBool(vsRegLength.TextMatrix(intCounter, SelectPrintedCol)) == true)
				{
					blnPartiallyPrinted = true;
				}
				else
				{
					blnBatchPrinted = false;
				}
			}
			if (blnBatchPrinted)
			{
				blnPartiallyPrinted = false;
			}
			if (blnBatchPrinted)
			{
				cmdPrint.Enabled = true;
				cmdSave.Visible = true;
				cmdQueue.Visible = false;
			}
			else
			{
				if (blnPartiallyPrinted == false)
				{
					mnuProcessQuit.Enabled = false;
					//cmdCancel.Enabled = true;
					vsRegLength.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					cmdBack.Enabled = true;
					cmdQueue.Enabled = true;
				}
				cmdPrint.Enabled = true;
			}
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			DialogResult answer = 0;
			int intCounter;
			try
			{
				if (fecherFoundation.Strings.Trim(txtLow.Text) == "" || fecherFoundation.Strings.Trim(txtHigh.Text) == "")
				{
					MessageBox.Show("You must enter a low and a high vehicle number before you may continue.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}

                if (Conversion.Val(txtLow.Text) < 1 || Conversion.Val(txtHigh.Text) < 1)
                {
                    MessageBox.Show("You must enter a low and a high vehicle number that are greater than or equal to 1.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (Conversion.Val(txtLow.Text) > Conversion.Val(vsRegLength.TextMatrix(vsRegLength.Rows - 1, 0)) || Conversion.Val(txtHigh.Text) > Conversion.Val(vsRegLength.TextMatrix(vsRegLength.Rows - 1, 0)))
                {
                    MessageBox.Show("You must enter a low and a high vehicle number that are less than or equal to the number of vehicles you are trying to register.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (Conversion.Val(txtLow.Text) > Conversion.Val(txtHigh.Text))
                {
                    MessageBox.Show("The high vehicle number must be greater than the low number.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                fraReprint.Visible = false;
				intStartRow = FCConvert.ToInt32(Math.Round(Conversion.Val(txtLow.Text)));
				intNumberToPrint = FCConvert.ToInt16((Conversion.Val(txtHigh.Text) - intStartRow) + 1);
				if (!MotorVehicle.Statics.blnLongTermLaserForms)
				{
					if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to the top as possible) next to the line titled:" + "\r\n" + "STATE OF MAINE", 23, 1, MotorVehicle.Statics.MVR10PrinterName) == DialogResult.Cancel)
					{
						MotorVehicle.Statics.PrintMVR3 = false;
					}
					else
					{
                        rptMVRT10.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                    }
				}
				else
				{
                    rptMVRT10Laser.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                }
				if (!MotorVehicle.Statics.blnLongTermLaserForms)
				{
					rptMVRT10.InstancePtr.Unload();
				}
				else
				{
					rptMVRT10Laser.InstancePtr.Unload();
				}
				if (MotorVehicle.Statics.PrintMVR3 == true)
				{
					if (fecherFoundation.Information.Err().Number == 0)
					{
						mnuProcessQuit.Enabled = false;
					}
				}
				else
				{
					cmdPrint.Enabled = true;
					return;
				}
				blnPartiallyPrinted = false;
				blnBatchPrinted = true;
				for (intCounter = 1; intCounter <= (vsRegLength.Rows - 1); intCounter++)
				{
					if (FCConvert.CBool(vsRegLength.TextMatrix(intCounter, SelectPrintedCol)) == true)
					{
						blnPartiallyPrinted = true;
					}
					else
					{
						blnBatchPrinted = false;
					}
				}
				if (blnBatchPrinted)
				{
					blnPartiallyPrinted = false;
				}
				if (blnBatchPrinted)
				{
					cmdPrint.Enabled = true;
					cmdSave.Visible = true;
					cmdQueue.Visible = false;
				}
				else
				{
					if (blnPartiallyPrinted == false)
					{
						mnuProcessQuit.Enabled = false;
						//cmdCancel.Enabled = true;
						vsRegLength.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						cmdBack.Enabled = true;
						cmdQueue.Enabled = true;
					}
					cmdPrint.Enabled = true;
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description, "Error Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				blnPartiallyPrinted = false;
				blnBatchPrinted = true;
				for (intCounter = 1; intCounter <= (vsRegLength.Rows - 1); intCounter++)
				{
					if (FCConvert.CBool(vsRegLength.TextMatrix(intCounter, SelectPrintedCol)) == true)
					{
						blnPartiallyPrinted = true;
					}
					else
					{
						blnBatchPrinted = false;
					}
				}
				if (blnBatchPrinted)
				{
					blnPartiallyPrinted = false;
				}
				if (blnBatchPrinted)
				{
					cmdPrint.Enabled = true;
					cmdSave.Visible = true;
					cmdQueue.Visible = false;
				}
				else
				{
					if (blnPartiallyPrinted == false)
					{
						mnuProcessQuit.Enabled = false;
						//cmdCancel.Enabled = true;
						vsRegLength.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						cmdBack.Enabled = true;
						cmdQueue.Enabled = true;
					}
					cmdPrint.Enabled = true;
				}
			}
		}

		private void cmdBack_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.Statics.gboolReRegBatchStarted = false;
			intStep -= 1;
			ShowStep();
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
			{
				vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(false));
			}
		}

		private void cmdNext_Click(object sender, System.EventArgs e)
		{
			if (intStep == 1)
			{
				if (cboFleets.SelectedIndex >= 0)
				{
					FillFleetRegVehicles();
				}
				else
				{
					MessageBox.Show("You must select a fleet / group before you may proceed.", "Select Fleet / Group", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				MotorVehicle.Statics.RegistrationType = "RRR";
				MotorVehicle.Statics.gboolReRegBatchStarted = true;
                if (cmbPlateOption.ListIndex == 0)
                {
                    useSamePlate = false;
                }
                else
                {
                    useSamePlate = true;
                }
                FillLengthCombo(cmbPlateOption.ListIndex);
				FillFleetLengthGrid();
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			PrintMVR10Batch();
		}

		private void cmdQueue_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsAM = new clsDRWrapper();
			int intLockCount = 0;
			DialogResult intChoice = 0;
			int intRndCount = 0;
			int x;
			clsDRWrapper rsUpdate = new clsDRWrapper();
			int counter;
			clsDRWrapper rsInventory = new clsDRWrapper();
			string strReturnedPlate = "";
			int counter2;
			clsDRWrapper rsFleet = new clsDRWrapper();
            int intFleetNumber = 0;

            try
            {
                // On Error GoTo ShowLineNumber
                fecherFoundation.Information.Err().Clear();
                this.Hide();

                if (cboFleets.ListIndex >= 0)
                {
                    intFleetNumber = cboFleets.ItemData(cboFleets.ListIndex);
                }

                if (!useSamePlate)
                {
                    for (counter = 1; counter <= (vsRegLength.Rows - 1); counter++)
                    {
                        if (vsRegLength.TextMatrix(counter, SelectNewPlateCol) != "") 
                            continue;

                        strReturnedPlate = MotorVehicle.GetLongTermPlate(FCConvert
                                                                        .ToDateTime(vsRegLength.TextMatrix(counter, SelectExpiresCol))
                                                                        .Year);

                        if (strReturnedPlate == "")
                        {
                            for (counter2 = 1; counter2 <= counter - 1; counter2++)
                            {
                                MotorVehicle.ReturnLongTermPlate(vsRegLength.TextMatrix(counter2, SelectNewPlateCol));
                                vsRegLength.TextMatrix(counter2, SelectNewPlateCol, "");
                            }

                            this.Show(App.MainForm);

                            return;
                        }
                        else
                        {
                            vsRegLength.TextMatrix(counter, SelectNewPlateCol, strReturnedPlate);
                        }
                    }
                }

                for (counter = 1; counter <= (vsRegLength.Rows - 1); counter++)
                {
                    MotorVehicle.Statics.rsFinal.OpenRecordset("SELECT * FROM Master WHERE ID = " + vsRegLength.TextMatrix(counter, SelectIDCol), "TWMV0000.vb1");
                    clsDRWrapper temp = MotorVehicle.Statics.rsFinal;
                    MotorVehicle.TransferRecordToArchive(ref temp, 0);
                    MotorVehicle.Statics.rsFinal = temp;
                    rsFleet.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE Deleted <> 1 AND Type = 'L' AND FleetNumber = " + rsFleet.Get_Fields_Int32("FleetNumber") + " ORDER BY p.FullName");
                    MotorVehicle.Statics.rsFinal.Edit();

                    if (!useSamePlate)
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldClass", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("Plate", vsRegLength.TextMatrix(counter, SelectNewPlateCol));
                        MotorVehicle.Statics.rsFinal.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
                        MotorVehicle.Statics.rsFinal.Set_Fields("PlateType", "N");
                    }
                    else
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("PlateType", "S");
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldClass", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                    }

                    MotorVehicle.Statics.rsFinal.Set_Fields("TransactionType", "RRR");
                    MotorVehicle.Statics.rsFinal.Set_Fields("EffectiveDate", DateTime.Today);
                    MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", vsRegLength.TextMatrix(counter, SelectExpiresCol));
                    MotorVehicle.Statics.rsFinal.Set_Fields("ReReg", "Y");
                    MotorVehicle.Statics.rsFinal.Set_Fields("Transfer", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("FleetNew", false);
                    MotorVehicle.Statics.rsFinal.Set_Fields("DoubleCTANumber", "");
                    MotorVehicle.Statics.rsFinal.Set_Fields("OpID", MotorVehicle.Statics.OpID);
                    MotorVehicle.Statics.rsFinal.Set_Fields("AgentFee", curAgentFee);
                    MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeCTA", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeTransfer", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeCorrection", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("RushTitleFee", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", FCConvert.ToDecimal(vsRegLength.TextMatrix(counter, SelectTotalCol)) - curAgentFee);
                    MotorVehicle.Statics.rsFinal.Set_Fields("RegRateFull", FCConvert.ToDecimal(vsRegLength.TextMatrix(counter, SelectTotalCol)) - curAgentFee);
                    MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditFull", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("CommercialCredit", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("InitialFee", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("outofrotation", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlate", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlateYN", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("StickerFee", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("TransferFee", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 0);
                    MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid", FCConvert.ToDecimal(vsRegLength.TextMatrix(counter, SelectTotalCol)) - curAgentFee);
                    MotorVehicle.Statics.rsFinal.Set_Fields("LocalPaid", curAgentFee);
                    MotorVehicle.Statics.rsFinal.Set_Fields("ForcedPlate", "N");
                    MotorVehicle.Statics.rsFinal.Set_Fields("TitleNumber", "");
                    MotorVehicle.Statics.rsFinal.Set_Fields("Leased", "N");

                    var muniName = fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName);

                    switch (muniName)
                    {
                        case "MAINE MOTOR TRANSPORT":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "MMTA SERVICES INC. 44004");

                            break;
                        case "ACE REGISTRATION SERVICES LLC":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "ACE REGISTRATION");
                            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", "ME");
                            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", "44021");

                            break;
                        case "COUNTRYWIDE TRAILER":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "COUNTRYWIDE TRAILER 44018");

                            break;
                        case "AB LEDUE ENTERPRISES":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "AB LEDUE ENTERPRISES");
                            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", "ME");
                            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", "44003");

                            break;
                        case "MAINE TRAILER":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "MAINE TRAILER ME 44005");

                            break;
                        case "HASKELL REGISTRATION":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Reisdence", "HASKELL REGISTRATION 44002");

                            break;
                    }

                    MotorVehicle.Statics.rsFinal.Set_Fields("Address", fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields_String("Address"))));
                    MotorVehicle.Statics.rsFinal.Set_Fields("City", fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields_String("City"))));
                    MotorVehicle.Statics.rsFinal.Set_Fields("State", fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields("State"))));

                    if (!fecherFoundation.FCUtils.IsNull(rsFleet.Get_Fields_String("Zip")))
                    {
                        if (FCConvert.ToString(rsFleet.Get_Fields_String("Zip")).Length < 5)
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("Zip", "0" + rsFleet.Get_Fields_String("Zip"));
                        }
                        else
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("Zip", rsFleet.Get_Fields_String("Zip"));
                        }
                    }
                    else
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("Zip", "");
                    }

                    MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode1", "C");
                    MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", rsFleet.Get_Fields_Int32("PartyID1"));

                    if (Conversion.Val(rsFleet.Get_Fields_Int32("PartyID2")) != 0)
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode2", "C");
                        MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", rsFleet.Get_Fields_Int32("PartyID2"));
                    }
                    else
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode2", "N");
                        MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", 0);
                    }

                    if (FCConvert.ToInt16(FCConvert.ToDouble(fecherFoundation.Strings.Trim(Strings.Left(vsRegLength.TextMatrix(counter, SelectLengthCol), 2)))) == 24)
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", true);
                    }
                    else
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", false);
                    }

                    rsAM.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = 0");
                    rsAM.AddNew();
                    rsAM.Set_Fields("RegistrationType", MotorVehicle.Statics.RegistrationType);
                    rsAM.Set_Fields("Extension", useSamePlate);
                    rsAM.Set_Fields("DateUpdated", DateTime.Now);
                    rsAM.Set_Fields("TitleDone", false);
                    rsAM.Set_Fields("UseTaxDone", false);
                    rsAM.Set_Fields("Make", MotorVehicle.Statics.rsFinal.Get_Fields_String("Make"));
                    rsAM.Set_Fields("Unit", MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
                    rsAM.Set_Fields("Year", MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
                    rsAM.Set_Fields("Style", MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
                    rsAM.Set_Fields("Color", MotorVehicle.Statics.rsFinal.Get_Fields_String("Color1") + MotorVehicle.Statics.rsFinal.Get_Fields_String("Color2"));
                    rsAM.Set_Fields("MaineReregistration", true);
                    rsAM.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    rsAM.Set_Fields("VIN", MotorVehicle.Statics.rsFinal.Get_Fields_String("VIN"));
                    rsAM.Set_Fields("TitleNumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber").ToUpper());
                    rsAM.Set_Fields("NetWeight", FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"))));
                    rsAM.Set_Fields("PartyID1", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"));
                    rsAM.Set_Fields("PartyID2", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"));
                    rsAM.Set_Fields("Address1", MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
                    rsAM.Set_Fields("City", MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
                    rsAM.Set_Fields("State", MotorVehicle.Statics.rsFinal.Get_Fields("State"));
                    rsAM.Set_Fields("Zip", MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
                    rsAM.Set_Fields("FleetNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
                    rsAM.Set_Fields("OldPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate"));
                    rsAM.Set_Fields("LegalResAddress", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResAddress"));
                    rsAM.Set_Fields("LegalResCity", MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
                    rsAM.Set_Fields("LegalResState", MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
                    rsAM.Set_Fields("LegalResZip", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResZip"));
                    rsAM.Set_Fields("ExpireDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
                    rsAM.Set_Fields("EffectiveDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"));
                    rsAM.Set_Fields("NumberOfYears", FCConvert.ToInt16(FCConvert.ToDouble(fecherFoundation.Strings.Trim(Strings.Left(vsRegLength.TextMatrix(counter, SelectLengthCol), 2)))));
                    rsAM.Set_Fields("RegistrationFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull"));
                    rsAM.Set_Fields("RegistrationCredit", 0);
                    rsAM.Set_Fields("TitleFee", 0);
                    rsAM.Set_Fields("RushTitleFee", 0);
                    rsAM.Set_Fields("SalesTax", 0);
                    rsAM.Set_Fields("StateFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull"));
                    rsAM.Set_Fields("AgentFee", curAgentFee);
                    rsAM.Set_Fields("AgentFeeTitle", 0);
                    rsAM.Set_Fields("TransferFee", 0);
                    rsAM.Set_Fields("Status", "A");
                    rsAM.Set_Fields("TimePayment", false);
                    rsAM.Set_Fields("TwentyFiveYearPlate", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwentyFiveYearPlate"));
                    MotorVehicle.Statics.rsFinal.Set_Fields("DateUpdated", rsAM.Get_Fields_DateTime("DateUpdated"));
                    MotorVehicle.Statics.rsFinal.Set_Fields("ArchiveID", MotorVehicle.Statics.FinalCompareID);

                    if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
                    {
                        MotorVehicle.Write_PDS_Work_Record_Stuff_MVRT10();
                    }

                    rsAM.Update();
                    MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegID", rsAM.Get_Fields_Int32("ID"));
                    MotorVehicle.Statics.strCodeP = "PXSLT";
                    rsUpdate.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
                    rsUpdate.AddNew();
                    rsUpdate.Set_Fields("AdjustmentCode", "I");
                    rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                    rsUpdate.Set_Fields("QuantityAdjusted", 1);
                    rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeP);
                    rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "- MVR10 Form");
                    rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                    rsUpdate.Set_Fields("TellerCloseoutID", 0);
                    rsUpdate.Update();
                    MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND isnull(FormattedInventory,'')  = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
                    rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);

                    if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                    {
                        rsInventory.Edit();

                        if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                        {
                            rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                        }

                        rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                        rsInventory.Set_Fields("Pending", false);
                        rsInventory.Set_Fields("Status", "I");
                        rsInventory.Set_Fields("PeriodCloseoutID", 0);
                        rsInventory.Set_Fields("TellerCloseoutID", 0);
                        rsInventory.Update();
                    }

                    MotorVehicle.Statics.rsFinal.Set_Fields("Inactive", false);
                    MotorVehicle.Statics.rsFinal.Update();
                    temp = MotorVehicle.Statics.rsFinal;
                    MotorVehicle.TransferRecordToPending(ref temp);
                    MotorVehicle.Statics.rsFinal = temp;
                }

                if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
                {
                    MotorVehicle.Write_Fleet_PDS_Work_Record_Stuff();
                }

                //App.DoEvents();
                MotorVehicle.Statics.rsFinalCompare.Reset();

                MotorVehicle.Statics.MVR3saved = fecherFoundation.Information.Err().Number == 0;

                MotorVehicle.ClearAllVariables();
                frmWait.InstancePtr.Unload();
                frmDataInput.InstancePtr.Unload();
                frmDataInputLongTermTrailers.InstancePtr.Unload();
                frmPreview.InstancePtr.Unload();

                if (!MotorVehicle.Statics.bolFromDosTrio && (!MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet))
                {
                    //frmPlateInfo.InstancePtr.Show(App.MainForm);
                    frmPlateInfo.InstancePtr.ShowForm();
                }
                else
                    if (MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet)
                    {
                        //App.DoEvents();
                        //MDIParent.InstancePtr.GetMainMenu();
                        MDIParent.InstancePtr.Menu18();
                        Close();

                        //Application.Exit();
                        App.MainForm.EndWaitMVModule();

                        return;
                    }

                frmSavePrint.InstancePtr.Unload();
                Close();

                return;
            }
            catch (Exception ex)
            {
				StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "An error occurred on line " + fecherFoundation.Information.Erl(), "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                /*? Resume FailedUpdate; */
                SaveError: ;

                switch (fecherFoundation.Information.Err(ex).Number)
                {
                    case 3260:
                    {
                        intLockCount += 1;

                        if (intLockCount > 5)
                        {
                            intChoice = MessageBox.Show("Error Trying to Save.  This record is currently locked by another user.  Would you like to try to save again?", "Record Locked", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question);

                            if (intChoice == DialogResult.Retry)
                            {
                                intLockCount = 1;
                            }
                        }

                        
                        break;
                    }
                    default:
                    {
                        MessageBox.Show("Error: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "A file called SaveErr.txt has been created in the " + Environment.CurrentDirectory + " directory of your server.  Please notify TRIO of your problem and E-mail the SaveErr.txt file so we may more quickly find the problem you are encountering.", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);

                        /*? Resume FailedUpdate; */
                        break;
                    }
                }

                //end switch
                FailedUpdate: ;
                MotorVehicle.ClearAllVariables();
                frmDataInput.InstancePtr.Unload();
                frmPreview.InstancePtr.Unload();
                frmSavePrint.InstancePtr.Unload();
                Close();
                MotorVehicle.Statics.rsFinal = null;
                MotorVehicle.Statics.rsFinalCompare = null;

                if (!MotorVehicle.Statics.bolFromDosTrio && !MotorVehicle.Statics.bolFromWindowsCR)
                {
                    frmPlateInfo.InstancePtr.ShowForm();
                }
            }
            finally
            {
                rsAM.DisposeOf();
                rsFleet.DisposeOf();
                rsInventory.DisposeOf();
                rsUpdate.DisposeOf();
            }
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsAM = new clsDRWrapper();
			int fnx;
			double TitleFee;
			int intLockCount = 0;
			DialogResult intChoice = 0;
			int intRndCount = 0;
			int x;
			clsDRWrapper rsTestFleetCTA = new clsDRWrapper();
			clsDRWrapper rsUpdate = new clsDRWrapper();
			int counter;
			clsDRWrapper rsTitle = new clsDRWrapper();
			clsDRWrapper rsInventory = new clsDRWrapper();
			clsDRWrapper rsFleet = new clsDRWrapper();
            int fleetNumber = 0;

			try
			{
				// On Error GoTo ShowLineNumber
				fecherFoundation.Information.Err().Clear();
				this.Hide();
                if (cboFleets.ListIndex >= 0)
                {
                    fleetNumber = cboFleets.ItemData(cboFleets.ListIndex);
                }
				for (counter = 1; counter <= (vsRegLength.Rows - 1); counter++)
				{
					MotorVehicle.Statics.rsFinal.OpenRecordset("SELECT * FROM Master WHERE ID = " + vsRegLength.TextMatrix(counter, SelectIDCol), "TWMV0000.vb1");
					clsDRWrapper temp = MotorVehicle.Statics.rsFinal;
					MotorVehicle.TransferRecordToArchive(ref temp, 0);
					MotorVehicle.Statics.rsFinal = temp;
					rsFleet.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE Deleted <> 1 AND Type = 'L' AND FleetNumber = " + rsFleet.Get_Fields_Int32("FleetNumber") + " ORDER BY p.FullName");
					MotorVehicle.Statics.rsFinal.Edit();
                    if (!useSamePlate)
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldClass", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("Plate", vsRegLength.TextMatrix(counter, SelectNewPlateCol));
                        MotorVehicle.Statics.rsFinal.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
                        MotorVehicle.Statics.rsFinal.Set_Fields("PlateType","N");
                    }
                    else
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("PlateType", "S");
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldClass", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                    }
                    MotorVehicle.Statics.rsFinal.Set_Fields("TransactionType", "RRR");
					MotorVehicle.Statics.rsFinal.Set_Fields("EffectiveDate", DateTime.Today);
					MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", vsRegLength.TextMatrix(counter, SelectExpiresCol));
					MotorVehicle.Statics.rsFinal.Set_Fields("ReReg", "Y");
					MotorVehicle.Statics.rsFinal.Set_Fields("Transfer", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("FleetNew", false);
					MotorVehicle.Statics.rsFinal.Set_Fields("DoubleCTANumber", "");
					MotorVehicle.Statics.rsFinal.Set_Fields("OpID", MotorVehicle.Statics.OpID);
					MotorVehicle.Statics.rsFinal.Set_Fields("AgentFee", curAgentFee);
					MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeCTA", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeTransfer", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("AgentFeeCorrection", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("RushTitleFee", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", FCConvert.ToDecimal(vsRegLength.TextMatrix(counter, SelectTotalCol)) - curAgentFee);
					MotorVehicle.Statics.rsFinal.Set_Fields("RegRateFull", FCConvert.ToDecimal(vsRegLength.TextMatrix(counter, SelectTotalCol)) - curAgentFee);
					MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditFull", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("CommercialCredit", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("InitialFee", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("outofrotation", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlate", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("ReservePlateYN", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("StickerFee", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("TransferFee", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 0);
					MotorVehicle.Statics.rsFinal.Set_Fields("StatePaid", FCConvert.ToDecimal(vsRegLength.TextMatrix(counter, SelectTotalCol)) - curAgentFee);
					MotorVehicle.Statics.rsFinal.Set_Fields("LocalPaid", curAgentFee);
					MotorVehicle.Statics.rsFinal.Set_Fields("ForcedPlate", "N");
					MotorVehicle.Statics.rsFinal.Set_Fields("TitleNumber", "");
					MotorVehicle.Statics.rsFinal.Set_Fields("Leased", "N");
                    
                    var muniName = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName));
					switch (muniName)
                    {
                        case "MAINE MOTOR TRANSPORT":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "MMTA SERVICES INC. 44004");

                            break;
                        case "ACE REGISTRATION SERVICES LLC":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "ACE REGISTRATION");
                            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", "ME");
                            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", "44021");

                            break;
                        
                        case "COUNTRYWIDE TRAILER":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "COUNTRYWIDE TRAILER 44018");

                            break;
                        case "AB LEDUE ENTERPRISES":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "AB LEDUE ENTERPRISES");
                            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", "ME");
                            MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", "44003");

                            break;
                        case "MAINE TRAILER":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "MAINE TRAILER ME 44005");

                            break;
                        case "HASKELL REGISTRATION":
                            MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "HASKELL REGISTRATION 44002");

                            break;
                    }

                    MotorVehicle.Statics.rsFinal.Set_Fields("Address", fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields_String("Address"))));
					MotorVehicle.Statics.rsFinal.Set_Fields("City", fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields_String("City"))));
					MotorVehicle.Statics.rsFinal.Set_Fields("State", fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleet.Get_Fields("State"))));
					if (!fecherFoundation.FCUtils.IsNull(rsFleet.Get_Fields_String("Zip")))
					{
						if (FCConvert.ToString(rsFleet.Get_Fields_String("Zip")).Length < 5)
						{
							MotorVehicle.Statics.rsFinal.Set_Fields("Zip", "0" + rsFleet.Get_Fields_String("Zip"));
						}
						else
						{
							MotorVehicle.Statics.rsFinal.Set_Fields("Zip", rsFleet.Get_Fields_String("Zip"));
						}
					}
					else
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("Zip", "");
					}
					MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode1", "C");
					MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", rsFleet.Get_Fields_Int32("PartyID1"));
					if (Conversion.Val(rsFleet.Get_Fields_Int32("PartyID2")) != 0)
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode2", "C");
						MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", rsFleet.Get_Fields_Int32("PartyID2"));
					}
					else
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("OwnerCode2", "N");
						MotorVehicle.Statics.rsFinal.Set_Fields("PartyID2", 0);
					}
					if (FCConvert.ToInt16(FCConvert.ToDouble(fecherFoundation.Strings.Trim(Strings.Left(vsRegLength.TextMatrix(counter, SelectLengthCol), 2)))) == 24)
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", true);
					}
					else
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", false);
					}
					rsAM.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = 0");
					rsAM.AddNew();
					rsAM.Set_Fields("UniqueIdentifier", FCConvert.ToString(Conversion.Val(vsRegLength.TextMatrix(counter, SelectUniqueIdentifierCol))));
					rsAM.Set_Fields("RegistrationType", MotorVehicle.Statics.RegistrationType);
                    rsAM.Set_Fields("Extension", useSamePlate);
					rsAM.Set_Fields("DateUpdated", DateTime.Now);
					rsAM.Set_Fields("TitleDone", false);
					rsAM.Set_Fields("UseTaxDone", false);
					rsAM.Set_Fields("Make", MotorVehicle.Statics.rsFinal.Get_Fields_String("Make"));
					rsAM.Set_Fields("Unit", MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
					rsAM.Set_Fields("Year", MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
					rsAM.Set_Fields("Style", MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
					rsAM.Set_Fields("Color", MotorVehicle.Statics.rsFinal.Get_Fields_String("Color1") + MotorVehicle.Statics.rsFinal.Get_Fields_String("Color2"));
					rsAM.Set_Fields("MaineReregistration", true);
					rsAM.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
					rsAM.Set_Fields("VIN", MotorVehicle.Statics.rsFinal.Get_Fields_String("VIN"));
					rsAM.Set_Fields("TitleNumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber").ToUpper());
					rsAM.Set_Fields("NetWeight", FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"))));
					rsAM.Set_Fields("PartyID1", MotorVehicle.Statics.rsFinal.Get_Fields("PartyID1"));                   
                    rsAM.Set_Fields("Address1", MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
					rsAM.Set_Fields("City", MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
					rsAM.Set_Fields("State", MotorVehicle.Statics.rsFinal.Get_Fields("State"));
					rsAM.Set_Fields("Zip", MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
					rsAM.Set_Fields("OldPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate"));
					rsAM.Set_Fields("FleetNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
					rsAM.Set_Fields("LegalResAddress", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResAddress"));
					rsAM.Set_Fields("LegalResCity", MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
					rsAM.Set_Fields("LegalResState", MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
					rsAM.Set_Fields("LegalResZip", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResZip"));
					rsAM.Set_Fields("ExpireDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
					rsAM.Set_Fields("EffectiveDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"));
					rsAM.Set_Fields("NumberOfYears", FCConvert.ToInt16(FCConvert.ToDouble(fecherFoundation.Strings.Trim(Strings.Left(vsRegLength.TextMatrix(counter, SelectLengthCol), 2)))));
					rsAM.Set_Fields("RegistrationFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull"));
					rsAM.Set_Fields("RegistrationCredit", 0);
					rsAM.Set_Fields("TitleFee", 0);
					rsAM.Set_Fields("RushTitleFee", 0);
					rsAM.Set_Fields("SalesTax", 0);
					rsAM.Set_Fields("StateFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateFull"));
					rsAM.Set_Fields("AgentFee", curAgentFee);
					rsAM.Set_Fields("AgentFeeTitle", 0);
					rsAM.Set_Fields("TransferFee", 0);
					rsAM.Set_Fields("Status", "A");
					rsAM.Set_Fields("TimePayment", false);
					rsAM.Set_Fields("TwentyFiveYearPlate", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TwentyFiveYearPlate"));
					MotorVehicle.Statics.rsFinal.Set_Fields("DateUpdated", rsAM.Get_Fields_DateTime("DateUpdated"));
					MotorVehicle.Statics.rsFinal.Set_Fields("ArchiveID", MotorVehicle.Statics.FinalCompareID);
					if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
					{
						MotorVehicle.Write_PDS_Work_Record_Stuff_MVRT10();
					}
					rsAM.Update();
					MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegID", rsAM.Get_Fields_Int32("ID"));
					MotorVehicle.Statics.strCodeP = "PXSLT";
					rsUpdate.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
					rsUpdate.AddNew();
					rsUpdate.Set_Fields("AdjustmentCode", "I");
					rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
					rsUpdate.Set_Fields("QuantityAdjusted", 1);
					rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeP);
					rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
					rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
					rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
					rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "- MVR10 Form");
					rsUpdate.Set_Fields("PeriodCloseoutID", 0);
					rsUpdate.Set_Fields("TellerCloseoutID", 0);
					rsUpdate.Update();
					MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND (FormattedInventory,'') = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
					rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);
					if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
					{
						rsInventory.Edit();
						if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
						{
							rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
						}
						rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
						rsInventory.Set_Fields("Pending", false);
						rsInventory.Set_Fields("Status", "I");
						rsInventory.Set_Fields("PeriodCloseoutID", 0);
						rsInventory.Set_Fields("TellerCloseoutID", 0);
						rsInventory.Update();
					}
					MotorVehicle.Statics.rsFinal.Set_Fields("Inactive", false);
					MotorVehicle.Statics.rsFinal.Update();
				}
				if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR)
				{
					MotorVehicle.Write_Fleet_PDS_Work_Record_Stuff();
				}
				//App.DoEvents();
				MotorVehicle.Statics.rsFinalCompare.Reset();
				if (fecherFoundation.Information.Err().Number == 0)
				{
					MotorVehicle.Statics.MVR3saved = true;
				}
				else
				{
					MotorVehicle.Statics.MVR3saved = false;
				}
				MotorVehicle.ClearAllVariables();
				frmWait.InstancePtr.Unload();
				frmDataInput.InstancePtr.Unload();
				frmDataInputLongTermTrailers.InstancePtr.Unload();
				frmPreview.InstancePtr.Unload();
				if (!MotorVehicle.Statics.bolFromDosTrio && (!MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet))
				{
					//frmPlateInfo.InstancePtr.Show(App.MainForm);
					frmPlateInfo.InstancePtr.ShowForm();
				}
				else if (MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet)
				{
					//App.DoEvents();
					//MDIParent.InstancePtr.GetMainMenu();
					MDIParent.InstancePtr.Menu18();
					Close();
					//Application.Exit();
					App.MainForm.EndWaitMVModule();
					return;
				}
				frmSavePrint.InstancePtr.Unload();
				Close();
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "An error occurred on line " + fecherFoundation.Information.Erl(), "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume FailedUpdate; */
				SaveError:
				;
				switch (fecherFoundation.Information.Err(ex).Number)
				{
					case 3260:
						{
							intLockCount += 1;
							if (intLockCount > 5)
							{
								intChoice = (MessageBox.Show("Error Trying to Save.  This record is currently locked by another user.  Would you like to try to save again?", "Record Locked", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question));
								if (intChoice == DialogResult.Retry)
								{
									intLockCount = 1;
								}
								else
								{
									/*? Resume FailedUpdate; */
								}
							}
							//App.DoEvents();
							intRndCount = (FCConvert.ToInt32(Math.Pow(intLockCount, 2)) * 4000);
							for (x = 1; x <= intRndCount; x++)
							{
							}
							// x
							/*? Resume; */
							break;
						}
					default:
						{
							MessageBox.Show("Error: " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description + "\r\n" + "\r\n" + "This Registration Will NOT Be Saved!!" + "\r\n" + "\r\n" + "A file called SaveErr.txt has been created in the " + Environment.CurrentDirectory + " directory of your server.  Please notify TRIO of your problem and E-mail the SaveErr.txt file so we may more quickly find the problem you are encountering.", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							/*? Resume FailedUpdate; */
							break;
						}
				}
				//end switch
				FailedUpdate:
				;
				MotorVehicle.ClearAllVariables();
				frmDataInput.InstancePtr.Unload();
				frmPreview.InstancePtr.Unload();
				frmSavePrint.InstancePtr.Unload();
				Close();
				MotorVehicle.Statics.rsFinal = null;
				MotorVehicle.Statics.rsFinalCompare = null;
				if (!MotorVehicle.Statics.bolFromDosTrio && !MotorVehicle.Statics.bolFromWindowsCR)
				{
					//frmPlateInfo.InstancePtr.Show(App.MainForm);
					frmPlateInfo.InstancePtr.ShowForm();
				}
				else
				{
					////MDIParent.InstancePtr.GRID.Focus();
				}
			}
		}

		private void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
			{
				vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(true));
			}
		}

		private void frmLongTermReRegBatch_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmLongTermReRegBatch_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			MotorVehicle.Statics.RegistrationType = "RRR";
			RegisterCol = 1;
			PlateCol = 2;
			VINCol = 3;
			YearCol = 4;
			MakeCol = 5;
			ModelCol = 6;
			UnitCol = 7;
			CurrentExpireDateCol = 8;
			NetWeightCol = 9;
			IDCol = 10;
			//vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsVehicles.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsVehicles.ColDataType(RegisterCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsVehicles.ColHidden(IDCol, true);
			vsVehicles.ColFormat(NetWeightCol, "#,##0");
			vsVehicles.TextMatrix(0, RegisterCol, "Register");
			vsVehicles.TextMatrix(0, PlateCol, "Plate");
			vsVehicles.TextMatrix(0, VINCol, "VIN");
			vsVehicles.TextMatrix(0, YearCol, "Year");
			vsVehicles.TextMatrix(0, MakeCol, "Make");
			vsVehicles.TextMatrix(0, ModelCol, "Model");
			vsVehicles.TextMatrix(0, UnitCol, "Unit");
			vsVehicles.TextMatrix(0, CurrentExpireDateCol, "Expiration Date");
			vsVehicles.TextMatrix(0, NetWeightCol, "Net Weight");
			vsVehicles.ColWidth(0, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.015));
			vsVehicles.ColWidth(VINCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.15));
			vsVehicles.ColWidth(YearCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.09));
			vsVehicles.ColWidth(MakeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.09));
			vsVehicles.ColWidth(UnitCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.12));
			vsVehicles.ColWidth(ModelCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
			vsVehicles.ColWidth(PlateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
			vsVehicles.ColWidth(CurrentExpireDateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
			vsVehicles.ColWidth(NetWeightCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
			SelectCurrentExpireDateCol = 1;
			SelectNetWeightCol = 2;
			SelectPrintedCol = 3;
			SelectNewPlateCol = 4;
			SelectIDCol = 5;
			SelectUniqueIdentifierCol = 6;
			SelectPlateCol = 7;
			SelectVINCol = 8;
			SelectYearCol = 9;
			SelectMakeCol = 10;
			SelectModelCol = 11;
			SelectUnitCol = 12;
			SelectLengthCol = 13;
			SelectExpiresCol = 14;
			SelectTotalCol = 15;
			//vsRegLength.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsRegLength.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsRegLength.ColComboList(SelectLengthCol, "2 Years|3 Years|4 Years|5 Years|6 Years|7 Years|8 Years|9 Years|10 Years|11 Years|12 Years");
			vsRegLength.ColFormat(SelectTotalCol, "#,##0.00");
			vsRegLength.ColHidden(SelectCurrentExpireDateCol, true);
			vsRegLength.ColHidden(SelectNetWeightCol, true);
			vsRegLength.ColHidden(SelectPrintedCol, true);
			vsRegLength.ColHidden(SelectNewPlateCol, true);
			vsRegLength.ColHidden(SelectIDCol, true);
			vsRegLength.ColHidden(SelectUniqueIdentifierCol, true);
			vsRegLength.TextMatrix(0, SelectPlateCol, "Plate");
			vsRegLength.TextMatrix(0, SelectVINCol, "VIN");
			vsRegLength.TextMatrix(0, SelectYearCol, "Year");
			vsRegLength.TextMatrix(0, SelectMakeCol, "Make");
			vsRegLength.TextMatrix(0, SelectModelCol, "Model");
			vsRegLength.TextMatrix(0, SelectUnitCol, "Unit");
			vsRegLength.TextMatrix(0, SelectLengthCol, "Length of Reg");
			vsRegLength.TextMatrix(0, SelectExpiresCol, "Expires");
			vsRegLength.TextMatrix(0, SelectTotalCol, "Total");
			vsRegLength.ColWidth(0, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.03));
			vsRegLength.ColWidth(SelectVINCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.15));
			vsRegLength.ColWidth(SelectYearCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.09));
			vsRegLength.ColWidth(SelectMakeCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.09));
			vsRegLength.ColWidth(SelectUnitCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.11));
			vsRegLength.ColWidth(SelectModelCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.09));
			vsRegLength.ColWidth(SelectPlateCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.08));
			vsRegLength.ColWidth(SelectExpiresCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.09));
			vsRegLength.ColWidth(SelectNetWeightCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.08));
			vsRegLength.ColWidth(SelectLengthCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.1));
			vsRegLength.ColWidth(SelectTotalCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.1));
			FillFleetCombo();
			FillLengthCombo(0);
            FillPlateCombo();
            cmbPlateOption.ListIndex = 0;
			rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
			{
				curAgentFee = FCConvert.ToDecimal(Conversion.Val(rsDefaultInfo.Get_Fields_Decimal("AgentFeeLongTermTrailer")));
			}
			blnBatchPrinted = false;
			blnPartiallyPrinted = false;
			MotorVehicle.Statics.gboolFromReRegBatchRegistration = true;
			intStep = 1;
			ShowStep();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void FillLengthCombo(int plateOption)
		{
			int counter;
			cboLength.Clear();
            if (plateOption == 0)
            {
                for (counter = 2; counter <= MotorVehicle.Statics.gintLongTermYearsAllowed; counter++)
                {
                    cboLength.AddItem(FCConvert.ToString(counter) + " Years");
                    cboLength.ItemData(cboLength.NewIndex);
                }
            }
            else
            {
                for (counter = 3; counter <= 12; counter++)
                {
                    cboLength.AddItem(FCConvert.ToString(counter) + " Years");
                    cboLength.ItemData(cboLength.NewIndex);
                }
            }
			
		}

		private void frmLongTermReRegBatch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				if (blnBatchPrinted || blnPartiallyPrinted)
				{
					MessageBox.Show("You must complete the batch process now that the forms have been printed.", "Complete Process", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
			}
			MotorVehicle.Statics.gboolFromReRegBatchRegistration = false;
			MotorVehicle.Statics.gboolReRegBatchStarted = false;
			// kk05232015 tromv-999
		}

		private void frmLongTermReRegBatch_Resize(object sender, System.EventArgs e)
		{
			vsVehicles.ColWidth(0, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.015));
			vsVehicles.ColWidth(VINCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.15));
			vsVehicles.ColWidth(YearCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.09));
			vsVehicles.ColWidth(MakeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.09));
			vsVehicles.ColWidth(UnitCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.12));
			vsVehicles.ColWidth(ModelCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
			vsVehicles.ColWidth(PlateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
			vsVehicles.ColWidth(CurrentExpireDateCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
			vsVehicles.ColWidth(NetWeightCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1));
			vsRegLength.ColWidth(0, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.03));
			vsRegLength.ColWidth(SelectVINCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.15));
			vsRegLength.ColWidth(SelectYearCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.09));
			vsRegLength.ColWidth(SelectMakeCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.09));
			vsRegLength.ColWidth(SelectUnitCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.11));
			vsRegLength.ColWidth(SelectModelCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.09));
			vsRegLength.ColWidth(SelectPlateCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.08));
			vsRegLength.ColWidth(SelectExpiresCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.09));
			vsRegLength.ColWidth(SelectNetWeightCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.08));
			vsRegLength.ColWidth(SelectLengthCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.1));
			vsRegLength.ColWidth(SelectTotalCol, FCConvert.ToInt32(vsRegLength.WidthOriginal * 0.1));

            fraReprint.CenterToContainer(this.ClientArea);
        }

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vsRegLength_ChangeEdit(object sender, System.EventArgs e)
		{
			AdjustVehicleLength(vsRegLength.Row, FCConvert.ToInt16(FCConvert.ToDouble(fecherFoundation.Strings.Trim(Strings.Left(vsRegLength.EditText, 2)))));
			CalculateTotalDue();
		}

		private void vsRegLength_RowColChange(object sender, System.EventArgs e)
		{
			if (vsRegLength.Col == SelectLengthCol)
			{
				vsRegLength.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsRegLength.EditCell();
			}
			else
			{
				vsRegLength.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsVehicles_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsVehicles.Row > 0)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) == true)
				{
					vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(false));
				}
				else
				{
					vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(true));
				}
			}
		}

		private void vsVehicles_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsVehicles.Row > 0)
			{
				if (KeyCode == Keys.Space)
				{
					KeyCode = 0;
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) == true)
					if (FCConvert.CBool(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) == true)
					{
						vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(false));
					}
					else
					{
						vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(true));
					}
				}
			}
		}

		private void ShowStep()
		{
			if (intStep == 1)
			{
				cmdBack.Enabled = false;
				cmdNext.Visible = true;
				cmdSave.Visible = false;
				cmdPrint.Visible = false;
				cmdQueue.Visible = false;
				fraFleetSelect.Visible = true;
				fraSelectVehicles.Visible = false;
				fraRegLength.Visible = false;
			}
			else if (intStep == 2)
			{
				cmdBack.Enabled = true;
				cmdNext.Visible = true;
				cmdSave.Visible = false;
				cmdPrint.Visible = false;
				cmdQueue.Visible = false;
				fraFleetSelect.Visible = false;
				fraSelectVehicles.Visible = true;
				fraRegLength.Visible = false;
				lblFleetNameVehicles.Text = cboFleets.Text;
			}
			else
			{
				cmdBack.Enabled = true;
				cmdNext.Visible = false;
				cmdSave.Visible = false;
				cmdPrint.Visible = true;
				cmdQueue.Visible = true;
				fraFleetSelect.Visible = false;
				fraSelectVehicles.Visible = false;
				fraRegLength.Visible = true;
				lblFleetLength.Text = cboFleets.Text;
			}
		}

		private void FillFleetCombo()
		{
			clsDRWrapper rsFleet = new clsDRWrapper();
			cboFleets.Clear();
			if (cmbGroup.Text == "Fleet")
            {
                rsFleet.OpenRecordset("SELECT c.*, p.FullName as Owner1Name FROM FleetMaster as c LEFT JOIN " +
                                      rsFleet.CurrentPrefix +
                                      "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type = 'L' AND ExpiryMonth <> 99 ORDER BY p.FullName","MotorVehicle");     
			}
			else
			{
                rsFleet.OpenRecordset("SELECT c.*, p.FullName as Owner1Name FROM FleetMaster as c LEFT JOIN " +
                                      rsFleet.CurrentPrefix +
                                      "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type = 'L' AND ExpiryMonth = 99 ORDER BY p.FullName","MotorVehicle");
            }
            if (rsFleet.EndOfFile() != true && rsFleet.BeginningOfFile() != true)
			{
				rsFleet.MoveLast();
				rsFleet.MoveFirst();
				do
				{
					cboFleets.AddItem(rsFleet.Get_Fields_Int32("FleetNumber") + Strings.Space(6 - FCConvert.ToString(rsFleet.Get_Fields_Int32("FleetNumber")).Length) + rsFleet.Get_Fields("Owner1Name"));
                    cboFleets.ItemData(cboFleets.NewIndex, rsFleet.Get_Fields_Int32("FleetNumber"));
                    rsFleet.MoveNext();
				}
				while (rsFleet.EndOfFile() != true);
			}
			rsFleet.Reset();
		}

		private void optFleet_CheckedChanged(object sender, System.EventArgs e)
		{
			FillFleetCombo();
		}

		private void optGroup_CheckedChanged(object sender, System.EventArgs e)
		{
			FillFleetCombo();
		}

		private void FillFleetRegVehicles()
		{
			int counter = 0;
			int counter2;
			int tempWidth;
			DateTime CheckDate;
			int fnx;
			int total;
			string strBadData = "";
            DateTime xdate;
            int intTemp = 0;

			try
			{
				// On Error GoTo ErrorTag
				fecherFoundation.Information.Err().Clear();
                vsVehicles.Rows = 1;
                if (cboFleets.SelectedIndex != -1)
				{					
					MotorVehicle.Statics.rsVehicles.OpenRecordset("SELECT * FROM Master WHERE Status <> 'T' AND FleetNumber = " + FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(Strings.Left(cboFleets.Text, 5)))) + " ORDER BY Plate");
					if (MotorVehicle.Statics.rsVehicles.EndOfFile() != true && MotorVehicle.Statics.rsVehicles.BeginningOfFile() != true)
					{
						frmWait.InstancePtr.Show();
						frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Searching";
						frmWait.InstancePtr.Refresh();
						counter = 1;
						do
                        {
                            xdate = MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate");
                            intTemp = DateTime.Now.DifferenceInMonths(xdate);
                            if (intTemp > 12)
                            {
                                goto NextVehicle;
                            }
                            else if (intTemp < -6 && cmbPlateOption.ListIndex > 0)
                            {

                            }
                            else if (xdate.Year > DateTime.Now.Year )
                            {
                                if (DateTime.Now.Month < 10)
                                {
                                    goto NextVehicle;
                                }
                            }

							vsVehicles.Rows += 1;
							vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(false));
							strBadData = "plate information";
							vsVehicles.TextMatrix(counter, PlateCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("plate")));
							strBadData = "VIN information";
							vsVehicles.TextMatrix(counter, VINCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Vin")));
							strBadData = "year information";
							vsVehicles.TextMatrix(counter, YearCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields("Year")));
							strBadData = "make information";
							vsVehicles.TextMatrix(counter, MakeCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("make")));
							strBadData = "model information";
							vsVehicles.TextMatrix(counter, ModelCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("model")));
							strBadData = "unit information";
							vsVehicles.TextMatrix(counter, UnitCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("UnitNumber")));
							vsVehicles.TextMatrix(counter, CurrentExpireDateCol, Strings.Format(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy"));
							vsVehicles.TextMatrix(counter, NetWeightCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields("NetWeight")));
							vsVehicles.TextMatrix(counter, IDCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("ID")));
							counter += 1;
							NextVehicle:
							;
							MotorVehicle.Statics.rsVehicles.MoveNext();
						}
						while (MotorVehicle.Statics.rsVehicles.EndOfFile() != true);
						frmWait.InstancePtr.Unload();
						//vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, vsVehicles.Rows - 1, 0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
					}
					if (vsVehicles.Rows <= 25)
					{
						vsVehicles.Height = vsVehicles.Rows * vsVehicles.RowHeight(0) + 75;
					}
					else
					{
						vsVehicles.Height = 25 * vsVehicles.RowHeight(0) + 75;
					}
					if (vsVehicles.Rows <= 1)
					{
						if (cmbGroup.Text == "Fleet")
						{
							MessageBox.Show("No vehicles found for this Fleet.", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							MessageBox.Show("No vehicles found for this Group.", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						return;
					}
					intStep = 2;
					ShowStep();
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				if (fecherFoundation.Information.Err(ex).Number == 94)
				{
					if (strBadData != "Class" && strBadData != "Plate")
					{
						MessageBox.Show("Vehicle " + MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsVehicles.Get_Fields_String("plate") + " is missing " + strBadData);
					}
					else
					{
						MessageBox.Show("One of the vehicles in the fleet is missing " + strBadData);
					}
				}
				else
				{
					MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				}
				frmWait.InstancePtr.Unload();
			}
		}

		private void FillFleetLengthGrid()
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			vsRegLength.Rows = 1;
			for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(counter, RegisterCol)) == true)
				if (FCConvert.CBool(vsVehicles.TextMatrix(counter, RegisterCol)) == true)
				{
					vsRegLength.Rows += 1;
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, 0, FCConvert.ToString(vsRegLength.Rows - 1));
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectPrintedCol, FCConvert.ToString(false));
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectNewPlateCol, "");
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectPlateCol, vsVehicles.TextMatrix(counter, PlateCol));
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectVINCol, vsVehicles.TextMatrix(counter, VINCol));
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectYearCol, vsVehicles.TextMatrix(counter, YearCol));
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectMakeCol, vsVehicles.TextMatrix(counter, MakeCol));
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectModelCol, vsVehicles.TextMatrix(counter, ModelCol));
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectUnitCol, vsVehicles.TextMatrix(counter, UnitCol));
					// vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectCurrentExpireDateCol) = vsVehicles.TextMatrix(counter, CurrentExpireDateCol)
					// DJW01162014 Changed this from checking to see if the expiraiotn date was more than a year in the past to seeing if the expiraiton year was less than the current year
					if (FCConvert.ToDateTime(vsVehicles.TextMatrix(counter, CurrentExpireDateCol)).Year < DateTime.Today.Year)
					{
						vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectCurrentExpireDateCol, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					}
					else
					{
						vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectCurrentExpireDateCol, vsVehicles.TextMatrix(counter, CurrentExpireDateCol));
					}
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectNetWeightCol, vsVehicles.TextMatrix(counter, NetWeightCol));
					vsRegLength.TextMatrix(vsRegLength.Rows - 1, SelectIDCol, vsVehicles.TextMatrix(counter, IDCol));
				}
			}
			if (vsRegLength.Rows > 1)
			{
				cboLength.SelectedIndex = 0;
				cboLength_Click();
				intStep = 3;
				ShowStep();
			}
			else
			{
				MessageBox.Show("You must select at leqast one vehicle before you may proceed.", "Select Vehicle", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void Recalculate(int intRow, int intLength = -1)
		{
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite
			Decimal curTotal;
			int intRegLength = 0;
			if (intLength > 0)
			{
				intRegLength = intLength;
			}
			else
			{
				intRegLength = FCConvert.ToInt16(FCConvert.ToDouble(fecherFoundation.Strings.Trim(Strings.Left(vsRegLength.TextMatrix(intRow, SelectLengthCol), 2))));
			}
			if (intRegLength == 24)
			{
				curTotal = 80;
			}
			else
			{
				if (Conversion.Val(vsRegLength.TextMatrix(intRow, SelectNetWeightCol)) != 2000)
				{
					curTotal = 12 * intRegLength;
				}
				else
				{
					curTotal = 5 * intRegLength;
				}
			}
			vsRegLength.TextMatrix(intRow, SelectTotalCol, FCConvert.ToString(curTotal + curAgentFee));
		}

		private void PrintMVR10Batch()
		{
			// vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
			DialogResult answer = 0;
			clsPrinterFunctions clsCTAPrinter = new clsPrinterFunctions();
			int lngFormCheck;
			// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
			int intCounter;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				mnuProcessQuit.Enabled = false;
				//cmdCancel.Enabled = false;
				cmdPrint.Enabled = false;
				cmdBack.Enabled = false;
				cmdQueue.Enabled = false;
				cboLength.Enabled = false;
				vsRegLength.Editable = FCGrid.EditableSettings.flexEDNone;
				if (blnBatchPrinted || blnPartiallyPrinted)
				{
					txtHigh.Text = "";
					txtLow.Text = "";
					fraReprint.Visible = true;
					txtLow.Focus();
					Label2.Text = "Please enter the range of vehicle numbers you wish to reprint";
				}
				else
				{
					intStartRow = 1;
					intNumberToPrint = (vsRegLength.Rows - 1);
                    if (!useSamePlate)
                    {
                        if (!GetPlatesFroRegistration())
                        {
                            if (blnPartiallyPrinted == false)
                            {
                                mnuProcessQuit.Enabled = false;
                                //cmdCancel.Enabled = true;
                                vsRegLength.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                                cboLength.Enabled = true;
                                cmdBack.Enabled = true;
                                cmdQueue.Enabled = true;
                            }

                            cmdPrint.Enabled = true;
                            return;
                        }
                    }

					if (!MotorVehicle.Statics.blnLongTermLaserForms)
					{
						if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to the top as possible) next to the line titled:" + "\r\n" + "STATE OF MAINE", 23, 1, MotorVehicle.Statics.MVR10PrinterName) == DialogResult.Cancel)
						{
							MotorVehicle.Statics.PrintMVR3 = false;
						}
						else
						{
                            rptMVRT10.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                        }
					}
					else
					{
                        rptMVRT10Laser.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                    }
					if (!MotorVehicle.Statics.blnLongTermLaserForms)
					{
						rptMVRT10.InstancePtr.Unload();
					}
					else
					{
						rptMVRT10Laser.InstancePtr.Unload();
					}
					if (MotorVehicle.Statics.PrintMVR3 == true)
					{
						if (fecherFoundation.Information.Err().Number == 0)
						{
							mnuProcessQuit.Enabled = false;
						}
					}
					else
					{
						cmdPrint.Enabled = true;
						return;
					}
					blnPartiallyPrinted = false;
					blnBatchPrinted = true;
					for (intCounter = 1; intCounter <= (vsRegLength.Rows - 1); intCounter++)
                    {
                        if (FCConvert.CBool(vsRegLength.TextMatrix(intCounter, SelectPrintedCol)) == true)
                        {
                            blnPartiallyPrinted = true;
                        }
                        else
                        {
                            blnBatchPrinted = false;
                        }
                    }
					if (blnBatchPrinted)
					{
						blnPartiallyPrinted = false;
					}
				}
				if (blnBatchPrinted)
				{
					cmdPrint.Enabled = true;
					cmdSave.Visible = true;
					cmdQueue.Visible = false;
				}
				else
				{
					if (blnPartiallyPrinted == false)
					{
						mnuProcessQuit.Enabled = false;
						vsRegLength.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						cboLength.Enabled = true;
						cmdBack.Enabled = true;
						cmdQueue.Enabled = true;
					}
					cmdPrint.Enabled = true;
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + fecherFoundation.Information.Err(ex).Description, "Error Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				blnPartiallyPrinted = false;
				blnBatchPrinted = true;
				for (intCounter = 1; intCounter <= (vsRegLength.Rows - 1); intCounter++)
				{
					if (FCConvert.CBool(vsRegLength.TextMatrix(intCounter, SelectPrintedCol)) == true)
					{
						blnPartiallyPrinted = true;
					}
					else
					{
						blnBatchPrinted = false;
					}
				}
				if (blnBatchPrinted)
				{
					blnPartiallyPrinted = false;
				}
				if (blnBatchPrinted)
				{
					cmdPrint.Enabled = true;
					cmdSave.Visible = true;
					cmdQueue.Visible = false;
				}
				else
				{
					if (blnPartiallyPrinted == false)
					{
						mnuProcessQuit.Enabled = false;
						vsRegLength.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						cboLength.Enabled = true;
						cmdBack.Enabled = true;
						cmdQueue.Enabled = true;
					}
					cmdPrint.Enabled = true;
				}
			}
		}

		private bool GetPlatesFroRegistration()
		{
			bool GetPlatesFroRegistration = false;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			int counter2;
			for (counter = 1; counter <= (vsRegLength.Rows - 1); counter++)
			{
				vsRegLength.TextMatrix(counter, SelectNewPlateCol, MotorVehicle.GetLongTermPlate(FCConvert.ToDateTime(vsRegLength.TextMatrix(counter, SelectExpiresCol)).Year));
				if (vsRegLength.TextMatrix(counter, SelectNewPlateCol) == "")
				{
					if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
					{
						for (counter2 = 1; counter2 <= counter - 1; counter2++)
						{
							if (MotorVehicle.ReturnLongTermPlate(vsRegLength.TextMatrix(counter2, SelectNewPlateCol)))
							{
								vsRegLength.TextMatrix(counter2, SelectNewPlateCol, "");
							}
						}
					}
					GetPlatesFroRegistration = false;
					return GetPlatesFroRegistration;
				}
			}
			GetPlatesFroRegistration = true;
			return GetPlatesFroRegistration;
		}

		public void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbGroup.Text == "Fleet")
			{
				optFleet_CheckedChanged(sender, e);
			}
			else if (cmbGroup.Text == "Group")
			{
				optGroup_CheckedChanged(sender, e);
			}
		}

        private void FillPlateCombo()
        {
            cmbPlateOption.Clear();
            cmbPlateOption.AddItem("New Plate");
            cmbPlateOption.AddItem("Same Plate");
        }

        
	}
}
