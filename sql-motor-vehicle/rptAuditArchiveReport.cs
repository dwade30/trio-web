//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptAuditArchiveReport.
	/// </summary>
	public partial class rptAuditArchiveReport : BaseSectionReport
	{
		public rptAuditArchiveReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Audit Archive Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptAuditArchiveReport_ReportEnd;
		}

        private void RptAuditArchiveReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static rptAuditArchiveReport InstancePtr
		{
			get
			{
				return (rptAuditArchiveReport)Sys.GetInstance(typeof(rptAuditArchiveReport));
			}
		}

		protected rptAuditArchiveReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAuditArchiveReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		private int intpage;
		private clsDRWrapper rsData = new clsDRWrapper();
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				fecherFoundation.Information.Err().Clear();
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					eArgs.EOF = false;
				}
				else
				{
					rsData.MoveNext();
					eArgs.EOF = rsData.EndOfFile();
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				modErrorHandler.SetErrorHandler(ex);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txtTime As object	OnWrite(string)
			string strSQL;
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "hh:mm tt");
			blnFirstRecord = true;
			strSQL = "";
			if (frmSetupAuditArchiveReport.InstancePtr.cmbPlateAll.Text == "All")
			{
				// do nothing
			}
			else
			{
				strSQL += "UserField1 = '" + fecherFoundation.Strings.Trim(frmSetupAuditArchiveReport.InstancePtr.cboPlate.Text) + "' AND ";
			}
			if (frmSetupAuditArchiveReport.InstancePtr.cmbVINSelected.Text == "All")
			{
				// do nothing
			}
			else
			{
				strSQL += "UserField2 = '" + fecherFoundation.Strings.Trim(frmSetupAuditArchiveReport.InstancePtr.cboVIN.Text) + "' AND ";
			}
			if (frmSetupAuditArchiveReport.InstancePtr.cmbScreenAll.Text == "All")
			{
				// do nothing
			}
			else
			{
				strSQL += "Location = '" + frmSetupAuditArchiveReport.InstancePtr.cboScreen.Text + "' AND ";
			}
			if (frmSetupAuditArchiveReport.InstancePtr.cmbDateAll.Text == "All")
			{
				// do nothing
			}
			else
			{
				strSQL += "(DateUpdated >= '" + frmSetupAuditArchiveReport.InstancePtr.txtLowDate.Text + "' AND DateUpdated <= '" + frmSetupAuditArchiveReport.InstancePtr.txtHighDate.Text + "') AND ";
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 5);
				rsData.OpenRecordset("Select * from AuditChangesArchive WHERE " + strSQL + " ORDER BY DateUpdated DESC, TimeUpdated DESC, UserField1, UserField2", MotorVehicle.DEFAULTDATABASE);
			}
			else
			{
				rsData.OpenRecordset("Select * from AuditChangesArchive ORDER BY DateUpdated DESC, TimeUpdated DESC, UserField1, UserField2", MotorVehicle.DEFAULTDATABASE);
			}
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldPlate.Text = FCConvert.ToString(rsData.Get_Fields_String("UserField1"));
			fldVIN.Text = FCConvert.ToString(rsData.Get_Fields_String("UserField2"));
			fldOPID.Text = FCConvert.ToString(rsData.Get_Fields_String("UserField3"));
			txtUser.Text = FCConvert.ToString(rsData.Get_Fields("UserID"));
			txtDateTimeField.Text = Strings.Format(rsData.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy") + " " + Strings.Format(rsData.Get_Fields_DateTime("TimeUpdated"), "h:mm:ss tt");
			fldLocation.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location")));
			fldDescription.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ChangeDescription")));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtPage As object	OnWrite(string)
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void rptAuditArchiveReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptAuditArchiveReport properties;
			//rptAuditArchiveReport.Caption	= "Audit Archive Report";
			//rptAuditArchiveReport.Icon	= "rptAuditArchiveReport.dsx":0000";
			//rptAuditArchiveReport.Left	= 0;
			//rptAuditArchiveReport.Top	= 0;
			//rptAuditArchiveReport.Width	= 19080;
			//rptAuditArchiveReport.Height	= 12990;
			//rptAuditArchiveReport.StartUpPosition	= 3;
			//rptAuditArchiveReport.WindowState	= 2;
			//rptAuditArchiveReport.SectionData	= "rptAuditArchiveReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
