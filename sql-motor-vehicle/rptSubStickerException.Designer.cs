﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubStickerException.
	/// </summary>
	partial class rptSubStickerException
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSubStickerException));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIssued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReplacement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExplanation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOpID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalMoney = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReplacement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExplanation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOpID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMoney)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldClassPlate,
            this.fldIssued,
            this.fldReplacement,
            this.fldExplanation,
            this.fldOpID,
            this.fldFee});
			this.Detail.Height = 0.2291667F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.Height = 0.19F;
			this.fldClassPlate.Left = 0.09375F;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldClassPlate.Text = "Field1";
			this.fldClassPlate.Top = 0.03125F;
			this.fldClassPlate.Width = 1F;
			// 
			// fldIssued
			// 
			this.fldIssued.Height = 0.19F;
			this.fldIssued.Left = 1.21875F;
			this.fldIssued.Name = "fldIssued";
			this.fldIssued.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.fldIssued.Top = 0.03125F;
			this.fldIssued.Width = 0.84375F;
			// 
			// fldReplacement
			// 
			this.fldReplacement.Height = 0.19F;
			this.fldReplacement.Left = 2.15625F;
			this.fldReplacement.Name = "fldReplacement";
			this.fldReplacement.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldReplacement.Text = "Field1";
			this.fldReplacement.Top = 0.03125F;
			this.fldReplacement.Width = 1.13575F;
			// 
			// fldExplanation
			// 
			this.fldExplanation.Height = 0.19F;
			this.fldExplanation.Left = 3.437F;
			this.fldExplanation.Name = "fldExplanation";
			this.fldExplanation.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldExplanation.Text = "Field1";
			this.fldExplanation.Top = 0.03099998F;
			this.fldExplanation.Width = 2.40625F;
			// 
			// fldOpID
			// 
			this.fldOpID.Height = 0.19F;
			this.fldOpID.Left = 6.90625F;
			this.fldOpID.Name = "fldOpID";
			this.fldOpID.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldOpID.Text = "Field1";
			this.fldOpID.Top = 0.03125F;
			this.fldOpID.Width = 0.5F;
			// 
			// fldFee
			// 
			this.fldFee.Height = 0.19F;
			this.fldFee.Left = 6.0625F;
			this.fldFee.Name = "fldFee";
			this.fldFee.Style = "font-family: \'Roman 12cpi\'; text-align: right; ddo-char-set: 1";
			this.fldFee.Text = "Field1";
			this.fldFee.Top = 0.03125F;
			this.fldFee.Width = 0.59375F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label2,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label8,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13});
			this.GroupHeader1.Height = 0.729F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.03125F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label2.Text = "CLASS / PLATE";
			this.Label2.Top = 0.53125F;
			this.Label2.Width = 1.15625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label4.Text = "REPLACEMENT";
			this.Label4.Top = 0.375F;
			this.Label4.Width = 1.09375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.19F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.437F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label5.Text = "EXPLANATION---------------------";
			this.Label5.Top = 0.531F;
			this.Label5.Width = 2.28125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.19F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 6.90625F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
			this.Label6.Text = "OPID";
			this.Label6.Top = 0.53125F;
			this.Label6.Width = 0.5F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.15625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label8.Text = "STICKER";
			this.Label8.Top = 0.53125F;
			this.Label8.Width = 0.96875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 2.5625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label10.Text = "---- STICKERS ----";
			this.Label10.Top = 0.15625F;
			this.Label10.Width = 1.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 1.25F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label11.Text = "DATE";
			this.Label11.Top = 0.375F;
			this.Label11.Width = 0.8125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 1.25F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.Label12.Text = "ISSUED";
			this.Label12.Top = 0.53125F;
			this.Label12.Width = 0.8125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 6.0625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Roman 12cpi\'; text-align: right; ddo-char-set: 1";
			this.Label13.Text = "FEE";
			this.Label13.Top = 0.53125F;
			this.Label13.Width = 0.59375F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label9,
            this.fldTotalCount,
            this.fldTotalMoney});
			this.GroupFooter1.Height = 0.3125F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.03125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Label9.Text = "TOTAL STICKERS:";
			this.Label9.Top = 0.0625F;
			this.Label9.Width = 1.21875F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 1.692F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldTotalCount.Text = "Field1";
			this.fldTotalCount.Top = 0.062F;
			this.fldTotalCount.Width = 0.75F;
			// 
			// fldTotalMoney
			// 
			this.fldTotalMoney.Height = 0.1875F;
			this.fldTotalMoney.Left = 3.177083F;
			this.fldTotalMoney.Name = "fldTotalMoney";
			this.fldTotalMoney.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldTotalMoney.Text = "Field1";
			this.fldTotalMoney.Top = 0.0625F;
			this.fldTotalMoney.Width = 1.15625F;
			// 
			// rptSubStickerException
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReplacement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExplanation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOpID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMoney)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIssued;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReplacement;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExplanation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOpID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFee;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalMoney;
	}
}
