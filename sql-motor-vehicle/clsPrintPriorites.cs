﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;

namespace TWMV0000
{
	public class clsPrintCodes
	{
		//=========================================================
		// kk01192018 tromvs-96  Adding enum for print priorities so the next time they change we only need to change them here
		private enum prtPri
		{
			enumNotForHwyUse = 1,
			enumDuneBuggy = 2,
			enumSMEClassB10Ton = 3,
			enumModifiedVehicle = 4,
			enumReserveNumFee = 5,
			enumReserveNumFeePd = 6,
			enumVanityFeePd = 7,
			enumVeteranDecal = 8,
			enumVanityFee25 = 9,
			enumVanityFee50 = 10,
			enumReplFee = 11,
			enumDupRegFee = 12,
			enumDupRegNoFee = 13,
			enumDupSticker = 14,
			// enumDupDecal = 15    'kk01192018 tromvs-96  Duplicate Decal removed with Truck Camper reg. Renumbered the rest of the Print Codes
			enumLostPlate = 15,
			enumLostPltStkr = 16,
			enumSpecialtyPlateFee = 17,
			enumSpecialtyPlate = 18,
			enumExciseTaxRebate = 19,
			enumPlateChange = 20,
			enumFarmUseOnly = 21,
			enumTransfer = 22,
			enumNewPlate = 23,
			enumAddressChange = 24,
			enumNewRegOldPlate = 25,
			enumNewExpireDate = 26,
			enumRentalVehicle = 27,
			enumRVWChange = 28,
			enumVINCorrection = 29,
			enumNameAdded = 30,
			enumNameDeleted = 31,
			enumNameChanged = 32,
			enumSurvSpouse = 33,
			enumIslandUseOnly = 34,
			enumOtherEntered = 35,
			enumRoadTractor = 36,
			enumFleetVehicle = 37,
			enumForHire = 38,
			enumMedalOfHonor = 39,
			enumGoldStarFamily = 40,
			enumStockCar = 41,
			enumHearse = 42,
			enumMobileHome = 43,
			enumOfficeTrailer = 44,
			enumCamperTrailer = 45,
			enumNonExciseTrailer = 46,
			enumNonExciseTrueTrailer = 47,
			enumSMEClassA = 48,
			enumNonCompensation = 49,
			enumFishTruck = 50,
			enumSnowplow = 51,
		}

		const int maxPrtPri = 51;

		public string NotForHwyUse
		{
			get
			{
				string NotForHwyUse = "";
				NotForHwyUse = Strings.Format(FCConvert.ToInt32(prtPri.enumNotForHwyUse), "00");
				return NotForHwyUse;
			}
		}

		public string DuneBuggy
		{
			get
			{
				string DuneBuggy = "";
				DuneBuggy = Strings.Format(FCConvert.ToInt32(prtPri.enumDuneBuggy), "00");
				return DuneBuggy;
			}
		}

		public string SMEClassB10Ton
		{
			get
			{
				string SMEClassB10Ton = "";
				SMEClassB10Ton = Strings.Format(FCConvert.ToInt32(prtPri.enumSMEClassB10Ton), "00");
				return SMEClassB10Ton;
			}
		}

		public string ModifiedVehicle
		{
			get
			{
				string ModifiedVehicle = "";
				ModifiedVehicle = Strings.Format(FCConvert.ToInt32(prtPri.enumModifiedVehicle), "00");
				return ModifiedVehicle;
			}
		}

		public string ReserveNumFee
		{
			get
			{
				string ReserveNumFee = "";
				ReserveNumFee = Strings.Format(FCConvert.ToInt32(prtPri.enumReserveNumFee), "00");
				return ReserveNumFee;
			}
		}

		public string ReserveNumFeePd
		{
			get
			{
				string ReserveNumFeePd = "";
				ReserveNumFeePd = Strings.Format(FCConvert.ToInt32(prtPri.enumReserveNumFeePd), "00");
				return ReserveNumFeePd;
			}
		}

		public string VanityFeePd
		{
			get
			{
				string VanityFeePd = "";
				VanityFeePd = Strings.Format(FCConvert.ToInt32(prtPri.enumVanityFeePd), "00");
				return VanityFeePd;
			}
		}

		public string VeteranDecal
		{
			get
			{
				string VeteranDecal = "";
				VeteranDecal = Strings.Format(FCConvert.ToInt32(prtPri.enumVeteranDecal), "00");
				return VeteranDecal;
			}
		}

		public string VanityFee25
		{
			get
			{
				string VanityFee25 = "";
				VanityFee25 = Strings.Format(FCConvert.ToInt32(prtPri.enumVanityFee25), "00");
				return VanityFee25;
			}
		}

		public string VanityFee50
		{
			get
			{
				string VanityFee50 = "";
				VanityFee50 = Strings.Format(FCConvert.ToInt32(prtPri.enumVanityFee50), "00");
				return VanityFee50;
			}
		}

		public string ReplFee
		{
			get
			{
				string ReplFee = "";
				ReplFee = Strings.Format(FCConvert.ToInt32(prtPri.enumReplFee), "00");
				return ReplFee;
			}
		}

		public string DupRegFee
		{
			get
			{
				string DupRegFee = "";
				DupRegFee = Strings.Format(FCConvert.ToInt32(prtPri.enumDupRegFee), "00");
				return DupRegFee;
			}
		}

		public string DupRegNoFee
		{
			get
			{
				string DupRegNoFee = "";
				DupRegNoFee = Strings.Format(FCConvert.ToInt32(prtPri.enumDupRegNoFee), "00");
				return DupRegNoFee;
			}
		}

		public string DupSticker
		{
			get
			{
				string DupSticker = "";
				DupSticker = Strings.Format(FCConvert.ToInt32(prtPri.enumDupSticker), "00");
				return DupSticker;
			}
		}
		// Public Property Get DupDecal() As String             'kk01192018 tromvs-96  Duplicate Decal removed with Truck Camper
		// DupDecal = "15"
		// End Property
		public string LostPlate
		{
			get
			{
				string LostPlate = "";
				LostPlate = Strings.Format(FCConvert.ToInt32(prtPri.enumLostPlate), "00");
				return LostPlate;
			}
		}

		public string LostPltStkr
		{
			get
			{
				string LostPltStkr = "";
				LostPltStkr = Strings.Format(FCConvert.ToInt32(prtPri.enumLostPltStkr), "00");
				return LostPltStkr;
			}
		}

		public string SpecialtyPlateFee
		{
			get
			{
				string SpecialtyPlateFee = "";
				SpecialtyPlateFee = Strings.Format(FCConvert.ToInt32(prtPri.enumSpecialtyPlateFee), "00");
				return SpecialtyPlateFee;
			}
		}

		public string SpecialtyPlate
		{
			get
			{
				string SpecialtyPlate = "";
				SpecialtyPlate = Strings.Format(FCConvert.ToInt32(prtPri.enumSpecialtyPlate), "00");
				return SpecialtyPlate;
			}
		}

		public string ExciseTaxRebate
		{
			get
			{
				string ExciseTaxRebate = "";
				ExciseTaxRebate = Strings.Format(FCConvert.ToInt32(prtPri.enumExciseTaxRebate), "00");
				return ExciseTaxRebate;
			}
		}

		public string PlateChange
		{
			get
			{
				string PlateChange = "";
				PlateChange = Strings.Format(FCConvert.ToInt32(prtPri.enumPlateChange), "00");
				return PlateChange;
			}
		}

		public string FarmUseOnly
		{
			get
			{
				string FarmUseOnly = "";
				FarmUseOnly = Strings.Format(FCConvert.ToInt32(prtPri.enumFarmUseOnly), "00");
				return FarmUseOnly;
			}
		}

		public string Transfer
		{
			get
			{
				string Transfer = "";
				Transfer = Strings.Format(FCConvert.ToInt32(prtPri.enumTransfer), "00");
				return Transfer;
			}
		}

		public string NewPlate
		{
			get
			{
				string NewPlate = "";
				NewPlate = Strings.Format(FCConvert.ToInt32(prtPri.enumNewPlate), "00");
				return NewPlate;
			}
		}

		public string AddressChange
		{
			get
			{
				string AddressChange = "";
				AddressChange = Strings.Format(FCConvert.ToInt32(prtPri.enumAddressChange), "00");
				return AddressChange;
			}
		}

		public string NewRegOldPlate
		{
			get
			{
				string NewRegOldPlate = "";
				NewRegOldPlate = Strings.Format(FCConvert.ToInt32(prtPri.enumNewRegOldPlate), "00");
				return NewRegOldPlate;
			}
		}

		public string NewExpireDate
		{
			get
			{
				string NewExpireDate = "";
				NewExpireDate = Strings.Format(FCConvert.ToInt32(prtPri.enumNewExpireDate), "00");
				return NewExpireDate;
			}
		}

		public string RentalVehicle
		{
			get
			{
				string RentalVehicle = "";
				RentalVehicle = Strings.Format(FCConvert.ToInt32(prtPri.enumRentalVehicle), "00");
				return RentalVehicle;
			}
		}

		public string RVWChange
		{
			get
			{
				string RVWChange = "";
				RVWChange = Strings.Format(FCConvert.ToInt32(prtPri.enumRVWChange), "00");
				return RVWChange;
			}
		}

		public string VINCorrection
		{
			get
			{
				string VINCorrection = "";
				VINCorrection = Strings.Format(FCConvert.ToInt32(prtPri.enumVINCorrection), "00");
				return VINCorrection;
			}
		}

		public string NameAdded
		{
			get
			{
				string NameAdded = "";
				NameAdded = Strings.Format(FCConvert.ToInt32(prtPri.enumNameAdded), "00");
				return NameAdded;
			}
		}

		public string NameDeleted
		{
			get
			{
				string NameDeleted = "";
				NameDeleted = Strings.Format(FCConvert.ToInt32(prtPri.enumNameDeleted), "00");
				return NameDeleted;
			}
		}

		public string NameChanged
		{
			get
			{
				string NameChanged = "";
				NameChanged = Strings.Format(FCConvert.ToInt32(prtPri.enumNameChanged), "00");
				return NameChanged;
			}
		}

		public string SurvSpouse
		{
			get
			{
				string SurvSpouse = "";
				SurvSpouse = Strings.Format(FCConvert.ToInt32(prtPri.enumSurvSpouse), "00");
				return SurvSpouse;
			}
		}

		public string IslandUseOnly
		{
			get
			{
				string IslandUseOnly = "";
				IslandUseOnly = Strings.Format(FCConvert.ToInt32(prtPri.enumIslandUseOnly), "00");
				return IslandUseOnly;
			}
		}

		public string OtherEntered
		{
			get
			{
				string OtherEntered = "";
				OtherEntered = Strings.Format(FCConvert.ToInt32(prtPri.enumOtherEntered), "00");
				return OtherEntered;
			}
		}

		public string RoadTractor
		{
			get
			{
				string RoadTractor = "";
				RoadTractor = Strings.Format(FCConvert.ToInt32(prtPri.enumRoadTractor), "00");
				return RoadTractor;
			}
		}

		public string FleetVehicle
		{
			get
			{
				string FleetVehicle = "";
				FleetVehicle = Strings.Format(FCConvert.ToInt32(prtPri.enumFleetVehicle), "00");
				return FleetVehicle;
			}
		}

		public string ForHire
		{
			get
			{
				string ForHire = "";
				ForHire = Strings.Format(FCConvert.ToInt32(prtPri.enumForHire), "00");
				return ForHire;
			}
		}

		public string MedalOfHonor
		{
			get
			{
				string MedalOfHonor = "";
				MedalOfHonor = Strings.Format(FCConvert.ToInt32(prtPri.enumMedalOfHonor), "00");
				return MedalOfHonor;
			}
		}

		public string GoldStarFamily
		{
			get
			{
				string GoldStarFamily = "";
				GoldStarFamily = Strings.Format(FCConvert.ToInt32(prtPri.enumGoldStarFamily), "00");
				return GoldStarFamily;
			}
		}

		public string StockCar
		{
			get
			{
				string StockCar = "";
				StockCar = Strings.Format(FCConvert.ToInt32(prtPri.enumStockCar), "00");
				return StockCar;
			}
		}

		public string Hearse
		{
			get
			{
				string Hearse = "";
				Hearse = Strings.Format(FCConvert.ToInt32(prtPri.enumHearse), "00");
				return Hearse;
			}
		}

		public string MobileHome
		{
			get
			{
				string MobileHome = "";
				MobileHome = Strings.Format(FCConvert.ToInt32(prtPri.enumMobileHome), "00");
				return MobileHome;
			}
		}

		public string OfficeTrailer
		{
			get
			{
				string OfficeTrailer = "";
				OfficeTrailer = Strings.Format(FCConvert.ToInt32(prtPri.enumOfficeTrailer), "00");
				return OfficeTrailer;
			}
		}

		public string CamperTrailer
		{
			get
			{
				string CamperTrailer = "";
				CamperTrailer = Strings.Format(FCConvert.ToInt32(prtPri.enumCamperTrailer), "00");
				return CamperTrailer;
			}
		}

		public string NonExciseTrailer
		{
			get
			{
				string NonExciseTrailer = "";
				NonExciseTrailer = Strings.Format(FCConvert.ToInt32(prtPri.enumNonExciseTrailer), "00");
				return NonExciseTrailer;
			}
		}

		public string NonExciseTrueTrailer
		{
			get
			{
				string NonExciseTrueTrailer = "";
				NonExciseTrueTrailer = Strings.Format(FCConvert.ToInt32(prtPri.enumNonExciseTrueTrailer), "00");
				return NonExciseTrueTrailer;
			}
		}

		public string SMEClassA
		{
			get
			{
				string SMEClassA = "";
				SMEClassA = Strings.Format(FCConvert.ToInt32(prtPri.enumSMEClassA), "00");
				return SMEClassA;
			}
		}

		public string NonCompensation
		{
			get
			{
				string NonCompensation = "";
				NonCompensation = Strings.Format(FCConvert.ToInt32(prtPri.enumNonCompensation), "00");
				return NonCompensation;
			}
		}

		public string FishTruck
		{
			get
			{
				string FishTruck = "";
				FishTruck = Strings.Format(FCConvert.ToInt32(prtPri.enumFishTruck), "00");
				return FishTruck;
			}
		}

		public string Snowplow
		{
			get
			{
				string Snowplow = "";
				Snowplow = Strings.Format(FCConvert.ToInt32(prtPri.enumSnowplow), "00");
				return Snowplow;
			}
		}

		public int MaxPrintPriority
		{
			get
			{
				int MaxPrintPriority = 0;
				MaxPrintPriority = maxPrtPri;
				return MaxPrintPriority;
			}
		}

		public bool ReturnPrintPriorityInclude(int intPrintPriority)
		{
			bool ReturnPrintPriorityInclude = false;
			// These are the print priority codes that the user can manually add to the reg
			// Moved this from the MotorVehicle module to here
			if (intPrintPriority == FCConvert.ToInt32(prtPri.enumAddressChange) || intPrintPriority == FCConvert.ToInt32(prtPri.enumNameChanged) || intPrintPriority == FCConvert.ToInt32(prtPri.enumSurvSpouse) || intPrintPriority == FCConvert.ToInt32(prtPri.enumOtherEntered) || intPrintPriority == FCConvert.ToInt32(prtPri.enumOfficeTrailer) || intPrintPriority == FCConvert.ToInt32(prtPri.enumFishTruck) || intPrintPriority == FCConvert.ToInt32(prtPri.enumSnowplow))
			{
				ReturnPrintPriorityInclude = true;
			}
			else
			{
				ReturnPrintPriorityInclude = false;
			}
			return ReturnPrintPriorityInclude;
		}
		// vbPorter upgrade warning: intPrintPriority As int	OnWrite(int, string)
		public string ReturnPrintPriorityText(int intPrintPriority)
		{
			string returnPrintPriorityText;

            switch (intPrintPriority)
            {
                // Returns the text that should be returned for a given code
                // These get stored in the database to make reporting easier(?) I guess
                // because BMV dictates what they are
                // Moved this from the MotorVehicle module to here
                case (int) prtPri.enumNotForHwyUse:
                    returnPrintPriorityText = "NOT TO BE DRIVEN ON HWY";

                    break;
                case (int) prtPri.enumDuneBuggy:
                    returnPrintPriorityText = "DUNE BUGGY";

                    break;
                case (int) prtPri.enumSMEClassB10Ton:
                    returnPrintPriorityText = "SME CL B>10TON=PERM";

                    break;
                case (int) prtPri.enumModifiedVehicle:
                    returnPrintPriorityText = "MODIFIED VEH";

                    break;
                case (int) prtPri.enumReserveNumFee:
                    returnPrintPriorityText = "RESRV # FEE $";

                    break;
                case (int) prtPri.enumReserveNumFeePd:
                    returnPrintPriorityText = "RESRV # FEE PD";

                    break;
                case (int) prtPri.enumVanityFeePd:
                    returnPrintPriorityText = "VANITY FEE PD";

                    break;
                case (int) prtPri.enumVeteranDecal:
                    returnPrintPriorityText = "TYPE OF VETERAN DECAL";

                    break;
                case (int) prtPri.enumVanityFee25:
                    returnPrintPriorityText = "VANITY FEE $25.00";

                    break;
                case (int) prtPri.enumVanityFee50:
                    returnPrintPriorityText = "VANITY FEE $50.00";

                    break;
                case (int) prtPri.enumReplFee:
                    returnPrintPriorityText = "REPL FEE $";

                    break;
                case (int) prtPri.enumDupRegFee:
                    returnPrintPriorityText = "DUP REG $5.00";

                    break;
                case (int) prtPri.enumDupRegNoFee:
                    returnPrintPriorityText = "DUP REG-NO FEE";

                    break;
                case (int) prtPri.enumDupSticker:
                    returnPrintPriorityText = "DUP STICKER $";

                    break;
                case (int) prtPri.enumLostPlate:
                    returnPrintPriorityText = "LOST PLATE $";

                    break;
                case (int) prtPri.enumLostPltStkr:
                    returnPrintPriorityText = "LOST PL/ST $";

                    break;
                case (int) prtPri.enumSpecialtyPlateFee:
                    returnPrintPriorityText = "TYPE OF SPECIALTY PL $";

                    break;
                case (int) prtPri.enumSpecialtyPlate:
                    returnPrintPriorityText = "TYPE OF SPECIALTY PL";

                    break;
                case (int) prtPri.enumExciseTaxRebate:
                    returnPrintPriorityText = "EXC TAX REBATE";

                    break;
                case (int) prtPri.enumPlateChange:
                    returnPrintPriorityText = "PLATE CHANGE";

                    break;
                case (int) prtPri.enumFarmUseOnly:
                    returnPrintPriorityText = "FARM USE ONLY";

                    break;
                case (int) prtPri.enumTransfer:
                    returnPrintPriorityText = "TRANSFER";

                    break;
                case (int) prtPri.enumNewPlate:
                    returnPrintPriorityText = "NP";

                    break;
                case (int) prtPri.enumAddressChange:
                    returnPrintPriorityText = "ADDRESS CHNG";

                    break;
                case (int) prtPri.enumNewRegOldPlate:
                    returnPrintPriorityText = "NROP";

                    break;
                case (int) prtPri.enumNewExpireDate:
                    returnPrintPriorityText = "NEW EXP DATE";

                    break;
                case (int) prtPri.enumRentalVehicle:
                    returnPrintPriorityText = "RENTAL VEH";

                    break;
                case (int) prtPri.enumRVWChange:
                    returnPrintPriorityText = "RVW CHNG";

                    break;
                case (int) prtPri.enumVINCorrection:
                    returnPrintPriorityText = "VIN CORR";

                    break;
                case (int) prtPri.enumNameAdded:
                    returnPrintPriorityText = "NAME ADDED";

                    break;
                case (int) prtPri.enumNameDeleted:
                    returnPrintPriorityText = "NAME DELETED";

                    break;
                case (int) prtPri.enumNameChanged:
                    returnPrintPriorityText = "NAME CHANGED";

                    break;
                case (int) prtPri.enumSurvSpouse:
                    returnPrintPriorityText = "SURV SPOUSE";

                    break;
                case (int) prtPri.enumIslandUseOnly:
                    returnPrintPriorityText = "ISLAND USE ONLY";

                    break;
                case (int) prtPri.enumOtherEntered:
                    returnPrintPriorityText = "WHATEVER TYPED / OTHER";

                    break;
                case (int) prtPri.enumRoadTractor:
                    returnPrintPriorityText = "ROAD TRACTOR";

                    break;
                case (int) prtPri.enumFleetVehicle:
                    returnPrintPriorityText = "FLEET VEH";

                    break;
                case (int) prtPri.enumForHire:
                    returnPrintPriorityText = "HIRE";

                    break;
                case (int) prtPri.enumMedalOfHonor:
                    returnPrintPriorityText = "MEDAL OF HONOR";

                    break;
                case (int) prtPri.enumGoldStarFamily:
                    returnPrintPriorityText = "GOLD STAR FAM";

                    break;
                case (int) prtPri.enumStockCar:
                    returnPrintPriorityText = "STOCK CAR";

                    break;
                case (int) prtPri.enumHearse:
                    returnPrintPriorityText = "HEARSE";

                    break;
                case (int) prtPri.enumMobileHome:
                    returnPrintPriorityText = "MOBILE HOME";

                    break;
                case (int) prtPri.enumOfficeTrailer:
                    returnPrintPriorityText = "OFFICE TL";

                    break;
                case (int) prtPri.enumCamperTrailer:
                    returnPrintPriorityText = "CAMPER TL";

                    break;
                case (int) prtPri.enumNonExciseTrailer:
                    returnPrintPriorityText = "NON-EXC TL";

                    break;
                case (int) prtPri.enumNonExciseTrueTrailer:
                    returnPrintPriorityText = "NON-EXC TRUE TL";

                    break;
                case (int) prtPri.enumSMEClassA:
                    returnPrintPriorityText = "SME CL A";

                    break;
                case (int) prtPri.enumNonCompensation:
                    returnPrintPriorityText = "NON-COMPENSATION";

                    break;
                case (int) prtPri.enumFishTruck:
                    returnPrintPriorityText = "FISH TRUCK";

                    break;
                case (int) prtPri.enumSnowplow:
                    returnPrintPriorityText = "SNOWPLOW ONLY";

                    break;
                default:
                    returnPrintPriorityText = "";

                    break;
            }
			return returnPrintPriorityText;
		}

		public int MaxVeteranDecalCode
		{
			get
			{
				int MaxVeteranDecalCode = 0;
				MaxVeteranDecalCode = 35;
				return MaxVeteranDecalCode;
			}
		}

		public string GetVeteranDecalText(string strDecalCode)
		{
			string GetVeteranDecalText = "";
			string strRetTxt = "";
			if (strDecalCode == "" || strDecalCode == "X")
			{
				strRetTxt = "";
			}
			else
            {
                switch (strDecalCode)
                {
                    // OLD TRIO CODE
                    // BMV # From Appendix G
                    case "01":

                        // "D"            ' 1 - Distinguished Service Cross Medal
                        strRetTxt = "DSC";

                        break;
                    case "02":

                        // "A"            ' 2 - Air Force Cross Medal
                        strRetTxt = "AFC";

                        break;
                    case "03":

                        // "N"            ' 3 - Navy Cross Medal
                        strRetTxt = "NC";

                        break;
                    case "04":

                        // "S"            ' 4 - Silver Star Medal
                        strRetTxt = "SS";

                        break;
                    case "05":

                        // "F"            ' 5 - Distinguished Flying Cross
                        strRetTxt = "DFC";

                        break;
                    case "06":

                        // "5"            ' 6 - Soldier's Medal
                        strRetTxt = "SM";

                        break;
                    case "07":

                        // "M"            ' 7 - Navy Marine Corps Medal
                        strRetTxt = "NMCM";

                        break;
                    case "08":

                        // "1"            ' 8 - Airman's Medal
                        strRetTxt = "AM";

                        break;
                    case "09":

                        // "C"            ' 9 - Coast Guard Medal
                        strRetTxt = "CGM";

                        break;
                    case "10":

                        // "B"            '10 - Bronze Star Medal
                        strRetTxt = "BSM";

                        break;
                    case "11":

                        // "3"            '11 - European/African Campaign Medal WWII
                        strRetTxt = "EAMECM";

                        break;
                    case "12":

                        // "2"            '12 - Asiatic-Pacific Campaign Medal WWII
                        strRetTxt = "APCM";

                        break;
                    case "13":

                        // "K"            '13 - Korean Service Medal
                        strRetTxt = "KSM";

                        break;
                    case "14":

                        // "E"            '14 - Armed Forces Expeditionary Medal
                        strRetTxt = "AFM";

                        break;
                    case "15":

                        // "V"            '15 - Vietnam Service Medal
                        strRetTxt = "VSM";

                        break;
                    case "16":

                        // "6"            '16 - Southwest Asia Service
                        strRetTxt = "SWASM";

                        break;
                    case "17":

                        // "4"            '17 - Kosovo Medal
                        strRetTxt = "KM";

                        break;
                    case "18":

                        // "G"            '18 - United States Army
                        strRetTxt = "ARMY";

                        break;
                    case "19":

                        // "H"            '19 - United States Navy
                        strRetTxt = "NAVY";

                        break;
                    case "20":

                        // "J"            '20 - United States Air Force
                        strRetTxt = "USAF";

                        break;
                    case "21":

                        // "I"            '21 - United States Marine Corps
                        strRetTxt = "USMC";

                        break;
                    case "22":

                        // "L"            '22 - United States Coast Guard
                        strRetTxt = "USCG";

                        break;
                    case "23":

                        // "O"            '23 - Global War on Terrorism Expeditionary Medal
                        strRetTxt = "GWT";

                        break;
                    case "24":

                        // "P"            '24 - Korean Defense Service Medal
                        strRetTxt = "KDS";

                        break;
                    case "25":

                        // "8"            '25 - Iraq Campaign Medal
                        strRetTxt = "ICM";

                        break;
                    case "26":

                        // "7"            '26 - Afghanistan Campaign Medal
                        strRetTxt = "ACM";

                        break;
                    case "27":

                        // "9"            '27 - Combat Infantry Badge
                        strRetTxt = "CIB";

                        break;
                    case "28":

                        // "Q"            '28 - Combat Medic Badge
                        strRetTxt = "CMB";

                        break;
                    case "29":

                        // "R"            '29 - Army Combat Badge
                        strRetTxt = "CAB";

                        break;
                    case "30":

                        // "T"            '30 - Marine Combat Action Badge
                        strRetTxt = "CRM";

                        break;
                    case "31":

                        // "U"            '31 - Navy Combat Action Badge
                        strRetTxt = "CRN";

                        break;
                    case "32":

                        // "W"            '32 - Air Force Combat Action Medal
                        strRetTxt = "CAM";

                        break;
                    case "33":

                        // "Y"            '33 - National Emergency Service Medal
                        strRetTxt = "NES";

                        break;
                    case "34":

                        // "Z"            '34 - Wabanaki Decal
                        strRetTxt = "WAB";

                        break;
                    case "35":

                        // 35 - Special Air Medal Commemorative Decal
                        strRetTxt = "AMC";

                        break;
                    default:
                        strRetTxt = "";

                        break;
                }
            }
			GetVeteranDecalText = strRetTxt;
			return GetVeteranDecalText;
		}

		public string GetVeteranDecalDesc(ref string strDecalCode)
		{
			string GetVeteranDecalDesc = "";
			string strRetTxt = "";
			if (strDecalCode == "" || strDecalCode == "X")
			{
				strRetTxt = "NONE";
			}
			else
            {
                switch (strDecalCode)
                {
                    // OLD TRIO CODE
                    // BMV # From Appendix G
                    case "01":

                        // "D"            ' 1 - Distinguished Service Cross Medal
                        strRetTxt = "DISTINGUISHED SERVICE CROSS MEDAL";

                        break;
                    case "02":

                        // "A"            ' 2 - Air Force Cross Medal
                        strRetTxt = "AIR FORCE CROSS MEDAL";

                        break;
                    case "03":

                        // "N"            ' 3 - Navy Cross Medal
                        strRetTxt = "NAVY CROSS MEDAL";

                        break;
                    case "04":

                        // "S"            ' 4 - Silver Star Medal
                        strRetTxt = "SILVER STAR MEDAL";

                        break;
                    case "05":

                        // "F"            ' 5 - Distinguished Flying Cross
                        strRetTxt = "DISTINGUISHED FLYING CROSS MEDAL";

                        break;
                    case "06":

                        // "5"            ' 6 - Soldier's Medal
                        strRetTxt = "SOLDIER'S MEDAL";

                        break;
                    case "07":

                        // "M"            ' 7 - Navy Marine Corps Medal
                        strRetTxt = "NAVY MARINE CORPS MEDAL";

                        break;
                    case "08":

                        // "1"            ' 8 - Airman's Medal
                        strRetTxt = "AIRMAN'S MEDAL";

                        break;
                    case "09":

                        // "C"            ' 9 - Coast Guard Medal
                        strRetTxt = "COAST GUARD MEDAL";

                        break;
                    case "10":

                        // "B"            '10 - Bronze Star Medal
                        strRetTxt = "BRONZE STAR MEDAL";

                        break;
                    case "11":

                        // "3"            '11 - European/African Campaign Medal WWII
                        strRetTxt = "EUROPEAN/AFRICAN CAMPAIGN MEDAL WWII";

                        break;
                    case "12":

                        // "2"            '12 - Asiatic-Pacific Campaign Medal WWII
                        strRetTxt = "ASIATIC-PACIFIC CAMPAIGN MEDAL WWII";

                        break;
                    case "13":

                        // "K"            '13 - Korean Service Medal
                        strRetTxt = "KOREAN SERVICE MEDAL";

                        break;
                    case "14":

                        // "E"            '14 - Armed Forces Expeditionary Medal
                        strRetTxt = "ARMED FORCES EXPEDITIONARY MEDAL";

                        break;
                    case "15":

                        // "V"            '15 - Vietnam Service Medal
                        strRetTxt = "VIETNAM SERVICE MEDAL";

                        break;
                    case "16":

                        // "6"            '16 - Southwest Asia Service
                        strRetTxt = "SOUTHWEST ASIA SERVICE";

                        break;
                    case "17":

                        // "4"            '17 - Kosovo Medal
                        strRetTxt = "KOSOVO MEDAL";

                        break;
                    case "18":

                        // "G"            '18 - United States Army
                        strRetTxt = "UNITED STATES ARMY";

                        break;
                    case "19":

                        // "H"            '19 - United States Navy
                        strRetTxt = "UNITED STATES NAVY";

                        break;
                    case "20":

                        // "J"            '20 - United States Air Force
                        strRetTxt = "UNITED STATES AIR FORCE";

                        break;
                    case "21":

                        // "I"            '21 - United States Marine Corps
                        strRetTxt = "UNITED STATES MARINE CORPS";

                        break;
                    case "22":

                        // "L"            '22 - United States Coast Guard
                        strRetTxt = "UNITED STATES COAST GUARD";

                        break;
                    case "23":

                        // "O"            '23 - Global War on Terrorism Expeditionary Medal
                        strRetTxt = "GLOBAL WAR ON TERRORISM EXPEDITIONARY MEDAL";

                        break;
                    case "24":

                        // "P"            '24 - Korean Defense Service Medal
                        strRetTxt = "KOREAN DEFENSE SERVICE MEDAL";

                        break;
                    case "25":

                        // "8"            '25 - Iraq Campaign Medal
                        strRetTxt = "IRAQ CAMPAIGN MEDAL";

                        break;
                    case "26":

                        // "7"            '26 - Afghanistan Campaign Medal
                        strRetTxt = "AFGHANISTAN CAMPAIGN MEDAL";

                        break;
                    case "27":

                        // "9"            '27 - Combat Infantry Badge
                        strRetTxt = "COMBAT INFANTRY BADGE";

                        break;
                    case "28":

                        // "Q"            '28 - Combat Medic Badge
                        strRetTxt = "COMBAT MEDIC BADGE";

                        break;
                    case "29":

                        // "R"            '29 - Army Combat Badge
                        strRetTxt = "ARMY COMBAT BADGE";

                        break;
                    case "30":

                        // "T"            '30 - Marine Combat Action Badge
                        strRetTxt = "MARINE COMBAT ACTION BADGE";

                        break;
                    case "31":

                        // "U"            '31 - Navy Combat Action Badge
                        strRetTxt = "NAVY COMBAT ACTION BADGE";

                        break;
                    case "32":

                        // "W"            '32 - Air Force Combat Action Medal
                        strRetTxt = "AIR FORCE COMBAT ACTION MEDAL";

                        break;
                    case "33":

                        // "Y"            '33 - National Emergency Service Medal
                        strRetTxt = "NATIONAL EMERGENCY SERVICE MEDAL";

                        break;
                    case "34":

                        // "Z"            '34 - Wabanaki Decal
                        strRetTxt = "WABANAKI DECAL";

                        break;
                    case "35":

                        // 35 - Special Air Medal Commemorative Decal
                        strRetTxt = "SPECIAL AIR MEDAL COMMEMORATIVE DECAL";

                        break;
                    default:
                        strRetTxt = "";

                        break;
                }
            }
			GetVeteranDecalDesc = strRetTxt;
			return GetVeteranDecalDesc;
		}

		public string GetVeteranDecalCode(string strDecalText)
		{
			string GetVeteranDecalCode = "";
			string strRetCode = "";
			// Could return "X" or "" for nothing
            var decalText = fecherFoundation.Strings.Trim(strDecalText);

            if (decalText == "")
			{
				strRetCode = "";
			}
			else
            {
                switch (decalText)
                {
                    // OLD TRIO CODE
                    // BMV # From Appendix G
                    case "DSC":

                        // 1 - Distinguished Service Cross Medal
                        strRetCode = "01";
                        // "D"
                        break;
                    case "AFC":

                        // 2 - Air Force Cross Medal
                        strRetCode = "02";
                        // "A"
                        break;
                    case "NC":

                        // 3 - Navy Cross Medal
                        strRetCode = "03";
                        // "N"
                        break;
                    case "SS":

                        // 4 - Silver Star Medal
                        strRetCode = "04";
                        // "S"
                        break;
                    case "DFC":

                        // 5 - Distinguished Flying Cross
                        strRetCode = "05";
                        // "F"
                        break;
                    case "SM":

                        // 6 - Soldier's Medal
                        strRetCode = "06";
                        // "5"
                        break;
                    case "NMCM":

                        // 7 - Navy Marine Corps Medal
                        strRetCode = "07";
                        // "M"
                        break;
                    case "AM":

                        // 8 - Airman's Medal
                        strRetCode = "08";
                        // "1"
                        break;
                    case "CGM":

                        // 9 - Coast Guard Medal
                        strRetCode = "09";
                        // "C"
                        break;
                    case "BSM":

                        // 10 - Bronze Star Medal
                        strRetCode = "10";
                        // "B"
                        break;
                    case "EAMECM":

                        // 11 - European/African Campaign Medal WWII
                        strRetCode = "11";
                        // "3"
                        break;
                    case "APCM":

                        // 12 - Asiatic-Pacific Campaign Medal WWII
                        strRetCode = "12";
                        // "2"
                        break;
                    case "KSM":

                        // 13 - Korean Service Medal
                        strRetCode = "13";
                        // "K"
                        break;
                    case "AFM":

                        // 14 - Armed Forces Expeditionary Medal
                        strRetCode = "14";
                        // "E"
                        break;
                    case "VSM":

                        // 15 - Vietnam Service Medal
                        strRetCode = "15";
                        // "V"
                        break;
                    case "SWASM":

                        // 16 - Southwest Asia Service
                        strRetCode = "16";
                        // "6"
                        break;
                    case "KM":

                        // 17 - Kosovo Medal
                        strRetCode = "17";
                        // "4"
                        break;
                    case "ARMY":

                        // 18 - United States Army
                        strRetCode = "18";
                        // "G"
                        break;
                    case "NAVY":

                        // 19 - United States Navy
                        strRetCode = "19";
                        // "H"
                        break;
                    case "USAF":

                        // 20 - United States Air Force
                        strRetCode = "20";
                        // "J"
                        break;
                    case "USMC":

                        // 21 - United States Marine Corps
                        strRetCode = "21";
                        // "I"
                        break;
                    case "USCG":

                        // 22 - United States Coast Guard
                        strRetCode = "22";
                        // "L"
                        break;
                    case "GWT":

                        // 23 - Global War on Terrorism Expeditionary Medal
                        strRetCode = "23";
                        // "O"
                        break;
                    case "KDS":

                        // 24 - Korean Defense Service Medal
                        strRetCode = "24";
                        // "P"
                        break;
                    case "ICM":

                        // 25 - Iraq Campaign Medal
                        strRetCode = "25";
                        // "8"
                        break;
                    case "ACM":

                        // 26 - Afghanistan Campaign Medal
                        strRetCode = "26";
                        // "7"
                        break;
                    case "CIB":

                        // 27 - Combat Infantry Badge
                        strRetCode = "27";
                        // "9"
                        break;
                    case "CMB":

                        // 28 - Combat Medic Badge
                        strRetCode = "28";
                        // "Q"
                        break;
                    case "CAB":

                        // 29 - Army Combat Badge
                        strRetCode = "29";
                        // "R"
                        break;
                    case "CRM":

                        // 30 - Marine Combat Action Badge
                        strRetCode = "30";
                        // "T"
                        break;
                    case "CRN":

                        // 31 - Navy Combat Action Badge
                        strRetCode = "31";
                        // "U"
                        break;
                    case "CAM":

                        // 32 - Air Force Combat Action Medal
                        strRetCode = "32";
                        // "W"
                        break;
                    case "NES":

                        // 33 - National Emergency Service Medal
                        strRetCode = "33";
                        // "Y"
                        break;
                    case "WAB":

                        // 34 - Wabanaki Decal
                        strRetCode = "34";
                        // "Z"
                        break;
                    case "AMC":

                        // 35 - Special Air Medal Commemorative Decal
                        strRetCode = "35";
                        // Added for 11/1/2017 legislative updates
                        break;
                    default:
                        strRetCode = "";

                        break;
                }
            }
			GetVeteranDecalCode = strRetCode;
			return GetVeteranDecalCode;
		}

		public string UpdateVeteranDecalCode(string strDecalCode)
		{
			string UpdateVeteranDecalCode = "";
			string strRetTxt = "";
			if (strDecalCode == "" || strDecalCode == "X")
			{
				strRetTxt = "";
			}
			else
            {
                switch (strDecalCode)
                {
                    // BMV # From Appendix G
                    case "01":
                    case "D":

                        // 1 - Distinguished Service Cross Medal
                        strRetTxt = "01";

                        break;
                    case "02":
                    case "A":

                        // 2 - Air Force Cross Medal
                        strRetTxt = "02";

                        break;
                    case "03":
                    case "N":

                        // 3 - Navy Cross Medal
                        strRetTxt = "03";

                        break;
                    case "04":
                    case "S":

                        // 4 - Silver Star Medal
                        strRetTxt = "04";

                        break;
                    case "05":
                    case "F":

                        // 5 - Distinguished Flying Cross
                        strRetTxt = "05";

                        break;
                    case "06":
                    case "5":

                        // 6 - Soldier's Medal
                        strRetTxt = "06";

                        break;
                    case "07":
                    case "M":

                        // 7 - Navy Marine Corps Medal
                        strRetTxt = "07";

                        break;
                    case "08":
                    case "1":

                        // 8 - Airman's Medal
                        strRetTxt = "08";

                        break;
                    case "09":
                    case "C":

                        // 9 - Coast Guard Medal
                        strRetTxt = "09";

                        break;
                    case "10":
                    case "B":

                        // 10 - Bronze Star Medal
                        strRetTxt = "10";

                        break;
                    case "11":
                    case "3":

                        // 11 - European/African Campaign Medal WWII
                        strRetTxt = "11";

                        break;
                    case "12":
                    case "2":

                        // 12 - Asiatic-Pacific Campaign Medal WWII
                        strRetTxt = "12";

                        break;
                    case "13":
                    case "K":

                        // 13 - Korean Service Medal
                        strRetTxt = "13";

                        break;
                    case "14":
                    case "E":

                        // 14 - Armed Forces Expeditionary Medal
                        strRetTxt = "14";

                        break;
                    case "15":
                    case "V":

                        // 15 - Vietnam Service Medal
                        strRetTxt = "15";

                        break;
                    case "16":
                    case "6":

                        // 16 - Southwest Asia Service
                        strRetTxt = "16";

                        break;
                    case "17":
                    case "4":

                        // 17 - Kosovo Medal
                        strRetTxt = "17";

                        break;
                    case "18":
                    case "G":

                        // 18 - United States Army
                        strRetTxt = "18";

                        break;
                    case "19":
                    case "H":

                        // 19 - United States Navy
                        strRetTxt = "19";

                        break;
                    case "20":
                    case "J":

                        // 20 - United States Air Force
                        strRetTxt = "20";

                        break;
                    case "21":
                    case "I":

                        // 21 - United States Marine Corps
                        strRetTxt = "21";

                        break;
                    case "22":
                    case "L":

                        // 22 - United States Coast Guard
                        strRetTxt = "22";

                        break;
                    case "23":
                    case "O":

                        // 23 - Global War on Terrorism Expeditionary Medal
                        strRetTxt = "23";

                        break;
                    case "24":
                    case "P":

                        // 24 - Korean Defense Service Medal
                        strRetTxt = "24";

                        break;
                    case "25":
                    case "8":

                        // 25 - Iraq Campaign Medal
                        strRetTxt = "25";

                        break;
                    case "26":
                    case "7":

                        // 26 - Afghanistan Campaign Medal
                        strRetTxt = "26";

                        break;
                    case "27":
                    case "9":

                        // 27 - Combat Infantry Badge
                        strRetTxt = "27";

                        break;
                    case "28":
                    case "Q":

                        // 28 - Combat Medic Badge
                        strRetTxt = "28";

                        break;
                    case "29":
                    case "R":

                        // 29 - Army Combat Badge
                        strRetTxt = "29";

                        break;
                    case "30":
                    case "T":

                        // 30 - Marine Combat Action Badge
                        strRetTxt = "30";

                        break;
                    case "31":
                    case "U":

                        // 31 - Navy Combat Action Badge
                        strRetTxt = "31";

                        break;
                    case "32":
                    case "W":

                        // 32 - Air Force Combat Action Medal
                        strRetTxt = "32";

                        break;
                    case "33":
                    case "Y":

                        // 33 - National Emergency Service Medal
                        strRetTxt = "33";

                        break;
                    case "34":
                    case "Z":

                        // 34 - Wabanaki Decal
                        strRetTxt = "34";

                        break;
                    case "35":

                        // 35 - Special Air Medal Commemorative Decal
                        strRetTxt = "35";

                        break;
                    default:
                        strRetTxt = "";

                        break;
                }
            }
			UpdateVeteranDecalCode = strRetTxt;
			return UpdateVeteranDecalCode;
		}
	}
}
