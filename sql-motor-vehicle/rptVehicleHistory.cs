﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptVehicleHistory.
	/// </summary>
	public partial class rptVehicleHistory : BaseSectionReport
	{
		public rptVehicleHistory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Vehicle History";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptVehicleHistory InstancePtr
		{
			get
			{
				return (rptVehicleHistory)Sys.GetInstance(typeof(rptVehicleHistory));
			}
		}

		protected rptVehicleHistory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptVehicleHistory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int intRowCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				intRowCounter += 1;
				if (intRowCounter <= frmVehicleHistory.InstancePtr.vsResults.Rows - 1)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label9 As object	OnWrite(string)
			// vbPorter upgrade warning: Label35 As object	OnWrite(string)
			// vbPorter upgrade warning: Label34 As object	OnWrite(string)
			// vbPorter upgrade warning: lblSearchCriteria As object	OnWrite(string)
			string strStatus = "";
			string strDates = "";
			int counter;
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			intRowCounter = 1;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label34.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (frmVehicleHistory.InstancePtr.cmbSearchType.Text == "VIN")
			{
				lblSearchCriteria.Text = "VIN: " + frmVehicleHistory.InstancePtr.vsResults.TextMatrix(1, frmVehicleHistory.InstancePtr.VINCol);
			}
			else
			{
				lblSearchCriteria.Text = "Plate: " + frmVehicleHistory.InstancePtr.vsResults.TextMatrix(1, frmVehicleHistory.InstancePtr.PlateCol);
			}
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			//FC:FINAL:MSH - i.issue #1842: wrong width
			//this.PrintWidth = 15100;
			this.PrintWidth = 15100 / 1440F;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldClass As object	OnWrite(string)
			// vbPorter upgrade warning: fldOwner As object	OnWrite(string)
			// vbPorter upgrade warning: fldPlate As object	OnWrite(string)
			// vbPorter upgrade warning: fldYear As object	OnWrite(string)
			// vbPorter upgrade warning: fldMake As object	OnWrite(string)
			// vbPorter upgrade warning: fldModel As object	OnWrite(string)
			// vbPorter upgrade warning: fldVIN As object	OnWrite(string)
			// vbPorter upgrade warning: fldExpires As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransType As object	OnWrite(string)
			// vbPorter upgrade warning: fldMVR3 As object	OnWrite(string)
			// vbPorter upgrade warning: fldTransDate As object	OnWrite(string)
			fldClass.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.ClassCol);
			fldOwner.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.OwnerCol);
			fldPlate.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.PlateCol);
			fldYear.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.YearCol);
			fldMake.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.MakeCol);
			fldModel.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.ModelCol);
			fldVIN.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.VINCol);
			fldExpires.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.ExpiresCol);
			fldTransType.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.TransTypeCol);
			chkVoid.Checked = FCConvert.CBool(frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.VoidedCol));
			fldMVR3.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.MVR3Col);
			fldTransDate.Text = frmVehicleHistory.InstancePtr.vsResults.TextMatrix(intRowCounter, frmVehicleHistory.InstancePtr.TransDateCol);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label10 As object	OnWrite(string)
			Label10.Text = "Page " + this.PageNumber;
		}

		private void rptVehicleHistory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptVehicleHistory properties;
			//rptVehicleHistory.Caption	= "Vehicle History";
			//rptVehicleHistory.Left	= 0;
			//rptVehicleHistory.Top	= 0;
			//rptVehicleHistory.Width	= 11880;
			//rptVehicleHistory.Height	= 8595;
			//rptVehicleHistory.StartUpPosition	= 3;
			//rptVehicleHistory.SectionData	= "rptVehicleHistory.dsx":0000;
			//End Unmaped Properties
		}
	}
}
