//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.DataBaseLayer;
using fecherFoundation.VisualBasicLayer;
using System.ComponentModel;
using Microsoft.Ajax.Utilities;
using SharedApplication.Extensions;
using TWMV0000.Models;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmPlateInfo : BaseForm
	{
		public frmPlateInfo()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;

			this.lblPlateType = new System.Collections.Generic.List<FCLabel>();
            this.lblPlateType.AddControlArrayElement(this.lblPlateType_0, 0);
            this.lblPlateType.AddControlArrayElement(this.lblPlateType_1, 1);
            this.lblPlateType.AddControlArrayElement(this.lblPlateType_2, 2);

            this.txtPlateType = new System.Collections.Generic.List<T2KOverTypeBox>();
            this.txtPlateType.AddControlArrayElement(this.txtPlateType_0, 0);
            this.txtPlateType.AddControlArrayElement(this.txtPlateType_1, 1);
            this.txtPlateType.AddControlArrayElement(this.txtPlateType_2, 2);

            //FC:FINAL:DDU:#2233 - fixed textbox to don't allow simbols, only alfanumerics
            txtPlateType_0.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',8,32,38,45,48..57,65..90,97..122");
			txtPlateType_1.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',8,32,38,45,48..57,65..90,97..122");
			txtPlateType_2.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter',8,32,38,45,48..57,65..90,97..122");
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPlateInfo InstancePtr
		{
			get
			{
				return (frmPlateInfo)Sys.GetInstance(typeof(frmPlateInfo));
			}
		}

		//FC:FINAL:DDU:#2372 - moved checker before activating the form
		public void ShowForm()
		{
			if (MotorVehicle.JobComplete("frmDataInput"))
			{
				this.Show(App.MainForm);
			}
			else
			{
				//Close();
			}

		}

		protected frmPlateInfo _InstancePtr = null;
		//=========================================================
		int intPlateIndex;
		bool bolFormAlreadyLoaded;
		bool SettingValues;
        public bool OKFlag = true;
		bool PlateFromSearch;
		public bool GoingToDataInput;
		int ClassCol;
		int PlateCol;
		int VINCol;
		int YearCol;
		int MakeCol;
		int ModelCol;
		int UnitCol;
		bool blnEditFlag;
		int TotalCol;
		int RegisterCol;
		public int ExpMonth;
		int FleetNumber;
		bool WrongFee;
		bool SecondResize;
		// vbPorter upgrade warning: TotalState As Decimal	OnWrite(int, Decimal)
		Decimal TotalState;
		// vbPorter upgrade warning: TotalAgent As Decimal	OnWrite(int, Decimal)
		Decimal TotalAgent;
		// vbPorter upgrade warning: TotalLocalTransfer As Decimal	OnWrite(int, Decimal)
		Decimal TotalLocalTransfer;
		// vbPorter upgrade warning: TotalExcise As Decimal	OnWrite(int, Decimal)
		Decimal TotalExcise;
		// vbPorter upgrade warning: TotalSalesTax As Decimal	OnWrite(int, Decimal)
		Decimal TotalSalesTax;
		// vbPorter upgrade warning: TotalTitleFee As Decimal	OnWrite(int, Decimal)
		Decimal TotalTitleFee;
		string strResCodeChecker = "";
		string OwnerCode = "";
		string FirstName = "";
		string LastName = "";
		string StickerType;
		public string strAgentCheck = "";
		public bool blnCameFromVehicleUpdate;

		private void cboClass_DoubleClick(object sender, System.EventArgs e)
		{
			if (!SettingValues)
				Support.SendKeys("{TAB}", false);
		}

		private void cboClass_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strPlate = "";
			if (MotorVehicle.Statics.rsPlateForReg.IsntAnything())
			{
				cboClass.Text = "";
			}
			else
			{
				if (cboClass.SelectedIndex != -1)
				{
					if (MotorVehicle.Statics.Class == "MC" && (cboClass.Text == "XV" || cboClass.Text == "MM" || cboClass.Text == "MQ" || cboClass.Text == "MX"))
					{
						MotorVehicle.Statics.Class = cboClass.Text;
						MotorVehicle.Statics.Class1 = cboClass.Text;
					}
					// kk05172018  tromvs-96  Don't allow new TC (Truck Camper) registrations
					if (cboClass.Text == "TC" && MotorVehicle.Statics.RegistrationType != "DUPREG" && MotorVehicle.Statics.RegistrationType != "LOST" && MotorVehicle.Statics.RegistrationType != "CORR")
					{
						MessageBox.Show("Effective November 1, 2017, municipalities will no longer be registering truck campers and therefore will not be issuing decals", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						cboClass.Text = "";
						e.Cancel = true;
						return;
					}
					if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 2)
					{
						if (cboClass.Text == "TX" || cboClass.Text == "CS" || cboClass.Text == "ST")
						{
							MessageBox.Show("You may not change the plate to a TX, CS, or ST class plate", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							cboClass.Text = "";
							e.Cancel = true;
						}
						else
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != cboClass.Text)
							{
								strPlate = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
								MotorVehicle.Statics.rsPlateForReg.AddNew();
								MotorVehicle.Statics.rsPlateForReg.Set_Fields("Plate", strPlate);
								MotorVehicle.Statics.NewToTheSystem = true;
								MotorVehicle.Statics.Class = cboClass.Text;
							}
						}
					}
				}
			}
		}

		private void cboClass2_DoubleClick(object sender, System.EventArgs e)
		{
			if (!SettingValues)
				Support.SendKeys("{TAB}", false);
		}

		private void cboClass2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strPlate = "";
			if (MotorVehicle.Statics.rsSecondPlate.IsntAnything())
			{
				cboClass2.Text = "";
			}
			else
			{
				if (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != cboClass2.Text)
				{
					strPlate = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate"));
					MotorVehicle.Statics.rsSecondPlate.AddNew();
					MotorVehicle.Statics.rsSecondPlate.Set_Fields("Plate", strPlate);
					MotorVehicle.Statics.NewToTheSystem = true;
					MotorVehicle.Statics.Class2 = cboClass2.Text;
				}
			}
		}

		private void cboFleets_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int counter = 0;
			int counter2 = 0;
			int tempWidth = 0;
			DateTime CheckDate;
			int fnx;
			int total = 0;
			string strBadData = "";
			bool blnPending = false;

			try
			{
				fecherFoundation.Information.Err().Clear();
				if (!blnEditFlag)
				{
					if (cboFleets.SelectedIndex != -1)
					{
						GetFleetInfo();
						if (!MotorVehicle.Statics.RegisteringFleet)
						{
							MotorVehicle.Statics.rsVehicles.OpenRecordset("SELECT * FROM Master WHERE Status <> 'T' AND FleetNumber = " + FCConvert.ToString(Conversion.Val(fecherFoundation.Strings.Trim(Strings.Left(cboFleets.Text, 5)))) + " ORDER BY Plate");
							if (MotorVehicle.Statics.rsVehicles.EndOfFile() != true && MotorVehicle.Statics.rsVehicles.BeginningOfFile() != true)
							{
								frmWait.InstancePtr.Show();
								frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Searching";
								frmWait.InstancePtr.Refresh();
								MotorVehicle.Statics.rsVehicles.MoveLast();
								MotorVehicle.Statics.rsVehicles.MoveFirst();
								vsVehicles.Rows = MotorVehicle.Statics.rsVehicles.RecordCount() + 1;
								vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, vsVehicles.Rows - 1, 0, Color.Red);
								tempWidth = 0;
								counter = 1;
								blnPending = false;
								do
								{
									WrongFee = false;
									CheckDate = fecherFoundation.DateAndTime.DateAdd("m", 5, DateTime.Today);
									CheckDate = FCConvert.ToDateTime(Strings.Format(FCConvert.ToString(CheckDate.Month) + "/01/" + FCConvert.ToString(CheckDate.Year), "MM/dd/yyyy"));
									CheckDate = frmDataInput.InstancePtr.FindLastDay(FCConvert.ToString(CheckDate));
									if (MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate").ToOADate() > CheckDate.ToOADate())
									{
										vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, Color.Lime);
										vsVehicles.TextMatrix(counter, 0, "Registered");
										vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(false));
									}
									else
									{
										if (MotorVehicle.Statics.RegisteringFleet)
										{
											if (MotorVehicle.Statics.VehiclesRegistered[counter - 1] == false)
											{
												vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, Color.Black);
												vsVehicles.TextMatrix(counter, 0, "Not Registering");
												vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(false));
											}
											else
											{
												vsVehicles.TextMatrix(counter, 0, "Unregistered");
												vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(true));
											}
										}
										else
										{
											vsVehicles.TextMatrix(counter, 0, "Unregistered");
											vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(true));
										}
									}
									// kk05022017 tromv-1272  Check for "NEW" plates in fleet list
									if (FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Plate")) == "NEW")
									{
										vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, Color.Red);
										vsVehicles.TextMatrix(counter, 0, "Can't Register");
										vsVehicles.TextMatrix(counter, RegisterCol, FCConvert.ToString(false));
										blnPending = true;
									}
									strBadData = "class information";
									vsVehicles.TextMatrix(counter, ClassCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")));
									strBadData = "plate information";
									vsVehicles.TextMatrix(counter, PlateCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("plate")));
									strBadData = "VIN information";
									vsVehicles.TextMatrix(counter, VINCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Vin")));
									strBadData = "year information";
									vsVehicles.TextMatrix(counter, YearCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields("Year")));
									strBadData = "make information";
									vsVehicles.TextMatrix(counter, MakeCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("make")));
									strBadData = "model information";
									vsVehicles.TextMatrix(counter, ModelCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("model")));
									if (cmbGroup.Text == "Group")
									{
										strBadData = "expiration date information";
										vsVehicles.TextMatrix(counter, UnitCol, Strings.Format(MotorVehicle.Statics.rsVehicles.Get_Fields("ExpireDate"), "MM/dd/yy"));
									}
									else
									{
										strBadData = "unit information";
										vsVehicles.TextMatrix(counter, UnitCol, FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("UnitNumber")));
									}
									strBadData = "fees information";
									vsVehicles.TextMatrix(counter, TotalCol, FCConvert.ToString(CalcStateFee(MotorVehicle.Statics.rsVehicles) + CalcLocalFee(MotorVehicle.Statics.rsVehicles)));
									if (WrongFee)
									{
										vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, TotalCol, Color.Red);
									}
									counter += 1;
									MotorVehicle.Statics.rsVehicles.MoveNext();
								}
								while (MotorVehicle.Statics.rsVehicles.EndOfFile() != true);
								frmWait.InstancePtr.Unload();
								// kk05032017 tromv-1272  Warn user that some of the registrations can't be done
								if (blnPending)
								{
									MessageBox.Show("Unable to process registrations in pending status. Change the status from pending to process \"NEW\" registrations", "Pending Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								//vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, vsVehicles.Rows - 1, 0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
								for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
								{
									if (vsVehicles.TextMatrix(counter, 0) == "Unregistered")
									{
										vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, 0xFF0000);
										vsVehicles.TextMatrix(counter, 0, "Next");
										break;
									}
								}
								if (vsVehicles.Visible == false)
								{
									vsVehicles.Visible = true;
								}
								// DoEvents
								if (cmbGroup.Text == "Group")
								{
									vsVehicles.TextMatrix(0, UnitCol, "Expires");
								}
								else
								{
									vsVehicles.TextMatrix(0, UnitCol, "Unit");
								}
								
								if (vsVehicles.Visible == true)
								{
									vsVehicles.Focus();
								}
							}
							else
							{
								if (cmbGroup.Text == "Fleet")
								{
									MessageBox.Show("No vehicles found for this Fleet", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								else
								{
									MessageBox.Show("No vehicles found for this Group", "No Vehicles", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
							}
						}
						else
						{
							vsVehicles.Rows = MotorVehicle.Statics.NumberOfVehicles + 1;
							vsVehicles.Cols = 10;
							for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
							{
								counter2 = 0;
								if (FCUtils.IsEmpty(MotorVehicle.Statics.VehicleInformation[counter2, counter]))
								{
									vsVehicles.Rows -= 1;
									goto DataDone;
								}
								for (counter2 = 0; counter2 <= 9; counter2++)
								{
									vsVehicles.TextMatrix(counter, counter2, FCConvert.ToString(MotorVehicle.Statics.VehicleInformation[counter2, counter]));
								}
								if (vsVehicles.TextMatrix(counter, 0) == "Registered")
								{
									vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, Color.Lime);
								}
								else if (vsVehicles.TextMatrix(counter, 0) == "Not Registering")
								{
									vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, Color.Black);
								}
								else if (vsVehicles.TextMatrix(counter, 0) == "Next")
								{
									vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, 0xFF0000);
								}
								else
								{
									vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, Color.Red);
								}
							}
							DataDone:
							for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
							{
								if (vsVehicles.TextMatrix(counter, 0) == "Unregistered")
								{
									vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, 0xFF0000);
									vsVehicles.TextMatrix(counter, 0, "Next");
									break;
								}
							}
							if (vsVehicles.Visible == false)
							{
								vsVehicles.Visible = true;
							}
							if (cmbGroup.Text == "Group")
							{
								vsVehicles.TextMatrix(0, UnitCol, "Expires");
							}
							else
							{
								vsVehicles.TextMatrix(0, UnitCol, "Unit");
							}
							Form_Resize();
							if (vsVehicles.Visible == true)
							{
								vsVehicles.Focus();
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				if (fecherFoundation.Information.Err(ex).Number == 94)
				{
					if (strBadData != "Class" && strBadData != "Plate")
					{
						MessageBox.Show("Vehicle " + MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsVehicles.Get_Fields_String("plate") + " is missing " + strBadData);
					}
					else
					{
						MessageBox.Show("One of the vehicles in the fleet is missing " + strBadData);
					}
				}
				else
				{
					MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + "\r\n" + fecherFoundation.Information.Err(ex).Description);
				}
				StaticSettings.GlobalTelemetryService.TrackException(ex);
				frmWait.InstancePtr.Unload();
			}
		}

		public void cboFleets_Click()
		{
			cboFleets_SelectedIndexChanged(cboFleets, new System.EventArgs());
		}

		private void cboLongTermRegPlateYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (txtPlateType[2].Visible == true && txtPlateType[2].Text.Length > 0)
			{
				cmdProcess.Enabled = true;
				return;
			}
			else if (txtPlateType[1].Visible == true && txtPlateType[1].Text.Length > 0)
			{
				cmdProcess.Enabled = true;
				return;
			}
			else if (cboLongTermRegPlateYear.Visible == true && cboLongTermRegPlateYear.SelectedIndex >= 0)
			{
				cmdProcess.Enabled = true;
			}
		}

		private void cboLongTermRegPlateYear_Leave(object sender, System.EventArgs e)
		{
			if (txtPlateType[2].Visible == true && txtPlateType[2].Text.Length > 0)
			{
				cmdProcess.Enabled = true;
				return;
			}
			else if (txtPlateType[1].Visible == true && txtPlateType[1].Text.Length > 0)
			{
				cmdProcess.Enabled = true;
				return;
			}
			else if (cboLongTermRegPlateYear.Visible == true && cboLongTermRegPlateYear.SelectedIndex >= 0)
			{
				cmdProcess.Enabled = true;
				if (txtPlateType[1].Visible == true)
				{
					txtPlateType[1].Focus();
				}
            }
		}

		private void cboLongTermRegPlateYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if(!cboLongTermRegPlateYear.Visible)
			{
				return;
			}
			GetNewLongTermPlate(fecherFoundation.Strings.Trim(cboLongTermRegPlateYear.Text));
			if (MotorVehicle.Statics.rsPlateForReg.IsntAnything())
			{
				e.Cancel = true;
				return;
			}
		}

		public void cmdCancel_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.Statics.PreEffectiveDate = DateTime.Today;
			MotorVehicle.Statics.PendingRegistration = false;
			fraFleetRegistration.Visible = false;
			MotorVehicle.Statics.RegisteringFleet = false;
			cmbGroup.Enabled = true;
			cboFleets.Enabled = true;
			MotorVehicle.Statics.rsVehicles.Reset();
			cmdStartOver_Click();
			if (MotorVehicle.Statics.bolFromWindowsCR)
			{
				MotorVehicle.Write_Fleet_PDS_Work_Record_Stuff();
				MDIParent.InstancePtr.Menu18();
			}
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			DialogResult ans = 0;
			if (MotorVehicle.Statics.FromTransfer)
			{
				ans = MessageBox.Show("You are in the middle of processing a registration.  Do you wish to cancel this registration?", "Cancel Registration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					MotorVehicle.Statics.FromTransfer = false;
					Close();
				}
				else
				{
					return;
				}
			}
			else
			{
				Close();
			}
		}

		private void cmdGetInfo_Click(object sender, System.EventArgs e)
		{
			string strCheck = "";
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			EnableDisableForm(true);
			fraResults.Visible = false;
			fraSelection.Visible = false;
			if (MotorVehicle.Statics.RegistrationType == "HELD")
			{
				if (intPlateIndex == 0)
				{
					strCheck = vsResults.TextMatrix(vsResults.Row, 0);
					MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE MasterID = " + FCConvert.ToString(Conversion.Val(strCheck)) + " AND Status = 'H'");
					MotorVehicle.Statics.rsPlateForReg.MoveLast();
					MotorVehicle.Statics.rsPlateForReg.MoveFirst();
					if (MotorVehicle.Statics.rsPlateForReg.RecordCount() == 0)
					{
						MessageBox.Show("There was no Held Registration found for the Plate  " + MotorVehicle.Statics.Plate1 + "", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					MotorVehicle.Statics.Plate1 = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
					txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
					MotorVehicle.Statics.strSql = "SELECT * FROM HeldRegistrationMaster WHERE MasterID = " + FCConvert.ToString(Conversion.Val(strCheck)) + " AND Status = 'FC'";
					MotorVehicle.Statics.rsFinalCompare.OpenRecordset(MotorVehicle.Statics.strSql);
					if (MotorVehicle.Statics.rsFinalCompare.EndOfFile() != true && MotorVehicle.Statics.rsFinalCompare.BeginningOfFile() != true)
					{
						MotorVehicle.Statics.rsFinalCompare.MoveLast();
						MotorVehicle.Statics.rsFinalCompare.MoveFirst();
						pInfo = pCont.GetParty(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID1"));
						MotorVehicle.Statics.orgOwnerInfo.Owner1Name = fecherFoundation.Strings.UCase(pInfo.FullName);
						if (Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID2")) != 0)
						{
							pInfo = pCont.GetParty(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID2"));
							MotorVehicle.Statics.orgOwnerInfo.Owner2Name = fecherFoundation.Strings.UCase(pInfo.FullName);
						}
						else
						{
							MotorVehicle.Statics.orgOwnerInfo.Owner2Name = "";
						}
						if (Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID3")) != 0)
						{
							pInfo = pCont.GetParty(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID3"));
							MotorVehicle.Statics.orgOwnerInfo.Owner3Name = fecherFoundation.Strings.UCase(pInfo.FullName);
						}
						else
						{
							MotorVehicle.Statics.orgOwnerInfo.Owner3Name = "";
						}
					}
					MotorVehicle.Statics.Class = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
					cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.Class);
					SettingValues = false;
					goto endofsearchtag;
				}
			}
			SetPlateFromSearch();
			if (intPlateIndex == 0)
			{
				if (MotorVehicle.Statics.rsPlateForReg.IsntAnything())
				{
					if (cmdProcess.Enabled == true)
						cmdProcess.Enabled = false;
					if (cboLongTermRegPlateYear.Visible)
					{
						cboLongTermRegPlateYear.Focus();
					}
					else
					{
						txtPlateType[0].Focus();
					}
					return;
				}
			}
			else if (intPlateIndex == 1)
			{
				if (MotorVehicle.Statics.rsSecondPlate.IsntAnything())
				{
					if (cmdProcess.Enabled == true)
						cmdProcess.Enabled = false;
					txtPlateType[1].Focus();
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.rsThirdPlate.IsntAnything())
				{
					if (cmdProcess.Enabled == true)
						cmdProcess.Enabled = false;
					txtPlateType[2].Focus();
					return;
				}
			}
			if (MotorVehicle.Statics.RegistrationType != "UPD" && MotorVehicle.Statics.RegistrationType != "BOOST")
			{
				clsDRWrapper temp = MotorVehicle.Statics.rsPlateForReg;
				if (intPlateIndex == 0)
				{
					MotorVehicle.TransferRecordToArchive(ref temp, intPlateIndex);
				}
				else if (intPlateIndex == 1)
				{
					MotorVehicle.TransferRecordToArchive(ref temp, intPlateIndex);
				}
				else
				{
					MotorVehicle.TransferRecordToArchive(ref temp, intPlateIndex);
				}
				MotorVehicle.Statics.rsPlateForReg = temp;
			}
			endofsearchtag:
			;
			if (cmdProcess.Enabled == true)
			{
				if (txtPlateType[1].Visible == true)
				{
					if (MotorVehicle.Statics.rsSecondPlate.IsntAnything())
					{
						txtPlateType[1].Focus();
					}
					else
					{
						cmdProcess_Click();
					}
				}
				else
				{
					cmdProcess_Click();
				}
			}
		}

		private void cmdPreRegister_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsPendingInventory = new clsDRWrapper();
			clsDRWrapper rsPendingActivity = new clsDRWrapper();
			clsDRWrapper rsUpdateActivity = new clsDRWrapper();
			clsDRWrapper rsActivityMaster = new clsDRWrapper();
			clsDRWrapper rsCurrentInfo = new clsDRWrapper();
            clsDRWrapper rsPreReg = new clsDRWrapper();

            try
            {
				string strFields = "";
				string strValues = "";
				int lngTemp;
				int fnx;
				if (cboFleets.SelectedIndex == -1)
				{
					MessageBox.Show("You must select a fleet or group before you may continue", "No Fleet / Group Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "STAAB AGENCY" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
				{
					rsPreReg.OpenRecordset("SELECT c.*, p.FullName, p.FirstName, p.LastName FROM PendingActivityMaster as c LEFT JOIN " + rsPreReg.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE (Class <> 'TL' OR Subclass <> 'L9') AND FleetNumber = " + FCConvert.ToString(FleetNumber));
				}
				else
				{
					rsPreReg.OpenRecordset("SELECT c.*, p.FullName, p.FirstName, p.LastName FROM PendingActivityMaster as c LEFT JOIN " + rsPreReg.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE FleetNumber = " + FCConvert.ToString(FleetNumber));
				}
				if (rsPreReg.EndOfFile() != true && rsPreReg.BeginningOfFile() != true)
				{
					rsPreReg.MoveLast();
					rsPreReg.MoveFirst();
					TotalAgent = 0;
					TotalState = 0;
					TotalExcise = 0;
					TotalLocalTransfer = 0;
					TotalSalesTax = 0;
					TotalTitleFee = 0;
					OwnerCode = FCConvert.ToString(rsPreReg.Get_Fields_String("OwnerCode1"));
					strResCodeChecker = FCConvert.ToString(rsPreReg.Get_Fields_String("ResidenceCode"));
					if (OwnerCode == "I")
					{
						LastName = fecherFoundation.Strings.UCase(FCConvert.ToString(rsPreReg.Get_Fields_String("LastName")));
						FirstName = fecherFoundation.Strings.UCase(FCConvert.ToString(rsPreReg.Get_Fields_String("FirstName")));
					}
					else
					{
						FirstName = fecherFoundation.Strings.UCase(FCConvert.ToString(rsPreReg.Get_Fields_String("FullName")));
					}
					MotorVehicle.Statics.lngFleetOwnerPartyID = FCConvert.ToString(rsPreReg.Get_Fields_Int32("PartyID1"));
					rsActivityMaster.OpenRecordset("SELECT * FROM ActivityMaster");
					do
					{
						TotalState += (rsPreReg.Get_Fields("StatePaid") - (rsPreReg.Get_Fields("TitleFee") + rsPreReg.Get_Fields("SalesTax")));
						TotalAgent += rsPreReg.Get_Fields("AgentFee");
						TotalLocalTransfer += rsPreReg.Get_Fields_Decimal("ExciseTransferCharge");
						if (rsPreReg.Get_Fields_Decimal("ExciseTaxCharged") - rsPreReg.Get_Fields_Decimal("ExciseCreditUsed") < 0)
						{
							// do nothing
						}
						else
						{
							TotalExcise += (rsPreReg.Get_Fields_Decimal("ExciseTaxCharged") - rsPreReg.Get_Fields_Decimal("ExciseCreditUsed"));
						}
						TotalSalesTax += rsPreReg.Get_Fields("SalesTax");
						TotalTitleFee += rsPreReg.Get_Fields("TitleFee");
						if (FCConvert.ToInt32(rsPreReg.Get_Fields_Int32("MonthStickerNumber")) != 0)
						{
							string suffix = rsPreReg.Get_Fields_Int32("MonthStickerMonth").ToString().PadLeft(2, '0');

							MakePreRegCode("M", rsPreReg.Get_Fields_Int32("MonthStickerCharge") + rsPreReg.Get_Fields_Int32("MonthStickerNoCharge"), suffix);
							rsPendingInventory.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + MotorVehicle.Statics.strCodeM + "' AND AdjustmentCode = 'P' AND Low = '" + rsPreReg.Get_Fields_Int32("MonthStickerNumber") + "'");
							if (rsPendingInventory.EndOfFile() != true && rsPendingInventory.BeginningOfFile() != true)
							{
								rsPendingInventory.MoveLast();
								rsPendingInventory.MoveFirst();
								rsPendingInventory.Edit();
								rsPendingInventory.Set_Fields("AdjustmentCode", "I");
								rsPendingInventory.Update();
							}
							else
							{
								MessageBox.Show("Pending Month Sticker was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No Month Sticker Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							rsPendingInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeM + "' AND Number = " + rsPreReg.Get_Fields_Int32("MonthStickerNumber") + " AND Status = 'P'");
							if (rsPendingInventory.EndOfFile() != true && rsPendingInventory.BeginningOfFile() != true)
							{
								rsPendingInventory.MoveLast();
								rsPendingInventory.MoveFirst();
								rsPendingInventory.Edit();
								rsPendingInventory.Set_Fields("Status", "I");
								rsPendingInventory.Update();
							}
							else
							{
								MessageBox.Show("Pending Month Sticker was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No Month Sticker Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						if (FCConvert.ToInt32(rsPreReg.Get_Fields_Int32("YearStickerNumber")) != 0)
						{
							var suffix = rsPreReg.Get_Fields_DateTime("ExpireDate").ToString().Right(2);
							MakePreRegCode("Y", rsPreReg.Get_Fields_Int32("YearStickerCharge") + rsPreReg.Get_Fields_Int32("YearStickerNoCharge"), suffix);
							rsPendingInventory.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = '" + MotorVehicle.Statics.strCodeY + "' AND AdjustmentCode = 'P' AND Low = '" + rsPreReg.Get_Fields_Int32("YearStickerNumber") + "'");
							if (rsPendingInventory.EndOfFile() != true && rsPendingInventory.BeginningOfFile() != true)
							{
								rsPendingInventory.MoveLast();
								rsPendingInventory.MoveFirst();
								rsPendingInventory.Edit();
								rsPendingInventory.Set_Fields("AdjustmentCode", "I");
								rsPendingInventory.Update();
							}
							else
							{
								MessageBox.Show("Pending Year Sticker was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No Year Sticker Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							rsPendingInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + rsPreReg.Get_Fields_Int32("YearStickerNumber") + " AND Status = 'P'");
							if (rsPendingInventory.EndOfFile() != true && rsPendingInventory.BeginningOfFile() != true)
							{
								rsPendingInventory.MoveLast();
								rsPendingInventory.MoveFirst();
								rsPendingInventory.Edit();
								rsPendingInventory.Set_Fields("Status", "I");
								rsPendingInventory.Update();
							}
							else
							{
								MessageBox.Show("Pending Year Sticker was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No Year Sticker Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						rsPendingInventory.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = 'MXS00' AND AdjustmentCode = 'P' AND Low = '" + rsPreReg.Get_Fields_Int32("MVR3") + "'");
						if (rsPendingInventory.EndOfFile() != true && rsPendingInventory.BeginningOfFile() != true)
						{
							rsPendingInventory.MoveLast();
							rsPendingInventory.MoveFirst();
							rsPendingInventory.Edit();
							rsPendingInventory.Set_Fields("AdjustmentCode", "I");
							rsPendingInventory.Update();
						}
						else
						{
							MessageBox.Show("Pending MVR3 was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No MVR3 Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						rsPendingInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' AND Number = " + rsPreReg.Get_Fields_Int32("MVR3") + " AND Status = 'P'");
						if (rsPendingInventory.EndOfFile() != true && rsPendingInventory.BeginningOfFile() != true)
						{
							rsPendingInventory.MoveLast();
							rsPendingInventory.MoveFirst();
							rsPendingInventory.Edit();
							rsPendingInventory.Set_Fields("Status", "I");
							rsPendingInventory.Update();
						}
						else
						{
							MessageBox.Show("Pending MVR3 was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No MVR3 Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						if (rsPreReg.Get_Fields_Boolean("RENTAL") == true && rsPreReg.Get_Fields_Boolean("NoFeeDupReg") == true)
						{
							rsPendingInventory.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE InventoryType = 'MXS00' AND AdjustmentCode = 'P' AND Low = '" + rsPreReg.Get_Fields_Int32("NoFeeMVR3Number") + "'");
							if (rsPendingInventory.EndOfFile() != true && rsPendingInventory.BeginningOfFile() != true)
							{
								rsPendingInventory.MoveLast();
								rsPendingInventory.MoveFirst();
								rsPendingInventory.Edit();
								rsPendingInventory.Set_Fields("AdjustmentCode", "I");
								rsPendingInventory.Update();
							}
							else
							{
								MessageBox.Show("Pending No Fee Duplicate MVR3 was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No Duplicate MVR3 Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							rsPendingInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' AND Number = " + rsPreReg.Get_Fields_Int32("NoFeeMVR3Number") + " AND Status = 'P'");
							if (rsPendingInventory.EndOfFile() != true && rsPendingInventory.BeginningOfFile() != true)
							{
								rsPendingInventory.MoveLast();
								rsPendingInventory.MoveFirst();
								rsPendingInventory.Edit();
								rsPendingInventory.Set_Fields("Status", "I");
								rsPendingInventory.Update();
							}
							else
							{
								MessageBox.Show("Pending No Fee Duplicate MVR3 was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No Duplicate MVR3 Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						if ((FCConvert.ToString(rsPreReg.Get_Fields("Transfer")) == "N" || FCConvert.ToString(rsPreReg.Get_Fields("Transfer")) == "R") && FCConvert.ToString(rsPreReg.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(rsPreReg.Get_Fields_String("plate")) != "NEW")
						{
							rsPendingInventory.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE AdjustmentCode = 'P' AND Low = '" + rsPreReg.Get_Fields_String("plate") + "'");
							if (rsPendingInventory.EndOfFile() != true && rsPendingInventory.BeginningOfFile() != true)
							{
								rsPendingInventory.MoveLast();
								rsPendingInventory.MoveFirst();
								rsPendingInventory.Edit();
								rsPendingInventory.Set_Fields("AdjustmentCode", "I");
								rsPendingInventory.Update();
							}
							else
							{
								MessageBox.Show("Pending Plate was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No Plate Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							rsPendingInventory.OpenRecordset("SELECT * FROM Inventory WHERE FormattedInventory = '" + rsPreReg.Get_Fields_String("plate") + "' AND Status = 'P'");
							if (rsPendingInventory.EndOfFile() != true && rsPendingInventory.BeginningOfFile() != true)
							{
								rsPendingInventory.MoveLast();
								rsPendingInventory.MoveFirst();
								rsPendingInventory.Edit();
								rsPendingInventory.Set_Fields("Status", "I");
								rsPendingInventory.Update();
							}
							else
							{
								MessageBox.Show("Pending Plate was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No Plate Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						if (rsPreReg.Get_Fields_Boolean("RENTAL") == true && rsPreReg.Get_Fields_Boolean("NoFeeDupReg") == true)
						{
							rsPendingActivity.OpenRecordset("SELECT * FROM PendingExceptionReport WHERE MVR3Number = " + rsPreReg.Get_Fields_Int32("NoFeeMVR3Number"));
							if (rsPendingActivity.EndOfFile() != true && rsPendingActivity.BeginningOfFile() != true)
							{
								rsPendingActivity.MoveLast();
								rsPendingActivity.MoveFirst();
								rsUpdateActivity.OpenRecordset("SELECT * FROM ExceptionReport");
								rsUpdateActivity.AddNew();
								rsUpdateActivity.Set_Fields("Type", rsPendingActivity.Get_Fields("Type"));
								rsUpdateActivity.Set_Fields("MVR3Number", rsPendingActivity.Get_Fields_Int32("MVR3Number"));
								rsUpdateActivity.Set_Fields("newclass", rsPendingActivity.Get_Fields_String("newclass"));
								rsUpdateActivity.Set_Fields("NewPlate", rsPendingActivity.Get_Fields_String("NewPlate"));
								rsUpdateActivity.Set_Fields("reason", rsPendingActivity.Get_Fields_String("reason"));
								rsUpdateActivity.Set_Fields("OpID", rsPendingActivity.Get_Fields_String("OpID"));
								rsUpdateActivity.Set_Fields("PeriodCloseoutID", 0);
								rsUpdateActivity.Update();
								rsPendingActivity.Delete();
								rsPendingActivity.Update();
							}
							else
							{
								MessageBox.Show("Pending Exception Report Item was not Found for Plate " + rsPreReg.Get_Fields_String("plate") + "", "No Exception Report Item Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						if (FCConvert.ToInt32(rsPreReg.Get_Fields_String("CTANumber")) != 0 && fecherFoundation.Strings.Trim(FCConvert.ToString(rsPreReg.Get_Fields_String("CTANumber"))) != "")
						{
							rsPendingActivity.OpenRecordset("SELECT * FROM PendingTitleApplications WHERE CTANumber = '" + rsPreReg.Get_Fields_String("CTANumber") + "'");
							if (rsPendingActivity.EndOfFile() != true && rsPendingActivity.BeginningOfFile() != true)
							{
								rsPendingActivity.MoveLast();
								rsPendingActivity.MoveFirst();
								rsCurrentInfo.OpenRecordset("SELECT * FROM TitleApplications");
								rsCurrentInfo.AddNew();
								rsCurrentInfo.Set_Fields("CTANumber", rsPendingActivity.Get_Fields_String("CTANumber").ToUpper());
								rsCurrentInfo.Set_Fields("Fee", rsPendingActivity.Get_Fields("Fee"));
								rsCurrentInfo.Set_Fields("Class", rsPendingActivity.Get_Fields_String("Class"));
								rsCurrentInfo.Set_Fields("Plate", rsPendingActivity.Get_Fields_String("Plate"));
								rsCurrentInfo.Set_Fields("RegistrationDate", rsPendingActivity.Get_Fields_DateTime("RegistrationDate"));
								rsCurrentInfo.Set_Fields("CustomerName", rsPendingActivity.Get_Fields_String("CustomerName"));
								rsCurrentInfo.Set_Fields("OPID", rsPendingActivity.Get_Fields_String("OPID"));
								rsCurrentInfo.Set_Fields("DoubleIfApplicable", rsPendingActivity.Get_Fields_Int32("DoubleIfApplicable"));
								rsCurrentInfo.Set_Fields("PeriodCloseoutID", 0);
								rsCurrentInfo.Update();
								rsPendingActivity.Delete();
								rsPendingActivity.Update();
							}
						}
						rsCurrentInfo.OpenRecordset("SELECT * FROM ActivityMaster");
						rsCurrentInfo.AddNew();
						for (fnx = 1; fnx <= rsPreReg.FieldsCount - 1; fnx++)
						{
							if (rsPreReg.Get_FieldsIndexName(fnx) == "OldMVR3")
							{
								rsCurrentInfo.Set_Fields(rsPreReg.Get_FieldsIndexName(fnx), 0);
							}
							else if (rsPreReg.Get_FieldsIndexName(fnx) == "DateUpdated")
							{
								rsCurrentInfo.Set_Fields(rsPreReg.Get_FieldsIndexName(fnx), DateTime.Today);
							}
							else
							{
								rsCurrentInfo.Set_Fields(rsPreReg.Get_FieldsIndexName(fnx), rsPreReg.Get_FieldsIndexValue(fnx));
							}
						}
						rsCurrentInfo.Update();
						rsPreReg.Delete();
						rsPreReg.Update();
						rsPreReg.MoveNext();
					}
					while (rsPreReg.EndOfFile() != true);
					if (MotorVehicle.Statics.bolFromDosTrio == true || MotorVehicle.Statics.bolFromWindowsCR == true)
					{
						Write_Pending_PDS_Work_Record_Stuff();
						if (MotorVehicle.Statics.bolFromWindowsCR)
						{
							MDIParent.InstancePtr.Menu18();
						}
					}
					else
					{
						MessageBox.Show("Total Amount Owed:  " + Strings.Format(TotalAgent + TotalState + TotalExcise + TotalLocalTransfer + TotalSalesTax + TotalTitleFee, "$#,##0.00"), "Amount Owed", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					if (cmbGroup.Text == "Fleet")
					{
						MessageBox.Show("There are currently no Pre Registrations for this Fleet", "No Pre Registrations Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("There are currently no Pre Registrations for this Group", "No Pre Registrations Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
            finally
            {
                rsPendingInventory.Dispose();
                rsPendingActivity.Dispose();
                rsUpdateActivity.Dispose();
                rsActivityMaster.Dispose();
                rsCurrentInfo.Dispose();
                rsPreReg.Dispose();
			}
        }

		public void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper temp = new clsDRWrapper();
			clsDRWrapper rsSubClass2 = new clsDRWrapper();
			clsDRWrapper rsTPlate = new clsDRWrapper();
			clsDRWrapper rsHeldInfo = new clsDRWrapper();
			clsDRWrapper rsBoosters = new clsDRWrapper();
			clsDRWrapper rsUser = new clsDRWrapper();
            clsDRWrapper rsClass = new clsDRWrapper();
            clsDRWrapper rsSubClass = new clsDRWrapper();

			int counter;
			// vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
			DialogResult answer = 0;
			// vbPorter upgrade warning: xdate As object	OnWrite(object, DateTime)
			DateTime xdate = default(DateTime);
			DateTime LowDate;
			string strPlate = "";
			string strClass = "";
			string strTestPlate = "";
            int intTemp = 0;

            try
            {
				if (MotorVehicle.Statics.FromTransfer)
				{
					modGNBas.Sleep(1000);
				}
				if (txtPlateType[1].Visible == false)
				{
					MotorVehicle.Statics.FinalCompareID2 = 0;
				}
				if (txtPlateType[2].Visible == false)
				{
					MotorVehicle.Statics.FinalCompareID3 = 0;
				}

				MotorVehicle.Statics.CorrectingNewRegMotorCycle = false;
				PlateFromSearch = false;
				MotorVehicle.Statics.gboolFromLongTermDataEntry = false;
				if (txtPlateType[0].Visible)
				{
					if (MotorVehicle.Statics.rsPlateForReg.IsntAnything())
					{
						MessageBox.Show("You must enter a valid plate before you may proceed", "Enter Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtPlateType[0].Focus();
						return;
					}
					else
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate")) != "NEW")
						{
							if (cmbRegType1.Text != "Complete Held Registration")
							{
								rsHeldInfo.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE Plate = '" + MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate") + "' AND Status = 'H'");
								if (rsHeldInfo.EndOfFile() != true && rsHeldInfo.BeginningOfFile() != true)
								{
									answer = MessageBox.Show("There is a held registration in the system for plate " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate") + ".  Would you like to complete the Held Registration?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
									if (answer == DialogResult.No)
									{
										// do nothing
									}
									else
									{
										OKFlag = true;
										cmbRegType1.Text = "Complete Held Registration";
										//App.DoEvents();
										txtPlateType[0].Focus();
										txtPlateType[0].Text = FCConvert.ToString(rsHeldInfo.Get_Fields_String("Plate"));
										return;
									}
								}
							}
						}
					}
				}
				if (txtPlateType[1].Visible)
				{
					if (MotorVehicle.Statics.rsSecondPlate.IsntAnything())
					{
						MessageBox.Show("You must enter a valid plate before you may proceed", "Enter Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtPlateType[1].Focus();
						return;
					}
					else
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate")) != "NEW")
						{
							if (cmbRegType1.Text != "Complete Held Registration")
							{
								rsHeldInfo.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE Plate = '" + MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate") + "' AND Status = 'H'");
								if (rsHeldInfo.EndOfFile() != true && rsHeldInfo.BeginningOfFile() != true)
								{
									answer = MessageBox.Show("There is a held registration in the system for plate " + MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate") + ".  Would you like to complete the Held Registration?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
									if (answer == DialogResult.No)
									{
										// do nothing
									}
									else
									{
										OKFlag = true;
										cmbRegType1.Text = "Complete Held Registration";
										//App.DoEvents();
										txtPlateType[0].Focus();
										txtPlateType[0].Text = FCConvert.ToString(rsHeldInfo.Get_Fields_String("Plate"));
										return;
									}
								}
							}
						}
					}
				}
				if (txtPlateType[2].Visible)
				{
					if (MotorVehicle.Statics.rsThirdPlate.IsntAnything())
					{
						MessageBox.Show("You must enter a valid plate before you may proceed", "Enter Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtPlateType[2].Focus();
						return;
					}
					else
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("Plate")) != "NEW")
						{
							if (cmbRegType1.Text != "Complete Held Registration")
							{
								rsHeldInfo.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE Plate = '" + MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("Plate") + "' AND Status = 'H'");
								if (rsHeldInfo.EndOfFile() != true && rsHeldInfo.BeginningOfFile() != true)
								{
									answer = MessageBox.Show("There is a held registration in the system for plate " + MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("Plate") + ".  Would you like to complete the Held Registration?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
									if (answer == DialogResult.No)
									{
										// do nothing
									}
									else
									{
										OKFlag = true;
										cmbRegType1.Text = "Complete Held Registration";
										//App.DoEvents();
										txtPlateType[0].Focus();
										txtPlateType[0].Text = FCConvert.ToString(rsHeldInfo.Get_Fields_String("Plate"));
										return;
									}
								}
							}
						}
					}
				}
				frmDataInput.InstancePtr.Unload();
				frmDataInputLongTermTrailers.InstancePtr.Unload();
				if (MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.RegisteringFleet)
				{
					GoingToDataInput = true;
				}
				if ((MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.PlateType == 2) || CheckValidClassSubClass() == false)
				{
					MotorVehicle.Statics.Subclass = "!!";
					MotorVehicle.Statics.RememberedSubclass = "!!";
				}
				if (cmbRegType1.Text != "New To System / Update")
				{
					rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "SystemSettings");
					if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
					{
						if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
						{
							MessageBox.Show("You do not have a high enough Operator Level to use this option", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						if (MotorVehicle.Statics.OpID != "988")
						{
							MessageBox.Show("The operator ID " + MotorVehicle.Statics.OpID + " could not be found.", "Invalid Operator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
				}
				if (MotorVehicle.Statics.RegistrationType == "DUPREG" && cboClass.Text == "AP")
				{
					MessageBox.Show("You may not perform this type of registration for this vehicle class", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (MotorVehicle.Statics.RegistrationType == "LOST")
				{
					if (txtPlateType[0].Text == "NEW" || MotorVehicle.Statics.rsSecondPlate.Get_Fields_Boolean("ETO"))
					{
						MessageBox.Show("You must input a valid plate to proceed", "Invalid Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				if (MotorVehicle.Statics.RegistrationType == "RRR")
				{
					strPlate = txtPlateType[0].Text;
					strClass = cboClass.Text;
					strTestPlate = strPlate.Replace("-", "");
					if (strClass == "CO" && Conversion.Val(strTestPlate) >= 800000 && Conversion.Val(strTestPlate) <= 899999)
					{
						MessageBox.Show("The plate you are attempting to use should be a TT class plate.  Please process using New Plate with the existing plate number and the new class", "Invalid Plate Class", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (MotorVehicle.Statics.RegistrationType == "RRT")
				{
					strPlate = txtPlateType[0].Text;
					strClass = cboClass.Text;
					strTestPlate = strPlate.Replace("-", "");
					if (strClass == "CO" && Conversion.Val(strTestPlate) >= 800000 && Conversion.Val(strTestPlate) <= 899999)
					{
						MessageBox.Show("The plate you are attempting to use should be a TT class plate.  Please process using New Plate with the existing plate number and the new class", "Invalid Plate Class", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (MotorVehicle.Statics.RegistrationType == "CORR")
				{
					if (MotorVehicle.Statics.PlateType == 1)
					{
						strPlate = txtPlateType[0].Text;
						strClass = cboClass.Text;
						strTestPlate = strPlate.Replace("-", "");
						if (strClass == "CO" && Conversion.Val(strTestPlate) >= 800000 && Conversion.Val(strTestPlate) <= 899999)
						{
							MessageBox.Show("The plate you are attempting to use should be a TT class plate.  Please process using Plate Change with the existing plate number and the new class", "Invalid Plate Class", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					else
					{
						strPlate = txtPlateType[0].Text;
						strClass = cboClass.Text;
						MotorVehicle.Statics.CorrectingNewRegMotorCycle =
							MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("TransactionType") == "NRR";
						if (FCConvert.ToBoolean(MotorVehicle.Statics.rsSecondPlate.Get_Fields_Boolean("ETO")))
						{
							MessageBox.Show("You may not process a correction on an Excise Tax Only registration", "Invalid Plate Class", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						else if (strPlate == "NEW")
						{
							MessageBox.Show("You may not process a correction to assign a new plate that must be completed by BMV.", "Invalid Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
				}
				else
				{
					strPlate = txtPlateType[0].Text;
					strClass = cboClass.Text;
				}
				//FC:FINAL:DDU:#i1920 - fixed index of -1 to return empty string as in original
				string aux = "";
				if (cboClass.SelectedIndex != -1)
				{
					aux = cboClass.Items[cboClass.SelectedIndex].ToString();
				}
				// kk11202015 CHECK FOR A PLATE ENTERED AS OLD IN INVENTORY TO PREVENT DUPES - tromv-1114
				if (MotorVehicle.Statics.RegistrationType == "UPD" || ((MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "LOST") && MotorVehicle.Statics.PlateType == 2) || (MotorVehicle.Statics.RegistrationType == "RRR" && MotorVehicle.Statics.PlateType == 3) || ((MotorVehicle.Statics.RegistrationType == "RRT" || MotorVehicle.Statics.RegistrationType == "NRT") && MotorVehicle.Statics.PlateType == 4))
				{
					if (!ConfirmOldPlate(aux, fecherFoundation.Strings.Trim(txtPlateType[0].Text)))
					{
						return;
					}
				}
				MotorVehicle.Statics.PreviousETO = false;
				MotorVehicle.Statics.blnReserve = false;
				MotorVehicle.Statics.NewFleet = false;
				MotorVehicle.Statics.AddressChange = false;
				MotorVehicle.Statics.blnChangeToFleet = false;
				MotorVehicle.Statics.gblnDelayedReg = false;
				MotorVehicle.Statics.gblnCurrentConfig = false;
				MotorVehicle.Statics.gblnAskedCurrentConfig = false;
				MotorVehicle.Statics.lngFleetNumberBeingChangedTo = 0;
				if (txtPlateType[0].Visible == true)
				{
					bool blnLongTerm = false;
					if (MotorVehicle.Statics.Class == "TL" && MotorVehicle.Statics.Subclass == "L9")
					{
						blnLongTerm = true;
					}
					else
					{
						blnLongTerm = false;
					}
					if (MotorVehicle.GetInitialPlateFees2(aux, fecherFoundation.Strings.Trim(txtPlateType[0].Text), false, false, true, blnLongTerm) == 0)
					{
						strPlate = strPlate.Replace("-", "");
						strPlate = txtPlateType[0].Text + Strings.StrDup(8 - fecherFoundation.Strings.Trim(txtPlateType[0].Text).Length, " ") + txtPlateType[0].Text + Strings.StrDup(8 - fecherFoundation.Strings.Trim(txtPlateType[0].Text).Length, " ");
						MotorVehicle.GetPlateRange(strPlate);
						if (MotorVehicle.Statics.Class == "TL" && MotorVehicle.Statics.Subclass == "L9")
						{
							MotorVehicle.Statics.Plate1 = MotorVehicle.CreateFormattedInventory(MotorVehicle.Statics.Prefix1, MotorVehicle.Statics.Numeric1, MotorVehicle.Statics.Suffix1, "PXDLT");
						}
						else
						{
							MotorVehicle.Statics.Plate1 = MotorVehicle.CreateFormattedInventory(MotorVehicle.Statics.Prefix1, MotorVehicle.Statics.Numeric1, MotorVehicle.Statics.Suffix1, "PXD" + aux);
						}
					}
					else
					{
						// Changed to strplate because when doing a re reg same plate on a new so you had to type in the plate and it is a vanity plate it stayed as new
						MotorVehicle.Statics.Plate1 = strPlate;
						// rsPlateForReg.Fields("Plate")
					}
					if (cboClass.Text == "")
					{
						for (counter = 0; counter <= cboClass.Items.Count - 1; counter++)
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == cboClass.Items[counter].ToString())
							{
								MotorVehicle.Statics.Plate1Sub = counter;
								break;
							}
						}
					}
					else
					{
						MotorVehicle.Statics.Plate1Sub = cboClass.SelectedIndex;
					}
				}
				else if (cboLongTermRegPlateYear.Visible == true)
				{
					MotorVehicle.Statics.Plate1 = cboLongTermRegPlateYear.Text;
				}
				else
				{
					MotorVehicle.Statics.Plate1 = "";
				}
				if (MotorVehicle.Statics.RegistrationType == "HELD")
				{
					MotorVehicle.Statics.HeldReg = true;
				}
				else
				{
					MotorVehicle.Statics.HeldReg = false;
				}
				if (MotorVehicle.Statics.RegistrationType == "DUPREG")
				{
					if (MotorVehicle.Statics.NewToTheSystem)
					{
						MessageBox.Show("You cannot process a Duplicate Registration for a Plate that is not in the System", "New To System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						cboClass.Text = "";
						txtPlateType[0].Text = "";
						txtPlateType[0].Focus();
						return;
					}
				}
				else if (MotorVehicle.Statics.RegistrationType == "RRT" && MotorVehicle.Statics.PlateType == 2)
				{
					if (MotorVehicle.Statics.NewToTheSystem)
					{
						MessageBox.Show("You cannot process a Re Registration Transfer for a Plate that is not in the System", "New To System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1")) == "")
						{
							if (!(MotorVehicle.Statics.rsSecondPlate == null))
							{
								if (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("OwnerCode1")) == "")
								{
									// do nothing
								}
								else
								{
									MotorVehicle.Statics.NewToTheSystem = false;
								}
							}
							else
							{
								// do nothing
							}
							cboClass.Text = "";
							txtPlateType[0].Text = "";
							txtPlateType[0].Focus();
						}
						else
						{
							if (!(MotorVehicle.Statics.rsPlateForReg == null))
							{
								if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1")) == "")
								{
									// do nothing
								}
								else
								{
									MotorVehicle.Statics.NewToTheSystem = false;
								}
							}
							else
							{
								// do nothing
							}
							cboClass2.Text = "";
							txtPlateType[1].Text = "";
							txtPlateType[1].Focus();
						}
						return;
					}
				}
				// DJW@04292014 Added check to make sure old plate matches credit plate on NRT process
				if (MotorVehicle.Statics.RegistrationType == "NRT")
				{
					if ((fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "STAAB AGENCY" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION") && cmbAnnualReg.Text == "Long Term")
					{
						if (MotorVehicle.Statics.PlateType == 4)
						{
							if (Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate")), 2) != Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate")), 2))
							{
								MessageBox.Show("You may not process a transfer with a plate from a different year", "Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
					}
				}

				if (MotorVehicle.Statics.RegistrationType == "HELD")
				{
					MotorVehicle.Statics.HeldPlate = true;
				}
				else
				{
					MotorVehicle.Statics.HeldPlate = false;
				}
				// kk02152017 tromv-1224 Old reg must be expired for re-reg transfer. Have to check here because we don't have the right expiration date in CalculateDates
				if (MotorVehicle.Statics.RegistrationType == "RRT")
				{
					if (MotorVehicle.Statics.PlateType == 1 || MotorVehicle.Statics.PlateType == 3 || MotorVehicle.Statics.PlateType == 4)
					{
						xdate = FCConvert.ToDateTime(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate"));
					}
					else if (MotorVehicle.Statics.PlateType == 2)
					{
						xdate = FCConvert.ToDateTime(MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate"));
					}
					LowDate = fecherFoundation.DateAndTime.DateAdd("m", -1, xdate);
					// kk04142017 tromv-1224  Allow RRT if reg expires in the current month or earlier
					if (LowDate.ToOADate() >= DateTime.Today.ToOADate())
					{
						MessageBox.Show("This registration cannot be processed because it is not expired", "Cannot Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						frmWait.InstancePtr.Unload();
						return;
					}
				}
				// kk03302017 tromv-1236  Don't transfer credits from a pending reg
				if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
				{
					if (MotorVehicle.Statics.PlateType == 1)
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Status")) == "P" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate")) == "")
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate")) == "NEW" || MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("ETO"))
							{
								MessageBox.Show("You must change the status of this vehicle from pending before you may continue with this process", "Unable to Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
								MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
								MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
								MotorVehicle.Statics.SecondCorrectID = 0;
								MotorVehicle.Statics.SecondCorrectPlate = "";
								MotorVehicle.Statics.SecondCorrectClass = "";
								OKFlag = true;
								MotorVehicle.Statics.InfoForReReg = false;
								MotorVehicle.Statics.InfoForCorrect = false;
								MotorVehicle.Statics.InfoForDuplicate = false;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
								cmdProcess_Click();
								return;
							}
						}
					}
					else if (MotorVehicle.Statics.RegistrationType == "NRT" || (MotorVehicle.Statics.RegistrationType == "RRT" && MotorVehicle.Statics.PlateType == 2))
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Status")) == "P" || FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate")) == "")
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate")) == "NEW" || MotorVehicle.Statics.rsSecondPlate.Get_Fields_Boolean("ETO"))
							{
								MessageBox.Show("You must change the status of this vehicle from pending before you may continue with this process", "Unable to Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[1].Text;
								MotorVehicle.Statics.FirstCorrectClass = cboClass2.Text;
								MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID"));
								MotorVehicle.Statics.SecondCorrectID = 0;
								MotorVehicle.Statics.SecondCorrectPlate = "";
								MotorVehicle.Statics.SecondCorrectClass = "";
								OKFlag = true;
								MotorVehicle.Statics.InfoForReReg = false;
								MotorVehicle.Statics.InfoForCorrect = false;
								MotorVehicle.Statics.InfoForDuplicate = false;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate"));
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class"));
								MotorVehicle.Statics.Class1 = cboClass2.Text;
								cmdProcess_Click();
								return;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("Status")) == "P" || FCConvert.ToString(MotorVehicle.Statics.rsThirdPlate.Get_Fields_DateTime("ExpireDate")) == "")
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("Plate")) == "NEW" || MotorVehicle.Statics.rsThirdPlate.Get_Fields_Boolean("ETO"))
							{
								MessageBox.Show("You must change the status of this vehicle from pending before you may continue with this process", "Unable to Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[2].Text;
								// FirstCorrectClass = cboClass3.Text
								MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsThirdPlate.Get_Fields_Int32("ID"));
								MotorVehicle.Statics.SecondCorrectID = 0;
								MotorVehicle.Statics.SecondCorrectPlate = "";
								MotorVehicle.Statics.SecondCorrectClass = "";
								OKFlag = true;
								MotorVehicle.Statics.InfoForReReg = false;
								MotorVehicle.Statics.InfoForCorrect = false;
								MotorVehicle.Statics.InfoForDuplicate = false;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("Plate"));
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("Class"));
								cmdProcess_Click();
								return;
							}
						}
					}
				}
				if (MotorVehicle.Statics.RegistrationType == "RRT" && MotorVehicle.Statics.PlateType == 1)
				{
					if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode2")) == "N")
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("OwnerCode2")) != "N")
						{
							MessageBox.Show("You may need to fill out a name deletion or addition form for this registration because there are a different number of owners on the two vehicles", "Name Addition or Deletion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					else
					{
						if (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("OwnerCode2")) == "N")
						{
							MessageBox.Show("You may need to fill out a name deletion or addition form for this registration because there are a different number of owners on the two vehicles", "Name Addition or Deletion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
					if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Address") != MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Address"))
					{
						MotorVehicle.Statics.AddressChange = true;
					}
					else
					{
						MotorVehicle.Statics.AddressChange = false;
					}
				}
				if (txtPlateType[1].Visible == true)
				{
					if (MotorVehicle.Statics.rsSecondPlate.IsntAnything())
					{
						txtPlateType[1].Focus();
						return;
					}
					// Plate2 = txtPlateType(1).Text
					MotorVehicle.Statics.Plate2 = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate"));
					if (cboClass2.Text == "")
					{
						for (counter = 0; counter <= cboClass2.Items.Count - 1; counter++)
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) == cboClass2.Items[counter].ToString())
							{
								MotorVehicle.Statics.Plate2Sub = counter;
								break;
							}
						}
					}
					else
					{
						MotorVehicle.Statics.Plate2Sub = cboClass2.SelectedIndex;
					}
				}
				else
				{
					MotorVehicle.Statics.Plate2 = "";
				}
				if (txtPlateType[2].Visible == true)
				{
					// Plate3 = txtPlateType(2).Text
					MotorVehicle.Statics.Plate3 = FCConvert.ToString(MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("Plate"));
				}
				else
				{
					MotorVehicle.Statics.Plate3 = "";
				}
				if (cmbRegType1.Text == "Re-Registration Regular")
				{


					if (MotorVehicle.Statics.NewToTheSystem)
					{
						if (MotorVehicle.IsMotorcycle(cboClass.Text))
						{
							MessageBox.Show("This registration appears to have information missing that is required to finish processing.  Failure to fill in all the required information could cause inaccurate reports.  Please verify the required information listed in red", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
							MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
							if (txtPlateType[1].Visible == true)
							{
								MotorVehicle.Statics.SecondCorrectPlate = txtPlateType[1].Text;
								MotorVehicle.Statics.SecondCorrectClass = cboClass2.Text;
							}
							else
							{
								MotorVehicle.Statics.SecondCorrectPlate = "";
								MotorVehicle.Statics.SecondCorrectClass = "";
							}
							MotorVehicle.Statics.CorrectPlateType = MotorVehicle.Statics.PlateType - 1;
							OKFlag = true;
							cmbRegType1.Text = "New To System / Update";
							if (MotorVehicle.Statics.CorrectPlateType == 0)
							{
								txtPlateType[0].Text = MotorVehicle.Statics.FirstCorrectPlate;
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.FirstCorrectClass);
							}
							else
							{
								txtPlateType[0].Text = MotorVehicle.Statics.SecondCorrectPlate;
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.SecondCorrectClass);
							}
							MotorVehicle.Statics.InfoForReReg = true;
							cmdProcess_Click();
							return;
						}
					}

					if (!MotorVehicle.Statics.NewToTheSystem)
					{
						if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("ETO")))
						{
							if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("ETO") && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "TX" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "AP" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "ST" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "CS" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate")) != "NEW")
							{
								answer = MessageBox.Show("Is this an Excise Already Paid transaction?", "Excise Already Paid?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (answer == DialogResult.Yes)
								{
									if (MotorVehicle.Statics.Class == "AP" || (MotorVehicle.Statics.Class == "AM" && MotorVehicle.Statics.Subclass == "A3") || (MotorVehicle.Statics.Class == "PC" && MotorVehicle.Statics.Subclass == "P6") || (MotorVehicle.Statics.Class == "BU" && MotorVehicle.Statics.Subclass == "B2"))
									{
										MessageBox.Show("You must change the status of this vehicle from pending before you may continue with this process", "Unable to Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
										MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
										MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
										MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
										MotorVehicle.Statics.SecondCorrectID = 0;
										MotorVehicle.Statics.SecondCorrectPlate = "";
										MotorVehicle.Statics.SecondCorrectClass = "";
										OKFlag = true;
										MotorVehicle.Statics.InfoForReReg = true;
										cmbRegType1.Text = "New To System / Update";
										txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
										txtPlateType_Validate(0, false);
										cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
										cmdProcess_Click();
										return;
									}
									// End If
								}
							}
						}
					}
				}
				if (MotorVehicle.Statics.Class != "TL" || MotorVehicle.Statics.Subclass != "L9")
				{
					if (cmbRegType1.Text == "Correction" && !blnCameFromVehicleUpdate)
					{
						if (MotorVehicle.Statics.PlateType == 1)
						{
							// kk11192015 Changed P5 to P6
							if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("RegRateCharge")) == 0 && MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("ETO") != true && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "AP" && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "AM" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "A3") && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "PC" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "P6" || MotorVehicle.Statics.AllowRentals) && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "CI" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "CS" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "ST" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "DV" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "MM" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "PO" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "XV" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "VX")
							{
								MessageBox.Show("This registration appears to have information missing that is required to finish processing.  Failure to fill in all the required information could cause inaccurate reports.  Please verify the required information listed in red", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								if (MotorVehicle.Statics.CorrectLongTermReg && MotorVehicle.Statics.PlateType != 1)
								{
									MotorVehicle.Statics.FirstCorrectPlate = cboLongTermRegPlateYear.Text;
								}
								else
								{
									MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
									MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
									MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
								}

								if (txtPlateType_1.Visible)
								{
									MotorVehicle.Statics.SecondCorrectPlate = txtPlateType[1].Text;
									MotorVehicle.Statics.SecondCorrectID = MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID");
									MotorVehicle.Statics.SecondCorrectClass = cboClass2.Text;
								}
								else
								{
									MotorVehicle.Statics.SecondCorrectPlate = "";
									MotorVehicle.Statics.SecondCorrectID = 0;
									MotorVehicle.Statics.SecondCorrectClass = "";
								}

								OKFlag = true;
								MotorVehicle.Statics.InfoForCorrect = true;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
								cmdProcess_Click();
								return;
							}
							else if ((MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseTaxCharged") > 0 && MotorVehicle.Statics.rsPlateForReg.Get_Fields("LocalPaid") == 0) || ((MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("ExciseTaxCharged") == 0 && !FCConvert.ToBoolean(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("ExciseExempt"))) && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "SE" && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "CL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "C2") && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "CL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "C4") && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "CL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "C6") && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "DV" || (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "D1" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "D2")) && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "TL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "L2") && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "TL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "L4") && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "TL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "L7") && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "TL" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "L9") && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "CI" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "CS" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "ST" && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "XV" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "X2") && (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) != "VX" || (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "V1" && FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass")) != "V2"))))
							{
								// tromv-1148 2.21.18 kjr added excise exempt check
								MessageBox.Show("This registration appears to have information missing that is required to finish processing.  Failure to fill in all the required information could cause inaccurate reports.  Please verify the required information listed in red", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								if (MotorVehicle.Statics.CorrectLongTermReg && MotorVehicle.Statics.PlateType != 1)
								{
									MotorVehicle.Statics.FirstCorrectPlate = cboLongTermRegPlateYear.Text;
								}
								else
								{
									MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
									MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
									MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
								}

								if (txtPlateType[1].Visible)
								{
									MotorVehicle.Statics.SecondCorrectID = MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID");
									MotorVehicle.Statics.SecondCorrectPlate = txtPlateType[1].Text;
									MotorVehicle.Statics.SecondCorrectClass = cboClass2.Text;
								}
								else
								{
									MotorVehicle.Statics.SecondCorrectID = 0;
									MotorVehicle.Statics.SecondCorrectPlate = "";
									MotorVehicle.Statics.SecondCorrectClass = "";
								}

								OKFlag = true;
								MotorVehicle.Statics.InfoForCorrect = true;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
								cmdProcess_Click();
								return;
							}
							else if (FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields("StatePaid")) == 0 && MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("RegRateFull") > MotorVehicle.Statics.rsPlateForReg.Get_Fields_Decimal("TransferCreditFull"))
							{
								MessageBox.Show("This registration appears to have information missing that is required to finish processing.  Failure to fill in all the required information could cause inaccurate reports.  Please verify the required information listed in red", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								if (MotorVehicle.Statics.CorrectLongTermReg && MotorVehicle.Statics.PlateType != 1)
								{
									MotorVehicle.Statics.FirstCorrectPlate = cboLongTermRegPlateYear.Text;
								}
								else
								{
									MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
									MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
									MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
								}

								if (txtPlateType[1].Visible)
								{
									MotorVehicle.Statics.SecondCorrectID = MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID");
									MotorVehicle.Statics.SecondCorrectPlate = txtPlateType[1].Text;
									MotorVehicle.Statics.SecondCorrectClass = cboClass2.Text;
								}
								else
								{
									MotorVehicle.Statics.SecondCorrectID = 0;
									MotorVehicle.Statics.SecondCorrectPlate = "";
									MotorVehicle.Statics.SecondCorrectClass = "";
								}
								OKFlag = true;
								MotorVehicle.Statics.InfoForCorrect = true;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
								cmdProcess_Click();
								return;
							}
							else if (!Information.IsDate(MotorVehicle.Statics.rsPlateForReg.Get_Fields("EffectiveDate")))
							{
								MessageBox.Show("This registration appears to have information missing that is required to finish processing.  Failure to fill in all the required information could cause inaccurate reports.  Please verify the required information listed in red", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								if (MotorVehicle.Statics.CorrectLongTermReg && MotorVehicle.Statics.PlateType != 1)
								{
									MotorVehicle.Statics.FirstCorrectPlate = cboLongTermRegPlateYear.Text;
								}
								else
								{
									MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
									MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
									MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
								}

								if (txtPlateType[1].Visible)
								{
									MotorVehicle.Statics.SecondCorrectID = MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID");
									MotorVehicle.Statics.SecondCorrectPlate = txtPlateType[1].Text;
									MotorVehicle.Statics.SecondCorrectClass = cboClass2.Text;
								}
								else
								{
									MotorVehicle.Statics.SecondCorrectID = 0;
									MotorVehicle.Statics.SecondCorrectPlate = "";
									MotorVehicle.Statics.SecondCorrectClass = "";
								}
								OKFlag = true;
								MotorVehicle.Statics.InfoForCorrect = true;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
								cmdProcess_Click();
								return;
							}
						}
						else
						{
							// kk11192015 Changed P5 to P6
							if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_Decimal("RegRateCharge") == 0 && MotorVehicle.Statics.rsSecondPlate.Get_Fields_Boolean("ETO") != true && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "AP" && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "AM" || FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "A3") && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "PC" || FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "P6") && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "CI" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "CS" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "ST" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "DV" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "MM" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "PO" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "XV" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "VX" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "PH")
							{
								MessageBox.Show("This registration appears to have information missing that is required to finish processing.  Failure to fill in all the required information could cause inaccurate reports.  Please verify the required information listed in red", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								if (MotorVehicle.Statics.CorrectLongTermReg)
								{
									MotorVehicle.Statics.FirstCorrectPlate = cboLongTermRegPlateYear.Text;
								}
								else
								{
									MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
									MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
									MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
								}
								if (txtPlateType[1].Visible == true)
								{
									MotorVehicle.Statics.SecondCorrectPlate = txtPlateType[1].Text;
									MotorVehicle.Statics.SecondCorrectClass = cboClass2.Text;
									MotorVehicle.Statics.SecondCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID"));
								}
								else
								{
									MotorVehicle.Statics.SecondCorrectPlate = "";
									MotorVehicle.Statics.SecondCorrectClass = "";
									MotorVehicle.Statics.SecondCorrectID = 0;
								}
								OKFlag = true;
								MotorVehicle.Statics.InfoForCorrect = true;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("plate"));
								PlateFromSearch = false;
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class"));
								cmdProcess_Click();
								return;
							}
							else if ((MotorVehicle.Statics.rsSecondPlate.Get_Fields_Decimal("ExciseTaxCharged") > 0 && FCConvert.ToInt32(MotorVehicle.Statics.rsSecondPlate.Get_Fields("LocalPaid")) == 0) || ((MotorVehicle.Statics.rsSecondPlate.Get_Fields_Decimal("ExciseTaxCharged") == 0 && !FCConvert.ToBoolean(MotorVehicle.Statics.rsSecondPlate.Get_Fields_Boolean("ExciseExempt"))) && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "SE" && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "CL" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "C2") && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "CL" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "C4") && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "CL" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "C6") && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "DV" || (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "D1" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "D2")) && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "TL" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "L2") && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "TL" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "L4") && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "TL" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "L7") && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "TL" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "L9") && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "CI" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "CS" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "ST" && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "XV" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "X2") && (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) != "VX" || (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "V1" && FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass")) != "V2")))))
							{
								// tromv-1148 2.21.18 kjr added excise exempt check
								MessageBox.Show("This registration appears to have information missing that is required to finish processing.  Failure to fill in all the required information could cause inaccurate reports.  Please verify the required information listed in red", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								if (MotorVehicle.Statics.CorrectLongTermReg)
								{
									MotorVehicle.Statics.FirstCorrectPlate = cboLongTermRegPlateYear.Text;
								}
								else
								{
									MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
									MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
									MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
								}
								if (txtPlateType[1].Visible == true)
								{
									MotorVehicle.Statics.SecondCorrectPlate = txtPlateType[1].Text;
									MotorVehicle.Statics.SecondCorrectClass = cboClass2.Text;
									MotorVehicle.Statics.SecondCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID"));
								}
								else
								{
									MotorVehicle.Statics.SecondCorrectPlate = "";
									MotorVehicle.Statics.SecondCorrectClass = "";
									MotorVehicle.Statics.SecondCorrectID = 0;
								}
								OKFlag = true;
								MotorVehicle.Statics.InfoForCorrect = true;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("plate"));
								PlateFromSearch = false;
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class"));
								cmdProcess_Click();
								return;
							}
							else if (FCConvert.ToInt32(MotorVehicle.Statics.rsSecondPlate.Get_Fields("StatePaid")) == 0 && MotorVehicle.Statics.rsSecondPlate.Get_Fields_Decimal("RegRateFull") > MotorVehicle.Statics.rsSecondPlate.Get_Fields_Decimal("TransferCreditFull"))
							{
								MessageBox.Show("This registration appears to have information missing that is required to finish processing.  Failure to fill in all the required information could cause inaccurate reports.  Please verify the required information listed in red", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								if (MotorVehicle.Statics.CorrectLongTermReg)
								{
									MotorVehicle.Statics.FirstCorrectPlate = cboLongTermRegPlateYear.Text;
								}
								else
								{
									MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
									MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
									MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
								}
								if (txtPlateType[1].Visible == true)
								{
									MotorVehicle.Statics.SecondCorrectPlate = txtPlateType[1].Text;
									MotorVehicle.Statics.SecondCorrectClass = cboClass2.Text;
									MotorVehicle.Statics.SecondCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID"));
								}
								else
								{
									MotorVehicle.Statics.SecondCorrectPlate = "";
									MotorVehicle.Statics.SecondCorrectClass = "";
									MotorVehicle.Statics.SecondCorrectID = 0;
								}
								OKFlag = true;
								MotorVehicle.Statics.InfoForCorrect = true;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("plate"));
								PlateFromSearch = false;
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class"));
								cmdProcess_Click();
								return;
							}
							else if (!Information.IsDate(MotorVehicle.Statics.rsSecondPlate.Get_Fields("EffectiveDate")))
							{
								MessageBox.Show("This registration appears to have information missing that is required to finish processing.  Failure to fill in all the required information could cause inaccurate reports.  Please verify the required information listed in red", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
								if (MotorVehicle.Statics.CorrectLongTermReg)
								{
									MotorVehicle.Statics.FirstCorrectPlate = cboLongTermRegPlateYear.Text;
								}
								else
								{
									MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
									MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
									MotorVehicle.Statics.FirstCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID"));
								}
								if (txtPlateType[1].Visible == true)
								{
									MotorVehicle.Statics.SecondCorrectPlate = txtPlateType[1].Text;
									MotorVehicle.Statics.SecondCorrectClass = cboClass2.Text;
									MotorVehicle.Statics.SecondCorrectID = FCConvert.ToInt32(MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID"));
								}
								else
								{
									MotorVehicle.Statics.SecondCorrectPlate = "";
									MotorVehicle.Statics.SecondCorrectClass = "";
									MotorVehicle.Statics.SecondCorrectID = 0;
								}
								OKFlag = true;
								MotorVehicle.Statics.InfoForCorrect = true;
								cmbRegType1.Text = "New To System / Update";
								txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("plate"));
								PlateFromSearch = false;
								txtPlateType_Validate(0, false);
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class"));
								cmdProcess_Click();
								return;
							}
						}
					}
				else if (MotorVehicle.Statics.RegistrationType == "DUPREG" && !Information.IsDate(MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExciseTaxDate")))
				{
					MessageBox.Show("This registration appears to have information missing that is required to finish processing.  Failure to fill in all the required information could cause inaccurate reports.  Please verify the required information listed in red", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					MotorVehicle.Statics.CorrectLongTermReg = cmbAnnualReg.Text == "Long Term";
					if (MotorVehicle.Statics.CorrectLongTermReg && MotorVehicle.Statics.PlateType != 1)
					{
						MotorVehicle.Statics.FirstCorrectPlate = cboLongTermRegPlateYear.Text;
					}
					else
					{
						MotorVehicle.Statics.FirstCorrectPlate = txtPlateType[0].Text;
						MotorVehicle.Statics.FirstCorrectClass = cboClass.Text;
						MotorVehicle.Statics.FirstCorrectID = MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID");
					}

					if (txtPlateType[1].Visible)
					{
						MotorVehicle.Statics.SecondCorrectID = MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID");
						MotorVehicle.Statics.SecondCorrectPlate = txtPlateType[1].Text;
						MotorVehicle.Statics.SecondCorrectClass = cboClass2.Text;
					}
					else
					{
						MotorVehicle.Statics.SecondCorrectID = 0;
						MotorVehicle.Statics.SecondCorrectPlate = "";
						MotorVehicle.Statics.SecondCorrectClass = "";
					}

					OKFlag = true;
					MotorVehicle.Statics.InfoForDuplicate = true;
					cmbRegType1.Text = "New To System / Update";
					txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
					txtPlateType_Validate(0, false);
					cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
					cmdProcess_Click();
					return;
				}
					else
					{
						blnCameFromVehicleUpdate = false;
						// kk01062016 tromv-1070  Need to reset this flag so the warning is displayed later on
					}
				}
				if (MotorVehicle.Statics.RegistrationType == "NRT" && MotorVehicle.Statics.PlateType == 1)
				{
					if (!MotorVehicle.Statics.NewToTheSystem)
					{
						MotorVehicle.Statics.TradedMake = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("make"));
						MotorVehicle.Statics.TradedModel = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("model"));
						MotorVehicle.Statics.TradedYear = MotorVehicle.Statics.rsPlateForReg.Get_Fields("Year") + "";
						MotorVehicle.Statics.TradedVIN = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Vin"));
					}
				}
				if (cmbRegType1.Text == "Special Reg Permit")
				{
					if (MotorVehicle.Statics.TownLevel == 2 || MotorVehicle.Statics.TownLevel == 3 || MotorVehicle.Statics.TownLevel == 4 || MotorVehicle.Statics.TownLevel == 5)
					{
						Close();
						frmSpecialPermit.InstancePtr.Show(App.MainForm);
					}
					else
					{
						MessageBox.Show("You do not have a high enough authorization level from the BMV to do this type of process", "Invalid Authorization Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					return;
				}
				else if (cmbRegType1.Text == "Booster Permit")
				{
					if (MotorVehicle.Statics.TownLevel == 3 || MotorVehicle.Statics.TownLevel == 4)
					{
						if (MotorVehicle.Statics.PlateType == 2)
						{
							bool executeCheckOwners = false;
							if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("registeredWeightNew") == MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("registeredWeightNew"))
							{
								rsBoosters.OpenRecordset("SELECT * FROM Booster WHERE VehicleID = " + MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID") + " And Inactive = 0 and isnull(Voided, 0) = 0 and ExpireDate >= '" + DateTime.Today.ToShortDateString() + "' and ExpireDate <= '" + MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExpireDate") + "'", "TWMV0000.vb1");
								if (rsBoosters.EndOfFile() != true && rsBoosters.BeginningOfFile() != true)
								{
									// do nothing
								}
								else
								{
									MessageBox.Show("No boosters found that are eligible be transfered", "No Boosters", MessageBoxButtons.OK, MessageBoxIcon.Information);
									goto ExitSub;
								}
								executeCheckOwners = true;
								goto CheckOwners;
							}
							else
							{
								rsBoosters.OpenRecordset("SELECT * FROM Booster WHERE VehicleID = " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID") + " And Inactive = 0 and isnull(Voided, 0) = 0 and ExpireDate >= '" + DateTime.Today.ToShortDateString() + "' and ExpireDate <= '" + MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExpireDate") + "'", "TWMV0000.vb1");
								if (rsBoosters.EndOfFile() != true && rsBoosters.BeginningOfFile() != true)
								{
									do
									{
										if (rsBoosters.Get_Fields_Int32("BoostedFrom") == MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("registeredWeightNew"))
										{
											executeCheckOwners = true;
											goto CheckOwners;
										}
										rsBoosters.MoveNext();
									}
									while (rsBoosters.EndOfFile() != true);
								}
								MessageBox.Show("The Registered Vehicles Weight must be the same on both vehicles to do a booster transfer", "Invalid Plates", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						CheckOwners:
							;
							if (executeCheckOwners)
							{
								if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("OwnerCode1")) == "I")
								{
									if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("PartyID1"))) == fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("PartyID1"))))
									{
										Close();
										frmBoosterTransfer.InstancePtr.Show(App.MainForm);
									}
									else
									{
										MessageBox.Show("The Owner must be the same on both vehicles to do a booster transfer", "Invalid Plates", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									}
								}
								else
								{
									if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("Owner1"))) == fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields("Owner1"))))
									{
										Close();
										frmBoosterTransfer.InstancePtr.Show(App.MainForm);
									}
									else
									{
										MessageBox.Show("The Owner must be the same on both vehicles to do a booster transfer", "Invalid Plates", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									}
								}
								executeCheckOwners = false;
							}
						}
						else if (MotorVehicle.Statics.PlateType == 3)
						{
							rsBoosters.OpenRecordset("SELECT * FROM Booster WHERE VehicleID = " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("ID") + " And Inactive = 0 and isnull(Voided, 0) = 0 and ExpireDate >= '" + DateTime.Today.ToShortDateString() + "'", "TWMV0000.vb1");
							if (rsBoosters.EndOfFile() != true && rsBoosters.BeginningOfFile() != true)
							{
								Close();
								frmBoosterDuplicate.InstancePtr.Show(App.MainForm);
							}
							else
							{
								MessageBox.Show("The vehicle you entered does not have any active boosters associated with it", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						else
						{
							frmBoosterPlate.InstancePtr.Show(App.MainForm);
							Close();
						}
					}
					else
					{
						MessageBox.Show("You do not have a high enough authorization level from the BMV to do this type of process", "Invalid Authorization Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				ExitSub:
					;
					return;
				}
				else if (cmbRegType1.Text == "Transit Plate")
				{
					if (MotorVehicle.Statics.TownLevel == 3 || MotorVehicle.Statics.TownLevel == 4 || MotorVehicle.Statics.TownLevel == 2 || MotorVehicle.Statics.TownLevel == 5)
					{
						frmTransitPlate.InstancePtr.Show(App.MainForm);
						Close();
					}
					else
					{
						MessageBox.Show("You do not have a high enough authorization level from the BMV to do this type of process", "Invalid Authorization Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					return;
				}
				else if (cmbRegType1.Text == "Process CTA")
				{
					clsDRWrapper temp3 = MotorVehicle.Statics.rsFinal;
					temp = MotorVehicle.Statics.rsPlateForReg;
					MotorVehicle.TransferRecordSetsA(ref temp3, ref temp);
					MotorVehicle.Statics.rsPlateForReg = temp;
					MotorVehicle.Statics.rsFinal = temp3;
					frmSingleLongTermCTA.InstancePtr.Show(App.MainForm);
					Close();
					return;
				}
				else if (cmbRegType1.Text == "Correction" && MotorVehicle.Statics.PlateType == 2)
				{
					if (MotorVehicle.Statics.TownLevel == 6)
					{
						MessageBox.Show("You do not have a high enough authorization level from the BMV to do this type of process", "Invalid Authorization Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbRegType1.Text == "New Registration Transfer")
				{
					if (MotorVehicle.Statics.TownLevel == 1)
					{
						MessageBox.Show("You do not have a high enough authorization level from the BMV to do this type of process", "Invalid Authorization Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}

				if (txtPlateType[0].Visible == true && fecherFoundation.Strings.Trim(txtPlateType[0].Text) == "")
				{
					txtPlateType[0].Focus();
					return;
				}
				if (txtPlateType[1].Visible == true && fecherFoundation.Strings.Trim(txtPlateType[1].Text) == "")
				{
					txtPlateType[1].Focus();
					return;
				}
				if (txtPlateType[2].Visible == true && fecherFoundation.Strings.Trim(txtPlateType[2].Text) == "")
				{
					txtPlateType[2].Focus();
					return;
				}
				if (cboLongTermRegPlateYear.Visible == true && cboLongTermRegPlateYear.SelectedIndex < 0)
				{
					cboLongTermRegPlateYear.Focus();
					return;
				}
				// the following indicates a change in the class dropdown after one was selected from the from the master file.
				// the plate found in the master file is not the correct one
				if (MotorVehicle.Statics.rsPlateForReg.IsntAnything())
				{
					GetExistingPlate(0);
					if (!MotorVehicle.Statics.rsPlateForReg.IsntAnything())
						txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
				}
				if (MotorVehicle.Statics.Class1 != "" && MotorVehicle.Statics.Subclass != "")
				{
					if (MotorVehicle.Statics.Class1 != cboClass.Text && cboClass.Visible && (MotorVehicle.Statics.Class1 != "MC" && cboClass.Text != "MP"))
					{
						answer = MessageBox.Show("The class in the drop-down box has been changed. Does this indicate that the plate found in the Master file is not the correct one?", "Plate not in Master file?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (answer == DialogResult.Yes)
						{
							MotorVehicle.Statics.NewToTheSystem = true;
							MotorVehicle.Statics.Class1 = fecherFoundation.Strings.Trim(cboClass.Text);
							MotorVehicle.Statics.Subclass = MotorVehicle.Statics.Class1;
							MotorVehicle.Statics.Class = MotorVehicle.Statics.Class1;
							// kk04042017 tromv-1063  Clear out anything in rsPlateForReg and start a new record
							MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM Master WHERE ID = -1");
							MotorVehicle.Statics.rsPlateForReg.AddNew();
							MotorVehicle.Statics.rsPlateForReg.Set_Fields("Plate", txtPlateType[0].Text);
							MotorVehicle.Statics.rsPlateForReg.Set_Fields("Class", MotorVehicle.Statics.Class);
							rsSubClass.OpenRecordset("SELECT COUNT(BMVCode) as TotalCount FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class1 + "'", "TWMV0000.vb1");
							if (rsSubClass.EndOfFile() != true && rsSubClass.BeginningOfFile() != true)
							{
								if (rsSubClass.Get_Fields("TotalCount") > 1)
								{
									MotorVehicle.Statics.strSql =
										"SELECT SystemCode, Description from CLASS WHERE BmvCode =  '" + MotorVehicle.Statics.Class1 +
										"' ORDER BY SystemCode";

									rsSubClass.OpenRecordset(MotorVehicle.Statics.strSql);
									rsSubClass.MoveLast();
									rsSubClass.MoveFirst();
									frmListBox.InstancePtr.lstSelectPlate.Clear();
									frmListBox.InstancePtr.lstSelectPlate.Columns.Add("");
									frmListBox.InstancePtr.lstSelectPlate.Columns[1].Resizable = false;
									int listWidth = frmListBox.InstancePtr.lstSelectPlate.Width;
									frmListBox.InstancePtr.lstSelectPlate.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.25);
									frmListBox.InstancePtr.lstSelectPlate.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.7);
									frmListBox.InstancePtr.lstSelectPlate.GridLineStyle = GridLineStyle.None;
									frmListBox.InstancePtr.lblHeader.Text = "Select the Subclass for " + MotorVehicle.Statics.Plate1;
									while (!rsSubClass.EndOfFile())
									{
										frmListBox.InstancePtr.lstSelectPlate.AddItem(rsSubClass.Get_Fields_String("SystemCode") + "\t" + rsSubClass.Get_Fields_String("Description"));
										rsSubClass.MoveNext();
									}
									frmListBox.InstancePtr.Show(FCForm.FormShowEnum.Modal);
									if (MotorVehicle.Statics.ReturnFromListBox != "")
									{
										MotorVehicle.Statics.Subclass = Strings.Mid(MotorVehicle.Statics.ReturnFromListBox, 1, 2);
									}
									else
									{
										cboClass.Text = "";
										MotorVehicle.Statics.Class1 = "";
										MotorVehicle.Statics.Subclass = "";
										temp.Reset();
										rsSubClass2.Reset();
										rsTPlate.Reset();
										return;
									}
								}
							}
							if ((MotorVehicle.Statics.RegistrationType == "UPD") ||
								(MotorVehicle.Statics.RegistrationType == "RRR" && (MotorVehicle.Statics.PlateType == 1 || MotorVehicle.Statics.PlateType == 3)) ||
								(MotorVehicle.Statics.RegistrationType == "RRT" && (MotorVehicle.Statics.PlateType == 1 || MotorVehicle.Statics.PlateType == 2 || MotorVehicle.Statics.PlateType == 4)) ||
								(MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.PlateType == 2) ||
								(MotorVehicle.Statics.RegistrationType == "NRT" && (MotorVehicle.Statics.PlateType == 1 || MotorVehicle.Statics.PlateType == 4)))
							{
								// do nothing
							}
							else
							{
								MotorVehicle.Statics.ForcedPlate = "N";
								//FC:FINAL:MSH - i.issue #1766: initialize listview columns for displaying data as in original app (WiseJ doesn't display spaces and tabs)
								frmListBox.InstancePtr.lstSelectPlate.Clear();
								frmListBox.InstancePtr.lstSelectPlate.Columns.Add("");
								frmListBox.InstancePtr.lstSelectPlate.Columns[1].Resizable = false;
								int listWidth = frmListBox.InstancePtr.lstSelectPlate.Width;
								frmListBox.InstancePtr.lstSelectPlate.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.2);
								frmListBox.InstancePtr.lstSelectPlate.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.75);
								frmListBox.InstancePtr.lstSelectPlate.GridLineStyle = GridLineStyle.None;
								frmListBox.InstancePtr.lstSelectPlate.AddItem("I" + "\t" + "Issue Plate as Coded (Agent Code Required)");
								frmListBox.InstancePtr.lstSelectPlate.AddItem("S" + "\t" + "Indicate this is a Special Request Plate");
								frmListBox.InstancePtr.lstSelectPlate.AddItem("P" + "\t" + "Indicate Prepaid Fees");
								// (Non-Chickadee)"
								// frmListBox.lstSelectPlate.AddItem "R" & vbTab & "Previously Reserved Chickadee Plate (Prepaid)"
								frmListBox.InstancePtr.lstSelectPlate.AddItem("T" + "\t" + "Temporary Plate Being Issued, Reserve Now");
								frmListBox.InstancePtr.lblHeader.Text = "The Plate specified was not found in inventory. How would you like this Plate coded?";
								//frmListBox.InstancePtr.lstSelectPlate.WidthOriginal = 5500;
								//frmListBox.InstancePtr.WidthOriginal = 6000;
								frmListBox.InstancePtr.Show(FCForm.FormShowEnum.Modal);
								MotorVehicle.Statics.ForcedPlate = Strings.Mid(MotorVehicle.Statics.ReturnFromListBox, 1, 1);
							}
						}
						else
						{
							MotorVehicle.Statics.Class1 = "";
							MotorVehicle.Statics.Subclass = "";
							cboClass.Text = "";
							cmdProcess.Enabled = false;
							temp.Reset();
							rsSubClass2.Reset();
							rsTPlate.Reset();
							return;
						}
					}
					else if (MotorVehicle.Statics.Class1 == "MC" && cboClass.Text == "MP")
					{
						MotorVehicle.Statics.Class1 = "MP";
						MotorVehicle.Statics.Subclass = "MP";
					}
				}
				// 
				if (MotorVehicle.Statics.Class1 == "")
				{
					if (cboClass.Text != "")
					{
						MotorVehicle.Statics.Class1 = fecherFoundation.Strings.Trim(cboClass.Text);
						if (fecherFoundation.Strings.Trim(MotorVehicle.Statics.Subclass) == "")
							MotorVehicle.Statics.Subclass = "!!";
					}
					else
					{
						MessageBox.Show(" Please select a Class from the Drop-Down box", "Class Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						cboClass.Focus();
						Support.SendKeys("{F4}", false);
						FCGlobal.Screen.MousePointer = 0;
						temp.Reset();
						rsSubClass2.Reset();
						rsTPlate.Reset();
						return;
					}
				}
				// 
				if (MotorVehicle.Statics.RegistrationType != "HELD")
				{
					if (MotorVehicle.Statics.Subclass != MotorVehicle.Statics.RememberedSubclass)
					{
						// When two plates  are used and the subclasses are different,
						// the subclass is set to the plate of the first plate
						if (MotorVehicle.Statics.NewToTheSystem == false)
						{
							if (MotorVehicle.Statics.RememberedSubclass != "")
							{
								MotorVehicle.Statics.Subclass = MotorVehicle.Statics.RememberedSubclass;
							}
						}
					}
				}
				if (MotorVehicle.Statics.Subclass == "!!" || fecherFoundation.Strings.Trim(MotorVehicle.Statics.Subclass) == "" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
				{
					if ((fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION") && cmbAnnualReg.Text == "Long Term")
					{
						MotorVehicle.Statics.Subclass = "L9";
					}
					else
					{
						if ((MotorVehicle.Statics.Class1 == "BH") || (MotorVehicle.Statics.Class1 == "PC") || (MotorVehicle.Statics.Class1 == "TL") || (MotorVehicle.Statics.Class1 == "AM") || (MotorVehicle.Statics.Class1 == "BU") || (MotorVehicle.Statics.Class1 == "CL") || (MotorVehicle.Statics.Class1 == "DV") || (MotorVehicle.Statics.Class1 == "EM") || (MotorVehicle.Statics.Class1 == "FD") || (MotorVehicle.Statics.Class1 == "IU") || (MotorVehicle.Statics.Class1 == "TR") || (MotorVehicle.Statics.Class1 == "VT") || (MotorVehicle.Statics.Class1 == "BB") || (MotorVehicle.Statics.Class1 == "LB") || (MotorVehicle.Statics.Class1 == "LS") || (MotorVehicle.Statics.Class1 == "DS") || (MotorVehicle.Statics.Class1 == "WB") || (MotorVehicle.Statics.Class1 == "TS") || (MotorVehicle.Statics.Class1 == "SW") || (MotorVehicle.Statics.Class1 == "BC") || (MotorVehicle.Statics.Class1 == "AW") || (MotorVehicle.Statics.Class1 == "XV") || (MotorVehicle.Statics.Class1 == "VX") || (MotorVehicle.Statics.Class1 == "CV") || (MotorVehicle.Statics.Class1 == "MO") || (MotorVehicle.Statics.Class1 == "PH") || (MotorVehicle.Statics.Class1 == "PO") || (MotorVehicle.Statics.Class1 == "PS") || (MotorVehicle.Statics.Class1 == "AG") || (MotorVehicle.Statics.Class1 == "CR") || (MotorVehicle.Statics.Class1 == "CD") || (MotorVehicle.Statics.Class1 == "CM") || (MotorVehicle.Statics.Class1 == "DX") || (MotorVehicle.Statics.Class1 == "GS") || (MotorVehicle.Statics.Class1 == "UM"))
						{
							MotorVehicle.Statics.strSql = "SELECT SystemCode, Description from CLASS WHERE BmvCode = '" + MotorVehicle.Statics.Class1 + "' ORDER BY SystemCode";
							rsSubClass.OpenRecordset(MotorVehicle.Statics.strSql);
							rsSubClass.MoveLast();
							rsSubClass.MoveFirst();
							frmListBox.InstancePtr.lstSelectPlate.Clear();
							frmListBox.InstancePtr.lstSelectPlate.Columns.Add("");
							frmListBox.InstancePtr.lstSelectPlate.Columns[1].Resizable = false;
							int listWidth = frmListBox.InstancePtr.lstSelectPlate.Width;
							frmListBox.InstancePtr.lstSelectPlate.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.25);
							frmListBox.InstancePtr.lstSelectPlate.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.7);
							frmListBox.InstancePtr.lstSelectPlate.GridLineStyle = GridLineStyle.None;
							frmListBox.InstancePtr.lblHeader.Text = "Select the Subclass for " + MotorVehicle.Statics.Plate1;
							while (!rsSubClass.EndOfFile())
							{
								frmListBox.InstancePtr.lstSelectPlate.AddItem(rsSubClass.Get_Fields_String("SystemCode") + "\t" + rsSubClass.Get_Fields_String("Description"));
								rsSubClass.MoveNext();
							}
							if (MotorVehicle.Statics.Class1 == "PC")
							{
								frmListBox.InstancePtr.lstSelectPlate.SelectedIndex = 1;
							}
							else if (MotorVehicle.Statics.Class1 == "TL")
							{
								if (fecherFoundation.Strings.Trim(fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName)) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
								{
									frmListBox.InstancePtr.lstSelectPlate.SelectedIndex = 4;
								}
								else
								{
									frmListBox.InstancePtr.lstSelectPlate.SelectedIndex = 1;
								}
							}
							else
							{
								frmListBox.InstancePtr.lstSelectPlate.SelectedIndex = 0;
							}
							frmListBox.InstancePtr.Show(FCForm.FormShowEnum.Modal);
							if (MotorVehicle.Statics.ReturnFromListBox != "")
							{
								MotorVehicle.Statics.Subclass = Strings.Mid(MotorVehicle.Statics.ReturnFromListBox, 1, 2);
							}
							else
							{
								cboClass.Text = "";
								MotorVehicle.Statics.Class1 = "";
								MotorVehicle.Statics.Subclass = "";
								temp.Reset();
								rsSubClass2.Reset();
								rsTPlate.Reset();
								return;
							}
						}
						else
						{
							MotorVehicle.Statics.Subclass = MotorVehicle.Statics.Class1;
						}
					}
				}
				MotorVehicle.Statics.Class = MotorVehicle.Statics.Class1;
				MotorVehicle.Statics.gboolLongRegExtension = false;
				MotorVehicle.Statics.gboolLongRegExtensionSamePlate = false;

				if (MotorVehicle.Statics.Class == "TL" && MotorVehicle.Statics.Subclass == "L9")
				{
					if ((MotorVehicle.Statics.RegistrationType != "RRR") && MotorVehicle.Statics.RegistrationType != "LOST" && MotorVehicle.Statics.RegistrationType != "NRT" && (MotorVehicle.Statics.RegistrationType != "RRT" || MotorVehicle.Statics.PlateType == 2) && MotorVehicle.Statics.RegistrationType != "NRR" && MotorVehicle.Statics.RegistrationType != "UPD" && MotorVehicle.Statics.RegistrationType != "HELD" && MotorVehicle.Statics.RegistrationType != "DUPREG" && MotorVehicle.Statics.RegistrationType != "CORR")
					{
						MessageBox.Show("You may not perform this process on a Long Term Trailer", "Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else if (MotorVehicle.Statics.RegistrationType == "CORR")
					{
						if (MotorVehicle.Statics.PlateType == 2)
						{
							if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass") != "L9")
							{
								MessageBox.Show(
									"You may not process a Correction from Long Term Trailers to any other type of Trailer.",
									"Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}

							var mName = modGlobalConstants.Statics.MuniName.ToUpper();
							if (mName == "MAINE MOTOR TRANSPORT" || mName == "AB LEDUE ENTERPRISES" ||
								mName == "COUNTRYWIDE TRAILER" ||
								mName == "HASKELL REGISTRATION")
							{
								if ((cmbLongTerm.Text == "Long Term" && MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate").Right(2) != MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate").Left(2)) || (cmbLongTerm.Text != "Long Term" && MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate").Right(2) != MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate").Left(2)))
								{
									if (MotorVehicle.Statics.LostPlate && MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate").Year == DateTime.Now.Year + 1 && DateTime.Now.Month >= 10 && DateTime.Now.Month < 12)
									{
										MessageBox.Show(
											"You may not process a plate change with a plate from a different year. You may process an extension using Re-Registration Regular.",
											"Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);

									}
									else
									{
										MessageBox.Show(
											"You may not process a plate change with a plate from a different year.",
											"Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									}
									return;
								}
							}
						}
					}
					else if (MotorVehicle.Statics.RegistrationType == "LOST")
					{
						var mName = modGlobalConstants.Statics.MuniName.ToUpper();
						if (mName == "MAINE MOTOR TRANSPORT" || mName == "AB LEDUE ENTERPRISES" ||
							mName == "COUNTRYWIDE TRAILER" ||
							mName == "HASKELL REGISTRATION" || mName == "ACE REGISTRATION SERVICES LLC" || mName == "MAINE TRAILER")
						{
							if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("FleetNumber") == 0)
							{
								MessageBox.Show(
									"You may not perform this process on a vehicle not assigned to a fleet / group.",
									"Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}

						if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass") != "L9")
						{
							MessageBox.Show(
								"You may not process a plate change from Long Term Trailers to any other type of Trailer.",
								"Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}

						if (mName == "MAINE MOTOR TRANSPORT" || mName == "AB LEDUE ENTERPRISES" ||
							mName == "COUNTRYWIDE TRAILER" ||
							mName == "HASKELL REGISTRATION")
						{
							if ((cmbLongTerm.Text == "Long Term" &&
								 MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate").Right(2) !=
								 MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate").Left(2)) ||
								(cmbLongTerm.Text != "Long Term" &&
								 MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate").Right(2) !=
								 MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate").Left(2)))
							{
								if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate").Year == DateTime.Now.Year + 1 && DateTime.Now.Month >= 10 && DateTime.Now.Month < 12)
								{
									MessageBox.Show(
										"You may not process a plate change with a plate from a different year. You may process an extension using Re-Registration Regular.",
										"Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);

								}
								else
								{
									MessageBox.Show(
										"You may not process a plate change with a plate from a different year.",
										"Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}

								return;
							}
						}
					}
					else if (MotorVehicle.Statics.RegistrationType == "DUPREG")
					{
						var mName = modGlobalConstants.Statics.MuniName.ToUpper();
						if (mName == "MAINE MOTOR TRANSPORT" || mName == "AB LEDUE ENTERPRISES" || mName == "COUNTRYWIDE TRAILER" || mName == "HASKELL REGISTRATION" || mName == "ACE REGISTRATION SERVICES LLC" || mName == "MAINE TRAILER")
						{
							if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("FleetNumber") == 0)
							{
								MessageBox.Show(
									@"You may not perform this process on a vehicle not assigned to a fleet / group.",
									"Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
					}
					else if (MotorVehicle.Statics.RegistrationType == "RRR" && MotorVehicle.Statics.PlateType == 1)
					{
						if (!MotorVehicle.Statics.rsPlateForReg.EndOfFile())
						{
							if (Information.IsDate(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate")))
							{
								xdate = MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate");
								intTemp = xdate.DifferenceInMonths(DateTime.Now);
								if (intTemp > 12 || intTemp < -6)
								{
									MessageBox.Show(
										"You may not process an extension on a plate expired more than 6 months or expiring more than 12 months from now",
										"Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}

								MotorVehicle.Statics.gboolLongRegExtensionSamePlate = true;
							}
						}
					}
					else if (MotorVehicle.Statics.RegistrationType == "RRR" && (MotorVehicle.Statics.PlateType == 2 || MotorVehicle.Statics.PlateType == 3))
					{
						if (Information.IsDate(MotorVehicle.Statics.rsSecondPlate.Get_Fields("ExpireDate")))
						{
							if (FCConvert.ToInt32(FCConvert.ToDateTime(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate")).Year) > DateTime.Today.Year)
							{
								int intYear = 0;
								if (FCConvert.ToInt32(FCConvert.ToDateTime(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate")).Year) > DateTime.Today.Year + 1)
								{
									if (MessageBox.Show("Is this an extension?", "Extension?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
									{
										MotorVehicle.Statics.gboolLongRegExtension = true;
									}
									if (!MotorVehicle.Statics.gboolLongRegExtension)
									{
										MessageBox.Show("You must wait until closer to the expiration date to re register this long term trailer", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Information);
										return;
									}
									else
									{
										if (Strings.InStr(1, FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate")), "-", CompareConstants.vbBinaryCompare) == 0)
										{
											intYear = FCConvert.ToInt16(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
										}
										else
										{
											intYear = FCConvert.ToInt32(Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate")), 2)) + 2000;
										}
										if (intYear - DateTime.Today.Year > 20 || intYear - MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate").Year < 2)
										{
											MessageBox.Show("Extensions may not be done for more than 20 years or less than 2 years", "Invalid Extension", MessageBoxButtons.OK, MessageBoxIcon.Information);
											return;
										}
									}
								}
								else
								{
									if (DateTime.Today.Month < 10 || (DateTime.Today.Month < 12 && MotorVehicle.Statics.LostPlate))
									{
										if (MessageBox.Show("Is this an extension?", "Extension?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
										{
											MotorVehicle.Statics.gboolLongRegExtension = true;
										}
										if (!MotorVehicle.Statics.gboolLongRegExtension)
										{
											if (DateTime.Now.Month < 10)
											{
												MessageBox.Show(
													"You must wait until October 1st to re register any long term trailer expiring next year",
													"Invalid Registration", MessageBoxButtons.OK,
													MessageBoxIcon.Information);
												return;
											}
										}
										else
										{
											if (Strings.InStr(1, FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate")), "-", CompareConstants.vbBinaryCompare) == 0)
											{
												intYear = FCConvert.ToInt16(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
											}
											else
											{
												intYear = FCConvert.ToInt32(Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate")), 2)) + 2000;
											}
											if (intYear - DateTime.Today.Year > 20 || intYear - FCConvert.ToInt32(FCConvert.ToDateTime(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate")).Year) < 2)
											{
												MessageBox.Show("Extensions may not be done for more than 20 years or less than 2 years", "Invalid Extension", MessageBoxButtons.OK, MessageBoxIcon.Information);
												return;
											}
										}
									}
								}
							}
						}
					}
				}
				else
				{
					if (MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.PlateType == 1 && txtPlateType[0].Text != "NEW")
					{
						temp.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class + "' AND SystemCode = '" + MotorVehicle.Statics.Subclass + "'", "TWMV0000.vb1");
						if (temp.BeginningOfFile() != true && temp.EndOfFile() != true)
						{
							if (FCConvert.ToInt32(temp.Get_Fields_String("LocationNew")) != 1)
							{
								MessageBox.Show("You may not perform this type of registration for this vehicle class", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
					}
				}
				if (MotorVehicle.Statics.RegistrationType != "UPD" && MotorVehicle.Statics.Class == "PC" && MotorVehicle.Statics.Subclass == "P5")
				{
					if (MotorVehicle.Statics.AllowRentals == false)
					{
						MessageBox.Show("You may not process a registration for this vehicle class", "Invalid Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			// 
			redotag1:
				;
				if ((MotorVehicle.Statics.Class == "PC" && MotorVehicle.Statics.Subclass == "CR") || (MotorVehicle.Statics.Class == "CR" && MotorVehicle.Statics.Subclass == "P2"))
				{
					string newclass = "";
					newclass = Interaction.InputBox("The Class is 'PC', but the Subclass is 'CR'. Please type the correct Class in the box. ( 'PC' or 'CR')", "", null);
					if (newclass == "")
					{
						cmdStartOver_Click();
						return;
					}
					else if (fecherFoundation.Strings.UCase(newclass) != "PC" && fecherFoundation.Strings.UCase(newclass) != "CR")
					{
						goto redotag1;
					}
					else
					{
						if (fecherFoundation.Strings.UCase(newclass) == "PC")
						{
							MotorVehicle.Statics.Class = fecherFoundation.Strings.UCase(newclass);
							MotorVehicle.Statics.Class1 = fecherFoundation.Strings.UCase(newclass);
							MotorVehicle.Statics.Subclass = "P2";
						}
						else
						{
							MotorVehicle.Statics.Class = fecherFoundation.Strings.UCase(newclass);
							MotorVehicle.Statics.Class1 = fecherFoundation.Strings.UCase(newclass);
							MotorVehicle.Statics.Subclass = fecherFoundation.Strings.UCase(newclass);
						}
					}
				}
				if (MotorVehicle.Statics.ForcedPlate == "T")
				{
				redotag:
					;
					frmTempPlate.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					if (MotorVehicle.Statics.TempPlateNumber != 0)
					{
						MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Status = 'A' AND substring(Code,4,2) = 'TM' AND Number = " + FCConvert.ToString(MotorVehicle.Statics.TempPlateNumber) + "";
						rsTPlate.OpenRecordset(MotorVehicle.Statics.strSql);
						if (rsTPlate.EndOfFile() != true && rsTPlate.BeginningOfFile() != true)
						{
							rsTPlate.MoveLast();
							rsTPlate.MoveFirst();
						}
						else
						{
							goto redotag;
						}
					}
					else
					{
						MotorVehicle.ClearVariables();
						optRegType_Click("Re-Registration Regular");
						return;
					}
					rsTPlate.Reset();
				}
				// 
				if (MotorVehicle.Statics.NewPlate && MotorVehicle.Statics.Class == "VT")
				{
					MessageBox.Show("Be sure to check the registrant's DD2-14 form", "Veterans Plate Check", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else if (MotorVehicle.Statics.NewPlate && MotorVehicle.Statics.Class == "PH" || MotorVehicle.Statics.Class == "PM")
				{
					MessageBox.Show("Be sure to check for Evidence of award (NOTE: this can be, BUT DOESNT HAVE TO BE, THE DD2-14 form)", "Check Evidence of Award", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				rsClass.Reset();
				rsClass.OpenRecordset("SELECT * FROM Class");
				if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
				{
					rsClass.MoveLast();
					rsClass.MoveFirst();
					MotorVehicle.Statics.FixedMonth = false;
					if (rsClass.FindFirstRecord2("BMVCode, SystemCode", MotorVehicle.Statics.Class1 + "," + MotorVehicle.Statics.Subclass, ","))
					{
						if (FCConvert.ToString(rsClass.Get_Fields_String("renewaldate")) == "F")
						{
							if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class1))
							{
								string strRegT = "";
								if (MotorVehicle.Statics.RegistrationType != "HELD")
								{
									strRegT = MotorVehicle.Statics.RegistrationType;
								}
								else
								{
									strRegT = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields("TransactionType"));
								}
								if (strRegT == "RRR")
								{
									if (MotorVehicle.Statics.PlateType == 1)
									{
										if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate") < FCConvert.ToDateTime("3/1/2012"))
										{
											// DJW 3/14/12 Change in MC Legislation   And rsPlateForReg.Fields("ExpireDate") > Date
											MotorVehicle.Statics.FixedMonth = false;
											MotorVehicle.Statics.gintFixedMonthMonth = 0;
										}
										else
										{
											MotorVehicle.Statics.FixedMonth = true;
											MotorVehicle.Statics.gintFixedMonthMonth = FCConvert.ToInt16(rsClass.Get_Fields_Int16("FixedMonth"));
										}
									}
									else
									{
										if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate") < FCConvert.ToDateTime("3/1/2012"))
										{
											MotorVehicle.Statics.FixedMonth = false;
											MotorVehicle.Statics.gintFixedMonthMonth = 0;
										}
										else
										{
											MotorVehicle.Statics.FixedMonth = true;
											MotorVehicle.Statics.gintFixedMonthMonth = FCConvert.ToInt16(rsClass.Get_Fields_Int16("FixedMonth"));
										}
									}
								}
								else if (strRegT == "NRR")
								{
									if (DateTime.Today.ToOADate() < FCConvert.ToDateTime("3/1/2012").ToOADate())
									{
										MotorVehicle.Statics.FixedMonth = false;
										MotorVehicle.Statics.gintFixedMonthMonth = 0;
									}
									else
									{
										MotorVehicle.Statics.FixedMonth = true;
										MotorVehicle.Statics.gintFixedMonthMonth = FCConvert.ToInt16(rsClass.Get_Fields_Int16("FixedMonth"));
									}
								}
								else if ((strRegT == "CORR" && MotorVehicle.Statics.PlateType == 2) || strRegT == "LOST")
								{
									if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate") < FCConvert.ToDateTime("3/1/2013"))
									{
										MotorVehicle.Statics.FixedMonth = false;
										MotorVehicle.Statics.gintFixedMonthMonth = 0;
									}
									else
									{
										MotorVehicle.Statics.FixedMonth = true;
										MotorVehicle.Statics.gintFixedMonthMonth = FCConvert.ToInt16(rsClass.Get_Fields_Int16("FixedMonth"));
									}
								}
								else
								{
									if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate") < FCConvert.ToDateTime("3/1/2013"))
									{
										MotorVehicle.Statics.FixedMonth = false;
										MotorVehicle.Statics.gintFixedMonthMonth = 0;
									}
									else
									{
										MotorVehicle.Statics.FixedMonth = true;
										MotorVehicle.Statics.gintFixedMonthMonth = FCConvert.ToInt16(rsClass.Get_Fields_Int16("FixedMonth"));
									}
								}
							}
							else
							{
								MotorVehicle.Statics.FixedMonth = true;
								MotorVehicle.Statics.gintFixedMonthMonth = FCConvert.ToInt16(rsClass.Get_Fields_Int16("FixedMonth"));
							}
						}
						else
						{
							MotorVehicle.Statics.FixedMonth = false;
							MotorVehicle.Statics.gintFixedMonthMonth = 0;
						}
					}
				}
				MotorVehicle.Statics.Class = MotorVehicle.Statics.Class1;
				if (MotorVehicle.Statics.RegistrationType == "UPD")
				{
					clsDRWrapper temp3 = MotorVehicle.Statics.rsFinal;
					temp = MotorVehicle.Statics.rsPlateForReg;
					MotorVehicle.TransferRecordSetsA(ref temp3, ref temp);
					MotorVehicle.Statics.rsFinal = temp3;
					MotorVehicle.Statics.rsPlateForReg = temp;
					frmWait.InstancePtr.Show();
					if (cmbRegType1.Text == "New To System / Update")
					{
						frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Retrieving Information";
						frmWait.InstancePtr.Refresh();
					}
					else
					{
						frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Building MVR3";
						frmWait.InstancePtr.Refresh();
					}
					frmWait.InstancePtr.Refresh();
					if (MotorVehicle.Statics.Class == "TL" && MotorVehicle.Statics.Subclass == "L9")
					{
						frmVehicleUpdateLongTerm.InstancePtr.Show(App.MainForm);
					}
					else
					{
						frmVehicleUpdate.InstancePtr.Show(App.MainForm);
					}
					frmWait.InstancePtr.Unload();
					return;
				}

				fecherFoundation.Information.Err().Clear();
				if (MotorVehicle.Statics.FromTransfer)
				{
					MotorVehicle.Statics.FromTransfer = false;
				}
				else if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.FirstFleetVehicle)
				{
					frmQuestions.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					MotorVehicle.Statics.FirstFleetVehicle = false;
				}
				else if (MotorVehicle.Statics.RegisteringFleet && !MotorVehicle.Statics.FirstFleetVehicle)
				{
					// do nothing
				}
				else
				{
					frmQuestions.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				}
				if (fecherFoundation.Information.Err().Number == 12345 || fecherFoundation.Information.Err().Number == 12346 || fecherFoundation.Information.Err().Number == 12347)
				{
					Close();
					return;
				}
				// 
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Building MVR3";
				frmWait.InstancePtr.Refresh();
				if (MotorVehicle.Statics.RegistrationType == "LOST")
				{
					Lost_Plate_Registration();
					return;
				}
				else if (MotorVehicle.Statics.RegistrationType == "GVW")
				{
					MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM Master Where (Plate = '" + MotorVehicle.Statics.Plate1 + "' OR platestripped = '" + fecherFoundation.Strings.Trim(MotorVehicle.Statics.Plate1).Replace("&", "").Replace(" ", "").Replace("-", "") + "' ) And Class = '" + MotorVehicle.Statics.Class + "'");
					if (MotorVehicle.Statics.rsPlateForReg.EndOfFile() != true && MotorVehicle.Statics.rsPlateForReg.BeginningOfFile() != true)
					{
						MotorVehicle.Statics.rsPlateForReg.MoveLast();
						MotorVehicle.Statics.rsPlateForReg.MoveFirst();
					}
					else
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("There is no record in the database with that plate number.  Unable to process this transaction", "Invalid Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
				}
				else if (MotorVehicle.Statics.RegistrationType == "DUPSTK")
				{
					if (MotorVehicle.Statics.rsPlateForReg.RecordCount() < 1)
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Record is not in the Motor Vehicle Database.  Unable to process this transaction", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						frmPlateInfo.InstancePtr.Unload();
					}
					else
					{
						if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("MessageFlag")) || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Address")))) == "REGISTRATION SUSPENDED")
						{
							if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("MessageFlag") + "") == 2)
							{
								MessageBox.Show(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserMessage")), "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							else if (Conversion.Val(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("MessageFlag") + "") == 3)
							{
								frmWait.InstancePtr.Unload();
								//App.DoEvents();
								MessageBox.Show(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("UserMessage")), "Message", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								return;
							}
						}
						Duplicate_Stickers();
					}
					return;
				}
				// 
				if (MotorVehicle.Statics.RegistrationType == "HELD")
				{
					MotorVehicle.Statics.HoldRegistrationDollar = true;
					MotorVehicle.Statics.rsFinal.OpenRecordset(MotorVehicle.Statics.rsPlateForReg.Name());
					if (MotorVehicle.Statics.rsFinal.EndOfFile() != true && MotorVehicle.Statics.rsFinal.BeginningOfFile() != true)
					{
						MotorVehicle.Statics.rsFinal.MoveLast();
						MotorVehicle.Statics.rsFinal.MoveFirst();
						MotorVehicle.Statics.rsFinal.Edit();
					}
					else
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("There is no Record in the master file matching the Held registration", "No Matching Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
					Close();
					if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "L9")
					{
						frmDataInputLongTermTrailers.InstancePtr.Show(App.MainForm);
					}
					else
					{
						frmDataInput.InstancePtr.Show(App.MainForm);
					}
					frmWait.InstancePtr.Unload();
					return;
				}
				if (MotorVehicle.Statics.RegistrationType == "RRR" && MotorVehicle.Statics.Class == "AP" && (MotorVehicle.Statics.PlateType == 2 || MotorVehicle.Statics.PlateType == 3))
				{
					CheckForChangeToFleet();
					if (MotorVehicle.Statics.blnChangeToFleet == false)
					{
						if ((FCConvert.ToInt32(FCConvert.ToDateTime(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate")).Month) == DateTime.Today.Month) && (FCConvert.ToInt32(FCConvert.ToDateTime(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate")).Year) == (DateTime.Today.Year - 1)))
						{
							xdate = fecherFoundation.DateAndTime.DateAdd("yyyy", 1, FCConvert.ToDateTime(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate")));
						}
						else
						{
							xdate = FCConvert.ToDateTime(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate"));
						}
						LowDate = fecherFoundation.DateAndTime.DateAdd("m", -6, xdate);
						if (LowDate.ToOADate() > DateTime.Today.ToOADate())
						{
							MessageBox.Show("This registration cannot be processed. The expiration date of " + MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate") + "\r\n" + "is greater than 6 months away from now", "Cannot Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							frmWait.InstancePtr.Unload();
							return;
						}
					}
				}
				else
				{
					MotorVehicle.Statics.blnChangeToFleet = false;
					MotorVehicle.Statics.lngFleetNumberBeingChangedTo = 0;
				}
				temp = MotorVehicle.Statics.rsPlateForReg;
				clsDRWrapper temp1 = MotorVehicle.Statics.rsSecondPlate;
				clsDRWrapper temp2 = MotorVehicle.Statics.rsThirdPlate;
				MotorVehicle.BuildFinalRS(ref temp, ref temp1, ref temp2);
				MotorVehicle.Statics.rsPlateForReg = temp;
				MotorVehicle.Statics.rsSecondPlate = temp1;
				MotorVehicle.Statics.rsThirdPlate = temp2;
				if (MotorVehicle.Statics.rsFinal.IsntAnything())
				{
					frmWait.InstancePtr.Unload();
					cboClass.Text = "";
					cmdProcess.Enabled = false;
					if (cboLongTermRegPlateYear.Visible)
					{
						cboLongTermRegPlateYear.Focus();
					}
					else
					{
						txtPlateType[0].Focus();
					}
					return;
				}

				if (MotorVehicle.Statics.RegistrationType == "DUPREG")
				{
					if (Strings.UCase(Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address")))) != "REGISTRATION SUSPENDED" && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag") + "") != 3)
					{
						if (!FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag")) || Strings.UCase(Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address")))) == "REGISTRATION SUSPENDED")
						{
							if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag") + "") == 2)
							{
								MessageBox.Show(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage")), "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "L9")
						{
							MotorVehicle.Statics.gboolFromLongTermDataEntry = true;
							//FC:FINAL:DDU:#2264 - set focus to correct form
							frmPlateInfo.InstancePtr.Unload();
							frmWait.InstancePtr.Unload();
							frmSavePrint.InstancePtr.Show(App.MainForm);
							MessageBox.Show("Please be sure to collect " + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee"), "$#,##0.00"), "Collect Fee", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							//FC:FINAL:DDU:#2264 - set focus to correct form
							frmPlateInfo.InstancePtr.Unload();
							frmWait.InstancePtr.Unload();
							frmPreview.InstancePtr.DisableReturnButton = true;
							frmPreview.InstancePtr.DisableUpdateReasonCodes = true;
							frmPreview.InstancePtr.Show(App.MainForm);
						}
					}

					return;
				}

				if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address")))) == "REGISTRATION SUSPENDED")
				{
					if ((MotorVehicle.Statics.RegistrationType == "RRR" || MotorVehicle.Statics.RegistrationType == "RRT" || MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT") && !MotorVehicle.Statics.PreviousETO)
					{
						MessageBox.Show("The Registrant must contact the Bureau of Motor Vehicles before completing this registration.  This will be processed as an Excise Tax Only transaction", "Contact BMV", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						MotorVehicle.Statics.blnSuspended = true;
						MotorVehicle.Statics.ETO = true;
					}
					else
					{
						// Dave 6/8/06
						MessageBox.Show("The Registrant must contact the Bureau of Motor Vehicles before completing this registration", "Contact BMV", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						frmWait.InstancePtr.Unload();
						frmPreview.InstancePtr.Unload();
						cboClass.Text = "";
						cmdProcess.Enabled = false;
						if (txtPlateType[0].Visible == true)
						{
							txtPlateType[0].Focus();
						}
						else if (cboLongTermRegPlateYear.Visible == true)
						{
							cboLongTermRegPlateYear.Focus();
						}
						else
						{
							frmPlateInfo.InstancePtr.Unload();
						}
						return;
					}
				}
				if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag")) || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Address")))) == "REGISTRATION SUSPENDED")
				{
					if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("MessageFlag") + "") == 3)
					{
						frmWait.InstancePtr.Unload();
						this.Refresh();
						MessageBox.Show(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("UserMessage")), "Message", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
				}
				if (MotorVehicle.Statics.blnChangeToFleet)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("FleetNumber", MotorVehicle.Statics.lngFleetNumberBeingChangedTo);
					MotorVehicle.Statics.rsFinal.Set_Fields("FleetNew", true);
				}
				if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TL" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CL") && MotorVehicle.Statics.RegistrationType != "CORR")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("TwoYear", 0);
				}
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) == "NEW" && MotorVehicle.Statics.ReplacePlate)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("plate", MotorVehicle.Statics.ReplacementPlateNumber);
				}

				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) != "ECR" && (MotorVehicle.Statics.RegistrationType != "RRR" || MotorVehicle.Statics.PlateType != 2))
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("TaxIDNumber", MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("TaxIDNumber"));
				}
				MotorVehicle.Statics.rsFinal.Set_Fields("TitleDone", false);
				MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", false);
				MotorVehicle.Statics.lngCTAAddNewID = 0;
				MotorVehicle.Statics.lngUTCAddNewID = 0;
				if (MotorVehicle.Statics.TownLevel == 6)
				{
					if (MotorVehicle.Statics.RegistrationType == "RRR" || MotorVehicle.Statics.RegistrationType == "NRR")
					{
						MotorVehicle.Statics.ETO = true;
					}
				}
				else if (MotorVehicle.Statics.TownLevel == 1)
				{
					if (MotorVehicle.Statics.RegistrationType == "NRR")
					{
						MotorVehicle.Statics.ETO = true;
					}
				}
				else if (MotorVehicle.Statics.TownLevel == 5)
				{
					if (MotorVehicle.Statics.RegistrationType == "NRR")
					{
						if (MotorVehicle.Statics.PlateType != 2 || (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("registeredWeightNew")) > 26000 && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CO" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CC" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AC" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TT" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "LC")))
						{
							MotorVehicle.Statics.ETO = true;
						}
					}
				}
				else if (MotorVehicle.Statics.TownLevel == 2)
				{
					if (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT")
					{
						if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("registeredWeightNew")) > 26000 && (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CO" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TT" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "CC" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AC" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "AF" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "LC"))
						{
							MotorVehicle.Statics.ETO = true;
						}
					}
				}
				Hide();
				if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "TL" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Subclass")) == "L9")
				{
					var mName = modGlobalConstants.Statics.MuniName.ToUpper();

					if ((mName == "MAINE MOTOR TRANSPORT" || mName == "AB LEDUE ENTERPRISES" || mName == "COUNTRYWIDE TRAILER" || mName == "HASKELL REGISTRATION") && cmbLongTerm.Text != "Long Term")
					{
						MessageBox.Show("You must select the long term option before you may proceed with this registration.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						Close();
						return;
					}
					else
					{
						frmDataInputLongTermTrailers.InstancePtr.Show(App.MainForm);
					}
				}
				else
				{
					frmDataInput.InstancePtr.IsPreReg = cmbPreReg.Text == "Pre Reg";
					FCUtils.ApplicationUpdate(App.MainForm);
					frmDataInput.InstancePtr.Show(App.MainForm);
					FCUtils.ApplicationUpdate(App.MainForm);
				}
				GoingToDataInput = false;
				frmWait.InstancePtr.Unload();
				if (MotorVehicle.Statics.FromTransfer)
				{
					return;
				}
				if (fecherFoundation.Information.Err().Number != 0 || FCConvert.ToString(modGNBas.Statics.Response) == "BAD")
				{
					Close();
					return;
				}
				Close();
			}
            finally
            {
                //temp.Dispose(); Clears out rsPlateForReg
                rsSubClass2.Dispose();
                rsTPlate.Dispose();
                rsHeldInfo.Dispose();
                rsBoosters.Dispose();
                rsUser.Dispose();
				rsClass.Dispose();
				rsSubClass.Dispose();
			}
        }

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void cmdRegister_Click(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			if (cboFleets.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a fleet or group before you may continue", "No Fleet / Group Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			MotorVehicle.Statics.PreEffectiveDate = FCConvert.ToDateTime(txtEffectiveDate.Text);
			MotorVehicle.Statics.PendingRegistration = cmbPreReg.Text == "Pre Reg";
			if (!MotorVehicle.Statics.RegisteringFleet)
			{
				MotorVehicle.Statics.RegisteringFleet = true;
				MotorVehicle.Statics.FirstFleetVehicle = true;
				MotorVehicle.Statics.RememberedIndex = cboFleets.SelectedIndex;
				MotorVehicle.Statics.FleetBeingRegistered = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(Strings.Left(cboFleets.Text, 5)))));
				if (cmbGroup.Text == "Group")
				{
					MotorVehicle.Statics.GroupOrFleet = "G";
				}
				else
				{
					MotorVehicle.Statics.GroupOrFleet = "F";
				}
				MotorVehicle.Statics.VehiclesRegistered = new bool[vsVehicles.Rows - 1 + 1];
				MotorVehicle.Statics.VehicleInformation = new object[10 + 1, vsVehicles.Rows - 1 + 1];
				MotorVehicle.Statics.NumberOfVehicles = vsVehicles.Rows - 1;
			}
			for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
			{
				if (vsVehicles.TextMatrix(counter, 0) == "Next")
				{
					MotorVehicle.Statics.VehicleBeingRegistered = counter;
				}
				MotorVehicle.Statics.VehiclesRegistered[counter - 1] = FCConvert.CBool(vsVehicles.TextMatrix(counter, RegisterCol));
			}
			for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
			{
				for (counter2 = 0; counter2 <= vsVehicles.Cols - 1; counter2++)
				{
					MotorVehicle.Statics.VehicleInformation[counter2, counter] = vsVehicles.TextMatrix(counter, counter2);
				}
			}
			fraFleetRegistration.Visible = false;
			cmbRegType1.Text = "Re-Registration Regular";
			cmbPlate.SelectedIndex = 0;
			txtPlateType[0].Text = fecherFoundation.Strings.Trim(vsVehicles.TextMatrix(MotorVehicle.Statics.VehicleBeingRegistered, PlateCol));
			txtPlateType_Validate(0, false);
			GoingToDataInput = true;
			cmdProcess_Click();
		}

		private void cmdRegisterNew_Click(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			if (cboFleets.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a fleet or group before you may continue", "No Fleet / Group Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Information.IsDate(txtEffectiveDate.Text))
			{
				MotorVehicle.Statics.PreEffectiveDate = FCConvert.ToDateTime(txtEffectiveDate.Text);
			}
			MotorVehicle.Statics.PendingRegistration = cmbPreReg.Text == "Pre Reg";
			MotorVehicle.Statics.RememberedIndex = cboFleets.SelectedIndex;
			MotorVehicle.Statics.FleetBeingRegistered = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(Strings.Left(cboFleets.Text, 5)))));
			if (cmbGroup.Text == "Group")
			{
				MotorVehicle.Statics.GroupOrFleet = "G";
			}
			else
			{
				MotorVehicle.Statics.GroupOrFleet = "F";
			}
			MotorVehicle.Statics.VehiclesRegistered = new bool[vsVehicles.Rows + 1];
			MotorVehicle.Statics.VehicleInformation = new object[10 + 1, vsVehicles.Rows + 1];
			if (vsVehicles.Visible == true)
			{
				MotorVehicle.Statics.NumberOfVehicles = vsVehicles.Rows;
			}
			else
			{
				MotorVehicle.Statics.NumberOfVehicles = 0;
			}
			MotorVehicle.Statics.VehiclesRegistered[0] = true;
			if (vsVehicles.Visible == true)
			{
				for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
				{
					if (vsVehicles.TextMatrix(counter, 0) == "Next")
					{
						MotorVehicle.Statics.VehicleBeingRegistered = counter;
						vsVehicles.TextMatrix(counter, 0, "Unregistered");
					}
					MotorVehicle.Statics.VehiclesRegistered[counter] = FCConvert.CBool(vsVehicles.TextMatrix(counter, RegisterCol));
				}
				for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
				{
					for (counter2 = 0; counter2 <= vsVehicles.Cols - 1; counter2++)
					{
						MotorVehicle.Statics.VehicleInformation[counter2, counter] = vsVehicles.TextMatrix(counter, counter2);
					}
				}
			}
			if (!MotorVehicle.Statics.RegisteringFleet)
			{
				MotorVehicle.Statics.RegisteringFleet = true;
				MotorVehicle.Statics.FirstFleetVehicle = true;
			}
			fraFleetRegistration.Visible = false;
			cmbRegType1.Text = "New Registration Regular";
			cmbPlate.SelectedIndex = 0;
			if (cboLongTermRegPlateYear.Visible)
			{
				cboLongTermRegPlateYear.Focus();
			}
			else
			{
				txtPlateType[0].Focus();
			}
			GoingToDataInput = true;
			for (counter = cmbRegType1.Items.Count - 1; counter >= 0; counter--)
			{
				if (cmbRegType1.Items[counter].ToString() != "New Registration Regular" && cmbRegType1.Items[counter].ToString() != "Register Fleet / Group")
				{
					cmbRegType1.Items.RemoveAt(counter);
				}
			}
			OKFlag = true;
			cmbRegType1.SelectedIndex = cmbRegType1.Items.IndexOf("New Registration Regular");
			cmbPlate.SelectedIndex = cmbPlate.Items.IndexOf("New Plate");
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			EnableDisableForm(true);
			cmdProcess.Enabled = false;
			fraResults.Visible = false;
			fraSelection.Visible = false;
		}

		private void cmdStartOver_Click(object sender, System.EventArgs e)
		{
			DialogResult ans = 0;
			if (MotorVehicle.Statics.FromTransfer)
			{
				ans = MessageBox.Show("You are in the middle of processing a registration.  Do you wish to cancel this registration?", "Cancel Registration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					MotorVehicle.Statics.FromTransfer = false;
				}
				else
				{
					return;
				}
				ClearScreen(true);
				lblRegType.Text = "";
				cmdSearch.Enabled = false;
				cmdProcess.Enabled = false;
			}
			else
			{
				ClearScreen(true);
				lblRegType.Text = "";
				cmdSearch.Enabled = false;
				cmdProcess.Enabled = false;
			}
			MotorVehicle.Statics.InfoForCorrect = false;
			MotorVehicle.Statics.InfoForReReg = false;
			MotorVehicle.Statics.InfoForDuplicate = false;
		}

		public void cmdStartOver_Click()
		{
			cmdStartOver_Click(cmdStartOver, new System.EventArgs());
		}

		private void frmPlateInfo_Activated(object sender, System.EventArgs e)
		{
			if (bolFormAlreadyLoaded)
				goto NewReg;
			ClearScreen(true);
			LoadClass();
			LoadYears();
			bolFormAlreadyLoaded = true;
			PlateFromSearch = false;
			NewReg:
			;
			MotorVehicle.Statics.RememberedPlate = "";
			if (MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.RegisteringFleet)
			{
				GoingToDataInput = true;
			}
			if (!GoingToDataInput)
			{
				if (MotorVehicle.Statics.RegisteringFleet)
				{
					OKFlag = true;
					cmbRegType1.Text = "Register Fleet / Group";
					cboFleets.SelectedIndex = MotorVehicle.Statics.RememberedIndex;
					cboFleets_Click();
				}
			}
			if (MotorVehicle.Statics.blnTryCorrectAgain)
			{
				OKFlag = true;
				cmbRegType1.Text = "Correction";
				if (MotorVehicle.Statics.SecondCorrectPlate == "")
				{
					cmbPlate.SelectedIndex = 0;
				}
				else
				{
					cmbPlate.SelectedIndex = 1;
				}
				if (MotorVehicle.Statics.CorrectLongTermReg)
				{
					cmbAnnualReg.Text = "Long Term";
				}
				if (MotorVehicle.Statics.CorrectLongTermReg)
				{
					cboLongTermRegPlateYear.Text = MotorVehicle.Statics.FirstCorrectPlate;
				}
				else
				{
					txtPlateType[0].Text = MotorVehicle.Statics.FirstCorrectPlate;
					txtPlateType_Validate(0, false);
					cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.FirstCorrectClass);
				}
				if (MotorVehicle.Statics.SecondCorrectPlate != "")
				{
					txtPlateType[1].Text = MotorVehicle.Statics.SecondCorrectPlate;
					txtPlateType_Validate(0, false);
					cboClass2.SelectedIndex = intClassIndex(MotorVehicle.Statics.SecondCorrectClass);
				}
				MotorVehicle.Statics.blnTryCorrectAgain = false;
				cmdProcess_Click();
			}
			if (MotorVehicle.Statics.blnTryReRegAgain)
			{
				MotorVehicle.Statics.blnTryReRegAgain = false;
				OKFlag = true;
				cmbRegType1.Text = "Re-Registration Regular";
				cmbPlate.SelectedIndex = MotorVehicle.Statics.CorrectPlateType;
				txtPlateType[0].Text = MotorVehicle.Statics.FirstCorrectPlate;
				txtPlateType_Validate(0, false);
				cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.FirstCorrectClass);
				if (MotorVehicle.Statics.SecondCorrectPlate != "")
				{
					txtPlateType[1].Text = MotorVehicle.Statics.SecondCorrectPlate;
					txtPlateType_Validate(0, false);
					cboClass2.SelectedIndex = intClassIndex(MotorVehicle.Statics.SecondCorrectClass);
				}
				//App.DoEvents();
				cmdProcess_Click();
			}
			if (MotorVehicle.Statics.blnTryDuplicateAgain)
			{
				MotorVehicle.Statics.blnTryDuplicateAgain = false;
				OKFlag = true;
				cmbRegType1.Text = "Duplicate Registration";
				cmbPlate.SelectedIndex = MotorVehicle.Statics.CorrectPlateType;
				txtPlateType[0].Text = MotorVehicle.Statics.FirstCorrectPlate;
				txtPlateType_Validate(0, false);
				cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.FirstCorrectClass);
				if (MotorVehicle.Statics.SecondCorrectPlate != "")
				{
					txtPlateType[1].Text = MotorVehicle.Statics.SecondCorrectPlate;
					txtPlateType_Validate(0, false);
					cboClass2.SelectedIndex = intClassIndex(MotorVehicle.Statics.SecondCorrectClass);
				}
				cmdProcess_Click();
			}
		}

		private void frmPlateInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F5)
			{
				if (cmdSearch.Enabled == true)
				{
					KeyCode = (Keys)0;
					mnuSearch_Click();
				}
			}
			if (fraFleetRegistration.Visible == false)
			{
				if (txtPlateType[0].Visible == false && cmbPlate.Visible == false)
				{
					if (txtPlateType[0].Enabled == true)
					{
						switch (KeyCode)
						{
							case Keys.D1:
							case Keys.NumPad1:
								{
									if (cmbRegType1.Items.Contains("Re-Registration Regular"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Re-Registration Regular";
									}
									break;
								}
							case Keys.D2:
							case Keys.NumPad2:
								{
									if (cmbRegType1.Items.Contains("Re-Registration Transfer"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Re-Registration Transfer";
									}
									break;
								}
							case Keys.D3:
							case Keys.NumPad3:
								{
									if (cmbRegType1.Items.Contains("New Registration Regular"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "New Registration Regular";
									}
									break;
								}
							case Keys.D4:
							case Keys.NumPad4:
								{
									if (cmbRegType1.Items.Contains("New Registration Transfer"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "New Registration Transfer";
									}
									break;
								}
							case Keys.D5:
							case Keys.NumPad5:
								{
									if (cmbRegType1.Items.Contains("Complete Held Registration"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Complete Held Registration";
									}
									break;
								}
							case Keys.D6:
							case Keys.NumPad6:
								{
									if (cmbRegType1.Items.Contains("Correction"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Correction";
									}
									break;
								}
							case Keys.D7:
							case Keys.NumPad7:
								{
									if (cmbRegType1.Items.Contains("Duplicate Registration"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Duplicate Registration";
									}
									break;
								}
							case Keys.D8:
							case Keys.NumPad8:
								{
									if (cmbRegType1.Items.Contains("Lost Plate / Stickers"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Lost Plate / Stickers";
									}
									break;
								}
							case Keys.D9:
							case Keys.NumPad9:
								{
									if (cmbRegType1.Items.Contains("Duplicate Stickers"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Duplicate Stickers";
									}
									break;
								}
							case Keys.A:
								{
									if (cmbRegType1.Items.Contains("RVW Change"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "RVW Change";
									}
									break;
								}
							case Keys.B:
								{
									if (cmbRegType1.Items.Contains("Transit Plate"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Transit Plate";
									}
									break;
								}
							case Keys.C:
								{
									if (cmbRegType1.Items.Contains("Booster Permit"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Booster Permit";
									}
									break;
								}
							case Keys.D:
								{
									if (cmbRegType1.Items.Contains("Special Reg Permit"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Special Reg Permit";
									}
									break;
								}
							case Keys.E:
								{
									if (cmbRegType1.Items.Contains("Void MVR"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Void MVR";
									}
									break;
								}
							case Keys.F:
								{
									if (cmbRegType1.Items.Contains("New To System / Update"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "New To System / Update";
									}
									break;
								}
							case Keys.H:
								{
									if (cmbRegType1.Items.Contains("Register Fleet / Group"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Register Fleet / Group";
									}
									break;
								}
							case Keys.I:
								{
									if (cmbRegType1.Items.Contains("Void Permit"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Void Permit";
									}
									break;
								}
							case Keys.J:
								{
									if (cmbRegType1.Items.Contains("Process CTA"))
									{
										OKFlag = true;
										cmbRegType1.Focus();
										cmbRegType1.Text = "Process CTA";
									}
									break;
								}
						}
					}
				}
			}
			if (cmbPlate.Visible == true && txtPlateType[0].Visible == false && cboLongTermRegPlateYear.Visible == false)
			{
				if (cmbPlate.Enabled == true && txtPlateType[0].Enabled == true)
				{
					switch (KeyCode)
					{
						case Keys.A:
							{
								cmbPlate.SelectedIndex = 0;
								break;
							}
						case Keys.B:
							{
								if (cmbPlate.Items.Count > 1)
									cmbPlate.SelectedIndex = 1;
								break;
							}
						case Keys.C:
							{
								if (cmbPlate.Items.Count > 2)
									cmbPlate.SelectedIndex = 2;
								break;
							}
						case Keys.D:
							{
								if (cmbPlate.Items.Count > 3)
									cmbPlate.SelectedIndex = 3;
								break;
							}
					}
				}
			}
		}

		private void frmPlateInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			DialogResult ans = 0;
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (MotorVehicle.Statics.FromTransfer)
				{
					ans = MessageBox.Show("You are in the middle of processing a registration.  Do you wish to cancel this registration?", "Cancel Registration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						MotorVehicle.Statics.FromTransfer = false;
					}
					else
					{
						return;
					}
				}
				if (fraResults.Visible == true || fraSelection.Visible == true)
				{
					EnableDisableForm(true);
					fraResults.Visible = false;
					fraSelection.Visible = false;
				}
				else if (fraFleetRegistration.Visible == true)
				{
					cmdCancel_Click();
				}
				else
				{
					if (MotorVehicle.Statics.RegisteringFleet)
					{
						cmdCancel_Click();
					}
					Close();
				}
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmPlateInfo.InstancePtr.ActiveControl.GetName() == txtCriteria.Name)
				{
					// Do Nothing
				}
				else if (frmPlateInfo.InstancePtr.ActiveControl.GetName() == "optRegType")
				{
					KeyAscii = (Keys)0;
					OKFlag = true;
					optRegType_Click(frmPlateInfo.InstancePtr.ActiveControl.Text);
				}
				else if (frmPlateInfo.InstancePtr.ActiveControl.GetName() == "vsResults")
				{
					vsResults_DblClick(null, null);
				}
				else
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPlateInfo_Load(object sender, System.EventArgs e)
		{
			int counter;
			if (MotorVehicle.JobComplete("frmDataInput"))
			{
				MotorVehicle.ClearAllVariables();
				if (MotorVehicle.Statics.RegisteringFleet)
				{
					fraFleetRegistration.Visible = true;
					fraFleetRegistration.BringToFront();
				}
			}

			if (!modSecurity.ValidPermissions(this, MotorVehicle.VEHICLEUPDATE))
			{
				if (cmbRegType1.Items.Contains("New To System / Update"))
				{
					cmbRegType1.Items.Remove("New To System / Update");
				}
			}
			else
			{
				if (!cmbRegType1.Items.Contains("New To System / Update"))
				{
					cmbRegType1.Items.Insert(15, "New To System / Update");
				}
			}
			if (modGlobalConstants.Statics.gboolCR && !MotorVehicle.Statics.bolFromWindowsCR)
			{
				for (counter = cmbRegType1.Items.Count - 1; counter >= 0; counter--)
				{
					if (cmbRegType1.Items[counter].ToString() != "Re-Registration Regular" && cmbRegType1.Items[counter].ToString() != "Re-Registration Transfer"
						&& cmbRegType1.Items[counter].ToString() != "New Registration Regular" && cmbRegType1.Items[counter].ToString() != "New Registration Transfer"
						&& cmbRegType1.Items[counter].ToString() != "New To System / Update" 
						&& cmbRegType1.Items[counter].ToString() != "Register Fleet / Group")
					{
						cmbRegType1.Items.RemoveAt(counter);
					}
				}
			}
			RegisterCol = 1;
			ClassCol = 2;
			PlateCol = 3;
			VINCol = 4;
			YearCol = 5;
			MakeCol = 6;
			ModelCol = 7;
			UnitCol = 8;
			TotalCol = 9;
			vsVehicles.ColDataType(RegisterCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsVehicles.ColFormat(TotalCol, "#,##0.00");
			vsVehicles.TextMatrix(0, RegisterCol, "Register");
			vsVehicles.TextMatrix(0, ClassCol, "Class");
			vsVehicles.TextMatrix(0, PlateCol, "Plate");
			vsVehicles.TextMatrix(0, VINCol, "VIN");
			vsVehicles.TextMatrix(0, YearCol, "Year");
			vsVehicles.TextMatrix(0, MakeCol, "Make");
			vsVehicles.TextMatrix(0, ModelCol, "Model");
			vsVehicles.TextMatrix(0, UnitCol, "Unit");
			vsVehicles.TextMatrix(0, TotalCol, "Total");
            vsVehicles.ColWidth(0, (int)(vsVehicles.WidthOriginal * 0.10));
            vsVehicles.ColWidth(1, (int)(vsVehicles.WidthOriginal * 0.08));
            vsVehicles.ColWidth(ClassCol, (int)(vsVehicles.WidthOriginal * 0.07));
            vsVehicles.ColWidth(PlateCol, (int)(vsVehicles.WidthOriginal * 0.10));
            vsVehicles.ColWidth(VINCol, (int)(vsVehicles.WidthOriginal * 0.20));
			vsVehicles.ColWidth(YearCol, (int)(vsVehicles.WidthOriginal * 0.05));
            vsVehicles.ColWidth(MakeCol, (int)(vsVehicles.WidthOriginal * 0.10));
            vsVehicles.ColWidth(ModelCol, (int)(vsVehicles.WidthOriginal * 0.10));
            vsVehicles.ColWidth(UnitCol, (int)(vsVehicles.WidthOriginal * 0.05));
            vsVehicles.ColWidth(TotalCol, (int)(vsVehicles.WidthOriginal * 0.10));
			if (vsResults.Rows > 1)
			{
				vsResults.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 6, vsResults.Rows - 1, 6, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsResults.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 4, vsResults.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsResults.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 3, vsResults.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			vsResults.TextMatrix(0, 1, "Owner");
			vsResults.TextMatrix(0, 2, "Class");
			vsResults.TextMatrix(0, 3, "Plate");
			vsResults.TextMatrix(0, 4, "Year");
			vsResults.TextMatrix(0, 5, "Make");
			vsResults.TextMatrix(0, 6, "Model");
			vsResults.TextMatrix(0, 7, "Expires");
			vsResults.ColDataType(7, FCGrid.DataTypeSettings.flexDTDate);
			vsResults.ColWidth(0, 0);
			vsResults.ColWidth(1, FCConvert.ToInt32(vsResults.WidthOriginal * 0.344477));
			vsResults.ColWidth(2, FCConvert.ToInt32(vsResults.WidthOriginal * 0.0790603));
			vsResults.ColWidth(3, FCConvert.ToInt32(vsResults.WidthOriginal * 0.1060018));
			vsResults.ColWidth(4, FCConvert.ToInt32(vsResults.WidthOriginal * 0.0790603));
			vsResults.ColWidth(5, FCConvert.ToInt32(vsResults.WidthOriginal * 0.0903546));
			vsResults.ColWidth(6, FCConvert.ToInt32(vsResults.WidthOriginal * 0.1016489));
			vsResults.ColWidth(7, FCConvert.ToInt32(vsResults.WidthOriginal * 0.1198847));
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "STAAB AGENCY" || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
			{
				lblAnnualReg.Visible = true;
				cmbAnnualReg.Visible = true;
			}
			else
			{
				lblAnnualReg.Visible = false;
				cmbAnnualReg.Visible = false;
			}
			if (fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "STAAB AGENCY" || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")
			{
				if (!cmbRegType1.Items.Contains("Process CTA"))
				{
					cmbRegType1.Items.Insert(19, "Process CTA");
				}
				if (cmbRegType1.Items.Contains("Process CTA"))
				{
					cmbRegType1.Items.Remove("Process CTA");
				}
			}
			else
			{
				if (cmbRegType1.Items.Contains("Process CTA"))
				{
					cmbRegType1.Items.Remove("Process CTA");
				}
			}
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
		}

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			intPlateIndex = 0;
			bolFormAlreadyLoaded = false;
			SettingValues = false;
			bolFormAlreadyLoaded = false;
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				if (MotorVehicle.Statics.RegisteringFleet)
				{
					e.Cancel = true;
					MessageBox.Show("Select the Completed button at the bottom of the screen to finish this process", "Invalid Option", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void frmPlateInfo_Resize(object sender, System.EventArgs e)
		{
			vsResults.ColWidth(0, 0);
			vsResults.ColWidth(1, FCConvert.ToInt32(vsResults.WidthOriginal * 0.344477));
			vsResults.ColWidth(2, FCConvert.ToInt32(vsResults.WidthOriginal * 0.0790603));
			vsResults.ColWidth(3, FCConvert.ToInt32(vsResults.WidthOriginal * 0.1060018));
			vsResults.ColWidth(4, FCConvert.ToInt32(vsResults.WidthOriginal * 0.0790603));
			vsResults.ColWidth(5, FCConvert.ToInt32(vsResults.WidthOriginal * 0.0903546));
			vsResults.ColWidth(6, FCConvert.ToInt32(vsResults.WidthOriginal * 0.1016489));
			vsResults.ColWidth(7, FCConvert.ToInt32(vsResults.WidthOriginal * 0.1198847));
			if (SecondResize == false)
			{
				SecondResize = true;
				Form_Resize();
			}
			else
			{
				SecondResize = false;
			}

            fraSubClass.CenterToContainer(this.ClientArea);
            fraSelection.CenterToContainer(this.ClientArea);
        }

		public void Form_Resize()
		{
			frmPlateInfo_Resize(this, new System.EventArgs());
		}

		private void mnuBOOSTER_Click(object sender, System.EventArgs e)
		{
			MenuSelection(12, 0);
		}

		private void mnuCORRPlateChange_Click(object sender, System.EventArgs e)
		{
			MenuSelection(6, 1);
		}

		private void mnuCORRwoPlateChange_Click(object sender, System.EventArgs e)
		{
			MenuSelection(6, 0);
		}

		private void mnuDUP_Click(object sender, System.EventArgs e)
		{
			MenuSelection(7, 0);
		}

		private void mnuDUPSTK_Click(object sender, System.EventArgs e)
		{
			MenuSelection(9, 0);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.Statics.RegisteringFleet = false;
			Close();
		}

		private void mnuGVW_Click(object sender, System.EventArgs e)
		{
			MenuSelection(10, 0);
		}

		private void mnuHELD_Click(object sender, System.EventArgs e)
		{
			MenuSelection(5, 0);
		}

		private void mnuLOSTLost_Click()
		{
			MenuSelection(8, 0);
		}

		private void mnuLOSTNewPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(8, 0);
		}

		private void mnuLOSTOldPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(8, 1);
		}

		private void mnuNRRNewPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(3, 0);
		}

		private void mnuNRROldPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(3, 1);
		}

		private void mnuNRRReplacementPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(3, 2);
		}

		private void mnuNRTNewClass_Click(object sender, System.EventArgs e)
		{
			MenuSelection(4, 1);
		}

		private void mnuNRTNewLost_Click(object sender, System.EventArgs e)
		{
			MenuSelection(4, 2);
		}

		private void mnuNRTOldPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(4, 3);
		}

		private void mnuNRTTransferPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(4, 0);
		}

		private void mnuREDBOOK_Click(object sender, System.EventArgs e)
		{
			MenuSelection(16, 99);
			cmdProcess_Click();
		}

		private void mnuRRRNew_Click(object sender, System.EventArgs e)
		{
			MenuSelection(1, 1);
		}

		private void mnuRRROld_Click(object sender, System.EventArgs e)
		{
			MenuSelection(1, 2);
		}

		private void mnuRRRSame_Click(object sender, System.EventArgs e)
		{
			MenuSelection(1, 0);
		}

		private void mnuRRTNewPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(2, 2);
		}

		private void mnuRRTOldPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(2, 3);
		}

		private void mnuRRTReRegPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(2, 1);
		}

		private void mnuRRTTransferPlate_Click(object sender, System.EventArgs e)
		{
			MenuSelection(2, 0);
		}

		private void mnuSearch_Click(object sender, System.EventArgs e)
		{
			int ii;
			fraSelection.CenterToContainer(this.ClientArea);
			txtCriteria.Text = "";
			cmbSearchBy.Text = "Name";
			vsResults.Rows = 1;
			fraSelection.Visible = true;
			Label3.Text = "Enter The Search Criteria" + "\r\n" + "(Press Enter To Search)";
			fraResults.Visible = false;
			EnableDisableForm(false);
			txtCriteria.Focus();
		}

		public void mnuSearch_Click()
		{
			mnuSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void mnuSPECIAL_Click(object sender, System.EventArgs e)
		{
			MenuSelection(13, 99);
		}

		private void mnuTRANSIT_Click(object sender, System.EventArgs e)
		{
			MenuSelection(11, 99);
		}

		private void mnuUPDATE_Click(object sender, System.EventArgs e)
		{
			MenuSelection(15, 0);
		}

		private void mnuVOID_Click(object sender, System.EventArgs e)
		{
			MenuSelection(14, 99);
		}

		private void optAnnual_CheckedChanged(object sender, System.EventArgs e)
		{
			vsVehicles.Visible = false;
			FillFleetCombo();
		}

		private void optAnnualReg_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 3; counter++)
			{
				if (cmbPlate.SelectedIndex == counter)
				{
					optPlate_Click(counter);
					break;
				}
			}
			if (cmbRegType1.Items.Contains("Process CTA"))
			{
				cmbRegType1.Items.Remove("Process CTA");
			}
		}

		private void optFleet_CheckedChanged(object sender, System.EventArgs e)
		{
			vsVehicles.Visible = false;
			FillFleetCombo();
		}

		private void optGroup_CheckedChanged(object sender, System.EventArgs e)
		{
			vsVehicles.Visible = false;
			FillFleetCombo();
		}

		private void optLongTerm_CheckedChanged(object sender, System.EventArgs e)
		{
			vsVehicles.Visible = false;
			FillFleetCombo();
		}

		private void optLongTermReg_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 3; counter++)
			{
				if (cmbPlate.SelectedIndex == counter)
				{
					optPlate_Click(counter);
					break;
				}
			}
			if (!cmbRegType1.Items.Contains("Process CTA"))
			{
				cmbRegType1.Items.Insert(19, "Process CTA");
			}
		}

		public void optPlate_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			DialogResult ans = 0;
			if (SettingValues)
				return;
			SettingValues = true;
			MotorVehicle.Statics.PlateType = Index + 1;

			if (MotorVehicle.Statics.RegistrationType == "RRR")
			{
				if (Index == 0)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter Plate Number from Previous Registration", "", "", true, false, "This is the plate currently on the vehicle you are Re-Registering", "", "");
					MotorVehicle.Statics.LostPlate = false;
				}
				else if (Index == 1)
				{
					MotorVehicle.Statics.NewPlate = true;
					SetObjectsForPlate("Enter New Plate / Decal Number", "Enter Plate Number from Previous Registration", "", true, true, "If Excise Tax Only and Plate not Known -- Enter \"NEW\" for Plate Number", "This is the plate currently on the vehicle you are Re-Registering", "", cmbAnnualReg.Text == "Long Term" && cmbAnnualReg.Visible);
					ans = MessageBox.Show("Is this a lost, stolen, or mutilated plate?", "Plate Charge?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						MotorVehicle.Statics.LostPlate = true;
					}
					else
					{
						MotorVehicle.Statics.LostPlate = false;
					}
				}
				else if (Index == 2)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter Plate Number from the plate you will be using", "Enter Plate Number from Previous Registration", "", true, true, "If excise tax only and plate not known -- Enter \"NEW\" for plate number", "This is the plate currently on the vehicle you are Re-Registering", "");
					MotorVehicle.Statics.LostPlate = false;
				}
			}
			else if (MotorVehicle.Statics.RegistrationType == "RRT")
			{
				if (Index == 0)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter the Plate Number From the Vehicle the Transfer is taken from", "Enter Plate Number from Previous Registration", "", true, true, "This is the plate number from the vehicle you are NOT retaining", "This is the plate currently on the vehicle you are Re-Registering", "");
					MotorVehicle.Statics.LostPlate = false;
				}
				else if (Index == 1)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter Plate from Previous Registration", "Enter Plate Number from the Vehicle the Transfer is taken from", "", true, true, "This is the Plate currently on the Vehicle you are Re-Registering", "This is the plate from the Vehicle not retained", "");
					MotorVehicle.Statics.LostPlate = false;
				}
				else if (Index == 2)
				{
					MotorVehicle.Statics.NewPlate = true;
					SetObjectsForPlate("Enter New Plate / Decal Number", "Enter Plate Number from Previous Registration", "Enter  the Plate Number from the vehicle the transfer is Taken from", true, true, "If Excise Tax Only and Plate not known -- Enter \"NEW\" for Plate Number", "This is the plate currently on the vehicle you are Re-Registering", "This is the plate from the vehicle not retained", cmbAnnualReg.Text == "Long Term" && cmbAnnualReg.Visible);
					ans = MessageBox.Show("Is this a lost, stolen, or mutilated plate?", "Plate Charge?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						MotorVehicle.Statics.LostPlate = true;
					}
					else
					{
						MotorVehicle.Statics.LostPlate = false;
					}
				}
				else if (Index == 3)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter Old Plate / Decal Number", "Enter Plate Number from Previous Registration", "Enter the Plate Number from the vehicle the transfer is Taken from", true, true, "If Excise Tax Only and Plate not known -- Enter \"NEW\" for Plate Number", "This is the plate currently on the vehicle you are Re-Registering", "This is the plate from the Vehicle not retained");
					MotorVehicle.Statics.LostPlate = false;
				}
			}
			else if (MotorVehicle.Statics.RegistrationType == "NRR")
			{
				if (Index == 0)
				{
					MotorVehicle.Statics.NewPlate = true;
					SetObjectsForPlate("Enter the new Plate / Decal Number", "", "", true, false, "If Excise Tax Only and Plate not Known -- Enter \"NEW\" for Plate Number", "", "", cmbAnnualReg.Text == "Long Term" && cmbAnnualReg.Visible);
					MotorVehicle.Statics.LostPlate = false;
				}
				else if (Index == 1)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter the Old Plate Number", "", "", true, false, "This is the plate supplied by the customer", "", "");
					MotorVehicle.Statics.LostPlate = false;
				}
				else if (Index == 2)
				{
					MotorVehicle.Statics.NewPlate = true;
					SetObjectsForPlate("Enter New Plate Number", "Enter number from the plate you are Replacing", "", true, true, "This is the Replacement Plate being issued", "This is the plate the customer has chosen not to use", "", cmbAnnualReg.Text == "Long Term" && cmbAnnualReg.Visible);
					MotorVehicle.Statics.LostPlate = false;
					ans = MessageBox.Show("Should the replacement plate fee be charged?", "Plate Charge?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						MotorVehicle.Statics.NoReplacementFee = false;
					}
					else
					{
						MotorVehicle.Statics.NoReplacementFee = true;
					}
				}
			}
			else if (MotorVehicle.Statics.RegistrationType == "NRT")
			{
				if (Index == 0)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter the plate the transfer is taken From", "", "", true, false, "This is the plate from the vehicle not Retained", "", "");
					MotorVehicle.Statics.LostPlate = false;
				}
				else if (Index == 1)
				{
					MotorVehicle.Statics.NewPlate = true;
					SetObjectsForPlate("Enter the new Plate / Decal Number", "Enter Plate Number from the vehicle the transfer is taken from", "", true, true, "If Excise Tax Only and Plate not Known -- Enter \"NEW\" for Plate Number", "This is the plate from the Vehicle not retained", "", cmbAnnualReg.Text == "Long Term" && cmbAnnualReg.Visible);
					ans = MessageBox.Show("Is this a lost, stolen, or mutilated plate?", "Plate Charge?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						MotorVehicle.Statics.LostPlate = true;
					}
					else
					{
						MotorVehicle.Statics.LostPlate = false;
					}
				}
				else if (Index == 2)
				{
					MotorVehicle.Statics.NewPlate = true;
					SetObjectsForPlate("Enter the new Plate / Decal Number", "Enter Plate Number from the vehicle the transfer is taken from", "", true, true, "If Excise Tax Only and Plate not Known -- Enter \"NEW\" for Plate Number", "This is the  plate the Transfer is being taken from", "", cmbAnnualReg.Text == "Long Term" && cmbAnnualReg.Visible);
					ans = MessageBox.Show("Is this a lost, stolen, or mutilated plate?", "Plate Charge?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						MotorVehicle.Statics.LostPlate = true;
					}
					else
					{
						MotorVehicle.Statics.LostPlate = false;
					}
				}
				else if (Index == 3)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter the Old Plate Number", "Enter the Plate Number from the vehicle the transfer is taken from", "", true, true, "If Excise Tax Only and Plate not known -- Enter \"NEW\" for Plate Number", "This is the plate the transfer is being taken from", "");
					MotorVehicle.Statics.LostPlate = false;
				}
			}
			else if (MotorVehicle.Statics.RegistrationType == "CORR")
			{
				if (Index == 0)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter Plate Number from Previous Registration", "", "", true, false, "Enter Plate Number from Previous Registration", "This is the plate from the registration that needs correction", "");
					MotorVehicle.Statics.LostPlate = false;
				}
				else if (Index == 1)
				{
					MotorVehicle.Statics.NewPlate = true;
					SetObjectsForPlate("Enter the New Plate / Decal Number", "Enter the Old Plate Number", "", true, true, "If Excise Tax Only and plate not known -- Enter \"NEW\" for Plate Number", "This is the plate that is being replaced", "", cmbAnnualReg.Text == "Long Term" && cmbAnnualReg.Visible);
					ans = MessageBox.Show("Is this a lost, stolen, or mutilated plate?", "Plate Charge?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						MotorVehicle.Statics.LostPlate = true;
					}
					else
					{
						MotorVehicle.Statics.LostPlate = false;
					}
				}
			}
			else if (MotorVehicle.Statics.RegistrationType == "BOOST")
			{
				if (Index == 0 || Index == 2)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter Current Plate Number", "", "", true, false, "This is the plate from the Current registration", "", "");
					MotorVehicle.Statics.LostPlate = false;
				}
				else
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter the New Plate", "Enter the Old Plate Number", "", true, true, "This is the vehicle the booster is getting transferred to", "This is the plate that the booster is coming from", "", cmbAnnualReg.Text == "Long Term" && cmbAnnualReg.Visible);
					MotorVehicle.Statics.LostPlate = false;
				}
			}
			else if (MotorVehicle.Statics.RegistrationType == "HELD")
			{
				MotorVehicle.Statics.NewPlate = false;
				SetObjectsForPlate("Enter Plate Number from Held Registration", "", "", true, false, "This is the plate from the Held registration", "", "");
				MotorVehicle.Statics.LostPlate = false;
			}
			else if (MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "DUPSTK" || MotorVehicle.Statics.RegistrationType == "GVW" || MotorVehicle.Statics.RegistrationType == "UPD" || MotorVehicle.Statics.RegistrationType == "CTA")
			{
				MotorVehicle.Statics.NewPlate = false;
				SetObjectsForPlate("Enter Current Plate Number", "", "", true, false, "This is the plate from the Current registration", "", "");
				MotorVehicle.Statics.LostPlate = false;
			}
			else if (MotorVehicle.Statics.RegistrationType == "LOST")
			{
				if (Index == 0)
				{
					SetObjectsForPlate("Enter New Plate Number", "Enter Number from Lost Plate", "", true, true, "This is a New Plate from inventory", "", "", cmbAnnualReg.Text == "Long Term" && cmbAnnualReg.Visible);
					MotorVehicle.Statics.NewPlate = true;
				}
				else if (Index == 1)
				{
					MotorVehicle.Statics.NewPlate = false;
					SetObjectsForPlate("Enter Old Plate Number", "Enter Number from Lost Plate", "", true, true, "This is the plate the customer has with them", "", "");
				}
			}
			// 
			cmdSearch.Enabled = true;
			if (txtPlateType[0].Enabled == true && txtPlateType[0].Visible == true)
			{
				txtPlateType[0].Focus();
			}
			else if (cboLongTermRegPlateYear.Visible == true)
			{
				cboLongTermRegPlateYear.Focus();
			}
			SettingValues = false;
		}

		public void optPlate_Click(int Index)
		{
			optPlate_CheckedChanged(Index, cmbPlate, new System.EventArgs());
		}

		private void optPlate_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbPlate.SelectedIndex;
			optPlate_CheckedChanged(index, sender, e);
		}

		private void optRegType_CheckedChanged(object sender, System.EventArgs e)
		{
			int fnx;
			if (OKFlag)
			{
				if (cmbRegType1.Text == "Register Fleet / Group")
				{
					cmbPlate.Visible = false;
					lblPlate.Visible = false;
					lblRegType.Text = "";
					for (fnx = 0; fnx <= 2; fnx++)
					{
						txtPlateType[fnx].Visible = false;
						txtPlateType[fnx].Text = "";
						lblPlateType[fnx].Visible = false;
					}
					cboClass.Visible = false;
					cboClass2.Visible = false;
					cboLongTermRegPlateYear.Visible = false;
					if (cmbRegType1.Text == "Register Fleet / Group")
					{
						if (cmbAnnualReg.Visible == true && cmbAnnualReg.Text == "Long Term")
						{
							Close();
							frmLongTermReRegBatch.InstancePtr.Show(App.MainForm);
						}
						else
						{
							FillFleetCombo();
							if (MotorVehicle.Statics.RegisteringFleet)
							{
								if (MotorVehicle.Statics.GroupOrFleet == "G")
								{
									cmbGroup.Text = "Group";
								}
								else
								{
									cmbGroup.Text = "Fleet";
								}
								blnEditFlag = true;
								cboFleets.SelectedIndex = MotorVehicle.Statics.RememberedIndex;
								blnEditFlag = false;
							}
							else
							{
								MotorVehicle.Statics.curFleetExcise = 0;
								MotorVehicle.Statics.curFleetStatePaid = 0;
								MotorVehicle.Statics.curFleetAgentFee = 0;
								MotorVehicle.Statics.curFleetSalesTax = 0;
								MotorVehicle.Statics.curFleetTitleFee = 0;
								MotorVehicle.Statics.strFleetNameAndNumber = "";
								vsVehicles.Visible = false;
							}
							if (Information.IsDate(MotorVehicle.Statics.PreEffectiveDate) && MotorVehicle.Statics.PreEffectiveDate != DateTime.MinValue)
							{
								txtEffectiveDate.Text = Strings.Format(MotorVehicle.Statics.PreEffectiveDate, "MM/dd/yyyy");
							}
							else
							{
								txtEffectiveDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
							}
							if (MotorVehicle.Statics.PendingRegistration)
							{
								cmbPreReg.Text = "Pre Reg";
							}
							else
							{
								cmbPreReg.Text = "Regular";
							}
							fraFleetRegistration.Visible = true;
							fraFleetRegistration.BringToFront();
							cmdProcess.Enabled = false;
						}
					}
					else
					{
						fraFleetRegistration.Visible = false;
						cmdProcess.Enabled = true;
						cmdProcess_Click();
					}
					return;
				}
				SettingValues = true;
				MotorVehicle.Statics.NewPlate = false;
				GetReadyToSetValues();
				SettingValues = false;
			}
			else
			{
				cmbPlate.Visible = false;
				lblPlate.Visible = false;
				lblRegType.Text = "";
				for (fnx = 0; fnx <= 2; fnx++)
				{
					txtPlateType[fnx].Visible = false;
					txtPlateType[fnx].Text = "";
					lblPlateType[fnx].Visible = false;
				}
				cboClass.Visible = false;
				cboClass2.Visible = false;
				cboLongTermRegPlateYear.Visible = false;
				cmdProcess.Enabled = true;
			}
		}

		public void optRegType_Click(string text)
		{
			cmbRegType1.Text = text;
			optRegType_CheckedChanged(cmbRegType1, new System.EventArgs());
		}

		public void LoadClass()
		{
            clsDRWrapper rsClass = new clsDRWrapper();

            try
            {
                MotorVehicle.Statics.strSql = "SELECT DISTINCT BMVCode FROM Class";
                rsClass.OpenRecordset(MotorVehicle.Statics.strSql);
                if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
                {
                    rsClass.MoveLast();
                    rsClass.MoveFirst();
                    while (!rsClass.EndOfFile())
                    {
                        cboClass.AddItem(rsClass.Get_Fields_String("BMVCode"));
                        cboClass2.AddItem(rsClass.Get_Fields_String("BMVCode"));
                        rsClass.MoveNext();
                    }
                    cboClass.Text = "";
                    cboClass2.Text = "";
                }
			}
            finally
            {
				rsClass.Dispose();
            }
			
		}

		public void LoadYears()
		{
			clsDRWrapper rsLongTermYear = new clsDRWrapper();

            try
            {
                MotorVehicle.Statics.strSql =
                    "SELECT DISTINCT left(Number, 2) as LongYear FROM Inventory WHERE Code = 'PXSLT' AND Status = 'A' ORDER BY left(Number, 2)";
                rsLongTermYear.OpenRecordset(MotorVehicle.Statics.strSql);
                if (rsLongTermYear.EndOfFile() != true && rsLongTermYear.BeginningOfFile() != true)
                {
                    rsLongTermYear.MoveLast();
                    rsLongTermYear.MoveFirst();
                    while (!rsLongTermYear.EndOfFile())
                    {
                        if (Conversion.Val("20" + rsLongTermYear.Get_Fields("LongYear")) > DateTime.Now.Year)
                        {
                            cboLongTermRegPlateYear.AddItem("20" + rsLongTermYear.Get_Fields("LongYear"));
                        }

                        rsLongTermYear.MoveNext();
                    }

                    cboLongTermRegPlateYear.Text = "";
                }
            }
            finally
            {
				rsLongTermYear.Dispose();
            }
        }

		private void optSearchBy_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 4)
			{
				lblVehicle.Text = "Please Enter Year Make Model in the following format   YYYYMMMMDDDDDD";
				lblVehicle.Visible = true;
			}
			else if (Index == 1)
			{
				lblVehicle.Text = "Please enter the Plate Number only";
				lblVehicle.Visible = true;
			}
			else if (Index == 5 || Index == 6)
			{
				lblVehicle.Text = "Please enter the Sticker Number only";
				lblVehicle.Visible = true;
			}
			else
			{
				lblVehicle.Text = "";
				lblVehicle.Visible = false;
			}
			if (txtCriteria.Visible == true)
			{
				txtCriteria.Focus();
				txtCriteria.SelectionStart = 0;
				txtCriteria.SelectionLength = txtCriteria.Text.Length;
			}
		}

		private void optSearchBy_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSearchBy.SelectedIndex;
			optSearchBy_CheckedChanged(index, sender, e);
		}

		private void Timer1_Tick(object sender, System.EventArgs e)
		{
			if (!MotorVehicle.Statics.FromTransfer)
			{
				Timer1.Enabled = false;
			}
			else
			{
				if (MotorVehicle.JobComplete("frmDataInput"))
				{
					PlateFromSearch = false;
					Timer1.Enabled = false;
					MotorVehicle.RegisterNew();
					cmdProcess_Click();
				}
			}
		}

		private void txtCriteria_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				string strField = "";
				string strSSS = "";
				int counter = 0;
				if (KeyAscii == Keys.Return)
				{
					KeyAscii = (Keys)0;
					if ((!Information.IsNumeric(Strings.Left(txtCriteria.Text, 1)) && (string.Compare(fecherFoundation.Strings.UCase(Strings.Left(txtCriteria.Text, 1)), "A") < 0 || string.Compare(fecherFoundation.Strings.UCase(Strings.Left(txtCriteria.Text, 1)), "Z") > 0)) || (!Information.IsNumeric(Strings.Right(txtCriteria.Text, 1)) && (string.Compare(fecherFoundation.Strings.UCase(Strings.Right(txtCriteria.Text, 1)), "A") < 0 || string.Compare(fecherFoundation.Strings.UCase(Strings.Right(txtCriteria.Text, 1)), "Z") > 0)))
					{
						MessageBox.Show("Invalid Search Criteria", "Invalid Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					fecherFoundation.Information.Err().Clear();
					FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
					frmWait.InstancePtr.lblMessage.Text = "Please Wait" + "\r\n" + "Searching";
					frmWait.InstancePtr.Show();
					frmWait.InstancePtr.Refresh();
					if (cmbSearchBy.Text == "Name")
					{
						strField = "Reg1LastName";
					}
					else if (cmbSearchBy.Text == "Plate")
					{
						strField = "Plate";
					}
					else if (cmbSearchBy.Text == "VIN")
					{
						strField = "VIN";
					}
					else if (cmbSearchBy.Text == "MVR3 / Tax Receipt #")
					{
						strField = "MVR3";
					}
					else if (cmbSearchBy.Text == "Vehicle Search (Year/Make/Model)")
					{
						strField = "Year/Make/Model";
					}
					else if (cmbSearchBy.Text == "Year Sticker")
					{
						strField = "YearStickerNumber";
					}
					else if (cmbSearchBy.Text == "Month Sticker")
					{
						strField = "MonthStickerNumber";
					}
					else
					{
						FCGlobal.Screen.MousePointer = 0;
						frmWait.InstancePtr.Unload();
						MessageBox.Show("You must choose a field to search", "Choose A Field", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (fecherFoundation.Strings.Trim(txtCriteria.Text) == "")
					{
						FCGlobal.Screen.MousePointer = 0;
						frmWait.InstancePtr.Unload();
						MessageBox.Show("You must enter a search criteria", "Choose A Field", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}

                    var results = PerformRegistraitonSearch();

					if (results.Count > 0)
                    { 
                        foreach (var result in results)
                        {
						    vsResults.AddItem(result.Id + "\t" + result.Name + "\t" + result.Class + "\\" + result.Subclass + "\t" + result.Plate + "\t" + result.Year  + "\t" + result.Make + "\t" + result.Model + "\t" + result.ExpireDate);
					    }
					    
					    fraResults.Visible = true;
					    fraResults.BringToFront();
					    lblRecordsReturned.Enabled = true;
					    lblRecordsReturned.Visible = true;
					    lblRecordsReturned.Text = "Records Returned = " + results.Count;
					    fraSelection.Visible = false;
					    cmdGetInfo.Enabled = true;
					    cmdReturn.Enabled = true;
					    vsResults.Select(1, 1);
					    vsResults.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					    frmWait.InstancePtr.Unload();
				    }
				    else
				    {
					    frmWait.InstancePtr.Unload();
					    FCGlobal.Screen.MousePointer = 0;
					    MessageBox.Show("There are no matching records", "No Search Results", MessageBoxButtons.OK, MessageBoxIcon.Information);
				    }
					
				}
				FCGlobal.Screen.MousePointer = 0;
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
				return;
			}
			catch (Exception ex)
			{
				FCGlobal.Screen.MousePointer = 0;
				frmWait.InstancePtr.Unload();
                MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "\r\n" + "\r\n" + fecherFoundation.Information.Err(ex).Description);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
		    }
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

        private List<RegistrationSearchResult> PerformRegistraitonSearch()
        {
            var results = new List<RegistrationSearchResult>();
			clsDRWrapper rr1 = new clsDRWrapper();
			string strField = "";

            try
            {
                if (cmbSearchBy.Text == "Name")
                {
                    strField = "Reg1LastName";
                }
                else if (cmbSearchBy.Text == "Plate")
                {
                    strField = "Plate";
                }
                else if (cmbSearchBy.Text == "VIN")
                {
                    strField = "VIN";
                }
                else if (cmbSearchBy.Text == "MVR3 / Tax Receipt #")
                {
                    strField = "MVR3";
                }
                else if (cmbSearchBy.Text == "Vehicle Search (Year/Make/Model)")
                {
                    strField = "Year/Make/Model";
                }
                else if (cmbSearchBy.Text == "Year Sticker")
                {
                    strField = "YearStickerNumber";
                }
                else if (cmbSearchBy.Text == "Month Sticker")
                {
                    strField = "MonthStickerNumber";
                }

                if (MotorVehicle.Statics.RegistrationType == "HELD")
                {
                    if (cmbSearchBy.Text == "Vehicle Search (Year/Make/Model)")
                    {
                        results = BuildList("SELECT c.*, p.FullName FROM HeldRegistrationMaster as c LEFT JOIN " +
                                            rr1.CurrentPrefix +
                                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE (Year = " +
                                            Strings.Mid(txtCriteria.Text, 1, 4) + " AND Make = '" +
                                            Strings.Mid(txtCriteria.Text, 5, 4) + "' AND Model = '" +
                                            Strings.Mid(txtCriteria.Text, 9, 6) + "') AND Status = 'H' ORDER BY Year");
                    }
                    else if (strField == "Reg1LastName")
                    {
                        results = CreateSQLQuery(txtCriteria.Text, "Held");
                    }
                    else if (strField == "YearStickerNumber")
                    {
                        results = BuildList("SELECT c.*, p.FullName FROM HeldRegistrationMaster as c LEFT JOIN " +
                                            rr1.CurrentPrefix +
                                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE YearStickerNumber = " +
                                            txtCriteria.Text + " AND Status = 'H' ORDER BY p.FullName");
                    }
                    else if (strField == "MonthStickerNumber")
                    {
                        results = BuildList("SELECT c.*, p.FullName FROM HeldRegistrationMaster as c LEFT JOIN " +
                                            rr1.CurrentPrefix +
                                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE MonthStickerNumber = " +
                                            txtCriteria.Text + " AND Status = 'H' ORDER BY p.FullName");
                    }
                    else if (strField == "Plate")
                    {
                        results = BuildList("SELECT c.*, p.FullName FROM HeldRegistrationMaster as c LEFT JOIN " +
                                            rr1.CurrentPrefix +
                                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE (Plate LIKE '" +
                                            txtCriteria.Text + "%' OR PlateStripped LIKE '" + txtCriteria.Text +
                                            "%') AND Status = 'H' ORDER BY " + strField);
                    }
                    else
                    {
                        results = BuildList("SELECT c.*, p.FullName FROM HeldRegistrationMaster as c LEFT JOIN " +
                                            rr1.CurrentPrefix +
                                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE " +
                                            strField + " LIKE '" + txtCriteria.Text + "%' AND Status = 'H' ORDER BY " +
                                            strField);
                    }
                }
                else
                {
                    if (cmbSearchBy.Text == "Vehicle Search (Year/Make/Model)")
                    {
                        results = BuildList("SELECT c.*, p.FullName FROM Master as c LEFT JOIN " + rr1.CurrentPrefix +
                                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Year = " +
                                            Strings.Mid(txtCriteria.Text, 1, 4) + " AND Make = '" +
                                            Strings.Mid(txtCriteria.Text, 5, 4) + "' AND Model = '" +
                                            Strings.Mid(txtCriteria.Text, 9, 6) + "' AND Status <> 'T' ORDER BY Year");
                    }
                    else if (strField == "Reg1LastName")
                    {
                        results = CreateSQLQuery(txtCriteria.Text, "Regular");
                    }
                    else if (strField == "YearStickerNumber")
                    {
                        results = BuildList("SELECT c.*, p.FullName FROM Master as c LEFT JOIN " + rr1.CurrentPrefix +
                                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE YearStickerNumber = " +
                                            txtCriteria.Text + " AND Status <> 'T' ORDER BY p.FullName");
                    }
                    else if (strField == "MonthStickerNumber")
                    {
                        results = BuildList("SELECT c.*, p.FullName FROM Master as c LEFT JOIN " + rr1.CurrentPrefix +
                                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE MonthStickerNumber = " +
                                            txtCriteria.Text + " AND Status <> 'T' ORDER BY p.FullName");
                    }
                    else if (strField == "Plate")
                    {
                        results = BuildList("SELECT c.*, p.FullName FROM Master as c LEFT JOIN " + rr1.CurrentPrefix +
                                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE (Plate LIKE '" +
                                            txtCriteria.Text + "%' OR PlateStripped LIKE '" + txtCriteria.Text +
                                            "%') AND Status <> 'T' ORDER BY " + strField);
                    }
                    else
                    {
                        results = BuildList("SELECT c.*, p.FullName FROM Master as c LEFT JOIN " + rr1.CurrentPrefix +
                                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE " +
                                            strField + " LIKE '" + txtCriteria.Text + "%' AND Status <> 'T' ORDER BY " +
                                            strField);
                    }
                }

                return results;
            }
            finally
            {
				rr1.Dispose();
            }
        }

        private List<RegistrationSearchResult> BuildList(string sql)
        {
            var results = new List<RegistrationSearchResult>();

            using (clsDRWrapper rr1 = new clsDRWrapper())
            {
				rr1.OpenRecordset(sql, "MotorVehicle");

                if (rr1.EndOfFile() != true && rr1.BeginningOfFile() != true)
                {
                    rr1.MoveLast();
                    rr1.MoveFirst();
                    while (!rr1.EndOfFile())
                    {
                        results.Add(new RegistrationSearchResult
                        {
                            Id = rr1.Get_Fields_Int32("ID"),
                            Name = MotorVehicle.GetPartyNameMiddleInitial(rr1.Get_Fields_Int32("PartyID1"), true).Trim(),
                            Class = rr1.Get_Fields_String("Class").Trim(),
                            Subclass = rr1.Get_Fields_String("Subclass").Trim(),
                            Plate = rr1.Get_Fields_String("plate").Trim(),
                            Year = rr1.Get_Fields_String("Year").Trim(),
                            Make = rr1.Get_Fields_String("make").Trim(),
                            Model = rr1.Get_Fields_String("model").Trim(),
                            ExpireDate = rr1.Get_Fields_DateTime("ExpireDate") == new DateTime(1899, 12, 30) ? "" : rr1.Get_Fields_DateTime("ExpireDate").ToString("MM/dd/yyyy")
                        });
                        rr1.MoveNext();
                    }
                }
			}
            
			return results;
		}

        private void txtEffectiveDate_Leave(object sender, System.EventArgs e)
		{
			if (Information.IsDate(txtEffectiveDate.Text))
			{
				if (fecherFoundation.DateAndTime.DateValue(txtEffectiveDate.Text).ToOADate() > DateTime.Today.ToOADate())
				{
					if (cmbPreReg.Items.Contains("Regular"))
					{
						cmbPreReg.Text = "Pre Reg";
						cmbPreReg.Items.Remove("Regular");
					}
				}
				else
				{
					if (!cmbPreReg.Items.Contains("Regular"))
					{
						cmbPreReg.Items.Add("Regular");
						cmbPreReg.Text = "Regular";
					}
				}
			}
		}

		private void txtPlateType_Enter(int Index, object sender, System.EventArgs e)
		{
			if (fraResults.Visible == false)
			{
				intPlateIndex = Index;
			}
		}

		private void txtPlateType_Enter(object sender, System.EventArgs e)
		{
			int index = txtPlateType.GetIndex((Global.T2KOverTypeBox)sender);
			txtPlateType_Enter(index, sender, e);
		}

		private void txtPlateType_KeyDownEvent(int Index, object sender, KeyEventArgs e)
		{
			if (txtPlateType[2].Visible == true && txtPlateType[2].Text.Length > 0)
			{
				cmdProcess.Enabled = true;
				return;
			}
			else if (txtPlateType[1].Visible == true && txtPlateType[1].Text.Length > 0)
			{
				cmdProcess.Enabled = true;
				return;
			}
			else if (txtPlateType[0].Visible == true && txtPlateType[0].Text.Length > 0)
			{
				cmdProcess.Enabled = true;
			}
		}

		private void txtPlateType_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int index = txtPlateType.GetIndex((Global.T2KOverTypeBox)sender);
			txtPlateType_KeyDownEvent(index, sender, e);
		}

		public void GetNewPlate(string PL)
		{
			clsDRWrapper rsPL = new clsDRWrapper();
			string SavedOpID = "";
			bool executePlateChecker = false;

            try
            {
                if (PL == "NEW")
                {
                    executePlateChecker = true;
                    goto PlateChecker;
                }

                if (MotorVehicle.Statics.PlateGroup == 0)
                {
                    MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE (FormattedInventory = '" + PL +
                                                  "' OR Prefix + convert(nvarchar, Number) + Suffix = '" + PL +
                                                  "') AND Status = 'A'";
                }
                else
                {
                    MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE (FormattedInventory = '" + PL +
                                                  "' OR Prefix + convert(nvarchar, Number) + Suffix = '" + PL +
                                                  "') AND Status = 'A' AND ([Group] = " +
                                                  FCConvert.ToString(MotorVehicle.Statics.PlateGroup) +
                                                  " OR [Group] = 0)";
                }

                rsPL.OpenRecordset(MotorVehicle.Statics.strSql);
                if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
                {
                    rsPL.MoveLast();
                    rsPL.MoveFirst();
                    if (rsPL.RecordCount() == 1)
                    {
                        MotorVehicle.Statics.Class = Strings.Mid(FCConvert.ToString(rsPL.Get_Fields("Code")), 4, 2);
                        if (MotorVehicle.Statics.Class == "LT")
                        {
                            MotorVehicle.Statics.Class = "TL";
                            MotorVehicle.Statics.Subclass = "L9";
                        }
                        else
                        {
                            MotorVehicle.Statics.Subclass = "!!";
                        }

                        MotorVehicle.Statics.strCodeP = FCConvert.ToString(rsPL.Get_Fields("Code"));
                        if (Information.IsNumeric(MotorVehicle.Statics.Class))
                        {
                            if (Strings.Mid(FCConvert.ToString(rsPL.Get_Fields("Code")), 1, 1) == "S")
                            {
                                StickerType = "S";
                            }
                            else
                            {
                                StickerType = "D";
                            }
                        }
                    }
                    else if (rsPL.RecordCount() > 1)
                    {
                        frmListBox.InstancePtr.lstSelectPlate.Clear();
                        frmListBox.InstancePtr.lstSelectPlate.Columns.Add("");
                        frmListBox.InstancePtr.lstSelectPlate.Columns[1].Resizable = false;
                        int listWidth = frmListBox.InstancePtr.lstSelectPlate.Width;
                        frmListBox.InstancePtr.lstSelectPlate.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.45);
                        frmListBox.InstancePtr.lstSelectPlate.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.5);
                        frmListBox.InstancePtr.lstSelectPlate.GridLineStyle = GridLineStyle.None;
                        while (!rsPL.EndOfFile())
                        {
                            frmListBox.InstancePtr.lstSelectPlate.AddItem(
                                rsPL.Get_Fields_Int32("ID") +
                                Strings.Space(10 - FCConvert.ToString(rsPL.Get_Fields_Int32("ID")).Length) +
                                (rsPL.Get_Fields_String("FormattedInventory") + "\t" +
                                 Strings.Mid(FCConvert.ToString(rsPL.Get_Fields("Code")), 4, 2)));
                            rsPL.MoveNext();
                        }

                        frmListBox.InstancePtr.lstSelectPlate.AddItem("None of the Above");
                        frmListBox.InstancePtr.lblHeader.Text =
                            "More than one plate was found in inventory. Please select the correct one";
                        frmListBox.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                        if (MotorVehicle.Statics.ReturnFromListBox == "None of the Above")
                        {
                            executePlateChecker = true;
                            goto PlateChecker;
                        }

                        MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE ID = " +
                                                      FCConvert.ToString(FCConvert.ToInt32(
                                                          FCConvert.ToDouble(
                                                              Strings.Mid(MotorVehicle.Statics.ReturnFromListBox, 1,
                                                                  9))));
                        rsPL.OpenRecordset(MotorVehicle.Statics.strSql);
                        if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
                        {
                            rsPL.MoveLast();
                            rsPL.MoveFirst();
                            // this record is the one selected form a list
                            MotorVehicle.Statics.Class = Strings.Mid(FCConvert.ToString(rsPL.Get_Fields("Code")), 4, 2);
                            if (MotorVehicle.Statics.Class == "LT")
                            {
                                MotorVehicle.Statics.Class = "TL";
                                MotorVehicle.Statics.Subclass = "L9";
                            }
                            else
                            {
                                MotorVehicle.Statics.Subclass = "!!";
                            }

                            MotorVehicle.Statics.strCodeP = FCConvert.ToString(rsPL.Get_Fields("Code"));
                            if (Information.IsNumeric(MotorVehicle.Statics.Class))
                            {
                                if (Strings.Mid(FCConvert.ToString(rsPL.Get_Fields("Code")), 1, 1) == "S")
                                {
                                    StickerType = "S";
                                }
                                else
                                {
                                    StickerType = "D";
                                }
                            }
                        }
                    }

                    MotorVehicle.Statics.ForcedPlate = "N";
                    MotorVehicle.Statics.Plate1 = FCConvert.ToString(rsPL.Get_Fields_String("FormattedInventory"));
					ReturnNewRecordWithPlate(0, MotorVehicle.Statics.Plate1);
				}
                else
                {
                    executePlateChecker = true;
                    goto PlateChecker;
                }

                PlateChecker: ;
                if (executePlateChecker)
                {
                    if (PL != "NEW")
                    {
                        MotorVehicle.Statics.ForcedPlate = "N";
                        //FC:FINAL:MSH - i.issue #1766: initialize listview columns for displaying data as in original app (WiseJ doesn't display spaces and tabs)
                        frmListBox.InstancePtr.lstSelectPlate.Clear();
                        frmListBox.InstancePtr.lstSelectPlate.Columns.Add("");
                        frmListBox.InstancePtr.lstSelectPlate.Columns[1].Resizable = false;
                        int listWidth = frmListBox.InstancePtr.lstSelectPlate.Width;
                        frmListBox.InstancePtr.lstSelectPlate.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.2);
                        frmListBox.InstancePtr.lstSelectPlate.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.75);
                        frmListBox.InstancePtr.lstSelectPlate.GridLineStyle = GridLineStyle.None;
                        frmListBox.InstancePtr.lstSelectPlate.AddItem(
                            "I" + "\t" + "Issue Plate as Coded (Agent Code Required)");
                        frmListBox.InstancePtr.lstSelectPlate.AddItem(
                            "S" + "\t" + "Indicate this is a Special Request Plate");
                        frmListBox.InstancePtr.lstSelectPlate.AddItem("P" + "\t" + "Indicate Prepaid Fees");
                        frmListBox.InstancePtr.lstSelectPlate.AddItem(
                            "T" + "\t" + "Temporary Plate Being Issued, Reserve Now");
                        frmListBox.InstancePtr.lblHeader.Text =
                            "The Plate specified was not found in inventory. How would you like this Plate coded?";
                        frmListBox.InstancePtr.lstSelectPlate.SelectedIndex = 0;
                        frmListBox.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                        if (MotorVehicle.Statics.ReturnFromListBox == "")
                        {
                            ReturnNotFound(0);
							return;
                        }
                        else
                        {
                            if (Strings.Mid(MotorVehicle.Statics.ReturnFromListBox, 1, 1) == "I")
                            {
                                SavedOpID = MotorVehicle.Statics.OpID;
                                frmOpID.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                                if (MotorVehicle.Statics.OpID != "")
                                {
                                    MotorVehicle.Statics.OpID = SavedOpID;
                                }
                                else
                                {
                                    MotorVehicle.Statics.OpID = SavedOpID;
									ReturnNotFound(0);
									return;
								}
                            }

                            MotorVehicle.Statics.Class = "";
                            MotorVehicle.Statics.Subclass = "";
                            ReturnNewRecordWithPlate(0, MotorVehicle.Statics.Plate1);
                            MotorVehicle.Statics.ForcedPlate =
                                Strings.Mid(MotorVehicle.Statics.ReturnFromListBox, 1, 1);
                        }
                    }
                    else
                    {
                        MotorVehicle.Statics.Class = "";
                        MotorVehicle.Statics.Subclass = "";
                        MotorVehicle.Statics.ChickConversion = "N";
						ReturnNewRecordWithPlate(0, "NEW");
					}

                    executePlateChecker = false;
                }
            }
            finally
            {
				rsPL.Dispose();
            }
        }

		public void GetNewLongTermPlate(string PL)
		{
			MotorVehicle.Statics.Class = "TL";
            MotorVehicle.Statics.Class1 = "TL";
            MotorVehicle.Statics.Subclass = "L9";
            MotorVehicle.Statics.strCodeP = "PXSLT";
            StickerType = "S";
            MotorVehicle.Statics.ForcedPlate = "N";
            MotorVehicle.Statics.Plate1 = PL;
			ReturnNewRecordWithPlate(0, MotorVehicle.Statics.Plate1);
        }

		public void GetExistingPlate(int index)
		{
            string PL = txtPlateType[index].Text.Trim();
            int masterId = 0;

			DialogResult ans = 0;
			clsDRWrapper rsPL = new clsDRWrapper();

            try
            {
                if (MotorVehicle.Statics.InfoForCorrect || MotorVehicle.Statics.blnTryCorrectAgain)
                {
                    if (PL == MotorVehicle.Statics.FirstCorrectPlate)
                    {
                        if (MotorVehicle.Statics.FirstCorrectID > 0)
                        {
                            MotorVehicle.Statics.strSql =
                                "SELECT * FROM Master WHERE ID = " +
                                FCConvert.ToString(MotorVehicle.Statics.FirstCorrectID) + " AND (Plate = '" + PL +
                                "' OR PlateStripped = '" + PL + "') ORDER BY DateUpdated DESC";
                        }
                        else
                        {
                            MotorVehicle.Statics.strSql =
                                "SELECT * FROM Master WHERE Plate = '" + PL + "' OR PlateStripped = '" + PL +
                                "' ORDER BY DateUpdated DESC";
                        }
                    }
                    else
                    {
                        if (MotorVehicle.Statics.SecondCorrectID > 0)
                        {
                            MotorVehicle.Statics.strSql =
                                "SELECT * FROM Master WHERE ID = " +
                                FCConvert.ToString(MotorVehicle.Statics.SecondCorrectID) + " AND (Plate = '" + PL +
                                "' OR PlateStripped = '" + PL + "') ORDER BY DateUpdated DESC";
                        }
                        else
                        {
                            MotorVehicle.Statics.strSql =
                                "SELECT * FROM Master WHERE Plate = '" + PL + "' OR PlateStripped = '" + PL +
                                "' ORDER BY DateUpdated DESC";
                        }
                    }
                }
                else
                {
                    MotorVehicle.Statics.strSql = "SELECT * FROM Master WHERE Plate = '" + PL +
                                                  "' OR PlateStripped = '" + PL + "' ORDER BY DateUpdated DESC";
                }

                rsPL.OpenRecordset(MotorVehicle.Statics.strSql);
                bool executeNonTag = false;
                if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
                {
                    rsPL.MoveLast();
                    rsPL.MoveFirst();
                    if (rsPL.RecordCount() > 1)
                    {
                        frmMultiplePlates.InstancePtr.vsVehicles.Rows = 1;
                        while (!rsPL.EndOfFile())
                        {
                            if (!(rsPL.IsFieldNull("ID")))
                            {
                                frmMultiplePlates.InstancePtr.vsVehicles.AddItem("");
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 0,
                                    FCConvert.ToString(rsPL.Get_Fields_Int32("ID")));
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 1,
                                    FCConvert.ToString(rsPL.Get_Fields_String("Class")));
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 2,
                                    fecherFoundation.Strings.Trim(
                                        MotorVehicle.GetPartyNameMiddleInitial(rsPL.Get_Fields_Int32("PartyID1"),
                                            true)));
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 3,
                                    FCConvert.ToString(rsPL.Get_Fields_String("make")));
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 4,
                                    FCConvert.ToString(rsPL.Get_Fields_String("model")));
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 5,
                                    Strings.Format(rsPL.Get_Fields_DateTime("ExpireDate"), "MM/dd/yy"));
                            }

                            rsPL.MoveNext();
                        }

                        frmMultiplePlates.InstancePtr.vsVehicles.AddItem("");
                        frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                            frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 0, "0");
                        frmMultiplePlates.InstancePtr.vsVehicles.AddItem("");
                        frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                            frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 0, "0");
                        frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                            frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 2, "None of the Above");
                        frmMultiplePlates.InstancePtr.lblHeader.Text =
                            "Multiple instances of the Plate are in the file. Please select the correct one";
                        frmMultiplePlates.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                        if (MotorVehicle.Statics.RegisteringFleet)
                        {
                            GoingToDataInput = true;
                        }

                        if (Conversion.Val(MotorVehicle.Statics.ReturnFromListBox) == 0)
                        {
                            rsPL.OpenRecordset("SELECT * FROM Master WHERE ID = -1");
                            executeNonTag = true;
                            goto NoneTag;
                        }

                        MotorVehicle.Statics.strSql = "SELECT * FROM Master WHERE ID = " +
                                                      FCConvert.ToString(
                                                          Conversion.Val(MotorVehicle.Statics.ReturnFromListBox));
                        rsPL.OpenRecordset(MotorVehicle.Statics.strSql);
                        if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
                        {
                            rsPL.MoveLast();
                            rsPL.MoveFirst();
                            masterId = rsPL.Get_Fields_Int32("Id");
							MotorVehicle.Statics.Class = FCConvert.ToString(rsPL.Get_Fields_String("Class"));
                            MotorVehicle.Statics.Subclass = FCConvert.ToString(rsPL.Get_Fields_String("Subclass"));
                            ReturnExistingRecord(index, masterId);
                        }
                    }
                    else if (rsPL.RecordCount() == 1)
                    {
                        MotorVehicle.Statics.Class = FCConvert.ToString(rsPL.Get_Fields_String("Class"));
                        MotorVehicle.Statics.Subclass = FCConvert.ToString(rsPL.Get_Fields_String("Subclass"));
                        masterId = rsPL.Get_Fields_Int32("Id");
                        ReturnExistingRecord(index, masterId);
					}

                    if (rsPL.Get_Fields_Boolean("Inactive") == true)
                    {
                        ans = MessageBox.Show(
                            "This vehicle has been registered by another person.  Do you wish to continue with this registration?",
                            "Continue Registration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (ans == DialogResult.Yes)
                        {
                            modGlobalFunctions.AddCYAEntry_6("MV",
                                "Registering vehicle that has already been registered by another owner",
                                "Plate: " + rsPL.Get_Fields_String("plate"));
                        }
                        else
                        {
							ReturnNotFound(index);
							return;
                        }
                    }

                    if (MotorVehicle.Statics.RegistrationType != "UPD" &&
                        MotorVehicle.Statics.RegistrationType != "BOOST")
                    {
                        MotorVehicle.TransferRecordToArchive(ref rsPL, intPlateIndex);
                    }
                }
                else
                {
                    executeNonTag = true;
                    goto NoneTag;
                }

                NoneTag: ;
                if (executeNonTag)
                {
                    MotorVehicle.Statics.Class = "";
                    MotorVehicle.Statics.Subclass = "";
                    rsPL.AddNew();
                    rsPL.Set_Fields("Plate", PL);
					if ((MotorVehicle.Statics.RegistrationType != "RRT" && index == 0) &&
                        (MotorVehicle.Statics.RegistrationType != "NRT" || MotorVehicle.Statics.PlateType != 4))
                    {
                        MotorVehicle.Statics.NewToTheSystem = true;
                    }

                    ReturnNewRecordWithPlate(index, PL);
					if (MotorVehicle.Statics.RegistrationType == "BOOST")
                    {
                        return;
                    }

                    executeNonTag = false;
                }

                string strPlate = "";
                bool tempFlag = false;
                if (cmbRegType1.Text == "Re-Registration Transfer" &&
                    fecherFoundation.Strings.Trim(rsPL.Get_Fields_String("plate")) == "NEW")
                {
                    if (cmbPlate.SelectedIndex != 0 && cmbPlate.SelectedIndex != 1)
                    {
                        tempFlag = false;
                        MotorVehicle.Statics.ReplacePlate = false;
                    }
                }
                else if (cmbRegType1.Text == "New Registration Transfer" &&
                         fecherFoundation.Strings.Trim(rsPL.Get_Fields_String("plate")) == "NEW")
                {
                    if (cmbPlate.SelectedIndex != 0)
                    {
                        tempFlag = false;
                        MotorVehicle.Statics.ReplacePlate = false;
                    }
                }
                else if (cmbRegType1.Text == "Re-Registration Regular" &&
                         fecherFoundation.Strings.Trim(rsPL.Get_Fields_String("plate")) == "NEW")
                {
                    if (cmbPlate.SelectedIndex == 0)
                    {
                        tempFlag = true;
                        MotorVehicle.Statics.ReplacePlate = true;
                    }
                    else
                    {
                        tempFlag = false;
                        MotorVehicle.Statics.ReplacePlate = false;
                    }
                }
                else
                {
                    tempFlag = false;
                    MotorVehicle.Statics.ReplacePlate = false;
                }

                if (tempFlag)
                {
                    strPlate = Interaction.InputBox("Please enter the new plate number", "Plate Number", "");
                    if (strPlate == "")
                    {
                        ReturnNotFound(index);
						return;
                    }
                    else
                    {
                        int counter;
                        bool checkerflag = false;
                        string tempplate = "";
                        tempplate = "";
                        checkerflag = false;
                        for (counter = 1; counter <= strPlate.Length; counter++)
                        {
                            if (Strings.Mid(strPlate, counter, 1) == " " ||
                                Information.IsNumeric(Strings.Mid(strPlate, counter, 1)))
                            {
                                tempplate += Strings.Mid(strPlate, counter, 1);
                            }
                            else if (Convert.ToByte(
                                         fecherFoundation.Strings.UCase(Strings.Mid(strPlate, counter, 1))[0]) >= 65 &&
                                     Convert.ToByte(
                                         fecherFoundation.Strings.UCase(Strings.Mid(strPlate, counter, 1))[0]) <= 90)
                            {
                                tempplate += fecherFoundation.Strings.UCase(Strings.Mid(strPlate, counter, 1));
                            }
                            else
                            {
                                checkerflag = true;
                            }
                        }

                        if (checkerflag)
                        {
                            MessageBox.Show("There is an illegal character in this plate number",
                                "Illegal Plate Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            ReturnNotFound(index);
							return;
                        }

                        strPlate = tempplate;
                        MotorVehicle.Statics.ReplacementPlateNumber = tempplate;
                        txtPlateType[0].Text = strPlate;
                        MotorVehicle.Statics.Plate1 = strPlate;
                        MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE (FormattedInventory = '" +
                                                      strPlate +
                                                      "' OR Prefix + convert(nvarchar, Number) + Suffix = '" +
                                                      strPlate + "') AND Status = 'A'";
                        using (clsDRWrapper rsInventory = new clsDRWrapper())
                        {


                            rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);
                            if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                            {
                                rsInventory.MoveLast();
                                rsInventory.MoveFirst();
                                if (rsInventory.RecordCount() == 1)
                                {
                                    MotorVehicle.Statics.Class =
                                        Strings.Mid(FCConvert.ToString(rsInventory.Get_Fields("Code")), 4, 2);
                                    MotorVehicle.Statics.Subclass = "!!";
                                    MotorVehicle.Statics.strCodeP = FCConvert.ToString(rsInventory.Get_Fields("Code"));
                                }
                                else if (rsInventory.RecordCount() > 1)
                                {
                                    frmListBox.InstancePtr.lstSelectPlate.Clear();
                                    frmListBox.InstancePtr.lstSelectPlate.Columns.Add("");
                                    frmListBox.InstancePtr.lstSelectPlate.Columns[1].Resizable = false;
                                    int listWidth = frmListBox.InstancePtr.lstSelectPlate.Width;
                                    frmListBox.InstancePtr.lstSelectPlate.Columns[0].Width =
                                        FCConvert.ToInt32(listWidth * 0.35);
                                    frmListBox.InstancePtr.lstSelectPlate.Columns[1].Width =
                                        FCConvert.ToInt32(listWidth * 0.6);
                                    frmListBox.InstancePtr.lstSelectPlate.GridLineStyle = GridLineStyle.None;
                                    while (!rsInventory.EndOfFile())
                                    {
                                        frmListBox.InstancePtr.lstSelectPlate.AddItem(
                                            rsInventory.Get_Fields_String("FormattedInventory") + "\t" +
                                            rsInventory.Get_Fields_String("Class"));
                                        rsInventory.MoveNext();
                                    }

                                    frmListBox.InstancePtr.lstSelectPlate.AddItem("None of the Above");
                                    frmListBox.InstancePtr.lblHeader.Text =
                                        "More than one Plate was found in Inventory. Please select the correct one";
                                    frmListBox.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                                    MotorVehicle.Statics.strSql =
                                        "SELECT * FROM Inventory WHERE ID = " + FCConvert.ToString(
                                            FCConvert.ToInt32(
                                                FCConvert.ToDouble(
                                                    Strings.Mid(MotorVehicle.Statics.ReturnFromListBox, 1, 9))));
                                    rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);
                                    if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                                    {
                                        rsInventory.MoveLast();
                                        rsInventory.MoveFirst();
                                        MotorVehicle.Statics.Class =
                                            FCConvert.ToString(rsInventory.Get_Fields_String("Class"));
                                        MotorVehicle.Statics.Subclass =
                                            FCConvert.ToString(rsInventory.Get_Fields_String("Subclass"));
                                    }
                                }


                                MotorVehicle.Statics.ForcedPlate = "N";
                            }
                            else
                            {
                                if (strPlate != "NEW")
                                {
                                    MotorVehicle.Statics.ForcedPlate = "N";
                                    frmListBox.InstancePtr.lstSelectPlate.Clear();
                                    frmListBox.InstancePtr.lstSelectPlate.Columns.Add("");
                                    frmListBox.InstancePtr.lstSelectPlate.Columns[1].Resizable = false;
                                    int listWidth = frmListBox.InstancePtr.lstSelectPlate.Width;
                                    frmListBox.InstancePtr.lstSelectPlate.Columns[0].Width =
                                        FCConvert.ToInt32(listWidth * 0.2);
                                    frmListBox.InstancePtr.lstSelectPlate.Columns[1].Width =
                                        FCConvert.ToInt32(listWidth * 0.75);
                                    frmListBox.InstancePtr.lstSelectPlate.GridLineStyle = GridLineStyle.None;
                                    frmListBox.InstancePtr.lstSelectPlate.AddItem(
                                        "I" + "\t" + "Issue Plate as Coded (Agent Code Required)");
                                    frmListBox.InstancePtr.lstSelectPlate.AddItem(
                                        "S" + "\t" + "Indicate this is a Special Request Plate");
                                    frmListBox.InstancePtr.lstSelectPlate.AddItem("P" + "\t" + "Indicate Prepaid Fees");
                                    frmListBox.InstancePtr.lstSelectPlate.AddItem(
                                        "T" + "\t" + "Temporary Plate Being Issued, Reserve Now");
                                    frmListBox.InstancePtr.lblHeader.Text =
                                        "The Plate specified was not found in inventory. How would you like this Plate coded?";
                                    frmListBox.InstancePtr.lstSelectPlate.SelectedIndex = 0;
                                    frmListBox.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                                    if (MotorVehicle.Statics.ReturnFromListBox == "")
                                    {
                                        return;
                                    }
                                    else
                                    {
                                        MotorVehicle.Statics.ForcedPlate =
                                            Strings.Mid(MotorVehicle.Statics.ReturnFromListBox, 1, 1);
                                    }
                                }
                                else
                                {
                                    MotorVehicle.Statics.Class = "";
                                    MotorVehicle.Statics.Subclass = "";
                                    MotorVehicle.Statics.ChickConversion = "N";
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                rsPL.Dispose();
			}
        }

        private void ReturnNotFound(int index)
        {
            switch (index)
            {
                case 0:
                    MotorVehicle.Statics.rsPlateForReg.Reset();
                    break;
                case 1:
                    MotorVehicle.Statics.rsSecondPlate.Reset();
                    break;
                case 2:
                    MotorVehicle.Statics.rsThirdPlate.Reset();
                    break;
            }
		}

        private void ReturnNewRecordWithPlate(int index, string plate)
        {
            var sql = "SELECT * FROM Master WHERE ID = 0";
            switch (index)
            {
                case 0:
                    MotorVehicle.Statics.rsPlateForReg.OpenRecordset(sql, "MotorVehicle");
                    MotorVehicle.Statics.rsPlateForReg.AddNew();
                    MotorVehicle.Statics.rsPlateForReg.Set_Fields("Plate", plate);
					break;
                case 1:
                    MotorVehicle.Statics.rsSecondPlate.OpenRecordset(sql, "MotorVehicle");
                    MotorVehicle.Statics.rsSecondPlate.AddNew();
                    MotorVehicle.Statics.rsSecondPlate.Set_Fields("Plate", plate);
					break;
                case 2:
                    MotorVehicle.Statics.rsThirdPlate.OpenRecordset(sql, "MotorVehicle");
                    MotorVehicle.Statics.rsThirdPlate.AddNew();
                    MotorVehicle.Statics.rsThirdPlate.Set_Fields("Plate", plate);
					break;
            }
		}

        private void ReturnExistingRecord(int index, int id)
        {
            var sql = "SELECT * FROM Master WHERE ID = " + id;
            switch (index)
            {
                case 0:
                    MotorVehicle.Statics.rsPlateForReg.OpenRecordset(sql, "MotorVehicle");
                    break;
                case 1:
                    MotorVehicle.Statics.rsSecondPlate.OpenRecordset(sql, "MotorVehicle");
                    break;
                case 2:
                    MotorVehicle.Statics.rsThirdPlate.OpenRecordset(sql, "MotorVehicle");
                    break;
            }
        }

		public void GetHeldPlate(string PL)
		{
			clsDRWrapper GetHeldPlate = null;
			clsDRWrapper rsPL = new clsDRWrapper();
			clsDRWrapper rsUT = new clsDRWrapper();
			clsDRWrapper rsNewPL = new clsDRWrapper();

            try
            {
                MotorVehicle.Statics.strSql = "SELECT * FROM HeldRegistrationMaster WHERE (Plate = '" + PL +
                                              "' OR PlateStripped = '" + PL +
                                              "') AND Status = 'H' ORDER BY DateUpdated DESC";
                rsPL.OpenRecordset(MotorVehicle.Statics.strSql);
                bool executeNoneTag = false;
                if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
                {
                    rsPL.MoveLast();
                    rsPL.MoveFirst();
                    if (rsPL.RecordCount() > 1)
                    {
                        frmMultiplePlates.InstancePtr.vsVehicles.Rows = 1;
                        while (!rsPL.EndOfFile())
                        {
                            if (!(rsPL.IsFieldNull("ID")))
                            {
                                frmMultiplePlates.InstancePtr.vsVehicles.AddItem("");
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 0,
                                    FCConvert.ToString(rsPL.Get_Fields_Int32("ID")));
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 1,
                                    FCConvert.ToString(rsPL.Get_Fields_String("Class")));
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 2,
                                    fecherFoundation.Strings.Trim(
                                        MotorVehicle.GetPartyNameMiddleInitial(rsPL.Get_Fields_Int32("PartyID1"),
                                            true)));
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 3,
                                    FCConvert.ToString(rsPL.Get_Fields_String("make")));
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 4,
                                    FCConvert.ToString(rsPL.Get_Fields_String("model")));
                                frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                                    frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 5,
                                    Strings.Format(rsPL.Get_Fields_DateTime("ExpireDate"), "MM/dd/yy"));
                            }

                            rsPL.MoveNext();
                        }

                        frmMultiplePlates.InstancePtr.vsVehicles.AddItem("");
                        frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                            frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 0, "0");
                        frmMultiplePlates.InstancePtr.vsVehicles.AddItem("");
                        frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                            frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 0, "0");
                        frmMultiplePlates.InstancePtr.vsVehicles.TextMatrix(
                            frmMultiplePlates.InstancePtr.vsVehicles.Rows - 1, 2, "None of the Above");
                        frmMultiplePlates.InstancePtr.lblHeader.Text =
                            "Multiple instances of the Plate are in the file. Please select the correct one";
                        frmMultiplePlates.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                        if (Conversion.Val(MotorVehicle.Statics.ReturnFromListBox) == 0)
                        {
                            executeNoneTag = true;
                            goto NoneTag;
                        }

                        MotorVehicle.Statics.strSql = "SELECT * FROM HeldRegistrationMaster WHERE ID = " +
                                                      FCConvert.ToString(
                                                          Conversion.Val(MotorVehicle.Statics.ReturnFromListBox));
                        rsNewPL.OpenRecordset(MotorVehicle.Statics.strSql);
                        MotorVehicle.Statics.rsPlateForReg.OpenRecordset(MotorVehicle.Statics.strSql);

						if (rsNewPL.EndOfFile() != true && rsNewPL.BeginningOfFile() != true)
                        {
                            rsNewPL.MoveLast();
                            rsNewPL.MoveFirst();
                            MotorVehicle.Statics.Class = FCConvert.ToString(rsNewPL.Get_Fields_String("Class"));
                            MotorVehicle.Statics.Subclass = FCConvert.ToString(rsNewPL.Get_Fields_String("Subclass"));
                            GetHeldPlate = rsNewPL;
                            rsUT.OpenRecordset("SELECT * FROM HeldTitleUseTax WHERE HoldID = " +
                                               rsNewPL.Get_Fields_Int32("ID"));
                            if (rsUT.EndOfFile() != true && rsUT.BeginningOfFile() != true)
                            {
                                // do nothing
                            }
                            else
                            {
                                rsUT.OpenRecordset("SELECT * FROM HeldTitleUseTax WHERE Plate = '" +
                                                   GetHeldPlate.Get_Fields_String("plate") + "'");
                            }

                            if (rsUT.EndOfFile() != true && rsUT.BeginningOfFile() != true)
                            {
                                rsUT.MoveLast();
                                rsUT.MoveFirst();
                                MotorVehicle.Statics.lngUTCAddNewID =
                                    FCConvert.ToInt32(rsUT.Get_Fields_Int32("UseTaxID"));
                                MotorVehicle.Statics.lngCTAAddNewID =
                                    FCConvert.ToInt32(rsUT.Get_Fields_Int32("TitleID"));
                                if (FCConvert.ToInt32(rsUT.Get_Fields_Boolean("PrintTitle")) == 0)
                                {
                                    MotorVehicle.Statics.NeedToPrintCTA = false;
                                }
                                else
                                {
                                    MotorVehicle.Statics.NeedToPrintCTA = true;
                                }

                                if (FCConvert.ToInt32(rsUT.Get_Fields_Boolean("PrintUseTax")) == 0)
                                {
                                    MotorVehicle.Statics.NeedToPrintUseTax = false;
                                }
                                else
                                {
                                    MotorVehicle.Statics.NeedToPrintUseTax = true;
                                }

                                if (FCConvert.ToInt32(rsUT.Get_Fields_Boolean("PrintTitleDouble")) == 0)
                                {
                                    MotorVehicle.Statics.PrintDouble = false;
                                }
                                else
                                {
                                    MotorVehicle.Statics.PrintDouble = true;
                                }
                            }

                            rsUT.OpenRecordset("SELECT * FROM Title title WHERE ID = " +
                                               FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
                            if (rsUT.EndOfFile() != true && rsUT.BeginningOfFile() != true)
                            {
                                if (FCConvert.ToInt32(rsUT.Get_Fields_String("DoubleNumber")) != 0)
                                {
                                    MotorVehicle.Statics.rsDoubleTitle.OpenRecordset(
                                        "SELECT * FROM DoubleCTA WHERE ID = " + rsUT.Get_Fields_String("DoubleNumber"));
                                }
                            }
                        }
                    }
                    else if (rsPL.RecordCount() == 1)
                    {
                        MotorVehicle.Statics.Class = FCConvert.ToString(rsPL.Get_Fields_String("Class"));
                        MotorVehicle.Statics.Subclass = FCConvert.ToString(rsPL.Get_Fields_String("Subclass"));
                        GetHeldPlate = rsPL;
                        rsUT.OpenRecordset("SELECT * FROM HeldTitleUseTax WHERE HoldID = " +
                                           GetHeldPlate.Get_Fields_Int32("ID"));
                        if (rsUT.EndOfFile() != true && rsUT.BeginningOfFile() != true)
                        {
                            // do nothing
                        }
                        else
                        {
                            rsUT.OpenRecordset("SELECT * FROM HeldTitleUseTax WHERE Plate = '" +
                                               GetHeldPlate.Get_Fields_String("plate") + "'");
                        }

                        if (rsUT.EndOfFile() != true && rsUT.BeginningOfFile() != true)
                        {
                            rsUT.MoveLast();
                            rsUT.MoveFirst();
                            MotorVehicle.Statics.lngUTCAddNewID = FCConvert.ToInt32(rsUT.Get_Fields_Int32("UseTaxID"));
                            MotorVehicle.Statics.lngCTAAddNewID = FCConvert.ToInt32(rsUT.Get_Fields_Int32("TitleID"));
                            if (FCConvert.ToInt32(rsUT.Get_Fields_Boolean("PrintTitle")) == 0)
                            {
                                MotorVehicle.Statics.NeedToPrintCTA = false;
                            }
                            else
                            {
                                MotorVehicle.Statics.NeedToPrintCTA = true;
                            }

                            if (FCConvert.ToInt32(rsUT.Get_Fields_Boolean("PrintUseTax")) == 0)
                            {
                                MotorVehicle.Statics.NeedToPrintUseTax = false;
                            }
                            else
                            {
                                MotorVehicle.Statics.NeedToPrintUseTax = true;
                            }

                            if (FCConvert.ToInt32(rsUT.Get_Fields_Boolean("PrintTitleDouble")) == 0)
                            {
                                MotorVehicle.Statics.PrintDouble = false;
                            }
                            else
                            {
                                MotorVehicle.Statics.PrintDouble = true;
                            }
                        }

                        rsUT.OpenRecordset("SELECT * FROM Title title WHERE ID = " +
                                           FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
                        if (rsUT.EndOfFile() != true && rsUT.BeginningOfFile() != true)
                        {
                            if (FCConvert.ToInt32(rsUT.Get_Fields_String("DoubleNumber")) != 0)
                            {
                                MotorVehicle.Statics.rsDoubleTitle.OpenRecordset(
                                    "SELECT * FROM DoubleCTA WHERE ID = " + rsUT.Get_Fields_String("DoubleNumber"));
                            }
                        }
                    }
                }
                else
                {
                    executeNoneTag = true;
                    goto NoneTag;
                }

                NoneTag: ;
                if (executeNoneTag)
                {
                    MotorVehicle.Statics.Class = "";
                    MotorVehicle.Statics.Subclass = "";
                    MotorVehicle.Statics.rsPlateForReg.OpenRecordset(
                        "SELECT * FROM HeldRegistrationMaster WHERE Id = 0", "MotorVehicle");
                    MotorVehicle.Statics.rsPlateForReg.AddNew();
                    MotorVehicle.Statics.NewToTheSystem = true;
                    executeNoneTag = false;
                    return;
                }

                if (GetHeldPlate.RecordCount() > 0)
                {
                    MotorVehicle.Statics.rsPlateForReg.OpenRecordset(
                        "SELECT * FROM HeldRegistrationMaster WHERE Id = " + GetHeldPlate.Get_Fields_Int32("Id"), "MotorVehicle");
				}
            }
            finally
            {
                GetHeldPlate.Dispose();
                rsPL.Dispose();
                rsUT.Dispose();
                rsNewPL.Dispose();
			}
        }

		public void ClearScreen(bool bolClearOPTs)
		{
			int fnx;
			if (bolClearOPTs == true)
			{
                bool disableEvents = cmbRegType1.DisableEvents;
                cmbRegType1.DisableEvents = true;
                cmbRegType1.Text = "";
                cmbRegType1.DisableEvents = disableEvents;
			}
			for (fnx = 0; fnx <= 2; fnx++)
			{
				lblPlateType[fnx].Visible = false;
				txtPlateType[fnx].Visible = false;
				txtPlateType[fnx].Text = "";
			}
			lblRegType.Text = "";
			chkExciseTaxPaid.Visible = false;
			cmbPlate.Visible = false;
			lblPlate.Visible = false;
			cboClass.Visible = false;
			cboClass2.Visible = false;
			cboLongTermRegPlateYear.Visible = false;
			if (txtPlateType[0].Text != "")
				cmdProcess.Enabled = false;
		}

		private void EnableDisableForm(bool bol1)
		{
			int counter;
			foreach (Control obj in this.GetAllControls())
			{
				if (obj.Name == fraResults.Name)
				{
					// Do Nothing
				}
				else if (obj.Name == fraSelection.Name)
				{
					// Do Nothing
				}
				else if (obj.Name == "cmbSearchBy")
				{
					// Do Nothing
				}
				else if (obj.Name == vsResults.Name)
				{
					// Do Nothing
				}
				else if (obj.Name == txtCriteria.Name)
				{
					// Do Nothing
				}
				else if (obj.Name == cmdReturn.Name)
				{
					// Do Nothing
				}
				else if (obj.Name == cmdGetInfo.Name)
				{
					// Do Nothing
				}
				else if (obj.Name == lblVehicle.Name)
				{
					// Do Nothing
				}
				else if (obj.Name == Label3.Name)
				{
					// Do Nothing
				}
				else if (obj.Name == mnuProcess.Name)
				{
					mnuProcess.Enabled = bol1;
				}
				else if (obj.Name == cmdSearch.Name)
				{
					cmdSearch.Enabled = bol1;
				}
				else if (obj is FCLine)
				{
					// Do Nothing
				}
				else if (obj is FCToolStripMenuItem)
				{
					// Do Nothing
				}
                //FC:FINAL:#2232 - check for ClientArea too
                else if (obj.Name == "ClientArea")
                {
                    // Do Nothing
                }
                else
				{
					obj.Enabled = bol1;
				}
			}
			if (modGlobalConstants.Statics.gboolCR && !MotorVehicle.Statics.bolFromWindowsCR)
			{
				for (counter = cmbRegType1.Items.Count - 1; counter >= 0; counter--)
				{
					if (cmbRegType1.Items[counter].ToString() != "Re-Registration Regular" && cmbRegType1.Items[counter].ToString() != "Re-Registration Transfer"
						&& cmbRegType1.Items[counter].ToString() != "New Registration Regular" && cmbRegType1.Items[counter].ToString() != "New Registration Transfer"
						&& cmbRegType1.Items[counter].ToString() != "New To System / Update" 
						&& cmbRegType1.Items[counter].ToString() != "Register Fleet / Group")
					{
						cmbRegType1.Items.RemoveAt(counter);
					}
				}
			}
		}

		private void SetPlateFromSearch()
		{
			int YY;
			string strCheck = "";
			DialogResult ans = 0;
			SettingValues = true;
			PlateFromSearch = true;
			if (intPlateIndex == 0)
			{
				strCheck = vsResults.TextMatrix(vsResults.Row, 0);
				MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(Conversion.Val(strCheck)));
				MotorVehicle.Statics.rsPlateForReg.MoveLast();
				MotorVehicle.Statics.rsPlateForReg.MoveFirst();
				if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("Inactive") == true)
				{
					ans = MessageBox.Show("This vehicle has been registered by another person.  Do you wish to continue with this registration?", "Continue Registration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_6("MV", "Registering vehicle that has already been registered by another owner", "Plate: " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
					}
					else
					{
						MotorVehicle.Statics.rsPlateForReg.Reset();
						PlateFromSearch = false;
						cmdProcess.Enabled = false;
						return;
					}
				}
				if (MotorVehicle.Statics.RegistrationType != "UPD")
				{
					if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Status")) == "T")
					{
						MessageBox.Show("You may not perform this process on a terminated registration", "Terminated Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						MotorVehicle.Statics.rsPlateForReg.Reset();
						PlateFromSearch = false;
						cmdProcess.Enabled = false;
						return;
					}
				}
				if (MotorVehicle.Statics.RegistrationType == "DUPREG")
				{
					if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("ETO") == true)
					{
						MessageBox.Show("You may not perform a Duplicate Registration or Duplicate Sticker process on an Excise Tax Only registration", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						MotorVehicle.Statics.rsPlateForReg.Reset();
						PlateFromSearch = false;
						cmdProcess.Enabled = false;
						return;
					}
					else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class") == "TX")
					{
						MessageBox.Show("You may not perform a Duplicate Registration process on a TX class vehicle", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						MotorVehicle.Statics.rsPlateForReg.Reset();
						PlateFromSearch = false;
						cmdProcess.Enabled = false;
						return;
					}
				}
				if (MotorVehicle.Statics.RegistrationType == "DUPSTK" || MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT" || MotorVehicle.Statics.RegistrationType == "BOOST")
				{
					if ((MotorVehicle.Statics.RegistrationType == "RRT" && MotorVehicle.Statics.PlateType != 1) || (MotorVehicle.Statics.RegistrationType == "NRT" && MotorVehicle.Statics.PlateType == 4))
					{
						// do nothing
					}
					else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate").ToOADate() < DateTime.Today.ToOADate() && (MotorVehicle.Statics.RegistrationType != "CORR" || MotorVehicle.Statics.PlateType != 2))
					{
						MessageBox.Show("You may not perform this process on this registration because it has expired", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						MotorVehicle.Statics.rsPlateForReg.Reset();
						PlateFromSearch = false;
						cmdProcess.Enabled = false;
						return;
					}
					else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("EffectiveDate").ToOADate() > DateTime.Today.ToOADate() && Information.IsDate(MotorVehicle.Statics.rsPlateForReg.Get_Fields("EffectiveDate")))
					{
						if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
						{
							MessageBox.Show("You may not perform this process on this vehicle because it has already been pre-registered.  If you wish to perform this transfer you must void the pre-registration and then run the transfer.  The registrant will need to apply to the town and the state to get a refund of the registration fees", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							MotorVehicle.Statics.rsPlateForReg.Reset();
							PlateFromSearch = false;
							cmdProcess.Enabled = false;
							return;
						}
					}
				}
				if (MotorVehicle.Statics.RegistrationType == "BOOST")
				{
					if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "CC" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "CO" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "TT" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "AP" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "AC" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "AF" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "LC")
					{
						// do nothing
					}
					else
					{
						MessageBox.Show("You may only boost a vehicle with the class CC, CO, TT, AP, AC, AF, LC or FM", "Invalid Class", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						MotorVehicle.Statics.rsPlateForReg.Reset();
						PlateFromSearch = false;
						cmdProcess.Enabled = false;
						return;
					}
				}
				MotorVehicle.Statics.Class = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
				MotorVehicle.Statics.Subclass = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Subclass"));
				cboClass.Visible = true;
				cboClass.Text = MotorVehicle.Statics.Class;
				cmdProcess.Enabled = true;
				txtPlateType[intPlateIndex].Visible = true;
				txtPlateType[intPlateIndex].Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate")));
				MotorVehicle.Statics.Plate1 = fecherFoundation.Strings.Trim(txtPlateType[intPlateIndex].Text);
				string strPlate = "";
				bool tempFlag = false;
				if (cmbRegType1.Text == "Re-Registration Transfer" && fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"))) == "NEW")
				{
					if (cmbPlate.SelectedIndex != 0 && cmbPlate.SelectedIndex != 1)
					{
						tempFlag = false;
						MotorVehicle.Statics.ReplacePlate = false;
					}
				}
				else if (cmbRegType1.Text == "New Registration Transfer" && fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"))) == "NEW")
				{
					if (cmbPlate.SelectedIndex != 0)
					{
						tempFlag = false;
						MotorVehicle.Statics.ReplacePlate = false;
					}
				}
				else if (cmbRegType1.Text == "Re-Registration Regular" && fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"))) == "NEW")
				{
					if (cmbPlate.SelectedIndex == 0)
					{
						tempFlag = true;
						MotorVehicle.Statics.ReplacePlate = true;
					}
					else
					{
						tempFlag = false;
						MotorVehicle.Statics.ReplacePlate = false;
					}
				}
				else if (cmbRegType1.Text == "New Registration Regular" && fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"))) == "NEW")
				{
					if (cmbPlate.SelectedIndex == 0)
					{
						tempFlag = true;
						MotorVehicle.Statics.ReplacePlate = true;
					}
					else
					{
						tempFlag = false;
						MotorVehicle.Statics.ReplacePlate = false;
					}
				}
				else
				{
					tempFlag = false;
					MotorVehicle.Statics.ReplacePlate = false;
				}
				if (tempFlag)
				{
					strPlate = Interaction.InputBox("Please enter the new plate number", "Plate Number", "");
					if (strPlate == "")
					{
						MessageBox.Show("This is an invalid plate", "Illegal Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtPlateType[0].Text = "";
						cboClass.Text = "";
						cboClass.Visible = false;
						MotorVehicle.Statics.Plate1 = "";
						MotorVehicle.Statics.rsPlateForReg.Reset();
						return;
					}
					else
					{
						int counter;
						bool checkerflag = false;
						string tempplate = "";
						tempplate = "";
						checkerflag = false;
						for (counter = 1; counter <= strPlate.Length; counter++)
						{
							if (Strings.Mid(strPlate, counter, 1) == " " || Information.IsNumeric(Strings.Mid(strPlate, counter, 1)))
							{
								tempplate += Strings.Mid(strPlate, counter, 1);
							}
							else if (Convert.ToByte(fecherFoundation.Strings.UCase(Strings.Mid(strPlate, counter, 1))[0]) >= 65 && Convert.ToByte(fecherFoundation.Strings.UCase(Strings.Mid(strPlate, counter, 1))[0]) <= 90)
							{
								tempplate += fecherFoundation.Strings.UCase(Strings.Mid(strPlate, counter, 1));
							}
							else
							{
								checkerflag = true;
							}
						}
						if (checkerflag)
						{
							MessageBox.Show("There is an illegal character in this plate number", "Illegal Plate Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							MotorVehicle.Statics.rsPlateForReg.Reset();
							txtPlateType[0].Text = "";
							cboClass.Text = "";
							cboClass.Visible = false;
							MotorVehicle.Statics.Plate1 = "";
							return;
						}
						strPlate = tempplate;
						MotorVehicle.Statics.ReplacementPlateNumber = tempplate;
						txtPlateType[0].Text = strPlate;
						MotorVehicle.Statics.Plate1 = strPlate;

                        using (clsDRWrapper rsPL = new clsDRWrapper())
                        {
							MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE (FormattedInventory = '" + strPlate + "' OR Prefix + convert(nvarchar, Number) + Suffix = '" + strPlate + "') AND Status = 'A'";
							rsPL.OpenRecordset(MotorVehicle.Statics.strSql);
							if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
							{
								rsPL.MoveLast();
								rsPL.MoveFirst();
								if (rsPL.RecordCount() == 1)
								{
									MotorVehicle.Statics.Class = Strings.Mid(FCConvert.ToString(rsPL.Get_Fields("Code")), 4, 2);
									MotorVehicle.Statics.Subclass = "!!";
									MotorVehicle.Statics.strCodeP = FCConvert.ToString(rsPL.Get_Fields("Code"));
								}
								else if (rsPL.RecordCount() > 1)
								{
									frmListBox.InstancePtr.lstSelectPlate.Clear();
									frmListBox.InstancePtr.lstSelectPlate.Columns.Add("");
									frmListBox.InstancePtr.lstSelectPlate.Columns[1].Resizable = false;
									int listWidth = frmListBox.InstancePtr.lstSelectPlate.Width;
									frmListBox.InstancePtr.lstSelectPlate.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.35);
									frmListBox.InstancePtr.lstSelectPlate.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.6);
									frmListBox.InstancePtr.lstSelectPlate.GridLineStyle = GridLineStyle.None;
									while (!rsPL.EndOfFile())
									{
										frmListBox.InstancePtr.lstSelectPlate.AddItem(rsPL.Get_Fields_String("FormattedInventory") + "\t" + rsPL.Get_Fields_String("Class"));
										rsPL.MoveNext();
									}
									frmListBox.InstancePtr.lstSelectPlate.AddItem("None of the Above");
									frmListBox.InstancePtr.lblHeader.Text = "More than one Plate was found in Inventory. Please select the correct one";
									frmListBox.InstancePtr.Show(FCForm.FormShowEnum.Modal);
									MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE ID = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(Strings.Mid(MotorVehicle.Statics.ReturnFromListBox, 1, 9))));
									rsPL.OpenRecordset(MotorVehicle.Statics.strSql);
									if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
									{
										rsPL.MoveLast();
										rsPL.MoveFirst();
										MotorVehicle.Statics.Class = FCConvert.ToString(rsPL.Get_Fields_String("Class"));
										MotorVehicle.Statics.Subclass = FCConvert.ToString(rsPL.Get_Fields_String("Subclass"));
									}
								}
								MotorVehicle.Statics.ForcedPlate = "N";
							}
							else
							{
								if (strPlate != "NEW")
								{
									MotorVehicle.Statics.ForcedPlate = "N";
									frmListBox.InstancePtr.lstSelectPlate.Clear();
									frmListBox.InstancePtr.lstSelectPlate.Columns.Add("");
									frmListBox.InstancePtr.lstSelectPlate.Columns[1].Resizable = false;
									int listWidth = frmListBox.InstancePtr.lstSelectPlate.Width;
									frmListBox.InstancePtr.lstSelectPlate.Columns[0].Width = FCConvert.ToInt32(listWidth * 0.2);
									frmListBox.InstancePtr.lstSelectPlate.Columns[1].Width = FCConvert.ToInt32(listWidth * 0.75);
									frmListBox.InstancePtr.lstSelectPlate.GridLineStyle = GridLineStyle.None;
									frmListBox.InstancePtr.lstSelectPlate.AddItem("I" + "\t" + "Issue Plate as Coded (Agent Code Required)");
									frmListBox.InstancePtr.lstSelectPlate.AddItem("S" + "\t" + "Indicate this is a Special Request Plate");
									frmListBox.InstancePtr.lstSelectPlate.AddItem("P" + "\t" + "Indicate Prepaid Fees");
									frmListBox.InstancePtr.lstSelectPlate.AddItem("T" + "\t" + "Temporary Plate Being Issued, Reserve Now");
									frmListBox.InstancePtr.lblHeader.Text = "The Plate specified was not found in inventory. How would you like this Plate coded?";
									frmListBox.InstancePtr.lstSelectPlate.SelectedIndex = 0;
									frmListBox.InstancePtr.Show(FCForm.FormShowEnum.Modal);
									if (MotorVehicle.Statics.ReturnFromListBox == "")
									{
										return;
									}
									else
									{
										MotorVehicle.Statics.ForcedPlate = Strings.Mid(MotorVehicle.Statics.ReturnFromListBox, 1, 1);
									}
								}
								else
								{
									MotorVehicle.Statics.Class = "";
									MotorVehicle.Statics.Subclass = "";
									MotorVehicle.Statics.ChickConversion = "N";
								}
							}
						}
                    }
				}
			}
			else if (intPlateIndex == 1)
			{
				strCheck = vsResults.TextMatrix(vsResults.Row, 0);
				MotorVehicle.Statics.rsSecondPlate.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(Conversion.Val(strCheck)));
				MotorVehicle.Statics.rsSecondPlate.MoveLast();
				MotorVehicle.Statics.rsSecondPlate.MoveFirst();
				if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_Boolean("Inactive") == true)
				{
					ans = MessageBox.Show("This vehicle has been registered by another person.  Do you wish to continue with this registration?", "Continue Registration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_6("MV", "Registering vehicle that has already been registered by another owner", "Plate: " + MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("plate"));
					}
					else
					{
						MotorVehicle.Statics.rsSecondPlate.Reset();
						PlateFromSearch = false;
						cmdProcess.Enabled = false;
						return;
					}
				}
				if (MotorVehicle.Statics.RegistrationType == "DUPSTK" || MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
				{
					if (MotorVehicle.Statics.RegistrationType == "RRT" && MotorVehicle.Statics.PlateType != 2)
					{
						// do nothing
					}
					else if (MotorVehicle.Statics.RegistrationType == "RRT" && (MotorVehicle.Statics.PlateType == 3 || MotorVehicle.Statics.PlateType == 4))
					{
						// do nothing
					}
					else if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate").ToOADate() < DateTime.Now.ToOADate())
					{
						MessageBox.Show("You may not perform this process on this registration because it has expired", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						MotorVehicle.Statics.rsSecondPlate.Reset();
						PlateFromSearch = false;
						cmdProcess.Enabled = false;
						return;
					}
					else if (!MotorVehicle.Statics.rsSecondPlate.IsFieldNull("EffectiveDate") && MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("EffectiveDate").ToOADate() > DateTime.Now.ToOADate())
					{
						if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
						{
							MessageBox.Show("You may not perform this process on this vehicle because it has already been pre-registered.  If you wish to perform this transfer you must void the pre-registration and then run the transfer.  The registrant will need to apply to the town and the state to get a refund of the registration fees", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							MotorVehicle.Statics.rsSecondPlate.Reset();
							PlateFromSearch = false;
							cmdProcess.Enabled = false;
							return;
						}
					}
				}
				cboClass2.Visible = true;
				cboClass2.Text = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class"));
				cmdProcess.Enabled = true;
				txtPlateType[intPlateIndex].Visible = true;
				txtPlateType[intPlateIndex].Text = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("plate"));
				MotorVehicle.Statics.Plate2 = fecherFoundation.Strings.Trim(txtPlateType[intPlateIndex].Text);
			}
			else
			{
				strCheck = vsResults.TextMatrix(vsResults.Row, 0);
				MotorVehicle.Statics.rsThirdPlate.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(Conversion.Val(strCheck)));
				MotorVehicle.Statics.rsThirdPlate.MoveLast();
				MotorVehicle.Statics.rsThirdPlate.MoveFirst();
				txtPlateType[intPlateIndex].Visible = true;
				txtPlateType[intPlateIndex].Text = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("plate")));
				MotorVehicle.Statics.Plate3 = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("plate")));
				MotorVehicle.Statics.Class3 = FCConvert.ToString(MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("Class"));
				if (MotorVehicle.Statics.rsThirdPlate.Get_Fields_Boolean("Inactive") == true)
				{
					ans = MessageBox.Show("This vehicle has been registered by another person.  Do you wish to continue with this registration?", "Continue Registration?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_6("MV", "Registering vehicle that has already been registered by another owner", "Plate: " + MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("plate"));
					}
					else
					{
						MotorVehicle.Statics.rsThirdPlate.Reset();
						txtPlateType[2].Text = "";
						cmdProcess.Enabled = false;
						txtPlateType[2].Focus();
					}
				}
				if (MotorVehicle.Statics.RegistrationType == "RRT")
				{
					if (MotorVehicle.Statics.PlateType == 3 || MotorVehicle.Statics.PlateType == 4)
					{
						if (MotorVehicle.Statics.rsThirdPlate.Get_Fields_DateTime("ExpireDate").ToOADate() < DateTime.Now.ToOADate())
						{
							MessageBox.Show("You may not perform this process on this registration because it has expired", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							MotorVehicle.Statics.rsThirdPlate.Reset();
							txtPlateType[2].Text = "";
							cmdProcess.Enabled = false;
							txtPlateType[2].Focus();
						}
						else if ((!MotorVehicle.Statics.rsThirdPlate.IsFieldNull("EffectiveDate"))  && MotorVehicle.Statics.rsThirdPlate.Get_Fields_DateTime("EffectiveDate").ToOADate() > DateTime.Now.ToOADate())
						{
							if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
							{
								MessageBox.Show("You may not perform this process on this vehicle because it has already been pre-registered.  If you wish to perform this transfer you must void the pre-registration and then run the transfer.  The registrant will need to apply to the town and the state to get a refund of the registration fees", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.rsThirdPlate.Reset();
								txtPlateType[2].Text = "";
								cmdProcess.Enabled = false;
								txtPlateType[2].Focus();
							}
						}
					}
				}
			}
			if (cmdProcess.Enabled == true)
			{
				if (txtPlateType[1].Visible == true)
				{
					if (MotorVehicle.Statics.rsSecondPlate.IsntAnything())
					{
						txtPlateType[1].Focus();
					}
					else
					{
						cmdProcess.Focus();
					}
				}
			}
		}

		public void MenuSelection(int RegTypeIndex, int PlateIndex)
		{
			OKFlag = true;
			cmbRegType1.SelectedIndex = RegTypeIndex;
			if (PlateIndex != 99)
				cmbPlate.SelectedIndex = PlateIndex;
		}

		private void GetReadyToSetValues()
		{
			int fnx;
			ClearScreen(false);
			for (fnx = 0; fnx <= 2; fnx++)
			{
				txtPlateType[fnx].Visible = false;
				txtPlateType[fnx].Text = "";
				lblPlateType[fnx].Visible = false;
			}
			cboClass.Visible = false;
			cboClass2.Visible = false;
			cboLongTermRegPlateYear.Visible = false;
			if (txtPlateType[0].Text != "")
				cmdProcess.Enabled = false;
			if (cmbRegType1.Text == "Re-Registration Regular")
			{
				MotorVehicle.Statics.RegistrationType = "RRR";
				MotorVehicle.Statics.RegType = "R";
				SetValues("Re-Registration Regular", "Same Plate", "New Plate", "Old Plate", "", true, true, true, false, false, false, false, false, false, false);
				NON_SpecialRegistration();
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "Re-Registration Transfer")
			{
				MotorVehicle.Statics.RegistrationType = "RRT";
				MotorVehicle.Statics.RegType = "R";
				SetValues("Re-Registration Transfer", "Plate from Transfer Vehicle", "Plate from Re-Reg Vehicle", "New Plate", "Old Plate", true, true, true, true, false, false, false, false, false, false);
				NON_SpecialRegistration();
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "New Registration Regular")
			{
				MotorVehicle.Statics.RegistrationType = "NRR";
				MotorVehicle.Statics.RegType = "Y";
				SetValues("New Registration Regular", "New Plate", "Old Plate", "Replacement Plate", "", true, true, true, false, false, false, false, false, false, false);
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "New Registration Transfer")
			{
				MotorVehicle.Statics.RegistrationType = "NRT";
				MotorVehicle.Statics.RegType = "Y";
				SetValues("New Registration Transfer", "Plate from Transfer Vehicle", "New Plate - Class Change", "New Plate - Other", "Old Plate", true, true, true, true, false, false, false, false, false, false);
				NON_SpecialRegistration();
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "Complete Held Registration")
			{
				SettingValues = false;
				MotorVehicle.Statics.RegistrationType = "HELD";
				SetValues("Held Registration", "Held Plate", "New Plate", "Old Plate", "", true, false, false, false, false, false, false, false, false, false);
				SpecialRegType();
				SettingValues = true;
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "Correction")
			{
				MotorVehicle.Statics.RegistrationType = "CORR";
				SetValues("Correction", "without Plate Change", "with Plate Change", "Old Plate", "", true, true, false, false, false, false, false, false, false, false);
				NON_SpecialRegistration();
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "Duplicate Registration")
			{
				SettingValues = false;
				MotorVehicle.Statics.RegistrationType = "DUPREG";
				SetValues("Duplicate Registration", "Current Plate", "New Plate", "Old Plate", "", true, false, false, false, false, false, false, false, false, false);
				SpecialRegType();
				SettingValues = true;
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "Lost Plate / Stickers")
			{
				SettingValues = false;
				MotorVehicle.Statics.RegistrationType = "LOST";
				SetValues("Lost Plate / Stickers", "New Plate", "Old Plate", "", "", true, true, false, false, false, false, false, false, false, false);
				NON_SpecialRegistration();
				SettingValues = true;
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "Duplicate Stickers")
			{
				SettingValues = false;
				MotorVehicle.Statics.RegistrationType = "DUPSTK";
				SetValues("Duplicate Stickers", "Current Plate", "New Plate", "Old Plate", "", true, false, false, false, false, false, false, false, false, false);
				SpecialRegType();
				SettingValues = true;
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "RVW Change")
			{
				SettingValues = false;
				MotorVehicle.Statics.RegistrationType = "GVW";
				SetValues("RVW Change", "Current Plate", "New Plate", "Old Plate", "", true, false, false, false, false, false, false, false, false, false);
				SpecialRegType();
				SettingValues = true;
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "Transit Plate")
			{
				MotorVehicle.Statics.RegistrationType = "TRANSIT";
				SetValues("Transit Plate", "Same Plate", "New Plate", "Old Plate", "", false, false, false, false, false, false, false, false, false, false);
				NON_SpecialRegistration();
				cmdProcess.Enabled = true;
			}
			else if (cmbRegType1.Text == "Booster Permit")
			{
				SettingValues = false;
				MotorVehicle.Statics.RegistrationType = "BOOST";
				SetValues("Booster Permit", "Current Plate", "Transfer to New Plate", "Duplicate Booster", "", true, true, true, false, false, false, false, false, false, false);
				SpecialRegType();
				SettingValues = true;
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "Special Reg Permit")
			{
				SettingValues = false;
				MotorVehicle.Statics.RegistrationType = "SPREG";
				SetValues("Special Registration", "Registration", "Transfer", "Duplicate", "", true, true, true, false, false, false, false, false, false, false);
				NON_SpecialRegistration();
				SpecialRegType();
				SettingValues = true;
				cmdProcess.Enabled = true;
			}
			else if (cmbRegType1.Text == "Void MVR")
			{
				MotorVehicle.Statics.RegistrationType = "VOID";
				SetValues("", "Same Plate", "New Plate", "Old Plate", "", false, false, false, false, false, false, false, false, false, false);
				NON_SpecialRegistration();
				cmdProcess.Enabled = true;
			}
			else if (cmbRegType1.Text == "New To System / Update")
			{
				SettingValues = false;
				MotorVehicle.Statics.RegistrationType = "UPD";
				SetValues("New To System / Update", "Current Plate", "New Plate", "Old Plate", "", true, false, false, false, false, false, false, false, false, false);
				SpecialRegType();
				SettingValues = true;
				cmdProcess.Enabled = false;
			}
			else if (cmbRegType1.Text == "Void Permit")
			{
				MotorVehicle.Statics.RegistrationType = "VOIDBOOST";
				SetValues("", "Same Plate", "New Plate", "Old Plate", "", false, false, false, false, false, false, false, false, false, false);
				NON_SpecialRegistration();
				cmdProcess.Enabled = true;
			}
			else if (cmbRegType1.Text == "Process CTA")
			{
				SettingValues = false;
				MotorVehicle.Statics.RegistrationType = "CTA";
				SetValues("Process CTA", "CTA", "Duplicate CTA", "Old Plate", "", true, true, false, false, false, false, false, false, false, false);
				SpecialRegType();
				SettingValues = true;
				cmdProcess.Enabled = false;
			}
			if (cmbRegType1.Text == "Transit Plate")
			{
				// do nothing
			}
			else
			{
				cmbPlate.Visible = true;
				lblPlate.Visible = true;
			}
			txtPlateType[0].Text = "";
			txtPlateType[1].Text = "";
			txtPlateType[2].Text = "";
			cboClass.Text = "";
			cboClass2.Text = "";
			cboLongTermRegPlateYear.Text = "";
			if (MotorVehicle.Statics.RegistrationType == "VOID")
			{
				cmbPlate.Visible = false;
				lblPlate.Visible = false;
				modGNBas.Statics.Response = "";
				frmVoidMVR3.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				if (FCConvert.ToString(modGNBas.Statics.Response) == "QUIT")
				{
					cmdStartOver_Click();
				}
				else
				{
					Close();
				}
				modGNBas.Statics.Response = "";
			}
			else if (MotorVehicle.Statics.RegistrationType == "VOIDBOOST")
			{
				cmbPlate.Visible = false;
				lblPlate.Visible = false;
				modGNBas.Statics.Response = "";
				frmVoidBooster.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				if (FCConvert.ToString(modGNBas.Statics.Response) == "QUIT")
				{
					cmdStartOver_Click();
				}
				else
				{
					Close();
				}
				modGNBas.Statics.Response = "";
			}
		}

		public void SetValues(string RegType, string PL0, string PL1, string PL2, string PL3, bool optPlateZero, bool optPlateOne, bool optPlateTwo, bool optPlateThree, bool PlateTypeZeroVisible, bool PlateTypeOneVisible, bool PlateTypeTwoVisible, bool PlateTypeThreeVisible, bool cboClassVisible, bool cboClass2Visible)
		{
			lblRegType.Text = RegType;
			cmbPlate.Items.Clear();
			if (optPlateZero)
			{
				cmbPlate.Items.Add(PL0);
			}
			if (optPlateOne)
			{
				cmbPlate.Items.Add(PL1);
			}
			if (optPlateTwo)
			{
				cmbPlate.Items.Add(PL2);
			}
			if (optPlateThree)
			{
				cmbPlate.Items.Add(PL3);
			}
			txtPlateType[0].Visible = PlateTypeZeroVisible;
			txtPlateType[1].Visible = PlateTypeOneVisible;
			txtPlateType[2].Visible = PlateTypeTwoVisible;
			lblPlateType[0].Visible = PlateTypeZeroVisible;
			lblPlateType[1].Visible = PlateTypeOneVisible;
			lblPlateType[2].Visible = PlateTypeTwoVisible;
			cboClass.Visible = cboClassVisible;
			cboClass2.Visible = cboClass2Visible;
		}

		private void SpecialRegType()
		{
			cmbPlate.SelectedIndex = 0;
		}

		public void NON_SpecialRegistration()
		{
			txtPlateType[0].Visible = false;
			txtPlateType[1].Visible = false;
			txtPlateType[2].Visible = false;
			lblPlateType[0].Visible = false;
			lblPlateType[1].Visible = false;
			lblPlateType[2].Visible = false;
			cboClass.Visible = false;
			cboClass2.Visible = false;
			cboLongTermRegPlateYear.Visible = false;
		}

		private void txtPlateType_KeyPressEvent(int Index, object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii >= 65 && KeyAscii <= 90) || (KeyAscii >= 97 && KeyAscii <= 122) || KeyAscii == 8 || KeyAscii == 45 || KeyAscii == 38 || KeyAscii == 32)
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtPlateType_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int index = txtPlateType.GetIndex((Global.T2KOverTypeBox)sender);
			txtPlateType_KeyPressEvent(index, sender, e);
		}

		private void txtPlateType_Leave(int Index, object sender, System.EventArgs e)
		{
			if (txtPlateType[2].Visible == true && txtPlateType[2].Text.Length > 0)
			{
				cmdProcess.Enabled = true;
				return;
			}
			else if (txtPlateType[1].Visible == true && txtPlateType[1].Text.Length > 0)
			{
				cmdProcess.Enabled = true;
				return;
			}
			else if (txtPlateType[0].Visible == true && txtPlateType[0].Text.Length > 0)
			{
				cmdProcess.Enabled = true;
			}
		}

		private void txtPlateType_Leave(object sender, System.EventArgs e)
		{
			int index = txtPlateType.GetIndex((Global.T2KOverTypeBox)sender);
			txtPlateType_Leave(index, sender, e);
		}

		public void txtPlateType_Validate(int Index, bool x)
		{
			CancelEventArgs e = new CancelEventArgs();
			e.Cancel = x;
			txtPlateType_Validate(Index, null, e);
		}

		public void txtPlateType_Validate(int Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			string holdsubclass = "";
			clsDRWrapper rsBoost = new clsDRWrapper();

            try
            { 
			SettingValues = true;
			DialogResult answer = 0;
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			Control savedActiveControl;
			if (fraResults.Visible)
			{
				return;
			}
			MotorVehicle.Statics.NewToTheSystem = false;
			savedActiveControl = this.ActiveControl;
			switch (Index)
			{
				case 0:
					{
						if ((cmbRegType1.Text == "Re-Registration Regular" && cmbPlate.SelectedIndex == 1) || (cmbRegType1.Text == "Re-Registration Transfer" && cmbPlate.SelectedIndex == 2) || (cmbRegType1.Text == "New Registration Regular" && cmbPlate.SelectedIndex == 0) || (cmbRegType1.Text == "Lost Plate / Stickers" && cmbPlate.SelectedIndex == 0) || (cmbRegType1.Text == "New Registration Transfer" && (cmbPlate.SelectedIndex == 1 || cmbPlate.SelectedIndex == 2)))
						{
							if (fecherFoundation.Strings.Trim(txtPlateType[0].Text) == "NEW")
							{
								MotorVehicle.Statics.Plate1 = "NEW";
								if (MotorVehicle.Statics.TownLevel != 9)
								{
									MotorVehicle.Statics.ETO = true;
								}
								GetNewPlate(fecherFoundation.Strings.Trim(txtPlateType[0].Text));
								if (MotorVehicle.Statics.rsPlateForReg == null || MotorVehicle.Statics.rsPlateForReg.IsntAnything())
								{
									e.Cancel = true;
									return;
								}
								if (MotorVehicle.Statics.RegistrationType == "DUPREG")
								{
									if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "TX")
									{
										MessageBox.Show("You may not perform a Duplicate Registration process on a TX class vehicle", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										MotorVehicle.Statics.rsPlateForReg.Reset();
										txtPlateType[Index].Text = "";
										cmdProcess.Enabled = false;
										txtPlateType[Index].Focus();
										e.Cancel = true;
									}
								}
								if (MotorVehicle.Statics.RegistrationType == "DUPSTK" || MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
								{
									if (Information.IsDate(MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExpireDate")) && Information.IsDate(MotorVehicle.Statics.rsPlateForReg.Get_Fields("EffectiveDate")))
									{
										if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate").ToOADate() < DateTime.Today.ToOADate())
										{
											MessageBox.Show("You may not perform this process on this Registration because it has expired", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											MotorVehicle.Statics.rsPlateForReg.Reset();
											txtPlateType[Index].Text = "";
											cmdProcess.Enabled = false;
											txtPlateType[Index].Focus();
											e.Cancel = true;
										}
										else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("EffectiveDate").ToOADate() > DateTime.Today.ToOADate())
										{
											if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
											{
												MessageBox.Show("You may not perform this process on this vehicle because it has already been pre-registered.  If you wish to perform this transfer you must void the pre-registration and then run the transfer.  The registrant will need to apply to the town and the state to get a refund of the registration fees", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
												MotorVehicle.Statics.rsPlateForReg.Reset();
												txtPlateType[Index].Text = "";
												cmdProcess.Enabled = false;
												txtPlateType[Index].Focus();
												e.Cancel = true;
											}
										}
									}
								}
								if (MotorVehicle.Statics.rsPlateForReg == null || MotorVehicle.Statics.rsPlateForReg.IsntAnything())
								{
									e.Cancel = true;
									return;
								}
								if (MotorVehicle.Statics.Class == "")
								{
									cboClass.Focus();
									cboClass.SelectedIndex = intClassIndex("PC");
									Support.SendKeys("{F4}", false);
									FCGlobal.Screen.MousePointer = 0;
								}
								else
								{
									if (Information.IsNumeric(MotorVehicle.Statics.Class))
									{
										if (StickerType == "S")
										{
											MotorVehicle.Statics.Class = "IU";
										}
										else
										{
											MotorVehicle.Statics.Class = "TC";
											MessageBox.Show("Effective November 1, 2017, municipalities will no longer be registering truck campers and therefore will not be issuing decals", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											MotorVehicle.Statics.rsSecondPlate.Reset();
											txtPlateType[Index].Text = "";
											cmdProcess.Enabled = false;
											txtPlateType[Index].Focus();
											e.Cancel = true;
											return;
										}
									}
									cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.Class);
								}
								MotorVehicle.Statics.Class1 = fecherFoundation.Strings.Trim(MotorVehicle.Statics.Class);
							}
						}
						if (cboClass.SelectedIndex != -1 && MotorVehicle.Statics.Plate1 == txtPlateType[0].Text)
						{
							if (txtPlateType[1].Visible == true)
							{
								//txtPlateType[1].Focus();
							}
							else
							{

                            }
                            goto endtag;
						}
						if (fecherFoundation.Strings.Trim(txtPlateType[0].Text) == "" || vsResults.Visible == true)
							goto endtag;
						MotorVehicle.Statics.Plate1 = fecherFoundation.Strings.Trim(txtPlateType[0].Text);
						if (MotorVehicle.Statics.RegistrationType == "HELD")
						{
							GetHeldPlate(MotorVehicle.Statics.Plate1);
							if (MotorVehicle.Statics.rsPlateForReg.RecordCount() == 0)
							{
								MessageBox.Show("There was no held registration found for the plate  " + MotorVehicle.Statics.Plate1 + ".  You must clear out the plate number before trying to start over", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
								return;
							}
							MotorVehicle.Statics.rsPlateForReg.MoveLast();
							MotorVehicle.Statics.rsPlateForReg.MoveFirst();
							MotorVehicle.Statics.strSql = "SELECT * FROM HeldRegistrationMaster WHERE masterID = " + MotorVehicle.Statics.rsPlateForReg.Get_Fields_Int32("masterID") + " AND Status = 'FC'";
							MotorVehicle.Statics.rsFinalCompare.OpenRecordset(MotorVehicle.Statics.strSql);
							if (MotorVehicle.Statics.rsFinalCompare.EndOfFile() != true && MotorVehicle.Statics.rsFinalCompare.BeginningOfFile() != true)
							{
								MotorVehicle.Statics.rsFinalCompare.MoveLast();
								MotorVehicle.Statics.rsFinalCompare.MoveFirst();
								pInfo = pCont.GetParty(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID1"));
								MotorVehicle.Statics.orgOwnerInfo.Owner1Name = fecherFoundation.Strings.UCase(pInfo?.FullName) ?? "";
								if (Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID2")) != 0)
								{
									pInfo = pCont.GetParty(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID2"));
									MotorVehicle.Statics.orgOwnerInfo.Owner2Name = fecherFoundation.Strings.UCase(pInfo.FullName);
								}
								else
								{
									MotorVehicle.Statics.orgOwnerInfo.Owner2Name = "";
								}
								if (Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID3")) != 0)
								{
									pInfo = pCont.GetParty(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID3"));
									MotorVehicle.Statics.orgOwnerInfo.Owner3Name = fecherFoundation.Strings.UCase(pInfo.FullName);
								}
								else
								{
									MotorVehicle.Statics.orgOwnerInfo.Owner3Name = "";
								}
							}
							cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.Class);
							goto endtag;
						}
						if (MotorVehicle.Statics.NewPlate == true || (MotorVehicle.Statics.RegistrationType == "NRR" && MotorVehicle.Statics.PlateType == 3 && Index == 0))
						{
							GetNewPlate(fecherFoundation.Strings.Trim(txtPlateType[0].Text));
							if (MotorVehicle.Statics.rsPlateForReg == null || MotorVehicle.Statics.rsPlateForReg.IsntAnything())
							{
								e.Cancel = true;
								return;
							}
							if (MotorVehicle.Statics.RegistrationType == "DUPREG")
							{
								if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "TX")
								{
									MessageBox.Show("You may not perform a Duplicate Registration process on a TX class vehicle", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									MotorVehicle.Statics.rsPlateForReg.Reset();
									txtPlateType[Index].Text = "";
									cmdProcess.Enabled = false;
									txtPlateType[Index].Focus();
									e.Cancel = true;
								}
							}
							if (MotorVehicle.Statics.RegistrationType == "DUPSTK" || MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
							{
								if (Information.IsDate(MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExpireDate")) && Information.IsDate(MotorVehicle.Statics.rsPlateForReg.Get_Fields("EffectiveDate")))
								{
									if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate").ToOADate() < DateTime.Today.ToOADate())
									{
										MessageBox.Show("You may not perform this process on this Registration because it has expired", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										MotorVehicle.Statics.rsPlateForReg.Reset();
										txtPlateType[Index].Text = "";
										cmdProcess.Enabled = false;
										txtPlateType[Index].Focus();
										e.Cancel = true;
									}
									else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("EffectiveDate").ToOADate() > DateTime.Today.ToOADate())
									{
										if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
										{
											MessageBox.Show("You may not perform this process on this vehicle because it has already been pre-registered.  If you wish to perform this transfer you must void the pre-registration and then run the transfer.  The registrant will need to apply to the town and the state to get a refund of the registration fees", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											MotorVehicle.Statics.rsPlateForReg.Reset();
											txtPlateType[Index].Text = "";
											cmdProcess.Enabled = false;
											txtPlateType[Index].Focus();
											e.Cancel = true;
										}
									}
								}
							}
							if (MotorVehicle.Statics.rsPlateForReg == null || MotorVehicle.Statics.rsPlateForReg.IsntAnything())
							{
								e.Cancel = true;
								return;
							}
							if (MotorVehicle.Statics.Class == "")
							{
								cboClass.Focus();
								cboClass.SelectedIndex = intClassIndex("PC");
								Support.SendKeys("{F4}", false);
								FCGlobal.Screen.MousePointer = 0;
							}
							else
							{
								if (Information.IsNumeric(MotorVehicle.Statics.Class))
								{
									if (StickerType == "S")
									{
										MotorVehicle.Statics.Class = "IU";
									}
									else
									{
										MotorVehicle.Statics.Class = "TC";
										MessageBox.Show("Effective November 1, 2017, municipalities will no longer be registering truck campers and therefore will not be issuing decals", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										MotorVehicle.Statics.rsSecondPlate.Reset();
										txtPlateType[Index].Text = "";
										cmdProcess.Enabled = false;
										txtPlateType[Index].Focus();
										e.Cancel = true;
										return;
									}
								}
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.Class);
							}
							MotorVehicle.Statics.Class1 = fecherFoundation.Strings.Trim(MotorVehicle.Statics.Class);
						}
						else
						{
							if (PlateFromSearch == true)
							{
								PlateFromSearch = false;
							}
							else
							{
								GetExistingPlate(0);
								if (MotorVehicle.Statics.rsPlateForReg != null && !MotorVehicle.Statics.rsPlateForReg.IsntAnything())
									txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Plate"));
							}
							if (MotorVehicle.Statics.rsPlateForReg == null || MotorVehicle.Statics.rsPlateForReg.IsntAnything())
							{
								txtPlateType[Index].Text = "";
								cmdProcess.Enabled = false;
								txtPlateType[Index].Focus();
								e.Cancel = true;
								return;
							}
							if (MotorVehicle.Statics.RegistrationType != "UPD")
							{
								if (MotorVehicle.Statics.rsPlateForReg == null || MotorVehicle.Statics.rsPlateForReg.IsntAnything())
								{
									// do nothing
								}
								else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Status") == "T")
								{
									MessageBox.Show("You may not perform this process on a terminated registration", "Terminated Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									MotorVehicle.Statics.rsPlateForReg.Reset();
									txtPlateType[Index].Text = "";
									cmdProcess.Enabled = false;
									txtPlateType[Index].Focus();
									e.Cancel = true;
									return;
								}
							}
							if (MotorVehicle.Statics.RegistrationType == "CORR")
							{
								if (MotorVehicle.Statics.NewToTheSystem)
								{
									MessageBox.Show("You may not Perform a Correction on a Plate that is new to the System", "New to the System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									MotorVehicle.Statics.rsPlateForReg.Reset();
									cmdProcess.Enabled = false;
									txtPlateType[Index].Focus();
									e.Cancel = true;
									return;
								}
							}
							if (MotorVehicle.Statics.RegistrationType == "BOOST")
							{
								if (MotorVehicle.Statics.NewToTheSystem)
								{
									MessageBox.Show("You may not perform this process for this registration because it is new to the system", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									MotorVehicle.Statics.rsPlateForReg.Reset();
									txtPlateType[Index].Text = "";
									cmdProcess.Enabled = false;
									txtPlateType[Index].Focus();
									e.Cancel = true;
									return;
									cmdStartOver_Click();
									txtPlateType[0].Focus();
								}
								else if (FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "CC" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "CO" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "TT" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "FM" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "AP" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "AC" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "AF" || FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class")) == "LC")
								{
									// do nothing
								}
								else
								{
									MessageBox.Show("You may only boost a vehicle with the class CC, CO, TT, AP, AC, AF, LC or FM", "Invalid Class", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									MotorVehicle.Statics.rsPlateForReg.Reset();
									txtPlateType[Index].Text = "";
									cmdProcess.Enabled = false;
									txtPlateType[Index].Focus();
									e.Cancel = true;
									return;
									cmdStartOver_Click();
									txtPlateType[0].Focus();
								}
							}
							if (MotorVehicle.Statics.RegistrationType == "DUPREG")
							{
								if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_Boolean("ETO") == true)
								{
									MotorVehicle.Statics.ETO = true;
									MessageBox.Show("You may not perform a duplicate registration or duplicate sticker process on an excise tax only registration", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									MotorVehicle.Statics.rsPlateForReg.Reset();
									txtPlateType[Index].Text = "";
									cmdProcess.Enabled = false;
									txtPlateType[Index].Focus();
									e.Cancel = true;
									return;
								}
							}
							if (MotorVehicle.Statics.RegistrationType != "DUPREG" && MotorVehicle.Statics.RegistrationType != "LOST" && MotorVehicle.Statics.RegistrationType != "CORR")
							{
								if (MotorVehicle.Statics.Class == "TC")
								{
									MessageBox.Show("Effective November 1, 2017, municipalities will no longer be registering truck campers and therefore will not be issuing decals", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									MotorVehicle.Statics.rsPlateForReg.Reset();
									txtPlateType[Index].Text = "";
									cmdProcess.Enabled = false;
									txtPlateType[Index].Focus();
									e.Cancel = true;
									return;
								}
							}
							if (MotorVehicle.Statics.RegistrationType == "DUPSTK" || MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT" || MotorVehicle.Statics.RegistrationType == "BOOST" || MotorVehicle.Statics.RegistrationType == "GVW")
							{
								if ((MotorVehicle.Statics.RegistrationType == "RRT" && MotorVehicle.Statics.PlateType == 2) || (MotorVehicle.Statics.RegistrationType == "NRT" && MotorVehicle.Statics.PlateType == 4))
								{
									// do nothing
								}
								else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("ExpireDate").ToOADate() < DateTime.Today.ToOADate() && Information.IsDate(MotorVehicle.Statics.rsPlateForReg.Get_Fields("ExpireDate")))
								{
									MessageBox.Show("You may not perform this process on this registration because it has expired", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									MotorVehicle.Statics.rsPlateForReg.Reset();
									txtPlateType[Index].Text = "";
									cmdProcess.Enabled = false;
									txtPlateType[Index].Focus();
									e.Cancel = true;
								}
								else if (MotorVehicle.Statics.rsPlateForReg.Get_Fields_DateTime("EffectiveDate").ToOADate() > DateTime.Today.ToOADate() && Information.IsDate(MotorVehicle.Statics.rsPlateForReg.Get_Fields("EffectiveDate")))
								{
									if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
									{
										MessageBox.Show("You may not perform this process on this vehicle because it has already been pre-registered.  If you wish to perform this transfer you must void the pre-registration and then run the transfer.  The registrant will need to apply to the town and the state to get a refund of the registration fees", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										MotorVehicle.Statics.rsPlateForReg.Reset();
										txtPlateType[Index].Text = "";
										cmdProcess.Enabled = false;
										txtPlateType[Index].Focus();
										e.Cancel = true;
									}
								}
							}
							if (MotorVehicle.Statics.rsPlateForReg == null || MotorVehicle.Statics.rsPlateForReg.IsntAnything())
							{
								return;
								cmdStartOver_Click();
								txtPlateType[0].Focus();
							}
							if (MotorVehicle.Statics.Class == "")
							{
								cboClass.Focus();
								cboClass.SelectedIndex = intClassIndex("PC");
								Support.SendKeys("{F4}", false);
								FCGlobal.Screen.MousePointer = 0;
								MotorVehicle.Statics.Class1 = "";
								goto endtag;
							}
							if ((cmbRegType1.Text == "Re-Registration Transfer" && cmbPlate.SelectedIndex == 0) || (cmbRegType1.Text == "New Registration Transfer" && cmbPlate.SelectedIndex == 0))
							{
								clsDRWrapper temp = MotorVehicle.Statics.rsPlateForReg;
								if (!ValidateExpDate(ref temp))
								{
									MessageBox.Show("The registration date on this plate has already expired", "Invalid Transfer Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									txtPlateType[Index].Text = "";
									e.Cancel = true;
									goto endtag;
								}
								MotorVehicle.Statics.rsPlateForReg = temp;
							}
							if (MotorVehicle.Statics.rsPlateForReg.RecordCount() == 0)
							{
								answer = MessageBox.Show("There is no Plate in the Master file with that Number. Do you wish to continue?", "No Plate on File", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (answer == DialogResult.No)
								{
									ClearScreen(true);
									goto endtag;
								}
								else
								{
									MotorVehicle.Statics.NewToTheSystem = true;
									cboClass.Focus();
									cboClass.SelectedIndex = intClassIndex("PC");
									Support.SendKeys("{F4}", false);
									FCGlobal.Screen.MousePointer = 0;
								}
							}
							else
							{
								cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.Class);
							}
							MotorVehicle.Statics.Class1 = fecherFoundation.Strings.Trim(MotorVehicle.Statics.Class);
						}
						MotorVehicle.Statics.RememberedSubclass = MotorVehicle.Statics.Subclass;
						break;
					}
				case 1:
					{
						holdsubclass = MotorVehicle.Statics.Subclass;
						if (fecherFoundation.Strings.Trim(txtPlateType[1].Text) == "")
							goto endtag;
						MotorVehicle.Statics.Plate2 = fecherFoundation.Strings.Trim(txtPlateType[1].Text);
						if (txtPlateType[1].Text != "")
						{
							GetExistingPlate(1);
							if (!MotorVehicle.Statics.rsSecondPlate.IsntAnything())
								txtPlateType[1].Text = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Plate"));
							MotorVehicle.Statics.Class2 = MotorVehicle.Statics.Class;
							if (MotorVehicle.Statics.Class2 == "00")
								MotorVehicle.Statics.Class2 = "TC";
							cboClass2.SelectedIndex = intClassIndex(MotorVehicle.Statics.Class2);
						}
						if (MotorVehicle.Statics.Class == "TC")
						{
							if (MotorVehicle.Statics.RegistrationType != "DUPREG" && MotorVehicle.Statics.RegistrationType != "LOST" && MotorVehicle.Statics.RegistrationType != "CORR")
							{
								MessageBox.Show("Effective November 1, 2017, municipalities will no longer be registering truck campers and therefore will not be issuing decals", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.rsSecondPlate.Reset();
								txtPlateType[Index].Text = "";
								cmdProcess.Enabled = false;
								txtPlateType[Index].Focus();
								e.Cancel = true;
							}
						}
						if (MotorVehicle.Statics.RegistrationType == "DUPSTK" || MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT" || MotorVehicle.Statics.RegistrationType == "BOOST")
						{
							if (MotorVehicle.Statics.RegistrationType == "RRT" && MotorVehicle.Statics.PlateType == 1)
							{
								// do nothing
							}
							else if (MotorVehicle.Statics.RegistrationType == "RRT" && (MotorVehicle.Statics.PlateType == 3 || MotorVehicle.Statics.PlateType == 4))
							{
								// do nothing.
							}
							else if (Information.IsDate(MotorVehicle.Statics.rsSecondPlate.Get_Fields("ExpireDate")) && 
							         MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate").ToOADate() < DateTime.Now.ToOADate())
							{
								MessageBox.Show("You may not perform this process on this registration because it has expired", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.rsSecondPlate.Reset();
								txtPlateType[Index].Text = "";
								cmdProcess.Enabled = false;
								txtPlateType[Index].Focus();
								e.Cancel = true;
							}
							else if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("EffectiveDate").ToOADate() > DateTime.Today.ToOADate() && Information.IsDate(MotorVehicle.Statics.rsSecondPlate.Get_Fields("EffectiveDate")))
							{
								if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
								{
									MessageBox.Show("You may not perform this process on this vehicle because it has already been pre-registered.  If you wish to perform this transfer you must void the pre-registration and then run the transfer.  The registrant will need to apply to the town and the state to get a refund of the registration fees", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									MotorVehicle.Statics.rsSecondPlate.Reset();
									txtPlateType[Index].Text = "";
									cmdProcess.Enabled = false;
									txtPlateType[Index].Focus();
									e.Cancel = true;
								}
							}
						}
						if (MotorVehicle.Statics.RegistrationType == "CORR" && MotorVehicle.Statics.PlateType == 2)
						{
							if (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Status")) == "T")
							{
								MessageBox.Show("You may not perform this process on a terminated registration", "Terminated Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.rsSecondPlate.Reset();
								txtPlateType[Index].Text = "";
								cmdProcess.Enabled = false;
								txtPlateType[Index].Focus();
								e.Cancel = true;
								return;
							}
						}
						if (MotorVehicle.Statics.RegistrationType == "BOOST")
						{
							if (MotorVehicle.Statics.NewToTheSystem)
							{
								MessageBox.Show("You may not perform this process for this registration because it is new to the system", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								MotorVehicle.Statics.rsSecondPlate.Reset();
								txtPlateType[Index].Text = "";
								cmdProcess.Enabled = false;
								txtPlateType[Index].Focus();
								e.Cancel = true;
								return;
							}
							else
							{
								rsBoost.OpenRecordset("SELECT * FROM Booster WHERE isnull(Voided, 0) = 0 AND Inactive = 0 and ExpireDate >= '" + DateTime.Today.ToShortDateString() + "' AND VehicleID = " + MotorVehicle.Statics.rsSecondPlate.Get_Fields_Int32("ID"), "twmv0000.vb1");
								if (rsBoost.EndOfFile() != true && rsBoost.BeginningOfFile() != true)
								{
									// do nothing
								}
								else
								{
									MessageBox.Show("This plate does not have a booster associated with it", "Invalid Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									MotorVehicle.Statics.rsSecondPlate.Reset();
									txtPlateType[Index].Text = "";
									cmdProcess.Enabled = false;
									txtPlateType[Index].Focus();
									e.Cancel = true;
								}
							}
						}
						if (MotorVehicle.Statics.rsSecondPlate.IsntAnything())
						{
							e.Cancel = true;
							return;
						}
						if (MotorVehicle.Statics.rsSecondPlate.RecordCount() == 0)
						{
							string strTemp = "";
							if (MotorVehicle.Statics.RegistrationType != "RRR" && MotorVehicle.Statics.RegistrationType != "NRT" && (MotorVehicle.Statics.RegistrationType != "RRT" || MotorVehicle.Statics.PlateType != 2))
							{
								strTemp = "The Plate " + fecherFoundation.Strings.Trim(txtPlateType[1].Text) + " cannot be found in the Master file.  You must enter the information into the New To System / Update Screen before you may continue";
								MessageBox.Show(strTemp, "No Plate on File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								ClearScreen(true);
								goto endtag;
							}
							else
							{
								MotorVehicle.Statics.NewToTheSystem = true;
								cboClass2.Focus();
								cboClass2.SelectedIndex = intClassIndex("PC");
								Support.SendKeys("{F4}", false);
								FCGlobal.Screen.MousePointer = 0;
								MotorVehicle.Statics.Class2 = cboClass2.Text;
								MotorVehicle.Statics.Subclass2 = "";
							}
						}
						else
						{
							if (MotorVehicle.Statics.Class == "00")
								MotorVehicle.Statics.Class = "TC";
							cboClass2.SelectedIndex = intClassIndex(MotorVehicle.Statics.Class);
							MotorVehicle.Statics.Class2 = MotorVehicle.Statics.Class;
							MotorVehicle.Statics.Subclass2 = FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass"));
						}
						if ((cmbRegType1.Text == "Re-Registration Transfer" && cmbPlate.SelectedIndex == 1) || (cmbRegType1.Text == "New Registration Transfer" && (cmbPlate.SelectedIndex == 1 || cmbPlate.SelectedIndex == 2 || cmbPlate.SelectedIndex == 3)))
						{
							clsDRWrapper temp = MotorVehicle.Statics.rsSecondPlate;
							if (!ValidateExpDate(ref temp) && !MotorVehicle.Statics.NewToTheSystem)
							{
								MessageBox.Show("The registration date on this plate has already expired", "Invalid Transfer Plate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								txtPlateType[Index].Text = "";
								e.Cancel = true;
								return;
							}
							MotorVehicle.Statics.rsSecondPlate = temp;
						}
						MotorVehicle.Statics.Subclass = holdsubclass;
						break;
					}
				case 2:
					{
						holdsubclass = MotorVehicle.Statics.Subclass;
						MotorVehicle.Statics.Plate3 = fecherFoundation.Strings.Trim(txtPlateType[2].Text);
						if (txtPlateType[2].Text != "")
						{
							GetExistingPlate(2);
							if (!MotorVehicle.Statics.rsThirdPlate.IsntAnything())
								txtPlateType[2].Text = FCConvert.ToString(MotorVehicle.Statics.rsThirdPlate.Get_Fields_String("Plate"));
							MotorVehicle.Statics.Class3 = MotorVehicle.Statics.Class;
							MotorVehicle.Statics.Subclass = holdsubclass;
							if (MotorVehicle.Statics.rsThirdPlate.IsntAnything())
							{
								e.Cancel = true;
								return;
							}
							else
							{
								if (MotorVehicle.Statics.RegistrationType == "RRT")
								{
									if (MotorVehicle.Statics.PlateType == 3 || MotorVehicle.Statics.PlateType == 4)
									{
										if (MotorVehicle.Statics.rsThirdPlate.Get_Fields_DateTime("ExpireDate").ToOADate() < DateTime.Now.ToOADate())
										{
											MessageBox.Show("You may not perform this process on this registration because it has expired", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											MotorVehicle.Statics.rsThirdPlate.Reset();
											txtPlateType[Index].Text = "";
											cmdProcess.Enabled = false;
											txtPlateType[Index].Focus();
											e.Cancel = true;
										}
										else if (MotorVehicle.Statics.rsThirdPlate.Get_Fields_DateTime("EffectiveDate").ToOADate() > DateTime.Today.ToOADate())
										{
											if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRT")
											{
												MessageBox.Show("You may not perform this process on this vehicle because it has already been pre-registered.  If you wish to perform this transfer you must void the pre-registration and then run the transfer.  The registrant will need to apply to the town and the state to get a refund of the registration fees", "Invalid Registration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
												MotorVehicle.Statics.rsThirdPlate.Reset();
												txtPlateType[Index].Text = "";
												cmdProcess.Enabled = false;
												txtPlateType[Index].Focus();
												e.Cancel = true;
											}
										}
									}
								}
							}
						}
						break;
					}
			    }
			    endtag:
			    ;
			    SettingValues = false;
            }
            finally
            {
                rsBoost.Dispose();
            }
		}

		private void txtPlateType_Validate(object sender, CancelEventArgs e)
		{
			int index = txtPlateType.GetIndex((Global.T2KOverTypeBox)sender);
			txtPlateType_Validate(index, sender, e);
		}

		public void SetObjectsForPlate(string lblPlateCaption0, string lblPlateCaption1, string lblPlateCaption2, bool cboClass1Visible, bool cboClass2Visible, string Plate0TTT, string Plate1TTT, string Plate2TTT, bool blnNewLongTermPlate = false)
		{
			lblPlateType[0].Text = lblPlateCaption0;
			lblPlateType[1].Text = lblPlateCaption1;
			lblPlateType[2].Text = lblPlateCaption2;
			lblPlateType[0].Visible = (lblPlateType[0].Text != "" ? true : false);
			lblPlateType[1].Visible = (lblPlateType[1].Text != "" ? true : false);
			lblPlateType[2].Visible = (lblPlateType[2].Text != "" ? true : false);
			txtPlateType[0].Visible = (lblPlateType[0].Text != "" && !blnNewLongTermPlate ? true : false);
			txtPlateType[1].Visible = (lblPlateType[1].Text != "" ? true : false);
			txtPlateType[2].Visible = (lblPlateType[2].Text != "" ? true : false);
			cboLongTermRegPlateYear.Visible = blnNewLongTermPlate;
			cboClass.Visible = cboClass1Visible && !blnNewLongTermPlate;
			cboClass2.Visible = cboClass2Visible;
			if (!blnNewLongTermPlate)
			{
				ToolTip1.SetToolTip(lblPlateType[0], Plate0TTT);
			}
			else
			{
				ToolTip1.SetToolTip(lblPlateType[0], "Please enter the expiration year for the plate you would like to use");
			}
			ToolTip1.SetToolTip(lblPlateType[1], Plate1TTT);
			txtPlateType[1].ToolTipText = Plate1TTT;
			ToolTip1.SetToolTip(lblPlateType[2], Plate2TTT);
			txtPlateType[2].ToolTipText = Plate2TTT;
			cboClass.Text = "";
			cboClass2.Text = "";
			cboLongTermRegPlateYear.Text = "";
			txtPlateType[0].Text = "";
			txtPlateType[1].Text = "";
			txtPlateType[2].Text = "";
		}

		private void Lost_Plate_Registration()
		{
			object ans = null;
			clsDRWrapper rr = new clsDRWrapper();
			clsDRWrapper rsClass = new clsDRWrapper();
			rr.OpenRecordset("SELECT * FROM DefaultInfo");
			rr.MoveLast();
			rr.MoveFirst();
			// 
			frmWait.InstancePtr.Unload();
			// 
			if (FCConvert.ToString(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) == "TX")
			{
				MessageBox.Show("You may not perform this process on a TX class vehicle", "TX Class", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate").ToOADate() != 0)
			{
				frmLostPlate.InstancePtr.txtExpires.Text = Strings.Format(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");
			}
			else
			{
				ans = "";
				ans = Interaction.InputBox("Please Enter the Expiration Date", "ExpirationDate", null);
				if (Information.IsDate(ans))
				{
					frmLostPlate.InstancePtr.txtExpires.Text = Strings.Format(ans, "MM/dd/yyyy");
					MotorVehicle.Statics.rsSecondPlate.Edit();
					MotorVehicle.Statics.rsSecondPlate.Set_Fields("ExpireDate", Strings.Format(ans, "MM/dd/yyyy"));
					MotorVehicle.Statics.rsSecondPlate.Update();
				}
				else
				{
					MessageBox.Show("Invalid Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					frmLostPlate.InstancePtr.Unload();
					frmPlateInfo.InstancePtr.Unload();
					return;
				}
			}
			rsClass.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class") + "' AND SystemCode = '" + MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Subclass") + "'");
			if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
			{
				rsClass.MoveLast();
				rsClass.MoveFirst();
				if (FCConvert.ToInt32(rsClass.Get_Fields_Int32("NumberOfPlateStickers")) == 1)
				{
					frmLostPlate.InstancePtr.cmb1.Text = "1";
				}
				else
				{
					frmLostPlate.InstancePtr.cmb1.Text = "2";
				}
			}
			frmLostPlate.InstancePtr.lblM.Text = "(M) " + Strings.Format(frmLostPlate.InstancePtr.txtExpires.Text, "MM");
			if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.rsSecondPlate.Get_Fields_String("Class")) && MotorVehicle.Statics.FixedMonth)
			{
				frmLostPlate.InstancePtr.lblY.Text = "(C) " + Strings.Format(frmLostPlate.InstancePtr.txtExpires.Text, "yy");
			}
			else
			{
				frmLostPlate.InstancePtr.lblY.Text = "(Y) " + Strings.Format(frmLostPlate.InstancePtr.txtExpires.Text, "yy");
			}
            //FC:FINAL:SBE - #4049 - In VB6, Hide() is executed on Modal Form, and it is not closed. Form controls can be accessed later in the code
            frmLostPlate.InstancePtr.CloseOnUnload = false;
			frmLostPlate.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			if (FCConvert.ToString(modGNBas.Statics.Response) == "QUIT")
			{
				frmLostPlate.InstancePtr.Unload();
				frmPlateInfo.InstancePtr.Unload();
				return;
			}
			frmLostPlate.InstancePtr.Unload();
			frmPlateInfo.InstancePtr.Unload();
            var muniName = fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName));

            if (MotorVehicle.Statics.rsSecondPlate.RecordCount() == 0)
			{
				MotorVehicle.Statics.RegTypeLost = "Input";
				clsDRWrapper temp = MotorVehicle.Statics.rsSecondPlate;
				MotorVehicle.FillTypeTwo(ref temp);
				MotorVehicle.Statics.rsSecondPlate = temp;
				MotorVehicle.Statics.rsFinal.Set_Fields("TransactionType", "LPS");
				MotorVehicle.Statics.rsFinal.Set_Fields("OpID", MotorVehicle.Statics.OpID);
				if (MotorVehicle.Statics.ForcedPlate == "N")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("ForcedPlate", "N");
				}
				if (MotorVehicle.Statics.PlateType == 1)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateType", "N");
				}
				else if (MotorVehicle.Statics.PlateType == 2)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateType", "O");
				}
				MotorVehicle.Statics.rsFinalCompare.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
				temp = MotorVehicle.Statics.rsFinalCompare;
				clsDRWrapper temp1 = MotorVehicle.Statics.rsFinal;
				MotorVehicle.TransferRecordSetsB(ref temp, ref temp1);
				MotorVehicle.Statics.rsFinalCompare = temp;
				MotorVehicle.Statics.rsFinal = temp1;
				MotorVehicle.Statics.rsFinal.Set_Fields("plate", MotorVehicle.Statics.Plate1);
				MotorVehicle.Statics.rsFinal.Set_Fields("Class", MotorVehicle.Statics.Class);
				MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerCharge", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMonthCharge.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNoCharge", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMonthNoCharge.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerMonth", FCConvert.ToDateTime(frmLostPlate.InstancePtr.txtExpires.Text).Month);
				MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNumber", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMStickerNumber.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerCharge", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYearCharge.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNoCharge", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYearNoCharge.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerYear", FCConvert.ToDateTime(frmLostPlate.InstancePtr.txtExpires.Text).Year);
				MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNumber", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYStickerNumber.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("StickerFee", 0);
				if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge") + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge") != 0)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee") + ((MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge") + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge")) * rr.Get_Fields_Decimal("Stickers")));
				}
				MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
				MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", 0);
				MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", 0);
				MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", 0);
				MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeDupReg", false);
				
				if (muniName == "MAINE MOTOR TRANSPORT")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "MMTA SERVICES INC. 44004");
				}
				else if (muniName == "ACE REGISTRATION SERVICES LLC")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "ACE REGISTRATION");
					MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", "ME");
					MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", "44021");
				}
				else if (muniName == "STAAB AGENCY")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "STAAB AGENCY");
                }
				else if (muniName == "COUNTRYWIDE TRAILER")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "COUNTRYWIDE TRAILER 44018");
				}
				else if (muniName == "AB LEDUE ENTERPRISES")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "AB LEDUE ENTERPRISES");
					MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", "ME");
					MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", "44003");
				}
				else if (muniName == "MAINE TRAILER")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "MAINE TRAILER ME 44005");
				}
				else if (muniName == "HASKELL REGISTRATION")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "HASKELL REGISTRATION 44002");
				}


				frmDataInput.InstancePtr.Show(App.MainForm);
			}
			else
			{
				MotorVehicle.Statics.RegTypeLost = "Info";
				clsDRWrapper temp = MotorVehicle.Statics.rsSecondPlate;
				MotorVehicle.FillTypeTwo(ref temp);
				MotorVehicle.Statics.rsSecondPlate = temp;
				MotorVehicle.Statics.rsFinal.Set_Fields("OpID", MotorVehicle.Statics.OpID);
				MotorVehicle.Statics.rsFinal.Set_Fields("TitleDone", false);
				MotorVehicle.Statics.rsFinal.Set_Fields("UseTaxDone", false);
				if (MotorVehicle.Statics.ForcedPlate == "N")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("ForcedPlate", "N");
				}
				if (MotorVehicle.Statics.PlateType == 1)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateType", "N");
				}
				else if (MotorVehicle.Statics.PlateType == 2)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("PlateType", "O");
				}
				temp = MotorVehicle.Statics.rsFinalCompare;
				clsDRWrapper temp1 = MotorVehicle.Statics.rsFinal;
				MotorVehicle.TransferRecordSetsB(ref temp, ref temp1);
				MotorVehicle.Statics.rsFinalCompare = temp;
				MotorVehicle.Statics.rsFinal = temp1;
				MotorVehicle.Statics.TempMonth = Strings.Format(frmLostPlate.InstancePtr.txtExpires.Text, "MM");
				MotorVehicle.Statics.TempYear = Strings.Format(frmLostPlate.InstancePtr.txtExpires.Text, "yy");
				MotorVehicle.Statics.rsFinal.Set_Fields("TransactionType", "LPS");
				MotorVehicle.Statics.rsFinal.Set_Fields("plate", MotorVehicle.Statics.Plate1);
				MotorVehicle.Statics.rsFinal.Set_Fields("Class", MotorVehicle.Statics.Class);
				MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerCharge", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMonthCharge.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNoCharge", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMonthNoCharge.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerMonth", FCConvert.ToDateTime(frmLostPlate.InstancePtr.txtExpires.Text).Month);
				MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNumber", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMStickerNumber.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerCharge", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYearCharge.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNoCharge", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYearNoCharge.Text)));
				MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerYear", FCConvert.ToDateTime(frmLostPlate.InstancePtr.txtExpires.Text).Year);
				MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNumber", FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYStickerNumber.Text)));
				Decimal curPlateFee;
				if (Conversion.Val(rr.Get_Fields_Decimal("LongTermReplacementPlateFee")) == 0)
				{
					curPlateFee = 0;
				}
				else
				{
					curPlateFee = FCConvert.ToDecimal(rr.Get_Fields_Decimal("LongTermReplacementPlateFee"));
				}
				if (MotorVehicle.Statics.Class == "TL" && MotorVehicle.Statics.Subclass == "L9")
				{
					if (MotorVehicle.Statics.PlateType == 1)
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee", (frmLostPlate.InstancePtr.cmb1.Text == "1" ? curPlateFee : curPlateFee * 2));
					}
					else
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee", 0);
					}
					MotorVehicle.Statics.rsFinal.Set_Fields("TwentyFiveYearPlate", MotorVehicle.Statics.rsFinalCompare.Get_Fields_Boolean("TwentyFiveYearPlate"));
				}
				else
				{
					if (MotorVehicle.Statics.PlateType == 1)
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee", (frmLostPlate.InstancePtr.cmb1.Text != "2" ? rr.Get_Fields_Decimal("ReplacementPlate") : rr.Get_Fields_Decimal("ReplacementPlate") * 2));
					}
					else
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("ReplacementFee", 0);
					}
				}
				MotorVehicle.Statics.rsFinal.Set_Fields("StickerFee", 0);
				MotorVehicle.Statics.rsFinal.Set_Fields("TitleFee", 0);
				MotorVehicle.Statics.rsFinal.Set_Fields("SalesTax", 0);
				if (MotorVehicle.Statics.PlateType == 1 && MotorVehicle.Statics.RegistrationType == "LOST")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Transfer", "N");
				}
				if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge") + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge") != 0)
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("StickerFee", ((MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge") + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge")) * rr.Get_Fields_Decimal("Stickers")));
				}
				MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3", FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"))));
				MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", 0);
				MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeDupReg", false);
				MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeNew", 0);
				MotorVehicle.Statics.rsFinal.Set_Fields("PlateFeeReReg", 0);
				// End If
				if (muniName == "MAINE MOTOR TRANSPORT")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "MMTA SERVICES INC. 44004");
				}
				else if (muniName == "ACE REGISTRATION SERVICES LLC")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "ACE REGISTRATION");
					MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", "ME");
					MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", "44021");
				}
				else if (muniName == "STAAB AGENCY")
				{
					if (MotorVehicle.Statics.Class == "TL" && MotorVehicle.Statics.Subclass == "L9")
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "STAAB AGENCY 44006");
					}
					else
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "STAAB AGENCY");
					}
				}
				else if (muniName == "COUNTRYWIDE TRAILER")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "COUNTRYWIDE TRAILER 44018");
				}
				else if (muniName == "AB LEDUE ENTERPRISES")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "AB LEDUE ENTERPRISES");
					MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceState", "ME");
					MotorVehicle.Statics.rsFinal.Set_Fields("ResidenceCode", "44003");
				}
				else if (muniName == "MAINE TRAILER")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "MAINE TRAILER ME 44005");
				}
				else if (muniName == "HASKELL REGISTRATION")
				{
					MotorVehicle.Statics.rsFinal.Set_Fields("Residence", "HASKELL REGISTRATION 44002");
				}
				if (MotorVehicle.Statics.Class == "CO")
					MotorVehicle.Statics.Subclass = "CO";
				if (MotorVehicle.Statics.Class == "TT")
					MotorVehicle.Statics.Subclass = "TT";
				if (MotorVehicle.Statics.Class == "TL" && MotorVehicle.Statics.Subclass == "L9")
				{
					MotorVehicle.Statics.gboolFromLongTermDataEntry = true;
					if (FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"))) > 2000 && FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"))) < 3000)
					{
						MotorVehicle.Statics.rsFinal.Set_Fields("Plate", MotorVehicle.GetLongTermPlate(FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")))));
						MotorVehicle.Statics.rsFinal.Set_Fields("PlateStripped", fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"))).Replace("&", "").Replace(" ", "").Replace("-", ""));
					}
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"))) != "")
					{
						frmSavePrint.InstancePtr.Show(App.MainForm);
					}
				}
				else
                {
                    frmPreview.InstancePtr.DisableReturnButton = true;
                    frmPreview.InstancePtr.Show(App.MainForm);
                }
			}
		}

		public void Duplicate_Stickers()
		{
			clsDRWrapper rsAM = new clsDRWrapper();
			object ans = null;
			clsDRWrapper rr = new clsDRWrapper();
			clsDRWrapper rsClass = new clsDRWrapper();
			clsDRWrapper rsException = new clsDRWrapper();

            try
            {
                rr.OpenRecordset("SELECT * FROM DefaultInfo");
                if (rr.EndOfFile() != true && rr.BeginningOfFile() != true)
                {
                    rr.MoveLast();
                    rr.MoveFirst();
                    clsDRWrapper temp = MotorVehicle.Statics.rsPlateForReg;
                    MotorVehicle.FillTypeOne(ref temp);
                    MotorVehicle.Statics.rsPlateForReg = temp;
                    frmLostPlate.InstancePtr.cmb1.Visible = false;
                    frmLostPlate.InstancePtr.lbl1.Visible = false;
                    // 
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").ToOADate() != 0)
                    {
                        frmLostPlate.InstancePtr.txtExpires.Text =
                            Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"),
                                "MM/dd/yyyy");
                    }
                    else
                    {
                        ans = "";
                        ans = Interaction.InputBox("Please Enter the Expiration Date", "ExpirationDate", null);
                        if (Information.IsDate(ans))
                        {
                            frmLostPlate.InstancePtr.txtExpires.Text = Strings.Format(ans, "MM/dd/yyyy");
                            MotorVehicle.Statics.rsFinal.Set_Fields("ExpireDate", Strings.Format(ans, "MM/dd/yyyy"));
                        }
                        else
                        {
                            MessageBox.Show("Invalid Date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            frmLostPlate.InstancePtr.Unload();
                            frmPlateInfo.InstancePtr.Unload();
                            return;
                        }
                    }

                    rsClass.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class +
                                          "' AND SystemCode = '" + MotorVehicle.Statics.Subclass + "'");
                    if (rsClass.EndOfFile() != true && rsClass.BeginningOfFile() != true)
                    {
                        rsClass.MoveLast();
                        rsClass.MoveFirst();
                        if (FCConvert.ToInt32(rsClass.Get_Fields_Int32("NumberOfPlateStickers")) == 1)
                        {
                            frmLostPlate.InstancePtr.cmb1.Text = "1";
                        }
                        else
                        {
                            frmLostPlate.InstancePtr.cmb1.Text = "2";
                        }
                    }

                    frmLostPlate.InstancePtr.lblM.Text =
                        "(M) " + Strings.Format(frmLostPlate.InstancePtr.txtExpires.Text, "MM");
                    if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) && MotorVehicle.Statics.FixedMonth)
                    {
                        frmLostPlate.InstancePtr.lblY.Text =
                            "(C) " + Strings.Format(frmLostPlate.InstancePtr.txtExpires.Text, "yy");
                    }
                    else
                    {
                        frmLostPlate.InstancePtr.lblY.Text =
                            "(Y) " + Strings.Format(frmLostPlate.InstancePtr.txtExpires.Text, "yy");
                    }

                    MotorVehicle.Statics.TempMonth = Strings.Format(frmLostPlate.InstancePtr.txtExpires.Text, "MM");
                    MotorVehicle.Statics.TempYear = Strings.Format(frmLostPlate.InstancePtr.txtExpires.Text, "yy");
                    frmLostPlate.InstancePtr.CloseOnUnload = false;
                    frmLostPlate.InstancePtr.Show(FCForm.FormShowEnum.Modal);
                    if (FCConvert.ToString(modGNBas.Statics.Response) != "OK")
                    {
                        frmPlateInfo.InstancePtr.Unload();
                    }
                    else
                    {
                        int fnx;
                        rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE ID = 0");
                        rsAM.AddNew();
                        for (fnx = 0; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
                        {
                            if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "TransactionType")
                            {
                                rsAM.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), "DPS");
                            }
                            else if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "OPID")
                            {
                                rsAM.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx),
                                    MotorVehicle.Statics.OpID);
                            }
                            else if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "DateUpdated")
                            {
                                rsAM.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), DateTime.Now);
                            }
                            else if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "OldMVR3")
                            {
                                rsAM.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), 0);
                            }
                            else if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) == "TellerCloseoutID")
                            {
                                rsAM.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), 0);
                            }
                            else
                            {
                                if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) != "ID" &&
                                    MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) != "ID")
                                {
                                    if (FCConvert.ToString(
                                            MotorVehicle.Statics.rsFinal.Get_Fields(
                                                MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx))) != "")
                                    {
                                        rsAM.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx),
                                            MotorVehicle.Statics.rsFinal.Get_Fields(
                                                MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx)));
                                    }
                                }
                            }
                        }

                        string tempstr = "";
                        EnterReason: ;
                        tempstr = Interaction.InputBox("Please enter a reason for issuing duplicate stickers",
                            "Reason Needed", null);
                        if (tempstr == "")
                        {
                            MessageBox.Show("You must enter a reason before you can save this registration",
                                "Invalid Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            goto EnterReason;
                        }

                        rsAM.Set_Fields("MonthStickerCharge", 0);
                        rsAM.Set_Fields("MonthStickerNoCharge", 0);
                        rsAM.Set_Fields("MonthStickerNumber", 0);
                        rsAM.Set_Fields("YearStickerCharge", 0);
                        rsAM.Set_Fields("YearStickerNoCharge", 0);
                        rsAM.Set_Fields("YearStickerNumber", 0);
                        rsAM.Set_Fields("YearStickerYear", 0);
                        rsAM.Set_Fields("ExciseHalfRate", false);
                        rsAM.Set_Fields("ExciseProrate", false);
                        rsAM.Set_Fields("AgentFee", 0);
                        rsAM.Set_Fields("ExciseTaxFull", 0);
                        rsAM.Set_Fields("ExciseTaxCharged", 0);
                        rsAM.Set_Fields("ExciseCreditFull", 0);
                        rsAM.Set_Fields("ExciseCreditUsed", 0);
                        rsAM.Set_Fields("ExciseTransferCharge", 0);
                        rsAM.Set_Fields("ExciseCreditMVR3", 0);
                        rsAM.Set_Fields("ExciseTaxDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
                        rsAM.Set_Fields("ExcisePaidDate", null);
                        rsAM.Set_Fields("ExcisePaidMVR3", 0);
                        rsAM.Set_Fields("RegHalf", false);
                        rsAM.Set_Fields("RegProrate", false);
                        rsAM.Set_Fields("ProrateMonth", 0);
                        rsAM.Set_Fields("RegRateFull", 0);
                        rsAM.Set_Fields("RegRateCharge", 0);
                        rsAM.Set_Fields("TransferCreditFull", 0);
                        rsAM.Set_Fields("TransferCreditUsed", 0);
                        rsAM.Set_Fields("DuplicateRegistrationFee", 0);
                        rsAM.Set_Fields("CommercialCredit", 0);
                        rsAM.Set_Fields("InitialFee", 0);
                        rsAM.Set_Fields("outofrotation", 0);
                        rsAM.Set_Fields("ReservePlateYN", false);
                        rsAM.Set_Fields("ReservePlate", 0);
                        rsAM.Set_Fields("PlateFeeNew", 0);
                        rsAM.Set_Fields("PlateFeeReReg", 0);
                        rsAM.Set_Fields("ReplacementFee", 0);
                        rsAM.Set_Fields("StickerFee", 0);
                        rsAM.Set_Fields("TransferFee", 0);
                        rsAM.Set_Fields("GIFTCERTIFICATEAMOUNT", 0);
                        rsAM.Set_Fields("GiftCertificateNumber", 0);
                        rsAM.Set_Fields("SalesTax", 0);
                        rsAM.Set_Fields("TitleFee", 0);
                        rsAM.Set_Fields("InfoCodes", "");
                        rsAM.Set_Fields("InfoMessage", "");
                        rsAM.Set_Fields("ECorrectInfoCodes", "");
                        rsAM.Set_Fields("ECorrectInfoMessage", "");
                        rsAM.Set_Fields("StatePaid", 0);
                        rsAM.Set_Fields("LocalPaid", 0);
                        rsAM.Set_Fields("ETO", false);
                        rsAM.Set_Fields("TwoYear", false);
                        rsAM.Set_Fields("MonthStickerCharge",
                            FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMonthCharge.Text)));
                        rsAM.Set_Fields("MonthStickerNoCharge",
                            FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMonthNoCharge.Text)));
                        rsAM.Set_Fields("MonthStickerNumber",
                            FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMStickerNumber.Text)));
                        rsAM.Set_Fields("MonthStickerMonth",
                            FCConvert.ToDateTime(frmLostPlate.InstancePtr.txtExpires.Text).Month);
                        rsAM.Set_Fields("YearStickerCharge",
                            FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYearCharge.Text)));
                        rsAM.Set_Fields("YearStickerNoCharge",
                            FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYearNoCharge.Text)));
                        rsAM.Set_Fields("YearStickerNumber",
                            FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYStickerNumber.Text)));
                        rsAM.Set_Fields("YearStickerYear",
                            Strings.Right(
                                FCConvert.ToString(FCConvert.ToDateTime(frmLostPlate.InstancePtr.txtExpires.Text).Year),
                                2));
                        rsAM.Set_Fields("StickerFee",
                            (rsAM.Get_Fields_Int32("MonthStickerCharge") + rsAM.Get_Fields_Int32("YearStickerCharge")) *
                            rr.Get_Fields_Decimal("Stickers"));
                        MotorVehicle.Statics.TransactionID = FCConvert.ToString(rsAM.Get_Fields_Int32("ID"));
                        rsAM.Update();
                        if (fecherFoundation.Strings.Trim(frmLostPlate.InstancePtr.txtYStickerNumber.Text) != "")
                        {
                            MotorVehicle.Statics.TempYear += FCConvert.ToString(
                                MotorVehicle.strPadZeros(
                                    fecherFoundation.Strings.Trim(frmLostPlate.InstancePtr.txtYStickerNumber.Text), 7));
                            if (MotorVehicle.IsMotorcycle(MotorVehicle.Statics.Class) &&
                                MotorVehicle.Statics.FixedMonth)
                            {
                                MotorVehicle.Statics.TempYear += "C";
                            }
                            else
                            {
                                if (Conversion.Val(frmLostPlate.InstancePtr.txtYearCharge.Text) +
                                    Conversion.Val(frmLostPlate.InstancePtr.txtYearNoCharge.Text) == 2)
                                {
                                    MotorVehicle.Statics.TempYear += "D";
                                }
                                else
                                {
                                    MotorVehicle.Statics.TempYear += "S";
                                }
                            }
                        }
                        else
                        {
                            MotorVehicle.Statics.TempYear = "";
                        }

                        if (fecherFoundation.Strings.Trim(frmLostPlate.InstancePtr.txtMStickerNumber.Text) != "")
                        {
                            MotorVehicle.Statics.TempMonth += FCConvert.ToString(
                                MotorVehicle.strPadZeros(
                                    fecherFoundation.Strings.Trim(frmLostPlate.InstancePtr.txtMStickerNumber.Text), 7));
                            if (Conversion.Val(frmLostPlate.InstancePtr.txtMonthCharge.Text) +
                                Conversion.Val(frmLostPlate.InstancePtr.txtMonthNoCharge.Text) == 2)
                            {
                                MotorVehicle.Statics.TempMonth += "D";
                            }
                            else
                            {
                                MotorVehicle.Statics.TempMonth += "S";
                            }
                        }
                        else
                        {
                            MotorVehicle.Statics.TempMonth = "";
                        }

                        rsException.OpenRecordset("SELECT * FROM ExceptionReport WHERE ID = 0");
                        if (Conversion.Val(frmLostPlate.InstancePtr.txtMonthCharge.Text) +
                            Conversion.Val(frmLostPlate.InstancePtr.txtMonthNoCharge.Text) > 0)
                        {
                            rsException.AddNew();
                            rsException.Set_Fields("Type", "1S");
                            rsException.Set_Fields("OriginalStickerNumber",
                                FCConvert.ToString(
                                    Conversion.Val(
                                        MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"))));
                            rsException.Set_Fields("OriginalMMYY", "M");
                            rsException.Set_Fields("OriginalMonthYear",
                                FCConvert.ToString(
                                    Conversion.Val(
                                        MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth"))));
                            rsException.Set_Fields("ReplacementMonthYear",
                                FCConvert.ToDateTime(frmLostPlate.InstancePtr.txtExpires.Text).Month);
                            rsException.Set_Fields("OriginalNumberOfStickers",
                                Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge")) +
                                Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNoCharge")));
                            rsException.Set_Fields("OriginalDoubleSingle",
                                FCConvert.ToString(
                                    Conversion.Val(rsException.Get_Fields_Int32("OriginalNumberOfStickers"))));
                            rsException.Set_Fields("ReplacementStickerNumber",
                                FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMStickerNumber.Text)));
                            rsException.Set_Fields("ReplacementMMYY", "M");
                            rsException.Set_Fields("ReplacementNumberOfStickers",
                                Conversion.Val(frmLostPlate.InstancePtr.txtMonthCharge.Text) +
                                Conversion.Val(frmLostPlate.InstancePtr.txtMonthNoCharge.Text));
                            rsException.Set_Fields("ReplacementDoubleSingle",
                                FCConvert.ToString(
                                    Conversion.Val(rsException.Get_Fields_Int32("ReplacementNumberOfStickers"))));
                            rsException.Set_Fields("OriginalClass",
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                            rsException.Set_Fields("OriginalPlate",
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                            rsException.Set_Fields("ExceptionReportDate", DateTime.Today);
                            rsException.Set_Fields("OpID", MotorVehicle.Statics.OpID);
                            rsException.Set_Fields("reason", fecherFoundation.Strings.Trim(tempstr));
                            rsException.Set_Fields("MVR3Number", 0);
                            rsException.Set_Fields("Fee",
                                Conversion.Val(frmLostPlate.InstancePtr.txtMonthCharge.Text) *
                                Conversion.Val(rr.Get_Fields_Decimal("Stickers")));
                            rsException.Set_Fields("AddOrSubtract", "+");
                            rsException.Set_Fields("MVR3Printed", false);
                            rsException.Set_Fields("PeriodCloseoutID", 0);
                            rsException.Set_Fields("TellerCloseoutID", 0);
                            rsException.Update();
                        }

                        if (Conversion.Val(frmLostPlate.InstancePtr.txtYearCharge.Text) +
                            Conversion.Val(frmLostPlate.InstancePtr.txtYearNoCharge.Text) > 0)
                        {
                            rsException.AddNew();
                            rsException.Set_Fields("Type", "1S");
                            rsException.Set_Fields("OriginalStickerNumber",
                                FCConvert.ToString(
                                    Conversion.Val(
                                        MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"))));
                            if (MotorVehicle.Statics.TempYear == "C")
                            {
                                rsException.Set_Fields("OriginalMMYY", "C");
                            }
                            else
                            {
                                rsException.Set_Fields("OriginalMMYY", "Y");
                            }

                            rsException.Set_Fields("OriginalMonthYear",
                                FCConvert.ToString(
                                    Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerYear"))));
                            rsException.Set_Fields("ReplacementMonthYear",
                                FCConvert.ToDateTime(frmLostPlate.InstancePtr.txtExpires.Text).Year);
                            rsException.Set_Fields("OriginalNumberOfStickers",
                                Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge")) +
                                Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNoCharge")));
                            rsException.Set_Fields("OriginalDoubleSingle",
                                FCConvert.ToString(
                                    Conversion.Val(rsException.Get_Fields_Int32("OriginalNumberOfStickers"))));
                            rsException.Set_Fields("ReplacementStickerNumber",
                                FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYStickerNumber.Text)));
                            if (MotorVehicle.Statics.TempYear == "C")
                            {
                                rsException.Set_Fields("ReplacementMMYY", "C");
                            }
                            else
                            {
                                rsException.Set_Fields("ReplacementMMYY", "Y");
                            }

                            rsException.Set_Fields("ReplacementNumberOfStickers",
                                Conversion.Val(frmLostPlate.InstancePtr.txtYearCharge.Text) +
                                Conversion.Val(frmLostPlate.InstancePtr.txtYearNoCharge.Text));
                            rsException.Set_Fields("ReplacementDoubleSingle",
                                FCConvert.ToString(
                                    Conversion.Val(rsException.Get_Fields_Int32("ReplacementNumberOfStickers"))));
                            rsException.Set_Fields("OriginalClass",
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                            rsException.Set_Fields("OriginalPlate",
                                MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                            rsException.Set_Fields("ExceptionReportDate", DateTime.Today);
                            rsException.Set_Fields("OpID", MotorVehicle.Statics.OpID);
                            rsException.Set_Fields("reason", fecherFoundation.Strings.Trim(tempstr));
                            rsException.Set_Fields("MVR3Number", 0);
                            rsException.Set_Fields("Fee",
                                Conversion.Val(frmLostPlate.InstancePtr.txtYearCharge.Text) *
                                Conversion.Val(rr.Get_Fields_Decimal("Stickers")));
                            rsException.Set_Fields("AddOrSubtract", "+");
                            rsException.Set_Fields("MVR3Printed", false);
                            rsException.Set_Fields("PeriodCloseoutID", 0);
                            rsException.Set_Fields("TellerCloseoutID", 0);
                            rsException.Update();
                        }

                        MotorVehicle.Statics.rsFinal.Edit();
                        if (Conversion.Val(frmLostPlate.InstancePtr.txtMonthCharge.Text) +
                            Conversion.Val(frmLostPlate.InstancePtr.txtMonthNoCharge.Text) > 0)
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerCharge",
                                FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMonthCharge.Text)));
                            MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNoCharge",
                                FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMonthNoCharge.Text)));
                            MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNumber",
                                FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtMStickerNumber.Text)));
                            MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerMonth",
                                FCConvert.ToDateTime(frmLostPlate.InstancePtr.txtExpires.Text).Month);
                        }
                        else
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerCharge", 0);
						MotorVehicle.Statics.rsFinal.Set_Fields("MonthStickerNoCharge", 0);
                        }

                        if (Conversion.Val(frmLostPlate.InstancePtr.txtYearCharge.Text) +
                            Conversion.Val(frmLostPlate.InstancePtr.txtYearNoCharge.Text) > 0)
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerCharge",
                                FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYearCharge.Text)));
                            MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNoCharge",
                                FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYearNoCharge.Text)));
                            MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNumber",
                                FCConvert.ToString(Conversion.Val(frmLostPlate.InstancePtr.txtYStickerNumber.Text)));
                            MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerYear",
                                Strings.Right(
                                    FCConvert.ToString(FCConvert.ToDateTime(frmLostPlate.InstancePtr.txtExpires.Text)
                                        .Year), 2));
                        }
                        else
                        {
                            MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerCharge", 0);
						MotorVehicle.Statics.rsFinal.Set_Fields("YearStickerNoCharge", 0);
                        }

                        MotorVehicle.Statics.rsFinal.Set_Fields("StickerFee",
                            (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge") + "") +
                             Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge") + "")) *
                            Conversion.Val(rr.Get_Fields_Decimal("Stickers")));
                        MotorVehicle.Statics.rsFinal.Set_Fields("TransactionType", "DPS");
                        MotorVehicle.Statics.rsFinal.Update();
                        if (MotorVehicle.Statics.bolFromDosTrio || MotorVehicle.Statics.bolFromWindowsCR)
                        {
                            MotorVehicle.Write_PDS_Work_Record_Stuff();
                        }

                        using (clsDRWrapper rsInventory = new clsDRWrapper())
                        {
							string MonthCode = "";
							string YearCode = "";
							// ****   INVENTORY ADJUSTMENT STARTS HERE ****
							if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber")) !=
								0 && (Conversion.Val(frmLostPlate.InstancePtr.txtMonthCharge.Text) +
									  Conversion.Val(frmLostPlate.InstancePtr.txtMonthNoCharge.Text) > 0))
							{
								MonthCode = "SM" + Strings.Right(MotorVehicle.Statics.TempMonth, 1) +
											Strings.Left(MotorVehicle.Statics.TempMonth, 2);
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Number = " +
														  MotorVehicle.Statics.rsFinal.Get_Fields_Int32(
															  "MonthStickerNumber") + " AND Code = '" + MonthCode + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									rsInventory.Edit();
									rsInventory.Set_Fields("InventoryDate", DateTime.Today);
									rsInventory.Set_Fields("OpID", MotorVehicle.Statics.OpID);
									rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
									rsInventory.Set_Fields("Status", "I");
									rsInventory.Set_Fields("PeriodCloseoutID", 0);
									rsInventory.Set_Fields("TellerCloseoutID", 0);
									rsInventory.Update();
								}

								rsInventory.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
								rsInventory.AddNew();
								rsInventory.Set_Fields("AdjustmentCode", "I");
								rsInventory.Set_Fields("DateofAdjustment", DateTime.Today);
								rsInventory.Set_Fields("QuantityAdjusted", 1);
								rsInventory.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeM);
								rsInventory.Set_Fields("Low",
									MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber").ToString());
								rsInventory.Set_Fields("High",
									MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber").ToString());
								rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
								rsInventory.Set_Fields("Reason",
									"Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" +
									MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
								rsInventory.Set_Fields("PeriodCloseoutID", 0);
								rsInventory.Set_Fields("TellerCloseoutID", 0);
								rsInventory.Update();
							}

							if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber")) !=
								0 && (Conversion.Val(frmLostPlate.InstancePtr.txtYearCharge.Text) +
									  Conversion.Val(frmLostPlate.InstancePtr.txtYearNoCharge.Text) > 0))
							{
								YearCode = "SY" + Strings.Right(MotorVehicle.Statics.TempYear, 1) +
										   Strings.Left(MotorVehicle.Statics.TempYear, 2);
								rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Number = " +
														  MotorVehicle.Statics.rsFinal
															  .Get_Fields_Int32("YearStickerNumber") + " AND Code = '" +
														  YearCode + "'");
								if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
								{
									rsInventory.MoveLast();
									rsInventory.MoveFirst();
									rsInventory.Edit();
									rsInventory.Set_Fields("InventoryDate", DateTime.Today);
									rsInventory.Set_Fields("OpID", MotorVehicle.Statics.OpID);
									rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
									rsInventory.Set_Fields("Status", "I");
									rsInventory.Set_Fields("PeriodCloseoutID", 0);
									rsInventory.Set_Fields("TellerCloseoutID", 0);
									rsInventory.Update();
								}

								rsInventory.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
								rsInventory.AddNew();
								rsInventory.Set_Fields("AdjustmentCode", "I");
								rsInventory.Set_Fields("DateofAdjustment", DateTime.Today);
								rsInventory.Set_Fields("QuantityAdjusted", 1);
								rsInventory.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeY);
								rsInventory.Set_Fields("Low",
									MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber").ToString());
								rsInventory.Set_Fields("High",
									MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber").ToString());
								rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
								rsInventory.Set_Fields("Reason",
									"Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" +
									MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
								rsInventory.Set_Fields("PeriodCloseoutID", 0);
								rsInventory.Set_Fields("TellerCloseoutID", 0);
								rsInventory.Update();
							}
						}
                    }
                }
                else
                {
                    MessageBox.Show("DefaultInfo Table is empty.  Please call TRIO", "Error In DefaultInfo Table",
                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }

                frmPlateInfo.InstancePtr.Unload();
                frmLostPlate.InstancePtr.Unload();
                if (MotorVehicle.Statics.bolFromWindowsCR)
                {
                    MDIParent.InstancePtr.Menu18();
                }
            }
            finally
            {
                rsAM.Dispose();
                rr.Dispose();
                rsClass.Dispose();
                rsException.Dispose();
			}
        }

		private string Get_Name_Plate_ID(ref clsDRWrapper rr1)
		{
			string Get_Name_Plate_ID = "";
			// 
			bool bolReg2;
			bool FormMax = false;
			string strOwner1;
			string strOwner2;
			string strOwner3;
			strOwner1 = MotorVehicle.GetPartyNameMiddleInitial(rr1.Get_Fields_Int32("PartyID1"), true);
			strOwner2 = MotorVehicle.GetPartyNameMiddleInitial(rr1.Get_Fields_Int32("PartyID2"), true);
			strOwner3 = MotorVehicle.GetPartyNameMiddleInitial(rr1.Get_Fields_Int32("PartyID3"), true);
			bolReg2 = false;
			Get_Name_Plate_ID = "";
			// 
			if (this.WindowState == FormWindowState.Maximized)
			{
				FormMax = true;
			}
			else
			{
				FormMax = false;
			}
			if (cmbSearchBy.Text == "Plate" || cmbSearchBy.Text == "Year Sticker" || cmbSearchBy.Text == "Month Sticker")
			{
				if (fecherFoundation.Strings.Trim(strOwner1) != "")
				{
					Get_Name_Plate_ID = fecherFoundation.Strings.Trim(strOwner1);
				}
				if (fecherFoundation.Strings.Trim(Get_Name_Plate_ID) != "")
					goto FoundTag;
				if (fecherFoundation.Strings.Trim(strOwner2) != "")
				{
					Get_Name_Plate_ID = fecherFoundation.Strings.Trim(strOwner2);
					bolReg2 = true;
				}
				if (fecherFoundation.Strings.Trim(Get_Name_Plate_ID) != "")
					goto FoundTag;
				if (fecherFoundation.Strings.Trim(strOwner3) != "")
				{
					Get_Name_Plate_ID = fecherFoundation.Strings.Trim(strOwner3);
					bolReg2 = true;
				}
				if (fecherFoundation.Strings.Trim(Get_Name_Plate_ID) != "")
					goto FoundTag;
			}
			// OWNER1
			if (fecherFoundation.Strings.Trim(strOwner1) != "")
			{
				if (cmbSearchBy.Text == "Name")
				{
					if (Strings.Mid(strOwner1, 1, txtCriteria.Text.Length) == txtCriteria.Text)
					{
						Get_Name_Plate_ID = fecherFoundation.Strings.Trim(strOwner1);
					}
				}
				else
				{
					Get_Name_Plate_ID = fecherFoundation.Strings.Trim(strOwner1);
				}
			}
			if (fecherFoundation.Strings.Trim(Get_Name_Plate_ID) != "")
				goto FoundTag;
			// OWNER2
			if (fecherFoundation.Strings.Trim(strOwner2) != "")
			{
				if (cmbSearchBy.Text == "Name")
				{
					if (Strings.Mid(strOwner2, 1, txtCriteria.Text.Length) == txtCriteria.Text)
					{
						Get_Name_Plate_ID = fecherFoundation.Strings.Trim(strOwner2);
					}
					bolReg2 = true;
				}
				else
				{
					Get_Name_Plate_ID = fecherFoundation.Strings.Trim(strOwner2);
					bolReg2 = true;
				}
			}
			// OWNER3
			if (fecherFoundation.Strings.Trim(strOwner3) != "")
			{
				if (cmbSearchBy.Text == "Name")
				{
					if (Strings.Mid(strOwner3, 1, txtCriteria.Text.Length) == txtCriteria.Text)
					{
						Get_Name_Plate_ID = fecherFoundation.Strings.Trim(strOwner3);
					}
					bolReg2 = true;
				}
				else
				{
					Get_Name_Plate_ID = fecherFoundation.Strings.Trim(strOwner3);
					bolReg2 = true;
				}
			}
			FoundTag:
			;
			if (FormMax)
			{
				if (Get_Name_Plate_ID.Length <= 34)
				{
					Get_Name_Plate_ID = Get_Name_Plate_ID + Strings.StrDup(34 - Get_Name_Plate_ID.Length, " ");
				}
				else
				{
					Get_Name_Plate_ID = Strings.Mid(Get_Name_Plate_ID, 1, 34);
				}
				Get_Name_Plate_ID = Get_Name_Plate_ID + "  " + fecherFoundation.Strings.Trim(rr1.Get_Fields_String("plate")) + Strings.StrDup(8 - fecherFoundation.Strings.Trim(rr1.Get_Fields_String("plate")).Length, " ") + "  " + Strings.Format(rr1.Get_Fields_Int32("ID"), "000000");
			}
			else
			{
				if (Get_Name_Plate_ID.Length <= 28)
				{
					Get_Name_Plate_ID = Get_Name_Plate_ID + Strings.StrDup(28 - Get_Name_Plate_ID.Length, " ");
				}
				else
				{
					Get_Name_Plate_ID = Strings.Mid(Get_Name_Plate_ID, 1, 28);
				}
				Get_Name_Plate_ID = Get_Name_Plate_ID + " " + fecherFoundation.Strings.Trim(rr1.Get_Fields_String("plate")) + Strings.StrDup(8 - fecherFoundation.Strings.Trim(rr1.Get_Fields_String("plate")).Length, " ") + " " + Strings.Format(rr1.Get_Fields_Int32("ID"), "000000");
			}
			// If Sec Owner Flag
			if (FormMax)
			{
				if (bolReg2 == true)
				{
					Get_Name_Plate_ID = Get_Name_Plate_ID + "**  ";
				}
				else
				{
					Get_Name_Plate_ID = Get_Name_Plate_ID + "    ";
				}
			}
			else
			{
				if (bolReg2 == true)
				{
					Get_Name_Plate_ID = Get_Name_Plate_ID + "** ";
				}
				else
				{
					Get_Name_Plate_ID = Get_Name_Plate_ID + "   ";
				}
			}
			// Expiration Date
			if (FormMax)
			{
				if (!rr1.IsFieldNull("ExpireDate"))
				{
					Get_Name_Plate_ID = Get_Name_Plate_ID + Strings.Format(rr1.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy") + "    ";
				}
				else
				{
					Get_Name_Plate_ID = Get_Name_Plate_ID + "              ";
				}
			}
			else
			{
				if (!rr1.IsFieldNull("ExpireDate"))
				{
					Get_Name_Plate_ID = Get_Name_Plate_ID + Strings.Format(rr1.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy") + "  ";
				}
				else
				{
					Get_Name_Plate_ID = Get_Name_Plate_ID + "            ";
				}
			}
			// Class
			if (fecherFoundation.Strings.Trim(rr1.Get_Fields_String("Class")) != "")
			{
				Get_Name_Plate_ID = Get_Name_Plate_ID + rr1.Get_Fields_String("Class") + "/";
			}
			else
			{
				Get_Name_Plate_ID = Get_Name_Plate_ID + "  /";
			}
			// SubClass
			if (fecherFoundation.Strings.Trim(rr1.Get_Fields_String("Subclass")) != "")
			{
				Get_Name_Plate_ID = Get_Name_Plate_ID + rr1.Get_Fields_String("Subclass") + "  ";
			}
			else
			{
				Get_Name_Plate_ID = Get_Name_Plate_ID + "    ";
			}
			// Year
			if (fecherFoundation.Strings.Trim(rr1.Get_Fields("Year")) != "")
			{
				Get_Name_Plate_ID = Get_Name_Plate_ID + Strings.Format(rr1.Get_Fields("Year"), "0000") + "-";
			}
			else
			{
				Get_Name_Plate_ID = Get_Name_Plate_ID + "    -";
			}
			// Make
			if (fecherFoundation.Strings.Trim(rr1.Get_Fields_String("make")) != "")
			{
				Get_Name_Plate_ID = Get_Name_Plate_ID + Strings.Format(rr1.Get_Fields_String("make"), "    ") + "-";
			}
			else
			{
				Get_Name_Plate_ID = Get_Name_Plate_ID + "    -";
			}
			// Model
			if (fecherFoundation.Strings.Trim(rr1.Get_Fields_String("model")) != "")
			{
				Get_Name_Plate_ID = Get_Name_Plate_ID + Strings.Format(rr1.Get_Fields_String("model"), "      ");
			}
			else
			{
				Get_Name_Plate_ID = Get_Name_Plate_ID + "      ";
			}
			return Get_Name_Plate_ID;
		}

		private int intClassIndex(string str1)
		{
			int intClassIndex = 0;
			int jj;
			intClassIndex = -1;
			for (jj = 0; jj <= cboClass.Items.Count - 1; jj++)
			{
				if (fecherFoundation.Strings.Trim(cboClass.Items[jj].ToString()) == fecherFoundation.Strings.Trim(str1))
				{
					intClassIndex = jj;
					break;
				}
			}
			return intClassIndex;
		}

		private bool ValidateExpDate(ref clsDRWrapper rs)
		{
			bool ValidateExpDate = false;
			if ((Information.IsDate(rs.Get_Fields("ExpireDate")) && rs.Get_Fields_DateTime("ExpireDate").ToOADate() >= DateTime.Today.ToOADate())
                || (!Information.IsDate(rs.Get_Fields("ExpireDate")) && rs.Get_Fields("ExpireDate") == ""))
			{
				ValidateExpDate = true;
			}
			else
			{
				ValidateExpDate = false;
			}
			return ValidateExpDate;
		}

		private void FillFleetCombo()
		{
            using (clsDRWrapper rsFleet = new clsDRWrapper())
            {
                cboFleets.Clear();
                if (cmbGroup.Text == "Fleet")
                {
                    if (cmbLongTerm.Text == "Long Term")
                    {
                        rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type = 'L' AND ExpiryMonth <> 99 ORDER BY p.FullName");
                    }
                    else
                    {
                        rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type <> 'L' AND ExpiryMonth <> 99 ORDER BY p.FullName");
                    }
                }
                else
                {
                    if (cmbLongTerm.Text == "Long Term")
                    {
                        rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type = 'L' AND ExpiryMonth = 99 ORDER BY p.FullName");
                    }
                    else
                    {
                        rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type <> 'L' AND ExpiryMonth = 99 ORDER BY p.FullName");
                    }
                }
                if (rsFleet.EndOfFile() != true && rsFleet.BeginningOfFile() != true)
                {
                    rsFleet.MoveLast();
                    rsFleet.MoveFirst();
                    do
                    {
                        cboFleets.AddItem(rsFleet.Get_Fields_Int32("FleetNumber") + Strings.Space(6 - FCConvert.ToString(rsFleet.Get_Fields_Int32("FleetNumber")).Length) + fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleet.Get_Fields_String("FullName"))));
                        rsFleet.MoveNext();
                    }
                    while (rsFleet.EndOfFile() != true);
                }
			}
        }

		private void vsResults_DblClick(object sender, System.EventArgs e)
		{
			string strCheck = "";
			cPartyController pCont = new cPartyController();
			cParty pInfo;
			EnableDisableForm(true);
			fraResults.Visible = false;
			fraSelection.Visible = false;
			if (MotorVehicle.Statics.RegistrationType == "HELD")
			{
				if (intPlateIndex == 0)
				{
					strCheck = vsResults.TextMatrix(vsResults.MouseRow, 0);
					MotorVehicle.Statics.rsPlateForReg.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE ID = " + FCConvert.ToString(Conversion.Val(strCheck)) + "AND Status = 'H'");
					MotorVehicle.Statics.rsPlateForReg.MoveLast();
					MotorVehicle.Statics.rsPlateForReg.MoveFirst();
					if (MotorVehicle.Statics.rsPlateForReg.RecordCount() == 0)
					{
						MessageBox.Show("There was no Held Registration found for the Plate  " + MotorVehicle.Statics.Plate1);
						return;
					}
					MotorVehicle.Statics.Plate1 = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
					txtPlateType[0].Text = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("plate"));
					MotorVehicle.Statics.strSql = "SELECT * FROM HeldRegistrationMaster WHERE ID = " + FCConvert.ToString(Conversion.Val(strCheck)) + " AND Status = 'FC'";
					MotorVehicle.Statics.rsFinalCompare.OpenRecordset(MotorVehicle.Statics.strSql);
					if (MotorVehicle.Statics.rsFinalCompare.EndOfFile() != true && MotorVehicle.Statics.rsFinalCompare.BeginningOfFile() != true)
					{
						MotorVehicle.Statics.rsFinalCompare.MoveLast();
						MotorVehicle.Statics.rsFinalCompare.MoveFirst();
						pInfo = pCont.GetParty(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID1"));
						MotorVehicle.Statics.orgOwnerInfo.Owner1Name = fecherFoundation.Strings.UCase(pInfo.FullName);
						if (Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID2")) != 0)
						{
							pInfo = pCont.GetParty(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID2"));
							MotorVehicle.Statics.orgOwnerInfo.Owner2Name = fecherFoundation.Strings.UCase(pInfo.FullName);
						}
						else
						{
							MotorVehicle.Statics.orgOwnerInfo.Owner2Name = "";
						}
						if (Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID3")) != 0)
						{
							pInfo = pCont.GetParty(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("PartyID3"));
							MotorVehicle.Statics.orgOwnerInfo.Owner3Name = fecherFoundation.Strings.UCase(pInfo.FullName);
						}
						else
						{
							MotorVehicle.Statics.orgOwnerInfo.Owner3Name = "";
						}
					}
					MotorVehicle.Statics.Class = FCConvert.ToString(MotorVehicle.Statics.rsPlateForReg.Get_Fields_String("Class"));
					cboClass.SelectedIndex = intClassIndex(MotorVehicle.Statics.Class);
					SettingValues = false;
					goto endofplatetag;
				}
			}
			SetPlateFromSearch();
			if (!PlateFromSearch)
			{
				if (cmdProcess.Enabled == true)
					cmdProcess.Enabled = false;
				if (cboLongTermRegPlateYear.Visible)
				{
					cboLongTermRegPlateYear.Focus();
				}
				else
				{
					txtPlateType[0].Focus();
				}
				return;
			}
			if (MotorVehicle.Statics.rsPlateForReg.IsntAnything())
			{
				if (cmdProcess.Enabled == true)
					cmdProcess.Enabled = false;
				if (cboLongTermRegPlateYear.Visible)
				{
					cboLongTermRegPlateYear.Focus();
				}
				else
				{
					txtPlateType[0].Focus();
				}
				return;
			}
			if (MotorVehicle.Statics.RegistrationType != "UPD" && MotorVehicle.Statics.RegistrationType != "BOOST")
			{
				clsDRWrapper temp = MotorVehicle.Statics.rsPlateForReg;
				if (intPlateIndex == 0)
				{
					MotorVehicle.TransferRecordToArchive(ref temp, intPlateIndex);
				}
				else if (intPlateIndex == 1)
				{
					MotorVehicle.TransferRecordToArchive(ref temp, intPlateIndex);
				}
				else
				{
					MotorVehicle.TransferRecordToArchive(ref temp, intPlateIndex);
				}
				MotorVehicle.Statics.rsPlateForReg = temp;
			}
			endofplatetag:
			;
			if (cmdProcess.Enabled == true)
			{
				if (txtPlateType[1].Visible == true || cboLongTermRegPlateYear.Visible == true)
				{
					if (MotorVehicle.Statics.rsSecondPlate.IsntAnything())
					{
						txtPlateType[1].Focus();
					}
					else
					{
						if (txtPlateType[2].Visible == true)
						{
							if (MotorVehicle.Statics.rsThirdPlate.IsntAnything())
							{
								txtPlateType[2].Focus();
							}
							else
							{
								cmdProcess_Click();
							}
						}
						else
						{
							cmdProcess_Click();
						}
					}
				}
				else
				{
					cmdProcess_Click();
				}
			}
		}

		private void vsVehicles_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsVehicles.TextMatrix(vsVehicles.Row, 0) != "Registered" && vsVehicles.TextMatrix(vsVehicles.Row, 0) != "Can't Register")
			{
				if (FCConvert.CBool(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) == true)
				{
					vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(false));
					SetVehicleStatus();
				}
				else
				{
					vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(true));
					SetVehicleStatus();
				}
			}
		}

		private void vsVehicles_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsVehicles.TextMatrix(vsVehicles.Row, 0) != "Registered" && vsVehicles.TextMatrix(vsVehicles.Row, 0) != "Can't Register")
				{
					if (FCConvert.CBool(vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol)) == true)
					{
						vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(false));
						SetVehicleStatus();
					}
					else
					{
						vsVehicles.TextMatrix(vsVehicles.Row, RegisterCol, FCConvert.ToString(true));
						SetVehicleStatus();
					}
				}
			}
		}

		private void SetVehicleStatus()
		{
			int counter;
			int counter2;
			bool FoundNext;
			StartAgain:
			;
			FoundNext = false;
			for (counter = 1; counter <= vsVehicles.Rows - 1; counter++)
			{
				if (vsVehicles.TextMatrix(counter, 0) == "Next")
				{
					if (FoundNext)
					{
						vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, Color.Red);
						vsVehicles.TextMatrix(counter, 0, "Unregistered");
					}
					else
					{
						FoundNext = true;
						if (FCConvert.CBool(vsVehicles.TextMatrix(counter, RegisterCol)) == false)
						{
							vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, Color.Black);
							vsVehicles.TextMatrix(counter, 0, "Not Registering");
							cmdRegister.Enabled = false;
							for (counter2 = 1; counter2 <= vsVehicles.Rows - 1; counter2++)
							{
								if (FCConvert.CBool(vsVehicles.TextMatrix(counter2, RegisterCol)) == true && vsVehicles.TextMatrix(counter2, 0) == "Unregistered")
								{
									vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter2, 0, 0xFF0000);
									vsVehicles.TextMatrix(counter2, 0, "Next");
									cmdRegister.Enabled = true;
									goto StartAgain;
								}
							}
						}
					}
				}
				else if (vsVehicles.TextMatrix(counter, 0) == "Unregistered")
				{
					if (FCConvert.CBool(vsVehicles.TextMatrix(counter, RegisterCol)) == false)
					{
						vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, Color.Black);
						vsVehicles.TextMatrix(counter, 0, "Not Registering");
					}
					else if (!FoundNext)
					{
						FoundNext = true;
						vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, 0xFF0000);
						vsVehicles.TextMatrix(counter, 0, "Next");
					}
				}
				else if (vsVehicles.TextMatrix(counter, 0) == "Not Registering")
				{
					if (FCConvert.CBool(vsVehicles.TextMatrix(counter, RegisterCol)) == true)
					{
						if (!FoundNext)
						{
							vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, 0xFF0000);
							vsVehicles.TextMatrix(counter, 0, "Next");
							FoundNext = true;
							if (cmdRegister.Enabled == false)
							{
								cmdRegister.Enabled = true;
							}
						}
						else
						{
							vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, Color.Red);
							vsVehicles.TextMatrix(counter, 0, "Unregistered");
						}
					}
				}
			}
			if (vsVehicles.Visible == true)
			{
				vsVehicles.Focus();
			}
			Form_Resize();
		}
		private Decimal CalcStateFee(clsDRWrapper rs)
		{
			Decimal CalcStateFee = 0;
			Decimal RegFee;
			Decimal InitFee;
			Decimal CommCred;
			Decimal SpecialFee;
			int tempweight = 0;
			if (FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")) != "SE")
			{
				if (Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("RegisteredWeightNew")) == 0)
				{
					tempweight = 0;
				}
				else
				{
					tempweight = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("RegisteredWeightNew")));
				}
			}
			else
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields("NetWeight"))) == "")
				{
					tempweight = 0;
				}
				else
				{
					tempweight = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields("NetWeight")));
				}
			}

            var specialtyPlate =
                MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class"));

            if ((specialtyPlate == "BB") || specialtyPlate == "BH" || (specialtyPlate == "LB") || (specialtyPlate == "CR") || (specialtyPlate == "UM") || (specialtyPlate == "AG") || (specialtyPlate == "AC") || (specialtyPlate == "AF") || (specialtyPlate == "TS") || (specialtyPlate == "BC") || (specialtyPlate == "AW") || (specialtyPlate == "LC"))
			{
				SpecialFee = 15;
			}
			else if (MotorVehicle.GetSpecialtyPlate(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")) == "SW")
			{
				SpecialFee = 20;
			}
			else
			{
				SpecialFee = 0;
			}
			RegFee = GetReg(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class"), MotorVehicle.Statics.rsVehicles.Get_Fields_String("Subclass"), tempweight);
			if (MotorVehicle.Statics.rsVehicles.Get_Fields_Boolean("RENTAL") && (FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")) == "CR" || FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")) == "UM" || (FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class")) == "PC" && FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Subclass")) != "P5")))
			{
				RegFee *= 2;
			}
			if (cmbGroup.Text == "Fleet")
			{
				if (!(MotorVehicle.Statics.rsVehicles.IsFieldNull("ExpireDate")))
				{
					if (FCConvert.ToDateTime(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate")).Month != ExpMonth)
					{
						RegFee = FCConvert.ToDecimal(Strings.Format(Pro(ref RegFee, DateDiff()), "##0.00"));
					}
				}
				else
				{
					WrongFee = true;
					RegFee = 0;
				}
			}
			InitFee = MotorVehicle.GetInitialPlateFees2(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Class"), MotorVehicle.Statics.rsVehicles.Get_Fields_String("Plate"));
			CommCred = GetComm();
			CalcStateFee = RegFee - CommCred;
			if (CalcStateFee < 0)
				CalcStateFee = 0;
			CalcStateFee += InitFee + SpecialFee;
			return CalcStateFee;
		}
		private Decimal CalcLocalFee(clsDRWrapper rs)
		{
			Decimal CalcLocalFee = 0;
			int MilYear = 0;
			float MilRate = 0;
			double Base = 0;
			Decimal Excise;
			float AgentFee = 0;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			string tempRes = "";
			clsDRWrapper rsExciseExempt = new clsDRWrapper();

            try
            {
                rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultInfo");
                if (FCConvert.ToString(rs.Get_Fields_String("ResidenceCode")).Length < 5)
                {
                    tempRes = "0" + rs.Get_Fields_String("ResidenceCode");
                }
                else
                {
                    tempRes = FCConvert.ToString(rs.Get_Fields_String("ResidenceCode"));
                }

                if (FCConvert.ToString(rs.Get_Fields_String("Class")) != "AP")
                {
                    if (tempRes == FCConvert.ToString(rsDefaultInfo.Get_Fields_String("ResidenceCode")))
                    {
                        AgentFee = FCConvert.ToSingle(rsDefaultInfo.Get_Fields_Decimal("AgentFeeReRegLocal"));
                    }
                    else
                    {
                        AgentFee = FCConvert.ToSingle(rsDefaultInfo.Get_Fields_Decimal("AgentFeeReRegOOT"));
                    }
                }
                else
                {
                    AgentFee = 0;
                }

                rsExciseExempt.OpenRecordset(
                    "SELECT * FROM Class WHERE BMVCode = '" + rs.Get_Fields_String("Class") + "' AND SystemCode = '" +
                    rs.Get_Fields_String("Subclass") + "'", "TWMV0000.vb1");
                if (rsExciseExempt.EndOfFile() != true && rsExciseExempt.BeginningOfFile() != true)
                {
                    if (FCConvert.ToString(rsExciseExempt.Get_Fields_String("ExciseRequired")) == "N")
                    {
                        Excise = 0;
                    }
                    else
                    {
                        if (FCConvert.ToInt32(
                                Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("MillYear"))) == 0)
                        {
                            MilYear = DateTime.Today.Year - rs.Get_Fields("Year") + 1;
                            WrongFee = true;
                        }
                        else
                        {
                            MilYear = MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("MillYear") + 1;
                        }

                        if (MilYear > 6)
                        {
                            MilYear = 6;
                        }

                        switch (MilYear)
                        {
                            case 1:
                            {
                                MilRate = 0.024f;
                                break;
                            }

                            case 2:
                            {
                                MilRate = 0.0175f;
                                break;
                            }

                            case 3:
                            {
                                MilRate = 0.0135f;
                                break;
                            }

                            case 4:
                            {
                                MilRate = 0.01f;
                                break;
                            }

                            case 5:
                            {
                                MilRate = 0.0065f;
                                break;
                            }

                            case 6:
                            {
                                MilRate = 0.004f;
                                break;
                            }
                        }

                        Base = rs.Get_Fields_Double("BasePrice");
                        if (Base == 0)
                        {
                            WrongFee = true;
                        }

                        Excise = FCConvert.ToDecimal(Strings.Format(MilRate * Base, "#0.00"));
                        if (cmbGroup.Text == "Fleet")
                        {
                            if (!MotorVehicle.Statics.rsVehicles.IsFieldNull("ExpireDate"))
                            {
                                if (FCConvert.ToDateTime(
                                        MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate")).Month !=
                                    ExpMonth)
                                {
                                    Excise = FCConvert.ToDecimal(
                                        Strings.Format(Pro(ref Excise, DateDiff()), "#,##0.00"));
                                }
                            }
                            else
                            {
                                WrongFee = true;
                            }
                        }

                        if (Excise < 5)
                            Excise = 5;
                    }
                }
                else
                {
                    if (FCConvert.ToInt32(
                            Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("MillYear"))) == 0)
                    {
                        MilYear = DateTime.Today.Year - FCConvert.ToInt32(Conversion.Val(rs.Get_Fields("Year"))) + 1;
                        WrongFee = true;
                    }
                    else
                    {
                        MilYear = FCConvert.ToInt32(
                                      Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("MillYear"))) + 1;
                    }

                    if (MilYear > 6)
                    {
                        MilYear = 6;
                    }

                    switch (MilYear)
                    {
                        case 1:
                        {
                            MilRate = 0.024f;
                            break;
                        }

                        case 2:
                        {
                            MilRate = 0.0175f;
                            break;
                        }

                        case 3:
                        {
                            MilRate = 0.0135f;
                            break;
                        }

                        case 4:
                        {
                            MilRate = 0.01f;
                            break;
                        }

                        case 5:
                        {
                            MilRate = 0.0065f;
                            break;
                        }

                        case 6:
                        {
                            MilRate = 0.004f;
                            break;
                        }
                    }

                    //end switch
                    Base = rs.Get_Fields_Double("BasePrice");
                    if (Base == 0)
                    {
                        WrongFee = true;
                    }

                    Excise = FCConvert.ToDecimal(Strings.Format(MilRate * Base, "#0.00"));
                    if (cmbGroup.Text == "Fleet")
                    {
                        if (!(MotorVehicle.Statics.rsVehicles.IsFieldNull("ExpireDate")))
                        {
                            if (FCConvert.ToDateTime(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate"))
                                    .Month != ExpMonth)
                            {
                                Excise = FCConvert.ToDecimal(Strings.Format(Pro(ref Excise, DateDiff()), "#,##0.00"));
                            }
                        }
                        else
                        {
                            WrongFee = true;
                        }
                    }

                    if (Excise < 5)
                        Excise = 5;
                }

                CalcLocalFee = FCConvert.ToDecimal(Strings.Format(FCConvert.ToSingle(Excise) + AgentFee, "#0.00"));
                return CalcLocalFee;
            }
            finally
            {
                rsDefaultInfo.Dispose();
                rsExciseExempt.Dispose();
			}
			
		}
		private Decimal GetReg(string Class, string Subclass, int weight)
		{
			Decimal GetReg = 0;
			int WT;
			clsDRWrapper rsClassFee = new clsDRWrapper();
			clsDRWrapper rsGVWFee = new clsDRWrapper();

            try
            {
                GetReg = 0;
                WT = weight;
                if (FCConvert.ToString(Class) != "AM" && Class != "BH" && FCConvert.ToString(Class) != "BU" &&
                    FCConvert.ToString(Class) != "CL" && FCConvert.ToString(Class) != "DV" &&
                    FCConvert.ToString(Class) != "FD" && FCConvert.ToString(Class) != "IU" &&
                    FCConvert.ToString(Class) != "TL" && FCConvert.ToString(Class) != "MC" &&
                    FCConvert.ToString(Class) != "PC" && FCConvert.ToString(Class) != "TR" &&
                    FCConvert.ToString(Class) != "VT" && FCConvert.ToString(Class) != "XV" &&
                    FCConvert.ToString(Class) != "VX" && FCConvert.ToString(Class) != "EM")
                {
                    Subclass = Class;
                }

                MotorVehicle.Statics.strSql = "SELECT * FROM CLASS WHERE BMVCode = '" + Class +
                                              "' And SystemCode =  '" + Subclass + "'";
                rsClassFee.OpenRecordset(MotorVehicle.Statics.strSql);
                if (rsClassFee.EndOfFile() != true && rsClassFee.BeginningOfFile() != true)
                {
                    rsClassFee.MoveLast();
                    rsClassFee.MoveFirst();
                    if (fecherFoundation.FCUtils.IsNull(rsClassFee.Get_Fields("RegistrationFee")) == false)
                    {
                        if (rsClassFee.Get_Fields_Double("RegistrationFee") == 0.01 ||
                            rsClassFee.Get_Fields_Double("RegistrationFee") == 0.02 ||
                            rsClassFee.Get_Fields_Double("RegistrationFee") == 0.03)
                        {
                            string tempClass = "";
                            tempClass = "FM";
                            if (rsClassFee.Get_Fields_Double("RegistrationFee") == 0.01)
                            {
                                tempClass = "TK";
                            }
                            else if (rsClassFee.Get_Fields_Double("RegistrationFee") == 0.03 && WT > 54000 &&
                                     FCConvert.ToString(Class) != "SE")
                            {
                                tempClass = "F2";
                            }
                            else if (Class == "SE")
                            {
                                tempClass = "SE";
                            }

                            MotorVehicle.Statics.strSql =
                                "SELECT * FROM GrossVehicleWeight WHERE Type = '" + tempClass + "' AND Low <= " +
                                FCConvert.ToString(WT) + " AND High >= " + FCConvert.ToString(WT);
                            rsGVWFee.OpenRecordset(MotorVehicle.Statics.strSql);
                            if (rsGVWFee.EndOfFile() != true && rsGVWFee.BeginningOfFile() != true)
                            {
                                rsGVWFee.MoveLast();
                                rsGVWFee.MoveFirst();
                                GetReg = FCConvert.ToDecimal(rsGVWFee.Get_Fields("Fee"));
                                if (GetReg == 0)
                                    GetReg = 35;
                                return GetReg;
                            }
                        }

                        GetReg = FCConvert.ToDecimal(rsClassFee.Get_Fields("RegistrationFee"));
                    }
                    else
                    {
                        GetReg = 0;
                    }
                }
                else
                {
                    WrongFee = true;
                }

                return GetReg;
            }
            finally
            {
                rsClassFee.Dispose();
                rsGVWFee.Dispose();
			}
            
		}
		private Decimal GetComm()
		{
			Decimal GetComm = 0;
			if (Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_String("plate")) > 799999 && Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_String("plate")) < 900000)
			{
				if (Conversion.Val(MotorVehicle.Statics.rsVehicles.Get_Fields_Int32("RegisteredWeightNew")) > 23000)
				{
					if (Strings.Mid(FCConvert.ToString(MotorVehicle.Statics.rsVehicles.Get_Fields_String("Style")), 2, 1) == "5")
					{
						GetComm = 40;
					}
					else
					{
						GetComm = 0;
					}
				}
			}
			return GetComm;
		}

		private Decimal Pro(ref Decimal x, int y)
		{
			Decimal Pro = 0;
			double total;
			total = FCConvert.ToDouble(x / 12);
			total *= y;
			Pro = FCConvert.ToDecimal(total);
			return Pro;
		}

		private int DateDiff()
		{
			int DateDiff = 0;
			int x = 0;
			if (FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate").Month) < ExpMonth)
			{
				x = ExpMonth - FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate").Month) + 1;
			}
			else
			{
				x = 12 - (FCConvert.ToInt32(MotorVehicle.Statics.rsVehicles.Get_Fields_DateTime("ExpireDate").Month) - ExpMonth) - 1;
			}
			if (x == 0)
			{
				x = 12;
			}
			DateDiff = x;
			return DateDiff;
		}

		private void GetFleetInfo()
		{
			using (clsDRWrapper rsFleetInfo = new clsDRWrapper())
			{
                FleetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(fecherFoundation.Strings.Trim(Strings.Mid(cboFleets.Text, 1, 5)))));
                rsFleetInfo.OpenRecordset("SELECT * FROM FleetMaster WHERE FleetNumber = " + FCConvert.ToString(FleetNumber));
                ExpMonth = FCConvert.ToInt16(rsFleetInfo.Get_Fields_Int32("ExpiryMonth"));
			}
        }

		private void MakePreRegCode(string x, int numberOfStickers, string suffix)
		{
			if (x == "M")
			{
				if (numberOfStickers > 1)
				{
					MotorVehicle.Statics.strCodeM = "SMD" + suffix;
                }
				else
				{
					MotorVehicle.Statics.strCodeM = "SMS" + suffix;
                }
			}
			else
			{
				if (numberOfStickers > 1)
				{
					MotorVehicle.Statics.strCodeY = "SYD" + suffix;
				}
				else
				{
					MotorVehicle.Statics.strCodeY = "SYS" + suffix;
				}
			}
		}

		public void Write_Pending_PDS_Work_Record_Stuff()
		{
			string strRec3 = new string('\0', 241);
			string strRec5A = new string('\0', 241);
			clsDRWrapper rsCheck = new clsDRWrapper();
			bool AddToExcise = false;
			clsDRWrapper rsResCheck = new clsDRWrapper();

            try
            {
                rsCheck.OpenRecordset("SELECT * FROM DefaultInfo");
                if (FCConvert.ToString(rsCheck.Get_Fields_String("ExciseVSAgent")) == "A")
                {
                    AddToExcise = false;
                }
                else
                {
                    AddToExcise = true;
                }

                if (MotorVehicle.Statics.bolFromWindowsCR)
                {
                    if (modRegionalTown.IsRegionalTown())
                    {
                        rsResCheck.OpenRecordset("SELECT * FROM tblRegions WHERE ResCode = '" + strResCodeChecker + "'",
                            "CentralData");
                        if (rsResCheck.EndOfFile() != true && rsResCheck.BeginningOfFile() != true)
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                                modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase",
                                FCConvert.ToString(rsResCheck.Get_Fields_Int32("ID")));
                        }
                        else
                        {
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                                modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                        }
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                            modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ResCase", FCConvert.ToString(1));
                    }

                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                        modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "ProcessReceipt", "Y");
                    if (AddToExcise)
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                            modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise",
                            Strings.Format(TotalExcise + TotalLocalTransfer, "0.00"));
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                            modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Excise", Strings.Format(TotalExcise, "0.00"));
                    }

                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                        modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "StatePaid", Strings.Format(TotalState, "0.00"));
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                        modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "AgentFee", Strings.Format(TotalAgent, "0.00"));
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                        modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "SalesTax", Strings.Format(TotalSalesTax, "0.00"));
                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                        modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "TitleFee", Strings.Format(TotalTitleFee, "0.00"));
                    if (cmbGroup.Text == "Fleet")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                            modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate",
                            "Fleet " + fecherFoundation.Strings.Trim(Strings.Left(cboFleets.Text, 5)));
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                            modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Plate",
                            "Group " + fecherFoundation.Strings.Trim(Strings.Left(cboFleets.Text, 5)));
                    }

                    if (OwnerCode == "I")
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                            modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner",
                            fecherFoundation.Strings.Trim(LastName) + ", " + fecherFoundation.Strings.Trim(FirstName));
                    }
                    else
                    {
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                            modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "Owner",
                            fecherFoundation.Strings.Trim(FirstName));
                    }

                    modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER,
                        modGlobalConstants.REGISTRYKEY + "CR\\MV\\", "PartyID",
                        MotorVehicle.Statics.lngFleetOwnerPartyID);

                    if (MotorVehicle.Statics.correlationId != new Guid())
                    {
                        MotorVehicle.CompleteMVTransaction();
                    }

                    return;
                }

                if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "SOUTH PORTLAND")
                {
                    FCFileSystem.FileOpen(11, "C:\\TrioVB\\TSREAS55.NET", OpenMode.Random, (OpenAccess) (-1),
                        (OpenShare) (-1), 241);
                }
                else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "GORHAM")
                {
                    FCFileSystem.FileOpen(11, "C:\\TrioVB\\TSREAS55.NET", OpenMode.Random, (OpenAccess) (-1),
                        (OpenShare) (-1), 241);
                }
                else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "PORTLAND")
                {
                    FCFileSystem.FileOpen(11, "C:\\TrioVB\\TSREAS55.NET", OpenMode.Random, (OpenAccess) (-1),
                        (OpenShare) (-1), 241);
                }
                else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "BUXTON")
                {
                    FCFileSystem.FileOpen(11, "C:\\TrioVB\\TSREAS55.NET", OpenMode.Random, (OpenAccess) (-1),
                        (OpenShare) (-1), 241);
                }
                else if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "NORTH BERWICK")
                {
                    FCFileSystem.FileOpen(11, "C:\\TrioVB\\TSREAS55.NET", OpenMode.Random, (OpenAccess) (-1),
                        (OpenShare) (-1), 241);
                }
                else
                {
                    FCFileSystem.FileOpen(11, "S:\\TSREAS55.NET", OpenMode.Random, (OpenAccess) (-1), (OpenShare) (-1),
                        241);
                }

                FCFileSystem.FileGet(11, ref strRec3, -1, true);
                FCFileSystem.FileGet(11, ref strRec5A, -1, true);
                // 
                strRec3 = Strings.StrDup(241, " ");
                strRec5A = Strings.StrDup(241, " ");
                // 
                Strings.MidSet(ref strRec3, 3, 3, "MV3");
                if (AddToExcise)
                {
                    Strings.MidSet(ref strRec3, 6, 7,
                        Strings.Format(
                            FCConvert.ToInt32(Strings.Format(TotalExcise + TotalLocalTransfer, "00000.00")) * 100,
                            "0000000"));
                }
                else
                {
                    Strings.MidSet(ref strRec3, 6, 7,
                        Strings.Format(FCConvert.ToInt32(Strings.Format(TotalExcise, "00000.00")) * 100, "0000000"));
                }

                Strings.MidSet(ref strRec5A, 3, 3, "MV5");
                if (AddToExcise)
                {
                    Strings.MidSet(ref strRec5A, 6, 6,
                        Strings.Format(
                            FCConvert.ToInt32(Strings.Format(TotalExcise + TotalLocalTransfer, "0000.00")) * 100,
                            "000000"));
                }
                else
                {
                    Strings.MidSet(ref strRec5A, 6, 6,
                        Strings.Format(FCConvert.ToInt32(Strings.Format(TotalExcise, "0000.00")) * 100, "000000"));
                }

                Strings.MidSet(ref strRec5A, 12, 6,
                    Strings.Format(FCConvert.ToInt32(Strings.Format(TotalState, "0000.00")) * 100, "000000"));
                if (AddToExcise)
                {
                    Strings.MidSet(ref strRec5A, 18, 6,
                        Strings.Format(FCConvert.ToInt32(Strings.Format(TotalAgent, "000.00")) * 100, "000000"));
                }
                else
                {
                    Strings.MidSet(ref strRec5A, 18, 6,
                        Strings.Format(
                            FCConvert.ToInt32(Strings.Format(TotalAgent + TotalLocalTransfer, "000.00")) * 100,
                            "000000"));
                }

                Strings.MidSet(ref strRec5A, 24, 7,
                    Strings.Format(FCConvert.ToInt32(Strings.Format(TotalSalesTax, "0000.00")) * 100, "0000000"));
                Strings.MidSet(ref strRec5A, 31, 5,
                    Strings.Format(FCConvert.ToInt32(Strings.Format(TotalTitleFee, "000.00")) * 100, "00000"));
                if (OwnerCode == "I")
                {
                    Strings.MidSet(ref strRec5A, 53, 30,
                        FCConvert.ToString(MotorVehicle.strPadSpaceR(
                            fecherFoundation.Strings.Trim(LastName) + ", " + fecherFoundation.Strings.Trim(FirstName),
                            30)));
                }
                else
                {
                    Strings.MidSet(ref strRec5A, 53, 30,
                        FCConvert.ToString(MotorVehicle.strPadSpaceR(fecherFoundation.Strings.Trim(FirstName), 30)));
                }

                // 
                FCFileSystem.FilePut(11, strRec3, -1, true);
                FCFileSystem.FilePut(11, strRec5A, -1, true);
                FCFileSystem.FileClose(11);
            }
            finally
            {
                rsCheck.Dispose();
                rsResCheck.Dispose();
			}
        }

		private List<RegistrationSearchResult> CreateSQLQuery(string x, string strType)
		{
			string temp;
			int tempAscii = 0;
			string strEnd = "";
            clsDRWrapper rr1 = new clsDRWrapper();
            clsDRWrapper rr2 = new clsDRWrapper();
            clsDRWrapper rr3 = new clsDRWrapper();
			var results = new List<RegistrationSearchResult>();

            try
            {
                x = FCConvert.ToString(MotorVehicle.FixQuotes(x));
                temp = Strings.Right(x, 1);
                if (string.Compare(fecherFoundation.Strings.UCase(temp), "Z") < 0)
                {
                    tempAscii = Convert.ToByte(temp[0]);
                    tempAscii += 1;
                    temp = FCConvert.ToString(Convert.ToChar(tempAscii));
                    strEnd = Strings.Left(x, x.Length - 1) + temp;
                    if (strType == "Regular")
                    {
                        rr1.OpenRecordset(
                            "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM Master as c LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE p.FullNameLF >= '" +
                            x + "' AND p.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY p.FullNameLF");
                        rr2.OpenRecordset(
                            "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM Master as c LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE q.FullNameLF >= '" +
                            x + "' AND q.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY q.FullNameLF");
                        rr3.OpenRecordset(
                            "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM Master as c LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE r.FullNameLF >= '" +
                            x + "' AND r.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY r.FullNameLF");
                    }
                    else
                    {
                        rr1.OpenRecordset(
                            "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM HeldRegistrationMaster as c LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE p.FullNameLF >= '" +
                            x + "' AND p.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY p.FullNameLF");
                        rr2.OpenRecordset(
                            "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM HeldRegistrationMaster as c LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE q.FullNameLF >= '" +
                            x + "' AND q.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY q.FullNameLF");
                        rr3.OpenRecordset(
                            "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM HeldRegistrationMaster as c LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                            rr1.CurrentPrefix +
                            "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE r.FullNameLF >= '" +
                            x + "' AND r.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY r.FullNameLF");
                    }
                }
                else
                {
                    strEnd = CreateEndString(x);
                    if (strEnd == "")
                    {
                        if (strType == "Regular")
                        {
                            rr1.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM Master as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE p.FullNameLF >= '" +
                                x + "' AND Status <> 'T' ORDER BY p.FullNameLF");
                            rr2.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM Master as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE q.FullNameLF >= '" +
                                x + "' AND Status <> 'T' ORDER BY q.FullNameLF");
                            rr3.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM Master as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE r.FullNameLF >= '" +
                                x + "' AND Status <> 'T' ORDER BY r.FullNameLF");
                        }
                        else
                        {
                            rr1.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM HeldRegistrationMaster as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE p.FullNameLF >= '" +
                                x + "' AND Status <> 'T' ORDER BY p.FullNameLF");
                            rr2.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM HeldRegistrationMaster as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE q.FullNameLF >= '" +
                                x + "' AND Status <> 'T' ORDER BY q.FullNameLF");
                            rr3.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM HeldRegistrationMaster as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE r.FullNameLF >= '" +
                                x + "' AND Status <> 'T' ORDER BY r.FullNameLF");
                        }
                    }
                    else
                    {
                        if (strType == "Regular")
                        {
                            rr1.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM Master as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE p.FullNameLF >= '" +
                                x + "' AND p.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY p.FullNameLF");
                            rr2.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM Master as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE q.FullNameLF >= '" +
                                x + "' AND q.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY q.FullNameLF");
                            rr3.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM Master as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE r.FullNameLF >= '" +
                                x + "' AND r.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY r.FullNameLF");
                        }
                        else
                        {
                            rr1.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM HeldRegistrationMaster as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE p.FullNameLF >= '" +
                                x + "' AND p.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY p.FullNameLF");
                            rr2.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM HeldRegistrationMaster as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE q.FullNameLF >= '" +
                                x + "' AND q.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY q.FullNameLF");
                            rr3.OpenRecordset(
                                "SELECT c.*, p.FullNameLF as Party1FullNameLF, q.FullNameLF as Party2FullNameLF, r.FullNameLF as Party3FullNameLF FROM HeldRegistrationMaster as c LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID LEFT JOIN " +
                                rr1.CurrentPrefix +
                                "CentralParties.dbo.PartyAndAddressView as r ON c.PartyID3 = r.ID WHERE r.FullNameLF >= '" +
                                x + "' AND r.FullNameLF < '" + strEnd + "' AND Status <> 'T' ORDER BY r.FullNameLF");
                        }
                    }
                }

                if ((rr1.EndOfFile() != true && rr1.BeginningOfFile() != true) ||
                    (rr2.EndOfFile() != true && rr2.BeginningOfFile() != true) ||
                    (rr3.EndOfFile() != true && rr3.BeginningOfFile() != true))
                {
                    if (rr1.EndOfFile() != true && rr1.BeginningOfFile() != true)
                    {
                        rr1.MoveLast();
                        rr1.MoveFirst();
                        while (!rr1.EndOfFile())
                        {
                            results.Add(new RegistrationSearchResult
                            {
                                Id = rr1.Get_Fields_Int32("ID"),
                                Name = MotorVehicle.GetPartyNameMiddleInitial(rr1.Get_Fields_Int32("PartyID1"), true)
                                    .Trim(),
                                Class = rr1.Get_Fields_String("Class").Trim(),
                                Subclass = rr1.Get_Fields_String("Subclass").Trim(),
                                Plate = rr1.Get_Fields_String("plate").Trim(),
                                Year = rr1.Get_Fields_String("Year").Trim(),
                                Make = rr1.Get_Fields_String("make").Trim(),
                                Model = rr1.Get_Fields_String("model").Trim(),
                                ExpireDate = rr1.Get_Fields_DateTime("ExpireDate") == new DateTime(1899, 12, 30) ? "" : rr1.Get_Fields_DateTime("ExpireDate").ToString("MM/dd/yyyy")
							});
                            rr1.MoveNext();
                        }
                    }

                    if (rr2.EndOfFile() != true && rr2.BeginningOfFile() != true)
                    {
                        rr2.MoveLast();
                        rr2.MoveFirst();
                        while (!rr2.EndOfFile())
                        {
                            if (Strings.Mid(
                                    fecherFoundation.Strings.UCase(
                                        fecherFoundation.Strings.Trim(rr2.Get_Fields("Party1FullNameLF") + " ")), 1,
                                    txtCriteria.Text.Length) != txtCriteria.Text)
                            {
                                results.Add(new RegistrationSearchResult
                                {
                                    Id = rr2.Get_Fields_Int32("ID"),
                                    Name = MotorVehicle
                                        .GetPartyNameMiddleInitial(rr2.Get_Fields_Int32("PartyID2"), true).Trim(),
                                    Class = rr2.Get_Fields_String("Class").Trim(),
                                    Subclass = rr2.Get_Fields_String("Subclass").Trim(),
                                    Plate = rr2.Get_Fields_String("plate").Trim(),
                                    Year = rr2.Get_Fields_String("Year").Trim(),
                                    Make = rr2.Get_Fields_String("make").Trim(),
                                    Model = rr2.Get_Fields_String("model").Trim(),
                                    ExpireDate = rr2.Get_Fields_DateTime("ExpireDate") == new DateTime(1899, 12, 30) ? "" : rr2.Get_Fields_DateTime("ExpireDate").ToString("MM/dd/yyyy")
								});
                            }

                            rr2.MoveNext();
                        }
                    }

                    if (rr3.EndOfFile() != true && rr3.BeginningOfFile() != true)
                    {
                        rr3.MoveLast();
                        rr3.MoveFirst();
                        while (!rr3.EndOfFile())
                        {
                            if (Strings.Mid(
                                    fecherFoundation.Strings.UCase(
                                        fecherFoundation.Strings.Trim(rr3.Get_Fields("Party1FullNameLF") + " ")), 1,
                                    txtCriteria.Text.Length) != txtCriteria.Text &&
                                Strings.Mid(
                                    fecherFoundation.Strings.UCase(
                                        fecherFoundation.Strings.Trim(rr3.Get_Fields("Party2FullNameLF") + " ")), 1,
                                    txtCriteria.Text.Length) != txtCriteria.Text)
                            {
                                results.Add(new RegistrationSearchResult
                                {
                                    Id = rr3.Get_Fields_Int32("ID"),
                                    Name = MotorVehicle
                                        .GetPartyNameMiddleInitial(rr3.Get_Fields_Int32("PartyID3"), true)
                                        .Trim(),
                                    Class = rr3.Get_Fields_String("Class").Trim(),
                                    Subclass = rr3.Get_Fields_String("Subclass").Trim(),
                                    Plate = rr3.Get_Fields_String("plate").Trim(),
                                    Year = rr3.Get_Fields_String("Year").Trim(),
                                    Make = rr3.Get_Fields_String("make").Trim(),
                                    Model = rr3.Get_Fields_String("model").Trim(),
                                    ExpireDate = rr3.Get_Fields_DateTime("ExpireDate") == new DateTime(1899, 12, 30) ? "" : rr3.Get_Fields_DateTime("ExpireDate").ToString("MM/dd/yyyy")
                                });
                            }

                            rr3.MoveNext();
                        }
                    }

                }

                return results;
            }
            finally
            {
				rr1.Dispose();
				rr2.Dispose();
				rr3.Dispose();
            }
			
		}

		private string CreateEndString(string strCriteria)
		{
			string CreateEndString = "";
			int counter;
			bool blnPossible;
			string temp = "";
			int tempAscii = 0;
			blnPossible = false;
			for (counter = 1; counter <= strCriteria.Length; counter++)
			{
				if (fecherFoundation.Strings.UCase(Strings.Mid(strCriteria, counter, 1)) != "Z")
				{
					blnPossible = true;
					break;
				}
			}
			if (blnPossible)
			{
				counter = strCriteria.Length;
				do
				{
					temp = Strings.Mid(strCriteria, counter, 1);
					if (string.Compare(fecherFoundation.Strings.UCase(temp), "Z") < 0)
					{
						tempAscii = Convert.ToByte(temp[0]);
						tempAscii += 1;
						temp = FCConvert.ToString(Convert.ToChar(tempAscii));
						Strings.MidSet(ref strCriteria, counter, 1, temp);
						break;
					}
					else
					{
						Strings.MidSet(ref strCriteria, counter, 1, "A");
					}
					counter -= 1;
				}
				while (counter > 0);
				CreateEndString = strCriteria;
			}
			else
			{
				CreateEndString = "";
			}
			return CreateEndString;
		}

		private void CheckForChangeToFleet()
		{
			DateTime xdate;
			DateTime LowDate;
			MotorVehicle.Statics.blnChangeToFleet = false;
			if ((FCConvert.ToInt32(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate").Month) == DateTime.Today.Month) && (FCConvert.ToInt32(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate").Year) == (DateTime.Today.Year - 1)))
			{
				xdate = fecherFoundation.DateAndTime.DateAdd("yyyy", 1, MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate"));
			}
			else
			{
				xdate = FCConvert.ToDateTime(MotorVehicle.Statics.rsSecondPlate.Get_Fields_DateTime("ExpireDate"));
			}
			LowDate = fecherFoundation.DateAndTime.DateAdd("m", -5, xdate);
			if (LowDate.ToOADate() > DateTime.Today.ToOADate())
			{
				if (!FCConvert.ToBoolean(MotorVehicle.Statics.rsSecondPlate.Get_Fields_Boolean("ETO")))
				{
					if (MotorVehicle.Statics.Class == "AP")
					{
						frmFleetSelect.InstancePtr.Show(FCForm.FormShowEnum.Modal);
					}
					else
					{
						MotorVehicle.Statics.blnChangeToFleet = false;
					}
				}
			}
		}

		private bool CheckValidClassSubClass()
		{
			using (clsDRWrapper rsInfo = new clsDRWrapper())
            {
                rsInfo.OpenRecordset("SELECT * FROM Class WHERE BMVCode = '" + MotorVehicle.Statics.Class + "' AND SystemCode = '" + MotorVehicle.Statics.Subclass + "'");
                if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
			}
        }

		private bool ConfirmOldPlate(string parClass, string parPlate)
		{
			bool ConfirmOldPlate = false;
			string strSql = "";
			string strMsg = "";
			string strFound = "";
			bool boolRet;
			boolRet = true;
			if (parClass == "TL")
			{
				strSql = "SELECT Code, FormattedInventory FROM Inventory WHERE Status = 'A' AND (Code = 'PXSTL' OR Code = 'PXSLT') AND ((Prefix + CONVERT(NVARCHAR, Number) + Suffix) = '" + parPlate + "' OR FormattedInventory = '" + parPlate + "')";
			}
			else
			{
				strSql = "SELECT Code, FormattedInventory FROM Inventory WHERE Status = 'A' AND (Code = 'PXS" + parClass + "' OR Code = 'PXD" + parClass + "') AND ((Prefix + CONVERT(NVARCHAR, Number) + Suffix) = '" + parPlate + "' OR FormattedInventory = '" + parPlate + "')";
			}
			using (clsDRWrapper rsPCk = new clsDRWrapper())
            {
                rsPCk.OpenRecordset(strSql);
                if (!rsPCk.EndOfFile())
                {
                    while (!rsPCk.EndOfFile())
                    {
                        strFound += rsPCk.Get_Fields_String("FormattedInventory") + " " + Strings.Right(FCConvert.ToString(rsPCk.Get_Fields("Code")), 2) + ",";
                        rsPCk.MoveNext();
                    }
                    strFound = Strings.Left(strFound, strFound.Length - 1);
                    if (rsPCk.RecordCount() == 1)
                    {
                        strMsg = "The plate " + strFound + " is available in inventory. Are you sure the Old Plate is correct?";
                    }
                    else
                    {
                        strMsg = "The plates " + strFound + " are available in inventory. Are you sure the Old Plate is correct?";
                    }
                    if (DialogResult.No == MessageBox.Show(strMsg, "Plate in Inventory", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        boolRet = false;
                    }
                }
                ConfirmOldPlate = boolRet;
                return ConfirmOldPlate;
			}
        }

		public void cmbLongTerm_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbLongTerm.Text == "Annual")
			{
				optAnnual_CheckedChanged(sender, e);
			}
			else if (cmbLongTerm.Text == "Long Term")
			{
				optLongTerm_CheckedChanged(sender, e);
			}
		}

		public void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbGroup.Text == "Group")
			{
				optGroup_CheckedChanged(sender, e);
			}
			else if (cmbGroup.Text == "Fleet")
			{
				optFleet_CheckedChanged(sender, e);
			}
		}

		private void cmbAnnualReg_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAnnualReg.Text == "Annual")
			{
				optAnnualReg_CheckedChanged(sender, e);
			}
			else if (cmbAnnualReg.Text == "Long Term")
			{
				optLongTermReg_CheckedChanged(sender, e);
			}
		}
	}
}
