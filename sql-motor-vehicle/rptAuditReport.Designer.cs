﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptAuditReport.
	/// </summary>
	partial class rptAuditReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAuditReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUser = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateTimeField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOPID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUser)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateTimeField)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOPID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtUser,
				this.fldPlate,
				this.txtDateTimeField,
				this.fldDescription,
				this.fldLocation,
				this.fldVIN,
				this.fldOPID
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label9,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label1,
				this.txtMuniName,
				this.txtDate,
				this.txtPage,
				this.txtTime,
				this.Line12,
				this.Label15,
				this.Label17,
				this.Label20,
				this.Label21
			});
			this.PageHeader.Height = 0.7708333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0.01041667F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.5F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label9.Text = "UserID";
			this.Label9.Top = 0.5625F;
			this.Label9.Width = 0.6875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 4.375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label2.Text = "Date /";
			this.Label2.Top = 0.5625F;
			this.Label2.Width = 0.5625F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 4.8125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label3.Text = "Time";
			this.Label3.Top = 0.5625F;
			this.Label3.Width = 0.625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 7.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label4.Text = "Description";
			this.Label4.Top = 0.5625F;
			this.Label4.Width = 1.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.4375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "background-color: rgb(255,255,255); color: rgb(0,0,0); font-family: \'Tahoma\'; fon" + "t-size: 12pt; font-weight: bold; text-align: center; ddo-char-set: 1";
			this.Label1.Text = "Audit Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.53125F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0F;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0F;
			this.txtMuniName.Width = 1.4375F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.19F;
			this.txtDate.Left = 8.96875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.4375F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.21875F;
			this.txtPage.Left = 8.96875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.4375F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.4375F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 0.75F;
			this.Line12.Width = 10.34375F;
			this.Line12.X1 = 0F;
			this.Line12.X2 = 10.34375F;
			this.Line12.Y1 = 0.75F;
			this.Line12.Y2 = 0.75F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label15.Text = "Plate";
			this.Label15.Top = 0.5625F;
			this.Label15.Width = 0.6875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 5.875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label17.Text = "Screen";
			this.Label17.Top = 0.5625F;
			this.Label17.Width = 1.25F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.875F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label20.Text = "VIN";
			this.Label20.Top = 0.5625F;
			this.Label20.Width = 0.6875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 2.75F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
			this.Label21.Text = "Initials";
			this.Label21.Top = 0.5625F;
			this.Label21.Width = 0.6875F;
			// 
			// txtUser
			// 
			this.txtUser.Height = 0.1875F;
			this.txtUser.Left = 3.5F;
			this.txtUser.Name = "txtUser";
			this.txtUser.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtUser.Text = null;
			this.txtUser.Top = 0F;
			this.txtUser.Width = 0.625F;
			// 
			// fldPlate
			// 
			this.fldPlate.Height = 0.1875F;
			this.fldPlate.Left = 0F;
			this.fldPlate.Name = "fldPlate";
			this.fldPlate.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.fldPlate.Text = null;
			this.fldPlate.Top = 0F;
			this.fldPlate.Width = 0.8125F;
			// 
			// txtDateTimeField
			// 
			this.txtDateTimeField.Height = 0.1875F;
			this.txtDateTimeField.Left = 4.375F;
			this.txtDateTimeField.Name = "txtDateTimeField";
			this.txtDateTimeField.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtDateTimeField.Text = null;
			this.txtDateTimeField.Top = 0F;
			this.txtDateTimeField.Width = 1.5F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 7.1875F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.fldDescription.Text = null;
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 3.1875F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 5.9375F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.fldLocation.Text = null;
			this.fldLocation.Top = 0F;
			this.fldLocation.Width = 1.1875F;
			// 
			// fldVIN
			// 
			this.fldVIN.Height = 0.1875F;
			this.fldVIN.Left = 0.875F;
			this.fldVIN.Name = "fldVIN";
			this.fldVIN.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.fldVIN.Text = null;
			this.fldVIN.Top = 0F;
			this.fldVIN.Width = 1.8125F;
			// 
			// fldOPID
			// 
			this.fldOPID.Height = 0.1875F;
			this.fldOPID.Left = 2.75F;
			this.fldOPID.Name = "fldOPID";
			this.fldOPID.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.fldOPID.Text = null;
			this.fldOPID.Top = 0F;
			this.fldOPID.Width = 0.4375F;
			// 
			// rptAuditReport
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.47917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUser)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateTimeField)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOPID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUser;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateTimeField;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOPID;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
