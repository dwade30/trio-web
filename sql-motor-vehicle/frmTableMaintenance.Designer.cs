//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmTableMaintenance.
	/// </summary>
	partial class frmTableMaintenance
	{
		public fecherFoundation.FCComboBox cmbTable;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblInstruct;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblLevel;
		public fecherFoundation.FCFrame fraTables;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCTabControl tabTables;
		public fecherFoundation.FCTabPage tabTables_Page1;
		public FCGrid vsState;
		public FCGrid vsResCodes;
		public fecherFoundation.FCTabPage tabTables_Page2;
		public FCGrid vsCounty;
		public fecherFoundation.FCTabPage tabTables_Page3;
		public FCGrid vsMake;
		public fecherFoundation.FCTabPage tabTables_Page4;
		public fecherFoundation.FCLabel lblInstruct_0;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblLevel_0;
		public fecherFoundation.FCLabel lblLevel_1;
		public fecherFoundation.FCLabel lblLevel_2;
		public fecherFoundation.FCLabel lblLevel_3;
		public fecherFoundation.FCLabel lblInstruct_1;
		public fecherFoundation.FCLabel lblInstruct_2;
		public FCGrid vsOperators;
		public fecherFoundation.FCTabPage tabTables_Page5;
		public FCGrid vsColor;
		public fecherFoundation.FCTabPage tabTables_Page6;
		public FCGrid vsStyle;
		public fecherFoundation.FCTabPage tabTables_Page7;
		public FCGrid vsReasonCodes;
		public FCGrid vsPrint;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCTabPage tabTables_Page8;
		public FCGrid vsAgentFee;
		public fecherFoundation.FCTabPage tabTables_Page9;
		public FCGrid vsClass;
		public fecherFoundation.FCTabPage tabTables_Page10;
		public FCGrid vsDefaults;
		public fecherFoundation.FCTabPage tabTables_Page11;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public FCGrid vsGrossTable1;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public FCGrid vsGrossTable2;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public FCGrid vsGrossTable3;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessPrint;
		public fecherFoundation.FCToolStripMenuItem mnuseperator1;
		public fecherFoundation.FCToolStripMenuItem mnuSaveQuit;
		public fecherFoundation.FCToolStripMenuItem mnusperator2;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle9 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle10 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle11 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle12 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle13 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle14 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle15 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle16 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle17 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle18 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle19 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle20 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle21 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle22 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle23 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle24 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle25 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle26 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle27 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle28 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle29 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle30 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbTable = new fecherFoundation.FCComboBox();
			this.fraTables = new fecherFoundation.FCFrame();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.tabTables = new fecherFoundation.FCTabControl();
			this.tabTables_Page1 = new fecherFoundation.FCTabPage();
			this.vsState = new fecherFoundation.FCGrid();
			this.vsResCodes = new fecherFoundation.FCGrid();
			this.tabTables_Page2 = new fecherFoundation.FCTabPage();
			this.vsCounty = new fecherFoundation.FCGrid();
			this.tabTables_Page3 = new fecherFoundation.FCTabPage();
			this.vsMake = new fecherFoundation.FCGrid();
			this.tabTables_Page4 = new fecherFoundation.FCTabPage();
			this.lblInstruct_0 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblLevel_0 = new fecherFoundation.FCLabel();
			this.lblLevel_1 = new fecherFoundation.FCLabel();
			this.lblLevel_2 = new fecherFoundation.FCLabel();
			this.lblLevel_3 = new fecherFoundation.FCLabel();
			this.lblInstruct_1 = new fecherFoundation.FCLabel();
			this.lblInstruct_2 = new fecherFoundation.FCLabel();
			this.vsOperators = new fecherFoundation.FCGrid();
			this.tabTables_Page5 = new fecherFoundation.FCTabPage();
			this.vsColor = new fecherFoundation.FCGrid();
			this.tabTables_Page6 = new fecherFoundation.FCTabPage();
			this.vsStyle = new fecherFoundation.FCGrid();
			this.tabTables_Page7 = new fecherFoundation.FCTabPage();
			this.vsReasonCodes = new fecherFoundation.FCGrid();
			this.vsPrint = new fecherFoundation.FCGrid();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.tabTables_Page8 = new fecherFoundation.FCTabPage();
			this.vsAgentFee = new fecherFoundation.FCGrid();
			this.tabTables_Page9 = new fecherFoundation.FCTabPage();
			this.vsClass = new fecherFoundation.FCGrid();
			this.tabTables_Page10 = new fecherFoundation.FCTabPage();
			this.vsDefaults = new fecherFoundation.FCGrid();
			this.tabTables_Page11 = new fecherFoundation.FCTabPage();
			this.SSTab1 = new fecherFoundation.FCTabControl();
			this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
			this.vsGrossTable1 = new fecherFoundation.FCGrid();
			this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
			this.vsGrossTable2 = new fecherFoundation.FCGrid();
			this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
			this.vsGrossTable3 = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuseperator1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveQuit = new fecherFoundation.FCToolStripMenuItem();
			this.mnusperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSaveQuit = new fecherFoundation.FCButton();
			this.cmdProcessPrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTables)).BeginInit();
			this.fraTables.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.tabTables.SuspendLayout();
			this.tabTables_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsResCodes)).BeginInit();
			this.tabTables_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCounty)).BeginInit();
			this.tabTables_Page3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsMake)).BeginInit();
			this.tabTables_Page4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsOperators)).BeginInit();
			this.tabTables_Page5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsColor)).BeginInit();
			this.tabTables_Page6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsStyle)).BeginInit();
			this.tabTables_Page7.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsReasonCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPrint)).BeginInit();
			this.tabTables_Page8.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAgentFee)).BeginInit();
			this.tabTables_Page9.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsClass)).BeginInit();
			this.tabTables_Page10.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsDefaults)).BeginInit();
			this.tabTables_Page11.SuspendLayout();
			this.SSTab1.SuspendLayout();
			this.SSTab1_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsGrossTable1)).BeginInit();
			this.SSTab1_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsGrossTable2)).BeginInit();
			this.SSTab1_Page3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsGrossTable3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveQuit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 538);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.tabTables);
			this.ClientArea.Controls.Add(this.fraTables);
			this.ClientArea.Size = new System.Drawing.Size(1078, 478);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdProcessPrint);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdProcessPrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(295, 30);
			this.HeaderText.Text = "Table / Option Processing";
			// 
			// cmbTable
			// 
			this.cmbTable.AutoSize = false;
			this.cmbTable.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTable.FormattingEnabled = true;
			this.cmbTable.Items.AddRange(new object[] {
            "Residence Codes",
            "State Codes",
            "Style Codes",
            "County Codes",
            "Make Codes",
            "Fees",
            "Operator ID Codes",
            "Color Codes",
            "Default Values",
            "Reason Codes",
            "Print Priorities",
            "Class Codes",
            "Commercial Truck / Truck Tractor GVW",
            "Farm Truck / Motor Home GVW",
            "Class A Special Mobile Equiptment GVW"});
			this.cmbTable.Location = new System.Drawing.Point(20, 30);
			this.cmbTable.Name = "cmbTable";
			this.cmbTable.Size = new System.Drawing.Size(463, 40);
			this.cmbTable.TabIndex = 40;
			// 
			// fraTables
			// 
			this.fraTables.BackColor = System.Drawing.Color.White;
			this.fraTables.Controls.Add(this.cmdCancel);
			this.fraTables.Controls.Add(this.cmbTable);
			this.fraTables.Controls.Add(this.cmdPrint);
			this.fraTables.Location = new System.Drawing.Point(30, 108);
			this.fraTables.Name = "fraTables";
			this.fraTables.Size = new System.Drawing.Size(503, 150);
			this.fraTables.TabIndex = 21;
			this.fraTables.Text = "Print Table";
			this.fraTables.Visible = false;
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(126, 90);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(94, 40);
			this.cmdCancel.TabIndex = 39;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "actionButton";
			this.cmdPrint.Location = new System.Drawing.Point(20, 90);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(76, 40);
			this.cmdPrint.TabIndex = 37;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// tabTables
			// 
			this.tabTables.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.tabTables.Controls.Add(this.tabTables_Page1);
			this.tabTables.Controls.Add(this.tabTables_Page2);
			this.tabTables.Controls.Add(this.tabTables_Page3);
			this.tabTables.Controls.Add(this.tabTables_Page4);
			this.tabTables.Controls.Add(this.tabTables_Page5);
			this.tabTables.Controls.Add(this.tabTables_Page6);
			this.tabTables.Controls.Add(this.tabTables_Page7);
			this.tabTables.Controls.Add(this.tabTables_Page8);
			this.tabTables.Controls.Add(this.tabTables_Page9);
			this.tabTables.Controls.Add(this.tabTables_Page10);
			this.tabTables.Controls.Add(this.tabTables_Page11);
			this.tabTables.Location = new System.Drawing.Point(30, 30);
			this.tabTables.Name = "tabTables";
			this.tabTables.Orientation = Wisej.Web.Orientation.Horizontal;
			this.tabTables.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
			this.tabTables.ShowFocusRect = false;
			this.tabTables.Size = new System.Drawing.Size(1019, 437);
            this.tabTables.SelectedIndex = 5;
			this.tabTables.TabIndex = 0;
			this.tabTables.TabsPerRow = 0;
            this.tabTables.Text = "Gross Vehicle Weight Tables";
			this.tabTables.WordWrap = false;
			this.tabTables.SelectedIndexChanged += new System.EventHandler(this.tabTables_SelectedIndexChanged);
			// 
			// tabTables_Page1
			// 
			this.tabTables_Page1.Controls.Add(this.vsState);
			this.tabTables_Page1.Controls.Add(this.vsResCodes);
			this.tabTables_Page1.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page1.Name = "tabTables_Page1";
            this.tabTables_Page1.Text = "Residence / State Codes";
			// 
			// vsState
			// 
			this.vsState.AllowSelection = false;
			this.vsState.AllowUserToResizeColumns = false;
			this.vsState.AllowUserToResizeRows = false;
			this.vsState.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
			this.vsState.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsState.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsState.BackColorBkg = System.Drawing.Color.Empty;
			this.vsState.BackColorFixed = System.Drawing.Color.Empty;
			this.vsState.BackColorSel = System.Drawing.Color.Empty;
			this.vsState.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsState.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsState.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsState.ColumnHeadersHeight = 30;
			this.vsState.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsState.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsState.DragIcon = null;
			this.vsState.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsState.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsState.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsState.FrozenCols = 0;
			this.vsState.GridColor = System.Drawing.Color.Empty;
			this.vsState.GridColorFixed = System.Drawing.Color.Empty;
			this.vsState.Location = new System.Drawing.Point(523, 20);
			this.vsState.Name = "vsState";
			this.vsState.OutlineCol = 0;
			this.vsState.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsState.RowHeightMin = 0;
			this.vsState.Rows = 50;
			this.vsState.ScrollTipText = null;
			this.vsState.ShowColumnVisibilityMenu = false;
			this.vsState.ShowFocusCell = false;
			this.vsState.Size = new System.Drawing.Size(474, 351);
			this.vsState.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsState.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsState.TabIndex = 2;
			this.vsState.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsState_BeforeEdit);
			this.vsState.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsState_ValidateEdit);
			this.vsState.KeyDown += new Wisej.Web.KeyEventHandler(this.vsState_KeyDownEvent);
			this.vsState.DoubleClick += new System.EventHandler(this.vsState_DblClick);
			// 
			// vsResCodes
			// 
			this.vsResCodes.AllowSelection = false;
			this.vsResCodes.AllowUserToResizeColumns = false;
			this.vsResCodes.AllowUserToResizeRows = false;
			this.vsResCodes.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
			this.vsResCodes.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsResCodes.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsResCodes.BackColorBkg = System.Drawing.Color.Empty;
			this.vsResCodes.BackColorFixed = System.Drawing.Color.Empty;
			this.vsResCodes.BackColorSel = System.Drawing.Color.Empty;
			this.vsResCodes.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsResCodes.Cols = 10;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsResCodes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsResCodes.ColumnHeadersHeight = 30;
			this.vsResCodes.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsResCodes.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsResCodes.DragIcon = null;
			this.vsResCodes.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsResCodes.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsResCodes.FrozenCols = 0;
			this.vsResCodes.GridColor = System.Drawing.Color.Empty;
			this.vsResCodes.GridColorFixed = System.Drawing.Color.Empty;
			this.vsResCodes.Location = new System.Drawing.Point(20, 20);
			this.vsResCodes.Name = "vsResCodes";
			this.vsResCodes.OutlineCol = 0;
			this.vsResCodes.ReadOnly = true;
			this.vsResCodes.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsResCodes.RowHeightMin = 0;
			this.vsResCodes.Rows = 50;
			this.vsResCodes.ScrollTipText = null;
			this.vsResCodes.ShowColumnVisibilityMenu = false;
			this.vsResCodes.ShowFocusCell = false;
			this.vsResCodes.Size = new System.Drawing.Size(482, 351);
			this.vsResCodes.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsResCodes.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsResCodes.TabIndex = 1;
			this.vsResCodes.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsResCodes_BeforeEdit);
			this.vsResCodes.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsResCodes_ValidateEdit);
			this.vsResCodes.KeyDown += new Wisej.Web.KeyEventHandler(this.vsResCodes_KeyDownEvent);
			this.vsResCodes.DoubleClick += new System.EventHandler(this.vsResCodes_DblClick);
			// 
			// tabTables_Page2
			// 
			this.tabTables_Page2.Controls.Add(this.vsCounty);
			this.tabTables_Page2.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page2.Name = "tabTables_Page2";
			this.tabTables_Page2.Text = "County Codes";
			// 
			// vsCounty
			// 
			this.vsCounty.AllowSelection = false;
			this.vsCounty.AllowUserToResizeColumns = false;
			this.vsCounty.AllowUserToResizeRows = false;
			this.vsCounty.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsCounty.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsCounty.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsCounty.BackColorBkg = System.Drawing.Color.Empty;
			this.vsCounty.BackColorFixed = System.Drawing.Color.Empty;
			this.vsCounty.BackColorSel = System.Drawing.Color.Empty;
			this.vsCounty.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsCounty.Cols = 10;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsCounty.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.vsCounty.ColumnHeadersHeight = 30;
			this.vsCounty.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsCounty.DefaultCellStyle = dataGridViewCellStyle6;
			this.vsCounty.DragIcon = null;
			this.vsCounty.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsCounty.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsCounty.FrozenCols = 0;
			this.vsCounty.GridColor = System.Drawing.Color.Empty;
			this.vsCounty.GridColorFixed = System.Drawing.Color.Empty;
			this.vsCounty.Location = new System.Drawing.Point(20, 20);
			this.vsCounty.Name = "vsCounty";
			this.vsCounty.OutlineCol = 0;
			this.vsCounty.ReadOnly = true;
			this.vsCounty.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsCounty.RowHeightMin = 0;
			this.vsCounty.Rows = 50;
			this.vsCounty.ScrollTipText = null;
			this.vsCounty.ShowColumnVisibilityMenu = false;
			this.vsCounty.ShowFocusCell = false;
			this.vsCounty.Size = new System.Drawing.Size(976, 351);
			this.vsCounty.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsCounty.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsCounty.TabIndex = 3;
			this.vsCounty.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsCounty_BeforeEdit);
			this.vsCounty.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsCounty_ValidateEdit);
			this.vsCounty.KeyDown += new Wisej.Web.KeyEventHandler(this.vsCounty_KeyDownEvent);
			this.vsCounty.DoubleClick += new System.EventHandler(this.vsCounty_DblClick);
			// 
			// tabTables_Page3
			// 
			this.tabTables_Page3.Controls.Add(this.vsMake);
			this.tabTables_Page3.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page3.Name = "tabTables_Page3";
			this.tabTables_Page3.Text = "Make Codes";
			// 
			// vsMake
			// 
			this.vsMake.AllowSelection = false;
			this.vsMake.AllowUserToResizeColumns = false;
			this.vsMake.AllowUserToResizeRows = false;
			this.vsMake.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsMake.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsMake.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsMake.BackColorBkg = System.Drawing.Color.Empty;
			this.vsMake.BackColorFixed = System.Drawing.Color.Empty;
			this.vsMake.BackColorSel = System.Drawing.Color.Empty;
			this.vsMake.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsMake.CellCharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.vsMake.Cols = 10;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsMake.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.vsMake.ColumnHeadersHeight = 30;
			this.vsMake.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsMake.DefaultCellStyle = dataGridViewCellStyle8;
			this.vsMake.DragIcon = null;
			this.vsMake.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsMake.ExtendLastCol = true;
			this.vsMake.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsMake.FrozenCols = 0;
			this.vsMake.GridColor = System.Drawing.Color.Empty;
			this.vsMake.GridColorFixed = System.Drawing.Color.Empty;
			this.vsMake.Location = new System.Drawing.Point(20, 20);
			this.vsMake.Name = "vsMake";
			this.vsMake.OutlineCol = 0;
			this.vsMake.ReadOnly = true;
			this.vsMake.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsMake.RowHeightMin = 0;
			this.vsMake.Rows = 50;
			this.vsMake.ScrollTipText = null;
			this.vsMake.ShowColumnVisibilityMenu = false;
			this.vsMake.ShowFocusCell = false;
			this.vsMake.Size = new System.Drawing.Size(976, 351);
			this.vsMake.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsMake.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsMake.TabIndex = 4;
			this.vsMake.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsMake_KeyPressEdit);
			this.vsMake.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsMake_ValidateEdit);
			this.vsMake.CurrentCellChanged += new System.EventHandler(this.vsMake_RowColChange);
			this.vsMake.KeyDown += new Wisej.Web.KeyEventHandler(this.vsMake_KeyDownEvent);
			// 
			// tabTables_Page4
			// 
			this.tabTables_Page4.Controls.Add(this.lblInstruct_0);
			this.tabTables_Page4.Controls.Add(this.Label2);
			this.tabTables_Page4.Controls.Add(this.lblLevel_0);
			this.tabTables_Page4.Controls.Add(this.lblLevel_1);
			this.tabTables_Page4.Controls.Add(this.lblLevel_2);
			this.tabTables_Page4.Controls.Add(this.lblLevel_3);
			this.tabTables_Page4.Controls.Add(this.lblInstruct_1);
			this.tabTables_Page4.Controls.Add(this.lblInstruct_2);
			this.tabTables_Page4.Controls.Add(this.vsOperators);
			this.tabTables_Page4.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page4.Name = "tabTables_Page4";
			this.tabTables_Page4.Text = "Operator ID Codes";
			// 
			// lblInstruct_0
			// 
			this.lblInstruct_0.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblInstruct_0.Location = new System.Drawing.Point(794, 20);
			this.lblInstruct_0.Name = "lblInstruct_0";
			this.lblInstruct_0.Size = new System.Drawing.Size(165, 45);
			this.lblInstruct_0.TabIndex = 15;
			this.lblInstruct_0.Text = "OPERATOR ID\'S MUST CONSIST OF THREE LETTERS ";
			// 
			// Label2
			// 
			this.Label2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.Label2.Location = new System.Drawing.Point(794, 85);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(98, 15);
			this.Label2.TabIndex = 16;
			this.Label2.Text = "AGENT LEVELS";
			// 
			// lblLevel_0
			// 
			this.lblLevel_0.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblLevel_0.Location = new System.Drawing.Point(794, 120);
			this.lblLevel_0.Name = "lblLevel_0";
			this.lblLevel_0.Size = new System.Drawing.Size(119, 15);
			this.lblLevel_0.TabIndex = 17;
			this.lblLevel_0.Text = "1 - PRIMARY AGENT";
			// 
			// lblLevel_1
			// 
			this.lblLevel_1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblLevel_1.Location = new System.Drawing.Point(794, 155);
			this.lblLevel_1.Name = "lblLevel_1";
			this.lblLevel_1.Size = new System.Drawing.Size(84, 15);
			this.lblLevel_1.TabIndex = 18;
			this.lblLevel_1.Text = "2 - ALTERNATE";
			// 
			// lblLevel_2
			// 
			this.lblLevel_2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblLevel_2.Location = new System.Drawing.Point(794, 190);
			this.lblLevel_2.Name = "lblLevel_2";
			this.lblLevel_2.Size = new System.Drawing.Size(88, 15);
			this.lblLevel_2.TabIndex = 19;
			this.lblLevel_2.Text = "3 - VIEW ONLY";
			// 
			// lblLevel_3
			// 
			this.lblLevel_3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblLevel_3.Location = new System.Drawing.Point(794, 225);
			this.lblLevel_3.Name = "lblLevel_3";
			this.lblLevel_3.Size = new System.Drawing.Size(84, 15);
			this.lblLevel_3.TabIndex = 20;
			this.lblLevel_3.Text = "4 - REGULAR";
			// 
			// lblInstruct_1
			// 
			this.lblInstruct_1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblInstruct_1.Location = new System.Drawing.Point(794, 260);
			this.lblInstruct_1.Name = "lblInstruct_1";
			this.lblInstruct_1.Size = new System.Drawing.Size(183, 60);
			this.lblInstruct_1.TabIndex = 33;
			this.lblInstruct_1.Text = "INVENTORY GROUPS MAY ONLY CONTAIN THE  NUMBERS 0 - 12";
			// 
			// lblInstruct_2
			// 
			this.lblInstruct_2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblInstruct_2.Location = new System.Drawing.Point(794, 340);
			this.lblInstruct_2.Name = "lblInstruct_2";
			this.lblInstruct_2.Size = new System.Drawing.Size(100, 22);
			this.lblInstruct_2.TabIndex = 34;
			this.lblInstruct_2.Text = "0 = ALL GROUPS";
			// 
			// vsOperators
			// 
			this.vsOperators.AllowSelection = false;
			this.vsOperators.AllowUserToResizeColumns = false;
			this.vsOperators.AllowUserToResizeRows = false;
			this.vsOperators.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsOperators.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsOperators.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsOperators.BackColorBkg = System.Drawing.Color.Empty;
			this.vsOperators.BackColorFixed = System.Drawing.Color.Empty;
			this.vsOperators.BackColorSel = System.Drawing.Color.Empty;
			this.vsOperators.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsOperators.CellCharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.vsOperators.Cols = 10;
			dataGridViewCellStyle9.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsOperators.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
			this.vsOperators.ColumnHeadersHeight = 30;
			this.vsOperators.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle10.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsOperators.DefaultCellStyle = dataGridViewCellStyle10;
			this.vsOperators.DragIcon = null;
			this.vsOperators.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsOperators.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsOperators.FrozenCols = 0;
			this.vsOperators.GridColor = System.Drawing.Color.Empty;
			this.vsOperators.GridColorFixed = System.Drawing.Color.Empty;
			this.vsOperators.Location = new System.Drawing.Point(20, 20);
			this.vsOperators.Name = "vsOperators";
			this.vsOperators.OutlineCol = 0;
			this.vsOperators.ReadOnly = true;
			this.vsOperators.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsOperators.RowHeightMin = 0;
			this.vsOperators.Rows = 50;
			this.vsOperators.ScrollTipText = null;
			this.vsOperators.ShowColumnVisibilityMenu = false;
			this.vsOperators.ShowFocusCell = false;
			this.vsOperators.Size = new System.Drawing.Size(741, 347);
			this.vsOperators.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsOperators.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsOperators.TabIndex = 5;
			this.vsOperators.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsOperators_KeyPressEdit);
			this.vsOperators.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsOperators_ValidateEdit);
			this.vsOperators.KeyDown += new Wisej.Web.KeyEventHandler(this.vsOperators_KeyDownEvent);
			// 
			// tabTables_Page5
			// 
			this.tabTables_Page5.Controls.Add(this.vsColor);
			this.tabTables_Page5.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page5.Name = "tabTables_Page5";
			this.tabTables_Page5.Text = "Color Codes";
			// 
			// vsColor
			// 
			this.vsColor.AllowSelection = false;
			this.vsColor.AllowUserToResizeColumns = false;
			this.vsColor.AllowUserToResizeRows = false;
			this.vsColor.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsColor.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsColor.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsColor.BackColorBkg = System.Drawing.Color.Empty;
			this.vsColor.BackColorFixed = System.Drawing.Color.Empty;
			this.vsColor.BackColorSel = System.Drawing.Color.Empty;
			this.vsColor.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsColor.Cols = 10;
			dataGridViewCellStyle11.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsColor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
			this.vsColor.ColumnHeadersHeight = 30;
			this.vsColor.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle12.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsColor.DefaultCellStyle = dataGridViewCellStyle12;
			this.vsColor.DragIcon = null;
			this.vsColor.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsColor.ExtendLastCol = true;
			this.vsColor.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsColor.FrozenCols = 0;
			this.vsColor.GridColor = System.Drawing.Color.Empty;
			this.vsColor.GridColorFixed = System.Drawing.Color.Empty;
			this.vsColor.Location = new System.Drawing.Point(20, 20);
			this.vsColor.Name = "vsColor";
			this.vsColor.OutlineCol = 0;
			this.vsColor.ReadOnly = true;
			this.vsColor.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsColor.RowHeightMin = 0;
			this.vsColor.Rows = 50;
			this.vsColor.ScrollTipText = null;
			this.vsColor.ShowColumnVisibilityMenu = false;
			this.vsColor.ShowFocusCell = false;
			this.vsColor.Size = new System.Drawing.Size(976, 351);
			this.vsColor.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsColor.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsColor.TabIndex = 6;
			// 
			// tabTables_Page6
			// 
			this.tabTables_Page6.Controls.Add(this.vsStyle);
			this.tabTables_Page6.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page6.Name = "tabTables_Page6";
			this.tabTables_Page6.Text = "Style Codes";
			// 
			// vsStyle
			// 
			this.vsStyle.AllowSelection = false;
			this.vsStyle.AllowUserToResizeColumns = false;
			this.vsStyle.AllowUserToResizeRows = false;
			this.vsStyle.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsStyle.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsStyle.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsStyle.BackColorBkg = System.Drawing.Color.Empty;
			this.vsStyle.BackColorFixed = System.Drawing.Color.Empty;
			this.vsStyle.BackColorSel = System.Drawing.Color.Empty;
			this.vsStyle.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsStyle.CellCharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.vsStyle.Cols = 10;
			dataGridViewCellStyle13.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsStyle.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
			this.vsStyle.ColumnHeadersHeight = 30;
			this.vsStyle.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle14.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsStyle.DefaultCellStyle = dataGridViewCellStyle14;
			this.vsStyle.DragIcon = null;
			this.vsStyle.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsStyle.ExtendLastCol = true;
			this.vsStyle.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsStyle.FrozenCols = 0;
			this.vsStyle.GridColor = System.Drawing.Color.Empty;
			this.vsStyle.GridColorFixed = System.Drawing.Color.Empty;
			this.vsStyle.Location = new System.Drawing.Point(20, 20);
			this.vsStyle.Name = "vsStyle";
			this.vsStyle.OutlineCol = 0;
			this.vsStyle.ReadOnly = true;
			this.vsStyle.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsStyle.RowHeightMin = 0;
			this.vsStyle.Rows = 50;
			this.vsStyle.ScrollTipText = null;
			this.vsStyle.ShowColumnVisibilityMenu = false;
			this.vsStyle.ShowFocusCell = false;
			this.vsStyle.Size = new System.Drawing.Size(976, 351);
			this.vsStyle.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsStyle.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsStyle.TabIndex = 7;
			this.vsStyle.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsStyle_KeyPressEdit);
			this.vsStyle.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsStyle_ValidateEdit);
			this.vsStyle.KeyDown += new Wisej.Web.KeyEventHandler(this.vsStyle_KeyDownEvent);
			// 
			// tabTables_Page7
			// 
			this.tabTables_Page7.Controls.Add(this.vsReasonCodes);
			this.tabTables_Page7.Controls.Add(this.vsPrint);
			this.tabTables_Page7.Controls.Add(this.Label8);
			this.tabTables_Page7.Controls.Add(this.Label11);
			this.tabTables_Page7.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page7.Name = "tabTables_Page7";
			this.tabTables_Page7.Text = "Reason Codes /  Print Priorities";
			// 
			// vsReasonCodes
			// 
			this.vsReasonCodes.AllowSelection = false;
			this.vsReasonCodes.AllowUserToResizeColumns = false;
			this.vsReasonCodes.AllowUserToResizeRows = false;
			this.vsReasonCodes.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
			this.vsReasonCodes.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsReasonCodes.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsReasonCodes.BackColorBkg = System.Drawing.Color.Empty;
			this.vsReasonCodes.BackColorFixed = System.Drawing.Color.Empty;
			this.vsReasonCodes.BackColorSel = System.Drawing.Color.Empty;
			this.vsReasonCodes.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsReasonCodes.Cols = 10;
			dataGridViewCellStyle15.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsReasonCodes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
			this.vsReasonCodes.ColumnHeadersHeight = 30;
			this.vsReasonCodes.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle16.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsReasonCodes.DefaultCellStyle = dataGridViewCellStyle16;
			this.vsReasonCodes.DragIcon = null;
			this.vsReasonCodes.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsReasonCodes.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsReasonCodes.FrozenCols = 0;
			this.vsReasonCodes.GridColor = System.Drawing.Color.Empty;
			this.vsReasonCodes.GridColorFixed = System.Drawing.Color.Empty;
			this.vsReasonCodes.Location = new System.Drawing.Point(20, 20);
			this.vsReasonCodes.Name = "vsReasonCodes";
			this.vsReasonCodes.OutlineCol = 0;
			this.vsReasonCodes.ReadOnly = true;
			this.vsReasonCodes.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsReasonCodes.RowHeightMin = 0;
			this.vsReasonCodes.Rows = 50;
			this.vsReasonCodes.ScrollTipText = null;
			this.vsReasonCodes.ShowColumnVisibilityMenu = false;
			this.vsReasonCodes.ShowFocusCell = false;
			this.vsReasonCodes.Size = new System.Drawing.Size(467, 308);
			this.vsReasonCodes.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsReasonCodes.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsReasonCodes.TabIndex = 8;
			this.vsReasonCodes.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsReasonCodes_ValidateEdit);
			this.vsReasonCodes.KeyDown += new Wisej.Web.KeyEventHandler(this.vsReasonCodes_KeyDownEvent);
			// 
			// vsPrint
			// 
			this.vsPrint.AllowSelection = false;
			this.vsPrint.AllowUserToResizeColumns = false;
			this.vsPrint.AllowUserToResizeRows = false;
			this.vsPrint.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
			this.vsPrint.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsPrint.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsPrint.BackColorBkg = System.Drawing.Color.Empty;
			this.vsPrint.BackColorFixed = System.Drawing.Color.Empty;
			this.vsPrint.BackColorSel = System.Drawing.Color.Empty;
			this.vsPrint.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsPrint.Cols = 10;
			dataGridViewCellStyle17.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsPrint.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
			this.vsPrint.ColumnHeadersHeight = 30;
			this.vsPrint.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle18.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsPrint.DefaultCellStyle = dataGridViewCellStyle18;
			this.vsPrint.DragIcon = null;
			this.vsPrint.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsPrint.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsPrint.FrozenCols = 0;
			this.vsPrint.GridColor = System.Drawing.Color.Empty;
			this.vsPrint.GridColorFixed = System.Drawing.Color.Empty;
			this.vsPrint.Location = new System.Drawing.Point(508, 20);
			this.vsPrint.Name = "vsPrint";
			this.vsPrint.OutlineCol = 0;
			this.vsPrint.ReadOnly = true;
			this.vsPrint.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsPrint.RowHeightMin = 0;
			this.vsPrint.Rows = 50;
			this.vsPrint.ScrollTipText = null;
			this.vsPrint.ShowColumnVisibilityMenu = false;
			this.vsPrint.ShowFocusCell = false;
			this.vsPrint.Size = new System.Drawing.Size(486, 308);
			this.vsPrint.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsPrint.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsPrint.TabIndex = 10;
			this.vsPrint.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsPrint_ValidateEdit);
			this.vsPrint.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPrint_KeyDownEvent);
			// 
			// Label8
			// 
			this.Label8.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Label8.Location = new System.Drawing.Point(550, 362);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(120, 15);
			this.Label8.TabIndex = 13;
			this.Label8.Text = "PRINT PRIORITY";
			// 
			// Label11
			// 
			this.Label11.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Label11.Location = new System.Drawing.Point(20, 362);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(182, 15);
			this.Label11.TabIndex = 12;
			this.Label11.Text = "INFORMATION REASON CODES";
			// 
			// tabTables_Page8
			// 
			this.tabTables_Page8.Controls.Add(this.vsAgentFee);
			this.tabTables_Page8.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page8.Name = "tabTables_Page8";
			this.tabTables_Page8.Text = "Fees";
			// 
			// vsAgentFee
			// 
			this.vsAgentFee.AllowSelection = false;
			this.vsAgentFee.AllowUserToResizeColumns = false;
			this.vsAgentFee.AllowUserToResizeRows = false;
			this.vsAgentFee.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsAgentFee.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsAgentFee.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsAgentFee.BackColorBkg = System.Drawing.Color.Empty;
			this.vsAgentFee.BackColorFixed = System.Drawing.Color.Empty;
			this.vsAgentFee.BackColorSel = System.Drawing.Color.Empty;
			this.vsAgentFee.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsAgentFee.CellCharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.vsAgentFee.Cols = 10;
			dataGridViewCellStyle19.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsAgentFee.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
			this.vsAgentFee.ColumnHeadersHeight = 30;
			this.vsAgentFee.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle20.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsAgentFee.DefaultCellStyle = dataGridViewCellStyle20;
			this.vsAgentFee.DragIcon = null;
			this.vsAgentFee.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsAgentFee.ExtendLastCol = true;
			this.vsAgentFee.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsAgentFee.FrozenCols = 0;
			this.vsAgentFee.GridColor = System.Drawing.Color.Empty;
			this.vsAgentFee.GridColorFixed = System.Drawing.Color.Empty;
			this.vsAgentFee.Location = new System.Drawing.Point(20, 20);
			this.vsAgentFee.Name = "vsAgentFee";
			this.vsAgentFee.OutlineCol = 0;
			this.vsAgentFee.ReadOnly = true;
			this.vsAgentFee.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsAgentFee.RowHeightMin = 0;
			this.vsAgentFee.Rows = 50;
			this.vsAgentFee.ScrollTipText = null;
			this.vsAgentFee.ShowColumnVisibilityMenu = false;
			this.vsAgentFee.ShowFocusCell = false;
			this.vsAgentFee.Size = new System.Drawing.Size(976, 351);
			this.vsAgentFee.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsAgentFee.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsAgentFee.TabIndex = 11;
			this.vsAgentFee.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsAgentFee_KeyPressEdit);
			this.vsAgentFee.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsAgentFee_ValidateEdit);
			// 
			// tabTables_Page9
			// 
			this.tabTables_Page9.Controls.Add(this.vsClass);
			this.tabTables_Page9.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page9.Name = "tabTables_Page9";
			this.tabTables_Page9.Text = "Class Codes";
			// 
			// vsClass
			// 
			this.vsClass.AllowSelection = false;
			this.vsClass.AllowUserToResizeColumns = false;
			this.vsClass.AllowUserToResizeRows = false;
			this.vsClass.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			//this.vsClass.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsClass.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsClass.BackColorBkg = System.Drawing.Color.Empty;
			this.vsClass.BackColorFixed = System.Drawing.Color.Empty;
			this.vsClass.BackColorSel = System.Drawing.Color.Empty;
			this.vsClass.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsClass.CellCharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.vsClass.Cols = 10;
			dataGridViewCellStyle21.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsClass.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
			this.vsClass.ColumnHeadersHeight = 30;
			this.vsClass.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle22.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsClass.DefaultCellStyle = dataGridViewCellStyle22;
			this.vsClass.DragIcon = null;
			this.vsClass.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsClass.FixedCols = 3;
			this.vsClass.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsClass.FrozenCols = 2;
			this.vsClass.GridColor = System.Drawing.Color.Empty;
			this.vsClass.GridColorFixed = System.Drawing.Color.Empty;
			this.vsClass.Location = new System.Drawing.Point(20, 20);
			this.vsClass.Name = "vsClass";
			this.vsClass.OutlineCol = 0;
			this.vsClass.ReadOnly = true;
			this.vsClass.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsClass.RowHeightMin = 0;
			this.vsClass.Rows = 50;
			this.vsClass.ScrollTipText = null;
			this.vsClass.ShowColumnVisibilityMenu = false;
			this.vsClass.ShowFocusCell = false;
			this.vsClass.Size = new System.Drawing.Size(976, 351);
			this.vsClass.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsClass.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsClass.TabIndex = 9;
			this.vsClass.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsClass_KeyPressEdit);
			this.vsClass.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsClass_ValidateEdit);
			this.vsClass.CurrentCellChanged += new System.EventHandler(this.vsClass_RowColChange);
			this.vsClass.KeyDown += new Wisej.Web.KeyEventHandler(this.vsClass_KeyDownEvent);
			// 
			// tabTables_Page10
			// 
			this.tabTables_Page10.Controls.Add(this.vsDefaults);
			this.tabTables_Page10.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page10.Name = "tabTables_Page10";
			this.tabTables_Page10.Text = "Default Values";
			// 
			// vsDefaults
			// 
			this.vsDefaults.AllowSelection = false;
			this.vsDefaults.AllowUserToResizeColumns = false;
			this.vsDefaults.AllowUserToResizeRows = false;
			this.vsDefaults.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsDefaults.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDefaults.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDefaults.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDefaults.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDefaults.BackColorSel = System.Drawing.Color.Empty;
			this.vsDefaults.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsDefaults.CellCharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.vsDefaults.Cols = 10;
			dataGridViewCellStyle23.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDefaults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
			this.vsDefaults.ColumnHeadersHeight = 30;
			this.vsDefaults.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle24.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDefaults.DefaultCellStyle = dataGridViewCellStyle24;
			this.vsDefaults.DragIcon = null;
			this.vsDefaults.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDefaults.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDefaults.FrozenCols = 0;
			this.vsDefaults.GridColor = System.Drawing.Color.Empty;
			this.vsDefaults.GridColorFixed = System.Drawing.Color.Empty;
			this.vsDefaults.Location = new System.Drawing.Point(20, 20);
			this.vsDefaults.Name = "vsDefaults";
			this.vsDefaults.OutlineCol = 0;
			this.vsDefaults.ReadOnly = true;
			this.vsDefaults.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDefaults.RowHeightMin = 0;
			this.vsDefaults.Rows = 50;
			this.vsDefaults.ScrollTipText = null;
			this.vsDefaults.ShowColumnVisibilityMenu = false;
			this.vsDefaults.ShowFocusCell = false;
			this.vsDefaults.Size = new System.Drawing.Size(976, 351);
			this.vsDefaults.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsDefaults.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsDefaults.TabIndex = 14;
			this.vsDefaults.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsDefaults_KeyPressEdit);
			this.vsDefaults.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsDefaults_ValidateEdit);
			this.vsDefaults.CurrentCellChanged += new System.EventHandler(this.vsDefaults_RowColChange);
			this.vsDefaults.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsDefaults_MouseMoveEvent);
			// 
			// tabTables_Page11
			// 
			this.tabTables_Page11.Controls.Add(this.SSTab1);
			this.tabTables_Page11.Location = new System.Drawing.Point(1, 47);
			this.tabTables_Page11.Name = "tabTables_Page11";
			this.tabTables_Page11.Text = "Gross Vehicle Weight Tables";
			// 
			// SSTab1
			// 
			this.SSTab1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.SSTab1.Controls.Add(this.SSTab1_Page1);
			this.SSTab1.Controls.Add(this.SSTab1_Page2);
			this.SSTab1.Controls.Add(this.SSTab1_Page3);
			this.SSTab1.Location = new System.Drawing.Point(20, 20);
			this.SSTab1.Name = "SSTab1";
			this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
			this.SSTab1.ShowFocusRect = false;
			this.SSTab1.Size = new System.Drawing.Size(976, 351);
			this.SSTab1.TabIndex = 36;
			this.SSTab1.TabsPerRow = 0;
            this.SSTab1.Text = "Commercial Truck / Truck Tractor";
			this.SSTab1.WordWrap = false;
			// 
			// SSTab1_Page1
			// 
			this.SSTab1_Page1.Controls.Add(this.vsGrossTable1);
			this.SSTab1_Page1.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Text = "Commercial Truck / Truck Tractor";
			// 
			// vsGrossTable1
			// 
			this.vsGrossTable1.AllowSelection = false;
			this.vsGrossTable1.AllowUserToResizeColumns = false;
			this.vsGrossTable1.AllowUserToResizeRows = false;
			this.vsGrossTable1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsGrossTable1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsGrossTable1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsGrossTable1.BackColorBkg = System.Drawing.Color.Empty;
			this.vsGrossTable1.BackColorFixed = System.Drawing.Color.Empty;
			this.vsGrossTable1.BackColorSel = System.Drawing.Color.Empty;
			this.vsGrossTable1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsGrossTable1.Cols = 5;
			dataGridViewCellStyle25.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsGrossTable1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
			this.vsGrossTable1.ColumnHeadersHeight = 30;
			this.vsGrossTable1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle26.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsGrossTable1.DefaultCellStyle = dataGridViewCellStyle26;
			this.vsGrossTable1.DragIcon = null;
			this.vsGrossTable1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsGrossTable1.FixedCols = 2;
			this.vsGrossTable1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsGrossTable1.FrozenCols = 1;
			this.vsGrossTable1.GridColor = System.Drawing.Color.Empty;
			this.vsGrossTable1.GridColorFixed = System.Drawing.Color.Empty;
			this.vsGrossTable1.Location = new System.Drawing.Point(20, 20);
			this.vsGrossTable1.Name = "vsGrossTable1";
			this.vsGrossTable1.OutlineCol = 0;
			this.vsGrossTable1.ReadOnly = true;
			this.vsGrossTable1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsGrossTable1.RowHeightMin = 0;
			this.vsGrossTable1.Rows = 50;
			this.vsGrossTable1.ScrollTipText = null;
			this.vsGrossTable1.ShowColumnVisibilityMenu = false;
			this.vsGrossTable1.ShowFocusCell = false;
			this.vsGrossTable1.Size = new System.Drawing.Size(932, 271);
			this.vsGrossTable1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsGrossTable1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsGrossTable1.TabIndex = 38;
            this.vsGrossTable1.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(VsGrossTable_EditingControlShowing);
			this.vsGrossTable1.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsGrossTable1_KeyPressEdit);
			this.vsGrossTable1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsGrossTable1_ValidateEdit);
			this.vsGrossTable1.KeyDown += new Wisej.Web.KeyEventHandler(this.vsGrossTable1_KeyDownEvent);
			// 
			// SSTab1_Page2
			// 
			this.SSTab1_Page2.Controls.Add(this.vsGrossTable2);
			this.SSTab1_Page2.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page2.Name = "SSTab1_Page2";
			this.SSTab1_Page2.Text = "Farm Truck / Motor Home";
			// 
			// vsGrossTable2
			// 
			this.vsGrossTable2.AllowSelection = false;
			this.vsGrossTable2.AllowUserToResizeColumns = false;
			this.vsGrossTable2.AllowUserToResizeRows = false;
			this.vsGrossTable2.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsGrossTable2.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsGrossTable2.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsGrossTable2.BackColorBkg = System.Drawing.Color.Empty;
			this.vsGrossTable2.BackColorFixed = System.Drawing.Color.Empty;
			this.vsGrossTable2.BackColorSel = System.Drawing.Color.Empty;
			this.vsGrossTable2.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsGrossTable2.Cols = 5;
			dataGridViewCellStyle27.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsGrossTable2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle27;
			this.vsGrossTable2.ColumnHeadersHeight = 30;
			this.vsGrossTable2.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle28.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsGrossTable2.DefaultCellStyle = dataGridViewCellStyle28;
			this.vsGrossTable2.DragIcon = null;
			this.vsGrossTable2.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsGrossTable2.FixedCols = 2;
			this.vsGrossTable2.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsGrossTable2.FrozenCols = 1;
			this.vsGrossTable2.GridColor = System.Drawing.Color.Empty;
			this.vsGrossTable2.GridColorFixed = System.Drawing.Color.Empty;
			this.vsGrossTable2.Location = new System.Drawing.Point(20, 20);
			this.vsGrossTable2.Name = "vsGrossTable2";
			this.vsGrossTable2.OutlineCol = 0;
			this.vsGrossTable2.ReadOnly = true;
			this.vsGrossTable2.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsGrossTable2.RowHeightMin = 0;
			this.vsGrossTable2.Rows = 50;
			this.vsGrossTable2.ScrollTipText = null;
			this.vsGrossTable2.ShowColumnVisibilityMenu = false;
			this.vsGrossTable2.ShowFocusCell = false;
			this.vsGrossTable2.Size = new System.Drawing.Size(932, 271);
			this.vsGrossTable2.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsGrossTable2.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsGrossTable2.TabIndex = 40;
            this.vsGrossTable2.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(VsGrossTable_EditingControlShowing);
			this.vsGrossTable2.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsGrossTable2_KeyPressEdit);
			this.vsGrossTable2.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsGrossTable2_ValidateEdit);
			this.vsGrossTable2.KeyDown += new Wisej.Web.KeyEventHandler(this.vsGrossTable2_KeyDownEvent);
			// 
			// SSTab1_Page3
			// 
			this.SSTab1_Page3.Controls.Add(this.vsGrossTable3);
			this.SSTab1_Page3.Location = new System.Drawing.Point(1, 47);
			this.SSTab1_Page3.Name = "SSTab1_Page3";
			this.SSTab1_Page3.Text = "Class A Special Mobile Equipment";
			// 
			// vsGrossTable3
			// 
			this.vsGrossTable3.AllowSelection = false;
			this.vsGrossTable3.AllowUserToResizeColumns = false;
			this.vsGrossTable3.AllowUserToResizeRows = false;
			this.vsGrossTable3.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsGrossTable3.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsGrossTable3.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsGrossTable3.BackColorBkg = System.Drawing.Color.Empty;
			this.vsGrossTable3.BackColorFixed = System.Drawing.Color.Empty;
			this.vsGrossTable3.BackColorSel = System.Drawing.Color.Empty;
			this.vsGrossTable3.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsGrossTable3.Cols = 5;
			dataGridViewCellStyle29.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsGrossTable3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
			this.vsGrossTable3.ColumnHeadersHeight = 30;
			this.vsGrossTable3.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle30.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsGrossTable3.DefaultCellStyle = dataGridViewCellStyle30;
			this.vsGrossTable3.DragIcon = null;
			this.vsGrossTable3.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsGrossTable3.FixedCols = 2;
			this.vsGrossTable3.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsGrossTable3.FrozenCols = 1;
			this.vsGrossTable3.GridColor = System.Drawing.Color.Empty;
			this.vsGrossTable3.GridColorFixed = System.Drawing.Color.Empty;
			this.vsGrossTable3.Location = new System.Drawing.Point(20, 20);
			this.vsGrossTable3.Name = "vsGrossTable3";
			this.vsGrossTable3.OutlineCol = 0;
			this.vsGrossTable3.ReadOnly = true;
			this.vsGrossTable3.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsGrossTable3.RowHeightMin = 0;
			this.vsGrossTable3.Rows = 50;
			this.vsGrossTable3.ScrollTipText = null;
			this.vsGrossTable3.ShowColumnVisibilityMenu = false;
			this.vsGrossTable3.ShowFocusCell = false;
			this.vsGrossTable3.Size = new System.Drawing.Size(932, 271);
			this.vsGrossTable3.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsGrossTable3.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsGrossTable3.TabIndex = 41;
            this.vsGrossTable3.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(VsGrossTable_EditingControlShowing);
			this.vsGrossTable3.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsGrossTable3_KeyPressEdit);
			this.vsGrossTable3.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsGrossTable3_ValidateEdit);
			this.vsGrossTable3.KeyDown += new Wisej.Web.KeyEventHandler(this.vsGrossTable3_KeyDownEvent);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessPrint,
            this.mnuseperator1,
            this.mnuSaveQuit,
            this.mnusperator2,
            this.mnuQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessPrint
			// 
			this.mnuProcessPrint.Index = 0;
			this.mnuProcessPrint.Name = "mnuProcessPrint";
			this.mnuProcessPrint.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuProcessPrint.Text = "Print Table";
			this.mnuProcessPrint.Click += new System.EventHandler(this.mnuProcessPrint_Click);
			// 
			// mnuseperator1
			// 
			this.mnuseperator1.Index = 1;
			this.mnuseperator1.Name = "mnuseperator1";
			this.mnuseperator1.Text = "-";
			// 
			// mnuSaveQuit
			// 
			this.mnuSaveQuit.Index = 2;
			this.mnuSaveQuit.Name = "mnuSaveQuit";
			this.mnuSaveQuit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveQuit.Text = "Save and Exit";
			this.mnuSaveQuit.Click += new System.EventHandler(this.mnuSaveQuit_Click);
			// 
			// mnusperator2
			// 
			this.mnusperator2.Index = 3;
			this.mnusperator2.Name = "mnusperator2";
			this.mnusperator2.Text = "-";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 4;
			this.mnuQuit.Name = "mnuQuit";
			this.mnuQuit.Text = "Exit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// cmdSaveQuit
			// 
			this.cmdSaveQuit.AppearanceKey = "acceptButton";
			this.cmdSaveQuit.Location = new System.Drawing.Point(457, 30);
			this.cmdSaveQuit.Name = "cmdSaveQuit";
			this.cmdSaveQuit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveQuit.Size = new System.Drawing.Size(80, 48);
			this.cmdSaveQuit.TabIndex = 0;
			this.cmdSaveQuit.Text = "Save";
			this.cmdSaveQuit.Click += new System.EventHandler(this.mnuSaveQuit_Click);
			// 
			// cmdProcessPrint
			// 
			this.cmdProcessPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdProcessPrint.AppearanceKey = "toolbarButton";
			this.cmdProcessPrint.Location = new System.Drawing.Point(946, 29);
			this.cmdProcessPrint.Name = "cmdProcessPrint";
			this.cmdProcessPrint.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdProcessPrint.Size = new System.Drawing.Size(84, 24);
			this.cmdProcessPrint.TabIndex = 1;
			this.cmdProcessPrint.Text = "Print Table";
			this.cmdProcessPrint.Click += new System.EventHandler(this.mnuProcessPrint_Click);
			// 
			// frmTableMaintenance
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1078, 646);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmTableMaintenance";
			this.Text = "Table / Option Processing";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmTableMaintenance_Load);
			this.Activated += new System.EventHandler(this.frmTableMaintenance_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTableMaintenance_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTableMaintenance_KeyPress);
			this.Resize += new System.EventHandler(this.frmTableMaintenance_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTables)).EndInit();
			this.fraTables.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.tabTables.ResumeLayout(false);
			this.tabTables_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsResCodes)).EndInit();
			this.tabTables_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsCounty)).EndInit();
			this.tabTables_Page3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsMake)).EndInit();
			this.tabTables_Page4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsOperators)).EndInit();
			this.tabTables_Page5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsColor)).EndInit();
			this.tabTables_Page6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsStyle)).EndInit();
			this.tabTables_Page7.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsReasonCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPrint)).EndInit();
			this.tabTables_Page8.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsAgentFee)).EndInit();
			this.tabTables_Page9.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsClass)).EndInit();
			this.tabTables_Page10.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsDefaults)).EndInit();
			this.tabTables_Page11.ResumeLayout(false);
			this.SSTab1.ResumeLayout(false);
			this.SSTab1_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsGrossTable1)).EndInit();
			this.SSTab1_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsGrossTable2)).EndInit();
			this.SSTab1_Page3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsGrossTable3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessPrint)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSaveQuit;
        private FCButton cmdProcessPrint;
    }
}