//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmInventoryAdjust.
	/// </summary>
	partial class frmInventoryAdjust
	{
		public fecherFoundation.FCComboBox cmbAdjust;
		public fecherFoundation.FCComboBox cmbAdd;
		public fecherFoundation.FCLabel lblAdd;
		public fecherFoundation.FCTextBox txtGroup;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCTextBox txtComment;
		public Global.T2KDateBox txtInventoryAdjustDate;
		public fecherFoundation.FCTextBox txtInitials;
		public fecherFoundation.FCTextBox txtHigh;
		public fecherFoundation.FCTextBox txtLow;
		public fecherFoundation.FCLabel lblWarning;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblAdjustType;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbAdjust = new fecherFoundation.FCComboBox();
            this.cmbAdd = new fecherFoundation.FCComboBox();
            this.lblAdd = new fecherFoundation.FCLabel();
            this.txtGroup = new fecherFoundation.FCTextBox();
            this.cmdDone = new fecherFoundation.FCButton();
            this.txtComment = new fecherFoundation.FCTextBox();
            this.txtInventoryAdjustDate = new Global.T2KDateBox();
            this.txtInitials = new fecherFoundation.FCTextBox();
            this.txtHigh = new fecherFoundation.FCTextBox();
            this.txtLow = new fecherFoundation.FCTextBox();
            this.lblWarning = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblAdjustType = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInventoryAdjustDate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdDone);
            this.BottomPanel.Location = new System.Drawing.Point(0, 388);
            this.BottomPanel.Size = new System.Drawing.Size(536, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.cmbAdjust);
            this.ClientArea.Controls.Add(this.txtGroup);
            this.ClientArea.Controls.Add(this.cmbAdd);
            this.ClientArea.Controls.Add(this.lblAdd);
            this.ClientArea.Controls.Add(this.txtComment);
            this.ClientArea.Controls.Add(this.txtInventoryAdjustDate);
            this.ClientArea.Controls.Add(this.txtInitials);
            this.ClientArea.Controls.Add(this.txtHigh);
            this.ClientArea.Controls.Add(this.txtLow);
            this.ClientArea.Controls.Add(this.lblWarning);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.lblAdjustType);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(556, 498);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblAdjustType, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblWarning, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtLow, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtHigh, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtInitials, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtInventoryAdjustDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblAdd, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbAdd, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtGroup, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbAdjust, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(556, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.AutoSize = false;
            this.HeaderText.Size = new System.Drawing.Size(296, 30);
            this.HeaderText.Text = "Inventory Adjustment";
            // 
            // cmbAdjust
            // 
            this.cmbAdjust.Items.AddRange(new object[] {
            "NOT show as an adjustment",
            "Show as an adjustment"});
            this.cmbAdjust.Location = new System.Drawing.Point(201, 128);
            this.cmbAdjust.Name = "cmbAdjust";
            this.cmbAdjust.Size = new System.Drawing.Size(327, 40);
            this.cmbAdjust.TabIndex = 1001;
            this.cmbAdjust.Text = "Show as an adjustment";
            this.cmbAdjust.Visible = false;
            this.cmbAdjust.SelectedIndexChanged += new System.EventHandler(this.cmbAdjust_SelectedIndexChanged);
            // 
            // cmbAdd
            // 
            this.cmbAdd.Items.AddRange(new object[] {
            "Add to Inventory",
            "Remove from Inventory"});
            this.cmbAdd.Location = new System.Drawing.Point(201, 68);
            this.cmbAdd.Name = "cmbAdd";
            this.cmbAdd.Size = new System.Drawing.Size(327, 40);
            this.cmbAdd.TabIndex = 6;
            this.cmbAdd.Text = "Add to Inventory";
            this.cmbAdd.SelectedIndexChanged += new System.EventHandler(this.cmbAdd_SelectedIndexChanged);
            // 
            // lblAdd
            // 
            this.lblAdd.AutoSize = true;
            this.lblAdd.Location = new System.Drawing.Point(30, 82);
            this.lblAdd.Name = "lblAdd";
            this.lblAdd.Size = new System.Drawing.Size(129, 15);
            this.lblAdd.TabIndex = 7;
            this.lblAdd.Text = "SELECT AN ACTION";
            // 
            // txtGroup
            // 
            this.txtGroup.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroup.Location = new System.Drawing.Point(163, 298);
            this.txtGroup.MaxLength = 2;
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Size = new System.Drawing.Size(41, 40);
            this.txtGroup.TabIndex = 5;
            this.txtGroup.Text = "0";
            this.txtGroup.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.ToolTip1.SetToolTip(this.txtGroup, "Inventory Group 0 - 12");
            this.txtGroup.Validating += new System.ComponentModel.CancelEventHandler(this.txtGroup_Validating);
            // 
            // cmdDone
            // 
            this.cmdDone.AppearanceKey = "acceptButton";
            this.cmdDone.Enabled = false;
            this.cmdDone.Location = new System.Drawing.Point(224, 30);
            this.cmdDone.Name = "cmdDone";
            this.cmdDone.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdDone.Size = new System.Drawing.Size(102, 48);
            this.cmdDone.TabIndex = 7;
            this.cmdDone.Text = "Process";
            this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
            // 
            // txtComment
            // 
            this.txtComment.BackColor = System.Drawing.SystemColors.Window;
            this.txtComment.Location = new System.Drawing.Point(224, 298);
            this.txtComment.MaxLength = 254;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(304, 40);
            this.txtComment.TabIndex = 6;
            // 
            // txtInventoryAdjustDate
            // 
            this.txtInventoryAdjustDate.Location = new System.Drawing.Point(30, 298);
            this.txtInventoryAdjustDate.Mask = "##/##/####";
            this.txtInventoryAdjustDate.MaxLength = 10;
            this.txtInventoryAdjustDate.Name = "txtInventoryAdjustDate";
            this.txtInventoryAdjustDate.Size = new System.Drawing.Size(115, 22);
            this.txtInventoryAdjustDate.TabIndex = 4;
            // 
            // txtInitials
            // 
            this.txtInitials.BackColor = System.Drawing.SystemColors.Window;
            this.txtInitials.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtInitials.Location = new System.Drawing.Point(323, 213);
            this.txtInitials.MaxLength = 3;
            this.txtInitials.Name = "txtInitials";
            this.txtInitials.Size = new System.Drawing.Size(88, 40);
            this.txtInitials.TabIndex = 3;
            this.txtInitials.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtInitials.TextChanged += new System.EventHandler(this.txtInitials_TextChanged);
            this.txtInitials.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtInitials_KeyPress);
            // 
            // txtHigh
            // 
            this.txtHigh.BackColor = System.Drawing.SystemColors.Window;
            this.txtHigh.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtHigh.Location = new System.Drawing.Point(194, 213);
            this.txtHigh.Name = "txtHigh";
            this.txtHigh.Size = new System.Drawing.Size(110, 40);
            this.txtHigh.TabIndex = 2;
            this.txtHigh.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtHigh_KeyPress);
            // 
            // txtLow
            // 
            this.txtLow.BackColor = System.Drawing.SystemColors.Window;
            this.txtLow.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLow.Location = new System.Drawing.Point(30, 213);
            this.txtLow.Name = "txtLow";
            this.txtLow.Size = new System.Drawing.Size(110, 40);
            this.txtLow.TabIndex = 1;
            this.txtLow.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLow_KeyPress);
            // 
            // lblWarning
            // 
            this.lblWarning.Location = new System.Drawing.Point(30, 357);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(498, 31);
            this.lblWarning.TabIndex = 21;
            this.lblWarning.Text = "WARNING: LARGE INVENTORY ADDITIONS MAY TAKE SEVERAL MINUTES TO PROCESS.  PLEASE D" +
    "O NOT ATTEMPT TO CLOSE THE PROGRAM UNTIL THE PROCESS COMPLETES";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(163, 273);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(50, 15);
            this.Label6.TabIndex = 17;
            this.Label6.Text = "GROUP";
            // 
            // lblAdjustType
            // 
            this.lblAdjustType.Location = new System.Drawing.Point(30, 30);
            this.lblAdjustType.Name = "lblAdjustType";
            this.lblAdjustType.Size = new System.Drawing.Size(262, 19);
            this.lblAdjustType.TabIndex = 13;
            this.lblAdjustType.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(224, 273);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(304, 15);
            this.Label5.TabIndex = 12;
            this.Label5.Text = "COMMENT";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 273);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(113, 15);
            this.Label4.TabIndex = 11;
            this.Label4.Text = "DATE";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(323, 188);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(84, 15);
            this.Label3.TabIndex = 10;
            this.Label3.Text = "USER INITIALS";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(158, 227);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(20, 15);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "TO";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 188);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(274, 15);
            this.Label1.TabIndex = 1002;
            this.Label1.Text = "RANGE";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(30, 142);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(159, 15);
            this.fcLabel1.TabIndex = 22;
            this.fcLabel1.Text = "THIS REMOVAL SHOULD";
            this.fcLabel1.Visible = false;
            // 
            // frmInventoryAdjust
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(556, 558);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmInventoryAdjust";
            this.Text = "Inventory Adjustment";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmInventoryAdjust_Load);
            this.Activated += new System.EventHandler(this.frmInventoryAdjust_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmInventoryAdjust_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInventoryAdjustDate)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCLabel fcLabel1;
	}
}
