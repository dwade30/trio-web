//Fecher vbPorter - Version 1.0.0.59

using System;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using fecherFoundation;
using Global;
using GrapeCity.ActiveReports.Export.Word.Section;
using SharedApplication.Enums;
using SharedApplication.MotorVehicle.Commands;
using TWSharedLibrary;
using Wisej.Web;
using Application = System.Windows.Forms.Application;

namespace TWMV0000
{
    public partial class frmSavePrint : BaseForm
    {
        public frmSavePrint()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null && Application.OpenForms.Count == 0)
                _InstancePtr = this;
        }
        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmSavePrint InstancePtr
        {
            get
            {
                return (frmSavePrint)Sys.GetInstance(typeof(frmSavePrint));
            }
        }

        protected frmSavePrint _InstancePtr;
        //=========================================================
        bool bolHoldingRegistration;
        clsDRWrapper rsDCTA = new clsDRWrapper();
        clsDRWrapper rsExceptionReport = new clsDRWrapper();
        clsDRWrapper rsExciseTax = new clsDRWrapper();
        clsDRWrapper rsGCRegister = new clsDRWrapper();
        clsDRWrapper rsInventoryAdjustments = new clsDRWrapper();
        clsDRWrapper rsInventory = new clsDRWrapper();
        clsDRWrapper rsTempPlateRegister = new clsDRWrapper();
        clsDRWrapper rsTitle = new clsDRWrapper();
        clsDRWrapper rsTitleApp = new clsDRWrapper();
        clsDRWrapper rsUseTax = new clsDRWrapper();
        clsDRWrapper rsFinal2Activity = new clsDRWrapper();
        bool AlreadyWaited;
        bool PrintingDouble;
        string ReverseLine;
        int lngNoFeeDupRegMVR3Number;
        public bool blnExport;
        bool blnLabelClicked;

        const string needed = "NEEDED";
        const string done = "DONE";
        const string twgnenty = "TWGNENTY";
        const string motorVehicle = "MOTOR_VEHICLE";
        const string NEWLINE = "\r\n";

        public bool IsPreReg { set; get; }

        private void cmdReturn_Click(object sender, EventArgs e)
        {
            InstancePtr.Unload();
        }

        private void frmSavePrint_KeyDown(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (!blnLabelClicked)
            {
                switch (KeyCode)
                {
                    case Keys.F9:
                        {
                            if (lblCTA.Enabled)
                            {
                                
                                lblCTA_Click();
                            }

                            break;
                        }

                    case Keys.F2:
                        {
                            if (lblUseTax.Enabled)
                            {
                                
                                lblUseTax_Click();
                            }

                            break;
                        }

                    case Keys.F3:
                        {
                            if (lblMVR3.Enabled)
                            {
                                
                                lblMVR3_Click();
                            }

                            break;
                        }

                    case Keys.F4:
                        {
                            if (lblHold.Enabled)
                            {
                                
                                lblHold_Click();
                            }

                            break;
                        }

                    case Keys.F5:
                        {
                            if (lblSave.Enabled)
                            {
                                
                                lblSave_Click();
                            }

                            break;
                        }

                    case Keys.F6:
                        {
                            if (lblBatchRegistration.Enabled && lblBatchRegistration.Visible)
                            {
                                
                                lblBatchRegistration_Click();
                            }

                            break;
                        }
                }
            }
        }

        private void frmSavePrint_Load(object sender, EventArgs e)
        {

            MotorVehicle.Statics.gboolFromBatchRegistration = false;
            var titleIsDone = MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone") && FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone"));
            lblCTA.Enabled = MotorVehicle.Statics.blnTownPrintsLongTermUseTaxCTA && MotorVehicle.Statics.gboolFromLongTermDataEntry 
                ? titleIsDone 
                : !MotorVehicle.Statics.gboolFromLongTermDataEntry && titleIsDone;

            var useTaxIsDone = MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone") && FCConvert.ToBoolean(MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone"));

            lblUseTax.Enabled = MotorVehicle.Statics.blnTownPrintsUseTax && !MotorVehicle.Statics.gboolFromLongTermDataEntry
                ? useTaxIsDone 
                : MotorVehicle.Statics.blnTownPrintsLongTermUseTaxCTA && MotorVehicle.Statics.gboolFromLongTermDataEntry && useTaxIsDone;

            lblSave.Enabled = false;

            if ((MotorVehicle.Statics.gboolFromLongTermDataEntry && MotorVehicle.Statics.RegistrationType == "NRR" && (Strings.UCase(Strings.Trim(modGlobalConstants.Statics.MuniName)) == "MAINE MOTOR TRANSPORT" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || Strings.UCase(Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION")) || (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber")) != 0 && MotorVehicle.Statics.RegistrationType == "NRR" && !MotorVehicle.Statics.RegisteringFleet))
            {
                Label7.Visible = true;
                lblBatchRegistration.Visible = true;
            }
            else
            {
                Label7.Visible = false;
                lblBatchRegistration.Visible = false;
            }

            lblBatchRegistration.Enabled = MotorVehicle.Statics.NeedToPrintCTA || MotorVehicle.Statics.NeedToPrintUseTax;

            if (MotorVehicle.Statics.NeedToPrintCTA) lblTitleDone.Text = needed;

            if (MotorVehicle.Statics.NeedToPrintUseTax) lblUseTaxDone.Text = needed;

            lblMVR3Done.Text = needed;

            lblHold.Enabled = MotorVehicle.Statics.RegistrationType != "DUPREG" && MotorVehicle.Statics.RegistrationType != "LOST" && MotorVehicle.Statics.RegistrationType != "DUPSTK" && MotorVehicle.Statics.RegistrationType != "GVW" && MotorVehicle.Statics.RegistrationType != "CORR" && !MotorVehicle.Statics.HoldRegistrationDollar;

            lblMVR3.Text = !MotorVehicle.Statics.gboolFromLongTermDataEntry ? "Print MVR3" : "Print MVRT10";

            lblPrintQueue.Visible = MotorVehicle.Statics.gboolFromLongTermDataEntry && (Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES" || Strings.UCase(Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION");
            blnLabelClicked = false;
            modGlobalFunctions.SetTRIOColors(this, false);
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
        }

        private void frmSavePrint_MouseMove(object sender, MouseEventArgs e)
        {
            ClearColor();
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            bolHoldingRegistration = false;
            // 
            rsDCTA.Reset();
            rsExceptionReport.Reset();
            rsExciseTax.Reset();
            rsGCRegister.Reset();
            rsInventoryAdjustments.Reset();
            rsInventory.Reset();
            rsTempPlateRegister.Reset();
            rsTitle.Reset();
            MotorVehicle.Statics.rsDoubleTitle.Reset();
            rsTitleApp.Reset();
            rsUseTax.Reset();
            rsFinal2Activity.Reset();
            // 
            ReverseLine = "";
        }

        private void lblBatchRegistration_Click(object sender, EventArgs e)
        {
            frmBatchProcessing.InstancePtr.Show(FormShowEnum.Modal);
        }

        public void lblBatchRegistration_Click()
        {
            lblBatchRegistration_Click(lblBatchRegistration, new EventArgs());
        }

        private void lblBatchRegistration_MouseMove(object sender, MouseEventArgs e)
        {
            HighlightField(ref lblBatchRegistration);
        }

        private void lblCTA_Click(object sender, EventArgs e)
        {
            clsDRWrapper rsTitle = new clsDRWrapper();
            string NewCTA = "";
            string temp = "";
            blnLabelClicked = true;
            if (MotorVehicle.Statics.lngCTAAddNewID != 0)
            {
                rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
                if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
                {
                    rsTitle.MoveLast();
                    rsTitle.MoveFirst();
                }
            }
            if (Strings.UCase(lblTitleDone.Text) == done)
            {
                NewCTA = Interaction.InputBox("Please enter the new CTA Number to use.", "CTA Number", null);
                if (NewCTA == "")
                {
                    MessageBox.Show("You must enter a new CTA Number before you may print.", "Invalid CTA Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    blnLabelClicked = false;
                    return;
                }

                MotorVehicle.Statics.rsFinal.Set_Fields("CTANumber", NewCTA.ToUpper	());
            }
            
            temp = StaticSettings.GlobalSettingService.GetSettingValue(SettingOwnerType.Machine, "CTAPrinterName")?.SettingValue ?? "";

			if (isTxtFile(temp))
            {
                rptNewCTA.InstancePtr.Run(false);
                rptNewCTA.InstancePtr.Unload();
            }
            else
            {
                if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to level as possible) next to the line titled:" + NEWLINE + "THIS IS NOT A CERTIFICATE OF TITLE", 47, 1, MotorVehicle.Statics.gstrCTAPrinterName) == DialogResult.Cancel)
                {
                    MotorVehicle.Statics.blnPrintCTA = false;
                }
                else
                {
                    rptNewCTA.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
                    rptNewCTA.InstancePtr.Unload();
                }
            }
            if (MotorVehicle.Statics.blnPrintCTA)
            {
                if (isNotDoublePrint(rsTitle))
                {
                    lblTitleDone.Text = "DONE";
                    if (isMvr3NotNeeded() && Strings.UCase(lblUseTaxDone.Text) != needed)
                    {
                        lblSave.Enabled = true;
                    }
                    if (Strings.UCase(lblUseTaxDone.Text) != needed)
                    {
                        lblBatchRegistration.Enabled = true;
                    }
                }
                else
                {
                    MotorVehicle.Statics.boolDoubleCTA = true;
                    MessageBox.Show("Click OK when ready to print the second CTA.", "Double CTA");

                    temp = StaticSettings.GlobalSettingService.GetSettingValue(SettingOwnerType.Machine, "CTAPrinterName")?.SettingValue ?? "";

					if (isTxtFile(temp))
                    {
                        rptNewCTA.InstancePtr.Run(false);
                        rptNewCTA.InstancePtr.Unload();
                    }
                    else
                    {
                        if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to level as possible) next to the line titled:" + NEWLINE + "THIS IS NOT A CERTIFICATE OF TITLE", 47, 1, MotorVehicle.Statics.gstrCTAPrinterName) == DialogResult.Cancel)
                        {
                            MotorVehicle.Statics.blnPrintCTA = false;
                        }
                        else
                        {
                            rptNewCTA.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
                            rptNewCTA.InstancePtr.Unload();
                        }
                    }
                    if (MotorVehicle.Statics.blnPrintDoubleCTA)
                    {
                        lblTitleDone.Text = "DONE";
                        if (isMvr3NotNeeded() && isUseTaxNotNeeded()) lblSave.Enabled = true;
                        if (isUseTaxNotNeeded()) lblBatchRegistration.Enabled = true;
                    }
                }
            }
            Information.Err().Clear();
            
            blnLabelClicked = false;
        }

        private static bool isNotDoublePrint(clsDRWrapper rsTitle)
        {
            return FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) == 0 || (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0 && MotorVehicle.Statics.PrintDouble == false);
        }

        private bool isUseTaxNotNeeded()
        {
            return Strings.UCase(lblUseTaxDone.Text) != needed;
        }

        public void lblCTA_Click()
        {
            lblCTA_Click(lblCTA, new EventArgs());
        }

        private void lblCTA_MouseMove(object sender, MouseEventArgs e)
        {
            HighlightField(ref lblCTA);
        }

        private void HighlightField(ref FCLabel fieldToHighlight)
        {
            if (fieldToHighlight.BackColor == SystemColors.Highlight) return;

            ClearColor();
            ChangeBackground(ref fieldToHighlight);
        }

        private void lblHold_Click(object sender, EventArgs e)
        {
            clsDRWrapper rsTitleUseTax = new clsDRWrapper();
            clsDRWrapper rsPlate = new clsDRWrapper();
            MotorVehicle.Statics.rsFinalCompare.Edit();
            FCGlobal.Screen.MousePointer = (MousePointerConstants)11;
            clsDRWrapper rsHold = new clsDRWrapper();
            clsDRWrapper rsTitle = new clsDRWrapper();

            blnLabelClicked = true;
            lblMVR3.Enabled = false;

            MotorVehicle.Statics.strSql = "SELECT * FROM HeldRegistrationMaster WHERE ID = 0";
            rsHold.OpenRecordset(MotorVehicle.Statics.strSql);
            if (rsHold.EndOfFile() != true && rsHold.BeginningOfFile() != true)
            {
                rsHold.MoveFirst();
                rsHold.MoveLast();
                rsHold.AddNew();
            }
            else
            {
                rsHold.AddNew();
            }
            for (MotorVehicle.Statics.fnx = 0; MotorVehicle.Statics.fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; MotorVehicle.Statics.fnx++)
            {
                var fnx = MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx);

                switch (fnx)
                {
                    case "ID":
                        rsHold.Set_Fields("MasterID", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));

                        break;
                    case "MasterID":
                        rsHold.Set_Fields(fnx, MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));

                        break;
                    default:
                        rsHold.Set_Fields(fnx, MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(MotorVehicle.Statics.fnx));

                        break;
                }
            }
            rsHold.Set_Fields("Status", "H");
            rsHold.Set_Fields("MVR3", 0);
            rsHold.Set_Fields("MonthStickerCharge", 0);
            rsHold.Set_Fields("MonthStickerNoCharge", 0);
            rsHold.Set_Fields("MonthStickerMonth", 0);
            rsHold.Set_Fields("MonthStickerNumber", 0);
            rsHold.Set_Fields("YearStickerCharge", 0);
            rsHold.Set_Fields("YearStickerNoCharge", 0);
            rsHold.Set_Fields("YearStickerYear", 0);
            rsHold.Set_Fields("YearStickerNumber", 0);
            rsHold.Set_Fields("DateUpdated", DateTime.Today);
            rsHold.Update();
            rsHold.AddNew();
            MotorVehicle.Statics.rsFinalCompare.Set_Fields("Status", "FC");
            for (MotorVehicle.Statics.fnx = 0; MotorVehicle.Statics.fnx <= MotorVehicle.Statics.rsFinalCompare.FieldsCount - 1; MotorVehicle.Statics.fnx++)
            {
                if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx) == "Plate" && (MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.NewToTheSystem))
                {
                    rsHold.Set_Fields(MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexName(MotorVehicle.Statics.fnx), 
                                      Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexValue(MotorVehicle.Statics.fnx))) == "" 
                                          ? MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate") 
                                          : MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexValue(MotorVehicle.Statics.fnx));
                }
                else
                {
                    if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx) == "ID")
                        rsHold.Set_Fields("MasterID", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));
                    else 
                        if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx) == "MasterID")
                            rsHold.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(MotorVehicle.Statics.fnx), MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("ID"));
                        else
                            rsHold.Set_Fields(MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexName(MotorVehicle.Statics.fnx), MotorVehicle.Statics.rsFinalCompare.Get_FieldsIndexValue(MotorVehicle.Statics.fnx));
                }
            }
            rsHold.Set_Fields("DateUpdated", DateTime.Today);
            rsHold.Update();

            var plate = MotorVehicle.Statics.rsFinal.Get_Fields_String("plate");
            Hold_Plate_From_Inventory(FCConvert.ToString(plate));
            rsTitleUseTax.OpenRecordset("SELECT * FROM HeldTitleUseTax WHERE Plate = '" + plate + "'");
            if (rsTitleUseTax.EndOfFile() != true && rsTitleUseTax.BeginningOfFile() != true)
            {
                rsTitleUseTax.MoveLast();
                rsTitleUseTax.MoveFirst();
                rsTitleUseTax.Edit();
            }
            else
            {
                rsTitleUseTax.AddNew();
            }
            rsTitleUseTax.Set_Fields("HoldID", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));
            rsTitleUseTax.Set_Fields("plate", plate);
            rsTitleUseTax.Set_Fields("UseTaxID", MotorVehicle.Statics.lngUTCAddNewID);
            rsTitleUseTax.Set_Fields("TitleID", MotorVehicle.Statics.lngCTAAddNewID);
            rsTitleUseTax.Set_Fields("PrintTitle", MotorVehicle.Statics.NeedToPrintCTA ? 1 : 0);

            rsTitleUseTax.Set_Fields("PrintUseTax", MotorVehicle.Statics.NeedToPrintUseTax ? 1 : 0);

            rsTitleUseTax.Set_Fields("PrintTitleDouble", MotorVehicle.Statics.PrintDouble ? 1 : 0);
            rsTitleUseTax.Update();
            rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
            if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
            {
                if (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
                {
                    MotorVehicle.Statics.rsDoubleTitle.Update();
                }
            }
            MotorVehicle.Statics.rsFinal.Reset();
            rsHold.Reset();
            MotorVehicle.Statics.rsFinalCompare.Reset();
            rsPlate.Reset();
            MotorVehicle.Statics.rsDoubleTitle.Reset();
            FCGlobal.Screen.MousePointer = 0;
            blnLabelClicked = false;
            InstancePtr.Close();
            frmPreview.InstancePtr.Unload();
            frmDataInput.InstancePtr.Unload();
            frmDataInputLongTermTrailers.InstancePtr.Unload();
        }

        public void lblHold_Click()
        {
            lblHold_Click(lblHold, new EventArgs());
        }

        private void lblHold_MouseMove(object sender, MouseEventArgs e)
        {
            HighlightField(ref lblHold);
        }

        private void lblMVR3_Click(object sender, EventArgs e)
        {
            // vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
            DialogResult answer = 0;
            string temp = "";
            int NewMVR3Number = 0;
            clsDRWrapper rsTemp = new clsDRWrapper();
            if (blnLabelClicked) return;

            blnLabelClicked = true;
            lblHold.Enabled = false;
            MotorVehicle.Statics.blnPrintingDupReg = false;
            // MsgBox "Start Procedure"
            if (!MotorVehicle.Statics.gboolFromLongTermDataEntry)
            {
                if (Strings.UCase(lblMVR3Done.Text) == done)
                {
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg") == false && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RENTAL"))
                    {
                        answer = MessageBox.Show("Are you trying to print out the No Fee Duplicate Registration for this Rental Vehicle?", "Print Duplicate?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (answer != DialogResult.No)
                        {
                            MotorVehicle.Statics.blnPrintingDupReg = true;
                            MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeDupReg", true);
                            PrintNoFeeDupReg();
                            blnLabelClicked = false;

                            return;
                        }
                    }

                    temp = Interaction.InputBox("Please input the New MVR3 Number for the Reprint." + NEWLINE + NEWLINE + "The last number used:  " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"), "Next MVR3", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") + 1));
                    if (temp.Length > 8)
                        return;

                    NewMVR3Number = FCConvert.ToInt32(Math.Round(Conversion.Val(temp)));
                    if (NewMVR3Number != 0)
                    {
                        if (ValidateMVR3(ref NewMVR3Number))
                        {
                            // Grease the destroyed MVR3#
                            if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != 0 && FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")) != NewMVR3Number)
                            {
                                rsTemp.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                                if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                                {
                                    rsTemp.Edit();
                                    if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                                    {
                                        rsTemp.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                                    }
                                    rsTemp.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                                    rsTemp.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                                    rsTemp.Set_Fields("Status", "V");
                                    rsTemp.Update();
                                    MotorVehicle.Statics.OldMVR3Number = FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                                }
                            }
                            // ************************
                            MotorVehicle.Statics.rsFinal.Set_Fields("MVR3", NewMVR3Number);
                        }
                        else
                        {
                            MessageBox.Show("There is no MVR3 with that number in inventory.", "No MVR3", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            blnLabelClicked = false;
                            return;
                            // no such number in inventory
                        }
                    }
                    else
                    {
                        // bad input in text box
                        blnLabelClicked = false;
                        return;
                    }
                }

                temp = MotorVehicle.Statics.MVR3PrinterName;

                if (isTxtFile(temp))
                {
                    blnExport = true;

                    rptMVR3Laser.InstancePtr.IsPreReg = IsPreReg;
                    rptMVR3Laser.InstancePtr.Run(false);
                }
                else
                {
                    rptMVR3Laser.InstancePtr.IsPreReg = IsPreReg;
                    rptMVR3Laser.InstancePtr.PrintReportOnDotMatrix("MVR3PrinterName");
                }

                rptMVR3Laser.InstancePtr.Unload();

                if (MotorVehicle.Statics.PrintMVR3)
                {
                    if (Information.Err().Number == 0)
                    {
                        lblMVR3Done.Text = "DONE";
                        lblPrintQueue.Enabled = false;
                        cmdReturn.Enabled = false;
                        lblHold.Enabled = false;
                        if (isTitleNotNeeded() && Strings.UCase(lblUseTaxDone.Text) != needed)
                        {
                            lblSave.Enabled = true;
                            lblBatchRegistration.Enabled = false;
                        }
                    }
                }
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg") == false && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("RENTAL"))
                {
                    answer = MessageBox.Show("Would you like to print a No Fee Duplicate Registration for this Rental Vehicle?", "Print Duplicate?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (answer != DialogResult.No)
                    {
                        MotorVehicle.Statics.blnPrintingDupReg = true;
                        MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeDupReg", true);
                        PrintNoFeeDupReg();
                    }
                }
                blnLabelClicked = false;
            }
            else
            {
                temp = MotorVehicle.Statics.MVR10PrinterName;

                if (isTxtFile(temp))
                {
                    blnExport = true;
                    if (!MotorVehicle.Statics.blnLongTermLaserForms)
                    {
                        rptMVRT10.InstancePtr.Run(false);
                    }
                    else
                    {
                        rptMVRT10Laser.InstancePtr.Run(false);
                    }
                }
                else
                {
                    if (!MotorVehicle.Statics.blnLongTermLaserForms)
                    {
                        if (modPrinterFunctions.PrintXsForAlignment("The X should have printed (with the bottoms as close to the top as possible) next to the line titled:" + NEWLINE + "STATE OF MAINE", 23, 1, temp) == DialogResult.Cancel)
                        {
                            MotorVehicle.Statics.PrintMVR3 = false;
                        }
                        else
                        {
                            blnExport = false;
                            rptMVRT10.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                        }
                    }
                    else
                    {
                        blnExport = false;
                        rptMVRT10Laser.InstancePtr.PrintReportOnDotMatrix("MVR10PrinterName");
                    }
                }
                if (!MotorVehicle.Statics.blnLongTermLaserForms)
                {
                    rptMVRT10.InstancePtr.Unload();
                }
                else
                {
                    rptMVRT10Laser.InstancePtr.Unload();
                }

                if (MotorVehicle.Statics.PrintMVR3)
                {
                    if (Information.Err().Number == 0)
                    {
                        lblMVR3Done.Text = "DONE";
                        lblPrintQueue.Enabled = false;
                        cmdReturn.Enabled = false;
                        lblHold.Enabled = false;
                        if (isTitleNotNeeded() && Strings.UCase(lblUseTaxDone.Text) != needed)
                        {
                            lblSave.Enabled = true;
                            lblBatchRegistration.Enabled = false;
                        }
                    }
                }
                blnLabelClicked = false;
            }
        }

        public void lblMVR3_Click()
        {
            lblMVR3_Click(lblMVR3, new EventArgs());
        }

        private void lblMVR3_MouseMove(object sender, MouseEventArgs e)
        {
            HighlightField(ref lblMVR3);
        }

        private void lblPrintQueue_Click(object sender, EventArgs e)
        {
            clsDRWrapper rsAM = new clsDRWrapper();
            int fnx;
            int lngTemp;
            string strValues = "";
            string strFields = "";
            double TitleFee = 0;
            int intLockCount = 0;
            DialogResult intChoice = 0;
            int intRndCount = 0;
            int x;
            string strErrorTag = "";
            clsDRWrapper ArchiveRecord = new clsDRWrapper();
            clsDRWrapper rsTestFleetCTA = new clsDRWrapper();
            string strVin = "";
            clsDRWrapper rsUpdate = new clsDRWrapper();
            clsDRWrapper CheckRS = new clsDRWrapper();
            clsDRWrapper rsHeld = new clsDRWrapper();
            string NP = "";
            string tempString = "";
            string strStart = "";
            try
            {
                // On Error GoTo ShowLineNumber
                Information.Err().Clear();
                rsAM.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = 0");
                rsAM.AddNew();
                if (MotorVehicle.Statics.RegistrationType == "DUPREG")
                {
                    rsAM.Set_Fields("RegistrationType", "DPR");
                    rsAM.Set_Fields("RegistrationFee", 0);
                    rsAM.Set_Fields("RegistrationCredit", 0);
                    rsAM.Set_Fields("TitleFee", 0);
                    rsAM.Set_Fields("RushTitleFee", 0);
                    rsAM.Set_Fields("SalesTax", 0);
                    rsAM.Set_Fields("StateFee", 0);
                    rsAM.Set_Fields("AgentFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee"));
                    rsAM.Set_Fields("AgentFeeTitle", 0);
                    rsAM.Set_Fields("TransferFee", 0);
                }
                else if (MotorVehicle.Statics.RegistrationType == "LOST")
                {
                    rsAM.Set_Fields("RegistrationType", "LPS");
                    rsAM.Set_Fields("RegistrationFee", 0);
                    rsAM.Set_Fields("RegistrationCredit", 0);
                    rsAM.Set_Fields("TitleFee", 0);
                    rsAM.Set_Fields("RushTitleFee", 0);
                    rsAM.Set_Fields("SalesTax", 0);
                    rsAM.Set_Fields("StateFee", 0);
                    rsAM.Set_Fields("AgentFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee"));
                    rsAM.Set_Fields("AgentFeeTitle", 0);
                    rsAM.Set_Fields("TransferFee", 0);
                }
                else if (MotorVehicle.Statics.RegistrationType == "CORR")
                {
                    rsAM.Set_Fields("RegistrationType", "ECO");
                    rsAM.Set_Fields("RegistrationFee", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge")));
                    rsAM.Set_Fields("RegistrationCredit", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("TransferCreditUsed")));
                    rsAM.Set_Fields("TitleFee", MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee") - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TitleFee")));
                    rsAM.Set_Fields("RushTitleFee", MotorVehicle.Statics.rsFinal.Get_Fields("RushTitleFee") - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("RushTitleFee")));
                    rsAM.Set_Fields("SalesTax", MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax") - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("SalesTax")));
                    rsAM.Set_Fields("StateFee", MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid") - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("StatePaid")));
                    rsAM.Set_Fields("AgentFee", MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee") - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("AgentFee")));
                    rsAM.Set_Fields("AgentFeeTitle", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCTA")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("AgentFeeCTA")));
                    rsAM.Set_Fields("TransferFee", MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee") - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransferFee")));
                    rsAM.Set_Fields("InfoMessage", MotorVehicle.Statics.rsFinal.Get_Fields_String("InfoMessage"));
                }
                else
                {
                    rsAM.Set_Fields("RegistrationType", MotorVehicle.Statics.RegistrationType);
                    rsAM.Set_Fields("RegistrationFee", frmDataInputLongTermTrailers.InstancePtr.txtFEERegFee.Text);
                    rsAM.Set_Fields("RegistrationCredit", frmDataInputLongTermTrailers.InstancePtr.txtFEECreditTransfer.Text);
                    rsAM.Set_Fields("TitleFee", frmDataInputLongTermTrailers.InstancePtr.txtFEETitleFee.Text);
                    rsAM.Set_Fields("RushTitleFee", frmDataInputLongTermTrailers.InstancePtr.txtFEERushTitleFee.Text);
                    rsAM.Set_Fields("SalesTax", frmDataInputLongTermTrailers.InstancePtr.txtFEESalesTax.Text);
                    rsAM.Set_Fields("StateFee", frmDataInputLongTermTrailers.InstancePtr.txtFees.Text);
                    rsAM.Set_Fields("AgentFee", frmDataInputLongTermTrailers.InstancePtr.txtFEEAgentFeeReg.Text);
                    rsAM.Set_Fields("AgentFeeTitle", frmDataInputLongTermTrailers.InstancePtr.txtFeeAgentFeeCTA.Text);
                    rsAM.Set_Fields("TransferFee", frmDataInputLongTermTrailers.InstancePtr.txtFEETransferFee.Text);
                }
                rsAM.Set_Fields("UniqueIdentifier", 0);
                rsAM.Set_Fields("Extension", MotorVehicle.Statics.gboolLongRegExtension || MotorVehicle.Statics.gboolLongRegExtensionSamePlate);
                rsAM.Set_Fields("TitleDone", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone"));
                rsAM.Set_Fields("UseTaxDone", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone"));
                if (FCConvert.ToBoolean(rsAM.Get_Fields_Boolean("UseTaxDone")))
                {
                    rsAM.Set_Fields("UseTaxID", MotorVehicle.Statics.lngUTCAddNewID);
                }
                rsAM.Set_Fields("DateUpdated", DateTime.Today);
                rsAM.Set_Fields("TimeUpdated", DateAndTime.TimeOfDay);
                rsAM.Set_Fields("Make", MotorVehicle.Statics.rsFinal.Get_Fields_String("Make"));
                rsAM.Set_Fields("Unit", MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
                rsAM.Set_Fields("Year", MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
                rsAM.Set_Fields("Style", MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
                rsAM.Set_Fields("Color", MotorVehicle.Statics.rsFinal.Get_Fields_String("Color1") + MotorVehicle.Statics.rsFinal.Get_Fields_String("Color2"));
                if (MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST")
                {
                    rsAM.Set_Fields("MaineReregistration", false);
                    rsAM.Set_Fields("TimePayment", false);
                }
                else
                {
                    rsAM.Set_Fields("MaineReregistration", (frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text == "YES - Expires this year" || frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text == "YES - Expires next year"));
                    rsAM.Set_Fields("TimePayment", frmDataInputLongTermTrailers.InstancePtr.chkTimePayment.CheckState == CheckState.Checked);
                }
                rsAM.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                rsAM.Set_Fields("VIN", MotorVehicle.Statics.rsFinal.Get_Fields_String("VIN"));
                rsAM.Set_Fields("TitleNumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber").ToUpper());
                rsAM.Set_Fields("NetWeight", FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight"))));
                rsAM.Set_Fields("OwnerName", MotorVehicle.Statics.rsFinal.Get_Fields("Owner1"));
                rsAM.Set_Fields("Address1", MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
                rsAM.Set_Fields("City", MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
                rsAM.Set_Fields("State", MotorVehicle.Statics.rsFinal.Get_Fields("State"));
                rsAM.Set_Fields("Zip", MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
                rsAM.Set_Fields("LegalResAddress", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResAddress"));
                rsAM.Set_Fields("LegalResCity", MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
                rsAM.Set_Fields("LegalResState", MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
                rsAM.Set_Fields("LegalResZip", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResZip"));
                rsAM.Set_Fields("ExpireDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));
                if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("EffectiveDate")))
                {
                    rsAM.Set_Fields("EffectiveDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"));
                }
                rsAM.Set_Fields("FleetNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
                rsAM.Set_Fields("OldPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate"));
                if (MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.gboolLongRegExtension)
                {
                    rsUpdate.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + FCConvert.ToString(Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("LongTermTrailerRegKey"))));
                    if (rsUpdate.EndOfFile() != true && rsUpdate.BeginningOfFile() != true)
                    {
                        if (MotorVehicle.Statics.gboolLongRegExtension)
                        {
                            rsAM.Set_Fields("NumberOfYears", rsUpdate.Get_Fields_Int32("NumberOfYears") + frmDataInputLongTermTrailers.InstancePtr.cboLength.SelectedIndex + frmDataInputLongTermTrailers.InstancePtr.cboLength.ItemData(frmDataInputLongTermTrailers.InstancePtr.cboLength.ListIndex));
                        }
                        else
                        {
                            rsAM.Set_Fields("NumberOfYears", rsUpdate.Get_Fields_Int32("NumberOfYears"));
                        }
                    }
                    else
                    {
                    CheckAgain3:
                        
                        object temp = strStart;
                        frmInput.InstancePtr.Init(ref temp, "Enter Start Year", "Please enter the start year to be printed on the registration.");
                        strStart = temp.ToString();
                        if (!Information.IsNumeric(strStart) || Conversion.Val(strStart) <= 0)
                        {
                            MessageBox.Show("You must enter a numeric value for the start year.", "Invalid Start Year");
                            goto CheckAgain3;
                        }

                        rsAM.Set_Fields("NumberOfYears", FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Year) - Conversion.Val(strStart));
                        rsAM.Set_Fields("NumberOfYears", 0);
                    }
                }
                else
                {
                    rsAM.Set_Fields("NumberOfYears", frmDataInputLongTermTrailers.InstancePtr.cboLength.ItemData(frmDataInputLongTermTrailers.InstancePtr.cboLength.SelectedIndex));
                }
                rsAM.Set_Fields("Status", "A");
                MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegKey", rsAM.Get_Fields_Int32("ID"));
                MotorVehicle.Statics.rsFinal.Set_Fields("TimeUpdated", rsAM.Get_Fields_DateTime("TimeUpdated"));
                MotorVehicle.Statics.rsFinal.Set_Fields("DateUpdated", rsAM.Get_Fields_DateTime("DateUpdated"));
                if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRR")
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("ArchiveKey", MotorVehicle.Statics.rsFinalCompare.Get_Fields("Key"));
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("ArchiveKey", 0);
                }

                rsAM.Update();
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone"))
                {
                    rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));
                    if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
                    {
                        rsTitle.MoveLast();
                        rsTitle.MoveFirst();
                        rsUpdate.OpenRecordset("SELECT * FROM LongTermTitleApplications WHERE ID = 0");
                        if (MotorVehicle.Statics.FleetCTA)
                        {
                            if (MotorVehicle.Statics.VehiclesCovered > 1)
                            {
                                rsTestFleetCTA.OpenRecordset("SELECT * FROM LongTermTitleApplications WHERE CTANumber = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber") + "'");
                                if (rsTestFleetCTA.EndOfFile() != true && rsTestFleetCTA.BeginningOfFile() != true)
                                {
                                    goto SkipCTA;
                                }
                            }
                            TitleFee = MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee") * MotorVehicle.Statics.VehiclesCovered;
                        }
                        else
                        {
                            TitleFee = MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee");
                        }
                        rsUpdate.AddNew();
                        rsUpdate.Set_Fields("CTANumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber").ToUpper());
                        rsUpdate.Set_Fields("Fee", TitleFee);
                        rsUpdate.Set_Fields("RushFee", MotorVehicle.Statics.rsFinal.Get_Fields("RushTitleFee"));
                        rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
                        rsUpdate.Set_Fields("Plate", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                        rsUpdate.Set_Fields("RegistrationDate", DateTime.Today);
                        rsUpdate.Set_Fields("CustomerName", rsTitle.Get_Fields_String("Name1"));
                        rsUpdate.Set_Fields("OPID", MotorVehicle.Statics.UserID);
                        rsUpdate.Set_Fields("DoubleIfApplicable", rsTitle.Get_Fields_String("DoubleNumber"));
                        rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                        rsUpdate.Set_Fields("MSRP", rsTitle.Get_Fields_String("MSRP"));
                        rsUpdate.Update();
                    }
                SkipCTA:
                    ;
                    if (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
                    {
                        MotorVehicle.Statics.rsDoubleTitle.Update();
                    }
                }
                if (FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"))) > 2000 && FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"))) < 3000)
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("Plate", MotorVehicle.GetLongTermPlate(FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate")))));
                }
                if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) != "NEW" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) != "DPR")
                {
                    if (MotorVehicle.Statics.strCodeP == "") MakeCodeP();
                    rsUpdate.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
                    rsUpdate.AddNew();
                    rsUpdate.Set_Fields("AdjustmentCode", "I");
                    rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                    rsUpdate.Set_Fields("QuantityAdjusted", 1);
                    rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeP);
                    rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"));
                    rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "- MVR10 Form");
                    rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                    rsUpdate.Set_Fields("TellerCloseoutID", 0);
                    rsUpdate.Update();
                }
                if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) != "NEW" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) != "DPR")
                {
                    // And rsFinal.fields("plate") <> rsFinalCompare.fields("Plate
                    MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND FormattedInventory & '' = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
                    rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);
                    if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                    {
                        rsInventory.Edit();
                        if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                        {
                            rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                        }
                        rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                        // .Fields("MVR3") =
                        rsInventory.Set_Fields("Status", "I");
                        rsInventory.Set_Fields("PeriodCloseoutID", 0);
                        rsInventory.Set_Fields("TellerCloseoutID", 0);
                        rsInventory.Update();
                    }
                }
                CheckRS.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE MasterID = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));
                if (CheckRS.EndOfFile() != true && CheckRS.BeginningOfFile() != true)
                {
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate")) != "")
                    {
                        rsHeld.OpenRecordset("SELECT * FROM Master WHERE Plate = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate") + "'");
                        if (rsHeld.EndOfFile() != true && rsHeld.BeginningOfFile() != true)
                        {
                            rsHeld.Edit();
                        }
                        else
                        {
                            rsHeld.AddNew();
                        }
                    }
                    else
                    {
                        rsHeld.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
                        rsHeld.AddNew();
                    }
                    for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
                    {
                        if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) != "ID")
                        {
                            rsHeld.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
                        }
                    }
                    MotorVehicle.TransferRecordToPending(ref rsHeld);
                    rsHeld.Update();
                    ArchiveRecord.Execute("DELETE FROM HeldRegistrationMaster WHERE MasterID = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"), "TWMV0000.vb1");
                }
                else
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("Inactive", false);
                    clsDRWrapper temp = MotorVehicle.Statics.rsFinal;
                    MotorVehicle.TransferRecordToPending(ref temp);
                    MotorVehicle.Statics.rsFinal = temp;
                    MotorVehicle.Statics.rsFinal.Update();
                }
                MotorVehicle.Statics.rsFinalCompare.Reset();
                MotorVehicle.Statics.MVR3saved = Information.Err().Number == 0;
                MotorVehicle.ClearAllVariables();
                frmWait.InstancePtr.Unload();
                frmDataInput.InstancePtr.Unload();
                frmDataInputLongTermTrailers.InstancePtr.Unload();
                frmPreview.InstancePtr.Unload();
                if (!MotorVehicle.Statics.bolFromDosTrio && (!MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet))
                {
                    frmPlateInfo.InstancePtr.ShowForm();
                }
                else if (MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet)
                {
                    MDIParent.InstancePtr.Menu18();
                    Close();
                    App.MainForm.EndWaitMVModule();
                    return;
                }
                blnLabelClicked = false;
                Close();
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                MessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + NEWLINE + Information.Err(ex).Description + NEWLINE + NEWLINE + "This Registration Will NOT Be Saved!!" + NEWLINE + NEWLINE + "An error occurred on line " + Information.Erl(), "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);

                switch (Information.Err(ex).Number)
                {
                    case 3260:
                        {
                            intLockCount += 1;
                            if (intLockCount > 5)
                            {
                                intChoice = MessageBox.Show("Error Trying to Save.  This record is currently locked by another user.  Would you like to try to save again?", "Record Locked", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question);
                                if (intChoice == DialogResult.Retry)
                                {
                                    intLockCount = 1;
                                }
                            }
                            //App.DoEvents();
                            intRndCount = (FCConvert.ToInt32(Math.Pow(intLockCount, 2)) * 4000);
                            for (x = 1; x <= intRndCount; x++)
                            {
                            }
                            // x
                            /*? Resume; */
                            break;
                        }
                    default:
                        {
                            MessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + NEWLINE + Information.Err(ex).Description + NEWLINE + NEWLINE + "This Registration Will NOT Be Saved!!" + NEWLINE + NEWLINE + "A file called SaveErr.txt has been created in the " + Environment.CurrentDirectory + " directory of your server.  Please notify TRIO of your problem and E-mail the SaveErr.txt file so we may more quickly find the problem you are encountering.", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            /*? Resume FailedUpdate; */
                            break;
                        }
                }
            //end switch
            MotorVehicle.ClearAllVariables();
                frmDataInput.InstancePtr.Unload();
                frmPreview.InstancePtr.Unload();
                InstancePtr.Close();
                Close();
                MotorVehicle.Statics.rsFinal = null;
                MotorVehicle.Statics.rsFinalCompare = null;
                MotorVehicle.Statics.rsDoubleTitle = null;
                if (!MotorVehicle.Statics.bolFromDosTrio && !MotorVehicle.Statics.bolFromWindowsCR) frmPlateInfo.InstancePtr.ShowForm();
            }
        }

        private void lblPrintQueue_MouseMove(object sender, MouseEventArgs e)
        {
            HighlightField(ref lblPrintQueue);
        }

        private void lblSave_Click(object sender, EventArgs e)
        {
            //DAO.Field ff = new DAO.Field();
            clsDRWrapper rsAM = new clsDRWrapper();
            int fnx;
            clsDRWrapper rsTemp = new clsDRWrapper();
            string strValues = "";
            string strFields = "";
            double TitleFee = 0;
            int intLockCount = 0;
            DialogResult intChoice = 0;
            int intRndCount = 0;
            int x;
            int LastMVR3 = 0;
            clsDRWrapper rsMVR3 = new clsDRWrapper();
            string strErrorTag;
            clsDRWrapper rsID = new clsDRWrapper();
            clsDRWrapper ArchiveRecord = new clsDRWrapper();
            clsDRWrapper rsTestFleetCTA = new clsDRWrapper();
            clsDRWrapper rsMVRecord = new clsDRWrapper();
            string strVin = "";
            clsDRWrapper rsUpdate = new clsDRWrapper();
            clsDRWrapper CheckRS = new clsDRWrapper();
            clsDRWrapper rsHeld = new clsDRWrapper();
            string NP = "";
            string tempString = "";
            clsDRWrapper rsFees = new clsDRWrapper();
            try
            {
                Information.Err().Clear();
                var plate = MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate");

                if (MotorVehicle.Statics.gboolFromLongTermDataEntry)
                {
                    SaveLongTermRegistration(rsAM, plate, rsFees, rsUpdate, rsTestFleetCTA, TitleFee, CheckRS, rsHeld, ArchiveRecord);

                    return;
                }
                frmWait.InstancePtr.Show();
                frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + NEWLINE + "Saving Data";
                frmWait.InstancePtr.Refresh();

                Debug.WriteLine("Start Save" + "\t" + FCConvert.ToString(DateTime.Now));
                blnLabelClicked = true;
                ResetUpdateVariables(false);
                InstancePtr.Hide();

                MotorVehicle.Statics.rsFinal.Set_Fields("DateUpdated", DateTime.Now);
                
                SetExciseTaxOrCredit();

                // kk07252016  - Save the print priorities/reason codes and User entered text so proper duplicate can be created
                if (MotorVehicle.Statics.RegistrationType != "DUPREG")
                {
                    var sb = new StringBuilder();
                    for (x = 1; x <= (MotorVehicle.Statics.gPPriority.UBound()); x++)
                    {
                        // Save the priority number and text so the dup print priority can be added in the correct spot
                        if (x > 1) sb.Append("|");
                       
                        sb.Append(MotorVehicle.Statics.gPPriNumber[x] + "," + MotorVehicle.Statics.gPPriority[x]);
                    }

                    tempString = sb.ToString();
                    if (Strings.Trim(tempString) != "")
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("PrintPriorities", tempString);
                    }
                    if (Strings.Trim(MotorVehicle.Statics.RRTextAll) != "")
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("ReasonCodes", Strings.Trim(MotorVehicle.Statics.RRTextAll));
                    }
                }
                // 
                strErrorTag = "ActivityMaster";
                SetupActivityMaster(rsAM);

                if (MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.RegistrationType == "GVW")
                {
                    SetupActivityMaster_CORR_GVW(rsUpdate);
                }
                else
                {
                    CreateActivityMaster(rsUpdate, rsFees);
                }

                Debug.WriteLine("Saved ActivityMaster" + "\t" + FCConvert.ToString(DateTime.Now));

            DoneFillingTag:
                Information.Err().Clear();
                // DJW@11182009 We should never save the duplicate registration fee into the master record only the activitymaster
                if (MotorVehicle.Statics.RegistrationType == "DUPREG")
                {
                    MotorVehicle.Statics.rsFinal.Set_Fields("DuplicateRegistrationFee", 0);
                }
                string tempOldMVR3 = "";

                if ((MotorVehicle.Statics.RegistrationType == "RRR" || MotorVehicle.Statics.RegistrationType == "RRT") && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("OldMVR3")) == 0)
                {
                    tempOldMVR3 = Interaction.InputBox("Please enter the old MVR3 number", "Old MVR3 Number Needed", null);
                    if (tempOldMVR3 == "" || !Information.IsNumeric(tempOldMVR3))
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3", 0);
                    }
                    else
                    {
                        MotorVehicle.Statics.rsFinal.Set_Fields("OldMVR3", tempOldMVR3);
                    }
                }

                HoldTransactionId(rsID);

                if (MotorVehicle.Statics.bolFromDosTrio || MotorVehicle.Statics.bolFromWindowsCR)
                {
                    MotorVehicle.Write_PDS_Work_Record_Stuff();
                }
                LastMVR3 = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3");

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone"))
                {
                    SaveTitleInfo(rsUpdate, rsTestFleetCTA, plate);
                }
                Debug.WriteLine("Saved Title" + "\t" + FCConvert.ToString(DateTime.Now));
           
                if (Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber")) != "" && Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber"))) != Strings.Trim(MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("GiftCertificateNumber")))
                {
                    SaveGiftCertificateInfo(rsUpdate, plate);
                }
                Debug.WriteLine("Saved Gift Cert" + "\t" + FCConvert.ToString(DateTime.Now));

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("ETO"))
                {
                    SaveETOInformation(rsUpdate);
                }
                Debug.WriteLine("Saved Title" + "\t" + FCConvert.ToString(DateTime.Now));

                Do_InventoryAdjustments_LongTerm(rsUpdate, rsMVR3, plate);

                Debug.WriteLine("Saved Inv Adj" + "\t" + FCConvert.ToString(DateTime.Now));
                // ****   INVENTORY ADJUSTMENT ENDS HERE ****

                // 
                if (MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "N" && MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate") == "T")
                {
                    SaveTemporaryPlateInfo(rsUpdate, plate);
                }
                Debug.WriteLine("Saved Temp Plate" + "\t" + FCConvert.ToString(DateTime.Now));

                Do_UpdateInventory();

                Debug.WriteLine("Saved Inventory" + "\t" + FCConvert.ToString(DateTime.Now));
                
                // 
                // master file
                // 
                CheckRS.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE MasterID = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));
                if (CheckRS.EndOfFile() != true && CheckRS.BeginningOfFile() != true)
                {
                    // MsgBox "Start Held Registration Save"
                    //App.DoEvents();
                    // held registration only
                    strErrorTag = "HeldRegistrationMaster";
                    //App.DoEvents();
                    // CheckRS.Close
                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate")) != "")
                    {
                        rsHeld.OpenRecordset("SELECT * FROM Master WHERE Plate = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate") + "'");
                        if (rsHeld.EndOfFile() != true && rsHeld.BeginningOfFile() != true)
                        {
                            rsHeld.Edit();
                            /*? 591 */
                        }
                        else
                        {
                            rsHeld.AddNew();
                            /*? 593 */
                        }
                        /*? 594 */
                    }
                    else
                    {
                        rsHeld.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
                        rsHeld.AddNew();
                        /*? 597 */
                    }
                    for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
                    {
                        if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) != "ID")
                        {
                            rsHeld.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
                            /*? 601 */
                        }
                        /*? 602 */
                    }
                    rsHeld.Update();
                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID));
                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Update();
                        /*? 615 */
                    }
                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID2));
                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", ArchiveRecord.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Update();
                    }
                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID3));
                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", ArchiveRecord.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                        ArchiveRecord.Update();
                    }
                    ArchiveRecord = null;
                    //FC:FINAL:SBE - #i2132 - variable declared with As New
                    ArchiveRecord = new clsDRWrapper();
                    ArchiveRecord.Execute("DELETE FROM HeldRegistrationMaster WHERE MasterID = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"), "TWMV0000.vb1");
                    // 
                    /*? 618 */
                }
                else
                {
                    // MsgBox "Start Archive Master Save"
                    //App.DoEvents();
                    strErrorTag = "ArchiveMaster";
                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID));
                    // MsgBox "Get Archive Record"
                    //App.DoEvents();
                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", FCConvert.ToString(ArchiveRecord.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Set_Fields("MVR3", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Update();
                        //App.DoEvents();
                    }
                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID2));
                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", FCConvert.ToString(ArchiveRecord.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Set_Fields("MVR3", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Update();
                        //App.DoEvents();
                    }
                    ArchiveRecord.OpenRecordset("SELECT * FROM ArchiveMaster WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.FinalCompareID3));
                    if (ArchiveRecord.EndOfFile() != true && ArchiveRecord.BeginningOfFile() != true)
                    {
                        ArchiveRecord.Edit();
                        ArchiveRecord.Set_Fields("OldMVR3", FCConvert.ToString(ArchiveRecord.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Set_Fields("MVR3", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3")));
                        ArchiveRecord.Update();
                        //App.DoEvents();
                    }
                    ArchiveRecord.Reset();
                    // If RegistrationType = "DUPREG" Then
                    // Sleep (3000)
                    // End If
                    if (MotorVehicle.Statics.RegisteringFleet)
                    {
                        // MsgBox "Save Fleet Information"
                        //App.DoEvents();
                        if (MotorVehicle.Statics.RegistrationType == "NRR")
                        {
                            MotorVehicle.Statics.VehicleInformation[0, MotorVehicle.Statics.NumberOfVehicles] = "Registered";
                            MotorVehicle.Statics.VehicleInformation[1, MotorVehicle.Statics.NumberOfVehicles] = true;
                            MotorVehicle.Statics.VehicleInformation[2, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class");
                            MotorVehicle.Statics.VehicleInformation[3, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("plate");
                            MotorVehicle.Statics.VehicleInformation[4, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin");
                            MotorVehicle.Statics.VehicleInformation[5, MotorVehicle.Statics.NumberOfVehicles] = FCConvert.ToInt32(Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("Year")));
                            MotorVehicle.Statics.VehicleInformation[6, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
                            MotorVehicle.Statics.VehicleInformation[7, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");
                            if (MotorVehicle.Statics.GroupOrFleet == "G")
                            {
                                MotorVehicle.Statics.VehicleInformation[8, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate");
                                /*? 645 */
                            }
                            else
                            {
                                if (Strings.Trim(MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber")) != "")
                                {
                                    MotorVehicle.Statics.VehicleInformation[8, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber");
                                    /*? 648 */
                                }
                                /*? 649 */
                            }
                            MotorVehicle.Statics.VehicleInformation[9, MotorVehicle.Statics.NumberOfVehicles] = MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid") + MotorVehicle.Statics.rsFinal.Get_Fields("LocalPaid");
                            /*? 651 */
                        }
                        else
                        {
                            MotorVehicle.Statics.VehicleInformation[0, MotorVehicle.Statics.VehicleBeingRegistered] = "Registered";
                            MotorVehicle.Statics.VehicleInformation[9, MotorVehicle.Statics.VehicleBeingRegistered] = MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid") + MotorVehicle.Statics.rsFinal.Get_Fields("LocalPaid");
                            /*? 654 */
                        }
                        MotorVehicle.Statics.VehiclesRegistered[MotorVehicle.Statics.VehicleBeingRegistered] = true;
                        /*? 656 */
                    }
                    strErrorTag = "Master";
                    strVin = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin"));
                    // MsgBox "Start Update Master Record"
                    //App.DoEvents();
                    MotorVehicle.Statics.rsFinal.Set_Fields("Inactive", false);
                    MotorVehicle.Statics.rsFinal.Update();
                    // MsgBox "End Update Master Record"
                    //App.DoEvents();
                }
                Debug.WriteLine("Saved Master" + "\t" + FCConvert.ToString(DateTime.Now));
                modRegistry.SaveRegistryKey("LastMVR3Used", FCConvert.ToString(LastMVR3));
                // 
                strErrorTag = "Terminate Master Record";
                if (MotorVehicle.Statics.RecordIDToTerminate != 0)
                {
                    // MsgBox "Terminate Master Record"
                    //App.DoEvents();
                    if (MotorVehicle.Statics.RegistrationType == "RRT" && (MotorVehicle.Statics.PlateType == 2 || MotorVehicle.Statics.PlateType == 3 || MotorVehicle.Statics.PlateType == 4))
                    {
                        rsTemp.Execute("DELETE FROM Master WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.RecordIDToTerminate), "TWMV0000.vb1");
                        /*? 666 */
                    }
                    else
                    {
                        rsTemp.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.RecordIDToTerminate));
                        if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                        {
                            rsTemp.Edit();
                            rsTemp.Set_Fields("Status", "T");
                            rsTemp.Update();
                            rsTemp.Reset();
                            /*? 674 */
                        }
                        /*? 675 */
                    }
                    /*? 676 */
                }
                if ((MotorVehicle.Statics.RegistrationType == "NRR" || MotorVehicle.Statics.RegistrationType == "NRT") && MotorVehicle.Statics.blnMakeVehicleInactive)
                {
                    // MsgBox "Set Inactive Flag"
                    //App.DoEvents();
                    rsMVRecord.OpenRecordset("SELECT * FROM Master WHERE VIN = '" + strVin + "'");
                    if (rsMVRecord.EndOfFile() != true && rsMVRecord.BeginningOfFile() != true)
                    {
                        rsMVRecord.Edit();
                        rsMVRecord.Set_Fields("Inactive", true);
                        rsMVRecord.Update();
                        /*? 683 */
                    }
                    /*? 684 */
                }
                MotorVehicle.Statics.blnMakeVehicleInactive = FCConvert.ToBoolean(0);
                // MsgBox "Reset Variables"
                //App.DoEvents();
                Debug.WriteLine("Start Reset Variables" + "\t" + FCConvert.ToString(DateTime.Now));
                MotorVehicle.Statics.rsDoubleTitle.Reset();
                MotorVehicle.Statics.rsFinalCompare.Reset();
                if (Information.Err().Number == 0)
                {
                    MotorVehicle.Statics.MVR3saved = true;
                    /*? 689 */
                }
                else
                {
                    MotorVehicle.Statics.MVR3saved = false;
                    /*? 691 */
                }
                MotorVehicle.ClearAllVariables();
                frmWait.InstancePtr.Unload();
                frmDataInput.InstancePtr.Unload();
                frmPreview.InstancePtr.Unload();
                if (!MotorVehicle.Statics.bolFromDosTrio && (!MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet))
                {
                    //frmPlateInfo.InstancePtr.Show(App.MainForm);
                    frmPlateInfo.InstancePtr.ShowForm();
                    /*? 698 */
                }
                else if (MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet)
                {
                    //App.DoEvents();
                    //MDIParent.InstancePtr.GetMainMenu();
                    MDIParent.InstancePtr.Menu18();
                    Close();
                    /*? 703 */
                    //Application.Exit();
                    App.MainForm.EndWaitMVModule();

                    /*? 704 */
                    return;
                    /*? 705 */
                }
                else if (!MotorVehicle.Statics.RegisteringFleet)
                {
                    //MDIParent.InstancePtr.GRID.Focus();
                    /*? 707 */
                }
                blnLabelClicked = false;
                Debug.WriteLine("End Save" + "\t" + FCConvert.ToString(DateTime.Now));
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                // ShowLineNumber:
                MessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + NEWLINE + Information.Err(ex).Description + NEWLINE + NEWLINE + "This Registration Will NOT Be Saved!!" + NEWLINE + NEWLINE + "An error occurred on line " + Information.Erl(), "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            /*? Resume FailedUpdate; */
            switch (Information.Err(ex).Number)
                {
                    case 3260:
                        {
                            intLockCount += 1;
                            if (intLockCount > 5)
                            {
                                intChoice = MessageBox.Show("Error Trying to Save.  This record is currently locked by another user.  Would you like to try to save again?", "Record Locked", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question);
                                if (intChoice == DialogResult.Retry)
                                {
                                    intLockCount = 1;
                                }
                            }
                            //App.DoEvents();
                            intRndCount = (FCConvert.ToInt32(Math.Pow(intLockCount, 2)) * 4000);
                            for (x = 1; x <= intRndCount; x++)
                            {
                            }
                            // x
                            /*? Resume; */
                            break;
                        }
                    default:
                        {
                            MessageBox.Show("Error: " + FCConvert.ToString(Information.Err(ex).Number) + NEWLINE + Information.Err(ex).Description + NEWLINE + NEWLINE + "This Registration Will NOT Be Saved!!" + NEWLINE + NEWLINE + "A file called SaveErr.txt has been created in the " + Environment.CurrentDirectory + " directory of your server.  Please notify TRIO of your problem and E-mail the SaveErr.txt file so we may more quickly find the problem you are encountering.", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            /*? Resume FailedUpdate; */
                            break;
                        }
                }
            //end switch

                MotorVehicle.ClearAllVariables();
                frmDataInput.InstancePtr.Unload();
                frmPreview.InstancePtr.Unload();
                MotorVehicle.Statics.rsFinal = null;
                MotorVehicle.Statics.rsFinalCompare = null;
                if (!MotorVehicle.Statics.bolFromDosTrio && !MotorVehicle.Statics.bolFromWindowsCR)
                {
                    //frmPlateInfo.InstancePtr.Show(App.MainForm);
                    frmPlateInfo.InstancePtr.ShowForm();
                }
  
            }
        }

        private static void SetupActivityMaster_CORR_GVW(clsDRWrapper rsUpdate)
        {
            int fnx;

            // set all values  in rsam to rsfinal then calculate dollar difference ??
            MotorVehicle.Statics.rsECorrect.OpenRecordset("SELECT * FROM tblMasterTemp WHERE ID = 0");
            MotorVehicle.Statics.rsECorrect.AddNew();

            for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
            {
                if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) != "ID")
                {
                    MotorVehicle.Statics.rsECorrect.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
                }

                /*? 29 */
            }

            clsDRWrapper temp = MotorVehicle.Statics.rsFinal;
            clsDRWrapper temp1 = MotorVehicle.Statics.rsFinalCompare;
            clsDRWrapper temp2 = MotorVehicle.Statics.rsECorrect;
            MotorVehicle.CompareRecordsForECorrect(ref temp, ref temp1, ref temp2);
            MotorVehicle.Statics.rsFinal = temp;
            MotorVehicle.Statics.rsFinalCompare = temp1;
            MotorVehicle.Statics.rsECorrect = temp2;

            if (MotorVehicle.Statics.RegistrationType == "CORR")
            {
                SetCorrectionReason();
            }

            CreateActivityMaster_CORR_GVW(rsUpdate);

            MotorVehicle.Statics.rsECorrect.CancelUpdate();
        }

        private static void SetupActivityMaster(clsDRWrapper rsAM)
        {
            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PreEffectiveDate.ToOADate() > DateTime.Today.ToOADate())
            {
                rsAM.OpenRecordset("SELECT * FROM PendingActivityMaster WHERE ID = 0");
            }
            else
            {
                rsAM.OpenRecordset("SELECT * FROM ActivityMaster WHERE ID = 0");
            }
        }

        private static void CreateActivityMaster_CORR_GVW(clsDRWrapper rsUpdate)
        {
            int fnx;
            rsUpdate.OpenRecordset("SELECT * FROM ActivityMaster WHERE ID = 0");
            rsUpdate.AddNew();

            for (fnx = 1; fnx <= MotorVehicle.Statics.rsECorrect.FieldsCount - 1; fnx++)
            {
                var fieldsIndexName = MotorVehicle.Statics.rsECorrect.Get_FieldsIndexName(fnx);

                switch (fieldsIndexName)
                {
                    case "OldMVR3":
                        rsUpdate.Set_Fields(fieldsIndexName, 0);

                        break;
                    case "TransactionType" when MotorVehicle.Statics.RegistrationType == "GVW":
                        rsUpdate.Set_Fields(fieldsIndexName, "GVW");

                        break;
                    case "TransactionType":
                        rsUpdate.Set_Fields(fieldsIndexName, "ECO");

                        break;
                    case "TellerCloseoutID":
                        rsUpdate.Set_Fields(fieldsIndexName, 0);

                        break;
                    default:
                        rsUpdate.Set_Fields(fieldsIndexName, MotorVehicle.Statics.rsECorrect.Get_FieldsIndexValue(fnx));

                        break;
                }
            }

            rsUpdate.Update();
        }

        private void SaveLongTermRegistration(clsDRWrapper rsAM, string plate, clsDRWrapper rsFees, clsDRWrapper rsUpdate, clsDRWrapper rsTestFleetCTA, double TitleFee, clsDRWrapper CheckRS, clsDRWrapper rsHeld, clsDRWrapper ArchiveRecord)
        {
            CreateNewLongTermRegistration(rsAM, plate, rsFees);

            MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegID", rsAM.Get_Fields_Int32("ID"));
            MotorVehicle.Statics.rsFinal.Set_Fields("InfoMessage", "");

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone"))
            {
                SaveTitleFeeAndCTA(rsUpdate, rsTestFleetCTA, TitleFee, plate);
            }

            SaveInventoryAdjustment_CodeP_StatusI(rsUpdate, plate);

            SaveInventoryAdjustment_StatusI();

            CheckRS.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE MasterID = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"));

            if (CheckRS.EndOfFile() != true && CheckRS.BeginningOfFile() != true)
            {
                UpdateHeldRegistration(rsHeld, ArchiveRecord);
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("Inactive", false);
                MotorVehicle.Statics.rsFinal.Update();
            }

            MotorVehicle.Statics.rsFinalCompare.Reset();
            MotorVehicle.Statics.MVR3saved = Information.Err()
                                                        .Number
                                             == 0;
            MotorVehicle.ClearAllVariables();
            frmWait.InstancePtr.Unload();
            frmDataInput.InstancePtr.Unload();
            frmDataInputLongTermTrailers.InstancePtr.Unload();
            frmPreview.InstancePtr.Unload();

            if (!MotorVehicle.Statics.bolFromDosTrio && (!MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet))
            {
                frmPlateInfo.InstancePtr.ShowForm();
            }
            else
                if (MotorVehicle.Statics.bolFromWindowsCR && !MotorVehicle.Statics.RegisteringFleet)
                {
                    MDIParent.InstancePtr.Menu18();
                    Close();
                    App.MainForm.EndWaitMVModule();

                    return;
                }

            blnLabelClicked = false;
            Close();

            return;
        }

        private void Do_InventoryAdjustments_LongTerm(clsDRWrapper rsUpdate, clsDRWrapper rsMVR3, string plate)
        {
            SaveInventoryAdjustments(rsUpdate);

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") != 0)
            {
                SaveLastMVRNumber(rsMVR3);

                SaveMVR3Info(rsUpdate);

                SaveRentalInfo(rsUpdate);
            }

            if ((MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber")) || (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "IU" && Information.IsNumeric(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("plate")))
            {
                SaveCodeYAdjustment(rsUpdate, plate);
            }

            if ((MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "N" || MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "R") && MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate") == "N" && MotorVehicle.Statics.RegistrationType != "DUPREG" && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != "NEW" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "IU")
            {
                SaveCodePAdjustment(rsUpdate, plate);
            }
        }

        private void Do_UpdateInventory()
        {
            string strErrorTag;

            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                UpdateInventory_CodeM_StatusP();
            }
            else
            {
                UpdateInventory_CodeM_StatusI();
            }

            // 

            strErrorTag = "MVR3 Inventory";

            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                UpdateInventory_MVR3_StatusP();
            }
            else
            {
                UpdateInventory_MVR3_StatusI();
            }

            strErrorTag = "Rental MVR3 Inventory";

            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                UpdateInventory_RentalMVR3_StatusP();
            }
            else
            {
                UpdateInventory_RentalMVR3_StatusI();
            }

            // 

            strErrorTag = "Year Inventory";

            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                SaveYearInventory_FleetPending();
            }
            else
            {
                SaveYearInventory_NonFleetPending();
            }

            // 

            strErrorTag = "Plate Inventory";

            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                SavePlateInventory_FleetPending();
            }
            else
            {
                SavePlateInventory_NonFleetPending();
            }

            // 

            strErrorTag = "Temporary Plate Inventory";

            if (MotorVehicle.Statics.ForcedPlate == "T")
            {
                SaveTemporaryPlateInventory();
            }
        }

        private void SaveTemporaryPlateInventory()
        {
            rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE SUBSTRING(Code,4,5) = 'TM' AND Number = " + FCConvert.ToString(MotorVehicle.Statics.TempPlateNumber));

            if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
            {
                rsInventory.Edit();

                if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                {
                    rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                }

                rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                rsInventory.Set_Fields("Status", "I");
                rsInventory.Set_Fields("PeriodCloseoutID", 0);
                rsInventory.Set_Fields("TellerCloseoutID", 0);
                rsInventory.Update();
                /*? 580 */ /*? 581 */
            }
        }

        private void SavePlateInventory_NonFleetPending()
        {
            if ((MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "N" || MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "R") && MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate") == "N" && MotorVehicle.Statics.RegistrationType != "DUPREG" && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != "NEW" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "IU")
            {
                // And rsFinal.fields("plate") <> rsFinalCompare.fields("Plate
                MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND FormattedInventory = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
                rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);

                if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                {
                    rsInventory.Edit();

                    if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                    {
                        rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                    }

                    rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsInventory.Set_Fields("Status", "I");
                    rsInventory.Set_Fields("PeriodCloseoutID", 0);
                    rsInventory.Set_Fields("TellerCloseoutID", 0);
                    rsInventory.Update();
                    /*? 559 */ /*? 560 */
                }

                /*? 561 */
            }
        }

        private void SavePlateInventory_FleetPending()
        {
            if ((MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "N" || MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType") == "R") && MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate") == "N" && MotorVehicle.Statics.RegistrationType != "DUPREG" && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != "NEW" && MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") != "IU")
            {
                // And rsFinal.fields("plate") <> rsFinalCompare.fields("Plate
                MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND FormattedInventory = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
                rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);

                if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                {
                    rsInventory.Edit();

                    if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                    {
                        rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                    }

                    rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsInventory.Set_Fields("Status", "P");
                    rsInventory.Set_Fields("PeriodCloseoutID", 0);
                    rsInventory.Set_Fields("TellerCloseoutID", 0);
                    rsInventory.Update();
                    /*? 532 */ /*? 533 */
                }

                /*? 534 */
            }
        }

        private void SaveYearInventory_NonFleetPending()
        {
            if ((MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber")) || (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "IU" && Information.IsNumeric(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("plate")))
            {
                if (Strings.Trim(MotorVehicle.Statics.strCodeY) == "")
                {
                    if (MotorVehicle.Statics.Class == "IU")
                    {
                        MotorVehicle.Statics.strCodeY = "SYD" + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "yy");
                    }
                    else
                    {
                        MakeCode("Y");
                    }
                }

                EditInventory_MVR3_StatusI();
            }
        }

        private void EditInventory_MVR3_StatusI()
        {
            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) == "IU")
            {
                rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));
            }
            else
            {
                rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
                /*? 488 */
            }

            if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
            {
                rsInventory.Edit();

                if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                {
                    rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                }

                rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                rsInventory.Set_Fields("Status", "I");
                rsInventory.Set_Fields("PeriodCloseoutID", 0);
                rsInventory.Set_Fields("TellerCloseoutID", 0);
                rsInventory.Update();
                /*? 503 */ /*? 504 */
            }
        }

        private void SaveYearInventory_FleetPending()
        {
            if ((MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("YearStickerNumber")) || (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "IU" && Information.IsNumeric(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) && MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_String("plate")))
            {
                if (Strings.Trim(MotorVehicle.Statics.strCodeY) == "")
                {
                    if (MotorVehicle.Statics.Class == "IU")
                    {
                        MotorVehicle.Statics.strCodeY = "SYD" + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "yy");
                        /*? 448 */
                    }
                    else
                    {
                        MakeCode("Y");
                        /*? 450 */
                    }

                    /*? 451 */
                }

                if (MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") == "IU")
                {
                    rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate"));
                }
                else
                {
                    rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeY + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
                }

                if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                {
                    EditInventory_MVR3_StatusP();
                }
            }
        }

        private void EditInventory_MVR3_StatusP()
        {
            rsInventory.Edit();

            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
            {
                rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
            }

            rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
            rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
            rsInventory.Set_Fields("Status", "P");
            rsInventory.Set_Fields("PeriodCloseoutID", 0);
            rsInventory.Set_Fields("TellerCloseoutID", 0);
            rsInventory.Update();
        }

        private void SaveInventoryAdjustment_StatusI()
        {
            if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) != "NEW" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) != "DPR")
            {
                // And rsFinal.fields("plate") <> rsFinalCompare.fields("Plate
                MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeP + "' AND isnull(FormattedInventory , '') = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'";
                rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);

                if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                {
                    rsInventory.Edit();

                    if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                    {
                        rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                    }

                    rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);

                    // .Fields("MVR3") =
                    rsInventory.Set_Fields("Status", "I");
                    rsInventory.Set_Fields("PeriodCloseoutID", 0);
                    rsInventory.Set_Fields("TellerCloseoutID", 0);
                    rsInventory.Update();
                }
            }
        }

        private void SaveInventoryAdjustment_CodeP_StatusI(clsDRWrapper rsUpdate, string plate)
        {
            if ((FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "N" || FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("PlateType")) == "R") && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("ForcedPlate")) == "N" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("plate")) != "NEW" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Class")) != "IU" && FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType")) != "DPR")
            {
                if (MotorVehicle.Statics.strCodeP == "")
                {
                    MakeCodeP();
                }

                rsUpdate.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");
                rsUpdate.AddNew();
                rsUpdate.Set_Fields("AdjustmentCode", "I");
                rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                rsUpdate.Set_Fields("QuantityAdjusted", 1);
                rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeP);
                rsUpdate.Set_Fields("Low", plate);
                rsUpdate.Set_Fields("High", plate);
                rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "- MVR10 Form");
                rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                rsUpdate.Set_Fields("TellerCloseoutID", 0);
                rsUpdate.Update();
            }
        }

        private static void UpdateHeldRegistration(clsDRWrapper rsHeld, clsDRWrapper ArchiveRecord)
        {
            int fnx;

            if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate")) != "")
            {
                rsHeld.OpenRecordset("SELECT * FROM Master WHERE Plate = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate") + "'");

                if (rsHeld.EndOfFile() != true && rsHeld.BeginningOfFile() != true)
                {
                    rsHeld.Edit();
                }
                else
                {
                    rsHeld.AddNew();
                }
            }
            else
            {
                rsHeld.OpenRecordset("SELECT * FROM Master WHERE ID = 0");
                rsHeld.AddNew();
            }

            for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
            {
                if (MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx) != "ID")
                {
                    rsHeld.Set_Fields(MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx), MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx));
                }
            }

            rsHeld.Update();
            ArchiveRecord.Execute("DELETE FROM HeldRegistrationMaster WHERE MasterID = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("ID"), "TWMV0000.vb1");
        }

        private void SaveTitleFeeAndCTA(clsDRWrapper rsUpdate, clsDRWrapper rsTestFleetCTA, double TitleFee, string plate)
        {
            rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));

            if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
            {
                rsTitle.MoveLast();
                rsTitle.MoveFirst();
                CreateNewTitleApplication(rsUpdate);

                if (HoldTitleFee(rsTestFleetCTA, ref TitleFee))
                {
                    SaveLongTerm_CTA(rsUpdate, TitleFee, plate);
                }
            }
        }

        private void SaveLongTerm_CTA(clsDRWrapper rsUpdate, double TitleFee, string plate)
        {
            rsUpdate.AddNew();
            rsUpdate.Set_Fields("CTANumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber"));
            rsUpdate.Set_Fields("Fee", TitleFee);
            rsUpdate.Set_Fields("RushFee", MotorVehicle.Statics.rsFinal.Get_Fields("RushTitleFee"));
            rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
            rsUpdate.Set_Fields("Plate", plate);
            rsUpdate.Set_Fields("RegistrationDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
            rsUpdate.Set_Fields("CustomerName", rsTitle.Get_Fields_String("Name1"));
            rsUpdate.Set_Fields("OPID", MotorVehicle.Statics.UserID);
            rsUpdate.Set_Fields("DoubleIfApplicable", rsTitle.Get_Fields_String("DoubleNumber"));
            rsUpdate.Set_Fields("PeriodCloseoutID", 0);
            rsUpdate.Set_Fields("MSRP", rsTitle.Get_Fields_String("MSRP"));
            rsUpdate.Update();

            if (FCConvert.ToInt32(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
            {
                MotorVehicle.Statics.rsDoubleTitle.Update();
            }
        }

        private static bool HoldTitleFee(clsDRWrapper rsTestFleetCTA, ref double TitleFee)
        {
            if (MotorVehicle.Statics.FleetCTA)
            {
                if (MotorVehicle.Statics.VehiclesCovered > 1)
                {
                    rsTestFleetCTA.OpenRecordset("SELECT * FROM LongTermTitleApplications WHERE CTANumber = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber") + "'");

                    if (rsTestFleetCTA.EndOfFile() != true && rsTestFleetCTA.BeginningOfFile() != true)
                    {
                        return false;
                    }
                }

                TitleFee = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee") * MotorVehicle.Statics.VehiclesCovered);
            }
            else
            {
                TitleFee = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"));
            }

            return true;
        }

        private static void CreateNewTitleApplication(clsDRWrapper rsUpdate)
        {
            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                rsUpdate.OpenRecordset("SELECT * FROM PendingTitleApplications WHERE ID = 0");
            }
            else
            {
                rsUpdate.OpenRecordset("SELECT * FROM LongTermTitleApplications WHERE ID = 0");
            }
        }

        private static void CreateNewLongTermRegistration(clsDRWrapper rsAM, string plate, clsDRWrapper rsFees)
        {
            rsAM.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = 0");
            rsAM.AddNew();

            SetFees(rsAM);

            rsAM.Set_Fields("UniqueIdentifier", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("LongTermTrailerRegUniqueIdentifier")));
            rsAM.Set_Fields("Extension", MotorVehicle.Statics.gboolLongRegExtension || MotorVehicle.Statics.gboolLongRegExtensionSamePlate);
            rsAM.Set_Fields("TitleDone", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("TitleDone"));
            rsAM.Set_Fields("UseTaxDone", MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("UseTaxDone"));
            if (FCConvert.ToBoolean(rsAM.Get_Fields_Boolean("UseTaxDone"))) rsAM.Set_Fields("UseTaxID", MotorVehicle.Statics.lngUTCAddNewID);
            rsAM.Set_Fields("DateUpdated", DateTime.Now);
            rsAM.Set_Fields("Make", MotorVehicle.Statics.rsFinal.Get_Fields_String("Make"));
            rsAM.Set_Fields("Unit", MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber"));
            rsAM.Set_Fields("Year", MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
            rsAM.Set_Fields("Style", MotorVehicle.Statics.rsFinal.Get_Fields_String("Style"));
            rsAM.Set_Fields("Color", MotorVehicle.Statics.rsFinal.Get_Fields_String("Color1") + MotorVehicle.Statics.rsFinal.Get_Fields_String("Color2"));

            if (MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST")
            {
                rsAM.Set_Fields("MaineReregistration", false);
                rsAM.Set_Fields("TimePayment", false);
            }
            else
            {
                rsAM.Set_Fields("MaineReregistration", (frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text == "YES - Expires this year" || frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text == "YES - Expires next year"));
                rsAM.Set_Fields("TimePayment", frmDataInputLongTermTrailers.InstancePtr.chkTimePayment.CheckState == CheckState.Checked);
            }

            rsAM.Set_Fields("Plate", plate);
            rsAM.Set_Fields("VIN", MotorVehicle.Statics.rsFinal.Get_Fields_String("VIN"));
            rsAM.Set_Fields("TitleNumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber").ToUpper());
            rsAM.Set_Fields("NetWeight", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")));
            rsAM.Set_Fields("PartyID1", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"));
            rsAM.Set_Fields("PartyID2", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2"));
            rsAM.Set_Fields("Address1", MotorVehicle.Statics.rsFinal.Get_Fields_String("Address"));
            rsAM.Set_Fields("City", MotorVehicle.Statics.rsFinal.Get_Fields_String("City"));
            rsAM.Set_Fields("State", MotorVehicle.Statics.rsFinal.Get_Fields("State"));
            rsAM.Set_Fields("Zip", MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip"));
            rsAM.Set_Fields("LegalResAddress", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResAddress"));
            rsAM.Set_Fields("LegalResCity", MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence"));
            rsAM.Set_Fields("LegalResState", MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState"));
            rsAM.Set_Fields("LegalResZip", MotorVehicle.Statics.rsFinal.Get_Fields_String("LegalResZip"));
            rsAM.Set_Fields("ExpireDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"));

            if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("EffectiveDate")))
            {
                rsAM.Set_Fields("EffectiveDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"));
            }

            rsAM.Set_Fields("FleetNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("FleetNumber"));
            rsAM.Set_Fields("OldPlate", MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate"));
            SetNumberOfYears(rsFees, rsAM);
            rsAM.Set_Fields("Status", "A");
            MotorVehicle.Statics.rsFinal.Set_Fields("DateUpdated", rsAM.Get_Fields_DateTime("DateUpdated"));
            SetArchiveId();

            if (MotorVehicle.Statics.bolFromDosTrio || MotorVehicle.Statics.bolFromWindowsCR)
            {
                MotorVehicle.Write_PDS_Work_Record_Stuff_MVRT10();
            }

            rsAM.Update();
        }

        private static void SetArchiveId()
        {
            if (MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "RRR")
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ArchiveID", MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("ID"));
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ArchiveID", 0);
            }
        }

        private static void SetNumberOfYears(clsDRWrapper rsFees, clsDRWrapper rsAM)
        {
            if (MotorVehicle.Statics.RegistrationType == "DUPREG" || MotorVehicle.Statics.RegistrationType == "LOST" || MotorVehicle.Statics.RegistrationType == "NRT" || MotorVehicle.Statics.RegistrationType == "CORR" || MotorVehicle.Statics.gboolLongRegExtension)
            {
                rsFees.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("LongTermTrailerRegKey")));

                if (rsFees.EndOfFile() != true && rsFees.BeginningOfFile() != true)
                {
                    if (MotorVehicle.Statics.gboolLongRegExtension)
                    {
                        rsAM.Set_Fields("NumberOfYears", rsFees.Get_Fields_Int32("NumberOfYears") + frmDataInputLongTermTrailers.InstancePtr.cboLength.ItemData(frmDataInputLongTermTrailers.InstancePtr.cboLength.SelectedIndex));
                    }
                    else
                    {
                        rsAM.Set_Fields("NumberOfYears", rsFees.Get_Fields_Int32("NumberOfYears"));
                    }
                }
                else
                {
                    rsAM.Set_Fields("NumberOfYears", MotorVehicle.Statics.glngNumberOfYears);
                }
            }
            else
            {
                rsAM.Set_Fields("NumberOfYears", frmDataInputLongTermTrailers.InstancePtr.cboLength.ItemData(frmDataInputLongTermTrailers.InstancePtr.cboLength.SelectedIndex));
            }
        }

        private static void SetFees(clsDRWrapper rsAM)
        {
            switch (MotorVehicle.Statics.RegistrationType)
            {
                case "DUPREG":
                    SetFees_DPR(rsAM);

                    break;
                case "LOST":
                    SetFees_LPS(rsAM);

                    break;
                case "CORR":
                    SetFees_ECO(rsAM);

                    break;
                default:
                    SetFees_Regular(rsAM);

                    break;
            }
        }

        private static void SetFees_Regular(clsDRWrapper rsAM)
        {
            rsAM.Set_Fields("RegistrationType", MotorVehicle.Statics.RegistrationType);
            rsAM.Set_Fields("RegistrationFee", frmDataInputLongTermTrailers.InstancePtr.txtFEERegFee.Text);
            rsAM.Set_Fields("RegistrationCredit", frmDataInputLongTermTrailers.InstancePtr.txtFEECreditTransfer.Text);
            rsAM.Set_Fields("TitleFee", frmDataInputLongTermTrailers.InstancePtr.txtFEETitleFee.Text);
            rsAM.Set_Fields("RushTitleFee", frmDataInputLongTermTrailers.InstancePtr.txtFEERushTitleFee.Text);
            rsAM.Set_Fields("SalesTax", frmDataInputLongTermTrailers.InstancePtr.txtFEESalesTax.Text);
            rsAM.Set_Fields("StateFee", frmDataInputLongTermTrailers.InstancePtr.txtFees.Text);
            rsAM.Set_Fields("AgentFee", frmDataInputLongTermTrailers.InstancePtr.txtFEEAgentFeeReg.Text);
            rsAM.Set_Fields("AgentFeeTitle", frmDataInputLongTermTrailers.InstancePtr.txtFeeAgentFeeCTA.Text);
            rsAM.Set_Fields("TransferFee", frmDataInputLongTermTrailers.InstancePtr.txtFEETransferFee.Text);
        }

        private static void SetFees_ECO(clsDRWrapper rsAM)
        {
            rsAM.Set_Fields("RegistrationType", "ECO");
            rsAM.Set_Fields("RegistrationFee", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("RegRateCharge")));
            rsAM.Set_Fields("RegistrationCredit", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("TransferCreditUsed")));
            rsAM.Set_Fields("TitleFee", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TitleFee")));
            rsAM.Set_Fields("RushTitleFee", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("RushTitleFee")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("RushTitleFee")));
            rsAM.Set_Fields("SalesTax", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("SalesTax")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("SalesTax")));
            rsAM.Set_Fields("StateFee", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("StatePaid")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("StatePaid")));
            rsAM.Set_Fields("AgentFee", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("AgentFee")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("AgentFee")));
            rsAM.Set_Fields("AgentFeeTitle", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("AgentFeeCTA")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields_Decimal("AgentFeeCTA")));
            rsAM.Set_Fields("TransferFee", Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee")) - Conversion.Val(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransferFee")));
            rsAM.Set_Fields("InfoMessage", MotorVehicle.Statics.rsFinal.Get_Fields_String("InfoMessage"));
        }

        private static void SetFees_LPS(clsDRWrapper rsAM)
        {
            rsAM.Set_Fields("RegistrationType", "LPS");
            rsAM.Set_Fields("RegistrationFee", 0);
            rsAM.Set_Fields("RegistrationCredit", 0);
            rsAM.Set_Fields("TitleFee", 0);
            rsAM.Set_Fields("RushTitleFee", 0);
            rsAM.Set_Fields("SalesTax", 0);
            rsAM.Set_Fields("StateFee", 0);
            rsAM.Set_Fields("AgentFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee"));
            rsAM.Set_Fields("AgentFeeTitle", 0);
            rsAM.Set_Fields("TransferFee", 0);
        }

        private static void SetFees_DPR(clsDRWrapper rsAM)
        {
            rsAM.Set_Fields("RegistrationType", "DPR");
            rsAM.Set_Fields("RegistrationFee", 0);
            rsAM.Set_Fields("RegistrationCredit", 0);
            rsAM.Set_Fields("TitleFee", 0);
            rsAM.Set_Fields("RushTitleFee", 0);
            rsAM.Set_Fields("SalesTax", 0);
            rsAM.Set_Fields("StateFee", 0);
            rsAM.Set_Fields("AgentFee", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee"));
            rsAM.Set_Fields("AgentFeeTitle", 0);
            rsAM.Set_Fields("TransferFee", 0);
        }

        private static void SetExciseTaxOrCredit()
        {
            if (MotorVehicle.Statics.blnChangeToFleet)
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxFull", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxFull") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditFull"));
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxCharged", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseTaxCharged") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ExciseCreditUsed"));
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditFull", 0);
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditUsed", 0);
                MotorVehicle.Statics.rsFinal.Set_Fields("ExciseCreditMVR3", 0);
                /*? 15 */
            }

            // 
        }

        private static void SetCorrectionReason()
        {
            string reason;

            do
            {
                reason = Interaction.InputBox("Please enter a reason for this correction", "Reason Needed", null);

                if (reason.Trim()
                          .Length
                    > 0)
                    break;

                MessageBox.Show("You must enter a reason before you can save this registration.", "Invalid Reason", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            } while (true);

            MotorVehicle.Statics.rsECorrect.Set_Fields("InfoMessage", reason);
        }

        private static void CreateActivityMaster(clsDRWrapper rsUpdate, clsDRWrapper rsFees)
        {
            int fnx;

            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                rsUpdate.OpenRecordset("SELECT * FROM PendingActivityMaster WHERE ID = 0");
                /*? 64 */
            }
            else
            {
                rsUpdate.OpenRecordset("SELECT * FROM ActivityMaster WHERE ID = 0");
                /*? 66 */
            }

            rsUpdate.AddNew();

            for (fnx = 1; fnx <= MotorVehicle.Statics.rsFinal.FieldsCount - 1; fnx++)
            {
                var fieldsIndexName = MotorVehicle.Statics.rsFinal.Get_FieldsIndexName(fnx)
                                                  .ToLower();

                switch (fieldsIndexName)
                {
                    case "oldmvr3":
                    case "tellercloseoutid":
                        rsUpdate.Set_Fields(fieldsIndexName, 0);

                        break;
                    case "masterid":
                        break;
                    case "id":

                        // do nothing
                        break;

                    default:
                    {
                        var fieldsIndexValue = MotorVehicle.Statics.rsFinal.Get_FieldsIndexValue(fnx);

                        if (fieldsIndexName != "AgentFee")
                        {
                            rsUpdate.Set_Fields(fieldsIndexName, fieldsIndexValue);
                        }
                        else
                        {
                            if (MotorVehicle.Statics.RegistrationType == "DUPREG")
                            {
                                var classString = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class");

                                if (classString == "CI" || classString == "DV")
                                {
                                    rsUpdate.Set_Fields(fieldsIndexName, 0);
                                }
                                else
                                {
                                    rsFees.OpenRecordset("SELECT * FROM DefaultInfo");

                                    if (rsFees.EndOfFile() != true && rsFees.BeginningOfFile() != true)
                                    {
                                        rsUpdate.Set_Fields(fieldsIndexName, rsFees.Get_Fields_Decimal("DupRegAgentFee"));
                                    }
                                    else
                                    {
                                        rsUpdate.Set_Fields(fieldsIndexName, fieldsIndexValue);
                                    }
                                }
                            }
                            else
                            {
                                rsUpdate.Set_Fields(fieldsIndexName, fieldsIndexValue);
                            }
                        }

                        break;
                    }
                }
            }

            rsUpdate.Update();
        }

        private bool SaveTitleInfo(clsDRWrapper rsUpdate, clsDRWrapper rsTestFleetCTA, string plate)
        {
            double TitleFee;
            rsTitle.OpenRecordset("SELECT * FROM Title WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngCTAAddNewID));

            if (rsTitle.EndOfFile() != true && rsTitle.BeginningOfFile() != true)
            {
                rsTitle.MoveLast();
                rsTitle.MoveFirst();

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                {
                    rsUpdate.OpenRecordset("SELECT * FROM PendingTitleApplications WHERE ID = 0");
                }
                else
                {
                    rsUpdate.OpenRecordset("SELECT * FROM TitleApplications WHERE ID = 0");
                }

                var _titleFee = GetTitleFee(rsTestFleetCTA);

                if (_titleFee == -1) return true;  // this skips the CTA

                // will be valid value here so use it
                TitleFee = _titleFee;

                SaveCTAInfo(rsUpdate, TitleFee, plate);
            }

            if (Conversion.Val(rsTitle.Get_Fields_String("DoubleNumber")) != 0)
            {
                MotorVehicle.Statics.rsDoubleTitle.Update();
            }

            return false;
        }

        private void UpdateInventory_RentalMVR3_StatusI()
        {
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Rental") && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg"))
            {
                rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + FCConvert.ToString(lngNoFeeDupRegMVR3Number));

                if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                {
                    rsInventory.Edit();

                    if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                    {
                        rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                    }

                    rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsInventory.Set_Fields("MVR3", lngNoFeeDupRegMVR3Number);
                    rsInventory.Set_Fields("Status", "I");
                    rsInventory.Set_Fields("PeriodCloseoutID", 0);
                    rsInventory.Set_Fields("TellerCloseoutID", 0);
                    rsInventory.Update();
                    /*? 438 */ /*? 439 */
                }

                /*? 440 */
            }
        }

        private void UpdateInventory_RentalMVR3_StatusP()
        {
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Rental") && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg"))
            {
                rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + FCConvert.ToString(lngNoFeeDupRegMVR3Number));

                if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                {
                    rsInventory.Edit();

                    if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                    {
                        rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                    }

                    rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsInventory.Set_Fields("MVR3", lngNoFeeDupRegMVR3Number);
                    rsInventory.Set_Fields("Status", "P");
                    rsInventory.Set_Fields("PeriodCloseoutID", 0);
                    rsInventory.Set_Fields("TellerCloseoutID", 0);
                    rsInventory.Update();
                    /*? 421 */ /*? 422 */
                }

                /*? 423 */
            }
        }

        private void UpdateInventory_MVR3_StatusI()
        {
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") != 0)
            {
                rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));

                if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                {
                    rsInventory.Edit();

                    if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                    {
                        rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                    }

                    rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsInventory.Set_Fields("Status", "I");
                    rsInventory.Set_Fields("PeriodCloseoutID", 0);
                    rsInventory.Set_Fields("TellerCloseoutID", 0);
                    rsInventory.Update();
                    /*? 402 */ /*? 403 */
                }

                /*? 404 */
            }
        }

        private void UpdateInventory_MVR3_StatusP()
        {
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") != 0)
            {
                rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00' and Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));

                if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                {
                    rsInventory.Edit();
                    rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                    rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsInventory.Set_Fields("Status", "P");
                    rsInventory.Set_Fields("PeriodCloseoutID", 0);
                    rsInventory.Set_Fields("TellerCloseoutID", 0);
                    rsInventory.Update();
                    /*? 381 */ /*? 382 */
                }

                /*? 383 */
            }
        }

        private void UpdateInventory_CodeM_StatusI()
        {
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MonthStickerNumber"))
            {
                if (Strings.Trim(MotorVehicle.Statics.strCodeM) == "")
                {
                    MakeCode("M");
                    /*? 342 */
                }

                rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeM + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));

                if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                {
                    rsInventory.Edit();

                    if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("ExciseTaxDate")))
                    {
                        rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                    }

                    rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsInventory.Set_Fields("Status", "I");
                    rsInventory.Set_Fields("PeriodCloseoutID", 0);
                    rsInventory.Set_Fields("TellerCloseoutID", 0);
                    rsInventory.Update();
                    /*? 358 */ /*? 359 */
                }

                /*? 360 */
            }
        }

        private void UpdateInventory_CodeM_StatusP()
        {
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MonthStickerNumber"))
            {
                if (Strings.Trim(MotorVehicle.Statics.strCodeM) == "")
                {
                    MakeCode("M");
                    /*? 319 */
                }

                rsInventory.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + MotorVehicle.Statics.strCodeM + "' AND Number = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));

                if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
                {
                    rsInventory.Edit();
                    rsInventory.Set_Fields("InventoryDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
                    rsInventory.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                    rsInventory.Set_Fields("MVR3", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsInventory.Set_Fields("Status", "P");
                    rsInventory.Set_Fields("PeriodCloseoutID", 0);
                    rsInventory.Set_Fields("TellerCloseoutID", 0);
                    rsInventory.Update();
                    /*? 335 */ /*? 336 */
                }

                /*? 337 */
            }
        }

        private static void SaveTemporaryPlateInfo(clsDRWrapper rsUpdate, string plate)
        {
            string st1 = "";
            rsUpdate.OpenRecordset("SELECT * FROM TemporaryPlateRegister WHERE ID = 0");
            st1 = Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"), true));
            rsUpdate.AddNew();
            rsUpdate.Set_Fields("TemporaryPlateNumber", MotorVehicle.Statics.TempPlateNumber);
            rsUpdate.Set_Fields("OwnerCode1", MotorVehicle.Statics.rsFinal.Get_Fields_String("OwnerCode1"));
            rsUpdate.Set_Fields("Owner1", st1);
            rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
            rsUpdate.Set_Fields("Plate", plate);
            rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
            rsUpdate.Set_Fields("PeriodCloseoutID", 0);
            rsUpdate.Update();
        }

        private void SaveCodePAdjustment(clsDRWrapper rsUpdate, string plate)
        {
            if (MotorVehicle.Statics.strCodeP == "")
            {
                MakeCodeP();
                /*? 281 */
            }

            rsUpdate.AddNew();

            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                rsUpdate.Set_Fields("AdjustmentCode", "P");
                rsUpdate.Set_Fields("DateofAdjustment", MotorVehicle.Statics.PreEffectiveDate);
                /*? 286 */
            }
            else
            {
                rsUpdate.Set_Fields("AdjustmentCode", "I");
                rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                /*? 289 */
            }

            rsUpdate.Set_Fields("QuantityAdjusted", 1);
            rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeP);
            rsUpdate.Set_Fields("Low", plate);
            rsUpdate.Set_Fields("High", plate);
            rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
            rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
            rsUpdate.Set_Fields("PeriodCloseoutID", 0);
            rsUpdate.Set_Fields("TellerCloseoutID", 0);
            rsUpdate.Update();
            /*? 300 */
        }

        private static void SaveCodeYAdjustment(clsDRWrapper rsUpdate, string plate)
        {
            if (Strings.Trim(MotorVehicle.Statics.strCodeY) == "")
            {
                if (MotorVehicle.Statics.Class == "IU")
                    MotorVehicle.Statics.strCodeY = "SYD" + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "yy");
                else
                    MakeCode("Y");
            }

            rsUpdate.AddNew();

            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                rsUpdate.Set_Fields("AdjustmentCode", "P");
                rsUpdate.Set_Fields("DateofAdjustment", MotorVehicle.Statics.PreEffectiveDate);
                /*? 258 */
            }
            else
            {
                rsUpdate.Set_Fields("AdjustmentCode", "I");
                rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                /*? 261 */
            }

            rsUpdate.Set_Fields("QuantityAdjusted", 1);
            rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeY);

            if (MotorVehicle.Statics.Class == "IU")
            {
                rsUpdate.Set_Fields("Low", plate);
                rsUpdate.Set_Fields("High", plate);
                /*? 267 */
            }
            else
            {
                rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
                rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNumber"));
                /*? 270 */
            }

            rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
            rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
            rsUpdate.Set_Fields("PeriodCloseoutID", 0);
            rsUpdate.Set_Fields("TellerCloseoutID", 0);
            rsUpdate.Update();
            /*? 277 */
        }

        private void SaveRentalInfo(clsDRWrapper rsUpdate)
        {
            if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Rental") && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg"))
            {
                rsUpdate.AddNew();

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                {
                    rsUpdate.Set_Fields("AdjustmentCode", "P");
                    rsUpdate.Set_Fields("DateofAdjustment", MotorVehicle.Statics.PreEffectiveDate);
                    /*? 213 */
                }
                else
                {
                    rsUpdate.Set_Fields("AdjustmentCode", "I");
                    rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                    /*? 216 */
                }

                rsUpdate.Set_Fields("QuantityAdjusted", 1);
                rsUpdate.Set_Fields("InventoryType", "MXS00");
                rsUpdate.Set_Fields("Low", lngNoFeeDupRegMVR3Number);
                rsUpdate.Set_Fields("High", lngNoFeeDupRegMVR3Number);
                rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                rsUpdate.Set_Fields("Reason", "No Fee Dup Reg Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + FCConvert.ToString(lngNoFeeDupRegMVR3Number));
                rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                rsUpdate.Set_Fields("TellerCloseoutID", 0);
                rsUpdate.Update();
            }
        }

        private static void SaveMVR3Info(clsDRWrapper rsUpdate)
        {
            rsUpdate.AddNew();

            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                rsUpdate.Set_Fields("AdjustmentCode", "P");
                rsUpdate.Set_Fields("DateofAdjustment", MotorVehicle.Statics.PreEffectiveDate);
                /*? 194 */
            }
            else
            {
                rsUpdate.Set_Fields("AdjustmentCode", "I");
                rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                /*? 197 */
            }

            rsUpdate.Set_Fields("QuantityAdjusted", 1);
            rsUpdate.Set_Fields("InventoryType", "MXS00");
            rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
            rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
            rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
            rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
            rsUpdate.Set_Fields("PeriodCloseoutID", 0);
            rsUpdate.Set_Fields("TellerCloseoutID", 0);
            rsUpdate.Update();
        }

        private void SaveLastMVRNumber(clsDRWrapper rsMVR3)
        {
            int tempPrinter = StaticSettings.GlobalCommandDispatcher.Send(new GetPrinterGroup()).Result;

            rsMVR3.OpenRecordset("SELECT * FROM LastMVRNumber WHERE PrinterNumber = " + FCConvert.ToString(tempPrinter));

            if (rsMVR3.EndOfFile() != true && rsMVR3.BeginningOfFile() != true)
            {
                rsMVR3.Edit();

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Rental") && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg"))
                {
                    rsMVR3.Set_Fields("LastMVRNumber", lngNoFeeDupRegMVR3Number);
                    /*? 177 */
                }
                else
                {
                    rsMVR3.Set_Fields("LastMVRNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    /*? 179 */
                }

                /*? 180 */
            }
            else
            {
                rsMVR3.AddNew();
                rsMVR3.Set_Fields("PrinterNumber", tempPrinter);

                if (MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("Rental") && MotorVehicle.Statics.rsFinal.Get_Fields_Boolean("NoFeeDupReg"))
                {
                    rsMVR3.Set_Fields("LastMVRNumber", lngNoFeeDupRegMVR3Number);
                    /*? 185 */
                }
                else
                {
                    rsMVR3.Set_Fields("LastMVRNumber", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    /*? 187 */
                }

                /*? 188 */
            }

            rsMVR3.Update();
        }

        private void SaveInventoryAdjustments(clsDRWrapper rsUpdate)
        {
            rsUpdate.OpenRecordset("SELECT * FROM InventoryAdjustments WHERE ID = 0");

            if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
            {
                if (FCUtils.IsEmptyDateTime(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"))) MotorVehicle.Statics.rsFinal.Set_Fields("ExciseTaxDate", Strings.Format(MotorVehicle.Statics.PreEffectiveDate, "MM/dd/yyyy"));
                /*? 143 */
            }

            if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != 0 && MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber") != MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("MonthStickerNumber"))
            {
                if (Strings.Trim(MotorVehicle.Statics.strCodeM) == "")
                {
                    MakeCode("M");
                    /*? 147 */
                }

                rsUpdate.AddNew();

                if (MotorVehicle.Statics.RegisteringFleet && MotorVehicle.Statics.PendingRegistration)
                {
                    rsUpdate.Set_Fields("AdjustmentCode", "P");
                    rsUpdate.Set_Fields("DateofAdjustment", MotorVehicle.Statics.PreEffectiveDate);
                    /*? 152 */
                }
                else
                {
                    rsUpdate.Set_Fields("AdjustmentCode", "I");
                    rsUpdate.Set_Fields("DateofAdjustment", DateTime.Today);
                    /*? 155 */
                }

                rsUpdate.Set_Fields("QuantityAdjusted", 1);
                rsUpdate.Set_Fields("InventoryType", MotorVehicle.Statics.strCodeM);
                rsUpdate.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));
                rsUpdate.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNumber"));
                rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
                rsUpdate.Set_Fields("Reason", "Issued-" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "-" + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                rsUpdate.Set_Fields("PeriodCloseoutID", 0);
                rsUpdate.Set_Fields("TellerCloseoutID", 0);
                rsUpdate.Update();
                /*? 166 */
            }
        }

        private static void HoldTransactionId(clsDRWrapper rsID)
        {
            rsID.OpenRecordset("SELECT * FROM ActivityMaster WHERE MVR3 = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") + " AND Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "'");

            if (rsID.EndOfFile() != true && rsID.BeginningOfFile() != true)
            {
                MotorVehicle.Statics.TransactionID = FCConvert.ToString(rsID.Get_Fields_Int32("ID"));
            }
            else
            {
                MotorVehicle.Statics.TransactionID = "";
            }
        }

        private static double GetTitleFee(clsDRWrapper rsTestFleetCTA)
        {
            double titleFee;

            if (MotorVehicle.Statics.FleetCTA)
            {
                if (MotorVehicle.Statics.VehiclesCovered > 1)
                {
                    rsTestFleetCTA.OpenRecordset("SELECT * FROM TitleApplications WHERE CTANumber = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber") + "'");

                    if (rsTestFleetCTA.EndOfFile() != true && rsTestFleetCTA.BeginningOfFile() != true)
                    {
                        return -1;
                    }
                }

                titleFee = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee") * MotorVehicle.Statics.VehiclesCovered);
            }
            else
            {
                titleFee = Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TitleFee"));
            }

            return titleFee;
        }

        private void SaveCTAInfo(clsDRWrapper rsUpdate, double TitleFee, string plate)
        {
            rsUpdate.AddNew();
            rsUpdate.Set_Fields("CTANumber", MotorVehicle.Statics.rsFinal.Get_Fields_String("CTANumber").ToUpper());
            rsUpdate.Set_Fields("Fee", TitleFee);
            rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
            rsUpdate.Set_Fields("Plate", plate);
            rsUpdate.Set_Fields("RegistrationDate", MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExciseTaxDate"));
            rsUpdate.Set_Fields("CustomerName", rsTitle.Get_Fields_String("Name1"));
            rsUpdate.Set_Fields("OPID", MotorVehicle.Statics.UserID);
            rsUpdate.Set_Fields("DoubleIfApplicable", rsTitle.Get_Fields_String("DoubleNumber"));
            rsUpdate.Set_Fields("PeriodCloseoutID", 0);
            rsUpdate.Set_Fields("MSRP", rsTitle.Get_Fields_String("MSRP"));
            rsUpdate.Update();
        }

        private static void SaveGiftCertificateInfo(clsDRWrapper rsUpdate, string plate)
        {
            rsUpdate.OpenRecordset("SELECT * FROM GiftCertificateRegister WHERE ID = 0");
            rsUpdate.AddNew();
            rsUpdate.Set_Fields("DateRedeemed", DateTime.Today);
            rsUpdate.Set_Fields("CertificateNumber", Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertificateNumber"))));
            rsUpdate.Set_Fields("Name", Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"))));
            rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
            rsUpdate.Set_Fields("Plate", plate);
            rsUpdate.Set_Fields("Amount", MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("GiftCertificateAmount"));
            rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.OpID);
            rsUpdate.Set_Fields("PeriodCloseoutID", 0);
            rsUpdate.Set_Fields("GiftCertOrVoucher", MotorVehicle.Statics.rsFinal.Get_Fields_String("GiftCertOrVoucher"));
            rsUpdate.Set_Fields("RegistrationID", MotorVehicle.Statics.TransactionID);
            rsUpdate.Update();
        }

        private static void SaveETOInformation(clsDRWrapper rsUpdate)
        {
            rsUpdate.OpenRecordset("SELECT * FROM ExciseTax WHERE ID = 0");
            rsUpdate.AddNew();
            rsUpdate.Set_Fields("MVR3Number", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
            rsUpdate.Set_Fields("Class", MotorVehicle.Statics.rsFinal.Get_Fields_String("Class"));
            rsUpdate.Set_Fields("DateofTransaction", DateTime.Today);
            rsUpdate.Set_Fields("OpID", MotorVehicle.Statics.UserID);
            rsUpdate.Set_Fields("PeriodCloseoutID", 0);
            rsUpdate.Update();
        }

        public void lblSave_Click()
        {
            lblSave_Click(lblSave, new EventArgs());
        }

        private void lblSave_MouseMove(object sender, MouseEventArgs e)
        {
            HighlightField(ref lblSave);
        }

        private void lblUseTax_Click(object sender, EventArgs e)
        {
            var rsLoad = new clsDRWrapper();
            int intLeaseUseTaxFormVersion = 0;

            try
            {
                blnLabelClicked = true;

                
                rsLoad.OpenRecordset("select * from WMV", "twmv0000.vb1");
                if (!rsLoad.EndOfFile()) intLeaseUseTaxFormVersion = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("LeaseUseTaxFormVersion"))));

                PrintUseTax(intLeaseUseTaxFormVersion);

                if (MotorVehicle.Statics.blnPrintUseTax)
                {
                    lblUseTaxDone.Text = done;

                    if (isTitleNotNeeded() && isMvr3NotNeeded()) lblSave.Enabled = true;

                    if (isTitleNotNeeded()) lblBatchRegistration.Enabled = true;
                }

                Information.Err().Clear();

                blnLabelClicked = false;
            }
            finally
            {
                rsLoad.DisposeOf();
            }
        }

        private bool isMvr3NotNeeded()
        {
            return Strings.UCase(lblMVR3Done.Text) != needed;
        }

        private void PrintUseTax(int intLeaseUseTaxFormVersion)
        {
            string temp;

            if (MotorVehicle.IsLease())
            {
                temp = StaticSettings.GlobalSettingService.GetSettingValue(SettingOwnerType.Machine, "CTAPrinterName")?.SettingValue ?? "";

				if (isTxtFile(temp))
                {
                    rptLeaseUseTaxCertificate.InstancePtr.Run(false);
                    rptLeaseUseTaxCertificate.InstancePtr.Unload();
                }
                else
                {
                    if (modPrinterFunctions.PrintXsForAlignment("This 'X' should have printed next to the line USE TAX CERTIFICATE." + NEWLINE + "If this is correct press 'Yes', to reprint the 'X' press 'No', to quit press 'Cancel'.", 52, 1, MotorVehicle.Statics.gstrCTAPrinterName) != DialogResult.Cancel)
                    {
                        if (intLeaseUseTaxFormVersion == 1)
                        {
                            rptLeaseUseTaxCertificateV2.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
                            rptLeaseUseTaxCertificateV2.InstancePtr.Unload();
                        }
                        else
                        {
                            rptLeaseUseTaxCertificate.InstancePtr.PrintReportOnDotMatrix("ctaprintername");
                            rptLeaseUseTaxCertificate.InstancePtr.Unload();
                        }
                    }
                    else
                    {
                        MotorVehicle.Statics.blnPrintUseTax = false;
                    }
                }
            }
            else
            {
	            temp = StaticSettings.GlobalSettingService
		                   .GetSettingValue(SettingOwnerType.Machine, "UseTaxPrinterName")?.SettingValue ?? "";


				if (isTxtFile(temp))
                {
                    rptUseTaxCert2017.InstancePtr.Run(false);
                    rptUseTaxCert2017.InstancePtr.Unload();
                }
                else
                {
                    if (MotorVehicle.Statics.intUseTaxFormVersion < 3)
                    {
                        MessageBox.Show("Old Versions of the Use Tax Certificate are no longer supported. Please update the Use Tax form settings.");
                    }
                    else
                    {
                        rptUseTaxCert2017.InstancePtr.TestPrint = false;
                        rptUseTaxCert2017.InstancePtr.PrintUTCForm = modRegistry.GetRegistryKey("USETAX_PRINTFORM") == "Y";
                        rptUseTaxCert2017.InstancePtr.Run();

                        if ( modRegistry.GetRegistryKey("USETAX_DUPLEX") == "Y")
                        {
                            // Print 2-sided
                            rptUseTaxCert2017.InstancePtr.PrintReportOnDotMatrix("UseTaxPrinterName");
                        }
                        else
                        {
                            // Manually print 2-sided
                            rptUseTaxCert2017.InstancePtr.PrintReportOnDotMatrix("UseTaxPrinterName", printerSettings: new DirectPrintingParams { FromPage = 1, ToPage = 1 });
                            MessageBox.Show("Printing front side. When finished, insert the paper to print the back of the form and click OK.");
                            rptUseTaxCert2017.InstancePtr.PrintReportOnDotMatrix("UseTaxPrinterName", printerSettings: new DirectPrintingParams { FromPage = 2, ToPage = 2 });
                        }

                        rptUseTaxCert2017.InstancePtr.Unload();
                    }
                }
            }

        }

        private static bool isTxtFile(string fileName)
        {
            return Strings.UCase(Strings.Right(fileName, 4)) == ".TXT";
        }

        public void lblUseTax_Click()
        {
            lblUseTax_Click(lblUseTax, new EventArgs());
        }

        private void lblUseTax_MouseMove(object sender, MouseEventArgs e)
        {
            HighlightField(ref lblUseTax);
        }

        private void ChangeBackground(ref FCLabel lbl)
        {
            lbl.BackColor = SystemColors.Highlight;
            lbl.ForeColor = SystemColors.HighlightText;
        }

        private void ClearColor()
        {
            lblCTA.BackColor = SystemColors.Control;
            lblCTA.ForeColor = SystemColors.ControlText;
            lblUseTax.BackColor = SystemColors.Control;
            lblUseTax.ForeColor = SystemColors.ControlText;
            lblMVR3.BackColor = SystemColors.Control;
            lblMVR3.ForeColor = SystemColors.ControlText;
            lblHold.BackColor = SystemColors.Control;
            lblHold.ForeColor = SystemColors.ControlText;
            lblSave.BackColor = SystemColors.Control;
            lblSave.ForeColor = SystemColors.ControlText;
            lblBatchRegistration.BackColor = SystemColors.Control;
            lblBatchRegistration.ForeColor = SystemColors.ControlText;
            lblPrintQueue.BackColor = SystemColors.Control;
            lblPrintQueue.ForeColor = SystemColors.ControlText;
        }

        private bool ValidateMVR3(ref int xx)
        {
            bool ValidateMVR3 = false;
            ValidateMVR3 = true;
            clsDRWrapper rsMVR3 = new clsDRWrapper();
            rsMVR3.OpenRecordset("SELECT * FROM Inventory WHERE Code = '" + "MXS00" + "'  AND Status <> 'I' AND Number = " + FCConvert.ToString(xx));
            if (rsMVR3.EndOfFile() != true && rsMVR3.BeginningOfFile() != true)
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") == xx)
                {
                    ValidateMVR3 = false;
                }
                else
                {
                    clsDRWrapper rsV = new clsDRWrapper();
                    rsV.OpenRecordset("SELECT * FROM InventoryAdjustments");
                    rsV.AddNew();
                    rsV.Set_Fields("DateOfAdjustment", DateTime.Today);
                    rsV.Set_Fields("High", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsV.Set_Fields("Low", MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"));
                    rsV.Set_Fields("OpID", MotorVehicle.Statics.rsFinal.Get_Fields_String("OpID"));
                    rsV.Set_Fields("reason", "Printer Alignment/Jam Problem");
                    rsV.Set_Fields("PeriodCloseoutID", 0);
                    rsV.Set_Fields("QuantityAdjusted", 1);
                    rsV.Set_Fields("InventoryType", "MXS00");
                    rsV.Set_Fields("AdjustmentCode", "V");
                    rsV.Update();
                }
            }
            else
            {
                ValidateMVR3 = false;
            }
            return ValidateMVR3;
        }

        private static void MakeCode(string x)
        {
            if (x == "M")
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerCharge") + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerNoCharge") > 1)
                {
                    MotorVehicle.Statics.strCodeM = "SMD";
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth") < 10)
                    {
                        MotorVehicle.Statics.strCodeM += "0" + Strings.Trim(Conversion.Str(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth")));
                    }
                    else
                    {
                        MotorVehicle.Statics.strCodeM += Strings.Trim(Conversion.Str(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth")));
                    }
                }
                else
                {
                    MotorVehicle.Statics.strCodeM = "SMS";
                    if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth") < 10)
                    {
                        MotorVehicle.Statics.strCodeM += "0" + Strings.Trim(Conversion.Str(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth")));
                    }
                    else
                    {
                        MotorVehicle.Statics.strCodeM += Strings.Trim(Conversion.Str(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MonthStickerMonth")));
                    }
                }
            }
            else
            {
                if (MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerCharge") + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("YearStickerNoCharge") > 1)
                {
                    MotorVehicle.Statics.strCodeY = "SYD" + Strings.Right(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")), 2);
                }
                else
                {
                    MotorVehicle.Statics.strCodeY = "SYS" + Strings.Right(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")), 2);
                }
            }
        }

        private void ResetUpdateVariables(bool x)
        { }

        private void Hold_Plate_From_Inventory(string plate)
        {
            clsDRWrapper rsPL = new clsDRWrapper();
            rsPL.OpenRecordset("SELECT * FROM Inventory WHERE Status = 'A' AND FormattedInventory = '" + plate + "'");
            if (rsPL.EndOfFile() != true && rsPL.BeginningOfFile() != true)
            {
                rsPL.MoveLast();
                rsPL.MoveFirst();
                rsPL.Edit();
                rsPL.Set_Fields("Status", "H");
                rsPL.Update();
            }
        }

        private void PrintNoFeeDupReg()
        {
            // vbPorter upgrade warning: answer As int	OnWrite(DialogResult)
            DialogResult answer = 0;
            int NewMVR3Number;
            string temp;
            clsDRWrapper rsMVR3 = new clsDRWrapper();
            temp = Interaction.InputBox("Please input the MVR3 Number for the No Fee Duplicate Registration." + NEWLINE + NEWLINE + "The last number used:  " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3"), "Next MVR3", FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("MVR3") + 1));
            if (temp.Length > 8)
                return;
            NewMVR3Number = FCConvert.ToInt32(Math.Round(Conversion.Val(temp)));
            if (NewMVR3Number != 0)
            {
                rsMVR3.OpenRecordset("SELECT * FROM Inventory WHERE Code = 'MXS00'  AND Status <> 'I' AND Number = " + FCConvert.ToString(NewMVR3Number));
                if (rsMVR3.EndOfFile() != true && rsMVR3.BeginningOfFile() != true)
                {
                    lngNoFeeDupRegMVR3Number = NewMVR3Number;
                }
                else
                {
                    MessageBox.Show("There is no MVR3 with that number in inventory.", "No MVR3", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeDupReg", false);
                    return;
                    // no such number in inventory
                }
            }
            else
            {
                // bad input in text box
                MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeDupReg", false);
                return;
            }
            MotorVehicle.Statics.rsFinal.Set_Fields("NoFeeMVR3Number", FCConvert.ToInt32(FCConvert.ToDouble(temp)));

            temp = MotorVehicle.Statics.MVR3PrinterName;
            if (isTxtFile(temp))
            {
                blnExport = true;
                rptMVR3Laser.InstancePtr.Run(false);
            }
            else
            {
                blnExport = false;
                rptMVR3Laser.InstancePtr.PrintReportOnDotMatrix("MVR3PrinterName");
            }

            rptMVR3Laser.InstancePtr.Unload();

            if (MotorVehicle.Statics.PrintMVR3)
            {
                if (Information.Err().Number == 0)
                {
                    lblMVR3Done.Text = "DONE";
                    lblPrintQueue.Enabled = false;
                    cmdReturn.Enabled = false;
                    lblHold.Enabled = false;
                    if (isTitleNotNeeded() && Strings.UCase(lblUseTaxDone.Text) != needed)
                    {
                        lblSave.Enabled = true;
                    }
                }
            }
            rsMVR3.Reset();
        }

        private bool isTitleNotNeeded()
        {
            return Strings.UCase(lblTitleDone.Text) != needed;
        }

        private void MakeCodeP()
        {
            clsDRWrapper rs = new clsDRWrapper();
            rs.OpenRecordset("SELECT * FROM Inventory WHERE (FormattedInventory) = '" + MotorVehicle.Statics.rsFinal.Get_Fields_String("plate") + "'");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                MotorVehicle.Statics.strCodeP = FCConvert.ToString(rs.Get_Fields("Code"));
            }
        }
    }
}
