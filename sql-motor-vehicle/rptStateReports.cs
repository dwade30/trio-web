﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using System;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptStateReports.
	/// </summary>
	public partial class rptStateReports : BaseSectionReport
	{
		public rptStateReports()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "State Reports";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptStateReports InstancePtr
		{
			get
			{
				return (rptStateReports)Sys.GetInstance(typeof(rptStateReports));
			}
		}

		protected rptStateReports _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptStateReports	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstReport;
		string strCurrentReport = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: SubReport3 As object	OnWrite(rptDiskVerification, rptTownSummary, rptTownDetail, rptTitleApplication, rptNewExceptionReport, rptVoidedMVR3, rptExciseTaxOnly, rptInventoryAdjustments)
			rptStateReports.InstancePtr.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			if (frmReport.InstancePtr.strReportPrinting == "DV")
			{
				SubReport3.Report = new rptDiskVerification();
			}
			else if (frmReport.InstancePtr.strReportPrinting == "TS")
			{
				SubReport3.Report = new rptTownSummary();
			}
			else if (frmReport.InstancePtr.strReportPrinting == "TD")
			{
				SubReport3.Report = new rptTownDetail();
			}
			else if (frmReport.InstancePtr.strReportPrinting == "TA")
			{
				SubReport3.Report = new rptTitleApplication();
			}
			else if (frmReport.InstancePtr.strReportPrinting == "EX")
			{
				SubReport3.Report = new rptNewExceptionReport();
			}
			else if (frmReport.InstancePtr.strReportPrinting == "VD")
			{
				SubReport3.Report = new rptVoidedMVR3();
			}
			else if (frmReport.InstancePtr.strReportPrinting == "ET")
			{
				SubReport3.Report = new rptExciseTaxOnly();
			}
			else if (frmReport.InstancePtr.strReportPrinting == "IA")
			{
				SubReport3.Report = new rptInventoryAdjustments();
			}
			else
			{
				rptStateReports.InstancePtr.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				if (blnFirstReport)
				{
					blnFirstReport = false;
					eArgs.EOF = false;
				}
				else
				{
					if (CheckForAnotherReport(ref strCurrentReport))
					{
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//this.Document.Printer.PrintQuality = ddPQDraft;
			blnFirstReport = true;
			strCurrentReport = frmReport.InstancePtr.strReportPrinting;
			//Application.DoEvents();
		}

		private bool CheckForAnotherReport(ref string strReport)
		{
			bool CheckForAnotherReport = false;
			int counter = 0;
			CheckForAnotherReport = false;
			if (strReport == "MV")
			{
				counter = 1;
			}
			else if (strReport == "SS")
			{
				counter = 2;
			}
			else if (strReport == "DS")
			{
				counter = 3;
			}
			else if (strReport == "DE")
			{
				counter = 4;
			}
			else if (strReport == "PL")
			{
				counter = 5;
			}
			else
			{
				return CheckForAnotherReport;
			}
			if (frmReport.InstancePtr.cmbAll.Text != "Print All")
			{
				for (counter = counter; counter <= 5; counter++)
				{
					if (frmReport.InstancePtr.Check1[counter].CheckState == Wisej.Web.CheckState.Checked)
					{
						CheckForAnotherReport = true;
						break;
					}
				}
			}
			else
			{
				CheckForAnotherReport = true;
			}
			switch (counter)
			{
				case 1:
					{
						strCurrentReport = "SS";
						break;
					}
				case 2:
					{
						strCurrentReport = "DS";
						break;
					}
				case 3:
					{
						strCurrentReport = "DE";
						break;
					}
				case 4:
					{
						strCurrentReport = "PL";
						break;
					}
				case 5:
					{
						strCurrentReport = "PM";
						break;
					}
				default:
					{
						strCurrentReport = "NONE";
						break;
					}
			}
			//end switch
			return CheckForAnotherReport;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: SubReport3 As object	OnWrite(rptReconDecal, rptReconPermits, rptReconMVR3, rptReconSingle, rptReconDouble, rptReconPlates)
			if (strCurrentReport == "DE")
			{
				SubReport3.Report = new rptReconDecal();
			}
			else if (strCurrentReport == "PM")
			{
				SubReport3.Report = new rptReconPermits();
			}
			else if (strCurrentReport == "MV")
			{
				SubReport3.Report = new rptReconMVR3();
			}
			else if (strCurrentReport == "SS")
			{
				SubReport3.Report = new rptReconSingle();
			}
			else if (strCurrentReport == "DS")
			{
				SubReport3.Report = new rptReconDouble();
			}
			else if (strCurrentReport == "PL")
			{
				SubReport3.Report = new rptReconPlates();
			}
		}

		private void rptStateReports_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptStateReports properties;
			//rptStateReports.Caption	= "State Reports";
			//rptStateReports.Icon	= "rptStateReports.dsx":0000";
			//rptStateReports.Left	= 0;
			//rptStateReports.Top	= 0;
			//rptStateReports.Width	= 11880;
			//rptStateReports.Height	= 8595;
			//rptStateReports.StartUpPosition	= 3;
			//rptStateReports.SectionData	= "rptStateReports.dsx":058A;
			//End Unmaped Properties
		}
	}
}
