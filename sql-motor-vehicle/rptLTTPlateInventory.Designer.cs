﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptLTTPlateInventory.
	/// </summary>
	partial class rptLTTPlateInventory
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLTTPlateInventory));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.txtDescTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtOnHand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtRecorded = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtIssues = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAdjusted = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtItem = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlatesIssued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlatesReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlatesOnHandAtStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlatesVoided = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlatesOnHandAtEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtDescTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOnHand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRecorded)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssues)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjusted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItem)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlatesIssued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlatesReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlatesOnHandAtStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlatesVoided)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlatesOnHandAtEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtOnHand,
				this.txtRecorded,
				this.txtIssues,
				this.txtAdjusted,
				this.txtEnd,
				this.txtItem,
				this.txtPlatesIssued,
				this.txtPlatesReceived,
				this.txtPlatesOnHandAtStart,
				this.txtPlatesVoided,
				this.txtPlatesOnHandAtEnd
			});
			this.Detail.Height = 0.625F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport2,
				this.txtDescTitle,
				this.lblPage,
				this.Label1,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16
			});
			this.PageHeader.Height = 2.0625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// SubReport2
			// 
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 0.9375F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 0.34375F;
			this.SubReport2.Width = 7.46875F;
			// 
			// txtDescTitle
			// 
			this.txtDescTitle.Height = 0.25F;
			this.txtDescTitle.Left = 0.0625F;
			this.txtDescTitle.Name = "txtDescTitle";
			this.txtDescTitle.Style = "font-family: \'Roman 10cpi\'; font-size: 12pt; font-weight: bold; ddo-char-set: 1";
			this.txtDescTitle.Text = "Field1";
			this.txtDescTitle.Top = 1.375F;
			this.txtDescTitle.Width = 3.0625F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.75F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblPage.Text = "VENDOR ID#";
			this.lblPage.Top = 0.0625F;
			this.lblPage.Width = 0.9375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.75F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Roman 10cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label1.Text = "**** LTT PLATE INVENTORY REPORT****";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 4.96875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.375F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label12.Text = "Plates on Hand Start of Period";
			this.Label12.Top = 1.6875F;
			this.Label12.Width = 1.25F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.375F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 6.1875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label13.Text = "Plates on Hand End of Period";
			this.Label13.Top = 1.6875F;
			this.Label13.Width = 1.1875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.375F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label14.Text = "Plates Voided During Period";
			this.Label14.Top = 1.6875F;
			this.Label14.Width = 1.3125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.375F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.1875F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label15.Text = "Plates Issued During Period";
			this.Label15.Top = 1.6875F;
			this.Label15.Width = 1.0625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.375F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1.625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Roman 17cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.Label16.Text = "Plates Received During Period";
			this.Label16.Top = 1.6875F;
			this.Label16.Width = 1.1875F;
			// 
			// txtOnHand
			// 
			this.txtOnHand.CanShrink = true;
			this.txtOnHand.Height = 0.25F;
			this.txtOnHand.Left = 0.375F;
			this.txtOnHand.Name = "txtOnHand";
			this.txtOnHand.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.txtOnHand.Text = null;
			this.txtOnHand.Top = 0F;
			this.txtOnHand.Width = 0.75F;
			// 
			// txtRecorded
			// 
			this.txtRecorded.CanShrink = true;
			this.txtRecorded.Height = 0.25F;
			this.txtRecorded.Left = 1.6875F;
			this.txtRecorded.Name = "txtRecorded";
			this.txtRecorded.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.txtRecorded.Text = null;
			this.txtRecorded.Top = 0F;
			this.txtRecorded.Width = 1.0625F;
			// 
			// txtIssues
			// 
			this.txtIssues.CanShrink = true;
			this.txtIssues.Height = 0.25F;
			this.txtIssues.Left = 3.1875F;
			this.txtIssues.Name = "txtIssues";
			this.txtIssues.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.txtIssues.Text = null;
			this.txtIssues.Top = 0F;
			this.txtIssues.Width = 1.0625F;
			// 
			// txtAdjusted
			// 
			this.txtAdjusted.CanShrink = true;
			this.txtAdjusted.Height = 0.25F;
			this.txtAdjusted.Left = 4.6875F;
			this.txtAdjusted.Name = "txtAdjusted";
			this.txtAdjusted.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.txtAdjusted.Text = null;
			this.txtAdjusted.Top = 0F;
			this.txtAdjusted.Width = 1.0625F;
			// 
			// txtEnd
			// 
			this.txtEnd.CanShrink = true;
			this.txtEnd.Height = 0.25F;
			this.txtEnd.Left = 6.1875F;
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.txtEnd.Text = null;
			this.txtEnd.Top = 0F;
			this.txtEnd.Width = 1.0625F;
			// 
			// txtItem
			// 
			this.txtItem.CanShrink = true;
			this.txtItem.Height = 0.1875F;
			this.txtItem.Left = 0.0625F;
			this.txtItem.Name = "txtItem";
			this.txtItem.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.txtItem.Text = "ITEMIZED LISTING----------------";
			this.txtItem.Top = 0.25F;
			this.txtItem.Width = 3.0625F;
			// 
			// txtPlatesIssued
			// 
			this.txtPlatesIssued.CanShrink = true;
			this.txtPlatesIssued.Height = 0.1875F;
			this.txtPlatesIssued.Left = 3.1875F;
			this.txtPlatesIssued.Name = "txtPlatesIssued";
			this.txtPlatesIssued.Style = "font-family: \'Roman 12cpi\'; font-size: 8pt; ddo-char-set: 1";
			this.txtPlatesIssued.Text = null;
			this.txtPlatesIssued.Top = 0.4375F;
			this.txtPlatesIssued.Width = 1.375F;
			// 
			// txtPlatesReceived
			// 
			this.txtPlatesReceived.CanShrink = true;
			this.txtPlatesReceived.Height = 0.1875F;
			this.txtPlatesReceived.Left = 1.625F;
			this.txtPlatesReceived.Name = "txtPlatesReceived";
			this.txtPlatesReceived.Style = "font-family: \'Roman 12cpi\'; font-size: 8pt; ddo-char-set: 1";
			this.txtPlatesReceived.Text = null;
			this.txtPlatesReceived.Top = 0.4375F;
			this.txtPlatesReceived.Width = 1.375F;
			// 
			// txtPlatesOnHandAtStart
			// 
			this.txtPlatesOnHandAtStart.CanShrink = true;
			this.txtPlatesOnHandAtStart.Height = 0.1875F;
			this.txtPlatesOnHandAtStart.Left = 0.125F;
			this.txtPlatesOnHandAtStart.Name = "txtPlatesOnHandAtStart";
			this.txtPlatesOnHandAtStart.Style = "font-family: \'Roman 12cpi\'; font-size: 8pt; ddo-char-set: 1";
			this.txtPlatesOnHandAtStart.Text = null;
			this.txtPlatesOnHandAtStart.Top = 0.4375F;
			this.txtPlatesOnHandAtStart.Width = 1.375F;
			// 
			// txtPlatesVoided
			// 
			this.txtPlatesVoided.CanShrink = true;
			this.txtPlatesVoided.Height = 0.1875F;
			this.txtPlatesVoided.Left = 4.625F;
			this.txtPlatesVoided.Name = "txtPlatesVoided";
			this.txtPlatesVoided.Style = "font-family: \'Roman 12cpi\'; font-size: 8pt; ddo-char-set: 1";
			this.txtPlatesVoided.Text = null;
			this.txtPlatesVoided.Top = 0.4375F;
			this.txtPlatesVoided.Width = 1.375F;
			// 
			// txtPlatesOnHandAtEnd
			// 
			this.txtPlatesOnHandAtEnd.CanShrink = true;
			this.txtPlatesOnHandAtEnd.Height = 0.1875F;
			this.txtPlatesOnHandAtEnd.Left = 6.125F;
			this.txtPlatesOnHandAtEnd.Name = "txtPlatesOnHandAtEnd";
			this.txtPlatesOnHandAtEnd.Style = "font-family: \'Roman 12cpi\'; font-size: 8pt; ddo-char-set: 1";
			this.txtPlatesOnHandAtEnd.Text = null;
			this.txtPlatesOnHandAtEnd.Top = 0.4375F;
			this.txtPlatesOnHandAtEnd.Width = 1.375F;
			// 
			// rptLTTPlateInventory
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtDescTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOnHand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRecorded)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIssues)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjusted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtItem)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlatesIssued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlatesReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlatesOnHandAtStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlatesVoided)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlatesOnHandAtEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOnHand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRecorded;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIssues;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAdjusted;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEnd;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtItem;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlatesIssued;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlatesReceived;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlatesOnHandAtStart;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlatesVoided;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlatesOnHandAtEnd;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
