//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTownSummary.
	/// </summary>
	public partial class rptTownSummary : BaseSectionReport
	{
		public rptTownSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Town Summary Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptTownSummary_ReportEnd;
		}

        private void RptTownSummary_ReportEnd(object sender, EventArgs e)
        {
			rsTDH.DisposeOf();
            rs.DisposeOf();
            rsTDS.DisposeOf();

		}

        public static rptTownSummary InstancePtr
		{
			get
			{
				return (rptTownSummary)Sys.GetInstance(typeof(rptTownSummary));
			}
		}

		protected rptTownSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTownSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsTDH = new clsDRWrapper();
		//clsDRWrapper rsTemp = new clsDRWrapper();
		string str1 = "";
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rsTDS = new clsDRWrapper();
		string strLine = "";
		bool blnTransitDone;
		bool blnSalesTaxDone;
		bool blnBrstCancerDone;
		int intPageNumber;
		
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            var strHeading = "";

			if (!rsTDH.EndOfFile())
			{
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
				return;
			}
			txtCategory.Text = "";
			txtUnits.Text = "";
			txtDollars.Text = "";
			txtBcategory.Text = "";
			txtBUnits.Text = "";
			txtBDollars.Text = "";
			Field1.Visible = true;
			if (FCConvert.ToString(rsTDH.Get_Fields_String("Heading")) == "SALES TAX - NO FEE")
			{
				rsTDH.MoveNext();
			}
			else if (rsTDH.Get_Fields_String("Heading") == "TRUCK CAMPER")
			{
				rsTDH.MoveNext();
			}
			else if (rsTDH.Get_Fields_String("Heading") == "SALES TAX - PAID")
			{
				if (!blnSalesTaxDone)
				{
					rsTDH.MovePrevious();
					blnSalesTaxDone = true;
				}
				else
				{
					rsTDH.MoveNext();
				}
			}
			
			str1 = "SELECT SUM(Units) as TotalUnits, SUM(Fee) as TotalFee FROM TownDetailSummary WHERE RateGroup = 99999 And SummaryCategory = " + rsTDH.Get_Fields_Int32("CategoryCode") + " GROUP BY SummaryCategory ORDER BY SummaryCategory";
			rsTDS.OpenRecordset(str1);
            strHeading = rsTDH.Get_Fields_String("Heading");
			if (rsTDS.EndOfFile() != true && rsTDS.BeginningOfFile() != true)
			{
				rsTDS.MoveLast();
				rsTDS.MoveFirst();
                if (strHeading.Length < 21)
                {
                    txtCategory.Text = rsTDH.Get_Fields_String("Heading") + Strings.StrDup(20 - FCConvert.ToString(rsTDH.Get_Fields_String("Heading")).Length, ".");
                    txtBcategory.Text = txtCategory.Text;
                }
                else
                {
                    txtCategory.Text = strHeading;
                    txtBcategory.Text = strHeading;
                }				
				txtUnits.Text = Strings.Format(Strings.Format(rsTDS.Get_Fields("TotalUnits"), "#,###"), "@@@@@");
				txtDollars.Text = Strings.Format(Strings.Format(rsTDS.Get_Fields("TotalFee"), "#,###.00"), "@@@@@@@@@@@");				
			}
			else
			{
				if (FCConvert.ToInt32(rsTDH.Get_Fields_Int32("CategoryCode")) != 299)
				{
                    if (strHeading.Length < 21)
                    {
                        txtCategory.Text = rsTDH.Get_Fields_String("Heading") +
                                           Strings.StrDup(
                                               20 - FCConvert.ToString(rsTDH.Get_Fields_String("Heading")).Length, ".");
                        txtBcategory.Text = txtCategory.Text;
                    }
                    else
                    {
                        txtCategory.Text = strHeading;
                        txtBcategory.Text = strHeading;
                    }

                    txtDollars.Text = Strings.Format(Strings.Format(0, "#,###.00"), "@@@@@@@@@@@");
				}
				else
				{
					Field1.Visible = false;
				}
			}
			if (FCConvert.ToString(rsTDH.Get_Fields_String("Heading")) == "TRUCK CAMPER" || FCConvert.ToString(rsTDH.Get_Fields_String("Heading")) == "SALES TAX - PAID")
			{
				// do nothing
			}			
			else
			{
				rsTDH.MoveNext();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rs1 = new clsDRWrapper();
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			string strLevel;
			intPageNumber = 0;
			this.Document.Printer.PrinterName = frmReport.InstancePtr.ReportPrinter;
			blnTransitDone = false;
			blnSalesTaxDone = false;
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}

			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							int lngPK1 = 0;
							MotorVehicle.Statics.strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(MotorVehicle.Statics.strSql);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
			}
			rsTDH.OpenRecordset("SELECT * FROM TownSummaryHeadings ORDER BY CategoryCode");
			if (rsTDH.EndOfFile() != true && rsTDH.BeginningOfFile() != true)
			{
				rsTDH.MoveLast();
				rsTDH.MoveFirst();
			}
			rs1.DisposeOf();
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeading();
			intPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}
	}
}
