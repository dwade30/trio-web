//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptMVRT10Laser.
	/// </summary>
	public partial class rptMVRT10Laser : BaseSectionReport
    {
		public rptMVRT10Laser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "MVRT-10";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMVRT10Laser InstancePtr
		{
			get
			{
				return (rptMVRT10Laser)Sys.GetInstance(typeof(rptMVRT10Laser));
			}
		}

		protected rptMVRT10Laser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMVRT10Laser	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		bool blnFirstRecord;
		bool blnSetPageSize;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// vbPorter upgrade warning: cnt As int	OnWriteFCConvert.ToInt32(
			int cnt;
			if (MotorVehicle.Statics.gboolFromBatchRegistration)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					intCounter += 1;
					if (intCounter >= frmBatchProcessing.InstancePtr.intNumberToPrint)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
			else if (MotorVehicle.Statics.gboolFromReRegBatchRegistration)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					intCounter += 1;
					if (intCounter >= frmLongTermReRegBatch.InstancePtr.intNumberToPrint)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
			else if (MotorVehicle.Statics.gboolFromImport)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					intCounter += 1;
					if (intCounter >= MotorVehicle.Statics.lngRecords)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
			else if (MotorVehicle.Statics.gboolFromPrintQueue)
			{
				for (cnt = intCounter + 1; cnt <= (frmPrintQueue.InstancePtr.vsPrintQueue.Rows - 1); cnt++)
				{
					if (FCConvert.CBool(frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(cnt, frmPrintQueue.InstancePtr.intSelectCol)))
					{
						intCounter = cnt;
						break;
					}
				}
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					if (intCounter != cnt)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Printer tempPrinter = new Printer();
			int x;
			double dblLabelsAdjustment;
			// MsgBox "6"
			if (MotorVehicle.Statics.gboolFromBatchRegistration || MotorVehicle.Statics.gboolFromReRegBatchRegistration || MotorVehicle.Statics.gboolFromPrintQueue || MotorVehicle.Statics.gboolFromImport)
			{
				blnFirstRecord = true;
				blnSetPageSize = true;
				intCounter = 0;
			}
			// dblLabelsAdjustment = Val(GetMVVariable("LaserMVRT10Adjustment"))
			dblLabelsAdjustment = Conversion.Val(MotorVehicle.GetComputerMVSetting("LaserMVRT10Adjustment"));
			this.PageSettings.Margins.Top += FCConvert.ToSingle(dblLabelsAdjustment * 270) / 1440F;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string c = "";
			string MVTyp = "";
			string Info = "";
			string CL1 = "";
			string CL2 = "";
			string PRINTOWNER2 = "";
			Decimal RegFee = 0;
			string Town = "";
			string SD = "";
			string printfile = "";
			// vbPorter upgrade warning: datStartYear As DateTime	OnWrite(string)
			DateTime datStartYear;
			clsDRWrapper rsLong = new clsDRWrapper();
			clsDRWrapper rsDefaults = new clsDRWrapper();
			clsDRWrapper rsLongTermActivityRecord = new clsDRWrapper();
			object strStart = 0;
			clsDRWrapper rsFleet = new clsDRWrapper();
			MotorVehicle.Statics.PrintMVR3 = false;
			fecherFoundation.Information.Err().Clear();

            try
            {
                // On Error GoTo ErrorTag
                fecherFoundation.Information.Err().Clear();
                rsDefaults.OpenRecordset("SELECT * FROM DefaultInfo", "TWMV0000.vb1");
                bool executeCheckAgain2 = false;
                bool firstIf = false;
                bool executeCheckAgain20 = false;
                bool thirdIf = false;

                var frmBatchProcessor = TWMV0000.frmBatchProcessing.InstancePtr;   
                var instancePtrIntStartRow = frmBatchProcessor.intStartRow + intCounter;

                var transactionType = MotorVehicle.Statics.rsFinal.Get_Fields("TransactionType");

                var transactionType_LongReg = rsLong.Get_Fields("TransactionType");

                var getRandomId = FCConvert.ToInt32(FCUtils.Rnd() * 999999) + 1;

                var muniName = fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName);

                if (!MotorVehicle.Statics.gboolFromReRegBatchRegistration && !MotorVehicle.Statics.gboolFromPrintQueue)
                {
                    firstIf = true;

                    if (MotorVehicle.Statics.gboolFromImport)
                    {
                        MotorVehicle.Statics.rsFinal.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngIDs[intCounter]), "TWMV0000.vb1");
                    }

                    if (MotorVehicle.Statics.gboolFromBatchRegistration)
                    {
                        fldVIN.Text = frmBatchProcessor.vsVehicles.TextMatrix(instancePtrIntStartRow, frmBatchProcessor.intVINCol);
                        fldYear.Text = frmBatchProcessor.vsVehicles.TextMatrix(instancePtrIntStartRow, frmBatchProcessor.intYearCol);
                        fldMake.Text = frmBatchProcessor.vsVehicles.TextMatrix(instancePtrIntStartRow, frmBatchProcessor.intMakeCol);
                        fldUnit.Text = frmBatchProcessor.vsVehicles.TextMatrix(instancePtrIntStartRow, frmBatchProcessor.intUnitCol);
                        fldPlate.Text = frmBatchProcessor.vsVehicles.TextMatrix(instancePtrIntStartRow, frmBatchProcessor.intPlateCol);
                        fldStyle.Text = frmBatchProcessor.vsVehicles.TextMatrix(instancePtrIntStartRow, frmBatchProcessor.intStyleCol);
                        fldCTANumber.Text = frmBatchProcessor.vsVehicles.TextMatrix(instancePtrIntStartRow, frmBatchProcessor.intCTACol);
                    }
                    else
                    {
                        fldMake.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
                        fldYear.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));

                        var unitNumber = MotorVehicle.Statics.rsFinal.Get_Fields_String("UnitNumber");

                        if (FCConvert.ToString(unitNumber) != "...." && FCConvert.ToString(unitNumber) != "    ")
                        {
                            fldUnit.Text = unitNumber;
                        }

                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate"))) != "NEW")
                        {
                            fldPlate.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate");
                        }

                        fldStyle.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Style");
                        fldVIN.Text = Strings.Left(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin")), 17);

                        if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber")).Length > 1)
                        {
                            fldCTANumber.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("TitleNumber");
                        }
                    }

                    fldColor1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("color1");

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("color2")) != "NA")
                    {
                        fldColor2.Text = "/" + MotorVehicle.Statics.rsFinal.Get_Fields_String("color2");
                    }

                    // DJW@01122012 - BMV told us to only print 2000 if the vehicle weight was less than or equal to it and if it was more to not print it on the form
                    // DJW@04102013 - TROMV-733 BMV does not want any weight to print on this form ever
                    // DJW@04292013 - TROMV 741 BMV wants seperatenotice to show for 2000 lb trialers
                    fld2000lbsNotice.Visible = false;

                    if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) != true)
                    {
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) != 0 && Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("NetWeight")) <= 2000)
                        {
                            // fldNetWeight.Text = Format(Format(2000, "######"), "@@@@@@")
                            fld2000lbsNotice.Visible = true;
                        }
                    }

                    SetReRegYesNo();

                    fldOwner1.Text = MotorVehicle.GetPartyNameMiddleInitial(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"));

                    fldOwner2.Text = MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1") > 0 ? MotorVehicle.GetPartyNameMiddleInitial(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID2")) : "";

                    fldAddress.Text = !MotorVehicle.Statics.blnSuspended ? MotorVehicle.Statics.rsFinal.Get_Fields_String("Address") : "REGISTRATION SUSPENDED";

                    fldCity.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("City");

                    if (FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("State")) != "CC")
                    {
                        fldState.Text = MotorVehicle.Statics.rsFinal.Get_Fields("State");
                    }

                    fldZip.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Zip");
                    fldResidenceCity.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Residence");
                    fldResidenceState.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceState");
                    fldResidenceCode.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("ResidenceCode");

                    RegFee = SetRegFee();

                    if (FCConvert.ToString(transactionType) == "LPS" || FCConvert.ToString(transactionType) == "NRT" || FCConvert.ToString(transactionType) == "DPR" || FCConvert.ToString(transactionType) == "ECO" || MotorVehicle.Statics.gboolLongRegExtension || MotorVehicle.Statics.gboolFromImport)
                    {
                        SetupLongTermActivityRecord(rsLongTermActivityRecord);

                        if (rsLongTermActivityRecord.EndOfFile() != true && rsLongTermActivityRecord.BeginningOfFile() != true)
                        {
                            if (rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears") > 0)
                            {
                                SetLongRegExtensionDescription(rsLongTermActivityRecord);
                            }
                            else
                            {
                                executeCheckAgain2 = true;
                            }
                        }
                        else
                        {
                            executeCheckAgain2 = true;
                        }
                    }
                    else
                    {
                        if (Information.IsDate(MotorVehicle.Statics.rsFinal.Get_Fields("EffectiveDate")))
                        {
                            datStartYear = GetStartYear();

                            if (MotorVehicle.Statics.RegistrationType == "RRR")
                                GetDescriptionRRR(datStartYear);
                            else
                                GetDescriptionNonRRR();
                        }
                        else
                        {
                            fldDescription.Text = "Start Year: ";
                        }
                    }
                }
                else
                    if (MotorVehicle.Statics.gboolFromReRegBatchRegistration)
                    {
                        rsLong.OpenRecordset("SELECT * FROM Master WHERE ID = " + frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter, frmLongTermReRegBatch.InstancePtr.SelectIDCol), "TWMV0000.vb1");

                        if (rsLong.EndOfFile() != true && rsLong.BeginningOfFile() != true)
                        {
                            rsFleet.OpenRecordset("SELECT c.*, p.FullName as Party1FullName, q.FullName as Party2FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as q ON c.PartyID2 = q.ID WHERE Deleted <> 1 AND Type = 'L' AND FleetNumber = " + rsLong.Get_Fields_Int32("FleetNumber") + " ORDER BY p.FullName");
                            fldPlate.Text = frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter, frmLongTermReRegBatch.InstancePtr.SelectNewPlateCol);
                            fldCTANumber.Text = "";
                            fldMake.Text = rsLong.Get_Fields_String("make");
                            fldYear.Text = rsLong.Get_Fields_String("Year");

                            if (FCConvert.ToString(rsLong.Get_Fields_String("UnitNumber")) != "...." && FCConvert.ToString(rsLong.Get_Fields_String("UnitNumber")) != "    ")
                            {
                                fldUnit.Text = rsLong.Get_Fields_String("UnitNumber");
                            }

                            fldStyle.Text = rsLong.Get_Fields_String("Style");
                            fldVIN.Text = Strings.Left(FCConvert.ToString(rsLong.Get_Fields_String("Vin")), 17);
                            fldColor1.Text = rsLong.Get_Fields_String("color1");

                            if (FCConvert.ToString(rsLong.Get_Fields_String("color2")) != "NA")
                            {
                                fldColor2.Text = "/" + rsLong.Get_Fields_String("color2");
                            }

                            // DJW@01122012 - BMV told us to only print 2000 if the vehicle weight was less than or equal to it and if it was more to not print it on the form
                            // DJW@04102013 - TROMV-733 BMV does not want any weight to print on this form ever
                            // DJW@04292013 - TROMV 741 BMV wants seperatenotice to show for 2000 lb trialers
                            fld2000lbsNotice.Visible = false;

                            if (fecherFoundation.FCUtils.IsNull(rsLong.Get_Fields("NetWeight")) != true)
                            {
                                if (Conversion.Val(rsLong.Get_Fields("NetWeight")) != 0 && Conversion.Val(rsLong.Get_Fields("NetWeight")) <= 2000)
                                {
                                    // fldNetWeight.Text = Format(Format(2000, "######"), "@@@@@@")
                                    fld2000lbsNotice.Visible = true;
                                }
                            }

                            fldReRegYes.Visible = true;
                            fldReRegNo.Visible = false;
                            fldOwner1.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleet.Get_Fields("Party1FullName")));
                            fldOwner2.Text = fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleet.Get_Fields("Party2FullName")));

                            if (!MotorVehicle.Statics.blnSuspended)
                            {
                                fldAddress.Text = rsFleet.Get_Fields_String("Address");
                            }
                            else
                            {
                                fldAddress.Text = "REGISTRATION SUSPENDED";
                            }

                            fldCity.Text = rsFleet.Get_Fields_String("City");

                            if (FCConvert.ToString(rsFleet.Get_Fields("State")) != "CC")
                            {
                                fldState.Text = rsFleet.Get_Fields_String("State");
                            }

                            fldZip.Text = rsFleet.Get_Fields_String("Zip");

                            if (muniName == "MAINE MOTOR TRANSPORT")
                            {
                                fldResidenceCity.Text = "MMTA SERVICES INC. 44004";
                            }
                            else
                                if (muniName == "ACE REGISTRATION SERVICES LLC")
                                {
                                    fldResidenceCity.Text = "ACE REGISTRATION";
                                }
                                else
                                    if (muniName == "COUNTRYWIDE TRAILER")
                                        {
                                            fldResidenceCity.Text = "COUNTRYWIDE TRAILER 44018";
                                        }
                                        else
                                            if (muniName == "AB LEDUE ENTERPRISES")
                                            {
                                                fldResidenceCity.Text = "AB LEDUE ENTERPRISES";
                                            }
                                            else
                                                if (muniName == "MAINE TRAILER")
                                                {
                                                    fldResidenceCity.Text = "MAINE TRAILER ME 44005";
                                                }
                                                else
                                                    if (muniName == "HASKELL REGISTRATION")
                                                    {
                                                        fldResidenceCity.Text = "HASKELL REGISTRATION 44002";
                                                    }
                                                    else
                                                    {
                                                        fldResidenceCity.Text = rsLong.Get_Fields_String("Residence");
                                                    }

                            if (muniName == "ACE REGISTRATION SERVICES LLC")
                            {
                                fldResidenceState.Text = "ME";
                            }
                            else
                                if (muniName == "AB LEDUE ENTERPRISES")
                                {
                                    fldResidenceState.Text = "ME";
                                }
                                else
                                {
                                    fldResidenceState.Text = rsLong.Get_Fields_String("ResidenceState");
                                }

                            if (muniName == "ACE REGISTRATION SERVICES LLC")
                            {
                                fldResidenceCode.Text = "44021";
                            }
                            else
                                if (muniName == "AB LEDUE ENTERPRISES")
                                {
                                    fldResidenceCode.Text = "44003";
                                }
                                else
                                {
                                    fldResidenceCode.Text = rsLong.Get_Fields_String("ResidenceCode");
                                }

                            // fldRegFee.Text = Format(Format(.vsRegLength.TextMatrix(.intStartRow + intCounter, .SelectTotalCol) - curAgentFee, "#,##0.00"), "@@@@@@@")
                            // fldRegCredit.Text = Format(Format(0, "###0.00"), "@@@@@@@")
                            // fldFees.Text = Format(Format(.vsRegLength.TextMatrix(.intStartRow + intCounter, .SelectTotalCol) - curAgentFee, "#,##0.00"), "@@@@@@@")
                            // datStartYear = "3/1/" & Year(.vsRegLength.TextMatrix(.intStartRow + intCounter, .SelectCurrentExpireDateCol))
                            if (DateTime.Today.Month >= 10)
                            {
                                datStartYear = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(DateTime.Today.Year + 1));
                            }
                            else
                            {
                                datStartYear = FCConvert.ToDateTime("3/1/" + FCConvert.ToString(DateTime.Today.Year));
                            }

                            fldTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                            fldDescription.Text = "Start Year: " + Strings.Right(Strings.Format(datStartYear, "MM/dd/yyyy"), 4);
                            fldOldPlate.Text = frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter, frmLongTermReRegBatch.InstancePtr.SelectPlateCol);
                            lblTransaction.Text = "";
                            fldExpires.Text = Strings.Format(frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter, frmLongTermReRegBatch.InstancePtr.SelectExpiresCol), "MM/dd/yyyy");

                            if (muniName == "MAINE MOTOR TRANSPORT" || muniName == "HASKELL REGISTRATION" || muniName == "ACE REGISTRATION SERVICES LLC" || muniName == "MAINE TRAILER")
                            {
                                frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter, frmLongTermReRegBatch.InstancePtr.SelectUniqueIdentifierCol, FCConvert.ToString(getRandomId));
                                fldValidationLine1.Text = "LTT" + " " + Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt");
                                fldValidationLine1.Text = fldValidationLine1.Text + " " + rsDefaults.Get_Fields_String("ResidenceCode") + "#" + Strings.Format(frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter, frmLongTermReRegBatch.InstancePtr.SelectUniqueIdentifierCol), "000000") + " " + Strings.Format(FCConvert.ToDecimal(frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter, frmLongTermReRegBatch.InstancePtr.SelectTotalCol)) - frmLongTermReRegBatch.InstancePtr.curAgentFee, "#,##0.00") + " " + MotorVehicle.Statics.OpID;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Error printing MVR-10.  Can't find vehicle with key " + frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter, frmLongTermReRegBatch.InstancePtr.SelectVINCol), "Unable to Find Vehicle", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            MotorVehicle.Statics.PrintMVR3 = false;
                            rptMVRT10.InstancePtr.Cancel();
                            this.Close();
                        }
                    }
                    else
                    {
                        rsLong.OpenRecordset("SELECT * FROM Master WHERE ID = " + frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter, frmPrintQueue.InstancePtr.intNumberCol), "TWMV0000.vb1");

                        if (rsLong.EndOfFile() != true && rsLong.BeginningOfFile() != true)
                        {
                            thirdIf = true;
                            fldPlate.Text = frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter, frmPrintQueue.InstancePtr.intPlateCol);

                            if (FCConvert.ToString(rsLong.Get_Fields_String("TitleNumber"))
                                         .Length
                                > 1)
                            {
                                fldCTANumber.Text = rsLong.Get_Fields_String("TitleNumber");
                            }
                            else
                            {
                                fldCTANumber.Text = "";
                            }

                            fldMake.Text = rsLong.Get_Fields_String("make");
                            fldYear.Text = rsLong.Get_Fields_String("Year");

                            if (FCConvert.ToString(rsLong.Get_Fields_String("UnitNumber")) != "...." && FCConvert.ToString(rsLong.Get_Fields_String("UnitNumber")) != "    ")
                            {
                                fldUnit.Text = rsLong.Get_Fields_String("UnitNumber");
                            }
                            else
                            {
                                fldUnit.Text = "";
                            }

                            fldStyle.Text = rsLong.Get_Fields_String("Style");
                            fldVIN.Text = Strings.Left(FCConvert.ToString(rsLong.Get_Fields_String("Vin")), 17);
                            fldColor1.Text = rsLong.Get_Fields_String("color1");

                            if (FCConvert.ToString(rsLong.Get_Fields_String("color2")) != "NA")
                            {
                                fldColor2.Text = "/" + rsLong.Get_Fields_String("color2");
                            }
                            else
                            {
                                fldColor2.Text = "";
                            }

                            // DJW@01122012 - BMV told us to only print 2000 if the vehicle weight was less than or equal to it and if it was more to not print it on the form
                            // DJW@04102013 - TROMV-733 BMV does not want any weight to print on this form ever
                            // DJW@04292013 - TROMV 741 BMV wants seperatenotice to show for 2000 lb trialers
                            fld2000lbsNotice.Visible = false;

                            if (fecherFoundation.FCUtils.IsNull(rsLong.Get_Fields("NetWeight")) != true)
                            {
                                if (Conversion.Val(rsLong.Get_Fields("NetWeight")) != 0 && Conversion.Val(rsLong.Get_Fields("NetWeight")) <= 2000)
                                {
                                    // fldNetWeight.Text = Format(Format(2000, "######"), "@@@@@@")
                                    fld2000lbsNotice.Visible = true;
                                }
                            }

                            if (FCConvert.ToString(transactionType_LongReg) == "RRR")
                            {
                                fldReRegYes.Visible = true;
                                fldReRegNo.Visible = false;
                            }
                            else
                            {
                                fldReRegYes.Visible = false;
                                fldReRegNo.Visible = true;
                            }

                            fldOwner1.Text = MotorVehicle.GetPartyNameMiddleInitial(rsLong.Get_Fields_Int32("PartyID1"));

                            if (rsLong.Get_Fields_Int32("PartyID1") > 0)
                            {
                                fldOwner2.Text = MotorVehicle.GetPartyNameMiddleInitial(rsLong.Get_Fields_Int32("PartyID2"));
                            }
                            else
                            {
                                fldOwner2.Text = "";
                            }

                            if (!MotorVehicle.Statics.blnSuspended)
                            {
                                fldAddress.Text = rsLong.Get_Fields_String("Address");
                            }
                            else
                            {
                                fldAddress.Text = "REGISTRATION SUSPENDED";
                            }

                            fldCity.Text = rsLong.Get_Fields_String("City");

                            if (FCConvert.ToString(rsLong.Get_Fields("State")) != "CC")
                            {
                                fldState.Text = rsLong.Get_Fields_String("State");
                            }
                            else
                            {
                                fldState.Text = "";
                            }

                            fldZip.Text = rsLong.Get_Fields_String("Zip");
                            fldResidenceCity.Text = rsLong.Get_Fields_String("Residence");
                            fldResidenceState.Text = rsLong.Get_Fields_String("ResidenceState");
                            fldResidenceCode.Text = rsLong.Get_Fields_String("ResidenceCode");

                            if (FCConvert.ToString(transactionType_LongReg) == "DPR")
                            {
                                RegFee = FCConvert.ToDecimal(rsLong.Get_Fields_Decimal("DuplicateRegistrationFee"));
                            }
                            else
                                if (transactionType_LongReg == "LPS")
                                {
                                    RegFee = FCConvert.ToDecimal(rsLong.Get_Fields_Decimal("ReplacementFee"));
                                }
                                else
                                    if (transactionType_LongReg == "ECO")
                                    {
                                        RegFee = 0;
                                    }
                                    else
                                    {
                                        if (fecherFoundation.FCUtils.IsNull(rsLong.Get_Fields_Decimal("RegRateCharge")) == true) rsLong.Set_Fields("RegRateCharge", 0);
                                        if (rsLong.IsFieldNull("RegRateCharge")) rsLong.Set_Fields("RegRateCharge", 0);
                                        if (fecherFoundation.FCUtils.IsNull(rsLong.Get_Fields_Decimal("TransferCreditUsed")) == true) rsLong.Set_Fields("TransferCreditUsed", 0);
                                        if (rsLong.IsFieldNull("TransferCreditUsed")) rsLong.Set_Fields("TransferCreditUsed", 0);
                                        if (fecherFoundation.FCUtils.IsNull(rsLong.Get_Fields("TransferFee")) == true) rsLong.Set_Fields("TransferFee", 0);
                                        if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsLong.Get_Fields("TransferFee"))) == "") rsLong.Set_Fields("TransferFee", 0);
                                        RegFee = FCConvert.ToDecimal(rsLong.Get_Fields_Decimal("RegRateCharge") - rsLong.Get_Fields_Decimal("TransferCreditUsed"));
                                        if (RegFee < 0) RegFee = 0;
                                        RegFee += Conversion.Val(rsLong.Get_Fields("TransferFee"));
                                    }

                            if (FCConvert.ToString(transactionType_LongReg) == "LPS" || FCConvert.ToString(transactionType_LongReg) == "NRT" || FCConvert.ToString(transactionType_LongReg) == "DPR" || FCConvert.ToString(transactionType_LongReg) == "ECO")
                            {
                                rsLongTermActivityRecord.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + rsLong.Get_Fields("LongTermTrailerRegKey"));

                                if (rsLongTermActivityRecord.EndOfFile() != true && rsLongTermActivityRecord.BeginningOfFile() != true)
                                {
                                    if (rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears") > 0)
                                    {
                                        fldDescription.Text = "Start Year: "
                                                              + FCConvert.ToString(rsLongTermActivityRecord.Get_Fields_DateTime("ExpireDate")
                                                                                                           .Year
                                                                                   - rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears"));
                                    }
                                    else
                                    {
                                        executeCheckAgain20 = true;
                                    }
                                }
                                else
                                {
                                    executeCheckAgain20 = true;
                                }
                            }
                            else
                            {
                                if (Information.IsDate(rsLong.Get_Fields("EffectiveDate")))
                                {
                                    if (FCConvert.ToInt32(rsLong.Get_Fields_DateTime("EffectiveDate")
                                                                .Month)
                                        >= 10)
                                    {
                                        datStartYear = FCConvert.ToDateTime("3/1/"
                                                                            + FCConvert.ToString(FCConvert.ToInt32(rsLong.Get_Fields_DateTime("EffectiveDate")
                                                                                                                         .Year)
                                                                                                 + 1));
                                    }
                                    else
                                    {
                                        datStartYear = FCConvert.ToDateTime("3/1/"
                                                                            + FCConvert.ToString(rsLong.Get_Fields_DateTime("EffectiveDate")
                                                                                                       .Year));
                                    }

                                    if (FCConvert.ToString(transactionType_LongReg) == "RRR")
                                    {
                                        rsLongTermActivityRecord.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + rsLong.Get_Fields_Int32("LongTermTrailerRegID"));

                                        if (rsLongTermActivityRecord.EndOfFile() != true && rsLongTermActivityRecord.BeginningOfFile() != true)
                                        {
                                            if (rsLongTermActivityRecord.Get_Fields_DateTime("ExpireDate")
                                                                        .Year
                                                - rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears")
                                                == datStartYear.Year)
                                            {
                                                fldDescription.Text = "Start Year: " + Strings.Right(Strings.Format(datStartYear, "MM/dd/yyyy"), 4);
                                            }
                                            else
                                            {
                                                fldDescription.Text = "Start Year: " + Strings.Right(Strings.Format(DateTime.Today, "MM/dd/yyyy"), 4);
                                            }
                                        }
                                        else
                                        {
                                            fldDescription.Text = "Start Year: " + Strings.Right(Strings.Format(datStartYear, "MM/dd/yyyy"), 4);
                                        }
                                    }
                                    else
                                        if (transactionType_LongReg == "NRR")
                                        {
                                            fldDescription.Text = "Start Year: " + Strings.Right(Strings.Format(rsLong.Get_Fields_DateTime("EffectiveDate"), "MM/dd/yyyy"), 4);
                                        }
                                        else
                                        {
                                            fldDescription.Text = "Start Year: " + Strings.Right(Strings.Format(DateTime.Today, "MM/dd/yyyy"), 4);
                                        }
                                }
                                else
                                {
                                    fldDescription.Text = "Start Year: ";
                                }
                            }

                            goto CheckAgain20;
                        }
                        else
                        {
                            MessageBox.Show("Error printing MVR-10.  Can't find vehicle with key " + frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter, frmPrintQueue.InstancePtr.intNumberCol), "Unable to Find Vehicle", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            MotorVehicle.Statics.PrintMVR3 = false;
                            rptMVRT10.InstancePtr.Cancel();
                            this.Close();
                        }
                    }

                CheckAgain2: ;

                if (executeCheckAgain2)
                {
                    frmInput.InstancePtr.Init(ref strStart, "Enter Start Year", "Please enter the start year to be printed on the registration.");

                    if (!Information.IsNumeric(strStart) || FCConvert.ToInt32(strStart) <= 0)
                    {
                        MessageBox.Show("You must enter a numeric value for the start year.", "Invalid Start Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        executeCheckAgain2 = true;

                        goto CheckAgain2;
                    }
                    else
                    {
                        MotorVehicle.Statics.glngNumberOfYears = FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Year) - FCConvert.ToInt32(strStart);
                        fldDescription.Text = "Start Year: " + FCConvert.ToString(strStart);
                    }

                    executeCheckAgain2 = false;
                    firstIf = true;
                }

                if (firstIf)
                {
                    fldTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");

                    var infoMessage = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_String("InfoMessage")));

                    if (MotorVehicle.Statics.gboolFromImport)
                    {
                        lblTransaction.Text = infoMessage;
                        fldOldPlate.Text = "";
                    }
                    else
                    {
                        var oldPlate = MotorVehicle.Statics.rsFinal.Get_Fields_String("OldPlate");

                        switch (transactionType)
                        {
                            case "DPR":
                                lblTransaction.Text = "DUPLICATE";
                                fldOldPlate.Text = "";

                                break;
                            case "ECO":
                                lblTransaction.Text = infoMessage;
                                fldOldPlate.Text = "";

                                break;
                            case "LPS":
                                lblTransaction.Text = "LOST PLATE";
                                fldOldPlate.Text = oldPlate;

                                break;
                            case "RRR":
                                lblTransaction.Text = "";
                                fldOldPlate.Text = oldPlate;

                                break;
                            case "NRT":
                                lblTransaction.Text = "TRANSFER";
                                fldOldPlate.Text = "";

                                break;
                            default:
                                lblTransaction.Text = "";
                                fldOldPlate.Text = "";

                                break;
                        }
                    }

                    fldExpires.Text = Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");

                    if (isSpecialMuni(muniName))
                    {
                        if (Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("LongTermTrailerRegUniqueIdentifier")) == 0)
                        {
                            SetupLongTermUniqueId(getRandomId);
                        }

                        fldValidationLine1.Text = "LTT";

                        if (MotorVehicle.Statics.gboolFromImport)
                        {
                            fldValidationLine1.Text = "CORRECTION";
                        }
                        else
                        {
                            switch (transactionType)
                            {
                                case "DPR":
                                    fldValidationLine1.Text = "DUPLICATE";

                                    break;
                                case "ECO":
                                    fldValidationLine1.Text = "CORRECTION";

                                    break;
                                case "LPS":
                                    fldValidationLine1.Text = "LOST PLATE";

                                    break;
                                case "NRT":
                                    fldValidationLine1.Text = "TRANSFER";

                                    break;
                                default:
                                    fldValidationLine1.Text = "LTT";

                                    break;
                            }
                        }

                        fldValidationLine1.Text = fldValidationLine1.Text + " " + Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt");
                        fldValidationLine1.Text = fldValidationLine1.Text + " " + rsDefaults.Get_Fields_String("ResidenceCode") + "#" + Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("LongTermTrailerRegUniqueIdentifier"), "000000") + " " + Strings.Format(RegFee, "#,##0.00") + " " + MotorVehicle.Statics.OpID;
                    }

                    firstIf = false;
                }

                CheckAgain20: ;

                if (executeCheckAgain20)
                {
                    frmInput.InstancePtr.Init(ref strStart, "Enter Start Year", "Please enter the start year to be printed on the registration.");

                    if (!Information.IsNumeric(strStart) || FCConvert.ToInt32(strStart) <= 0)
                    {
                        MessageBox.Show("You must enter a numeric value for the start year.", "Invalid Start Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        executeCheckAgain2 = true;

                        goto CheckAgain2;
                    }

                    MotorVehicle.Statics.glngNumberOfYears = FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate").Year- FCConvert.ToInt32(strStart));
                    fldDescription.Text = "Start Year: " + FCConvert.ToString(strStart);

                    executeCheckAgain20 = false;
                    thirdIf = true;
                }

                if (thirdIf)
                {
                    fldTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");

                    if (FCConvert.ToString(transactionType_LongReg) == "DPR")
                    {
                        lblTransaction.Text = "DUPLICATE";
                        fldOldPlate.Text = "";
                    }
                    else
                    {
                        switch (transactionType_LongReg)
                        {
                            case "ECO":
                                lblTransaction.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLong.Get_Fields_String("InfoMessage")));
                                fldOldPlate.Text = "";

                                break;
                            case "LPS":
                                lblTransaction.Text = "LOST PLATE";
                                fldOldPlate.Text = rsLong.Get_Fields_String("OldPlate");

                                break;
                            case "RRR":
                                fldOldPlate.Text = rsLong.Get_Fields_String("OldPlate");
                                lblTransaction.Text = "";

                                break;
                            case "NRT":
                                lblTransaction.Text = "TRANSFER";
                                fldOldPlate.Text = "";

                                break;
                            default:
                                lblTransaction.Text = "";
                                fldOldPlate.Text = "";

                                break;
                        }
                    }

                    fldExpires.Text = Strings.Format(rsLong.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");

                    if (isSpecialMuni(muniName))
                    {
                        if (Conversion.Val(frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter, frmPrintQueue.InstancePtr.intUniqueIdentifierCol)) == 0)
                        {
                            frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter, frmPrintQueue.InstancePtr.intUniqueIdentifierCol, FCConvert.ToString(getRandomId));
                        }

                        fldValidationLine1.Text = "LTT";

                        switch (transactionType_LongReg)
                        {
                            case "DPR":
                                fldValidationLine1.Text = "DUPLICATE";

                                break;
                            case "ECO":
                                fldValidationLine1.Text = "CORRECTION";

                                break;
                            case "LPS":
                                fldValidationLine1.Text = "LOST PLATE";

                                break;
                            case "NRT":
                                fldValidationLine1.Text = "TRANSFER";

                                break;
                            default:
                                fldValidationLine1.Text = "LTT";

                                break;
                        }

                        fldValidationLine1.Text = fldValidationLine1.Text + " " + Strings.Format(DateTime.Now, "MM/dd/yyyy hh:mm tt");
                        fldValidationLine1.Text = fldValidationLine1.Text + " " + rsDefaults.Get_Fields_String("ResidenceCode") + "#" + Strings.Format(frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter, frmPrintQueue.InstancePtr.intUniqueIdentifierCol), "000000") + " " + Strings.Format(RegFee, "#,##0.00") + " " + MotorVehicle.Statics.OpID;
                    }

                    thirdIf = false;
                }

                fldVINForm2.Text = fldVIN.Text;
                fldYearForm2.Text = fldYear.Text;
                fldMakeForm2.Text = fldMake.Text;
                fldUnitForm2.Text = fldUnit.Text;
                fldPlateForm2.Text = fldPlate.Text;
                fldStyleForm2.Text = fldStyle.Text;
                fldCTANumberForm2.Text = fldCTANumber.Text;
                fldColor1Form2.Text = fldColor1.Text;
                fldColor2Form2.Text = fldColor2.Text;
                fld2000lbsNoticeForm2.Visible = fld2000lbsNotice.Visible;
                fldReRegYesForm2.Visible = fldReRegYes.Visible;
                fldReRegNoForm2.Visible = fldReRegNo.Visible;
                fldOwner1Form2.Text = fldOwner1.Text;
                fldOwner2Form2.Text = fldOwner2.Text;
                fldAddressForm2.Text = fldAddress.Text;
                fldCityForm2.Text = fldCity.Text;
                fldStateForm2.Text = fldState.Text;
                fldZipForm2.Text = fldZip.Text;
                fldResidenceCityForm2.Text = fldResidenceCity.Text;
                fldResidenceStateForm2.Text = fldResidenceState.Text;
                fldResidenceCodeForm2.Text = fldResidenceCode.Text;
                fldDescriptionForm2.Text = fldDescription.Text;
                fldTransactionDateForm2.Text = fldTransactionDate.Text;
                lblTransactionForm2.Text = lblTransaction.Text;
                fldOldPlateForm2.Text = fldOldPlate.Text;
                fldExpiresForm2.Text = fldExpires.Text;
                fldValidationLine1Form2.Text = fldValidationLine1.Text;
                fldVINForm3.Text = fldVIN.Text;
                fldYearForm3.Text = fldYear.Text;
                fldMakeForm3.Text = fldMake.Text;
                fldUnitForm3.Text = fldUnit.Text;
                fldPlateForm3.Text = fldPlate.Text;
                fldStyleForm3.Text = fldStyle.Text;
                fldCTANumberForm3.Text = fldCTANumber.Text;
                fldColor1Form3.Text = fldColor1.Text;
                fldColor2Form3.Text = fldColor2.Text;
                fld2000lbsNoticeForm3.Visible = fld2000lbsNotice.Visible;
                fldReRegYesForm3.Visible = fldReRegYes.Visible;
                fldReRegNoForm3.Visible = fldReRegNo.Visible;
                fldOwner1Form3.Text = fldOwner1.Text;
                fldOwner2Form3.Text = fldOwner2.Text;
                fldAddressForm3.Text = fldAddress.Text;
                fldCityForm3.Text = fldCity.Text;
                fldStateForm3.Text = fldState.Text;
                fldZipForm3.Text = fldZip.Text;
                fldResidenceCityForm3.Text = fldResidenceCity.Text;
                fldResidenceStateForm3.Text = fldResidenceState.Text;
                fldResidenceCodeForm3.Text = fldResidenceCode.Text;
                fldDescriptionForm3.Text = fldDescription.Text;
                fldTransactionDateForm3.Text = fldTransactionDate.Text;
                lblTransactionForm3.Text = lblTransaction.Text;
                fldOldPlateForm3.Text = fldOldPlate.Text;
                fldExpiresForm3.Text = fldExpires.Text;
                fldValidationLine1Form3.Text = fldValidationLine1.Text;
                bcdPlate.Text = fldPlate.Text.Replace("-", "");
                fecherFoundation.Information.Err().Clear();

                if (fecherFoundation.Information.Err().Number== 0)
                {
                    MotorVehicle.Statics.PrintMVR3 = true;

                    if (MotorVehicle.Statics.gboolFromBatchRegistration)
                    {
                        frmBatchProcessor.vsVehicles.TextMatrix(instancePtrIntStartRow, frmBatchProcessor.intPrintedCol, FCConvert.ToString(true));
                    }
                    else
                        if (MotorVehicle.Statics.gboolFromReRegBatchRegistration)
                        {
                            frmLongTermReRegBatch.InstancePtr.vsRegLength.TextMatrix(frmLongTermReRegBatch.InstancePtr.intStartRow + intCounter, frmLongTermReRegBatch.InstancePtr.SelectPrintedCol, FCConvert.ToString(true));
                        }
                        else
                            if (MotorVehicle.Statics.gboolFromPrintQueue)
                            {
                                frmPrintQueue.InstancePtr.vsPrintQueue.TextMatrix(intCounter, frmPrintQueue.InstancePtr.intPrintedStatusCol, "Yes");
                            }
                }
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                if (fecherFoundation.Information.Err(ex).Number!= 0)
                {
                    MotorVehicle.PrinterError("MVR10 Printer");
                    MotorVehicle.Statics.PrintMVR3 = false;
                    this.Cancel();
                    this.Close();
                }
            }
            finally
            {
                rsFleet.DisposeOf();
                rsDefaults.DisposeOf();
                rsLong.DisposeOf();
                rsLongTermActivityRecord.DisposeOf();
            }
		}

        private static bool isSpecialMuni(string muniName)
        {
            return muniName == "MAINE MOTOR TRANSPORT" 
                   || muniName == "HASKELL REGISTRATION" 
                   || muniName == "ACE REGISTRATION SERVICES LLC" 
                   || muniName == "MAINE TRAILER";
        }

        private static void SetupLongTermUniqueId(int getRandomId)
        {
            if (MotorVehicle.Statics.gboolFromImport)
            {
                MotorVehicle.Statics.rsFinal.Edit();
                MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegUniqueIdentifier", getRandomId);
                MotorVehicle.Statics.rsFinal.Update();
            }
            else
            {
                MotorVehicle.Statics.rsFinal.Set_Fields("LongTermTrailerRegUniqueIdentifier", getRandomId);
            }
        }

        private void GetDescriptionNonRRR()
        {
            if (MotorVehicle.Statics.RegistrationType == "NRR")
            {
                fldDescription.Text = "Start Year: " + Strings.Right(Strings.Format(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate"), "MM/dd/yyyy"), 4);
            }
            else
            {
                fldDescription.Text = "Start Year: " + Strings.Right(Strings.Format(DateTime.Today, "MM/dd/yyyy"), 4);
            }
        }

        private void GetDescriptionRRR(DateTime datStartYear)
        {
            if (frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text == "YES - Expires next year")
            {
                fldDescription.Text = "Start Year: " + Strings.Right(Strings.Format(datStartYear, "MM/dd/yyyy"), 4);
            }
            else
            {
                fldDescription.Text = "Start Year: " + Strings.Right(Strings.Format(DateTime.Today, "MM/dd/yyyy"), 4);
            }
        }

        private static DateTime GetStartYear()
        {
            DateTime datStartYear;

            if (FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate")
                                              .Month)
                >= 10)
            {
                datStartYear = FCConvert.ToDateTime("3/1/"
                                                    + FCConvert.ToString(FCConvert.ToInt32(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate")
                                                                                                       .Year
                                                                                           + 1)));
            }
            else
            {
                datStartYear = FCConvert.ToDateTime("3/1/"
                                                    + FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("EffectiveDate")
                                                                                     .Year));
            }

            return datStartYear;
        }

        private void SetLongRegExtensionDescription(clsDRWrapper rsLongTermActivityRecord)
        {
            if (MotorVehicle.Statics.gboolLongRegExtension)
            {
                fldDescription.Text = "Start Year: "
                                      + FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields_DateTime("ExpireDate")
                                                                       .Year
                                                           - rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears")
                                                           - (frmDataInputLongTermTrailers.InstancePtr.cboLength.ItemData(frmDataInputLongTermTrailers.InstancePtr.cboLength.SelectedIndex)));
            }
            else
            {
                fldDescription.Text = "Start Year: "
                                      + FCConvert.ToString(rsLongTermActivityRecord.Get_Fields_DateTime("ExpireDate")
                                                                                   .Year
                                                           - rsLongTermActivityRecord.Get_Fields_Int32("NumberOfYears"));
            }
        }

        private static void SetupLongTermActivityRecord(clsDRWrapper rsLongTermActivityRecord)
        {
            if (MotorVehicle.Statics.gboolFromImport)
            {
                rsLongTermActivityRecord.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + MotorVehicle.Statics.rsFinal.Get_Fields_Int32("LongTermTrailerRegID"));
            }
            else
            {
                rsLongTermActivityRecord.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE ID = " + MotorVehicle.Statics.rsFinalCompare.Get_Fields_Int32("LongTermTrailerRegID"));
            }
        }

        private static decimal SetRegFee()
        {
            decimal RegFee;

            if (MotorVehicle.Statics.RegistrationType == "DUPREG")
            {
                RegFee = FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("DuplicateRegistrationFee"));
            }
            else
                if (MotorVehicle.Statics.RegistrationType == "LOST")
                {
                    RegFee = FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("ReplacementFee"));
                }
                else
                    if (MotorVehicle.Statics.RegistrationType == "CORR")
                    {
                        RegFee = 0;
                    }
                    else
                    {
                        if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge")) == true) MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", 0);
                        if (MotorVehicle.Statics.rsFinal.IsFieldNull("RegRateCharge")) MotorVehicle.Statics.rsFinal.Set_Fields("RegRateCharge", 0);
                        if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed")) == true) MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed", 0);
                        if (MotorVehicle.Statics.rsFinal.IsFieldNull("TransferCreditUsed")) MotorVehicle.Statics.rsFinal.Set_Fields("TransferCreditUsed", 0);
                        if (fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee")) == true) MotorVehicle.Statics.rsFinal.Set_Fields("TransferFee", 0);
                        RegFee = FCConvert.ToDecimal(MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("RegRateCharge") - MotorVehicle.Statics.rsFinal.Get_Fields_Decimal("TransferCreditUsed"));
                        if (RegFee < 0) RegFee = 0;
                        RegFee += Conversion.Val(MotorVehicle.Statics.rsFinal.Get_Fields("TransferFee"));
                    }

            return RegFee;
        }

        private void SetReRegYesNo()
        {
            if (MotorVehicle.Statics.RegistrationType == "DUPREG")
            {
                if (FCConvert.ToString(MotorVehicle.Statics.rsFinalCompare.Get_Fields("TransactionType")) == "RRR")
                {
                    fldReRegYes.Visible = true;
                    fldReRegNo.Visible = false;
                }
                else
                {
                    fldReRegYes.Visible = false;
                    fldReRegNo.Visible = true;
                }
            }
            else
                if (MotorVehicle.Statics.RegistrationType == "LOST")
                {
                    fldReRegYes.Visible = false;
                    fldReRegNo.Visible = true;
                }
                else
                {
                    if (MotorVehicle.Statics.gboolFromImport)
                    {
                        fldReRegYes.Visible = false;
                        fldReRegNo.Visible = true;
                    }
                    else
                    {
                        if (frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text == "YES - Expires this year" || frmDataInputLongTermTrailers.InstancePtr.cmbNoMaineReReg.Text == "YES - Expires next year")
                        {
                            fldReRegYes.Visible = true;
                            fldReRegNo.Visible = false;
                        }
                        else
                        {
                            fldReRegYes.Visible = false;
                            fldReRegNo.Visible = true;
                        }
                    }
                }
        }
    }
}
