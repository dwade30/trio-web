//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using SharedApplication.Extensions;
using SharedApplication.MotorVehicle;
using SharedApplication.MotorVehicle.Commands;
using SharedApplication.MotorVehicle.Enums;
using SharedApplication.MotorVehicle.Interfaces;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmInventoryAdjust : BaseForm
	{
		public frmInventoryAdjust()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;

            base.Cancelled += Adjustment_Cancelled;
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmInventoryAdjust InstancePtr
		{
			get
			{
				return (frmInventoryAdjust)Sys.GetInstance(typeof(frmInventoryAdjust));
			}
		}

		protected frmInventoryAdjust _InstancePtr = null;
		//=========================================================
		string High = "";
		string Low = "";
		string InventoryType = "";
		string InventoryMY = "";
		string InventorySD = "";
		string InventoryClass = "";
        private IInventoryService inventoryService;
        private bool cancelled = false;

		private bool ValidateInitials(string initials)
        {
            clsDRWrapper rsIDCheck = new clsDRWrapper();

			if (initials == "")
            {
                MessageBox.Show("You must enter your operator ID before you may continue.", "Invalid Operator ID", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return false;
            }

            rsIDCheck.OpenRecordset("SELECT * FROM Operators WHERE Code = '" + initials.ToUpper() + "'", "SystemSettings");
            if (rsIDCheck.EndOfFile() != true && rsIDCheck.BeginningOfFile() != true)
            {
                if (FCConvert.ToInt32(rsIDCheck.Get_Fields_String("Level")) == 3)
                {
                    MessageBox.Show("The Operator ID you entered is not valid to perform inventory additions/removals", "Invalid Operator ID", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            else if (initials != "988")
            {
                MessageBox.Show("The Operator ID you entered is not valid to perform inventory additions/removals", "Invalid Operator ID", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			string strMessage = "";
			DialogResult intAns = DialogResult.None;
            var isRemoving = false;

            Application.ShowLoader = false;
			Application.Update(this);

            if (ValidateInitials(txtInitials.Text.Trim()))
            {
                MotorVehicle.Statics.UserID = txtInitials.Text;
			}
            else
            {
				txtInitials.Focus();
				return;
			}

			if (!cmbAdd.Enabled)
			{
				if (ChangeGroup())
				{
                    Close();
				}
				return;
			}

			if (txtLow.Text.Length == 0)
				return;
			if (txtLow.Text.Length != 0 && txtHigh.Text.Length == 0)
			{
				// vbPorter upgrade warning: answer As string	OnWrite(DialogResult)
				DialogResult answer = DialogResult.None;
				answer = MessageBox.Show("Would you like to make the High number equal to the Low number?", "Is there only one item?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (answer == DialogResult.Yes)
				{
					txtHigh.Text = txtLow.Text;
				}
				else
				{
					txtLow.Text = "";
					txtHigh.Text = "";
					return;
				}
			}

			if (MotorVehicle.Statics.ListType != "P" || MotorVehicle.Statics.ListSubType != "LT")
			{
				while (Strings.Left(txtLow.Text, 1) == "0")
				{
					txtLow.Text = Strings.Right(txtLow.Text, txtLow.Text.Length - 1);
				}
				while (Strings.Left(txtHigh.Text, 1) == "0")
				{
					txtHigh.Text = Strings.Right(txtHigh.Text, txtHigh.Text.Length - 1);
				}
			}
			if (cmbAdd.Text == "Remove from Inventory")
			{
				if (fecherFoundation.Strings.Trim(txtComment.Text) == "")
				{
					MessageBox.Show("You must enter a reason before you may continue with this Inventory Removal.", "Reason Needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtComment.Focus();
					return;
				}

                isRemoving = true;
            }
			// 
			string strPlate;
			if (txtHigh.Text != "")
			{
				High = txtHigh.Text;
			}
			if (txtLow.Text != "")
			{
				Low = txtLow.Text;
			}
			// The strPlate is built to pass the High and Low number to the GetPlateRange function
			// it will return 2 prfixes, 2 suffixes and 2 numbers ( low,high)
			High += Strings.StrDup(8 - fecherFoundation.Strings.Trim(High).Length, " ");
			Low += Strings.StrDup(8 - fecherFoundation.Strings.Trim(Low).Length, " ");
			strPlate = Low + High;
			MotorVehicle.GetPlateRange(strPlate);
			// 
			if (MotorVehicle.Statics.Prefix1.Replace("0", "") != MotorVehicle.Statics.Prefix2.Replace("0", "") || MotorVehicle.Statics.Suffix1 != MotorVehicle.Statics.Suffix2)
			{
				MessageBox.Show("The prefixes or suffixes do not match. Please check your numbers and try again.", "Number Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (Conversion.Val(MotorVehicle.Statics.Numeric1) > Conversion.Val(MotorVehicle.Statics.Numeric2))
			{
				MessageBox.Show("The low number in the range is larger than the high number.  Please check your numbers and try again.", "Number Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			
			if (cmbAdd.Text == "Remove from Inventory")
			{
				intAns = MessageBox.Show("You are about to remove " + FCConvert.ToString((FCConvert.ToInt32(MotorVehicle.Statics.Numeric2) - (FCConvert.ToInt32(MotorVehicle.Statics.Numeric1)) + 1)) + " items from inventory.  Are you sure you wish to continue?", "Remove Inventory?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			}
			else
			{
				intAns = MessageBox.Show("You are about to add " + FCConvert.ToString((FCConvert.ToInt32(MotorVehicle.Statics.Numeric2) - (FCConvert.ToInt32(MotorVehicle.Statics.Numeric1)) + 1)) + " items into inventory.  Are you sure you wish to continue?", "Add Inventory?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			}
			if (intAns == DialogResult.No)
			{
				return;
			}

            if (MotorVehicle.Statics.ListType == "P")
			{
				InventoryType = "P";
				InventoryMY = "X";
				InventoryClass = MotorVehicle.Statics.ListSubType;
				InventorySD = GetSD(ref MotorVehicle.Statics.ListSubType);
			}
			else if (MotorVehicle.Statics.ListType == "MVR")
			{
				InventoryType = "M";
				InventoryMY = "X";
				InventorySD = "S";
				InventoryClass = "00";

			}
			else if (MotorVehicle.Statics.ListType == "DY")
			{
				InventoryType = "S";
				InventoryMY = "Y";
				InventorySD = "D";
				InventoryClass = MotorVehicle.Statics.ListSubType;
			}
			else if (MotorVehicle.Statics.ListType == "COMBINATION")
			{
				InventoryType = "S";
				InventoryMY = "Y";
				InventorySD = "C";
				InventoryClass = MotorVehicle.Statics.ListSubType;
			}
			else if (MotorVehicle.Statics.ListType == "DM")
			{
				InventoryType = "S";
				InventoryMY = "M";
				InventorySD = "D";
				InventoryClass = MotorVehicle.Statics.ListSubType;
			}
			else if (MotorVehicle.Statics.ListType == "SY")
			{
				InventoryType = "S";
				InventoryMY = "Y";
				InventorySD = "S";
				InventoryClass = MotorVehicle.Statics.ListSubType;
			}
			else if (MotorVehicle.Statics.ListType == "SM")
			{
				InventoryType = "S";
				InventoryMY = "M";
				InventorySD = "S";
				InventoryClass = MotorVehicle.Statics.ListSubType;
			}
			else if (MotorVehicle.Statics.ListType == "BOOSTER")
			{
				InventoryType = "B";
				InventoryMY = "X";
				InventorySD = "S";
				InventoryClass = "XX";
			}
			else if (MotorVehicle.Statics.ListType == "SPECIALREGPERMIT")
			{
				InventoryType = "R";
				InventoryMY = "X";
				InventorySD = "S";
				InventoryClass = "XX";
			}
			else if (MotorVehicle.Statics.ListType == "DECAL")
			{
				InventoryType = "D";
				InventoryMY = "Y";
				InventorySD = "S";
				InventoryClass = MotorVehicle.Statics.ListSubType;
            }

			if (InventoryType != "" && InventoryMY != "" && InventorySD != "" && InventoryClass != "")
            {
                inventoryService = StaticSettings.GlobalCommandDispatcher.Send(new GetInventoryService()).Result;
                inventoryService.CancelUnavailable += Cancel_Unavailable;

				ShowWait("Please Wait..." + "\r\n" + "Processing", true);
				Application.StartTask(() =>
                {
                    try
                    {
                        if (PerformInventoryAdjustment(InventoryType, InventoryMY, InventorySD, InventoryClass,
                            txtInventoryAdjustDate.Text, isRemoving))
                        {
                            //if (frmInventory.InstancePtr.cmbInventoryType.Text == "MVR3s")
                            //{
                            //    frmInventory.InstancePtr.optInventoryType_Click(0);
                            //}
                            //else if (frmInventory.InstancePtr.cmbInventoryType.Text == "Boosters")
                            //{
                            //    frmInventory.InstancePtr.optInventoryType_Click(7);
                            //}
                            //else if (frmInventory.InstancePtr.cmbInventoryType.Text == "MVR10s")
                            //{
                            //    frmInventory.InstancePtr.optInventoryType_Click(8);
                            //}
                            //else if (frmInventory.InstancePtr.cmbInventoryType.Text == "Plates")
                            //{
                            //    frmInventory.InstancePtr.lstPlates_DblClick();
                            //}
                            //else
                            //{
                            //    frmInventory.InstancePtr.lstItems_DblClick();
                            //}

                            Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        StaticSettings.GlobalTelemetryService.TrackException(ex);
						FCMessageBox.Show(ex.Message, MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Error");
                    }
                    finally
                    {
						EndWait();
                    }
                });
            }
        }

        private bool PerformInventoryAdjustment(string inventoryType, string inventoryMonthYear, string inventorySingleDouble, string inventoryClass, string inventoryDate, bool isRemoval)
        {
            string message;
            string code;

            cancelled = false;
            code = BuildCode(inventoryType, inventoryMonthYear, inventorySingleDouble, inventoryClass);
            if (AddToInventory(MotorVehicle.Statics.Prefix1, MotorVehicle.Statics.Numeric1, MotorVehicle.Statics.Numeric2, MotorVehicle.Statics.Suffix1, code, inventoryDate,isRemoval) == true)
            {
                EndWait();
                message = "The Inventory";
                //if (cmbAdd.Text == "Add to Inventory")
				if (!isRemoval)
                {
                    message += " Addition was completed";
                }
                else
                {
                    message += " Removal was completed";
                }
                MessageBox.Show(message, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MotorVehicle.Statics.ListRefresh = true;
                return true;
            }
            else
            {
                EndWait();
                if (!cancelled)
                {
                    message = "There was an Error. The Inventory";
                    //if (cmbAdd.Text == "Add to Inventory")
					if (!isRemoval)
                    {
                        message += " Addition was NOT completed";
                    }
                    else
                    {
                        message += " Removal was NOT completed";
                    }
                    MessageBox.Show(message, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
                return false;
			}
        }
		
        private void Adjustment_Cancelled(object sender, EventArgs e)
        {
            inventoryService.CancelAdjustment();
            cancelled = true;
        }

        private void Cancel_Unavailable(object sender, EventArgs e)
        {
            UpdateCancel(false);
        }

		public bool AddToInventory(string P1, string N1, string N2, string s1, string cd, string dt,bool isRemoval)
		{
			DialogResult answer = 0;
			try
            {
                InventoryAdjustmentType adjustType =
					isRemoval
						? InventoryAdjustmentType.Delete
						: InventoryAdjustmentType.Add;

                var adjustmentInfo = new InventoryAdjustmentInfo
                {
                    AdjustmentType = adjustType,
                    Code = cd,
                    Prefix = P1,
                    Suffix = s1,
                    Low = N1.ToIntegerValue(),
                    High = N2.ToIntegerValue()
                };

				var validationResult = inventoryService.ValidateInventoryAdjustmentInfo(adjustmentInfo);

				if (adjustType == InventoryAdjustmentType.Add)
				{
					if (validationResult == InventoryAdjustmentValidationResult.InventoryExistsVoidedIssued)
					{
						answer = MessageBox.Show("One of the Items you tried to add is shown as Issued or Voided. Would you like to change its status back to available?", "Already in Inventory", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
					else if (validationResult == InventoryAdjustmentValidationResult.InventoryExistsAvailable)
					{
						answer = MessageBox.Show("One of the Items you tried to add is already in Inventory.", "Already in Inventory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
					else if (validationResult == InventoryAdjustmentValidationResult.VerifyRange)
					{
						answer = MessageBox.Show("There is a difference in length on the range you entered.  Would you like to continue with this addition?", "Verify Range", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					}
				}
				else
				{
					if (validationResult == InventoryAdjustmentValidationResult.InventoryNotFound)
					{
						MessageBox.Show("One or more of the items you are trying to remove was not found in the database.  Please check your range and try again.", "Unable To Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return false;
					}
					else if (validationResult == InventoryAdjustmentValidationResult.InventoryExistsVoidedIssued)
					{
						answer = MessageBox.Show("One of the Items you tried to delete has been issued, voided or deleted.", "Already Issued", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
				}

				if (answer == DialogResult.No || cancelled)
				{
					return false;
				}
				else
				{
					var adjustmentResult = inventoryService.AdjustInventory(new AdjustInventoryConfigurationInfo
					{
						AdjustmentDetails = adjustmentInfo,
						Reason = txtComment.Text.Trim(),
						OpId = txtInitials.Text.Trim(),
						Group = txtGroup.Text.ToShortValue()
					});

					return adjustmentResult;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				if (StaticSettings.GlobalTelemetryService != null)
                {
                    StaticSettings.GlobalTelemetryService.TrackException(ex);
                }

                return false;
			}
		}

		private void frmInventoryAdjust_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.INVENTORYMAINTENANCE))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			txtInventoryAdjustDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
			//FC:FINAL:DDU:#i1847 - Text property isn't valid for FCListView
			if (MotorVehicle.Statics.ListType == "MVR")
			{
				lblAdjustType.Text = "MVR3s";
			}
			else if (MotorVehicle.Statics.ListType == "P")
			{
				lblAdjustType.Text = Strings.Mid(frmInventory.InstancePtr.lstPlates.Items[frmInventory.InstancePtr.lstPlates.SelectedIndex].Text, 1, 2) + "   Plates";
			}
			else if (MotorVehicle.Statics.ListType == "DY")
			{
				if (Conversion.Val(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text) > 90)
				{
					lblAdjustType.Text = "19" + frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text + " Double Year Stickers";
				}
				else
				{
					lblAdjustType.Text = "20" + frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text + " Double Year Stickers";
				}
			}
			else if (MotorVehicle.Statics.ListType == "COMBINATION")
			{
				if (Conversion.Val(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text) > 90)
				{
					lblAdjustType.Text = "19" + frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text + " Combination Stickers";
				}
				else
				{
					lblAdjustType.Text = "20" + frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text + " Combination Stickers";
				}
			}
			else if (MotorVehicle.Statics.ListType == "DM")
			{
				lblAdjustType.Text = frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text + " Double Month Stickers";
			}
			else if (MotorVehicle.Statics.ListType == "SY")
			{
				if (Conversion.Val(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text) > 90)
				{
					lblAdjustType.Text = "19" + frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text + " Single Year Stickers";
				}
				else
				{
					lblAdjustType.Text = "20" + frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text + " Single Year Stickers";
				}
			}
			else if (MotorVehicle.Statics.ListType == "SM")
			{
				lblAdjustType.Text = frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text + " Single Month Stickers";
			}
			else if (MotorVehicle.Statics.ListType == "BOOSTER")
			{
				lblAdjustType.Text = "Boosters";
			}
			else if (MotorVehicle.Statics.ListType == "SPECIALREGPERMIT")
			{
				lblAdjustType.Text = "Special Registration Permits";
			}
			else if (MotorVehicle.Statics.ListType == "DECAL")
			{
				if (Conversion.Val(frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text) > 90)
				{
					lblAdjustType.Text = "19" + frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text + " Decals";
				}
				else
				{
					lblAdjustType.Text = "20" + frmInventory.InstancePtr.lstItems.Items[frmInventory.InstancePtr.lstItems.SelectedIndex].Text + " Decals";
				}
			}

            if (txtLow.Text.HasText() && txtHigh.Text.HasText() && txtInitials.Visible && txtInitials.Enabled)
            {
                txtInitials.Focus();
            }
		}

		private void frmInventoryAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				if (FCGlobal.Screen.ActiveControl is FCTextBox || FCGlobal.Screen.ActiveControl is Global.T2KDateBox)
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}

			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmInventoryAdjust_Load(object sender, System.EventArgs e)
		{
            txtInitials.Text = MotorVehicle.Statics.UserID;
            modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			SetCustomFormColors();
		}

		public string BuildCode(string IType, string IMY, string ISD, string IClass)
		{
			return IType + IMY + ISD + IClass;
        }

		public string GetSD(ref string Something)
		{
			string GetSD = "";
			// this function uses the class code of plates to decide if it is a Single or a Double
			if (Something == "TT")
			{
				GetSD = "S";
			}
			else if (Something == "MC")
			{
				GetSD = "S";
			}
			else if (Something == "SE")
			{
				GetSD = "S";
			}
			else if (Something == "TL")
			{
				GetSD = "S";
			}
			else if (Something == "XX")
			{
				GetSD = "S";
			}
			else if (Something == "CL")
			{
				GetSD = "S";
			}
			else if (Something == "LT")
			{
				GetSD = "S";
			}
			else
			{
				GetSD = "D";
			}
			return GetSD;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			High = "";
			Low = "";
			InventoryType = "";
			InventoryMY = "";
			InventorySD = "";
			InventoryClass = "";
		}

		private void optActualRemoval_CheckedChanged(object sender, System.EventArgs e)
		{
			if (txtInitials.Text.Length > 1)
			{
				cmdDone.Enabled = true;
			}
			else
			{
				cmdDone.Enabled = false;
			}
			if (txtLow.Visible == true)
			{
				txtLow.Focus();
			}
		}

		private void optAdd_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbAdjust.Enabled = false;
            fcLabel1.Enabled = false;
            if (txtInitials.Text.Length > 1)
			{
				cmdDone.Enabled = true;
			}
			else
			{
				cmdDone.Enabled = false;
			}
			txtLow.Focus();
		}

		private void optAdjust_CheckedChanged(object sender, System.EventArgs e)
		{
			if (txtInitials.Text.Length > 1)
			{
				cmdDone.Enabled = true;
			}
			else
			{
				cmdDone.Enabled = false;
			}
			txtLow.Focus();
		}

		private void optRemove_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbAdjust.Enabled = true;
            fcLabel1.Enabled = true;

            cmbAdjust.Text = "Show as an adjustment";
		}

		private void txtGroup_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtGroup.Text != "")
			{
				if (!Information.IsNumeric(txtGroup.Text) || (Conversion.Val(txtGroup.Text) < 0 || Conversion.Val(txtGroup.Text) > 12))
				{
					MessageBox.Show("Inventory Groups may only be a Number between 0 and 12.", "Invalid Group Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtGroup.Focus();
				}
			}
		}

		private void txtHigh_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtInitials_TextChanged(object sender, System.EventArgs e)
		{
			if (txtInitials.Text.Length > 1)
			{
				if (cmbAdjust.Text == "Show as an adjustment" || cmbAdd.Text == "Add to Inventory" || cmbAdjust.Text == "NOT Show as an adjustment")
				{
					cmdDone.Enabled = true;
				}
				else
				{
					cmdDone.Enabled = false;
				}
			}
		}

		private void txtInitials_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLow_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private bool ChangeGroup()
		{
			bool ChangeGroup = false;
			string strPlate;
			clsDRWrapper rsInventory = new clsDRWrapper();
			int counter;
            int fnx;
            string Code = "";
            string InventoryDT = "";

			ChangeGroup = false;
			if (txtHigh.Text != "")
			{
				High = txtHigh.Text;
			}
			if (txtLow.Text != "")
			{
				Low = txtLow.Text;
			}
			// The strPlate is built to pass the High and Low number to the GetPlateRange function
			// it will return 2 prfixes, 2 suffixes and 2 numbers ( low,high)
			High += Strings.StrDup(8 - fecherFoundation.Strings.Trim(High).Length, " ");
			Low += Strings.StrDup(8 - fecherFoundation.Strings.Trim(Low).Length, " ");
			strPlate = Low + High;
			MotorVehicle.GetPlateRange(strPlate);
			// 
			if (MotorVehicle.Statics.Prefix1.Replace("0", "") != MotorVehicle.Statics.Prefix2.Replace("0", "") || MotorVehicle.Statics.Suffix1 != MotorVehicle.Statics.Suffix2)
			{
				MessageBox.Show("The prefixes or suffixes do not match. Please check your numbers and try again.", "Number Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return ChangeGroup;
			}
			else if (Conversion.Val(MotorVehicle.Statics.Numeric1) > Conversion.Val(MotorVehicle.Statics.Numeric2))
			{
				MessageBox.Show("The low number in the range is larger than the high number.  Please check your numbers and try again.", "Number Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return ChangeGroup;
			}
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Processing";
			frmWait.InstancePtr.Refresh();
			if (MotorVehicle.Statics.ListType == "P")
			{
				InventoryType = "P";
				InventoryMY = "X";
				InventoryClass = MotorVehicle.Statics.ListSubType;
				InventorySD = GetSD(ref MotorVehicle.Statics.ListSubType);
				InventoryDT = txtInventoryAdjustDate.Text;
				Code = BuildCode(InventoryType, InventoryMY, InventorySD, InventoryClass);
			}
			else if (MotorVehicle.Statics.ListType == "MVR")
			{
				InventoryType = "M";
				InventoryMY = "X";
				InventorySD = "S";
				InventoryClass = "00";
				InventoryDT = txtInventoryAdjustDate.Text;
				Code = BuildCode(InventoryType, InventoryMY, InventorySD, InventoryClass);
			}
			else if (MotorVehicle.Statics.ListType == "DY")
			{
				InventoryType = "S";
				InventoryMY = "Y";
				InventorySD = "D";
				InventoryClass = MotorVehicle.Statics.ListSubType;
				InventoryDT = txtInventoryAdjustDate.Text;
				Code = BuildCode(InventoryType, InventoryMY, InventorySD, InventoryClass);
			}
			else if (MotorVehicle.Statics.ListType == "COMBINATION")
			{
				InventoryType = "S";
				InventoryMY = "Y";
				InventorySD = "C";
				InventoryClass = MotorVehicle.Statics.ListSubType;
				InventoryDT = txtInventoryAdjustDate.Text;
				Code = BuildCode(InventoryType, InventoryMY, InventorySD, InventoryClass);
			}
			else if (MotorVehicle.Statics.ListType == "DM")
			{
				InventoryType = "S";
				InventoryMY = "M";
				InventorySD = "D";
				InventoryClass = MotorVehicle.Statics.ListSubType;
				InventoryDT = txtInventoryAdjustDate.Text;
				Code = BuildCode(InventoryType, InventoryMY, InventorySD, InventoryClass);
			}
			else if (MotorVehicle.Statics.ListType == "SY")
			{
				InventoryType = "S";
				InventoryMY = "Y";
				InventorySD = "S";
				InventoryClass = MotorVehicle.Statics.ListSubType;
				InventoryDT = txtInventoryAdjustDate.Text;
				Code = BuildCode(InventoryType, InventoryMY, InventorySD, InventoryClass);
			}
			else if (MotorVehicle.Statics.ListType == "SM")
			{
				InventoryType = "S";
				InventoryMY = "M";
				InventorySD = "S";
				InventoryClass = MotorVehicle.Statics.ListSubType;
				InventoryDT = txtInventoryAdjustDate.Text;
				Code = BuildCode(InventoryType, InventoryMY, InventorySD, InventoryClass);
			}
			else if (MotorVehicle.Statics.ListType == "BOOSTER")
			{
				InventoryType = "B";
				InventoryMY = "X";
				InventorySD = "S";
				InventoryClass = "XX";
				InventoryDT = txtInventoryAdjustDate.Text;
				Code = BuildCode(InventoryType, InventoryMY, InventorySD, InventoryClass);
			}
			else if (MotorVehicle.Statics.ListType == "SPECIALREGPERMIT")
			{
				InventoryType = "R";
				InventoryMY = "X";
				InventorySD = "S";
				InventoryClass = "XX";
				InventoryDT = txtInventoryAdjustDate.Text;
				Code = BuildCode(InventoryType, InventoryMY, InventorySD, InventoryClass);
			}
			else if (MotorVehicle.Statics.ListType == "DECAL")
			{
				InventoryType = "D";
				InventoryMY = "Y";
				InventorySD = "S";
				InventoryClass = MotorVehicle.Statics.ListSubType;
				InventoryDT = txtInventoryAdjustDate.Text;
				Code = BuildCode(InventoryType, InventoryMY, InventorySD, InventoryClass);
			}
			strPlate = "";
			for (fnx = FCConvert.ToInt32(MotorVehicle.Statics.Numeric1); fnx <= FCConvert.ToInt32(MotorVehicle.Statics.Numeric2); fnx++)
			{
				if (strPlate == "")
				{
					strPlate = MotorVehicle.Statics.Prefix1 + FCConvert.ToString(fnx) + MotorVehicle.Statics.Suffix1;
					MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + Code + "' AND Prefix + convert(nvarchar, Number) + Suffix = '" + strPlate + "'";
				}
				else
				{
					if ((MotorVehicle.Statics.Prefix1 + FCConvert.ToString(fnx) + MotorVehicle.Statics.Suffix1).Length > strPlate.Length && Strings.Right(MotorVehicle.Statics.Prefix1, 1) == "0")
					{
						MotorVehicle.Statics.Prefix1 = Strings.Left(MotorVehicle.Statics.Prefix1, MotorVehicle.Statics.Prefix1.Length - ((MotorVehicle.Statics.Prefix1 + FCConvert.ToString(fnx) + MotorVehicle.Statics.Suffix1).Length - strPlate.Length));
						strPlate = MotorVehicle.Statics.Prefix1 + FCConvert.ToString(fnx) + MotorVehicle.Statics.Suffix1;
					}
					else
					{
						strPlate = MotorVehicle.Statics.Prefix1 + FCConvert.ToString(fnx) + MotorVehicle.Statics.Suffix1;
					}
					MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE Code = '" + Code + "' AND Prefix + convert(nvarchar, Number) + Suffix = '" + strPlate + "'";
				}
				rsInventory.OpenRecordset(MotorVehicle.Statics.strSql);
				if (rsInventory.EndOfFile() != true && rsInventory.BeginningOfFile() != true)
				{
					rsInventory.Edit();
					rsInventory.Set_Fields("Group", FCConvert.ToString(Conversion.Val(txtGroup.Text)));
					rsInventory.Update();
				}
			}
			ChangeGroup = true;
			frmWait.InstancePtr.Unload();
			return ChangeGroup;
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			clsDRWrapper clsChildList;
			string strPerm = "";
			int lngChild = 0;
			clsChildList = new clsDRWrapper();
			modGlobalConstants.Statics.clsSecurityClass.Get_Children(ref clsChildList, ref lngFuncID);
			while (!clsChildList.EndOfFile())
			{
				lngChild = FCConvert.ToInt32(clsChildList.GetData("childid"));
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngChild));
				switch (lngChild)
				{
					case MotorVehicle.ADDINVENTORY:
						{
							if (strPerm == "N")
							{
								if (cmbAdd.Items.Contains("Add to Inventory"))
								{
									cmbAdd.Items.Remove("Add to Inventory");
								}
							}
							else
							{
								if (!cmbAdd.Items.Contains("Add to Inventory"))
								{
									cmbAdd.Items.Insert(0, "Add to Inventory");
								}
							}
							break;
						}
					case MotorVehicle.REMOVEINVENTORY:
						{
							if (strPerm == "N")
							{
								if (cmbAdd.Items.Contains("Remove from Inventory"))
								{
									cmbAdd.Items.Remove("Remove from Inventory");
								}
							}
							else
							{
								if (!cmbAdd.Items.Contains("Remove from Inventory"))
								{
									cmbAdd.Items.Insert(1, "Remove from Inventory");
								}
							}
							break;
						}
				}
				//end switch
				clsChildList.MoveNext();
			}
		}

		private void SetCustomFormColors()
		{
			lblWarning.ForeColor = Color.Red;
		}

		public void cmbAdjust_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAdjust.Text == "NOT show as an adjustment")
			{
				optAdjust_CheckedChanged(sender, e);
			}
			else if (cmbAdjust.Text == "Show as an adjustment")
			{
				optActualRemoval_CheckedChanged(sender, e);
			}
		}

		public void cmbAdd_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAdd.Text == "Add to Inventory")
			{
				optAdd_CheckedChanged(sender, e);
			}
			else if (cmbAdd.Text == "Remove from Inventory")
			{
				optRemove_CheckedChanged(sender, e);
			}
		}
	}
}
