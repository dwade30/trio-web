//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmSelectCode.
	/// </summary>
	partial class frmSelectCode
	{
		public fecherFoundation.FCFrame fraCodes;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCComboBox cboCodesList;
		public fecherFoundation.FCLabel lblInstructions;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			//FC:FINAL:DDU:#i1960 - hold disposing of form, we need just to hide and use it's controls data
			if (dontDispose)
			{
				return;
			}
			else
			{
				if (_InstancePtr == this)
				{
					_InstancePtr = null;
					Sys.ClearInstance(this);
				}
				if (disposing)
				{
					if (components != null)
					{
						components.Dispose();
					}
				}
				base.Dispose(disposing);
			}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fraCodes = new fecherFoundation.FCFrame();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdOK = new fecherFoundation.FCButton();
			this.cboCodesList = new fecherFoundation.FCComboBox();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCodes)).BeginInit();
			this.fraCodes.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 307);
			this.BottomPanel.Size = new System.Drawing.Size(523, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraCodes);
			this.ClientArea.Location = new System.Drawing.Point(0, 0);
			this.ClientArea.Size = new System.Drawing.Size(523, 307);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(523, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// fraCodes
			// 
			this.fraCodes.Controls.Add(this.cmdCancel);
			this.fraCodes.Controls.Add(this.cmdOK);
			this.fraCodes.Controls.Add(this.cboCodesList);
			this.fraCodes.Controls.Add(this.lblInstructions);
			this.fraCodes.Location = new System.Drawing.Point(30, 30);
			this.fraCodes.Name = "fraCodes";
			this.fraCodes.Size = new System.Drawing.Size(465, 185);
			this.fraCodes.TabIndex = 0;
			this.fraCodes.Text = "Valid Codes";
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(114, 125);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(95, 40);
			this.cmdCancel.TabIndex = 3;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.Location = new System.Drawing.Point(20, 125);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(64, 40);
			this.cmdOK.TabIndex = 2;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cboCodesList
			// 
			this.cboCodesList.AutoSize = false;
			this.cboCodesList.BackColor = System.Drawing.SystemColors.Window;
			this.cboCodesList.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCodesList.FormattingEnabled = true;
			this.cboCodesList.Location = new System.Drawing.Point(20, 65);
			this.cboCodesList.Name = "cboCodesList";
			this.cboCodesList.Size = new System.Drawing.Size(274, 40);
			this.cboCodesList.TabIndex = 1;
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(20, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(425, 15);
			this.lblInstructions.TabIndex = 4;
			this.lblInstructions.Text = "PLEASE SELECT THE CODE YOU WISH TO USE AND CLICK THE OK BUTTON";
			this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmSelectCode
            // 
            this.AcceptButton = this.cmdOK;
			this.ClientSize = new System.Drawing.Size(523, 245);
			this.ControlBox = false;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmSelectCode";
			this.ShowInTaskbar = false;
			this.Text = "";
			this.Load += new System.EventHandler(this.frmSelectCode_Load);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCodes)).EndInit();
			this.fraCodes.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}