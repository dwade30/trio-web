//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmOpID.
	/// </summary>
	partial class frmOpID
	{
		public fecherFoundation.FCTextBox txtOpId;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtOpId = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 105);
            this.BottomPanel.Size = new System.Drawing.Size(276, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtOpId);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(296, 138);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtOpId, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(296, 0);
            this.TopPanel.Visible = false;
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // txtOpId
            // 
            this.txtOpId.AutoComplete = Wisej.Web.AutoComplete.Off;
            this.txtOpId.BackColor = System.Drawing.SystemColors.Window;
            this.txtOpId.Location = new System.Drawing.Point(30, 65);
            this.txtOpId.MaxLength = 3;
            this.txtOpId.Name = "txtOpId";
            this.txtOpId.Size = new System.Drawing.Size(209, 40);
            this.txtOpId.TabIndex = 0;
            this.txtOpId.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtOpId.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOpId_KeyPress);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(238, 30);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "PLEASE INPUT YOUR MOTOR VEHICLE OPERATOR IDENTIFICATION NUMBER";
            // 
            // frmOpID
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(296, 138);
            this.ControlBox = false;
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmOpID";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Operator ID";
            this.Load += new System.EventHandler(this.frmOpID_Load);
            this.Activated += new System.EventHandler(this.frmOpID_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmOpID_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
