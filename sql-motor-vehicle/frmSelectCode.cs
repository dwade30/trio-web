﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmSelectCode : BaseForm
	{
		public frmSelectCode()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSelectCode InstancePtr
		{
			get
			{
				return (frmSelectCode)Sys.GetInstance(typeof(frmSelectCode));
			}
		}

		protected frmSelectCode _InstancePtr = null;
		//=========================================================
		private string[] arrCodes = null;
		private bool boolLoading;
		public string SelectedCode = "";
		//FC:FINAL:DDU:#i1960 - hold disposing of form, we need just to hide and use it's controls data
		public bool dontDispose = false;

		public void LoadList(ref string strList)
		{
			string[] strAry1 = null;
			string[] strAry2 = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			boolLoading = true;
			try
			{
				//FC:FINAL:DDU:#i2161 - clear list of elements firstly
				cboCodesList.Items.Clear();
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strAry1 = Strings.Split(strList, "|", -1, CompareConstants.vbTextCompare);
				arrCodes = new string[Information.UBound(strAry1, 1) + 1];
				for (x = 0; x <= (Information.UBound(strAry1, 1)); x++)
				{
					strAry2 = Strings.Split(strAry1[x], ";", -1, CompareConstants.vbTextCompare);
					cboCodesList.AddItem(strAry2[1], x);
					arrCodes[x] = strAry2[0];
				}
				// x
				boolLoading = false;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + " in frmSelectCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			if (cboCodesList.SelectedIndex < 0)
			{
				MessageBox.Show("Please select a valid code.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				//FC:FINAL:DDU:#i2161 added checking .SelectedIndex value before getting item from list to avoid exceptions if there is no selected item or list is empty
				SelectedCode = cboCodesList.SelectedIndex >= 0 ? arrCodes[cboCodesList.SelectedIndex] : "";
				//FC:FINAL:DDU:#i1960 - hold disposing of form, we need just to hide and use it's controls data
				dontDispose = true;
				this.Visible = false;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			SelectedCode = "";
			//FC:FINAL:DDU:#i1960 - hold disposing of form, we need just to hide and use it's controls data
			dontDispose = true;
			this.Visible = false;
		}

		private void frmSelectCode_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSelectCode properties;
			//frmSelectCode.ScaleWidth	= 4200;
			//frmSelectCode.ScaleHeight	= 1965;
			//frmSelectCode.LinkTopic	= "Form1";
			//End Unmaped Properties
			this.Left = (FCGlobal.Screen.Width - this.Width) / 2;
			this.Top = (FCGlobal.Screen.Height - this.Height) / 2;
			modGlobalFunctions.SetTRIOColors(this, false);
			SelectedCode = "";
            cboCodesList.DropDownHeight = (FCGlobal.Screen.Height / 2) - (this.Height / 2);
        }
	}
}
