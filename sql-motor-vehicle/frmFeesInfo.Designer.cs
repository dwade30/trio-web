//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmFeesInfo.
	/// </summary>
	partial class frmFeesInfo
	{
		public fecherFoundation.FCComboBox cmbVoucher;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCTextBox txtGCNumber;
		public fecherFoundation.FCButton cmdReturn;
		public Global.T2KBackFillDecimal txtFEEReplacementFee;
		public Global.T2KBackFillDecimal txtFEEStickerFee;
		public Global.T2KBackFillDecimal txtFEETransferFee;
		public Global.T2KBackFillDecimal txtFEEGiftCertificate;
		public Global.T2KBackFillDecimal txtFEEInitialPlate;
		public Global.T2KBackFillDecimal txtFEEOutOfRotation;
		public Global.T2KBackFillDecimal txtFEEReservePlate;
		public Global.T2KBackFillDecimal txtFEEPlateClass;
		public Global.T2KBackFillDecimal txtFEETotal;
		public Global.T2KBackFillDecimal txtFEECreditComm;
		public Global.T2KBackFillDecimal txtFEECreditTransfer;
		public Global.T2KBackFillDecimal txtFEERegFee;
		public Global.T2KBackFillDecimal txtDupRegFee;
		public Global.T2KBackFillDecimal txtIncreaseRVW;
		public fecherFoundation.FCLabel Label2_12;
		public fecherFoundation.FCLabel Label2_11;
		public fecherFoundation.FCLabel Label2_9;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2_10;
		public fecherFoundation.FCLabel Label2_8;
		public fecherFoundation.FCLabel Label2_7;
		public fecherFoundation.FCLabel Label2_6;
		public fecherFoundation.FCLabel Label2_5;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label2_3;
		public fecherFoundation.FCLabel Label2_2;
		public fecherFoundation.FCLabel Label2_1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbVoucher = new fecherFoundation.FCComboBox();
			this.txtGCNumber = new fecherFoundation.FCTextBox();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.txtFEEReplacementFee = new Global.T2KBackFillDecimal();
			this.txtFEEStickerFee = new Global.T2KBackFillDecimal();
			this.txtFEETransferFee = new Global.T2KBackFillDecimal();
			this.txtFEEGiftCertificate = new Global.T2KBackFillDecimal();
			this.txtFEEInitialPlate = new Global.T2KBackFillDecimal();
			this.txtFEEOutOfRotation = new Global.T2KBackFillDecimal();
			this.txtFEEReservePlate = new Global.T2KBackFillDecimal();
			this.txtFEEPlateClass = new Global.T2KBackFillDecimal();
			this.txtFEETotal = new Global.T2KBackFillDecimal();
			this.txtFEECreditComm = new Global.T2KBackFillDecimal();
			this.txtFEECreditTransfer = new Global.T2KBackFillDecimal();
			this.txtFEERegFee = new Global.T2KBackFillDecimal();
			this.txtDupRegFee = new Global.T2KBackFillDecimal();
			this.txtIncreaseRVW = new Global.T2KBackFillDecimal();
			this.Label2_12 = new fecherFoundation.FCLabel();
			this.Label2_11 = new fecherFoundation.FCLabel();
			this.Label2_9 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2_10 = new fecherFoundation.FCLabel();
			this.Label2_8 = new fecherFoundation.FCLabel();
			this.Label2_7 = new fecherFoundation.FCLabel();
			this.Label2_6 = new fecherFoundation.FCLabel();
			this.Label2_5 = new fecherFoundation.FCLabel();
			this.Label2_0 = new fecherFoundation.FCLabel();
			this.Label2_3 = new fecherFoundation.FCLabel();
			this.Label2_2 = new fecherFoundation.FCLabel();
			this.Label2_1 = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEReplacementFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEStickerFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEETransferFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEGiftCertificate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEInitialPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEOutOfRotation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEReservePlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEPlateClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEETotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEECreditComm)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEECreditTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEERegFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDupRegFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIncreaseRVW)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdReturn);
			this.BottomPanel.Location = new System.Drawing.Point(0, 757);
			this.BottomPanel.Size = new System.Drawing.Size(575, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbVoucher);
			this.ClientArea.Controls.Add(this.txtGCNumber);
			this.ClientArea.Controls.Add(this.txtFEEReplacementFee);
			this.ClientArea.Controls.Add(this.txtFEEStickerFee);
			this.ClientArea.Controls.Add(this.txtFEETransferFee);
			this.ClientArea.Controls.Add(this.txtFEEGiftCertificate);
			this.ClientArea.Controls.Add(this.txtFEEInitialPlate);
			this.ClientArea.Controls.Add(this.txtFEEOutOfRotation);
			this.ClientArea.Controls.Add(this.txtFEEReservePlate);
			this.ClientArea.Controls.Add(this.txtFEEPlateClass);
			this.ClientArea.Controls.Add(this.txtFEETotal);
			this.ClientArea.Controls.Add(this.txtFEECreditComm);
			this.ClientArea.Controls.Add(this.txtFEECreditTransfer);
			this.ClientArea.Controls.Add(this.txtFEERegFee);
			this.ClientArea.Controls.Add(this.txtDupRegFee);
			this.ClientArea.Controls.Add(this.txtIncreaseRVW);
			this.ClientArea.Controls.Add(this.Label2_12);
			this.ClientArea.Controls.Add(this.Label2_11);
			this.ClientArea.Controls.Add(this.Label2_9);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label2_10);
			this.ClientArea.Controls.Add(this.Label2_8);
			this.ClientArea.Controls.Add(this.Label2_7);
			this.ClientArea.Controls.Add(this.Label2_6);
			this.ClientArea.Controls.Add(this.Label2_5);
			this.ClientArea.Controls.Add(this.Label2_0);
			this.ClientArea.Controls.Add(this.Label2_3);
			this.ClientArea.Controls.Add(this.Label2_2);
			this.ClientArea.Controls.Add(this.Label2_1);
			this.ClientArea.Size = new System.Drawing.Size(595, 628);
			this.ClientArea.Controls.SetChildIndex(this.Label2_1, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_2, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_3, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_0, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_5, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_6, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_7, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_8, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_10, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_9, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_11, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2_12, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtIncreaseRVW, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtDupRegFee, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEERegFee, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEECreditTransfer, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEECreditComm, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEETotal, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEEPlateClass, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEEReservePlate, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEEOutOfRotation, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEEInitialPlate, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEEGiftCertificate, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEETransferFee, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEEStickerFee, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtFEEReplacementFee, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtGCNumber, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbVoucher, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(595, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(184, 28);
			this.HeaderText.Text = "Fees Information";
			// 
			// cmbVoucher
			// 
			this.cmbVoucher.Items.AddRange(new object[] {
            "Voucher",
            "Gift Certificate"});
			this.cmbVoucher.Location = new System.Drawing.Point(30, 530);
			this.cmbVoucher.Name = "cmbVoucher";
			this.cmbVoucher.Size = new System.Drawing.Size(194, 40);
			this.cmbVoucher.TabIndex = 1001;
			this.cmbVoucher.Text = "Voucher";
			// 
			// txtGCNumber
			// 
			this.txtGCNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtGCNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtGCNumber.Enabled = false;
			this.txtGCNumber.Location = new System.Drawing.Point(256, 580);
			this.txtGCNumber.MaxLength = 6;
			this.txtGCNumber.Name = "txtGCNumber";
			this.txtGCNumber.Size = new System.Drawing.Size(282, 40);
			this.txtGCNumber.TabIndex = 11;
			this.txtGCNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGCNumber_KeyDown);
			this.txtGCNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtGCNumber_KeyPress);
			// 
			// cmdReturn
			// 
			this.cmdReturn.AppearanceKey = "acceptButton";
			this.cmdReturn.Location = new System.Drawing.Point(256, 30);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdReturn.Size = new System.Drawing.Size(78, 48);
			this.cmdReturn.TabIndex = 15;
			this.cmdReturn.Text = "Save";
			this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
			this.cmdReturn.KeyDown += new Wisej.Web.KeyEventHandler(this.cmdReturn_KeyDown);
			// 
			// txtFEEReplacementFee
			// 
			this.txtFEEReplacementFee.Location = new System.Drawing.Point(256, 380);
			this.txtFEEReplacementFee.MaxLength = 8;
			this.txtFEEReplacementFee.Name = "txtFEEReplacementFee";
			this.txtFEEReplacementFee.Size = new System.Drawing.Size(282, 22);
			this.txtFEEReplacementFee.TabIndex = 7;
			this.txtFEEReplacementFee.Text = "0.00";
			this.txtFEEReplacementFee.Leave += new System.EventHandler(this.txtFEEReplacementFee_Leave);
			this.txtFEEReplacementFee.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEEReplacementFee_KeyDownEvent);
			// 
			// txtFEEStickerFee
			// 
			this.txtFEEStickerFee.Location = new System.Drawing.Point(256, 430);
			this.txtFEEStickerFee.MaxLength = 8;
			this.txtFEEStickerFee.Name = "txtFEEStickerFee";
			this.txtFEEStickerFee.Size = new System.Drawing.Size(282, 22);
			this.txtFEEStickerFee.TabIndex = 8;
			this.txtFEEStickerFee.Text = "0.00";
			this.txtFEEStickerFee.Leave += new System.EventHandler(this.txtFEEStickerFee_Leave);
			this.txtFEEStickerFee.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEEStickerFee_KeyDownEvent);
			// 
			// txtFEETransferFee
			// 
			this.txtFEETransferFee.Location = new System.Drawing.Point(256, 480);
			this.txtFEETransferFee.MaxLength = 8;
			this.txtFEETransferFee.Name = "txtFEETransferFee";
			this.txtFEETransferFee.Size = new System.Drawing.Size(282, 22);
			this.txtFEETransferFee.TabIndex = 9;
			this.txtFEETransferFee.Text = "0.00";
			this.txtFEETransferFee.Leave += new System.EventHandler(this.txtFEETransferFee_Leave);
			this.txtFEETransferFee.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEETransferFee_KeyDownEvent);
			// 
			// txtFEEGiftCertificate
			// 
			this.txtFEEGiftCertificate.ForeColor = System.Drawing.Color.FromName("@invalid");
			this.txtFEEGiftCertificate.Location = new System.Drawing.Point(256, 530);
			this.txtFEEGiftCertificate.MaxLength = 8;
			this.txtFEEGiftCertificate.Name = "txtFEEGiftCertificate";
			this.txtFEEGiftCertificate.Size = new System.Drawing.Size(282, 22);
			this.txtFEEGiftCertificate.TabIndex = 10;
			this.txtFEEGiftCertificate.Text = "0.00";
			this.txtFEEGiftCertificate.Leave += new System.EventHandler(this.txtFEEGiftCertificate_Leave);
			this.txtFEEGiftCertificate.TextChanged += new System.EventHandler(this.txtFEEGiftCertificate_Change);
			this.txtFEEGiftCertificate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEEGiftCertificate_KeyDownEvent);
			// 
			// txtFEEInitialPlate
			// 
			this.txtFEEInitialPlate.Location = new System.Drawing.Point(256, 180);
			this.txtFEEInitialPlate.MaxLength = 8;
			this.txtFEEInitialPlate.Name = "txtFEEInitialPlate";
			this.txtFEEInitialPlate.Size = new System.Drawing.Size(282, 22);
			this.txtFEEInitialPlate.TabIndex = 3;
			this.txtFEEInitialPlate.Text = "0.00";
			this.txtFEEInitialPlate.Leave += new System.EventHandler(this.txtFEEInitialPlate_Leave);
			this.txtFEEInitialPlate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEEInitialPlate_KeyDownEvent);
			// 
			// txtFEEOutOfRotation
			// 
			this.txtFEEOutOfRotation.Location = new System.Drawing.Point(256, 230);
			this.txtFEEOutOfRotation.MaxLength = 8;
			this.txtFEEOutOfRotation.Name = "txtFEEOutOfRotation";
			this.txtFEEOutOfRotation.Size = new System.Drawing.Size(282, 22);
			this.txtFEEOutOfRotation.TabIndex = 4;
			this.txtFEEOutOfRotation.Text = "0.00";
			this.txtFEEOutOfRotation.Leave += new System.EventHandler(this.txtFEEOutOfRotation_Leave);
			this.txtFEEOutOfRotation.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEEOutOfRotation_KeyDownEvent);
			// 
			// txtFEEReservePlate
			// 
			this.txtFEEReservePlate.Location = new System.Drawing.Point(256, 280);
			this.txtFEEReservePlate.MaxLength = 8;
			this.txtFEEReservePlate.Name = "txtFEEReservePlate";
			this.txtFEEReservePlate.Size = new System.Drawing.Size(282, 22);
			this.txtFEEReservePlate.TabIndex = 5;
			this.txtFEEReservePlate.Text = "0.00";
			this.txtFEEReservePlate.Leave += new System.EventHandler(this.txtFEEReservePlate_Leave);
			this.txtFEEReservePlate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEEReservePlate_KeyDownEvent);
			// 
			// txtFEEPlateClass
			// 
			this.txtFEEPlateClass.Location = new System.Drawing.Point(256, 330);
			this.txtFEEPlateClass.MaxLength = 8;
			this.txtFEEPlateClass.Name = "txtFEEPlateClass";
			this.txtFEEPlateClass.Size = new System.Drawing.Size(282, 22);
			this.txtFEEPlateClass.TabIndex = 6;
			this.txtFEEPlateClass.Text = "0.00";
			this.txtFEEPlateClass.Leave += new System.EventHandler(this.txtFEEPlateClass_Leave);
			this.txtFEEPlateClass.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEEPlateClass_KeyDownEvent);
			// 
			// txtFEETotal
			// 
			this.txtFEETotal.Location = new System.Drawing.Point(256, 730);
			this.txtFEETotal.MaxLength = 8;
			this.txtFEETotal.Name = "txtFEETotal";
			this.txtFEETotal.Size = new System.Drawing.Size(282, 22);
			this.txtFEETotal.TabIndex = 14;
			this.txtFEETotal.Text = "0.00";
			this.txtFEETotal.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEETotal_KeyDownEvent);
			// 
			// txtFEECreditComm
			// 
			this.txtFEECreditComm.ForeColor = System.Drawing.Color.FromName("@invalid");
			this.txtFEECreditComm.Location = new System.Drawing.Point(256, 80);
			this.txtFEECreditComm.MaxLength = 8;
			this.txtFEECreditComm.Name = "txtFEECreditComm";
			this.txtFEECreditComm.Size = new System.Drawing.Size(282, 22);
			this.txtFEECreditComm.TabIndex = 1;
			this.txtFEECreditComm.Text = "0.00";
			this.txtFEECreditComm.Leave += new System.EventHandler(this.txtFEECreditComm_Leave);
			this.txtFEECreditComm.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEECreditComm_KeyDownEvent);
			// 
			// txtFEECreditTransfer
			// 
			this.txtFEECreditTransfer.ForeColor = System.Drawing.Color.FromName("@invalid");
			this.txtFEECreditTransfer.Location = new System.Drawing.Point(256, 130);
			this.txtFEECreditTransfer.MaxLength = 8;
			this.txtFEECreditTransfer.Name = "txtFEECreditTransfer";
			this.txtFEECreditTransfer.Size = new System.Drawing.Size(282, 22);
			this.txtFEECreditTransfer.TabIndex = 2;
			this.txtFEECreditTransfer.Text = "0.00";
			this.txtFEECreditTransfer.Leave += new System.EventHandler(this.txtFEECreditTransfer_Leave);
			this.txtFEECreditTransfer.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEECreditTransfer_KeyDownEvent);
			// 
			// txtFEERegFee
			// 
			this.txtFEERegFee.Location = new System.Drawing.Point(256, 30);
			this.txtFEERegFee.MaxLength = 8;
			this.txtFEERegFee.Name = "txtFEERegFee";
			this.txtFEERegFee.Size = new System.Drawing.Size(282, 22);
			this.txtFEERegFee.TabIndex = 1002;
			this.txtFEERegFee.Text = "0.00";
			this.txtFEERegFee.Leave += new System.EventHandler(this.txtFEERegFee_Leave);
			this.txtFEERegFee.KeyDown += new Wisej.Web.KeyEventHandler(this.txtFEERegFee_KeyDownEvent);
			// 
			// txtDupRegFee
			// 
			this.txtDupRegFee.Location = new System.Drawing.Point(256, 630);
			this.txtDupRegFee.MaxLength = 8;
			this.txtDupRegFee.Name = "txtDupRegFee";
			this.txtDupRegFee.Size = new System.Drawing.Size(282, 22);
			this.txtDupRegFee.TabIndex = 12;
			this.txtDupRegFee.Text = "0.00";
			this.txtDupRegFee.Leave += new System.EventHandler(this.txtDupRegFee_Leave);
			this.txtDupRegFee.KeyDown += new Wisej.Web.KeyEventHandler(this.txtDupRegFee_KeyDownEvent);
			// 
			// txtIncreaseRVW
			// 
			this.txtIncreaseRVW.Location = new System.Drawing.Point(256, 680);
			this.txtIncreaseRVW.MaxLength = 8;
			this.txtIncreaseRVW.Name = "txtIncreaseRVW";
			this.txtIncreaseRVW.Size = new System.Drawing.Size(282, 22);
			this.txtIncreaseRVW.TabIndex = 13;
			this.txtIncreaseRVW.Text = "0.00";
			this.txtIncreaseRVW.KeyDown += new Wisej.Web.KeyEventHandler(this.txtIncreaseRVW_KeyDownEvent);
			// 
			// Label2_12
			// 
			this.Label2_12.Enabled = false;
			this.Label2_12.Location = new System.Drawing.Point(30, 694);
			this.Label2_12.Name = "Label2_12";
			this.Label2_12.Size = new System.Drawing.Size(104, 13);
			this.Label2_12.TabIndex = 29;
			this.Label2_12.Text = "INCR. RVW FEE";
			// 
			// Label2_11
			// 
			this.Label2_11.Enabled = false;
			this.Label2_11.Location = new System.Drawing.Point(30, 644);
			this.Label2_11.Name = "Label2_11";
			this.Label2_11.Size = new System.Drawing.Size(120, 13);
			this.Label2_11.TabIndex = 28;
			this.Label2_11.Text = "DUPLICATE REG. FEE";
			// 
			// Label2_9
			// 
			this.Label2_9.Enabled = false;
			this.Label2_9.Location = new System.Drawing.Point(30, 594);
			this.Label2_9.Name = "Label2_9";
			this.Label2_9.Size = new System.Drawing.Size(108, 13);
			this.Label2_9.TabIndex = 27;
			this.Label2_9.Text = "G. C. / VOUCHER  #";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 44);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(130, 16);
			this.Label3.TabIndex = 26;
			this.Label3.Text = "REGISTRATION RATE";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 144);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(130, 13);
			this.Label1.TabIndex = 25;
			this.Label1.Text = "TRANSFER CREDIT";
			// 
			// Label2_10
			// 
			this.Label2_10.Location = new System.Drawing.Point(30, 94);
			this.Label2_10.Name = "Label2_10";
			this.Label2_10.Size = new System.Drawing.Size(150, 16);
			this.Label2_10.TabIndex = 24;
			this.Label2_10.Text = "COMMERCIAL CREDIT";
			// 
			// Label2_8
			// 
			this.Label2_8.Enabled = false;
			this.Label2_8.Location = new System.Drawing.Point(30, 744);
			this.Label2_8.Name = "Label2_8";
			this.Label2_8.Size = new System.Drawing.Size(98, 13);
			this.Label2_8.TabIndex = 23;
			this.Label2_8.Text = "TOTAL FEES";
			// 
			// Label2_7
			// 
			this.Label2_7.Location = new System.Drawing.Point(30, 344);
			this.Label2_7.Name = "Label2_7";
			this.Label2_7.Size = new System.Drawing.Size(98, 13);
			this.Label2_7.TabIndex = 22;
			this.Label2_7.Text = "PLATE CLASS";
			// 
			// Label2_6
			// 
			this.Label2_6.Location = new System.Drawing.Point(30, 294);
			this.Label2_6.Name = "Label2_6";
			this.Label2_6.Size = new System.Drawing.Size(98, 13);
			this.Label2_6.TabIndex = 21;
			this.Label2_6.Text = "RESERVE PLATE";
			// 
			// Label2_5
			// 
			this.Label2_5.Location = new System.Drawing.Point(30, 244);
			this.Label2_5.Name = "Label2_5";
			this.Label2_5.Size = new System.Drawing.Size(120, 13);
			this.Label2_5.TabIndex = 20;
			this.Label2_5.Text = "OUT OF ROTATION";
			// 
			// Label2_0
			// 
			this.Label2_0.Location = new System.Drawing.Point(30, 194);
			this.Label2_0.Name = "Label2_0";
			this.Label2_0.Size = new System.Drawing.Size(98, 13);
			this.Label2_0.TabIndex = 19;
			this.Label2_0.Text = "VANITY PLATE";
			// 
			// Label2_3
			// 
			this.Label2_3.Location = new System.Drawing.Point(30, 494);
			this.Label2_3.Name = "Label2_3";
			this.Label2_3.Size = new System.Drawing.Size(98, 13);
			this.Label2_3.TabIndex = 18;
			this.Label2_3.Text = "TRANSFER FEE";
			// 
			// Label2_2
			// 
			this.Label2_2.Enabled = false;
			this.Label2_2.Location = new System.Drawing.Point(30, 444);
			this.Label2_2.Name = "Label2_2";
			this.Label2_2.Size = new System.Drawing.Size(98, 13);
			this.Label2_2.TabIndex = 17;
			this.Label2_2.Text = "STICKER FEE";
			// 
			// Label2_1
			// 
			this.Label2_1.Location = new System.Drawing.Point(30, 394);
			this.Label2_1.Name = "Label2_1";
			this.Label2_1.Size = new System.Drawing.Size(108, 13);
			this.Label2_1.TabIndex = 16;
			this.Label2_1.Text = "REPLACEMENT FEE";
			// 
			// frmFeesInfo
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(595, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmFeesInfo";
			this.Text = "Fees Information";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.frmFeesInfo_QueryUnload);
			this.Load += new System.EventHandler(this.frmFeesInfo_Load);
			this.VisibleChanged += new System.EventHandler(this.FrmFeesInfo_VisibleChanged);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFeesInfo_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEReplacementFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEStickerFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEETransferFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEGiftCertificate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEInitialPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEOutOfRotation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEReservePlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEEPlateClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEETotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEECreditComm)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEECreditTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFEERegFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDupRegFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIncreaseRVW)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		//private FCButton cmdExit;
	}
}
