//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmSetupExciseTaxEstimate.
	/// </summary>
	partial class frmSetupExciseTaxEstimate
	{
		public fecherFoundation.FCComboBox cmbAllStyle;
		public fecherFoundation.FCComboBox cmbSpecific;
		public fecherFoundation.FCComboBox cmbCollected;
		public fecherFoundation.FCLabel lblCollected;
		public fecherFoundation.FCComboBox cmbDetail;
		public fecherFoundation.FCLabel lblDetail;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCGrid vsStyle;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraSpecificClass;
		public fecherFoundation.FCComboBox cboClass;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCComboBox cboEndMonth;
		public fecherFoundation.FCComboBox cboEndYear;
		public fecherFoundation.FCComboBox cboStartYear;
		public fecherFoundation.FCComboBox cboStartMonth;
		public fecherFoundation.FCLabel lblTo;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbAllStyle = new fecherFoundation.FCComboBox();
			this.cmbSpecific = new fecherFoundation.FCComboBox();
			this.cmbCollected = new fecherFoundation.FCComboBox();
			this.lblCollected = new fecherFoundation.FCLabel();
			this.cmbDetail = new fecherFoundation.FCComboBox();
			this.lblDetail = new fecherFoundation.FCLabel();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.vsStyle = new fecherFoundation.FCGrid();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.fraSpecificClass = new fecherFoundation.FCFrame();
			this.cboClass = new fecherFoundation.FCComboBox();
			this.fraRange = new fecherFoundation.FCFrame();
			this.cboEndMonth = new fecherFoundation.FCComboBox();
			this.cboEndYear = new fecherFoundation.FCComboBox();
			this.cboStartYear = new fecherFoundation.FCComboBox();
			this.cboStartMonth = new fecherFoundation.FCComboBox();
			this.lblTo = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsStyle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificClass)).BeginInit();
			this.fraSpecificClass.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(1000, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.cmbCollected);
			this.ClientArea.Controls.Add(this.lblCollected);
			this.ClientArea.Controls.Add(this.fraRange);
			this.ClientArea.Controls.Add(this.cmbDetail);
			this.ClientArea.Controls.Add(this.lblDetail);
			this.ClientArea.Size = new System.Drawing.Size(1000, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(1000, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(213, 30);
			this.HeaderText.Text = "Excise Tax Report";
			// 
			// cmbAllStyle
			// 
			this.cmbAllStyle.AutoSize = false;
			this.cmbAllStyle.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllStyle.FormattingEnabled = true;
			this.cmbAllStyle.Items.AddRange(new object[] {
            "All",
            "Selected"});
			this.cmbAllStyle.Location = new System.Drawing.Point(20, 30);
			this.cmbAllStyle.Name = "cmbAllStyle";
			this.cmbAllStyle.Size = new System.Drawing.Size(265, 40);
			this.cmbAllStyle.TabIndex = 22;
			this.cmbAllStyle.Text = "All";
			this.cmbAllStyle.SelectedIndexChanged += new System.EventHandler(this.cmbAllStyle_SelectedIndexChanged);
			// 
			// cmbSpecific
			// 
			this.cmbSpecific.AutoSize = false;
			this.cmbSpecific.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSpecific.FormattingEnabled = true;
			this.cmbSpecific.Items.AddRange(new object[] {
            "Specific",
            "All"});
			this.cmbSpecific.Location = new System.Drawing.Point(20, 30);
			this.cmbSpecific.Name = "cmbSpecific";
			this.cmbSpecific.Size = new System.Drawing.Size(265, 40);
			this.cmbSpecific.TabIndex = 17;
			this.cmbSpecific.Text = "All";
			this.cmbSpecific.SelectedIndexChanged += new System.EventHandler(this.cmbSpecific_SelectedIndexChanged);
			// 
			// cmbCollected
			// 
			this.cmbCollected.AutoSize = false;
			this.cmbCollected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCollected.FormattingEnabled = true;
			this.cmbCollected.Items.AddRange(new object[] {
            "Actual Collected",
            "Uncollected Estimate",
            "Both"});
			this.cmbCollected.Location = new System.Drawing.Point(229, 90);
			this.cmbCollected.Name = "cmbCollected";
			this.cmbCollected.Size = new System.Drawing.Size(276, 40);
			this.cmbCollected.TabIndex = 19;
			this.cmbCollected.Text = "Actual Collected";
			// 
			// lblCollected
			// 
			this.lblCollected.AutoSize = true;
			this.lblCollected.Location = new System.Drawing.Point(30, 104);
			this.lblCollected.Name = "lblCollected";
			this.lblCollected.Size = new System.Drawing.Size(147, 15);
			this.lblCollected.TabIndex = 20;
			this.lblCollected.Text = "AMOUNTS TO INCLUDE";
			// 
			// cmbDetail
			// 
			this.cmbDetail.AutoSize = false;
			this.cmbDetail.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDetail.FormattingEnabled = true;
			this.cmbDetail.Items.AddRange(new object[] {
            "Detail",
            "Summary"});
			this.cmbDetail.Location = new System.Drawing.Point(229, 30);
			this.cmbDetail.Name = "cmbDetail";
			this.cmbDetail.Size = new System.Drawing.Size(276, 40);
			this.cmbDetail.TabIndex = 21;
			this.cmbDetail.Text = "Summary";
			// 
			// lblDetail
			// 
			this.lblDetail.AutoSize = true;
			this.lblDetail.Location = new System.Drawing.Point(30, 44);
			this.lblDetail.Name = "lblDetail";
			this.lblDetail.Size = new System.Drawing.Size(95, 15);
			this.lblDetail.TabIndex = 22;
			this.lblDetail.Text = "REPORT TYPE";
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.vsStyle);
			this.Frame4.Controls.Add(this.cmbAllStyle);
			this.Frame4.Location = new System.Drawing.Point(30, 150);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(475, 411);
			this.Frame4.TabIndex = 18;
			this.Frame4.Text = "Vehicle Style";
			// 
			// vsStyle
			// 
			this.vsStyle.AllowSelection = false;
			this.vsStyle.AllowUserToResizeColumns = false;
			this.vsStyle.AllowUserToResizeRows = false;
			this.vsStyle.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsStyle.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsStyle.BackColorBkg = System.Drawing.Color.Empty;
			this.vsStyle.BackColorFixed = System.Drawing.Color.Empty;
			this.vsStyle.BackColorSel = System.Drawing.Color.Empty;
			this.vsStyle.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsStyle.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsStyle.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsStyle.ColumnHeadersHeight = 30;
			this.vsStyle.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsStyle.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsStyle.DragIcon = null;
			this.vsStyle.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsStyle.ExtendLastCol = true;
			this.vsStyle.FixedCols = 0;
			this.vsStyle.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsStyle.FrozenCols = 0;
			this.vsStyle.GridColor = System.Drawing.Color.Empty;
			this.vsStyle.GridColorFixed = System.Drawing.Color.Empty;
			this.vsStyle.Location = new System.Drawing.Point(20, 90);
			this.vsStyle.Name = "vsStyle";
			this.vsStyle.OutlineCol = 0;
			this.vsStyle.ReadOnly = true;
			this.vsStyle.RowHeadersVisible = false;
			this.vsStyle.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsStyle.RowHeightMin = 0;
			this.vsStyle.Rows = 1;
			this.vsStyle.ScrollTipText = null;
			this.vsStyle.ShowColumnVisibilityMenu = false;
			this.vsStyle.ShowFocusCell = false;
			this.vsStyle.Size = new System.Drawing.Size(435, 301);
			this.vsStyle.StandardTab = true;
			this.vsStyle.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsStyle.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsStyle.TabIndex = 22;
			this.vsStyle.KeyDown += new Wisej.Web.KeyEventHandler(this.vsStyle_KeyDownEvent);
			this.vsStyle.Click += new System.EventHandler(this.vsStyle_ClickEvent);
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.fraSpecificClass);
			this.Frame3.Controls.Add(this.cmbSpecific);
			this.Frame3.Location = new System.Drawing.Point(525, 30);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(305, 150);
			this.Frame3.TabIndex = 13;
			this.Frame3.Text = "Vehicle Class";
			// 
			// fraSpecificClass
			// 
			this.fraSpecificClass.AppearanceKey = "groupBoxNoBorders";
			this.fraSpecificClass.Controls.Add(this.cboClass);
			this.fraSpecificClass.Enabled = false;
			this.fraSpecificClass.Location = new System.Drawing.Point(20, 90);
			this.fraSpecificClass.Name = "fraSpecificClass";
			this.fraSpecificClass.Size = new System.Drawing.Size(265, 40);
			this.fraSpecificClass.TabIndex = 16;
			// 
			// cboClass
			// 
			this.cboClass.AutoSize = false;
			this.cboClass.BackColor = System.Drawing.SystemColors.Window;
			this.cboClass.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboClass.FormattingEnabled = true;
			this.cboClass.Location = new System.Drawing.Point(0, 0);
			this.cboClass.Name = "cboClass";
			this.cboClass.Size = new System.Drawing.Size(265, 40);
			this.cboClass.TabIndex = 17;
			// 
			// fraRange
			// 
			this.fraRange.Controls.Add(this.cboEndMonth);
			this.fraRange.Controls.Add(this.cboEndYear);
			this.fraRange.Controls.Add(this.cboStartYear);
			this.fraRange.Controls.Add(this.cboStartMonth);
			this.fraRange.Controls.Add(this.lblTo);
			this.fraRange.Location = new System.Drawing.Point(30, 580);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(669, 90);
			this.fraRange.TabIndex = 3;
			this.fraRange.Text = "Date Range";
			// 
			// cboEndMonth
			// 
			this.cboEndMonth.AutoSize = false;
			this.cboEndMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndMonth.FormattingEnabled = true;
			this.cboEndMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
			this.cboEndMonth.Location = new System.Drawing.Point(354, 30);
			this.cboEndMonth.Name = "cboEndMonth";
			this.cboEndMonth.Size = new System.Drawing.Size(193, 40);
			this.cboEndMonth.TabIndex = 7;
			// 
			// cboEndYear
			// 
			this.cboEndYear.AutoSize = false;
			this.cboEndYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndYear.FormattingEnabled = true;
			this.cboEndYear.Location = new System.Drawing.Point(557, 30);
			this.cboEndYear.Name = "cboEndYear";
			this.cboEndYear.Size = new System.Drawing.Size(92, 40);
			this.cboEndYear.TabIndex = 6;
			// 
			// cboStartYear
			// 
			this.cboStartYear.AutoSize = false;
			this.cboStartYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboStartYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStartYear.FormattingEnabled = true;
			this.cboStartYear.Location = new System.Drawing.Point(223, 30);
			this.cboStartYear.Name = "cboStartYear";
			this.cboStartYear.Size = new System.Drawing.Size(92, 40);
			this.cboStartYear.TabIndex = 5;
			// 
			// cboStartMonth
			// 
			this.cboStartMonth.AutoSize = false;
			this.cboStartMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboStartMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStartMonth.FormattingEnabled = true;
			this.cboStartMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
			this.cboStartMonth.Location = new System.Drawing.Point(20, 30);
			this.cboStartMonth.Name = "cboStartMonth";
			this.cboStartMonth.Size = new System.Drawing.Size(193, 40);
			this.cboStartMonth.TabIndex = 4;
			// 
			// lblTo
			// 
			this.lblTo.Location = new System.Drawing.Point(325, 46);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(19, 24);
			this.lblTo.TabIndex = 8;
			this.lblTo.Text = "TO";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(279, 30);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(147, 48);
			this.cmdFilePreview.TabIndex = 0;
			this.cmdFilePreview.Text = "Print Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(1276, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(46, 24);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmSetupExciseTaxEstimate
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1000, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSetupExciseTaxEstimate";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Excise Tax Report";
			this.Load += new System.EventHandler(this.frmSetupExciseTaxEstimate_Load);
			this.Activated += new System.EventHandler(this.frmSetupExciseTaxEstimate_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupExciseTaxEstimate_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsStyle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificClass)).EndInit();
			this.fraSpecificClass.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdFilePreview;
        private FCButton cmdFilePrint;
    }
}