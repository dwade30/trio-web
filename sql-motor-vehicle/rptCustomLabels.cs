//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports.Document.Section;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptCustomLabels.
	/// </summary>
	public partial class rptCustomLabels : BaseSectionReport
	{
		public rptCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Custom Labels";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptCustomLabels_ReportEnd;
		}

        private void RptCustomLabels_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static rptCustomLabels InstancePtr
		{
			get
			{
				return (rptCustomLabels)Sys.GetInstance(typeof(rptCustomLabels));
			}
		}

		protected rptCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               09/12/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               08/27/2003              *
		// ********************************************************
		// THIS REPORT IS TOTOALLY NOT GENERIC AND HAS BEEN ALTERED FOR THIS TWCL
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rsMort = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As int	OnWrite(int, double)
		float intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		bool boolEmpty;
		string strLeftAdjustment;
		int lngVertAdjust;
		int intTypeOfLabel;
		string strReport;
		bool blnLocation;
		int lngTotalCopies;
		int lngCurrentCopy;
		bool blnGoToAck;
		int RowCounter;
		// kk05162017 tromv-1115  Change from Integer to Long
		bool blnFirstRecord;
		bool blnPrintingSecondLabel;
		clsPrintLabel labLabels = new clsPrintLabel();
        private float lngPrintWidth;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			if (strReport == "REMINDERLABELS")
			{
				CheckAgain:
				;
				if (blnFirstRecord || blnPrintingSecondLabel)
				{
					blnFirstRecord = false;
				}
				else
				{
					RowCounter += 1;
				}
				if (blnPrintingSecondLabel)
				{
					eArgs.EOF = false;
				}
				else
				{
					if (RowCounter >= frmReminder.InstancePtr.vsVehicles.Rows)
					{
						eArgs.EOF = true;
					}
					else
					{
						//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
						//if (FCConvert.ToBoolean(frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 1)) == true)
						if (FCConvert.CBool(frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 1)) == true)
						{
							// do nothing
						}
						else
						{
							goto CheckAgain;
						}
						blnPrintingSecondLabel = false;
						eArgs.EOF = false;
					}
				}
			}
			else
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
				}
				else
				{
					rsData.MoveNext();
				}
				eArgs.EOF = rsData.EndOfFile();
			}
		}
		// vbPorter upgrade warning: intLabelType As int	OnWriteFCConvert.ToInt32(
		public void Init(ref string strSQL, string strReportType, int intLabelType, ref string strPrinterName, ref string strFontToUse, bool blnFromAck = false)
		{
			string strDBName = "";
			int intReturn;
			int x;
			bool boolUseFont;
			double dblLabelsAdjustment;
			//clsDRWrapper rsAdjustInfo = new clsDRWrapper();
			int cnt;
			blnGoToAck = blnFromAck;
			// rsAdjustInfo.OpenRecordset "SELECT * FROM DefaultInfo"
			// If rsAdjustInfo.EndOfFile <> True And rsAdjustInfo.BeginningOfFile <> True Then
			// dblLabelsAdjustment = rsAdjustInfo.Fields("LabelAdjustment")
			// Else
			dblLabelsAdjustment = Conversion.Val(MotorVehicle.GetMVVariable("LaserLabelAdjustment"));
			// End If
			strReport = strReportType;
			strFont = strFontToUse;
			this.Document.Printer.PrinterName = strPrinterName;
			if (strSQL != "")
			{
				rsData.OpenRecordset(strSQL);
			}
			intTypeOfLabel = intLabelType;
			RowCounter = 1;
			// make them choose the printer first if you have to use a custom form
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			boolDifferentPageSize = false;
			strLeftAdjustment = "";
			lngVertAdjust = 0;
			int intIndex;
			intIndex = labLabels.Get_IndexFromID(intTypeOfLabel);
			if (labLabels.Get_IsDymoLabel(intIndex))
			{
				switch (labLabels.Get_ID(intIndex))
				{
					case modLabels.CNSTLBLTYPEDYMO30256:
						{
							strPrinterName = this.Document.Printer.PrinterName;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							;
							this.PageSettings.Margins.Top = 1.128F;
							this.PageSettings.Margins.Bottom = 0;
							this.PageSettings.Margins.Right = 0;
							this.PageSettings.Margins.Left = 0.128F;
							modGlobalConstants.Statics.PrintWidth = 4F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = 4F;
							lngPrintWidth = 4F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							Detail.Height = 2.3125F - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440F;
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = 4F;
							PageSettings.PaperWidth = 2.31F;
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							break;
						}
					case modLabels.CNSTLBLTYPEDYMO30252:
						{
							strPrinterName = this.Document.Printer.PrinterName;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							this.PageSettings.Margins.Top = 0.128F;
							this.PageSettings.Margins.Bottom = 0;
							this.PageSettings.Margins.Right = 0;
							this.PageSettings.Margins.Left = 0.128F;
							modGlobalConstants.Statics.PrintWidth = 3.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = 3.5F;
							lngPrintWidth = 3.5F - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							Detail.Height = 1.1F - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440F;
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = 3.5F;
							PageSettings.PaperWidth = 1.1F;
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							break;
						}
				}
			}
			else if (labLabels.Get_IsLaserLabel(intIndex))
			{
				this.PageSettings.Margins.Top = 0.5F;
				this.PageSettings.Margins.Top += FCConvert.ToSingle(dblLabelsAdjustment) * 270 / 1440F;
				this.PageSettings.Margins.Left = labLabels.Get_LeftMargin(intIndex);
				this.PageSettings.Margins.Right = labLabels.Get_RightMargin(intIndex);
				modGlobalConstants.Statics.PrintWidth = labLabels.Get_PageWidth(intIndex) - labLabels.Get_LeftMargin(intIndex) - labLabels.Get_RightMargin(intIndex);
				intLabelWidth = labLabels.Get_LabelWidth(intIndex);
				Detail.Height = labLabels.Get_LabelHeight(intIndex) + labLabels.Get_VerticalSpace(intIndex);
				if (labLabels.Get_LabelsWide(intIndex) > 0)
				{
					Detail.ColumnCount = labLabels.Get_LabelsWide(intIndex);
					Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intIndex);
				}
				lngPrintWidth = FCConvert.ToInt32(modGlobalConstants.Statics.PrintWidth);
			}
			
			CreateDataFields();
			frmReportViewer.InstancePtr.Init(this, strPrinterName);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252)
            {

                for (int cnt = 0; cnt <= this.Document.Printer.PaperSizes.Count - 1; cnt++)
				{
					switch (intTypeOfLabel)
					{
						case modLabels.CNSTLBLTYPEDYMO30252:
							{
								if (fecherFoundation.Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30252 ADDRESS")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30256:
							{
								if (fecherFoundation.Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30256 SHIPPING")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
					}
					//end switch
				}
				// cnt
            }
			else
			{
                this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomFormat", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
			}
			blnLocation = false;
			blnFirstRecord = true;
			RowCounter = 1;
		}

		private void CreateDataFields()
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow;
			int intNumber;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			intNumber = 4;
			for (intRow = 1; intRow <= intNumber; intRow++)
			{
				NewField = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtData" + FCConvert.ToString(intRow));
				NewField.CanGrow = false;
				//NewField.Name = "txtData" + FCConvert.ToString(intRow);
				NewField.Top = (((intRow - 1) * 225) + lngVertAdjust) / 1440F;
				NewField.Left = 144 / 1440F;
				// one space
				NewField.Width = (FCConvert.ToInt32(modGlobalConstants.Statics.PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440F;
				NewField.Height = 225 / 1440F;
				NewField.Text = string.Empty;
				NewField.MultiLine = false;
                NewField.WrapMode = WrapMode.WordWrap;
				NewField.Font = fecherFoundation.Strings.Trim(strFont) != string.Empty ? new Font(strFont, NewField.Font.Size) : new Font(lblFont.Font.Name, NewField.Font.Size);
                
                if (intRow != 1) 
                    continue;

                NewField = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtAcct");
                NewField.CanGrow = false;
                //NewField.Name = "txtAcct";
                NewField.Top = (((intRow - 1) * 225) + lngVertAdjust) / 1440F;
                NewField.Left = (FCConvert.ToInt32(modGlobalConstants.Statics.PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 1145 / 1440F;
                NewField.Width = 999 / 1440F;
                NewField.Height = 225 / 1440F;
                NewField.Text = string.Empty;
                NewField.MultiLine = false;
                NewField.WrapMode = WrapMode.WordWrap;
                if (fecherFoundation.Strings.Trim(strFont) != string.Empty)
                {
                    NewField.Font = new Font(strFont, NewField.Font.Size);
                }
                else
                {
                    NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
                }
                //Detail.Controls.Add(NewField);
            }
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			
		}
		
		private void PrintLabels()
		{
			// this will fill in the fields for labels
			int intControl;
			int intRow;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			// vbPorter upgrade warning: Amount As double	OnWriteFCConvert.ToDecimal(
			double Amount = 0;
			Decimal amount1 = 0;
			Decimal amount2 = 0;
			Decimal amount3 = 0;
			switch (strReport)
            {
                case "FLEETS":
                {
                    str1 = fecherFoundation.Strings.UCase(FCConvert.ToString(rsData.Get_Fields_String("FullName")));
                    str2 = FCConvert.ToString(rsData.Get_Fields_String("Address"));
                    var cityStateZip = fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("City"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields("State"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip")));
                    var zip4 = FCConvert.ToString(rsData.Get_Fields_String("Zip4"));

                    if (zip4 != "")
                    {
                        str3 = cityStateZip + "-" + fecherFoundation.Strings.Trim(zip4);
                    }
                    else
                    {
                        str3 = cityStateZip;
                    }

                    break;
                }
                case "REMINDERLABELS":
                {
                    if (!blnPrintingSecondLabel)
                    {
                        str1 = "   Plate: " + frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 3);
                        str2 = "Vehicle: " + fecherFoundation.Strings.Trim(frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 4)) + " " + fecherFoundation.Strings.Trim(frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 5)) + " " + fecherFoundation.Strings.Trim(frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 6));
                        if (frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 7) != "")
                        {
                            amount1 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 7));
                        }
                        if (frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 8) != "")
                        {
                            amount2 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 8));
                        }
                        if (frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 9) != "")
                        {
                            amount3 = FCConvert.ToDecimal(frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 9));
                        }
                        if (frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 7) != "")
                        {
                            Amount = FCConvert.ToDouble(amount1 + amount2 + amount3);
                            str4 = "Amount:  " + Strings.Format(Amount, "$#,##0.00");
                        }
                        else
                        {
                            str4 = "Amount:  Call Office";
                        }
                        if (!MotorVehicle.Statics.rsReport.FindFirstRecord("ID", Conversion.Val(frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 0))))
                        {
                            // do nothing
                        }
                        else
                        {
                            str3 = "Expires: " + Strings.Format(MotorVehicle.Statics.rsReport.Get_Fields_DateTime("ExpireDate"), "MM/dd/yyyy");
                        }
                    }
                    else
                    {
                        str1 = "           " + frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 3);
                        str2 = frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 10);
                        if (!MotorVehicle.Statics.rsReport.FindFirstRecord("ID", Conversion.Val(frmReminder.InstancePtr.vsVehicles.TextMatrix(RowCounter, 0))))
                        {
                            // do nothing
                        }
                        else
                        {
                            var cityStateZip = fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("City"))) + ", " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields("State"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("Zip")));

                            if (!fecherFoundation.FCUtils.IsNull(MotorVehicle.Statics.rsReport.Get_Fields_String("Address")))
                            {
                                str3 = FCConvert.ToString(MotorVehicle.Statics.rsReport.Get_Fields_String("Address"));
                                str4 = cityStateZip;
                            }
                            else
                            {
                                str3 = cityStateZip;
                                str4 = "";
                            }
                        }
                    }
                    str5 = "";
                    blnPrintingSecondLabel = blnPrintingSecondLabel == false;

                    break;
                }
            }
			if (intTypeOfLabel == 2)
			{
				// small labels
				if (str1.Length > 18)
				{
					str5 = str4;
					str4 = str3;
					str3 = str2;
					str2 = Strings.Right(str1, str1.Length - 18);
					str1 = Strings.Left(str1, 18);
				}
			}
			else
			{
				if (str1.Length > 35)
				{
					// 4" labels
					str5 = str4;
					str4 = str3;
					str3 = str2;
					str2 = Strings.Right(str1, str1.Length - 35);
					str1 = Strings.Left(str1, 35);
				}
			}
			// condense the labels if some are blank
			// If boolMort Then
			if (fecherFoundation.Strings.Trim(str4) == string.Empty)
			{
				str4 = str5;
				str5 = "";
			}
			// End If
			if (fecherFoundation.Strings.Trim(str3) == string.Empty)
			{
				str3 = str4;
				if (boolMort)
				{
					str4 = str5;
					str5 = "";
				}
				else
				{
					str4 = "";
				}
			}
			if (fecherFoundation.Strings.Trim(str2) == string.Empty)
			{
				str2 = str3;
				str3 = str4;
				if (boolMort)
				{
					str4 = str5;
					str5 = "";
				}
				else
				{
					str4 = "";
				}
			}
			if (fecherFoundation.Strings.Trim(str1) == string.Empty)
			{
				str1 = str2;
				str2 = str3;
				str3 = str4;
				if (boolMort)
				{
					str4 = str5;
					str5 = "";
				}
				else
				{
					str4 = "";
				}
			}
			for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
			{
				if (fecherFoundation.Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
				{
					intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
                    var ctl = Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox;

                    if (ctl == null) continue;

                    switch (intRow)
					{
						case 1:
							{
								ctl.Text = strLeftAdjustment + str1;
								break;
							}
						case 2:
							{
								ctl.Text = strLeftAdjustment + str2;
								break;
							}
						case 3:
							{
								ctl.Text = strLeftAdjustment + str3;
								break;
							}
						case 4:
							{
								ctl.Text = strLeftAdjustment + str4;
								break;
							}
						case 5:
							{
								ctl.Text = strLeftAdjustment + str5;
								break;
							}
					}
					//end switch
				}
			}
			// intControl
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			PrintLabels();
		}

		private void rptCustomLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCustomLabels properties;
			//rptCustomLabels.Caption	= "Custom Labels";
			//rptCustomLabels.Icon	= "rptCustomLabels.dsx":0000";
			//rptCustomLabels.Left	= 0;
			//rptCustomLabels.Top	= 0;
			//rptCustomLabels.Width	= 11880;
			//rptCustomLabels.Height	= 8595;
			//rptCustomLabels.StartUpPosition	= 3;
			//rptCustomLabels.SectionData	= "rptCustomLabels.dsx":058A;
			//End Unmaped Properties
		}
	}
}
