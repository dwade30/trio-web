﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmMultiplePlates : BaseForm
	{
		public frmMultiplePlates()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmMultiplePlates InstancePtr
		{
			get
			{
				return (frmMultiplePlates)Sys.GetInstance(typeof(frmMultiplePlates));
			}
		}

		protected frmMultiplePlates _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int IDCol;
		int ClassCol;
		int OwnerCol;
		int MakeCol;
		int ModelCol;
		int ExpirationCol;

		private void frmMultiplePlates_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			this.Refresh();
		}

		private void frmMultiplePlates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMultiplePlates properties;
			//frmMultiplePlates.FillStyle	= 0;
			//frmMultiplePlates.ScaleWidth	= 5880;
			//frmMultiplePlates.ScaleHeight	= 3810;
			//frmMultiplePlates.LinkTopic	= "Form2";
			//frmMultiplePlates.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			IDCol = 0;
			ClassCol = 1;
			OwnerCol = 2;
			MakeCol = 3;
			ModelCol = 4;
			ExpirationCol = 5;
			vsVehicles.ColHidden(IDCol, true);
			vsVehicles.TextMatrix(0, ClassCol, "Class");
			vsVehicles.TextMatrix(0, OwnerCol, "Owner");
			vsVehicles.TextMatrix(0, MakeCol, "Make");
			vsVehicles.TextMatrix(0, ModelCol, "Model");
			vsVehicles.TextMatrix(0, ExpirationCol, "Expires");
            //vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsVehicles.Cols - 1, 4);
            vsVehicles.ColAlignment(ClassCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsVehicles.ColAlignment(OwnerCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsVehicles.ColAlignment(MakeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsVehicles.ColAlignment(ModelCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsVehicles.ColAlignment(ExpirationCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsVehicles.ColWidth(ClassCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.0908265));
			vsVehicles.ColWidth(OwnerCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.3814713));
			vsVehicles.ColWidth(MakeCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1089918));
			vsVehicles.ColWidth(ModelCol, FCConvert.ToInt32(vsVehicles.WidthOriginal * 0.1453224));
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmMultiplePlates_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				vsVehicles_DblClick(null, null);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			vsVehicles_DblClick(null, null);
		}

		private void vsVehicles_DblClick(object sender, System.EventArgs e)
		{
            // if (vsVehicles.MouseRow > 0)
            if (vsVehicles.Row > 0)
			{
				// MotorVehicle.Statics.ReturnFromListBox = vsVehicles.TextMatrix(vsVehicles.MouseRow, IDCol);
                MotorVehicle.Statics.ReturnFromListBox = vsVehicles.TextMatrix(vsVehicles.Row, IDCol);
                Close();
			}
			else
			{
				MessageBox.Show("Please select an item from the list box.", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}
	}
}
