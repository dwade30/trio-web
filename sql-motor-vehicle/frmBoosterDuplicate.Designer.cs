//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmBoosterDuplicate.
	/// </summary>
	partial class frmBoosterDuplicate
	{
		public fecherFoundation.FCTextBox txtPermit;
		public fecherFoundation.FCComboBox cboOriginalBooster;
		public fecherFoundation.FCLabel lblFee;
		public fecherFoundation.FCLabel lblFeeAmount;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtPermit = new fecherFoundation.FCTextBox();
			this.cboOriginalBooster = new fecherFoundation.FCComboBox();
			this.lblFee = new fecherFoundation.FCLabel();
			this.lblFeeAmount = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 237);
			this.BottomPanel.Size = new System.Drawing.Size(764, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtPermit);
			this.ClientArea.Controls.Add(this.cboOriginalBooster);
			this.ClientArea.Controls.Add(this.lblFee);
			this.ClientArea.Controls.Add(this.lblFeeAmount);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(764, 177);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(764, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(207, 30);
			this.HeaderText.Text = "Duplicate Booster";
			// 
			// txtPermit
			// 
			this.txtPermit.AutoSize = false;
			this.txtPermit.BackColor = System.Drawing.SystemColors.Window;
			this.txtPermit.LinkItem = null;
			this.txtPermit.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPermit.LinkTopic = null;
			this.txtPermit.Location = new System.Drawing.Point(211, 125);
			this.txtPermit.Name = "txtPermit";
			this.txtPermit.Size = new System.Drawing.Size(215, 40);
			this.txtPermit.TabIndex = 3;
			this.txtPermit.ToolTipText = null;
			this.txtPermit.Validating += new System.ComponentModel.CancelEventHandler(this.txtPermit_Validating);
			// 
			// cboOriginalBooster
			// 
			this.cboOriginalBooster.AutoSize = false;
			this.cboOriginalBooster.BackColor = System.Drawing.SystemColors.Window;
			this.cboOriginalBooster.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboOriginalBooster.FormattingEnabled = true;
			this.cboOriginalBooster.Location = new System.Drawing.Point(211, 65);
			this.cboOriginalBooster.Name = "cboOriginalBooster";
			this.cboOriginalBooster.Size = new System.Drawing.Size(527, 40);
			this.cboOriginalBooster.TabIndex = 0;
			this.cboOriginalBooster.Text = "Combo1";
			this.cboOriginalBooster.ToolTipText = null;
			// 
			// lblFee
			// 
			this.lblFee.Location = new System.Drawing.Point(30, 30);
			this.lblFee.Name = "lblFee";
			this.lblFee.Size = new System.Drawing.Size(27, 15);
			this.lblFee.TabIndex = 5;
			this.lblFee.Text = "FEE";
			this.lblFee.ToolTipText = null;
			// 
			// lblFeeAmount
			// 
			this.lblFeeAmount.Location = new System.Drawing.Point(87, 30);
			this.lblFeeAmount.Name = "lblFeeAmount";
			this.lblFeeAmount.Size = new System.Drawing.Size(66, 15);
			this.lblFeeAmount.TabIndex = 4;
			this.lblFeeAmount.Text = "$0.00";
			this.lblFeeAmount.ToolTipText = null;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 139);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(120, 17);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "DUPLICATE PERMIT";
			this.Label2.ToolTipText = null;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 79);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(120, 17);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "ORIGINAL BOOSTER";
			this.Label1.ToolTipText = null;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(187, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(100, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Process";
			this.cmdProcessSave.ToolTipText = null;
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmBoosterDuplicate
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(764, 345);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBoosterDuplicate";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Duplicate Booster";
			this.Load += new System.EventHandler(this.frmBoosterDuplicate_Load);
			this.Activated += new System.EventHandler(this.frmBoosterDuplicate_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBoosterDuplicate_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcessSave;
	}
}