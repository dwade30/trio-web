//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSwintecUseTaxCertificate.
	/// </summary>
	public partial class rptSwintecUseTaxCertificate : BaseSectionReport
	{
		public rptSwintecUseTaxCertificate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Use Tax Certificate";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptSwintecUseTaxCertificate InstancePtr
		{
			get
			{
				return (rptSwintecUseTaxCertificate)Sys.GetInstance(typeof(rptSwintecUseTaxCertificate));
			}
		}

		protected rptSwintecUseTaxCertificate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSwintecUseTaxCertificate	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// Last Updated:              01/28/2002
		private void GetInformation()
		{
			// this sub will get all of the information needed and fill the fields on the Form
			double dblTaxAmount;
			double dblPurchasePrice;
			double dblAllowance;
			clsDRWrapper rsUseTax = new clsDRWrapper();
			int TB;
			string SellerAddress;
			string ExmptCode;
			string Owner;
			string LienHolder1 = "";
			int x;
			// 
			rsUseTax.OpenRecordset("SELECT * FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID), "TWMV0000.vb1");
			if (rsUseTax.EndOfFile() != true && rsUseTax.BeginningOfFile() != true)
			{
				rsUseTax.MoveLast();
				rsUseTax.MoveFirst();
			}
			else
			{
				MotorVehicle.Statics.blnPrintUseTax = false;
				this.Cancel();
				return;
			}
			fldType1.Text = rsUseTax.Get_Fields_String("PurchasedType");
			fldType2.Text = rsUseTax.Get_Fields_String("TradedType");
			fldMake1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
			fldModel1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");
			fldYear1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
			if (Conversion.Val(rsUseTax.Get_Fields_String("TradedYear")) != 0)
			{
				fldMake2.Text = rsUseTax.Get_Fields_String("TradedMake");
				fldModel2.Text = rsUseTax.Get_Fields_String("TradedModel");
				fldYear2.Text = rsUseTax.Get_Fields_String("TradedYear");
			}
			if (MotorVehicle.Statics.FleetUseTax)
			{
				fldVin1.Text = "S.A.L.";
			}
			else
			{
				fldVin1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin");
			}
			fldVin2.Text = rsUseTax.Get_Fields_String("TradedVIN");
			fldSellerName.Text = rsUseTax.Get_Fields_String("SellerName");
			fldDateofTransfer.Text = Strings.Format(rsUseTax.Get_Fields_DateTime("TradedDate"), "MM/dd/yyyy");
			SellerAddress = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerAddress1"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerCity"))) + " " + rsUseTax.Get_Fields_String("SellerState") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerZipAndZip4")));
			fldSellerAddress.Text = fecherFoundation.Strings.Trim(SellerAddress);
			dblPurchasePrice = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("PurchasePrice")) == false)
				dblPurchasePrice = Conversion.Val(rsUseTax.Get_Fields_Decimal("PurchasePrice"));
			fldFullPurchasePrice.Text = Strings.Format(dblPurchasePrice, "#,###");
			dblAllowance = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("Allowance")) == false)
				dblAllowance = Conversion.Val(rsUseTax.Get_Fields_Decimal("Allowance"));
			fldAllowance.Text = Strings.Format(dblAllowance, "#,###");
			fldNetAmount.Text = Strings.Format(dblPurchasePrice - dblAllowance, "#,###");
			dblTaxAmount = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("TaxAmount")) == false)
				dblTaxAmount = Conversion.Val(rsUseTax.Get_Fields_Decimal("TaxAmount"));
			fldUseTax.Text = Strings.Format(dblTaxAmount, "#,##0.00");
			ExmptCode = FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptCode"));
			if (ExmptCode == "A")
			{
				fldExemptTypeA.Text = "A";
				fldExemptNumber.Text = rsUseTax.Get_Fields_String("ExemptNumber");
			}
			else if (ExmptCode == "B")
			{
				fldExemptTypeB.Text = "B";
				fldWhereRegistered.Text = rsUseTax.Get_Fields_String("ExemptPreviousState");
				fldRegNumber.Text = rsUseTax.Get_Fields_String("ExemptRegistrationNumber");
				fldDateOfOriginalReg.Text = Strings.Format(rsUseTax.Get_Fields_DateTime("ExemptDate"), "MM/dd/yyyy");
			}
			else if (ExmptCode == "C")
			{
				fldExemptTypeC.Text = "C";
				fldOtherStateTax.Text = rsUseTax.Get_Fields_String("ExemptState");
				fldOtherAmount.Text = Strings.Format(Strings.Format(rsUseTax.Get_Fields_Decimal("ExemptAmountPaid"), "###,###.00"), "@@@@@@@@@@");
			}
			else if (ExmptCode == "D")
			{
				fldExemptTypeD.Text = "D";
				fldVAVeteran.Text = rsUseTax.Get_Fields_String("ExemptVACNumber");
			}
			else if (ExmptCode == "E")
			{
				fldExemptTypeE.Text = "E";
				fldOtherReason.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptDescription")));
			}
			Owner = fecherFoundation.Strings.Trim(MotorVehicle.GetPartyName(MotorVehicle.Statics.rsFinal.Get_Fields_Int32("PartyID1"), true));
			if (Owner != fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields("PurchaserName"))))
			{
				Owner += Strings.StrDup(39, " ");
				fldDifferentOwner.Text = Owner;
			}
			fldLienHolderName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("LH1Name")));
			fldLienHolderAddress.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("LH1Address1"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("LH1City"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("LH1State"))) + " " + fecherFoundation.Strings.RTrim(FCConvert.ToString(rsUseTax.Get_Fields_String("LH1ZipAndZip4")));
			fldPurchaserName.Text = rsUseTax.Get_Fields_String("PurchaserName");
			fldPurchaserSocialSec.Text = rsUseTax.Get_Fields_String("PurchaserSSN");
			fldPurchaserAddress.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("PurchaserAddress")));
			fldPurchaserCity.Text = rsUseTax.Get_Fields_String("PurchaserCity");
			fldPurchaserState.Text = rsUseTax.Get_Fields_String("PurchaserState");
			fldPurchaserZip.Text = rsUseTax.Get_Fields_String("PurchaserZipAndZip4");
			if (MotorVehicle.Statics.FleetUseTax)
			{
				fldClassPlate.Text = "S.A.L.";
			}
			else
			{
				fldClassPlate.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate");
			}
			fldRegDate.Text = Strings.Format(DateTime.Today, "MM/dd/yy");
			fldTaxAmount.Text = Strings.Format(Strings.Format(dblTaxAmount, "###,###.00"), "@@@@@@@@@@");
			fldDatePaid1.Text = Strings.Format(DateTime.Today, "MM/dd/yy");
			rsUseTax.DisposeOf();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Printer tempPrinter = new Printer();
			int x;
			this.Document.Printer.PrinterName = MotorVehicle.Statics.gstrCTAPrinterName;
			/*? For Each */
			foreach (FCPrinter p in FCGlobal.Printers)
			{
				if (p.DeviceName == MotorVehicle.Statics.gstrCTAPrinterName)
				{
					if (tempPrinter.DriverName == "EPSON24")
					{
						for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
						{
							(this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font("Courier 10cpi", (this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Size);
						}
					}
					break;
				}
			}
			/*? Next */
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			GetInformation();
			MotorVehicle.Statics.blnPrintUseTax = true;
		}

		private void rptSwintecUseTaxCertificate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptSwintecUseTaxCertificate properties;
			//rptSwintecUseTaxCertificate.Caption	= "Use Tax Certificate";
			//rptSwintecUseTaxCertificate.Icon	= "rptSwintecUseTacCertificate.dsx":0000";
			//rptSwintecUseTaxCertificate.Left	= 0;
			//rptSwintecUseTaxCertificate.Top	= 0;
			//rptSwintecUseTaxCertificate.Width	= 11880;
			//rptSwintecUseTaxCertificate.Height	= 8595;
			//rptSwintecUseTaxCertificate.StartUpPosition	= 3;
			//rptSwintecUseTaxCertificate.SectionData	= "rptSwintecUseTacCertificate.dsx":058A;
			//End Unmaped Properties
		}
	}
}
