//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmTransitPlate.
	/// </summary>
	partial class frmTransitPlate
	{
		public fecherFoundation.FCCheckBox chkMunicipalVehicle;
		public fecherFoundation.FCCheckBox chkRoundTrip;
		public fecherFoundation.FCFrame fraCodes;
		public fecherFoundation.FCComboBox cboCodesList;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCTextBox txtDestination;
		public fecherFoundation.FCTextBox txtOrigin;
		public fecherFoundation.FCTextBox txtPlate;
		public fecherFoundation.FCFrame fraApplicant;
		public fecherFoundation.FCTextBox txtResidenceCode;
		public fecherFoundation.FCTextBox txtCustomerNumber;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdEdit;
		public Global.T2KDateBox txtDOB;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblCustomerNumber;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblCustomerInfo;
		public fecherFoundation.FCLabel lblDOB;
		public fecherFoundation.FCFrame fraVehicle;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCTextBox txtMake;
		public fecherFoundation.FCTextBox txtModel;
		public fecherFoundation.FCTextBox txtVIN;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblMake;
		public fecherFoundation.FCLabel lblModel;
		public fecherFoundation.FCLabel lblVin;
		public Global.T2KDateBox txtExpireDate;
		public Global.T2KDateBox txtEffectiveDate;
		public fecherFoundation.FCLabel lblDestination;
		public fecherFoundation.FCLabel lblOrigin;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblPermit;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTransitPlate));
			this.chkMunicipalVehicle = new fecherFoundation.FCCheckBox();
			this.chkRoundTrip = new fecherFoundation.FCCheckBox();
			this.fraCodes = new fecherFoundation.FCFrame();
			this.cboCodesList = new fecherFoundation.FCComboBox();
			this.cmdOK = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.txtDestination = new fecherFoundation.FCTextBox();
			this.txtOrigin = new fecherFoundation.FCTextBox();
			this.txtPlate = new fecherFoundation.FCTextBox();
			this.fraApplicant = new fecherFoundation.FCFrame();
			this.txtResidenceCode = new fecherFoundation.FCTextBox();
			this.txtCustomerNumber = new fecherFoundation.FCTextBox();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.cmdEdit = new fecherFoundation.FCButton();
			this.txtDOB = new Global.T2KDateBox();
			this.Label4 = new fecherFoundation.FCLabel();
			this.lblCustomerNumber = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblCustomerInfo = new fecherFoundation.FCLabel();
			this.lblDOB = new fecherFoundation.FCLabel();
			this.fraVehicle = new fecherFoundation.FCFrame();
			this.txtYear = new fecherFoundation.FCTextBox();
			this.txtMake = new fecherFoundation.FCTextBox();
			this.txtModel = new fecherFoundation.FCTextBox();
			this.txtVIN = new fecherFoundation.FCTextBox();
			this.lblYear = new fecherFoundation.FCLabel();
			this.lblMake = new fecherFoundation.FCLabel();
			this.lblModel = new fecherFoundation.FCLabel();
			this.lblVin = new fecherFoundation.FCLabel();
			this.txtExpireDate = new Global.T2KDateBox();
			this.txtEffectiveDate = new Global.T2KDateBox();
			this.lblDestination = new fecherFoundation.FCLabel();
			this.lblOrigin = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblPermit = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkMunicipalVehicle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRoundTrip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCodes)).BeginInit();
			this.fraCodes.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraApplicant)).BeginInit();
			this.fraApplicant.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraVehicle)).BeginInit();
			this.fraVehicle.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(489, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraCodes);
			this.ClientArea.Controls.Add(this.chkMunicipalVehicle);
			this.ClientArea.Controls.Add(this.chkRoundTrip);
			this.ClientArea.Controls.Add(this.txtDestination);
			this.ClientArea.Controls.Add(this.txtOrigin);
			this.ClientArea.Controls.Add(this.txtPlate);
			this.ClientArea.Controls.Add(this.fraApplicant);
			this.ClientArea.Controls.Add(this.fraVehicle);
			this.ClientArea.Controls.Add(this.txtExpireDate);
			this.ClientArea.Controls.Add(this.txtEffectiveDate);
			this.ClientArea.Controls.Add(this.lblDestination);
			this.ClientArea.Controls.Add(this.lblOrigin);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblPermit);
			this.ClientArea.Size = new System.Drawing.Size(489, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(489, 60);
			this.TopPanel.TabIndex = 0;
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(150, 30);
			this.HeaderText.Text = "Transit Plate";
			// 
			// chkMunicipalVehicle
			// 
			this.chkMunicipalVehicle.Location = new System.Drawing.Point(176, 845);
			this.chkMunicipalVehicle.Name = "chkMunicipalVehicle";
			this.chkMunicipalVehicle.Size = new System.Drawing.Size(155, 27);
			this.chkMunicipalVehicle.TabIndex = 13;
			this.chkMunicipalVehicle.Text = "Municipal Vehicle";
			this.chkMunicipalVehicle.ToolTipText = null;
			// 
			// chkRoundTrip
			// 
			this.chkRoundTrip.Location = new System.Drawing.Point(30, 845);
			this.chkRoundTrip.Name = "chkRoundTrip";
			this.chkRoundTrip.Size = new System.Drawing.Size(108, 27);
			this.chkRoundTrip.TabIndex = 12;
			this.chkRoundTrip.Text = "Round Trip";
			this.chkRoundTrip.ToolTipText = null;
			// 
			// fraCodes
			// 
			this.fraCodes.BackColor = System.Drawing.Color.White;
			this.fraCodes.Controls.Add(this.cboCodesList);
			this.fraCodes.Controls.Add(this.cmdOK);
			this.fraCodes.Controls.Add(this.cmdCancel);
			this.fraCodes.Controls.Add(this.lblInstructions);
			this.fraCodes.Location = new System.Drawing.Point(30, 68);
			this.fraCodes.Name = "fraCodes";
			this.fraCodes.Size = new System.Drawing.Size(421, 178);
			this.fraCodes.TabIndex = 14;
			this.fraCodes.Text = "Valid Codes";
			this.fraCodes.Visible = false;
			// 
			// cboCodesList
			// 
			this.cboCodesList.AutoSize = false;
			this.cboCodesList.BackColor = System.Drawing.SystemColors.Window;
			this.cboCodesList.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCodesList.FormattingEnabled = true;
			this.cboCodesList.Location = new System.Drawing.Point(20, 68);
			this.cboCodesList.Name = "cboCodesList";
			this.cboCodesList.Size = new System.Drawing.Size(381, 40);
			this.cboCodesList.TabIndex = 1;
			this.cboCodesList.ToolTipText = null;
			this.cboCodesList.Leave += new System.EventHandler(this.cboCodesList_Leave);
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.Location = new System.Drawing.Point(20, 118);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(67, 40);
			this.cmdOK.TabIndex = 2;
			this.cmdOK.Text = "OK";
			this.cmdOK.ToolTipText = null;
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(117, 118);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(93, 40);
			this.cmdCancel.TabIndex = 3;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.ToolTipText = null;
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(20, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(381, 31);
			this.lblInstructions.TabIndex = 0;
			this.lblInstructions.Text = "PLEASE SELECT THE CODE YOU WISH TO USE AND CLICK THE OK BUTTON";
			this.lblInstructions.ToolTipText = null;
			// 
			// txtDestination
			// 
			this.txtDestination.AutoSize = false;
			this.txtDestination.BackColor = System.Drawing.SystemColors.Window;
			this.txtDestination.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtDestination.LinkItem = null;
			this.txtDestination.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDestination.LinkTopic = null;
			this.txtDestination.Location = new System.Drawing.Point(141, 795);
			this.txtDestination.MaxLength = 30;
			this.txtDestination.Name = "txtDestination";
			this.txtDestination.Size = new System.Drawing.Size(310, 40);
			this.txtDestination.TabIndex = 11;
			this.txtDestination.ToolTipText = null;
			// 
			// txtOrigin
			// 
			this.txtOrigin.AutoSize = false;
			this.txtOrigin.BackColor = System.Drawing.SystemColors.Window;
			this.txtOrigin.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtOrigin.LinkItem = null;
			this.txtOrigin.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOrigin.LinkTopic = null;
			this.txtOrigin.Location = new System.Drawing.Point(141, 745);
			this.txtOrigin.MaxLength = 30;
			this.txtOrigin.Name = "txtOrigin";
			this.txtOrigin.Size = new System.Drawing.Size(310, 40);
			this.txtOrigin.TabIndex = 9;
			this.txtOrigin.ToolTipText = null;
			// 
			// txtPlate
			// 
			this.txtPlate.AutoSize = false;
			this.txtPlate.BackColor = System.Drawing.SystemColors.Window;
			this.txtPlate.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtPlate.LinkItem = null;
			this.txtPlate.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPlate.LinkTopic = null;
			this.txtPlate.Location = new System.Drawing.Point(165, 130);
			this.txtPlate.MaxLength = 6;
			this.txtPlate.Name = "txtPlate";
			this.txtPlate.Size = new System.Drawing.Size(130, 40);
			this.txtPlate.TabIndex = 5;
			this.txtPlate.ToolTipText = null;
			this.txtPlate.Validating += new System.ComponentModel.CancelEventHandler(this.txtPlate_Validating);
			// 
			// fraApplicant
			// 
			this.fraApplicant.Controls.Add(this.txtResidenceCode);
			this.fraApplicant.Controls.Add(this.txtCustomerNumber);
			this.fraApplicant.Controls.Add(this.cmdSearch);
			this.fraApplicant.Controls.Add(this.cmdEdit);
			this.fraApplicant.Controls.Add(this.txtDOB);
			this.fraApplicant.Controls.Add(this.Label4);
			this.fraApplicant.Controls.Add(this.lblCustomerNumber);
			this.fraApplicant.Controls.Add(this.Label3);
			this.fraApplicant.Controls.Add(this.lblCustomerInfo);
			this.fraApplicant.Controls.Add(this.lblDOB);
			this.fraApplicant.Location = new System.Drawing.Point(30, 180);
			this.fraApplicant.Name = "fraApplicant";
			this.fraApplicant.Size = new System.Drawing.Size(421, 275);
			this.fraApplicant.TabIndex = 6;
			this.fraApplicant.Text = "Applicant Information";
			// 
			// txtResidenceCode
			// 
			this.txtResidenceCode.AutoSize = false;
			this.txtResidenceCode.BackColor = System.Drawing.SystemColors.Window;
			this.txtResidenceCode.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtResidenceCode.LinkItem = null;
			this.txtResidenceCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtResidenceCode.LinkTopic = null;
			this.txtResidenceCode.Location = new System.Drawing.Point(153, 215);
			this.txtResidenceCode.MaxLength = 5;
			this.txtResidenceCode.Name = "txtResidenceCode";
			this.txtResidenceCode.Size = new System.Drawing.Size(130, 40);
			this.txtResidenceCode.TabIndex = 9;
			this.txtResidenceCode.ToolTipText = null;
			// 
			// txtCustomerNumber
			// 
			this.txtCustomerNumber.AutoSize = false;
			this.txtCustomerNumber.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
			this.txtCustomerNumber.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtCustomerNumber.LinkItem = null;
			this.txtCustomerNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCustomerNumber.LinkTopic = null;
			this.txtCustomerNumber.Location = new System.Drawing.Point(153, 30);
			this.txtCustomerNumber.Name = "txtCustomerNumber";
			this.txtCustomerNumber.Size = new System.Drawing.Size(148, 40);
			this.txtCustomerNumber.TabIndex = 1;
			this.txtCustomerNumber.ToolTipText = null;
			this.txtCustomerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCustomerNumber_KeyPress);
			this.txtCustomerNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustomerNumber_Validating);
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
            //this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.ImageSource = "icon - search";
            this.cmdSearch.Location = new System.Drawing.Point(311, 30);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(40, 40);
			this.cmdSearch.TabIndex = 2;
			this.cmdSearch.ToolTipText = null;
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// cmdEdit
			// 
			this.cmdEdit.AppearanceKey = "actionButton";
			this.cmdEdit.Enabled = false;
            //this.cmdEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdEdit.Image")));
            this.cmdEdit.ImageSource = "icon - edit";
            this.cmdEdit.Location = new System.Drawing.Point(361, 30);
			this.cmdEdit.Name = "cmdEdit";
			this.cmdEdit.Size = new System.Drawing.Size(40, 40);
			this.cmdEdit.TabIndex = 3;
			this.cmdEdit.ToolTipText = null;
			this.cmdEdit.Click += new System.EventHandler(this.cmdEdit_Click);
			// 
			// txtDOB
			// 
			this.txtDOB.Location = new System.Drawing.Point(153, 165);
			this.txtDOB.Mask = "##/##/####";
			this.txtDOB.Name = "txtDOB";
			this.txtDOB.SelLength = 0;
			this.txtDOB.SelStart = 0;
			this.txtDOB.Size = new System.Drawing.Size(130, 40);
			this.txtDOB.TabIndex = 7;
			this.txtDOB.Text = "  /  /";
			this.txtDOB.ToolTipText = null;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 229);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(109, 16);
			this.Label4.TabIndex = 8;
			this.Label4.Text = "RESIDENCE CODE";
			this.Label4.ToolTipText = null;
			// 
			// lblCustomerNumber
			// 
			this.lblCustomerNumber.Location = new System.Drawing.Point(20, 44);
			this.lblCustomerNumber.Name = "lblCustomerNumber";
			this.lblCustomerNumber.Size = new System.Drawing.Size(88, 18);
			this.lblCustomerNumber.TabIndex = 0;
			this.lblCustomerNumber.Text = "CUSTOMER #";
			this.lblCustomerNumber.ToolTipText = null;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 80);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(72, 18);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "CUSTOMER";
			this.Label3.ToolTipText = null;
			// 
			// lblCustomerInfo
			// 
			this.lblCustomerInfo.Location = new System.Drawing.Point(153, 80);
			this.lblCustomerInfo.Name = "lblCustomerInfo";
			this.lblCustomerInfo.Size = new System.Drawing.Size(248, 75);
			this.lblCustomerInfo.TabIndex = 5;
			this.lblCustomerInfo.ToolTipText = null;
			// 
			// lblDOB
			// 
			this.lblDOB.Location = new System.Drawing.Point(20, 179);
			this.lblDOB.Name = "lblDOB";
			this.lblDOB.Size = new System.Drawing.Size(88, 18);
			this.lblDOB.TabIndex = 6;
			this.lblDOB.Text = "DATE OF BIRTH";
			this.lblDOB.ToolTipText = null;
			// 
			// fraVehicle
			// 
			this.fraVehicle.Controls.Add(this.txtYear);
			this.fraVehicle.Controls.Add(this.txtMake);
			this.fraVehicle.Controls.Add(this.txtModel);
			this.fraVehicle.Controls.Add(this.txtVIN);
			this.fraVehicle.Controls.Add(this.lblYear);
			this.fraVehicle.Controls.Add(this.lblMake);
			this.fraVehicle.Controls.Add(this.lblModel);
			this.fraVehicle.Controls.Add(this.lblVin);
			this.fraVehicle.Location = new System.Drawing.Point(30, 465);
			this.fraVehicle.Name = "fraVehicle";
			this.fraVehicle.Size = new System.Drawing.Size(421, 270);
			this.fraVehicle.TabIndex = 7;
			this.fraVehicle.Text = "Vehicle Information";
			// 
			// txtYear
			// 
			this.txtYear.AutoSize = false;
			this.txtYear.BackColor = System.Drawing.SystemColors.Window;
			this.txtYear.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtYear.LinkItem = null;
			this.txtYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtYear.LinkTopic = null;
			this.txtYear.Location = new System.Drawing.Point(94, 30);
			this.txtYear.MaxLength = 4;
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(307, 40);
			this.txtYear.TabIndex = 1;
			this.txtYear.ToolTipText = null;
			this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
			// 
			// txtMake
			// 
			this.txtMake.AutoSize = false;
			this.txtMake.BackColor = System.Drawing.SystemColors.Window;
			this.txtMake.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtMake.LinkItem = null;
			this.txtMake.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMake.LinkTopic = null;
			this.txtMake.Location = new System.Drawing.Point(94, 90);
			this.txtMake.MaxLength = 4;
			this.txtMake.Name = "txtMake";
			this.txtMake.Size = new System.Drawing.Size(307, 40);
			this.txtMake.TabIndex = 3;
			this.txtMake.ToolTipText = null;
			this.txtMake.Validating += new System.ComponentModel.CancelEventHandler(this.txtMake_Validating);
			// 
			// txtModel
			// 
			this.txtModel.AutoSize = false;
			this.txtModel.BackColor = System.Drawing.SystemColors.Window;
			this.txtModel.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtModel.LinkItem = null;
			this.txtModel.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtModel.LinkTopic = null;
			this.txtModel.Location = new System.Drawing.Point(94, 150);
			this.txtModel.MaxLength = 15;
			this.txtModel.Name = "txtModel";
			this.txtModel.Size = new System.Drawing.Size(307, 40);
			this.txtModel.TabIndex = 5;
			this.txtModel.ToolTipText = null;
			// 
			// txtVIN
			// 
			this.txtVIN.AutoSize = false;
			this.txtVIN.BackColor = System.Drawing.SystemColors.Window;
			this.txtVIN.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtVIN.LinkItem = null;
			this.txtVIN.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtVIN.LinkTopic = null;
			this.txtVIN.Location = new System.Drawing.Point(94, 210);
			this.txtVIN.MaxLength = 18;
			this.txtVIN.Name = "txtVIN";
			this.txtVIN.Size = new System.Drawing.Size(307, 40);
			this.txtVIN.TabIndex = 7;
			this.txtVIN.ToolTipText = null;
			this.txtVIN.Validating += new System.ComponentModel.CancelEventHandler(this.txtVIN_Validating);
			// 
			// lblYear
			// 
			this.lblYear.Location = new System.Drawing.Point(20, 44);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(39, 16);
			this.lblYear.TabIndex = 0;
			this.lblYear.Text = "YEAR";
			this.lblYear.ToolTipText = null;
			// 
			// lblMake
			// 
			this.lblMake.Location = new System.Drawing.Point(20, 104);
			this.lblMake.Name = "lblMake";
			this.lblMake.Size = new System.Drawing.Size(41, 16);
			this.lblMake.TabIndex = 2;
			this.lblMake.Text = "MAKE";
			this.lblMake.ToolTipText = null;
			// 
			// lblModel
			// 
			this.lblModel.Location = new System.Drawing.Point(20, 164);
			this.lblModel.Name = "lblModel";
			this.lblModel.Size = new System.Drawing.Size(45, 16);
			this.lblModel.TabIndex = 4;
			this.lblModel.Text = "MODEL";
			this.lblModel.ToolTipText = null;
			// 
			// lblVin
			// 
			this.lblVin.Location = new System.Drawing.Point(20, 224);
			this.lblVin.Name = "lblVin";
			this.lblVin.Size = new System.Drawing.Size(33, 16);
			this.lblVin.TabIndex = 6;
			this.lblVin.Text = "VIN #";
			this.lblVin.ToolTipText = null;
			// 
			// txtExpireDate
			// 
			this.txtExpireDate.Location = new System.Drawing.Point(165, 80);
			this.txtExpireDate.Mask = "##/##/####";
			this.txtExpireDate.MaxLength = 10;
			this.txtExpireDate.Name = "txtExpireDate";
			this.txtExpireDate.SelLength = 0;
			this.txtExpireDate.SelStart = 0;
			this.txtExpireDate.Size = new System.Drawing.Size(130, 40);
			this.txtExpireDate.TabIndex = 3;
			this.txtExpireDate.TabStop = false;
			this.txtExpireDate.Text = "  /  /";
			this.txtExpireDate.ToolTipText = null;
			this.txtExpireDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtExpireDate_Validate);
			// 
			// txtEffectiveDate
			// 
			this.txtEffectiveDate.Location = new System.Drawing.Point(165, 30);
			this.txtEffectiveDate.Mask = "##/##/####";
			this.txtEffectiveDate.MaxLength = 10;
			this.txtEffectiveDate.Name = "txtEffectiveDate";
			this.txtEffectiveDate.SelLength = 0;
			this.txtEffectiveDate.SelStart = 0;
			this.txtEffectiveDate.Size = new System.Drawing.Size(130, 40);
			this.txtEffectiveDate.TabIndex = 1;
			this.txtEffectiveDate.TabStop = false;
			this.txtEffectiveDate.Text = "  /  /";
			this.txtEffectiveDate.ToolTipText = null;
			this.txtEffectiveDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtEffectiveDate_Validate);
			// 
			// lblDestination
			// 
			this.lblDestination.Location = new System.Drawing.Point(30, 809);
			this.lblDestination.Name = "lblDestination";
			this.lblDestination.Size = new System.Drawing.Size(85, 16);
			this.lblDestination.TabIndex = 10;
			this.lblDestination.Text = "DESTINATION";
			this.lblDestination.ToolTipText = null;
			// 
			// lblOrigin
			// 
			this.lblOrigin.Location = new System.Drawing.Point(30, 759);
			this.lblOrigin.Name = "lblOrigin";
			this.lblOrigin.Size = new System.Drawing.Size(43, 16);
			this.lblOrigin.TabIndex = 8;
			this.lblOrigin.Text = "ORIGIN";
			this.lblOrigin.ToolTipText = null;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(95, 16);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "EFFECTIVE DATE";
			this.Label1.ToolTipText = null;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 94);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(103, 16);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "EXPIRATION DATE";
			this.Label2.ToolTipText = null;
			// 
			// lblPermit
			// 
			this.lblPermit.Location = new System.Drawing.Point(30, 144);
			this.lblPermit.Name = "lblPermit";
			this.lblPermit.Size = new System.Drawing.Size(50, 16);
			this.lblPermit.TabIndex = 4;
			this.lblPermit.Text = "PLATE #";
			this.lblPermit.ToolTipText = null;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(172, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(80, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Save";
			this.cmdProcessSave.ToolTipText = null;
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmTransitPlate
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(489, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmTransitPlate";
			this.Text = "Transit Plate";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmTransitPlate_Load);
			this.Activated += new System.EventHandler(this.frmTransitPlate_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTransitPlate_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTransitPlate_KeyPress);
			this.Resize += new System.EventHandler(this.frmTransitPlate_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkMunicipalVehicle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRoundTrip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCodes)).EndInit();
			this.fraCodes.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraApplicant)).EndInit();
			this.fraApplicant.ResumeLayout(false);
			this.fraApplicant.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDOB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraVehicle)).EndInit();
			this.fraVehicle.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtExpireDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdProcessSave;
    }
}
