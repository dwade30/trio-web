//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWMV0000
{
	public partial class frmTellerReport : BaseForm
	{
		public frmTellerReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
			this.chkTeller = new System.Collections.Generic.List<FCCheckBox>();
			this.chkTeller.AddRange(new FCCheckBox[] {
			this.chkTeller_0,
			this.chkTeller_1,
			this.chkTeller_2,
			this.chkTeller_3,
			this.chkTeller_4,
			this.chkTeller_5,
			this.chkTeller_6,
			this.chkTeller_7,
			this.chkTeller_8,
			this.chkTeller_9,
			this.chkTeller_10,
			this.chkTeller_11,
			this.chkTeller_12,
			this.chkTeller_13,
			this.chkTeller_14,
			this.chkTeller_15});
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTellerReport InstancePtr
		{
			get
			{
				return (frmTellerReport)Sys.GetInstance(typeof(frmTellerReport));
			}
		}

		protected frmTellerReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         5/11/2001
		// This form will be used to print teller reports
		// The person will select the tellers they want reported
		// and select a date range then a report will be created
		// listing the registrations processed by the specified
		// tellers along with money collected and a grand total
		// ********************************************************
		int fnx;
		bool blnOK;
		string[] Tellers = new string[16 + 1];
		public int lngPK1;
		public int lngPK2;
		bool blnPrint;

		private void cboReportTime_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			switch (cboReportTime.SelectedIndex)
			{
				case 0:
					{
						txtStartDate.Text = "";
						txtEndDate.Text = "";
						cboStart.SelectedIndex = -1;
						cboEnd.SelectedIndex = -1;
						fraRange.Visible = false;
						fraSetDates.Visible = false;
						cmdFilePreview.Enabled = true;
						cmdFilePreview.Focus();
						break;
					}
				case 1:
					{
						fraSetDates.Visible = true;
						fraRange.Visible = false;
						txtStartDate.Text = "";
						txtEndDate.Text = "";
						cboStart.Focus();
						cmdFilePreview.Enabled = true;
						break;
					}
				case 2:
					{
						txtStartDate.Text = "";
						txtEndDate.Text = "";
						cboStart.SelectedIndex = -1;
						cboEnd.SelectedIndex = -1;
						fraRange.Visible = false;
						fraSetDates.Visible = false;
						cmdFilePreview.Enabled = false;
						CreateSQL();
						break;
					}
				case 3:
					{
						fraSetDates.Visible = false;
						fraRange.Visible = true;
						cboStart.SelectedIndex = -1;
						cboEnd.SelectedIndex = -1;
						txtStartDate.Focus();
						cmdFilePreview.Enabled = true;
						break;
					}
				case 4:
					{
						txtStartDate.Text = "";
						txtEndDate.Text = "";
						cboStart.SelectedIndex = -1;
						cboEnd.SelectedIndex = -1;
						fraRange.Visible = false;
						fraSetDates.Visible = false;
						cmdFilePreview.Enabled = true;
						cmdFilePreview.Focus();
						break;
					}
				case 5:
					{
						txtStartDate.Text = "";
						txtEndDate.Text = "";
						cboStart.SelectedIndex = -1;
						cboEnd.SelectedIndex = -1;
						fraRange.Visible = false;
						fraSetDates.Visible = false;
						cmdFilePreview.Enabled = true;
						cmdFilePreview.Focus();
						break;
					}
				case 6:
					{
						txtStartDate.Text = "";
						txtEndDate.Text = "";
						cboStart.SelectedIndex = -1;
						cboEnd.SelectedIndex = -1;
						fraRange.Visible = false;
						fraSetDates.Visible = false;
						cmdFilePreview.Enabled = true;
						cmdFilePreview.Focus();
						break;
					}
			}
			//end switch
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			if (cboStartDate.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a Starting Date before continuing.", "Invalid Starting Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cboEndDate.SelectedIndex == -1)
			{
				MessageBox.Show("You must select an Ending Date before continuing.", "Invalid Ending Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (fecherFoundation.DateAndTime.DateValue(cboStartDate.Text).ToOADate() >= fecherFoundation.DateAndTime.DateValue(cboEndDate.Text).ToOADate())
			{
				MessageBox.Show("Your Starting Date must be earlier then your Ending Date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			fraTellerCloseout.Visible = false;
			MotorVehicle.Statics.strTellerSQL += " (TellerID = '" + Tellers[fnx] + "' AND TellerCloseoutDate BETWEEN #" + cboStartDate.Text + "# AND #" + cboEndDate.Text + "#) OR";
			fnx += 1;
			if (fnx <= 15)
			{
				if (cmbAll.Text == "All")
				{
					if (Tellers[fnx] != "")
					{
						lblTeller.Text = chkTeller[fnx].Text;
						FillComboDates(ref fnx);
						fraTellerCloseout.Visible = true;
					}
					else
					{
						MotorVehicle.Statics.strTellerSQL = Strings.Left(MotorVehicle.Statics.strTellerSQL, MotorVehicle.Statics.strTellerSQL.Length - 3);
						cmdFilePreview.Enabled = true;
						cmdFilePreview.Focus();
					}
				}
				else
				{
					TryAgain2:
					;
					if (chkTeller[fnx].CheckState == Wisej.Web.CheckState.Checked)
					{
						lblTeller.Text = chkTeller[fnx].Text;
						FillComboDates(ref fnx);
						fraTellerCloseout.Visible = true;
					}
					else
					{
						fnx += 1;
						if (fnx <= 15)
						{
							goto TryAgain2;
						}
						else
						{
							MotorVehicle.Statics.strTellerSQL = Strings.Left(MotorVehicle.Statics.strTellerSQL, MotorVehicle.Statics.strTellerSQL.Length - 3);
							cmdFilePreview.Enabled = true;
							cmdFilePreview.Focus();
						}
					}
				}
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			blnPrint = false;
			PrintInfo();
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.Statics.strTellerSQL = "";
			lblTeller.Text = "";
			cboStart.SelectedIndex = -1;
			cboEnd.SelectedIndex = -1;
			fraTellerCloseout.Visible = false;
			cboReportTime.SelectedIndex = -1;
		}

		private void frmTellerReport_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.TELLERREPORT))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			SetTellers();
			SetCloseoutDates();
			this.Refresh();
		}

		private void frmTellerReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTellerReport properties;
			//frmTellerReport.FillStyle	= 0;
			//frmTellerReport.ScaleWidth	= 9300;
			//frmTellerReport.ScaleHeight	= 6930;
			//frmTellerReport.LinkTopic	= "Form2";
			//frmTellerReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmTellerReport_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}

            fraTellerCloseout.CenterToContainer(this.ClientArea);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void frmTellerReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			blnPrint = false;
			PrintInfo();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			blnPrint = true;
			PrintInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SetCloseoutDates()
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM PeriodCloseout ORDER BY ID DESC");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				while (!rs.EndOfFile())
				{
					//FC:FINAL:MSH - i.issue #1855: add dates in short format (as in original app)
					//cboStart.AddItem(FCConvert.ToString(rs.Get_Fields("IssueDate")));//Strings.Format(rs.Get_Fields("IssueDate"),"MM/dd/yyyy"));
					//cboEnd.AddItem(FCConvert.ToString(rs.Get_Fields("IssueDate"))); //Strings.Format(rs.Get_Fields("IssueDate"), "MM/dd/yyyy"));
					cboStart.AddItem(FCConvert.ToString(rs.Get_Fields_DateTime("IssueDate")));
					cboEnd.AddItem(FCConvert.ToString(rs.Get_Fields_DateTime("IssueDate")));
					rs.MoveNext();
				}
			}
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int fnx;
			for (fnx = 0; fnx <= 15; fnx++)
			{
				if (chkTeller[fnx].Visible == true)
				{
					chkTeller[fnx].CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			fraTellers.Enabled = false;
		}

		private void optCloseout_Click()
		{
			fraSetDates.Visible = true;
			fraRange.Visible = false;
			txtStartDate.Text = "";
			txtEndDate.Text = "";
			cboStart.Focus();
		}

		private void optRange_Click()
		{
			fraSetDates.Visible = false;
			fraRange.Visible = true;
			cboStart.SelectedIndex = -1;
			cboEnd.SelectedIndex = -1;
			txtStartDate.Focus();
		}

		private void optSelect_CheckedChanged(object sender, System.EventArgs e)
		{
			fraTellers.Enabled = true;
		}

		private void optToday_Click()
		{
			txtStartDate.Text = "";
			txtEndDate.Text = "";
			cboStart.SelectedIndex = -1;
			cboEnd.SelectedIndex = -1;
			fraRange.Visible = false;
			fraSetDates.Visible = false;
			cmdFilePreview.Focus();
		}

		private void SetTellers()
		{
			clsDRWrapper rs = new clsDRWrapper();
			// vbPorter upgrade warning: fnx As int	OnWriteFCConvert.ToInt32(
			int fnx;
			rs.OpenRecordset("SELECT * FROM Operators", "SystemSettings");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				if (rs.RecordCount() > 16)
				{
					MessageBox.Show("You may not have more than 16 tellers.  You must delete one or more tellers.  Only the first 16 tellers will be displayed.", "Too Many Tellers", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				for (fnx = 0; fnx <= (rs.RecordCount() - 1); fnx++)
				{
					if (fnx > 15)
						break;
					chkTeller[fnx].Visible = true;
				}
				fnx = 0;
				do
				{
					chkTeller[fnx].Text = FCConvert.ToString(rs.Get_Fields_String("Name"));
					Tellers[fnx] = FCConvert.ToString(rs.Get_Fields("Code"));
					fnx += 1;
					rs.MoveNext();
					if (fnx > 15)
						break;
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void CreateSQL()
		{
			MotorVehicle.Statics.strTellerSQL = "SELECT ID FROM TellerCloseoutTable WHERE";
			if (cmbAll.Text == "All")
			{
				fnx = 0;
				if (Tellers[fnx] != "")
				{
					lblTeller.Text = chkTeller[fnx].Text;
					FillComboDates(ref fnx);
					fraTellerCloseout.Visible = true;
				}
				else
				{
					MessageBox.Show("You have not created any Operators!!", "No Operators", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				fnx = 0;
				TryAgain:
				;
				if (chkTeller[fnx].CheckState == Wisej.Web.CheckState.Checked)
				{
					lblTeller.Text = chkTeller[fnx].Text;
					FillComboDates(ref fnx);
					fraTellerCloseout.Visible = true;
				}
				else
				{
					fnx += 1;
					if (fnx <= 15)
					{
						goto TryAgain;
					}
					else
					{
						MessageBox.Show("No Teller Have Been Selected!!", "No Tellers Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
		}

		private void FillComboDates(ref int x)
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + Tellers[fnx] + "'");
			cboStartDate.Clear();
			cboEndDate.Clear();
			cboStartDate.SelectedIndex = -1;
			cboEndDate.SelectedIndex = -1;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				do
				{
					cboStartDate.AddItem(Strings.Format(rs.Get_Fields_DateTime("TellerCloseoutDate"), "MM/dd/yyyy"));
					cboEndDate.AddItem(Strings.Format(rs.Get_Fields_DateTime("TellerCloseoutDate"), "MM/dd/yyyy"));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void PrintInfo()
		{
			int fnx;
			bool blnSelected = false;
			string strSQL = "";
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsTransit = new clsDRWrapper();
			clsDRWrapper rsSpecial = new clsDRWrapper();
			clsDRWrapper rsBooster = new clsDRWrapper();
			string strDates = "";
			if (cmbAll.Text == "Print Selected")
			{
				blnSelected = false;
				for (fnx = 0; fnx <= 15; fnx++)
				{
					if (chkTeller[fnx].Visible == true)
					{
						if (chkTeller[fnx].CheckState == Wisej.Web.CheckState.Checked)
							blnSelected = true;
					}
				}
				if (blnSelected == false)
				{
					MessageBox.Show("You must select at least 1 teller before you may run this report.", "Select Teller", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cboReportTime.SelectedIndex == -1)
			{
				MessageBox.Show("You must enter a date range to report for before you may continue", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (cboReportTime.SelectedIndex == 1)
			{
				if (cboStart.SelectedIndex <= cboEnd.SelectedIndex)
				{
					MessageBox.Show("Your Starting Date is Later than your Ending Date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else if (cboStart.SelectedIndex == -1 || cboEnd.SelectedIndex == -1)
				{
					MessageBox.Show("You must Select a Starting and Ending Date before you may run this Report.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cboReportTime.SelectedIndex == 3)
			{
				if (!Information.IsDate(txtStartDate.Text))
				{
					MessageBox.Show("Your Starting Date is not a Valid Date.", "Invalid Start Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else if (!Information.IsDate(txtEndDate.Text))
				{
					MessageBox.Show("Your Ending Date is not a Valid Date.", "Invalid End Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else if (fecherFoundation.DateAndTime.DateValue(txtStartDate.Text).ToOADate() > fecherFoundation.DateAndTime.DateValue(txtEndDate.Text).ToOADate())
				{
					MessageBox.Show("Your Starting Date is Later than your Ending Date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cboReportTime.SelectedIndex == 1)
			{
				strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + cboStart.Text + "' AND '" + cboEnd.Text + "' ORDER BY IssueDate DESC";
				rs.OpenRecordset(strSQL);
				lngPK1 = 0;
				lngPK2 = 0;
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.MoveFirst();
					lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				}
			}
			if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 1)
			{
				strDates = " PeriodCloseoutID > " + FCConvert.ToString(frmTellerReport.InstancePtr.lngPK1) + " And PeriodCloseoutID <= " + FCConvert.ToString(frmTellerReport.InstancePtr.lngPK2);
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 3)
			{
				strDates = " Convert(Date, DateUpdated) BETWEEN '" + frmTellerReport.InstancePtr.txtStartDate.Text + "' and '" + frmTellerReport.InstancePtr.txtEndDate.Text + "'";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 0)
			{
				strDates = " Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "'";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 2)
			{
				strDates = " TellerCloseoutID IN (" + MotorVehicle.Statics.strTellerSQL + ")";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 4)
			{
				strDates = " PeriodCloseoutID = 0";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 5)
			{
				strDates = " PeriodCloseoutID = 0 AND TellerCloseoutID <> 0";
			}
			else if (frmTellerReport.InstancePtr.cboReportTime.SelectedIndex == 6)
			{
				strDates = " PeriodCloseoutID = 0 AND TellerCloseoutID = 0";
			}
			if (cmbAll.Text == "All")
			{
				rsTransit.OpenRecordset("SELECT * FROM TransitPlates WHERE Voided = 0 AND " + strDates);
				rsBooster.OpenRecordset("SELECT * FROM Booster WHERE Voided = 0 AND " + strDates);
				rsSpecial.OpenRecordset("SELECT * FROM SpecialRegistration WHERE Voided = 0 AND " + strDates);
				if (cboReportTime.SelectedIndex == 0)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "' ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 1)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 3)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND Convert(Date, DateUpdated) BETWEEN '" + txtStartDate.Text + "' and '" + txtEndDate.Text + "' ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 2)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND TellerCloseoutID IN (" + MotorVehicle.Statics.strTellerSQL + ") ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 4)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 5)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID <> 0 ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 6)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID = 0 ORDER BY OpID, MVR3");
				}
			}
			else
			{
				strSQL = "AND (";
				for (fnx = 0; fnx <= 15; fnx++)
				{
					if (chkTeller[fnx].CheckState == Wisej.Web.CheckState.Checked)
					{
						strSQL += "OpID = '" + Tellers[fnx] + "' OR ";
					}
				}
				strSQL = Strings.Left(strSQL, strSQL.Length - 4) + ")";
				rsTransit.OpenRecordset("SELECT * FROM TransitPlates WHERE Voided = 0 AND " + strDates + strSQL);
				rsBooster.OpenRecordset("SELECT * FROM Booster WHERE Voided = 0 AND " + strDates + strSQL);
				rsSpecial.OpenRecordset("SELECT * FROM SpecialRegistration WHERE Voided = 0 AND " + strDates + strSQL);
				if (cboReportTime.SelectedIndex == 0)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "' " + strSQL + " ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 1)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + strSQL + " ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 3)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND Convert(Date, DateUpdated) BETWEEN '" + txtStartDate.Text + "' and '" + txtEndDate.Text + "' " + strSQL + " ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 2)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND TellerCloseoutID IN (" + MotorVehicle.Statics.strTellerSQL + ")" + strSQL + " ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 4)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 " + strSQL + " ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 5)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID <> 0 " + strSQL + " ORDER BY OpID, MVR3");
				}
				else if (cboReportTime.SelectedIndex == 6)
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID = 0 " + strSQL + " ORDER BY OpID, MVR3");
				}
			}
			if (MotorVehicle.Statics.rsTellers.EndOfFile() != true && MotorVehicle.Statics.rsTellers.BeginningOfFile() != true)
			{
				if (cmbAll.Text == "All")
				{
					if (cboReportTime.SelectedIndex == 0)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "' UNION SELECT DISTINCT OpID FROM Booster UNION SELECT DISTINCT OpID FROM SpecialRegistration UNION SELECT DISTINCT OpID FROM TransitPlates) as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 1)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + " UNION SELECT DISTINCT OpID FROM Booster UNION SELECT DISTINCT OpID FROM SpecialRegistration UNION SELECT DISTINCT OpID FROM TransitPlates) as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 3)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND Convert(Date, DateUpdated) BETWEEN '" + txtStartDate.Text + "' and '" + txtEndDate.Text + "' UNION SELECT DISTINCT OpID FROM Booster UNION SELECT DISTINCT OpID FROM SpecialRegistration UNION SELECT DISTINCT OpID FROM TransitPlates) as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 2)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND TellerCloseoutID IN (" + MotorVehicle.Statics.strTellerSQL + ") UNION SELECT DISTINCT OpID FROM Booster UNION SELECT DISTINCT OpID FROM SpecialRegistration UNION SELECT DISTINCT OpID FROM TransitPlates) as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 4)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 UNION SELECT DISTINCT OpID FROM Booster UNION SELECT DISTINCT OpID FROM SpecialRegistration UNION SELECT DISTINCT OpID FROM TransitPlates) as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 5)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID <> 0 UNION SELECT DISTINCT OpID FROM Booster UNION SELECT DISTINCT OpID FROM SpecialRegistration UNION SELECT DISTINCT OpID FROM TransitPlates) as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 6)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID = 0 UNION SELECT DISTINCT OpID FROM Booster UNION SELECT DISTINCT OpID FROM SpecialRegistration UNION SELECT DISTINCT OpID FROM TransitPlates) as temp ORDER BY OpID");
					}
				}
				else
				{
					if (cboReportTime.SelectedIndex == 0)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND Convert(Date, DateUpdated) = '" + DateTime.Today.ToShortDateString() + "' " + strSQL + " UNION SELECT DISTINCT OpID FROM Booster WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM SpecialRegistration WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM TransitPlates WHERE " + strDates + strSQL + ") as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 1)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " AND OldMVR3 <= " + FCConvert.ToString(lngPK2) + strSQL + " UNION SELECT DISTINCT OpID FROM Booster WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM SpecialRegistration WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM TransitPlates WHERE " + strDates + strSQL + ") as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 3)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND Convert(Date, DateUpdated) BETWEEN '" + txtStartDate.Text + "' and '" + txtEndDate.Text + "' " + strSQL + " UNION SELECT DISTINCT OpID FROM Booster WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM SpecialRegistration WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM TransitPlates WHERE " + strDates + strSQL + ") as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 2)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND TellerCloseoutID IN (" + MotorVehicle.Statics.strTellerSQL + ")" + strSQL + " UNION SELECT DISTINCT OpID FROM Booster WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM SpecialRegistration WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM TransitPlates WHERE " + strDates + strSQL + ") as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 4)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 " + strSQL + " UNION SELECT DISTINCT OpID FROM Booster WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM SpecialRegistration WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM TransitPlates WHERE " + strDates + strSQL + ") as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 5)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID <> 0 " + strSQL + " UNION SELECT DISTINCT OpID FROM Booster WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM SpecialRegistration WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM TransitPlates WHERE " + strDates + strSQL + ") as temp ORDER BY OpID");
					}
					else if (cboReportTime.SelectedIndex == 6)
					{
						MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT * FROM (SELECT DISTINCT OpID FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 = 0 AND TellerCloseoutID = 0 " + strSQL + " UNION SELECT DISTINCT OpID FROM Booster WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM SpecialRegistration WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM TransitPlates WHERE " + strDates + strSQL + ") as temp ORDER BY OpID");
					}
				}
				if (cboReportTime.SelectedIndex != 2)
				{
					MotorVehicle.Statics.strTellerSQL = "";
				}
				if (blnPrint)
				{
					modDuplexPrinting.DuplexPrintReport(rptTellerReport.InstancePtr);
				}
				else
				{
					frmReportViewer.InstancePtr.Init(rptTellerReport.InstancePtr);
				}
			}
			else if ((rsTransit.EndOfFile() != true && rsTransit.BeginningOfFile() != true) || (rsSpecial.EndOfFile() != true && rsSpecial.BeginningOfFile() != true) || (rsBooster.EndOfFile() != true && rsBooster.BeginningOfFile() != true))
			{
				if (cmbAll.Text == "All")
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT DISTINCT OpID FROM Booster UNION SELECT DISTINCT OpID FROM SpecialRegistration UNION SELECT DISTINCT OpID FROM TransitPlates");
				}
				else
				{
					MotorVehicle.Statics.rsTellers.OpenRecordset("SELECT DISTINCT OpID FROM Booster WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM SpecialRegistration WHERE " + strDates + strSQL + " UNION SELECT DISTINCT OpID FROM TransitPlates WHERE " + strDates + strSQL);
				}
				if (cboReportTime.SelectedIndex != 2)
				{
					MotorVehicle.Statics.strTellerSQL = "";
				}
				if (blnPrint)
				{
					//FC:TODO
					//vbPorterConverter.DrawStringLine(rptNoActivityTellerReport.InstancePtr, true);
				}
				else
				{
					frmReportViewer.InstancePtr.Init(rptNoActivityTellerReport.InstancePtr);
				}
			}
			else
			{
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void SetCustomFormColors()
		{
			//cmbAll.ForeColor = Color.Blue;
		}

		public void cmbAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAll.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Print Selected")
			{
				optSelect_CheckedChanged(sender, e);
			}
		}
	}
}
