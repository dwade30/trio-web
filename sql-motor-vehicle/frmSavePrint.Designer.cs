//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Diagnostics;
using fecherFoundation.DataBaseLayer;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmSavePrint.
	/// </summary>
	partial class frmSavePrint
	{
		public FCCommonDialog dlg1;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCLabel lblPrintQueue;
		public fecherFoundation.FCLabel lblBatchRegistration;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblF1;
		public fecherFoundation.FCLabel lblMVR3Done;
		public fecherFoundation.FCLabel lblUseTaxDone;
		public fecherFoundation.FCLabel lblTitleDone;
		public fecherFoundation.FCLabel lblSave;
		public fecherFoundation.FCLabel lblHold;
		public fecherFoundation.FCLabel lblMVR3;
		public fecherFoundation.FCLabel lblUseTax;
		public fecherFoundation.FCLabel lblCTA;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}

                rsDCTA.DisposeOf();
                rsExceptionReport.DisposeOf();
                rsExciseTax.DisposeOf();
                rsGCRegister.DisposeOf();
                rsInventoryAdjustments.DisposeOf();
                rsInventory.DisposeOf();
                rsTempPlateRegister.DisposeOf();
                rsTitle.DisposeOf();
                rsTitleApp.DisposeOf();
                rsUseTax.DisposeOf();
                rsFinal2Activity.DisposeOf();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.lblPrintQueue = new fecherFoundation.FCLabel();
			this.lblBatchRegistration = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblF1 = new fecherFoundation.FCLabel();
			this.lblMVR3Done = new fecherFoundation.FCLabel();
			this.lblUseTaxDone = new fecherFoundation.FCLabel();
			this.lblTitleDone = new fecherFoundation.FCLabel();
			this.lblSave = new fecherFoundation.FCLabel();
			this.lblHold = new fecherFoundation.FCLabel();
			this.lblMVR3 = new fecherFoundation.FCLabel();
			this.lblUseTax = new fecherFoundation.FCLabel();
			this.lblCTA = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 586);
			this.BottomPanel.Size = new System.Drawing.Size(451, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdReturn);
			this.ClientArea.Controls.Add(this.lblPrintQueue);
			this.ClientArea.Controls.Add(this.lblBatchRegistration);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.Label5);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblF1);
			this.ClientArea.Controls.Add(this.lblMVR3Done);
			this.ClientArea.Controls.Add(this.lblUseTaxDone);
			this.ClientArea.Controls.Add(this.lblTitleDone);
			this.ClientArea.Controls.Add(this.lblSave);
			this.ClientArea.Controls.Add(this.lblHold);
			this.ClientArea.Controls.Add(this.lblMVR3);
			this.ClientArea.Controls.Add(this.lblUseTax);
			this.ClientArea.Controls.Add(this.lblCTA);
			this.ClientArea.Size = new System.Drawing.Size(451, 526);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(451, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(278, 30);
			this.HeaderText.Text = "Save / Print Registration";
			// 
			// dlg1
			// 
			this.dlg1.Name = "dlg1";
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			// 
			// cmdReturn
			// 
			this.cmdReturn.AppearanceKey = "actionButton";
			this.cmdReturn.Location = new System.Drawing.Point(30, 275);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Size = new System.Drawing.Size(184, 40);
			this.cmdReturn.TabIndex = 5;
			this.cmdReturn.Text = "Return to Preview";
			this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
			// 
			// lblPrintQueue
			// 
			this.lblPrintQueue.BorderStyle = 1;
			this.lblPrintQueue.Location = new System.Drawing.Point(66, 240);
			this.lblPrintQueue.Name = "lblPrintQueue";
			this.lblPrintQueue.Size = new System.Drawing.Size(275, 15);
			this.lblPrintQueue.TabIndex = 20;
			this.lblPrintQueue.Text = "ADD TO PRINT QUEUE";
			this.lblPrintQueue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblPrintQueue.Click += new System.EventHandler(this.lblPrintQueue_Click);
			this.lblPrintQueue.MouseMove += new Wisej.Web.MouseEventHandler(this.lblPrintQueue_MouseMove);
			// 
			// lblBatchRegistration
			// 
			this.lblBatchRegistration.BorderStyle = 1;
			this.lblBatchRegistration.Location = new System.Drawing.Point(66, 205);
			this.lblBatchRegistration.Name = "lblBatchRegistration";
			this.lblBatchRegistration.Size = new System.Drawing.Size(275, 15);
			this.lblBatchRegistration.TabIndex = 19;
			this.lblBatchRegistration.Text = "BATCH REGISTRATION";
			this.lblBatchRegistration.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblBatchRegistration.Click += new System.EventHandler(this.lblBatchRegistration_Click);
			this.lblBatchRegistration.MouseMove += new Wisej.Web.MouseEventHandler(this.lblBatchRegistration_MouseMove);
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(30, 205);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(16, 15);
			this.Label7.TabIndex = 18;
			this.Label7.Text = "F6";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(30, 170);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(16, 15);
			this.Label5.TabIndex = 13;
			this.Label5.Text = "F5";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 135);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(16, 15);
			this.Label4.TabIndex = 12;
			this.Label4.Text = "F4";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 100);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(16, 15);
			this.Label3.TabIndex = 11;
			this.Label3.Text = "F3";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 65);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(16, 15);
			this.Label2.TabIndex = 10;
			this.Label2.Text = "F2";
			// 
			// lblF1
			// 
			this.lblF1.Location = new System.Drawing.Point(30, 30);
			this.lblF1.Name = "lblF1";
			this.lblF1.Size = new System.Drawing.Size(16, 15);
			this.lblF1.TabIndex = 9;
			this.lblF1.Text = "F1";
			// 
			// lblMVR3Done
			// 
			this.lblMVR3Done.Location = new System.Drawing.Point(361, 100);
			this.lblMVR3Done.Name = "lblMVR3Done";
			this.lblMVR3Done.Size = new System.Drawing.Size(62, 15);
			this.lblMVR3Done.TabIndex = 8;
			// 
			// lblUseTaxDone
			// 
			this.lblUseTaxDone.Location = new System.Drawing.Point(361, 65);
			this.lblUseTaxDone.Name = "lblUseTaxDone";
			this.lblUseTaxDone.Size = new System.Drawing.Size(62, 15);
			this.lblUseTaxDone.TabIndex = 7;
			// 
			// lblTitleDone
			// 
			this.lblTitleDone.Location = new System.Drawing.Point(361, 30);
			this.lblTitleDone.Name = "lblTitleDone";
			this.lblTitleDone.Size = new System.Drawing.Size(62, 15);
			this.lblTitleDone.TabIndex = 6;
			// 
			// lblSave
			// 
			this.lblSave.BorderStyle = 1;
			this.lblSave.Location = new System.Drawing.Point(66, 170);
			this.lblSave.Name = "lblSave";
			this.lblSave.Size = new System.Drawing.Size(275, 15);
			this.lblSave.TabIndex = 4;
			this.lblSave.Text = "SAVE REGISTRATION";
			this.lblSave.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblSave.Click += new System.EventHandler(this.lblSave_Click);
			this.lblSave.MouseMove += new Wisej.Web.MouseEventHandler(this.lblSave_MouseMove);
			// 
			// lblHold
			// 
			this.lblHold.BorderStyle = 1;
			this.lblHold.Location = new System.Drawing.Point(66, 135);
			this.lblHold.Name = "lblHold";
			this.lblHold.Size = new System.Drawing.Size(275, 15);
			this.lblHold.TabIndex = 3;
			this.lblHold.Text = "HOLD REGISTRATION";
			this.lblHold.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblHold.Click += new System.EventHandler(this.lblHold_Click);
			this.lblHold.MouseMove += new Wisej.Web.MouseEventHandler(this.lblHold_MouseMove);
			// 
			// lblMVR3
			// 
			this.lblMVR3.BorderStyle = 1;
			this.lblMVR3.Location = new System.Drawing.Point(66, 100);
			this.lblMVR3.Name = "lblMVR3";
			this.lblMVR3.Size = new System.Drawing.Size(275, 15);
			this.lblMVR3.TabIndex = 2;
			this.lblMVR3.Text = "PRINT MVR-3";
			this.lblMVR3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblMVR3.Click += new System.EventHandler(this.lblMVR3_Click);
			this.lblMVR3.MouseMove += new Wisej.Web.MouseEventHandler(this.lblMVR3_MouseMove);
			// 
			// lblUseTax
			// 
			this.lblUseTax.BorderStyle = 1;
			this.lblUseTax.Location = new System.Drawing.Point(66, 65);
			this.lblUseTax.Name = "lblUseTax";
			this.lblUseTax.Size = new System.Drawing.Size(275, 15);
			this.lblUseTax.TabIndex = 1;
			this.lblUseTax.Text = "PRINT USE TAX CERTIFICATE";
			this.lblUseTax.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblUseTax.Click += new System.EventHandler(this.lblUseTax_Click);
			this.lblUseTax.MouseMove += new Wisej.Web.MouseEventHandler(this.lblUseTax_MouseMove);
			// 
			// lblCTA
			// 
			this.lblCTA.BorderStyle = 1;
			this.lblCTA.Location = new System.Drawing.Point(66, 30);
			this.lblCTA.Name = "lblCTA";
			this.lblCTA.Size = new System.Drawing.Size(275, 15);
			this.lblCTA.TabIndex = 23;
			this.lblCTA.Text = "PRINT CERTIFICATE OF TITLE APPLICATION";
			this.lblCTA.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblCTA.Click += new System.EventHandler(this.lblCTA_Click);
			this.lblCTA.MouseMove += new Wisej.Web.MouseEventHandler(this.lblCTA_MouseMove);
			// 
			// frmSavePrint
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(451, 586);
			this.ControlBox = false;
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmSavePrint";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Save / Print Registration";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmSavePrint_Load);
			this.MouseMove += new Wisej.Web.MouseEventHandler(this.frmSavePrint_MouseMove);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSavePrint_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}