//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmTellerCloseout : BaseForm
	{
		public frmTellerCloseout()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            this.chkTeller = new System.Collections.Generic.List<FCCheckBox>();
            this.chkTeller.AddControlArrayElement(chkTeller_0, 0);
            this.chkTeller.AddControlArrayElement(chkTeller_1, 1);
            this.chkTeller.AddControlArrayElement(chkTeller_2, 2);
            this.chkTeller.AddControlArrayElement(chkTeller_3, 3);
            this.chkTeller.AddControlArrayElement(chkTeller_4, 4);
            this.chkTeller.AddControlArrayElement(chkTeller_5, 5);
            this.chkTeller.AddControlArrayElement(chkTeller_6, 6);
            this.chkTeller.AddControlArrayElement(chkTeller_7, 7);
            this.chkTeller.AddControlArrayElement(chkTeller_8, 8);
            this.chkTeller.AddControlArrayElement(chkTeller_9, 9);
            this.chkTeller.AddControlArrayElement(chkTeller_10, 10);
            this.chkTeller.AddControlArrayElement(chkTeller_11, 11);
            this.chkTeller.AddControlArrayElement(chkTeller_12, 12);
            this.chkTeller.AddControlArrayElement(chkTeller_13, 13);
            this.chkTeller.AddControlArrayElement(chkTeller_14, 14);
            this.chkTeller.AddControlArrayElement(chkTeller_15, 15);

            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmTellerCloseout InstancePtr
		{
			get
			{
				return (frmTellerCloseout)Sys.GetInstance(typeof(frmTellerCloseout));
			}
		}

		protected frmTellerCloseout _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         6/22/2001
		// This form will be used by customers to closeout a tellers
		// transactions for a certain amount of time.  After being
		// closed out a teller report may be run for the period to see
		// all the transaction information
		// ********************************************************
		string[] Tellers = new string[16 + 1];

		private void cmdCloseout_Click(object sender, System.EventArgs e)
		{
			int intCounter;
			DateTime DateTime;
			int lngID = 0;
			clsDRWrapper rsPC1 = new clsDRWrapper();
			try
			{
				for (intCounter = 0; intCounter <= chkTeller.UBound(); intCounter++)
				{
					if (cmbAll.Text == "Closeout Selected")
					{
						if (chkTeller[intCounter].CheckState == Wisej.Web.CheckState.Checked)
						{
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", fecherFoundation.Strings.UCase(Tellers[intCounter]));
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "' AND Upper(TellerID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								rsPC1.Edit();
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							}
							else
							{
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", fecherFoundation.Strings.UCase(Tellers[intCounter]));
								rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
							}
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							MotorVehicle.Statics.strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for Special Registrations
							MotorVehicle.Statics.strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for Transit Plates
							MotorVehicle.Statics.strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for Booster Permits
							MotorVehicle.Statics.strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for Gift Certificates
							MotorVehicle.Statics.strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for InventoryAdjustments
							MotorVehicle.Statics.strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for ExciseTax
							MotorVehicle.Statics.strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for ExceptionReport
							MotorVehicle.Statics.strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for TitleApplications
							MotorVehicle.Statics.strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for TemporaryPlateRegister
							MotorVehicle.Statics.strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for Inventory
							MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
						}
					}
					else
					{
						if (chkTeller[intCounter].Visible == true)
						{
							// check to see if there is a closeout entry for this teller or if this is the first one
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerID = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								// do nothing
							}
							else
							{
								// if this is the first entry add one in for yesterday so they can do a teller report with teller closeout period
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", fecherFoundation.Strings.UCase(Tellers[intCounter]));
								rsPC1.Set_Fields("TellerCloseoutDate", fecherFoundation.DateAndTime.DateAdd("d", -1, DateTime.Today));
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
								rsPC1.Update();
							}
							// make a closeout entry in the TellerCloseoutTable table in the database
							rsPC1.OpenRecordset("SELECT * FROM TellerCloseoutTable WHERE TellerCloseoutDate = '" + DateTime.Today.ToShortDateString() + "' AND Upper(TellerID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'");
							if (rsPC1.EndOfFile() != true && rsPC1.BeginningOfFile() != true)
							{
								rsPC1.Edit();
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
							}
							else
							{
								rsPC1.AddNew();
								rsPC1.Set_Fields("TellerID", fecherFoundation.Strings.UCase(Tellers[intCounter]));
								rsPC1.Set_Fields("TellerCloseoutDate", DateTime.Today);
								rsPC1.Set_Fields("TellerCloseoutTime", fecherFoundation.DateAndTime.TimeOfDay);
								rsPC1.Set_Fields("PeriodCloseoutID", 0);
							}
							rsPC1.Update();
							lngID = FCConvert.ToInt32(rsPC1.Get_Fields_Int32("ID"));
							// Set Teller CloseoutID for ActivityMaster
							MotorVehicle.Statics.strSql = "SELECT * FROM ActivityMaster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for Special Registrations
							MotorVehicle.Statics.strSql = "SELECT * FROM SpecialRegistration WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for Transit Plates
							MotorVehicle.Statics.strSql = "SELECT * FROM TransitPlates WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for Booster Permits
							MotorVehicle.Statics.strSql = "SELECT * FROM Booster WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for Gift Certificates
							MotorVehicle.Statics.strSql = "SELECT * FROM GiftCertificateRegister WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for InventoryAdjustments
							MotorVehicle.Statics.strSql = "SELECT * FROM InventoryAdjustments WHERE TellerCloseoutID < 1 AND AdjustmentCode <> 'P' AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for ExciseTax
							MotorVehicle.Statics.strSql = "SELECT * FROM ExciseTax WHERE TellerCloseoutID < 1";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for ExceptionReport
							MotorVehicle.Statics.strSql = "SELECT * FROM ExceptionReport WHERE TellerCloseoutID < 1";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for TitleApplications
							MotorVehicle.Statics.strSql = "SELECT * FROM TitleApplications WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for TemporaryPlateRegister
							MotorVehicle.Statics.strSql = "SELECT * FROM TemporaryPlateRegister WHERE TellerCloseoutID < 1";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
							// Set Teller CloseoutID for Inventory
							MotorVehicle.Statics.strSql = "SELECT * FROM Inventory WHERE TellerCloseoutID < 1 AND UPPER(OpID) = '" + fecherFoundation.Strings.UCase(Tellers[intCounter]) + "'";
							if (MotorVehicle.SetTellerID(ref MotorVehicle.Statics.strSql, ref lngID) == false)
								goto TellerEndError;
						}
					}
				}
				MessageBox.Show("Teller Closeout Completed Successfully!!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				rsPC1.Reset();
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				rsPC1.FindFirstRecord("ID", lngID);
				rsPC1.Delete();
				rsPC1.Update();
				rsPC1.Reset();
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = 0;
				MessageBox.Show("Teller Closeout was not successful.  Error number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " was encountered.  (" + fecherFoundation.Information.Err(ex).Description + ")", "Error processing closeout.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				fecherFoundation.Information.Err(ex).Clear();
				return;
			}
			TellerEndError:
			;
			rsPC1.FindFirstRecord("ID", lngID);
			rsPC1.Delete();
			rsPC1.Update();
			rsPC1.Reset();
			frmWait.InstancePtr.Unload();
			FCGlobal.Screen.MousePointer = 0;
			MessageBox.Show("Teller Closeout was not successful.  Error number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + " was encountered.  (" + fecherFoundation.Information.Err().Description + ")", "Error processing closeout.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			fecherFoundation.Information.Err().Clear();
			return;
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmTellerCloseout_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.TELLERCLOSEOUT))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			SetTellers();
			this.Refresh();
		}

		private void frmTellerCloseout_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTellerCloseout properties;
			//frmTellerCloseout.FillStyle	= 0;
			//frmTellerCloseout.ScaleWidth	= 9300;
			//frmTellerCloseout.ScaleHeight	= 7080;
			//frmTellerCloseout.LinkTopic	= "Form2";
			//frmTellerCloseout.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmTellerCloseout_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void frmTellerCloseout_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SetTellers()
		{
			clsDRWrapper rs = new clsDRWrapper();
			// vbPorter upgrade warning: fnx As int	OnWriteFCConvert.ToInt32(
			int fnx;
			rs.OpenRecordset("SELECT * FROM Operators", "SystemSettings");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				for (fnx = 0; fnx <= (rs.RecordCount() - 1); fnx++)
				{
					chkTeller[fnx].Visible = true;
				}
				fnx = 0;
				do
				{
					chkTeller[fnx].Text = FCConvert.ToString(rs.Get_Fields_String("Name"));
					Tellers[fnx] = FCConvert.ToString(rs.Get_Fields("Code"));
					fnx += 1;
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int fnx;
			for (fnx = 0; fnx <= 15; fnx++)
			{
				if (chkTeller[fnx].Visible == true)
				{
					chkTeller[fnx].CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			fraTellers.Enabled = false;
		}

		private void optSelect_CheckedChanged(object sender, System.EventArgs e)
		{
			fraTellers.Enabled = true;
		}

		private void SetCustomFormColors()
		{
			cmbAll.ForeColor = Color.Blue;
		}

		public void cmbAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAll.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Closeout Selected")
			{
				optSelect_CheckedChanged(sender, e);
			}
		}
	}
}
