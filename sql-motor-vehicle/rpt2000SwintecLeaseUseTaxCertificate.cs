//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rpt2000SwintecLeaseUseTaxCertificate.
	/// </summary>
	public partial class rpt2000SwintecLeaseUseTaxCertificate : BaseSectionReport
	{
		public rpt2000SwintecLeaseUseTaxCertificate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Use Tax Certificate";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rpt2000SwintecLeaseUseTaxCertificate InstancePtr
		{
			get
			{
				return (rpt2000SwintecLeaseUseTaxCertificate)Sys.GetInstance(typeof(rpt2000SwintecLeaseUseTaxCertificate));
			}
		}

		protected rpt2000SwintecLeaseUseTaxCertificate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt2000SwintecLeaseUseTaxCertificate	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// Last Updated:              01/28/2002
		private void GetInformation()
		{
			// this sub will get all of the information needed and fill the fields on the Form
			double dblTaxAmount;
			double dblPurchasePrice;
			double dblAllowance;
			double dblPaymentAmount;
			int lngNumberOfPayments;
			double dblDownPayment;
			clsDRWrapper rsUseTax = new clsDRWrapper();
			int TB;
			string SellerAddress;
			string ExmptCode;
			string Owner = "";
			string LienHolder1 = "";
			int x;
			// 
			rsUseTax.OpenRecordset("SELECT * FROM UseTax WHERE ID = " + FCConvert.ToString(MotorVehicle.Statics.lngUTCAddNewID), "TWMV0000.vb1");
			if (rsUseTax.EndOfFile() != true && rsUseTax.BeginningOfFile() != true)
			{
				rsUseTax.MoveLast();
				rsUseTax.MoveFirst();
			}
			else
			{
				MotorVehicle.Statics.blnPrintUseTax = false;
				this.Cancel();
				return;
			}
			fldMake1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("make");
			fldModel1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("model");
			fldYear1.Text = FCConvert.ToString(MotorVehicle.Statics.rsFinal.Get_Fields("Year"));
			fldVin1.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Vin");
			fldSellerName.Text = rsUseTax.Get_Fields_String("SellerName");
			fldDateofTransfer.Text = Strings.Format(rsUseTax.Get_Fields("TradedDate"), "MM/dd/yyyy");
			SellerAddress = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerAddress1"))) + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerCity"))) + " " + rsUseTax.Get_Fields_String("SellerState") + " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("SellerZipAndZip4")));
			fldSellerAddress.Text = fecherFoundation.Strings.Trim(SellerAddress);
			dblPaymentAmount = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Double("LeasePaymentAmount")) == false)
				dblPaymentAmount = rsUseTax.Get_Fields_Double("LeasePaymentAmount");
			fldLeasePayment.Text = Strings.Format(dblPaymentAmount, "#,###");
			lngNumberOfPayments = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Int32("NumberOfPayments")) == false)
				lngNumberOfPayments = FCConvert.ToInt32(rsUseTax.Get_Fields_Int32("NumberOfPayments"));
			fldNumberOfPayments.Text = Strings.Format(lngNumberOfPayments, "#,###");
			fldPaymentTotal.Text = Strings.Format(dblPaymentAmount * lngNumberOfPayments, "#,###");
			dblDownPayment = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Double("LeaseDownPayment")) == false)
				dblDownPayment = rsUseTax.Get_Fields_Double("LeaseDownPayment");
			fldDownPayment.Text = Strings.Format(dblDownPayment, "#,###");
			dblPurchasePrice = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("PurchasePrice")) == false)
				dblPurchasePrice = Conversion.Val(rsUseTax.Get_Fields_Decimal("PurchasePrice"));
			dblAllowance = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("Allowance")) == false)
				dblAllowance = Conversion.Val(rsUseTax.Get_Fields_Decimal("Allowance"));
			fldAllowance.Text = Strings.Format(dblAllowance, "#,###");
			fldTotalAmount.Text = Strings.Format(dblPurchasePrice + dblAllowance, "#,###");
			dblTaxAmount = 0;
			if (fecherFoundation.FCUtils.IsNull(rsUseTax.Get_Fields_Decimal("TaxAmount")) == false)
				dblTaxAmount = Conversion.Val(rsUseTax.Get_Fields_Decimal("TaxAmount"));
			fldUseTax.Text = Strings.Format(dblTaxAmount, "#,##0.00");
			ExmptCode = FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptCode"));
			if (ExmptCode == "A")
			{
				fldExemptTypeA.Text = "A";
				fldExemptNumber.Text = rsUseTax.Get_Fields_String("ExemptNumber");
			}
			else if (ExmptCode == "B")
			{
				fldExemptTypeB.Text = "B";
			}
			else if (ExmptCode == "C")
			{
				fldExemptTypeC.Text = "C";
			}
			else if (ExmptCode == "D")
			{
				fldExemptTypeD.Text = "D";
				fldOtherReason.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("ExemptDescription")));
			}
			fldPurchaserName.Text = rsUseTax.Get_Fields_String("PurchaserName");
			fldPurchaserSocialSec.Text = rsUseTax.Get_Fields_String("PurchaserSSN");
			fldPurchaserAddress.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rsUseTax.Get_Fields_String("PurchaserAddress")));
			fldPurchaserCity.Text = rsUseTax.Get_Fields_String("PurchaserCity");
			fldPurchaserState.Text = rsUseTax.Get_Fields_String("PurchaserState");
			fldPurchaserZip.Text = rsUseTax.Get_Fields_String("PurchaserZipAndZip4");
			if (MotorVehicle.Statics.FleetUseTax)
			{
				fldClassPlate.Text = "S.A.L.";
			}
			else
			{
				fldClassPlate.Text = MotorVehicle.Statics.rsFinal.Get_Fields_String("Class") + " " + MotorVehicle.Statics.rsFinal.Get_Fields_String("Plate");
			}
			fldRegDate.Text = Strings.Format(DateTime.Today, "MM/dd/yy");
			fldTaxAmount.Text = Strings.Format(Strings.Format(dblTaxAmount, "###,###.00"), "@@@@@@@@@@");
			rsUseTax.DisposeOf();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Printer tempPrinter = new Printer();
			int x;
			this.Document.Printer.PrinterName = MotorVehicle.Statics.gstrCTAPrinterName;
			/*? For Each */
			foreach (FCPrinter p in FCGlobal.Printers)
			{
				//tempPrinter(In Printers);
				if (p.DeviceName == MotorVehicle.Statics.gstrCTAPrinterName)
				{
					if (tempPrinter.DriverName == "EPSON24")
					{
						for (x = 0; x <= this.Detail.Controls.Count - 1; x++)
						{
							(this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font("Courier 10cpi", (this.Detail.Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font.Size);
						}
					}
					break;
				}
			}
			/*? Next */
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			GetInformation();
			MotorVehicle.Statics.blnPrintUseTax = true;
		}

		private void rpt2000SwintecLeaseUseTaxCertificate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rpt2000SwintecLeaseUseTaxCertificate properties;
			//rpt2000SwintecLeaseUseTaxCertificate.Caption	= "Use Tax Certificate";
			//rpt2000SwintecLeaseUseTaxCertificate.Icon	= "rpt2000SwintecLeaseUseTaxCertificate.dsx":0000";
			//rpt2000SwintecLeaseUseTaxCertificate.Left	= 0;
			//rpt2000SwintecLeaseUseTaxCertificate.Top	= 0;
			//rpt2000SwintecLeaseUseTaxCertificate.Width	= 11880;
			//rpt2000SwintecLeaseUseTaxCertificate.Height	= 8595;
			//rpt2000SwintecLeaseUseTaxCertificate.StartUpPosition	= 3;
			//rpt2000SwintecLeaseUseTaxCertificate.SectionData	= "rpt2000SwintecLeaseUseTaxCertificate.dsx":058A;
			//End Unmaped Properties
		}
	}
}
