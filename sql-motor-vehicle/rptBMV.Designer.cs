﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptBMV.
	/// </summary>
	partial class rptBMV
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBMV));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblVehicles = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPlate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOwner = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMake = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblModel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtError = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblVehicleCount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblVehicles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtError)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVehicleCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtError,
				this.txtPlate,
				this.txtOwner,
				this.txtYear,
				this.txtMake,
				this.txtModel
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblVehicles,
				this.lblPlate,
				this.lblOwner,
				this.lblYear,
				this.lblMake,
				this.lblModel,
				this.Line1,
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3
			});
			this.PageHeader.Height = 0.90625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblVehicleCount
			});
			this.GroupFooter1.Height = 0.3958333F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblVehicles
			// 
			this.lblVehicles.Height = 0.1875F;
			this.lblVehicles.HyperLink = null;
			this.lblVehicles.Left = 0F;
			this.lblVehicles.Name = "lblVehicles";
			this.lblVehicles.Style = "font-family: \'Tahoma\'; text-align: center";
			this.lblVehicles.Text = "Label2";
			this.lblVehicles.Top = 0.375F;
			this.lblVehicles.Width = 6.96875F;
			// 
			// lblPlate
			// 
			this.lblPlate.Height = 0.1875F;
			this.lblPlate.HyperLink = null;
			this.lblPlate.Left = 0.125F;
			this.lblPlate.Name = "lblPlate";
			this.lblPlate.Style = "font-family: \'Tahoma\'";
			this.lblPlate.Text = "Plate";
			this.lblPlate.Top = 0.6875F;
			this.lblPlate.Width = 0.375F;
			// 
			// lblOwner
			// 
			this.lblOwner.Height = 0.1875F;
			this.lblOwner.HyperLink = null;
			this.lblOwner.Left = 1F;
			this.lblOwner.Name = "lblOwner";
			this.lblOwner.Style = "font-family: \'Tahoma\'";
			this.lblOwner.Text = "Owner";
			this.lblOwner.Top = 0.6875F;
			this.lblOwner.Width = 0.5F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 4.3125F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Tahoma\'";
			this.lblYear.Text = "Year";
			this.lblYear.Top = 0.6875F;
			this.lblYear.Width = 0.4375F;
			// 
			// lblMake
			// 
			this.lblMake.Height = 0.1875F;
			this.lblMake.HyperLink = null;
			this.lblMake.Left = 5.0625F;
			this.lblMake.Name = "lblMake";
			this.lblMake.Style = "font-family: \'Tahoma\'";
			this.lblMake.Text = "Make";
			this.lblMake.Top = 0.6875F;
			this.lblMake.Width = 0.5625F;
			// 
			// lblModel
			// 
			this.lblModel.Height = 0.1875F;
			this.lblModel.HyperLink = null;
			this.lblModel.Left = 6.0625F;
			this.lblModel.Name = "lblModel";
			this.lblModel.Style = "font-family: \'Tahoma\'";
			this.lblModel.Text = "Model";
			this.lblModel.Top = 0.6875F;
			this.lblModel.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.875F;
			this.Line1.Width = 6.875F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 6.9375F;
			this.Line1.Y1 = 0.875F;
			this.Line1.Y2 = 0.875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "BMV Update File Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 7F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 5.65625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 5.65625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// txtError
			// 
			this.txtError.Height = 0.1875F;
			this.txtError.Left = 0.1875F;
			this.txtError.Name = "txtError";
			this.txtError.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtError.Text = null;
			this.txtError.Top = 0F;
			this.txtError.Width = 6.34375F;
			// 
			// txtPlate
			// 
			this.txtPlate.Height = 0.1875F;
			this.txtPlate.Left = 0.09375F;
			this.txtPlate.Name = "txtPlate";
			this.txtPlate.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtPlate.Text = "Field1";
			this.txtPlate.Top = 0F;
			this.txtPlate.Width = 0.8125F;
			// 
			// txtOwner
			// 
			this.txtOwner.Height = 0.1875F;
			this.txtOwner.Left = 1F;
			this.txtOwner.Name = "txtOwner";
			this.txtOwner.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtOwner.Text = "Field1";
			this.txtOwner.Top = 0F;
			this.txtOwner.Width = 3.15625F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.1875F;
			this.txtYear.Left = 4.28125F;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtYear.Text = "Field1";
			this.txtYear.Top = 0F;
			this.txtYear.Width = 0.5625F;
			// 
			// txtMake
			// 
			this.txtMake.Height = 0.1875F;
			this.txtMake.Left = 5.09375F;
			this.txtMake.Name = "txtMake";
			this.txtMake.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtMake.Text = "Field1";
			this.txtMake.Top = 0F;
			this.txtMake.Width = 0.6875F;
			// 
			// txtModel
			// 
			this.txtModel.Height = 0.1875F;
			this.txtModel.Left = 6.09375F;
			this.txtModel.Name = "txtModel";
			this.txtModel.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.txtModel.Text = "Field1";
			this.txtModel.Top = 0F;
			this.txtModel.Width = 0.84375F;
			// 
			// lblVehicleCount
			// 
			this.lblVehicleCount.Height = 0.1875F;
			this.lblVehicleCount.HyperLink = null;
			this.lblVehicleCount.Left = 3.25F;
			this.lblVehicleCount.Name = "lblVehicleCount";
			this.lblVehicleCount.Style = "font-family: \'Tahoma\'";
			this.lblVehicleCount.Text = "Owner";
			this.lblVehicleCount.Top = 0.125F;
			this.lblVehicleCount.Width = 2.375F;
			// 
			// rptBMV
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblVehicles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtError)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVehicleCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtError;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOwner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtModel;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVehicles;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPlate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOwner;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMake;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblModel;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVehicleCount;
	}
}
