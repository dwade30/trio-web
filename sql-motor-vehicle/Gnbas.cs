﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;

namespace TWMV0000
{
	public class modGNBas
	{
		//=========================================================
		// These Declares are used to get the block cursor
		[DllImport("user32")]
		public static extern int CreateCaret(int hwnd, int hBitmap, int nWidth, int nHeight);

		[DllImport("user32")]
		public static extern int ShowCaret(int hwnd);

		[DllImport("user32")]
		public static extern int GetFocus();

		[DllImport("user32")]
		public static extern object SetCursorPos(int X, int Y);
		// screen setup items
		const string Logo = "TRIO Software Corporation";
		public const string SearchInputBoxTitle = "Real Estate Assessment Search";

		[DllImport("kernel32")]
		public static extern void Sleep(int dwMilliseconds);

		[DllImport("kernel32")]
		public static extern int WinExec(string lpCmdLine, int nCmdShow);

		[DllImport("kernel32")]
		public static extern int GlobalAlloc(int wFlags, int dwBytes);

		public struct ControlProportions
		{
			public float WidthProportions;
			public float HeightProportions;
			public float TopProportions;
			public float LeftProportions;
		};
		//
		//
		public static void GetPaths()
		{
			Statics.PPDatabase = "P:\\";
			Statics.TrioEXE = "\\T2K\\";
			Statics.TrioDATA = "\\VBTRIO\\VBTEST\\";
			Statics.PictureFormat = ".BMP";
			Statics.SketchLocation = "\\VBTRIO\\VBTEST\\";
			Statics.Display = "TRIOTEMP.TXT";
		}

		public static void G9996_LOCK()
		{
			if (Statics.HighLockNumber == 0)
				Statics.HighLockNumber = Statics.LowLockNumber;
			///*? Lock #LockFileNumber, */			LowLockNumber; /*? To */ HighLockNumber;
			if (Statics.Bbort != "Y")
			{
				Statics.LocksHeld[Statics.LockFileNumber] += 1;
				Statics.LowLock[Statics.LockFileNumber, Statics.LocksHeld[Statics.LockFileNumber]] = Statics.LowLockNumber;
				Statics.HighLock[Statics.LockFileNumber, Statics.LocksHeld[Statics.LockFileNumber]] = Statics.HighLockNumber;
			}
			Statics.HighLockNumber = 0;
		}

		public static void G9997_UNLOCK()
		{
			// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
			int X;
			int Y;
			if (Statics.LocksHeld[Statics.LockFileNumber] != 0)
			{
				for (X = Statics.LocksHeld[Statics.LockFileNumber]; X >= 1; X--)
				{
					if (Statics.LowLock[Statics.LockFileNumber, X] != 0)
					{
						//Unlock(LockFileNumber, LowLock[LockFileNumber, X]); /*? To */ HighLock[(LockFileNumber), X]; /*? ) */
						Statics.LowLock[Statics.LockFileNumber, X] = 0;
						Statics.HighLock[Statics.LockFileNumber, X] = 0;
					}
				}
				// X
				Statics.LocksHeld[Statics.LockFileNumber] = 0;
			}
		}

		public static void G9998_ABORT()
		{
			int X;
			// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
			int Y;
			/*? On Error Resume Next  */// to be removed later
			for (X = 1; X <= 50; X++)
			{
				if (Statics.LocksHeld[X] > 0)
				{
					for (Y = Statics.LocksHeld[X]; Y >= 1; Y--)
					{
						if (Statics.LowLock[X, Y] != 0)
						{
							//Unlock(X, LowLock[X, Y]); /*? To */ HighLock[(X), Y]; /*? ) */
							Statics.LowLock[X, Y] = 0;
							Statics.HighLock[X, Y] = 0;
						}
					}
					// Y
				}
				Statics.LocksHeld[X] = 0;
			}
			// X
		}

		public static void ConvertToString()
		{
			Statics.IntVarTemp = Conversion.Str(Statics.IntvarNumeric);
			Statics.INTVARSTRING = "";
			Statics.IntVarTemp = fecherFoundation.Strings.LTrim(Statics.IntVarTemp);
			Statics.INTVARSTRING = Strings.StrDup(Statics.intLen - Statics.IntVarTemp.Length, "0") + Statics.IntVarTemp;
		}

		public static void ConvertEdited(ref string newfld)
		{
			// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
			int X;
			int Y;
			for (X = 1; X <= (newfld.Length); X++)
			{
				if (Strings.Mid(newfld, X, 1) == "," || Strings.Mid(newfld, X, 1) == "." || Strings.Mid(newfld, X, 1) == "$")
				{
					for (Y = X; Y >= 2; Y--)
					{
						Strings.MidSet(ref newfld, Y, 1, Strings.Mid(newfld, Y - 1, 1));
					}
					// Y
					Strings.MidSet(ref newfld, 1, 1, " ");
				}
			}
			// X
			newfld = fecherFoundation.Strings.Trim(newfld);
			// IntLen = 9
			Statics.IntvarNumeric = Conversion.Val(newfld);
			ConvertToString();
			if (Statics.Bbort == "Y")
				return;
		}

		public static void MakeCaption()
		{
			Statics.TitleCaption = "TRIO Software Corporation";
			Statics.TitleTime = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (Strings.Mid(Statics.TitleTime, 1, 1) == "0")
				Strings.MidSet(ref Statics.TitleTime, 1, 1, " ");
		}

		public static void ErrorRoutine()
		{
			//vbPorterConverter.GoSubStack mGoSubStack = new vbPorterConverter.GoSubStack();
			goto GoSubPass;
			GoSubReturn:
			//int GoSubLastPosition = mGoSubStack.GetPosition();
			//if (GoSubLastPosition == 1) goto GoSubReturnPosition1;
			//MessageBox.Show("Return without GoSub");
			GoSubPass:
			// vbPorter upgrade warning: answer As object	OnWrite(DialogResult)
			DialogResult answer;
			FCGlobal.Screen.MousePointer = 0;
			if (fecherFoundation.Information.Err().Number == 24 || fecherFoundation.Information.Err().Number == 25 || fecherFoundation.Information.Err().Number == 27 || fecherFoundation.Information.Err().Number == 71)
			{
				answer = MessageBox.Show("Printer appears to not be available.", null, MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);
				if (answer != DialogResult.Cancel)
				{
					MessageBox.Show("Processing-------", "Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					Statics.Bbort = "Y";
				}
			}
			else if (modReplaceWorkFiles.Statics.gstrNetworkFlag != "N" && fecherFoundation.Information.Err().Number > 69 && fecherFoundation.Information.Err().Number < 71)
			{
				//mGoSubStack.PutPosition(1); goto Err_9999_NET;
			}
			else
			{
				MessageBox.Show("  Error Number " + fecherFoundation.Information.Err().Number + "  " + fecherFoundation.Information.Err().Description + "\r\n" + "  Has been encountered - Program Terminated", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Statics.Bbort = "Y";
			}
			GoSubReturnPosition1:
			;
			if (Statics.EntryMenuFlag == false)
			{
				modBlockEntry.WriteYY();
                //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
                //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
                App.MainForm.OpenModule("TWGNENTY");
            }
            Wisej.Web.Application.Exit();
			Err_9999_NET:
			;
			answer = MessageBox.Show("Record is locked - Do you want to retry?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (answer == DialogResult.No)
				Statics.Bbort = "Y";
			goto GoSubReturn;
		}

		public static void PrintFormat()
		{
			// vbPorter upgrade warning: fmtextraspc As int	OnWriteFCConvert.ToInt32(
			int fmtextraspc;
			// vbPorter upgrade warning: Formatted As object	OnWrite(string)
			object Formatted = null;
			if (Statics.fmttype == "Whole")
			{
				Formatted = Strings.Format(Statics.fmtval, "###0");
			}
			else if (Statics.fmttype == "WholeC")
			{
				Formatted = Strings.Format(Statics.fmtval, "#,##0");
			}
			else if (Statics.fmttype == "Dollar")
			{
				Formatted = Strings.Format(Statics.fmtval / 100.0, "0.00");
			}
			else if (Statics.fmttype == "DollarC")
			{
				Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0.00");
			}
			else if (Statics.fmttype == "WholeS")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "###0-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "###0+");
				}
			}
			else if (Statics.fmttype == "WholeSC")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "#,##0-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0+");
				}
			}
			else if (Statics.fmttype == "DollarS")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "0.00-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "0.00+");
				}
			}
			else if (Statics.fmttype == "DollarSC")
			{
				if (Statics.fmtval < 0)
				{
					Formatted = Strings.Format((Statics.fmtval * -1) / 100.0, "#,##0.00-");
				}
				else
				{
					Formatted = Strings.Format(Statics.fmtval / 100.0, "#,##0.00+");
				}
			}
			fmtextraspc = 0;
			if (FCConvert.ToString(Formatted).Length < Statics.fmtlgth)
				fmtextraspc = (Statics.fmtlgth - FCConvert.ToString(Formatted).Length);
			if (fmtextraspc != 0)
				FCFileSystem.Print(40, "");
			if (Statics.fmtspcbef != 0)
				FCFileSystem.Print(40, "");
			FCFileSystem.Print(40, Formatted.ToStringES());
			if (Statics.fmtspcaft != 0)
				FCFileSystem.Print(40, "");
			if (Statics.fmtcolon != "Y")
				FCFileSystem.PrintLine(40, " ");
		}

		public static void CheckNumeric()
		{
			for (Statics.X = 1; Statics.X <= (FCConvert.ToString(Statics.Resp1).Length); Statics.X++)
			{
				Statics.Resp2 = Convert.ToByte(Strings.Mid(FCConvert.ToString(Statics.Resp1), Statics.X, 1)[0]);
				if (Statics.Resp2 < 48 || Statics.Resp2 > 57)
				{
					string str = FCConvert.ToString(Statics.Resp1);
					Strings.MidSet(ref str, Statics.X, Strings.Len(Statics.Resp1) - Statics.X, Strings.Mid(FCConvert.ToString(Statics.Resp1), Statics.X + 1, FCConvert.ToString(Statics.Resp1).Length - 1));
					str = FCConvert.ToString(Statics.Resp1);
					Strings.MidSet(ref str, Strings.Len(Statics.Resp1), 1, " ");
					break;
				}
			}
			// X
		}

		public static object SetInsertMode(ref int InsertMode)
		{
			object SetInsertMode = null;
			// 0 = Insert Mode        = SelLength of 0
			// 1 = OverType Mode      = SelLength of 1
			// The Variable "OverType" returns either 0 or 1 for SelLength
			// 
			if ((FCGlobal.Screen.ActiveControl as FCTextBox).SelectionLength == 1)
			{
				Statics.OverType = 0;
			}
			else
			{
				Statics.OverType = 1;
			}
			return SetInsertMode;
		}

		public static void GetValue()
		{
			// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
			int Y;
			// vbPorter upgrade warning: YY As int	OnWriteFCConvert.ToInt32(
			int YY;
			Statics.NewData = fecherFoundation.Strings.Trim(Statics.NewData);
			for (Y = 1; Y <= (Statics.NewData.Length); Y++)
			{
				if (Strings.Mid(Statics.NewData, Y, 1) == "," || Strings.Mid(Statics.NewData, Y, 1) == ".")
				{
					for (YY = Y; YY <= (Statics.NewData.Length - 1); YY++)
					{
						Strings.MidSet(ref Statics.NewData, YY, 1, Strings.Mid(Statics.NewData, YY + 1, 1));
					}
					// YY
					Strings.MidSet(ref Statics.NewData, Strings.Len(Statics.NewData), 1, " ");
				}
				else if (fecherFoundation.Strings.UCase(Strings.Mid(Statics.NewData, Y, 1)) == "C")
				{
					Statics.NewData = "0";
					break;
				}
			}
			// Y
			Statics.NewValue = FCConvert.ToInt32(Math.Round(Conversion.Val(Statics.NewData)));
		}

		public static double Round_8(double dValue, short iDigits)
		{
			return Round(ref dValue, ref iDigits);
		}
		// vbPorter upgrade warning: dValue As double	OnWrite(Decimal, double)
		public static double Round(ref double dValue, ref short iDigits)
		{
			double Round = 0;
            //FC:FINAL:JTA - double type will change the value after multiplication (100 * 284.715 will return 28471.4999999999999...). Use Decimal type instead
            //Round = Conversion.Int(Conversion.Val(dValue * (Math.Pow(10, iDigits))) + 0.5) / (Math.Pow(10, iDigits));
            Round = Conversion.Int(Conversion.Val(FCConvert.ToDecimal(dValue * (Math.Pow(10, iDigits))) + 0.5m)) / (Math.Pow(10, iDigits));
            return Round;
		}

		public static string Quotes(ref string strValue)
		{
			string Quotes = "";
			Quotes = FCConvert.ToString(Convert.ToChar(34)) + strValue + FCConvert.ToString(Convert.ToChar(34));
			return Quotes;
		}

		public static void LockRecord(ref int lngLowNumber, ref int lngHighNumber, ref int intFileNumber)
		{
			/*? On Error Resume Next  */
			if (lngHighNumber == 0)
				lngHighNumber = lngLowNumber;
			///*? Lock #intFileNumber, */			lngLowNumber; /*? To */ lngHighNumber;
			Statics.LocksHeld[intFileNumber] += 1;
			Statics.LowLock[intFileNumber, Statics.LocksHeld[intFileNumber]] = lngLowNumber;
			Statics.HighLock[intFileNumber, Statics.LocksHeld[intFileNumber]] = lngHighNumber;
			fecherFoundation.Information.Err().Clear();
		}

		public static void UnLockRecord(ref int lngLowNumber, ref int lngHighNumber, ref int intFileNumber)
		{
			/*? On Error Resume Next  */// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
			int X;
			int Y;
			if (Statics.LocksHeld[intFileNumber] != 0)
			{
				for (X = Statics.LocksHeld[intFileNumber]; X >= 1; X--)
				{
					if (Statics.LowLock[intFileNumber, X] != 0)
					{
						//Unlock(intFileNumber, LowLock[intFileNumber, X]); /*? To */ HighLock[(intFileNumber), X]; /*? ) */
						Statics.LowLock[intFileNumber, X] = 0;
						Statics.HighLock[intFileNumber, X] = 0;
					}
				}
				// X
				Statics.LocksHeld[intFileNumber] = 0;
			}
			fecherFoundation.Information.Err().Clear();
		}

		public static void UnlockALLRecords()
		{
			/*? On Error Resume Next  */
			int X;
			// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
			int Y;
			for (X = 1; X <= 50; X++)
			{
				if (Statics.LocksHeld[X] > 0)
				{
					for (Y = Statics.LocksHeld[X]; Y >= 1; Y--)
					{
						if (Statics.LowLock[X, Y] != 0)
						{
							//Unlock(X, LowLock[X, Y]); /*? To */ HighLock[(X), Y]; /*? ) */
							Statics.LowLock[X, Y] = 0;
							Statics.HighLock[X, Y] = 0;
						}
					}
					// Y
				}
				Statics.LocksHeld[X] = 0;
			}
			// X
			fecherFoundation.Information.Err().Clear();
		}

		public static bool ExitTrioNow()
		{
			bool ExitTrioNow = false;
			// vbPorter upgrade warning: ans As object	OnWrite(DialogResult)
			DialogResult ans;
			// msgbox '260' = vbyesno with the 'NO' the default button.
			ans = MessageBox.Show("Would you like to exit TRIO?", "Exit TRIO", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.Yes)
			{
				ExitTrioNow = true;
				FCFileSystem.FileClose();
				Wisej.Web.Application.Exit();
			}
			else
			{
				ExitTrioNow = false;
			}
			return ExitTrioNow;
		}
		// vbPorter upgrade warning: frm As Form	OnWrite(frmPrePrints, frmTransferAnalysis, frmSpecialPermit, frmTransitPlate, frmBoosterPlate, frmFleetMaster, frmPrintFleet, frmPurgeHeld, frmTellerReport, frmTellerCloseout, frmPurgeTerminated, frmVehicleUpdate, frmPurgeReports, frmSettings, frmReminder, frmPurgeExpired, frmPurgeArchive, frmPurgeAuditArchive)
		public static void SaveWindowSize(FCForm frm)
		{
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainLeft", FCConvert.ToString(frm.LeftOriginal));
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainTop", FCConvert.ToString(frm.TopOriginal));
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainWidth", FCConvert.ToString(frm.WidthOriginal));
			Interaction.SaveSetting("TWGNENTY", "GENERAL_SETTINGS", "MainHeight", FCConvert.ToString(frm.HeightOriginal));
		}
		// vbPorter upgrade warning: frm As Form	OnWrite(frmTableMaintenance, frmLeaseUseTax, frmCTA, frmInventory, frmPreview, frmReasonCodes, frmPrePrints, frmTransferAnalysis, frmExceptionReportItems, frmInventoryStatus, frmSpecialPermit, frmTransitPlate, frmBoosterPlate, frmFleetMaster, frmPlateInfo, frmPrintFleet, frmPurgeHeld, frmPrintVIN, frmTellerReport, frmTellerCloseout, frmPurgeTerminated, frmVehicleUpdate, frmSettings, frmReminder, frmUseTax, frmDataInput, frmSingleLongTermCTA)
		public static void GetWindowSize(FCForm frm)
		{
			// Dim lngHeight   As Long
			// Dim lngWidth    As Long
			// Dim lngTop      As Long
			// Dim lngLeft     As Long
			// 
			// lngHeight = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainHeight", 6500)
			// lngLeft = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainLeft", 1000)
			// lngTop = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainTop", 1000)
			// lngWidth = GetSetting("TWGNENTY", "GENERAL_SETTINGS", "MainWidth", 6500)
			// If lngHeight <> frm.Height Then
			// If lngWidth <> frm.Width Then
			// If frm.WindowState <> vbNormal Then
			// frm.WindowState = vbNormal
			// End If
			// frm.Height = lngHeight
			// frm.Left = lngLeft
			// frm.Top = lngTop
			// frm.Width = lngWidth
			// If Screen.Height - frm.Height <= 100 Then
			// If Screen.Width - frm.Width <= 100 Then
			// frm.WindowState = vbMaximized
			// End If
			// End If
			// End If
			// End If
			modGlobalFunctions.SetFixedSize(frm, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		public class StaticVariables
		{
			public string Application = "";
			public string ProgramName = "";
			public string ProgramShort = "";
			public string TitleCaption = string.Empty;
			public string TitleTime = string.Empty;
			public string Ltime = "";
			// used in format print statements
			public int fmtlgth;
			public int fmtval;
			public string fmttype = "";
			public FCFixedString fmtcolon = new FCFixedString(1);
			public int fmtspcbef;
			public int fmtspcaft;
			// used in GNMenu
			public string MenuTitle = "";
			public string MenuShort = "";
			public int MenuCount;
			public FCFixedString MenuOptions = new FCFixedString(19);
			public string[] MenuTitles = new string[19 + 1];
			public FCFixedString MenuSelected = new FCFixedString(1);
			// used in ConvertToString
			public double IntvarNumeric;
			public string INTVARSTRING = string.Empty;
			public string IntVarTemp = string.Empty;
			public int intLen;
			public string newfld = "";
			// used by search routines
			public string SEQ = "";
			public int House;
			public string SEARCH = "";
			public bool LongScreenSearch;
			// used for insert/overtype mode in text boxes
			public int OverType;
			// used in getvalue
			public string NewData = "";
			public int NewValue;
			//
			public FCFixedString EntryMenuResponse = new FCFixedString(2);
			public FCFixedString MainMenuResponse = new FCFixedString(1);
			public FCFixedString MenuResponse = new FCFixedString(1);
			//
			public FCFixedString CurrentApplication = new FCFixedString(1);
			public string Escape = "";
			public int CommentAccount;
			public string CommentName = "";
			// vbPorter upgrade warning: X As int	OnWriteFCConvert.ToInt32(
			public int X;
			public string ABORT = "";
			public string Bbort = "";
			public string SortFileSpec = "";
			public int NextNumber;
			public bool SketcherCalled;
			public int Bp;
			// Message Box Response Variables
			// These are used to hold a Message Box Response
			public object MessageResp1;
			public object MessageResp2;
			public object MessageResp3;
			// Prefixes and Variables used for Pictures and Sketches
			public FCFileSystem SketchFile = new FCFileSystem();
			public FCFileSystem PictureFile = new FCFileSystem();
			public string PictureFormat = string.Empty;
			public string PictureLocation = "";
			public string DocumentLocationName = "";
			public string SketchLocation = string.Empty;
			// record locking fields
			public int LockFileNumber;
			public int LowLockNumber;
			public int HighLockNumber;
			public int[,] LowLock = new int[50 + 1, 20 + 1];
			public int[,] HighLock = new int[50 + 1, 20 + 1];
			public int[] LocksHeld = new int[50 + 1];
			public string CityTown = "";
			// Prefixes for open statements
			public string PPDatabase = string.Empty;
			public string TrioEXE = string.Empty;
			public string TrioDATA = string.Empty;
			public string TrioSDrive = "";
			// File Editting copying, moving, deleting
			public FCFileSystem CopyFile = new FCFileSystem();
			public FCFileSystem MoveFile = new FCFileSystem();
			public FCFileSystem DelFile = new FCFileSystem();
			public int[] SpecLength = new int[10 + 1];
			public string[] SpecCase = new string[10 + 1];
			public string[] SpecKeyword = new string[10 + 1];
			public string[] SpecM1 = new string[10 + 1];
			public string[] SpecM2 = new string[10 + 1];
			public string[] SpecM3 = new string[10 + 1];
			public string[] SpecM4 = new string[10 + 1];
			public string[] SpecM5 = new string[10 + 1];
			public string[] SpecM6 = new string[10 + 1];
			public string[] SpecM7 = new string[10 + 1];
			public string[] SpecM8 = new string[10 + 1];
			public string[] SpecM9 = new string[10 + 1];
			public string[] SpecM10 = new string[10 + 1];
			public string[] SpecType = new string[10 + 1];
			public string[] SpecParam = new string[10 + 1];
			public string[] SpecLow = new string[10 + 1];
			public string[] SpecHigh = new string[10 + 1];
			public int RespLength;
			public string RespCase = "";
			public string RespM1 = "";
			public string RespM2 = "";
			public string RespM3 = "";
			public string RespM4 = "";
			public string RespM5 = "";
			public string RespM6 = "";
			public string RespM7 = "";
			public string RespM8 = "";
			public string RespM9 = "";
			public string RespM10 = "";
			public string RespOK = "";
			public string RespType = "";
			public string RespParam = "";
			public string RespLow = "";
			public string RespHigh = "";
			// vbPorter upgrade warning: Response As object	OnWrite(string)
			public object Response;
			public object RespKeyword;
			public object Resp1;
			// vbPorter upgrade warning: Resp2 As object	OnWriteFCConvert.ToInt32(
			public int Resp2;
			public object Resp3;
			public object Resp4;
			public object Resp5;
			public object Resp6;
			public object Resp7;
			public object Resp8;
			public object Resp9;
			public object Resp10;
			public bool SystemSetup;
			public bool LinkPicture;
			// Public Declare Function sendmessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
			public string Display = string.Empty;
			public bool EntryMenuFlag;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
