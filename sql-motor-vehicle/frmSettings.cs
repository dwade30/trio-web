//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Drawing.Printing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.MotorVehicle.Commands;
using TWSharedLibrary;

namespace TWMV0000
{
	public partial class frmSettings : BaseForm
	{
		public frmSettings()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;

		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSettings InstancePtr
		{
			get
			{
				return (frmSettings)Sys.GetInstance(typeof(frmSettings));
			}
		}

		protected frmSettings _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsGeneral = new clsDRWrapper();
		// vbPorter upgrade warning: intMVR3LaserFrameHeight As int	OnWriteFCConvert.ToInt32(
		int intMVR3LaserFrameHeight;
		bool blnUnload;
		bool blnLoading;
		private clsGridAccount vsGrid = new clsGridAccount();
		private clsGridAccount vsExpGrid = new clsGridAccount();
		bool blnDirty;

		private void chkCreateJournal_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCreateJournal.CheckState == Wisej.Web.CheckState.Checked)
			{
				fraRevAcct.Enabled = true;
				vsRevAcct.Enabled = true;
				lblRevAcct.Enabled = true;
				vsExpAcct.Enabled = true;
				lblExpAcct.Enabled = true;
			}
			else
			{
				vsRevAcct.TextMatrix(0, 0, "");
				vsRevAcct.Enabled = false;
				fraRevAcct.Enabled = false;
				lblRevAcct.Enabled = false;
				vsExpAcct.TextMatrix(0, 0, "");
				vsExpAcct.Enabled = false;
				lblExpAcct.Enabled = false;
			}
			blnDirty = true;
		}

		private void chkCta_CheckedChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void chkUseTax_CheckedChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}



		private void cmdClearLocks_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.UnlockFleet();
			MessageBox.Show("Locks Cleared.", "Locks Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void cmdPrintLeasedUseTaxTest_Click(object sender, System.EventArgs e)
		{
			if (cmbLeaseUseTaxV1.Text == "Newer (Bigger margins)")
			{
				PrintLeasedUseTaxTest(1);
			}
			else
			{
				PrintLeasedUseTaxTest(2);
			}
		}

		private void PrintLeasedUseTaxTest(int intFormType)
		{
			if (modPrinterFunctions.PrintXsForAlignment("This 'X' should have printed next to the line USE TAX CERTIFICATE." + "\r\n" + "If this is correct press 'Yes', to reprint the 'X' press 'No', to quit press 'Cancel'.", 52, 1, MotorVehicle.Statics.gstrCTAPrinterName) != DialogResult.Cancel)
			{
				if (intFormType == 1)
				{
					rptLeaseUseTaxCertificateV2.InstancePtr.PrintTest();
					rptLeaseUseTaxCertificateV2.InstancePtr.Unload();
				}
				else
				{
					rptLeaseUseTaxCertificate.InstancePtr.PrintTest();
					rptLeaseUseTaxCertificate.InstancePtr.Unload();
				}
			}
			else
			{
				MotorVehicle.Statics.blnPrintUseTax = false;
			}
		}

		private void cmdPrintUseTaxTest_Click(object sender, System.EventArgs e)
		{
            rptUseTaxCert2017.InstancePtr.TestPrint = true;
            rptUseTaxCert2017.InstancePtr.PageSettings.Duplex = Duplex.Vertical;
            rptUseTaxCert2017.InstancePtr.PrintUTCForm = modRegistry.GetRegistryKey("USETAX_PRINTFORM", "MOTOR_VEHICLE") == "Y";
            rptUseTaxCert2017.InstancePtr.Run();

            if (modRegistry.GetRegistryKey("USETAX_DUPLEX", "MOTOR_VEHICLE") == "Y")
            {
	            rptUseTaxCert2017.InstancePtr.PrintReportOnDotMatrix("UseTaxPrinterName");
            }
            else
            {
	            rptUseTaxCert2017.InstancePtr.PrintReportOnDotMatrix("UseTaxPrinterName", printerSettings: new DirectPrintingParams() { FromPage = 1, ToPage = 1 });
	            MessageBox.Show("Printing front side. When finished, insert the paper to print the back of the form and click OK.");
	            rptUseTaxCert2017.InstancePtr.PrintReportOnDotMatrix("UseTaxPrinterName", printerSettings: new DirectPrintingParams() { FromPage = 2, ToPage = 2 });
            }

            rptUseTaxCert2017.InstancePtr.Unload();

		}

		private void frmSettings_Activated(object sender, System.EventArgs e)
		{
			string strReportDrive;
			string strTemp;
			
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.MOTORVEHICLESETTINGS))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}

            txtPrinterGroup.Text = StaticSettings.GlobalCommandDispatcher.Send(new GetPrinterGroup()).Result.ToString();			
            
			rsGeneral.OpenRecordset("SELECT * FROM WMV");
			if (FCConvert.ToInt32(rsGeneral.Get_Fields_Int16("MVR3Digits")) == 2)
			{
				cmbCheckDigits.Text = "2";
			}
			else if (FCConvert.ToInt32(rsGeneral.Get_Fields_Int16("MVR3Digits")) == 3)
			{
				cmbCheckDigits.Text = "3";
			}
			else if (FCConvert.ToInt32(rsGeneral.Get_Fields_Int16("MVR3Digits")) == 4)
			{
				cmbCheckDigits.Text = "4";
			}
			else if (FCConvert.ToInt32(rsGeneral.Get_Fields_Int16("MVR3Digits")) == 1)
			{
				cmbCheckDigits.Text = "1";
			}
			else if (FCConvert.ToInt32(rsGeneral.Get_Fields_Int16("MVR3Digits")) == 0)
			{
				cmbCheckDigits.Text = "0";
			}
			
			if (FCConvert.ToBoolean(rsGeneral.Get_Fields_Boolean("LongTermPrintUseTaxCTA")))
			{
				cmbLongTermPrintCTAUseTaxYes.Text = "Yes";
			}
			else
			{
				cmbLongTermPrintCTAUseTaxYes.Text = "No";
			}
			if (FCConvert.ToBoolean(rsGeneral.Get_Fields_Boolean("LongTermAutoPopulateUnit")))
			{
				chkAutoPopulateUnit.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkAutoPopulateUnit.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			strTemp = MotorVehicle.GetComputerMVSetting("MVRT10UseLaser");
			if (strTemp == "")
			{
				if (FCConvert.ToBoolean(rsGeneral.Get_Fields_Boolean("Longtermlaserforms")))
				{
					strTemp = "True";
				}
				else
				{
					strTemp = "False";
				}
			}
			if (strTemp != "")
			{
				if (FCConvert.CBool(strTemp))
				{
					cmbLongTermLaserFormsYes.Text = "Yes";
				}
				else
				{
					cmbLongTermLaserFormsYes.Text = "No";
				}
			}
			else
			{
				cmbLongTermLaserFormsYes.Text = "No";
			}
			
			txtFTPAddress.Text = FCConvert.ToString(rsGeneral.Get_Fields_String("LongTermFTPAddress"));
			txtFTPUser.Text = FCConvert.ToString(rsGeneral.Get_Fields_String("LongTermFTPUser"));
			txtFTPPassword.Text = FCConvert.ToString(rsGeneral.Get_Fields_String("LongTermFTPPassword"));
			if (FCConvert.ToBoolean(rsGeneral.Get_Fields_Boolean("RRCreateCRJournal")))
			{
				chkCreateJournal.CheckState = Wisej.Web.CheckState.Checked;
				vsRevAcct.TextMatrix(0, 0, fecherFoundation.Strings.Trim(rsGeneral.Get_Fields_String("RRRevAcct") + ""));
				vsExpAcct.TextMatrix(0, 0, fecherFoundation.Strings.Trim(rsGeneral.Get_Fields_String("RRExpAcct") + ""));
			}
			else
			{
				chkCreateJournal.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			
			txtLabelAdjustment.Text = FCConvert.ToString(MotorVehicle.GetMVVariable("LaserLabelAdjustment"));
			strTemp = MotorVehicle.GetComputerMVSetting("LaserMVRT10Adjustment");
			if (strTemp != "")
			{
				txtLongTermLaserAdjustment.Text = FCConvert.ToString(Conversion.Val(strTemp));
			}
			else
			{
				txtLongTermLaserAdjustment.Text = "0";
			}
			strTemp = MotorVehicle.GetComputerMVSetting("MVR3ETopAdjustment");
			if (strTemp != "")
			{
				txtMVR3E_TopAdj.Text = FCConvert.ToString(Conversion.Val(strTemp));
			}
			else
			{
				txtMVR3E_TopAdj.Text = "0";
			}
			strTemp = MotorVehicle.GetComputerMVSetting("MVR3ELeftAdjustment");
			if (strTemp != "")
			{
				txtMVR3E_LeftAdj.Text = FCConvert.ToString(Conversion.Val(strTemp));
			}
			else
			{
				txtMVR3E_LeftAdj.Text = "0";
			}
			if (FCConvert.ToBoolean(rsGeneral.Get_Fields_Boolean("PrintUseTax")))
			{
				cmbPrintUseTaxNo.Text = "Yes";
			}
			else
			{
				cmbPrintUseTaxNo.Text = "No";
			}
			strTemp = MotorVehicle.GetComputerMVSetting("UTCTopAdjustment");
			if (strTemp != "")
			{
				txtUTCTopAdj.Text = FCConvert.ToString(Conversion.Val(strTemp));
			}
			else
			{
				txtUTCTopAdj.Text = "0";
			}
			strTemp = MotorVehicle.GetComputerMVSetting("UTCLeftAdjustment");
			if (strTemp != "")
			{
				txtUTCLeftAdj.Text = FCConvert.ToString(Conversion.Val(strTemp));
			}
			else
			{
				txtUTCLeftAdj.Text = "0";
			}
            strTemp = modRegistry.GetRegistryKey("USETAX_DUPLEX", "MOTOR_VEHICLE");
            if (strTemp == "Y")
				chkUTCDuplex.CheckState = Wisej.Web.CheckState.Checked;
            strTemp = modRegistry.GetRegistryKey("USETAX_PRINTFORM", "MOTOR_VEHICLE");
            if (strTemp == "Y")
				chkUTCPrintForm.CheckState = Wisej.Web.CheckState.Checked;
			if (FCConvert.ToBoolean(rsGeneral.Get_Fields_Boolean("PrintUseTax")))
			{
				cmbPrintUseTaxNo.Text = "Yes";
			}
			else
			{
				cmbPrintUseTaxNo.Text = "No";
			}
			if (FCConvert.ToInt32(rsGeneral.Get_Fields_Int32("LeaseUseTaxFormVersion")) == 1)
			{
				cmbLeaseUseTaxV1.Text = "Newer (Bigger margins)";
			}
			else
			{
				cmbLeaseUseTaxV1.Text = "Original";
			}
			if (FCConvert.ToString(modRegistry.GetRegistryKey("ShowReminders")) != "")
			{
				if (FCConvert.ToBoolean(modRegistry.GetRegistryKey("ShowReminders")) == true)
				{
					cmbReminderNo.Text = "Yes";
				}
				else
				{
                    cmbReminderNo.Text = "No";
				}
			}
			else
			{
                cmbReminderNo.Text = "No";
			}
			
			if (FCConvert.ToString(rsGeneral.Get_Fields_String("UseTellerCloseout")) == "Y")
			{
				cmbTellerYes.Text = "Yes";
			}
			else
			{
				cmbTellerYes.Text = "No";
			}
			if (FCConvert.ToBoolean(rsGeneral.Get_Fields_Boolean("AllowExciseRebates")))
			{
				cmbExciseRebateNo.Text = "Yes";
			}
			else
			{
				cmbExciseRebateNo.Text = "No";
			}

			if (rsGeneral.Get_Fields_String("MVR3Warning") == "N" || rsGeneral.Get_Fields_String("MVR3Warning") == "0" || rsGeneral.Get_Fields_String("MVR3Warning").ToLower() == "false")
            {
				cmbYes.Text = "No";
			}
			else
			{
				cmbYes.Text = "Yes";
			}
			
			blnDirty = false;
			this.Refresh();
		}

		private void frmSettings_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (fecherFoundation.Strings.UCase(this.ActiveControl.GetName()) == "VSREVACCT")
			{
				modNewAccountBox.CheckFormKeyDown(vsRevAcct, vsRevAcct.Row, vsRevAcct.Col, KeyCode, Shift, vsRevAcct.EditSelStart, vsRevAcct.EditText, vsRevAcct.EditSelLength);
			}
			if (fecherFoundation.Strings.UCase(this.ActiveControl.GetName()) == "VSEXPACCT")
			{
				modNewAccountBox.CheckFormKeyDown(vsExpAcct, vsExpAcct.Row, vsExpAcct.Col, KeyCode, Shift, vsExpAcct.EditSelStart, vsExpAcct.EditText, vsExpAcct.EditSelLength);
			}
		}

		private void frmSettings_Load(object sender, System.EventArgs e)
		{
			vsGrid.GRID7Light = vsRevAcct;
			vsGrid.DefaultAccountType = "R";
			vsGrid.AccountCol = -1;
			vsExpGrid.GRID7Light = vsExpAcct;
			vsExpGrid.DefaultAccountType = "E";
			vsExpGrid.AccountCol = -1;
			if (modGlobalConstants.Statics.gboolBD)
			{
				fraRapidRenewal.Enabled = true;
				chkCreateJournal.Enabled = true;
			}
			if (MotorVehicle.Statics.gboolAllowLongTermTrailers)
			{
				if (!cmbPrinting.Items.Contains("Long Term"))
				{
					cmbPrinting.Items.Add("Long Term");
				}
			}
			else
			{
				if (cmbPrinting.Items.Contains("Long Term"))
				{
					cmbPrinting.Items.Remove("Long Term");
				}
			}

			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSettings_Resize(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			DialogResult ans = 0;
			if (blnDirty)
			{
				ans = MessageBox.Show("Are you sure you wish to exit this screen?", "Exit Screen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.No)
				{
					e.Cancel = true;
					return;
				}
			}
			if (this.WindowState != FormWindowState.Minimized)
			{
				modGNBas.SaveWindowSize(this);
			}
		}

		private void frmSettings_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			ValidateControls();
			if (fecherFoundation.Information.Err().Number == 0)
			{
				blnUnload = false;
				SaveInfo();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			ValidateControls();
			if (fecherFoundation.Information.Err().Number == 0)
			{
				blnUnload = true;
				SaveInfo();
			}
		}

		private void SaveInfo()
		{
			clsDRWrapper rsUser = new clsDRWrapper();
			cmbPrinting.Focus();
			rsUser.OpenRecordset("SELECT * FROM Operators WHERE upper(Code) = '" + MotorVehicle.Statics.OpID + "'", "TWGN0000.vb1");
			if (rsUser.EndOfFile() != true && rsUser.BeginningOfFile() != true)
			{
				if (FCConvert.ToInt32(rsUser.Get_Fields_String("Level")) == 3)
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (MotorVehicle.Statics.OpID != "988")
				{
					MessageBox.Show("You do not have a high enough Operator Level to change this information.", "Invalid Operator Level", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (chkCreateJournal.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (!modValidateAccount.AccountValidate(vsRevAcct.TextMatrix(0, 0)))
				{
					MessageBox.Show("You must enter a valid revenue account before you may proceed.", "Invalid Rev Acct", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cmbPrinting.Text = "General";
					vsRevAcct.Focus();
					return;
				}
				if (!modValidateAccount.AccountValidate(vsExpAcct.TextMatrix(0, 0)))
				{
					MessageBox.Show("You must enter a valid merchant fee account before you may proceed.", "Invalid Exp Acct", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cmbPrinting.Text = "General";
					vsExpAcct.Focus();
					return;
				}
			}
			if (!Information.IsNumeric(txtPrinterGroup.Text))
			{
				MessageBox.Show("You must enter a number between 1 and 99.", "Invalid Printer Group", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				StaticSettings.GlobalSettingService.SaveSetting(SettingOwnerType.Machine, "MVR3PrinterGroup", txtPrinterGroup.Text);
			}

            if (chkUTCDuplex.CheckState == Wisej.Web.CheckState.Checked)
			{
                modRegistry.SaveRegistryKey("USETAX_DUPLEX", "Y", "MOTOR_VEHICLE");
            }
			else
			{
                modRegistry.SaveRegistryKey("USETAX_DUPLEX", "N", "MOTOR_VEHICLE");
            }
			if (chkUTCPrintForm.CheckState == Wisej.Web.CheckState.Checked)
			{
                modRegistry.SaveRegistryKey("USETAX_PRINTFORM", "Y", "MOTOR_VEHICLE");
            }
			else
			{
                modRegistry.SaveRegistryKey("USETAX_PRINTFORM", "N", "MOTOR_VEHICLE");
            }
			MotorVehicle.SetComputerMVSetting("UTCTopAdjustment", FCConvert.ToString(Conversion.Val(txtUTCTopAdj.Text)));
			MotorVehicle.SetComputerMVSetting("UTCLeftAdjustment", FCConvert.ToString(Conversion.Val(txtUTCLeftAdj.Text)));
						
			rsGeneral.OpenRecordset("SELECT * FROM WMV");
			rsGeneral.Edit();
			if (cmbCheckDigits.Text == "2")
			{
				rsGeneral.Set_Fields("MVR3Digits", 2);
			}
			else if (cmbCheckDigits.Text == "3")
			{
				rsGeneral.Set_Fields("MVR3Digits", 3);
			}
			else if (cmbCheckDigits.Text == "4")
			{
				rsGeneral.Set_Fields("MVR3Digits", 4);
			}
			else if (cmbCheckDigits.Text == "1")
			{
				rsGeneral.Set_Fields("MVR3Digits", 1);
			}
			else if (cmbCheckDigits.Text == "0")
			{
				rsGeneral.Set_Fields("MVR3Digits", 0);
			}

			if (chkCreateJournal.CheckState == Wisej.Web.CheckState.Checked)
			{
				rsGeneral.Set_Fields("RRCreateCRJournal", true);
				rsGeneral.Set_Fields("RRRevAcct", vsRevAcct.TextMatrix(0, 0));
				rsGeneral.Set_Fields("RRExpAcct", vsExpAcct.TextMatrix(0, 0));
			}
			else
			{
				rsGeneral.Set_Fields("RRCreateCRJournal", false);
				rsGeneral.Set_Fields("RRRevAcct", "");
				rsGeneral.Set_Fields("RRExpAcct", "");
			}
			rsGeneral.Set_Fields("LongTermFTPAddress", fecherFoundation.Strings.Trim(txtFTPAddress.Text));
			rsGeneral.Set_Fields("LongTermFTPUser", fecherFoundation.Strings.Trim(txtFTPUser.Text));
			rsGeneral.Set_Fields("LongTermFTPPassword", fecherFoundation.Strings.Trim(txtFTPPassword.Text));
			rsGeneral.Update();
			
			rsGeneral.Edit();
			
			if (cmbPrintUseTaxNo.Text == "Yes")
			{
				rsGeneral.Set_Fields("PrintUseTax", true);
				MotorVehicle.Statics.blnTownPrintsUseTax = true;
			}
			else
			{
				rsGeneral.Set_Fields("PrintUseTax", false);
				MotorVehicle.Statics.blnTownPrintsUseTax = false;
			}
			if (cmbLongTermPrintCTAUseTaxYes.Text == "Yes")
			{
				rsGeneral.Set_Fields("LongTermPrintUseTaxCTA", true);
				MotorVehicle.Statics.blnTownPrintsLongTermUseTaxCTA = true;
			}
			else
			{
				rsGeneral.Set_Fields("LongTermPrintUseTaxCTA", false);
				MotorVehicle.Statics.blnTownPrintsLongTermUseTaxCTA = false;
			}
			if (cmbLongTermLaserFormsYes.Text == "Yes")
			{
				MotorVehicle.Statics.blnLongTermLaserForms = true;
			}
			else
			{
				MotorVehicle.Statics.blnLongTermLaserForms = false;
			}
			
			if (chkAutoPopulateUnit.CheckState == Wisej.Web.CheckState.Checked)
			{
				rsGeneral.Set_Fields("LongTermAutoPopulateUnit", true);
				MotorVehicle.Statics.blnLongTermAutoPopulateUnit = true;
			}
			else
			{
				rsGeneral.Set_Fields("LongTermAutoPopulateUnit", false);
				MotorVehicle.Statics.blnLongTermAutoPopulateUnit = false;
			}
			rsGeneral.Set_Fields("UseTaxFormVersion", 3);
			MotorVehicle.Statics.intUseTaxFormVersion = 3;
			if (cmbLeaseUseTaxV1.Text == "Newer (Bigger margins)")
			{
				rsGeneral.Set_Fields("LeaseUseTaxFormVersion", 1);
			}
			else
			{
				rsGeneral.Set_Fields("LeaseUseTaxFormVersion", 0);
			}
			if (cmbExciseRebateNo.Text == "Yes")
			{
				rsGeneral.Set_Fields("AllowExciseRebates", true);
				MotorVehicle.Statics.blnAllowExciseRebates = true;
			}
			else
			{
				rsGeneral.Set_Fields("AllowExciseRebates", false);
				MotorVehicle.Statics.blnAllowExciseRebates = false;
			}
			rsGeneral.Update();
			if (cmbReminderNo.Text == "Yes")
			{
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "ShowReminders", FCConvert.ToString(true));
				MotorVehicle.Statics.ShowReminders = true;
			}
			else
			{
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "ShowReminders", FCConvert.ToString(false));
				MotorVehicle.Statics.ShowReminders = false;
			}
			
			if (cmbTellerYes.Text == "Yes")
			{
				rsGeneral.Edit();
				rsGeneral.Set_Fields("UseTellerCloseout", "Y");
				rsGeneral.Update();
				MotorVehicle.Statics.blnUseTellerCloseout = true;
			}
			else
			{
				rsGeneral.Edit();
				rsGeneral.Set_Fields("UseTellerCloseout", "N");
				rsGeneral.Update();
				MotorVehicle.Statics.blnUseTellerCloseout = false;
			}
			rsGeneral.Edit();
			if (cmbYes.Text == "Yes")
			{
				rsGeneral.Set_Fields("MVR3Warning", "Y");
				MotorVehicle.Statics.MVR3Warning = true;
			}
			else
			{
				rsGeneral.Set_Fields("MVR3Warning", "N");
				MotorVehicle.Statics.MVR3Warning = false;
			}
			rsGeneral.Set_Fields("LaserLabelAdjustment", FCConvert.ToString(Conversion.Val(txtLabelAdjustment.Text)));
			rsGeneral.Update();
			MotorVehicle.SetComputerMVSetting("LaserMVRT10Adjustment", FCConvert.ToString(Conversion.Val(txtLongTermLaserAdjustment.Text)));
			MotorVehicle.SetComputerMVSetting("MVRT10UseLaser", FCConvert.ToString(cmbLongTermLaserFormsYes.Text == "Yes"));
			MotorVehicle.SetComputerMVSetting("MVR3ETopAdjustment", FCConvert.ToString(Conversion.Val(txtMVR3E_TopAdj.Text)));
			MotorVehicle.SetComputerMVSetting("MVR3ELeftAdjustment", FCConvert.ToString(Conversion.Val(txtMVR3E_LeftAdj.Text)));

			MessageBox.Show("Save Successful", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			blnDirty = false;
            //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
            App.MainForm.ReloadNavigationMenu();
            MotorVehicle.CheckMVR3Number();
			if (blnUnload)
			{
				Close();
			}
		}

		private void optCheckDigits_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void optCheckDigits_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbCheckDigits.SelectedIndex;
			optCheckDigits_CheckedChanged(index, sender, e);
		}

		private void optCTAUseTax_CheckedChanged(object sender, System.EventArgs e)
		{
			fraPrintingOptions.Visible = false;
			fraCTAUseTaxOptions.Visible = true;
			fraLongTermOptions.Visible = false;
			fraGeneralOptions.Visible = false;
		}

		private void optGeneral_CheckedChanged(object sender, System.EventArgs e)
		{
			fraPrintingOptions.Visible = false;
			fraCTAUseTaxOptions.Visible = false;
			fraLongTermOptions.Visible = false;
			fraGeneralOptions.Visible = true;
		}

		private void optLongTerm_CheckedChanged(object sender, System.EventArgs e)
		{
			fraPrintingOptions.Visible = false;
			fraCTAUseTaxOptions.Visible = false;
			fraLongTermOptions.Visible = true;
			fraGeneralOptions.Visible = false;
		}

		private void optNo_CheckedChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void optPrinting_CheckedChanged(object sender, System.EventArgs e)
		{
			fraPrintingOptions.Visible = true;
			fraCTAUseTaxOptions.Visible = false;
			fraLongTermOptions.Visible = false;
			fraGeneralOptions.Visible = false;
		}

		private void optPrintUseTaxNo_CheckedChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void optPrintUseTaxYes_CheckedChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void optReminderNo_CheckedChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void optReminderYes_CheckedChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void optTellerNo_CheckedChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void optTellerYes_CheckedChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void optYes_CheckedChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtLabelAdjustment_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtLongTermLaserAdjustment_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtMVR3E_TopAdj_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtMVR3E_LeftAdj_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtMVR3E_TopAdj_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMVR3E_TopAdj.Text) < -0.5 || Conversion.Val(txtMVR3E_TopAdj.Text) > 0.5)
			{
				MessageBox.Show("Please enter a top margin adjustment between -0.5 and 0.5 inches.", "Invalid Adjustment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtMVR3E_TopAdj.Focus();
				e.Cancel = true;
			}
		}

		private void txtMVR3E_LeftAdj_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMVR3E_LeftAdj.Text) < -0.5 || Conversion.Val(txtMVR3E_LeftAdj.Text) > 0.5)
			{
				MessageBox.Show("Please enter a left margin adjustment between -0.5 and 0.5 inches.", "Invalid Adjustment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtMVR3E_LeftAdj.Focus();
				e.Cancel = true;
			}
		}

		private void txtUTCTopAdj_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtUTCLeftAdj_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtUTCTopAdj_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtUTCTopAdj.Text) < -0.5 || Conversion.Val(txtUTCTopAdj.Text) > 0.5)
			{
				MessageBox.Show("Please enter a top margin adjustment between -0.5 and 0.5 inches.", "Invalid Adjustment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtUTCTopAdj.Focus();
				e.Cancel = true;
			}
		}

		private void txtUTCLeftAdj_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtUTCLeftAdj.Text) < -0.3 || Conversion.Val(txtUTCLeftAdj.Text) > 0.3)
			{
				MessageBox.Show("Please enter a left margin adjustment between -0.3 and 0.3 inches.", "Invalid Adjustment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtUTCLeftAdj.Focus();
				e.Cancel = true;
			}
		}

		private void txtPrinterGroup_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtPrinterGroup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLabelAdjustment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLongTermLaserAdjustment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMVR3E_TopAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMVR3E_LeftAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtUTCTopAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtUTCLeftAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReportPath_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void vsRevAcct_ChangeEdit(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void vsExpAcct_ChangeEdit(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		public void cmbPrintUseTaxNo_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbPrintUseTaxNo.Text == "Yes")
			{
				optPrintUseTaxYes_CheckedChanged(sender, e);
			}
			else if (cmbPrintUseTaxNo.Text == "No")
			{
				optPrintUseTaxNo_CheckedChanged(sender, e);
			}
		}

		public void cmbReminderNo_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbReminderNo.Text == "Yes")
			{
				optReminderYes_CheckedChanged(sender, e);
			}
			else if (cmbReminderNo.Text == "No")
			{
				optReminderNo_CheckedChanged(sender, e);
			}
		}

		public void cmbYes_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbYes.Text == "Yes")
			{
				optYes_CheckedChanged(sender, e);
			}
			else if (cmbYes.Text == "No")
			{
				optNo_CheckedChanged(sender, e);
			}
		}

		public void cmbTellerYes_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbTellerYes.Text == "Yes")
			{
				optTellerYes_CheckedChanged(sender, e);
			}
			else if (cmbTellerYes.Text == "No")
			{
				optTellerNo_CheckedChanged(sender, e);
			}
		}

		private void cmbPrinting_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbPrinting.Text == "General")
			{
				optGeneral_CheckedChanged(sender, e);
			}
			else if (cmbPrinting.Text == "Printing")
			{
				optPrinting_CheckedChanged(sender, e);
			}
			else if (cmbPrinting.Text == "CTA & Use Tax")
			{
				optCTAUseTax_CheckedChanged(sender, e);
			}
			else if (cmbPrinting.Text == "Long Term")
			{
				optLongTerm_CheckedChanged(sender, e);
			}
		}
	}
}
