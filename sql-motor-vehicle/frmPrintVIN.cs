//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmPrintVIN : BaseForm
	{
		public frmPrintVIN()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintVIN InstancePtr
		{
			get
			{
				return (frmPrintVIN)Sys.GetInstance(typeof(frmPrintVIN));
			}
		}

		protected frmPrintVIN _InstancePtr = null;
		//=========================================================
		clsDRWrapper rsFleet = new clsDRWrapper();
		clsDRWrapper rsMakes = new clsDRWrapper();
		clsDRWrapper rsVehicles = new clsDRWrapper();
		int OwnerCol;
		int ClassCol;
		int PlateCol;
		int VINCol;
		int PrintCol;
		bool SecondResize;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			if (cmdPrint.Enabled)
			{
				MotorVehicle.Statics.rsReport = rsVehicles;
				modDuplexPrinting.DuplexPrintReport(rptVINList.InstancePtr);
				rptVINList.InstancePtr.Unload();
			}
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			int tempfleet = 0;
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			if (cmbFleet.Text == "CTA Number")
			{
				if (fecherFoundation.Strings.Trim(txtCTANumber.Text) == "")
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("You must input a CTA Number before you may continue.", "Invalid CTA Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				rsVehicles.OpenRecordset("SELECT * FROM Master WHERE CTANumber = '" + fecherFoundation.Strings.Trim(txtCTANumber.Text) + "' ORDER BY VIN");
				if (rsVehicles.EndOfFile() != true && rsVehicles.BeginningOfFile() != true)
				{
					rsVehicles.MoveLast();
					rsVehicles.MoveFirst();
					FillGrid();
					frmWait.InstancePtr.Unload();
					vsVehicles.Visible = true;
					cmdPrint.Enabled = true;
					cmdSelect.Enabled = true;
					vsVehicles.Focus();
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("There were no records found with this CTA Number.", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				if (cboFleets.SelectedIndex == -1)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("You must select a fleet before you can perform a search.", "Missing Search Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					cboFleets.Focus();
					return;
				}
				else if (cboMakes.SelectedIndex == -1)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("You must select a make before you can perform a search.", "Missing Search Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					cboMakes.Focus();
					return;
				}
				else if (fecherFoundation.Strings.Trim(txtModel.Text) == "")
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("You must enter a model before you can perform a search.", "Missing Search Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtModel.Focus();
					return;
				}
				else if (!Information.IsNumeric(txtYear.Text) || fecherFoundation.Strings.Trim(txtYear.Text) == "")
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("You must enter a year before you can perform a search.", "Missing Search Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					cboFleets.Focus();
					return;
				}
				else
				{
					tempfleet = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboFleets.Text, 5))));
					rsVehicles.OpenRecordset("SELECT * FROM Master WHERE FleetNumber = " + FCConvert.ToString(tempfleet) + "AND Make = '" + cboMakes.Text + "' AND Model = '" + fecherFoundation.Strings.Trim(txtModel.Text) + "' AND Year = " + FCConvert.ToString(Conversion.Val(txtYear.Text)) + " ORDER BY VIN");
					if (rsVehicles.EndOfFile() != true && rsVehicles.BeginningOfFile() != true)
					{
						rsVehicles.MoveLast();
						rsVehicles.MoveFirst();
						FillGrid();
						frmWait.InstancePtr.Unload();
						vsVehicles.Visible = true;
						cmdPrint.Enabled = true;
						cmdSelect.Enabled = true;
						vsVehicles.Focus();
					}
					else
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("There were no records found that meet the search criteria.", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
		}

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			for (counter = 1; counter <= (vsVehicles.Rows - 1); counter++)
			{
				vsVehicles.TextMatrix(counter, 0, FCConvert.ToString(true));
			}
		}

		private void frmPrintVIN_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!modSecurity.ValidPermissions(this, MotorVehicle.VINLIST))
			{
				MessageBox.Show("Your permission setting for this function is set to none or is missing.", "No Permissions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}
			PrintCol = 0;
			OwnerCol = 1;
			ClassCol = 2;
			PlateCol = 3;
			VINCol = 4;
			vsVehicles.TextMatrix(0, OwnerCol, "Owner");
			vsVehicles.TextMatrix(0, ClassCol, "Class");
			vsVehicles.TextMatrix(0, PlateCol, "Plate");
			vsVehicles.TextMatrix(0, VINCol, "VIN");
			vsVehicles.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 4, 4);
			vsVehicles.ColDataType(PrintCol, FCGrid.DataTypeSettings.flexDTBoolean);

            vsVehicles.ColWidth(0, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.10)));
            vsVehicles.ColWidth(OwnerCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.30)));
            vsVehicles.ColWidth(ClassCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.10)));
            vsVehicles.ColWidth(PlateCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.15)));
            vsVehicles.ColWidth(VINCol, FCConvert.ToInt32((vsVehicles.WidthOriginal * 0.35)));

            if (cboFleets.SelectedIndex == -1)
			{
				FillFleetCombo();
			}
			if (cboMakes.SelectedIndex == -1)
			{
				rsMakes.OpenRecordset("SELECT DISTINCT Code FROM Make ORDER BY Code");
				if (rsMakes.EndOfFile() != true && rsMakes.BeginningOfFile() != true)
				{
					rsMakes.MoveLast();
					rsMakes.MoveFirst();
					do
					{
						cboMakes.AddItem(FCConvert.ToString(rsMakes.Get_Fields("Code")));
						rsMakes.MoveNext();
					}
					while (rsMakes.EndOfFile() != true);
				}
			}
			this.Refresh();
			txtCTANumber.Focus();
		}

		private void FillFleetCombo()
		{
			cboFleets.Clear();
			if (cmbAnnual.Text == "Long Term")
			{
				rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type = 'L' AND ExpiryMonth <> 99 ORDER BY p.FullName");
			}
			else
			{
				rsFleet.OpenRecordset("SELECT c.*, p.FullName FROM FleetMaster as c LEFT JOIN " + rsFleet.CurrentPrefix + "CentralParties.dbo.PartyAndAddressView as p ON c.PartyID1 = p.ID WHERE Deleted <> 1 AND Type <> 'L' AND ExpiryMonth <> 99 ORDER BY p.FullName");
			}
			if (rsFleet.EndOfFile() != true && rsFleet.BeginningOfFile() != true)
			{
				rsFleet.MoveLast();
				rsFleet.MoveFirst();
				do
				{
					cboFleets.AddItem(rsFleet.Get_Fields_Int32("FleetNumber") + Strings.Space(5 - FCConvert.ToString(rsFleet.Get_Fields_Int32("FleetNumber")).Length) + fecherFoundation.Strings.UCase(FCConvert.ToString(rsFleet.Get_Fields_String("FullName"))));
					rsFleet.MoveNext();
				}
				while (rsFleet.EndOfFile() != true);
			}
		}

		private void frmPrintVIN_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintVIN properties;
			//frmPrintVIN.FillStyle	= 0;
			//frmPrintVIN.ScaleWidth	= 9045;
			//frmPrintVIN.ScaleHeight	= 7230;
			//frmPrintVIN.LinkTopic	= "Form2";
			//frmPrintVIN.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGNBas.GetWindowSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			if (MotorVehicle.Statics.gboolAllowLongTermTrailers)
			{
				lblAnnual.Visible = true;
				cmbAnnual.Visible = true;
			}
			else
			{
				lblAnnual.Visible = false;
				cmbAnnual.Visible = false;
				//FC:FINAL:DDU - Managed by design part
				//fraCriteria.WidthOriginal = 6045;
				//fraCriteria.LeftOriginal = 1500;
			}
		}

		private void frmPrintVIN_Resize(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: temp As int	OnWriteFCConvert.ToInt32(
			int temp;
			int counter2;
			//vsVehicles.AutoSize(0, 4);
			temp = 0;
			//FC:FINAL:MSH - i.issue #1891: don't need to resize and move grid (Managed by design part)
			//for (counter2 = 0; counter2 <= 4; counter2++)
			//{
			//	temp += vsVehicles.ColWidth(counter2);
			//}
			//if (vsVehicles.Rows <= 13)
			//{
			//	//FC:FINAL:MSH - i.issue #1883: use property with sizes from original app
			//	//vsVehicles.Height = (vsVehicles.Rows * vsVehicles.RowHeight(0)) + 75;
			//	vsVehicles.HeightOriginal = (vsVehicles.Rows * vsVehicles.RowHeight(0)) + 205;
			//}
			//else
			//{
			//	//FC:FINAL:MSH - i.issue #1883: use property with sizes from original app
			//	//vsVehicles.Height = (13 * vsVehicles.RowHeight(0)) + 75;
			//	vsVehicles.HeightOriginal = (13 * vsVehicles.RowHeight(0)) + 205;
			//}
			////FC:FINAL:MSH - i.issue #1883: use property with sizes from original app
			////vsVehicles.Width = temp + 300;
			////vsVehicles.Left = FCConvert.ToInt32((this.WidthOriginal - vsVehicles.WidthOriginal) / 2.0);
			//vsVehicles.WidthOriginal = temp + 300;
			//vsVehicles.LeftOriginal = FCConvert.ToInt32((this.WidthOriginal - vsVehicles.WidthOriginal) / 2.0);
			if (SecondResize == false)
			{
				SecondResize = true;
				Form_Resize();
			}
			else
			{
				SecondResize = false;
			}
		}

		public void Form_Resize()
		{
			frmPrintVIN_Resize(this, new System.EventArgs());
		}

		private void frmPrintVIN_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmdPrint.Enabled)
			{
				MotorVehicle.Statics.rsReport = rsVehicles;
				frmReportViewer.InstancePtr.Init(rptVINList.InstancePtr);
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmdPrint.Enabled)
			{
				MotorVehicle.Statics.rsReport = rsVehicles;
				modDuplexPrinting.DuplexPrintReport(rptVINList.InstancePtr);
				rptVINList.InstancePtr.Unload();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optAnnual_CheckedChanged(object sender, System.EventArgs e)
		{
			FillFleetCombo();
		}

		private void optCTA_CheckedChanged(object sender, System.EventArgs e)
		{
			fraCriteria.Visible = false;
			cboFleets.SelectedIndex = -1;
			cboMakes.SelectedIndex = -1;
			txtModel.Text = "";
			txtYear.Text = "";
			fraCTA.Visible = true;
			txtCTANumber.Focus();
		}

		private void optFleet_CheckedChanged(object sender, System.EventArgs e)
		{
			fraCTA.Visible = false;
			txtCTANumber.Text = "";
			fraCriteria.Visible = true;
			cboFleets.Focus();
		}

		private void FillGrid()
		{
			int counter;
			int temp;
			counter = 1;
			vsVehicles.Rows = 1;
			vsVehicles.Rows = rsVehicles.RecordCount() + 1;
			do
			{
				vsVehicles.TextMatrix(counter, 0, FCConvert.ToString(false));
				vsVehicles.TextMatrix(counter, OwnerCol, fecherFoundation.Strings.Trim(MotorVehicle.GetPartyNameMiddleInitial(rsVehicles.Get_Fields_Int32("PartyID1"))));
				if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("Class")))
				{
					vsVehicles.TextMatrix(counter, ClassCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("Class"))));
				}
				if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("plate")))
				{
					vsVehicles.TextMatrix(counter, PlateCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("plate"))));
				}
				if (!fecherFoundation.FCUtils.IsNull(rsVehicles.Get_Fields_String("Vin")))
				{
					vsVehicles.TextMatrix(counter, VINCol, fecherFoundation.Strings.Trim(FCConvert.ToString(rsVehicles.Get_Fields_String("Vin"))));
				}
				counter += 1;
				rsVehicles.MoveNext();
			}
			while (rsVehicles.EndOfFile() != true);
			Form_Resize();
		}

		private void optLongTerm_CheckedChanged(object sender, System.EventArgs e)
		{
			FillFleetCombo();
		}

		private void txtModel_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vsVehicles_ClickEvent(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1883: wrong inout value for FCConvert.ToBoolean
			//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, 0)) != true)
			if (FCUtils.CBool(vsVehicles.TextMatrix(vsVehicles.Row, 0)) != true)
			{
				vsVehicles.TextMatrix(vsVehicles.Row, 0, FCConvert.ToString(true));
			}
			else
			{
				vsVehicles.TextMatrix(vsVehicles.Row, 0, FCConvert.ToString(false));
			}
		}

		private void vsVehicles_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				//FC:FINAL:MSH - i.issue #1883: wrong inout value for FCConvert.ToBoolean
				//if (FCConvert.ToBoolean(vsVehicles.TextMatrix(vsVehicles.Row, 0)) != true)
				if (FCUtils.CBool(vsVehicles.TextMatrix(vsVehicles.Row, 0)) != true)
				{
					vsVehicles.TextMatrix(vsVehicles.Row, 0, FCConvert.ToString(true));
				}
				else
				{
					vsVehicles.TextMatrix(vsVehicles.Row, 0, FCConvert.ToString(false));
				}
			}
		}

		public void cmbAnnual_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAnnual.Text == "Annual")
			{
				optAnnual_CheckedChanged(sender, e);
			}
			else if (cmbAnnual.Text == "Long Term")
			{
				optLongTerm_CheckedChanged(sender, e);
			}
		}

		public void cmbFleet_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbFleet.Text == "Fleet/Make/Model/Year")
			{
				optFleet_CheckedChanged(sender, e);
			}
			else if (cmbFleet.Text == "CTA Number")
			{
				optCTA_CheckedChanged(sender, e);
			}
		}
	}
}
