//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	public partial class frmFleetSelect : BaseForm
	{
		public frmFleetSelect()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmFleetSelect InstancePtr
		{
			get
			{
				return (frmFleetSelect)Sys.GetInstance(typeof(frmFleetSelect));
			}
		}

		protected frmFleetSelect _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         1/15/03
		// This form will be used when trying to add a vehicle to a
		// fleet when the expiration date of the vehicle is beyond
		// 5 months in the future
		// ********************************************************
		private void frmFleetSelect_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modGlobalRoutines.FormExist(this)))
			{
				return;
			}
			if (!MotorVehicle.Statics.blnChangeToFleet)
			{
				Close();
				return;
			}
			cboFleets.SelectedIndex = 0;
			cboFleets.Focus();
			this.Refresh();
		}

		private void frmFleetSelect_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFleetSelect properties;
			//frmFleetSelect.FillStyle	= 0;
			//frmFleetSelect.ScaleWidth	= 2895;
			//frmFleetSelect.ScaleHeight	= 1050;
			//frmFleetSelect.LinkTopic	= "Form2";
			//frmFleetSelect.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsFleetInfo = new clsDRWrapper();
			rsFleetInfo.OpenRecordset("SELECT * FROM FleetMaster WHERE Deleted <> 1 AND Type <> 'L' AND ExpiryMonth <> 99 ORDER BY Owner1Name");
			if (rsFleetInfo.EndOfFile() != true && rsFleetInfo.BeginningOfFile() != true)
			{
				MotorVehicle.Statics.blnChangeToFleet = true;
				cboFleets.Clear();
				do
				{
					cboFleets.AddItem(Strings.Format(rsFleetInfo.Get_Fields_Int32("FleetNumber"), "00000") + "   " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsFleetInfo.Get_Fields("Owner1Name"))));
					rsFleetInfo.MoveNext();
				}
				while (rsFleetInfo.EndOfFile() != true);
			}
			else
			{
				MotorVehicle.Statics.blnChangeToFleet = false;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
			modGlobalFunctions.SetTRIOColors(this, false);
		}

		private void frmFleetSelect_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				MotorVehicle.Statics.blnChangeToFleet = false;
				MotorVehicle.Statics.lngFleetNumberBeingChangedTo = 0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.Statics.blnChangeToFleet = false;
			MotorVehicle.Statics.lngFleetNumberBeingChangedTo = 0;
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			MotorVehicle.Statics.lngFleetNumberBeingChangedTo = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboFleets.Text, 2))));
			MotorVehicle.Statics.blnFleetRunOnce = false;
			Close();
		}

		private void cboFleets_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboFleets.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}
	}
}
