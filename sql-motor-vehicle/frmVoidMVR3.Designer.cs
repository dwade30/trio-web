//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmVoidMVR3.
	/// </summary>
	partial class frmVoidMVR3
	{
		public fecherFoundation.FCComboBox cmbMVR3;
		public fecherFoundation.FCFrame fraMVRT10;
		public fecherFoundation.FCButton cmdSelect;
		public FCGrid vsMVRT10;
		public fecherFoundation.FCTextBox txtReason;
		public fecherFoundation.FCCheckBox chkPlate;
		public fecherFoundation.FCCheckBox chkYear;
		public fecherFoundation.FCCheckBox chkMonth;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCButton cmdYes;
		public fecherFoundation.FCButton cmdNO;
        public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCLabel lblStickers;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCLabel lblVehicle;
		public fecherFoundation.FCLabel lblAddress;
		public fecherFoundation.FCLabel lblPlateClass;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCTextBox txtVoidedMVR3;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCFrame fraUnissued;
		public fecherFoundation.FCLabel lblUnissued2;
		public fecherFoundation.FCLabel lblUnissued;
		public fecherFoundation.FCLabel lblReason;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdFileProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbMVR3 = new fecherFoundation.FCComboBox();
            this.fraMVRT10 = new fecherFoundation.FCFrame();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.vsMVRT10 = new fecherFoundation.FCGrid();
            this.cmdQuit = new fecherFoundation.FCButton();
            this.txtReason = new fecherFoundation.FCTextBox();
            this.chkPlate = new fecherFoundation.FCCheckBox();
            this.chkYear = new fecherFoundation.FCCheckBox();
            this.chkMonth = new fecherFoundation.FCCheckBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cmdYes = new fecherFoundation.FCButton();
            this.cmdNO = new fecherFoundation.FCButton();
            this.lblStickers = new fecherFoundation.FCLabel();
            this.lblDate = new fecherFoundation.FCLabel();
            this.lblVehicle = new fecherFoundation.FCLabel();
            this.lblAddress = new fecherFoundation.FCLabel();
            this.lblPlateClass = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.txtVoidedMVR3 = new fecherFoundation.FCTextBox();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.fraUnissued = new fecherFoundation.FCFrame();
            this.lblUnissued2 = new fecherFoundation.FCLabel();
            this.lblUnissued = new fecherFoundation.FCLabel();
            this.lblReason = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdFileProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMVRT10)).BeginInit();
            this.fraMVRT10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMVRT10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraUnissued)).BeginInit();
            this.fraUnissued.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Controls.Add(this.cmdQuit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(814, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdFileProcess);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fraUnissued);
            this.ClientArea.Controls.Add(this.cmbMVR3);
            this.ClientArea.Controls.Add(this.txtReason);
            this.ClientArea.Controls.Add(this.chkYear);
            this.ClientArea.Controls.Add(this.chkMonth);
            this.ClientArea.Controls.Add(this.txtVoidedMVR3);
            this.ClientArea.Controls.Add(this.lblReason);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.chkPlate);
            this.ClientArea.Controls.Add(this.fraMVRT10);
            this.ClientArea.Size = new System.Drawing.Size(834, 708);
            this.ClientArea.Controls.SetChildIndex(this.fraMVRT10, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkPlate, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblReason, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtVoidedMVR3, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkMonth, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtReason, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbMVR3, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraUnissued, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdFileProcess, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(834, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(125, 28);
            this.HeaderText.Text = "Void MVR3";
            // 
            // cmbMVR3
            // 
            this.cmbMVR3.Items.AddRange(new object[] {
            "MVR-3",
            "MVRT-10"});
            this.cmbMVR3.Location = new System.Drawing.Point(30, 30);
            this.cmbMVR3.Name = "cmbMVR3";
            this.cmbMVR3.Size = new System.Drawing.Size(136, 40);
            this.cmbMVR3.TabIndex = 0;
            this.cmbMVR3.Text = "MVR-3";
            this.cmbMVR3.SelectedIndexChanged += new System.EventHandler(this.cmbMVR3_SelectedIndexChanged);
            // 
            // fraMVRT10
            // 
            this.fraMVRT10.AppearanceKey = "groupBoxNoBorders";
            this.fraMVRT10.BackColor = System.Drawing.Color.White;
            this.fraMVRT10.Controls.Add(this.cmdSelect);
            this.fraMVRT10.Controls.Add(this.vsMVRT10);
            this.fraMVRT10.FormatCaption = false;
            this.fraMVRT10.Location = new System.Drawing.Point(30, 90);
            this.fraMVRT10.Name = "fraMVRT10";
            this.fraMVRT10.Size = new System.Drawing.Size(619, 410);
            this.fraMVRT10.TabIndex = 20;
            this.fraMVRT10.Text = "Select MVRT-10 Form To Void";
            this.fraMVRT10.Visible = false;
            // 
            // cmdSelect
            // 
            this.cmdSelect.AppearanceKey = "actionButton";
            this.cmdSelect.Location = new System.Drawing.Point(0, 349);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Size = new System.Drawing.Size(87, 40);
            this.cmdSelect.TabIndex = 23;
            this.cmdSelect.Text = "Select";
            this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
            // 
            // vsMVRT10
            // 
            this.vsMVRT10.Cols = 6;
            this.vsMVRT10.ExtendLastCol = true;
            this.vsMVRT10.Location = new System.Drawing.Point(0, 30);
            this.vsMVRT10.Name = "vsMVRT10";
            this.vsMVRT10.Rows = 1;
            this.vsMVRT10.ShowFocusCell = false;
            this.vsMVRT10.Size = new System.Drawing.Size(575, 307);
            this.vsMVRT10.TabIndex = 21;
            this.vsMVRT10.DoubleClick += new System.EventHandler(this.vsMVRT10_DblClick);
            // 
            // cmdQuit
            // 
            this.cmdQuit.Location = new System.Drawing.Point(320, 30);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(147, 48);
            this.cmdQuit.TabIndex = 0;
            this.cmdQuit.Text = "Quit";
            this.cmdQuit.Visible = false;
            this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
            // 
            // txtReason
            // 
            this.txtReason.BackColor = System.Drawing.SystemColors.Window;
            this.txtReason.Location = new System.Drawing.Point(145, 540);
            this.txtReason.MaxLength = 29;
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(330, 40);
            this.txtReason.TabIndex = 14;
            // 
            // chkPlate
            // 
            this.chkPlate.Location = new System.Drawing.Point(30, 493);
            this.chkPlate.Name = "chkPlate";
            this.chkPlate.Size = new System.Drawing.Size(231, 22);
            this.chkPlate.TabIndex = 6;
            this.chkPlate.Text = "Check this box to return the Plates";
            this.chkPlate.Visible = false;
            // 
            // chkYear
            // 
            this.chkYear.Location = new System.Drawing.Point(30, 460);
            this.chkYear.Name = "chkYear";
            this.chkYear.Size = new System.Drawing.Size(272, 22);
            this.chkYear.TabIndex = 5;
            this.chkYear.Text = "Check this box to return the Year Stickers";
            this.chkYear.Visible = false;
            // 
            // chkMonth
            // 
            this.chkMonth.Location = new System.Drawing.Point(30, 427);
            this.chkMonth.Name = "chkMonth";
            this.chkMonth.Size = new System.Drawing.Size(281, 22);
            this.chkMonth.TabIndex = 4;
            this.chkMonth.Text = "Check this box to return the Month Stickers";
            this.chkMonth.Visible = false;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cmdYes);
            this.Frame1.Controls.Add(this.cmdNO);
            this.Frame1.Controls.Add(this.lblStickers);
            this.Frame1.Controls.Add(this.lblDate);
            this.Frame1.Controls.Add(this.lblVehicle);
            this.Frame1.Controls.Add(this.lblAddress);
            this.Frame1.Controls.Add(this.lblPlateClass);
            this.Frame1.Controls.Add(this.lblName);
            this.Frame1.Location = new System.Drawing.Point(30, 150);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(405, 264);
            this.Frame1.TabIndex = 8;
            this.Frame1.Text = "Is This The Correct Information?";
            this.Frame1.Visible = false;
            // 
            // cmdYes
            // 
            this.cmdYes.AppearanceKey = "actionButton";
            this.cmdYes.Location = new System.Drawing.Point(106, 204);
            this.cmdYes.Name = "cmdYes";
            this.cmdYes.Size = new System.Drawing.Size(70, 40);
            this.cmdYes.TabIndex = 2;
            this.cmdYes.Text = "YES";
            this.cmdYes.Click += new System.EventHandler(this.cmdYes_Click);
            // 
            // cmdNO
            // 
            this.cmdNO.AppearanceKey = "actionButton";
            this.cmdNO.Location = new System.Drawing.Point(20, 204);
            this.cmdNO.Name = "cmdNO";
            this.cmdNO.Size = new System.Drawing.Size(66, 40);
            this.cmdNO.TabIndex = 3;
            this.cmdNO.Text = "NO";
            this.cmdNO.Click += new System.EventHandler(this.cmdNO_Click);
            // 
            // lblStickers
            // 
            this.lblStickers.Location = new System.Drawing.Point(20, 165);
            this.lblStickers.Name = "lblStickers";
            this.lblStickers.Size = new System.Drawing.Size(364, 17);
            this.lblStickers.TabIndex = 15;
            // 
            // lblDate
            // 
            this.lblDate.Location = new System.Drawing.Point(20, 138);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(364, 17);
            this.lblDate.TabIndex = 13;
            // 
            // lblVehicle
            // 
            this.lblVehicle.Location = new System.Drawing.Point(20, 84);
            this.lblVehicle.Name = "lblVehicle";
            this.lblVehicle.Size = new System.Drawing.Size(364, 17);
            this.lblVehicle.TabIndex = 12;
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(20, 57);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(364, 17);
            this.lblAddress.TabIndex = 11;
            // 
            // lblPlateClass
            // 
            this.lblPlateClass.Location = new System.Drawing.Point(20, 111);
            this.lblPlateClass.Name = "lblPlateClass";
            this.lblPlateClass.Size = new System.Drawing.Size(364, 17);
            this.lblPlateClass.TabIndex = 10;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(20, 30);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(364, 17);
            this.lblName.TabIndex = 9;
            // 
            // txtVoidedMVR3
            // 
            this.txtVoidedMVR3.BackColor = System.Drawing.SystemColors.Window;
            this.txtVoidedMVR3.Location = new System.Drawing.Point(300, 90);
            this.txtVoidedMVR3.MaxLength = 10;
            this.txtVoidedMVR3.Name = "txtVoidedMVR3";
            this.txtVoidedMVR3.Size = new System.Drawing.Size(135, 40);
            this.txtVoidedMVR3.TabIndex = 1;
            this.txtVoidedMVR3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtVoidedMVR3.Enter += new System.EventHandler(this.txtVoidedMVR3_Enter);
            this.txtVoidedMVR3.Leave += new System.EventHandler(this.txtVoidedMVR3_Leave);
            this.txtVoidedMVR3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtVoidedMVR3_KeyPress);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(344, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Size = new System.Drawing.Size(147, 48);
            this.cmdProcess.TabIndex = 22;
            this.cmdProcess.Text = "Process Void";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // fraUnissued
            // 
            this.fraUnissued.AppearanceKey = "groupBoxNoBorders";
            this.fraUnissued.BackColor = System.Drawing.Color.White;
            this.fraUnissued.Controls.Add(this.lblUnissued2);
            this.fraUnissued.Controls.Add(this.lblUnissued);
            this.fraUnissued.Location = new System.Drawing.Point(170, 290);
            this.fraUnissued.Name = "fraUnissued";
            this.fraUnissued.Size = new System.Drawing.Size(325, 122);
            this.fraUnissued.TabIndex = 24;
            this.fraUnissued.Visible = false;
            // 
            // lblUnissued2
            // 
            this.lblUnissued2.Location = new System.Drawing.Point(0, 26);
            this.lblUnissued2.Name = "lblUnissued2";
            this.lblUnissued2.Size = new System.Drawing.Size(308, 33);
            this.lblUnissued2.TabIndex = 26;
            this.lblUnissued2.Text = "ENTER REASON AND CLICK PROCESS VOID TO CONTINUE";
            // 
            // lblUnissued
            // 
            this.lblUnissued.Name = "lblUnissued";
            this.lblUnissued.Size = new System.Drawing.Size(308, 25);
            this.lblUnissued.TabIndex = 25;
            this.lblUnissued.Text = "VOIDING UNISSUED MVR";
            // 
            // lblReason
            // 
            this.lblReason.Location = new System.Drawing.Point(30, 554);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(48, 17);
            this.lblReason.TabIndex = 17;
            this.lblReason.Text = "REASON";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(454, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(195, 13);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "PRESS ENTER TO  RETRIEVE DATA";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 104);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(211, 17);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "ENTER THE MVR3 NUMBER TO VOID";
            // 
            // cmdFileProcess
            // 
            this.cmdFileProcess.Enabled = false;
            this.cmdFileProcess.Location = new System.Drawing.Point(724, 30);
            this.cmdFileProcess.Name = "cmdFileProcess";
            this.cmdFileProcess.Size = new System.Drawing.Size(73, 25);
            this.cmdFileProcess.TabIndex = 0;
            this.cmdFileProcess.Text = "Process";
            this.cmdFileProcess.Visible = false;
            this.cmdFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
            // 
            // frmVoidMVR3
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(834, 768);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmVoidMVR3";
            this.Text = "Void MVR3";
            this.Load += new System.EventHandler(this.frmVoidMVR3_Load);
            this.FormClosed += new Wisej.Web.FormClosedEventHandler(this.FrmVoidMVR3_FormClosed);
            this.Resize += new System.EventHandler(this.frmVoidMVR3_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMVRT10)).EndInit();
            this.fraMVRT10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMVRT10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraUnissued)).EndInit();
            this.fraUnissued.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
