//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTitleApplication.
	/// </summary>
	public partial class rptTitleApplication : BaseSectionReport
	{
		public rptTitleApplication()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Title Application Report";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptTitleApplication_ReportEnd;
		}

        private void RptTitleApplication_ReportEnd(object sender, EventArgs e)
        {
			rs.DisposeOf();
            rs2.DisposeOf();
            rsAM.DisposeOf();

		}

        public static rptTitleApplication InstancePtr
		{
			get
			{
				return (rptTitleApplication)Sys.GetInstance(typeof(rptTitleApplication));
			}
		}

		protected rptTitleApplication _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTitleApplication	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL = "";
		int lngPK1;
		int lngPK2;
		int intHeadingNumber;
		clsDRWrapper rs2 = new clsDRWrapper();
		double MoneyTotal;
		int total;
		clsDRWrapper rsAM = new clsDRWrapper();
		bool boolFirstPart;
		bool boolRsDone;
		bool boolSecondPart;
		bool boolThirdPart;
		int intPageNumber;
		bool boolData;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			txtTitle.Text = "";
			txtTitle2.Text = "";
			txtFee.Text = "";
			txtFee2.Text = "";
			txtClass.Text = "";
			txtPlate.Text = "";
			txtPlate2.Text = "";
			txtDT.Text = "";
			txtDate2.Text = "";
			txtCustomer.Text = "";
			txtCustomer2.Text = "";
			txtOPID.Text = "";
			txtOPID2.Text = "";
			if (boolFirstPart == true)
			{
				eArgs.EOF = false;
				if (boolRsDone == false)
				{
					FirstPart();
				}
				else
				{
					SecondPart();
				}
			}
			else if (!rsAM.EndOfFile())
			{
				eArgs.EOF = false;
				ThirdPart();
			}
			else
			{
				eArgs.EOF = true;
				return;
			}
			// txtTitle = ""
			// txtTitle2 = ""
			// txtFee.Text = ""
			// txtFee2.Text = ""
			// txtClass.Text = ""
			// txtPlate.Text = ""
			// txtPlate2.Text = ""
			// txtDT.Text = ""
			// txtDate2.Text = ""
			// txtCustomer = ""
			// txtCustomer2 = ""
			// txtOPID = ""
			// txtOPID2 = ""
			if (boolSecondPart == true)
			{
				lblTotals.Text = "Totals";
				txtTotals.Text = Strings.Format(Strings.Format(MoneyTotal, "#,#00.00"), "@@@@@@");
				txtCLTotal.Text = Strings.Format(total, "@@");
			}
			if (boolThirdPart == true)
			{
				lblTotals.Text = "Totals";
				txtTotals.Text = Strings.Format(Strings.Format(MoneyTotal, "#,#00.00"), "@@@@@@");
				txtCLTotal.Text = Strings.Format(total, "@@");
			}
		}

		private void ThirdPart()
		{
			boolData = true;
			total += 1;
			MoneyTotal += rsAM.Get_Fields_Double("TitleFee");
			txtTitle.Text = Strings.Format(rsAM.Get_Fields_String("CTANumber").ToUpper(), "!@@@@@@@@@@");
			txtFee.Text = Strings.Format(rsAM.Get_Fields("TitleFee"), "00.00");
			txtClass.Text = Strings.Format(rsAM.Get_Fields_String("Class"), "!@@");
			txtPlate.Text = Strings.Format(rsAM.Get_Fields_String("plate"), "!@@@@@@@@");
			txtDT.Text = Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy");
			txtCustomer.Text = Strings.Format(MotorVehicle.GetPartyName(rsAM.Get_Fields_Int32("PartyID1"), true), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			// kk03062017 tromv-1246  Use name on CTA if filled in
			rs2.OpenRecordset("SELECT CustomerName FROM TitleApplications WHERE CTANumber = '" + rsAM.Get_Fields_String("CTANumber") + "' " +
							  "AND Plate = '" + rsAM.Get_Fields_String("Plate") + "' AND PeriodCloseoutID = " + rsAM.Get_Fields_Int32("OldMVR3"));
			if (!rs2.EndOfFile() && !rs2.BeginningOfFile())
			{
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("CustomerName"))) != "")
				{
					txtCustomer.Text = Strings.Format(rs2.Get_Fields_String("CustomerName"), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
				}
			}
			txtOPID.Text = Strings.Format(rsAM.Get_Fields_String("OpID"), "@@@");

            //FC:FINAl:SBE - #3060 - compare values using FCUtils.ObjectEqual, otherwise Exception is thrown when Get_Fields returns a non numeric value ("AB99999")
            //if (fecherFoundation.FCUtils.IsNull(rsAM.Get_Fields_String("DoubleCTANumber")) == false && FCConvert.ToString(rsAM.Get_Fields_String("DoubleCTANumber")).Length != 0 && FCConvert.ToInt32(rsAM.Get_Fields_String("DoubleCTANumber")) != 0)
            if (fecherFoundation.FCUtils.IsNull(rsAM.Get_Fields_String("DoubleCTANumber")) == false && FCConvert.ToString(rsAM.Get_Fields_String("DoubleCTANumber")).Length != 0 && !FCUtils.ObjectEqual(0, rsAM.Get_Fields_String("DoubleCTANumber")))
			{
				total += 1;
				// kk03082017 tromv-1246  Rework double cta to not cause RTE if no record for double cta
				txtTitle2.Text = Strings.Format(rsAM.Get_Fields_String("DoubleCTANumber").ToUpper(), "!@@@@@@@@@@");
				txtFee2.Text = "N/A";
				txtPlate2.Text = "- DOUBLE -";
				txtDate2.Text = "";
				txtOPID2.Text = rsAM.Get_Fields_String("OpID");
				txtCustomer2.Text = Strings.Space(34);
				rs2.OpenRecordset("SELECT Customer FROM DoubleCTA WHERE CTANumber = '" + rsAM.Get_Fields_String("DoubleCTANumber") + "'");
				if (!rs2.EndOfFile() && !rs2.BeginningOfFile())
				{
					if (FCConvert.ToString(rs2.Get_Fields_String("Customer")).Length <= 33)
					{
						txtCustomer2.Text = Strings.Format(rs2.Get_Fields_String("Customer"), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
					}
					else
					{
						txtCustomer2.Text = Strings.Mid(FCConvert.ToString(rs2.Get_Fields_String("Customer")), 1, 33);
					}
				}
			}
			rsAM.MoveNext();
			if (rsAM.EndOfFile())
			{
				boolThirdPart = true;
			}
		}

		private void FirstPart()
		{
			boolData = true;
			total += 1;
			MoneyTotal += rs.Get_Fields_Double("Fee");
			txtTitle.Text = Strings.Format(rsAM.Get_Fields_String("CTANumber"), "!@@@@@@@@@@");
			txtFee.Text = Strings.Format(rsAM.Get_Fields("Fee"), "00.00");
			txtClass.Text = Strings.Format(rsAM.Get_Fields_String("Class"), "!@@");
			txtPlate.Text = Strings.Format(rsAM.Get_Fields_String("plate"), "!@@@@@@@@");
			txtDT.Text = Strings.Format(rsAM.Get_Fields_DateTime("RegistrationDate"), "MM/dd/yyyy");
			txtCustomer.Text = Strings.Format(rsAM.Get_Fields_String("CustomerName"), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			txtOPID.Text = Strings.Format(rsAM.Get_Fields_String("OpID"), "@@@");
			if (fecherFoundation.FCUtils.IsNull(rsAM.Get_Fields_Int32("DoubleIfApplicable")) == false && FCConvert.ToString(rsAM.Get_Fields_Int32("DoubleIfApplicable")).Length != 0 && FCConvert.ToInt32(rsAM.Get_Fields_Int32("DoubleIfApplicable")) != 0)
			{
				rs2.OpenRecordset("SELECT * FROM DoubleCTA WHERE ID = " + rsAM.Get_Fields_Int32("DoubleIfApplicable"));
				txtTitle2.Text = Strings.Format(rs2.Get_Fields_String("CTANumber"), "!@@@@@@@@@@");
				txtFee2.Text = "N/A";
				txtPlate2.Text = "- DOUBLE -";
				txtDate2.Text = "";
				if (FCConvert.ToString(rs2.Get_Fields_String("Customer")).Length <= 33)
				{
					txtCustomer2.Text = Strings.Format(rs2.Get_Fields_String("Customer"), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
				}
				else
				{
					txtCustomer2.Text = Strings.Mid(FCConvert.ToString(rs2.Get_Fields_String("Customer")), 1, 33);
				}
				txtOPID2.Text = rsAM.Get_Fields_String("OpID");
			}
			rs.MoveNext();
			if (rs.EndOfFile())
			{
				boolRsDone = true;
			}
		}

		private void SecondPart()
		{
			if (!rsAM.EndOfFile())
			{
				boolData = true;
				total += 1;
				MoneyTotal += rsAM.Get_Fields_Double("TitleFee");
				txtTitle.Text = Strings.Format(rsAM.Get_Fields_String("CTANumber"), "!@@@@@@@@@@");
				txtFee.Text = Strings.Format(rsAM.Get_Fields("TitleFee"), "00.00");
				txtClass.Text = Strings.Format(rsAM.Get_Fields_String("Class"), "!@@");
				txtPlate.Text = Strings.Format(rsAM.Get_Fields_String("plate"), "!@@@@@@@@");
				txtDT.Text = Strings.Format(rsAM.Get_Fields_DateTime("DateUpdated"), "MM/dd/yyyy");
				txtCustomer.Text = Strings.Format(MotorVehicle.GetPartyName(rsAM.Get_Fields_Int32("PartyID1"), true), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
				// kk03062017 tromv-1246  Use name on CTA if filled in
				rs2.OpenRecordset("SELECT CustomerName FROM TitleApplications WHERE CTANumber = '" + rsAM.Get_Fields_String("CTANumber") + "'");
				if (!rs2.EndOfFile() && !rs2.BeginningOfFile())
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("CustomerName"))) != "")
					{
						txtCustomer.Text = Strings.Format(rs2.Get_Fields_String("CustomerName"), "!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
					}
				}
				txtOPID.Text = Strings.Format(rsAM.Get_Fields_String("OpID"), "@@@");
				rsAM.MoveNext();
				if (rsAM.EndOfFile())
				{
					boolFirstPart = false;
					boolSecondPart = true;
				}
			}
			else
			{
				boolFirstPart = false;
				boolSecondPart = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rs1 = new clsDRWrapper();
			string strVendorID = "";
			string strMuni = "";
			string strTownCode = "";
			string strAgent = "";
			string strVersion = "";
			string strPhone = "";
			string strProcessDate = "";
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			//Application.DoEvents();
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							// Dim lngPK1 As Long
							strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSQL);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			strSQL = "SELECT * FROM PeriodCloseout WHERE IssueDate BETWEEN '" + frmReport.InstancePtr.cboStart.Text + "' AND '" + frmReport.InstancePtr.cboEnd.Text + "' ORDER BY IssueDate DESC";
			rs.OpenRecordset(strSQL);
			lngPK1 = 0;
			lngPK2 = 0;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				rs.MoveFirst();
				lngPK2 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			}
			boolFirstPart = false;
			if (frmReport.InstancePtr.cmbInterim.Text == "Interim Reports")
			{
				// kk01162018 tromv-1292  Added NoFeeCTA to Title App Report Query
				strSQL = "SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 < 1 AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND (TitleDone = 1 or TitleFee <> 0 or NoFeeCTA = 1) and rtrim(CTANumber) <> '' ORDER BY CTANumber";
			}
			else
			{
				strSQL = "SELECT * FROM ActivityMaster WHERE Status <> 'V' AND OldMVR3 > " + FCConvert.ToString(lngPK1) + " And OldMVR3 <= " + FCConvert.ToString(lngPK2) + " AND TransactionType <> 'DPR' AND TransactionType <> 'LPS' AND TransactionType <> 'DPS' AND (TitleDone = 1 or TitleFee <> 0 or NoFeeCTA = 1) and rtrim(CTANumber) <> '' ORDER BY CTANumber";
			}
			rsAM.OpenRecordset(strSQL);
			if (rsAM.EndOfFile() != true && rsAM.BeginningOfFile() != true)
			{
				rsAM.MoveLast();
				rsAM.MoveFirst();
			}
			rs1.DisposeOf();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			SubReport2.Report = new rptSubReportHeading();
			intPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (boolData == false)
			{
				txtNoInfo.Text = "No Information";
			}
		}
	}
}
