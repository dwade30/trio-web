//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptTwoColumnTable.
	/// </summary>
	public partial class rptTwoColumnTable : BaseSectionReport
	{
		public rptTwoColumnTable()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Table Printout";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptTwoColumnTable_ReportEnd;
		}

        private void RptTwoColumnTable_ReportEnd(object sender, EventArgs e)
        {
            rsPeriodCloseout.DisposeOf();
        }

        public static rptTwoColumnTable InstancePtr
		{
			get
			{
				return (rptTwoColumnTable)Sys.GetInstance(typeof(rptTwoColumnTable));
			}
		}

		protected rptTwoColumnTable _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTwoColumnTable	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int Row;
		int Col;
		clsDRWrapper rsPeriodCloseout = new clsDRWrapper();
		int PageCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (MotorVehicle.Statics.TableName == "Residence Codes")
			{
				CheckRes:
				;
				if (Row > frmTableMaintenance.InstancePtr.vsResCodes.Rows - 1)
				{
					eArgs.EOF = true;
					return;
				}
				else
				{
					if (frmTableMaintenance.InstancePtr.vsResCodes.TextMatrix(Row, Col) == "")
					{
						Row += 1;
						goto CheckRes;
					}
					Field1.Text = frmTableMaintenance.InstancePtr.vsResCodes.TextMatrix(Row, Col);
					Field2.Text = frmTableMaintenance.InstancePtr.vsResCodes.TextMatrix(Row, Col + 1);
					Row += 1;
				}
			}
			else if (MotorVehicle.Statics.TableName == "State Codes")
			{
				CheckState:
				;
				if (Row > frmTableMaintenance.InstancePtr.vsState.Rows - 1)
				{
					eArgs.EOF = true;
					return;
				}
				else
				{
					if (frmTableMaintenance.InstancePtr.vsState.TextMatrix(Row, Col) == "")
					{
						Row += 1;
						goto CheckState;
					}
					Field1.Text = frmTableMaintenance.InstancePtr.vsState.TextMatrix(Row, Col);
					Field2.Text = frmTableMaintenance.InstancePtr.vsState.TextMatrix(Row, Col + 1);
					Row += 1;
				}
			}
			else if (MotorVehicle.Statics.TableName == "Print Priorities")
			{
				if (frmTableMaintenance.InstancePtr.vsPrint.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsPrint.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsPrint.TextMatrix(Row, Col + 1);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "County Codes")
			{
				if (frmTableMaintenance.InstancePtr.vsCounty.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsCounty.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsCounty.TextMatrix(Row, Col + 1);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Fees")
			{
				if (frmTableMaintenance.InstancePtr.vsAgentFee.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsAgentFee.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsAgentFee.TextMatrix(Row, Col + 1);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Color Codes")
			{
				if (frmTableMaintenance.InstancePtr.vsColor.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsColor.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsColor.TextMatrix(Row, Col + 1);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Default Values")
			{
				if (frmTableMaintenance.InstancePtr.vsDefaults.TextMatrix(Row, Col) == "")
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = frmTableMaintenance.InstancePtr.vsDefaults.TextMatrix(Row, Col);
				Field2.Text = frmTableMaintenance.InstancePtr.vsDefaults.TextMatrix(Row, Col + 1);
				Row += 1;
			}
			else if (MotorVehicle.Statics.TableName == "Period Closeout")
			{
				if (rsPeriodCloseout.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				Field1.Text = Strings.Format(rsPeriodCloseout.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
				Field2.Text = Strings.Format(rsPeriodCloseout.Get_Fields_DateTime("IssueDate"), "hh:mm tt");
				Row += 1;
				rsPeriodCloseout.MoveNext();
			}
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label35.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label34.Text = Strings.Format(fecherFoundation.DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (MotorVehicle.Statics.TableName == "Residence Codes")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "State Codes")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Print Priorities")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "County Codes")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Fees")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Color Codes")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Default Values")
			{
				Row = 1;
				Col = 2;
			}
			else if (MotorVehicle.Statics.TableName == "Period Closeout")
			{
				Row = 1;
				Col = 2;
				rsPeriodCloseout.OpenRecordset("SELECT * FROM PeriodCloseout");
				if (rsPeriodCloseout.EndOfFile() != true && rsPeriodCloseout.BeginningOfFile() != true)
				{
					rsPeriodCloseout.MoveLast();
					rsPeriodCloseout.MoveFirst();
				}
			}
			//Application.DoEvents();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			if (MotorVehicle.Statics.TableName == "Period Closeout")
			{
				lblTitle.Text = "Period Closeout Listing";
			}
			else
			{
				lblTitle.Text = MotorVehicle.Statics.TableName;
			}
			if (MotorVehicle.Statics.TableName == "Residence Codes")
			{
				Label1.Text = "Code";
				Label2.Text = "Town Name";
			}
			else if (MotorVehicle.Statics.TableName == "State Codes")
			{
				Label1.Text = "Abbreviation";
				Label2.Text = "State";
			}
			else if (MotorVehicle.Statics.TableName == "Print Priorities")
			{
				Label1.Text = "Priority";
				Label2.Text = "Description";
			}
			else if (MotorVehicle.Statics.TableName == "County Codes")
			{
				Label1.Text = "Code";
				Label2.Text = "Name";
			}
			else if (MotorVehicle.Statics.TableName == "Fees")
			{
				Label1.Text = "Category";
				Label2.Text = "Value";
			}
			else if (MotorVehicle.Statics.TableName == "Color Codes")
			{
				Label1.Text = "Code";
				Label2.Text = "Color";
			}
			else if (MotorVehicle.Statics.TableName == "Default Values")
			{
				Label1.Text = "Category";
				Label2.Text = "Value";
			}
			else if (MotorVehicle.Statics.TableName == "Period Closeout")
			{
				Label1.Text = "Closeout Date";
				Label2.Text = "Closeout Time";
			}
			//Application.DoEvents();
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label10.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptTwoColumnTable_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptTwoColumnTable properties;
			//rptTwoColumnTable.Caption	= "Table Printout";
			//rptTwoColumnTable.Icon	= "rptTwoColumnTable.dsx":0000";
			//rptTwoColumnTable.Left	= 0;
			//rptTwoColumnTable.Top	= 0;
			//rptTwoColumnTable.Width	= 11880;
			//rptTwoColumnTable.Height	= 8595;
			//rptTwoColumnTable.StartUpPosition	= 3;
			//rptTwoColumnTable.SectionData	= "rptTwoColumnTable.dsx":058A;
			//End Unmaped Properties
		}
	}
}
