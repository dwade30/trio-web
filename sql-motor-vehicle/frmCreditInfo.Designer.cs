//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for frmCreditInfo.
	/// </summary>
	partial class frmCreditInfo
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label2;
		public fecherFoundation.FCButton cmdReturn;
		public Global.T2KBackFillDecimal txtCommercialCredit;
		public Global.T2KBackFillDecimal txtTransferCredit;
		public Global.T2KBackFillDecimal txtTotalCredit;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdReturn = new fecherFoundation.FCButton();
            this.txtCommercialCredit = new Global.T2KBackFillDecimal();
            this.txtTransferCredit = new Global.T2KBackFillDecimal();
            this.txtTotalCredit = new Global.T2KBackFillDecimal();
            this.Label2_1 = new fecherFoundation.FCLabel();
            this.Label2_0 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommercialCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCredit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 234);
            this.BottomPanel.Size = new System.Drawing.Size(402, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdReturn);
            this.ClientArea.Controls.Add(this.txtCommercialCredit);
            this.ClientArea.Controls.Add(this.txtTransferCredit);
            this.ClientArea.Controls.Add(this.txtTotalCredit);
            this.ClientArea.Controls.Add(this.Label2_1);
            this.ClientArea.Controls.Add(this.Label2_0);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(402, 174);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(402, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(208, 30);
            this.HeaderText.Text = "Credit Information";
            // 
            // cmdReturn
            // 
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(30, 210);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(95, 40);
            this.cmdReturn.TabIndex = 6;
            this.cmdReturn.Text = "Return";
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // txtCommercialCredit
            // 
            this.txtCommercialCredit.Location = new System.Drawing.Point(224, 30);
            this.txtCommercialCredit.MaxLength = 8;
            this.txtCommercialCredit.Name = "txtCommercialCredit";
            this.txtCommercialCredit.Size = new System.Drawing.Size(150, 40);
            this.txtCommercialCredit.TabIndex = 0;
            this.txtCommercialCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtTransferCredit
            // 
            this.txtTransferCredit.Location = new System.Drawing.Point(224, 90);
            this.txtTransferCredit.MaxLength = 8;
            this.txtTransferCredit.Name = "txtTransferCredit";
            this.txtTransferCredit.Size = new System.Drawing.Size(150, 40);
            this.txtTransferCredit.TabIndex = 1;
            this.txtTransferCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtTotalCredit
            // 
            this.txtTotalCredit.Enabled = false;
            this.txtTotalCredit.Location = new System.Drawing.Point(224, 150);
            this.txtTotalCredit.MaxLength = 8;
            this.txtTotalCredit.Name = "txtTotalCredit";
            this.txtTotalCredit.Size = new System.Drawing.Size(150, 40);
            this.txtTotalCredit.TabIndex = 5;
            this.txtTotalCredit.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label2_1
            // 
            this.Label2_1.Location = new System.Drawing.Point(30, 164);
            this.Label2_1.Name = "Label2_1";
            this.Label2_1.Size = new System.Drawing.Size(90, 17);
            this.Label2_1.TabIndex = 4;
            this.Label2_1.Text = "TOTAL CREDIT";
            // 
            // Label2_0
            // 
            this.Label2_0.Location = new System.Drawing.Point(30, 44);
            this.Label2_0.Name = "Label2_0";
            this.Label2_0.Size = new System.Drawing.Size(130, 17);
            this.Label2_0.TabIndex = 3;
            this.Label2_0.Text = "COMMERCIAL CREDIT";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 104);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(110, 17);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "TRANSFER CREDIT";
            // 
            // frmCreditInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(402, 342);
            this.FillColor = 0;
            this.Name = "frmCreditInfo";
            this.Text = "Credit Information";
            this.Load += new System.EventHandler(this.frmCreditInfo_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommercialCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCredit)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}