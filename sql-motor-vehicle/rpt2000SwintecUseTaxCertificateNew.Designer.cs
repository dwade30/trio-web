﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rpt2000SwintecUseTaxCertificateNew.
	/// </summary>
	partial class rpt2000SwintecUseTaxCertificateNew
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt2000SwintecUseTaxCertificateNew));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVin1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLength = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSellerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSellerAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDateofTransfer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHP = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVin2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFullPurchasePrice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAllowance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherStateTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUseTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExemptTypeE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWhereRegistered = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLienHolderName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLienHolderAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDateOfOriginalReg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserSocialSec = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDatePaid1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaserState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDatePaid2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVAVeteran = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherReason = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDifferentOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVin1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLength)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateofTransfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVin2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFullPurchasePrice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAllowance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherStateTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUseTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWhereRegistered)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienHolderName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienHolderAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateOfOriginalReg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserSocialSec)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserZip)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDatePaid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserCity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDatePaid2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVAVeteran)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherReason)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDifferentOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldType1,
				this.fldType2,
				this.fldMake1,
				this.fldModel1,
				this.fldYear1,
				this.fldVin1,
				this.fldLength,
				this.fldSellerName,
				this.fldSellerAddress,
				this.fldDateofTransfer,
				this.fldMake2,
				this.fldModel2,
				this.fldYear2,
				this.fldHP,
				this.fldVin2,
				this.fldFullPurchasePrice,
				this.fldAllowance,
				this.fldExemptTypeA,
				this.fldOtherStateTax,
				this.fldOtherAmount,
				this.fldExemptNumber,
				this.fldNetAmount,
				this.fldUseTax,
				this.fldPurchaserName,
				this.fldExemptTypeC,
				this.fldExemptTypeD,
				this.fldExemptTypeB,
				this.fldExemptTypeE,
				this.fldWhereRegistered,
				this.fldLienHolderName,
				this.fldLienHolderAddress,
				this.fldRegNumber,
				this.fldDateOfOriginalReg,
				this.fldPurchaserSocialSec,
				this.fldPurchaserZip,
				this.fldRegDate,
				this.fldTaxAmount,
				this.fldDatePaid1,
				this.fldPurchaserAddress,
				this.fldPurchaserCity,
				this.fldPurchaserState,
				this.fldDatePaid2,
				this.fldVAVeteran,
				this.fldOtherReason,
				this.fldDifferentOwner,
				this.fldClassPlate
			});
			this.Detail.Height = 9.0625F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldType1
			// 
			this.fldType1.CanGrow = false;
			this.fldType1.Height = 0.1875F;
			this.fldType1.Left = 0.0625F;
			this.fldType1.MultiLine = false;
			this.fldType1.Name = "fldType1";
			this.fldType1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldType1.Text = " ";
			this.fldType1.Top = 0.0625F;
			this.fldType1.Width = 3.625F;
			// 
			// fldType2
			// 
			this.fldType2.CanGrow = false;
			this.fldType2.Height = 0.1875F;
			this.fldType2.Left = 4.0625F;
			this.fldType2.MultiLine = false;
			this.fldType2.Name = "fldType2";
			this.fldType2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldType2.Text = " ";
			this.fldType2.Top = 0.0625F;
			this.fldType2.Width = 3F;
			// 
			// fldMake1
			// 
			this.fldMake1.CanGrow = false;
			this.fldMake1.Height = 0.1875F;
			this.fldMake1.Left = 0.0625F;
			this.fldMake1.MultiLine = false;
			this.fldMake1.Name = "fldMake1";
			this.fldMake1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldMake1.Text = " ";
			this.fldMake1.Top = 0.40625F;
			this.fldMake1.Width = 2.125F;
			// 
			// fldModel1
			// 
			this.fldModel1.CanGrow = false;
			this.fldModel1.Height = 0.1875F;
			this.fldModel1.Left = 2.4375F;
			this.fldModel1.MultiLine = false;
			this.fldModel1.Name = "fldModel1";
			this.fldModel1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldModel1.Text = " ";
			this.fldModel1.Top = 0.40625F;
			this.fldModel1.Width = 0.8125F;
			// 
			// fldYear1
			// 
			this.fldYear1.CanGrow = false;
			this.fldYear1.Height = 0.1875F;
			this.fldYear1.Left = 3.3125F;
			this.fldYear1.MultiLine = false;
			this.fldYear1.Name = "fldYear1";
			this.fldYear1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldYear1.Text = " ";
			this.fldYear1.Top = 0.40625F;
			this.fldYear1.Width = 0.6875F;
			// 
			// fldVin1
			// 
			this.fldVin1.CanGrow = false;
			this.fldVin1.Height = 0.1875F;
			this.fldVin1.Left = 0.0625F;
			this.fldVin1.MultiLine = false;
			this.fldVin1.Name = "fldVin1";
			this.fldVin1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldVin1.Text = " ";
			this.fldVin1.Top = 0.875F;
			this.fldVin1.Width = 2.125F;
			// 
			// fldLength
			// 
			this.fldLength.CanGrow = false;
			this.fldLength.Height = 0.1875F;
			this.fldLength.Left = 2.25F;
			this.fldLength.MultiLine = false;
			this.fldLength.Name = "fldLength";
			this.fldLength.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldLength.Text = " ";
			this.fldLength.Top = 0.875F;
			this.fldLength.Width = 0.8125F;
			// 
			// fldSellerName
			// 
			this.fldSellerName.CanGrow = false;
			this.fldSellerName.Height = 0.1875F;
			this.fldSellerName.Left = 1.375F;
			this.fldSellerName.MultiLine = false;
			this.fldSellerName.Name = "fldSellerName";
			this.fldSellerName.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldSellerName.Text = " ";
			this.fldSellerName.Top = 1.1875F;
			this.fldSellerName.Width = 2.5F;
			// 
			// fldSellerAddress
			// 
			this.fldSellerAddress.CanGrow = false;
			this.fldSellerAddress.Height = 0.1875F;
			this.fldSellerAddress.Left = 1.375F;
			this.fldSellerAddress.MultiLine = false;
			this.fldSellerAddress.Name = "fldSellerAddress";
			this.fldSellerAddress.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldSellerAddress.Text = " ";
			this.fldSellerAddress.Top = 1.53125F;
			this.fldSellerAddress.Width = 5.1875F;
			// 
			// fldDateofTransfer
			// 
			this.fldDateofTransfer.CanGrow = false;
			this.fldDateofTransfer.Height = 0.1875F;
			this.fldDateofTransfer.Left = 5.875F;
			this.fldDateofTransfer.MultiLine = false;
			this.fldDateofTransfer.Name = "fldDateofTransfer";
			this.fldDateofTransfer.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldDateofTransfer.Text = " ";
			this.fldDateofTransfer.Top = 1.1875F;
			this.fldDateofTransfer.Width = 1F;
			// 
			// fldMake2
			// 
			this.fldMake2.CanGrow = false;
			this.fldMake2.Height = 0.1875F;
			this.fldMake2.Left = 4.0625F;
			this.fldMake2.MultiLine = false;
			this.fldMake2.Name = "fldMake2";
			this.fldMake2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldMake2.Text = " ";
			this.fldMake2.Top = 0.40625F;
			this.fldMake2.Width = 1.6875F;
			// 
			// fldModel2
			// 
			this.fldModel2.CanGrow = false;
			this.fldModel2.Height = 0.1875F;
			this.fldModel2.Left = 5.6875F;
			this.fldModel2.MultiLine = false;
			this.fldModel2.Name = "fldModel2";
			this.fldModel2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldModel2.Text = " ";
			this.fldModel2.Top = 0.40625F;
			this.fldModel2.Width = 0.8125F;
			// 
			// fldYear2
			// 
			this.fldYear2.CanGrow = false;
			this.fldYear2.Height = 0.1875F;
			this.fldYear2.Left = 6.46875F;
			this.fldYear2.MultiLine = false;
			this.fldYear2.Name = "fldYear2";
			this.fldYear2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldYear2.Text = " ";
			this.fldYear2.Top = 0.40625F;
			this.fldYear2.Width = 0.625F;
			// 
			// fldHP
			// 
			this.fldHP.CanGrow = false;
			this.fldHP.Height = 0.1875F;
			this.fldHP.Left = 3.25F;
			this.fldHP.MultiLine = false;
			this.fldHP.Name = "fldHP";
			this.fldHP.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldHP.Text = " ";
			this.fldHP.Top = 0.875F;
			this.fldHP.Width = 0.6875F;
			// 
			// fldVin2
			// 
			this.fldVin2.CanGrow = false;
			this.fldVin2.Height = 0.1875F;
			this.fldVin2.Left = 4.0625F;
			this.fldVin2.MultiLine = false;
			this.fldVin2.Name = "fldVin2";
			this.fldVin2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldVin2.Text = " ";
			this.fldVin2.Top = 0.875F;
			this.fldVin2.Width = 2.125F;
			// 
			// fldFullPurchasePrice
			// 
			this.fldFullPurchasePrice.CanGrow = false;
			this.fldFullPurchasePrice.Height = 0.1875F;
			this.fldFullPurchasePrice.Left = 5.78125F;
			this.fldFullPurchasePrice.MultiLine = false;
			this.fldFullPurchasePrice.Name = "fldFullPurchasePrice";
			this.fldFullPurchasePrice.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldFullPurchasePrice.Text = " ";
			this.fldFullPurchasePrice.Top = 1.78125F;
			this.fldFullPurchasePrice.Width = 1.09375F;
			// 
			// fldAllowance
			// 
			this.fldAllowance.CanGrow = false;
			this.fldAllowance.Height = 0.1875F;
			this.fldAllowance.Left = 5.78125F;
			this.fldAllowance.MultiLine = false;
			this.fldAllowance.Name = "fldAllowance";
			this.fldAllowance.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldAllowance.Text = " ";
			this.fldAllowance.Top = 2.03125F;
			this.fldAllowance.Width = 1.09375F;
			// 
			// fldExemptTypeA
			// 
			this.fldExemptTypeA.CanGrow = false;
			this.fldExemptTypeA.Height = 0.1875F;
			this.fldExemptTypeA.Left = 0.09375F;
			this.fldExemptTypeA.MultiLine = false;
			this.fldExemptTypeA.Name = "fldExemptTypeA";
			this.fldExemptTypeA.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeA.Text = " ";
			this.fldExemptTypeA.Top = 3.1875F;
			this.fldExemptTypeA.Width = 0.25F;
			// 
			// fldOtherStateTax
			// 
			this.fldOtherStateTax.CanGrow = false;
			this.fldOtherStateTax.Height = 0.1875F;
			this.fldOtherStateTax.Left = 5.5625F;
			this.fldOtherStateTax.MultiLine = false;
			this.fldOtherStateTax.Name = "fldOtherStateTax";
			this.fldOtherStateTax.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldOtherStateTax.Text = " ";
			this.fldOtherStateTax.Top = 3.4375F;
			this.fldOtherStateTax.Width = 1.5625F;
			// 
			// fldOtherAmount
			// 
			this.fldOtherAmount.CanGrow = false;
			this.fldOtherAmount.Height = 0.1875F;
			this.fldOtherAmount.Left = 5.5625F;
			this.fldOtherAmount.MultiLine = false;
			this.fldOtherAmount.Name = "fldOtherAmount";
			this.fldOtherAmount.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldOtherAmount.Text = " ";
			this.fldOtherAmount.Top = 3.625F;
			this.fldOtherAmount.Width = 1.5625F;
			// 
			// fldExemptNumber
			// 
			this.fldExemptNumber.CanGrow = false;
			this.fldExemptNumber.Height = 0.1875F;
			this.fldExemptNumber.Left = 3.25F;
			this.fldExemptNumber.MultiLine = false;
			this.fldExemptNumber.Name = "fldExemptNumber";
			this.fldExemptNumber.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptNumber.Text = " ";
			this.fldExemptNumber.Top = 3.4375F;
			this.fldExemptNumber.Width = 0.875F;
			// 
			// fldNetAmount
			// 
			this.fldNetAmount.CanGrow = false;
			this.fldNetAmount.Height = 0.1875F;
			this.fldNetAmount.Left = 5.78125F;
			this.fldNetAmount.MultiLine = false;
			this.fldNetAmount.Name = "fldNetAmount";
			this.fldNetAmount.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldNetAmount.Text = " ";
			this.fldNetAmount.Top = 2.28125F;
			this.fldNetAmount.Width = 1.09375F;
			// 
			// fldUseTax
			// 
			this.fldUseTax.CanGrow = false;
			this.fldUseTax.Height = 0.1875F;
			this.fldUseTax.Left = 5.78125F;
			this.fldUseTax.MultiLine = false;
			this.fldUseTax.Name = "fldUseTax";
			this.fldUseTax.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldUseTax.Text = " ";
			this.fldUseTax.Top = 2.59375F;
			this.fldUseTax.Width = 1.09375F;
			// 
			// fldPurchaserName
			// 
			this.fldPurchaserName.CanGrow = false;
			this.fldPurchaserName.Height = 0.1875F;
			this.fldPurchaserName.Left = 1.375F;
			this.fldPurchaserName.MultiLine = false;
			this.fldPurchaserName.Name = "fldPurchaserName";
			this.fldPurchaserName.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserName.Text = " ";
			this.fldPurchaserName.Top = 6.375F;
			this.fldPurchaserName.Width = 2F;
			// 
			// fldExemptTypeC
			// 
			this.fldExemptTypeC.CanGrow = false;
			this.fldExemptTypeC.Height = 0.1875F;
			this.fldExemptTypeC.Left = 4.21875F;
			this.fldExemptTypeC.MultiLine = false;
			this.fldExemptTypeC.Name = "fldExemptTypeC";
			this.fldExemptTypeC.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeC.Text = " ";
			this.fldExemptTypeC.Top = 3.1875F;
			this.fldExemptTypeC.Width = 0.25F;
			// 
			// fldExemptTypeD
			// 
			this.fldExemptTypeD.CanGrow = false;
			this.fldExemptTypeD.Height = 0.1875F;
			this.fldExemptTypeD.Left = 4.21875F;
			this.fldExemptTypeD.MultiLine = false;
			this.fldExemptTypeD.Name = "fldExemptTypeD";
			this.fldExemptTypeD.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeD.Text = " ";
			this.fldExemptTypeD.Top = 3.9375F;
			this.fldExemptTypeD.Width = 0.25F;
			// 
			// fldExemptTypeB
			// 
			this.fldExemptTypeB.CanGrow = false;
			this.fldExemptTypeB.Height = 0.1875F;
			this.fldExemptTypeB.Left = 0.09375F;
			this.fldExemptTypeB.MultiLine = false;
			this.fldExemptTypeB.Name = "fldExemptTypeB";
			this.fldExemptTypeB.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeB.Text = " ";
			this.fldExemptTypeB.Top = 3.9375F;
			this.fldExemptTypeB.Width = 0.25F;
			// 
			// fldExemptTypeE
			// 
			this.fldExemptTypeE.CanGrow = false;
			this.fldExemptTypeE.Height = 0.1875F;
			this.fldExemptTypeE.Left = 4.21875F;
			this.fldExemptTypeE.MultiLine = false;
			this.fldExemptTypeE.Name = "fldExemptTypeE";
			this.fldExemptTypeE.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldExemptTypeE.Text = " ";
			this.fldExemptTypeE.Top = 4.375F;
			this.fldExemptTypeE.Width = 0.25F;
			// 
			// fldWhereRegistered
			// 
			this.fldWhereRegistered.CanGrow = false;
			this.fldWhereRegistered.Height = 0.1875F;
			this.fldWhereRegistered.Left = 2.375F;
			this.fldWhereRegistered.MultiLine = false;
			this.fldWhereRegistered.Name = "fldWhereRegistered";
			this.fldWhereRegistered.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldWhereRegistered.Text = " ";
			this.fldWhereRegistered.Top = 4.1875F;
			this.fldWhereRegistered.Width = 1.75F;
			// 
			// fldLienHolderName
			// 
			this.fldLienHolderName.CanGrow = false;
			this.fldLienHolderName.Height = 0.1875F;
			this.fldLienHolderName.Left = 1.375F;
			this.fldLienHolderName.MultiLine = false;
			this.fldLienHolderName.Name = "fldLienHolderName";
			this.fldLienHolderName.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldLienHolderName.Text = " ";
			this.fldLienHolderName.Top = 5.28125F;
			this.fldLienHolderName.Width = 2.5625F;
			// 
			// fldLienHolderAddress
			// 
			this.fldLienHolderAddress.CanGrow = false;
			this.fldLienHolderAddress.Height = 0.1875F;
			this.fldLienHolderAddress.Left = 4.375F;
			this.fldLienHolderAddress.MultiLine = false;
			this.fldLienHolderAddress.Name = "fldLienHolderAddress";
			this.fldLienHolderAddress.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldLienHolderAddress.Text = " ";
			this.fldLienHolderAddress.Top = 5.40625F;
			this.fldLienHolderAddress.Width = 2.5F;
			// 
			// fldRegNumber
			// 
			this.fldRegNumber.CanGrow = false;
			this.fldRegNumber.Height = 0.1875F;
			this.fldRegNumber.Left = 2.40625F;
			this.fldRegNumber.MultiLine = false;
			this.fldRegNumber.Name = "fldRegNumber";
			this.fldRegNumber.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldRegNumber.Text = " ";
			this.fldRegNumber.Top = 4.4375F;
			this.fldRegNumber.Width = 1.75F;
			// 
			// fldDateOfOriginalReg
			// 
			this.fldDateOfOriginalReg.CanGrow = false;
			this.fldDateOfOriginalReg.Height = 0.1875F;
			this.fldDateOfOriginalReg.Left = 2.40625F;
			this.fldDateOfOriginalReg.MultiLine = false;
			this.fldDateOfOriginalReg.Name = "fldDateOfOriginalReg";
			this.fldDateOfOriginalReg.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldDateOfOriginalReg.Text = " ";
			this.fldDateOfOriginalReg.Top = 4.6875F;
			this.fldDateOfOriginalReg.Width = 1.75F;
			// 
			// fldPurchaserSocialSec
			// 
			this.fldPurchaserSocialSec.CanGrow = false;
			this.fldPurchaserSocialSec.Height = 0.1875F;
			this.fldPurchaserSocialSec.Left = 3.375F;
			this.fldPurchaserSocialSec.MultiLine = false;
			this.fldPurchaserSocialSec.Name = "fldPurchaserSocialSec";
			this.fldPurchaserSocialSec.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserSocialSec.Text = " ";
			this.fldPurchaserSocialSec.Top = 6.375F;
			this.fldPurchaserSocialSec.Width = 1.75F;
			// 
			// fldPurchaserZip
			// 
			this.fldPurchaserZip.CanGrow = false;
			this.fldPurchaserZip.Height = 0.1875F;
			this.fldPurchaserZip.Left = 6F;
			this.fldPurchaserZip.MultiLine = false;
			this.fldPurchaserZip.Name = "fldPurchaserZip";
			this.fldPurchaserZip.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserZip.Text = " ";
			this.fldPurchaserZip.Top = 6.84375F;
			this.fldPurchaserZip.Width = 0.625F;
			// 
			// fldRegDate
			// 
			this.fldRegDate.CanGrow = false;
			this.fldRegDate.Height = 0.1875F;
			this.fldRegDate.Left = 2.375F;
			this.fldRegDate.MultiLine = false;
			this.fldRegDate.Name = "fldRegDate";
			this.fldRegDate.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldRegDate.Text = " ";
			this.fldRegDate.Top = 7.5F;
			this.fldRegDate.Width = 1.5625F;
			// 
			// fldTaxAmount
			// 
			this.fldTaxAmount.CanGrow = false;
			this.fldTaxAmount.Height = 0.1875F;
			this.fldTaxAmount.Left = 5.625F;
			this.fldTaxAmount.MultiLine = false;
			this.fldTaxAmount.Name = "fldTaxAmount";
			this.fldTaxAmount.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldTaxAmount.Text = " ";
			this.fldTaxAmount.Top = 7.5F;
			this.fldTaxAmount.Width = 1.5F;
			// 
			// fldDatePaid1
			// 
			this.fldDatePaid1.CanGrow = false;
			this.fldDatePaid1.Height = 0.1875F;
			this.fldDatePaid1.Left = 5.625F;
			this.fldDatePaid1.MultiLine = false;
			this.fldDatePaid1.Name = "fldDatePaid1";
			this.fldDatePaid1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldDatePaid1.Text = " ";
			this.fldDatePaid1.Top = 7.6875F;
			this.fldDatePaid1.Width = 1.5F;
			// 
			// fldPurchaserAddress
			// 
			this.fldPurchaserAddress.CanGrow = false;
			this.fldPurchaserAddress.Height = 0.1875F;
			this.fldPurchaserAddress.Left = 1.375F;
			this.fldPurchaserAddress.MultiLine = false;
			this.fldPurchaserAddress.Name = "fldPurchaserAddress";
			this.fldPurchaserAddress.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldPurchaserAddress.Text = " ";
			this.fldPurchaserAddress.Top = 6.84375F;
			this.fldPurchaserAddress.Width = 1.875F;
			// 
			// fldPurchaserCity
			// 
			this.fldPurchaserCity.CanGrow = false;
			this.fldPurchaserCity.Height = 0.1875F;
			this.fldPurchaserCity.Left = 3.375F;
			this.fldPurchaserCity.MultiLine = false;
			this.fldPurchaserCity.Name = "fldPurchaserCity";
			this.fldPurchaserCity.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserCity.Text = " ";
			this.fldPurchaserCity.Top = 6.84375F;
			this.fldPurchaserCity.Width = 1.4375F;
			// 
			// fldPurchaserState
			// 
			this.fldPurchaserState.CanGrow = false;
			this.fldPurchaserState.Height = 0.1875F;
			this.fldPurchaserState.Left = 5.375F;
			this.fldPurchaserState.MultiLine = false;
			this.fldPurchaserState.Name = "fldPurchaserState";
			this.fldPurchaserState.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldPurchaserState.Text = " ";
			this.fldPurchaserState.Top = 6.84375F;
			this.fldPurchaserState.Width = 0.625F;
			// 
			// fldDatePaid2
			// 
			this.fldDatePaid2.CanGrow = false;
			this.fldDatePaid2.Height = 0.1875F;
			this.fldDatePaid2.Left = 5.375F;
			this.fldDatePaid2.MultiLine = false;
			this.fldDatePaid2.Name = "fldDatePaid2";
			this.fldDatePaid2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldDatePaid2.Text = " ";
			this.fldDatePaid2.Top = 8.75F;
			this.fldDatePaid2.Width = 1.6875F;
			// 
			// fldVAVeteran
			// 
			this.fldVAVeteran.CanGrow = false;
			this.fldVAVeteran.Height = 0.1875F;
			this.fldVAVeteran.Left = 5.84375F;
			this.fldVAVeteran.MultiLine = false;
			this.fldVAVeteran.Name = "fldVAVeteran";
			this.fldVAVeteran.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldVAVeteran.Text = " ";
			this.fldVAVeteran.Top = 3.9375F;
			this.fldVAVeteran.Width = 1.5625F;
			// 
			// fldOtherReason
			// 
			this.fldOtherReason.CanGrow = false;
			this.fldOtherReason.Height = 0.3125F;
			this.fldOtherReason.Left = 4.625F;
			this.fldOtherReason.MultiLine = false;
			this.fldOtherReason.Name = "fldOtherReason";
			this.fldOtherReason.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldOtherReason.Text = " ";
			this.fldOtherReason.Top = 4.625F;
			this.fldOtherReason.Width = 2.5F;
			// 
			// fldDifferentOwner
			// 
			this.fldDifferentOwner.CanGrow = false;
			this.fldDifferentOwner.Height = 0.1875F;
			this.fldDifferentOwner.Left = 4.375F;
			this.fldDifferentOwner.MultiLine = false;
			this.fldDifferentOwner.Name = "fldDifferentOwner";
			this.fldDifferentOwner.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldDifferentOwner.Text = " ";
			this.fldDifferentOwner.Top = 5.15625F;
			this.fldDifferentOwner.Width = 2.5F;
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.CanGrow = false;
			this.fldClassPlate.Height = 0.1875F;
			this.fldClassPlate.Left = 0.625F;
			this.fldClassPlate.MultiLine = false;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
			this.fldClassPlate.Text = " ";
			this.fldClassPlate.Top = 7.5F;
			this.fldClassPlate.Width = 1.5625F;
			// 
			// rpt2000SwintecUseTaxCertificateNew
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.1388889F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.40625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.fldType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVin1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLength)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSellerAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateofTransfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVin2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFullPurchasePrice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAllowance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherStateTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUseTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExemptTypeE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWhereRegistered)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienHolderName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienHolderAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateOfOriginalReg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserSocialSec)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserZip)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDatePaid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserCity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaserState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDatePaid2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVAVeteran)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherReason)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDifferentOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVin1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLength;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSellerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSellerAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDateofTransfer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHP;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVin2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFullPurchasePrice;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAllowance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeA;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherStateTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUseTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExemptTypeE;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWhereRegistered;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienHolderName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienHolderAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDateOfOriginalReg;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserSocialSec;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDatePaid1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaserState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDatePaid2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVAVeteran;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherReason;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDifferentOwner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
