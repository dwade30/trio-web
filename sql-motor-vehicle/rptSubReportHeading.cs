//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptSubReportHeading.
	/// </summary>
	public partial class rptSubReportHeading : BaseSectionReport
	{
		public rptSubReportHeading()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Report Heading";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptSubReportHeading InstancePtr
		{
			get
			{
				return (rptSubReportHeading)Sys.GetInstance(typeof(rptSubReportHeading));
			}
		}

		protected rptSubReportHeading _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSubReportHeading	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirst;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirst)
			{
				blnFirst = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirst = true;
			//this.Printer.RenderMode = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: txtTownCounty As object	OnWrite(string)
			// vbPorter upgrade warning: txtAgent As object	OnWrite(string)
			// vbPorter upgrade warning: txtDate As object	OnWrite(string)
			// vbPorter upgrade warning: txtProcess As object	OnWrite(string)
			// vbPorter upgrade warning: txtAuthType As object	OnWrite(string)
			// vbPorter upgrade warning: txtDateReceived As object	OnWrite(string)
			// vbPorter upgrade warning: fldLine1 As object	OnWrite(string)
			// vbPorter upgrade warning: strLevel As string	OnWrite(int, string)
			string strLevel;
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rs1 = new clsDRWrapper();
			string strProcessDate = "";
			string strSql = "";
			int lngPK1 = 0;
			strLevel = FCConvert.ToString(MotorVehicle.Statics.TownLevel);
			if (strLevel == "9")
			{
				strLevel = "MANUAL";
			}
			else if (strLevel == "1")
			{
				strLevel = "RE-REG";
			}
			else if (strLevel == "2")
			{
				strLevel = "NEW";
			}
			else if (strLevel == "3")
			{
				strLevel = "TRUCK";
			}
			else if (strLevel == "4")
			{
				strLevel = "TRANSIT";
			}
			else if (strLevel == "5")
			{
				strLevel = "LIMITED NEW";
			}
			else if (strLevel == "6")
			{
				strLevel = "EXC TAX";
			}
			if (MotorVehicle.Statics.AllowRentals)
			{
				strLevel += "/RENTAL";
			}
			if (frmReport.InstancePtr.cmbInterim.Text != "Interim Reports")
			{
				if (Information.IsDate(frmReport.InstancePtr.cboEnd.Text) == true)
				{
					if (Information.IsDate(frmReport.InstancePtr.cboStart.Text) == true)
					{
						if (frmReport.InstancePtr.cboEnd.Text == frmReport.InstancePtr.cboStart.Text)
						{
							strSql = "SELECT * FROM PeriodCloseout WHERE IssueDate = '" + frmReport.InstancePtr.cboEnd.Text + "'";
							rs.OpenRecordset(strSql);
							lngPK1 = 0;
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								rs.MoveLast();
								rs.MoveFirst();
								lngPK1 = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
								rs.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = " + FCConvert.ToString(lngPK1 - 1));
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.MoveLast();
									rs.MoveFirst();
									strProcessDate = Strings.Format(rs.Get_Fields_DateTime("IssueDate"), "MM/dd/yyyy");
								}
								else
								{
									strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
								}
							}
							else
							{
								strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							}
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
						else
						{
							strProcessDate = Strings.Format(frmReport.InstancePtr.cboStart.Text, "MM/dd/yyyy");
							strProcessDate += "-" + Strings.Format(frmReport.InstancePtr.cboEnd.Text, "MM/dd/yyyy");
						}
					}
				}
				else
				{
					strProcessDate = "";
				}
			}
			else
			{
				strProcessDate = Strings.StrDup(23, " ");
			}
			rs1.OpenRecordset("SELECT * FROM DefaultInfo");
			if (rs1.EndOfFile() != true && rs1.BeginningOfFile() != true)
			{
				rs1.MoveLast();
				rs1.MoveFirst();
				txtMuni.Text = rs1.Get_Fields_String("ReportTown");
				txtTownCounty.Text = Strings.Format(rs1.Get_Fields_String("ResidenceCode"), "00000");
				txtAgent.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(rs1.Get_Fields_String("ReportAgent")));
				txtPhone.Text = rs1.Get_Fields_String("ReportTelephone");
				txtDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
				txtProcess.Text = strProcessDate;
				txtAuthType.Text = strLevel;
				txtDateReceived.Text = "___/___/___";
				txtVendorId.Text = "TRIO";
				txtVersionUpdateNumber.Text = Application.ProductVersion;
			}
			rs.DisposeOf();
			rs1.DisposeOf();
		}
	}
}
