﻿namespace TWMV0000
{
	/// <summary>
	/// Summary description for rptFleetRegistrationInfo.
	/// </summary>
	partial class rptFleetRegistrationInfo
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptFleetRegistrationInfo));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFleetNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFleetOwner = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExpirationMonth = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUnit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldClassPlate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVIN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUnit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBadFees = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalLocal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalAgentFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalExcise = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalLocalSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalRegistrationFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalInitialFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalStateSummary = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalSpecialtyFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFleetNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFleetOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpirationMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBadFees)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLocal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalState)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAgentFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalExcise)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLocalSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegistrationFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInitialFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalStateSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalSpecialtyFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldClassPlate,
				this.fldVIN,
				this.fldYear,
				this.fldMake,
				this.fldModel,
				this.fldUnit,
				this.fldLocal,
				this.fldState,
				this.fldTotal,
				this.lblBadFees
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.lblFleetNumber,
				this.lblFleetOwner,
				this.lblExpirationMonth,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.lblUnit,
				this.Line2,
				this.Label21,
				this.Label22,
				this.Label23
			});
			this.PageHeader.Height = 1.104167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			//
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label25,
				this.Label26,
				this.fldTotalAgentFee,
				this.Label27,
				this.fldTotalExcise,
				this.Line4,
				this.Label28,
				this.fldTotalLocalSummary,
				this.Label29,
				this.Label30,
				this.fldTotalRegistrationFee,
				this.Label31,
				this.fldTotalInitialFee,
				this.Line5,
				this.Label32,
				this.fldTotalStateSummary,
				this.Label33,
				this.fldTotalSpecialtyFee,
				this.Label34,
				this.fldGrandTotal,
				this.Label35
			});
			this.GroupFooter1.Height = 3.645833F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblType,
				this.lblDate,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Line1,
				this.Label12,
				this.Label13,
				this.Label14
			});
			this.GroupHeader2.Height = 0F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			//
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line3,
				this.fldTotalLocal,
				this.fldTotalState,
				this.fldTotalTotal,
				this.Label24
			});
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Fleet Registration Listing";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.6875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label8.Text = "Label8";
			this.Label8.Top = 0.1875F;
			this.Label8.Width = 1.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.Label9.Text = "Label9";
			this.Label9.Top = 0F;
			this.Label9.Width = 1.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 6.1875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0.1875F;
			this.Label10.Width = 1.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.1875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.Label11.Text = "Label11";
			this.Label11.Top = 0F;
			this.Label11.Width = 1.3125F;
			// 
			// lblFleetNumber
			// 
			this.lblFleetNumber.Height = 0.1875F;
			this.lblFleetNumber.HyperLink = null;
			this.lblFleetNumber.Left = 1.5F;
			this.lblFleetNumber.Name = "lblFleetNumber";
			this.lblFleetNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblFleetNumber.Text = "Fleet Registration Listing";
			this.lblFleetNumber.Top = 0.25F;
			this.lblFleetNumber.Width = 4.6875F;
			// 
			// lblFleetOwner
			// 
			this.lblFleetOwner.Height = 0.1875F;
			this.lblFleetOwner.HyperLink = null;
			this.lblFleetOwner.Left = 1.5F;
			this.lblFleetOwner.Name = "lblFleetOwner";
			this.lblFleetOwner.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblFleetOwner.Text = "Fleet Registration Listing";
			this.lblFleetOwner.Top = 0.4375F;
			this.lblFleetOwner.Width = 4.6875F;
			// 
			// lblExpirationMonth
			// 
			this.lblExpirationMonth.Height = 0.1875F;
			this.lblExpirationMonth.HyperLink = null;
			this.lblExpirationMonth.Left = 1.5F;
			this.lblExpirationMonth.Name = "lblExpirationMonth";
			this.lblExpirationMonth.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblExpirationMonth.Text = "Fleet Registration Listing";
			this.lblExpirationMonth.Top = 0.625F;
			this.lblExpirationMonth.Width = 4.6875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label15.Text = "Cl \\ Plate";
			this.Label15.Top = 0.90625F;
			this.Label15.Width = 0.84375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.90625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label16.Text = "VIN";
			this.Label16.Top = 0.90625F;
			this.Label16.Width = 1.40625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 2.375F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label17.Text = "Year";
			this.Label17.Top = 0.90625F;
			this.Label17.Width = 0.5F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.90625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label18.Text = "Make";
			this.Label18.Top = 0.90625F;
			this.Label18.Width = 0.375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3.34375F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label19.Text = "Model";
			this.Label19.Top = 0.90625F;
			this.Label19.Width = 0.5F;
			// 
			// lblUnit
			// 
			this.lblUnit.Height = 0.1875F;
			this.lblUnit.HyperLink = null;
			this.lblUnit.Left = 3.90625F;
			this.lblUnit.Name = "lblUnit";
			this.lblUnit.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblUnit.Text = "Unit";
			this.lblUnit.Top = 0.90625F;
			this.lblUnit.Width = 0.71875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.03125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.09375F;
			this.Line2.Width = 7.4375F;
			this.Line2.X1 = 0.03125F;
			this.Line2.X2 = 7.46875F;
			this.Line2.Y1 = 1.09375F;
			this.Line2.Y2 = 1.09375F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.78125F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label21.Text = "Local";
			this.Label21.Top = 0.90625F;
			this.Label21.Width = 0.8125F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 5.65625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label22.Text = "State";
			this.Label22.Top = 0.90625F;
			this.Label22.Width = 0.8125F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 6.53125F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label23.Text = "Total Fee";
			this.Label23.Top = 0.90625F;
			this.Label23.Width = 0.8125F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 0F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblType.Text = "Cl \\ Plate";
			this.lblType.Top = 0.90625F;
			this.lblType.Width = 0.84375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 0.90625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblDate.Text = "VIN";
			this.lblDate.Top = 0.90625F;
			this.lblDate.Width = 1.53125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.5F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label2.Text = "Year";
			this.Label2.Top = 0.90625F;
			this.Label2.Width = 0.5F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.03125F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label3.Text = "Make";
			this.Label3.Top = 0.90625F;
			this.Label3.Width = 0.375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.46875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label4.Text = "Model";
			this.Label4.Top = 0.90625F;
			this.Label4.Width = 0.5F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.03125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label5.Text = "Unit";
			this.Label5.Top = 0.90625F;
			this.Label5.Width = 0.53125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.09375F;
			this.Line1.Width = 7.4375F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 7.46875F;
			this.Line1.Y1 = 1.09375F;
			this.Line1.Y2 = 1.09375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label12.Text = "Local";
			this.Label12.Top = 0.90625F;
			this.Label12.Width = 0.90625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.59375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label13.Text = "State";
			this.Label13.Top = 0.90625F;
			this.Label13.Width = 0.90625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 6.5625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label14.Text = "Total Fee";
			this.Label14.Top = 0.90625F;
			this.Label14.Width = 0.90625F;
			// 
			// fldClassPlate
			// 
			this.fldClassPlate.Height = 0.19F;
			this.fldClassPlate.Left = 0F;
			this.fldClassPlate.Name = "fldClassPlate";
			this.fldClassPlate.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldClassPlate.Text = "Field1";
			this.fldClassPlate.Top = 0F;
			this.fldClassPlate.Width = 0.84375F;
			// 
			// fldVIN
			// 
			this.fldVIN.Height = 0.19F;
			this.fldVIN.Left = 0.90625F;
			this.fldVIN.Name = "fldVIN";
			this.fldVIN.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldVIN.Text = "Field1";
			this.fldVIN.Top = 0F;
			this.fldVIN.Width = 1.40625F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.19F;
			this.fldYear.Left = 2.34375F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldYear.Text = "Field1";
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.5F;
			// 
			// fldMake
			// 
			this.fldMake.Height = 0.19F;
			this.fldMake.Left = 2.875F;
			this.fldMake.Name = "fldMake";
			this.fldMake.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldMake.Text = "Field1";
			this.fldMake.Top = 0F;
			this.fldMake.Width = 0.375F;
			// 
			// fldModel
			// 
			this.fldModel.Height = 0.19F;
			this.fldModel.Left = 3.3125F;
			this.fldModel.Name = "fldModel";
			this.fldModel.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldModel.Text = "Field1";
			this.fldModel.Top = 0F;
			this.fldModel.Width = 0.5F;
			// 
			// fldUnit
			// 
			this.fldUnit.Height = 0.19F;
			this.fldUnit.Left = 3.875F;
			this.fldUnit.Name = "fldUnit";
			this.fldUnit.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldUnit.Text = "Field1";
			this.fldUnit.Top = 0F;
			this.fldUnit.Width = 0.75F;
			// 
			// fldLocal
			// 
			this.fldLocal.Height = 0.19F;
			this.fldLocal.Left = 4.75F;
			this.fldLocal.Name = "fldLocal";
			this.fldLocal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldLocal.Text = "Field1";
			this.fldLocal.Top = 0F;
			this.fldLocal.Width = 0.8125F;
			// 
			// fldState
			// 
			this.fldState.Height = 0.19F;
			this.fldState.Left = 5.625F;
			this.fldState.Name = "fldState";
			this.fldState.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldState.Text = "Field1";
			this.fldState.Top = 0F;
			this.fldState.Width = 0.8125F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.19F;
			this.fldTotal.Left = 6.5F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 0.8125F;
			// 
			// lblBadFees
			// 
			this.lblBadFees.Height = 0.19F;
			this.lblBadFees.HyperLink = null;
			this.lblBadFees.Left = 7.3125F;
			this.lblBadFees.Name = "lblBadFees";
			this.lblBadFees.Style = "font-size: 18pt";
			this.lblBadFees.Text = "*";
			this.lblBadFees.Top = 0F;
			this.lblBadFees.Visible = false;
			this.lblBadFees.Width = 0.15625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.71875F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 2.78125F;
			this.Line3.X1 = 4.71875F;
			this.Line3.X2 = 7.5F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// fldTotalLocal
			// 
			this.fldTotalLocal.Height = 0.19F;
			this.fldTotalLocal.Left = 4.75F;
			this.fldTotalLocal.Name = "fldTotalLocal";
			this.fldTotalLocal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalLocal.Text = "Field1";
			this.fldTotalLocal.Top = 0.0625F;
			this.fldTotalLocal.Width = 0.8125F;
			// 
			// fldTotalState
			// 
			this.fldTotalState.Height = 0.19F;
			this.fldTotalState.Left = 5.625F;
			this.fldTotalState.Name = "fldTotalState";
			this.fldTotalState.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalState.Text = "Field1";
			this.fldTotalState.Top = 0.0625F;
			this.fldTotalState.Width = 0.8125F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.19F;
			this.fldTotalTotal.Left = 6.5F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalTotal.Text = "Field1";
			this.fldTotalTotal.Top = 0.0625F;
			this.fldTotalTotal.Width = 0.8125F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 3.84375F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label24.Text = "Totals";
			this.Label24.Top = 0.0625F;
			this.Label24.Width = 0.75F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 2.96875F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.Label25.Text = "LOCAL FEES";
			this.Label25.Top = 0.125F;
			this.Label25.Width = 1.5625F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.19F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 2.3125F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label26.Text = "AGENT FEE:";
			this.Label26.Top = 0.4375F;
			this.Label26.Width = 1.0625F;
			// 
			// fldTotalAgentFee
			// 
			this.fldTotalAgentFee.Height = 0.19F;
			this.fldTotalAgentFee.Left = 4.0625F;
			this.fldTotalAgentFee.Name = "fldTotalAgentFee";
			this.fldTotalAgentFee.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalAgentFee.Text = "Field1";
			this.fldTotalAgentFee.Top = 0.4375F;
			this.fldTotalAgentFee.Width = 1.0625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.19F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 2.3125F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label27.Text = "EXCISE TAX:";
			this.Label27.Top = 0.59375F;
			this.Label27.Width = 1.0625F;
			// 
			// fldTotalExcise
			// 
			this.fldTotalExcise.Height = 0.19F;
			this.fldTotalExcise.Left = 4.0625F;
			this.fldTotalExcise.Name = "fldTotalExcise";
			this.fldTotalExcise.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalExcise.Text = "Field1";
			this.fldTotalExcise.Top = 0.59375F;
			this.fldTotalExcise.Width = 1.0625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 2.3125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.78125F;
			this.Line4.Width = 2.8125F;
			this.Line4.X1 = 2.3125F;
			this.Line4.X2 = 5.125F;
			this.Line4.Y1 = 0.78125F;
			this.Line4.Y2 = 0.78125F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.19F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 2.3125F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label28.Text = "TOTAL LOCAL:";
			this.Label28.Top = 0.8125F;
			this.Label28.Width = 1.0625F;
			// 
			// fldTotalLocalSummary
			// 
			this.fldTotalLocalSummary.Height = 0.19F;
			this.fldTotalLocalSummary.Left = 4.0625F;
			this.fldTotalLocalSummary.Name = "fldTotalLocalSummary";
			this.fldTotalLocalSummary.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalLocalSummary.Text = "Field1";
			this.fldTotalLocalSummary.Top = 0.8125F;
			this.fldTotalLocalSummary.Width = 1.0625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 2.96875F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.Label29.Text = "STATE FEES";
			this.Label29.Top = 1.21875F;
			this.Label29.Width = 1.5625F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.19F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 2.3125F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label30.Text = "REGISTRATION FEE:";
			this.Label30.Top = 1.53125F;
			this.Label30.Width = 1.6875F;
			// 
			// fldTotalRegistrationFee
			// 
			this.fldTotalRegistrationFee.Height = 0.19F;
			this.fldTotalRegistrationFee.Left = 4.0625F;
			this.fldTotalRegistrationFee.Name = "fldTotalRegistrationFee";
			this.fldTotalRegistrationFee.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalRegistrationFee.Text = "Field1";
			this.fldTotalRegistrationFee.Top = 1.53125F;
			this.fldTotalRegistrationFee.Width = 1.0625F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.19F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 2.3125F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label31.Text = "VANITY PLATE FEE:";
			this.Label31.Top = 1.6875F;
			this.Label31.Width = 1.6875F;
			// 
			// fldTotalInitialFee
			// 
			this.fldTotalInitialFee.Height = 0.19F;
			this.fldTotalInitialFee.Left = 4.0625F;
			this.fldTotalInitialFee.Name = "fldTotalInitialFee";
			this.fldTotalInitialFee.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalInitialFee.Text = "Field1";
			this.fldTotalInitialFee.Top = 1.6875F;
			this.fldTotalInitialFee.Width = 1.0625F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 2.3125F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.03125F;
			this.Line5.Width = 2.8125F;
			this.Line5.X1 = 2.3125F;
			this.Line5.X2 = 5.125F;
			this.Line5.Y1 = 2.03125F;
			this.Line5.Y2 = 2.03125F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.19F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 2.3125F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label32.Text = "TOTAL STATE:";
			this.Label32.Top = 2.0625F;
			this.Label32.Width = 1.0625F;
			// 
			// fldTotalStateSummary
			// 
			this.fldTotalStateSummary.Height = 0.19F;
			this.fldTotalStateSummary.Left = 4.0625F;
			this.fldTotalStateSummary.Name = "fldTotalStateSummary";
			this.fldTotalStateSummary.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalStateSummary.Text = "Field1";
			this.fldTotalStateSummary.Top = 2.0625F;
			this.fldTotalStateSummary.Width = 1.0625F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.19F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 2.3125F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label33.Text = "SPECIALTY PLATE FEE:";
			this.Label33.Top = 1.84375F;
			this.Label33.Width = 1.6875F;
			// 
			// fldTotalSpecialtyFee
			// 
			this.fldTotalSpecialtyFee.Height = 0.19F;
			this.fldTotalSpecialtyFee.Left = 4.0625F;
			this.fldTotalSpecialtyFee.Name = "fldTotalSpecialtyFee";
			this.fldTotalSpecialtyFee.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotalSpecialtyFee.Text = "Field1";
			this.fldTotalSpecialtyFee.Top = 1.84375F;
			this.fldTotalSpecialtyFee.Width = 1.0625F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.19F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 2.3125F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label34.Text = "GRAND TOTAL:";
			this.Label34.Top = 2.46875F;
			this.Label34.Width = 1.0625F;
			// 
			// fldGrandTotal
			// 
			this.fldGrandTotal.Height = 0.19F;
			this.fldGrandTotal.Left = 4.0625F;
			this.fldGrandTotal.Name = "fldGrandTotal";
			this.fldGrandTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldGrandTotal.Text = "Field1";
			this.fldGrandTotal.Top = 2.46875F;
			this.fldGrandTotal.Width = 1.0625F;
			// 
			// Label35
			// 
			this.Label35.Height = 0.1875F;
			this.Label35.HyperLink = null;
			this.Label35.Left = 0.40625F;
			this.Label35.Name = "Label35";
			this.Label35.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.Label35.Text = "WARNING - Fees may not be accurate due to missing information";
			this.Label35.Top = 3.21875F;
			this.Label35.Width = 6.71875F;
			// 
			// rptFleetRegistrationInfo
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFleetNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFleetOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpirationMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassPlate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUnit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBadFees)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLocal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalState)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAgentFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalExcise)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLocalSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegistrationFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInitialFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalStateSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalSpecialtyFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassPlate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVIN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBadFees;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFleetNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFleetOwner;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExpirationMonth;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUnit;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAgentFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalExcise;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalLocalSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalRegistrationFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInitialFee;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalStateSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalSpecialtyFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalLocal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
	}
}
